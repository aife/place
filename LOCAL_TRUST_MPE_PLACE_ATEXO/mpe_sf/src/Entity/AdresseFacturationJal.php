<?php

namespace App\Entity;

use App\Repository\AdresseFacturationJalRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AdresseFacturationJalRepository::class)
 * @ORM\Table(name="AdresseFacturationJal")
 */
class AdresseFacturationJal
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $organisme;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $oldServiceId;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $EmailAr;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $telecopie;

    /**
     * @ORM\Column(type="text")
     */
    private $informationFacturation;

    /**
     * @ORM\Column(type="boolean")
     */
    private $facturationSip = false;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $idBlobLogo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomFichier;

    /**
     * @ORM\ManyToOne(targetEntity=Service::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $service;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrganisme(): ?string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme): self
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getOldServiceId(): ?int
    {
        return $this->oldServiceId;
    }

    public function setOldServiceId(?int $oldServiceId): self
    {
        $this->oldServiceId = $oldServiceId;

        return $this;
    }

    public function getEmailAr(): ?string
    {
        return $this->EmailAr;
    }

    public function setEmailAr(string $EmailAr): self
    {
        $this->EmailAr = $EmailAr;

        return $this;
    }

    public function getTelecopie(): ?string
    {
        return $this->telecopie;
    }

    public function setTelecopie(string $telecopie): self
    {
        $this->telecopie = $telecopie;

        return $this;
    }

    public function getInformationFacturation(): ?string
    {
        return $this->informationFacturation;
    }

    public function setInformationFacturation(string $informationFacturation): self
    {
        $this->informationFacturation = $informationFacturation;

        return $this;
    }

    public function getFacturationSip(): ?bool
    {
        return $this->facturationSip;
    }

    public function setFacturationSip(bool $facturationSip): self
    {
        $this->facturationSip = $facturationSip;

        return $this;
    }

    public function getIdBlobLogo(): ?int
    {
        return $this->idBlobLogo;
    }

    public function setIdBlobLogo(?int $idBlobLogo): self
    {
        $this->idBlobLogo = $idBlobLogo;

        return $this;
    }

    public function getNomFichier(): ?string
    {
        return $this->nomFichier;
    }

    public function setNomFichier(?string $nomFichier): self
    {
        $this->nomFichier = $nomFichier;

        return $this;
    }

    public function getService(): ?Service
    {
        return $this->service;
    }

    public function setService(?Service $service): self
    {
        $this->service = $service;

        return $this;
    }
}
