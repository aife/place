<?php

namespace App\Entity\Offre;

use DateTime;
use App\Entity\Agent;
use App\Entity\BloborganismeFile;
use App\Entity\Consultation;
use App\Repository\Offre\AnnexeFinanciereRepository;
use App\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Offre\AnnexeFinanciereRepository")
 * @ORM\Table(name="annexe_financiere")
 */
class AnnexeFinanciere
{
    use TimestampableTrait;

    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation", inversedBy="annexesFinanciereManuel")
     * @ORM\JoinColumn(name="id_consultation", referencedColumnName="id")
     */
    private ?Consultation $consultation = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Agent", inversedBy="annexesFinanciereManuel")
     * @ORM\JoinColumn(name="id_agent", referencedColumnName="id")
     */
    private ?Agent $agent = null;

    /**
     * @var BloborganismeFile
     * @ORM\OneToOne (targetEntity="App\Entity\BloborganismeFile")
     * @ORM\JoinColumn(name="id_blob", referencedColumnName="id")
     */
    private ?BloborganismeFile $blob = null;

    /**
     * @ORM\Column(type="string", length=256)
     */
    private ?string $entreprise = null;

    /**
     * @var string
     *
     * @ORM\Column(type="integer", length=11)
     */
    private $numeroLot;

    /**
     * @var string
     *
     * @ORM\Column(type="integer", length=11)
     */
    private $numeroPli;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime")*/
    private $dateRemise;

    public function getId(): int
    {
        return $this->id;
    }

    public function getConsultation(): Consultation
    {
        return $this->consultation;
    }

    public function setConsultation(Consultation $consultation): self
    {
        $this->consultation = $consultation;

        return $this;
    }

    public function getAgent(): Agent
    {
        return $this->agent;
    }

    public function setAgent(Agent $agent): self
    {
        $this->agent = $agent;

        return $this;
    }

    public function getBlob(): BloborganismeFile
    {
        return $this->blob;
    }

    public function setBlob(BloborganismeFile $blob): self
    {
        $this->blob = $blob;

        return $this;
    }

    public function getEntreprise(): string
    {
        return $this->entreprise;
    }

    public function setEntreprise(string $entreprise): self
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    public function getNumeroLot(): ?string
    {
        return $this->numeroLot;
    }

    public function setNumeroLot(?string $numeroLot): self
    {
        $this->numeroLot = $numeroLot;

        return $this;
    }

    public function getNumeroPli(): ?string
    {
        return $this->numeroPli;
    }

    public function setNumeroPli(?string $numeroPli): self
    {
        $this->numeroPli = $numeroPli;

        return $this;
    }

    public function getDateRemise(): ?DateTime
    {
        return $this->dateRemise;
    }

    public function setDateRemise(?DateTime $dateRemise): self
    {
        $this->dateRemise = $dateRemise;

        return $this;
    }
}
