<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="t_support_publication")
 * @ORM\Entity(repositoryClass="App\Repository\TSupportPublicationRepository")
 */
class TSupportPublication
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="image_logo", type="string", nullable=true)
     */
    private ?string $imageLogo = null;

    /**
     * @ORM\Column(name="nom", type="string")
     */
    private ?string $nom = null;

    /**
     * @ORM\Column(name="visible", type="string")
     */
    private string $visible = '1';

    /**
     * @ORM\Column(name="ordre", type="string", nullable=true)
     */
    private ?string $ordre = null;

    /**
     * @ORM\Column(name="default_value", type="string")
     */
    private string $defaultValue = '0';

    /**
     * @ORM\Column(name="code", type="string", nullable=true)
     */
    private ?string $code = null;

    /**
     * @ORM\Column(name="actif", type="string")
     */
    private string $actif = '0';

    /**
     * @ORM\Column(name="url", type="string", nullable=true)
     */
    private ?string $url = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getImageLogo()
    {
        return $this->imageLogo;
    }

    /**
     * @param string $imageLogo
     *
     * @return $this
     */
    public function setImageLogo($imageLogo)
    {
        $this->imageLogo = $imageLogo;

        return $this;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     *
     * @return $this
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return string
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * @param string $visible
     *
     * @return $this
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * @param string $ordre
     *
     * @return $this
     */
    public function setOrdre($ordre)
    {
        $this->ordre = $ordre;

        return $this;
    }

    /**
     * @return string
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * @param string $defaultValue
     *
     * @return $this
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * @param string $actif
     *
     * @return $this
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }
}
