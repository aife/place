<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TEnveloppeDossierFormulaire.
 *
 * @ORM\Table(name="t_enveloppe_dossier_formulaire")
 * @ORM\Entity
 */
class TEnveloppeDossierFormulaire
{
    /**
     * @return int
     */
    public function getIdEnveloppeDossierFormulaire()
    {
        return $this->idEnveloppeDossierFormulaire;
    }

    /**
     * @param int $idEnveloppeDossierFormulaire
     */
    public function setIdEnveloppeDossierFormulaire($idEnveloppeDossierFormulaire)
    {
        $this->idEnveloppeDossierFormulaire = $idEnveloppeDossierFormulaire;
    }

    /**
     *
     * @ORM\Column(name="id_enveloppe_dossier_formulaire", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $idEnveloppeDossierFormulaire = null;

    /**
     * @ORM\Column(name="id_dossier_formulaire", type="integer", nullable=false)
     */
    private ?int $idDossierFormulaire = null;

    /**
     * @return int
     */
    public function getIdDossierFormulaire()
    {
        return $this->idDossierFormulaire;
    }

    /**
     * @param int $idDossierFormulaire
     */
    public function setIdDossierFormulaire($idDossierFormulaire)
    {
        $this->idDossierFormulaire = $idDossierFormulaire;
    }

    /**
     * @return int
     */
    public function getIdEnveloppe()
    {
        return $this->idEnveloppe;
    }

    /**
     * @param int $idEnveloppe
     */
    public function setIdEnveloppe($idEnveloppe)
    {
        $this->idEnveloppe = $idEnveloppe;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param string $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return int
     */
    public function getIdLot()
    {
        return $this->idLot;
    }

    /**
     * @param int $idLot
     */
    public function setIdLot($idLot)
    {
        $this->idLot = $idLot;
    }

    /**
     * @return int
     */
    public function getTypeEnveloppe()
    {
        return $this->typeEnveloppe;
    }

    /**
     * @param int $typeEnveloppe
     */
    public function setTypeEnveloppe($typeEnveloppe)
    {
        $this->typeEnveloppe = $typeEnveloppe;
    }

    /**
     * @return string
     */
    public function getLibelleForrmulaire()
    {
        return $this->libelleForrmulaire;
    }

    /**
     * @param string $libelleForrmulaire
     */
    public function setLibelleForrmulaire($libelleForrmulaire)
    {
        $this->libelleForrmulaire = $libelleForrmulaire;
    }

    /**
     * @return int
     */
    public function getCleExterneDispositif()
    {
        return $this->cleExterneDispositif;
    }

    /**
     * @param int $cleExterneDispositif
     */
    public function setCleExterneDispositif($cleExterneDispositif)
    {
        $this->cleExterneDispositif = $cleExterneDispositif;
    }

    /**
     * @return int
     */
    public function getCleExterneDossier()
    {
        return $this->cleExterneDossier;
    }

    /**
     * @param int $cleExterneDossier
     */
    public function setCleExterneDossier($cleExterneDossier)
    {
        $this->cleExterneDossier = $cleExterneDossier;
    }

    /**
     * @return string
     */
    public function getStatutValidation()
    {
        return $this->statutValidation;
    }

    /**
     * @param string $statutValidation
     */
    public function setStatutValidation($statutValidation)
    {
        $this->statutValidation = $statutValidation;
    }

    /**
     * @return string
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param string $dateCreation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }

    /**
     * @return string
     */
    public function getDateModif()
    {
        return $this->dateModif;
    }

    /**
     * @param string $dateModif
     */
    public function setDateModif($dateModif)
    {
        $this->dateModif = $dateModif;
    }

    /**
     * @return string
     */
    public function getDateValidation()
    {
        return $this->dateValidation;
    }

    /**
     * @param string $dateValidation
     */
    public function setDateValidation($dateValidation)
    {
        $this->dateValidation = $dateValidation;
    }

    /**
     * @return int
     */
    public function getStatutGenerationGlobale()
    {
        return $this->statutGenerationGlobale;
    }

    /**
     * @param int $statutGenerationGlobale
     */
    public function setStatutGenerationGlobale($statutGenerationGlobale)
    {
        $this->statutGenerationGlobale = $statutGenerationGlobale;
    }

    /**
     * @return int
     */
    public function getTypeReponse()
    {
        return $this->typeReponse;
    }

    /**
     * @param int $typeReponse
     */
    public function setTypeReponse($typeReponse)
    {
        $this->typeReponse = $typeReponse;
    }

    /**
     * @return int
     */
    public function getCleExterneFormulaire()
    {
        return $this->cleExterneFormulaire;
    }

    /**
     * @param int $cleExterneFormulaire
     */
    public function setCleExterneFormulaire($cleExterneFormulaire)
    {
        $this->cleExterneFormulaire = $cleExterneFormulaire;
    }

    /**
     * @ORM\Column(name="id_enveloppe", type="integer", nullable=false)
     */
    private ?int $idEnveloppe = null;

    /**
     * @ORM\Column(name="organisme", type="string", length=30, nullable=false)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(name="id_lot", type="integer", nullable=true)
     */
    private ?int $idLot = null;

    /**
     * @ORM\Column(name="type_enveloppe", type="integer", nullable=false)
     */
    private ?int $typeEnveloppe = null;

    /**
     * @ORM\Column(name="libelle_forrmulaire", type="string", length=255, nullable=false)
     */
    private ?string $libelleForrmulaire = null;

    /**
     * @ORM\Column(name="cle_externe_dispositif", type="integer", nullable=false)
     */
    private ?int $cleExterneDispositif = null;

    /**
     * @ORM\Column(name="cle_externe_dossier", type="integer", nullable=true)
     */
    private ?int $cleExterneDossier = null;

    /**
     * @ORM\Column(name="statut_validation", type="string", nullable=false)
     */
    private ?string $statutValidation = null;

    /**
     * @ORM\Column(name="date_creation", type="string", length=255, nullable=true)
     */
    private ?string $dateCreation = null;

    /**
     * @ORM\Column(name="date_modif", type="string", length=255, nullable=true)
     */
    private ?string $dateModif = null;

    /**
     * @ORM\Column(name="date_validation", type="string", length=255, nullable=true)
     */
    private ?string $dateValidation = null;

    /**
     * @ORM\Column(name="statut_generation_globale", type="integer", nullable=true)
     */
    private ?int $statutGenerationGlobale = null;

    /**
     * @var int
     *
     * @ORM\Column(name="type_reponse", type="integer", nullable=true)
     */
    private $typeReponse = '1';

    /**
     * @ORM\Column(name="cle_externe_formulaire", type="integer", nullable=true)
     */
    private ?int $cleExterneFormulaire = null;
}
