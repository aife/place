<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="Destinataire_Pub")
 * @ORM\Entity(repositoryClass="App\Repository\DestinatairePubRepository")
 */
class DestinatairePub
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="organisme", type="string")
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(name="id_avis", type="integer")
     */
    private ?int $idAvis = null;

    /**
     * @ORM\Column(name="id_support", type="integer")
     */
    private ?int $idSupport = null;

    /**
     * @ORM\Column(name="etat", type="string")
     */
    private string|int $etat = '0';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_modification", type="datetime")*/
    private $dateModification;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_publication", type="datetime", nullable=true)*/
    private $datePublication;

    /**
     * @ORM\Column(name="id_dispositif", type="integer")
     */
    private int $idDispositif = 0;

    /**
     * @ORM\Column(name="id_dossier", type="integer")
     */
    private int $idDossier = 0;

    /**
     * @ORM\Column(name="type_pub", type="integer")
     */
    private int $typePub = 0;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param string $organisme
     *
     * @return $this
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdAvis()
    {
        return $this->idAvis;
    }

    /**
     * @param int $idAvis
     *
     * @return $this
     */
    public function setIdAvis($idAvis)
    {
        $this->idAvis = $idAvis;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdSupport()
    {
        return $this->idSupport;
    }

    /**
     * @param int $idSupport
     *
     * @return $this
     */
    public function setIdSupport($idSupport)
    {
        $this->idSupport = $idSupport;

        return $this;
    }

    /**
     * @return int
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param int $etat
     *
     * @return $this
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }

    /**
     * @param DateTime $dateModification
     *
     * @return $this
     */
    public function setDateModification($dateModification)
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDatePublication()
    {
        return $this->datePublication;
    }

    /**
     * @param DateTime $datePublication
     *
     * @return $this
     */
    public function setDatePublication($datePublication)
    {
        $this->datePublication = $datePublication;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdDispositif()
    {
        return $this->idDispositif;
    }

    /**
     * @param int $idDispositif
     *
     * @return $this
     */
    public function setIdDispositif($idDispositif)
    {
        $this->idDispositif = $idDispositif;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdDossier()
    {
        return $this->idDossier;
    }

    /**
     * @param int $idDossier
     *
     * @return $this
     */
    public function setIdDossier($idDossier)
    {
        $this->idDossier = $idDossier;

        return $this;
    }

    /**
     * @return int
     */
    public function getTypePub()
    {
        return $this->typePub;
    }

    /**
     * @param int $typePub
     *
     * @return $this
     */
    public function setTypePub($typePub)
    {
        $this->typePub = $typePub;

        return $this;
    }
}
