<?php

namespace App\Entity;

use App\Entity\Procedure\TypeProcedureOrganisme;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\TypeContratEtTypeProcedureRepository;

/**
 * Class TypeContrat.
 *
 * @ORM\Table(name="t_type_contrat_et_procedure")
 * @ORM\Entity(repositoryClass=TypeContratEtTypeProcedureRepository::class)
 */
class TypeContratEtTypeProcedure
{
    public const FOREIGN_KEYS = [
        'organisme' => [
            'class' => Organisme::class,
            'property' => 'acronyme',
            'isObject' => false,
            'migrationService' => 'atexo.service.migration.organisme',
        ],
        'idTypeProcedure' => [
            'class' => TypeProcedureOrganisme::class,
            'property' => [
                'idTypeProcedure',
                'organisme',
            ],
            'isObject' => true,
            'isCompositePrimaryKey' => true,
            'migrationService' => 'atexo.service.migration.typeProcedureOrganisme',
        ],
    ];

    /**
     *
     * @ORM\Column(name="id_type_contrat_et_procedure", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $idTypeContratEtProcedure = null;

    /**
     * @ORM\Column(name="id_type_contrat", type="integer", nullable=false)
     */
    private string|int $idTypeContrat = '0';

    /**
     * @ORM\ManyToOne(
     *      targetEntity="App\Entity\Procedure\TypeProcedureOrganisme",
     *      inversedBy="typeContratEtProcedure",
     *      fetch="EAGER"
     * )
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_procedure", referencedColumnName="id_type_procedure"),
     *   @ORM\JoinColumn(name="organisme", referencedColumnName="organisme"),
     * })
     */
    private ?TypeProcedureOrganisme $idTypeProcedure;

    /**
     * @ORM\Column(name="organisme", type="string", length=30, nullable=false)
     */
    private ?string $organisme = null;

    /**
     * @return int
     */
    public function getIdTypeContratEtProcedure()
    {
        return $this->idTypeContratEtProcedure;
    }

    /**
     * @param ?int $idTypeContratEtProcedure
     */
    public function setIdTypeContratEtProcedure($idTypeContratEtProcedure)
    {
        $this->idTypeContratEtProcedure = $idTypeContratEtProcedure;
    }

    /**
     * @return int
     */
    public function getIdTypeContrat()
    {
        return $this->idTypeContrat;
    }

    /**
     * @param int $idTypeContrat
     */
    public function setIdTypeContrat($idTypeContrat)
    {
        $this->idTypeContrat = $idTypeContrat;
    }

    public function getIdTypeProcedure()
    {
        return $this->idTypeProcedure;
    }

    public function setIdTypeProcedure($idTypeProcedure)
    {
        $this->idTypeProcedure = $idTypeProcedure;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param string $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }
}
