<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TListeLotsCandidature.
 *
 * @ORM\Table(name="t_liste_lots_candidature")
 * @ORM\Entity(repositoryClass="App\Repository\TListeLotsCandidatureRepository")
 */
class TListeLotsCandidature
{
    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation", inversedBy="listLots", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="consultation_id", referencedColumnName="id")
     */
    private ?Consultation $consultation = null;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme", inversedBy="listLots")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private ?Organisme $organisme = null;

    /**
     * @ORM\Column(name="id_inscrit", type="integer", nullable=false)
     */
    private ?int $idInscrit = null;

    /**
     * @ORM\Column(name="id_entreprise", type="integer", nullable=false)
     */
    private ?int $idEntreprise = null;

    /**
     * @ORM\Column(name="id_etablissement", type="integer", nullable=false)
     */
    private ?int $idEtablissement = null;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private int $status = 99;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\TCandidature", inversedBy="lots")
     * @ORM\JoinColumn(name="id_candidature", referencedColumnName="id")
     */
    private ?TCandidature $candidature = null;

    /**
     * @ORM\Column(name="num_lot", type="integer", nullable=false)
     */
    private ?int $numLot = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Consultation
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    /**
     * @param Consultation $consultation
     */
    public function setConsultation($consultation)
    {
        $this->consultation = $consultation;
    }

    /**
     * @return Organisme
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param Organisme $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return int
     */
    public function getIdInscrit()
    {
        return $this->idInscrit;
    }

    /**
     * @param int $idInscrit
     */
    public function setIdInscrit($idInscrit)
    {
        $this->idInscrit = $idInscrit;
    }

    /**
     * @return int
     */
    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    /**
     * @param int $idEntreprise
     */
    public function setIdEntreprise($idEntreprise)
    {
        $this->idEntreprise = $idEntreprise;
    }

    /**
     * @return int
     */
    public function getIdEtablissement()
    {
        return $this->idEtablissement;
    }

    /**
     * @param int $idEtablissement
     */
    public function setIdEtablissement($idEtablissement)
    {
        $this->idEtablissement = $idEtablissement;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return TCandidature
     */
    public function getCandidature()
    {
        return $this->candidature;
    }

    /**
     * @param TCandidature $candidature
     */
    public function setCandidature($candidature)
    {
        $this->candidature = $candidature;
    }

    /**
     * @return int
     */
    public function getNumLot()
    {
        return $this->numLot;
    }

    /**
     * @param int $numLot
     */
    public function setNumLot($numLot)
    {
        $this->numLot = $numLot;
    }
}
