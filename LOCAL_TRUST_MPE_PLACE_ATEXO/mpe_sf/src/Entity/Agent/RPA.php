<?php

namespace App\Entity\Agent;

use Exception;
use App\Entity\Organisme;
use App\Entity\Service;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Table(name="RPA", uniqueConstraints={
 *          @UniqueConstraint(name="rpa_id",columns={"id"}),
 *          @UniqueConstraint(name="rpa_acronyme_org",columns={"acronymeOrg"})
 *      })
 * @ORM\Entity(repositoryClass="App\Repository\Agent\RPARepository")
 */
class RPA
{
    private EntityManager $em;

    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=200, name="acronymeOrg")
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme")
     */
    private $acronymeOrg;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private ?string $nom = null;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private ?string $prenom = null;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private ?string $adresse1 = null;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private ?string $adresse2 = null;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private ?string $codepostal = null;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private ?string $ville = null;

    /**
     * @ORM\Column(name="service_id", type="integer", nullable=true)
     */
    private ?int $serviceId = null;

    /**
     * @ORM\Column(type="string", length=200, name="Fonction", nullable=true)
     */
    private ?string $fonction = null;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private ?string $pays = null;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private ?string $dateCreation = null;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private ?string $dateModification = null;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private string $responsableArchive = '0';

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private ?string $email = null;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private ?string $telephone = null;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private ?string $fax = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getAcronymeOrg(): string
    {
        return $this->acronymeOrg;
    }

    public function setAcronymeOrg(string $acronymeOrg)
    {
        $this->acronymeOrg = $acronymeOrg;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme)
    {
        $this->organisme = $organisme;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }

    public function getAdresse1(): string
    {
        return $this->adresse1;
    }

    public function setAdresse1(string $adresse1)
    {
        $this->adresse1 = $adresse1;
    }

    public function getAdresse2(): string
    {
        return $this->adresse2;
    }

    public function setAdresse2(string $adresse2)
    {
        $this->adresse2 = $adresse2;
    }

    public function getCodepostal(): string
    {
        return $this->codepostal;
    }

    public function setCodepostal(string $codepostal)
    {
        $this->codepostal = $codepostal;
    }

    public function getVille(): string
    {
        return $this->ville;
    }

    public function setVille(string $ville)
    {
        $this->ville = $ville;
    }

    public function getServiceId(): int
    {
        return $this->serviceId;
    }

    public function setServiceId(int $idService = null)
    {
        $this->idService = $idService;
    }

    /**
     * @return string
     */
    public function getFonction()
    {
        return $this->fonction;
    }

    public function setFonction(string $fonction = null)
    {
        $this->fonction = $fonction;
    }

    /**
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    public function setPays(string $pays = null)
    {
        $this->pays = $pays;
    }

    /**
     * @return string
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    public function setDateCreation(string $dateCreation = null)
    {
        $this->dateCreation = $dateCreation;
    }

    /**
     * @return string
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }

    public function setDateModification(string $dateModification = null)
    {
        $this->dateModification = $dateModification;
    }

    public function getResponsableArchive(): string
    {
        return $this->responsableArchive;
    }

    public function setResponsableArchive(string $responsableArchive = '0')
    {
        $this->responsableArchive = $responsableArchive;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail(string $email = null)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone = null)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    public function setFax(string $fax = null)
    {
        $this->fax = $fax;
    }

    /**
     * Cette fonction permet de copier le RPA lui attribuant un nouveau service et un nouvel organisme de destination passés en paramètres.
     *
     * @param int    $idServiceDestination identifiant du service de destination
     * @param string $organismeDestination acronyme de l'organisme de destination dans lequel se trouve le clone
     *
     * @return bool|RPA false si le clone n'a pas réussi, sinon le nouvel objet RPA créé
     *
     * @throws Exception
     */
    public function copier(int $idServiceDestination, string $organismeDestination): bool|\App\Entity\Agent\RPA
    {
        $criterias = ['organisme' => $organismeDestination, 'id' => $idServiceDestination];

        $serviceDestinationObject = $this->em->getRepository(Service::class)->findOneBy($criterias);
        $organismeDestinationObject = $this->em->getRepository(Organisme::class)->findByAcronymeOrganisme($organismeDestination);

        if ($serviceDestinationObject instanceof Service && $organismeDestinationObject instanceof Organisme) {
            $nouveauRPA = clone $this;
            $nouveauRPA->setId($this->getMaxId() + 1);
            $nouveauRPA->setServiceId($idServiceDestination);
            $nouveauRPA->setAcronymeOrg($organismeDestination);
            $this->em->persist($nouveauRPA);
            $this->em->flush();

            return $nouveauRPA;
        }

        return false;
    }

    public function getMaxId()
    {
        $maxId = $this->em->getRepository(RPA::class)->findBy([], ['id' => 'DESC'], 1, 0)[0];

        return $maxId->getId();
    }
}
