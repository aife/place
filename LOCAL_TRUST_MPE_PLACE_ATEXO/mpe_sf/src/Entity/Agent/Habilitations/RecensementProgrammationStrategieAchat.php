<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Entity\Agent\Habilitations;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\Embeddable */
class RecensementProgrammationStrategieAchat
{
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $besoinUnitaireConsultation = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $besoinUnitaireCreationModification = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $demandeAchatConsultation = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $demandeAchatCreationModification = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $projetAchatConsultation = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $projetAchatCreationModification = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $validationOpportunite = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $validationAchat = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $validationBudget = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    private ?bool $validerProjetAchat = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $strategieAchatGestion = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $recensementProgrammationAdministration = false;

    public function isBesoinUnitaireConsultation(): bool
    {
        return $this->besoinUnitaireConsultation;
    }

    public function setBesoinUnitaireConsultation(
        bool $besoinUnitaireConsultation
    ): RecensementProgrammationStrategieAchat {
        $this->besoinUnitaireConsultation = $besoinUnitaireConsultation;
        return $this;
    }

    public function isBesoinUnitaireCreationModification(): bool
    {
        return $this->besoinUnitaireCreationModification;
    }

    public function setBesoinUnitaireCreationModification(
        bool $besoinUnitaireCreationModification
    ): RecensementProgrammationStrategieAchat {
        $this->besoinUnitaireCreationModification = $besoinUnitaireCreationModification;
        return $this;
    }

    public function isDemandeAchatConsultation(): bool
    {
        return $this->demandeAchatConsultation;
    }

    public function setDemandeAchatConsultation(
        bool $demandeAchatConsultation
    ): RecensementProgrammationStrategieAchat {
        $this->demandeAchatConsultation = $demandeAchatConsultation;
        return $this;
    }

    public function isDemandeAchatCreationModification(): bool
    {
        return $this->demandeAchatCreationModification;
    }

    public function setDemandeAchatCreationModification(
        bool $demandeAchatCreationModification
    ): RecensementProgrammationStrategieAchat {
        $this->demandeAchatCreationModification = $demandeAchatCreationModification;
        return $this;
    }

    public function isProjetAchatConsultation(): bool
    {
        return $this->projetAchatConsultation;
    }

    public function setProjetAchatConsultation(bool $projetAchatConsultation): RecensementProgrammationStrategieAchat
    {
        $this->projetAchatConsultation = $projetAchatConsultation;
        return $this;
    }

    public function isProjetAchatCreationModification(): bool
    {
        return $this->projetAchatCreationModification;
    }

    public function setProjetAchatCreationModification(
        bool $projetAchatCreationModification
    ): RecensementProgrammationStrategieAchat {
        $this->projetAchatCreationModification = $projetAchatCreationModification;
        return $this;
    }

    public function isValidationOpportunite(): bool
    {
        return $this->validationOpportunite;
    }

    public function setValidationOpportunite(bool $validationOpportunite): RecensementProgrammationStrategieAchat
    {
        $this->validationOpportunite = $validationOpportunite;
        return $this;
    }

    public function isValidationAchat(): bool
    {
        return $this->validationAchat;
    }

    public function setValidationAchat(bool $validationAchat): RecensementProgrammationStrategieAchat
    {
        $this->validationAchat = $validationAchat;
        return $this;
    }

    public function isValidationBudget(): bool
    {
        return $this->validationBudget;
    }

    public function setValidationBudget(bool $validationBudget): RecensementProgrammationStrategieAchat
    {
        $this->validationBudget = $validationBudget;
        return $this;
    }

    public function isValiderProjetAchat(): ?bool
    {
        return $this->validerProjetAchat;
    }

    public function setValiderProjetAchat(bool $validerProjetAchat): RecensementProgrammationStrategieAchat
    {
        $this->validerProjetAchat = $validerProjetAchat;
        return $this;
    }

    public function isStrategieAchatGestion(): bool
    {
        return $this->strategieAchatGestion;
    }

    public function setStrategieAchatGestion(bool $strategieAchatGestion): RecensementProgrammationStrategieAchat
    {
        $this->strategieAchatGestion = $strategieAchatGestion;
        return $this;
    }

    public function isRecensementProgrammationAdministration(): bool
    {
        return $this->recensementProgrammationAdministration;
    }

    public function setRecensementProgrammationAdministration(
        bool $recensementProgrammationAdministration
    ): RecensementProgrammationStrategieAchat {
        $this->recensementProgrammationAdministration = $recensementProgrammationAdministration;
        return $this;
    }
}
