<?php

namespace App\Entity\Agent;

use Doctrine\ORM\Mapping as ORM;

/**
 * NotificationAgent.
 *
 * @ORM\Table(name="t_notification_agent", indexes={@ORM\Index(name="idx_id_notification",
 *     columns={"id_notification"})}),
 * @ORM\Entity(repositoryClass="App\Repository\Agent\NotificationAgentRepository")
 */
class NotificationAgent
{
    public const NOTIFICATION_ACTIVE = '1';
    public const NOTIFICATION_DISABLE = '0';

    public const NOTIFICATION_READ = '1';
    public const NOTIFICATION_UNREAD = '0';

    /**
     * @ORM\Column(name="id_notification", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $idNotification;

    /**
     * @ORM\Column(name="id_agent", type="integer")
     */
    private ?int $idAgent = null;

    /**
     * @ORM\Column(name="id_type", type="integer")
     */
    private ?int $idType = null;

    /**
     * @ORM\Column(name="service_id", type="integer")
     */
    private ?int $idService = null;

    /**
     * @var string
     * @ORM\Column(name="organisme", type="string", length=45)
     */
    private $organisme;

    /**
     * @var string
     * @ORM\Column(name="reference_objet", type="string", length=255)
     */
    private $referenceObjet;

    /**
     * @var string
     * @ORM\Column(name="libelle_notification", type="string", length=255)
     */
    private $libelleNotifcation;

    /**
     * @var text
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     * @ORM\Column(name="notification_lue", type="string", length=10)
     */
    private $notificationLue;

    /**
     * @var string
     * @ORM\Column(name="notification_actif", type="string", length=10)
     */
    private $notificationActif;

    /**
     * @var string
     * @ORM\Column(name="url_notification", type="string", length=500)
     */
    private $urlNotification;

    /**
     * @var datetime
     * @ORM\Column(name="date_creation", type="datetime")
     */
    private $dateCreation;

    /**
     * @var datetime
     * @ORM\Column(name="date_lecture", type="datetime")
     */
    private $dateLecture;

    /**
     * @return int
     */
    public function getIdNotification()
    {
        return $this->idNotification;
    }

    /**
     * @return int
     */
    public function getIdAgent()
    {
        return $this->idAgent;
    }

    /**
     * @param int $idAgent
     */
    public function setIdAgent($idAgent)
    {
        $this->idAgent = $idAgent;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdType()
    {
        return $this->idType;
    }

    /**
     * @param int $idType
     */
    public function setIdType($idType)
    {
        $this->idType = $idType;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdService()
    {
        return $this->idService;
    }

    /**
     * @param int $idService
     */
    public function setIdService($idService)
    {
        $this->idService = $idService;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param string
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * @return string
     */
    public function getReferenceObjet()
    {
        return $this->referenceObjet;
    }

    /**
     * @param string
     */
    public function setReferenceObjet($referenceObjet)
    {
        $this->referenceObjet = $referenceObjet;

        return $this;
    }

    /**
     * @return string
     */
    public function getLibelleNotifcation()
    {
        return $this->libelleNotifcation;
    }

    /**
     * @param string
     */
    public function setLibelleNotifcation($libelleNotifcation)
    {
        $this->libelleNotifcation = $libelleNotifcation;

        return $this;
    }

    /**
     * @return text
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param text
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return bool
     */
    public function isNotificationLue()
    {
        return $this->notificationLue;
    }

    /**
     * @param bool
     */
    public function setNotificationLue($notificationLue)
    {
        $this->notificationLue = $notificationLue;

        return $this;
    }

    /**
     * @return bool
     */
    public function isNotificationActif()
    {
        return $this->notificationActif;
    }

    /**
     * @param bool
     */
    public function setNotificationActif($notificationActif)
    {
        $this->notificationActif = $notificationActif;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrlNotification()
    {
        return $this->urlNotification;
    }

    /**
     * @param string
     */
    public function setUrlNotification($urlNotification)
    {
        $this->urlNotification = $urlNotification;

        return $this;
    }

    /**
     * @return datetime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param datetime
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * @return datetime
     */
    public function getDateLecture()
    {
        return $this->dateLecture;
    }

    /**
     * @param datetime
     */
    public function setDateLecture($dateLecture)
    {
        $this->dateLecture = $dateLecture;
    }
}
