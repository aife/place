<?php

namespace App\Entity\Agent;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="t_vision_rma_agent_organisme")
 * @ORM\Entity(repositoryClass="App\Repository\Agent\VisionRmaAgentOrganismeRepository")
 */
class VisionRmaAgentOrganisme
{
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\ManyToOne(targetEntity="App\Entity\Agent")
     */
    private $idAgent;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=30)
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme")
     */
    private $acronyme;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getIdAgent(): int
    {
        return $this->idAgent;
    }

    public function setIdAgent(int $idAgent)
    {
        $this->idAgent = $idAgent;
    }

    public function getAcronyme(): string
    {
        return $this->acronyme;
    }

    public function setAcronyme(string $acronyme)
    {
        $this->acronyme = $acronyme;
    }
}
