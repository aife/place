<?php

namespace App\Entity\Agent;

use Exception;
use App\Entity\Organisme;
use App\Entity\Service;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Table(name="TireurPlan", uniqueConstraints={
 *          @UniqueConstraint(name="tireur_plan_id",columns={"id"}),
 *          @UniqueConstraint(name="tireur_plan_organisme",columns={"organisme"})
 *      })
 * @ORM\Entity(repositoryClass="App\Repository\Agent\TireurPlanRepository")
 */
class TireurPlan
{
    private EntityManager $em;

    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $nom = null;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $email = null;

    /**
     * @ORM\Column(name="service_id", type="integer", nullable=true)
     */
    private ?int $serviceId = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme)
    {
        $this->organisme = $organisme;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    public function setServiceId(int $serviceId = null)
    {
        $this->serviceId = $serviceId;
    }

    /**
     * Cette fonction permet de copier le TireurPlan lui attribuant un nouveau service et un nouvel organisme de destination passés en paramètres.
     *
     * @param int    $idServiceDestination identifiant du service de destination
     * @param string $organismeDestination acronyme de l'organisme de destination dans lequel se trouve le clone
     *
     * @return bool|TireurPlan false si le clone n'a pas réussi, sinon le nouvel objet JAL créé
     *
     * @throws Exception
     */
    public function copier(int $idServiceDestination, string $organismeDestination): bool|\App\Entity\Agent\TireurPlan
    {
        $criterias = ['organisme' => $organismeDestination, 'id' => $idServiceDestination];

        $serviceDestinationObject = $this->em->getRepository(Service::class)->findOneBy($criterias);
        $organismeDestinationObject = $this->em->getRepository(Organisme::class)->findByAcronymeOrganisme($organismeDestination);

        if ($serviceDestinationObject instanceof Service && $organismeDestinationObject instanceof Organisme) {
            $nouveauTireurPlan = clone $this;
            $nouveauTireurPlan->setId($this->getMaxId() + 1);
            $nouveauTireurPlan->setServiceId($idServiceDestination);
            $nouveauTireurPlan->setOrganisme($organismeDestination);
            $this->em->persist($nouveauTireurPlan);
            $this->em->flush();

            return $nouveauTireurPlan;
        }

        return false;
    }

    public function getMaxId()
    {
        $maxId = $this->em->getRepository(TireurPlan::class)->findBy([], ['id' => 'DESC'], 1, 0)[0];

        return $maxId->getId();
    }
}
