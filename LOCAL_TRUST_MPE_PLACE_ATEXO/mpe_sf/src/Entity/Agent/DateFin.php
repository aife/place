<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Entity\Agent;

use datetime;
use Doctrine\ORM\Mapping as ORM;

/**
 * DateFin.
 * @ORM\Entity
 * @ORM\Table(name="DATEFIN"),
 */
class DateFin
{
    /**
     * @ORM\Column(type="integer", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(name="consultation_ref", type="integer")
     */
    private int $consultationRef = 0;

    /**
     * @var datetime
     * @ORM\Column(name="datefin", type="string", length=50)*/
    private $dateFin;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private ?string $statut = null;

    /**
     * @var  blob
     * @ORM\Column(type="blob")
     */
    private $horodatage;

    /**
     * @var datetime
     * @ORM\Column(name="untrusteddate", type="string" , length=50)*/
    private $untrusteddate;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $agentId = null;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $consultationId = null;

    /**
     * @ORM\Column(name="dateFinLocale", type="string" , length=20)
     */
    private ?string $dateFinLocale = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme): DateFin
    {
        $this->organisme = $organisme;
        return $this;
    }

    public function getConsultationRef(): int
    {
        return $this->consultationRef;
    }

    public function setConsultationRef(int $consultationRef): DateFin
    {
        $this->consultationRef = $consultationRef;
        return $this;
    }

    /**
     * @return $dateFin
     */
    public function getDateFin(): string
    {
        return $this->dateFin;
    }

    /**
     * @param $dateFin
     */
    public function setDateFin($dateFin): DateFin
    {
        $this->dateFin = $dateFin;
        return $this;
    }

    public function getStatut(): string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): DateFin
    {
        $this->statut = $statut;
        return $this;
    }

    /**
     * @return blob
     */
    public function getHorodatage()
    {
        return $this->horodatage;
    }

    /**
     * @param $horodatage
     */
    public function setHorodatage($horodatage): DateFin
    {
        $this->horodatage = $horodatage;
        return $this;
    }

    public function getUntrusteddate(): string
    {
        return $this->untrusteddate;
    }

    /**
     * @param $untrusteddate
     */
    public function setUntrusteddate($untrusteddate): DateFin
    {
        $this->untrusteddate = $untrusteddate;
        return $this;
    }

    public function getAgentId(): int
    {
        return $this->agentId;
    }

    public function setAgentId(int $agentId): DateFin
    {
        $this->agentId = $agentId;
        return $this;
    }

    public function getConsultationId(): int
    {
        return $this->consultationId;
    }

    public function setConsultationId(int $consultationId): DateFin
    {
        $this->consultationId = $consultationId;
        return $this;
    }

    public function getDateFinLocale(): string
    {
        return $this->dateFinLocale;
    }

    public function setDateFinLocale(string $dateFinLocale): DateFin
    {
        $this->dateFinLocale = $dateFinLocale;
        return $this;
    }
}
