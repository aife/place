<?php

namespace App\Entity\Agent;

use Exception;
use App\Entity\Agent;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Table(name="Agent_Service_Metier", uniqueConstraints={
 *          @UniqueConstraint(name="agent_service_metier_id_agent",columns={"id_agent"}),
 *          @UniqueConstraint(name="agent_service_metier_id_service_metier",columns={"id_service_metier"})
 *      })
 * @ORM\Entity(repositoryClass="App\Repository\Agent\AgentServiceMetierRepository")
 */
class AgentServiceMetier
{
    private EntityManager $em;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default":0})
     * @ORM\ManyToOne(targetEntity="App\Entity\Agent")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idAgent = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default":0})
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme\ServiceMetier")
     */
    private $idServiceMetier = 0;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     */
    private int $idProfilService = 0;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private ?string $dateCreation = null;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private ?string $dateModification = null;

    public function getIdAgent(): int
    {
        return $this->idAgent;
    }

    public function setIdAgent(int $idAgent)
    {
        $this->idAgent = $idAgent;
    }

    public function getIdServiceMetier(): int
    {
        return $this->idServiceMetier;
    }

    public function setIdServiceMetier(int $idServiceMetier)
    {
        $this->idServiceMetier = $idServiceMetier;
    }

    public function getIdProfilService(): int
    {
        return $this->idProfilService;
    }

    public function setIdProfilService(int $idProfilService)
    {
        $this->idProfilService = $idProfilService;
    }

    /**
     * @return string
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    public function setDateCreation(string $dateCreation = null)
    {
        $this->dateCreation = $dateCreation;
    }

    /**
     * @return string
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }

    public function setDateModification(string $dateModification = null)
    {
        $this->dateModification = $dateModification;
    }

    /**
     * Cette fonction permet de cloner les objets AgentServiceMetier d'un agent et de les attribuer à l'agent dont l'identifiant est passé en paramètre.
     *
     * @param int $idAgent identifiant de l'agent auquel va être rattaché le clone
     *
     * @return bool|AgentServiceMetier false si le clone n'a pas réussi, sinon le nouvel objet AgentServiceMetier créé
     *
     * @throws Exception
     */
    public function copier(int $idAgent): bool|\App\Entity\Agent\AgentServiceMetier
    {
        $agentDestinationObject = $this->em->getRepository(Agent::class)->findOneBy(['id' => $idAgent]);

        if ($agentDestinationObject instanceof Agent) {
            $nouveauAgentServiceMetier = clone $this;
            $nouveauAgentServiceMetier->setIdAgent($idAgent);
            $this->em->persist($nouveauAgentServiceMetier);
            $this->em->flush();
            $agentDestinationObject = null;
            unset($agentDestinationObject);

            return $nouveauAgentServiceMetier;
        }

        return false;
    }
}
