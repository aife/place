<?php

namespace App\Entity\Agent;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\Output\AcheteurPublicOutput;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Exception;
use App\Entity\Organisme;
use App\Entity\Publicite\RenseignementsBoamp;
use App\Entity\Service;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * AcheteurPublic.
 *
 * @ORM\Table(name="AcheteurPublic", uniqueConstraints={
 *          @UniqueConstraint(name="acheteur_public_id",columns={"id"}),
 *          @UniqueConstraint(name="acheteur_public_organisme",columns={"organisme"})
 *      })
 * @ORM\Entity(repositoryClass="App\Repository\Agent\AcheteurPublicRepository")
 */
#[ApiResource(
    collectionOperations: [
        "get"
    ],
    itemOperations: [
    ],
    denormalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: AcheteurPublicOutput::class
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'serviceId' => 'exact',
        'organisme' => 'exact'
    ]
)]
class AcheteurPublic
{
    private EntityManager $em;

    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=30)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $denomination = null;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $prm = null;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $adresse = null;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private ?string $cp = null;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $ville = null;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private ?string $dept = null;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private ?string $typeOrg = null;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $telephone = null;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $fax = null;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $mail = null;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $url = null;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $urlAcheteur = null;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $factureNumero = null;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $factureCode = null;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $factureDenomination = null;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $facturationService = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $factureAdresse = null;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private ?string $factureCp = null;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $factureVille = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $facturePays = null;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     */
    private int $idAapc = 0;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $boampLogin = null;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $boampPassword = null;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $boampMail = null;
    /**
     * @ORM\Column(type="string", length=1)
     */
    private string $boampTarget = '0';

    /**
     * @ORM\Column(type="text")
     */
    private ?string $defaultFormValues = null;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $defautFormAmBoamp = null;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $defautFormAmBoampJoue = null;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $defautFormMapaBoamp = null;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $defautFormAconcours = null;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $defautFormAsBoampJoue = null;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $defautFormAaBoamp = null;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $defautFormArMapaBoamp = null;

    /**
     * @ORM\Column(type="text", name="defaut_form_05_boamp")
     */
    private ?string $defautForm05Boamp = null;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $defautFormRect = null;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $defautFormAaBoampJoue = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $serviceId = null;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $livraisonService = null;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $livraisonAdresse = null;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private ?string $livraisonCodePostal = null;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $livraisonVille = null;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $livraisonPays = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $typePouvoirActivite = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $codeNuts = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $modalitesFinancement = null;

    /**
     * @ORM\Column(type="integer", options={"default":90})
     */
    private int $moniteurProvenance = 90;

    /**
     * @ORM\Column(type="string", length=255,  options={"default":"Y8YG-69WD-4421-4G28"})
     */
    private string $codeAccesLogiciel = 'Y8YG-69WD-4421-4G28';

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $departementMiseEnLigne = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $referenceCommande = null;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $token = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme)
    {
        $this->organisme = $organisme;
    }

    public function getDenomination(): string
    {
        return $this->denomination;
    }

    public function setDenomination(string $denomination)
    {
        $this->denomination = $denomination;
    }

    public function getPrm(): string
    {
        return $this->prm;
    }

    public function setPrm(string $prm)
    {
        $this->prm = $prm;
    }

    public function getAdresse(): string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse)
    {
        $this->adresse = $adresse;
    }

    public function getCp(): string
    {
        return $this->cp;
    }

    public function setCp(string $cp)
    {
        $this->cp = $cp;
    }

    public function getVille(): string
    {
        return $this->ville;
    }

    public function setVille(string $ville)
    {
        $this->ville = $ville;
    }

    public function getDept(): string
    {
        return $this->dept;
    }

    public function setDept(string $dept)
    {
        $this->dept = $dept;
    }

    public function getTypeOrg(): string
    {
        return $this->typeOrg;
    }

    public function setTypeOrg(string $typeOrg)
    {
        $this->typeOrg = $typeOrg;
    }

    public function getTelephone(): string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone)
    {
        $this->telephone = $telephone;
    }

    public function getFax(): string
    {
        return $this->fax;
    }

    public function setFax(string $fax)
    {
        $this->fax = $fax;
    }

    public function getMail(): string
    {
        return $this->mail;
    }

    public function setMail(string $mail)
    {
        $this->mail = $mail;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getUrlAcheteur()
    {
        return $this->urlAcheteur;
    }

    public function setUrlAcheteur(string $urlAcheteur = null)
    {
        $this->urlAcheteur = $urlAcheteur;
    }

    public function getFactureNumero(): string
    {
        return $this->factureNumero;
    }

    public function setFactureNumero(string $factureNumero)
    {
        $this->factureNumero = $factureNumero;
    }

    public function getFactureCode(): string
    {
        return $this->factureCode;
    }

    public function setFactureCode(string $factureCode)
    {
        $this->factureCode = $factureCode;
    }

    public function getFactureDenomination(): string
    {
        return $this->factureDenomination;
    }

    public function setFactureDenomination(string $factureDenomination)
    {
        $this->factureDenomination = $factureDenomination;
    }

    /**
     * @return string
     */
    public function getFacturationService()
    {
        return $this->facturationService;
    }

    public function setFacturationService(string $facturationService = null)
    {
        $this->facturationService = $facturationService;
    }

    public function getFactureAdresse(): string
    {
        return $this->factureAdresse;
    }

    public function setFactureAdresse(string $factureAdresse)
    {
        $this->factureAdresse = $factureAdresse;
    }

    public function getFactureCp(): string
    {
        return $this->factureCp;
    }

    public function setFactureCp(string $factureCp)
    {
        $this->factureCp = $factureCp;
    }

    public function getFactureVille(): string
    {
        return $this->factureVille;
    }

    public function setFactureVille(string $factureVille)
    {
        $this->factureVille = $factureVille;
    }

    public function getFacturePays(): string
    {
        return $this->facturePays;
    }

    public function setFacturePays(string $facturePays)
    {
        $this->facturePays = $facturePays;
    }

    public function getIdAapc(): int
    {
        return $this->idAapc;
    }

    public function setIdAapc(int $idAapc = 0)
    {
        $this->idAapc = $idAapc;
    }

    public function getBoampLogin(): string
    {
        return $this->boampLogin;
    }

    public function setBoampLogin(string $boampLogin)
    {
        $this->boampLogin = $boampLogin;
    }

    public function getBoampPassword(): string
    {
        return $this->boampPassword;
    }

    public function setBoampPassword(string $boampPassword)
    {
        $this->boampPassword = $boampPassword;
    }

    public function getBoampMail(): string
    {
        return $this->boampMail;
    }

    public function setBoampMail(string $boampMail)
    {
        $this->boampMail = $boampMail;
    }

    public function getBoampTarget(): string
    {
        return $this->boampTarget;
    }

    public function setBoampTarget(string $boampTarget = '0')
    {
        $this->boampTarget = $boampTarget;
    }

    public function getDefaultFormValues(): string
    {
        return $this->defaultFormValues;
    }

    public function setDefaultFormValues(string $defaultFormValues)
    {
        $this->defaultFormValues = $defaultFormValues;
    }

    public function getDefautFormAmBoamp(): string
    {
        return $this->defautFormAmBoamp;
    }

    public function setDefautFormAmBoamp(string $defautFormAmBoamp)
    {
        $this->defautFormAmBoamp = $defautFormAmBoamp;
    }

    public function getDefautFormAmBoampJoue(): string
    {
        return $this->defautFormAmBoampJoue;
    }

    public function setDefautFormAmBoampJoue(string $defautFormAmBoampJoue)
    {
        $this->defautFormAmBoampJoue = $defautFormAmBoampJoue;
    }

    public function getDefautFormMapaBoamp(): string
    {
        return $this->defautFormMapaBoamp;
    }

    public function setDefautFormMapaBoamp(string $defautFormMapaBoamp)
    {
        $this->defautFormMapaBoamp = $defautFormMapaBoamp;
    }

    public function getDefautFormAconcours(): string
    {
        return $this->defautFormAconcours;
    }

    public function setDefautFormAconcours(string $defautFormAconcours)
    {
        $this->defautFormAconcours = $defautFormAconcours;
    }

    public function getDefautFormAsBoampJoue(): string
    {
        return $this->defautFormAsBoampJoue;
    }

    public function setDefautFormAsBoampJoue(string $defautFormAsBoampJoue)
    {
        $this->defautFormAsBoampJoue = $defautFormAsBoampJoue;
    }

    public function getDefautFormAaBoamp(): string
    {
        return $this->defautFormAaBoamp;
    }

    public function setDefautFormAaBoamp(string $defautFormAaBoamp)
    {
        $this->defautFormAaBoamp = $defautFormAaBoamp;
    }

    public function getDefautFormArMapaBoamp(): string
    {
        return $this->defautFormArMapaBoamp;
    }

    public function setDefautFormArMapaBoamp(string $defautFormArMapaBoamp)
    {
        $this->defautFormArMapaBoamp = $defautFormArMapaBoamp;
    }

    public function getDefautForm05Boamp(): string
    {
        return $this->defautForm05Boamp;
    }

    public function setDefautForm05Boamp(string $defautForm05Boamp)
    {
        $this->defautForm05Boamp = $defautForm05Boamp;
    }

    public function getDefautFormRect(): string
    {
        return $this->defautFormRect;
    }

    public function setDefautFormRect(string $defautFormRect)
    {
        $this->defautFormRect = $defautFormRect;
    }

    public function getDefautFormAaBoampJoue(): string
    {
        return $this->defautFormAaBoampJoue;
    }

    public function setDefautFormAaBoampJoue(string $defautFormAaBoampJoue)
    {
        $this->defautFormAaBoampJoue = $defautFormAaBoampJoue;
    }

    public function getServiceId(): int
    {
        return $this->serviceId;
    }

    public function setServiceId(int $serviceId = null)
    {
        $this->serviceId = $serviceId;
    }

    /**
     * @return string
     */
    public function getLivraisonService()
    {
        return $this->livraisonService;
    }

    public function setLivraisonService(string $livraisonService = null)
    {
        $this->livraisonService = $livraisonService;
    }

    /**
     * @return string
     */
    public function getLivraisonAdresse()
    {
        return $this->livraisonAdresse;
    }

    public function setLivraisonAdresse(string $livraisonAdresse = null)
    {
        $this->livraisonAdresse = $livraisonAdresse;
    }

    /**
     * @return string
     */
    public function getLivraisonCodePostal()
    {
        return $this->livraisonCodePostal;
    }

    public function setLivraisonCodePostal(string $livraisonCodePostal = null)
    {
        $this->livraisonCodePostal = $livraisonCodePostal;
    }

    /**
     * @return string
     */
    public function getLivraisonVille()
    {
        return $this->livraisonVille;
    }

    public function setLivraisonVille(string $livraisonVille = null)
    {
        $this->livraisonVille = $livraisonVille;
    }

    /**
     * @return string
     */
    public function getLivraisonPays()
    {
        return $this->livraisonPays;
    }

    public function setLivraisonPays(string $livraisonPays = null)
    {
        $this->livraisonPays = $livraisonPays;
    }

    /**
     * @return string
     */
    public function getTypePouvoirActivite()
    {
        return $this->typePouvoirActivite;
    }

    public function setTypePouvoirActivite(string $typePouvoirActivite = null)
    {
        $this->typePouvoirActivite = $typePouvoirActivite;
    }

    /**
     * @return string
     */
    public function getCodeNuts()
    {
        return $this->codeNuts;
    }

    public function setCodeNuts(string $codeNuts = null)
    {
        $this->codeNuts = $codeNuts;
    }

    /**
     * @return string
     */
    public function getModalitesFinancement()
    {
        return $this->modalitesFinancement;
    }

    public function setModalitesFinancement(string $modalitesFinancement = null)
    {
        $this->modalitesFinancement = $modalitesFinancement;
    }

    public function getMoniteurProvenance(): int
    {
        return $this->moniteurProvenance;
    }

    public function setMoniteurProvenance(int $moniteurProvenance = 90)
    {
        $this->moniteurProvenance = $moniteurProvenance;
    }

    public function getCodeAccesLogiciel(): string
    {
        return $this->codeAccesLogiciel;
    }

    public function setCodeAccesLogiciel(string $codeAccesLogiciel = 'Y8YG-69WD-4421-4G28')
    {
        $this->codeAccesLogiciel = $codeAccesLogiciel;
    }

    /**
     * @return string
     */
    public function getDepartementMiseEnLigne()
    {
        return $this->departementMiseEnLigne;
    }

    public function setDepartementMiseEnLigne(string $departementMiseEnLigne = null)
    {
        $this->departementMiseEnLigne = $departementMiseEnLigne;
    }

    public function getReferenceCommande(): string
    {
        return $this->referenceCommande;
    }

    public function setReferenceCommande(string $referenceCommande)
    {
        $this->referenceCommande = $referenceCommande;
    }

    /**
     * Cette fonction permet de copier l'acheteur public en lui attribuant un nouveau service et un nouvel organisme de destination passés en paramètres
     * Il garde l'ancien acheteur public en BD.
     *
     * @param int    $idServiceDestination identifiant du service de destination
     * @param string $organismeDestination acronyme de l'organisme de destination dans lequel se trouve le clone
     *
     * @return bool|Agent false si le clone n'a pas réussi, sinon le nouvel objet AcheteurPublic créé
     *
     * @throws Exception
     */
    public function copier(int $idServiceDestination, string $organismeDestination)
    {
        $criterias = ['organisme' => $organismeDestination, 'id' => $idServiceDestination];

        $serviceDestinationObject = $this->em->getRepository(Service::class)->findOneBy($criterias);
        $organismeDestinationObject = $this->em->getRepository(Organisme::class)->findByAcronymeOrganisme($organismeDestination);

        if ($serviceDestinationObject instanceof Service && $organismeDestinationObject instanceof Organisme) {
            $nouveauAcheteurPublic = clone $this;
            $nouveauAcheteurPublic->setId($this->getMaxId() + 1);
            $nouveauAcheteurPublic->setServiceId($idServiceDestination);
            $nouveauAcheteurPublic->setOrganisme($organismeDestination);

            $this->em->persist($nouveauAcheteurPublic);
            $this->em->flush();

            $listRenseignementBoamp = $this->em->getRepository(RenseignementsBoamp::class)->findBy(['idCompte' => $this->getId(), 'acronymeOrg' => $this->getOrganisme()]);
            foreach ($listRenseignementBoamp as $renseignementBoamp) {
                if ($renseignementBoamp instanceof RenseignementsBoamp) {
                    $renseignementBoamp->copier($nouveauAcheteurPublic);
                }
            }

            $serviceDestinationObject = null;
            $organismeDestinationObject = null;
            $renseignementBoamp = null;
            $newRenseignementsBoamp = null;

            unset($serviceDestinationObject);
            unset($organismeDestinationObject);
            unset($renseignementBoamp);
            unset($newRenseignementsBoamp);

            return $nouveauAcheteurPublic;
        }

        return false;
    }

    public function getMaxId()
    {
        $maxId = $this->em->getRepository(AcheteurPublic::class)->findBy([], ['id' => 'DESC'], 1, 0)[0];

        return $maxId->getId();
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }
}
