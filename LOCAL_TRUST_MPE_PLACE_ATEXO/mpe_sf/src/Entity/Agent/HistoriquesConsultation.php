<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Entity\Agent;

use Doctrine\ORM\Mapping as ORM;

/**
 * HistoriquesConsultation.
 * @ORM\Entity
 * @ORM\Table(),
 */
class HistoriquesConsultation
{
    /**
     * @ORM\Column(type="integer", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(type="integer")
     */
    private int $refConsultation = 0;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private ?string $statut = null;

    /**
     * @ORM\Column(name="id_agent", type="integer")
     */
    private ?int $agentId = null;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private ?string $nomAgent = null;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private ?string $prenomAgent = null;
    
    /**
     * @ORM\Column(type="string", length=250)
     */
    private ?string $nomElement = null;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private ?string $valeur = null;

    /**
     * @ORM\Column(name="valeur_detail_1",type="string", length=250)
     */
    private ?string $valeurDetail1 = null;

    /**
     * @ORM\Column(name="valeur_detail_2", type="string", length=250)
     */
    private ?string $valeurDetail2 = null;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private ?string $numeroLot = null;

    /**
     * @var blob
     * @ORM\Column(type="blob", nullable=true)
     */
    private $horodatage;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private ?string $untrusteddate = null;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $consultationId = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme): HistoriquesConsultation
    {
        $this->organisme = $organisme;
        return $this;
    }

    public function getRefConsultation(): int
    {
        return $this->refConsultation;
    }

    public function setRefConsultation(int $refConsultation): HistoriquesConsultation
    {
        $this->refConsultation = $refConsultation;
        return $this;
    }

    public function getStatut(): string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): HistoriquesConsultation
    {
        $this->statut = $statut;
        return $this;
    }

    public function getAgentId(): int
    {
        return $this->agentId;
    }

    public function setAgentId(int $agentId): HistoriquesConsultation
    {
        $this->agentId = $agentId;
        return $this;
    }

    public function getNomAgent(): string
    {
        return $this->nomAgent;
    }

    public function setNomAgent(string $nomAgent): HistoriquesConsultation
    {
        $this->nomAgent = $nomAgent;
        return $this;
    }

    public function getPrenomAgent(): string
    {
        return $this->prenomAgent;
    }

    public function setPrenomAgent(string $prenomAgent): HistoriquesConsultation
    {
        $this->prenomAgent = $prenomAgent;
        return $this;
    }

    public function getNomElement(): string
    {
        return $this->nomElement;
    }

    public function setNomElement(string $nomElement): HistoriquesConsultation
    {
        $this->nomElement = $nomElement;
        return $this;
    }

    public function getValeur(): string
    {
        return $this->valeur;
    }

    public function setValeur(string $valeur): HistoriquesConsultation
    {
        $this->valeur = $valeur;
        return $this;
    }

    public function getValeurDetail1(): string
    {
        return $this->valeurDetail1;
    }

    public function setValeurDetail1(string $valeurDetail1): HistoriquesConsultation
    {
        $this->valeurDetail1 = $valeurDetail1;
        return $this;
    }

    public function getValeurDetail2(): string
    {
        return $this->valeurDetail2;
    }

    public function setValeurDetail2(string $valeurDetail2): HistoriquesConsultation
    {
        $this->valeurDetail2 = $valeurDetail2;
        return $this;
    }

    public function getNumeroLot(): string
    {
        return $this->numeroLot;
    }

    public function setNumeroLot(string $numeroLot): HistoriquesConsultation
    {
        $this->numeroLot = $numeroLot;
        return $this;
    }

    /**
     * @return blob
     */
    public function getHorodatage()
    {
        return $this->horodatage;
    }

    /**
     * @param $horodatage
     */
    public function setHorodatage($horodatage): HistoriquesConsultation
    {
        $this->horodatage = $horodatage;
        return $this;
    }

    public function getUntrusteddate(): string
    {
        return $this->untrusteddate;
    }

    public function setUntrusteddate(?string $untrusteddate): HistoriquesConsultation
    {
        $this->untrusteddate = $untrusteddate;
        return $this;
    }

    public function getConsultationId(): int
    {
        return $this->consultationId;
    }

    public function setConsultationId(int $consultationId): HistoriquesConsultation
    {
        $this->consultationId = $consultationId;
        return $this;
    }
}
