<?php

namespace App\Entity;

use App\Entity\Procedure\TypeProcedureOrganisme;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TypeProcedure.
 *
 * @ORM\Table(name="TypeProcedure")
 * @ORM\Entity(repositoryClass="App\Repository\TypeProcedureRepository")
 */
class TypeProcedure
{
    /**
     *
     * @ORM\Column(name="id_type_procedure", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $idTypeProcedure = null;

    /**
     * @ORM\Column(name="libelle_type_procedure", type="string", length=100, nullable=false)
     */
    private string $libelleTypeProcedure = '';

    /**
     * @ORM\Column(name="libelle_type_procedure_fr", type="string", length=100, nullable=true)
     */
    private ?string $libelleTypeProcedureFr = '';

    /**
     * @ORM\Column(name="libelle_type_procedure_en", type="string", length=100, nullable=true)
     */
    private ?string $libelleTypeProcedureEn = '';

    /**
     * @ORM\Column(name="libelle_type_procedure_es", type="string", length=100, nullable=true)
     */
    private ?string $libelleTypeProcedureEs = '';

    /**
     * @ORM\Column(name="libelle_type_procedure_su", type="string", length=100, nullable=true)
     */
    private ?string $libelleTypeProcedureSu = '';

    /**
     * @ORM\Column(name="libelle_type_procedure_du", type="string", length=100, nullable=true)
     */
    private ?string $libelleTypeProcedureDu = '';

    /**
     * @ORM\Column(name="libelle_type_procedure_cz", type="string", length=100, nullable=true)
     */
    private ?string $libelleTypeProcedureCz = '';

    /**
     * @ORM\Column(name="libelle_type_procedure_ar", type="string", length=100, nullable=true)
     */
    private ?string $libelleTypeProcedureAr = null;

    /**
     * @ORM\Column(name="abbreviation", type="string", length=50, nullable=false)
     */
    private string $abbreviation = '';

    /**
     * @ORM\Column(name="type_boamp", type="integer", nullable=false)
     */
    private string|int $typeBoamp = '0';

    /**
     * @ORM\Column(name="categorie_procedure", type="integer", nullable=false)
     */
    private string|int $categorieProcedure = '0';

    /**
     * @ORM\Column(name="id_type_procedure_ANM", type="integer", nullable=false)
     */
    private string|int $idTypeProcedureAnm = '0';

    /**
     * @ORM\Column(name="delai_alerte", type="integer", nullable=false)
     */
    private string|int $delaiAlerte = '0';

    /**
     * @ORM\Column(name="mapa", type="string", nullable=false)
     */
    private string $mapa = '0';

    /**
     * @ORM\Column(name="consultation_transverse", type="string", nullable=false)
     */
    private string $consultationTransverse = '0';

    /**
     * @ORM\Column(name="code_recensement", type="string", length=3, nullable=true)
     */
    private ?string $codeRecensement = null;

    /**
     * @ORM\Column(name="abbreviation_portail_ANM", type="string", length=50, nullable=false)
     */
    private ?string $abbreviationPortailAnm = null;

    /**
     * @ORM\Column(name="id_modele", type="integer", nullable=false)
     */
    private string|int $idModele = '0';

    /**
     * @ORM\Column(name="libelle_type_procedure_it", type="string", length=100, nullable=true)
     */
    private ?string $libelleTypeProcedureIt = null;

    /**
     * @ORM\Column(name="value_binding_sub", type="string", length=255, nullable=false)
     */
    private ?string $valueBindingSub = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Consultation", mappedBy="typeProcedure")
     * @ORM\JoinColumn(name="id_type_procedure", referencedColumnName="id_type_procedure")
     */
    private Collection $consultations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Procedure\TypeProcedureOrganisme", mappedBy="procedurePortailType")
     * @ORM\JoinColumn(name="id_type_procedure", referencedColumnName="id_type_procedure_portail")
     */
    private Collection $typeProcedureOrganismes;


    public function __construct()
    {
        $this->typeProcedureOrganismes = new ArrayCollection();
        $this->consultations = new ArrayCollection();
    }

    /**
     * Get idTypeProcedure.
     *
     * @return int
     */
    public function getIdTypeProcedure()
    {
        return $this->idTypeProcedure;
    }

    /**
     * Set libelleTypeProcedure.
     *
     * @param string $libelleTypeProcedure
     *
     * @return TypeProcedure
     */
    public function setLibelleTypeProcedure($libelleTypeProcedure)
    {
        $this->libelleTypeProcedure = $libelleTypeProcedure;

        return $this;
    }

    /**
     * Get libelleTypeProcedure.
     *
     * @return string
     */
    public function getLibelleTypeProcedure()
    {
        return $this->libelleTypeProcedure;
    }

    /**
     * Set libelleTypeProcedureFr.
     *
     * @param string $libelleTypeProcedureFr
     *
     * @return TypeProcedure
     */
    public function setLibelleTypeProcedureFr($libelleTypeProcedureFr)
    {
        $this->libelleTypeProcedureFr = $libelleTypeProcedureFr;

        return $this;
    }

    /**
     * Get libelleTypeProcedureFr.
     *
     * @return string
     */
    public function getLibelleTypeProcedureFr()
    {
        return $this->libelleTypeProcedureFr;
    }

    /**
     * Set libelleTypeProcedureEn.
     *
     * @param string $libelleTypeProcedureEn
     *
     * @return TypeProcedure
     */
    public function setLibelleTypeProcedureEn($libelleTypeProcedureEn)
    {
        $this->libelleTypeProcedureEn = $libelleTypeProcedureEn;

        return $this;
    }

    /**
     * Get libelleTypeProcedureEn.
     *
     * @return string
     */
    public function getLibelleTypeProcedureEn()
    {
        return $this->libelleTypeProcedureEn;
    }

    /**
     * Set libelleTypeProcedureEs.
     *
     * @param string $libelleTypeProcedureEs
     *
     * @return TypeProcedure
     */
    public function setLibelleTypeProcedureEs($libelleTypeProcedureEs)
    {
        $this->libelleTypeProcedureEs = $libelleTypeProcedureEs;

        return $this;
    }

    /**
     * Get libelleTypeProcedureEs.
     *
     * @return string
     */
    public function getLibelleTypeProcedureEs()
    {
        return $this->libelleTypeProcedureEs;
    }

    /**
     * Set libelleTypeProcedureSu.
     *
     * @param string $libelleTypeProcedureSu
     *
     * @return TypeProcedure
     */
    public function setLibelleTypeProcedureSu($libelleTypeProcedureSu)
    {
        $this->libelleTypeProcedureSu = $libelleTypeProcedureSu;

        return $this;
    }

    /**
     * Get libelleTypeProcedureSu.
     *
     * @return string
     */
    public function getLibelleTypeProcedureSu()
    {
        return $this->libelleTypeProcedureSu;
    }

    /**
     * Set libelleTypeProcedureDu.
     *
     * @param string $libelleTypeProcedureDu
     *
     * @return TypeProcedure
     */
    public function setLibelleTypeProcedureDu($libelleTypeProcedureDu)
    {
        $this->libelleTypeProcedureDu = $libelleTypeProcedureDu;

        return $this;
    }

    /**
     * Get libelleTypeProcedureDu.
     *
     * @return string
     */
    public function getLibelleTypeProcedureDu()
    {
        return $this->libelleTypeProcedureDu;
    }

    /**
     * Set libelleTypeProcedureCz.
     *
     * @param string $libelleTypeProcedureCz
     *
     * @return TypeProcedure
     */
    public function setLibelleTypeProcedureCz($libelleTypeProcedureCz)
    {
        $this->libelleTypeProcedureCz = $libelleTypeProcedureCz;

        return $this;
    }

    /**
     * Get libelleTypeProcedureCz.
     *
     * @return string
     */
    public function getLibelleTypeProcedureCz()
    {
        return $this->libelleTypeProcedureCz;
    }

    /**
     * Set libelleTypeProcedureAr.
     *
     * @param string $libelleTypeProcedureAr
     *
     * @return TypeProcedure
     */
    public function setLibelleTypeProcedureAr($libelleTypeProcedureAr)
    {
        $this->libelleTypeProcedureAr = $libelleTypeProcedureAr;

        return $this;
    }

    /**
     * Get libelleTypeProcedureAr.
     *
     * @return string
     */
    public function getLibelleTypeProcedureAr()
    {
        return $this->libelleTypeProcedureAr;
    }

    /**
     * Set abbreviation.
     *
     * @param string $abbreviation
     *
     * @return TypeProcedure
     */
    public function setAbbreviation($abbreviation)
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    /**
     * Get abbreviation.
     *
     * @return string
     */
    public function getAbbreviation()
    {
        return $this->abbreviation;
    }

    /**
     * Set typeBoamp.
     *
     * @param int $typeBoamp
     *
     * @return TypeProcedure
     */
    public function setTypeBoamp($typeBoamp)
    {
        $this->typeBoamp = $typeBoamp;

        return $this;
    }

    /**
     * Get typeBoamp.
     *
     * @return int
     */
    public function getTypeBoamp()
    {
        return $this->typeBoamp;
    }

    /**
     * Set categorieProcedure.
     *
     * @param int $categorieProcedure
     *
     * @return TypeProcedure
     */
    public function setCategorieProcedure($categorieProcedure)
    {
        $this->categorieProcedure = $categorieProcedure;

        return $this;
    }

    /**
     * Get categorieProcedure.
     *
     * @return int
     */
    public function getCategorieProcedure()
    {
        return $this->categorieProcedure;
    }

    /**
     * Set idTypeProcedureAnm.
     *
     * @param int $idTypeProcedureAnm
     *
     * @return TypeProcedure
     */
    public function setIdTypeProcedureAnm($idTypeProcedureAnm)
    {
        $this->idTypeProcedureAnm = $idTypeProcedureAnm;

        return $this;
    }

    /**
     * Get idTypeProcedureAnm.
     *
     * @return int
     */
    public function getIdTypeProcedureAnm()
    {
        return $this->idTypeProcedureAnm;
    }

    /**
     * Set delaiAlerte.
     *
     * @param int $delaiAlerte
     *
     * @return TypeProcedure
     */
    public function setDelaiAlerte($delaiAlerte)
    {
        $this->delaiAlerte = $delaiAlerte;

        return $this;
    }

    /**
     * Get delaiAlerte.
     *
     * @return int
     */
    public function getDelaiAlerte()
    {
        return $this->delaiAlerte;
    }

    /**
     * Set mapa.
     *
     * @param string $mapa
     *
     * @return TypeProcedure
     */
    public function setMapa($mapa)
    {
        $this->mapa = $mapa;

        return $this;
    }

    /**
     * Get mapa.
     *
     * @return string
     */
    public function getMapa()
    {
        return $this->mapa;
    }

    /**
     * Set consultationTransverse.
     *
     * @param string $consultationTransverse
     *
     * @return TypeProcedure
     */
    public function setConsultationTransverse($consultationTransverse)
    {
        $this->consultationTransverse = $consultationTransverse;

        return $this;
    }

    /**
     * Get consultationTransverse.
     *
     * @return string
     */
    public function getConsultationTransverse()
    {
        return $this->consultationTransverse;
    }

    /**
     * Set codeRecensement.
     *
     * @param string $codeRecensement
     *
     * @return TypeProcedure
     */
    public function setCodeRecensement($codeRecensement)
    {
        $this->codeRecensement = $codeRecensement;

        return $this;
    }

    /**
     * Get codeRecensement.
     *
     * @return string
     */
    public function getCodeRecensement()
    {
        return $this->codeRecensement;
    }

    /**
     * Set abbreviationPortailAnm.
     *
     * @param string $abbreviationPortailAnm
     *
     * @return TypeProcedure
     */
    public function setAbbreviationPortailAnm($abbreviationPortailAnm)
    {
        $this->abbreviationPortailAnm = $abbreviationPortailAnm;

        return $this;
    }

    /**
     * Get abbreviationPortailAnm.
     *
     * @return string
     */
    public function getAbbreviationPortailAnm()
    {
        return $this->abbreviationPortailAnm;
    }

    /**
     * Set idModele.
     *
     * @param int $idModele
     *
     * @return TypeProcedure
     */
    public function setIdModele($idModele)
    {
        $this->idModele = $idModele;

        return $this;
    }

    /**
     * Get idModele.
     *
     * @return int
     */
    public function getIdModele()
    {
        return $this->idModele;
    }

    /**
     * Set libelleTypeProcedureIt.
     *
     * @param string $libelleTypeProcedureIt
     *
     * @return TypeProcedure
     */
    public function setLibelleTypeProcedureIt($libelleTypeProcedureIt)
    {
        $this->libelleTypeProcedureIt = $libelleTypeProcedureIt;

        return $this;
    }

    /**
     * Get libelleTypeProcedureIt.
     *
     * @return string
     */
    public function getLibelleTypeProcedureIt()
    {
        return $this->libelleTypeProcedureIt;
    }

    /**
     * @return string
     */
    public function getValueBindingSub()
    {
        return $this->valueBindingSub;
    }

    public function setValueBindingSub(string $valueBindingSub)
    {
        $this->valueBindingSub = $valueBindingSub;

        return $this;
    }

    public function getConsultations(): Collection
    {
        return $this->consultations;
    }

    public function setConsultations(ArrayCollection $consultations)
    {
        $this->consultations = $consultations;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getTypeProcedureOrganismes()
    {
        return $this->typeProcedureOrganismes;
    }

    /**
     * @param TypeProcedureOrganisme $consultation
     *
     * @return $this
     */
    public function addTypeProcedureOrganisme(TypeProcedureOrganisme $typeProcedureOrganisme)
    {
        $this->typeProcedureOrganismes[] = $typeProcedureOrganisme;
        $typeProcedureOrganisme->setProcedurePortailType($this);

        return $this;
    }
}
