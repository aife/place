<?php

namespace App\Entity;

use Stringable;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\Input\InscritInput;
use App\Dto\Output\InscritOutput;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Inscrit
 *
 * @ORM\Table(name="Inscrit", uniqueConstraints={@ORM\UniqueConstraint(name="email", columns={"email"}),
 *     @ORM\UniqueConstraint(name="login", columns={"login"})},
 *     indexes={
 *     @ORM\Index(name="id_etablissement_idx", columns={"id_etablissement"}),
 *     @ORM\Index(name="inscrit_id_idx", columns={"old_id"}),
 *     @ORM\Index(name="entreprise_id", columns={"entreprise_id"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\InscritRepository")
 */
#[ApiResource(
    collectionOperations: ['get','post'],
    itemOperations: ['get', 'put', 'patch','delete'],
    denormalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    input: InscritInput::class,
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: InscritOutput::class
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'dateValidationRgpd' => 'partial',
        'entreprise.siren' => 'exact',
        'etablissement.codeEtablissement' => 'exact',
        'etablissement.pays' => 'exact',
        'entreprise.paysenregistrement' => 'exact',
        'idExterne' => 'exact',
    ]
)]
class Inscrit extends Rgpd implements UserInterface, EquatableInterface, Stringable
{
    public const ARGON_TYPE = 'ARGON2';
    public const
        COMMUNICATION_PLACE = "COMMUNICATION_PLACE",
        COMMUNICATION_ENQUETE = "COMMUNICATION_ENQUETE",
        COMMUNICATION_SIA = "COMMUNICATION_SIA"
    ;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * Profil administrateur Entreprise
     */
    public const PROFIL_ATES = 2;

    private array $habilitations = [];

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Etablissement", inversedBy="inscrits", fetch="EAGER")
     * @ORM\JoinColumn(name="id_etablissement", referencedColumnName="id_etablissement")
     */
    private ?Etablissement $etablissement = null;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Offre", mappedBy="inscrit", fetch="LAZY")
     * @ORM\JoinColumn(name="id", referencedColumnName="inscrit_id")
     */
    private $offres;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\QuestionDCE", mappedBy="inscrit")
     */
    private Collection $questions;

    /**
     * @ORM\Column(name="entreprise_id", type="integer")
     */
    private int $entrepriseId = 0;

    /**
     * @ORM\Column(name="id_etablissement", type="integer", nullable=true)
     */
    private ?int $idEtablissement = null;

    /**
     * @var string|null
     * @ORM\Column(name="login", type="string", length=100, nullable=true)*/
    #[Groups('read')]
    private ?string $login = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mdp", type="string", length=255, nullable=true)
     */
    private ?string $mdp = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="num_cert", type="string", length=64, nullable=true)
     */
    private ?string $numCert = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cert", type="text", length=16777215, nullable=true)
     */
    private ?string $cert = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="civilite", type="boolean", nullable=false, options={"default"=0})
     */
    private $civilite = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=30, nullable=false)
     */
    private string $nom = '';

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=30, nullable=false)
     */
    private string $prenom = '';

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=80, nullable=false)
     */
    private $adresse = '';

    /**
     * @var string
     *
     * @ORM\Column(name="codepostal", type="string", length=20, nullable=false)
     */
    private string $codepostal = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=50, nullable=false)
     */
    private string $ville = '';

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=50, nullable=false)
     */
    private string $pays = '';

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=false)
     */
    private string $email = '';

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=20, nullable=false)
     */
    private string $telephone = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="categorie", type="text", length=65535, nullable=true)
     */
    private ?string $categorie = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="motstitreresume", type="text", length=16777215, nullable=true)
     */
    private ?string $motstitreresume = null;

    /**
     * @ORM\Column(name="periode", type="smallint", nullable=false, options={"default"=0})
     */
    private $periode = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="siret", type="string", length=5, nullable=false)
     */
    private ?string $siret = null;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=20, nullable=false)
     */
    private string $fax = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="code_cpv", type="text", length=65535, nullable=true)
     */
    private ?string $codeCpv = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id_langue", type="integer", nullable=true)
     */
    private ?int $idLangue = null;

    /**
     * @var int
     *
     * @ORM\Column(name="profil", type="integer", nullable=false, options={"default"="1"})
     */
    private int $profil = 1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adresse2", type="string", length=80, nullable=true)
     */
    private ?string $adresse2 = null;

    /**
     * @var string
     *
     * @ORM\Column(name="bloque", type="string", length=0, nullable=false, options={"default"="0"})
     */
    private string $bloque = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="id_initial", type="integer", nullable=false, options={"default"=0})
     */
    private int $idInitial = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="inscrit_annuaire_defense", type="string", length=0, nullable=false)
     */
    private string $inscritAnnuaireDefense;

    /**
     * @var string
     *
     * @ORM\Column(name="date_creation", type="string", length=20, nullable=false)
     */
    private $dateCreation = null;

    /**
     * @var string
     *
     * @ORM\Column(name="date_modification", type="string", length=20, nullable=false)
     */
    private $dateModification = null;

    /**
     * @var int
     *
     * @ORM\Column(name="tentatives_mdp", type="integer", nullable=false, options={"default"=0})
     */
    private int $tentativesMdp = 0;

    /**
     * @var string|null
     *
     * @ORM\Column(name="uid", type="string", length=50, nullable=true)
     */
    private ?string $uid = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="type_hash", type="string", length=10, nullable=true)
     */
    private $typeHash;

    /**
     * @var string
     *
     * @ORM\Column(name="id_externe", type="string", length=50, nullable=false, options={"default"="0"})
     */
    private string $idExterne = '0';

    /**
     * @ORM\Column(name="rgpd_communication", type="boolean", nullable=true)
     */
    private ?bool $rgpdCommunication = false;

    /**
     * @var int|null
     *
     * @ORM\Column(name="old_id", type="integer", nullable=true)
     */
    private $oldId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Entreprise", inversedBy="inscrits", fetch="EAGER")
     * @ORM\JoinColumn(name="entreprise_id", referencedColumnName="id")
     */
    private ?Entreprise $entreprise = null;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt = null;

    /**
     * @ORM\Column(name="deleted", type="boolean", nullable=true)
     */
    private $deleted = null;


    public function __toString(): string
    {
        return (string) $this->getId();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Inscrit
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set entrepriseId.
     *
     * @param int $entrepriseId
     *
     * @return Inscrit
     */
    public function setEntrepriseId($entrepriseId)
    {
        $this->entrepriseId = $entrepriseId;

        return $this;
    }

    /**
     * Get entrepriseId.
     *
     * @return int
     */
    public function getEntrepriseId(): int
    {
        return $this->entrepriseId;
    }

    /**
     * Set idEtablissement.
     *
     * @param int $idEtablissement
     *
     * @return Inscrit
     */
    public function setIdEtablissement($idEtablissement): self
    {
        $this->idEtablissement = $idEtablissement;

        return $this;
    }

    /**
     * Get idEtablissement.
     *
     * @return int
     */
    public function getIdEtablissement()
    {
        return $this->idEtablissement;
    }

    /**
     * Set login.
     *
     * @param string $login
     *
     * @return Inscrit
     */
    public function setLogin($login): self
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login.
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set mdp.
     *
     * @param string $mdp
     *
     * @return Inscrit
     */
    public function setMdp($mdp): self
    {
        $this->mdp = $mdp;

        return $this;
    }

    /**
     * Get mdp.
     *
     * @return string
     */
    public function getMdp()
    {
        return $this->mdp;
    }

    /**
     * Set numCert.
     *
     * @param string $numCert
     *
     * @return Inscrit
     */
    public function setNumCert($numCert): self
    {
        $this->numCert = $numCert;

        return $this;
    }

    /**
     * Get numCert.
     *
     * @return string
     */
    public function getNumCert()
    {
        return $this->numCert;
    }

    /**
     * Set cert.
     *
     * @param string $cert
     *
     * @return Inscrit
     */
    public function setCert($cert)
    {
        $this->cert = $cert;

        return $this;
    }

    /**
     * Get cert.
     *
     * @return string
     */
    public function getCert()
    {
        return $this->cert;
    }

    /**
     * Set civilite.
     *
     * @param bool $civilite
     *
     * @return Inscrit
     */
    public function setCivilite($civilite)
    {
        $this->civilite = $civilite;

        return $this;
    }

    /**
     * Get civilite.
     *
     * @return bool
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return Inscrit
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom.
     *
     * @param string $prenom
     *
     * @return Inscrit
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom.
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set adresse.
     *
     * @param string $adresse
     *
     * @return Inscrit
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse.
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set codepostal.
     *
     * @param string $codepostal
     *
     * @return Inscrit
     */
    public function setCodepostal($codepostal)
    {
        $this->codepostal = $codepostal;

        return $this;
    }

    /**
     * Get codepostal.
     *
     * @return string
     */
    public function getCodepostal()
    {
        return $this->codepostal;
    }

    /**
     * Set ville.
     *
     * @param string $ville
     *
     * @return Inscrit
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville.
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set pays.
     *
     * @param string $pays
     *
     * @return Inscrit
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays.
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Inscrit
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telephone.
     *
     * @param string $telephone
     *
     * @return Inscrit
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone.
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set categorie.
     *
     * @param string $categorie
     *
     * @return Inscrit
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie.
     *
     * @return string
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set motstitreresume.
     *
     * @param string $motstitreresume
     *
     * @return Inscrit
     */
    public function setMotstitreresume($motstitreresume)
    {
        $this->motstitreresume = $motstitreresume;

        return $this;
    }

    /**
     * Get motstitreresume.
     *
     * @return string
     */
    public function getMotstitreresume()
    {
        return $this->motstitreresume;
    }

    /**
     * Set periode.
     *
     * @return Inscrit
     */
    public function setPeriode($periode)
    {
        $this->periode = $periode;

        return $this;
    }

    /**
     * Get periode.
     */
    public function getPeriode()
    {
        return $this->periode;
    }

    /**
     * Set siret.
     *
     * @param string $siret
     *
     * @return Inscrit
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Get siret.
     *
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * Set fax.
     *
     * @param string $fax
     *
     * @return Inscrit
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax.
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set codeCpv.
     *
     * @param string $codeCpv
     *
     * @return Inscrit
     */
    public function setCodeCpv($codeCpv)
    {
        $this->codeCpv = $codeCpv;

        return $this;
    }

    /**
     * Get codeCpv.
     *
     * @return string
     */
    public function getCodeCpv()
    {
        return $this->codeCpv;
    }

    /**
     * Set idLangue.
     *
     * @param int $idLangue
     *
     * @return Inscrit
     */
    public function setIdLangue($idLangue)
    {
        $this->idLangue = $idLangue;

        return $this;
    }

    /**
     * Get idLangue.
     *
     * @return int
     */
    public function getIdLangue()
    {
        return $this->idLangue;
    }

    /**
     * Set profil.
     *
     * @param int $profil
     *
     * @return Inscrit
     */
    public function setProfil($profil)
    {
        $this->profil = $profil;

        return $this;
    }

    /**
     * Get profil.
     *
     * @return int
     */
    public function getProfil()
    {
        return $this->profil;
    }

    /**
     * Set adresse2.
     *
     * @param string $adresse2
     *
     * @return Inscrit
     */
    public function setAdresse2($adresse2)
    {
        $this->adresse2 = $adresse2;

        return $this;
    }

    /**
     * Get adresse2.
     *
     * @return string
     */
    public function getAdresse2()
    {
        return $this->adresse2;
    }

    /**
     * Set bloque.
     *
     * @param string $bloque
     *
     * @return Inscrit
     */
    public function setBloque($bloque)
    {
        $this->bloque = $bloque;

        return $this;
    }

    /**
     * Get bloque.
     *
     * @return string
     */
    public function getBloque()
    {
        return $this->bloque;
    }

    /**
     * Set idInitial.
     *
     * @param int $idInitial
     *
     * @return Inscrit
     */
    public function setIdInitial($idInitial)
    {
        $this->idInitial = $idInitial;

        return $this;
    }

    /**
     * Get idInitial.
     *
     * @return int
     */
    public function getIdInitial()
    {
        return $this->idInitial;
    }

    /**
     * Set inscritAnnuaireDefense.
     *
     * @param string $inscritAnnuaireDefense
     *
     * @return Inscrit
     */
    public function setInscritAnnuaireDefense($inscritAnnuaireDefense)
    {
        $this->inscritAnnuaireDefense = $inscritAnnuaireDefense;

        return $this;
    }

    /**
     * Get inscritAnnuaireDefense.
     *
     * @return string
     */
    public function getInscritAnnuaireDefense()
    {
        return $this->inscritAnnuaireDefense;
    }

    /**
     * Set dateCreation.
     *
     * @param string $dateCreation
     *
     * @return Inscrit
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation.
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateModification.
     *
     * @param string $dateModification
     *
     * @return Inscrit
     */
    public function setDateModification($dateModification)
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * Get dateModification.
     *
     * @return string
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }

    /**
     * Set tentativesMdp.
     *
     * @param int $tentativesMdp
     *
     * @return Inscrit
     */
    public function setTentativesMdp($tentativesMdp)
    {
        $this->tentativesMdp = $tentativesMdp;

        return $this;
    }

    /**
     * Get tentativesMdp.
     *
     * @return int
     */
    public function getTentativesMdp()
    {
        return $this->tentativesMdp;
    }

    /**
     * Set uid.
     *
     * @param string $uid
     *
     * @return Inscrit
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid.
     *
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    public function getEntreprise(): ?Entreprise
    {
        return $this->entreprise;
    }

    public function setEntreprise(?Entreprise $entreprise): self
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    /**
     * @return array
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function getRoles()
    {
        return ['ROLE_ENTREPRISE'];
    }

    /**
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function getUsername()
    {
        return $this->getLogin();
    }

    /**
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function getPassword()
    {
        return $this->mdp;
    }

    /**
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function getSalt()
    {
        return '';
    }

    /**
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function eraseCredentials()
    {
        $this->mot_de_passe = '';
    }

    /**
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function equals(UserInterface $user)
    {
        return $this->isEqualTo($user);
    }

    /**
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function isEqualTo(UserInterface $user)
    {
        return $user->getId() == $this->getId() && $user->getUsername() == $this->getUsername();
    }

    /**
     * @return array
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function getHabilitations()
    {
        return $this->habilitations;
    }

    /**
     * @param $habilitation
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function addHabilitation($habilitation)
    {
        $this->habilitations[] = $habilitation;
    }

    /**
     * @return $this
     */
    public function addOffre(Offre $offre)
    {
        $this->offres[] = $offre;
        $offre->setInscrit($this);

        return $this;
    }

    public function removeOffre(Offre $offre)
    {
        $this->offres->removeElement($offre);
    }

    /**
     * @return ArrayCollection
     */
    public function getOffres()
    {
        return $this->offres;
    }

    public function setEtablissement(Etablissement $etablissement = null): self
    {
        $this->etablissement = $etablissement;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEtablissement()
    {
        return $this->etablissement;
    }

    /**
     * @return string
     */
    public function getTypeHash(): string
    {
        return $this->typeHash;
    }

    /**
     * @param string $typeHash
     *
     * @return Inscrit
     */
    public function setTypeHash(string $typeHash): self
    {
        $this->typeHash = $typeHash;

        return $this;
    }

    public function getIdExterne(): string
    {
        return $this->idExterne;
    }

    public function setIdExterne(string $idExterne): Inscrit
    {
        $this->idExterne = $idExterne;
        return $this;
    }

    public function getRgpdCommunication(): ?bool
    {
        return $this->rgpdCommunication;
    }

    public function setRgpdCommunication(bool $rgpdCommunication): self
    {
        $this->rgpdCommunication = $rgpdCommunication;

        return $this;
    }

    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setDeletedAt($deletedAt): self
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }

    public function getDeleted()
    {
        return $this->deleted;
    }

    public function setDeleted($deleted): self
    {
        $this->deleted = $deleted;
        return $this;
    }

    public function __serialize(): array
    {
        return [$this->getId(), $this->getUsername()];
    }

    public function __unserialize(array $data): void
    {
        $this->id = $data[0];
        $this->login = $data[1];
    }
}
