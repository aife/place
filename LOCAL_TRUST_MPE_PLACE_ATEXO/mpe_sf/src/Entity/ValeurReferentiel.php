<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\Output\ValeurReferentielOutput;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\ORM\Mapping as ORM;

/**
 * ValeurReferentiel.
 *
 * @ORM\Table(name="ValeurReferentiel", indexes={@ORM\Index(name="id_referentiel", columns={"id_referentiel"})})
 * @ORM\Entity(repositoryClass="App\Repository\ValeurReferentielRepository")
 */
#[ApiResource(
    collectionOperations: ['get'],
    itemOperations: ['get'],
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: ValeurReferentielOutput::class,
    routePrefix: '/referentiels'
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'id' => 'exact',
        'idReferentiel' => 'exact',
        'libelleValeurReferentiel' => 'partial',
        'libelleValeurReferentielFr' => 'partial',
    ]
)]
class ValeurReferentiel
{
    const REFERENTIEL_CCAG = 20;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referentiel", inversedBy="valeursReferentielles")
     * @ORM\JoinColumn(name="id_referentiel", referencedColumnName="id_referentiel")
     */
    private ?Referentiel $referentiel = null;

    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private ?int $id = null;

    /**
     *
     * @ORM\Column(name="id_referentiel", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private string|int $idReferentiel = '0';

    /**
     * @ORM\Column(name="libelle_valeur_referentiel", type="text", length=65535, nullable=false)
     */
    private ?string $libelleValeurReferentiel = null;

    /**
     * @ORM\Column(name="libelle_valeur_referentiel_fr", type="text", length=65535, nullable=false)
     */
    private ?string $libelleValeurReferentielFr = null;

    /**
     * @ORM\Column(name="libelle_valeur_referentiel_en", type="text", length=65535, nullable=false)
     */
    private ?string $libelleValeurReferentielEn = null;

    /**
     * @ORM\Column(name="libelle_valeur_referentiel_es", type="text", length=65535, nullable=false)
     */
    private ?string $libelleValeurReferentielEs = null;

    /**
     * @ORM\Column(name="libelle_valeur_referentiel_su", type="text", length=65535, nullable=false)
     */
    private ?string $libelleValeurReferentielSu = null;

    /**
     * @ORM\Column(name="libelle_valeur_referentiel_du", type="text", length=65535, nullable=false)
     */
    private ?string $libelleValeurReferentielDu = null;

    /**
     * @ORM\Column(name="libelle_valeur_referentiel_cz", type="text", length=65535, nullable=false)
     */
    private ?string $libelleValeurReferentielCz = null;

    /**
     * @ORM\Column(name="libelle_valeur_referentiel_ar", type="text", length=65535, nullable=false)
     */
    private ?string $libelleValeurReferentielAr = null;

    /**
     * @ORM\Column(name="libelle_2", type="string", length=200, nullable=false)
     */
    private string $libelle2 = '';

    /**
     * @ORM\Column(name="libelle_valeur_referentiel_it", type="text", length=65535, nullable=false)
     */
    private ?string $libelleValeurReferentielIt = null;

    /**
     * Set id.
     *
     * @param int $id
     *
     * @return Valeurreferentiel
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idReferentiel.
     *
     * @param int $idReferentiel
     *
     * @return Valeurreferentiel
     */
    public function setIdReferentiel($idReferentiel)
    {
        $this->idReferentiel = $idReferentiel;

        return $this;
    }

    /**
     * Get idReferentiel.
     *
     * @return int
     */
    public function getIdReferentiel()
    {
        return $this->idReferentiel;
    }

    /**
     * Set libelleValeurReferentiel.
     *
     * @param string $libelleValeurReferentiel
     *
     * @return Valeurreferentiel
     */
    public function setLibelleValeurReferentiel($libelleValeurReferentiel)
    {
        $this->libelleValeurReferentiel = $libelleValeurReferentiel;

        return $this;
    }

    /**
     * Get libelleValeurReferentiel.
     *
     * @return string
     */
    public function getLibelleValeurReferentiel()
    {
        return $this->libelleValeurReferentiel;
    }

    /**
     * Set libelleValeurReferentielFr.
     *
     * @param string $libelleValeurReferentielFr
     *
     * @return Valeurreferentiel
     */
    public function setLibelleValeurReferentielFr($libelleValeurReferentielFr)
    {
        $this->libelleValeurReferentielFr = $libelleValeurReferentielFr;

        return $this;
    }

    /**
     * Get libelleValeurReferentielFr.
     *
     * @return string
     */
    public function getLibelleValeurReferentielFr()
    {
        return $this->libelleValeurReferentielFr;
    }

    /**
     * Set libelleValeurReferentielEn.
     *
     * @param string $libelleValeurReferentielEn
     *
     * @return Valeurreferentiel
     */
    public function setLibelleValeurReferentielEn($libelleValeurReferentielEn)
    {
        $this->libelleValeurReferentielEn = $libelleValeurReferentielEn;

        return $this;
    }

    /**
     * Get libelleValeurReferentielEn.
     *
     * @return string
     */
    public function getLibelleValeurReferentielEn()
    {
        return $this->libelleValeurReferentielEn;
    }

    /**
     * Set libelleValeurReferentielEs.
     *
     * @param string $libelleValeurReferentielEs
     *
     * @return Valeurreferentiel
     */
    public function setLibelleValeurReferentielEs($libelleValeurReferentielEs)
    {
        $this->libelleValeurReferentielEs = $libelleValeurReferentielEs;

        return $this;
    }

    /**
     * Get libelleValeurReferentielEs.
     *
     * @return string
     */
    public function getLibelleValeurReferentielEs()
    {
        return $this->libelleValeurReferentielEs;
    }

    /**
     * Set libelleValeurReferentielSu.
     *
     * @param string $libelleValeurReferentielSu
     *
     * @return Valeurreferentiel
     */
    public function setLibelleValeurReferentielSu($libelleValeurReferentielSu)
    {
        $this->libelleValeurReferentielSu = $libelleValeurReferentielSu;

        return $this;
    }

    /**
     * Get libelleValeurReferentielSu.
     *
     * @return string
     */
    public function getLibelleValeurReferentielSu()
    {
        return $this->libelleValeurReferentielSu;
    }

    /**
     * Set libelleValeurReferentielDu.
     *
     * @param string $libelleValeurReferentielDu
     *
     * @return Valeurreferentiel
     */
    public function setLibelleValeurReferentielDu($libelleValeurReferentielDu)
    {
        $this->libelleValeurReferentielDu = $libelleValeurReferentielDu;

        return $this;
    }

    /**
     * Get libelleValeurReferentielDu.
     *
     * @return string
     */
    public function getLibelleValeurReferentielDu()
    {
        return $this->libelleValeurReferentielDu;
    }

    /**
     * Set libelleValeurReferentielCz.
     *
     * @param string $libelleValeurReferentielCz
     *
     * @return Valeurreferentiel
     */
    public function setLibelleValeurReferentielCz($libelleValeurReferentielCz)
    {
        $this->libelleValeurReferentielCz = $libelleValeurReferentielCz;

        return $this;
    }

    /**
     * Get libelleValeurReferentielCz.
     *
     * @return string
     */
    public function getLibelleValeurReferentielCz()
    {
        return $this->libelleValeurReferentielCz;
    }

    /**
     * Set libelleValeurReferentielAr.
     *
     * @param string $libelleValeurReferentielAr
     *
     * @return Valeurreferentiel
     */
    public function setLibelleValeurReferentielAr($libelleValeurReferentielAr)
    {
        $this->libelleValeurReferentielAr = $libelleValeurReferentielAr;

        return $this;
    }

    /**
     * Get libelleValeurReferentielAr.
     *
     * @return string
     */
    public function getLibelleValeurReferentielAr()
    {
        return $this->libelleValeurReferentielAr;
    }

    /**
     * Set libelle2.
     *
     * @param string $libelle2
     *
     * @return Valeurreferentiel
     */
    public function setLibelle2($libelle2)
    {
        $this->libelle2 = $libelle2;

        return $this;
    }

    /**
     * Get libelle2.
     *
     * @return string
     */
    public function getLibelle2()
    {
        return $this->libelle2;
    }

    /**
     * Set libelleValeurReferentielIt.
     *
     * @param string $libelleValeurReferentielIt
     *
     * @return Valeurreferentiel
     */
    public function setLibelleValeurReferentielIt($libelleValeurReferentielIt)
    {
        $this->libelleValeurReferentielIt = $libelleValeurReferentielIt;

        return $this;
    }

    /**
     * Get libelleValeurReferentielIt.
     *
     * @return string
     */
    public function getLibelleValeurReferentielIt()
    {
        return $this->libelleValeurReferentielIt;
    }

    /**
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function setReferentiel(Referentiel $referentiel)
    {
        $this->referentiel = $referentiel;
    }

    /**
     * @return Referentiel
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016*/
    public function getReferentiel()
    {
        return $this->referentiel;
    }

    /**
     * Recuperer le libelle traduit.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getLibelleValeurReferentielTraduit()
    {
        //TODO reprendre le traitement sur MPE
    }
}
