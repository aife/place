<?php

namespace App\Entity;

use App\Repository\DocumentTemplateSurchargeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DocumentTemplateSurchargeRepository")
 */
class DocumentTemplateSurcharge
{
    public const PRIORITY_CLIENT = 1;
    public const PRIORITY_ORGANISME = 2;

    public const PATH_CLIENT = 'doc-template/client/';
    public const PATH_ORGANISME = 'doc-template/organisme/';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity=DocumentTemplate::class, inversedBy="documentTemplateSurcharges")
     * @ORM\JoinColumn(name="document_template_id", referencedColumnName="id")
     */
    private ?DocumentTemplate $documentTemplate = null;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $nomAfficher = null;

    /**
     * @ORM\ManyToOne(targetEntity=Organisme::class, inversedBy="documentTemplateSurcharges")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private ?Organisme $organisme = null;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $priority = null;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $fileName = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDocumentTemplate(): DocumentTemplate
    {
        return $this->documentTemplate;
    }

    public function setDocumentTemplate(DocumentTemplate $documentTemplate): self
    {
        $this->documentTemplate = $documentTemplate;

        return $this;
    }

    public function getNomAfficher(): ?string
    {
        return $this->nomAfficher;
    }

    public function setNomAfficher(string $nomAfficher): self
    {
        $this->nomAfficher = $nomAfficher;

        return $this;
    }

    public function getOrganisme(): ?Organisme
    {
        return $this->organisme;
    }

    public function setOrganisme(?Organisme $organisme): self
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    public function setFileName(string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }
}
