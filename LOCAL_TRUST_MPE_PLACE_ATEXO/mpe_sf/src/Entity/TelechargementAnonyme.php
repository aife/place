<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * TelechargementAnonyme.
 *
 * @ORM\Table(name="TelechargementAnonyme", indexes={@ORM\Index(name="organisme", columns={"organisme"}), @ORM\Index(name="consultation_id", columns={"consultation_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\TelechargementAnonymeRepository")
 */
class TelechargementAnonyme
{
    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme", inversedBy="telechargementAnonymes")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private ?Organisme $organisme = null;

    /**
     * @var Consultation
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation", inversedBy="telechargementAnonymes")
     * @ORM\JoinColumn(name="consultation_id", referencedColumnName="id")
     */
    private $consultation;

    /**
     * @ORM\Column(name="datetelechargement", type="string", length=20, nullable=false)
     */
    private string|DateTime $dateTelechargement = '0000-00-00 00:00:00';

    /**
     * @ORM\Column(name="tirage_plan", type="integer", nullable=false)
     */
    private string|int $tiragePlan = '0';

    /**
     * @ORM\Column(name="support", type="integer")
     */
    private ?int $support = null;

    /**
     * @ORM\Column(name="noms_fichiers_dce", type="text", length=65535, nullable=true)
     */
    private ?string $nomsFichiersDce = null;

    /**
     * @ORM\Column(name="poids_telechargement", type="integer", nullable=false)
     */
    private string|int $poidsTelechargement = '0';

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Organisme
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param Organisme $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    public function getConsultation()
    {
        return $this->consultation;
    }

    public function setConsultation($consultation)
    {
        $this->consultation = $consultation;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateTelechargement()
    {
        return $this->dateTelechargement;
    }

    /**
     * @param DateTime $dateTelechargement
     */
    public function setDateTelechargement($dateTelechargement)
    {
        $this->dateTelechargement = $dateTelechargement;
    }

    /**
     * @return int
     */
    public function getTiragePlan()
    {
        return $this->tiragePlan;
    }

    /**
     * @param int $tiragePlan
     */
    public function setTiragePlan($tiragePlan)
    {
        $this->tiragePlan = $tiragePlan;
    }

    /**
     * @return int
     */
    public function getSupport()
    {
        return $this->support;
    }

    /**
     * @param int $support
     */
    public function setSupport($support)
    {
        $this->support = $support;
    }

    /**
     * @return string
     */
    public function getNomsFichiersDce()
    {
        return $this->nomsFichiersDce;
    }

    /**
     * @param string $nomsFichiersDce
     */
    public function setNomsFichiersDce($nomsFichiersDce)
    {
        $this->nomsFichiersDce = $nomsFichiersDce;
    }

    /**
     * @return int
     */
    public function getPoidsTelechargement()
    {
        return $this->poidsTelechargement;
    }

    /**
     * @param int $poidsTelechargement
     */
    public function setPoidsTelechargement($poidsTelechargement)
    {
        $this->poidsTelechargement = $poidsTelechargement;
    }
}
