<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="Avis_Pub")
 * @ORM\Entity()
 */
class AvisPub
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="organisme", type="string")
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(name="consultation_id", type="integer")
     */
    private ?int $consultationId = null;

    /**
     * @ORM\Column(name="statut", type="string", nullable=false)
     */
    private string $statut = '0';

    /**
     * @ORM\Column(name="id_agent", type="integer")
     */
    private ?int $idAgent = null;

    /**
     * @ORM\Column(name="id_agent_validateur", type="integer", nullable=true)
     */
    private ?int $idAgentValidateur = null;

    /**
     * @ORM\Column(name="id_agent_validateur_eco", type="integer", nullable=true)
     */
    private ?int $idAgentValidateurEco = null;

    /**
     * @ORM\Column(name="id_agent_validateur_sip", type="integer", nullable=true)
     */
    private ?int $idAgentValidateurSip = null;

    /**
     * @ORM\Column(name="date_creation", type="string", length=20, nullable=false)
     */
    private ?string $dateCreation = null;

    /**
     * @ORM\Column(name="date_envoi", type="string", length=20, nullable=true)
     */
    private ?string $dateEnvoi = null;

    /**
     * @ORM\Column(name="type_avis", type="integer", nullable=false)
     */
    private ?int $typeAvis = null;

    /**
     * @ORM\Column(name="date_publication", type="string", length=20, nullable=true)
     */
    private ?string $datePublication = null;

    /**
     * @ORM\Column(name="date_validation", type="string", length=20, nullable=true)
     */
    private ?string $dateValidation = null;

    /**
     * @ORM\Column(name="Sip", type="string", length=5, nullable=true)
     */
    private ?string $sip = null;

    /**
     * @ORM\Column(name="id_avis_portail", type="integer", nullable=true)
     */
    private ?int $idAvisPortail = null;

    /**
     * @ORM\Column(name="id_avis_presse", type="integer", nullable=true)
     */
    private ?int $idAvisPresse = null;

    /**
     * @ORM\Column(name="id_avis_pdf_opoce", type="integer", nullable=true)
     */
    private ?int $idAvisPdfOpoce = null;

    /**
     * @ORM\Column(name="id_blob_logo", type="integer", nullable=true)
     */
    private ?int $idBlobLogo = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return int
     */
    public function getConsultationId()
    {
        return $this->consultationId;
    }

    /**
     * @param int $consultationId
     */
    public function setConsultationId($consultationId)
    {
        $this->consultationId = $consultationId;
    }

    public function getStatut(): string
    {
        return $this->statut;
    }

    public function setStatut(string $statut)
    {
        $this->statut = $statut;
    }

    public function getIdAgent(): int
    {
        return $this->idAgent;
    }

    public function setIdAgent(int $idAgent)
    {
        $this->idAgent = $idAgent;
    }

    public function getIdAgentValidateur(): int
    {
        return $this->idAgentValidateur;
    }

    public function setIdAgentValidateur(int $idAgentValidateur)
    {
        $this->idAgentValidateur = $idAgentValidateur;
    }

    public function getIdAgentValidateurEco(): int
    {
        return $this->idAgentValidateurEco;
    }

    public function setIdAgentValidateurEco(int $idAgentValidateurEco)
    {
        $this->idAgentValidateurEco = $idAgentValidateurEco;
    }

    public function getIdAgentValidateurSip(): int
    {
        return $this->idAgentValidateurSip;
    }

    public function setIdAgentValidateurSip(int $idAgentValidateurSip)
    {
        $this->idAgentValidateurSip = $idAgentValidateurSip;
    }

    public function getDateCreation(): string
    {
        return $this->dateCreation;
    }

    public function setDateCreation(string $dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }

    public function getDateEnvoi(): string
    {
        return $this->dateEnvoi;
    }

    public function setDateEnvoi(string $dateEnvoi)
    {
        $this->dateEnvoi = $dateEnvoi;
    }

    public function getTypeAvis(): int
    {
        return $this->typeAvis;
    }

    public function setTypeAvis(int $typeAvis)
    {
        $this->typeAvis = $typeAvis;
    }

    public function getDatePublication(): string
    {
        return $this->datePublication;
    }

    public function setDatePublication(string $datePublication)
    {
        $this->datePublication = $datePublication;
    }

    public function getDateValidation(): string
    {
        return $this->dateValidation;
    }

    public function setDateValidation(string $dateValidation)
    {
        $this->dateValidation = $dateValidation;
    }

    public function getSip(): string
    {
        return $this->sip;
    }

    public function setSip(string $sip)
    {
        $this->sip = $sip;
    }

    public function getIdAvisPortail(): int
    {
        return $this->idAvisPortail;
    }

    public function setIdAvisPortail(int $idAvisPortail)
    {
        $this->idAvisPortail = $idAvisPortail;
    }

    public function getIdAvisPresse(): int
    {
        return $this->idAvisPresse;
    }

    public function setIdAvisPresse(int $idAvisPresse)
    {
        $this->idAvisPresse = $idAvisPresse;
    }

    public function getIdAvisPdfOpoce(): int
    {
        return $this->idAvisPdfOpoce;
    }

    public function setIdAvisPdfOpoce(int $idAvisPdfOpoce)
    {
        $this->idAvisPdfOpoce = $idAvisPdfOpoce;
    }

    public function getIdBlobLogo(): int
    {
        return $this->idBlobLogo;
    }

    public function setIdBlobLogo(int $idBlobLogo)
    {
        $this->idBlobLogo = $idBlobLogo;
    }
}
