<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\EntrepriseDocumentRepository;

/**
 * EntrepriseDocument.
 *
 * @ORM\Table(name="t_document_entreprise", indexes={@ORM\Index(name="entreprise_document", columns={"id_entreprise"}), @ORM\Index(name="entreprise_document_type", columns={"id_type_document"})})
 * @ORM\Entity(repositoryClass=EntrepriseDocumentRepository::class)
 */
class EntrepriseDocument
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Entreprise", inversedBy="documents")
     * @ORM\JoinColumn(name="id_entreprise", referencedColumnName="id")
     */
    private ?Entreprise $entreprise = null;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\EntrepriseDocumentVersion", mappedBy="document")
     * @ORM\JoinColumn(name="id_document", referencedColumnName="id_document")
     */
    private Collection $versions;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DocumentType", inversedBy="documents")
     * @ORM\JoinColumn(name="id_document", referencedColumnName="id_type_document")
     */
    private ?DocumentType $typeDocument = null;

    /**
     *
     * @ORM\Column(name="id_document", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $idDocument;

    /**
     * @ORM\Column(name="id_entreprise", type="integer", nullable=false)
     */
    private ?int $idEntreprise = null;

    /**
     * @ORM\Column(name="nom_document", type="string", length=100, nullable=false)
     */
    private ?string $nomDocument = null;

    /**
     * @ORM\Column(name="id_type_document", type="integer", nullable=false)
     */
    private ?int $idTypeDocument = null;

    /**
     * @ORM\Column(name="id_derniere_version", type="integer", nullable=true)
     */
    private ?int $idDerniereVersion = null;

    /**
     * @ORM\Column(name="id_etablissement", type="integer", nullable=false)
     */
    private ?int $idEtablissement = null;

    /**
     * Get idDocument.
     *
     * @return int
     */
    public function getIdDocument()
    {
        return $this->idDocument;
    }

    /**
     * Set idEntreprise.
     *
     * @param int $idEntreprise
     *
     * @return EntrepriseDocument
     */
    public function setIdEntreprise($idEntreprise)
    {
        $this->idEntreprise = $idEntreprise;

        return $this;
    }

    /**
     * Get idEntreprise.
     *
     * @return int
     */
    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    /**
     * Set nomDocument.
     *
     * @param string $nomDocument
     *
     * @return EntrepriseDocument
     */
    public function setNomDocument($nomDocument)
    {
        $this->nomDocument = $nomDocument;

        return $this;
    }

    /**
     * Get nomDocument.
     *
     * @return string
     */
    public function getNomDocument()
    {
        return $this->nomDocument;
    }

    /**
     * Set idTypeDocument.
     *
     * @param int $idTypeDocument
     *
     * @return EntrepriseDocument
     */
    public function setIdTypeDocument($idTypeDocument)
    {
        $this->idTypeDocument = $idTypeDocument;

        return $this;
    }

    /**
     * Get idTypeDocument.
     *
     * @return int
     */
    public function getIdTypeDocument()
    {
        return $this->idTypeDocument;
    }

    /**
     * Set idDerniereVersion.
     *
     * @param int $idDerniereVersion
     *
     * @return EntrepriseDocument
     */
    public function setIdDerniereVersion($idDerniereVersion)
    {
        $this->idDerniereVersion = $idDerniereVersion;

        return $this;
    }

    /**
     * Get idDerniereVersion.
     *
     * @return int
     */
    public function getIdDerniereVersion()
    {
        return $this->idDerniereVersion;
    }

    /**
     * Entreprise constructor.
     */
    public function __construct()
    {
        $this->versions = new ArrayCollection();
    }

    /**
     * @return $this
     */
    public function addVersionDocument(EntrepriseDocumentVersion $version)
    {
        $this->versions[] = $version;
        $version->setDocument($this);

        return $this;
    }

    public function removeVersionDocument(EntrepriseDocumentVersion $version)
    {
        $this->versions->removeElement($version);
    }

    /**
     * @return ArrayCollection
     */
    public function getVersionsDocument()
    {
        return $this->versions;
    }

    public function setEntreprise(Entreprise $entreprise)
    {
        $this->entreprise = $entreprise;
    }

    /**
     * @return mixed
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }

    /**
     * @return DocumentType
     */
    public function getTypeDocument()
    {
        return $this->typeDocument;
    }

    public function setTypeDocument(DocumentType $typeDocument)
    {
        $this->typeDocument = $typeDocument;
    }

    /**
     * Set idEtablissement.
     *
     * @param int $idEtablissement
     *
     * @return EntrepriseDocument
     */
    public function setIdEtablissement($idEtablissement)
    {
        $this->idEtablissement = $idEtablissement;

        return $this;
    }

    /**
     * Get idEtablissement.
     *
     * @return int
     */
    public function getIdEtablissement()
    {
        return $this->idEtablissement;
    }
}
