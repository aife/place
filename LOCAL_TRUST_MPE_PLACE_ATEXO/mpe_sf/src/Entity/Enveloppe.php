<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Dto\Input\EnveloppeInput;
use App\Dto\Output\EnveloppeOutput;
use App\Entity\DossierVolumineux\DossierVolumineux;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Enveloppe.
 *
 * @ORM\Table(name="Enveloppe", indexes={@ORM\Index(name="offre_id", columns={"offre_id", "organisme"})})
 * @ORM\Entity(repositoryClass="App\Repository\EnveloppeRepository")
 */

#[ApiResource(
    collectionOperations: [
        'post' => ["security_post_denormalize" => "is_granted('CREATE_ENVELOPPE', object)"]
    ],
    itemOperations: [
        'get' => ["security" => "is_granted('VIEW_ENVELOPPE', object)"]
    ],
    input: EnveloppeInput::class,
    output: EnveloppeOutput::class,
)]
class Enveloppe
{
    public const TYPE_ENVELOPPE_BY_ID = [1 => 'CANDIDATURE', 2 => 'OFFRE', 3 => 'ANONYMAT', 4 => 'OFFRE_TECHNIQUE'];

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Offre", inversedBy="enveloppes")
     * @ORM\JoinColumn(name="offre_id", referencedColumnName="id")
     */
    private ?Offre $offre = null;
    // @ORM\JoinColumn(name="id_enveloppe_electro", referencedColumnName="id_enveloppe")
    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\EnveloppeFichier",
     *     mappedBy="enveloppe", cascade={"persist"}, fetch="EAGER")
     * @ORM\OrderBy({"typeFichier" = "ASC", "idFichier" = "ASC"})
     */
    public Collection $fichiers;

    /**
     *
     * @ORM\Column(name="id_enveloppe_electro", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $idEnveloppeElectro = null;

    /**
     * @ORM\Column(name="organisme", type="string", length=30)
     */
    private $organisme = '';

    /**
     * @ORM\Column(name="offre_id", type="integer", nullable=false)
     */
    private $offreId = '0';

    /**
     * @ORM\Column(name="champs_optionnels", type="blob", nullable=false)
     */
    private $champsOptionnels = null;

    /**
     * @ORM\Column(name="fichier", type="integer", nullable=false)
     */
    private $fichier = '0';

    /**
     * @ORM\Column(name="supprime", type="string", length=1, nullable=false)
     */
    private string $supprime = '0';

    /**
     * @ORM\Column(name="cryptage", type="string", length=1, nullable=false)
     */
    private string $cryptage = '1';

    /**
     * @ORM\Column(name="nom_fichier", type="string", length=255, nullable=false)
     */
    private string $nomFichier = '';

    /**
     * @ORM\Column(name="hash", type="string", length=40, nullable=false)
     */
    private string $hash = '';

    /**
     * @ORM\Column(name="type_env", type="integer", nullable=false)
     */
    private string|int $typeEnv = '0';

    /**
     * @ORM\Column(name="sous_pli", type="integer", nullable=false)
     */
    private string|int $sousPli = '0';

    /**
     * @ORM\Column(name="attribue", type="string", length=1, nullable=false)
     */
    private string $attribue = '0';

    /**
     * @ORM\Column(name="dateheure_ouverture", type="string", length=20, nullable=false)
     */
    private string $dateheureOuverture = '0000-00-00 00:00:00';

    /**
     * @ORM\Column(name="agent_id_ouverture", type="integer", nullable=true)
     */
    private ?int $agentIdOuverture = null;

    /**
     * @ORM\Column(name="agent_id_ouverture2", type="integer", nullable=true)
     */
    private ?int $agentIdOuverture2 = null;

    /**
     * @ORM\Column(name="donnees_ouverture", type="blob", nullable=false)
     */
    private $donneesOuverture = null;

    /**
     * @ORM\Column(name="horodatage_donnees_ouverture", type="blob", nullable=false)
     */
    private $horodatageDonneesOuverture = null;

    /**
     * @ORM\Column(name="statut_enveloppe", type="integer", nullable=false)
     */
    private string|int $statutEnveloppe = '1';

    /**
     * @ORM\Column(name="agent_telechargement", type="integer", nullable=true)
     */
    private ?int $agentTelechargement = null;

    /**
     * @ORM\Column(name="date_telechargement", type="string", length=20, nullable=true)
     */
    private ?string $dateTelechargement = null;

    /**
     * @ORM\Column(name="repertoire_telechargement", type="string", length=100, nullable=true)
     */
    private ?string $repertoireTelechargement = null;

    /**
     * @ORM\Column(name="nom_agent_ouverture", type="string", length=100, nullable=false)
     */
    private string $nomAgentOuverture = '';

    /**
     * @ORM\Column(name="dateheure_ouverture_agent2", type="string", length=20, nullable=false)
     */
    private string $dateheureOuvertureAgent2 = '0000-00-00 00:00:00';

    /**
     * @ORM\Column(name="enveloppe_fictive", type="string", nullable=true)
     */
    private ?string $enveloppeFictive = '0';

    /**
     * @ORM\Column(name="uid_response", type="string", length=255, nullable=true)
     */
    private ?string $uidResponse = '';

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DossierVolumineux\DossierVolumineux" , inversedBy="enveloppes")
     * @ORM\JoinColumn(name="id_dossier_volumineux", referencedColumnName="id")
     */
    private ?DossierVolumineux $dossierVolumineux = null;

    /**
     * @var string
     *
     * @ORM\Column(name="date_debut_dechiffrement", type="datetime", nullable=true)
     */
    private $dateDebutDechiffrement;

    /**
     * Set idEnveloppeElectro.
     *
     * @param int $idEnveloppeElectro
     *
     * @return Enveloppe
     */
    public function setIdEnveloppeElectro($idEnveloppeElectro)
    {
        $this->idEnveloppeElectro = $idEnveloppeElectro;

        return $this;
    }

    /**
     * Get idEnveloppeElectro.
     *
     * @return int
     */
    public function getIdEnveloppeElectro()
    {
        return $this->idEnveloppeElectro;
    }

    /**
     * Set organisme.
     *
     * @param string $organisme
     *
     * @return Enveloppe
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * Get organisme.
     *
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * Set offreId.
     *
     * @param int $offreId
     *
     * @return Enveloppe
     */
    public function setOffreId($offreId)
    {
        $this->offreId = $offreId;

        return $this;
    }

    /**
     * Get offreId.
     *
     * @return int
     */
    public function getOffreId()
    {
        return $this->offreId;
    }

    /**
     * Set champsOptionnels.
     *
     * @param string $champsOptionnels
     *
     * @return Enveloppe
     */
    public function setChampsOptionnels($champsOptionnels)
    {
        $this->champsOptionnels = $champsOptionnels;

        return $this;
    }

    /**
     * Get champsOptionnels.
     *
     * @return string
     */
    public function getChampsOptionnels()
    {
        return $this->champsOptionnels;
    }

    /**
     * Set fichier.
     *
     * @param int $fichier
     *
     * @return Enveloppe
     */
    public function setFichier($fichier)
    {
        $this->fichier = $fichier;

        return $this;
    }

    /**
     * Get fichier.
     *
     * @return int
     */
    public function getFichier()
    {
        return $this->fichier;
    }

    /**
     * Set supprime.
     *
     * @param string $supprime
     *
     * @return Enveloppe
     */
    public function setSupprime($supprime)
    {
        $this->supprime = $supprime;

        return $this;
    }

    /**
     * Get supprime.
     *
     * @return string
     */
    public function getSupprime()
    {
        return $this->supprime;
    }

    /**
     * Set cryptage.
     *
     * @param string $cryptage
     *
     * @return Enveloppe
     */
    public function setCryptage($cryptage)
    {
        $this->cryptage = $cryptage;

        return $this;
    }

    /**
     * Get cryptage.
     *
     * @return string
     */
    public function getCryptage()
    {
        return $this->cryptage;
    }

    /**
     * Set nomFichier.
     *
     * @param string $nomFichier
     *
     * @return Enveloppe
     */
    public function setNomFichier($nomFichier)
    {
        $this->nomFichier = $nomFichier;

        return $this;
    }

    /**
     * Get nomFichier.
     *
     * @return string
     */
    public function getNomFichier()
    {
        return $this->nomFichier;
    }

    /**
     * Set hash.
     *
     * @param string $hash
     *
     * @return Enveloppe
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash.
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set typeEnv.
     *
     * @param int $typeEnv
     *
     * @return Enveloppe
     */
    public function setTypeEnv($typeEnv)
    {
        $this->typeEnv = $typeEnv;

        return $this;
    }

    /**
     * Get typeEnv.
     *
     * @return int
     */
    public function getTypeEnv()
    {
        return $this->typeEnv;
    }

    /**
     * Set sousPli.
     *
     * @param int $sousPli
     *
     * @return Enveloppe
     */
    public function setSousPli($sousPli)
    {
        $this->sousPli = $sousPli;

        return $this;
    }

    /**
     * Get sousPli.
     *
     * @return int
     */
    public function getSousPli()
    {
        return $this->sousPli;
    }

    /**
     * Set attribue.
     *
     * @param string $attribue
     *
     * @return Enveloppe
     */
    public function setAttribue($attribue)
    {
        $this->attribue = $attribue;

        return $this;
    }

    /**
     * Get attribue.
     *
     * @return string
     */
    public function getAttribue()
    {
        return $this->attribue;
    }

    /**
     * Set dateheureOuverture.
     *
     * @param string $dateheureOuverture
     *
     * @return Enveloppe
     */
    public function setDateheureOuverture($dateheureOuverture)
    {
        $this->dateheureOuverture = $dateheureOuverture;

        return $this;
    }

    /**
     * Get dateheureOuverture.
     *
     * @return string
     */
    public function getDateheureOuverture()
    {
        return $this->dateheureOuverture;
    }

    /**
     * Set agentIdOuverture.
     *
     * @param int $agentIdOuverture
     *
     * @return Enveloppe
     */
    public function setAgentIdOuverture($agentIdOuverture)
    {
        $this->agentIdOuverture = $agentIdOuverture;

        return $this;
    }

    /**
     * Get agentIdOuverture.
     *
     * @return int
     */
    public function getAgentIdOuverture()
    {
        return $this->agentIdOuverture;
    }

    /**
     * Set agentIdOuverture2.
     *
     * @param int $agentIdOuverture2
     *
     * @return Enveloppe
     */
    public function setAgentIdOuverture2($agentIdOuverture2)
    {
        $this->agentIdOuverture2 = $agentIdOuverture2;

        return $this;
    }

    /**
     * Get agentIdOuverture2.
     *
     * @return int
     */
    public function getAgentIdOuverture2()
    {
        return $this->agentIdOuverture2;
    }

    /**
     * Set donneesOuverture.
     *
     * @param string $donneesOuverture
     *
     * @return Enveloppe
     */
    public function setDonneesOuverture($donneesOuverture)
    {
        $this->donneesOuverture = $donneesOuverture;

        return $this;
    }

    /**
     * Get donneesOuverture.
     *
     * @return string
     */
    public function getDonneesOuverture()
    {
        return $this->donneesOuverture;
    }

    /**
     * Set horodatageDonneesOuverture.
     *
     * @param string $horodatageDonneesOuverture
     *
     * @return Enveloppe
     */
    public function setHorodatageDonneesOuverture($horodatageDonneesOuverture)
    {
        $this->horodatageDonneesOuverture = $horodatageDonneesOuverture;

        return $this;
    }

    /**
     * Get horodatageDonneesOuverture.
     *
     * @return string
     */
    public function getHorodatageDonneesOuverture()
    {
        return $this->horodatageDonneesOuverture;
    }

    /**
     * Set statutEnveloppe.
     *
     * @param int $statutEnveloppe
     *
     * @return Enveloppe
     */
    public function setStatutEnveloppe($statutEnveloppe)
    {
        $this->statutEnveloppe = $statutEnveloppe;

        return $this;
    }

    /**
     * Get statutEnveloppe.
     *
     * @return int
     */
    public function getStatutEnveloppe()
    {
        return $this->statutEnveloppe;
    }

    /**
     * Set agentTelechargement.
     *
     * @param int $agentTelechargement
     *
     * @return Enveloppe
     */
    public function setAgentTelechargement($agentTelechargement)
    {
        $this->agentTelechargement = $agentTelechargement;

        return $this;
    }

    /**
     * Get agentTelechargement.
     *
     * @return int
     */
    public function getAgentTelechargement()
    {
        return $this->agentTelechargement;
    }

    /**
     * Set dateTelechargement.
     *
     * @param string $dateTelechargement
     *
     * @return Enveloppe
     */
    public function setDateTelechargement($dateTelechargement)
    {
        $this->dateTelechargement = $dateTelechargement;

        return $this;
    }

    /**
     * Get dateTelechargement.
     *
     * @return string
     */
    public function getDateTelechargement()
    {
        return $this->dateTelechargement;
    }

    /**
     * Set repertoireTelechargement.
     *
     * @param string $repertoireTelechargement
     *
     * @return Enveloppe
     */
    public function setRepertoireTelechargement($repertoireTelechargement)
    {
        $this->repertoireTelechargement = $repertoireTelechargement;

        return $this;
    }

    /**
     * Get repertoireTelechargement.
     *
     * @return string
     */
    public function getRepertoireTelechargement()
    {
        return $this->repertoireTelechargement;
    }

    /**
     * Set nomAgentOuverture.
     *
     * @param string $nomAgentOuverture
     *
     * @return Enveloppe
     */
    public function setNomAgentOuverture($nomAgentOuverture)
    {
        $this->nomAgentOuverture = $nomAgentOuverture;

        return $this;
    }

    /**
     * Get nomAgentOuverture.
     *
     * @return string
     */
    public function getNomAgentOuverture()
    {
        return $this->nomAgentOuverture;
    }

    /**
     * Set dateheureOuvertureAgent2.
     *
     * @param string $dateheureOuvertureAgent2
     *
     * @return Enveloppe
     */
    public function setDateheureOuvertureAgent2($dateheureOuvertureAgent2)
    {
        $this->dateheureOuvertureAgent2 = $dateheureOuvertureAgent2;

        return $this;
    }

    /**
     * Get dateheureOuvertureAgent2.
     *
     * @return string
     */
    public function getDateheureOuvertureAgent2()
    {
        return $this->dateheureOuvertureAgent2;
    }

    /**
     * @return Offre
     */
    public function getOffre()
    {
        return $this->offre;
    }

    public function setOffre(Offre $offre)
    {
        $this->offre = $offre;
    }

    /**
     * Consultation constructor.
     */
    public function __construct()
    {
        $this->fichiers = new ArrayCollection();
    }

    /**
     * @return $this
     */
    public function addFichierEnveloppe(EnveloppeFichier $fichierenveloppe)
    {
        $this->fichiers[] = $fichierenveloppe;
        $fichierenveloppe->setEnveloppe($this);

        return $this;
    }

    public function removeFichierEnveloppe(EnveloppeFichier $fichierenveloppe)
    {
        $this->fichiers->removeElement($fichierenveloppe);
    }

    /**
     * @return ArrayCollection
     */
    public function getFichierEnveloppes()
    {
        return $this->fichiers;
    }

    /**
     * Set enveloppeFictive.
     *
     * @param string $enveloppeFictive
     *
     * @return Enveloppe
     */
    public function setEnveloppeFictive($enveloppeFictive)
    {
        $this->enveloppeFictive = $enveloppeFictive;

        return $this;
    }

    /**
     * Get enveloppeFictive.
     *
     * @return string
     */
    public function getEnveloppeFictive()
    {
        return $this->enveloppeFictive;
    }

    public function getFichiers()
    {
        return $this->fichiers;
    }

    /**
     * @return string
     */
    public function getUidResponse()
    {
        return $this->uidResponse;
    }

    /**
     * @param string $uidResponse
     */
    public function setUidResponse($uidResponse)
    {
        $this->uidResponse = $uidResponse;
    }

    /**
     * @return DossierVolumineux
     */
    public function getDossierVolumineux()
    {
        // Je n'ai pas mis le returnType :DossierVolumineux car dossierVolumineux est nullable
        // et dans ce cas une erreur est lancée "on attend un object mais null est retourné"
        return $this->dossierVolumineux;
    }

    public function setDossierVolumineux(DossierVolumineux $dossierVolumineux)
    {
        $this->dossierVolumineux = $dossierVolumineux;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateDebutDechiffrement()
    {
        return $this->dateDebutDechiffrement;
    }

    /**
     * @param $dateDebutDechiffrement
     *
     * @return $this
     */
    public function setDateDebutDechiffrement($dateDebutDechiffrement)
    {
        $this->dateDebutDechiffrement = $dateDebutDechiffrement;

        return $this;
    }
}
