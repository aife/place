<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class HistorisationMotDePasse.
 *
 * @ORM\Table(name="historisation_mot_de_passe")
 * @ORM\Entity(repositoryClass="App\Repository\HistorisationMotDePasseRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class HistorisationMotDePasse
{
    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="ancien_mot_de_passe",  type="string", length=64, unique=true)
     */
    private ?string $ancienMotDePasse = null;

    /**
     * @ORM\Column(name="date_modification", type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $dateModification;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Inscrit" )
     * @ORM\JoinColumn(name="id_inscrit", referencedColumnName="id")
     */
    private ?Inscrit $inscrit = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getAncienMotDePasse(): string
    {
        return $this->ancienMotDePasse;
    }

    public function setAncienMotDePasse(string $ancienMotDePasse)
    {
        $this->ancienMotDePasse = $ancienMotDePasse;
    }

    public function getDateModification(): DateTime
    {
        return $this->dateModification;
    }

    public function setDateModification(DateTime $dateModification)
    {
        $this->dateModification = $dateModification;
    }

    public function getInscrit(): Inscrit
    {
        return $this->inscrit;
    }

    public function setInscrit(Inscrit $inscrit)
    {
        $this->inscrit = $inscrit;
    }
}
