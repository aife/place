<?php

namespace App\Entity;

use Exception;
use Doctrine\ORM\Mapping as ORM;

/**
 * CertificatPermanent.
 *
 * @ORM\Table(name="CertificatPermanent")
 * @ORM\Entity(repositoryClass="App\Repository\CertificatPermanentRepository")
 */
class CertificatPermanent
{
    public const FOREIGN_KEYS = [
        'organisme' => [
            'class' => Organisme::class,
            'property' => 'acronyme',
            'isObject' => false,
            'migrationService' => 'atexo.service.migration.organisme',
        ],
        'serviceId' => [
            'class' => Service::class,
            'isObject' => false,
            'property' => 'id',
            'migrationService' => 'atexo.service.migration.service',
        ],
        'idAgent' => [
            'class' => Agent::class,
            'isObject' => false,
            'property' => 'id',
            'migrationService' => 'atexo.service.migration.agent',
        ],
    ];

    private $em;
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     *
     * @ORM\Column(name="organisme", type="string", length=30)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private string $organisme = '';

    /**
     * @ORM\Column(name="Titre", type="string", length=100, nullable=false)
     */
    private string $titre = '';

    /**
     * @ORM\Column(name="Prenom", type="string", length=100, nullable=false)
     */
    private string $prenom = '';

    /**
     * @ORM\Column(name="Nom", type="string", length=100, nullable=false)
     */
    private string $nom = '';

    /**
     * @ORM\Column(name="EMail", type="string", length=100, nullable=false)
     */
    private string $email = '';

    /**
     * @ORM\Column(name="Certificat", type="text", length=16777215, nullable=false)
     */
    private ?string $certificat = null;

    /**
     * @ORM\Column(name="service_id", type="integer", nullable=true)
     */
    private ?int $serviceId = null;

    /**
     * @ORM\Column(name="master_key", type="string", nullable=false)
     */
    private string $masterKey = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="id_agent", type="integer", nullable=true)
     */
    private $idAgent = '0';

    /**
     * @ORM\Column(name="CSP", type="string", length=200, nullable=true)
     */
    private ?string $csp = '';

    /**
     * @ORM\Column(name="date_modification", type="string", length=200, nullable=true)
     */
    private ?string $dateModification = '';

    /**
     * @ORM\Column(name="certificat_universelle", type="string", nullable=false)
     */
    private string $certificatUniverselle = '0';

    /**
     * Set id.
     *
     * @param int $id
     *
     * @return CertificatPermanent
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set organisme.
     *
     * @param string $organisme
     *
     * @return CertificatPermanent
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * Get organisme.
     *
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * Set titre.
     *
     * @param string $titre
     *
     * @return CertificatPermanent
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre.
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set prenom.
     *
     * @param string $prenom
     *
     * @return CertificatPermanent
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom.
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return CertificatPermanent
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return CertificatPermanent
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set certificat.
     *
     * @param string $certificat
     *
     * @return CertificatPermanent
     */
    public function setCertificat($certificat)
    {
        $this->certificat = $certificat;

        return $this;
    }

    /**
     * Get certificat.
     *
     * @return string
     */
    public function getCertificat()
    {
        return $this->certificat;
    }

    /**
     * Set serviceId.
     *
     * @param int $serviceId
     *
     * @return CertificatPermanent
     */
    public function setServiceId($serviceId)
    {
        $this->serviceId = $serviceId;

        return $this;
    }

    /**
     * Get serviceId.
     *
     * @return int
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * Set masterKey.
     *
     * @param string $masterKey
     *
     * @return CertificatPermanent
     */
    public function setMasterKey($masterKey)
    {
        $this->masterKey = $masterKey;

        return $this;
    }

    /**
     * Get masterKey.
     *
     * @return string
     */
    public function getMasterKey()
    {
        return $this->masterKey;
    }

    /**
     * Set idAgent.
     *
     * @param int $idAgent
     *
     * @return CertificatPermanent
     */
    public function setIdAgent($idAgent)
    {
        $this->idAgent = $idAgent;

        return $this;
    }

    /**
     * Get idAgent.
     *
     * @return int
     */
    public function getIdAgent()
    {
        return $this->idAgent;
    }

    /**
     * Set csp.
     *
     * @param string $csp
     *
     * @return CertificatPermanent
     */
    public function setCsp($csp)
    {
        $this->csp = $csp;

        return $this;
    }

    /**
     * Get csp.
     *
     * @return string
     */
    public function getCsp()
    {
        return $this->csp;
    }

    /**
     * Set dateModification.
     *
     * @param string $dateModification
     *
     * @return CertificatPermanent
     */
    public function setDateModification($dateModification)
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * Get dateModification.
     *
     * @return string
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }

    /**
     * Set certificatUniverselle.
     *
     * @param string $certificatUniverselle
     *
     * @return CertificatPermanent
     */
    public function setCertificatUniverselle($certificatUniverselle)
    {
        $this->certificatUniverselle = $certificatUniverselle;

        return $this;
    }

    /**
     * Get certificatUniverselle.
     *
     * @return string
     */
    public function getCertificatUniverselle()
    {
        return $this->certificatUniverselle;
    }

    /**
     * Cette fonction permet de copier le JAL lui attribuant
     * un nouveau service et un nouvel organisme de destination
     * passés en paramètres.
     *
     * @param int $idServiceDestination identifiant du service de destination
     * @param string $organismeDestination acronyme de l'organisme de destination dans lequel se trouve le clone
     *
     * @return bool|CertificatPermanent
     *
     * @throws Exception
     */
    public function copier(int $idServiceDestination, string $organismeDestination)
    {
        $criterias = ['organisme' => $organismeDestination, 'id' => $idServiceDestination];

        $serviceDestinationObject = $this->em->getRepository(Service::class)
            ->findOneBy($criterias);
        $organismeDestinationObject = $this->em->getRepository(Organisme::class)
            ->findByAcronymeOrganisme($organismeDestination);

        if ($serviceDestinationObject instanceof Service && $organismeDestinationObject instanceof Organisme) {
            $certificatPermanent = clone $this;
            $certificatPermanent->setId($this->getMaxId() + 1);
            $certificatPermanent->setServiceId($idServiceDestination);
            $certificatPermanent->setOrganisme($organismeDestination);
            $certificatPermanent->setIdAgent(0);
            $this->em->persist($certificatPermanent);
            $this->em->flush();

            return $certificatPermanent;
        }

        return false;
    }

    public function getMaxId()
    {
        $maxId = $this->em->getRepository(CertificatPermanent::class)
            ->findBy([], ['id' => 'DESC'], 1, 0)[0];

        return $maxId->getId();
    }
}
