<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class DocumentServeurDocs.
 *
 * @ORM\Table(name="document_serveur_docs")
 * @ORM\Entity
 */
class DocumentServeurDocs
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="nom", type="string", length=100, nullable=false)
     */
    private ?string $nom = null;

    /**
     * @ORM\Column(name="poids", type="integer", nullable=false)
     */
    private ?int $poids = null;

    /**
     * @ORM\Column(name="type", type="string", length=20, nullable=false)
     */
    private ?string $type = null;

    /**
     * @ORM\Column(name="extension", type="string", length=10, nullable=false)
     */
    private ?string $extension = null;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return $this
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return int
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * @return $this
     */
    public function setPoids(int $poids)
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return $this
     */
    public function setType(string $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @return $this
     */
    public function setExtension(string $extension)
    {
        $this->extension = $extension;

        return $this;
    }
}
