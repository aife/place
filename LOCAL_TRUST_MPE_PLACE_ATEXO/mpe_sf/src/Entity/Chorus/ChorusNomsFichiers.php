<?php

namespace App\Entity\Chorus;

use Doctrine\ORM\Mapping as ORM;

/**
 * ChorusNomsFichiers.
 *
 * @ORM\Table(name="chorus_noms_fichiers", indexes={@ORM\Index(name="id_echange", columns={"id_echange"}),
 *     @ORM\Index(name="acronyme_organisme", columns={"acronyme_organisme"})})
 * @ORM\Entity(repositoryClass="App\Repository\Chorus\ChorusNomsFichiersRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ChorusNomsFichiers
{
    /**
     *
     * @ORM\Column(name="id_echange", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private ?int $idEchange = null;

    /**
     * @ORM\Column(name="numero_ordre", type="integer", length=10)
     */
    private ?int $numeroOrdre = null;

    /**
     * @ORM\Column(name="nom_fichier", type="string", length=200)
     */
    private ?string $nomFichier = null;

    /**
     * @ORM\Column(name="acronyme_organisme", type="string", length=200)
     */
    private ?string $acronymeOrganisme = null;

    /**
     * @ORM\Column(name="type_fichier", type="string", length=3)
     */
    private ?string $typeFichier = null;

    /**
     * @ORM\Column(name="date_ajout", type="string", length=20, nullable=true)
     */
    private ?string $dateAjout = null;

    public function getIdEchange(): int
    {
        return $this->idEchange;
    }

    public function setIdEchange(int $idEchange)
    {
        $this->idEchange = $idEchange;
    }

    public function getNumeroOrdre(): int
    {
        return $this->numeroOrdre;
    }

    public function setNumeroOrdre(int $numeroOrdre)
    {
        $this->numeroOrdre = $numeroOrdre;
    }

    public function getNomFichier(): string
    {
        return $this->nomFichier;
    }

    public function setNomFichier(string $nomFichier)
    {
        $this->nomFichier = $nomFichier;
    }

    public function getAcronymeOrganisme(): string
    {
        return $this->acronymeOrganisme;
    }

    public function setAcronymeOrganisme(string $acronymeOrganisme)
    {
        $this->acronymeOrganisme = $acronymeOrganisme;
    }

    public function getTypeFichier(): string
    {
        return $this->typeFichier;
    }

    public function setTypeFichier(string $typeFichier)
    {
        $this->typeFichier = $typeFichier;
    }

    public function getDateAjout(): string
    {
        return $this->dateAjout;
    }

    public function setDateAjout(string $dateAjout)
    {
        $this->dateAjout = $dateAjout;
    }
}
