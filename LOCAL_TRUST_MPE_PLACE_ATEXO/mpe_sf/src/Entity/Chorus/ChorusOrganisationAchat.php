<?php

namespace App\Entity\Chorus;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use App\Repository\Chorus\ChorusGroupementAchatRepository;

/**
 *
 *
 * @ORM\Table(name="Chorus_organisation_achat", uniqueConstraints={
 *          @UniqueConstraint(name="chorus_groupement_achat_id",columns={"id"}),
 *          @UniqueConstraint(name="chorus_groupement_achat_organisme",columns={"organisme"})
 *      })
 * @ORM\Entity(repositoryClass="App\Repository\Chorus\ChorusOrganisationAchatRepository")
 */
class ChorusOrganisationAchat
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=10)
     */
    private $organisme;


    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    private $code;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", options={"default":1})
     */
    private $actif = 1;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1)
     */
    private $fluxFen111 = '1';

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1)
     */
    private $fluxFen211 = '1';

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): ChorusOrganisationAchat
    {
        $this->id = $id;
        return $this;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme): ChorusOrganisationAchat
    {
        $this->organisme = $organisme;
        return $this;
    }

    public function getLibelle(): string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): ChorusOrganisationAchat
    {
        $this->libelle = $libelle;
        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }


    public function setCode(string $code): ChorusOrganisationAchat
    {
        $this->code = $code;
        return $this;
    }

    public function getActif(): int
    {
        return $this->actif;
    }

    public function setActif(int $actif): ChorusOrganisationAchat
    {
        $this->actif = $actif;
        return $this;
    }

    public function getFluxFen111(): string
    {
        return $this->fluxFen111;
    }

    public function setFluxFen111(string $fluxFen111): ChorusOrganisationAchat
    {
        $this->fluxFen111 = $fluxFen111;
        return $this;
    }

    public function getFluxFen211(): string
    {
        return $this->fluxFen211;
    }

    public function setFluxFen211(string $fluxFen211): ChorusOrganisationAchat
    {
        $this->fluxFen211 = $fluxFen211;
        return $this;
    }
}