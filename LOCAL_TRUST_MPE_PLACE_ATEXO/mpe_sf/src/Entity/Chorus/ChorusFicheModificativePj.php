<?php

namespace App\Entity\Chorus;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Table(name="t_chorus_fiche_modificative_pj")
 * @ORM\Entity(repositoryClass="App\Repository\Chorus\ChorusFicheModificativePjRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ChorusFicheModificativePj
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="id_fiche_modificative_pj")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idFicheModificativePj;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="App\Entity\Chorus\ChorusFicheModificative", inversedBy="ficheModificativePj", cascade={"persist"})
     * @ORM\JoinColumn(name="id_fiche_modificative", referencedColumnName="id_fiche_modificative")
     */
    private $idFicheModificative;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=300)
     */
    private $nomFichier;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20)
     */
    private $fichier;

    /**
     * @var string
     *
     * @ORM\Column(type="blob")
     */
    private $horodatage;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20)
     */
    private $untrusteddate;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $taille;

    /**
     * @return int
     */
    public function getIdFicheModificativePj(): int
    {
        return $this->idFicheModificativePj;
    }

    /**
     * @return int
     */
    public function getIdFicheModificative(): ChorusFicheModificative
    {
        return $this->idFicheModificative;
    }

    /**
     * @param int $idFicheModificative
     * @return ChorusFicheModificativePj
     */
    public function setIdFicheModificative(ChorusFicheModificative $idFicheModificative): ChorusFicheModificativePj
    {
        $this->idFicheModificative = $idFicheModificative;
        return $this;
    }

    /**
     * @return string
     */
    public function getNomFichier(): string
    {
        return $this->nomFichier;
    }

    /**
     * @param string $nomFichier
     * @return ChorusFicheModificativePj
     */
    public function setNomFichier(string $nomFichier): ChorusFicheModificativePj
    {
        $this->nomFichier = $nomFichier;
        return $this;
    }

    /**
     * @return string
     */
    public function getFichier(): string
    {
        return $this->fichier;
    }

    /**
     * @param string $fichier
     * @return ChorusFicheModificativePj
     */
    public function setFichier(string $fichier): ChorusFicheModificativePj
    {
        $this->fichier = $fichier;
        return $this;
    }

    /**
     * @return string
     */
    public function getHorodatage(): string
    {
        return $this->horodatage;
    }

    /**
     * @param string $horodatage
     * @return ChorusFicheModificativePj
     */
    public function setHorodatage(string $horodatage): ChorusFicheModificativePj
    {
        $this->horodatage = $horodatage;
        return $this;
    }

    /**
     * @return string
     */
    public function getUntrusteddate(): string
    {
        return $this->untrusteddate;
    }

    /**
     * @param string $untrusteddate
     * @return ChorusFicheModificativePj
     */
    public function setUntrusteddate(string $untrusteddate): ChorusFicheModificativePj
    {
        $this->untrusteddate = $untrusteddate;
        return $this;
    }

    /**
     * @return int
     */
    public function getTaille(): int
    {
        return $this->taille;
    }

    /**
     * @param int $taille
     * @return ChorusFicheModificativePj
     */
    public function setTaille(int $taille): ChorusFicheModificativePj
    {
        $this->taille = $taille;
        return $this;
    }
}
