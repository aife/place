<?php

namespace App\Entity\Chorus;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Table(name="Chorus_groupement_achat", uniqueConstraints={
 *          @UniqueConstraint(name="chorus_groupement_achat_id",columns={"id"}),
 *          @UniqueConstraint(name="chorus_groupement_achat_organisme",columns={"organisme"}),
 *          @UniqueConstraint(name="chorus_groupement_achat_id_oa",columns={"id_oa"}),
 *          @UniqueConstraint(name="chorus_groupement_achat_id_service",columns={"service_id"})
 *      })
 * @ORM\Entity(repositoryClass="App\Repository\Chorus\ChorusGroupementAchatRepository")
 */
class ChorusGroupementAchat
{
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     */
    private int $idOa = 0;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $libelle = null;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private ?string $code = null;

    /**
     * @ORM\Column(type="integer", options={"default":1})
     */
    private int $actif = 1;

    /**
     * @ORM\Column(name="service_id", type="integer", nullable=true)
     */
    private ?int $serviceId = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme)
    {
        $this->organisme = $organisme;
    }

    public function getIdOa(): int
    {
        return $this->idOa;
    }

    public function setIdOa(int $idOa = 0)
    {
        $this->idOa = $idOa;
    }

    public function getLibelle(): string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle)
    {
        $this->libelle = $libelle;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code)
    {
        $this->code = $code;
    }

    public function getActif(): int
    {
        return $this->actif;
    }

    public function setActif(int $actif = 1)
    {
        $this->actif = $actif;
    }

    public function getServiceId(): int
    {
        return $this->serviceId;
    }

    public function setServiceId(int $serviceId = null)
    {
        $this->serviceId = $serviceId;
    }
}
