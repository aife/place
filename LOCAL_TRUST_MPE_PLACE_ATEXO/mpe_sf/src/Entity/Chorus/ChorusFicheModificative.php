<?php

namespace App\Entity\Chorus;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Table(name="t_chorus_fiche_modificative")
 * @ORM\Entity(repositoryClass="App\Repository\Chorus\ChorusFicheModificativeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ChorusFicheModificative
{

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="id_fiche_modificative")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idFicheModificative;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $idEchange;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=30)
     */
    private $organisme;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $typeModification;

    /**
     * @var date
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $datePrevueNotification;

    /**
     * @var date
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateFinMarche;

    /**
     * @var date
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateFinMarcheModifie;

    /**
     * @var decimal
     *
     * @ORM\Column(type="decimal", nullable=true)
     */
    private $montantMarche;

    /**
     * @var decimal
     *
     * @ORM\Column(type="decimal", nullable=true)
     */
    private $montantActe;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $tauxTva;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $nombreFournisseurCotraitant;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $localitesFournisseurs;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $sirenFournisseur;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $siretFournisseur;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $nomFournisseur;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $typeFournisseur;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $visaAccf;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $visaPrefet;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $remarque;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $idBlobPieceJustificatives;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $idBlobFicheModificative;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)*/
    private $dateCreation;

    /**
     * @var string
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateModification;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Chorus\ChorusFicheModificativePj", mappedBy="idFicheModificative", cascade={"persist", "remove"})
     */
    private Collection $ficheModificativePj;

    public function __construct()
    {
        $this->ficheModificativePj = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getIdFicheModificative(): int
    {
        return $this->idFicheModificative;
    }

    /**
     * @return int
     */
    public function getIdEchange(): int
    {
        return $this->idEchange;
    }

    /**
     * @param int $idEchange
     */
    public function setIdEchange(int $idEchange): ChorusFicheModificative
    {
        $this->idEchange = $idEchange;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    /**
     * @param string $organisme
     * @return ChorusFicheModificative
     */
    public function setOrganisme(string $organisme): ChorusFicheModificative
    {
        $this->organisme = $organisme;
        return $this;
    }

    /**
     * @return int
     */
    public function getTypeModification(): ?int
    {
        return $this->typeModification;
    }

    /**
     * @param int $typeModification
     * @return ChorusFicheModificative
     */
    public function setTypeModification(?int $typeModification): ChorusFicheModificative
    {
        $this->typeModification = $typeModification;
        return $this;
    }

    /**
     * @return date
     */
    public function getDatePrevueNotification()
    {
        return $this->datePrevueNotification;
    }

    /**
     * @param date $datePrevueNotification
     */
    public function setDatePrevueNotification($datePrevueNotification = null): ChorusFicheModificative
    {
        $this->datePrevueNotification = $datePrevueNotification;
        return $this;
    }

    /**
     * @return date
     */
    public function getDateFinMarche()
    {
        return $this->dateFinMarche;
    }

    /**
     * @param date $dateFinMarche
     */
    public function setDateFinMarche($dateFinMarche = null): ChorusFicheModificative
    {
        $this->dateFinMarche = $dateFinMarche;
        return $this;
    }

    /**
     * @return date
     */
    public function getDateFinMarcheModifie()
    {
        return $this->dateFinMarcheModifie;
    }

    /**
     * @param date $dateFinMarcheModifie
     */
    public function setDateFinMarcheModifie($dateFinMarcheModifie = null): ChorusFicheModificative
    {
        $this->dateFinMarcheModifie = $dateFinMarcheModifie;
        return $this;
    }

    /**
     * @return decimal
     */
    public function getMontantMarche()
    {
        return $this->montantMarche;
    }

    /**
     * @param null $montantMarche
     * @return ChorusFicheModificative
     */
    public function setMontantMarche($montantMarche = null): ChorusFicheModificative
    {
        $this->montantMarche = $montantMarche;
        return $this;
    }

    /**
     * @return decimal
     */
    public function getMontantActe()
    {
        return $this->montantActe;
    }

    /**
     * @param null $montantActe
     * @return ChorusFicheModificative
     */
    public function setMontantActe($montantActe = null): ChorusFicheModificative
    {
        $this->montantActe = $montantActe;
        return $this;
    }

    /**
     * @return string
     */
    public function getTauxTva(): ?string
    {
        return $this->tauxTva;
    }

    /**
     * @param string $tauxTva
     */
    public function setTauxTva(?string $tauxTva): ChorusFicheModificative
    {
        $this->tauxTva = $tauxTva;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombreFournisseurCotraitant(): ?string
    {
        return $this->nombreFournisseurCotraitant;
    }

    /**
     * @param string|null $nombreFournisseurCotraitant
     * @return ChorusFicheModificative
     */
    public function setNombreFournisseurCotraitant(?string $nombreFournisseurCotraitant): ChorusFicheModificative
    {
        $this->nombreFournisseurCotraitant = $nombreFournisseurCotraitant;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocalitesFournisseurs(): ?string
    {
        return $this->localitesFournisseurs;
    }

    /**
     * @param string|null $localitesFournisseurs
     * @return ChorusFicheModificative
     */
    public function setLocalitesFournisseurs(?string $localitesFournisseurs): ChorusFicheModificative
    {
        $this->localitesFournisseurs = $localitesFournisseurs;
        return $this;
    }

    /**
     * @return string
     */
    public function getSirenFournisseur(): ?string
    {
        return $this->sirenFournisseur;
    }

    /**
     * @param string|null $sirenFournisseur
     * @return ChorusFicheModificative
     */
    public function setSirenFournisseur(?string $sirenFournisseur): ChorusFicheModificative
    {
        $this->sirenFournisseur = $sirenFournisseur;
        return $this;
    }

    /**
     * @return string
     */
    public function getSiretFournisseur(): ?string
    {
        return $this->siretFournisseur;
    }

    /**
     * @param string|null $siretFournisseur
     * @return ChorusFicheModificative
     */
    public function setSiretFournisseur(?string $siretFournisseur): ChorusFicheModificative
    {
        $this->siretFournisseur = $siretFournisseur;
        return $this;
    }

    /**
     * @return string
     */
    public function getNomFournisseur(): ?string
    {
        return $this->nomFournisseur;
    }

    /**
     * @param string|null $nomFournisseur
     * @return ChorusFicheModificative
     */
    public function setNomFournisseur(?string $nomFournisseur): ChorusFicheModificative
    {
        $this->nomFournisseur = $nomFournisseur;
        return $this;
    }

    /**
     * @return string
     */
    public function getTypeFournisseur(): ?string
    {
        return $this->typeFournisseur;
    }

    /**
     * @param string|null $typeFournisseur
     * @return ChorusFicheModificative
     */
    public function setTypeFournisseur(?string $typeFournisseur): ChorusFicheModificative
    {
        $this->typeFournisseur = $typeFournisseur;
        return $this;
    }

    /**
     * @return string
     */
    public function getVisaAccf(): string
    {
        return $this->visaAccf;
    }

    /**
     * @param string|null $visaAccf
     * @return ChorusFicheModificative
     */
    public function setVisaAccf($visaAccf = '0'): ChorusFicheModificative
    {
        $this->visaAccf = $visaAccf;
        return $this;
    }

    /**
     * @return string
     */
    public function getVisaPrefet(): string
    {
        return $this->visaPrefet;
    }

    /**
     * @param string|null $visaPrefet
     * @return ChorusFicheModificative
     */
    public function setVisaPrefet($visaPrefet = '0'): ChorusFicheModificative
    {
        $this->visaPrefet = $visaPrefet;
        return $this;
    }

    /**
     * @return string
     */
    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    /**
     * @param string|null $remarque
     * @return ChorusFicheModificative
     */
    public function setRemarque(?string $remarque): ChorusFicheModificative
    {
        $this->remarque = $remarque;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdBlobPieceJustificatives(): ?string
    {
        return $this->idBlobPieceJustificatives;
    }

    /**
     * @param string|null $idBlobPieceJustificatives
     * @return ChorusFicheModificative
     */
    public function setIdBlobPieceJustificatives(?string $idBlobPieceJustificatives): ChorusFicheModificative
    {
        $this->idBlobPieceJustificatives = $idBlobPieceJustificatives;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdBlobFicheModificative(): ?string
    {
        return $this->idBlobFicheModificative;
    }

    /**
     * @param string|null $idBlobFicheModificative
     * @return ChorusFicheModificative
     */
    public function setIdBlobFicheModificative(?string $idBlobFicheModificative): ChorusFicheModificative
    {
        $this->idBlobFicheModificative = $idBlobFicheModificative;
        return $this;
    }

    /**
     * @return string
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param DateTime $dateCreation
     */
    public function setDateCreation($dateCreation = null): ChorusFicheModificative
    {
        $this->dateCreation = $dateCreation;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }

    /**
     * @param string|null $dateModification
     * @return ChorusFicheModificative
     */
    public function setDateModification($dateModification = null): ChorusFicheModificative
    {
        $this->dateModification = $dateModification;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getFicheModificativePj(): ArrayCollection
    {
        return $this->ficheModificativePj;
    }
}
