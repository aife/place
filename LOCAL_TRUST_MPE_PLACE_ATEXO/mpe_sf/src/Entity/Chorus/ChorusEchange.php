<?php

namespace App\Entity\Chorus;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\ApiPlatformCustom\ContratContractants;
use App\Dto\Input\Chorus\ChorusEchangeInput;
use App\Dto\Output\ContratTitulaireOutput;
use App\Repository\Chorus\ChorusEchangeRepository;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 *
 * @ORM\Table(name="Chorus_echange")
 * @ORM\Entity(repositoryClass=ChorusEchangeRepository::class)
 */
#[ApiResource(
    collectionOperations: [
        "get",
        "create" => [
            "method"            => "POST",
            "input"             => ChorusEchangeInput::class,
            "security"          => "is_granted('CONTRAT_UPDATE')",
            "openapi_context"   => [
                "description"   => '## Ce WebService nécessite l\'habilitation "gerer_contrat".'
            ]
        ],
    ],
    itemOperations: [
        'get',
        "update" => [
            "method"            => "PUT",
            "input"             => ChorusEchangeInput::class,
            "security"          => "is_granted('CONTRAT_UPDATE')",
            "openapi_context"   => [
                "description"   => '## Ce WebService nécessite l\'habilitation "gerer_contrat".'
            ]
        ],
    ],
    denormalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: ContratTitulaireOutput::class,
)]
class ChorusEchange
{
    public const CHORUS_RETURN_STATUS_IN_PROGRESS = '0';
    public const ECHANGE_STATUS_DRAFT = '0';
    public const ECHANGE_STATUS_ERROR = '2';

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=30)
     */
    protected $organisme;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $idDecision;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $numOrdre;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $nomCreateur;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $prenomCreateur;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $idCreateur;

    /**
     * @var string
     *
     * @ORM\Column(name ="statutEchange", type="string", length=20, nullable=true)
     */
    protected $statutEchange;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $dateCreation;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $dateEnvoi;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $retourChorus;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=false)
     */
    protected $idEjAppliExt = 0;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $idOa;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $idGa;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $idTypeMarche;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $idTypeGroupement;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $idRegroupementComptable;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $dceItems;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $dumeAcheteurItems;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $dumeOeItems;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $idsEnvAe;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $idsEnvItems;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $idsPiecesExternes;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $idAgentEnvoi;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $nomAgent;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $prenomAgent;

    /**
     * @var boolean
     *
     * @ORM\Column(name="signACE", type="boolean", nullable=false)
     */
    protected $signAce = 0;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=9, nullable=true)
     */
    protected $siren;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    protected $siret;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $dateNotification;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $dateFinMarche;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $idActeJuridique;

    /**
     * @var string
     *
     * @ORM\Column(name="cpv_1", type="string", length=20, nullable=true)
     */
    protected $cpv1;

    /**
     * @var string
     *
     * @ORM\Column(name="cpv_2", type="text", nullable=true)
     */
    protected $cpv2;

    /**
     * @var string
     *
     * @ORM\Column(name="cpv_3", type="string", length=20, nullable=true)
     */
    protected $cpv3;

    /**
     * @var string
     *
     * @ORM\Column(name="cpv_4", type="string", length=20, nullable=true)
     */
    protected $cpv4;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $idTypeProcedure;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $idFormePrix;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $nbrEntreprisesCotraitantes;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $sousTraitanceDeclaree;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $carteAchat;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $clauseSociale;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $clauseEnvironnementale;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $nbrPropositionRecues;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $nbrPropositionDematerialisees;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $nom_fichier;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $erreurRejet;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    protected $codeCpvLibelle1;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    protected $codeCpvLibelle2;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    protected $codeCpvLibelle3;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    protected $codeCpvLibelle4;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $piecesNotifItems;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=225, nullable=false)
     */
    protected $idsBlobEnv;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $idsEnvSignItems;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $idsBlobSignEnv;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $montantHt;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $codePaysTitulaire;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $numeroSiretTitulaire;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $numeroSirenTitulaire;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $codesPaysCoTitulaire;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $numeroSiretCoTitulaire;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $numeroSirenCoTitulaire;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $ccagReference;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $pourcentageAvance;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $typeAvance;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $conditionsPaiement;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $identifiantAccordCadre;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $dateNotificationReelle;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $dateFinMarcheReelle;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $idsRapportSignature;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $typeContrat;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $intituleContrat;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $objetContrat;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $identifiantAccordCadreChapeau;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $typeFlux;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $typeEnvoi = 0;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $tmpFileName;

    /**
     * @var integer
     *
     * @ORM\Column(name="type_flux_a_envoyer", type="integer", nullable=true)
     */
    protected $typeFluxAEnvoyer;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $dateModification;

    /**
     * @var integer
     */
    protected $idContratTitulaire;

    /**
     * @var string
     *
     * @ORM\Column(name="raison_sociale_attributaire", type="string", length=255, nullable=true)
     */
    private $raisonSocialeAttributaire = null;

    /**
     * @var integer
     *
     * @ORM\Column(name="siret_attributaire", type="string", length=14, nullable=true)
     */
    private $siretAttributaire = null;

    /**
     * @var string
     *
     * @ORM\Column(name="code_ape", type="string", length=5, nullable=true)
     */
    private $codeApe = null;

    /**
     * @var string
     *
     * @ORM\Column(name="forme_juridique", type="string", length=255, nullable=true)
     */
    private $formeJuridique = null;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pme", type="boolean", options={"default":false})
     */
    private $pme = false;

    /**
     * @var string
     *
     * @ORM\Column(name="pays_territoire", type="string", length=255, nullable=true)
     */
    private $paysTerritoire = null;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_national_attributaire", type="string", length=20, nullable=true)
     */
    private $numeroNationalAttributaire = null;

    /**
     * @ORM\Column(name="uuid_externe_exec", type="string", length=50, nullable=true)
     */
    private ?string $uuidExterneExec = null;

    /**
     * @return int*/
    #[Groups('webservice')]
    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): ChorusEchange
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrganisme(): ?string
    {
        return $this->organisme;
    }

    /**
     * @param string $organisme
     * @return $this
     */
    public function setOrganisme(?string $organisme): self
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdDecision(): int
    {
        return $this->idDecision;
    }

    /**
     * @param int $idDecision
     * @return $this
     */
    public function setIdDecision(int $idDecision): self
    {
        $this->idDecision = $idDecision;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumOrdre(): ?string
    {
        return $this->numOrdre;
    }

    /**
     * @param string $numOrdre
     * @return $this
     */
    public function setNumOrdre(?string $numOrdre): self
    {
        $this->numOrdre = $numOrdre;

        return $this;
    }

    /**
     * @return string
     */
    public function getNomCreateur(): ?string
    {
        return $this->nomCreateur;
    }

    /**
     * @param string $nomCreateur
     * @return $this
     */
    public function setNomCreateur(?string $nomCreateur): self
    {
        $this->nomCreateur = $nomCreateur;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrenomCreateur(): ?string
    {
        return $this->prenomCreateur;
    }

    /**
     * @param string $prenomCreateur
     * @return $this
     */
    public function setPrenomCreateur(?string $prenomCreateur): self
    {
        $this->prenomCreateur = $prenomCreateur;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdCreateur(): int
    {
        return $this->idCreateur;
    }

    /**
     * @param int $idCreateur
     * @return $this
     */
    public function setIdCreateur(int $idCreateur): self
    {
        $this->idCreateur = $idCreateur;

        return $this;
    }

    /**
     * @return string*/
    #[Groups('webservice')]
    public function getStatutEchange(): ?string
    {
        return $this->statutEchange;
    }

    /**
     * @param string $statutEchange
     * @return $this
     */
    public function setStatutEchange(?string $statutEchange): self
    {
        $this->statutEchange = $statutEchange;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateCreation(): ?string
    {
        return $this->dateCreation;
    }

    /**
     * @param string $dateCreation
     * @return $this
     */
    public function setDateCreation(?string $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * @return string*/
    #[Groups('webservice')]
    public function getDateEnvoi(): ?string
    {
        return $this->dateEnvoi;
    }

    /**
     * @param string $dateEnvoi
     * @return $this
     */
    public function setDateEnvoi(?string $dateEnvoi): self
    {
        $this->dateEnvoi = $dateEnvoi;

        return $this;
    }

    /**
     * @return string
     */
    public function getRetourChorus(): ?string
    {
        return $this->retourChorus;
    }

    /**
     * @param string $retourChorus
     * @return $this
     */
    public function setRetourChorus(?string $retourChorus): self
    {
        $this->retourChorus = $retourChorus;

        return $this;
    }

    /**
     * @return string*/
    #[Groups('webservice')]
    public function getIdEjAppliExt(): ?string
    {
        return $this->idEjAppliExt;
    }

    /**
     * @param string $idEjAppliExt
     * @return $this
     */
    public function setIdEjAppliExt(?string $idEjAppliExt): self
    {
        $this->idEjAppliExt = $idEjAppliExt;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdOa(): ?int
    {
        return $this->idOa;
    }

    /**
     * @param int $idOa
     * @return $this
     */
    public function setIdOa(?int $idOa): self
    {
        $this->idOa = $idOa;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdGa(): ?int
    {
        return $this->idGa;
    }

    /**
     * @param int $idGa
     * @return $this
     */
    public function setIdGa(?int $idGa): self
    {
        $this->idGa = $idGa;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdTypeMarche(): ?int
    {
        return $this->idTypeMarche;
    }

    /**
     * @param int $idTypeMarche
     * @return $this
     */
    public function setIdTypeMarche(?int $idTypeMarche): self
    {
        $this->idTypeMarche = $idTypeMarche;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdTypeGroupement(): ?string
    {
        return $this->idTypeGroupement;
    }

    /**
     * @param string $idTypeGroupement
     * @return $this
     */
    public function setIdTypeGroupement(?string $idTypeGroupement): self
    {
        $this->idTypeGroupement = $idTypeGroupement;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdRegroupementComptable(): ?string
    {
        return $this->idRegroupementComptable;
    }

    /**
     * @param string $idRegroupementComptable
     * @return $this
     */
    public function setIdRegroupementComptable(?string $idRegroupementComptable): self
    {
        $this->idRegroupementComptable = $idRegroupementComptable;

        return $this;
    }

    /**
     * @return string
     */
    public function getDceItems(): ?string
    {
        return $this->dceItems;
    }

    /**
     * @param string $dceItems
     * @return $this
     */
    public function setDceItems(?string $dceItems): self
    {
        $this->dceItems = $dceItems;

        return $this;
    }

    /**
     * @return string
     */
    public function getDumeAcheteurItems(): ?string
    {
        return $this->dumeAcheteurItems;
    }

    /**
     * @param string $dumeAcheteurItems
     * @return $this
     */
    public function setDumeAcheteurItems(?string $dumeAcheteurItems): self
    {
        $this->dumeAcheteurItems = $dumeAcheteurItems;

        return $this;
    }

    /**
     * @return string
     */
    public function getDumeOeItems(): ?string
    {
        return $this->dumeOeItems;
    }

    /**
     * @param string $dumeOeItems
     * @return $this
     */
    public function setDumeOeItems(?string $dumeOeItems): self
    {
        $this->dumeOeItems = $dumeOeItems;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdsEnvAe(): ?string
    {
        return $this->idsEnvAe;
    }

    /**
     * @param string $idsEnvAe
     * @return $this
     */
    public function setIdsEnvAe(?string $idsEnvAe): self
    {
        $this->idsEnvAe = $idsEnvAe;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdsEnvItems(): ?string
    {
        return $this->idsEnvItems;
    }

    /**
     * @param string $idsEnvItems
     * @return $this
     */
    public function setIdsEnvItems(?string $idsEnvItems): self
    {
        $this->idsEnvItems = $idsEnvItems;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdsPiecesExternes(): ?string
    {
        return $this->idsPiecesExternes;
    }

    /**
     * @param string $idsPiecesExternes
     * @return $this
     */
    public function setIdsPiecesExternes(?string $idsPiecesExternes): self
    {
        $this->idsPiecesExternes = $idsPiecesExternes;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdAgentEnvoi(): int
    {
        return $this->idAgentEnvoi;
    }

    /**
     * @param int $idAgentEnvoi
     * @return $this
     */
    public function setIdAgentEnvoi(int $idAgentEnvoi): self
    {
        $this->idAgentEnvoi = $idAgentEnvoi;

        return $this;
    }

    /**
     * @return string
     */
    public function getNomAgent(): ?string
    {
        return $this->nomAgent;
    }

    /**
     * @param string $nomAgent
     * @return $this
     */
    public function setNomAgent(?string $nomAgent): self
    {
        $this->nomAgent = $nomAgent;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrenomAgent(): ?string
    {
        return $this->prenomAgent;
    }

    /**
     * @param string $prenomAgent
     * @return $this
     */
    public function setPrenomAgent(?string $prenomAgent): self
    {
        $this->prenomAgent = $prenomAgent;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSignAce()
    {
        return $this->signAce;
    }

    /**
     * @param $signAce
     * @return $this
     */
    public function setSignAce($signAce): self
    {
        $this->signAce = $signAce;

        return $this;
    }

    /**
     * @return string
     */
    public function getSiren(): ?string
    {
        return $this->siren;
    }

    /**
     * @param string $siren
     * @return $this
     */
    public function setSiren(?string $siren): self
    {
        $this->siren = $siren;

        return $this;
    }

    /**
     * @return string
     */
    public function getSiret(): ?string
    {
        return $this->siret;
    }

    /**
     * @param string $siret
     * @return $this
     */
    public function setSiret(?string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateNotification(): ?string
    {
        return $this->dateNotification;
    }

    /**
     * @param string $dateNotification
     * @return $this
     */
    public function setDateNotification(?string $dateNotification): self
    {
        $this->dateNotification = $dateNotification;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateFinMarche(): ?string
    {
        return $this->dateFinMarche;
    }

    /**
     * @param string $dateFinMarche
     * @return $this
     */
    public function setDateFinMarche(?string $dateFinMarche): self
    {
        $this->dateFinMarche = $dateFinMarche;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdActeJuridique(): ?string
    {
        return $this->idActeJuridique;
    }

    /**
     * @param string $idActeJuridique
     * @return $this
     */
    public function setIdActeJuridique(?string $idActeJuridique): self
    {
        $this->idActeJuridique = $idActeJuridique;

        return $this;
    }

    /**
     * @return string
     */
    public function getCpv1(): ?string
    {
        return $this->cpv1;
    }

    /**
     * @param string $cpv1
     * @return $this
     */
    public function setCpv1(?string $cpv1): self
    {
        $this->cpv1 = $cpv1;

        return $this;
    }

    /**
     * @return string
     */
    public function getCpv2(): ?string
    {
        return $this->cpv2;
    }

    /**
     * @param string $cpv2
     * @return $this
     */
    public function setCpv2(?string $cpv2): self
    {
        $this->cpv2 = $cpv2;

        return $this;
    }

    /**
     * @return string
     */
    public function getCpv3(): ?string
    {
        return $this->cpv3;
    }

    /**
     * @param string $cpv3
     * @return $this
     */
    public function setCpv3(?string $cpv3): self
    {
        $this->cpv3 = $cpv3;

        return $this;
    }

    /**
     * @return string
     */
    public function getCpv4(): ?string
    {
        return $this->cpv4;
    }

    /**
     * @param string $cpv4
     * @return $this
     */
    public function setCpv4(?string $cpv4): self
    {
        $this->cpv4 = $cpv4;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdTypeProcedure(): ?int
    {
        return $this->idTypeProcedure;
    }

    /**
     * @param int $idTypeProcedure
     * @return $this
     */
    public function setIdTypeProcedure(?int $idTypeProcedure): self
    {
        $this->idTypeProcedure = $idTypeProcedure;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdFormePrix(): ?string
    {
        return $this->idFormePrix;
    }

    /**
     * @param string $idFormePrix
     * @return $this
     */
    public function setIdFormePrix(?string $idFormePrix): self
    {
        $this->idFormePrix = $idFormePrix;

        return $this;
    }

    /**
     * @return string
     */
    public function getNbrEntreprisesCotraitantes(): ?string
    {
        return $this->nbrEntreprisesCotraitantes;
    }

    /**
     * @param string $nbrEntreprisesCotraitantes
     * @return $this
     */
    public function setNbrEntreprisesCotraitantes(?string $nbrEntreprisesCotraitantes): self
    {
        $this->nbrEntreprisesCotraitantes = $nbrEntreprisesCotraitantes;

        return $this;
    }

    /**
     * @return string
     */
    public function getSousTraitanceDeclaree(): ?string
    {
        return $this->sousTraitanceDeclaree;
    }

    /**
     * @param string $sousTraitanceDeclaree
     * @return $this
     */
    public function setSousTraitanceDeclaree(?string $sousTraitanceDeclaree): self
    {
        $this->sousTraitanceDeclaree = $sousTraitanceDeclaree;

        return $this;
    }

    /**
     * @return string
     */
    public function getCarteAchat(): ?string
    {
        return $this->carteAchat;
    }

    /**
     * @param string $carteAchat
     * @return $this
     */
    public function setCarteAchat(?string $carteAchat): self
    {
        $this->carteAchat = $carteAchat;

        return $this;
    }

    /**
     * @return string
     */
    public function getClauseSociale(): ?string
    {
        return $this->clauseSociale;
    }

    /**
     * @param string $clauseSociale
     * @return $this
     */
    public function setClauseSociale(?string $clauseSociale): self
    {
        $this->clauseSociale = $clauseSociale;

        return $this;
    }

    /**
     * @return string
     */
    public function getClauseEnvironnementale(): ?string
    {
        return $this->clauseEnvironnementale;
    }

    /**
     * @param string $clauseEnvironnementale
     * @return $this
     */
    public function setClauseEnvironnementale(?string $clauseEnvironnementale): self
    {
        $this->clauseEnvironnementale = $clauseEnvironnementale;

        return $this;
    }

    /**
     * @return string
     */
    public function getNbrPropositionRecues(): ?string
    {
        return $this->nbrPropositionRecues;
    }

    /**
     * @param string $nbrPropositionRecues
     */
    public function setNbrPropositionRecues(?string $nbrPropositionRecues): self
    {
        $this->nbrPropositionRecues = $nbrPropositionRecues;

        return $this;
    }

    /**
     * @return string
     */
    public function getNbrPropositionDematerialisees(): ?string
    {
        return $this->nbrPropositionDematerialisees;
    }

    /**
     * @param string $nbrPropositionDematerialisees
     */
    public function setNbrPropositionDematerialisees(?string $nbrPropositionDematerialisees): self
    {
        $this->nbrPropositionDematerialisees = $nbrPropositionDematerialisees;

        return $this;
    }

    /**
     * @return string
     */
    public function getNomFichier(): ?string
    {
        return $this->nom_fichier;
    }

    /**
     * @param string $nom_fichier
     */
    public function setNomFichier(?string $nom_fichier): self
    {
        $this->nom_fichier = $nom_fichier;

        return $this;
    }

    /**
     * @return ?string*/
    #[Groups('webservice')]
    public function getErreurRejet(): ?string
    {
        return $this->erreurRejet;
    }

    /**
     * @param string $erreurRejet
     */
    public function setErreurRejet(?string $erreurRejet): self
    {
        $this->erreurRejet = $erreurRejet;

        return $this;
    }

    /**
     * @return string
     */
    public function getCodeCpvLibelle1(): ?string
    {
        return $this->codeCpvLibelle1;
    }

    /**
     * @param string $codeCpvLibelle1
     */
    public function setCodeCpvLibelle1(?string $codeCpvLibelle1): self
    {
        $this->codeCpvLibelle1 = $codeCpvLibelle1;

        return $this;
    }

    /**
     * @return string
     */
    public function getCodeCpvLibelle2(): ?string
    {
        return $this->codeCpvLibelle2;
    }

    /**
     * @param string $codeCpvLibelle2
     */
    public function setCodeCpvLibelle2(?string $codeCpvLibelle2): self
    {
        $this->codeCpvLibelle2 = $codeCpvLibelle2;

        return $this;
    }

    /**
     * @return string
     */
    public function getCodeCpvLibelle3(): ?string
    {
        return $this->codeCpvLibelle3;
    }

    /**
     * @param string $codeCpvLibelle3
     */
    public function setCodeCpvLibelle3(?string $codeCpvLibelle3): self
    {
        $this->codeCpvLibelle3 = $codeCpvLibelle3;

        return $this;
    }

    /**
     * @return string
     */
    public function getCodeCpvLibelle4(): ?string
    {
        return $this->codeCpvLibelle4;
    }

    /**
     * @param string $codeCpvLibelle4
     */
    public function setCodeCpvLibelle4(?string $codeCpvLibelle4): self
    {
        $this->codeCpvLibelle4 = $codeCpvLibelle4;

        return $this;
    }

    /**
     * @return string
     */
    public function getPiecesNotifItems(): ?string
    {
        return $this->piecesNotifItems;
    }

    /**
     * @param string $piecesNotifItems
     */
    public function setPiecesNotifItems(?string $piecesNotifItems): self
    {
        $this->piecesNotifItems = $piecesNotifItems;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdsBlobEnv(): ?string
    {
        return $this->idsBlobEnv;
    }

    /**
     * @param string $idsBlobEnv
     */
    public function setIdsBlobEnv(?string $idsBlobEnv): self
    {
        $this->idsBlobEnv = $idsBlobEnv;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdsEnvSignItems(): ?string
    {
        return $this->idsEnvSignItems;
    }

    /**
     * @param string $idsEnvSignItems
     */
    public function setIdsEnvSignItems(?string $idsEnvSignItems): self
    {
        $this->idsEnvSignItems = $idsEnvSignItems;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdsBlobSignEnv(): ?string
    {
        return $this->idsBlobSignEnv;
    }

    /**
     * @param string $idsBlobSignEnv
     */
    public function setIdsBlobSignEnv(?string $idsBlobSignEnv): self
    {
        $this->idsBlobSignEnv = $idsBlobSignEnv;

        return $this;
    }

    /**
     * @return string
     */
    public function getMontantHt(): ?string
    {
        return $this->montantHt;
    }

    /**
     * @param string $montantHt
     */
    public function setMontantHt(?string $montantHt): self
    {
        $this->montantHt = $montantHt;

        return $this;
    }

    /**
     * @return string
     */
    public function getCodePaysTitulaire(): ?string
    {
        return $this->codePaysTitulaire;
    }

    /**
     * @param string $codePaysTitulaire
     */
    public function setCodePaysTitulaire(?string $codePaysTitulaire): self
    {
        $this->codePaysTitulaire = $codePaysTitulaire;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumeroSiretTitulaire(): ?string
    {
        return $this->numeroSiretTitulaire;
    }

    /**
     * @param string $numeroSiretTitulaire
     */
    public function setNumeroSiretTitulaire(?string $numeroSiretTitulaire): self
    {
        $this->numeroSiretTitulaire = $numeroSiretTitulaire;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumeroSirenTitulaire(): ?string
    {
        return $this->numeroSirenTitulaire;
    }

    /**
     * @param string $numeroSirenTitulaire
     */
    public function setNumeroSirenTitulaire(?string $numeroSirenTitulaire): self
    {
        $this->numeroSirenTitulaire = $numeroSirenTitulaire;

        return $this;
    }

    /**
     * @return string
     */
    public function getCodesPaysCoTitulaire(): ?string
    {
        return $this->codesPaysCoTitulaire;
    }

    /**
     * @param string $codesPaysCoTitulaire
     */
    public function setCodesPaysCoTitulaire(?string $codesPaysCoTitulaire): self
    {
        $this->codesPaysCoTitulaire = $codesPaysCoTitulaire;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumeroSiretCoTitulaire(): ?string
    {
        return $this->numeroSiretCoTitulaire;
    }

    /**
     * @param string $numeroSiretCoTitulaire
     */
    public function setNumeroSiretCoTitulaire(?string $numeroSiretCoTitulaire): self
    {
        $this->numeroSiretCoTitulaire = $numeroSiretCoTitulaire;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumeroSirenCoTitulaire(): ?string
    {
        return $this->numeroSirenCoTitulaire;
    }

    /**
     * @param string $numeroSirenCoTitulaire
     */
    public function setNumeroSirenCoTitulaire(?string $numeroSirenCoTitulaire): self
    {
        $this->numeroSirenCoTitulaire = $numeroSirenCoTitulaire;

        return $this;
    }

    /**
     * @return string
     */
    public function getCcagReference(): ?string
    {
        return $this->ccagReference;
    }

    /**
     * @param string $ccagReference
     */
    public function setCcagReference(?string $ccagReference): self
    {
        $this->ccagReference = $ccagReference;

        return $this;
    }

    /**
     * @return string
     */
    public function getPourcentageAvance(): ?string
    {
        return $this->pourcentageAvance;
    }

    /**
     * @param string $pourcentageAvance
     */
    public function setPourcentageAvance(?string $pourcentageAvance): self
    {
        $this->pourcentageAvance = $pourcentageAvance;

        return $this;
    }

    /**
     * @return string
     */
    public function getTypeAvance(): ?string
    {
        return $this->typeAvance;
    }

    /**
     * @param string $typeAvance
     */
    public function setTypeAvance(?string $typeAvance): self
    {
        $this->typeAvance = $typeAvance;

        return $this;
    }

    /**
     * @return string
     */
    public function getConditionsPaiement(): ?string
    {
        return $this->conditionsPaiement;
    }

    /**
     * @param string $conditionsPaiement
     */
    public function setConditionsPaiement(?string $conditionsPaiement): self
    {
        $this->conditionsPaiement = $conditionsPaiement;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdentifiantAccordCadre(): ?string
    {
        return $this->identifiantAccordCadre;
    }

    /**
     * @param string $identifiantAccordCadre
     */
    public function setIdentifiantAccordCadre(?string $identifiantAccordCadre): self
    {
        $this->identifiantAccordCadre = $identifiantAccordCadre;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateNotificationReelle(): ?string
    {
        return $this->dateNotificationReelle;
    }

    /**
     * @param string $dateNotificationReelle
     */
    public function setDateNotificationReelle(?string $dateNotificationReelle): self
    {
        $this->dateNotificationReelle = $dateNotificationReelle;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateFinMarcheReelle(): ?string
    {
        return $this->dateFinMarcheReelle;
    }

    /**
     * @param string $dateFinMarcheReelle
     */
    public function setDateFinMarcheReelle(?string $dateFinMarcheReelle): self
    {
        $this->dateFinMarcheReelle = $dateFinMarcheReelle;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdsRapportSignature(): ?string
    {
        return $this->idsRapportSignature;
    }

    /**
     * @param string $idsRapportSignature
     */
    public function setIdsRapportSignature(?string $idsRapportSignature): self
    {
        $this->idsRapportSignature = $idsRapportSignature;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getTypeContrat(): ?int
    {
        return $this->typeContrat;
    }

    /**
     * @param int $typeContrat
     * @return ChorusEchange
     */
    public function setTypeContrat(?int $typeContrat): self
    {
        $this->typeContrat = $typeContrat;

        return $this;
    }

    /**
     * @return string
     */
    public function getIntituleContrat(): ?string
    {
        return $this->intituleContrat;
    }

    /**
     * @param string $intituleContrat
     */
    public function setIntituleContrat(?string $intituleContrat): self
    {
        $this->intituleContrat = $intituleContrat;

        return $this;
    }

    /**
     * @return string
     */
    public function getObjetContrat(): ?string
    {
        return $this->objetContrat;
    }

    /**
     * @param string $objetContrat
     */
    public function setObjetContrat(?string $objetContrat): self
    {
        $this->objetContrat = $objetContrat;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdentifiantAccordCadreChapeau(): ?string
    {
        return $this->identifiantAccordCadreChapeau;
    }

    /**
     * @param string $identifiantAccordCadreChapeau
     */
    public function setIdentifiantAccordCadreChapeau(?string $identifiantAccordCadreChapeau): self
    {
        $this->identifiantAccordCadreChapeau = $identifiantAccordCadreChapeau;

        return $this;
    }

    /**
     * @return int
     */
    public function getTypeFlux(): ?int
    {
        return $this->typeFlux;
    }

    /**
     * @param int $typeFlux
     */
    public function setTypeFlux(int $typeFlux): self
    {
        $this->typeFlux = $typeFlux;

        return $this;
    }

    /**
     * @return int
     */
    public function getTypeEnvoi(): int
    {
        return $this->typeEnvoi;
    }

    /**
     * @param int $typeEnvoi
     */
    public function setTypeEnvoi(int $typeEnvoi): self
    {
        $this->typeEnvoi = $typeEnvoi;

        return $this;
    }

    /**
     * @return string
     */
    public function getTmpFileName(): ?string
    {
        return $this->tmpFileName;
    }

    /**
     * @param string $tmpFileName
     * @return $this
     */
    public function setTmpFileName(?string $tmpFileName): self
    {
        $this->tmpFileName = $tmpFileName;

        return $this;
    }

    /**
     * @return int
     */
    public function getTypeFluxAEnvoyer(): int
    {
        return $this->typeFluxAEnvoyer;
    }

    /**
     * @param int $typeFluxAEnvoyer
     * @return $this
     */
    public function setTypeFluxAEnvoyer(int $typeFluxAEnvoyer): self
    {
        $this->typeFluxAEnvoyer = $typeFluxAEnvoyer;

        return $this;
    }

    /**
     * @return ?int*/
    #[Groups('webservice')]
    public function getIdContratTitulaire(): ?int
    {
        return $this->idContratTitulaire;
    }

    /**
     * @param mixed $idContratTitulaire
     */
    public function setIdContratTitulaire(?int $idContratTitulaire): self
    {
        $this->idContratTitulaire = $idContratTitulaire;

        return $this;
    }

    /**
     * @return string
     */
    public function getRaisonSocialeAttributaire(): ?string
    {
        return $this->raisonSocialeAttributaire;
    }

    /**
     * @param string|null $raisonSocialeAttributaire
     * @return ChorusEchange
     */
    public function setRaisonSocialeAttributaire(?string $raisonSocialeAttributaire): self
    {
        $this->raisonSocialeAttributaire = $raisonSocialeAttributaire;

        return $this;
    }

    /**
     * @return string
     */
    public function getSiretAttributaire(): ?string
    {
        return $this->siretAttributaire;
    }

    /**
     * @param string|null $siretAttributaire
     * @return ChorusEchange
     */
    public function setSiretAttributaire(?string $siretAttributaire): self
    {
        $this->siretAttributaire = $siretAttributaire;

        return $this;
    }

    /**
     * @return string
     */
    public function getCodeApe(): ?string
    {
        return $this->codeApe;
    }

    /**
     * @param string|null $codeApe
     * @return ChorusEchange
     */
    public function setCodeApe(?string $codeApe): self
    {
        $this->codeApe = $codeApe;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormeJuridique(): ?string
    {
        return $this->formeJuridique;
    }

    /**
     * @param string|null $formeJuridique
     * @return ChorusEchange
     */
    public function setFormeJuridique(?string $formeJuridique): self
    {
        $this->formeJuridique = $formeJuridique;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPme(): bool
    {
        return $this->pme;
    }

    /**
     * @param bool $pme
     * @return ChorusEchange
     */
    public function setPme(bool $pme): self
    {
        $this->pme = $pme;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaysTerritoire(): ?string
    {
        return $this->paysTerritoire;
    }

    /**
     * @param string|null $paysTerritoire
     * @return ChorusEchange
     */
    public function setPaysTerritoire(?string $paysTerritoire): self
    {
        $this->paysTerritoire = $paysTerritoire;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumeroNationalAttributaire(): ?string
    {
        return $this->numeroNationalAttributaire;
    }

    /**
     * @param string|null $numeroNationalAttributaire
     * @return ChorusEchange
     */
    public function setNumeroNationalAttributaire(?string $numeroNationalAttributaire): self
    {
        $this->numeroNationalAttributaire = $numeroNationalAttributaire;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateModification(): ?string
    {
        return $this->dateModification;
    }

    /**
     * @param string $dateModification
     * @return $this
     */
    public function setDateModification(?string $dateModification): self
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    public function getUuidExterneExec(): ?string
    {
        return $this->uuidExterneExec;
    }

    public function setUuidExterneExec(?string $uuidExterneExec): self
    {
        $this->uuidExterneExec = $uuidExterneExec;

        return $this;
    }

    public function getRetourChorusLibelle(): string
    {
        switch ($this->getRetourChorus()) {
            case 'COM':
                return 'Intégré';
            case 'REJ':
                return 'Rejeté';
            case 'IRR':
                return 'Irrecevable';
            default:
                return 'Sans objet';
        }
    }

    public function getStatutEchangeLibelle(): string
    {
        switch ($this->getStatutEchange()) {
            case 'FAG':
                return 'Flux à générer';
            case 'EP':
                return 'Envoi Planifié';
            case 'ECV':
                return 'En cours de vérification, statut interne';
            case 'RPF':
                return 'Rejeté problème fournisseur, statut CHORUS avec un message d\'erreur';
            case 'RPT':
                return 'Rejeté problème technique, statut CHORUS avec un message d\'erreur';
            case '1':
                return 'Envoyé';
            case '2':
                return 'Non envoyé (erreur de génération)';
            default:
                return '';
        }
    }

    #[Groups('webservice')]
    public function getUuidContrat(): ?string
    {
        return $this->uuidExterneExec;
    }
}
