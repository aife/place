<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Autoformation\LangueRubrique;
use App\Entity\Autoformation\LangueModuleAutoformation;

/**
 * Langue.
 *
 * @ORM\Table(name="Langue")
 * @ORM\Entity(repositoryClass="App\Repository\LangueRepository")
 */
class Langue
{
    /**
     *
     * @ORM\Column(name="id_langue", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;
    /**
     * @ORM\Column(name="langue", type="string", length=10)
     */
    private ?string $langue = null;
    /**
     * @ORM\Column(name="active", type="string", length=1)
     */
    private ?string $active = null;
    /**
     * @ORM\Column(name="defaut", type="string", length=1)
     */
    private ?string $defaut = null;
    /**
     * @ORM\Column(name="theme_specifique", type="string", length=1)
     */
    private ?string $themeSpecifique = null;
    /**
     * @ORM\Column(name="obligatoire_pour_publication_consultation", type="string", length=1)
     */
    private ?string $obligatoirePourPublicationConsultation = null;
    /**
     * @var ConfigurationMessagesTraduction
     * @ORM\OneToMany(targetEntity="App\Entity\ConfigurationMessagesTraduction", mappedBy="langue")
     * @ORM\JoinColumn(name="id_langue", referencedColumnName="langue_id")
     */
    private $configurationMessagesTraduction;

    /**
     * @ORM\OneToMany(targetEntity=LangueRubrique::class, mappedBy="langue")
     */
    private $langueRubriques;

    /**
     * @ORM\OneToMany(targetEntity=LangueModuleAutoformation::class, mappedBy="langue", orphanRemoval=true)
     */
    private $langueModuleAutoformations;

    public function __construct()
    {
        $this->langueRubriques = new ArrayCollection();
        $this->langueModuleAutoformations = new ArrayCollection();
    }
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set langue.
     *
     * @param string $langue
     *
     * @return Langue
     */
    public function setLangue($langue)
    {
        $this->langue = $langue;

        return $this;
    }

    /**
     * Get langue.
     *
     * @return string
     */
    public function getLangue()
    {
        return $this->langue;
    }

    /**
     * Set active.
     *
     * @param string $active
     *
     * @return Langue
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return string
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set defaut.
     *
     * @param string $defaut
     *
     * @return Langue
     */
    public function setDefaut($defaut)
    {
        $this->defaut = $defaut;

        return $this;
    }

    /**
     * Get defaut.
     *
     * @return string
     */
    public function getDefaut()
    {
        return $this->defaut;
    }

    /**
     * Set themeSpecifique.
     *
     * @param string $themeSpecifique
     *
     * @return Langue
     */
    public function setThemeSpecifique($themeSpecifique)
    {
        $this->themeSpecifique = $themeSpecifique;

        return $this;
    }

    /**
     * Get themeSpecifique.
     *
     * @return string
     */
    public function getThemeSpecifique()
    {
        return $this->themeSpecifique;
    }

    /**
     * Set obligatoirePourPublicationConsultation.
     *
     * @param string $obligatoirePourPublicationConsultation
     *
     * @return Langue
     */
    public function setObligatoirePourPublicationConsultation($obligatoirePourPublicationConsultation)
    {
        $this->obligatoirePourPublicationConsultation = $obligatoirePourPublicationConsultation;

        return $this;
    }

    /**
     * Get obligatoirePourPublicationConsultation.
     *
     * @return string
     */
    public function getObligatoirePourPublicationConsultation()
    {
        return $this->obligatoirePourPublicationConsultation;
    }

    /**
     * @return ConfigurationMessagesTraduction
     */
    public function getConfigurationMessagesTraduction(): ConfigurationMessagesTraduction
    {
        return $this->configurationMessagesTraduction;
    }

    /**
     * @param ConfigurationMessagesTraduction $configurationMessagesTraduction
     */
    public function setConfigurationMessagesTraduction(
        ConfigurationMessagesTraduction $configurationMessagesTraduction
    ): void {
        $this->configurationMessagesTraduction = $configurationMessagesTraduction;
    }

    /**
     * @return Collection|LangueRubrique[]
     */
    public function getLangueRubriques(): Collection
    {
        return $this->langueRubriques;
    }

    public function addLangueRubrique(LangueRubrique $langueRubrique): self
    {
        if (!$this->langueRubriques->contains($langueRubrique)) {
            $this->langueRubriques[] = $langueRubrique;
            $langueRubrique->setLangue($this);
        }

        return $this;
    }

    public function removeLangueRubrique(LangueRubrique $langueRubrique): self
    {
        if ($this->langueRubriques->removeElement($langueRubrique)) {
            // set the owning side to null (unless already changed)
            if ($langueRubrique->getLangue() === $this) {
                $langueRubrique->setLangue(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|LangueModuleAutoformation[]
     */
    public function getLangueModuleAutoformations(): Collection
    {
        return $this->langueModuleAutoformations;
    }

    public function addLangueModuleAutoformation(LangueModuleAutoformation $langueModuleAutoformation): self
    {
        if (!$this->langueModuleAutoformations->contains($langueModuleAutoformation)) {
            $this->langueModuleAutoformations[] = $langueModuleAutoformation;
            $langueModuleAutoformation->setLangue($this);
        }

        return $this;
    }

    public function removeLangueModuleAutoformation(LangueModuleAutoformation $langueModuleAutoformation): self
    {
        if ($this->langueModuleAutoformations->removeElement($langueModuleAutoformation)) {
            // set the owning side to null (unless already changed)
            if ($langueModuleAutoformation->getLangue() === $this) {
                $langueModuleAutoformation->setLangue(null);
            }
        }

        return $this;
    }
}
