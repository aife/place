<?php

namespace App\Entity;

use ApiPlatform\Core\Action\PlaceholderAction;
use App\Controller\ApiPlatformCustom\ContratContractants;
use DateTime;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\Input\ContratTitulaireInput;
use App\Dto\Input\UpdateContratTitulaireInput;
use App\Dto\Output\ContratTitulaireOutput;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use App\Entity\Consultation\ClausesN1;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * ContratTitulaire
 *
 * @ORM\Table(name="t_contrat_titulaire")
 * @ORM\Entity(repositoryClass="App\Repository\ContratTitulaireRepository")
 */
#[ApiResource(
    collectionOperations: [
        'get',
        'get_place' => [
            "method"            => "GET",
            "path" => '/place/contrat-titulaires',
            "controller" => PlaceholderAction::class
        ],
        "create" => [
            "method"            => "POST",
            "input"             => ContratTitulaireInput::class,
            "security"          => "is_granted('CONTRAT_CREATE')",
            "openapi_context"   => [
                "description"   => '## Ce WebService nécessite l\'habilitation "creer_contrat".'
            ]
        ]
    ],
    itemOperations: [
        'get',
        'get_contractants' => [
            'method'        => 'GET',
            'path'          => '/contrat-titulaires/{idContratTitulaire}/contractants',
            'controller'    => ContratContractants::class
        ],
        "update" => [
            "method"            => "PUT",
            "input"             => UpdateContratTitulaireInput::class,
            "security"          => "is_granted('CONTRAT_UPDATE')",
            "openapi_context"   => [
                "description"   => '## Ce WebService nécessite l\'habilitation "gerer_contrat".'
            ]
        ],
        "patch" => [
            "method"            => "PATCH",
            "input"             => UpdateContratTitulaireInput::class,
            "security"          => "is_granted('CONTRAT_UPDATE')",
            "openapi_context"   => [
                "description"   => '## Ce WebService nécessite l\'habilitation "gerer_contrat".'
            ]
        ],
    ],
    denormalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: ContratTitulaireOutput::class,
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'idContratTitulaire' => 'exact',
        'numeroContrat' => 'exact',
        'statutContrat' => 'exact',
        'idTypeContrat' => 'exact',
        'dateModification' => 'partial',
        'objetContrat' => 'partial',
        'intitule' => 'partial',
    ]
)]
#[Post(validationContext: ['groups' => ['postValidation']])]
#[Put(validationContext: ['groups' => ['putValidation']])]
class ContratTitulaire
{
    public const CONTRAT_PRIX_FERME = 'Ferme';
    public const CONTRAT_PRIX_FERME_ACTUALISABLE = 'Ferme et actualisable';
    public const CONTRAT_PRIX_REVISABLE = 'Révisable';
    public const CATEGORIE_ENTREPRISE_PME = 'PME';

    public const CHOICE_HORS_PASSATION = [0, 1];
    public const CHOICE_MARCHE_DEFENSE = ['0', '1'];
    public const CHOICE_STATUT_CONTRAT = [
        0,
        'STATUT_DECISION_CONTRAT',
        'STATUT_DONNEES_CONTRAT_A_SAISIR',
        'STATUT_NUMEROTATION_AUTONOME',
        'STATUT_NOTIFICATION_CONTRAT',
        'STATUT_NOTIFICATION_CONTRAT_EFFECTUEE'
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id_contrat_titulaire", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idContratTitulaire;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ModificationContrat", mappedBy="idContratTitulaire")
     */
    private Collection $modificationContrats;

    /**
     * @var int
     *
     * @ORM\Column(name="id_type_contrat", type="integer", nullable=true)
     */
    private $idTypeContrat;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ContactContrat")
     * @ORM\JoinColumn(name="id_contact_contrat", referencedColumnName="id_contact_contrat")
     */
    private $idContactContrat;

    /**
     * @ORM\Column(name="numero_contrat", type="string", length=255, nullable=true)
     */
    private ?string $numeroContrat = null;

    /**
     * @ORM\Column(name="uuid", type="string", length=255, nullable=true)
     */
    private ?string $uuid = null;
    /**
     * @ORM\Column(name="lien_AC_SAD", type="integer", nullable=true)
     */
    private ?int $lienACSAD = null;

    /**
     * @var int
     *
     * @ORM\Column(name="id_contrat_multi", type="integer", nullable=true)
     */
    private $idContratMulti;

    /**
     *
     * @ORM\Column(name="organisme", type="string", length=45, nullable=true)*/
    #[Groups('webservice')]
    private ?string $organisme = null;

    /**
     * @ORM\Column(name="service_id", type="integer", nullable=true)
     */
    private ?int $serviceId = null;

    /**
     *
     * @ORM\Column(name="id_titulaire", type="integer", nullable=true)*/
    #[Groups('webservice')]
    private ?int $idTitulaire = null;

    /**
     * @ORM\Column(name="id_titulaire_etab", type="integer", nullable=true)
     */
    private ?int $idTitulaireEtab = null;

    /**
     * @ORM\Column(name="id_offre", type="integer", nullable=true)
     */
    private ?int $idOffre = null;

    /**
     * @ORM\Column(name="type_depot_reponse", type="string", length=1, nullable=false)
     */
    private ?string $typeDepotReponse = null;

    /**
     * @ORM\Column(name="objet_contrat", type="string", nullable=true)
     */
    private ?string $objetContrat = null;

    /**
     * @var int
     *
     * @ORM\Column(name="intitule", type="string", nullable=true)
     */
    private $intitule;

    /**
     * @var float
     *
     * @ORM\Column(name="montant_contrat", type="float", nullable=true)
     */
    private $montantContrat;

    /**
     * @ORM\Column(name="montant_subvention_publique", type="float", nullable=true)
     */
    private ?float $montantSubventionPublique = null;

    /**
     * @ORM\Column(name="id_tranche_budgetaire", type="integer", nullable=true)
     */
    private ?int $idTrancheBudgetaire = null;

    /**
     * @ORM\Column(name="montant_max_estime", type="integer", nullable=true)
     */
    private ?int $montantMaxEstime = null;

    /**
     * @ORM\Column(name="publication_montant", type="integer", nullable=true)
     */
    private ?int $publicationMontant = null;

    /**
     * @ORM\Column(name="id_motif_non_publication_montant", type="integer", nullable=true)
     */
    private ?int $idMotifNonPublicationMontant = null;

    /**
     * @ORM\Column(name="desc_motif_non_publication_montant", type="string", length=255, nullable=true)
     */
    private ?string $descMotifNonPublicationMontant = null;

    /**
     * @ORM\Column(name="publication_contrat", type="integer", nullable=true)
     */
    private ?int $publicationContrat = null;

    /**
     * @ORM\Column(name="num_EJ", type="string", length=45, nullable=true)
     */
    private ?string $numEJ = null;

    /**
     * @ORM\Column(name="statutEJ", type="string", nullable=true)
     */
    private ?string $statutEJ = null;

    /**
     * @ORM\Column(name="num_long_OEAP", type="string", length=45, nullable=true)
     */
    private ?string $numLongOEAP = null;

    /**
     * @ORM\Column(name="reference_libre", type="string", length=45, nullable=true)
     */
    private ?string $referenceLibre = null;

    /**
     * @ORM\Column(name="statut_contrat", type="integer", nullable=true)
     */
    private ?int $statutContrat = 0;

    /**
     * @ORM\Column(name="categorie", type="integer", nullable=true)
     */
    private ?int $categorie = null;

    /**
     * @ORM\Column(name="ccag_applicable", type="integer", nullable=true)
     */
    private ?int $ccagApplicable = null;

    /**
     * @ORM\Column(name="clause_sociale", type="string", length=10, nullable=true)
     */
    private ?string $clauseSociale = null;

    /**
     * @ORM\Column(name="clause_sociale_condition_execution", type="string", length=10, nullable=true)
     */
    private ?string $clauseSocialeConditionExecution = null;

    /**
     * @ORM\Column(name="clause_sociale_insertion", type="string", length=10, nullable=true)
     */
    private ?string $clauseSocialeInsertion = null;

    /**
     * @ORM\Column(name="clause_sociale_ateliers_proteges", type="string", length=10, nullable=true)
     */
    private ?string $clauseSocialeAteliersProteges = null;

    /**
     * @ORM\Column(name="clause_sociale_siae", type="string", length=10, nullable=true)
     */
    private ?string $clauseSocialeSiae = null;

    /**
     * @ORM\Column(name="clause_sociale_ess", type="string", length=10, nullable=true)
     */
    private ?string $clauseSocialeEss = null;

    /**
     * @ORM\Column(name="clause_environnementale", type="string", length=10, nullable=true)
     */
    private ?string $clauseEnvironnementale = null;

    /**
     * @ORM\Column(name="clause_env_specs_techniques", type="string", length=10, nullable=true)
     */
    private ?string $clauseEnvSpecsTechniques = null;

    /**
     * @ORM\Column(name="clause_env_cond_execution", type="string", length=10, nullable=true)
     */
    private ?string $clauseEnvCondExecution = null;

    /**
     * @ORM\Column(name="clause_env_criteres_select", type="string", length=10, nullable=true)
     */
    private ?string $clauseEnvCriteresSelect = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_prevue_notification", type="date", nullable=true)*/
    private $datePrevueNotification = '0000-00-00';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_prevue_fin_contrat", type="date", nullable=true)*/
    private $datePrevueFinContrat = '0000-00-00';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_prevue_max_fin_contrat", type="date", nullable=true)*/
    private $datePrevueMaxFinContrat = '0000-00-00';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_notification", type="date", nullable=true)*/
    private $dateNotification = '0000-00-00';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_fin_contrat", type="date", nullable=true)*/
    private $dateFinContrat = '0000-00-00';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_max_fin_contrat", type="date", nullable=true)*/
    private $dateMaxFinContrat = '0000-00-00';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_attribution", type="date", nullable=true)*/
    private $dateAttribution = '0000-00-00';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime", nullable=true)*/
    private $dateCreation = '0000-00-00 00:00:00';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_modification", type="datetime", nullable=true)*/
    private $dateModification = '0000-00-00 00:00:00';

    /**
     * @var bool
     *
     * @ORM\Column(name="envoi_interface", type="boolean", nullable=false)
     */
    private $envoiInterface = '0';

    /**
     * @ORM\Column(name="contrat_class_key", type="integer", nullable=false)
     */
    private int $contratClassKey = 1;

    /**
     * @ORM\Column(name="pme_pmi", type="integer", nullable=false)
     */
    private int $PmePmi = 0;

    /**
     * @ORM\Column(name="reference_consultation", type="string", length=255, nullable=true)
     */
    private ?string $referenceConsultation = null;

    /**
     *
     * @ORM\Column(name="hors_passation", type="integer", nullable=false)*/
    #[Groups('webservice')]
    private int $horsPassation = 0;

    /**
     * @ORM\Column(name="id_agent", type="integer", nullable=true)
     */
    private ?int $idAgent = null;

    /**
     * @ORM\Column(name="nom_agent", type="string", length=255, nullable=true)
     */
    private ?string $nomAgent = null;

    /**
     * @ORM\Column(name="prenom_agent", type="string", length=255, nullable=true)
     */
    private ?string $prenomAgent = null;

    /**
     * @ORM\Column(name="lieu_execution", type="string", nullable=true)
     */
    private ?string $lieuExecution = null;

    /**
     * @ORM\Column(name="code_cpv_1", type="string", length=8, nullable=true)
     */
    private ?string $codeCpv1 = null;

    /**
     * @ORM\Column(name="code_cpv_2", type="string", length=255, nullable=true)
     */
    private ?string $codeCpv2 = null;

    /**
     * @ORM\Column(name="procedure_passation_pivot", type="string", length=255, nullable=true)
     */
    private ?string $procedurePassationPivot = null;

    /**
     * @ORM\Column(name="duree_initiale_contrat", type="integer", nullable=true)
     */
    private ?int $dureeInitialeContrat = null;

    /**
     * @ORM\Column(name="forme_prix", type="string", length=255, nullable=false)
     */
    private ?string $formePrix = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_publication_initiale_de", type="datetime", nullable=true)*/
    private $datePublicationInitialeDe = '0000-00-00 00:00:00';

    /**
     * @ORM\Column(name="num_id_unique_marche_public", type="string", length=255, nullable=false)
     */
    private string $numIdUniqueMarchePublic = '';

    /**
     * @ORM\Column(name="nom_lieu_principal_execution", type="string", length=255, nullable=false)
     */
    private string $nomLieuPrincipalExecution = '';

    /**
     * @ORM\Column(name="code_lieu_principal_execution", type="string", length=20, nullable=false)
     */
    private string $codeLieuPrincipalExecution = '';

    /**
     * @ORM\Column(name="type_code_lieu_principal_execution", type="string", length=20, nullable=false)
     */
    private string $typeCodeLieuPrincipalExecution = '';

    /**
     * @ORM\Column(name="libelle_type_contrat_pivot", type="string", length=255, nullable=false)
     */
    private string $libelleTypeContratPivot = '';

    /**
     * @ORM\Column(name="siret_pa_accord_cadre", type="string", length=255, nullable=true)
     */
    private ?string $siretPaAccordCadre = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="ac_marche_subsequent", type="boolean", nullable=false)
     */
    private $acMarcheSubsequent = false;

    /**
     * @ORM\Column(name="libelle_type_procedure_mpe", type="string", length=255, nullable=true)
     */
    private ?string $libelleTypeProcedureMpe = null;

    /**
     * @ORM\Column(name="nb_total_propositions_lot", type="integer", nullable=true)
     */
    private ?int $nbTotalPropositionsLot = null;

    /**
     * @ORM\Column(name="nb_total_propositions_demat_lot", type="integer", nullable=true)
     */
    private ?int $nbTotalPropositionsDematLot = null;

    /**
     * @var string
     *
     * @ORM\Column(name="marche_defense", type="string", length=1, nullable=true)
     */
    private $marcheDefense = 0;

    /**
     * @ORM\Column(name="siret", type="string", length=255, nullable=true)
     */
    private ?string $siret = null;

    /**
     * @ORM\Column(name="nom_entite_acheteur", type="string", length=1000, nullable=true)
     */
    private ?string $nomEntiteAcheteur = null;

    /**
     * @ORM\Column(name="statut_publication_sn", type="integer", nullable=false)
     */
    private int $statutPublicationSn = 0;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeProcedurePivot",
     *                inversedBy="contratTitulaires",
     *                fetch="EAGER")
     * @ORM\JoinColumn(name="id_type_procedure_pivot", referencedColumnName="id")
     */
    private $idTypeProcedurePivot;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeProcedureConcessionPivot",
     *                inversedBy="contratTitulaires",
     *                fetch="EAGER")
     * @ORM\JoinColumn(name="id_type_procedure_concession_pivot", referencedColumnName="id")
     */
    private $idTypeProcedureConcessionPivot;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeContratPivot",
     *                inversedBy="contratTitulaires",
     *                fetch="EAGER")
     * @ORM\JoinColumn(name="id_type_contrat_pivot", referencedColumnName="id")
     */
    private $idTypeContratPivot;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeContratConcessionPivot",
     *                inversedBy="contratTitulaires",
     *                fetch="EAGER")
     * @ORM\JoinColumn(name="id_type_contrat_concession_pivot", referencedColumnName="id")
     */
    private $idTypeContratConcessionPivot;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_debut_execution", type="datetime", nullable=true)*/
    private $dateDebutExecution = '0000-00-00 00:00:00';

    /**
     * @var bool
     *
     * @ORM\Column(name="marche_insertion", type="boolean", nullable=true)
     */
    private $marcheInsertion;

    /**
     * @ORM\Column(name="clause_specification_technique", type="string", length=255, nullable=true)
     */
    private ?string $clauseSpecificationTechnique = null;

    private $consultation;

    private $typeContrat;

    private $contratTitulaireFavori;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_publication_sn", type="datetime", nullable=true)*/
    private $datePublicationSN = '0000-00-00 00:00:00';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_modification_sn", type="datetime", nullable=true)*/
    private $dateModificationSN = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="erreur_sn", type="text", length=65535, nullable=true)
     */
    private $erreurSn;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\EchangeDocumentaire\EchangeDoc", mappedBy="contratTitulaire")
     */
    private $echangeDoc;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $marcheInnovant = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Consultation\ClausesN1", mappedBy="contrat", orphanRemoval=true)
     */
    private Collection $clausesN1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="old_service_id", type="integer", nullable=true)
     */
    private ?int $oldServiceId = null;

    /**
     * @return DateTime
     */
    public function getDateDebutExecution()
    {
        return $this->dateDebutExecution;
    }

    public function setDateDebutExecution(?DateTimeInterface $dateDebutExecution)
    {
        $this->dateDebutExecution = $dateDebutExecution;
    }

    /**
     * ContratTitulaire constructor.
     */
    public function __construct()
    {
        $this->modificationContrats = new ArrayCollection();
        $this->echangeDoc = new ArrayCollection();
        $this->clausesN1 = new ArrayCollection();
    }

    public static function getFormes()
    {
        return [
            1 => self::CONTRAT_PRIX_FERME,
            2 => self::CONTRAT_PRIX_FERME_ACTUALISABLE,
            3 => self::CONTRAT_PRIX_REVISABLE,
        ];
    }

    /**
     * @return float
     */
    public function getMontantSubventionPublique()
    {
        return $this->montantSubventionPublique;
    }

    public function setMontantSubventionPublique(float $montantSubventionPublique)
    {
        $this->montantSubventionPublique = $montantSubventionPublique;
    }

    /**
     * @return int
     */
    public function getIdContratTitulaire()
    {
        return $this->idContratTitulaire;
    }

    /**
     * @return int
     */
    public function getIdTypeContrat()
    {
        return $this->idTypeContrat;
    }

    public function setIdTypeContrat(int $idTypeContrat)
    {
        $this->idTypeContrat = $idTypeContrat;
    }

    /**
     * @return int
     */
    public function getIdContactContrat()
    {
        return $this->idContactContrat;
    }

    public function getIdContact()
    {
        if (!empty($this->idContactContrat)) {
            return $this->idContactContrat->getId();
        }

        return null;
    }

    public function setIdContactContrat(null|int|ContactContrat $idContactContrat)
    {
        $this->idContactContrat = $idContactContrat;
    }

    /**
     * @return string
     */
    public function getNumeroContrat()
    {
        return $this->numeroContrat;
    }

    public function setNumeroContrat(?string $numeroContrat)
    {
        $this->numeroContrat = $numeroContrat;
    }

    /**
     * @return int
     */
    public function getLienACSAD()
    {
        return $this->lienACSAD;
    }

    public function setLienACSAD(int $lienACSAD)
    {
        $this->lienACSAD = $lienACSAD;
    }

    /**
     * @return int
     */
    public function getIdContratMulti()
    {
        return $this->idContratMulti;
    }

    public function setIdContratMulti(int $idContratMulti)
    {
        $this->idContratMulti = $idContratMulti;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return int
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    public function setServiceId(?int $serviceId)
    {
        $this->serviceId = $serviceId;
    }

    /**
     * @return int
     */
    public function getIdTitulaire()
    {
        return $this->idTitulaire;
    }

    public function setIdTitulaire(?int $idTitulaire)
    {
        $this->idTitulaire = $idTitulaire;
    }

    /**
     * @return int
     */
    public function getIdTitulaireEtab()
    {
        return $this->idTitulaireEtab;
    }

    public function setIdTitulaireEtab(?int $idTitulaireEtab)
    {
        $this->idTitulaireEtab = $idTitulaireEtab;
    }

    /**
     * @return int
     */
    public function getIdOffre()
    {
        return $this->idOffre;
    }

    public function setIdOffre(int $idOffre)
    {
        $this->idOffre = $idOffre;
    }

    /**
     * @return string
     */
    public function getTypeDepotReponse()
    {
        return $this->typeDepotReponse;
    }

    public function setTypeDepotReponse(string $typeDepotReponse)
    {
        $this->typeDepotReponse = $typeDepotReponse;
    }

    /**
     * @return string
     */
    public function getObjetContrat()
    {
        return $this->objetContrat;
    }

    public function setObjetContrat(string $objetContrat)
    {
        $this->objetContrat = $objetContrat;
    }

    /**
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * @param $intitule
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;
    }

    /**
     * @return float
     */
    public function getMontantContrat()
    {
        return $this->montantContrat;
    }

    public function setMontantContrat(float $montantContrat)
    {
        $this->montantContrat = $montantContrat;
    }

    /**
     * @return int
     */
    public function getIdTrancheBudgetaire()
    {
        return $this->idTrancheBudgetaire;
    }

    public function setIdTrancheBudgetaire(?int $idTrancheBudgetaire)
    {
        $this->idTrancheBudgetaire = $idTrancheBudgetaire;
    }

    /**
     * @return int
     */
    public function getMontantMaxEstime()
    {
        return $this->montantMaxEstime;
    }

    public function setMontantMaxEstime(?int $montantMaxEstime)
    {
        $this->montantMaxEstime = $montantMaxEstime;
    }

    /**
     * @return int
     */
    public function getPublicationMontant()
    {
        return $this->publicationMontant;
    }

    public function setPublicationMontant(?int $publicationMontant)
    {
        $this->publicationMontant = $publicationMontant;
    }

    /**
     * @return int
     */
    public function getIdMotifNonPublicationMontant()
    {
        return $this->idMotifNonPublicationMontant;
    }

    public function setIdMotifNonPublicationMontant(int $idMotifNonPublicationMontant)
    {
        $this->idMotifNonPublicationMontant = $idMotifNonPublicationMontant;
    }

    /**
     * @return string
     */
    public function getDescMotifNonPublicationMontant()
    {
        return $this->descMotifNonPublicationMontant;
    }

    public function setDescMotifNonPublicationMontant(string $descMotifNonPublicationMontant)
    {
        $this->descMotifNonPublicationMontant = $descMotifNonPublicationMontant;
    }

    /**
     * @return int
     */
    public function getPublicationContrat()
    {
        return $this->publicationContrat;
    }

    public function setPublicationContrat(int $publicationContrat)
    {
        $this->publicationContrat = $publicationContrat;
    }

    /**
     * @return string
     */
    public function getNumEJ()
    {
        return $this->numEJ;
    }

    public function setNumEJ(string $numEJ)
    {
        $this->numEJ = $numEJ;
    }

    /**
     * @return string
     */
    public function getStatutEJ()
    {
        return $this->statutEJ;
    }

    public function setStatutEJ(string $statutEJ)
    {
        $this->statutEJ = $statutEJ;
    }

    /**
     * @return string
     */
    public function getNumLongOEAP()
    {
        return $this->numLongOEAP;
    }

    public function setNumLongOEAP(string $numLongOEAP)
    {
        $this->numLongOEAP = $numLongOEAP;
    }

    /**
     * @return string
     */
    public function getReferenceLibre()
    {
        return $this->referenceLibre;
    }

    public function setReferenceLibre(string $referenceLibre)
    {
        $this->referenceLibre = $referenceLibre;
    }

    /**
     * @return int
     */
    public function getStatutContrat()
    {
        return $this->statutContrat;
    }

    public function setStatutContrat(int $statutContrat)
    {
        $this->statutContrat = $statutContrat;
    }

    /**
     * @return int
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    public function setCategorie(int $categorie)
    {
        $this->categorie = $categorie;
    }

    /**
     * @return int
     */
    public function getCcagApplicable()
    {
        return $this->ccagApplicable;
    }

    public function setCcagApplicable(int $ccagApplicable)
    {
        $this->ccagApplicable = $ccagApplicable;
    }

    /**
     * @return string
     */
    public function getClauseSociale()
    {
        return $this->clauseSociale;
    }

    public function setClauseSociale(string $clauseSociale)
    {
        $this->clauseSociale = $clauseSociale;
    }

    /**
     * @return string
     */
    public function getClauseSocialeConditionExecution()
    {
        return $this->clauseSocialeConditionExecution;
    }

    public function setClauseSocialeConditionExecution(string $clauseSocialeConditionExecution)
    {
        $this->clauseSocialeConditionExecution = $clauseSocialeConditionExecution;
    }

    /**
     * @return string
     */
    public function getClauseSocialeInsertion()
    {
        return $this->clauseSocialeInsertion;
    }

    public function setClauseSocialeInsertion(string $clauseSocialeInsertion)
    {
        $this->clauseSocialeInsertion = $clauseSocialeInsertion;
    }

    /**
     * @return string
     */
    public function getClauseSocialeAteliersProteges()
    {
        return $this->clauseSocialeAteliersProteges;
    }

    public function setClauseSocialeAteliersProteges(string $clauseSocialeAteliersProteges)
    {
        $this->clauseSocialeAteliersProteges = $clauseSocialeAteliersProteges;
    }

    /**
     * @return string
     */
    public function getClauseSocialeSiae()
    {
        return $this->clauseSocialeSiae;
    }

    public function setClauseSocialeSiae(string $clauseSocialeSiae)
    {
        $this->clauseSocialeSiae = $clauseSocialeSiae;
    }

    /**
     * @return string
     */
    public function getClauseSocialeEss()
    {
        return $this->clauseSocialeEss;
    }

    public function setClauseSocialeEss(string $clauseSocialeEss)
    {
        $this->clauseSocialeEss = $clauseSocialeEss;
    }

    /**
     * @return string
     */
    public function getClauseEnvironnementale()
    {
        return $this->clauseEnvironnementale;
    }

    public function setClauseEnvironnementale(string $clauseEnvironnementale)
    {
        $this->clauseEnvironnementale = $clauseEnvironnementale;
    }

    /**
     * @return string
     */
    public function getClauseEnvSpecsTechniques()
    {
        return $this->clauseEnvSpecsTechniques;
    }

    public function setClauseEnvSpecsTechniques(string $clauseEnvSpecsTechniques)
    {
        $this->clauseEnvSpecsTechniques = $clauseEnvSpecsTechniques;
    }

    /**
     * @return string
     */
    public function getClauseEnvCondExecution()
    {
        return $this->clauseEnvCondExecution;
    }

    public function setClauseEnvCondExecution(string $clauseEnvCondExecution)
    {
        $this->clauseEnvCondExecution = $clauseEnvCondExecution;
    }

    /**
     * @return string
     */
    public function getClauseEnvCriteresSelect()
    {
        return $this->clauseEnvCriteresSelect;
    }

    public function setClauseEnvCriteresSelect(string $clauseEnvCriteresSelect)
    {
        $this->clauseEnvCriteresSelect = $clauseEnvCriteresSelect;
    }

    /**
     * @return DateTime
     */
    public function getDatePrevueNotification()
    {
        return $this->datePrevueNotification;
    }

    public function setDatePrevueNotification(?\DateTimeInterface $datePrevueNotification)
    {
        $this->datePrevueNotification = $datePrevueNotification;
    }

    /**
     * @return DateTime
     */
    public function getDatePrevueFinContrat()
    {
        return $this->datePrevueFinContrat;
    }

    public function setDatePrevueFinContrat(?\DateTime $datePrevueFinContrat)
    {
        $this->datePrevueFinContrat = $datePrevueFinContrat;
    }

    /**
     * @return DateTime
     */
    public function getDatePrevueMaxFinContrat()
    {
        return $this->datePrevueMaxFinContrat;
    }

    public function setDatePrevueMaxFinContrat(?\DateTime $datePrevueMaxFinContrat)
    {
        $this->datePrevueMaxFinContrat = $datePrevueMaxFinContrat;
    }

    /**
     * @return DateTime
     */
    public function getDateNotification()
    {
        return $this->dateNotification;
    }

    public function setDateNotification(?\DateTime $dateNotification): void
    {
        $this->dateNotification = $dateNotification;
    }

    /**
     * @return DateTime
     */
    public function getDateFinContrat()
    {
        return $this->dateFinContrat;
    }

    public function setDateFinContrat(DateTime $dateFinContrat)
    {
        $this->dateFinContrat = $dateFinContrat;
    }

    /**
     * @return DateTime
     */
    public function getDateMaxFinContrat()
    {
        return $this->dateMaxFinContrat;
    }

    public function setDateMaxFinContrat(DateTime $dateMaxFinContrat)
    {
        $this->dateMaxFinContrat = $dateMaxFinContrat;
    }

    /**
     * @return DateTime
     */
    public function getDateAttribution()
    {
        return $this->dateAttribution;
    }

    public function setDateAttribution(DateTime $dateAttribution)
    {
        $this->dateAttribution = $dateAttribution;
    }

    /**
     * @return DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }

    /**
     * @return DateTime
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }

    public function setDateModification($dateModification)
    {
        $this->dateModification = $dateModification;
    }

    /**
     * @return bool
     */
    public function isEnvoiInterface()
    {
        return $this->envoiInterface;
    }

    public function setEnvoiInterface(bool $envoiInterface)
    {
        $this->envoiInterface = $envoiInterface;
    }

    /**
     * @return int
     */
    public function getContratClassKey()
    {
        return $this->contratClassKey;
    }

    public function setContratClassKey(int $contratClassKey)
    {
        $this->contratClassKey = $contratClassKey;
    }

    /**
     * @return int
     */
    public function getPmePmi()
    {
        return $this->PmePmi;
    }

    public function setPmePmi(int $PmePmi)
    {
        $this->PmePmi = $PmePmi;
    }

    /**
     * @return string
     */
    public function getReferenceConsultation()
    {
        return $this->referenceConsultation;
    }

    public function setReferenceConsultation(string $referenceConsultation)
    {
        $this->referenceConsultation = $referenceConsultation;
    }

    /**
     * @return int
     */
    public function getHorsPassation()
    {
        return $this->horsPassation;
    }

    public function setHorsPassation(int $horsPassation)
    {
        $this->horsPassation = $horsPassation;
    }

    /**
     * @return int
     */
    public function getIdAgent()
    {
        return $this->idAgent;
    }

    public function setIdAgent(int $idAgent)
    {
        $this->idAgent = $idAgent;
    }

    /**
     * @return string
     */
    public function getNomAgent()
    {
        return $this->nomAgent;
    }

    public function setNomAgent(string $nomAgent)
    {
        $this->nomAgent = $nomAgent;
    }

    /**
     * @return string
     */
    public function getPrenomAgent()
    {
        return $this->prenomAgent;
    }

    public function setPrenomAgent(string $prenomAgent)
    {
        $this->prenomAgent = $prenomAgent;
    }

    /**
     * @return string
     */
    public function getLieuExecution()
    {
        return $this->lieuExecution;
    }

    public function setLieuExecution(?string $lieuExecution)
    {
        $this->lieuExecution = $lieuExecution;
    }

    /**
     * @return string
     */
    public function getCodeCpv1()
    {
        return $this->codeCpv1;
    }

    public function setCodeCpv1(?string $codeCpv1)
    {
        $this->codeCpv1 = $codeCpv1;
    }

    /**
     * @return string
     */
    public function getCodeCpv2()
    {
        return $this->codeCpv2;
    }

    public function setCodeCpv2(?string $codeCpv2)
    {
        $this->codeCpv2 = $codeCpv2;
    }

    /**
     * @return string
     */
    public function getProcedurePassationPivot()
    {
        return $this->procedurePassationPivot;
    }

    public function setProcedurePassationPivot(string $procedurePassationPivot)
    {
        $this->procedurePassationPivot = $procedurePassationPivot;
    }

    /**
     * @return int
     */
    public function getDureeInitialeContrat()
    {
        return $this->dureeInitialeContrat;
    }

    public function setDureeInitialeContrat(?int $dureeInitialeContrat): void
    {
        $this->dureeInitialeContrat = $dureeInitialeContrat;
    }

    /**
     * @return string
     */
    public function getFormePrix()
    {
        return $this->formePrix;
    }

    public function setFormePrix(string $formePrix)
    {
        $this->formePrix = $formePrix;
    }

    /**
     * @return DateTimeInterface
     */
    public function getDatePublicationInitialeDe()
    {
        return $this->datePublicationInitialeDe;
    }

    public function setDatePublicationInitialeDe(?\DateTimeInterface $datePublicationInitialeDe)
    {
        $this->datePublicationInitialeDe = $datePublicationInitialeDe;
    }

    /**
     * @return string
     */
    public function getNumIdUniqueMarchePublic()
    {
        return $this->numIdUniqueMarchePublic;
    }

    public function setNumIdUniqueMarchePublic(string $numIdUniqueMarchePublic)
    {
        $this->numIdUniqueMarchePublic = $numIdUniqueMarchePublic;
    }

    /**
     * @return string
     */
    public function getNomLieuPrincipalExecution()
    {
        return $this->nomLieuPrincipalExecution;
    }

    public function setNomLieuPrincipalExecution(string $nomLieuPrincipalExecution)
    {
        $this->nomLieuPrincipalExecution = $nomLieuPrincipalExecution;
    }

    /**
     * @return string
     */
    public function getCodeLieuPrincipalExecution()
    {
        return $this->codeLieuPrincipalExecution;
    }

    public function setCodeLieuPrincipalExecution(string $codeLieuPrincipalExecution)
    {
        $this->codeLieuPrincipalExecution = $codeLieuPrincipalExecution;
    }

    /**
     * @return string
     */
    public function getTypeCodeLieuPrincipalExecution()
    {
        return $this->typeCodeLieuPrincipalExecution;
    }

    public function setTypeCodeLieuPrincipalExecution(string $typeCodeLieuPrincipalExecution)
    {
        $this->typeCodeLieuPrincipalExecution = $typeCodeLieuPrincipalExecution;
    }

    /**
     * @return string
     */
    public function getLibelleTypeContratPivot()
    {
        return $this->libelleTypeContratPivot;
    }

    public function setLibelleTypeContratPivot(string $libelleTypeContratPivot)
    {
        $this->libelleTypeContratPivot = $libelleTypeContratPivot;
    }

    /**
     * @return string
     */
    public function getSiretPaAccordCadre()
    {
        return $this->siretPaAccordCadre;
    }

    public function setSiretPaAccordCadre(string $siretPaAccordCadre)
    {
        $this->siretPaAccordCadre = $siretPaAccordCadre;
    }

    /**
     * @return bool
     */
    public function isAcMarcheSubsequent()
    {
        return $this->acMarcheSubsequent;
    }

    public function setAcMarcheSubsequent(bool $acMarcheSubsequent)
    {
        $this->acMarcheSubsequent = $acMarcheSubsequent;
    }

    /**
     * @return string
     */
    public function getLibelleTypeProcedureMpe()
    {
        return $this->libelleTypeProcedureMpe;
    }

    public function setLibelleTypeProcedureMpe(string $libelleTypeProcedureMpe)
    {
        $this->libelleTypeProcedureMpe = $libelleTypeProcedureMpe;
    }

    /**
     * @return int
     */
    public function getNbTotalPropositionsLot()
    {
        return $this->nbTotalPropositionsLot;
    }

    public function setNbTotalPropositionsLot(int $nbTotalPropositionsLot)
    {
        $this->nbTotalPropositionsLot = $nbTotalPropositionsLot;
    }

    /**
     * @return int
     */
    public function getNbTotalPropositionsDematLot()
    {
        return $this->nbTotalPropositionsDematLot;
    }

    public function setNbTotalPropositionsDematLot(int $nbTotalPropositionsDematLot)
    {
        $this->nbTotalPropositionsDematLot = $nbTotalPropositionsDematLot;
    }

    /**
     * @return string
     */
    public function getMarcheDefense()
    {
        return $this->marcheDefense;
    }

    public function setMarcheDefense(string $marcheDefense)
    {
        $this->marcheDefense = $marcheDefense;
    }

    /**
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    public function setSiret(string $siret)
    {
        $this->siret = $siret;
    }

    /**
     * @return string
     */
    public function getNomEntiteAcheteur()
    {
        return $this->nomEntiteAcheteur;
    }

    public function setNomEntiteAcheteur(string $nomEntiteAcheteur)
    {
        $this->nomEntiteAcheteur = $nomEntiteAcheteur;
    }

    public function getStatutPublicationSn(): int
    {
        return $this->statutPublicationSn;
    }

    public function setStatutPublicationSn(int $statutPublicationSn)
    {
        $this->statutPublicationSn = $statutPublicationSn;
    }

    public function getSiretFormater()
    {
        return str_replace(' - ', '', $this->siret ?? '');
    }

    /**
     * @param ModificationContrat $modificationContrat
     *
     * @return $this
     */
    public function addModificationContrat($modificationContrat)
    {
        $this->modificationContrats[] = $modificationContrat;

        return $this;
    }

    /**
     * @param ModificationContrat $modificationContrat
     */
    public function removeModificationContrat($modificationContrat)
    {
        $this->modificationContrats->removeElement($modificationContrat);
    }

    /**
     * @return ArrayCollection
     */
    public function getModificationContrats()
    {
        return $this->modificationContrats;
    }

    /**
     * @return int
     */
    public function getIdTypeProcedurePivot()
    {
        return $this->idTypeProcedurePivot;
    }

    public function setIdTypeProcedurePivot(int $idTypeProcedurePivot)
    {
        $this->idTypeProcedurePivot = $idTypeProcedurePivot;
    }

    /**
     * @return int
     */
    public function getIdTypeProcedureConcessionPivot()
    {
        return $this->idTypeProcedureConcessionPivot;
    }

    public function setIdTypeProcedureConcessionPivot(int $idTypeProcedureConcessionPivot)
    {
        $this->idTypeProcedureConcessionPivot = $idTypeProcedureConcessionPivot;
    }

    /**
     * @return int
     */
    public function getIdTypeContratPivot()
    {
        return $this->idTypeContratPivot;
    }

    public function setIdTypeContratPivot(int $idTypeContratPivot)
    {
        $this->idTypeContratPivot = $idTypeContratPivot;
    }

    /**
     * @return int
     */
    public function getIdTypeContratConcessionPivot()
    {
        return $this->idTypeContratConcessionPivot;
    }

    public function setIdTypeContratConcessionPivot(int $idTypeContratConcessionPivot)
    {
        $this->idTypeContratConcessionPivot = $idTypeContratConcessionPivot;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid)
    {
        $this->uuid = $uuid;
    }

    /* WS Get Set */

    /**
     * @return int
     */
    public function getId()
    {
        return $this->idContratTitulaire;
    }

    public function setId(string $idContratTitulaire)
    {
        $this->idContratTitulaire = $idContratTitulaire;
    }

    /**
     * @return string
     */
    public function getNumero()
    {
        return $this->numeroContrat;
    }

    public function setNumero(?string $numeroContrat)
    {
        $this->numeroContrat = $numeroContrat;
    }

    /**
     * @return string
     */
    public function getNumeroLong()
    {
        return $this->numLongOEAP;
    }

    public function setNumeroLong(?string $numLongOEAP)
    {
        $this->numLongOEAP = $numLongOEAP;
    }

    /**
     * @return string
     */
    public function getObjet()
    {
        return $this->objetContrat;
    }

    public function setObjet(string $objetContrat)
    {
        $this->objetContrat = $objetContrat;
    }

    /**
     * @return int
     */
    public function getIdService()
    {
        return $this->serviceId;
    }

    public function setIdService(int $serviceId)
    {
        $this->serviceId = $serviceId;
    }

    /**
     * @return int
     */
    public function getIdCreateur()
    {
        return $this->idAgent;
    }

    public function setIdCreateur(int $idAgent)
    {
        $this->idAgent = $idAgent;
    }

    /**
     * @return int
     */
    public function getStatut()
    {
        return $this->statutContrat;
    }

    public function setStatut(int $statutContrat)
    {
        $this->statutContrat = $statutContrat;
    }

    /**
     * @return mixed
     */
    public function getContactContrat()
    {
        return $this->contactContrat;
    }

    /**
     * @param mixed $contactContrat
     */
    public function setContactContrat($contactContrat)
    {
        $this->contactContrat = $contactContrat;
    }

    /**
     * @return mixed
     */
    public function getContact(): ?ContactContrat
    {
        return $this->idContactContrat;
    }

    /**
     * @param mixed $idContactContrat
     */
    public function setContact($idContactContrat)
    {
        $this->idContactContrat = $idContactContrat;
    }

    /**
     * @return mixed
     */
    public function getIdChapeau()
    {
        return $this->idContratMulti;
    }

    /**
     * @param mixed $idContratMulti
     */
    public function setIdChapeau($idContratMulti)
    {
        $this->idContratMulti = $idContratMulti;
    }

    /**
     * @return mixed
     */
    public function getChapeau()
    {
        return $this->idContratMulti;
    }

    /**
     * @param mixed $idContratMulti
     */
    public function setChapeau($idContratMulti)
    {
        $this->idContratMulti = $idContratMulti;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->idTypeContrat;
    }

    /**
     * @param mixed $idContratMulti
     */
    public function setType($idTypeContrat)
    {
        $this->idTypeContrat = $idTypeContrat;
    }

    /**
     * @return mixed
     */
    public function getMontant()
    {
        return $this->montantContrat;
    }

    /**
     * @param mixed $montantContrat
     */
    public function setMontant($montantContrat)
    {
        $this->montantContrat = $montantContrat;
    }

    /**
     * @return DateTime
     */
    public function getDateFin()
    {
        return $this->dateFinContrat;
    }

    public function setDateFin(?\DateTime $dateFinContrat): void
    {
        $this->dateFinContrat = $dateFinContrat;
    }

    /**
     * @return DateTime
     */
    public function getDateMaxFin()
    {
        return $this->dateMaxFinContrat;
    }

    public function setDateMaxFin(?\DateTime $dateMaxFinContrat): void
    {
        $this->dateMaxFinContrat = $dateMaxFinContrat;
    }

    /**
     * @return mixed
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    /**
     * @param mixed $consultation
     */
    public function setConsultation($consultation)
    {
        $this->consultation = $consultation;
    }

    /**
     * @return mixed
     */
    public function getTypeContrat()
    {
        return $this->typeContrat;
    }

    /**
     * @param mixed $typeContrat
     */
    public function setTypeContrat($typeContrat)
    {
        $this->typeContrat = $typeContrat;
    }

    /**
     * @return mixed
     */
    public function getContratTitulaireFavori()
    {
        return $this->contratTitulaireFavori;
    }

    /**
     * @param mixed $contratTitulaireFavori
     */
    public function setContratTitulaireFavori($contratTitulaireFavori)
    {
        $this->contratTitulaireFavori = $contratTitulaireFavori;
    }

    /**
     * @return bool
     */
    public function getMarcheInsertion()
    {
        return $this->marcheInsertion;
    }

    /**
     * @param bool $marcheInsertion
     */
    public function setMarcheInsertion($marcheInsertion)
    {
        $this->marcheInsertion = $marcheInsertion;
    }

    /**
     * @return string
     */
    public function getClauseSpecificationTechnique()
    {
        return $this->clauseSpecificationTechnique;
    }

    /**
     * @param string $clauseSpecificationTechnique
     */
    public function setClauseSpecificationTechnique($clauseSpecificationTechnique)
    {
        $this->clauseSpecificationTechnique = $clauseSpecificationTechnique;
    }

    public function getEchangeDoc(): ContratTitulaire
    {
        return $this->echangeDoc;
    }

    /**
     * @param mixed $echangeDoc
     */
    public function setEchangeDoc($echangeDoc)
    {
        $this->echangeDoc = $echangeDoc;
    }

    /**
     * @return DateTime
     */
    public function getDatePublicationSN(): ?DateTime
    {
        return $this->datePublicationSN;
    }

    public function setDatePublicationSN(?DateTimeInterface $datePublicationSN): self
    {
        $this->datePublicationSN = $datePublicationSN;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateModificationSN(): ?DateTime
    {
        return $this->dateModificationSN;
    }

    public function setDateModificationSN(?DateTimeInterface $dateModificationSN): self
    {
        $this->dateModificationSN = $dateModificationSN;
        return $this;
    }

    /**
     * @return string
     */
    public function getErreurSn(): ?string
    {
        return $this->erreurSn;
    }

    /**
     * @param string $erreurSn
     */
    public function setErreurSn(string $erreurSn): self
    {
        $this->erreurSn = $erreurSn;
        return $this;
    }

    /**
     * @return bool
     */
    public function isMarcheInnovant(): bool
    {
        return $this->marcheInnovant;
    }

    /**
     * @param bool $marcheInnovant
     */
    public function setMarcheInnovant(bool $marcheInnovant): self
    {
        $this->marcheInnovant = $marcheInnovant;
        return $this;
    }

    /**
     * @return Collection<int, ClausesN1>
     */
    public function getClausesN1(): Collection
    {
        return $this->clausesN1;
    }

    public function addClausesN1(ClausesN1 $clausesN1): self
    {
        if (!$this->clausesN1->contains($clausesN1)) {
            $this->clausesN1[] = $clausesN1;
            $clausesN1->setContrat($this);
        }

        return $this;
    }

    public function removeClausesN1(ClausesN1 $clausesN1): self
    {
        if ($this->clausesN1->removeElement($clausesN1)) {
            // set the owning side to null (unless already changed)
            if ($clausesN1->getContrat() === $this) {
                $clausesN1->setContrat(null);
            }
        }

        return $this;
    }


    public function hasThisClause($clauseName)
    {
        $getClause = "get" . ucfirst($clauseName);

        return (bool)$this->$getClause();
    }

    public function getOldServiceId(): ?int
    {
        return $this->oldServiceId;
    }
}
