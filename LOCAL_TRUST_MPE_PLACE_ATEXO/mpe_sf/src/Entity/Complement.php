<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="Complement")
 * @ORM\Entity(repositoryClass="App\Repository\ComplementRepository")
 */
class Complement
{
    /**
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="organisme", type="string")
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(name="consultation_id", type="integer")
     */
    private ?int $consultationId = null;

    /**
     * @ORM\Column(name="complement", type="integer")
     */
    private ?int $complement = null;

    /**
     * @ORM\Column(name="nom_fichier", type="string")
     */
    private ?string $nomFichier = null;

    /**
     * @ORM\Column(name="statut", type="string")
     */
    private ?string $statut = null;

    /**
     * @ORM\Column(name="horodatage", type="blob")
     */
    private $horodatage;

    /**
     * @ORM\Column(name="untrusteddate", type="datetime")
     */
    private ?\DateTime $untrusteddate = null;

    /**
     * @ORM\Column(name="agent_id", type="integer")
     */
    private ?int $agentId = null;

    /**
     * @ORM\ManyToOne(targetEntity=Consultation::class, inversedBy="complement")
     * @ORM\JoinColumn(name="consultation_id", referencedColumnName="id")
     */
    private ?Consultation $consultation = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param string $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return int
     */
    public function getConsultationId()
    {
        return $this->consultationId;
    }

    /**
     * @param int $consultationId
     */
    public function setConsultationId($consultationId)
    {
        $this->consultationId = $consultationId;
    }

    /**
     * @return int
     */
    public function getComplement()
    {
        return $this->complement;
    }

    /**
     * @param int $complement
     */
    public function setComplement($complement)
    {
        $this->complement = $complement;
    }

    /**
     * @return string
     */
    public function getNomFichier()
    {
        return $this->nomFichier;
    }

    /**
     * @param string $nomFichier
     */
    public function setNomFichier($nomFichier)
    {
        $this->nomFichier = $nomFichier;
    }

    /**
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @param string $statut
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;
    }

    /**
     * @return blob
     */
    public function getHorodatage()
    {
        return $this->horodatage;
    }

    /**
     * @param blob $horodatage
     */
    public function setHorodatage($horodatage)
    {
        $this->horodatage = $horodatage;
    }

    /**
     * @return string
     */
    public function getUntrusteddate()
    {
        return $this->untrusteddate;
    }

    /**
     * @param string $untrusteddate
     */
    public function setUntrusteddate($untrusteddate)
    {
        $this->untrusteddate = $untrusteddate;
    }

    /**
     * @return int
     */
    public function getAgentId()
    {
        return $this->agentId;
    }

    /**
     * @param int $agentId
     */
    public function setAgentId($agentId)
    {
        $this->agentId = $agentId;
    }

    public function getConsultation(): ?Consultation
    {
        return $this->consultation;
    }

    public function setConsultation(?Consultation $consultation): Complement
    {
        $this->consultation = $consultation;

        return $this;
    }
}
