<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class SocleHabilitationAgent.
 *
 * @ORM\Table(name="Socle_Habilitation_Agent")
 * @ORM\Entity(repositoryClass="App\Repository\SocleHabilitationAgentRepository")
 */
class SocleHabilitationAgent
{
    /**
     * @var Agent
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="App\Entity\Agent", inversedBy="socleHabilitations")
     * @ORM\JoinColumn(name="id_agent", referencedColumnName="id")
     */
    private $agent;

    /**
     * @ORM\Column(name="gestion_agent_pole_socle", type="string", nullable=false)
     */
    private string $gestionAgentPoleSocle = '0';

    /**
     * @ORM\Column(name="gestion_agents_socle", type="string", nullable=false)
     */
    private string $gestionAgentsSocle = '0';

    /**
     * @ORM\Column(name="droit_gestion_services_socle", type="string", nullable=false)
     */
    private string $droitGestionServicesSocle = '0';

    public function getAgent(): Agent
    {
        return $this->agent;
    }

    public function setAgent(Agent $agent): SocleHabilitationAgent
    {
        $this->agent = $agent;
        return $this;
    }

    public function getGestionAgentPoleSocle(): string
    {
        return $this->gestionAgentPoleSocle;
    }

    public function setGestionAgentPoleSocle(string $gestionAgentPoleSocle): SocleHabilitationAgent
    {
        $this->gestionAgentPoleSocle = $gestionAgentPoleSocle;

        return $this;
    }

    public function getGestionAgentsSocle(): string
    {
        return $this->gestionAgentsSocle;
    }

    public function setGestionAgentsSocle(string $gestionAgentsSocle): SocleHabilitationAgent
    {
        $this->gestionAgentsSocle = $gestionAgentsSocle;

        return $this;
    }

    public function getDroitGestionServicesSocle(): string
    {
        return $this->droitGestionServicesSocle;
    }

    public function setDroitGestionServicesSocle(string $droitGestionServicesSocle): SocleHabilitationAgent
    {
        $this->droitGestionServicesSocle = $droitGestionServicesSocle;

        return $this;
    }
}
