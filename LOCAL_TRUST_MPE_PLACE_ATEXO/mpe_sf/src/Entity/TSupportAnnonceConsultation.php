<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="t_support_annonce_consultation")
 * @ORM\Entity(repositoryClass="App\Repository\TSupportAnnonceConsultationRepository")
 */
class TSupportAnnonceConsultation
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TSupportPublication")
     * @ORM\JoinColumn(name="id_support", referencedColumnName="id")
     */
    private $idSupport;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TAnnonceConsultation")
     * @ORM\JoinColumn(name="id_annonce_cons", referencedColumnName="id")
     */
    private $idAnnonceCons;

    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="prenom_nom_agent_createur", type="string")
     */
    private ?string $prenomNomAgentCreateur = null;

    /**
     * @ORM\Column(name="id_agent", type="integer")
     */
    private ?int $idAgent = null;

    /**
     * @var string
     *
     * @ORM\Column(name="date_creation", type="datetime")
     */
    private $dateCreation;

    /**
     * @ORM\Column(name="statut", type="string")
     */
    private ?string $statut = null;

    /**
     * @var string
     *
     * @ORM\Column(name="date_statut", type="datetime")
     */
    private $dateStatut;

    /**
     * @ORM\Column(name="numero_avis", type="string", nullable=true)
     */
    private ?string $numeroAvis = null;

    /**
     * @ORM\Column(name="message_statut", type="string", nullable=true)
     */
    private ?string $messageStatut = null;

    /**
     * @ORM\Column(name="lien_publication", type="text", nullable=true)
     */
    private ?string $lienPublication = null;

    /**
     * @var string
     *
     * @ORM\Column(name="date_envoi_support", type="datetime", nullable=true)
     */
    private $dateEnvoiSupport;

    /**
     * @var string
     *
     * @ORM\Column(name="date_publication_support", type="datetime", nullable=true)
     */
    private $datePublicationSupport;

    /**
     * @ORM\Column(name="numero_avis_parent", type="string", nullable=true)
     */
    private ?string $numeroAvisParent = null;

    /**
     * @ORM\Column(name="id_offre", type="integer", nullable=true)
     */
    private ?int $idOffre = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPrenomNomAgentCreateur()
    {
        return $this->prenomNomAgentCreateur;
    }

    /**
     * @param string $prenomNomAgentCreateur
     *
     * @return $this
     */
    public function setPrenomNomAgentCreateur($prenomNomAgentCreateur)
    {
        $this->prenomNomAgentCreateur = $prenomNomAgentCreateur;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdAgent()
    {
        return $this->idAgent;
    }

    /**
     * @param int $idAgent
     *
     * @return $this
     */
    public function setIdAgent($idAgent)
    {
        $this->idAgent = $idAgent;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param string $dateCreation
     *
     * @return $this
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @param string $statut
     *
     * @return $this
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateStatut()
    {
        return $this->dateStatut;
    }

    /**
     * @param string $dateStatut
     *
     * @return $this
     */
    public function setDateStatut($dateStatut)
    {
        $this->dateStatut = $dateStatut;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumeroAvis()
    {
        return $this->numeroAvis;
    }

    /**
     * @param string $numeroAvis
     *
     * @return $this
     */
    public function setNumeroAvis($numeroAvis)
    {
        $this->numeroAvis = $numeroAvis;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessageStatut()
    {
        return $this->messageStatut;
    }

    /**
     * @param string $messageStatut
     *
     * @return $this
     */
    public function setMessageStatut($messageStatut)
    {
        $this->messageStatut = $messageStatut;

        return $this;
    }

    /**
     * @return string
     */
    public function getLienPublication()
    {
        return $this->lienPublication;
    }

    /**
     * @param string $lienPublication
     *
     * @return $this
     */
    public function setLienPublication($lienPublication)
    {
        $this->lienPublication = $lienPublication;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateEnvoiSupport()
    {
        return $this->dateEnvoiSupport;
    }

    /**
     * @param string $dateEnvoiSupport
     *
     * @return $this
     */
    public function setDateEnvoiSupport($dateEnvoiSupport)
    {
        $this->dateEnvoiSupport = $dateEnvoiSupport;

        return $this;
    }

    /**
     * @return string
     */
    public function getDatePublicationSupport()
    {
        return $this->datePublicationSupport;
    }

    /**
     * @param string $datePublicationSupport
     *
     * @return $this
     */
    public function setDatePublicationSupport($datePublicationSupport)
    {
        $this->datePublicationSupport = $datePublicationSupport;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumeroAvisParent()
    {
        return $this->numeroAvisParent;
    }

    /**
     * @param string $numeroAvisParent
     *
     * @return $this
     */
    public function setNumeroAvisParent($numeroAvisParent)
    {
        $this->numeroAvisParent = $numeroAvisParent;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdOffre()
    {
        return $this->idOffre;
    }

    /**
     * @param int $idOffre
     *
     * @return $this
     */
    public function setIdOffre($idOffre)
    {
        $this->idOffre = $idOffre;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdSupport()
    {
        return $this->idSupport;
    }

    /**
     * @param mixed $idSupport
     */
    public function setIdSupport($idSupport)
    {
        $this->idSupport = $idSupport;
    }

    /**
     * @return mixed
     */
    public function getIdAnnonceCons()
    {
        return $this->idAnnonceCons;
    }

    /**
     * @param mixed $idAnnonceCons
     */
    public function setIdAnnonceCons($idAnnonceCons)
    {
        $this->idAnnonceCons = $idAnnonceCons;
    }
}
