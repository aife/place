<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TGroupementEntreprise.
 *
 * @ORM\Table(name="t_groupement_entreprise", indexes={@ORM\Index(name="fk_Groupement_Entreprise_Type_Groupement", columns={"id_type_groupement"})})
 * @ORM\Entity(repositoryClass="App\Repository\TGroupementEntrepriseRepository")
 */
class TGroupementEntreprise
{
    /**
     *
     * @ORM\Column(name="id_groupement_entreprise", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $idGroupementEntreprise;

    /**
     * @ORM\Column(name="id_offre", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private ?int $idOffre = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime", precision=0, scale=0, nullable=true, unique=false)*/
    private $dateCreation;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\TTypeGroupementEntreprise")
     * @ORM\JoinColumn(name="id_type_groupement", referencedColumnName="id_type_groupement", nullable=true)
     */
    private ?TTypeGroupementEntreprise $idTypeGroupement = null;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\TMembreGroupementEntreprise", mappedBy="groupement",cascade={"persist"})
     * @ORM\JoinColumn(name="id_groupement_entreprise", referencedColumnName="id_groupement_entreprise")
     */
    private Collection $membresGroupement;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\TCandidature")
     * @ORM\JoinColumn(name="id_candidature", referencedColumnName="id")
     */
    private $candidature;

    /**
     * TGroupementEntreprise constructor.
     */
    public function __construct()
    {
        $this->membresGroupement = new ArrayCollection();
    }

    /**
     * Get idGroupementEntreprise.
     *
     * @return int
     */
    public function getIdGroupementEntreprise()
    {
        return $this->idGroupementEntreprise;
    }

    /**
     * Set idOffre.
     *
     * @param int $idOffre
     *
     * @return TGroupementEntreprise
     */
    public function setIdOffre($idOffre)
    {
        $this->idOffre = $idOffre;

        return $this;
    }

    /**
     * Get idOffre.
     *
     * @return int
     */
    public function getIdOffre()
    {
        return $this->idOffre;
    }

    /**
     * Set dateCreation.
     *
     * @param DateTime $dateCreation
     *
     * @return TGroupementEntreprise
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation.
     *
     * @return DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set idTypeGroupement.
     *
     *
     * @return TGroupementEntreprise
     */
    public function setIdTypeGroupement(TTypeGroupementEntreprise $idTypeGroupement = null)
    {
        $this->idTypeGroupement = $idTypeGroupement;

        return $this;
    }

    /**
     * Get idTypeGroupement.
     *
     * @return TTypeGroupementEntreprise
     */
    public function getIdTypeGroupement()
    {
        return $this->idTypeGroupement;
    }

    /**
     * ajoute un membre.
     *
     * @return $this
     */
    public function addMembresGroupement(TMembreGroupementEntreprise $membreGroupementEntreprise)
    {
        $this->membresGroupement[$membreGroupementEntreprise->getIdEntreprise()] = $membreGroupementEntreprise;

        return $this;
    }

    /**
     * supprime un membre.
     *
     * @return $this
     */
    public function removeMembresGroupement(TMembreGroupementEntreprise $membreGroupementEntreprise)
    {
        $this->membresGroupement->removeElement($membreGroupementEntreprise);

        return $this;
    }

    /**
     * supprime un membre by key.
     *
     * @param int $idEntreprise
     *
     * @return $this
     */
    public function removeMembresGroupementByKey($idEntreprise)
    {
        $this->membresGroupement->remove($idEntreprise);

        return $this;
    }

    /**
     * trouver un membre.
     *
     * @param int $idEntreprise
     *
     * @return bool
     */
    public function hasMembre($idEntreprise)
    {
        $hasMembre = $this->membresGroupement->containsKey($idEntreprise);

        if (!$hasMembre) {
            foreach ($this->membresGroupement as $membre) {
                if ($membre->hasMembre($idEntreprise)) {
                    $hasMembre = true;
                    break;
                }
            }
        }

        return $hasMembre;
    }

    /**
     * @return ArrayCollection
     */
    public function getMembresGroupement()
    {
        return $this->membresGroupement;
    }

    /**
     * trouver un membre.
     *
     * @param int $idEntreprise
     *
     * @return TMembreGroupementEntreprise
     */
    public function getMembreGroupementByKey($idEntreprise)
    {
        return $this->membresGroupement[$idEntreprise];
    }

    /**
     * @return mixed
     */
    public function getCandidature()
    {
        return $this->candidature;
    }

    /**
     * @param $candidature
     *
     * @return $this
     */
    public function setCandidature($candidature)
    {
        $this->candidature = $candidature;

        return $this;
    }
}
