<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\ApiPlatformCustom\LotDonneeComplementaire;
use App\Dto\Input\LotInput;
use App\Dto\Output\LotOutput;
use App\Entity\Consultation\ClausesN1;
use App\Entity\Consultation\CritereAttribution;
use App\Entity\Consultation\DonneeComplementaire;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Lot.
 *
 * @ORM\Table(name="CategorieLot", indexes={
 *     @ORM\Index(name="organisme",
 *     columns={"organisme", "consultation_id", "lot"}),
 *     @ORM\Index(name="Idx_CategorieLot", columns={"consultation_id"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\LotRepository")
 */
#[ApiResource(
    collectionOperations: [
        "get",
        "create" => [
            "method"            => "POST",
            "input"             => LotInput::class,
            "validation_groups" => ['Default', 'postValidation'],
            "security"          => "is_granted('LOT_CREATE')",
            "openapi_context"   => [
                "description"   => '## Ce WebService nécessite l\'habilitation "creer_consultation".'
            ]
        ]
    ],
    itemOperations: [
        "get",
        "update" => [
            "method" => "PUT",
            "input" => LotInput::class,
            "security" => "is_granted('LOT_UPDATE', object)",
            "openapi_context" => [
                "description" =>
                    '## Ce WebService nécessite l\'habilitation "modifier_consultation_avant_validation".',
            ],
        ],
        "patch" => [
            "method" => "PATCH",
            "input" => LotInput::class,
            "security" => "is_granted('LOT_UPDATE', object)",
            "openapi_context" => [
                "description" =>
                    '## Ce WebService nécessite l\'habilitation "modifier_consultation_avant_validation".',
            ],
        ],
        "delete" => [
            "method" => "DELETE",
            "security" => "is_granted('LOT_UPDATE', object)",
            "openapi_context" => [
                "description" =>
                    '## Ce WebService nécessite l\'habilitation "modifier_consultation_avant_validation".',
            ],
        ],
        "get_donnee_complementaire" => [
            "method"            => "GET",
            "path"              => "/lots/{id}/donnee-complementaire",
            'controller'        => LotDonneeComplementaire::class,
            "openapi_context"   => [
                'description' => 'Retrieves donnee complementaire',
                'responses' => [
                    '200' => [
                        'description' => 'Retrieves Donneee complementaire',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    'type' => 'object',
                                ],
                            ]
                        ]
                    ]
                ]
            ]
        ],
    ],
    shortName: 'Lot',
    attributes: ["security_message" => "Habilitation manquante pour effectuer cette action"],
    denormalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: LotOutput::class,
    routePrefix: '/'
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'consultation' => 'exact',
    ]
)]
class Lot
{
    public const CLAUSE_ACTIVE = 1;

    /**
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation", inversedBy="lots", fetch="EAGER")
     * @ORM\JoinColumn(name="consultation_id", referencedColumnName="id")
     */
    private ?Consultation $consultation = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Consultation\DonneeComplementaire", fetch="EAGER")
     * @ORM\JoinColumn(name="id_donnee_complementaire", referencedColumnName="id_donnee_complementaire")
     */
    private ?DonneeComplementaire $donneeComplementaire = null;

    /**
     * @ORM\Column(name="organisme", type="string", length=30)
     */
    private string $organisme = '';

    /**
     * @var int
     *
     * @ORM\Column(name="lot", type="integer")
     */
    private string|int $lot = '0';

    /**
     * @ORM\Column(name="description", type="string", length=1000, nullable=false)
     */
    private string $description = '';

    /**
     * @ORM\Column(name="id_tr_description", type="integer", nullable=true)
     */
    private ?int $idTrDescription = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CategorieConsultation", fetch="EAGER")
     * @ORM\JoinColumn(name="categorie", referencedColumnName="id")
     */
    private CategorieConsultation|string|null $categorie = '';

    /**
     * @ORM\Column(name="description_detail", type="string", length=1000, nullable=false)
     */
    private string $descriptionDetail = '';

    /**
     * @ORM\Column(name="id_tr_description_detail", type="integer", nullable=true)
     */
    private ?int $idTrDescriptionDetail = null;

    /**
     * @ORM\Column(name="code_cpv_1", type="string", length=8, nullable=true)
     */
    private ?string $codeCpv1 = null;

    /**
     * @ORM\Column(name="code_cpv_2", type="string", length=255, nullable=true)
     */
    private ?string $codeCpv2 = null;

    /**
     * @ORM\Column(name="description_fr", type="string", length=1000, nullable=false)
     */
    private string $descriptionFr = '';

    /**
     * @ORM\Column(name="description_en", type="string", length=255, nullable=false)
     */
    private string $descriptionEn = '';

    /**
     * @ORM\Column(name="description_es", type="string", length=255, nullable=false)
     */
    private string $descriptionEs = '';

    /**
     * @ORM\Column(name="description_su", type="string", length=255, nullable=false)
     */
    private string $descriptionSu = '';

    /**
     * @ORM\Column(name="description_du", type="string", length=255, nullable=false)
     */
    private string $descriptionDu = '';

    /**
     * @ORM\Column(name="description_cz", type="string", length=255, nullable=false)
     */
    private string $descriptionCz = '';

    /**
     * @ORM\Column(name="description_ar", type="text", nullable=true)
     */
    private ?string $descriptionAr = null;

    /**
     * @ORM\Column(name="description_detail_fr", type="string", length=1000, nullable=false)
     */
    private string $descriptionDetailFr = '';

    /**
     * @ORM\Column(name="description_detail_en", type="string", length=255, nullable=true)
     */
    private ?string $descriptionDetailEn = null;

    /**
     * @ORM\Column(name="description_detail_es", type="string", length=255, nullable=true)
     */
    private ?string $descriptionDetailEs = null;

    /**
     * @ORM\Column(name="description_detail_su", type="string", length=255, nullable=true)
     */
    private ?string $descriptionDetailSu = null;

    /**
     * @ORM\Column(name="description_detail_du", type="string", length=255, nullable=true)
     */
    private ?string $descriptionDetailDu = null;

    /**
     * @ORM\Column(name="description_detail_cz", type="string", length=255, nullable=true)
     */
    private ?string $descriptionDetailCz = null;

    /**
     * @ORM\Column(name="description_detail_ar", type="text", nullable=true)
     */
    private ?string $descriptionDetailAr = null;

    /**
     * @ORM\Column(name="id_lot_externe", type="integer", nullable=true)
     */
    private ?int $idLotExterne = null;

    /**
     * @ORM\Column(name="caution_provisoire", type="string", length=255, nullable=true)
     */
    private ?string $cautionProvisoire = null;

    /**
     * @ORM\Column(name="qualification", type="string", length=255, nullable=true)
     */
    private ?string $qualification = null;

    /**
     * @ORM\Column(name="agrements", type="string", length=255, nullable=true)
     */
    private ?string $agrements = null;

    /**
     * @ORM\Column(name="add_echantillion", type="string", length=255, nullable=true)
     */
    private ?string $addEchantillion = null;

    /**
     * @ORM\Column(name="date_limite_echantillion", type="string", length=50, nullable=true)
     */
    private ?string $dateLimiteEchantillion = null;

    /**
     * @ORM\Column(name="add_reunion", type="string", length=255, nullable=true)
     */
    private ?string $addReunion = null;

    /**
     * @ORM\Column(name="date_reunion", type="string", length=50, nullable=true)
     */
    private ?string $dateReunion = null;

    /**
     * @ORM\Column(name="variantes", type="string", length=1, nullable=true)
     */
    private ?string $variantes = null;

    /**
     * @ORM\Column(name="echantillon", type="string", nullable=false)
     */
    private string $echantillon = '0';

    /**
     * @ORM\Column(name="reunion", type="string", nullable=false)
     */
    private string $reunion = '0';

    /**
     * @ORM\Column(name="visites_lieux", type="string", nullable=false)
     */
    private string $visitesLieux = '0';

    /**
     * @ORM\Column(name="add_echantillion_fr", type="string", length=255, nullable=true)
     */
    private ?string $addEchantillionFr = null;

    /**
     * @ORM\Column(name="add_echantillion_en", type="string", length=255, nullable=true)
     */
    private ?string $addEchantillionEn = null;

    /**
     * @ORM\Column(name="add_echantillion_es", type="string", length=255, nullable=true)
     */
    private ?string $addEchantillionEs = null;

    /**
     * @ORM\Column(name="add_echantillion_su", type="string", length=255, nullable=true)
     */
    private ?string $addEchantillionSu = null;

    /**
     * @ORM\Column(name="add_echantillion_du", type="string", length=255, nullable=true)
     */
    private ?string $addEchantillionDu = null;

    /**
     * @ORM\Column(name="add_echantillion_cz", type="string", length=255, nullable=true)
     */
    private ?string $addEchantillionCz = null;

    /**
     * @ORM\Column(name="add_echantillion_ar", type="string", length=255, nullable=true)
     */
    private ?string $addEchantillionAr = null;

    /**
     * @ORM\Column(name="add_reunion_fr", type="string", length=255, nullable=true)
     */
    private ?string $addReunionFr = null;

    /**
     * @ORM\Column(name="add_reunion_en", type="string", length=255, nullable=true)
     */
    private ?string $addReunionEn = null;

    /**
     * @ORM\Column(name="add_reunion_es", type="string", length=255, nullable=true)
     */
    private ?string $addReunionEs = null;

    /**
     * @ORM\Column(name="add_reunion_su", type="string", length=255, nullable=true)
     */
    private ?string $addReunionSu = null;

    /**
     * @ORM\Column(name="add_reunion_du", type="string", length=255, nullable=true)
     */
    private ?string $addReunionDu = null;

    /**
     * @ORM\Column(name="add_reunion_cz", type="string", length=255, nullable=true)
     */
    private ?string $addReunionCz = null;

    /**
     * @ORM\Column(name="add_reunion_ar", type="string", length=255, nullable=true)
     */
    private ?string $addReunionAr = null;

    /**
     * @ORM\Column(name="description_detail_it", type="text", nullable=true)
     */
    private ?string $descriptionDetailIt = null;

    /**
     * @ORM\Column(name="description_it", type="text", nullable=true)
     */
    private ?string $descriptionIt = null;

    /**
     * @ORM\Column(name="add_echantillion_it", type="string", length=255, nullable=true)
     */
    private ?string $addEchantillionIt = null;

    /**
     * @ORM\Column(name="add_reunion_it", type="string", length=255, nullable=true)
     */
    private ?string $addReunionIt = null;

    /**
     * @ORM\Column(name="clause_sociale", type="string", nullable=false)
     */
    private string $clauseSociale = '0';

    /**
     * @ORM\Column(name="clause_environnementale", type="string", nullable=false)
     */
    private string $clauseEnvironnementale = '0';

    /**
     * @ORM\Column(name="decision", type="string", nullable=false)
     */
    private string $decision = '0';

    /**
     * @ORM\Column(name="clause_sociale_condition_execution", type="string", length=255, nullable=true)
     */
    private ?string $clauseSocialeConditionExecution = '0';

    /**
     * @ORM\Column(name="clause_sociale_insertion", type="string", length=255, nullable=true)
     */
    private ?string $clauseSocialeInsertion = '0';

    /**
     * @ORM\Column(name="clause_sociale_ateliers_proteges", type="string", length=255, nullable=true)
     */
    private ?string $clauseSocialeAteliersProteges = '0';

    /**
     * @ORM\Column(name="clause_sociale_siae", type="string", length=255, nullable=true)
     */
    private ?string $clauseSocialeSiae = '0';

    /**
     * @ORM\Column(name="clause_sociale_ess", type="string", length=255, nullable=true)
     */
    private ?string $clauseSocialeEss = '0';

    /**
     * @ORM\Column(name="clause_env_specs_techniques", type="string", length=255, nullable=true)
     */
    private ?string $clauseEnvSpecsTechniques = '0';

    /**
     * @ORM\Column(name="clause_env_cond_execution", type="string", length=255, nullable=true)
     */
    private ?string $clauseEnvCondExecution = '0';

    /**
     * @ORM\Column(name="clause_env_criteres_select", type="string", length=255, nullable=true)
     */
    private ?string $clauseEnvCriteresSelect = '0';

    /**
     * @ORM\Column(name="id_donnee_complementaire", type="integer", nullable=true)
     */
    private ?int $idDonneeComplementaire = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="marche_insertion", type="boolean", nullable=false)
     */
    private $marcheInsertion = false;

    /**
     * @ORM\Column(name="clause_specification_technique", type="string", length=255, nullable=true)
     */
    private ?string $clauseSpecificationTechnique = '0';

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\PieceGenereConsultation", mappedBy="lot")
     */
    private ?PieceGenereConsultation $pieceGeneree = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\OneToMany(targetEntity=CritereAttribution::class, mappedBy="lot")
     */
    private $critereAttribution;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Consultation\ClausesN1", mappedBy="lot", orphanRemoval=true)
     */
    private Collection $clausesN1;

    public function __construct()
    {
        $this->critereAttribution = new ArrayCollection();
        $this->clausesN1 = new ArrayCollection();
    }


    /**
     * Set organisme.
     *
     * @param string $organisme
     *
     * @return Lot
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * Get organisme.
     *
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * Set lot.
     *
     * @param int $lot
     *
     * @return Lot
     */
    public function setLot($lot)
    {
        $this->lot = $lot;

        return $this;
    }

    /**
     * Get lot.
     *
     * @return int
     */
    public function getLot()
    {
        return $this->lot;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Lot
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set idTrDescription.
     *
     * @param int $idTrDescription
     *
     * @return Lot
     */
    public function setIdTrDescription($idTrDescription)
    {
        $this->idTrDescription = $idTrDescription;

        return $this;
    }

    /**
     * Get idTrDescription.
     *
     * @return int
     */
    public function getIdTrDescription()
    {
        return $this->idTrDescription;
    }

    /**
     * Set categorie.
     */
    public function setCategorie(CategorieConsultation|string|null $categorie): Lot
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie.
     */
    public function getCategorie(): ?CategorieConsultation
    {
        return $this->categorie;
    }

    /**
     * Set descriptionDetail.
     *
     * @param string $descriptionDetail
     *
     * @return Lot
     */
    public function setDescriptionDetail($descriptionDetail)
    {
        $this->descriptionDetail = $descriptionDetail;

        return $this;
    }

    /**
     * Get descriptionDetail.
     *
     * @return string
     */
    public function getDescriptionDetail()
    {
        return $this->descriptionDetail;
    }

    /**
     * Set idTrDescriptionDetail.
     *
     * @param int $idTrDescriptionDetail
     *
     * @return Lot
     */
    public function setIdTrDescriptionDetail($idTrDescriptionDetail)
    {
        $this->idTrDescriptionDetail = $idTrDescriptionDetail;

        return $this;
    }

    /**
     * Get idTrDescriptionDetail.
     *
     * @return int
     */
    public function getIdTrDescriptionDetail()
    {
        return $this->idTrDescriptionDetail;
    }

    /**
     * Set codeCpv1.
     *
     * @param string $codeCpv1
     *
     * @return Lot
     */
    public function setCodeCpv1($codeCpv1)
    {
        $this->codeCpv1 = $codeCpv1;

        return $this;
    }

    /**
     * Get codeCpv1.
     *
     * @return string
     */
    public function getCodeCpv1()
    {
        return $this->codeCpv1;
    }

    /**
     * Set codeCpv2.
     *
     * @param string $codeCpv2
     *
     * @return Lot
     */
    public function setCodeCpv2($codeCpv2)
    {
        $this->codeCpv2 = $codeCpv2;

        return $this;
    }

    /**
     * Get codeCpv2.
     *
     * @return string
     */
    public function getCodeCpv2()
    {
        return $this->codeCpv2;
    }

    /**
     * Set descriptionFr.
     *
     * @param string $descriptionFr
     *
     * @return Lot
     */
    public function setDescriptionFr($descriptionFr)
    {
        $this->descriptionFr = $descriptionFr;

        return $this;
    }

    /**
     * Get descriptionFr.
     *
     * @return string
     */
    public function getDescriptionFr()
    {
        return $this->descriptionFr;
    }

    /**
     * Set descriptionEn.
     *
     * @param string $descriptionEn
     *
     * @return Lot
     */
    public function setDescriptionEn($descriptionEn)
    {
        $this->descriptionEn = $descriptionEn;

        return $this;
    }

    /**
     * Get descriptionEn.
     *
     * @return string
     */
    public function getDescriptionEn()
    {
        return $this->descriptionEn;
    }

    /**
     * Set descriptionEs.
     *
     * @param string $descriptionEs
     *
     * @return Lot
     */
    public function setDescriptionEs($descriptionEs)
    {
        $this->descriptionEs = $descriptionEs;

        return $this;
    }

    /**
     * Get descriptionEs.
     *
     * @return string
     */
    public function getDescriptionEs()
    {
        return $this->descriptionEs;
    }

    /**
     * Set descriptionSu.
     *
     * @param string $descriptionSu
     *
     * @return Lot
     */
    public function setDescriptionSu($descriptionSu)
    {
        $this->descriptionSu = $descriptionSu;

        return $this;
    }

    /**
     * Get descriptionSu.
     *
     * @return string
     */
    public function getDescriptionSu()
    {
        return $this->descriptionSu;
    }

    /**
     * Set descriptionDu.
     *
     * @param string $descriptionDu
     *
     * @return Lot
     */
    public function setDescriptionDu($descriptionDu)
    {
        $this->descriptionDu = $descriptionDu;

        return $this;
    }

    /**
     * Get descriptionDu.
     *
     * @return string
     */
    public function getDescriptionDu()
    {
        return $this->descriptionDu;
    }

    /**
     * Set descriptionCz.
     *
     * @param string $descriptionCz
     *
     * @return Lot
     */
    public function setDescriptionCz($descriptionCz)
    {
        $this->descriptionCz = $descriptionCz;

        return $this;
    }

    /**
     * Get descriptionCz.
     *
     * @return string
     */
    public function getDescriptionCz()
    {
        return $this->descriptionCz;
    }

    /**
     * Set descriptionAr.
     *
     * @param string $descriptionAr
     *
     * @return Lot
     */
    public function setDescriptionAr($descriptionAr)
    {
        $this->descriptionAr = $descriptionAr;

        return $this;
    }

    /**
     * Get descriptionAr.
     *
     * @return string
     */
    public function getDescriptionAr()
    {
        return $this->descriptionAr;
    }

    /**
     * Set descriptionDetailFr.
     *
     * @param string $descriptionDetailFr
     *
     * @return Lot
     */
    public function setDescriptionDetailFr($descriptionDetailFr)
    {
        $this->descriptionDetailFr = $descriptionDetailFr;

        return $this;
    }

    /**
     * Get descriptionDetailFr.
     *
     * @return string
     */
    public function getDescriptionDetailFr()
    {
        return $this->descriptionDetailFr;
    }

    /**
     * Set descriptionDetailEn.
     *
     * @param string $descriptionDetailEn
     *
     * @return Lot
     */
    public function setDescriptionDetailEn($descriptionDetailEn)
    {
        $this->descriptionDetailEn = $descriptionDetailEn;

        return $this;
    }

    /**
     * Get descriptionDetailEn.
     *
     * @return string
     */
    public function getDescriptionDetailEn()
    {
        return $this->descriptionDetailEn;
    }

    /**
     * Set descriptionDetailEs.
     *
     * @param string $descriptionDetailEs
     *
     * @return Lot
     */
    public function setDescriptionDetailEs($descriptionDetailEs)
    {
        $this->descriptionDetailEs = $descriptionDetailEs;

        return $this;
    }

    /**
     * Get descriptionDetailEs.
     *
     * @return string
     */
    public function getDescriptionDetailEs()
    {
        return $this->descriptionDetailEs;
    }

    /**
     * Set descriptionDetailSu.
     *
     * @param string $descriptionDetailSu
     *
     * @return Lot
     */
    public function setDescriptionDetailSu($descriptionDetailSu)
    {
        $this->descriptionDetailSu = $descriptionDetailSu;

        return $this;
    }

    /**
     * Get descriptionDetailSu.
     *
     * @return string
     */
    public function getDescriptionDetailSu()
    {
        return $this->descriptionDetailSu;
    }

    /**
     * Set descriptionDetailDu.
     *
     * @param string $descriptionDetailDu
     *
     * @return Lot
     */
    public function setDescriptionDetailDu($descriptionDetailDu)
    {
        $this->descriptionDetailDu = $descriptionDetailDu;

        return $this;
    }

    /**
     * Get descriptionDetailDu.
     *
     * @return string
     */
    public function getDescriptionDetailDu()
    {
        return $this->descriptionDetailDu;
    }

    /**
     * Set descriptionDetailCz.
     *
     * @param string $descriptionDetailCz
     *
     * @return Lot
     */
    public function setDescriptionDetailCz($descriptionDetailCz)
    {
        $this->descriptionDetailCz = $descriptionDetailCz;

        return $this;
    }

    /**
     * Get descriptionDetailCz.
     *
     * @return string
     */
    public function getDescriptionDetailCz()
    {
        return $this->descriptionDetailCz;
    }

    /**
     * Set descriptionDetailAr.
     *
     * @param string $descriptionDetailAr
     *
     * @return Lot
     */
    public function setDescriptionDetailAr($descriptionDetailAr)
    {
        $this->descriptionDetailAr = $descriptionDetailAr;

        return $this;
    }

    /**
     * Get descriptionDetailAr.
     *
     * @return string
     */
    public function getDescriptionDetailAr()
    {
        return $this->descriptionDetailAr;
    }

    /**
     * Set idLotExterne.
     *
     * @param int $idLotExterne
     *
     * @return Lot
     */
    public function setIdLotExterne($idLotExterne)
    {
        $this->idLotExterne = $idLotExterne;

        return $this;
    }

    /**
     * Get idLotExterne.
     *
     * @return int
     */
    public function getIdLotExterne()
    {
        return $this->idLotExterne;
    }

    /**
     * Set cautionProvisoire.
     *
     * @param string $cautionProvisoire
     *
     * @return Lot
     */
    public function setCautionProvisoire($cautionProvisoire)
    {
        $this->cautionProvisoire = $cautionProvisoire;

        return $this;
    }

    /**
     * Get cautionProvisoire.
     *
     * @return string
     */
    public function getCautionProvisoire()
    {
        return $this->cautionProvisoire;
    }

    /**
     * Set qualification.
     *
     * @param string $qualification
     *
     * @return Lot
     */
    public function setQualification($qualification)
    {
        $this->qualification = $qualification;

        return $this;
    }

    /**
     * Get qualification.
     *
     * @return string
     */
    public function getQualification()
    {
        return $this->qualification;
    }

    /**
     * Set agrements.
     *
     * @param string $agrements
     *
     * @return Lot
     */
    public function setAgrements($agrements)
    {
        $this->agrements = $agrements;

        return $this;
    }

    /**
     * Get agrements.
     *
     * @return string
     */
    public function getAgrements()
    {
        return $this->agrements;
    }

    /**
     * Set addEchantillion.
     *
     * @param string $addEchantillion
     *
     * @return Lot
     */
    public function setAddEchantillion($addEchantillion)
    {
        $this->addEchantillion = $addEchantillion;

        return $this;
    }

    /**
     * Get addEchantillion.
     *
     * @return string
     */
    public function getAddEchantillion()
    {
        return $this->addEchantillion;
    }

    /**
     * Set dateLimiteEchantillion.
     *
     * @param string $dateLimiteEchantillion
     *
     * @return Lot
     */
    public function setDateLimiteEchantillion($dateLimiteEchantillion)
    {
        $this->dateLimiteEchantillion = $dateLimiteEchantillion;

        return $this;
    }

    /**
     * Get dateLimiteEchantillion.
     *
     * @return string
     */
    public function getDateLimiteEchantillion()
    {
        return $this->dateLimiteEchantillion;
    }

    /**
     * Set addReunion.
     *
     * @param string $addReunion
     *
     * @return Lot
     */
    public function setAddReunion($addReunion)
    {
        $this->addReunion = $addReunion;

        return $this;
    }

    /**
     * Get addReunion.
     *
     * @return string
     */
    public function getAddReunion()
    {
        return $this->addReunion;
    }

    /**
     * Set dateReunion.
     *
     * @param string $dateReunion
     *
     * @return Lot
     */
    public function setDateReunion($dateReunion)
    {
        $this->dateReunion = $dateReunion;

        return $this;
    }

    /**
     * Get dateReunion.
     *
     * @return string
     */
    public function getDateReunion()
    {
        return $this->dateReunion;
    }

    /**
     * Set variantes.
     *
     * @param string $variantes
     *
     * @return Lot
     */
    public function setVariantes($variantes)
    {
        $this->variantes = $variantes;

        return $this;
    }

    /**
     * Get variantes.
     *
     * @return string
     */
    public function getVariantes()
    {
        return $this->variantes;
    }

    /**
     * Set echantillon.
     *
     * @param string $echantillon
     *
     * @return Lot
     */
    public function setEchantillon($echantillon)
    {
        $this->echantillon = $echantillon;

        return $this;
    }

    /**
     * Get echantillon.
     *
     * @return string
     */
    public function getEchantillon()
    {
        return $this->echantillon;
    }

    /**
     * Set reunion.
     *
     * @param string $reunion
     *
     * @return Lot
     */
    public function setReunion($reunion)
    {
        $this->reunion = $reunion;

        return $this;
    }

    /**
     * Get reunion.
     *
     * @return string
     */
    public function getReunion()
    {
        return $this->reunion;
    }

    /**
     * Set visitesLieux.
     *
     * @param string $visitesLieux
     *
     * @return Lot
     */
    public function setVisitesLieux($visitesLieux)
    {
        $this->visitesLieux = $visitesLieux;

        return $this;
    }

    /**
     * Get visitesLieux.
     *
     * @return string
     */
    public function getVisitesLieux()
    {
        return $this->visitesLieux;
    }

    /**
     * Set addEchantillionFr.
     *
     * @param string $addEchantillionFr
     *
     * @return Lot
     */
    public function setAddEchantillionFr($addEchantillionFr)
    {
        $this->addEchantillionFr = $addEchantillionFr;

        return $this;
    }

    /**
     * Get addEchantillionFr.
     *
     * @return string
     */
    public function getAddEchantillionFr()
    {
        return $this->addEchantillionFr;
    }

    /**
     * Set addEchantillionEn.
     *
     * @param string $addEchantillionEn
     *
     * @return Lot
     */
    public function setAddEchantillionEn($addEchantillionEn)
    {
        $this->addEchantillionEn = $addEchantillionEn;

        return $this;
    }

    /**
     * Get addEchantillionEn.
     *
     * @return string
     */
    public function getAddEchantillionEn()
    {
        return $this->addEchantillionEn;
    }

    /**
     * Set addEchantillionEs.
     *
     * @param string $addEchantillionEs
     *
     * @return Lot
     */
    public function setAddEchantillionEs($addEchantillionEs)
    {
        $this->addEchantillionEs = $addEchantillionEs;

        return $this;
    }

    /**
     * Get addEchantillionEs.
     *
     * @return string
     */
    public function getAddEchantillionEs()
    {
        return $this->addEchantillionEs;
    }

    /**
     * Set addEchantillionSu.
     *
     * @param string $addEchantillionSu
     *
     * @return Lot
     */
    public function setAddEchantillionSu($addEchantillionSu)
    {
        $this->addEchantillionSu = $addEchantillionSu;

        return $this;
    }

    /**
     * Get addEchantillionSu.
     *
     * @return string
     */
    public function getAddEchantillionSu()
    {
        return $this->addEchantillionSu;
    }

    /**
     * Set addEchantillionDu.
     *
     * @param string $addEchantillionDu
     *
     * @return Lot
     */
    public function setAddEchantillionDu($addEchantillionDu)
    {
        $this->addEchantillionDu = $addEchantillionDu;

        return $this;
    }

    /**
     * Get addEchantillionDu.
     *
     * @return string
     */
    public function getAddEchantillionDu()
    {
        return $this->addEchantillionDu;
    }

    /**
     * Set addEchantillionCz.
     *
     * @param string $addEchantillionCz
     *
     * @return Lot
     */
    public function setAddEchantillionCz($addEchantillionCz)
    {
        $this->addEchantillionCz = $addEchantillionCz;

        return $this;
    }

    /**
     * Get addEchantillionCz.
     *
     * @return string
     */
    public function getAddEchantillionCz()
    {
        return $this->addEchantillionCz;
    }

    /**
     * Set addEchantillionAr.
     *
     * @param string $addEchantillionAr
     *
     * @return Lot
     */
    public function setAddEchantillionAr($addEchantillionAr)
    {
        $this->addEchantillionAr = $addEchantillionAr;

        return $this;
    }

    /**
     * Get addEchantillionAr.
     *
     * @return string
     */
    public function getAddEchantillionAr()
    {
        return $this->addEchantillionAr;
    }

    /**
     * Set addReunionFr.
     *
     * @param string $addReunionFr
     *
     * @return Lot
     */
    public function setAddReunionFr($addReunionFr)
    {
        $this->addReunionFr = $addReunionFr;

        return $this;
    }

    /**
     * Get addReunionFr.
     *
     * @return string
     */
    public function getAddReunionFr()
    {
        return $this->addReunionFr;
    }

    /**
     * Set addReunionEn.
     *
     * @param string $addReunionEn
     *
     * @return Lot
     */
    public function setAddReunionEn($addReunionEn)
    {
        $this->addReunionEn = $addReunionEn;

        return $this;
    }

    /**
     * Get addReunionEn.
     *
     * @return string
     */
    public function getAddReunionEn()
    {
        return $this->addReunionEn;
    }

    /**
     * Set addReunionEs.
     *
     * @param string $addReunionEs
     *
     * @return Lot
     */
    public function setAddReunionEs($addReunionEs)
    {
        $this->addReunionEs = $addReunionEs;

        return $this;
    }

    /**
     * Get addReunionEs.
     *
     * @return string
     */
    public function getAddReunionEs()
    {
        return $this->addReunionEs;
    }

    /**
     * Set addReunionSu.
     *
     * @param string $addReunionSu
     *
     * @return Lot
     */
    public function setAddReunionSu($addReunionSu)
    {
        $this->addReunionSu = $addReunionSu;

        return $this;
    }

    /**
     * Get addReunionSu.
     *
     * @return string
     */
    public function getAddReunionSu()
    {
        return $this->addReunionSu;
    }

    /**
     * Set addReunionDu.
     *
     * @param string $addReunionDu
     *
     * @return Lot
     */
    public function setAddReunionDu($addReunionDu)
    {
        $this->addReunionDu = $addReunionDu;

        return $this;
    }

    /**
     * Get addReunionDu.
     *
     * @return string
     */
    public function getAddReunionDu()
    {
        return $this->addReunionDu;
    }

    /**
     * Set addReunionCz.
     *
     * @param string $addReunionCz
     *
     * @return Lot
     */
    public function setAddReunionCz($addReunionCz)
    {
        $this->addReunionCz = $addReunionCz;

        return $this;
    }

    /**
     * Get addReunionCz.
     *
     * @return string
     */
    public function getAddReunionCz()
    {
        return $this->addReunionCz;
    }

    /**
     * Set addReunionAr.
     *
     * @param string $addReunionAr
     *
     * @return Lot
     */
    public function setAddReunionAr($addReunionAr)
    {
        $this->addReunionAr = $addReunionAr;

        return $this;
    }

    /**
     * Get addReunionAr.
     *
     * @return string
     */
    public function getAddReunionAr()
    {
        return $this->addReunionAr;
    }

    /**
     * Set descriptionDetailIt.
     *
     * @param string $descriptionDetailIt
     *
     * @return Lot
     */
    public function setDescriptionDetailIt($descriptionDetailIt)
    {
        $this->descriptionDetailIt = $descriptionDetailIt;

        return $this;
    }

    /**
     * Get descriptionDetailIt.
     *
     * @return string
     */
    public function getDescriptionDetailIt()
    {
        return $this->descriptionDetailIt;
    }

    /**
     * Set descriptionIt.
     *
     * @param string $descriptionIt
     *
     * @return Lot
     */
    public function setDescriptionIt($descriptionIt)
    {
        $this->descriptionIt = $descriptionIt;

        return $this;
    }

    /**
     * Get descriptionIt.
     *
     * @return string
     */
    public function getDescriptionIt()
    {
        return $this->descriptionIt;
    }

    /**
     * Set addEchantillionIt.
     *
     * @param string $addEchantillionIt
     *
     * @return Lot
     */
    public function setAddEchantillionIt($addEchantillionIt)
    {
        $this->addEchantillionIt = $addEchantillionIt;

        return $this;
    }

    /**
     * Get addEchantillionIt.
     *
     * @return string
     */
    public function getAddEchantillionIt()
    {
        return $this->addEchantillionIt;
    }

    /**
     * Set addReunionIt.
     *
     * @param string $addReunionIt
     *
     * @return Lot
     */
    public function setAddReunionIt($addReunionIt)
    {
        $this->addReunionIt = $addReunionIt;

        return $this;
    }

    /**
     * Get addReunionIt.
     *
     * @return string
     */
    public function getAddReunionIt()
    {
        return $this->addReunionIt;
    }

    /**
     * Set clauseSociale.
     *
     * @param string $clauseSociale
     *
     * @return Lot
     */
    public function setClauseSociale($clauseSociale)
    {
        $this->clauseSociale = $clauseSociale;

        return $this;
    }

    /**
     * Get clauseSociale.
     *
     * @return string
     */
    public function getClauseSociale()
    {
        return $this->clauseSociale;
    }

    /**
     * Set clauseEnvironnementale.
     *
     * @param string $clauseEnvironnementale
     *
     * @return Lot
     */
    public function setClauseEnvironnementale($clauseEnvironnementale)
    {
        $this->clauseEnvironnementale = $clauseEnvironnementale;

        return $this;
    }

    /**
     * Get clauseEnvironnementale.
     *
     * @return string
     */
    public function getClauseEnvironnementale()
    {
        return $this->clauseEnvironnementale;
    }

    /**
     * Set decision.
     *
     * @param string $decision
     *
     * @return Lot
     */
    public function setDecision($decision)
    {
        $this->decision = $decision;

        return $this;
    }

    /**
     * Get decision.
     *
     * @return string
     */
    public function getDecision()
    {
        return $this->decision;
    }

    /**
     * Set clauseSocialeConditionExecution.
     *
     * @param string $clauseSocialeConditionExecution
     *
     * @return Lot
     */
    public function setClauseSocialeConditionExecution($clauseSocialeConditionExecution)
    {
        $this->clauseSocialeConditionExecution = $clauseSocialeConditionExecution;

        return $this;
    }

    /**
     * Get clauseSocialeConditionExecution.
     *
     * @return string
     */
    public function getClauseSocialeConditionExecution()
    {
        return $this->clauseSocialeConditionExecution;
    }

    /**
     * Set clauseSocialeInsertion.
     *
     * @param string $clauseSocialeInsertion
     *
     * @return Lot
     */
    public function setClauseSocialeInsertion($clauseSocialeInsertion)
    {
        $this->clauseSocialeInsertion = $clauseSocialeInsertion;

        return $this;
    }

    /**
     * Get clauseSocialeInsertion.
     *
     * @return string
     */
    public function getClauseSocialeInsertion()
    {
        return $this->clauseSocialeInsertion;
    }

    /**
     * Set clauseSocialeAteliersProteges.
     *
     * @param string $clauseSocialeAteliersProteges
     *
     * @return Lot
     */
    public function setClauseSocialeAteliersProteges($clauseSocialeAteliersProteges)
    {
        $this->clauseSocialeAteliersProteges = $clauseSocialeAteliersProteges;

        return $this;
    }

    /**
     * Get clauseSocialeAteliersProteges.
     *
     * @return string
     */
    public function getClauseSocialeAteliersProteges()
    {
        return $this->clauseSocialeAteliersProteges;
    }

    /**
     * Set clauseSocialeSiae.
     *
     * @param string $clauseSocialeSiae
     *
     * @return Lot
     */
    public function setClauseSocialeSiae($clauseSocialeSiae)
    {
        $this->clauseSocialeSiae = $clauseSocialeSiae;

        return $this;
    }

    /**
     * Get clauseSocialeSiae.
     *
     * @return string
     */
    public function getClauseSocialeSiae()
    {
        return $this->clauseSocialeSiae;
    }

    /**
     * Set clauseSocialeEss.
     *
     * @param string $clauseSocialeEss
     *
     * @return Lot
     */
    public function setClauseSocialeEss($clauseSocialeEss)
    {
        $this->clauseSocialeEss = $clauseSocialeEss;

        return $this;
    }

    /**
     * Get clauseSocialeEss.
     *
     * @return string
     */
    public function getClauseSocialeEss()
    {
        return $this->clauseSocialeEss;
    }

    /**
     * Set clauseEnvSpecsTechniques.
     *
     * @param string $clauseEnvSpecsTechniques
     *
     * @return Lot
     */
    public function setClauseEnvSpecsTechniques($clauseEnvSpecsTechniques)
    {
        $this->clauseEnvSpecsTechniques = $clauseEnvSpecsTechniques;

        return $this;
    }

    /**
     * Get clauseEnvSpecsTechniques.
     *
     * @return string
     */
    public function getClauseEnvSpecsTechniques()
    {
        return $this->clauseEnvSpecsTechniques;
    }

    /**
     * Set clauseEnvCondExecution.
     *
     * @param string $clauseEnvCondExecution
     *
     * @return Lot
     */
    public function setClauseEnvCondExecution($clauseEnvCondExecution)
    {
        $this->clauseEnvCondExecution = $clauseEnvCondExecution;

        return $this;
    }

    /**
     * Get clauseEnvCondExecution.
     *
     * @return string
     */
    public function getClauseEnvCondExecution()
    {
        return $this->clauseEnvCondExecution;
    }

    /**
     * Set clauseEnvCriteresSelect.
     *
     * @param string $clauseEnvCriteresSelect
     *
     * @return Lot
     */
    public function setClauseEnvCriteresSelect($clauseEnvCriteresSelect)
    {
        $this->clauseEnvCriteresSelect = $clauseEnvCriteresSelect;

        return $this;
    }

    /**
     * Get clauseEnvCriteresSelect.
     *
     * @return string
     */
    public function getClauseEnvCriteresSelect()
    {
        return $this->clauseEnvCriteresSelect;
    }

    /**
     * Set idDonneeComplementaire.
     *
     * @param int $idDonneeComplementaire
     *
     * @return Lot
     */
    public function setIdDonneeComplementaire($idDonneeComplementaire)
    {
        $this->idDonneeComplementaire = $idDonneeComplementaire;

        return $this;
    }

    /**
     * Get idDonneeComplementaire.
     *
     * @return int
     */
    public function getIdDonneeComplementaire()
    {
        return $this->idDonneeComplementaire;
    }

    public function setConsultation(Consultation $consultation)
    {
        $this->consultation = $consultation;
    }

    /**
     * @return Consultation
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    /**
     * Set marcheInsertion.
     *
     * @param bool $marcheInsertion
     *
     * @return Lot
     */
    public function setMarcheInsertion($marcheInsertion)
    {
        $this->marcheInsertion = $marcheInsertion;

        return $this;
    }

    /**
     * Get marcheInsertion.
     *
     * @return bool
     */
    public function getMarcheInsertion()
    {
        return $this->marcheInsertion;
    }

    /**
     * Set clauseSpecificationTechnique.
     *
     * @param string $clauseSpecificationTechnique
     *
     * @return Lot
     */
    public function setClauseSpecificationTechnique($clauseSpecificationTechnique)
    {
        $this->clauseSpecificationTechnique = $clauseSpecificationTechnique;

        return $this;
    }

    /**
     * Get clauseSpecificationTechnique.
     *
     * @return string
     */
    public function getClauseSpecificationTechnique()
    {
        return $this->clauseSpecificationTechnique;
    }

    /**
     * @return false|string[]
     */
    public function getArrayCpvSecondaire(): false|array
    {
        return explode('#', ltrim($this->codeCpv2, '#'));
    }

    /**
     * Permet de retourner si le lot à une clause donnée.
     *
     * @param $clauseName
     */
    public function hasThisClause($clauseName)
    {
        $getClause = 'get' . ucfirst($clauseName);
        if (self::CLAUSE_ACTIVE == $this->$getClause()) {
            return true;
        }

        return false;
    }

    /**
     * @return DonneeComplementaire
     */
    public function getDonneeComplementaire(): ?DonneeComplementaire
    {
        return $this->donneeComplementaire;
    }

    public function setDonneeComplementaire(?DonneeComplementaire $donneeComplementaire = null): Lot
    {
        $this->donneeComplementaire = $donneeComplementaire;

        return $this;
    }

    public function getPieceGeneree(): ?PieceGenereConsultation
    {
        return $this->pieceGeneree;
    }

    public function setPieceGeneree(?PieceGenereConsultation $pieceGenereConsultation): self
    {
        $this->pieceGeneree = $pieceGenereConsultation;

        return $this;
    }

    /**
     * @return Collection|CritereAttribution[]
     */
    public function getCritereAttribution(): Collection
    {
        return $this->critereAttribution;
    }

    public function addCritereAttribution(CritereAttribution $critereAttribution): self
    {
        if (!$this->critereAttribution->contains($critereAttribution)) {
            $this->critereAttribution[] = $critereAttribution;
            $critereAttribution->setLot($this);
        }

        return $this;
    }

    public function removeCritereAttribution(CritereAttribution $critereAttribution): self
    {
        if ($this->critereAttribution->removeElement($critereAttribution)) {
            // set the owning side to null (unless already changed)
            if ($critereAttribution->getLot() === $this) {
                $critereAttribution->setLot(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ClausesN1>
     */
    public function getClausesN1(): Collection
    {
        return $this->clausesN1;
    }

    public function addClausesN1(ClausesN1 $clausesN1): self
    {
        if (!$this->clausesN1->contains($clausesN1)) {
            $this->clausesN1[] = $clausesN1;
            $clausesN1->setLot($this);
        }

        return $this;
    }

    public function removeClausesN1(ClausesN1 $clausesN1): self
    {
        if ($this->clausesN1->removeElement($clausesN1)) {
            // set the owning side to null (unless already changed)
            if ($clausesN1->getLot() === $this) {
                $clausesN1->setLot(null);
            }
        }

        return $this;
    }
}
