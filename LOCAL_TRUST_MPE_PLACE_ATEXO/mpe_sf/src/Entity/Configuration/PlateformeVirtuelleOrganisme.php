<?php

namespace App\Entity\Configuration;

use App\Entity\Organisme;
use Doctrine\ORM\Mapping as ORM;

/**
 * PlateformeVirtuelleOrganisme.
 *
 * @ORM\Table(name="plateforme_virtuelle_organisme",
 *     indexes={@ORM\Index(name="plateforme_id_fk", columns={"plateforme_id"}),
 *     @ORM\Index(name="organisme_acronyme_fk", columns={"organisme_acronyme"})})
 * @ORM\Entity(repositoryClass="App\Repository\Configuration\PlateformeVirtuelleOrganismeRepository")
 */
class PlateformeVirtuelleOrganisme
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme", inversedBy="plateformeVirtuelleOrganisme")
     * @ORM\JoinColumn(name="organisme_acronyme", referencedColumnName="acronyme")
     */
    private Organisme $organisme;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Configuration\PlateformeVirtuelle", inversedBy="plateformeVirtuelleOrganisme")
     * @ORM\JoinColumn(name="plateforme_id", referencedColumnName="id")
     */
    private ?PlateformeVirtuelle $plateforme = null;

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setId($id): PlateformeVirtuelleOrganisme
    {
        $this->id = $id;

        return $this;
    }

    public function getOrganisme(): Organisme
    {
        return $this->organisme;
    }

    public function setOrganisme(Organisme $organisme): PlateformeVirtuelleOrganisme
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getPlateforme(): PlateformeVirtuelle
    {
        return $this->plateforme;
    }

    public function setPlateforme(PlateformeVirtuelle $plateforme): PlateformeVirtuelleOrganisme
    {
        $this->plateforme = $plateforme;

        return $this;
    }
}
