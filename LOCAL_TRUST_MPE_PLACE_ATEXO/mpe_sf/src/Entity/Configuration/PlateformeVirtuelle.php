<?php

namespace App\Entity\Configuration;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * PlateformeVirtuelle.
 *
 * @ORM\Table(name="plateforme_virtuelle")
 * @ORM\Entity(repositoryClass="App\Repository\Configuration\PlateformeVirtuelleRepository")
 */
class PlateformeVirtuelle
{
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Configuration\PlateformeVirtuelleOrganisme", mappedBy="plateforme")
     */
    private Collection $plateformeVirtuelleOrganisme;

    /**
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="domain", type="string", length=256, nullable=false)
     */
    private ?string $domain = null;

    /**
     * @ORM\Column(name="name", type="string", length=256, nullable=false)
     */
    private ?string $name = null;

    /**
     * @ORM\Column(name="code_design", type="string", length=25, nullable=true)
     */
    private ?string $codeDesign = null;

    /**
     * @ORM\Column(name="protocole", type="string", length=5)
     */
    private string $protocole = 'https';

    /**
     * @ORM\Column(name="no_reply", type="string", length=255, nullable=true)
     */
    private ?string $noReply = null;

    /**
     * @ORM\Column(name="footer_mail", type="text", nullable=true)
     */
    private ?string $footerMail = null;

    /**
     * @ORM\Column(name="from_pf_name", type="string", length=255, nullable=true)
     */
    private ?string $fromPfName = null;

    public function __construct()
    {
        $this->plateformeVirtuelleOrganisme = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): PlateformeVirtuelle
    {
        $this->id = $id;

        return $this;
    }

    public function getDomain(): string
    {
        return $this->domain;
    }

    public function setDomain(string $domain): PlateformeVirtuelle
    {
        $this->domain = $domain;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): PlateformeVirtuelle
    {
        $this->name = $name;

        return $this;
    }

    public function getCodeDesign(): ?string
    {
        return $this->codeDesign;
    }

    public function setCodeDesign(?string $codeDesign): PlateformeVirtuelle
    {
        $this->codeDesign = $codeDesign;

        return $this;
    }

    public function getPlateformeVirtuelleOrganisme(): ArrayCollection
    {
        return $this->plateformeVirtuelleOrganisme;
    }

    public function setPlateformeVirtuelleOrganisme(ArrayCollection $plateformeVirtuelleOrganisme): PlateformeVirtuelle
    {
        $this->plateformeVirtuelleOrganisme = $plateformeVirtuelleOrganisme;

        return $this;
    }

    public function getProtocole(): string
    {
        return $this->protocole;
    }

    public function setProtocole(string $protocole): PlateformeVirtuelle
    {
        $this->protocole = $protocole;

        return $this;
    }

    public function getNoReply(): ?string
    {
        return $this->noReply;
    }

    public function setNoReply(?string $noReply): PlateformeVirtuelle
    {
        $this->noReply = $noReply;

        return $this;
    }

    public function getFooterMail(): ?string
    {
        return $this->footerMail;
    }

    public function setFooterMail(?string $footerMail): PlateformeVirtuelle
    {
        $this->footerMail = $footerMail;

        return $this;
    }

    public function getFromPfName(): ?string
    {
        return $this->fromPfName;
    }

    public function setFromPfName(?string $fromPfName): PlateformeVirtuelle
    {
        $this->fromPfName = $fromPfName;

        return $this;
    }
}
