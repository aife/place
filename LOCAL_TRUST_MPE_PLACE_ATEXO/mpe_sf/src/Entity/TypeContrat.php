<?php

namespace App\Entity;

use Stringable;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\Output\TypeContratOutput;
use App\Repository\TypeContratRepository;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class TypeContrat.
 *
 * @ORM\Table(name="t_type_contrat")
 * @ORM\Entity(repositoryClass=TypeContratRepository::class)
 */
#[ORM\Table(name: 't_type_contrat')]
#[ORM\Entity(repositoryClass: TypeContratRepository::class)]
#[ApiResource(
    collectionOperations: ['get'],
    itemOperations: ['get'],
    shortName: 'Contrat',
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: TypeContratOutput::class,
    routePrefix: '/referentiels'
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'idExterne'             => 'exact',
        'libelleTypeContrat'    => 'exact'
    ]
)]
class TypeContrat implements Stringable
{
    /**
     *
     * @ORM\Column(name="id_type_contrat", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $idTypeContrat = null;

    /**
     * @ORM\Column(name="libelle_type_contrat", type="string", length=255, nullable=false)
     */
    private ?string $libelleTypeContrat = null;

    /**
     * @ORM\Column(name="abreviation_type_contrat", type="string", length=2, nullable=true)
     */
    private ?string $abreviationTypeContrat = null;

    /**
     * @ORM\Column(name="type_contrat_statistique", type="integer", length=6, nullable=false)
     */
    private string|int $typeContratStatistique = '0';

    /**
     * @ORM\Column(name="multi", type="string", length=50, nullable=false)
     */
    private string $multi = '0';

    /**
     * @ORM\Column(name="accord_cadre_sad", type="string", length=50, nullable=false)
     */
    private string $accordCadreSad = '0';

    /**
     * @ORM\Column(name="avec_chapeau", type="string", length=50, nullable=false)
     */
    private string $avecChapeau = '0';

    /**
     * @ORM\Column(name="avec_montant", type="string", length=50, nullable=false)
     */
    private string $avecMontant = '0';

    /**
     * @ORM\Column(name="mode_echange_chorus", type="string", length=50, nullable=false)
     */
    private string $modeEchangeChorus = '0';

    /**
     * @ORM\Column(name="marche_subsequent", type="string", length=50, nullable=false)
     */
    private string $marcheSubsequent = '0';

    /**
     * @ORM\Column(name="avec_montant_max", type="string", length=50, nullable=false)
     */
    private string $avecMontantMax = '0';

    /**
     * @ORM\Column(name="ordre_affichage", type="integer", nullable=true)
     */
    private ?int $ordreAffichage = null;

    /**
     * @ORM\Column(name="article_133", type="string", length=50, nullable=false)
     */
    private string $article133 = '0';

    /**
     * @ORM\Column(name="code_dume", type="string", length=50, nullable=true)
     */
    private ?string $codeDume = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="concession", type="boolean", options={"default":"0"})
     */
    private $concession;

    private $procedureCodes = [];

    public function __toString(): string
    {
        return $this->libelleTypeContrat;
    }

    /**
     * @return int
     */
    public function getIdTypeContrat()
    {
        return $this->idTypeContrat;
    }

    /**
     * @return string
     */
    public function getLibelleTypeContrat()
    {
        return $this->libelleTypeContrat;
    }

    /**
     * @ORM\Column(name="id_externe", type="string", length=255, nullable=false)
     */
    private string $idExterne = '';

    /**
     * @ORM\ManyToOne(targetEntity=TechniqueAchat::class, inversedBy="typeContrats")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id_technique_achat", name="technique_achat")
     */
    private TechniqueAchat $techniqueAchat;

    public function setLibelleTypeContrat(string $libelleTypeContrat)
    {
        $this->libelleTypeContrat = $libelleTypeContrat;
    }

    /**
     * @return string
     */
    public function getAbreviationTypeContrat()
    {
        return $this->abreviationTypeContrat;
    }

    public function setAbreviationTypeContrat(string $abreviationTypeContrat)
    {
        $this->abreviationTypeContrat = $abreviationTypeContrat;
    }

    /**
     * @return int
     */
    public function getTypeContratStatistique()
    {
        return $this->typeContratStatistique;
    }

    public function setTypeContratStatistique(int $typeContratStatistique)
    {
        $this->typeContratStatistique = $typeContratStatistique;
    }

    /**
     * @return string
     */
    public function getMulti()
    {
        return $this->multi;
    }

    public function setMulti(string $multi)
    {
        $this->multi = $multi;
    }

    /**
     * @return string
     */
    public function getAccordCadreSad()
    {
        return $this->accordCadreSad;
    }

    public function setAccordCadreSad(string $accordCadreSad)
    {
        $this->accordCadreSad = $accordCadreSad;
    }

    /**
     * @return string
     */
    public function getAvecChapeau()
    {
        return $this->avecChapeau;
    }

    public function setAvecChapeau(string $avecChapeau)
    {
        $this->avecChapeau = $avecChapeau;
    }

    /**
     * @return string
     */
    public function getAvecMontant()
    {
        return $this->avecMontant;
    }

    public function setAvecMontant(string $avecMontant)
    {
        $this->avecMontant = $avecMontant;

        return $this;
    }

    /**
     * @return string
     */
    public function getModeEchangeChorus()
    {
        return $this->modeEchangeChorus;
    }

    public function setModeEchangeChorus(string $modeEchangeChorus)
    {
        $this->modeEchangeChorus = $modeEchangeChorus;
    }

    /**
     * @return string
     */
    public function getMarcheSubsequent()
    {
        return $this->marcheSubsequent;
    }

    public function setMarcheSubsequent(string $marcheSubsequent)
    {
        $this->marcheSubsequent = $marcheSubsequent;
    }

    /**
     * @return string
     */
    public function getAvecMontantMax()
    {
        return $this->avecMontantMax;
    }

    public function setAvecMontantMax(string $avecMontantMax)
    {
        $this->avecMontantMax = $avecMontantMax;
    }

    /**
     * @return int
     */
    public function getOrdreAffichage()
    {
        return $this->ordreAffichage;
    }

    public function setOrdreAffichage(int $ordreAffichage)
    {
        $this->ordreAffichage = $ordreAffichage;
    }

    /**
     * @return string
     */
    public function getArticle133()
    {
        return $this->article133;
    }

    public function setArticle133(string $article133)
    {
        $this->article133 = $article133;
    }

    /**
     * @return string
     */
    public function getCodeDume()
    {
        return $this->codeDume;
    }

    public function setCodeDume(string $codeDume)
    {
        $this->codeDume = $codeDume;
    }

    public function isConcession(): bool
    {
        return $this->concession;
    }

    public function setConcession(bool $concession)
    {
        $this->concession = $concession;
    }

    /**
     * @return string
     */
    public function getIdExterne()
    {
        return $this->idExterne;
    }

    /**
     * @param string $idExterne
     */
    public function setIdExterne($idExterne)
    {
        $this->idExterne = $idExterne;
    }

    /**
     * @return array
     */
    public function getProcedureCodes()
    {
        return $this->procedureCodes;
    }

    /**
     * @param array $procedureCodes
     */
    public function setProcedureCodes($procedureCodes)
    {
        $this->procedureCodes = $procedureCodes;
    }

    /**
     * @return string
     */
    #[Groups('webservice')]
    public function getCodeExterne()
    {
        return $this->idExterne;
    }

    /**
     * @return string
     */
    #[Groups('webservice')]
    public function getLibelle()
    {
        return $this->libelleTypeContrat;
    }

    /**
     * @return string
     */
    #[Groups('webservice')]
    public function getAbreviation()
    {
        return $this->abreviationTypeContrat;
    }

    public function getTechniqueAchat(): ?TechniqueAchat
    {
        return $this->techniqueAchat;
    }

    public function setTechniqueAchat(?TechniqueAchat $techniqueAchat): self
    {
        $this->techniqueAchat = $techniqueAchat;

        return $this;
    }
}
