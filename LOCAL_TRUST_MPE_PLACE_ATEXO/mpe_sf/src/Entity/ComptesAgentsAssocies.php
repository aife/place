<?php

namespace App\Entity;

use App\Repository\ComptesAgentsAssociesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="comptes_agents_associes")
 * @ORM\Entity(repositoryClass=ComptesAgentsAssociesRepository::class)
 */
class ComptesAgentsAssocies
{
    /**
     * @ORM\Column(name="compte_principal", type="integer", unique=true)
     * @ORM\Id
     */
    private ?int $comptePrincipal = null;

    /**
     * @ORM\Column(name="compte_secondaire", type="integer", unique=true)
     * @ORM\Id
     */
    private ?int $compteSecondaire = null;

    /**
     * @ORM\Column(name="statut_activation_compte_secondaire", type="boolean")
     */
    private $statutActivationCompteSecondaire;

    /**
     * @ORM\Column(name="UID_ECHANGE", type="string", length=32, nullable=true)
     */
    private ?string $uidEchange = null;

    public function getComptePrincipal(): int
    {
        return $this->comptePrincipal;
    }

    public function setComptePrincipal(int $comptePrincipal): self
    {
        $this->comptePrincipal = $comptePrincipal;

        return $this;
    }

    public function getCompteSecondaire(): int
    {
        return $this->compteSecondaire;
    }

    public function setCompteSecondaire(int $compteSecondaire): self
    {
        $this->compteSecondaire = $compteSecondaire;

        return $this;
    }

    public function getStatutActivationCompteSecondaire(): ?bool
    {
        return $this->statutActivationCompteSecondaire;
    }

    public function setStatutActivationCompteSecondaire(bool $statutActivationCompteSecondaire): self
    {
        $this->statutActivationCompteSecondaire = $statutActivationCompteSecondaire;

        return $this;
    }

    public function getUidEchange(): ?string
    {
        return $this->uidEchange;
    }

    public function setUidEchange(?string $uidEchange): self
    {
        $this->uidEchange = $uidEchange;

        return $this;
    }
}
