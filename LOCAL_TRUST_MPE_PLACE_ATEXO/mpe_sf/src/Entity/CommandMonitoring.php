<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="command_monitoring")
 * @ORM\Entity(repositoryClass="App\Repository\CommandMonitoringRepository")
 */
class CommandMonitoring
{
    /**
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(name="name", type="string")
     */
    private string $name;

    /**
     * @ORM\Column(name="params", type="string")
     */
    private ?string $params = null;

    /**
     * @ORM\Column(name="start_date", type="datetime", nullable=false)
     */
    private DateTime $startDate;

    /**
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private ?DateTime $endDate = null;

    /**
     * @ORM\Column(name="status", type="string", length=15)
     */
    private string $status;

    /**
     * @ORM\Column(name="logs", type="string", nullable=true)
     */
    private ?string $logs = null;

    /**
     * @ORM\Column(name="command_output", type="string", nullable=true)
     */
    private ?string $commandOutput = null;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getParams(): ?string
    {
        return $this->params;
    }

    public function setParams(?string $params): self
    {
        $this->params = $params;

        return $this;
    }

    public function getStartDate(): DateTime
    {
        return $this->startDate;
    }

    public function setStartDate(DateTime $date): self
    {
        $this->startDate = $date;

        return $this;
    }

    public function getEndDate(): ?DateTime
    {
        return $this->endDate;
    }

    public function setEndDate(?DateTime $date): self
    {
        $this->endDate = $date;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getLogs(): ?string
    {
        return $this->logs;
    }

    public function setLogs(?string $logs): self
    {
        $this->logs = $logs;

        return $this;
    }

    public function getCommandOutput(): ?string
    {
        return $this->commandOutput;
    }

    public function setCommandOutput(?string $commandOutput): self
    {
        $this->commandOutput = $commandOutput;

        return $this;
    }
}
