<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * ConfigurationClient.
 *
 * @ORM\Table(name="configuration_client")
 * @ORM\Entity(repositoryClass="App\Repository\ConfigurationClientRepository")
 */
class ConfigurationClient
{
    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="parameter", type="string", length=300, unique=true)
     */
    private ?string $parameter = null;

    /**
     * @ORM\Column(name="value", type="text")
     */
    private ?string $value = null;

    /**
     * @var DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)*/
    private $updatedAt;

    public function __construct()
    {
        $this->setUpdatedAt(new DateTime());
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parameter.
     *
     * @param string $parameter
     *
     * @return ConfigurationClient
     */
    public function setParameter($parameter)
    {
        $this->parameter = $parameter;

        return $this;
    }

    /**
     * Get parameter.
     *
     * @return string
     */
    public function getParameter()
    {
        return $this->parameter;
    }

    /**
     * Set value.
     *
     * @param string $value
     *
     * @return ConfigurationClient
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTime $updatedAt): ConfigurationClient
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
