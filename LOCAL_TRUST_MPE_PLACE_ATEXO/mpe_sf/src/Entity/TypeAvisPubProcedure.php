<?php

namespace App\Entity;

use App\Repository\TypeAvisPubProcedureRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeAvisPubProcedureRepository::class)
 * @ORM\Table(name="Type_Avis_Pub_Procedure")
 */
class TypeAvisPubProcedure
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity=TypeAvisPub::class)
     * @ORM\JoinColumn(nullable=false, name="id_type_avis", referencedColumnName="id")
     */
    private $typeAvis;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=30)
     */
    private $organisme;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity=TypeProcedure::class)
     * @ORM\JoinColumn(nullable=false, name="id_type_procedure", referencedColumnName="id_type_procedure")
     */
    private $typeProcedure;

    public function getTypeAvis(): ?TypeAvisPub
    {
        return $this->typeAvis;
    }

    public function setTypeAvis(?TypeAvisPub $typeAvis): self
    {
        $this->typeAvis = $typeAvis;

        return $this;
    }

    public function getOrganisme(): ?string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme): self
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getTypeProcedure(): ?TypeProcedure
    {
        return $this->typeProcedure;
    }

    public function setTypeProcedure(?TypeProcedure $typeProcedure): self
    {
        $this->typeProcedure = $typeProcedure;

        return $this;
    }
}
