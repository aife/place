<?php

namespace App\Entity\Autoformation;

use App\Entity\Langue;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Autoformation\LangueModuleAutoformationRepository")
 */
class LangueModuleAutoformation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Langue", inversedBy="langueModuleAutoformations")
     * @ORM\JoinColumn(name="langue_id", referencedColumnName="id_langue")
     */
    private $langue;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Autoformation\ModuleAutoformation", inversedBy="langueModuleAutoformations")
     */
    private $moduleAutoformation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLangue(): ?Langue
    {
        return $this->langue;
    }

    public function setLangue(?Langue $langue): self
    {
        $this->langue = $langue;

        return $this;
    }

    public function getModuleAutoformation(): ?ModuleAutoformation
    {
        return $this->moduleAutoformation;
    }

    public function setModuleAutoformation(?ModuleAutoformation $moduleAutoformation): self
    {
        $this->moduleAutoformation = $moduleAutoformation;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
