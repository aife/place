<?php

namespace App\Entity\Autoformation;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Autoformation\ModuleAutoformationRepository")
 */
class ModuleAutoformation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Gedmo\SortableGroup
     * @ORM\ManyToOne(targetEntity="App\Entity\Autoformation\Rubrique", inversedBy="moduleAutoformations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rubrique;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(type="integer")
     */
    private $position;

    /**
     * @ORM\Column(type="integer")
     */
    private $duree;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pathPicture;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pathVideo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $urlExterne;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Autoformation\LangueModuleAutoformation", mappedBy="moduleAutoformation", cascade={"persist", "remove"})
     */
    private $langueModuleAutoformations;

    public function __construct()
    {
        $this->langueModuleAutoformations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRubrique(): ?Rubrique
    {
        return $this->rubrique;
    }

    public function setRubrique(?Rubrique $rubrique): self
    {
        $this->rubrique = $rubrique;

        return $this;
    }

    public function getDuree(): ?int
    {
        return $this->duree;
    }

    public function setDuree(int $duree): self
    {
        $this->duree = $duree;

        return $this;
    }

    public function getPathPicture(): ?string
    {
        return $this->pathPicture;
    }

    public function setPathPicture(string $pathPicture): self
    {
        $this->pathPicture = $pathPicture;

        return $this;
    }

    public function getPathVideo(): ?string
    {
        return $this->pathVideo;
    }

    public function setPathVideo(string $pathVideo): self
    {
        $this->pathVideo = $pathVideo;

        return $this;
    }

    public function getUrlExterne(): ?string
    {
        return $this->urlExterne;
    }

    public function setUrlExterne(string $urlExterne): self
    {
        $this->urlExterne = $urlExterne;

        return $this;
    }

    public function getPosition(): ?string
    {
        return $this->position;
    }


    public function setPosition(string $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return Collection|LangueModuleAutoformation[]
     */
    public function getLangueModuleAutoformations(): Collection
    {
        return $this->langueModuleAutoformations;
    }

    public function addLangueModuleAutoformation(LangueModuleAutoformation $langueModuleAutoformation): self
    {
        if (!$this->langueModuleAutoformations->contains($langueModuleAutoformation)) {
            $this->langueModuleAutoformations[] = $langueModuleAutoformation;
            $langueModuleAutoformation->setModuleAutoformation($this);
        }

        return $this;
    }

    public function removeLangueModuleAutoformation(LangueModuleAutoformation $langueModuleAutoformation): self
    {
        if ($this->langueModuleAutoformations->removeElement($langueModuleAutoformation)) {
            // set the owning side to null (unless already changed)
            if ($langueModuleAutoformation->getModuleAutoformation() === $this) {
                $langueModuleAutoformation->setModuleAutoformation(null);
            }
        }

        return $this;
    }
}
