<?php

namespace App\Entity\Autoformation;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Autoformation\RubriqueRepository")
 */
class Rubrique
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAgent = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Autoformation\LangueRubrique", mappedBy="rubrique", cascade={"persist", "remove"})
     */
    private $langueRubriques;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Autoformation\ModuleAutoformation", mappedBy="rubrique", cascade={"persist", "remove"})
     */
    private $moduleAutoformations;

    public function __construct()
    {
        $this->langueRubriques = new ArrayCollection();
        $this->moduleAutoformations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|LangueRubrique[]
     */
    public function getLangueRubriques(): Collection
    {
        return $this->langueRubriques;
    }

    public function addLangueRubrique(LangueRubrique $langueRubrique): self
    {
        if (!$this->langueRubriques->contains($langueRubrique)) {
            $this->langueRubriques[] = $langueRubrique;
            $langueRubrique->setRubrique($this);
        }

        return $this;
    }

    public function removeLangueRubrique(LangueRubrique $langueRubrique): self
    {
        if ($this->langueRubriques->removeElement($langueRubrique)) {
            // set the owning side to null (unless already changed)
            if ($langueRubrique->getRubrique() === $this) {
                $langueRubrique->setRubrique(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ModuleAutoformation[]
     */
    public function getModuleAutoformations(): Collection
    {
        return $this->moduleAutoformations;
    }

    public function addModuleAutoformation(ModuleAutoformation $moduleAutoformation): self
    {
        if (!$this->moduleAutoformations->contains($moduleAutoformation)) {
            $this->moduleAutoformations[] = $moduleAutoformation;
            $moduleAutoformation->setRubrique($this);
        }

        return $this;
    }

    public function removeModuleAutoformation(ModuleAutoformation $moduleAutoformation): self
    {
        if ($this->moduleAutoformations->removeElement($moduleAutoformation)) {
            // set the owning side to null (unless already changed)
            if ($moduleAutoformation->getRubrique() === $this) {
                $moduleAutoformation->setRubrique(null);
            }
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isAgent(): bool
    {
        return $this->isAgent;
    }

    /**
     * @param bool $rgpdEnquete
     * @return self
     */
    public function setIsAgent(bool $isAgent): self
    {
        $this->isAgent = $isAgent;

        return $this;
    }
}
