<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\CreateBloborganismeFileAction;
use App\Controller\DownloadBloborganismeFileAction;
use App\Dto\Output\MediaOutput;
use App\Repository\MediaUuidRepository;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;

/**
 * @ORM\Entity(repositoryClass=MediaUuidRepository::class)
 */
#[ApiResource(
    collectionOperations: [
        'post' => [
            'controller' => CreateBloborganismeFileAction::class,
            'deserialize' => false,
            'validation_groups' => ['Default', 'media_object_create'],
            'openapi_context' => [
                'requestBody' => [
                    'content' => [
                        'multipart/form-data' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'file' => [
                                        'type' => 'string',
                                        'format' => 'binary',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    iri: 'http://schema.org/MediaObject',
    itemOperations: [
        'get' => ["security" => "is_granted('MEDIA_VIEW', object)"],
        'get_download' => [
            'method' => 'GET',
            'path' => '/media-objects/{uuid}/download',
            "security" => "is_granted('MEDIA_VIEW', object)",
            'controller' => DownloadBloborganismeFileAction::class,
            'openapi_context' => [
                'responses' => [
                    '200' => [
                        'description' => 'Binary File Response'
                    ],
                    '404' => [
                        'description' => 'Resource not found'
                    ],
                ]
            ]
        ]
    ],
    shortName: 'media-objects',
    normalizationContext: [ AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: MediaOutput::class
)]
class MediaUuid
{
    public const EXPIRED_DOWNLOAD_TIME = 30 * 60;

    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    private string $uuid;

    /**
     * @ORM\ManyToOne(targetEntity=BloborganismeFile::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private BloborganismeFile $media;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTimeInterface $createdAt;

    public function getUuid()
    {
        return $this->uuid;
    }

    public function getMedia(): BloborganismeFile
    {
        return $this->media;
    }

    public function setMedia(BloborganismeFile $media): self
    {
        $this->media = $media;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
