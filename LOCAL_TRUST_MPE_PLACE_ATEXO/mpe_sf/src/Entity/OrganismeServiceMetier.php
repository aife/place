<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class OrganismeServiceMetier.
 *
 * @ORM\Table(name="Organisme_Service_Metier")
 * @ORM\Entity(repositoryClass="App\Repository\OrganismeServiceMetierRepository")
 */
class OrganismeServiceMetier
{
    /**
     *
     * @ORM\Column(name="id_auto", type="integer", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="organisme", type="string", nullable=true,length=30)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(name="id_service_metier", type="integer", nullable=false)
     */
    private ?int $idServiceMetier = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme): OrganismeServiceMetier
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getIdServiceMetier(): int
    {
        return $this->idServiceMetier;
    }

    public function setIdServiceMetier(int $idServiceMetier): OrganismeServiceMetier
    {
        $this->idServiceMetier = $idServiceMetier;

        return $this;
    }
}
