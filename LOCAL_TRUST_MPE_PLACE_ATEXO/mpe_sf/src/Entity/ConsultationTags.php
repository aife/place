<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\ORM\Mapping as ORM;

/**
 * ConsultationTags
 *
 * @ORM\Table(
 *     name="consultation_tags",
 *     indexes={@ORM\Index(name="consultation_id_tags_fk", columns={"consultation_id"})}
 *     )
 * @ORM\Entity(repositoryClass="App\Repository\ConsultationTagsRepository")
 */
#[ApiResource(
    denormalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'tagCode'           => 'exact',
        'consultation.id'   => 'exact'
    ]
)]

class ConsultationTags
{
    /**
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="tag_code", type="string", length=255, nullable=false)
     */
    private string $tagCode;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation", inversedBy="tags")
     * @ORM\JoinColumn(name="consultation_id", referencedColumnName="id")
     */
    private Consultation $consultation;

    public function getId(): int
    {
        return $this->id;
    }

    public function getTagCode(): ?string
    {
        return $this->tagCode;
    }

    public function setTagCode(string $tagCode): self
    {
        $this->tagCode = $tagCode;

        return $this;
    }

    public function getConsultation(): ?Consultation
    {
        return $this->consultation;
    }

    public function setConsultation(?Consultation $consultation): self
    {
        $this->consultation = $consultation;

        return $this;
    }
}
