<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Responsableengagement.
 *
 * @ORM\Table(name="responsableengagement")
 * @ORM\Entity(repositoryClass="App\Repository\ResponsableengagementRepository")
 */
class Responsableengagement
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Entreprise", inversedBy="responsableengagements",cascade={"persist"}))
     * @ORM\JoinColumn(name="entreprise_id", referencedColumnName="id")
     */
    private ?Entreprise $entreprise = null;

    /**
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="entreprise_id", type="integer", nullable=false)
     */
    private string|int $entrepriseId = '0';

    /**
     * @ORM\Column(name="nom", type="string", length=30, nullable=true)
     */
    private ?string $nom = null;

    /**
     * @ORM\Column(name="prenom", type="string", length=30, nullable=true)
     */
    private ?string $prenom = null;

    /**
     * @ORM\Column(name="qualite", type="string", length=32, nullable=true)
     */
    private ?string $qualite = null;

    /**
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private ?string $email = null;

    /**
     * @ORM\Column(name="telephone", type="string", length=100, nullable=true)
     */
    private ?string $telephone = null;

    /**
     * @ORM\Column(name="fax", type="string", length=100, nullable=true)
     */
    private ?string $fax = null;

    /**
     * @var int
     *
     * @ORM\Column(name="id_initial", type="integer", nullable=true)
     */
    private $idInitial = '0';

    /**
     * @return Entreprise
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }

    public function setEntreprise(Entreprise $entreprise)
    {
        $this->entreprise = $entreprise;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getEntrepriseId()
    {
        return $this->entrepriseId;
    }

    /**
     * @param int $entrepriseId
     */
    public function setEntrepriseId($entrepriseId)
    {
        $this->entrepriseId = $entrepriseId;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param string $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @return string
     */
    public function getQualite()
    {
        return $this->qualite;
    }

    /**
     * @param string $qualite
     */
    public function setQualite($qualite)
    {
        $this->qualite = $qualite;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param string $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return int
     */
    public function getIdInitial()
    {
        return $this->idInitial;
    }

    /**
     * @param int $idInitial
     */
    public function setIdInitial($idInitial)
    {
        $this->idInitial = $idInitial;
    }
}
