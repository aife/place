<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;

/**
 * CertificatChiffrement.
 *
 * @ORM\Table(name="CertificatChiffrement", indexes={@ORM\Index(name="consultation_id", columns={"consultation_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\CertificatChiffrementRepository")
 */

#[ApiResource(
    collectionOperations: ["get", "post"],
    itemOperations: ["get", "delete"]
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'consultation' => 'exact',
    ]
)]
class CertificatChiffrement
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation", inversedBy="certificatChiffrements")
     * @ORM\JoinColumn(name="consultation_id", referencedColumnName="id")
     */
    private ?Consultation $consultation = null;

    /**
     * @ORM\Column(name="consultation_id", type="integer")
     */
    private string|int $consultationId = '0';

    /**
     * @ORM\Column(name="type_env", type="integer")
     */
    private string|int $typeEnv = '0';

    /**
     * @ORM\Column(name="sous_pli", type="integer")
     */
    private string|int $sousPli = '0';

    /**
     * @ORM\Column(name="index_certificat", type="integer")
     */
    private string|int $indexCertificat = '1';

    /**
     * @ORM\Column(name="organisme", type="string", length=30)
     */
    private string $organisme = '';

    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="certificat", type="text", length=16777215, nullable=false)
     */
    private ?string $certificat = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set consultationId.
     *
     * @param int $consultationId
     *
     * @return CertificatChiffrement
     */
    public function setConsultationId($consultationId)
    {
        $this->consultationId = $consultationId;

        return $this;
    }

    /**
     * Get consultationId.
     *
     * @return int
     */
    public function getConsultationId()
    {
        return $this->consultationId;
    }

    /**
     * Set typeEnv.
     *
     * @param int $typeEnv
     *
     * @return CertificatChiffrement
     */
    public function setTypeEnv($typeEnv)
    {
        $this->typeEnv = $typeEnv;

        return $this;
    }

    /**
     * Get typeEnv.
     *
     * @return int
     */
    public function getTypeEnv()
    {
        return $this->typeEnv;
    }

    /**
     * Set sousPli.
     *
     * @param int $sousPli
     *
     * @return CertificatChiffrement
     */
    public function setSousPli($sousPli)
    {
        $this->sousPli = $sousPli;

        return $this;
    }

    /**
     * Get sousPli.
     *
     * @return int
     */
    public function getSousPli()
    {
        return $this->sousPli;
    }

    /**
     * Set indexCertificat.
     *
     * @param int $indexCertificat
     *
     * @return CertificatChiffrement
     */
    public function setIndexCertificat($indexCertificat)
    {
        $this->indexCertificat = $indexCertificat;

        return $this;
    }

    /**
     * Get indexCertificat.
     *
     * @return int
     */
    public function getIndexCertificat()
    {
        return $this->indexCertificat;
    }

    /**
     * Set organisme.
     *
     * @param string $organisme
     *
     * @return CertificatChiffrement
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * Get organisme.
     *
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * Set certificat.
     *
     * @param string $certificat
     *
     * @return CertificatChiffrement
     */
    public function setCertificat($certificat)
    {
        $this->certificat = $certificat;

        return $this;
    }

    /**
     * Get certificat.
     *
     * @return string
     */
    public function getCertificat()
    {
        return $this->certificat;
    }

    public function setConsultation(Consultation $consultation)
    {
        $this->consultation = $consultation;
    }

    /**
     * @return Consultation
     */
    public function getConsultation()
    {
        return $this->consultation;
    }
}
