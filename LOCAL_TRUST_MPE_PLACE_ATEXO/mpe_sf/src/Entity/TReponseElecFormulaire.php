<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TReponseElecFormulaire.
 *
 * @ORM\Table(name="t_reponse_elec_formulaire")
 * @ORM\Entity
 */
class TReponseElecFormulaire
{
    /**
     * Consultation constructor.
     */
    public function __construct()
    {
        $this->tdossierformulaires = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param string $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return int
     */
    public function getIdReponseElecFormulaire()
    {
        return $this->idReponseElecFormulaire;
    }

    /**
     * @param int $idReponseElecFormulaire
     */
    public function setIdReponseElecFormulaire($idReponseElecFormulaire)
    {
        $this->idReponseElecFormulaire = $idReponseElecFormulaire;
    }

    /**
     *
     * @ORM\Column(name="id_reponse_elec_formulaire", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $idReponseElecFormulaire = null;

    /**
     * @ORM\Column(name="organisme", type="string", length=30, nullable=false)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(name="consultation_id", type="integer", nullable=false)
     */
    private ?int $consultationId = null;

    /**
     * @ORM\Column(name="id_entreprise", type="integer", nullable=false)
     */
    private ?int $idEntreprise = null;

    /**
     * @ORM\Column(name="id_inscrit", type="integer", nullable=false)
     */
    private ?int $idInscrit = null;

    /**
     * @ORM\Column(name="statut_validation_globale", type="integer", nullable=false)
     */
    private ?int $statutValidationGlobale = null;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="TDossierFormulaire", mappedBy="tReponseElecFormulaire")
     * @ORM\JoinColumn(name="id_reponse_elec_formulaire", referencedColumnName="id_reponse_elec_formulaire")
     */
    private Collection $tdossierformulaires;

    /**
     * @return int
     */
    public function getConsultatioId()
    {
        return $this->consultationId;
    }

    /**
     * @param int $consultationId
     */
    public function setConsultationId($consultationId)
    {
        $this->consultationId = $consultationId;
    }

    /**
     * @return int
     */
    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    /**
     * @param int $idEntreprise
     */
    public function setIdEntreprise($idEntreprise)
    {
        $this->idEntreprise = $idEntreprise;
    }

    /**
     * @return int
     */
    public function getIdInscrit()
    {
        return $this->idInscrit;
    }

    /**
     * @param int $idInscrit
     */
    public function setIdInscrit($idInscrit)
    {
        $this->idInscrit = $idInscrit;
    }

    /**
     * @return int
     */
    public function getStatutValidationGlobale()
    {
        return $this->statutValidationGlobale;
    }

    /**
     * @param int $statutValidationGlobale
     */
    public function setStatutValidationGlobale($statutValidationGlobale)
    {
        $this->statutValidationGlobale = $statutValidationGlobale;
    }

    /**
     * @return ArrayCollection
     */
    public function getTdossierformulaires()
    {
        return $this->tdossierformulaires;
    }

    /**
     * @param ArrayCollection $tdossierformulaires
     */
    public function setTdossierformulaires($tdossierformulaires)
    {
        $this->tdossierformulaires = $tdossierformulaires;
    }
}
