<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EntrepriseInsee
 *
 * @ORM\Table(name="EntrepriseInsee")
 * @ORM\Entity(repositoryClass="App\Repository\EntrepriseInseeRepository")
 */
class EntrepriseInsee
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="siren", type="string", length=9, nullable=true)
     */
    private ?string $siren = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="raisonSociale", type="string", length=200, nullable=true)
     */
    private ?string $raisonsociale = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nbEtA", type="integer", nullable=true)
     */
    private ?int $nbeta = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="etat", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private ?string $etat = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="etatLib", type="string", length=40, nullable=true)
     */
    private ?string $etatlib = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="etatDebDate", type="string", length=20, nullable=true)
     */
    private ?string $etatdebdate = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="eff3112Tr", type="integer", nullable=true)
     */
    private ?int $eff3112tr = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="eff3112TrLib", type="string", length=200, nullable=true)
     */
    private ?string $eff3112trlib = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="effAn", type="string", length=10, nullable=true)
     */
    private ?string $effan = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apen", type="string", length=5, nullable=true)
     */
    private ?string $apen = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apenLib", type="string", length=200, nullable=true)
     */
    private ?string $apenlib = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cj", type="string", length=4, nullable=true)
     */
    private ?string $cj = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cjLib", type="string", length=200, nullable=true)
     */
    private ?string $cjlib = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="indNDC", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private ?string $indndc = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="indDoublon", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private ?string $inddoublon = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="indPurge", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private ?string $indpurge = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nicSiege", type="string", length=5, nullable=true, options={"default"="00000"})
     */
    private ?string $nicsiege = '00000';

    /**
     * @var string|null
     *
     * @ORM\Column(name="etatSiege", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private ?string $etatsiege = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="etatSiegeLib", type="string", length=10, nullable=true)
     */
    private ?string $etatsiegelib = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="etatDebDateSiege", type="string", length=20, nullable=true)
     */
    private ?string $etatdebdatesiege = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="eff3112TrSiege", type="integer", nullable=true)
     */
    private ?int $eff3112trsiege = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="eff3112TrSiegeLib", type="string", length=200, nullable=true)
     */
    private ?string $eff3112trsiegelib = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="effAnSiege", type="string", length=10, nullable=true)
     */
    private ?string $effansiege = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apetSiege", type="string", length=5, nullable=true)
     */
    private ?string $apetsiege = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apetSiegeLib", type="string", length=200, nullable=true)
     */
    private ?string $apetsiegelib = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="trtDerDateSiege", type="string", length=20, nullable=true)
     */
    private ?string $trtderdatesiege = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adrEtVoieNum", type="string", length=100, nullable=true)
     */
    private ?string $adretvoienum = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adrEtVoieType", type="string", length=100, nullable=true)
     */
    private ?string $adretvoietype = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adrEtVoieLib", type="string", length=100, nullable=true)
     */
    private ?string $adretvoielib = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adrEtDepCom", type="string", length=100, nullable=true)
     */
    private ?string $adretdepcom = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adrEtComLib", type="string", length=100, nullable=true)
     */
    private ?string $adretcomlib = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adrEtCodePost", type="string", length=100, nullable=true)
     */
    private ?string $adretcodepost = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adrEtPost1", type="string", length=200, nullable=true)
     */
    private ?string $adretpost1 = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adrEtPost2", type="string", length=200, nullable=true)
     */
    private ?string $adretpost2 = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adrEtPost3", type="string", length=200, nullable=true)
     */
    private ?string $adretpost3 = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nic", type="string", length=5, nullable=true, options={"default"="00000"})
     */
    private ?string $nic = '00000';

    /**
     * @var string|null
     *
     * @ORM\Column(name="etatEt", type="string", length=2, nullable=true, options={"fixed"=true})
     */
    private ?string $etatet = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="etatDebDateEt", type="string", length=15, nullable=true)
     */
    private ?string $etatdebdateet = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="catEt", type="string", length=2, nullable=true, options={"fixed"=true})
     */
    private ?string $catet = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="catEtLib", type="string", length=200, nullable=true)
     */
    private ?string $catetlib = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="eff3112TrEt", type="string", length=2, nullable=true, options={"fixed"=true})
     */
    private ?string $eff3112tret = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="eff3112TrEtLib", type="string", length=100, nullable=true)
     */
    private ?string $eff3112tretlib = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="effAnEt", type="string", length=15, nullable=true)
     */
    private ?string $effanet = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apet", type="string", length=5, nullable=true)
     */
    private ?string $apet = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apetLib", type="string", length=200, nullable=true)
     */
    private ?string $apetlib = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="trtDerDateEt", type="string", length=15, nullable=true)
     */
    private ?string $trtderdateet = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="etatEtLib", type="string", length=20, nullable=true)
     */
    private ?string $etatetlib = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSiren(): ?string
    {
        return $this->siren;
    }

    public function setSiren(?string $siren): self
    {
        $this->siren = $siren;

        return $this;
    }

    public function getRaisonsociale(): ?string
    {
        return $this->raisonsociale;
    }

    public function setRaisonsociale(?string $raisonsociale): self
    {
        $this->raisonsociale = $raisonsociale;

        return $this;
    }

    public function getNbeta(): ?int
    {
        return $this->nbeta;
    }

    public function setNbeta(?int $nbeta): self
    {
        $this->nbeta = $nbeta;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(?string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getEtatlib(): ?string
    {
        return $this->etatlib;
    }

    public function setEtatlib(?string $etatlib): self
    {
        $this->etatlib = $etatlib;

        return $this;
    }

    public function getEtatdebdate(): ?string
    {
        return $this->etatdebdate;
    }

    public function setEtatdebdate(?string $etatdebdate): self
    {
        $this->etatdebdate = $etatdebdate;

        return $this;
    }

    public function getEff3112tr(): ?int
    {
        return $this->eff3112tr;
    }

    public function setEff3112tr(?int $eff3112tr): self
    {
        $this->eff3112tr = $eff3112tr;

        return $this;
    }

    public function getEff3112trlib(): ?string
    {
        return $this->eff3112trlib;
    }

    public function setEff3112trlib(?string $eff3112trlib): self
    {
        $this->eff3112trlib = $eff3112trlib;

        return $this;
    }

    public function getEffan(): ?string
    {
        return $this->effan;
    }

    public function setEffan(?string $effan): self
    {
        $this->effan = $effan;

        return $this;
    }

    public function getApen(): ?string
    {
        return $this->apen;
    }

    public function setApen(?string $apen): self
    {
        $this->apen = $apen;

        return $this;
    }

    public function getApenlib(): ?string
    {
        return $this->apenlib;
    }

    public function setApenlib(?string $apenlib): self
    {
        $this->apenlib = $apenlib;

        return $this;
    }

    public function getCj(): ?string
    {
        return $this->cj;
    }

    public function setCj(?string $cj): self
    {
        $this->cj = $cj;

        return $this;
    }

    public function getCjlib(): ?string
    {
        return $this->cjlib;
    }

    public function setCjlib(?string $cjlib): self
    {
        $this->cjlib = $cjlib;

        return $this;
    }

    public function getIndndc(): ?string
    {
        return $this->indndc;
    }

    public function setIndndc(?string $indndc): self
    {
        $this->indndc = $indndc;

        return $this;
    }

    public function getInddoublon(): ?string
    {
        return $this->inddoublon;
    }

    public function setInddoublon(?string $inddoublon): self
    {
        $this->inddoublon = $inddoublon;

        return $this;
    }

    public function getIndpurge(): ?string
    {
        return $this->indpurge;
    }

    public function setIndpurge(?string $indpurge): self
    {
        $this->indpurge = $indpurge;

        return $this;
    }

    public function getNicsiege(): ?string
    {
        return $this->nicsiege;
    }

    public function setNicsiege(?string $nicsiege): self
    {
        $this->nicsiege = $nicsiege;

        return $this;
    }

    public function getEtatsiege(): ?string
    {
        return $this->etatsiege;
    }

    public function setEtatsiege(?string $etatsiege): self
    {
        $this->etatsiege = $etatsiege;

        return $this;
    }

    public function getEtatsiegelib(): ?string
    {
        return $this->etatsiegelib;
    }

    public function setEtatsiegelib(?string $etatsiegelib): self
    {
        $this->etatsiegelib = $etatsiegelib;

        return $this;
    }

    public function getEtatdebdatesiege(): ?string
    {
        return $this->etatdebdatesiege;
    }

    public function setEtatdebdatesiege(?string $etatdebdatesiege): self
    {
        $this->etatdebdatesiege = $etatdebdatesiege;

        return $this;
    }

    public function getEff3112trsiege(): ?int
    {
        return $this->eff3112trsiege;
    }

    public function setEff3112trsiege(?int $eff3112trsiege): self
    {
        $this->eff3112trsiege = $eff3112trsiege;

        return $this;
    }

    public function getEff3112trsiegelib(): ?string
    {
        return $this->eff3112trsiegelib;
    }

    public function setEff3112trsiegelib(?string $eff3112trsiegelib): self
    {
        $this->eff3112trsiegelib = $eff3112trsiegelib;

        return $this;
    }

    public function getEffansiege(): ?string
    {
        return $this->effansiege;
    }

    public function setEffansiege(?string $effansiege): self
    {
        $this->effansiege = $effansiege;

        return $this;
    }

    public function getApetsiege(): ?string
    {
        return $this->apetsiege;
    }

    public function setApetsiege(?string $apetsiege): self
    {
        $this->apetsiege = $apetsiege;

        return $this;
    }

    public function getApetsiegelib(): ?string
    {
        return $this->apetsiegelib;
    }

    public function setApetsiegelib(?string $apetsiegelib): self
    {
        $this->apetsiegelib = $apetsiegelib;

        return $this;
    }

    public function getTrtderdatesiege(): ?string
    {
        return $this->trtderdatesiege;
    }

    public function setTrtderdatesiege(?string $trtderdatesiege): self
    {
        $this->trtderdatesiege = $trtderdatesiege;

        return $this;
    }

    public function getAdretvoienum(): ?string
    {
        return $this->adretvoienum;
    }

    public function setAdretvoienum(?string $adretvoienum): self
    {
        $this->adretvoienum = $adretvoienum;

        return $this;
    }

    public function getAdretvoietype(): ?string
    {
        return $this->adretvoietype;
    }

    public function setAdretvoietype(?string $adretvoietype): self
    {
        $this->adretvoietype = $adretvoietype;

        return $this;
    }

    public function getAdretvoielib(): ?string
    {
        return $this->adretvoielib;
    }

    public function setAdretvoielib(?string $adretvoielib): self
    {
        $this->adretvoielib = $adretvoielib;

        return $this;
    }

    public function getAdretdepcom(): ?string
    {
        return $this->adretdepcom;
    }

    public function setAdretdepcom(?string $adretdepcom): self
    {
        $this->adretdepcom = $adretdepcom;

        return $this;
    }

    public function getAdretcomlib(): ?string
    {
        return $this->adretcomlib;
    }

    public function setAdretcomlib(?string $adretcomlib): self
    {
        $this->adretcomlib = $adretcomlib;

        return $this;
    }

    public function getAdretcodepost(): ?string
    {
        return $this->adretcodepost;
    }

    public function setAdretcodepost(?string $adretcodepost): self
    {
        $this->adretcodepost = $adretcodepost;

        return $this;
    }

    public function getAdretpost1(): ?string
    {
        return $this->adretpost1;
    }

    public function setAdretpost1(?string $adretpost1): self
    {
        $this->adretpost1 = $adretpost1;

        return $this;
    }

    public function getAdretpost2(): ?string
    {
        return $this->adretpost2;
    }

    public function setAdretpost2(?string $adretpost2): self
    {
        $this->adretpost2 = $adretpost2;

        return $this;
    }

    public function getAdretpost3(): ?string
    {
        return $this->adretpost3;
    }

    public function setAdretpost3(?string $adretpost3): self
    {
        $this->adretpost3 = $adretpost3;

        return $this;
    }

    public function getNic(): ?string
    {
        return $this->nic;
    }

    public function setNic(?string $nic): self
    {
        $this->nic = $nic;

        return $this;
    }

    public function getEtatet(): ?string
    {
        return $this->etatet;
    }

    public function setEtatet(?string $etatet): self
    {
        $this->etatet = $etatet;

        return $this;
    }

    public function getEtatdebdateet(): ?string
    {
        return $this->etatdebdateet;
    }

    public function setEtatdebdateet(?string $etatdebdateet): self
    {
        $this->etatdebdateet = $etatdebdateet;

        return $this;
    }

    public function getCatet(): ?string
    {
        return $this->catet;
    }

    public function setCatet(?string $catet): self
    {
        $this->catet = $catet;

        return $this;
    }

    public function getCatetlib(): ?string
    {
        return $this->catetlib;
    }

    public function setCatetlib(?string $catetlib): self
    {
        $this->catetlib = $catetlib;

        return $this;
    }

    public function getEff3112tret(): ?string
    {
        return $this->eff3112tret;
    }

    public function setEff3112tret(?string $eff3112tret): self
    {
        $this->eff3112tret = $eff3112tret;

        return $this;
    }

    public function getEff3112tretlib(): ?string
    {
        return $this->eff3112tretlib;
    }

    public function setEff3112tretlib(?string $eff3112tretlib): self
    {
        $this->eff3112tretlib = $eff3112tretlib;

        return $this;
    }

    public function getEffanet(): ?string
    {
        return $this->effanet;
    }

    public function setEffanet(?string $effanet): self
    {
        $this->effanet = $effanet;

        return $this;
    }

    public function getApet(): ?string
    {
        return $this->apet;
    }

    public function setApet(?string $apet): self
    {
        $this->apet = $apet;

        return $this;
    }

    public function getApetlib(): ?string
    {
        return $this->apetlib;
    }

    public function setApetlib(?string $apetlib): self
    {
        $this->apetlib = $apetlib;

        return $this;
    }

    public function getTrtderdateet(): ?string
    {
        return $this->trtderdateet;
    }

    public function setTrtderdateet(?string $trtderdateet): self
    {
        $this->trtderdateet = $trtderdateet;

        return $this;
    }

    public function getEtatetlib(): ?string
    {
        return $this->etatetlib;
    }

    public function setEtatetlib(?string $etatetlib): self
    {
        $this->etatetlib = $etatetlib;

        return $this;
    }
}

