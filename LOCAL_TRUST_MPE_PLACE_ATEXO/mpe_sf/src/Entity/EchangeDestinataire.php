<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class EchangeDestinataire.
 *
 * @ORM\Table(name="EchangeDestinataire")
 * @ORM\Entity(repositoryClass="App\Repository\EchangeDestinataireRepository")
 */
class EchangeDestinataire
{
    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme", inversedBy="echangeDestinataires")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private ?Organisme $organisme = null;

    /**
     * @ORM\Column(name="id_echange", type="integer", nullable=false)
     */
    private ?int $idEchange = null;

    /**
     * @ORM\Column(name="mail_destinataire", type="string", length=255, nullable=false)
     */
    private ?string $mailDestinataire = null;

    /**
     * @ORM\Column(name="ar", type="string", nullable=false)
     */
    private string $ar = '0';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_ar", type="datetime", nullable=false)*/
    private $dateAr;

    /**
     * @ORM\Column(name="uid", type="string", length=32, nullable=false)
     */
    private ?string $uid = null;

    /**
     * @ORM\Column(name="type_ar", type="integer", nullable=false)
     */
    private ?int $typeAr = null;

    /**
     * @ORM\Column(name="id_inscrit", type="integer", nullable=false)
     */
    private ?int $idInscrit = null;

    /**
     * @ORM\Column(name="id_entreprise", type="integer", nullable=false)
     */
    private ?int $idEntreprise = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Organisme
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param Organisme $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return int
     */
    public function getIdEchange()
    {
        return $this->idEchange;
    }

    /**
     * @param int $idEchange
     */
    public function setIdEchange($idEchange)
    {
        $this->idEchange = $idEchange;
    }

    /**
     * @return string
     */
    public function getMailDestinataire()
    {
        return $this->mailDestinataire;
    }

    /**
     * @param string $mailDestinataire
     */
    public function setMailDestinataire($mailDestinataire)
    {
        $this->mailDestinataire = $mailDestinataire;
    }

    /**
     * @return string
     */
    public function getAr()
    {
        return $this->ar;
    }

    /**
     * @param string $ar
     */
    public function setAr($ar)
    {
        $this->ar = $ar;
    }

    /**
     * @return DateTime
     */
    public function getDateAr()
    {
        return $this->dateAr;
    }

    /**
     * @param DateTime $dateAr
     */
    public function setDateAr($dateAr)
    {
        $this->dateAr = $dateAr;
    }

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return int
     */
    public function getTypeAr()
    {
        return $this->typeAr;
    }

    /**
     * @param int $typeAr
     */
    public function setTypeAr($typeAr)
    {
        $this->typeAr = $typeAr;
    }

    /**
     * @return int
     */
    public function getIdInscrit()
    {
        return $this->idInscrit;
    }

    /**
     * @param int $idInscrit
     */
    public function setIdInscrit($idInscrit)
    {
        $this->idInscrit = $idInscrit;
    }

    /**
     * @return int
     */
    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    /**
     * @param int $idEntreprise
     */
    public function setIdEntreprise($idEntreprise)
    {
        $this->idEntreprise = $idEntreprise;
    }
}
