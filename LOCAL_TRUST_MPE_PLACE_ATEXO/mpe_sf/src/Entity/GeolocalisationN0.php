<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * GeolocalisationN0.
 *
 * @ORM\Table(name="GeolocalisationN0")
 * @ORM\Entity(repositoryClass="App\Repository\GeolocalisationN0Repository")
 */
class GeolocalisationN0
{
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\GeolocalisationN1", mappedBy="geolocalisationN0")
     * @ORM\JoinColumn(name="id", referencedColumnName="id_geolocalisationN0")
     */
    private Collection $geolocalisationN1s;

    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="denomination", type="string", length=40, nullable=false)
     */
    private string $denomination = '';

    /**
     * @ORM\Column(name="denomination_fr", type="string", length=40, nullable=false)
     */
    private ?string $denominationFr = null;

    /**
     * @ORM\Column(name="denomination_en", type="string", length=40, nullable=false)
     */
    private ?string $denominationEn = null;

    /**
     * @ORM\Column(name="denomination_es", type="string", length=40, nullable=false)
     */
    private ?string $denominationEs = null;

    /**
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private ?int $type = null;

    /**
     * @ORM\Column(name="actif", type="string", nullable=false)
     */
    private string $actif = '1';

    /**
     * @ORM\Column(name="libelle_selectionner", type="string", length=40, nullable=false)
     */
    private string $libelleSelectionner = '';

    /**
     * @ORM\Column(name="libelle_selectionner_fr", type="string", length=40, nullable=false)
     */
    private ?string $libelleSelectionnerFr = null;

    /**
     * @ORM\Column(name="libelle_selectionner_en", type="string", length=40, nullable=false)
     */
    private ?string $libelleSelectionnerEn = null;

    /**
     * @ORM\Column(name="libelle_selectionner_es", type="string", length=40, nullable=false)
     */
    private ?string $libelleSelectionnerEs = null;

    /**
     * @ORM\Column(name="libelle_tous", type="string", length=40, nullable=false)
     */
    private string $libelleTous = '';

    /**
     * @ORM\Column(name="libelle_tous_fr", type="string", length=40, nullable=false)
     */
    private ?string $libelleTousFr = null;

    /**
     * @ORM\Column(name="libelle_tous_en", type="string", length=40, nullable=false)
     */
    private ?string $libelleTousEn = null;

    /**
     * @ORM\Column(name="libelle_tous_es", type="string", length=40, nullable=false)
     */
    private ?string $libelleTousEs = null;

    /**
     * @ORM\Column(name="libelle_Aucun", type="string", length=50, nullable=false)
     */
    private string $libelleAucun = '';

    /**
     * @ORM\Column(name="libelle_Aucun_fr", type="string", length=50, nullable=false)
     */
    private ?string $libelleAucunFr = null;

    /**
     * @ORM\Column(name="libelle_Aucun_en", type="string", length=50, nullable=false)
     */
    private ?string $libelleAucunEn = null;

    /**
     * @ORM\Column(name="libelle_Aucun_es", type="string", length=50, nullable=false)
     */
    private ?string $libelleAucunEs = null;

    /**
     * @ORM\Column(name="denomination_ar", type="string", length=40, nullable=false)
     */
    private ?string $denominationAr = null;

    /**
     * @ORM\Column(name="libelle_selectionner_ar", type="string", length=40, nullable=false)
     */
    private ?string $libelleSelectionnerAr = null;

    /**
     * @ORM\Column(name="libelle_tous_ar", type="string", length=40, nullable=false)
     */
    private ?string $libelleTousAr = null;

    /**
     * @ORM\Column(name="libelle_Aucun_ar", type="string", length=40, nullable=false)
     */
    private ?string $libelleAucunAr = null;

    /**
     * @ORM\Column(name="denomination_su", type="string", length=40, nullable=false)
     */
    private ?string $denominationSu = null;

    /**
     * @ORM\Column(name="libelle_selectionner_su", type="string", length=40, nullable=false)
     */
    private ?string $libelleSelectionnerSu = null;

    /**
     * @ORM\Column(name="libelle_tous_su", type="string", length=40, nullable=false)
     */
    private ?string $libelleTousSu = null;

    /**
     * @ORM\Column(name="libelle_Aucun_su", type="string", length=40, nullable=false)
     */
    private ?string $libelleAucunSu = null;

    /**
     * @ORM\Column(name="denomination_du", type="string", length=40, nullable=false)
     */
    private ?string $denominationDu = null;

    /**
     * @ORM\Column(name="libelle_selectionner_du", type="string", length=40, nullable=false)
     */
    private ?string $libelleSelectionnerDu = null;

    /**
     * @ORM\Column(name="libelle_tous_du", type="string", length=40, nullable=false)
     */
    private ?string $libelleTousDu = null;

    /**
     * @ORM\Column(name="libelle_Aucun_du", type="string", length=40, nullable=false)
     */
    private ?string $libelleAucunDu = null;

    /**
     * @ORM\Column(name="denomination_cz", type="string", length=40, nullable=false)
     */
    private ?string $denominationCz = null;

    /**
     * @ORM\Column(name="libelle_selectionner_cz", type="string", length=40, nullable=false)
     */
    private ?string $libelleSelectionnerCz = null;

    /**
     * @ORM\Column(name="libelle_tous_cz", type="string", length=40, nullable=false)
     */
    private ?string $libelleTousCz = null;

    /**
     * @ORM\Column(name="libelle_Aucun_cz", type="string", length=40, nullable=false)
     */
    private ?string $libelleAucunCz = null;

    /**
     * @ORM\Column(name="denomination_it", type="string", length=40, nullable=false)
     */
    private string $denominationIt = '';

    /**
     * @ORM\Column(name="libelle_selectionner_it", type="string", length=40, nullable=false)
     */
    private string $libelleSelectionnerIt = '';

    /**
     * @ORM\Column(name="libelle_tous_it", type="string", length=40, nullable=false)
     */
    private string $libelleTousIt = '';

    /**
     * @ORM\Column(name="libelle_Aucun_it", type="string", length=40, nullable=false)
     */
    private string $libelleAucunIt = '';

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set denomination.
     *
     * @param string $denomination
     *
     * @return GeolocalisationN0
     */
    public function setDenomination($denomination)
    {
        $this->denomination = $denomination;

        return $this;
    }

    /**
     * Get denomination.
     *
     * @return string
     */
    public function getDenomination()
    {
        return $this->denomination;
    }

    /**
     * Set denominationFr.
     *
     * @param string $denominationFr
     *
     * @return GeolocalisationN0
     */
    public function setDenominationFr($denominationFr)
    {
        $this->denominationFr = $denominationFr;

        return $this;
    }

    /**
     * Get denominationFr.
     *
     * @return string
     */
    public function getDenominationFr()
    {
        return $this->denominationFr;
    }

    /**
     * Set denominationEn.
     *
     * @param string $denominationEn
     *
     * @return GeolocalisationN0
     */
    public function setDenominationEn($denominationEn)
    {
        $this->denominationEn = $denominationEn;

        return $this;
    }

    /**
     * Get denominationEn.
     *
     * @return string
     */
    public function getDenominationEn()
    {
        return $this->denominationEn;
    }

    /**
     * Set denominationEs.
     *
     * @param string $denominationEs
     *
     * @return GeolocalisationN0
     */
    public function setDenominationEs($denominationEs)
    {
        $this->denominationEs = $denominationEs;

        return $this;
    }

    /**
     * Get denominationEs.
     *
     * @return string
     */
    public function getDenominationEs()
    {
        return $this->denominationEs;
    }

    /**
     * Set type.
     *
     * @param int $type
     *
     * @return GeolocalisationN0
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set actif.
     *
     * @param string $actif
     *
     * @return GeolocalisationN0
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * Get actif.
     *
     * @return string
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * Set libelleSelectionner.
     *
     * @param string $libelleSelectionner
     *
     * @return GeolocalisationN0
     */
    public function setLibelleSelectionner($libelleSelectionner)
    {
        $this->libelleSelectionner = $libelleSelectionner;

        return $this;
    }

    /**
     * Get libelleSelectionner.
     *
     * @return string
     */
    public function getLibelleSelectionner()
    {
        return $this->libelleSelectionner;
    }

    /**
     * Set libelleSelectionnerFr.
     *
     * @param string $libelleSelectionnerFr
     *
     * @return GeolocalisationN0
     */
    public function setLibelleSelectionnerFr($libelleSelectionnerFr)
    {
        $this->libelleSelectionnerFr = $libelleSelectionnerFr;

        return $this;
    }

    /**
     * Get libelleSelectionnerFr.
     *
     * @return string
     */
    public function getLibelleSelectionnerFr()
    {
        return $this->libelleSelectionnerFr;
    }

    /**
     * Set libelleSelectionnerEn.
     *
     * @param string $libelleSelectionnerEn
     *
     * @return GeolocalisationN0
     */
    public function setLibelleSelectionnerEn($libelleSelectionnerEn)
    {
        $this->libelleSelectionnerEn = $libelleSelectionnerEn;

        return $this;
    }

    /**
     * Get libelleSelectionnerEn.
     *
     * @return string
     */
    public function getLibelleSelectionnerEn()
    {
        return $this->libelleSelectionnerEn;
    }

    /**
     * Set libelleSelectionnerEs.
     *
     * @param string $libelleSelectionnerEs
     *
     * @return GeolocalisationN0
     */
    public function setLibelleSelectionnerEs($libelleSelectionnerEs)
    {
        $this->libelleSelectionnerEs = $libelleSelectionnerEs;

        return $this;
    }

    /**
     * Get libelleSelectionnerEs.
     *
     * @return string
     */
    public function getLibelleSelectionnerEs()
    {
        return $this->libelleSelectionnerEs;
    }

    /**
     * Set libelleTous.
     *
     * @param string $libelleTous
     *
     * @return GeolocalisationN0
     */
    public function setLibelleTous($libelleTous)
    {
        $this->libelleTous = $libelleTous;

        return $this;
    }

    /**
     * Get libelleTous.
     *
     * @return string
     */
    public function getLibelleTous()
    {
        return $this->libelleTous;
    }

    /**
     * Set libelleTousFr.
     *
     * @param string $libelleTousFr
     *
     * @return GeolocalisationN0
     */
    public function setLibelleTousFr($libelleTousFr)
    {
        $this->libelleTousFr = $libelleTousFr;

        return $this;
    }

    /**
     * Get libelleTousFr.
     *
     * @return string
     */
    public function getLibelleTousFr()
    {
        return $this->libelleTousFr;
    }

    /**
     * Set libelleTousEn.
     *
     * @param string $libelleTousEn
     *
     * @return GeolocalisationN0
     */
    public function setLibelleTousEn($libelleTousEn)
    {
        $this->libelleTousEn = $libelleTousEn;

        return $this;
    }

    /**
     * Get libelleTousEn.
     *
     * @return string
     */
    public function getLibelleTousEn()
    {
        return $this->libelleTousEn;
    }

    /**
     * Set libelleTousEs.
     *
     * @param string $libelleTousEs
     *
     * @return GeolocalisationN0
     */
    public function setLibelleTousEs($libelleTousEs)
    {
        $this->libelleTousEs = $libelleTousEs;

        return $this;
    }

    /**
     * Get libelleTousEs.
     *
     * @return string
     */
    public function getLibelleTousEs()
    {
        return $this->libelleTousEs;
    }

    /**
     * Set libelleAucun.
     *
     * @param string $libelleAucun
     *
     * @return GeolocalisationN0
     */
    public function setLibelleAucun($libelleAucun)
    {
        $this->libelleAucun = $libelleAucun;

        return $this;
    }

    /**
     * Get libelleAucun.
     *
     * @return string
     */
    public function getLibelleAucun()
    {
        return $this->libelleAucun;
    }

    /**
     * Set libelleAucunFr.
     *
     * @param string $libelleAucunFr
     *
     * @return GeolocalisationN0
     */
    public function setLibelleAucunFr($libelleAucunFr)
    {
        $this->libelleAucunFr = $libelleAucunFr;

        return $this;
    }

    /**
     * Get libelleAucunFr.
     *
     * @return string
     */
    public function getLibelleAucunFr()
    {
        return $this->libelleAucunFr;
    }

    /**
     * Set libelleAucunEn.
     *
     * @param string $libelleAucunEn
     *
     * @return GeolocalisationN0
     */
    public function setLibelleAucunEn($libelleAucunEn)
    {
        $this->libelleAucunEn = $libelleAucunEn;

        return $this;
    }

    /**
     * Get libelleAucunEn.
     *
     * @return string
     */
    public function getLibelleAucunEn()
    {
        return $this->libelleAucunEn;
    }

    /**
     * Set libelleAucunEs.
     *
     * @param string $libelleAucunEs
     *
     * @return GeolocalisationN0
     */
    public function setLibelleAucunEs($libelleAucunEs)
    {
        $this->libelleAucunEs = $libelleAucunEs;

        return $this;
    }

    /**
     * Get libelleAucunEs.
     *
     * @return string
     */
    public function getLibelleAucunEs()
    {
        return $this->libelleAucunEs;
    }

    /**
     * Set denominationAr.
     *
     * @param string $denominationAr
     *
     * @return GeolocalisationN0
     */
    public function setDenominationAr($denominationAr)
    {
        $this->denominationAr = $denominationAr;

        return $this;
    }

    /**
     * Get denominationAr.
     *
     * @return string
     */
    public function getDenominationAr()
    {
        return $this->denominationAr;
    }

    /**
     * Set libelleSelectionnerAr.
     *
     * @param string $libelleSelectionnerAr
     *
     * @return GeolocalisationN0
     */
    public function setLibelleSelectionnerAr($libelleSelectionnerAr)
    {
        $this->libelleSelectionnerAr = $libelleSelectionnerAr;

        return $this;
    }

    /**
     * Get libelleSelectionnerAr.
     *
     * @return string
     */
    public function getLibelleSelectionnerAr()
    {
        return $this->libelleSelectionnerAr;
    }

    /**
     * Set libelleTousAr.
     *
     * @param string $libelleTousAr
     *
     * @return GeolocalisationN0
     */
    public function setLibelleTousAr($libelleTousAr)
    {
        $this->libelleTousAr = $libelleTousAr;

        return $this;
    }

    /**
     * Get libelleTousAr.
     *
     * @return string
     */
    public function getLibelleTousAr()
    {
        return $this->libelleTousAr;
    }

    /**
     * Set libelleAucunAr.
     *
     * @param string $libelleAucunAr
     *
     * @return GeolocalisationN0
     */
    public function setLibelleAucunAr($libelleAucunAr)
    {
        $this->libelleAucunAr = $libelleAucunAr;

        return $this;
    }

    /**
     * Get libelleAucunAr.
     *
     * @return string
     */
    public function getLibelleAucunAr()
    {
        return $this->libelleAucunAr;
    }

    /**
     * Set denominationSu.
     *
     * @param string $denominationSu
     *
     * @return GeolocalisationN0
     */
    public function setDenominationSu($denominationSu)
    {
        $this->denominationSu = $denominationSu;

        return $this;
    }

    /**
     * Get denominationSu.
     *
     * @return string
     */
    public function getDenominationSu()
    {
        return $this->denominationSu;
    }

    /**
     * Set libelleSelectionnerSu.
     *
     * @param string $libelleSelectionnerSu
     *
     * @return GeolocalisationN0
     */
    public function setLibelleSelectionnerSu($libelleSelectionnerSu)
    {
        $this->libelleSelectionnerSu = $libelleSelectionnerSu;

        return $this;
    }

    /**
     * Get libelleSelectionnerSu.
     *
     * @return string
     */
    public function getLibelleSelectionnerSu()
    {
        return $this->libelleSelectionnerSu;
    }

    /**
     * Set libelleTousSu.
     *
     * @param string $libelleTousSu
     *
     * @return GeolocalisationN0
     */
    public function setLibelleTousSu($libelleTousSu)
    {
        $this->libelleTousSu = $libelleTousSu;

        return $this;
    }

    /**
     * Get libelleTousSu.
     *
     * @return string
     */
    public function getLibelleTousSu()
    {
        return $this->libelleTousSu;
    }

    /**
     * Set libelleAucunSu.
     *
     * @param string $libelleAucunSu
     *
     * @return GeolocalisationN0
     */
    public function setLibelleAucunSu($libelleAucunSu)
    {
        $this->libelleAucunSu = $libelleAucunSu;

        return $this;
    }

    /**
     * Get libelleAucunSu.
     *
     * @return string
     */
    public function getLibelleAucunSu()
    {
        return $this->libelleAucunSu;
    }

    /**
     * Set denominationDu.
     *
     * @param string $denominationDu
     *
     * @return GeolocalisationN0
     */
    public function setDenominationDu($denominationDu)
    {
        $this->denominationDu = $denominationDu;

        return $this;
    }

    /**
     * Get denominationDu.
     *
     * @return string
     */
    public function getDenominationDu()
    {
        return $this->denominationDu;
    }

    /**
     * Set libelleSelectionnerDu.
     *
     * @param string $libelleSelectionnerDu
     *
     * @return GeolocalisationN0
     */
    public function setLibelleSelectionnerDu($libelleSelectionnerDu)
    {
        $this->libelleSelectionnerDu = $libelleSelectionnerDu;

        return $this;
    }

    /**
     * Get libelleSelectionnerDu.
     *
     * @return string
     */
    public function getLibelleSelectionnerDu()
    {
        return $this->libelleSelectionnerDu;
    }

    /**
     * Set libelleTousDu.
     *
     * @param string $libelleTousDu
     *
     * @return GeolocalisationN0
     */
    public function setLibelleTousDu($libelleTousDu)
    {
        $this->libelleTousDu = $libelleTousDu;

        return $this;
    }

    /**
     * Get libelleTousDu.
     *
     * @return string
     */
    public function getLibelleTousDu()
    {
        return $this->libelleTousDu;
    }

    /**
     * Set libelleAucunDu.
     *
     * @param string $libelleAucunDu
     *
     * @return GeolocalisationN0
     */
    public function setLibelleAucunDu($libelleAucunDu)
    {
        $this->libelleAucunDu = $libelleAucunDu;

        return $this;
    }

    /**
     * Get libelleAucunDu.
     *
     * @return string
     */
    public function getLibelleAucunDu()
    {
        return $this->libelleAucunDu;
    }

    /**
     * Set denominationCz.
     *
     * @param string $denominationCz
     *
     * @return GeolocalisationN0
     */
    public function setDenominationCz($denominationCz)
    {
        $this->denominationCz = $denominationCz;

        return $this;
    }

    /**
     * Get denominationCz.
     *
     * @return string
     */
    public function getDenominationCz()
    {
        return $this->denominationCz;
    }

    /**
     * Set libelleSelectionnerCz.
     *
     * @param string $libelleSelectionnerCz
     *
     * @return GeolocalisationN0
     */
    public function setLibelleSelectionnerCz($libelleSelectionnerCz)
    {
        $this->libelleSelectionnerCz = $libelleSelectionnerCz;

        return $this;
    }

    /**
     * Get libelleSelectionnerCz.
     *
     * @return string
     */
    public function getLibelleSelectionnerCz()
    {
        return $this->libelleSelectionnerCz;
    }

    /**
     * Set libelleTousCz.
     *
     * @param string $libelleTousCz
     *
     * @return GeolocalisationN0
     */
    public function setLibelleTousCz($libelleTousCz)
    {
        $this->libelleTousCz = $libelleTousCz;

        return $this;
    }

    /**
     * Get libelleTousCz.
     *
     * @return string
     */
    public function getLibelleTousCz()
    {
        return $this->libelleTousCz;
    }

    /**
     * Set libelleAucunCz.
     *
     * @param string $libelleAucunCz
     *
     * @return GeolocalisationN0
     */
    public function setLibelleAucunCz($libelleAucunCz)
    {
        $this->libelleAucunCz = $libelleAucunCz;

        return $this;
    }

    /**
     * Get libelleAucunCz.
     *
     * @return string
     */
    public function getLibelleAucunCz()
    {
        return $this->libelleAucunCz;
    }

    /**
     * Set denominationIt.
     *
     * @param string $denominationIt
     *
     * @return GeolocalisationN0
     */
    public function setDenominationIt($denominationIt)
    {
        $this->denominationIt = $denominationIt;

        return $this;
    }

    /**
     * Get denominationIt.
     *
     * @return string
     */
    public function getDenominationIt()
    {
        return $this->denominationIt;
    }

    /**
     * Set libelleSelectionnerIt.
     *
     * @param string $libelleSelectionnerIt
     *
     * @return GeolocalisationN0
     */
    public function setLibelleSelectionnerIt($libelleSelectionnerIt)
    {
        $this->libelleSelectionnerIt = $libelleSelectionnerIt;

        return $this;
    }

    /**
     * Get libelleSelectionnerIt.
     *
     * @return string
     */
    public function getLibelleSelectionnerIt()
    {
        return $this->libelleSelectionnerIt;
    }

    /**
     * Set libelleTousIt.
     *
     * @param string $libelleTousIt
     *
     * @return GeolocalisationN0
     */
    public function setLibelleTousIt($libelleTousIt)
    {
        $this->libelleTousIt = $libelleTousIt;

        return $this;
    }

    /**
     * Get libelleTousIt.
     *
     * @return string
     */
    public function getLibelleTousIt()
    {
        return $this->libelleTousIt;
    }

    /**
     * Set libelleAucunIt.
     *
     * @param string $libelleAucunIt
     *
     * @return GeolocalisationN0
     */
    public function setLibelleAucunIt($libelleAucunIt)
    {
        $this->libelleAucunIt = $libelleAucunIt;

        return $this;
    }

    /**
     * Get libelleAucunIt.
     *
     * @return string
     */
    public function getLibelleAucunIt()
    {
        return $this->libelleAucunIt;
    }

    /**
     * Entreprise constructor.
     */
    public function __construct()
    {
        $this->geolocalisationN1s = new ArrayCollection();
    }

    /**
     * @return $this
     */
    public function addGeolocalisationN1(GeolocalisationN1 $geolocalisationn1)
    {
        $this->geolocalisationN1s[] = $geolocalisationn1;
        $geolocalisationn1->setGeolocalisationNo($this);

        return $this;
    }

    public function removeGeolocalisationN1(GeolocalisationN1 $geolocalisationn1)
    {
        $this->geolocalisationN1s->removeElement($geolocalisationn1);
    }

    /**
     * @return ArrayCollection
     */
    public function getGeolocalisationN1s()
    {
        return $this->geolocalisationN1s;
    }
}
