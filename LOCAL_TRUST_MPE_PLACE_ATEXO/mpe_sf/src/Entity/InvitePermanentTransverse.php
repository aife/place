<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InvitePermanentTransverse.
 *
 * @ORM\Table(name="invite_permanent_transverse")
 * @ORM\Entity(repositoryClass="App\Repository\InvitePermanentTransverseRepository")
 */
class InvitePermanentTransverse
{
    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="App\Entity\Service")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     */
    private $service;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="App\Entity\Agent")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id")
     */
    private $agent;

    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme")
     * @ORM\JoinColumn(name="acronyme", referencedColumnName="acronyme")
     */
    private $acronyme;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param int $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @return int
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * @param int $agent
     */
    public function setAgent($agent)
    {
        $this->agent = $agent;
    }

    /**
     * @return string
     */
    public function getAcronyme()
    {
        return $this->acronyme;
    }

    /**
     * @param string $acronyme
     */
    public function setAcronyme($acronyme)
    {
        $this->acronyme = $acronyme;
    }
}
