<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MarchePublie.
 *
 * @ORM\Table(name="MarchePublie")
 * @ORM\Entity(repositoryClass="App\Repository\MarchePublieRepository")
 */
class MarchePublie
{
    public const DATA_BOTH = 0;
    public const DATA_DEFAULT = 1;
    public const DATA_GOUV = 2;

    public const PUBLICATION_YES = 1;
    public const PUBLICATION_NO = 0;

    public const ID_SERVICE_CENTRALISEE = 0;

    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="organisme", type="string", length=30, nullable=false)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(name="numeroMarcheAnnee", type="integer", nullable=true)
     */
    private ?int $numeroMarcheAnnee = 0;

    /**
     * @ORM\Column(name="service_id", type="integer", nullable=true)
     */
    private ?int $serviceId = null;

    /**
     * @ORM\Column(name="isPubliee", type="string", length=1, nullable=false)
     */
    private int|string $isPubliee = 0;

    /**
     * @ORM\Column(name="isImportee", type="string", length=1, nullable=false)
     */
    private int|string $isImportee = 0;

    /**
     * @ORM\Column(name="newVersion", type="string", length=1, nullable=false)
     */
    private int|string $newVersion = 0;

    /**
     * @ORM\Column(name="dataSource", type="integer", length=11, nullable=false)
     */
    private int $dataSource = 0;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return int
     */
    public function getNumeroMarcheAnnee()
    {
        return $this->numeroMarcheAnnee;
    }

    public function setNumeroMarcheAnnee(int $numeroMarcheAnnee)
    {
        $this->numeroMarcheAnnee = $numeroMarcheAnnee;
    }

    /**
     * @return int
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    public function setServiceId(int $serviceId)
    {
        $this->serviceId = $serviceId;
    }

    /**
     * @return string
     */
    public function getisPubliee()
    {
        return $this->isPubliee;
    }

    public function setIsPubliee(string $isPubliee)
    {
        $this->isPubliee = $isPubliee;
    }

    /**
     * @return string
     */
    public function getisImportee()
    {
        return $this->isImportee;
    }

    public function setIsImportee(string $isImportee)
    {
        $this->isImportee = $isImportee;
    }

    /**
     * @return string
     */
    public function getNewVersion()
    {
        return $this->newVersion;
    }

    public function setNewVersion(string $newVersion)
    {
        $this->newVersion = $newVersion;
    }

    public function getDataSource(): int
    {
        return $this->dataSource;
    }

    public function setDataSource(int $dataSource): self
    {
        $this->dataSource = $dataSource;

        return $this;
    }
}
