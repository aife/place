<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AffiliationService.
 *
 * @ORM\Table(name="AffiliationService")
 * @ORM\Entity(repositoryClass="App\Repository\AffiliationServiceRepository")
 */
class AffiliationService
{
    /**
     * @ORM\Column(type="integer", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme", inversedBy="affiliations")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private ?Organisme $organisme = null;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Service")
     * @ORM\JoinColumn(name="service_parent_id", referencedColumnName="id")
     */
    private ?Service $serviceParentId = null;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Service")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     */
    private ?Service $serviceId = null;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Organisme
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    public function setOrganisme(Organisme $organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return Service
     */
    public function getServiceParentId()
    {
        return $this->serviceParentId;
    }

    /**
     * @return Service
     * @deprecated Use getServiceParentId instead
     */
    public function getIdPole()
    {
        return $this->getServiceParentId();
    }

    public function setServiceParentId(Service $serviceId)
    {
        $this->serviceParentId = $serviceId;
    }

    /**
     * @deprecated Use setServiceParentId instead
     */
    public function setIdPole(Service $idPole)
    {
        $this->setServiceParentId($idPole);
    }

    /**
     * @return Service
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * @return Service
     * @deprecated Use getServiceId instead
     */
    public function getIdService()
    {
        return $this->getServiceId();
    }

    public function setServiceId(Service $serviceId)
    {
        $this->serviceId = $serviceId;
    }

    /**
     * @deprecated Use setServiceId instead
     */
    public function setIdService(Service $serviceId)
    {
        $this->setServiceId($serviceId);
    }
}
