<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(
    collectionOperations: [],
    itemOperations: ['get']
)]
/**
 * @ORM\Table(name="AnnonceBoamp")
 * @ORM\Entity(repositoryClass="App\Repository\AnnonceBoampRepository")
 */
class AnnonceBoamp
{
    /**
     *
     * @ORM\Column(name="id_boamp", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="organisme", type="string")
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(name="consultation_id", type="integer")
     */
    private ?int $consultationId = null;

    /**
     * @ORM\Column(name="envoi_boamp", type="integer")
     */
    private int $envoiBoamp = 0;

    /**
     * @ORM\Column(name="date_envoi", type="string")
     */
    private ?string $dateEnvoi = null;

    /**
     * @var blob
     *
     * @ORM\Column(name="ann_xml", type="blob", nullable=true)
     */
    private $annXml;

    /**
     * @var blob
     *
     * @ORM\Column(name="ann_pdf", type="blob", nullable=true)
     */
    private $annPdf;

    /**
     * @var blob
     *
     * @ORM\Column(name="ann_form_values", type="blob", nullable=true)
     */
    private $annFormValues;

    /**
     * @var text
     *
     * @ORM\Column(name="ann_error", type="text", nullable=true)
     */
    private $annError;

    /**
     * @ORM\Column(name="type_boamp", type="string")
     */
    private ?string $typeBoamp = null;

    /**
     * @ORM\Column(name="type_ann", type="integer")
     */
    private ?int $typeAnn = 0;

    /**
     * @ORM\Column(name="datepub", type="string")
     */
    private ?string $datePub = null;

    /**
     * @ORM\Column(name="num_ann", type="integer")
     */
    private ?int $numAnn = 0;

    /**
     * @ORM\Column(name="parution", type="string")
     */
    private ?string $parution = null;

    /**
     * @ORM\Column(name="id_jo", type="string" )
     */
    private ?string $idJo = '';

    /**
     * @ORM\Column(name="erreurs", type="string")
     */
    private ?string $erreurs = null;

    /**
     * @ORM\Column(name="nom_fichier_xml", type="string")
     */
    private ?string $nomFichierXml = '';

    /**
     * @ORM\Column(name="envoi_joue", type="string")
     */
    private ?string $envoiJoue = '0';

    /**
     * @ORM\Column(name="mapa", type="string")
     */
    private ?string $mapa = '0';

    /**
     * @ORM\Column(name="implique_SAD", type="string")
     */
    private ?string $impliqueSAD = '0';

    /**
     * @var datetime
     *
     * @ORM\Column(name="date_maj", type="datetime")
     */
    private $dateMaj;

    /**
     * @ORM\Column(name="id_destination_form_xml", type="integer")
     */
    private ?int $idDestinationformxml = 0;

    /**
     * @ORM\Column(name="id_form_xml", type="integer")
     */
    private ?int $idFormXml = 0;

    /**
     * @ORM\Column(name="id_type_xml", type="integer")
     */
    private ?int $idTypexml = 0;

    /**
     * @ORM\Column(name="statut_destinataire", type="string")
     */
    private ?string $statutDestinataire = null;

    /**
     * @ORM\Column(name="accuse_reception", type="string")
     */
    private ?string $accuseReception = '';

    /**
     * @ORM\Column(name="lien", type="string")
     */
    private ?string $lien = '';

    /**
     * @ORM\Column(name="lien_boamp", type="string")
     */
    private ?string $lienBoamp = null;

    /**
     * @ORM\Column(name="lien_pdf", type="string")
     */
    private ?string $lienPdf = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param string $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return int
     */
    public function getConsultationId()
    {
        return $this->consultationId;
    }

    /**
     * @param int $consultationId
     */
    public function setConsultationId($consultationId)
    {
        $this->consultationId = $consultationId;
    }

    /**
     * @return int
     */
    public function getEnvoiBoamp()
    {
        return $this->envoiBoamp;
    }

    /**
     * @param int $envoiBoamp
     */
    public function setEnvoiBoamp($envoiBoamp)
    {
        $this->envoiBoamp = $envoiBoamp;
    }

    /**
     * @return string
     */
    public function getDateEnvoi()
    {
        return $this->dateEnvoi;
    }

    /**
     * @param string $dateEnvoi
     */
    public function setDateEnvoi($dateEnvoi)
    {
        $this->dateEnvoi = $dateEnvoi;
    }

    /**
     * @return blob
     */
    public function getAnnXml()
    {
        return $this->annXml;
    }

    /**
     * @param blob $annXml
     */
    public function setAnnXml($annXml)
    {
        $this->annXml = $annXml;
    }

    /**
     * @return blob
     */
    public function getAnnPdf()
    {
        return $this->annPdf;
    }

    /**
     * @param blob $annPdf
     */
    public function setAnnPdf($annPdf)
    {
        $this->annPdf = $annPdf;
    }

    /**
     * @return blob
     */
    public function getAnnFormValues()
    {
        return $this->annFormValues;
    }

    /**
     * @param blob $annFormValues
     */
    public function setAnnFormValues($annFormValues)
    {
        $this->annFormValues = $annFormValues;
    }

    /**
     * @return text
     */
    public function getAnnError()
    {
        return $this->annError;
    }

    /**
     * @param text $annError
     */
    public function setAnnError($annError)
    {
        $this->annError = $annError;
    }

    /**
     * @return string
     */
    public function getTypeBoamp()
    {
        return $this->typeBoamp;
    }

    /**
     * @param string $typeBoamp
     */
    public function setTypeBoamp($typeBoamp)
    {
        $this->typeBoamp = $typeBoamp;
    }

    /**
     * @return int
     */
    public function getTypeAnn()
    {
        return $this->typeAnn;
    }

    /**
     * @param int $typeAnn
     */
    public function setTypeAnn($typeAnn)
    {
        $this->typeAnn = $typeAnn;
    }

    /**
     * @return string
     */
    public function getDatePub()
    {
        return $this->datePub;
    }

    /**
     * @param string $datePub
     */
    public function setDatePub($datePub)
    {
        $this->datePub = $datePub;
    }

    /**
     * @return int
     */
    public function getNumAnn()
    {
        return $this->numAnn;
    }

    /**
     * @param int $numAnn
     */
    public function setNumAnn($numAnn)
    {
        $this->numAnn = $numAnn;
    }

    /**
     * @return string
     */
    public function getParution()
    {
        return $this->parution;
    }

    /**
     * @param string $parution
     */
    public function setParution($parution)
    {
        $this->parution = $parution;
    }

    /**
     * @return string
     */
    public function getIdJo()
    {
        return $this->idJo;
    }

    /**
     * @param string $idJo
     */
    public function setIdJo($idJo)
    {
        $this->idJo = $idJo;
    }

    /**
     * @return string
     */
    public function getErreurs()
    {
        return $this->erreurs;
    }

    /**
     * @param string $erreurs
     */
    public function setErreurs($erreurs)
    {
        $this->erreurs = $erreurs;
    }

    /**
     * @return string
     */
    public function getNomFichierXml()
    {
        return $this->nomFichierXml;
    }

    /**
     * @param string $nomFichierXml
     */
    public function setNomFichierXml($nomFichierXml)
    {
        $this->nomFichierXml = $nomFichierXml;
    }

    /**
     * @return string
     */
    public function getEnvoiJoue()
    {
        return $this->envoiJoue;
    }

    /**
     * @param string $envoiJoue
     */
    public function setEnvoiJoue($envoiJoue)
    {
        $this->envoiJoue = $envoiJoue;
    }

    /**
     * @return string
     */
    public function getMapa()
    {
        return $this->mapa;
    }

    /**
     * @param string $mapa
     */
    public function setMapa($mapa)
    {
        $this->mapa = $mapa;
    }

    /**
     * @return string
     */
    public function getImpliqueSAD()
    {
        return $this->impliqueSAD;
    }

    /**
     * @param string $impliqueSAD
     */
    public function setImpliqueSAD($impliqueSAD)
    {
        $this->impliqueSAD = $impliqueSAD;
    }

    /**
     * @return datetime
     */
    public function getDateMaj()
    {
        return $this->dateMaj;
    }

    /**
     * @param datetime $dateMaj
     */
    public function setDateMaj($dateMaj)
    {
        $this->dateMaj = $dateMaj;
    }

    /**
     * @return int
     */
    public function getIdDestinationformxml()
    {
        return $this->idDestinationformxml;
    }

    /**
     * @param int $idDestinationformxml
     */
    public function setIdDestinationformxml($idDestinationformxml)
    {
        $this->idDestinationformxml = $idDestinationformxml;
    }

    /**
     * @return int
     */
    public function getIdFormXml()
    {
        return $this->idFormXml;
    }

    /**
     * @param int $idFormXml
     */
    public function setIdFormXml($idFormXml)
    {
        $this->idFormXml = $idFormXml;
    }

    /**
     * @return int
     */
    public function getIdTypexml()
    {
        return $this->idTypexml;
    }

    /**
     * @param int $idTypexml
     */
    public function setIdTypexml($idTypexml)
    {
        $this->idTypexml = $idTypexml;
    }

    /**
     * @return string
     */
    public function getStatutDestinataire()
    {
        return $this->statutDestinataire;
    }

    /**
     * @param string $statutDestinataire
     */
    public function setStatutDestinataire($statutDestinataire)
    {
        $this->statutDestinataire = $statutDestinataire;
    }

    /**
     * @return string
     */
    public function getAccuseReception()
    {
        return $this->accuseReception;
    }

    /**
     * @param string $accuseReception
     */
    public function setAccuseReception($accuseReception)
    {
        $this->accuseReception = $accuseReception;
    }

    /**
     * @return string
     */
    public function getLien()
    {
        return $this->lien;
    }

    /**
     * @param string $lien
     */
    public function setLien($lien)
    {
        $this->lien = $lien;
    }

    /**
     * @return string
     */
    public function getLienBoamp()
    {
        return $this->lienBoamp;
    }

    /**
     * @param string $lienBoamp
     */
    public function setLienBoamp($lienBoamp)
    {
        $this->lienBoamp = $lienBoamp;
    }

    /**
     * @return string
     */
    public function getLienPdf()
    {
        return $this->lienPdf;
    }

    /**
     * @param string $lienPdf
     */
    public function setLienPdf($lienPdf)
    {
        $this->lienPdf = $lienPdf;
    }
}
