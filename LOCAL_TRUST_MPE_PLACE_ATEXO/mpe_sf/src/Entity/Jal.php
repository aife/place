<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="JAL")
 * @ORM\Entity(repositoryClass="App\Repository\JalRepository")
 */
class Jal
{
    /**
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="service_id", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private ?int $serviceId = null;

    /**
     * @var string
     *
     * @ORM\Column(name="organisme", type="string", length=30, nullable=false)
     */
    private string $organisme;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=100, nullable=false, options={"default"="''"})
     */
    private string $nom = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=false, options={"default"="''"})
     */
    private string $email = '\'\'';

    /**
     * @ORM\Column(type="string", length=100)
     */
    private string $emailAr;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private string $telecopie;

    /**
     * @var string
     *
     * @ORM\Column(name="information_facturation", type="text", length=65535, nullable=false)
     */
    private string $informationFacturation;

    /**
     * @var int|null
     *
     * @ORM\Column(name="old_service_id", type="integer", nullable=true)
     */
    private $oldServiceId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="old_id", type="integer", nullable=true)
     */
    private $oldId;


    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme): self
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getServiceId(): int
    {
        return $this->serviceId;
    }

    public function setServiceId(int $serviceId = null)
    {
        $this->serviceId = $serviceId;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getEmailAr(): ?string
    {
        return $this->emailAr;
    }

    public function setEmailAr(string $emailAr): self
    {
        $this->emailAr = $emailAr;

        return $this;
    }

    public function getTelecopie(): ?string
    {
        return $this->telecopie;
    }

    public function setTelecopie(string $telecopie): self
    {
        $this->telecopie = $telecopie;

        return $this;
    }

    public function getInformationFacturation(): ?string
    {
        return $this->informationFacturation;
    }

    public function setInformationFacturation(string $informationFacturation): self
    {
        $this->informationFacturation = $informationFacturation;

        return $this;
    }
}
