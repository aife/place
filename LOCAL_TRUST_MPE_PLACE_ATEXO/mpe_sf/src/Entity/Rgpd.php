<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

abstract class Rgpd
{
    /**
     * @ORM\Column(name="date_validation_rgpd", type="datetime", nullable=true)
     */
    protected ?DateTimeInterface $dateValidationRgpd = null;

    /**
     * @ORM\Column(name="rgpd_communication_place", type="boolean", nullable=true)
     */
    protected ?bool $rgpdCommunicationPlace = false;

    /**
     * @ORM\Column(name="rgpd_enquete", type="boolean", nullable=true)
     */
    protected ?bool $rgpdEnquete = false;

    public function getDateValidationRgpd(): ?DateTimeInterface
    {
        return $this->dateValidationRgpd;
    }

    public function setDateValidationRgpd(?DateTimeInterface $dateValidationRgpd): self
    {
        $this->dateValidationRgpd = $dateValidationRgpd;
        return $this;
    }

    public function isRgpdCommunicationPlace(): ?bool
    {
        return $this->rgpdCommunicationPlace;
    }

    public function setRgpdCommunicationPlace(bool $rgpdCommunicationPlace): self
    {
        $this->rgpdCommunicationPlace = $rgpdCommunicationPlace;

        return $this;
    }

    public function isRgpdEnquete(): ?bool
    {
        return $this->rgpdEnquete;
    }

    public function setRgpdEnquete(bool $rgpdEnquete): self
    {
        $this->rgpdEnquete = $rgpdEnquete;

        return $this;
    }
}
