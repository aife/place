<?php

namespace App\Entity\Api;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class SupervisionInterface.
 *
 * @ORM\Table(name="supervision_interface")
 * @ORM\Entity(repositoryClass="App\Repository\Api\SupervisionInterfaceRepository")
 */
class SupervisionInterface
{
    /**
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="nom_interface", type="string", length=50, nullable=false,
     *     options={"comment":"Nom de l'interface utilisé par le rapports"})*/
    #[Groups('webservice')]
    private ?string $interface = null;

    /**
     * @ORM\Column(type="string", length=50, nullable=false)*/
    #[Groups('webservice')]
    private ?string $service = null;

    /**
     * @ORM\Column(type="string", length=100, nullable=false,
     *     options={"comment":"Nom de l'interface utilisé par le rapports"})*/
    #[Groups('webservice')]
    private ?string $webserviceBatch = null;

    /**
     * @ORM\Column(name="total", type="integer", nullable=false)*/
    #[Groups('webservice')]
    private ?int $total = null;

    /**
     * @ORM\Column(name="total_ok", type="integer", nullable=true)*/
    #[Groups('webservice')]
    private ?int $totalOk = null;

    /**
     * @ORM\Column(name="poids", type="bigint", nullable=true)*/
    #[Groups('webservice')]
    private ?int $poids = null;

    /**
     * @ORM\Column(name="poids_ok", type="bigint", nullable=true)*/
    #[Groups('webservice')]
    private ?int $poidsOk = null;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=false)*/
    #[Groups('webservice')]
    private $createdDate;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getInterface()
    {
        return $this->interface;
    }

    public function setInterface(string $interface)
    {
        $this->interface = $interface;
    }

    /**
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }

    public function setService(string $service)
    {
        $this->service = $service;
    }

    /**
     * @return string
     */
    public function getWebserviceBatch()
    {
        return $this->webserviceBatch;
    }

    public function setWebserviceBatch(string $webserviceBatch)
    {
        $this->webserviceBatch = $webserviceBatch;
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    public function setTotal(int $total)
    {
        $this->total = $total;
    }

    /**
     * @return int
     */
    public function getTotalOk()
    {
        return $this->totalOk;
    }

    public function setTotalOk(int $totalOk)
    {
        $this->totalOk = $totalOk;
    }

    /**
     * @return int
     */
    public function getPoids()
    {
        return $this->poids;
    }

    public function setPoids(int $poids)
    {
        $this->poids = $poids;
    }

    /**
     * @return int
     */
    public function getPoidsOk()
    {
        return $this->poidsOk;
    }

    public function setPoidsOk(int $poidsOk)
    {
        $this->poidsOk = $poidsOk;
    }

    /**
     * @return DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    public function setCreatedDate(DateTime $createdDate)
    {
        $this->createdDate = $createdDate;
    }
}
