<?php

namespace App\Entity;

use DateTime;
use App\Controller\CreateBloborganismeFileAction;
use App\Controller\DownloadBloborganismeFileAction;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * BloborganismeFile.
 *
 * @ORM\Table(name="blobOrganisme_file")
 * @ORM\Entity(repositoryClass="App\Repository\BloborganismeFileRepository")
 * @Vich\Uploadable
 */

class BloborganismeFile
{
    public const EXTENSION_NAME_CHIFFRE = '.chiffre';

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")*/
    #[Groups('read')]
    private ?int $id = null;

    /**
     * @ORM\Column(name="organisme", type="string", length=30)
     */
    private string $organisme = '';

    /**
     * @ORM\Column(name="old_id", type="integer", nullable=true)
     */
    private ?int $oldId = null;

    /**
     * @ORM\Column(name="name", type="text", length=65535, nullable=false)
     */
    private ?string $name = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="deletion_datetime", type="datetime", nullable=true)*/
    private $deletionDatetime;

    /**
     * @ORM\Column(name="statut_synchro", type="integer")
     */
    private ?int $statutSynchro = null;

    /**
     * @ORM\Column(name="hash", type="string", length=255)*/
    #[Groups('read')]
    private ?string $hash = null;

    /**
     * @ORM\Column(name="dossier", type="string", length=255, nullable=true )
     */
    private ?string $dossier = null;

    /**
     * @ORM\Column(name="chemin", type="text", nullable=true)
     */
    private ?string $chemin = null;

    /**
     * @ORM\Column(name="extension", type="string", length=50, nullable=false )
     */
    private string $extension = '';

    /**
     * @Vich\UploadableField(mapping="media_object", fileNameProperty="chemin")*/
    #[Assert\NotNull(groups: ['media_object_create'])]
    public ?File $file = null;

    /**
     * Agent constructor.
     */
    public function __construct()
    {
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->oldId ?? $this->id;
    }

    /**
     * Set organisme.
     *
     * @param string $organisme
     *
     * @return $this
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * Get organisme.
     *
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set deletionDatetime.
     *
     * @param DateTime $deletionDatetime
     *
     * @return $this
     */
    public function setDeletionDatetime($deletionDatetime)
    {
        $this->deletionDatetime = $deletionDatetime;

        return $this;
    }

    /**
     * Get deletionDatetime.
     *
     * @return DateTime
     */
    public function getDeletionDatetime()
    {
        return $this->deletionDatetime;
    }

    public function getStatutSynchro(): int
    {
        return $this->statutSynchro;
    }

    public function setStatutSynchro(int $statutSynchro)
    {
        $this->statutSynchro = $statutSynchro;
    }

    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function setHash(string $hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * @return string
     */
    public function getDossier()
    {
        return $this->dossier;
    }

    /**
     * @param string $dossier
     */
    public function setDossier($dossier = null)
    {
        $this->dossier = $dossier;

        return $this;
    }

    /**
     * @return string
     */
    public function getChemin()
    {
        return $this->chemin;
    }

    public function setChemin(?string $chemin)
    {
        $this->chemin = $chemin;

        return $this;
    }

    /**
     * @return int
     */
    public function getOldId(): ?int
    {
        return $this->oldId;
    }

    public function setOldId(int $oldId): BloborganismeFile
    {
        $this->oldId = $oldId;

        return $this;
    }

    public function getExtension(): string
    {
        return $this->extension;
    }

    public function setExtension(string $extension): BloborganismeFile
    {
        $this->extension = $extension;

        return $this;
    }
}
