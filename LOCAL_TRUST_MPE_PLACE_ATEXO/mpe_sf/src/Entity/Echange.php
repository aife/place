<?php

namespace App\Entity;

use App\Entity\DossierVolumineux\DossierVolumineux;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Echange
 *
 * @ORM\Table(name="Echange", indexes={@ORM\Index(name="echange_email_expediteur_idx", columns={"email_expediteur"}),
 *     @ORM\Index(name="IDX_7ACADA23B6709BD0", columns={"id_dossier_volumineux"}),
 *     @ORM\Index(name="echange_organisme_idx", columns={"organisme"}),
 *     @ORM\Index(name="echange_ref_consultation_idx", columns={"ref_consultation"})})
 * @ORM\Entity(repositoryClass="App\Repository\EchangeRepository")
 */
class Echange
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme", inversedBy="echanges")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private ?Organisme $organisme = null;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\RelationEchange", mappedBy="echange")
     */
    private Collection $relationsEchange;

    /**
     * @ORM\Column(name="objet", type="string", length=256, nullable=false, options={"default"="''"})
     */
    private string $objet;

    /**
     * @ORM\Column(name="corps", type="text", length=65535, nullable=false)
     */
    private string $corps;

    /**
     * @ORM\Column(name="expediteur", type="string", length=100, nullable=false, options={"default"="''"})
     */
    private string $expediteur;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id_createur", type="integer", nullable=true, options={"default"=0})
     */
    private int $idCreateur = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ref_consultation", type="integer", nullable=false, options={"default"=0})
     */
    private int $refConsultation = 0;

    /**
     * @ORM\Column(name="consultation_id", type="integer", nullable=true)
     */
    private ?int $consultationId = null;

    /**
     * @var int
     *
     * @ORM\Column(name="option_envoi", type="integer", nullable=false, options={"default"=0})
     */
    private int $optionEnvoi = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="date_message", type="string", length=25, nullable=false, options={"default"="''"})
     */
    private string $dateMessage = '\'\'';

    /**
     * @var int
     *
     * @ORM\Column(name="format", type="integer", nullable=false, options={"default"=0})
     */
    private int $format = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="id_action_declencheur", type="integer", nullable=false, options={"default"=0})
     */
    private int $idActionDeclencheur = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=false, options={"default"=0})
     */
    private int $status = 0;

    /**
     * @ORM\Column(name="service_id", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private ?int $serviceId = null;

    /**
     * @ORM\Column(name="email_expediteur", type="string", length=100, nullable=false, options={"default"="''"})
     */
    private string $emailExpediteur = "''";

    /**
     * @ORM\Column(name="id_type_message", type="integer", nullable=false)
     */
    private int $idTypeMessage;

    /**
     * @var string|null
     *
     * @ORM\Column(name="destinataires_retraits", type="text", length=0, nullable=true)
     */
    private ?string $destinatairesRetraits = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="destinataires_questions", type="text", length=0, nullable=true)
     */
    private ?string $destinatairesQuestions = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="destinataires_depots", type="text", length=0, nullable=true)
     */
    private ?string $destinatairesDepots = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="destinataires_bd_fournisseurs", type="text", length=0, nullable=true)
     */
    private ?string $destinatairesDbFournisseurs = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="destinataires_libres", type="text", length=0, nullable=true)
     */
    private ?string $destinatairesLibres = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="page_source", type="string", length=256, nullable=true)
     */
    private ?string $pageSource = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="destinataires", type="text", length=0, nullable=true)
     */
    private ?string $destinataires = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DossierVolumineux\DossierVolumineux" , inversedBy="echanges")
     * @ORM\JoinColumn(name="id_dossier_volumineux", referencedColumnName="id")
     */
    private ?DossierVolumineux $dossierVolumineux = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="old_service_id", type="integer", nullable=true)
     */
    private $oldServiceId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="old_id", type="integer", nullable=true)
     */
    private $oldId;


    public function __construct()
    {
        $this->relationsEchange = new ArrayCollection();
    }

    /**
     * @return DossierVolumineux
     */
    public function getDossierVolumineux()
    {
        return $this->dossierVolumineux;
    }

    public function setDossierVolumineux(DossierVolumineux $dossierVolumineux)
    {
        $this->dossierVolumineux = $dossierVolumineux;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Organisme
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param Organisme $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return $this
     */
    public function addRelationEchange(RelationEchange $relationEchange)
    {
        $this->relationsEchange[] = $relationEchange;
        $relationEchange->setEchange($this);

        return $this;
    }

    public function removeRelationEchange(RelationEchange $relationEchange)
    {
        $this->relationsEchange->removeElement($relationEchange);
    }

    /**
     * @return ArrayCollection
     */
    public function getRelationsEchange()
    {
        return $this->relationsEchange;
    }

    /**
     * @return string
     */
    public function getObjet()
    {
        return $this->objet;
    }

    /**
     * @param string $objet
     */
    public function setObjet($objet)
    {
        $this->objet = $objet;
    }

    /**
     * @return string
     */
    public function getCorps()
    {
        return $this->corps;
    }

    public function setCorps(string $corps): self
    {
        $this->corps = $corps;

        return $this;
    }

    public function getExpediteur(): ?string
    {
        return $this->expediteur;
    }

    public function setExpediteur(string $expediteur): self
    {
        $this->expediteur = $expediteur;

        return $this;
    }

    public function getIdCreateur(): ?int
    {
        return $this->idCreateur;
    }

    public function setIdCreateur(int $idCreateur): self
    {
        $this->idCreateur = $idCreateur;

        return $this;
    }

    /**
     * @return int
     */
    public function getConsultationId()
    {
        return $this->consultationId;
    }

    /**
     * @param int $consultationId
     */
    public function setConsultationId($consultationId)
    {
        $this->consultationId = $consultationId;
    }

    /**
     * @return int
     */
    public function getOptionEnvoi()
    {
        return $this->optionEnvoi;
    }

    /**
     * @param int $optionEnvoi
     */
    public function setOptionEnvoi($optionEnvoi)
    {
        $this->optionEnvoi = $optionEnvoi;
    }

    public function getDateMessage(): ?string
    {
        return $this->dateMessage;
    }

    public function setDateMessage(string $dateMessage): self
    {
        $this->dateMessage = $dateMessage;

        return $this;
    }

    public function getFormat(): ?int
    {
        return $this->format;
    }

    public function setFormat(int $format): self
    {
        $this->format = $format;

        return $this;
    }

    public function getIdActionDeclencheur(): ?int
    {
        return $this->idActionDeclencheur;
    }

    public function setIdActionDeclencheur(int $idActionDeclencheur): self
    {
        $this->idActionDeclencheur = $idActionDeclencheur;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }
    /**
     * @return int
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * @param int $serviceId
     */
    public function setServiceId($serviceId)
    {
        $this->serviceId = $serviceId;
    }

    /**
     * @return string
     */
    public function getEmailExpediteur()
    {
        return $this->emailExpediteur;
    }

    public function setEmailExpediteur(string $emailExpediteur): self
    {
        $this->emailExpediteur = $emailExpediteur;

        return $this;
    }

    public function getIdTypeMessage(): ?int
    {
        return $this->idTypeMessage;
    }

    public function setIdTypeMessage(int $idTypeMessage): self
    {
        $this->idTypeMessage = $idTypeMessage;

        return $this;
    }

    /**
     * @return string
     */
    public function getDestinatairesRetraits()
    {
        return $this->destinatairesRetraits;
    }

    /**
     * @param string $destinatairesRetraits
     */
    public function setDestinatairesRetraits($destinatairesRetraits)
    {
        $this->destinatairesRetraits = $destinatairesRetraits;
    }

    /**
     * @return string
     */
    public function getDestinatairesQuestions()
    {
        return $this->destinatairesQuestions;
    }

    /**
     * @param string $destinatairesQuestions
     */
    public function setDestinatairesQuestions($destinatairesQuestions)
    {
        $this->destinatairesQuestions = $destinatairesQuestions;
    }

    /**
     * @return string
     */
    public function getDestinatairesDepots()
    {
        return $this->destinatairesDepots;
    }

    /**
     * @param string $destinatairesDepots
     */
    public function setDestinatairesDepots($destinatairesDepots)
    {
        $this->destinatairesDepots = $destinatairesDepots;
    }

    /**
     * @return string
     */
    public function getDestinatairesDbFournisseurs()
    {
        return $this->destinatairesDbFournisseurs;
    }

    /**
     * @param string $destinatairesDbFournisseurs
     */
    public function setDestinatairesDbFournisseurs($destinatairesDbFournisseurs)
    {
        $this->destinatairesDbFournisseurs = $destinatairesDbFournisseurs;
    }

    /**
     * @return string
     */
    public function getDestinatairesLibres()
    {
        return $this->destinatairesLibres;
    }

    /**
     * @param string $destinatairesLibres
     */
    public function setDestinatairesLibres($destinatairesLibres)
    {
        $this->destinatairesLibres = $destinatairesLibres;
    }

    /**
     * @return string
     */
    public function getPageSource()
    {
        return $this->pageSource;
    }

    /**
     * @param string $pageSource
     */
    public function setPageSource($pageSource)
    {
        $this->pageSource = $pageSource;
    }

    /**
     * @return string
     */
    public function getDestinataires()
    {
        return $this->destinataires;
    }

    /**
     * @param string $destinataires
     */
    public function setDestinataires($destinataires)
    {
        $this->destinataires = $destinataires;
    }
}
