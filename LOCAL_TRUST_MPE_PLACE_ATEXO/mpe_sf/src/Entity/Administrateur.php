<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Administrateur
 *
 * @ORM\Table(name="Administrateur", indexes={@ORM\Index(name="organisme", columns={"organisme"})})
 * @ORM\Entity(repositoryClass="App\Repository\AdministrateurRepository")
 */
class Administrateur implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var string
     *
     * @ORM\Column(name="original_login", type="string", length=100, nullable=false)
     */
    private string $originalLogin = '';

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=100, nullable=false)
     */
    private string $login = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="certificat", type="text", length=65535, nullable=true)
     */
    private ?string $certificat = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mdp", type="string", length=255, nullable=true)
     */
    private ?string $mdp = null;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=100, nullable=false)
     */
    private string $nom = '';

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=100, nullable=false)
     */
    private string $prenom = '';

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=false)
     */
    private string $email = '';

    /**
     * @var string
     *
     * @ORM\Column(name="admin_general", type="string", length=1, nullable=false, options={"fixed"=true})
     */
    private string $adminGeneral = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="tentatives_mdp", type="integer", nullable=false)
     */
    private $tentativesMdp = '0';

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme", inversedBy="administrateurs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     * })
     */
    private ?Organisme $organisme = null;

    /**
     * @ORM\ManyToMany(targetEntity=HabilitationAdministrateur::class, mappedBy="administrateurs")
     */
    private Collection $habilitationAdministrateurs;

    public function __construct()
    {
        $this->habilitationAdministrateurs = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOriginalLogin(): ?string
    {
        return $this->originalLogin;
    }

    public function setOriginalLogin(string $originalLogin): self
    {
        $this->originalLogin = $originalLogin;

        return $this;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    public function getCertificat(): ?string
    {
        return $this->certificat;
    }

    public function setCertificat(?string $certificat): self
    {
        $this->certificat = $certificat;

        return $this;
    }

    public function getMdp(): ?string
    {
        return $this->mdp;
    }

    public function setMdp(?string $mdp): self
    {
        $this->mdp = $mdp;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAdminGeneral(): ?string
    {
        return $this->adminGeneral;
    }

    public function setAdminGeneral(string $adminGeneral): self
    {
        $this->adminGeneral = $adminGeneral;

        return $this;
    }

    public function getTentativesMdp(): ?int
    {
        return $this->tentativesMdp;
    }

    public function setTentativesMdp(int $tentativesMdp): self
    {
        $this->tentativesMdp = $tentativesMdp;

        return $this;
    }

    public function getOrganisme(): ?Organisme
    {
        return $this->organisme;
    }

    public function setOrganisme(?Organisme $organisme): self
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getRoles()
    {
        return $this->getAdminGeneral() === '1' ? ['ROLE_SUPER_ADMIN'] : ['ROLE_ADMIN'];
    }

    public function getPassword()
    {
        return $this->mdp;
    }

    public function getUsername()
    {
        return $this->login;
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return Collection<int, HabilitationAdministrateur>
     */
    public function getHabilitationAdministrateurs(): Collection
    {
        return $this->habilitationAdministrateurs;
    }

    public function addHabilitationAdministrateur(HabilitationAdministrateur $habilitationAdministrateur): self
    {
        if (!$this->habilitationAdministrateurs->contains($habilitationAdministrateur)) {
            $this->habilitationAdministrateurs[] = $habilitationAdministrateur;
            $habilitationAdministrateur->addAdministrateur($this);
        }

        return $this;
    }

    public function removeHabilitationAdministrateur(HabilitationAdministrateur $habilitationAdministrateur): self
    {
        if ($this->habilitationAdministrateurs->removeElement($habilitationAdministrateur)) {
            $habilitationAdministrateur->removeAdministrateur($this);
        }

        return $this;
    }

    public function hasHabilitationAdministrateur(string $habilitationLabel): bool
    {
        return $this->habilitationAdministrateurs->exists(function ($key, $element) use ($habilitationLabel) {
            return $habilitationLabel === $element->getHabilitation();
        });
    }
}
