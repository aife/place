<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\Agent\ParametersModulesController;
use App\Controller\Agent\ParametersProcedures;
use App\Controller\Agent\ParametersProceduresController;
use App\Controller\ApiPlatformCustom\ConsultationDce;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="ProcedureEquivalence")
 * @ORM\Entity(repositoryClass="App\Repository\Procedure\ProcedureEquivalenceRepository")
 */
#[ApiResource(
    iri: 'http://schema.org/ParametersProcedures',
    shortName: 'parameters-equivalances',
    itemOperations: [
        'get_equivalence' => [
            "method" => "GET",
        ],
        'get' => [
            "method" => "GET",
            "path" => "/parameters-procedures/{id}",
            'controller' => ParametersProceduresController::class,
            'read' => false,
        ],
    ],
    collectionOperations: [
        'get_list_equivalence' => [
            'method' => 'GET'
        ],
        'get' => [
            'method' => 'GET',
            'path' => '/parameters-modules',
            'controller' => ParametersModulesController::class,
            'read' => false,
        ],
    ]
)]
class ProcedureEquivalence
{
    public const FOREIGN_KEYS = [
        'organisme' => [
            'class' => Organisme::class,
            'property' => 'acronyme',
            'isObject' => false,
            'migrationService' => 'atexo.service.migration.organisme',
        ],
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id_type_procedure", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idTypeProcedure = '0';

    /**
     *
     * @ORM\Column(name="organisme", type="string", length=30, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private ?string $organisme = '';

    /**
     * @ORM\Column(name="elec_resp", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $elecResp = '';

    /**
     * @ORM\Column(name="no_elec_resp", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $noElecResp = '';

    /**
     * @ORM\Column(name="cipher_enabled", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $cipherEnabled = '';

    /**
     * @ORM\Column(name="cipher_disabled", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $cipherDisabled = '';

    /**
     * @ORM\Column(name="signature_enabled", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $signatureEnabled = '';

    /**
     * @ORM\Column(name="signature_disabled", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $signatureDisabled = '';

    /**
     * @ORM\Column(name="env_candidature", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $envCandidature = '';

    /**
     * @ORM\Column(name="env_offre", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $envOffre = '';

    /**
     * @ORM\Column(name="env_anonymat", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $envAnonymat = '';

    /**
     * @ORM\Column(name="envoi_complet", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $envoiComplet = '';

    /**
     * @ORM\Column(name="envoi_differe", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $envoiDiffere = '';

    /**
     * @ORM\Column(name="procedure_publicite", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $procedurePublicite = '';

    /**
     * @ORM\Column(
     *      name="procedure_restreinte_candidature",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"fixed"=true}
     * )
     */
    private ?string $procedureRestreinteCandidature = '';

    /**
     * @ORM\Column(name="procedure_restreinte_offre", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $procedureRestreinteOffre = '';

    /**
     * @ORM\Column(name="envoi_mail_par_mpe", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $envoiMailParMpe = '0';

    /**
     * @ORM\Column(name="no_envoi_mail_par_mpe", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $noEnvoiMailParMpe = '0';

    /**
     * @ORM\Column(name="mise_en_ligne1", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $miseEnLigne1 = '';

    /**
     * @ORM\Column(name="mise_en_ligne2", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $miseEnLigne2 = '';

    /**
     * @ORM\Column(name="mise_en_ligne3", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $miseEnLigne3 = '';

    /**
     * @ORM\Column(name="mise_en_ligne4", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $miseEnLigne4 = '';

    /**
     * @ORM\Column(name="env_offre_type_unique", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $envOffreTypeUnique = '';

    /**
     * @ORM\Column(name="env_offre_type_multiple", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $envOffreTypeMultiple = '';

    /**
     * @ORM\Column(name="no_fichier_annonce", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $noFichierAnnonce = '';

    /**
     * @ORM\Column(name="fichier_importe", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $fichierImporte = '';

    /**
     * @ORM\Column(name="fichier_boamp", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $fichierBoamp = '';

    /**
     * @ORM\Column(name="reglement_cons", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $reglementCons = '';

    /**
     * @ORM\Column(name="dossier_dce", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $dossierDce = '';

    /**
     * @ORM\Column(name="partial_dce_download", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $partialDceDownload = '';

    /**
     * @ORM\Column(name="service", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $service = '';

    /**
     * @ORM\Column(name="constitution_dossier_reponse", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $constitutionDossierReponse = '';

    /**
     * @ORM\Column(name="env_offre_type_unique2", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $envOffreTypeUnique2 = '';

    /**
     * @ORM\Column(name="env_offre_type_multiple2", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $envOffreTypeMultiple2 = '';

    /**
     * @ORM\Column(name="gestion_envois_postaux", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $gestionEnvoisPostaux = '';

    /**
     * @ORM\Column(name="tireur_plan_non", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $tireurPlanNon = '';

    /**
     * @ORM\Column(name="tireur_plan_oui", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $tireurPlanOui = '';

    /**
     * @ORM\Column(name="tireur_plan_papier", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $tireurPlanPapier = '';

    /**
     * @ORM\Column(name="tireur_plan_cdrom", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $tireurPlanCdrom = '';

    /**
     * @ORM\Column(name="tireur_plan_nom", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $tireurPlanNom = '';

    /**
     * @ORM\Column(name="tirage_descriptif", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $tirageDescriptif = '';

    /**
     * @ORM\Column(
     *      name="delai_date_limite_remise_pli",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"fixed"=true}
     * )
     */
    private ?string $delaiDateLimiteRemisePli = '';

    /**
     * @ORM\Column(name="signature_propre", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $signaturePropre = '';

    /**
     * @ORM\Column(name="procedure_restreinte", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $procedureRestreinte = '';

    /**
     * @ORM\Column(name="ouverture_simultanee", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private ?string $ouvertureSimultanee = '';

    /**
     * @ORM\Column(
     *      name="type_decision_a_renseigner",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"default"="-0","fixed"=true}
     * )
     */
    private ?string $typeDecisionARenseigner = '-0';

    /**
     * @ORM\Column(
     *      name="type_decision_attribution_marche",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"default"="-0","fixed"=true}
     * )
     */
    private ?string $typeDecisionAttributionMarche = '-0';

    /**
     * @ORM\Column(
     *      name="type_decision_declaration_sans_suite",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"default"="-0","fixed"=true}
     * )
     */
    private ?string $typeDecisionDeclarationSansSuite = '-0';

    /**
     * @ORM\Column(
     *      name="type_decision_declaration_infructueux",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"default"="-0","fixed"=true}
     * )
     */
    private ?string $typeDecisionDeclarationInfructueux = '-0';

    /**
     * @ORM\Column(
     *      name="type_decision_selection_entreprise",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"default"="-0","fixed"=true}
     * )
     */
    private ?string $typeDecisionSelectionEntreprise = '-0';

    /**
     * @ORM\Column(
     *      name="type_decision_attribution_accord_cadre",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"default"="-0","fixed"=true}
     * )
     */
    private ?string $typeDecisionAttributionAccordCadre = '-0';

    /**
     * @ORM\Column(
     *      name="type_decision_admission_sad",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"default"="-0","fixed"=true}
     * )
     */
    private ?string $typeDecisionAdmissionSad = '-0';

    /**
     * @ORM\Column(
     *     name="type_decision_autre",
     *     type="string",
     *     length=2,
     *     nullable=false,
     *     options={"default"="-0","fixed"=true}
     * )
     */
    private ?string $typeDecisionAutre = '-0';

    /**
     * @ORM\Column(
     *     name="env_offre_technique",
     *     type="string",
     *     length=2,
     *     nullable=false,
     *     options={"default"="-0","fixed"=true}
     * )
     */
    private ?string $envOffreTechnique = '-0';

    /**
     * @ORM\Column(
     *     name="env_offre_technique_type_unique",
     *     type="string",
     *     length=2,
     *     nullable=false,
     *     options={"default"="-0","fixed"=true}
     * )
     */
    private ?string $envOffreTechniqueTypeUnique = '-0';

    /**
     * @ORM\Column(
     *     name="env_offre_technique_type_multiple",
     *     type="string",
     *     length=2,
     *     nullable=false,
     *     options={"default"="-0","fixed"=true}
     * )
     */
    private ?string $envOffreTechniqueTypeMultiple = '-0';

    /**
     * @ORM\Column(name="rep_obligatoire", type="string", length=2, nullable=false, options={"default"="-0"})
     */
    private ?string $repObligatoire = '-0';

    /**
     * @ORM\Column(name="no_rep_obligatoire", type="string", length=2, nullable=false, options={"default"="-0"})
     */
    private ?string $noRepObligatoire = '-0';

    /**
     * @ORM\Column(
     *     name="autre_piece_cons",
     *     type="string",
     *     length=2,
     *     nullable=false,
     *     options={"default"="-0","fixed"=true}
     * )
     */
    private ?string $autrePieceCons = '-0';

    /**
     * @ORM\Column(
     *     name="resp_elec_autre_plateforme",
     *     type="string",
     *     length=2,
     *     nullable=false,
     *     options={"default"="-0","fixed"=true}
     * )
     */
    private ?string $respElecAutrePlateforme = '-0';

    /**
     * @ORM\Column(
     *      name="mise_en_ligne_entite_coordinatrice",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"default"="-0","fixed"=true}
     * )
     */
    private ?string $miseEnLigneEntiteCoordinatrice = '-0';

    /**
     * @ORM\Column(
     *      name="autoriser_publicite",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"default"="1"}
     * )
     */
    private ?string $autoriserPublicite = '1';

    /**
     * @ORM\Column(
     *      name="poursuite_date_limite_remise_pli",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"fixed"=true}
     * )
     */
    private ?string $poursuiteDateLimiteRemisePli = null;

    /**
     * @ORM\Column(
     *      name="delai_poursuite_affichage",
     *      type="string",
     *      length=10,
     *      nullable=false,
     *      options={"comment"="Permet de stocker le delai de poursuite de l'affiche"}
     * )
     */
    private ?string $delaiPoursuiteAffichage = null;

    /**
     * @ORM\Column(
     *      name="delai_poursuivre_affichage_unite",
     *      type="string",
     *      length=0,
     *      nullable=false,
     *      options={"default"="DAY"}
     * )
     */
    private ?string $delaiPoursuivreAffichageUnite = 'DAY';

    /**
     * @ORM\Column(
     *      name="mode_ouverture_dossier",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"default"="+1","fixed"=true}
     * )
     */
    private ?string $modeOuvertureDossier = '+1';

    /**
     * @ORM\Column(
     *      name="mode_ouverture_reponse",
     *      type="string",
     *      length=2, nullable=false,
     *      options={"default"="+0","fixed"=true}
     * )
     */
    private ?string $modeOuvertureReponse = '+0';

    /**
     * @ORM\Column(
     *      name="marche_public_simplifie",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"default"="-0"}
     * )
     */
    private ?string $marchePublicSimplifie = '-0';

    /**
     * @ORM\Column(
     *      name="dume_demande",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"default"="-0"}
     * )
     */
    private ?string $dumeDemande = '-0';

    /**
     * @ORM\Column(
     *      name="type_procedure_dume",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"default"="-0"}
     * )
     */
    private ?string $typeProcedureDume = '-0';

    /**
     * @ORM\Column(
     *      name="type_formulaire_dume_standard",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"default"="+1"}
     * )
     */
    private ?string $typeFormulaireDumeStandard = '+1';

    /**
     * @ORM\Column(
     *      name="type_formulaire_dume_simplifie",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"default"="+0"}
     * )
     */
    private ?string $typeFormulaireDumeSimplifie = '+0';

    /**
     * @ORM\Column(
     *      name="afficher_code_cpv",
     *      type="string",
     *      length=2,
     *      nullable=true,
     *      options={"default"="1"}
     * )
     */
    private ?string $afficherCodeCpv = '1';

    /**
     * @ORM\Column(
     *      name="code_cpv_obligatoire",
     *      type="string",
     *      length=2,
     *      nullable=true,
     *      options={"default"="1"}
     * )
     */
    private ?string $codeCpvObligatoire = '1';

    /**
     * @ORM\Column(
     *      name="donnees_complementaire_non",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"default"="+0"}
     * )
     */
    private ?string $donneesComplementaireNon = '+0';

    /**
     * @ORM\Column(
     *      name="donnees_complementaire_oui",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"default"="+1"}
     * )
     */
    private ?string $donneesComplementaireOui = '+1';

    /**
     * @ORM\Column(
     *      name="signature_autoriser",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"default"="+0","fixed"=true}
     * )
     */
    private ?string $signatureAutoriser = '+0';

    /**
     * @ORM\Column(type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private string $annexeFinanciere = '';

    /**
     * @ORM\Column(
     *      name="autorisation_dossiers_volumineux_envol_non",
     *      type="string",
     *      length=2,
     *      nullable=false,
     *      options={"default"="+1"}
     * )
     */
    private string $autorisationDossiersVolumineuxEnvolNon = '+1';

    /**
     * @ORM\Column(
     *     name="autorisation_dossiers_volumineux_envol_oui",
     *     type="string",
     *     length=2,
     *     nullable=false,
     *     options={"default"="+0"}
     * )
     */
    private string $autorisationDossiersVolumineuxEnvolOui = '+0';

    /**
     * @ORM\Column(name="autoriser_publicite_non", type="string", length=2, nullable=false, options={"default"="+0"})
     */
    private string $autoriserPubliciteNon = '+0';

    /**
     * @ORM\Column(name="autoriser_publicite_oui", type="string", length=2, nullable=false, options={"default"="+1"})
     */
    private string $autoriserPubliciteOui = '+1';

    public function getIdTypeProcedure(): ?int
    {
        return $this->idTypeProcedure;
    }

    public function setIdTypeProcedure(?int $idTypeProcedure): self
    {
        $this->idTypeProcedure = $idTypeProcedure;

        return $this;
    }

    public function getOrganisme(): ?string
    {
        return $this->organisme;
    }

    public function setOrganisme(?string $organisme): self
    {
        $this->organisme = $organisme;
        return $this ;
    }

    public function getElecResp(): ?string
    {
        return $this->elecResp;
    }

    public function setElecResp(string $elecResp): self
    {
        $this->elecResp = $elecResp;

        return $this;
    }

    public function getNoElecResp(): ?string
    {
        return $this->noElecResp;
    }

    public function setNoElecResp(string $noElecResp): self
    {
        $this->noElecResp = $noElecResp;

        return $this;
    }

    public function getCipherEnabled(): ?string
    {
        return $this->cipherEnabled;
    }

    public function setCipherEnabled(string $cipherEnabled): self
    {
        $this->cipherEnabled = $cipherEnabled;

        return $this;
    }

    public function getCipherDisabled(): ?string
    {
        return $this->cipherDisabled;
    }

    public function setCipherDisabled(string $cipherDisabled): self
    {
        $this->cipherDisabled = $cipherDisabled;

        return $this;
    }

    public function getSignatureEnabled(): ?string
    {
        return $this->signatureEnabled;
    }

    public function setSignatureEnabled(string $signatureEnabled): self
    {
        $this->signatureEnabled = $signatureEnabled;

        return $this;
    }

    public function getSignatureDisabled(): ?string
    {
        return $this->signatureDisabled;
    }

    public function setSignatureDisabled(string $signatureDisabled): self
    {
        $this->signatureDisabled = $signatureDisabled;

        return $this;
    }

    public function getEnvCandidature(): ?string
    {
        return $this->envCandidature;
    }

    public function setEnvCandidature(string $envCandidature): self
    {
        $this->envCandidature = $envCandidature;

        return $this;
    }

    public function getEnvOffre(): ?string
    {
        return $this->envOffre;
    }

    public function setEnvOffre(string $envOffre): self
    {
        $this->envOffre = $envOffre;

        return $this;
    }

    public function getEnvAnonymat(): ?string
    {
        return $this->envAnonymat;
    }

    public function setEnvAnonymat(string $envAnonymat): self
    {
        $this->envAnonymat = $envAnonymat;

        return $this;
    }

    public function getEnvoiComplet(): ?string
    {
        return $this->envoiComplet;
    }

    public function setEnvoiComplet(string $envoiComplet): self
    {
        $this->envoiComplet = $envoiComplet;

        return $this;
    }

    public function getEnvoiDiffere(): ?string
    {
        return $this->envoiDiffere;
    }

    public function setEnvoiDiffere(string $envoiDiffere): self
    {
        $this->envoiDiffere = $envoiDiffere;

        return $this;
    }

    public function getProcedurePublicite(): ?string
    {
        return $this->procedurePublicite;
    }

    public function setProcedurePublicite(string $procedurePublicite): self
    {
        $this->procedurePublicite = $procedurePublicite;

        return $this;
    }

    public function getProcedureRestreinteCandidature(): ?string
    {
        return $this->procedureRestreinteCandidature;
    }

    public function setProcedureRestreinteCandidature(string $procedureRestreinteCandidature): self
    {
        $this->procedureRestreinteCandidature = $procedureRestreinteCandidature;

        return $this;
    }

    public function getProcedureRestreinteOffre(): ?string
    {
        return $this->procedureRestreinteOffre;
    }

    public function setProcedureRestreinteOffre(string $procedureRestreinteOffre): self
    {
        $this->procedureRestreinteOffre = $procedureRestreinteOffre;

        return $this;
    }

    public function getEnvoiMailParMpe(): ?string
    {
        return $this->envoiMailParMpe;
    }

    public function setEnvoiMailParMpe(string $envoiMailParMpe): self
    {
        $this->envoiMailParMpe = $envoiMailParMpe;

        return $this;
    }

    public function getNoEnvoiMailParMpe(): ?string
    {
        return $this->noEnvoiMailParMpe;
    }

    public function setNoEnvoiMailParMpe(string $noEnvoiMailParMpe): self
    {
        $this->noEnvoiMailParMpe = $noEnvoiMailParMpe;

        return $this;
    }

    public function getMiseEnLigne1(): ?string
    {
        return $this->miseEnLigne1;
    }

    public function setMiseEnLigne1(string $miseEnLigne1): self
    {
        $this->miseEnLigne1 = $miseEnLigne1;

        return $this;
    }

    public function getMiseEnLigne2(): ?string
    {
        return $this->miseEnLigne2;
    }

    public function setMiseEnLigne2(string $miseEnLigne2): self
    {
        $this->miseEnLigne2 = $miseEnLigne2;

        return $this;
    }

    public function getMiseEnLigne3(): ?string
    {
        return $this->miseEnLigne3;
    }

    public function setMiseEnLigne3(string $miseEnLigne3): self
    {
        $this->miseEnLigne3 = $miseEnLigne3;

        return $this;
    }

    public function getMiseEnLigne4(): ?string
    {
        return $this->miseEnLigne4;
    }

    public function setMiseEnLigne4(string $miseEnLigne4): self
    {
        $this->miseEnLigne4 = $miseEnLigne4;

        return $this;
    }

    public function getEnvOffreTypeUnique(): ?string
    {
        return $this->envOffreTypeUnique;
    }

    public function setEnvOffreTypeUnique(string $envOffreTypeUnique): self
    {
        $this->envOffreTypeUnique = $envOffreTypeUnique;

        return $this;
    }

    public function getEnvOffreTypeMultiple(): ?string
    {
        return $this->envOffreTypeMultiple;
    }

    public function setEnvOffreTypeMultiple(string $envOffreTypeMultiple): self
    {
        $this->envOffreTypeMultiple = $envOffreTypeMultiple;

        return $this;
    }

    public function getNoFichierAnnonce(): ?string
    {
        return $this->noFichierAnnonce;
    }

    public function setNoFichierAnnonce(string $noFichierAnnonce): self
    {
        $this->noFichierAnnonce = $noFichierAnnonce;

        return $this;
    }

    public function getFichierImporte(): ?string
    {
        return $this->fichierImporte;
    }

    public function setFichierImporte(string $fichierImporte): self
    {
        $this->fichierImporte = $fichierImporte;

        return $this;
    }

    public function getFichierBoamp(): ?string
    {
        return $this->fichierBoamp;
    }

    public function setFichierBoamp(string $fichierBoamp): self
    {
        $this->fichierBoamp = $fichierBoamp;

        return $this;
    }

    public function getReglementCons(): ?string
    {
        return $this->reglementCons;
    }

    public function setReglementCons(string $reglementCons): self
    {
        $this->reglementCons = $reglementCons;

        return $this;
    }

    public function getDossierDce(): ?string
    {
        return $this->dossierDce;
    }

    public function setDossierDce(string $dossierDce): self
    {
        $this->dossierDce = $dossierDce;

        return $this;
    }

    public function getPartialDceDownload(): ?string
    {
        return $this->partialDceDownload;
    }

    public function setPartialDceDownload(string $partialDceDownload): self
    {
        $this->partialDceDownload = $partialDceDownload;

        return $this;
    }

    public function getService(): ?string
    {
        return $this->service;
    }

    public function setService(string $service): self
    {
        $this->service = $service;

        return $this;
    }

    public function getConstitutionDossierReponse(): ?string
    {
        return $this->constitutionDossierReponse;
    }

    public function setConstitutionDossierReponse(string $constitutionDossierReponse): self
    {
        $this->constitutionDossierReponse = $constitutionDossierReponse;

        return $this;
    }

    public function getEnvOffreTypeUnique2(): ?string
    {
        return $this->envOffreTypeUnique2;
    }

    public function setEnvOffreTypeUnique2(string $envOffreTypeUnique2): self
    {
        $this->envOffreTypeUnique2 = $envOffreTypeUnique2;

        return $this;
    }

    public function getEnvOffreTypeMultiple2(): ?string
    {
        return $this->envOffreTypeMultiple2;
    }

    public function setEnvOffreTypeMultiple2(string $envOffreTypeMultiple2): self
    {
        $this->envOffreTypeMultiple2 = $envOffreTypeMultiple2;

        return $this;
    }

    public function getGestionEnvoisPostaux(): ?string
    {
        return $this->gestionEnvoisPostaux;
    }

    public function setGestionEnvoisPostaux(string $gestionEnvoisPostaux): self
    {
        $this->gestionEnvoisPostaux = $gestionEnvoisPostaux;

        return $this;
    }

    public function getTireurPlanNon(): ?string
    {
        return $this->tireurPlanNon;
    }

    public function setTireurPlanNon(string $tireurPlanNon): self
    {
        $this->tireurPlanNon = $tireurPlanNon;

        return $this;
    }

    public function getTireurPlanOui(): ?string
    {
        return $this->tireurPlanOui;
    }

    public function setTireurPlanOui(string $tireurPlanOui): self
    {
        $this->tireurPlanOui = $tireurPlanOui;

        return $this;
    }

    public function getTireurPlanPapier(): ?string
    {
        return $this->tireurPlanPapier;
    }

    public function setTireurPlanPapier(string $tireurPlanPapier): self
    {
        $this->tireurPlanPapier = $tireurPlanPapier;

        return $this;
    }

    public function getTireurPlanCdrom(): ?string
    {
        return $this->tireurPlanCdrom;
    }

    public function setTireurPlanCdrom(string $tireurPlanCdrom): self
    {
        $this->tireurPlanCdrom = $tireurPlanCdrom;

        return $this;
    }

    public function getTireurPlanNom(): ?string
    {
        return $this->tireurPlanNom;
    }

    public function setTireurPlanNom(string $tireurPlanNom): self
    {
        $this->tireurPlanNom = $tireurPlanNom;

        return $this;
    }

    public function getTirageDescriptif(): ?string
    {
        return $this->tirageDescriptif;
    }

    public function setTirageDescriptif(string $tirageDescriptif): self
    {
        $this->tirageDescriptif = $tirageDescriptif;

        return $this;
    }

    public function getDelaiDateLimiteRemisePli(): ?string
    {
        return $this->delaiDateLimiteRemisePli;
    }

    public function setDelaiDateLimiteRemisePli(string $delaiDateLimiteRemisePli): self
    {
        $this->delaiDateLimiteRemisePli = $delaiDateLimiteRemisePli;

        return $this;
    }

    public function getSignaturePropre(): ?string
    {
        return $this->signaturePropre;
    }

    public function setSignaturePropre(string $signaturePropre): self
    {
        $this->signaturePropre = $signaturePropre;

        return $this;
    }

    public function getProcedureRestreinte(): ?string
    {
        return $this->procedureRestreinte;
    }

    public function setProcedureRestreinte(string $procedureRestreinte): self
    {
        $this->procedureRestreinte = $procedureRestreinte;

        return $this;
    }

    public function getOuvertureSimultanee(): ?string
    {
        return $this->ouvertureSimultanee;
    }

    public function setOuvertureSimultanee(string $ouvertureSimultanee): self
    {
        $this->ouvertureSimultanee = $ouvertureSimultanee;

        return $this;
    }

    public function getTypeDecisionARenseigner(): ?string
    {
        return $this->typeDecisionARenseigner;
    }

    public function setTypeDecisionARenseigner(string $typeDecisionARenseigner): self
    {
        $this->typeDecisionARenseigner = $typeDecisionARenseigner;

        return $this;
    }

    public function getTypeDecisionAttributionMarche(): ?string
    {
        return $this->typeDecisionAttributionMarche;
    }

    public function setTypeDecisionAttributionMarche(string $typeDecisionAttributionMarche): self
    {
        $this->typeDecisionAttributionMarche = $typeDecisionAttributionMarche;

        return $this;
    }

    public function getTypeDecisionDeclarationSansSuite(): ?string
    {
        return $this->typeDecisionDeclarationSansSuite;
    }

    public function setTypeDecisionDeclarationSansSuite(string $typeDecisionDeclarationSansSuite): self
    {
        $this->typeDecisionDeclarationSansSuite = $typeDecisionDeclarationSansSuite;

        return $this;
    }

    public function getTypeDecisionDeclarationInfructueux(): ?string
    {
        return $this->typeDecisionDeclarationInfructueux;
    }

    public function setTypeDecisionDeclarationInfructueux(string $typeDecisionDeclarationInfructueux): self
    {
        $this->typeDecisionDeclarationInfructueux = $typeDecisionDeclarationInfructueux;

        return $this;
    }

    public function getTypeDecisionSelectionEntreprise(): ?string
    {
        return $this->typeDecisionSelectionEntreprise;
    }

    public function setTypeDecisionSelectionEntreprise(string $typeDecisionSelectionEntreprise): self
    {
        $this->typeDecisionSelectionEntreprise = $typeDecisionSelectionEntreprise;

        return $this;
    }

    public function getTypeDecisionAttributionAccordCadre(): ?string
    {
        return $this->typeDecisionAttributionAccordCadre;
    }

    public function setTypeDecisionAttributionAccordCadre(string $typeDecisionAttributionAccordCadre): self
    {
        $this->typeDecisionAttributionAccordCadre = $typeDecisionAttributionAccordCadre;

        return $this;
    }

    public function getTypeDecisionAdmissionSad(): ?string
    {
        return $this->typeDecisionAdmissionSad;
    }

    public function setTypeDecisionAdmissionSad(string $typeDecisionAdmissionSad): self
    {
        $this->typeDecisionAdmissionSad = $typeDecisionAdmissionSad;

        return $this;
    }

    public function getTypeDecisionAutre(): ?string
    {
        return $this->typeDecisionAutre;
    }

    public function setTypeDecisionAutre(string $typeDecisionAutre): self
    {
        $this->typeDecisionAutre = $typeDecisionAutre;

        return $this;
    }

    public function getEnvOffreTechnique(): ?string
    {
        return $this->envOffreTechnique;
    }

    public function setEnvOffreTechnique(string $envOffreTechnique): self
    {
        $this->envOffreTechnique = $envOffreTechnique;

        return $this;
    }

    public function getEnvOffreTechniqueTypeUnique(): ?string
    {
        return $this->envOffreTechniqueTypeUnique;
    }

    public function setEnvOffreTechniqueTypeUnique(string $envOffreTechniqueTypeUnique): self
    {
        $this->envOffreTechniqueTypeUnique = $envOffreTechniqueTypeUnique;

        return $this;
    }

    public function getEnvOffreTechniqueTypeMultiple(): ?string
    {
        return $this->envOffreTechniqueTypeMultiple;
    }

    public function setEnvOffreTechniqueTypeMultiple(string $envOffreTechniqueTypeMultiple): self
    {
        $this->envOffreTechniqueTypeMultiple = $envOffreTechniqueTypeMultiple;

        return $this;
    }

    public function getRepObligatoire(): ?string
    {
        return $this->repObligatoire;
    }

    public function setRepObligatoire(string $repObligatoire): self
    {
        $this->repObligatoire = $repObligatoire;

        return $this;
    }

    public function getNoRepObligatoire(): ?string
    {
        return $this->noRepObligatoire;
    }

    public function setNoRepObligatoire(string $noRepObligatoire): self
    {
        $this->noRepObligatoire = $noRepObligatoire;

        return $this;
    }

    public function getAutrePieceCons(): ?string
    {
        return $this->autrePieceCons;
    }

    public function setAutrePieceCons(string $autrePieceCons): self
    {
        $this->autrePieceCons = $autrePieceCons;

        return $this;
    }

    public function getRespElecAutrePlateforme(): ?string
    {
        return $this->respElecAutrePlateforme;
    }

    public function setRespElecAutrePlateforme(string $respElecAutrePlateforme): self
    {
        $this->respElecAutrePlateforme = $respElecAutrePlateforme;

        return $this;
    }

    public function getMiseEnLigneEntiteCoordinatrice(): ?string
    {
        return $this->miseEnLigneEntiteCoordinatrice;
    }

    public function setMiseEnLigneEntiteCoordinatrice(string $miseEnLigneEntiteCoordinatrice): self
    {
        $this->miseEnLigneEntiteCoordinatrice = $miseEnLigneEntiteCoordinatrice;

        return $this;
    }

    public function getAutoriserPublicite(): ?string
    {
        return $this->autoriserPublicite;
    }

    public function setAutoriserPublicite(string $autoriserPublicite): self
    {
        $this->autoriserPublicite = $autoriserPublicite;

        return $this;
    }

    public function getPoursuiteDateLimiteRemisePli(): ?string
    {
        return $this->poursuiteDateLimiteRemisePli;
    }

    public function setPoursuiteDateLimiteRemisePli(string $poursuiteDateLimiteRemisePli): self
    {
        $this->poursuiteDateLimiteRemisePli = $poursuiteDateLimiteRemisePli;

        return $this;
    }

    public function getDelaiPoursuiteAffichage(): ?string
    {
        return $this->delaiPoursuiteAffichage;
    }

    public function setDelaiPoursuiteAffichage(string $delaiPoursuiteAffichage): self
    {
        $this->delaiPoursuiteAffichage = $delaiPoursuiteAffichage;

        return $this;
    }

    public function getDelaiPoursuivreAffichageUnite(): ?string
    {
        return $this->delaiPoursuivreAffichageUnite;
    }

    public function setDelaiPoursuivreAffichageUnite(string $delaiPoursuivreAffichageUnite): self
    {
        $this->delaiPoursuivreAffichageUnite = $delaiPoursuivreAffichageUnite;

        return $this;
    }

    public function getModeOuvertureDossier(): ?string
    {
        return $this->modeOuvertureDossier;
    }

    public function setModeOuvertureDossier(string $modeOuvertureDossier): self
    {
        $this->modeOuvertureDossier = $modeOuvertureDossier;

        return $this;
    }

    public function getModeOuvertureReponse(): ?string
    {
        return $this->modeOuvertureReponse;
    }

    public function setModeOuvertureReponse(string $modeOuvertureReponse): self
    {
        $this->modeOuvertureReponse = $modeOuvertureReponse;

        return $this;
    }

    public function getMarchePublicSimplifie(): ?string
    {
        return $this->marchePublicSimplifie;
    }

    public function setMarchePublicSimplifie(string $marchePublicSimplifie): self
    {
        $this->marchePublicSimplifie = $marchePublicSimplifie;

        return $this;
    }

    public function getDumeDemande(): ?string
    {
        return $this->dumeDemande;
    }

    public function setDumeDemande(string $dumeDemande): self
    {
        $this->dumeDemande = $dumeDemande;

        return $this;
    }

    public function getTypeProcedureDume(): ?string
    {
        return $this->typeProcedureDume;
    }

    public function setTypeProcedureDume(string $typeProcedureDume): self
    {
        $this->typeProcedureDume = $typeProcedureDume;

        return $this;
    }

    public function getTypeFormulaireDumeStandard(): ?string
    {
        return $this->typeFormulaireDumeStandard;
    }

    public function setTypeFormulaireDumeStandard(string $typeFormulaireDumeStandard): self
    {
        $this->typeFormulaireDumeStandard = $typeFormulaireDumeStandard;

        return $this;
    }

    public function getTypeFormulaireDumeSimplifie(): ?string
    {
        return $this->typeFormulaireDumeSimplifie;
    }

    public function setTypeFormulaireDumeSimplifie(string $typeFormulaireDumeSimplifie): self
    {
        $this->typeFormulaireDumeSimplifie = $typeFormulaireDumeSimplifie;

        return $this;
    }

    public function getAfficherCodeCpv(): ?string
    {
        return $this->afficherCodeCpv;
    }

    public function setAfficherCodeCpv(?string $afficherCodeCpv): self
    {
        $this->afficherCodeCpv = $afficherCodeCpv;

        return $this;
    }

    public function getCodeCpvObligatoire(): ?string
    {
        return $this->codeCpvObligatoire;
    }

    public function setCodeCpvObligatoire(?string $codeCpvObligatoire): self
    {
        $this->codeCpvObligatoire = $codeCpvObligatoire;

        return $this;
    }

    public function getDonneesComplementaireNon(): ?string
    {
        return $this->donneesComplementaireNon;
    }

    public function setDonneesComplementaireNon(string $donneesComplementaireNon): self
    {
        $this->donneesComplementaireNon = $donneesComplementaireNon;

        return $this;
    }

    public function getDonneesComplementaireOui(): ?string
    {
        return $this->donneesComplementaireOui;
    }

    public function setDonneesComplementaireOui(string $donneesComplementaireOui): self
    {
        $this->donneesComplementaireOui = $donneesComplementaireOui;

        return $this;
    }

    public function getSignatureAutoriser(): ?string
    {
        return $this->signatureAutoriser;
    }

    public function setSignatureAutoriser(string $signatureAutoriser): self
    {
        $this->signatureAutoriser = $signatureAutoriser;

        return $this;
    }

    public function getAnnexeFinanciere(): string
    {
        return $this->annexeFinanciere;
    }

    public function setAnnexeFinanciere(string $annexeFinanciere): ProcedureEquivalence
    {
        $this->annexeFinanciere = $annexeFinanciere;

        return $this;
    }

    public function getAutorisationDossiersVolumineuxEnvolNon(): string
    {
        return $this->autorisationDossiersVolumineuxEnvolNon;
    }

    public function setAutorisationDossiersVolumineuxEnvolNon(string $autorisationDossiersVolumineuxEnvolNon): self
    {
        $this->autorisationDossiersVolumineuxEnvolNon = $autorisationDossiersVolumineuxEnvolNon;

        return $this;
    }

    public function getAutorisationDossiersVolumineuxEnvolOui(): string
    {
        return $this->autorisationDossiersVolumineuxEnvolOui;
    }

    public function setAutorisationDossiersVolumineuxEnvolOui(string $autorisationDossiersVolumineuxEnvolOui): self
    {
        $this->autorisationDossiersVolumineuxEnvolOui = $autorisationDossiersVolumineuxEnvolOui;

        return $this;
    }

    public function getAutoriserPubliciteNon(): string
    {
        return $this->autoriserPubliciteNon;
    }

    public function setAutoriserPubliciteNon(string $autoriserPubliciteNon): self
    {
        $this->autoriserPubliciteNon = $autoriserPubliciteNon;

        return $this;
    }

    public function getAutoriserPubliciteOui(): string
    {
        return $this->autoriserPubliciteOui;
    }

    public function setAutoriserPubliciteOui(string $autoriserPubliciteOui): self
    {
        $this->autoriserPubliciteOui = $autoriserPubliciteOui;

        return $this;
    }

}
