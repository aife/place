<?php

namespace App\Entity;

use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Repository\HabilitationTypeProcedureRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: HabilitationTypeProcedureRepository::class)]
/**
 * @ORM\Entity(repositoryClass=HabilitationTypeProcedureRepository::class)
 */
class HabilitationTypeProcedure
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    /**
     * @ORM\ManyToOne(targetEntity=ReferentielHabilitation::class)
     * @ORM\JoinColumn(nullable= false)
     */
    private ?ReferentielHabilitation $habilitation = null;

    #[ORM\ManyToOne(inversedBy: 'habilitationTypeProcedures')]
    #[ORM\JoinColumn(nullable: false)]
    /**
     * @ORM\ManyToOne(targetEntity=Agent::class, inversedBy="habilitationTypeProcedures")
     * @ORM\JoinColumn(nullable= false)
     */
    private ?Agent $agent = null;

    #[ORM\Column(nullable: true)]
    /**
     * @ORM\Column(type="integer", nullable= true)
     */
    private ?int $typeProcedure = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    /**
     * @ORM\ManyToOne(targetEntity=Organisme::class)
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private ?Organisme $organism = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHabilitation(): ?ReferentielHabilitation
    {
        return $this->habilitation;
    }

    public function setHabilitation(?ReferentielHabilitation $habilitation): static
    {
        $this->habilitation = $habilitation;

        return $this;
    }

    public function getAgent(): ?Agent
    {
        return $this->agent;
    }

    public function setAgent(?Agent $agent): static
    {
        $this->agent = $agent;

        return $this;
    }

    public function getTypeProcedure(): ?int
    {
        return $this->typeProcedure;
    }

    public function setTypeProcedure(?int $typeProcedure): static
    {
        $this->typeProcedure = $typeProcedure;

        return $this;
    }

    public function getOrganism(): ?Organisme
    {
        return $this->organism;
    }

    public function setOrganism(?Organisme $organism): static
    {
        $this->organism = $organism;

        return $this;
    }
}
