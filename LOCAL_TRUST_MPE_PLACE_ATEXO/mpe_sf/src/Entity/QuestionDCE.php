<?php

namespace App\Entity;

use DateTimeInterface;
use DateTime;
use App\Traits\Entity\PlateformeVirtuelleLinkTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * QuestionDCE.
 *
 * @ORM\Table(name="questions_dce")
 * @ORM\Entity(repositoryClass="App\Repository\QuestionDCERepository")
 */
class QuestionDCE
{
    use PlateformeVirtuelleLinkTrait;

    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme", inversedBy="questions")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private ?Organisme $organisme = null;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation", inversedBy="questions")
     * @ORM\JoinColumn(name="consultation_id", referencedColumnName="id")
     */
    private ?Consultation $consultation = null;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Inscrit", inversedBy="questions")
     * @ORM\JoinColumn(name="id_inscrit", referencedColumnName="id")
     */
    private ?Inscrit $inscrit = null;

    /**
     * @var BloborganismeFile\
     *
     * @ORM\OneToOne(targetEntity="App\Entity\BloborganismeFile")
     * @ORM\JoinColumn(name="id_fichier", referencedColumnName="id")
     */
    private ?BloborganismeFile $idFichier = null;

    /**
     * @ORM\Column(name="nom_fichier", type="string", nullable=true)
     */
    private ?string $nomFichier = null;

    /**
     * @ORM\Column(name="date_depot", type="datetime", nullable=false)
     */
    private string|DateTimeInterface|DateTime $dateDepot = '0000-00-00 00:00:00';

    /**
     * @ORM\Column(name="email", type="string", length=100, nullable=false)
     */
    private ?string $email = null;

    /**
     * @ORM\Column(name="nom", type="string", length=80, nullable=true)
     */
    private ?string $nom = null;

    /**
     * @ORM\Column(name="entreprise", type="string", length=50, nullable=true)
     */
    private ?string $entreprise = null;

    /**
     * @ORM\Column(name="adresse", type="string", length=200, nullable=true)
     */
    private ?string $adresse = null;

    /**
     * @ORM\Column(name="cp", type="string", length=100, nullable=true)
     */
    private ?string $cp = null;

    /**
     * @ORM\Column(name="ville", type="string", length=150, nullable=true)
     */
    private ?string $ville = null;

    /**
     * @ORM\Column(name="tel", type="string", length=150, nullable=true)
     */
    private ?string $tel = null;

    /**
     * @ORM\Column(name="fax", type="string", length=150, nullable=true)
     */
    private ?string $fax = null;

    /**
     * @ORM\Column(name="question", type="string", nullable=false)
     */
    private ?string $question = null;

    /**
     * @ORM\Column(name="statut", type="integer", nullable=false)
     */
    private int $statut = 0;

    /**
     * @ORM\Column(name="date_reponse", type="date", nullable=false)
     */
    private string|DateTimeInterface|DateTime $dateReponse = '0000-00-00';

    /**
     * @ORM\Column(name="personne_repondu", type="string", length=256, nullable=true)
     */
    private ?string $personneRepondu = null;

    /**
     * @ORM\Column(name="type_depot", type="string", nullable=false)
     */
    private string $typeDepot = '1';

    /**
     * @ORM\Column(name="pays", type="string", length=150, nullable=true)
     */
    private ?string $pays = null;

    /**
     * @ORM\Column(name="Observation", type="string", length=256, nullable=true)
     */
    private ?string $observation = null;

    /**
     * @ORM\Column(name="siret", type="string", length=14, nullable=true)
     */
    private ?string $siret = null;

    /**
     * @ORM\Column(name="identifiant_national", type="string", length=20, nullable=true)
     */
    private ?string $identifiantNational = null;

    /**
     * @ORM\Column(name="acronyme_pays", type="string", length=10, nullable=true)
     */
    private ?string $acronymePays = null;

    /**
     * @ORM\Column(name="adresse2", type="string", length=80, nullable=true)
     */
    private ?string $adresse2 = null;

    /**
     * @ORM\Column(name="prenom", type="string", length=80, nullable=true)
     */
    private ?string $prenom = null;

    /**
     * @ORM\Column(name="siret_etranger", type="string", length=20, nullable=true)
     */
    private ?string $siretEtranger = null;

    /**
     * @ORM\Column(name="id_entreprise", type="integer", nullable=false)
     */
    private ?int $idEntreprise = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Organisme
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param Organisme $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return Inscrit
     */
    public function getInscrit()
    {
        return $this->inscrit;
    }

    /**
     * @param Inscrit $inscrit
     */
    public function setInscrit($inscrit)
    {
        $this->inscrit = $inscrit;
    }

    /**
     * @return BloborganismeFile
     */
    public function getIdFichier()
    {
        return $this->idFichier;
    }

    /**
     * @param BloborganismeFile $idFichier
     */
    public function setIdFichier($idFichier)
    {
        $this->idFichier = $idFichier;
    }

    /**
     * @return string
     */
    public function getNomFichier()
    {
        return $this->nomFichier;
    }

    /**
     * @param string $nomFichier
     */
    public function setNomFichier($nomFichier)
    {
        $this->nomFichier = $nomFichier;
    }

    /**
     * @return DateTime
     */
    public function getDateDepot()
    {
        return $this->dateDepot;
    }

    /**
     * @param DateTime $dateDepot
     */
    public function setDateDepot($dateDepot)
    {
        $this->dateDepot = $dateDepot;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }

    /**
     * @param string $entreprise
     */
    public function setEntreprise($entreprise)
    {
        $this->entreprise = $entreprise;
    }

    /**
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param string $adresse
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }

    /**
     * @return string
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * @param string $cp
     */
    public function setCp($cp)
    {
        $this->cp = $cp;
    }

    /**
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @param string $ville
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }

    /**
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * @param string $tel
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param string $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param string $question
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    }

    /**
     * @return int
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @param int $statut
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;
    }

    /**
     * @return DateTime
     */
    public function getDateReponse()
    {
        return $this->dateReponse;
    }

    /**
     * @param DateTime $dateReponse
     */
    public function setDateReponse($dateReponse)
    {
        $this->dateReponse = $dateReponse;
    }

    /**
     * @return string
     */
    public function getPersonneRepondu()
    {
        return $this->personneRepondu;
    }

    /**
     * @param string $personneRepondu
     */
    public function setPersonneRepondu($personneRepondu)
    {
        $this->personneRepondu = $personneRepondu;
    }

    /**
     * @return string
     */
    public function getTypeDepot()
    {
        return $this->typeDepot;
    }

    /**
     * @param string $typeDepot
     */
    public function setTypeDepot($typeDepot)
    {
        $this->typeDepot = $typeDepot;
    }

    /**
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * @param string $pays
     */
    public function setPays($pays)
    {
        $this->pays = $pays;
    }

    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }

    /**
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * @param string $siret
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;
    }

    /**
     * @return string
     */
    public function getIdentifiantNational()
    {
        return $this->identifiantNational;
    }

    /**
     * @param string $identifiantNational
     */
    public function setIdentifiantNational($identifiantNational)
    {
        $this->identifiantNational = $identifiantNational;
    }

    /**
     * @return string
     */
    public function getAcronymePays()
    {
        return $this->acronymePays;
    }

    /**
     * @param string $acronymePays
     */
    public function setAcronymePays($acronymePays)
    {
        $this->acronymePays = $acronymePays;
    }

    /**
     * @return string
     */
    public function getAdresse2()
    {
        return $this->adresse2;
    }

    /**
     * @param string $adresse2
     */
    public function setAdresse2($adresse2)
    {
        $this->adresse2 = $adresse2;
    }

    /**
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param string $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @return string
     */
    public function getSiretEtranger()
    {
        return $this->siretEtranger;
    }

    /**
     * @param string $siretEtranger
     */
    public function setSiretEtranger($siretEtranger)
    {
        $this->siretEtranger = $siretEtranger;
    }

    /**
     * @return int
     */
    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    /**
     * @param int $idEntreprise
     */
    public function setIdEntreprise($idEntreprise)
    {
        $this->idEntreprise = $idEntreprise;
    }

    /**
     * @return Consultation
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    /**
     * @return QuestionDCE
     */
    public function setConsultation(Consultation $consultation)
    {
        $this->consultation = $consultation;

        return $this;
    }
}
