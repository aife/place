<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\CustomIdGenerator;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * AccessToken.
 *
 * @ORM\Table(name="t_access_token", indexes={@ORM\Index(name="idenfifiant_acces_token", columns={"id"})})
 * @ORM\Entity(repositoryClass="App\Repository\AccessTokenRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class AccessToken
{
    /**
     * UUID4.
     *
     * @var int
     * @ORM\Column(name="id", type="uuid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @ORM\Column(name="data", type="json")
     */
    private array $data;

    /**
     * @ORM\Column(name="ip", type="string", length=15)
     */
    private ?string $ip = null;

    /**
     * @ORM\Column(name="user_id", type="integer", length=11)
     */
    private ?int $userId = null;

    /**
     * @ORM\Column(name="user_type", type="integer", length=1)
     */
    private ?int $userType = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")*/
    private $createdAt;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data.
     */
    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data.
     *
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Set ip.
     *
     * @param string $ip
     *
     * @return AccessToken
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip.
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return AccessToken
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     *
     * @return AccessToken
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @param int $userType
     *
     * @return AccessToken
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new DateTime();
    }
}
