<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InscritOperationsTracker.
 *
 * @ORM\Table(name="trace_operations_inscrit", indexes={@ORM\Index(name="recherche", columns={"id_inscrit", "id_entreprise", "addr_ip", "date"}), @ORM\Index(name="ref_consutation", columns={"organisme", "consultation_id"})})
 * @ORM\Entity
 */
class InscritOperationsTracker
{
    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\InscritOperationsTrackerDetails", mappedBy="traceOperation")
     * @ORM\JoinColumn(name="id_trace", referencedColumnName="id_trace")
     */
    private Collection $detailsTracesOperations;

    /**
     *
     * @ORM\Column(name="id_trace", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $idTrace;

    /**
     * @ORM\Column(name="id_inscrit", type="integer", nullable=false)
     */
    private ?int $idInscrit = null;

    /**
     * @ORM\Column(name="id_entreprise", type="integer", nullable=false)
     */
    private ?int $idEntreprise = null;

    /**
     * @ORM\Column(name="addr_ip", type="string", length=15, nullable=false)
     */
    private ?string $addrIp = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)*/
    private $date;

    /**
     * @ORM\Column(name="operations", type="text", length=65535, nullable=false)
     */
    private ?string $operations = null;

    /**
     * @ORM\Column(name="organisme", type="string", length=30, nullable=true)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(name="consultation_id", type="string", length=30, nullable=true)
     */
    private ?string $consultationId = null;

    /**
     * @ORM\Column(name="afficher", type="string", nullable=false)
     */
    private string $afficher = '0';

    /**
     * Consultation constructor.
     */
    public function __construct()
    {
        $this->detailsTracesOperations = new ArrayCollection();
    }

    /**
     * Get idTrace.
     *
     * @return int
     */
    public function getIdTrace()
    {
        return $this->idTrace;
    }

    /**
     * Set idInscrit.
     *
     * @param int $idInscrit
     *
     * @return TraceOperationsInscrit
     */
    public function setIdInscrit($idInscrit)
    {
        $this->idInscrit = $idInscrit;

        return $this;
    }

    /**
     * Get idInscrit.
     *
     * @return int
     */
    public function getIdInscrit()
    {
        return $this->idInscrit;
    }

    /**
     * Set idEntreprise.
     *
     * @param int $idEntreprise
     *
     * @return TraceOperationsInscrit
     */
    public function setIdEntreprise($idEntreprise)
    {
        $this->idEntreprise = $idEntreprise;

        return $this;
    }

    /**
     * Get idEntreprise.
     *
     * @return int
     */
    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    /**
     * Set addrIp.
     *
     * @param string $addrIp
     *
     * @return TraceOperationsInscrit
     */
    public function setAddrIp($addrIp)
    {
        $this->addrIp = $addrIp;

        return $this;
    }

    /**
     * Get addrIp.
     *
     * @return string
     */
    public function getAddrIp()
    {
        return $this->addrIp;
    }

    /**
     * Set date.
     *
     * @param DateTime $date
     *
     * @return TraceOperationsInscrit
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set operations.
     *
     * @param string $operations
     *
     * @return TraceOperationsInscrit
     */
    public function setOperations($operations)
    {
        $this->operations = $operations;

        return $this;
    }

    /**
     * Get operations.
     *
     * @return string
     */
    public function getOperations()
    {
        return $this->operations;
    }

    /**
     * Set organisme.
     *
     * @param string $organisme
     *
     * @return TraceOperationsInscrit
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * Get organisme.
     *
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @return string
     */
    public function getConsultationId()
    {
        return $this->consultationId;
    }

    /**
     * @param string $consultationId
     */
    public function setConsultationId($consultationId)
    {
        $this->consultationId = $consultationId;
    }

    /**
     * Set afficher.
     *
     * @param string $afficher
     *
     * @return TraceOperationsInscrit
     */
    public function setAfficher($afficher)
    {
        $this->afficher = $afficher;

        return $this;
    }

    /**
     * Get afficher.
     *
     * @return string
     */
    public function getAfficher()
    {
        return $this->afficher;
    }

    /**
     * @return $this
     */
    public function addDetailsTracesOperation(InscritOperationsTrackerDetails $trackerDetail)
    {
        $this->detailsTracesOperations[] = $trackerDetail;
        $trackerDetail->setTraceOperation($this);

        return $this;
    }

    public function removeDetailsTracesOperation(InscritOperationsTrackerDetails $trackerDetail)
    {
        $this->detailsTracesOperations->removeElement($trackerDetail);
    }

    public function getDetailsTracesOperations(): ArrayCollection
    {
        return $this->detailsTracesOperations;
    }
}
