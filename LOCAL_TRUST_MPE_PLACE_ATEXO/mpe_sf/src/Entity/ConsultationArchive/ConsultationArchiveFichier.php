<?php

namespace App\Entity\ConsultationArchive;

use Stringable;
use Doctrine\ORM\Mapping as ORM;

/**
 * ConsultationArchive.
 *
 * @ORM\Table
 * @ORM\Entity
 */
class ConsultationArchiveFichier implements Stringable
{
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(type="integer")
     */
    private $consultationRef;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private ?string $cheminFichier = null;

    /**
     * @ORM\Column(type="integer")
     */
    private $poids;

    /**
     * @return mixed
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * @param mixed $poids
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return mixed
     */
    public function getConsultationRef()
    {
        return $this->consultationRef;
    }

    /**
     * @param mixed $consultationRef
     */
    public function setConsultationRef($consultationRef)
    {
        $this->consultationRef = $consultationRef;
    }

    public function getCheminFichier(): string
    {
        return $this->cheminFichier;
    }

    public function setCheminFichier(string $cheminFichier)
    {
        $this->cheminFichier = $cheminFichier;
    }

    public function __toString(): string
    {
        return sprintf(
            '%s => %s => %s',
            $this->getOrganisme(),
            $this->getConsultationRef(),
            $this->getCheminFichier()
        );
    }
}
