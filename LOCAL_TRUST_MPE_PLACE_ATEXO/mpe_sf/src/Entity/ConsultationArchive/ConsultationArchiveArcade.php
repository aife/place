<?php

namespace App\Entity\ConsultationArchive;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConsultationArchiveArcade.
 *
 * @ORM\Table(name="consultation_archive_arcade")
 * @ORM\Entity(repositoryClass="App\Repository\ConsultationArchive\ConsultationArchiveArcadeRepository")
 */
class ConsultationArchiveArcade
{
    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="ConsultationArchive")
     * @ORM\JoinColumn(name="consultation_archive_id", referencedColumnName="id")
     */
    private $consultationArchive;

    /**
     * @ORM\ManyToOne(targetEntity="ArchiveArcade")
     * @ORM\JoinColumn(name="archive_arcade_id", referencedColumnName="id", nullable=true)
     */
    private $archiveArcade;

    /**
     * @return mixed
     */
    public function getArchiveArcade()
    {
        return $this->archiveArcade;
    }

    /**
     * @param mixed $archiveArcade
     */
    public function setArchiveArcade($archiveArcade)
    {
        $this->archiveArcade = $archiveArcade;
    }

    /**
     * @return mixed
     */
    public function getConsultationArchive()
    {
        return $this->consultationArchive;
    }

    /**
     * @param mixed $consultationArchive
     */
    public function setConsultationArchive($consultationArchive)
    {
        $this->consultationArchive = $consultationArchive;
    }
}
