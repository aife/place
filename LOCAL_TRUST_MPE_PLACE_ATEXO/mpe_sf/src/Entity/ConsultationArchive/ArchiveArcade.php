<?php

namespace App\Entity\ConsultationArchive;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ArchiveArcade.
 *
 * @ORM\Table(name="archive_arcade")
 * @ORM\Entity(repositoryClass="App\Repository\ConsultationArchive\ArchiveArcadeRepository")
 */
class ArchiveArcade
{
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $annee = null;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $numSemaine = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $poidsArchive = null;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private ?string $cheminFichier = null;

    /**
     * @ORM\Column(type="datetime", nullable=true)*/
    #[Assert\DateTime(message: "Le format attendu est dateTime et non : '{{ value }}'.")]
    private $dateEnvoiDebut;

    /**
     * @ORM\Column(type="datetime", nullable=true)*/
    #[Assert\DateTime(message: "Le format attendu est dateTime et non : '{{ value }}'.")]
    private $dateEnvoiFin;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":false})*/
    #[Assert\Type('boolean')]
    private $statusTransmissionn;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $erreur = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * @param int $annee
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;
    }

    /**
     * @return int
     */
    public function getNumSemaine()
    {
        return $this->numSemaine;
    }

    /**
     * @param int $numSemaine
     */
    public function setNumSemaine($numSemaine)
    {
        $this->numSemaine = $numSemaine;
    }

    /**
     * @return int
     */
    public function getPoidsArchive()
    {
        return $this->poidsArchive;
    }

    /**
     * @param int $poidsArchive
     */
    public function setPoidsArchive($poidsArchive)
    {
        $this->poidsArchive = $poidsArchive;
    }

    /**
     * @return string
     */
    public function getCheminFichier()
    {
        return urldecode($this->cheminFichier);
    }

    /**
     * @param string $cheminFichier
     */
    public function setCheminFichier($cheminFichier)
    {
        $s = urlencode($cheminFichier);
        $cheminFichier = str_replace('%2F', '/', $s);
        $this->cheminFichier = $cheminFichier;
    }

    /**
     * @return DateTime
     */
    public function getDateEnvoiDebut()
    {
        return $this->dateEnvoiDebut;
    }

    /**
     * @param DateTime $dateEnvoiDebut
     */
    public function setDateEnvoiDebut($dateEnvoiDebut)
    {
        $this->dateEnvoiDebut = $dateEnvoiDebut;
    }

    /**
     * @return DateTime
     */
    public function getDateEnvoiFin()
    {
        return $this->dateEnvoiFin;
    }

    /**
     * @param DateTime $dateEnvoiFin
     */
    public function setDateEnvoiFin($dateEnvoiFin)
    {
        $this->dateEnvoiFin = $dateEnvoiFin;
    }

    /**
     * @return bool
     */
    public function isStatusTransmissionn()
    {
        return $this->statusTransmissionn;
    }

    /**
     * @param bool $statusTransmissionn
     */
    public function setStatusTransmissionn($statusTransmissionn)
    {
        $this->statusTransmissionn = $statusTransmissionn;
    }

    /**
     * @return string
     */
    public function getErreur()
    {
        return $this->erreur;
    }

    /**
     * @param string $erreur
     */
    public function setErreur($erreur)
    {
        $this->erreur = $erreur;
    }
}
