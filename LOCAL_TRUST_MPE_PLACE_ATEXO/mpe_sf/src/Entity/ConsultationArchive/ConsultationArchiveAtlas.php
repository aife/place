<?php

namespace App\Entity\ConsultationArchive;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ConsultationArchiveAtlas.
 *
 * @ORM\Table
 * @ORM\Entity
 */
class ConsultationArchiveAtlas
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $docId;

    /**
     * @ORM\Column(type="integer")
     */
    private $numeroBloc;

    /**
     * @ORM\Column(type="string")
     */
    private $compId;

    /**
     * @ORM\Column(type="integer")
     */
    private $consultationRef;

    /**
     * @ORM\Column(type="string")
     */
    private $organisme;

    /**
     * @ORM\Column(type="integer")
     */
    private $taille;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")*/
    #[Assert\DateTime(message: "Le format attendu est dateTime et non : '{{ value }}'.")]
    private $dateEnvoi;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDocId()
    {
        return $this->docId;
    }

    /**
     * @param mixed $docId
     */
    public function setDocId($docId)
    {
        $this->docId = $docId;
    }

    /**
     * @return mixed
     */
    public function getNumeroBloc()
    {
        return $this->numeroBloc;
    }

    /**
     * @param mixed $numeroBloc
     */
    public function setNumeroBloc($numeroBloc)
    {
        $this->numeroBloc = $numeroBloc;
    }

    /**
     * @return mixed
     */
    public function getCompId()
    {
        return $this->compId;
    }

    /**
     * @param mixed $compId
     */
    public function setCompId($compId)
    {
        $this->compId = $compId;
    }

    /**
     * @return mixed
     */
    public function getConsultationRef()
    {
        return $this->consultationRef;
    }

    /**
     * @param mixed $consultationRef
     */
    public function setConsultationRef($consultationRef)
    {
        $this->consultationRef = $consultationRef;
    }

    /**
     * @return mixed
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param mixed $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return mixed
     */
    public function getTaille()
    {
        return $this->taille;
    }

    /**
     * @param mixed $taille
     */
    public function setTaille($taille)
    {
        $this->taille = $taille;
    }

    public function getDateEnvoi(): DateTime
    {
        return $this->dateEnvoi;
    }

    public function setDateEnvoi(DateTime $dateEnvoi)
    {
        $this->dateEnvoi = $dateEnvoi;
    }
}
