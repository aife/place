<?php

namespace App\Entity\ConsultationArchive;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ConsultationArchive.
 *
 * @ORM\Table(name="consultation_archive")
 * @ORM\Entity(repositoryClass="App\Repository\ConsultationArchive\ConsultationArchiveRepository")
 */
class ConsultationArchive
{
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(type="integer")
     */
    private $consultationRef;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private ?string $cheminFichier = null;

    /**
     * @ORM\Column(type="datetime", nullable=true)*/
    #[Assert\DateTime(message: "Le format attendu est dateTime et non : '{{ value }}'.")]
    private $dateArchivage;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $poidsArchivage;

    /**
     * @ORM\Column(type="integer", nullable=true)*/
    #[Assert\Length(max: 4, maxMessage: "Le format attendu est une année de date valide et non : '{{ value }}'.")]
    private $anneeCreationConsultation;

    /**
     *
     * @ORM\Column(type="string", length=50)*/
    #[Assert\Choice(['NON_DEMARREE', 'EN_COURS_TRANSMISSION', 'FINALISEE', 'NON_CONCERNEE'], strict: true)]
    private ?string $statusGlobalTransmission = null;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":false})*/
    #[Assert\Type('boolean')]
    private $statusFragmentation;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $nombreBloc = null;

    /**
     * @ORM\Column(type="integer")
     */
    private $consultationId;

    /**
     * @return mixed
     */
    public function getConsultationId()
    {
        return $this->consultationId;
    }

    /**
     * @param mixed $consultationId
     */
    public function setConsultationId($consultationId): ConsultationArchive
    {
        $this->consultationId = $consultationId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isStatusFragmentation(): bool
    {
        return $this->statusFragmentation;
    }

    public function setStatusFragmentation(bool $statusFragmentation)
    {
        $this->statusFragmentation = $statusFragmentation;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCheminFichier()
    {
        return urldecode($this->cheminFichier);
    }

    /**
     * @param string $cheminFichier
     */
    public function setCheminFichier($cheminFichier)
    {
        $s = urlencode($cheminFichier);
        $cheminFichier = str_replace('%2F', '/', $s);
        $this->cheminFichier = $cheminFichier;
    }

    /**
     * @return mixed
     */
    public function getDateArchivage()
    {
        return $this->dateArchivage;
    }

    /**
     * @param mixed $dateArchivage
     */
    public function setDateArchivage($dateArchivage)
    {
        $this->dateArchivage = $dateArchivage;
    }

    /**
     * @return mixed
     */
    public function getPoidsArchivage()
    {
        return $this->poidsArchivage;
    }

    /**
     * @param mixed $poidsArchivage
     */
    public function setPoidsArchivage($poidsArchivage)
    {
        $this->poidsArchivage = $poidsArchivage;
    }

    /**
     * @return mixed
     */
    public function getAnneeCreationConsultation()
    {
        return $this->anneeCreationConsultation;
    }

    /**
     * @param mixed $anneeCreationConsultation
     */
    public function setAnneeCreationConsultation($anneeCreationConsultation)
    {
        $this->anneeCreationConsultation = $anneeCreationConsultation;
    }

    /**
     * @return string
     */
    public function getStatusGlobalTransmission()
    {
        return $this->statusGlobalTransmission;
    }

    /**
     * @param string $statusGlobalTransmission
     */
    public function setStatusGlobalTransmission($statusGlobalTransmission)
    {
        $this->statusGlobalTransmission = $statusGlobalTransmission;
    }

    /**
     * @return int
     */
    public function getNombreBloc()
    {
        return $this->nombreBloc;
    }

    /**
     * @param int $nombreBloc
     */
    public function setNombreBloc($nombreBloc)
    {
        $this->nombreBloc = $nombreBloc;
    }

    /**
     * @return mixed
     */
    public function getConsultationRef()
    {
        return $this->consultationRef;
    }

    /**
     * @param mixed $consultationRef
     */
    public function setConsultationRef($consultationRef)
    {
        $this->consultationRef = $consultationRef;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param string $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }
}
