<?php

namespace App\Entity\ConsultationArchive;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ConsultationArchiveBloc.
 *
 * @ORM\Table
 * @ORM\Entity
 */
class ConsultationArchiveBloc
{
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", nullable=true, unique=true)
     */
    private $docId;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $compId;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $cheminFichier = null;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $numeroBloc = null;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $poidsBloc = null;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")*/
    #[Assert\DateTime(message: "Le format attendu est dateTime et non : '{{ value }}'.")]
    private $dateEnvoiDebut;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")*/
    #[Assert\DateTime(message: "Le format attendu est dateTime et non : '{{ value }}'.")]
    private $dateEnvoiFin;

    /**
     * @var bool
     * @var string
     * @ORM\Column(type="boolean", options={"default":true})*/
    #[Assert\Type('boolean')]
    private $statusTransmission;

    /**
     * @ORM\ManyToOne(targetEntity="ConsultationArchive")
     * @ORM\JoinColumn(name="consultation_archive_id", referencedColumnName="id")
     */
    private $consultationArchive;

    /**
     * @var int
     *
     * @ORM\Column(type="string")
     */
    private $erreur;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getDocId()
    {
        return $this->docId;
    }

    /**
     * @param int $docId
     */
    public function setDocId($docId)
    {
        $this->docId = $docId;
    }

    /**
     * @return int
     */
    public function getNumeroBloc()
    {
        return $this->numeroBloc;
    }

    /**
     * @param int $numeroBloc
     */
    public function setNumeroBloc($numeroBloc)
    {
        $this->numeroBloc = $numeroBloc;
    }

    /**
     * @return int
     */
    public function getPoidsBloc()
    {
        return $this->poidsBloc;
    }

    /**
     * @param int $poidsBloc
     */
    public function setPoidsBloc($poidsBloc)
    {
        $this->poidsBloc = $poidsBloc;
    }

    /**
     * @return DateTime
     */
    public function getDateEnvoiDebut()
    {
        return $this->dateEnvoiDebut;
    }

    /**
     * @param DateTime $dateEnvoiDebut
     */
    public function setDateEnvoiDebut($dateEnvoiDebut)
    {
        $this->dateEnvoiDebut = $dateEnvoiDebut;
    }

    /**
     * @return DateTime
     */
    public function getDateEnvoiFin()
    {
        return $this->dateEnvoiFin;
    }

    /**
     * @param DateTime $dateEnvoiFin
     */
    public function setDateEnvoiFin($dateEnvoiFin)
    {
        $this->dateEnvoiFin = $dateEnvoiFin;
    }

    /**
     * @return mixed
     */
    public function getStatusTransmission()
    {
        return $this->statusTransmission;
    }

    /**
     * @param mixed $statusTransmission
     */
    public function setStatusTransmission($statusTransmission)
    {
        $this->statusTransmission = $statusTransmission;
    }

    /**
     * @return mixed
     */
    public function getConsultationArchive()
    {
        return $this->consultationArchive;
    }

    /**
     * @param mixed $consultationArchive
     */
    public function setConsultationArchive($consultationArchive)
    {
        $this->consultationArchive = $consultationArchive;
    }

    /**
     * @return int
     */
    public function getErreur()
    {
        return $this->erreur;
    }

    /**
     * @param int $erreur
     */
    public function setErreur($erreur)
    {
        $this->erreur = $erreur;
    }

    /**
     * @return mixed
     */
    public function getCheminFichier()
    {
        return urldecode($this->cheminFichier);
    }

    /**
     * @param mixed $cheminFichier
     */
    public function setCheminFichier($cheminFichier)
    {
        $s = urlencode($cheminFichier);
        $cheminFichier = str_replace('%2F', '/', $s);
        $this->cheminFichier = $cheminFichier;
    }

    /**
     * @return mixed
     */
    public function getCompId()
    {
        return $this->compId;
    }

    /**
     * @param mixed $compId
     */
    public function setCompId($compId)
    {
        $this->compId = $compId;
    }
}
