<?php

namespace App\Entity\DossierVolumineux;

use Stringable;
use App\Entity\Agent;
use App\Entity\BlobFichier;
use App\Entity\Consultation;
use App\Entity\Enveloppe;
use App\Entity\Inscrit;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * DossierVolumineux.
 *
 * @ORM\Table(name="dossier_volumineux",
 *     indexes={@ORM\Index(name="uuid_reference", columns={"uuid_reference"})}
 *     )
 * @ORM\Entity(repositoryClass="App\Repository\DossierVolumineux\DossierVolumineuxRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class DossierVolumineux implements Stringable
{
    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="uuid_reference",  type="string", length=30, unique=true)
     */
    private ?string $uuidReference = null;

    /**
     * @ORM\Column(name="nom",  type="string", length=30, nullable=true, options={"default": NULL})
     */
    private ?string $nom = null;

    /**
     * @ORM\Column(name="taille", type="bigint", options={"unsigned"=true}, nullable=true)
     */
    private ?int $taille = null;

    /**
     * @var DateTime
     * @ORM\Column(name="date_creation", type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $dateCreation;

    /**
     * @var bool
     * @ORM\Column(name="actif", type="boolean", options={"default" : 0})
     */
    private $actif;

    /**
     * @ORM\Column(name="statut", type="string", length=30, options={"default" : "init"})
     */
    private ?string $statut = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Agent",  inversedBy="dossierVolumineux")
     * @ORM\JoinColumn(name="id_agent", referencedColumnName="id")
     */
    private ?Agent $agent = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Inscrit")
     * @ORM\JoinColumn(name="id_inscrit", referencedColumnName="id")
     */
    private ?Inscrit $inscrit = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BlobFichier", inversedBy="dossierVolumineuxDescripteur", fetch="EAGER")
     * @ORM\JoinColumn(name="id_blob_descripteur", referencedColumnName="id")
     */
    private ?BlobFichier $blobDescripteur = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BlobFichier", inversedBy="dossierVolumineuxLogfile", fetch="EAGER")
     * @ORM\JoinColumn(name="id_blob_logfile", referencedColumnName="id")
     */
    private ?BlobFichier $blobLogfile = null;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Consultation", mappedBy="dossierVolumineux", cascade={"persist"})
     * @ORM\JoinColumn(name="id", referencedColumnName="id_dossier_volumineux")
     */
    private Collection $consultations;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Echange", mappedBy="dossierVolumineux", cascade={"persist"})
     * @ORM\JoinColumn(name="id", referencedColumnName="id_dossier_volumineux")
     */
    private Collection $echanges;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Enveloppe", mappedBy="dossierVolumineux")
     * @ORM\JoinColumn(name="id", referencedColumnName="id_dossier_volumineux")
     */
    private Collection $enveloppes;

    /**
     * @ORM\Column(name="uuid_technique",  type="string", length=255, unique=true)
     */
    private ?string $uuidTechnique = null;

    /**
     * @var DateTime
     * @ORM\Column(name="date_modification", type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $dateModification;

    /**
     * DossierVolumineux constructor.
     */
    public function __construct()
    {
        $this->consultations = new ArrayCollection();
        $this->enveloppes = new ArrayCollection();
        $this->echanges = new ArrayCollection();
    }

    public function getEchanges(): ArrayCollection
    {
        return $this->echanges;
    }

    public function setEchanges(ArrayCollection $echanges)
    {
        $this->echanges = $echanges;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUuidReference(): string
    {
        return $this->uuidReference;
    }

    /**
     * @param string $uuid
     */
    public function setUuidReference(string $uuidReference)
    {
        $this->uuidReference = strtoupper($uuidReference);
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return int
     */
    public function getTaille()
    {
        return $this->taille;
    }

    public function setTaille(int $taille)
    {
        $this->taille = $taille;
    }

    /**
     * @return DateTime
     */
    public function getDateCreation(): \DateTime
    {
        return $this->dateCreation;
    }

    /**
     * @param DateTime $dateCreation
     */
    public function setDateCreation(\DateTime $dateCreation = null)
    {
        if (null === $dateCreation) {
            $dateCreation = new \DateTime();
        }
        $this->dateCreation = $dateCreation;
    }

    public function getStatut(): string
    {
        return $this->statut;
    }

    /**
     * @param bool $statut
     */
    public function setStatut(string $statut)
    {
        $this->statut = $statut;
    }

    public function __toString(): string
    {
        return (string) $this->getId();
    }

    /**
     * @return Agent
     */
    public function getAgent()
    {
        return $this->agent;
    }

    public function setAgent(Agent $agent)
    {
        $this->agent = $agent;
    }

    /**
     * @return Inscrit
     */
    public function getInscrit()
    {
        return $this->inscrit;
    }

    public function setInscrit(Inscrit $inscrit)
    {
        $this->inscrit = $inscrit;
    }

    /**
     * @return BlobFichier
     */
    public function getBlobDescripteur()
    {
        return $this->blobDescripteur;
    }

    public function setBlobDescripteur(BlobFichier $blobDescripteur)
    {
        $this->blobDescripteur = $blobDescripteur;
    }

    /**
     * @return BlobFichier
     */
    public function getBlobLogfile()
    {
        return $this->blobLogfile;
    }

    /**
     * @param BlobFichier blobLogfile
     */
    public function setBlobLogfile(BlobFichier $bloLogfile)
    {
        $this->blobLogfile = $bloLogfile;
    }

    public function getConsultations()
    {
        return $this->consultations;
    }

    public function addConsultation(Consultation $consultation)
    {
        $this->consultations->add($consultation);
        $consultation->setDossierVolumineux($this);
    }

    public function getEnveloppes()
    {
        return $this->enveloppes;
    }

    public function addEnveloppe(Enveloppe $enveloppe)
    {
        $this->enveloppes->add($enveloppe);
        $enveloppe->setDossierVolumineux($this);
    }

    public function getUuidTechnique(): string
    {
        return $this->uuidTechnique;
    }

    /**
     * @param string $uuidTus
     */
    public function setUuidTechnique(string $uuidTechnique)
    {
        $this->uuidTechnique = $uuidTechnique;
    }

    public function isActif(): bool
    {
        return $this->actif;
    }

    public function setActif(bool $actif)
    {
        $this->actif = $actif;
    }

    /**
     * @return DateTime
     */
    public function getDateModification(): \DateTime
    {
        return $this->dateModification;
    }

    /**
     * @param DateTime $dateModification
     */
    public function setDateModification(\DateTime $dateModification = null)
    {
        if (null === $dateModification) {
            $dateModification = new \DateTime();
        }
        $this->dateModification = $dateModification;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }
}
