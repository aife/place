<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ContratTitulaireFavori.
 *
 * @ORM\Table(name="t_contrat_titulaire_favori")
 * @ORM\Entity
 */
class ContratTitulaireFavori
{
    /**
     *
     * @ORM\Column(name="id_contrat_titulaire_favori", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $idContratTitulaireFavori = null;

    /**
     * @ORM\Column(name="id_contrat_titulaire", type="integer", nullable=false)
     */
    private string|int $idContratTitulaire = '0';

    /**
     * @ORM\Column(name="id_agent", type="integer", nullable=false)
     */
    private string|int $idAgent = '0';

    /**
     * @ORM\Column(name="organisme", type="string", length=30, nullable=false)
     */
    private ?string $organisme = null;

    /**
     * @return int
     */
    public function getIdContratTitulaireFavori()
    {
        return $this->idContratTitulaireFavori;
    }

    public function setIdContratTitulaireFavori(int $idContratTitulaireFavori)
    {
        $this->idContratTitulaireFavori = $idContratTitulaireFavori;
    }

    /**
     * @return int
     */
    public function getIdContratTitulaire()
    {
        return $this->idContratTitulaire;
    }

    public function setIdContratTitulaire(int $idContratTitulaire)
    {
        $this->idContratTitulaire = $idContratTitulaire;
    }

    /**
     * @return int
     */
    public function getIdAgent()
    {
        return $this->idAgent;
    }

    public function setIdAgent(int $idAgent)
    {
        $this->idAgent = $idAgent;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme)
    {
        $this->organisme = $organisme;
    }
}
