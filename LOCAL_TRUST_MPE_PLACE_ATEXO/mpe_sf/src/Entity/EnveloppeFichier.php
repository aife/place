<?php

namespace App\Entity;

use DateTime;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Dto\Output\FichierEnveloppeOutput;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * EnveloppeFichier.
 *
 * @ORM\Table(name="fichierEnveloppe", indexes={@ORM\Index(name="id_enveloppe", columns={"id_enveloppe"}),
 *     @ORM\Index(name="Idx_acronyme_org", columns={"organisme"})})
 * @ORM\Entity(repositoryClass="App\Repository\EnveloppeFichierRepository")
 */

#[ApiResource(
    collectionOperations: [],
    itemOperations: [
        'get' => ["security" => "is_granted('VIEW_ENVELOPPE', object)"]
    ],
    output: FichierEnveloppeOutput::class,
)]
class EnveloppeFichier
{
    //Pièce jointe
    public const TYPE_PIECE_JOINTE = 1;

    //Edition sub
    public const TYPE_PIECE_SUB = 2;

    //Pièce libre
    public const TYPE_PIECE_LIBRE = 3;

    //Piece typée (pour l'acte d'engagement)
    public const TYPE_PIECE_TYPEE = 4;

    //Pièce annexe finiancière
    public const TYPE_PIECE_AFI = 5;

    public const TYPE_FICHIER_ACE = 'ACE';

    public const TYPE_FICHIER_AFI = 'AFI';

    public const TYPE_PIECE = [
        'PRI' => ['id' => 3, 'libelle' => 'Pièce libre'],
        'ACE' => ['id' => 4, 'libelle' => 'Acte d\'engagement'],
        'AFI' => ['id' => 5, 'libelle' => 'Annexe financière'],
        'SIG' => ['id' => 99, 'libelle' => 'Signature'],
    ];

    public const TYPE_FICHIER = ['PRI', 'ACE', 'AFI', 'SIG'];

    public const TYPE_FICHIER_SIGNATURE = 'SIG';

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Enveloppe", inversedBy="fichiers",
     *     cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="id_enveloppe", referencedColumnName="id_enveloppe_electro")
     */
    private ?Enveloppe $enveloppe = null;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\EnveloppeFichierBloc", mappedBy="fichierEnveloppe",
     *     cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="id_fichier", referencedColumnName="id_fichier")
     */
    private Collection $blocsFichiersEnveloppes;

    /**
     *
     * @ORM\Column(name="id_fichier", type="integer", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $idFichier = null;

    /**
     * @ORM\Column(name="organisme", type="string", length=30, unique=true)
     */
    private string $organisme = '';

    /**
     * @ORM\Column(name="id_enveloppe", type="integer", nullable=false)
     */
    private string|int $idEnveloppe = '0';

    /**
     * @ORM\Column(name="type_fichier", type="string", length=3, nullable=false)
     */
    private string $typeFichier = '';

    /**
     * @ORM\Column(name="num_ordre_fichier", type="integer", nullable=false)
     */
    private string|int $numOrdreFichier = '0';

    /**
     * @ORM\Column(name="nom_fichier", type="text", length=65535, nullable=false)
     */
    private ?string $nomFichier = null;

    /**
     * @ORM\Column(name="taille_fichier", type="string", length=50, nullable=false)
     */
    private string $tailleFichier = '';

    /**
     * @ORM\Column(name="signature_fichier", type="text", length=65535, nullable=true)
     */
    private ?string $signatureFichier = null;

    /**
     * @ORM\Column(name="hash", type="text", length=65535, nullable=false)
     */
    private ?string $hash = null;

    /**
     * @ORM\Column(name="verification_certificat", type="string", length=5, nullable=false)
     */
    private string $verificationCertificat = '';

    /**
     * Un fichier enveloppe a un blob.
     *
     * @ORM\OneToOne(targetEntity="App\Entity\BloborganismeFile")
     * @ORM\JoinColumn(name="id_blob", referencedColumnName="id")
     */
    private $blob;

    /**
     * @ORM\Column(name="id_blob", type="integer", nullable=true)
     */
    private ?int $idBlob = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\BloborganismeFile")
     * @ORM\JoinColumn(name="id_blob_signature", referencedColumnName="id")
     */
    private $blobSignature;

    /**
     * @ORM\Column(name="id_blob_signature", type="integer", nullable=true)
     */
    private ?int $idBlobSignature = null;

    /**
     * @ORM\Column(name="type_piece", type="integer", nullable=false)
     */
    private string|int $typePiece = '3';

    /**
     * @ORM\Column(name="id_type_piece", type="integer", nullable=false)
     */
    private string|int $idTypePiece = '0';

    /**
     * @ORM\Column(name="is_hash", type="string", nullable=false)
     */
    private string $isHash = '0';

    /**
     * @ORM\Column(name="nom_referentiel_certificat", type="string", length=255, nullable=true)
     */
    private ?string $nomReferentielCertificat = null;

    /**
     * @ORM\Column(name="statut_referentiel_certificat", type="integer", nullable=true)
     */
    private ?int $statutReferentielCertificat = null;

    /**
     * @ORM\Column(name="nom_referentiel_fonctionnel", type="string", length=255, nullable=true)
     */
    private ?string $nomReferentielFonctionnel = null;

    /**
     * @ORM\Column(name="message", type="string", length=255, nullable=true)
     */
    private ?string $message = null;

    /**
     * @ORM\Column(name="date_signature", type="string", length=20, nullable=true)
     */
    private ?string $dateSignature = null;

    /**
     * @ORM\Column(name="signature_infos", type="text", nullable=true)
     */
    private $signatureInfos;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="signature_infos_date", type="datetime", nullable=true)*/
    private $signatureInfosDate;

    /**
     * @ORM\Column(name="id_fichier_signature", type="integer", nullable=true)
     */
    private $idFichierSignature;

    private $fichierSignature;

    /**
     * @ORM\Column(name="uid_response", type="string", length=255, nullable=true)
     */
    private ?string $uidResponse = '';

    /**
     * @ORM\Column(name="type_signature_fichier", type="string", length=50, nullable=false)
     */
    private string $typeSignatureFichier = '';
    /**
     * @ORM\Column(name="hash256", type="text", length=65535, nullable=false)
     */
    private string $hash256 = '';

    private string $statutSignature = '';

    /**
     * @ORM\Column(name="resultat_verification_hash", type="string", length=1, nullable=true)
     */
    private ?string $resultatVerificationHash = '1';

    /**
     * @ORM\OneToOne(targetEntity=HistoriquePurge::class, mappedBy="fichierEnveloppe")
     */
    private ?HistoriquePurge $historiquePurge = null;

    /**
     * Set idFichier.
     *
     * @param int $idFichier
     *
     * @return EnveloppeFichier
     */
    public function setIdFichier($idFichier)
    {
        $this->idFichier = $idFichier;

        return $this;
    }

    /**
     * Get idFichier.
     *
     * @return int
     */
    public function getIdFichier()
    {
        return $this->idFichier;
    }

    /**
     * Set organisme.
     *
     * @param string $organisme
     *
     * @return EnveloppeFichier
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * Get organisme.
     *
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * Set idEnveloppe.
     *
     * @param int $idEnveloppe
     *
     * @return EnveloppeFichier
     */
    public function setIdEnveloppe($idEnveloppe)
    {
        $this->idEnveloppe = $idEnveloppe;

        return $this;
    }

    /**
     * Get idEnveloppe.
     *
     * @return int
     */
    public function getIdEnveloppe()
    {
        return $this->idEnveloppe;
    }

    /**
     * Set typeFichier.
     *
     * @param string $typeFichier
     *
     * @return EnveloppeFichier
     */
    public function setTypeFichier($typeFichier)
    {
        $this->typeFichier = $typeFichier;

        return $this;
    }

    /**
     * Get typeFichier.
     *
     * @return string
     */
    public function getTypeFichier()
    {
        return $this->typeFichier;
    }

    /**
     * Set numOrdreFichier.
     *
     * @param int $numOrdreFichier
     *
     * @return EnveloppeFichier
     */
    public function setNumOrdreFichier($numOrdreFichier)
    {
        $this->numOrdreFichier = $numOrdreFichier;

        return $this;
    }

    /**
     * Get numOrdreFichier.
     *
     * @return int
     */
    public function getNumOrdreFichier()
    {
        return $this->numOrdreFichier;
    }

    /**
     * Set nomFichier.
     *
     * @param string $nomFichier
     *
     * @return EnveloppeFichier
     */
    public function setNomFichier($nomFichier)
    {
        $this->nomFichier = $nomFichier;

        return $this;
    }

    /**
     * Get nomFichier.
     *
     * @return string
     */
    public function getNomFichier()
    {
        return $this->nomFichier;
    }

    /**
     * Set tailleFichier.
     *
     * @param string $tailleFichier
     *
     * @return EnveloppeFichier
     */
    public function setTailleFichier($tailleFichier)
    {
        $this->tailleFichier = $tailleFichier;

        return $this;
    }

    /**
     * Get tailleFichier.
     *
     * @return string
     */
    public function getTailleFichier()
    {
        return $this->tailleFichier;
    }

    /**
     * Set signatureFichier.
     *
     * @param string $signatureFichier
     *
     * @return EnveloppeFichier
     */
    public function setSignatureFichier($signatureFichier = null)
    {
        $this->signatureFichier = $signatureFichier;

        return $this;
    }

    /**
     * Get signatureFichier.
     *
     * @return string
     */
    public function getSignatureFichier()
    {
        return $this->signatureFichier;
    }

    /**
     * Set hash.
     *
     * @param string $hash
     *
     * @return EnveloppeFichier
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash.
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set verificationCertificat.
     *
     * @param string $verificationCertificat
     *
     * @return EnveloppeFichier
     */
    public function setVerificationCertificat($verificationCertificat)
    {
        $this->verificationCertificat = $verificationCertificat;

        return $this;
    }

    /**
     * Get verificationCertificat.
     *
     * @return string
     */
    public function getVerificationCertificat()
    {
        return $this->verificationCertificat;
    }

    /**
     * Set idBlobSignature.
     *
     * @param int $idBlobSignature
     *
     * @return EnveloppeFichier
     */
    public function setIdBlobSignature($idBlobSignature)
    {
        $this->idBlobSignature = $idBlobSignature;

        return $this;
    }

    /**
     * Get idBlobSignature.
     *
     * @return int
     */
    public function getIdBlobSignature()
    {
        return $this->idBlobSignature;
    }

    /**
     * Set typePiece.
     *
     * @param int $typePiece
     *
     * @return EnveloppeFichier
     */
    public function setTypePiece($typePiece)
    {
        $this->typePiece = $typePiece;

        return $this;
    }

    /**
     * Get typePiece.
     *
     * @return int
     */
    public function getTypePiece()
    {
        return $this->typePiece;
    }

    /**
     * Set idTypePiece.
     *
     * @param int $idTypePiece
     *
     * @return EnveloppeFichier
     */
    public function setIdTypePiece($idTypePiece)
    {
        $this->idTypePiece = $idTypePiece;

        return $this;
    }

    /**
     * Get idTypePiece.
     *
     * @return int
     */
    public function getIdTypePiece()
    {
        return $this->idTypePiece;
    }

    /**
     * Set isHash.
     *
     * @param string $isHash
     *
     * @return EnveloppeFichier
     */
    public function setIsHash($isHash)
    {
        $this->isHash = $isHash;

        return $this;
    }

    /**
     * Get isHash.
     *
     * @return string
     */
    public function getIsHash()
    {
        return $this->isHash;
    }

    /**
     * Set nomReferentielCertificat.
     *
     * @param string $nomReferentielCertificat
     *
     * @return EnveloppeFichier
     */
    public function setNomReferentielCertificat($nomReferentielCertificat)
    {
        $this->nomReferentielCertificat = $nomReferentielCertificat;

        return $this;
    }

    /**
     * Get nomReferentielCertificat.
     *
     * @return string
     */
    public function getNomReferentielCertificat()
    {
        return $this->nomReferentielCertificat;
    }

    /**
     * Set statutReferentielCertificat.
     *
     * @param int $statutReferentielCertificat
     *
     * @return EnveloppeFichier
     */
    public function setStatutReferentielCertificat($statutReferentielCertificat)
    {
        $this->statutReferentielCertificat = $statutReferentielCertificat;

        return $this;
    }

    /**
     * Get statutReferentielCertificat.
     *
     * @return int
     */
    public function getStatutReferentielCertificat()
    {
        return $this->statutReferentielCertificat;
    }

    /**
     * Set nomReferentielFonctionnel.
     *
     * @param string $nomReferentielFonctionnel
     *
     * @return EnveloppeFichier
     */
    public function setNomReferentielFonctionnel($nomReferentielFonctionnel)
    {
        $this->nomReferentielFonctionnel = $nomReferentielFonctionnel;

        return $this;
    }

    /**
     * Get nomReferentielFonctionnel.
     *
     * @return string
     */
    public function getNomReferentielFonctionnel()
    {
        return $this->nomReferentielFonctionnel;
    }

    /**
     * Set message.
     *
     * @param string $message
     *
     * @return EnveloppeFichier
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message.
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set dateSignature.
     *
     * @param string $dateSignature
     *
     * @return EnveloppeFichier
     */
    public function setDateSignature($dateSignature)
    {
        $this->dateSignature = $dateSignature;

        return $this;
    }

    /**
     * Get dateSignature.
     *
     * @return string
     */
    public function getDateSignature()
    {
        return $this->dateSignature;
    }

    /**
     * Consultation constructor.
     */
    public function __construct()
    {
        $this->blocsFichiersEnveloppes = new ArrayCollection();
    }

    /**
     * @return $this
     */
    public function addBlocFichierEnveloppe(EnveloppeFichierBloc $blocfichierenveloppe)
    {
        $this->blocsFichiersEnveloppes[] = $blocfichierenveloppe;
        $blocfichierenveloppe->setFichierEnveloppe($this);

        return $this;
    }

    public function removeBlocFichierEnveloppe(EnveloppeFichierBloc $blocfichierenveloppe)
    {
        $this->blocsFichiersEnveloppes->removeElement($blocfichierenveloppe);
    }

    /**
     * @return ArrayCollection
     */
    public function getBlocFichierEnveloppes()
    {
        return $this->blocsFichiersEnveloppes;
    }

    /**
     * @return Enveloppe
     */
    public function getEnveloppe()
    {
        return $this->enveloppe;
    }

    public function setEnveloppe(Enveloppe $enveloppe)
    {
        $this->enveloppe = $enveloppe;
    }

    /**
     * @return mixed
     */
    public function getSignatureInfos()
    {
        return $this->signatureInfos;
    }

    /**
     * @param mixed $signatureInfos
     */
    public function setSignatureInfos($signatureInfos)
    {
        $this->signatureInfos = $signatureInfos;
    }

    /**
     * @return DateTime
     */
    public function getSignatureInfosDate()
    {
        return $this->signatureInfosDate;
    }

    /**
     * @param DateTime $signatureInfosDate
     */
    public function setSignatureInfosDate($signatureInfosDate)
    {
        $this->signatureInfosDate = $signatureInfosDate;
    }

    /**
     * @return mixed
     */
    public function getIdFichierSignature()
    {
        return $this->idFichierSignature;
    }

    /**
     * @param mixed $idFichierSignature
     */
    public function setIdFichierSignature($idFichierSignature)
    {
        $this->idFichierSignature = $idFichierSignature;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFichierSignature()
    {
        return $this->fichierSignature;
    }

    /**
     * @param mixed $fichierSignature
     */
    public function setFichierSignature($fichierSignature)
    {
        $this->fichierSignature = $fichierSignature;
    }

    /**
     * @return string
     */
    public function getUidResponse()
    {
        return $this->uidResponse;
    }

    /**
     * @param string $uidResponse
     */
    public function setUidResponse($uidResponse)
    {
        $this->uidResponse = $uidResponse;
    }

    /**
     * @return string
     */
    public function getTypeSignatureFichier()
    {
        return $this->typeSignatureFichier;
    }

    /**
     * @param string $typeSignature
     */
    public function setTypeSignatureFichier($typeSignature)
    {
        $this->typeSignatureFichier = $typeSignature;
    }

    /**
     * @return string
     */
    public function getHash256()
    {
        return $this->hash256;
    }

    /**
     * @param string $hash256
     */
    public function setHash256($hash256)
    {
        $this->hash256 = $hash256;
    }

    /**
     * @return string
     */
    public function getStatutSignature()
    {
        return $this->statutSignature;
    }

    /**
     * @param string $statutSignature
     */
    public function setStatutSignature($statutSignature)
    {
        $this->statutSignature = $statutSignature;
    }

    public function getResultatVerificationHash(): string
    {
        return $this->resultatVerificationHash;
    }

    public function setResultatVerificationHash(string $resultatVerificationHash)
    {
        $this->resultatVerificationHash = $resultatVerificationHash;
    }

    public function getIdBlob(): int
    {
        return $this->idBlob;
    }

    public function setIdBlob(int $idBlob)
    {
        $this->idBlob = $idBlob;
    }

    public function getBlobSignature(): ?BloborganismeFile
    {
        return $this->blobSignature;
    }

    public function setBlobSignature($blobSignature): self
    {
        $this->blobSignature = $blobSignature;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBlob()
    {
        return $this->blob;
    }

    /**
     * @param mixed $blob
     */
    public function setBlob($blob)
    {
        $this->blob = $blob;

        return $this;
    }

    public function getHistoriquePurge(): ?HistoriquePurge
    {
        return $this->historiquePurge;
    }

    public function setHistoriquePurge(?HistoriquePurge $historiquePurge): void
    {
        $this->historiquePurge = $historiquePurge;
    }
}
