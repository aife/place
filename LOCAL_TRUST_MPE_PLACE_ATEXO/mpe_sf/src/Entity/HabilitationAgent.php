<?php

namespace App\Entity;

use Exception;
use App\Entity\Agent\Habilitations\RecensementProgrammationStrategieAchat;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;

/**
 * HabilitationAgent.
 *
 * @ORM\Table(name="HabilitationAgent", indexes={@ORM\Index(name="HabilitationAgent_ibfk_1", columns={"id_agent"})})
 * @ORM\Entity(repositoryClass="App\Repository\HabilitationAgentRepository")
 */
class HabilitationAgent
{
    public function __construct()
    {
        $this->recensementProgrammationStrategieAchat = new RecensementProgrammationStrategieAchat();
    }

    private EntityManager $em;

    /**
     * @ORM\Column(name="gestion_agent_pole", type="string", nullable=false)
     */
    private string $gestionAgentPole = '0';

    /**
     * @ORM\Column(name="gestion_fournisseurs_envois_postaux", type="string", nullable=false)
     */
    private string $gestionFournisseursEnvoisPostaux = '0';

    /**
     * @ORM\Column(name="gestion_bi_cles", type="string", nullable=false)
     */
    private string $gestionBiCles = '0';

    /**
     * @ORM\Column(name="creer_consultation", type="string", nullable=false)
     */
    private string $creerConsultation = '1';

    /**
     * @ORM\Column(name="modifier_consultation", type="string", nullable=false)
     */
    private string $modifierConsultation = '0';

    /**
     * @ORM\Column(name="valider_consultation", type="string", nullable=false)
     */
    private string $validerConsultation = '0';

    /**
     * @ORM\Column(name="publier_consultation", type="string", nullable=false)
     */
    private string $publierConsultation = '0';

    /**
     * @ORM\Column(name="suivre_consultation", type="string", nullable=false)
     */
    private string $suivreConsultation = '1';

    /**
     * @ORM\Column(name="suivre_consultation_pole", type="string", nullable=false)
     */
    private string $suivreConsultationPole = '0';

    /**
     * @ORM\Column(name="supprimer_enveloppe", type="string", nullable=false)
     */
    private string $supprimerEnveloppe = '0';

    /**
     * @ORM\Column(name="supprimer_consultation", type="string", nullable=false)
     */
    private string $supprimerConsultation = '0';

    /**
     * @ORM\Column(name="depouiller_candidature", type="string", nullable=false)
     */
    private string $depouillerCandidature = '1';

    /**
     * @ORM\Column(name="depouiller_offre", type="string", nullable=false)
     */
    private string $depouillerOffre = '1';

    /**
     * @ORM\Column(name="messagerie_securisee", type="string", nullable=false)
     */
    private string $messagerieSecurisee = '0';

    /**
     * @ORM\Column(name="acces_registre_depots_papier", type="string", nullable=false)
     */
    private string $accesRegistreDepotsPapier = '0';

    /**
     * @ORM\Column(name="acces_registre_retraits_papier", type="string", nullable=false)
     */
    private string $accesRegistreRetraitsPapier = '0';

    /**
     * @ORM\Column(name="acces_registre_questions_papier", type="string", nullable=false)
     */
    private string $accesRegistreQuestionsPapier = '0';

    /**
     * @ORM\Column(name="gerer_encheres", type="string", nullable=false)
     */
    private string $gererEncheres = '0';

    /**
     * @ORM\Column(name="suivre_encheres", type="string", nullable=false)
     */
    private string $suivreEncheres = '0';

    /**
     * @ORM\Column(name="suivi_entreprise", type="string", nullable=false)
     */
    private string $suiviEntreprise = '0';

    /**
     * @ORM\Column(name="envoi_boamp", type="string", nullable=false)
     */
    private string $envoiBoamp = '0';

    /**
     * @ORM\Column(name="acces_classement_lot", type="string", nullable=false)
     */
    private string $accesClassementLot = '0';

    /**
     * @ORM\Column(name="connecteur_sis", type="string", nullable=false)
     */
    private string $connecteurSis = '0';

    /**
     * @ORM\Column(name="connecteur_marco", type="string", nullable=false)
     */
    private string $connecteurMarco = '0';

    /**
     * @ORM\Column(name="repondre_aux_questions", type="string", nullable=false)
     */
    private string $repondreAuxQuestions = '0';

    /**
     * @ORM\Column(name="appel_projet_formation", type="string", nullable=false)
     */
    private string $appelProjetFormation = '0';

    /**
     * @ORM\Column(name="utiliser_client_CAO", type="string", nullable=false)
     */
    private string $utiliserClientCao = '0';

    /**
     * @ORM\Column(name="notification_boamp", type="string", nullable=false)
     */
    private string $notificationBoamp = '0';

    /**
     * @ORM\Column(name="administrer_compte", type="string", nullable=false)
     */
    private string $administrerCompte = '0';

    /**
     * @ORM\Column(name="gestion_mapa", type="string", nullable=false)
     */
    private string $gestionMapa = '0';

    /**
     * @ORM\Column(name="gestion_type_validation", type="string", nullable=false)
     */
    private string $gestionTypeValidation = '0';

    /**
     * @ORM\Column(name="approuver_consultation", type="string", nullable=false)
     */
    private string $approuverConsultation = '0';

    /**
     * @ORM\Column(name="administrer_procedure", type="string", nullable=false)
     */
    private string $administrerProcedure = '0';

    /**
     * @ORM\Column(name="restreindre_creation", type="string", nullable=false)
     */
    private string $restreindreCreation = '0';

    /**
     * @ORM\Column(name="creer_liste_marches", type="string", nullable=false)
     */
    private string $creerListeMarches = '0';

    /**
     * @ORM\Column(name="gestion_commissions", type="string", nullable=false)
     */
    private string $gestionCommissions = '0';

    /**
     * @ORM\Column(name="suivi_seul_consultation", type="string", nullable=false)
     */
    private string $suiviSeulConsultation = '0';

    /**
     * @ORM\Column(name="attribution_marche", type="string", nullable=false)
     */
    private string $attributionMarche = '0';

    /**
     * @ORM\Column(name="fiche_recensement", type="string", nullable=false)
     */
    private string $ficheRecensement = '0';

    /**
     * @ORM\Column(name="declarer_infructueux", type="string", nullable=false)
     */
    private string $declarerInfructueux = '0';

    /**
     * @ORM\Column(name="declarer_sans_suite", type="string", nullable=false)
     */
    private string $declarerSansSuite = '0';

    /**
     * @ORM\Column(name="creer_consultation_transverse", type="string", nullable=false)
     */
    private string $creerConsultationTransverse = '0';

    /**
     * @ORM\Column(name="ouvrir_candidature_en_ligne", type="string", nullable=false)
     */
    private string $ouvrirCandidatureEnLigne = '0';

    /**
     * @ORM\Column(name="ouvrir_candidature_a_distance", type="string", nullable=false)
     */
    private string $ouvrirCandidatureADistance = '0';

    /**
     * @ORM\Column(name="refuser_enveloppe", type="string", nullable=false)
     */
    private string $refuserEnveloppe = '0';

    /**
     * @ORM\Column(name="gerer_admissibilite", type="string", nullable=false)
     */
    private string $gererAdmissibilite = '0';

    /**
     * @ORM\Column(name="restaurer_enveloppe", type="string", nullable=false)
     */
    private string $restaurerEnveloppe = '0';

    /**
     * @ORM\Column(name="ouvrir_anonymat_en_ligne", type="string", nullable=false)
     */
    private string $ouvrirAnonymatEnLigne = '0';

    /**
     * @ORM\Column(name="ouvrir_offre_en_ligne", type="string", nullable=false)
     */
    private string $ouvrirOffreEnLigne = '0';

    /**
     * @ORM\Column(name="gestion_compte_boamp", type="string", nullable=false)
     */
    private string $gestionCompteBoamp = '0';

    /**
     * @ORM\Column(name="gestion_agents", type="string", nullable=false)
     */
    private string $gestionAgents = '0';

    /**
     * @ORM\Column(name="gestion_habilitations", type="string", nullable=false)
     */
    private string $gestionHabilitations = '0';

    /**
     * @ORM\Column(name="gerer_mapa_inferieur_montant", type="string", nullable=false)
     */
    private string $gererMapaInferieurMontant = '0';

    /**
     * @ORM\Column(name="gerer_mapa_superieur_montant", type="string", nullable=false)
     */
    private string $gererMapaSuperieurMontant = '0';

    /**
     * @ORM\Column(name="modifier_consultation_avant_validation", type="string", nullable=false)
     */
    private string $modifierConsultationAvantValidation = '0';

    /**
     * @ORM\Column(name="modifier_consultation_apres_validation", type="string", nullable=false)
     */
    private string $modifierConsultationApresValidation = '0';

    /**
     * @ORM\Column(name="acces_reponses", type="string", nullable=false)
     */
    private string $accesReponses = '0';

    /**
     * @ORM\Column(name="telechargement_groupe_anticipe_plis_chiffres", type="string", nullable=false)
     */
    private string $telechargementGroupeAnticipePlisChiffres = '0';

    /**
     * @ORM\Column(name="telechargement_unitaire_plis_chiffres", type="string", nullable=false)
     */
    private string $telechargementUnitairePlisChiffres = '0';

    /**
     * @ORM\Column(name="ouvrir_offre_a_distance", type="string", nullable=false)
     */
    private string $ouvrirOffreADistance = '0';

    /**
     * @ORM\Column(name="creer_annonce_information", type="string", nullable=false)
     */
    private string $creerAnnonceInformation = '0';

    /**
     * @ORM\Column(name="saisie_marches", type="string", nullable=false)
     */
    private string $saisieMarches = '0';

    /**
     * @ORM\Column(name="validation_marches", type="string", nullable=false)
     */
    private string $validationMarches = '0';

    /**
     * @ORM\Column(name="publication_marches", type="string", nullable=false)
     */
    private string $publicationMarches = '0';

    /**
     * @ORM\Column(name="gerer_statistiques_metier", type="string", nullable=false)
     */
    private string $gererStatistiquesMetier = '0';

    /**
     * @ORM\Column(name="gerer_archives", type="string", nullable=false)
     */
    private string $gererArchives = '0';

    /**
     * @ORM\Column(name="administrer_procedures_formalisees", type="string", nullable=false)
     */
    private string $administrerProceduresFormalisees = '0';

    /**
     * @ORM\Column(name="creer_annonce_attribution", type="string", nullable=false)
     */
    private string $creerAnnonceAttribution = '0';

    /**
     * @ORM\Column(name="acces_registre_retraits_electronique", type="string", nullable=false)
     */
    private string $accesRegistreRetraitsElectronique = '1';

    /**
     * @ORM\Column(name="acces_registre_questions_electronique", type="string", nullable=false)
     */
    private string $accesRegistreQuestionsElectronique = '1';

    /**
     * @ORM\Column(name="acces_registre_depots_electronique", type="string", nullable=false)
     */
    private string $accesRegistreDepotsElectronique = '1';

    /**
     * @ORM\Column(name="validation_simple", type="string", nullable=false)
     */
    private string $validationSimple = '1';

    /**
     * @ORM\Column(name="validation_intermediaire", type="string", nullable=false)
     */
    private string $validationIntermediaire = '1';

    /**
     * @ORM\Column(name="validation_finale", type="string", nullable=false)
     */
    private string $validationFinale = '1';

    /**
     * @ORM\Column(name="creer_suite_consultation", type="string", nullable=false)
     */
    private string $creerSuiteConsultation = '1';

    /**
     * @ORM\Column(name="hyper_admin", type="string", nullable=false)
     */
    private string $hyperAdmin = '0';

    /**
     * @ORM\Column(name="droit_gestion_services", type="string", nullable=false)
     */
    private string $droitGestionServices = '0';

    /**
     * @ORM\Column(name="suivi_acces", type="string", nullable=false)
     */
    private string $suiviAcces = '0';

    /**
     * @ORM\Column(name="statistiques_site", type="string", nullable=false)
     */
    private string $statistiquesSite = '0';

    /**
     * @ORM\Column(name="statistiques_QoS", type="string", nullable=false)
     */
    private string $statistiquesQos = '0';

    /**
     * @ORM\Column(name="ouvrir_anonymat_a_distance", type="string", nullable=false)
     */
    private string $ouvrirAnonymatADistance = '0';

    /**
     * @ORM\Column(name="gestion_compte_jal", type="string", nullable=false)
     */
    private string $gestionCompteJal = '0';

    /**
     * @ORM\Column(name="gestion_centrale_pub", type="string", nullable=true)
     */
    private ?string $gestionCentralePub = '0';

    /**
     * @ORM\Column(name="Gestion_Compte_Groupe_Moniteur", type="string", nullable=true)
     */
    private ?string $gestionCompteGroupeMoniteur = '0';

    /**
     * @ORM\Column(name="ouvrir_offre_technique_en_ligne", type="string", nullable=false)
     */
    private string $ouvrirOffreTechniqueEnLigne = '0';

    /**
     * @ORM\Column(name="ouvrir_offre_technique_a_distance", type="string", nullable=false)
     */
    private string $ouvrirOffreTechniqueADistance = '0';

    /**
     * @ORM\Column(name="activation_compte_entreprise", type="string", nullable=true)
     */
    private ?string $activationCompteEntreprise = '0';

    /**
     * @ORM\Column(name="importer_enveloppe", type="string", nullable=false)
     */
    private string $importerEnveloppe = '0';

    /**
     * @ORM\Column(name="suivi_seul_registre_depots_papier", type="string", nullable=false)
     */
    private string $suiviSeulRegistreDepotsPapier = '0';

    /**
     * @ORM\Column(name="suivi_seul_registre_retraits_papier", type="string", nullable=false)
     */
    private string $suiviSeulRegistreRetraitsPapier = '0';

    /**
     * @ORM\Column(name="suivi_seul_registre_questions_papier", type="string", nullable=false)
     */
    private string $suiviSeulRegistreQuestionsPapier = '0';

    /**
     * @ORM\Column(name="suivi_seul_registre_depots_electronique", type="string", nullable=false)
     */
    private string $suiviSeulRegistreDepotsElectronique = '1';

    /**
     * @ORM\Column(name="suivi_seul_registre_retraits_electronique", type="string", nullable=false)
     */
    private string $suiviSeulRegistreRetraitsElectronique = '1';

    /**
     * @ORM\Column(name="suivi_seul_registre_questions_electronique", type="string", nullable=false)
     */
    private string $suiviSeulRegistreQuestionsElectronique = '1';

    /**
     * @ORM\Column(name="modifier_consultation_mapa_inferieur_montant_apres_validation", type="string", nullable=false)
     */
    private string $modifierConsultationMapaInferieurMontantApresValidation = '0';

    /**
     * @ORM\Column(name="modifier_consultation_mapa_superieur_montant_apres_validation", type="string", nullable=false)
     */
    private string $modifierConsultationMapaSuperieurMontantApresValidation = '0';

    /**
     * @ORM\Column(name="modifier_consultation_procedures_formalisees_apres_validation", type="string", nullable=false)
     */
    private string $modifierConsultationProceduresFormaliseesApresValidation = '0';

    /**
     * @ORM\Column(name="gerer_les_entreprises", type="string", nullable=true)
     */
    private ?string $gererLesEntreprises = '0';

    /**
     * @ORM\Column(name="portee_societes_exclues", type="string", nullable=false)
     */
    private string $porteeSocietesExclues = '0';

    /**
     * @ORM\Column(name="portee_societes_exclues_tous_organismes", type="string", nullable=false)
     */
    private string $porteeSocietesExcluesTousOrganismes = '0';

    /**
     * @ORM\Column(name="modifier_societes_exclues", type="string", nullable=false)
     */
    private string $modifierSocietesExclues = '0';

    /**
     * @ORM\Column(name="supprimer_societes_exclues", type="string", nullable=false)
     */
    private string $supprimerSocietesExclues = '0';

    /**
     * @ORM\Column(name="resultat_analyse", type="string", nullable=false)
     */
    private string $resultatAnalyse = '0';

    /**
     * @ORM\Column(name="gerer_adresses_service", type="string", nullable=true)
     */
    private ?string $gererAdressesService = '0';

    /**
     * @ORM\Column(name="gerer_mon_service", type="string", nullable=true)
     */
    private ?string $gererMonService = '0';

    /**
     * @ORM\Column(name="download_archives", type="string", nullable=true)
     */
    private ?string $downloadArchives = '0';

    /**
     * @ORM\Column(name="creer_annonce_extrait_pv", type="string", nullable=false)
     */
    private string $creerAnnonceExtraitPv = '0';

    /**
     * @ORM\Column(name="creer_annonce_rapport_achevement", type="string", nullable=false)
     */
    private string $creerAnnonceRapportAchevement = '0';

    /**
     * @ORM\Column(name="gestion_certificats_agent", type="string", nullable=true)
     */
    private ?string $gestionCertificatsAgent = '0';

    /**
     * @ORM\Column(name="creer_avis_programme_previsionnel", type="string", nullable=true)
     */
    private ?string $creerAvisProgrammePrevisionnel = '0';

    /**
     * @ORM\Column(name="annuler_consultation", type="string", nullable=true)
     */
    private ?string $annulerConsultation = '0';

    /**
     * @ORM\Column(name="envoyer_publicite", type="string", nullable=false)
     */
    private string $envoyerPublicite = '1';

    /**
     * @ORM\Column(name="liste_marches_notifies", type="string", nullable=false)
     */
    private string $listeMarchesNotifies = '0';

    /**
     * @ORM\Column(name="suivre_message", type="string", nullable=false)
     */
    private string $suivreMessage = '1';

    /**
     * @ORM\Column(name="envoyer_message", type="string", nullable=false)
     */
    private string $envoyerMessage = '1';

    /**
     * @ORM\Column(name="suivi_flux_chorus_transversal", type="string", nullable=false)
     */
    private string $suiviFluxChorusTransversal = '0';

    /**
     * @ORM\Column(name="gestion_mandataire", type="string", nullable=true)
     */
    private ?string $gestionMandataire = '0';

    /**
     * @ORM\Column(name="gerer_newsletter", type="string", nullable=true)
     */
    private ?string $gererNewsletter = '0';

    /**
     * @ORM\Column(name="gestion_modeles_formulaire", type="string", nullable=false)
     */
    private string $gestionModelesFormulaire = '0';

    /**
     * @ORM\Column(name="gestion_adresses_facturation_jal", type="string", nullable=false)
     */
    private string $gestionAdressesFacturationJal = '0';

    /**
     * @ORM\Column(name="administrer_adresses_facturation_jal", type="string", nullable=false)
     */
    private string $administrerAdressesFacturationJal = '0';

    /**
     * @ORM\Column(name="redaction_documents_redac", type="string", nullable=false)
     */
    private string $redactionDocumentsRedac = '0';

    /**
     * @ORM\Column(name="validation_documents_redac", type="string", nullable=false)
     */
    private string $validationDocumentsRedac = '0';

    /**
     * @ORM\Column(name="gestion_mise_disposition_pieces_marche", type="string", nullable=false)
     */
    private string $gestionMiseDispositionPiecesMarche = '0';

    /**
     * @ORM\Column(name="annuaire_acheteur", type="string", nullable=false)
     */
    private string $annuaireAcheteur = '0';

    /**
     * @ORM\Column(name="reprendre_integralement_article", type="string", nullable=false)
     */
    private string $reprendreIntegralementArticle = '0';

    /**
     * @ORM\Column(name="administrer_clauses", type="string", nullable=false)
     */
    private string $administrerClauses = '0';

    /**
     * @ORM\Column(name="valider_clauses", type="string", nullable=false)
     */
    private string $validerClauses = '0';

    /**
     * @ORM\Column(name="administrer_canevas", type="string", nullable=false)
     */
    private string $administrerCanevas = '0';

    /**
     * @ORM\Column(name="valider_canevas", type="string", nullable=false)
     */
    private string $validerCanevas = '0';

    /**
     * @ORM\Column(name="administrer_clauses_entite_achats", type="string", nullable=false)
     */
    private string $administrerClausesEntiteAchats = '0';

    /**
     * @ORM\Column(name="generer_pieces_format_odt", type="string", nullable=false)
     */
    private string $genererPiecesFormatOdt = '0';

    /**
     * @ORM\Column(name="publier_version_clausier_editeur", type="string", nullable=false)
     */
    private string $publierVersionClausierEditeur = '0';

    /**
     * @ORM\Column(name="administrer_clauses_editeur", type="string", nullable=false)
     */
    private string $administrerClausesEditeur = '0';

    /**
     * @ORM\Column(name="valider_clauses_editeur", type="string", nullable=false)
     */
    private string $validerClausesEditeur = '0';

    /**
     * @ORM\Column(name="administrer_canevas_editeur", type="string", nullable=false)
     */
    private string $administrerCanevasEditeur = '0';

    /**
     * @ORM\Column(name="valider_canevas_editeur", type="string", nullable=false)
     */
    private string $validerCanevasEditeur = '0';

    /**
     * @ORM\Column(name="decision_suivi_seul", type="string", nullable=false)
     */
    private string $decisionSuiviSeul = '0';

    /**
     * @ORM\Column(name="ouvrir_candidature_hors_ligne", type="string", nullable=false)
     */
    private string $ouvrirCandidatureHorsLigne = '1';

    /**
     * @ORM\Column(name="ouvrir_offre_hors_ligne", type="string", nullable=false)
     */
    private string $ouvrirOffreHorsLigne = '1';

    /**
     * @ORM\Column(name="ouvrir_offre_technique_hors_ligne", type="string", nullable=false)
     */
    private string $ouvrirOffreTechniqueHorsLigne = '1';

    /**
     * @ORM\Column(name="ouvrir_anonymat_hors_ligne", type="string", nullable=false)
     */
    private string $ouvrirAnonymatHorsLigne = '1';

    /**
     * @ORM\Column(name="espace_collaboratif_gestionnaire", type="string", nullable=false)
     */
    private string $espaceCollaboratifGestionnaire = '0';

    /**
     * @ORM\Column(name="espace_collaboratif_contributeur", type="string", nullable=false)
     */
    private string $espaceCollaboratifContributeur = '0';

    /**
     * @ORM\Column(name="gerer_organismes", type="string", nullable=false)
     */
    private string $gererOrganismes = '0';

    /**
     * @ORM\Column(name="gerer_associations_agents", type="string", nullable=false)
     */
    private string $gererAssociationsAgents = '0';

    /**
     * @ORM\Column(name="module_redaction_uniquement", type="string", nullable=true)
     */
    private ?string $moduleRedactionUniquement = '0';

    /**
     * @ORM\Column(name="historique_navigation_inscrits", type="string", nullable=false)
     */
    private string $historiqueNavigationInscrits = '1';

    /**
     * @ORM\Column(name="telecharger_accords_cadres", type="string", nullable=false)
     */
    private string $telechargerAccordsCadres = '0';

    /**
     * @ORM\Column(name="creer_annonce_decision_resiliation", type="string", nullable=false)
     */
    private string $creerAnnonceDecisionResiliation = '0';

    /**
     * @ORM\Column(name="creer_annonce_synthese_rapport_audit", type="string", nullable=false)
     */
    private string $creerAnnonceSyntheseRapportAudit = '0';

    /**
     * @ORM\Column(name="gerer_operations", type="string", nullable=false)
     */
    private string $gererOperations = '0';

    /**
     * @ORM\Column(name="telecharger_siret_acheteur", type="string", nullable=true)
     */
    private ?string $telechargerSiretAcheteur = '0';

    /**
     * @ORM\Column(name="gerer_reouvertures_modification", type="string", nullable=false)
     */
    private string $gererReouverturesModification = '0';

    /**
     * @ORM\Column(name="acceder_tous_telechargements", type="string", nullable=true)
     */
    private ?string $accederTousTelechargements = '0';

    /**
     * @ORM\Column(name="creer_contrat", type="string", nullable=false)
     */
    private string $creerContrat = '0';

    /**
     * @ORM\Column(name="modifier_contrat", type="string", nullable=false)
     */
    private string $modifierContrat = '0';

    /**
     * @ORM\Column(name="consulter_contrat", type="string", nullable=false)
     */
    private string $consulterContrat = '0';

    /**
     * @ORM\Column(name="gerer_newsletter_redac", type="string", nullable=false)
     */
    private string $gererNewsletterRedac = '0';

    /**
     * @ORM\Column(name="profil_rma", type="string", nullable=false)
     */
    private string $profilRma = '0';

    /**
     * @ORM\Column(name="affectation_vision_rma", type="string", nullable=false)
     */
    private string $affectationVisionRma = '0';

    /**
     * @ORM\Column(name="gerer_gabarit_editeur", type="string", nullable=true)
     */
    private ?string $gererGabaritEditeur = '0';

    /**
     * @ORM\Column(name="gerer_gabarit", type="string", nullable=true)
     */
    private ?string $gererGabarit = '0';

    /**
     * @ORM\Column(name="gerer_gabarit_entite_achats", type="string", nullable=true)
     */
    private ?string $gererGabaritEntiteAchats = '0';

    /**
     * @ORM\Column(name="gerer_gabarit_agent", type="string", nullable=true)
     */
    private ?string $gererGabaritAgent = '0';

    /**
     * @ORM\Column(name="gerer_messages_accueil", type="string", nullable=true)
     */
    private ?string $gererMessagesAccueil = '0';

    /**
     * @ORM\Column(name="gerer_OA_GA", type="string", nullable=true)
     */
    private ?string $gererOaGa = '0';

    /**
     * @ORM\Column(name="deplacer_service", type="string", nullable=true)
     */
    private ?string $deplacerService = '0';

    /**
     * @ORM\Column(name="activer_version_clausier", type="string", nullable=true)
     */
    private ?string $activerVersionClausier = '0';

    /**
     * @ORM\Column(name="exec_voir_contrats_ea", type="string", nullable=false)
     */
    private string $execVoirContratsEa = '0';

    /**
     * @ORM\Column(name="exec_voir_contrats_ea_dependantes", type="string", nullable=false)
     */
    private string $execVoirContratsEaDependantes = '0';

    /**
     * @ORM\Column(name="exec_voir_contrats_organisme", type="string", nullable=false)
     */
    private string $execVoirContratsOrganisme = '0';

    /**
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="App\Entity\Agent", inversedBy="habilitation")
     * @ORM\JoinColumn(name="id_agent", referencedColumnName="id")
     */
    private $agent = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="acces_ws", type="boolean", nullable=false)
     */
    private $accesWs = false;

    /**
     * @ORM\Column(name="invite_permanent_mon_entite", type="string", nullable=true)
     */
    private $invitePermanentMonEntite = 0;

    /**
     * @ORM\Column(name="invite_permanent_entite_dependante", type="string", nullable=true)
     */
    private $invitePermanentEntiteDependante = 0;

    /**
     * @ORM\Column(name="invite_permanent_transverse", type="string", nullable=true)
     */
    private $invitePermanentTransverse = 0;

    /**
     * @var bool
     * @ORM\Column(name="espace_documentaire_consultation", type="boolean", nullable=false)
     */
    private $espaceDocumentaireConsultation = false;

    /**
     * @var bool
     * @ORM\Column(name="acces_echange_documentaire", type="boolean", nullable=false)
     */
    private $accesEchangeDocumentaire = false;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $administrerOrganisme = 0;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $gestionEnvol = 0;

    /**
     * @var bool
     *
     * @ORM\Column(type="string", type="boolean", nullable=false)
     */
    private $execModificationContrat = '0';

    /**
     * @ORM\Embedded(class = "App\Entity\Agent\Habilitations\RecensementProgrammationStrategieAchat",
     *     columnPrefix = false)
     */
    private RecensementProgrammationStrategieAchat $recensementProgrammationStrategieAchat;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $moduleAutoformation = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $gestionSpaserConsultations = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $gestionValidationEco = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $gestionValidationSip = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $rattachementService = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $duplicationConsultations = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $projetAchatLancementSourcing = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $projetAchatInvalidation = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $projetAchatAnnulation = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $lancementProcedure = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $recensementInvaliderProjetAchat = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $recensementAnnulerProjetAchat = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $administrationDocumentsModeles = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $supprimerContrat = false;

    /**
     * @return string
     */
    public function getGestionAgentPole()
    {
        return $this->gestionAgentPole;
    }

    /**
     * @param string $gestionAgentPole
     */
    public function setGestionAgentPole($gestionAgentPole)
    {
        $this->gestionAgentPole = $gestionAgentPole;
    }

    /**
     * @return string
     */
    public function getGestionFournisseursEnvoisPostaux()
    {
        return $this->gestionFournisseursEnvoisPostaux;
    }

    /**
     * @param string $gestionFournisseursEnvoisPostaux
     */
    public function setGestionFournisseursEnvoisPostaux($gestionFournisseursEnvoisPostaux)
    {
        $this->gestionFournisseursEnvoisPostaux = $gestionFournisseursEnvoisPostaux;
    }

    /**
     * @return string
     */
    public function getGestionBiCles()
    {
        return $this->gestionBiCles;
    }

    /**
     * @param string $gestionBiCles
     */
    public function setGestionBiCles($gestionBiCles)
    {
        $this->gestionBiCles = $gestionBiCles;
    }

    /**
     * @return string
     */
    public function getCreerConsultation()
    {
        return $this->creerConsultation;
    }

    /**
     * @param string $creerConsultation
     */
    public function setCreerConsultation($creerConsultation)
    {
        $this->creerConsultation = $creerConsultation;
    }

    /**
     * @return string
     */
    public function getModifierConsultation()
    {
        return $this->modifierConsultation;
    }

    /**
     * @param string $modifierConsultation
     */
    public function setModifierConsultation($modifierConsultation)
    {
        $this->modifierConsultation = $modifierConsultation;
    }

    /**
     * @return string
     */
    public function getValiderConsultation()
    {
        return $this->validerConsultation;
    }

    /**
     * @param string $validerConsultation
     */
    public function setValiderConsultation($validerConsultation)
    {
        $this->validerConsultation = $validerConsultation;
    }

    /**
     * @return string
     */
    public function getPublierConsultation()
    {
        return $this->publierConsultation;
    }

    /**
     * @param string $publierConsultation
     */
    public function setPublierConsultation($publierConsultation)
    {
        $this->publierConsultation = $publierConsultation;
    }

    /**
     * @return string
     */
    public function getSuivreConsultation()
    {
        return $this->suivreConsultation;
    }

    /**
     * @param string $suivreConsultation
     */
    public function setSuivreConsultation($suivreConsultation)
    {
        $this->suivreConsultation = $suivreConsultation;
    }

    /**
     * @return string
     */
    public function getSuivreConsultationPole()
    {
        return $this->suivreConsultationPole;
    }

    /**
     * @param string $suivreConsultationPole
     */
    public function setSuivreConsultationPole($suivreConsultationPole)
    {
        $this->suivreConsultationPole = $suivreConsultationPole;
    }

    /**
     * @return string
     */
    public function getSupprimerEnveloppe()
    {
        return $this->supprimerEnveloppe;
    }

    /**
     * @param string $supprimerEnveloppe
     */
    public function setSupprimerEnveloppe($supprimerEnveloppe)
    {
        $this->supprimerEnveloppe = $supprimerEnveloppe;
    }

    /**
     * @return string
     */
    public function getSupprimerConsultation()
    {
        return $this->supprimerConsultation;
    }

    /**
     * @param string $supprimerConsultation
     */
    public function setSupprimerConsultation($supprimerConsultation)
    {
        $this->supprimerConsultation = $supprimerConsultation;
    }

    /**
     * @return string
     */
    public function getDepouillerCandidature()
    {
        return $this->depouillerCandidature;
    }

    /**
     * @param string $depouillerCandidature
     */
    public function setDepouillerCandidature($depouillerCandidature)
    {
        $this->depouillerCandidature = $depouillerCandidature;
    }

    /**
     * @return string
     */
    public function getDepouillerOffre()
    {
        return $this->depouillerOffre;
    }

    /**
     * @param string $depouillerOffre
     */
    public function setDepouillerOffre($depouillerOffre)
    {
        $this->depouillerOffre = $depouillerOffre;
    }

    /**
     * @return string
     */
    public function getMessagerieSecurisee()
    {
        return $this->messagerieSecurisee;
    }

    /**
     * @param string $messagerieSecurisee
     */
    public function setMessagerieSecurisee($messagerieSecurisee)
    {
        $this->messagerieSecurisee = $messagerieSecurisee;
    }

    /**
     * @return string
     */
    public function getAccesRegistreDepotsPapier()
    {
        return $this->accesRegistreDepotsPapier;
    }

    /**
     * @param string $accesRegistreDepotsPapier
     */
    public function setAccesRegistreDepotsPapier($accesRegistreDepotsPapier)
    {
        $this->accesRegistreDepotsPapier = $accesRegistreDepotsPapier;
    }

    /**
     * @return string
     */
    public function getAccesRegistreRetraitsPapier()
    {
        return $this->accesRegistreRetraitsPapier;
    }

    /**
     * @param string $accesRegistreRetraitsPapier
     */
    public function setAccesRegistreRetraitsPapier($accesRegistreRetraitsPapier)
    {
        $this->accesRegistreRetraitsPapier = $accesRegistreRetraitsPapier;
    }

    /**
     * @return string
     */
    public function getAccesRegistreQuestionsPapier()
    {
        return $this->accesRegistreQuestionsPapier;
    }

    /**
     * @param string $accesRegistreQuestionsPapier
     */
    public function setAccesRegistreQuestionsPapier($accesRegistreQuestionsPapier)
    {
        $this->accesRegistreQuestionsPapier = $accesRegistreQuestionsPapier;
    }

    /**
     * @return string
     */
    public function getGererEncheres()
    {
        return $this->gererEncheres;
    }

    /**
     * @param string $gererEncheres
     */
    public function setGererEncheres($gererEncheres)
    {
        $this->gererEncheres = $gererEncheres;
    }

    /**
     * @return string
     */
    public function getSuivreEncheres()
    {
        return $this->suivreEncheres;
    }

    /**
     * @param string $suivreEncheres
     */
    public function setSuivreEncheres($suivreEncheres)
    {
        $this->suivreEncheres = $suivreEncheres;
    }

    /**
     * @return string
     */
    public function getSuiviEntreprise()
    {
        return $this->suiviEntreprise;
    }

    /**
     * @param string $suiviEntreprise
     */
    public function setSuiviEntreprise($suiviEntreprise)
    {
        $this->suiviEntreprise = $suiviEntreprise;
    }

    /**
     * @return string
     */
    public function getEnvoiBoamp()
    {
        return $this->envoiBoamp;
    }

    /**
     * @param string $envoiBoamp
     */
    public function setEnvoiBoamp($envoiBoamp)
    {
        $this->envoiBoamp = $envoiBoamp;
    }

    /**
     * @return string
     */
    public function getAccesClassementLot()
    {
        return $this->accesClassementLot;
    }

    /**
     * @param string $accesClassementLot
     */
    public function setAccesClassementLot($accesClassementLot)
    {
        $this->accesClassementLot = $accesClassementLot;
    }

    /**
     * @return string
     */
    public function getConnecteurSis()
    {
        return $this->connecteurSis;
    }

    /**
     * @param string $connecteurSis
     */
    public function setConnecteurSis($connecteurSis)
    {
        $this->connecteurSis = $connecteurSis;
    }

    /**
     * @return string
     */
    public function getConnecteurMarco()
    {
        return $this->connecteurMarco;
    }

    /**
     * @param string $connecteurMarco
     */
    public function setConnecteurMarco($connecteurMarco)
    {
        $this->connecteurMarco = $connecteurMarco;
    }

    /**
     * @return string
     */
    public function getRepondreAuxQuestions()
    {
        return $this->repondreAuxQuestions;
    }

    /**
     * @param string $repondreAuxQuestions
     */
    public function setRepondreAuxQuestions($repondreAuxQuestions)
    {
        $this->repondreAuxQuestions = $repondreAuxQuestions;
    }

    /**
     * @return string
     */
    public function getAppelProjetFormation()
    {
        return $this->appelProjetFormation;
    }

    /**
     * @param string $appelProjetFormation
     */
    public function setAppelProjetFormation($appelProjetFormation)
    {
        $this->appelProjetFormation = $appelProjetFormation;
    }

    /**
     * @return string
     */
    public function getUtiliserClientCao()
    {
        return $this->utiliserClientCao;
    }

    /**
     * @param string $utiliserClientCao
     */
    public function setUtiliserClientCao($utiliserClientCao)
    {
        $this->utiliserClientCao = $utiliserClientCao;
    }

    /**
     * @return string
     */
    public function getNotificationBoamp()
    {
        return $this->notificationBoamp;
    }

    /**
     * @param string $notificationBoamp
     */
    public function setNotificationBoamp($notificationBoamp)
    {
        $this->notificationBoamp = $notificationBoamp;
    }

    /**
     * @return string
     */
    public function getAdministrerCompte()
    {
        return $this->administrerCompte;
    }

    /**
     * @param string $administrerCompte
     */
    public function setAdministrerCompte($administrerCompte)
    {
        $this->administrerCompte = $administrerCompte;
    }

    /**
     * @return string
     */
    public function getGestionMapa()
    {
        return $this->gestionMapa;
    }

    /**
     * @param string $gestionMapa
     */
    public function setGestionMapa($gestionMapa)
    {
        $this->gestionMapa = $gestionMapa;
    }

    /**
     * @return string
     */
    public function getGestionTypeValidation()
    {
        return $this->gestionTypeValidation;
    }

    /**
     * @param string $gestionTypeValidation
     */
    public function setGestionTypeValidation($gestionTypeValidation)
    {
        $this->gestionTypeValidation = $gestionTypeValidation;
    }

    /**
     * @return string
     */
    public function getApprouverConsultation()
    {
        return $this->approuverConsultation;
    }

    /**
     * @param string $approuverConsultation
     */
    public function setApprouverConsultation($approuverConsultation)
    {
        $this->approuverConsultation = $approuverConsultation;
    }

    /**
     * @return string
     */
    public function getAdministrerProcedure()
    {
        return $this->administrerProcedure;
    }

    /**
     * @param string $administrerProcedure
     */
    public function setAdministrerProcedure($administrerProcedure)
    {
        $this->administrerProcedure = $administrerProcedure;
    }

    /**
     * @return string
     */
    public function getRestreindreCreation()
    {
        return $this->restreindreCreation;
    }

    /**
     * @param string $restreindreCreation
     */
    public function setRestreindreCreation($restreindreCreation)
    {
        $this->restreindreCreation = $restreindreCreation;
    }

    /**
     * @return string
     */
    public function getCreerListeMarches()
    {
        return $this->creerListeMarches;
    }

    /**
     * @param string $creerListeMarches
     */
    public function setCreerListeMarches($creerListeMarches)
    {
        $this->creerListeMarches = $creerListeMarches;
    }

    /**
     * @return string
     */
    public function getGestionCommissions()
    {
        return $this->gestionCommissions;
    }

    /**
     * @param string $gestionCommissions
     */
    public function setGestionCommissions($gestionCommissions)
    {
        $this->gestionCommissions = $gestionCommissions;
    }

    /**
     * @return string
     */
    public function getSuiviSeulConsultation()
    {
        return $this->suiviSeulConsultation;
    }

    /**
     * @param string $suiviSeulConsultation
     */
    public function setSuiviSeulConsultation($suiviSeulConsultation)
    {
        $this->suiviSeulConsultation = $suiviSeulConsultation;
    }

    /**
     * @return string
     */
    public function getAttributionMarche()
    {
        return $this->attributionMarche;
    }

    /**
     * @param string $attributionMarche
     */
    public function setAttributionMarche($attributionMarche)
    {
        $this->attributionMarche = $attributionMarche;
    }

    /**
     * @return string
     */
    public function getFicheRecensement()
    {
        return $this->ficheRecensement;
    }

    /**
     * @param string $ficheRecensement
     */
    public function setFicheRecensement($ficheRecensement)
    {
        $this->ficheRecensement = $ficheRecensement;
    }

    /**
     * @return string
     */
    public function getDeclarerInfructueux()
    {
        return $this->declarerInfructueux;
    }

    /**
     * @param string $declarerInfructueux
     */
    public function setDeclarerInfructueux($declarerInfructueux)
    {
        $this->declarerInfructueux = $declarerInfructueux;
    }

    /**
     * @return string
     */
    public function getDeclarerSansSuite()
    {
        return $this->declarerSansSuite;
    }

    /**
     * @param string $declarerSansSuite
     */
    public function setDeclarerSansSuite($declarerSansSuite)
    {
        $this->declarerSansSuite = $declarerSansSuite;
    }

    /**
     * @return string
     */
    public function getCreerConsultationTransverse()
    {
        return $this->creerConsultationTransverse;
    }

    /**
     * @param string $creerConsultationTransverse
     */
    public function setCreerConsultationTransverse($creerConsultationTransverse)
    {
        $this->creerConsultationTransverse = $creerConsultationTransverse;
    }

    /**
     * @return string
     */
    public function getOuvrirCandidatureEnLigne()
    {
        return $this->ouvrirCandidatureEnLigne;
    }

    /**
     * @param string $ouvrirCandidatureEnLigne
     */
    public function setOuvrirCandidatureEnLigne($ouvrirCandidatureEnLigne)
    {
        $this->ouvrirCandidatureEnLigne = $ouvrirCandidatureEnLigne;
    }

    /**
     * @return string
     */
    public function getOuvrirCandidatureADistance()
    {
        return $this->ouvrirCandidatureADistance;
    }

    /**
     * @param string $ouvrirCandidatureADistance
     */
    public function setOuvrirCandidatureADistance($ouvrirCandidatureADistance)
    {
        $this->ouvrirCandidatureADistance = $ouvrirCandidatureADistance;
    }

    /**
     * @return string
     */
    public function getRefuserEnveloppe()
    {
        return $this->refuserEnveloppe;
    }

    /**
     * @param string $refuserEnveloppe
     */
    public function setRefuserEnveloppe($refuserEnveloppe)
    {
        $this->refuserEnveloppe = $refuserEnveloppe;
    }

    /**
     * @return string
     */
    public function getGererAdmissibilite()
    {
        return $this->gererAdmissibilite;
    }

    /**
     * @param string $gererAdmissibilite
     */
    public function setGererAdmissibilite($gererAdmissibilite)
    {
        $this->gererAdmissibilite = $gererAdmissibilite;
    }

    /**
     * @return string
     */
    public function getRestaurerEnveloppe()
    {
        return $this->restaurerEnveloppe;
    }

    /**
     * @param string $restaurerEnveloppe
     */
    public function setRestaurerEnveloppe($restaurerEnveloppe)
    {
        $this->restaurerEnveloppe = $restaurerEnveloppe;
    }

    /**
     * @return string
     */
    public function getOuvrirAnonymatEnLigne()
    {
        return $this->ouvrirAnonymatEnLigne;
    }

    /**
     * @param string $ouvrirAnonymatEnLigne
     */
    public function setOuvrirAnonymatEnLigne($ouvrirAnonymatEnLigne)
    {
        $this->ouvrirAnonymatEnLigne = $ouvrirAnonymatEnLigne;
    }

    /**
     * @return string
     */
    public function getOuvrirOffreEnLigne()
    {
        return $this->ouvrirOffreEnLigne;
    }

    /**
     * @param string $ouvrirOffreEnLigne
     */
    public function setOuvrirOffreEnLigne($ouvrirOffreEnLigne)
    {
        $this->ouvrirOffreEnLigne = $ouvrirOffreEnLigne;
    }

    /**
     * @return string
     */
    public function getGestionCompteBoamp()
    {
        return $this->gestionCompteBoamp;
    }

    /**
     * @param string $gestionCompteBoamp
     */
    public function setGestionCompteBoamp($gestionCompteBoamp)
    {
        $this->gestionCompteBoamp = $gestionCompteBoamp;
    }

    /**
     * @return string
     */
    public function getGestionAgents()
    {
        return $this->gestionAgents;
    }

    /**
     * @param string $gestionAgents
     */
    public function setGestionAgents($gestionAgents)
    {
        $this->gestionAgents = $gestionAgents;
    }

    /**
     * @return string
     */
    public function getGestionHabilitations()
    {
        return $this->gestionHabilitations;
    }

    /**
     * @param string $gestionHabilitations
     */
    public function setGestionHabilitations($gestionHabilitations)
    {
        $this->gestionHabilitations = $gestionHabilitations;
    }

    /**
     * @return string
     */
    public function getGererMapaInferieurMontant()
    {
        return $this->gererMapaInferieurMontant;
    }

    /**
     * @param string $gererMapaInferieurMontant
     */
    public function setGererMapaInferieurMontant($gererMapaInferieurMontant)
    {
        $this->gererMapaInferieurMontant = $gererMapaInferieurMontant;
    }

    /**
     * @return string
     */
    public function getGererMapaSuperieurMontant()
    {
        return $this->gererMapaSuperieurMontant;
    }

    /**
     * @param string $gererMapaSuperieurMontant
     */
    public function setGererMapaSuperieurMontant($gererMapaSuperieurMontant)
    {
        $this->gererMapaSuperieurMontant = $gererMapaSuperieurMontant;
    }

    /**
     * @return string
     */
    public function getModifierConsultationAvantValidation()
    {
        return $this->modifierConsultationAvantValidation;
    }

    /**
     * @param string $modifierConsultationAvantValidation
     */
    public function setModifierConsultationAvantValidation($modifierConsultationAvantValidation)
    {
        $this->modifierConsultationAvantValidation = $modifierConsultationAvantValidation;
    }

    /**
     * @return string
     */
    public function getModifierConsultationApresValidation()
    {
        return $this->modifierConsultationApresValidation;
    }

    /**
     * @param string $modifierConsultationApresValidation
     */
    public function setModifierConsultationApresValidation($modifierConsultationApresValidation)
    {
        $this->modifierConsultationApresValidation = $modifierConsultationApresValidation;
    }

    /**
     * @return string
     */
    public function getAccesReponses()
    {
        return $this->accesReponses;
    }

    /**
     * @param string $accesReponses
     */
    public function setAccesReponses($accesReponses)
    {
        $this->accesReponses = $accesReponses;
    }

    /**
     * @return string
     */
    public function getTelechargementGroupeAnticipePlisChiffres()
    {
        return $this->telechargementGroupeAnticipePlisChiffres;
    }

    /**
     * @param string $telechargementGroupeAnticipePlisChiffres
     */
    public function setTelechargementGroupeAnticipePlisChiffres($telechargementGroupeAnticipePlisChiffres)
    {
        $this->telechargementGroupeAnticipePlisChiffres = $telechargementGroupeAnticipePlisChiffres;
    }

    /**
     * @return string
     */
    public function getTelechargementUnitairePlisChiffres()
    {
        return $this->telechargementUnitairePlisChiffres;
    }

    /**
     * @param string $telechargementUnitairePlisChiffres
     */
    public function setTelechargementUnitairePlisChiffres($telechargementUnitairePlisChiffres)
    {
        $this->telechargementUnitairePlisChiffres = $telechargementUnitairePlisChiffres;
    }

    /**
     * @return string
     */
    public function getOuvrirOffreADistance()
    {
        return $this->ouvrirOffreADistance;
    }

    /**
     * @param string $ouvrirOffreADistance
     */
    public function setOuvrirOffreADistance($ouvrirOffreADistance)
    {
        $this->ouvrirOffreADistance = $ouvrirOffreADistance;
    }

    /**
     * @return string
     */
    public function getCreerAnnonceInformation()
    {
        return $this->creerAnnonceInformation;
    }

    /**
     * @param string $creerAnnonceInformation
     */
    public function setCreerAnnonceInformation($creerAnnonceInformation)
    {
        $this->creerAnnonceInformation = $creerAnnonceInformation;
    }

    /**
     * @return string
     */
    public function getSaisieMarches()
    {
        return $this->saisieMarches;
    }

    /**
     * @param string $saisieMarches
     */
    public function setSaisieMarches($saisieMarches)
    {
        $this->saisieMarches = $saisieMarches;
    }

    /**
     * @return string
     */
    public function getValidationMarches()
    {
        return $this->validationMarches;
    }

    /**
     * @param string $validationMarches
     */
    public function setValidationMarches($validationMarches)
    {
        $this->validationMarches = $validationMarches;
    }

    /**
     * @return string
     */
    public function getPublicationMarches()
    {
        return $this->publicationMarches;
    }

    /**
     * @param string $publicationMarches
     */
    public function setPublicationMarches($publicationMarches)
    {
        $this->publicationMarches = $publicationMarches;
    }

    /**
     * @return string
     */
    public function getGererStatistiquesMetier()
    {
        return $this->gererStatistiquesMetier;
    }

    /**
     * @param string $gererStatistiquesMetier
     */
    public function setGererStatistiquesMetier($gererStatistiquesMetier)
    {
        $this->gererStatistiquesMetier = $gererStatistiquesMetier;
    }

    /**
     * @return string
     */
    public function getGererArchives()
    {
        return $this->gererArchives;
    }

    /**
     * @param string $gererArchives
     */
    public function setGererArchives($gererArchives)
    {
        $this->gererArchives = $gererArchives;
    }

    /**
     * @return string
     */
    public function getAdministrerProceduresFormalisees()
    {
        return $this->administrerProceduresFormalisees;
    }

    /**
     * @param string $administrerProceduresFormalisees
     */
    public function setAdministrerProceduresFormalisees($administrerProceduresFormalisees)
    {
        $this->administrerProceduresFormalisees = $administrerProceduresFormalisees;
    }

    /**
     * @return string
     */
    public function getCreerAnnonceAttribution()
    {
        return $this->creerAnnonceAttribution;
    }

    /**
     * @param string $creerAnnonceAttribution
     */
    public function setCreerAnnonceAttribution($creerAnnonceAttribution)
    {
        $this->creerAnnonceAttribution = $creerAnnonceAttribution;
    }

    /**
     * @return string
     */
    public function getAccesRegistreRetraitsElectronique()
    {
        return $this->accesRegistreRetraitsElectronique;
    }

    /**
     * @param string $accesRegistreRetraitsElectronique
     */
    public function setAccesRegistreRetraitsElectronique($accesRegistreRetraitsElectronique)
    {
        $this->accesRegistreRetraitsElectronique = $accesRegistreRetraitsElectronique;
    }

    /**
     * @return string
     */
    public function getAccesRegistreQuestionsElectronique()
    {
        return $this->accesRegistreQuestionsElectronique;
    }

    /**
     * @param string $accesRegistreQuestionsElectronique
     */
    public function setAccesRegistreQuestionsElectronique($accesRegistreQuestionsElectronique)
    {
        $this->accesRegistreQuestionsElectronique = $accesRegistreQuestionsElectronique;
    }

    /**
     * @return string
     */
    public function getAccesRegistreDepotsElectronique()
    {
        return $this->accesRegistreDepotsElectronique;
    }

    /**
     * @param string $accesRegistreDepotsElectronique
     */
    public function setAccesRegistreDepotsElectronique($accesRegistreDepotsElectronique)
    {
        $this->accesRegistreDepotsElectronique = $accesRegistreDepotsElectronique;
    }

    /**
     * @return string
     */
    public function getValidationSimple()
    {
        return $this->validationSimple;
    }

    /**
     * @param string $validationSimple
     */
    public function setValidationSimple($validationSimple)
    {
        $this->validationSimple = $validationSimple;
    }

    /**
     * @return string
     */
    public function getValidationIntermediaire()
    {
        return $this->validationIntermediaire;
    }

    /**
     * @param string $validationIntermediaire
     */
    public function setValidationIntermediaire($validationIntermediaire)
    {
        $this->validationIntermediaire = $validationIntermediaire;
    }

    /**
     * @return string
     */
    public function getValidationFinale()
    {
        return $this->validationFinale;
    }

    /**
     * @param string $validationFinale
     */
    public function setValidationFinale($validationFinale)
    {
        $this->validationFinale = $validationFinale;
    }

    /**
     * @return string
     */
    public function getCreerSuiteConsultation()
    {
        return $this->creerSuiteConsultation;
    }

    /**
     * @param string $creerSuiteConsultation
     */
    public function setCreerSuiteConsultation($creerSuiteConsultation)
    {
        $this->creerSuiteConsultation = $creerSuiteConsultation;
    }

    /**
     * @return string
     */
    public function getHyperAdmin()
    {
        return $this->hyperAdmin;
    }

    /**
     * @param string $hyperAdmin
     */
    public function setHyperAdmin($hyperAdmin)
    {
        $this->hyperAdmin = $hyperAdmin;
    }

    /**
     * @return string
     */
    public function getDroitGestionServices()
    {
        return $this->droitGestionServices;
    }

    /**
     * @param string $droitGestionServices
     */
    public function setDroitGestionServices($droitGestionServices)
    {
        $this->droitGestionServices = $droitGestionServices;
    }

    /**
     * @return string
     */
    public function getSuiviAcces()
    {
        return $this->suiviAcces;
    }

    /**
     * @param string $suiviAcces
     */
    public function setSuiviAcces($suiviAcces)
    {
        $this->suiviAcces = $suiviAcces;
    }

    /**
     * @return string
     */
    public function getStatistiquesSite()
    {
        return $this->statistiquesSite;
    }

    /**
     * @param string $statistiquesSite
     */
    public function setStatistiquesSite($statistiquesSite)
    {
        $this->statistiquesSite = $statistiquesSite;
    }

    /**
     * @return string
     */
    public function getStatistiquesQos()
    {
        return $this->statistiquesQos;
    }

    /**
     * @param string $statistiquesQos
     */
    public function setStatistiquesQos($statistiquesQos)
    {
        $this->statistiquesQos = $statistiquesQos;
    }

    /**
     * @return string
     */
    public function getOuvrirAnonymatADistance()
    {
        return $this->ouvrirAnonymatADistance;
    }

    /**
     * @param string $ouvrirAnonymatADistance
     */
    public function setOuvrirAnonymatADistance($ouvrirAnonymatADistance)
    {
        $this->ouvrirAnonymatADistance = $ouvrirAnonymatADistance;
    }

    /**
     * @return string
     */
    public function getGestionCompteJal()
    {
        return $this->gestionCompteJal;
    }

    /**
     * @param string $gestionCompteJal
     */
    public function setGestionCompteJal($gestionCompteJal)
    {
        $this->gestionCompteJal = $gestionCompteJal;
    }

    /**
     * @return string
     */
    public function getGestionCentralePub()
    {
        return $this->gestionCentralePub;
    }

    /**
     * @param string $gestionCentralePub
     */
    public function setGestionCentralePub($gestionCentralePub)
    {
        $this->gestionCentralePub = $gestionCentralePub;
    }

    /**
     * @return string
     */
    public function getGestionCompteGroupeMoniteur()
    {
        return $this->gestionCompteGroupeMoniteur;
    }

    /**
     * @param string $gestionCompteGroupeMoniteur
     */
    public function setGestionCompteGroupeMoniteur($gestionCompteGroupeMoniteur)
    {
        $this->gestionCompteGroupeMoniteur = $gestionCompteGroupeMoniteur;
    }

    /**
     * @return string
     */
    public function getOuvrirOffreTechniqueEnLigne()
    {
        return $this->ouvrirOffreTechniqueEnLigne;
    }

    /**
     * @param string $ouvrirOffreTechniqueEnLigne
     */
    public function setOuvrirOffreTechniqueEnLigne($ouvrirOffreTechniqueEnLigne)
    {
        $this->ouvrirOffreTechniqueEnLigne = $ouvrirOffreTechniqueEnLigne;
    }

    /**
     * @return string
     */
    public function getOuvrirOffreTechniqueADistance()
    {
        return $this->ouvrirOffreTechniqueADistance;
    }

    /**
     * @param string $ouvrirOffreTechniqueADistance
     */
    public function setOuvrirOffreTechniqueADistance($ouvrirOffreTechniqueADistance)
    {
        $this->ouvrirOffreTechniqueADistance = $ouvrirOffreTechniqueADistance;
    }

    /**
     * @return string
     */
    public function getActivationCompteEntreprise()
    {
        return $this->activationCompteEntreprise;
    }

    /**
     * @param string $activationCompteEntreprise
     */
    public function setActivationCompteEntreprise($activationCompteEntreprise)
    {
        $this->activationCompteEntreprise = $activationCompteEntreprise;
    }

    /**
     * @return string
     */
    public function getImporterEnveloppe()
    {
        return $this->importerEnveloppe;
    }

    /**
     * @param string $importerEnveloppe
     */
    public function setImporterEnveloppe($importerEnveloppe)
    {
        $this->importerEnveloppe = $importerEnveloppe;
    }

    /**
     * @return string
     */
    public function getSuiviSeulRegistreDepotsPapier()
    {
        return $this->suiviSeulRegistreDepotsPapier;
    }

    /**
     * @param string $suiviSeulRegistreDepotsPapier
     */
    public function setSuiviSeulRegistreDepotsPapier($suiviSeulRegistreDepotsPapier)
    {
        $this->suiviSeulRegistreDepotsPapier = $suiviSeulRegistreDepotsPapier;
    }

    /**
     * @return string
     */
    public function getSuiviSeulRegistreRetraitsPapier()
    {
        return $this->suiviSeulRegistreRetraitsPapier;
    }

    /**
     * @param string $suiviSeulRegistreRetraitsPapier
     */
    public function setSuiviSeulRegistreRetraitsPapier($suiviSeulRegistreRetraitsPapier)
    {
        $this->suiviSeulRegistreRetraitsPapier = $suiviSeulRegistreRetraitsPapier;
    }

    /**
     * @return string
     */
    public function getSuiviSeulRegistreQuestionsPapier()
    {
        return $this->suiviSeulRegistreQuestionsPapier;
    }

    /**
     * @param string $suiviSeulRegistreQuestionsPapier
     */
    public function setSuiviSeulRegistreQuestionsPapier($suiviSeulRegistreQuestionsPapier)
    {
        $this->suiviSeulRegistreQuestionsPapier = $suiviSeulRegistreQuestionsPapier;
    }

    /**
     * @return string
     */
    public function getSuiviSeulRegistreDepotsElectronique()
    {
        return $this->suiviSeulRegistreDepotsElectronique;
    }

    /**
     * @param string $suiviSeulRegistreDepotsElectronique
     */
    public function setSuiviSeulRegistreDepotsElectronique($suiviSeulRegistreDepotsElectronique)
    {
        $this->suiviSeulRegistreDepotsElectronique = $suiviSeulRegistreDepotsElectronique;
    }

    /**
     * @return string
     */
    public function getSuiviSeulRegistreRetraitsElectronique()
    {
        return $this->suiviSeulRegistreRetraitsElectronique;
    }

    /**
     * @param string $suiviSeulRegistreRetraitsElectronique
     */
    public function setSuiviSeulRegistreRetraitsElectronique($suiviSeulRegistreRetraitsElectronique)
    {
        $this->suiviSeulRegistreRetraitsElectronique = $suiviSeulRegistreRetraitsElectronique;
    }

    /**
     * @return string
     */
    public function getSuiviSeulRegistreQuestionsElectronique()
    {
        return $this->suiviSeulRegistreQuestionsElectronique;
    }

    /**
     * @param string $suiviSeulRegistreQuestionsElectronique
     */
    public function setSuiviSeulRegistreQuestionsElectronique($suiviSeulRegistreQuestionsElectronique)
    {
        $this->suiviSeulRegistreQuestionsElectronique = $suiviSeulRegistreQuestionsElectronique;
    }

    /**
     * @return string
     */
    public function getModifierConsultationMapaInferieurMontantApresValidation()
    {
        return $this->modifierConsultationMapaInferieurMontantApresValidation;
    }

    /**
     * @param string $modifierConsultationMapaInferieurMontantApresValidation
     */
    public function setModifierConsultationMapaInferieurMontantApresValidation(
        $modifierConsultationMapaInferieurMontantApresValidation
    ) {
        $this->modifierConsultationMapaInferieurMontantApresValidation =
            $modifierConsultationMapaInferieurMontantApresValidation;
    }

    /**
     * @return string
     */
    public function getModifierConsultationMapaSuperieurMontantApresValidation()
    {
        return $this->modifierConsultationMapaSuperieurMontantApresValidation;
    }

    /**
     * @param string $modifierConsultationMapaSuperieurMontantApresValidation
     */
    public function setModifierConsultationMapaSuperieurMontantApresValidation(
        $modifierConsultationMapaSuperieurMontantApresValidation
    ) {
        $this->modifierConsultationMapaSuperieurMontantApresValidation =
            $modifierConsultationMapaSuperieurMontantApresValidation;
    }

    /**
     * @return string
     */
    public function getModifierConsultationProceduresFormaliseesApresValidation()
    {
        return $this->modifierConsultationProceduresFormaliseesApresValidation;
    }

    /**
     * @param string $modifierConsultationProceduresFormaliseesApresValidation
     */
    public function setModifierConsultationProceduresFormaliseesApresValidation(
        $modifierConsultationProceduresFormaliseesApresValidation
    ) {
        $this->modifierConsultationProceduresFormaliseesApresValidation =
            $modifierConsultationProceduresFormaliseesApresValidation;
    }

    /**
     * @return string
     */
    public function getGererLesEntreprises()
    {
        return $this->gererLesEntreprises;
    }

    /**
     * @param string $gererLesEntreprises
     */
    public function setGererLesEntreprises($gererLesEntreprises)
    {
        $this->gererLesEntreprises = $gererLesEntreprises;
    }

    /**
     * @return string
     */
    public function getPorteeSocietesExclues()
    {
        return $this->porteeSocietesExclues;
    }

    /**
     * @param string $porteeSocietesExclues
     */
    public function setPorteeSocietesExclues($porteeSocietesExclues)
    {
        $this->porteeSocietesExclues = $porteeSocietesExclues;
    }

    /**
     * @return string
     */
    public function getPorteeSocietesExcluesTousOrganismes()
    {
        return $this->porteeSocietesExcluesTousOrganismes;
    }

    /**
     * @param string $porteeSocietesExcluesTousOrganismes
     */
    public function setPorteeSocietesExcluesTousOrganismes($porteeSocietesExcluesTousOrganismes)
    {
        $this->porteeSocietesExcluesTousOrganismes = $porteeSocietesExcluesTousOrganismes;
    }

    /**
     * @return string
     */
    public function getModifierSocietesExclues()
    {
        return $this->modifierSocietesExclues;
    }

    /**
     * @param string $modifierSocietesExclues
     */
    public function setModifierSocietesExclues($modifierSocietesExclues)
    {
        $this->modifierSocietesExclues = $modifierSocietesExclues;
    }

    /**
     * @return string
     */
    public function getSupprimerSocietesExclues()
    {
        return $this->supprimerSocietesExclues;
    }

    /**
     * @param string $supprimerSocietesExclues
     */
    public function setSupprimerSocietesExclues($supprimerSocietesExclues)
    {
        $this->supprimerSocietesExclues = $supprimerSocietesExclues;
    }

    /**
     * @return string
     */
    public function getResultatAnalyse()
    {
        return $this->resultatAnalyse;
    }

    /**
     * @param string $resultatAnalyse
     */
    public function setResultatAnalyse($resultatAnalyse)
    {
        $this->resultatAnalyse = $resultatAnalyse;
    }

    /**
     * @return string
     */
    public function getGererAdressesService()
    {
        return $this->gererAdressesService;
    }

    /**
     * @param string $gererAdressesService
     */
    public function setGererAdressesService($gererAdressesService)
    {
        $this->gererAdressesService = $gererAdressesService;
    }

    /**
     * @return string
     */
    public function getGererMonService()
    {
        return $this->gererMonService;
    }

    /**
     * @param string $gererMonService
     */
    public function setGererMonService($gererMonService)
    {
        $this->gererMonService = $gererMonService;
    }

    /**
     * @return string
     */
    public function getDownloadArchives()
    {
        return $this->downloadArchives;
    }

    /**
     * @param string $downloadArchives
     */
    public function setDownloadArchives($downloadArchives)
    {
        $this->downloadArchives = $downloadArchives;
    }

    /**
     * @return string
     */
    public function getCreerAnnonceExtraitPv()
    {
        return $this->creerAnnonceExtraitPv;
    }

    /**
     * @param string $creerAnnonceExtraitPv
     */
    public function setCreerAnnonceExtraitPv($creerAnnonceExtraitPv)
    {
        $this->creerAnnonceExtraitPv = $creerAnnonceExtraitPv;
    }

    /**
     * @return string
     */
    public function getCreerAnnonceRapportAchevement()
    {
        return $this->creerAnnonceRapportAchevement;
    }

    /**
     * @param string $creerAnnonceRapportAchevement
     */
    public function setCreerAnnonceRapportAchevement($creerAnnonceRapportAchevement)
    {
        $this->creerAnnonceRapportAchevement = $creerAnnonceRapportAchevement;
    }

    /**
     * @return string
     */
    public function getGestionCertificatsAgent()
    {
        return $this->gestionCertificatsAgent;
    }

    /**
     * @param string $gestionCertificatsAgent
     */
    public function setGestionCertificatsAgent($gestionCertificatsAgent)
    {
        $this->gestionCertificatsAgent = $gestionCertificatsAgent;
    }

    /**
     * @return string
     */
    public function getCreerAvisProgrammePrevisionnel()
    {
        return $this->creerAvisProgrammePrevisionnel;
    }

    /**
     * @param string $creerAvisProgrammePrevisionnel
     */
    public function setCreerAvisProgrammePrevisionnel($creerAvisProgrammePrevisionnel)
    {
        $this->creerAvisProgrammePrevisionnel = $creerAvisProgrammePrevisionnel;
    }

    /**
     * @return string
     */
    public function getAnnulerConsultation()
    {
        return $this->annulerConsultation;
    }

    /**
     * @param string $annulerConsultation
     */
    public function setAnnulerConsultation($annulerConsultation)
    {
        $this->annulerConsultation = $annulerConsultation;
    }

    /**
     * @return string
     */
    public function getEnvoyerPublicite()
    {
        return $this->envoyerPublicite;
    }

    /**
     * @param string $envoyerPublicite
     */
    public function setEnvoyerPublicite($envoyerPublicite)
    {
        $this->envoyerPublicite = $envoyerPublicite;
    }

    /**
     * @return string
     */
    public function getListeMarchesNotifies()
    {
        return $this->listeMarchesNotifies;
    }

    /**
     * @param string $listeMarchesNotifies
     */
    public function setListeMarchesNotifies($listeMarchesNotifies)
    {
        $this->listeMarchesNotifies = $listeMarchesNotifies;
    }

    /**
     * @return string
     */
    public function getSuivreMessage()
    {
        return $this->suivreMessage;
    }

    /**
     * @param string $suivreMessage
     */
    public function setSuivreMessage($suivreMessage)
    {
        $this->suivreMessage = $suivreMessage;
    }

    /**
     * @return string
     */
    public function getEnvoyerMessage()
    {
        return $this->envoyerMessage;
    }

    /**
     * @param string $envoyerMessage
     */
    public function setEnvoyerMessage($envoyerMessage)
    {
        $this->envoyerMessage = $envoyerMessage;
    }

    /**
     * @return string
     */
    public function getSuiviFluxChorusTransversal()
    {
        return $this->suiviFluxChorusTransversal;
    }

    /**
     * @param string $suiviFluxChorusTransversal
     */
    public function setSuiviFluxChorusTransversal($suiviFluxChorusTransversal)
    {
        $this->suiviFluxChorusTransversal = $suiviFluxChorusTransversal;
    }

    /**
     * @return string
     */
    public function getGestionMandataire()
    {
        return $this->gestionMandataire;
    }

    /**
     * @param string $gestionMandataire
     */
    public function setGestionMandataire($gestionMandataire)
    {
        $this->gestionMandataire = $gestionMandataire;
    }

    /**
     * @return string
     */
    public function getGererNewsletter()
    {
        return $this->gererNewsletter;
    }

    /**
     * @param string $gererNewsletter
     */
    public function setGererNewsletter($gererNewsletter)
    {
        $this->gererNewsletter = $gererNewsletter;
    }

    /**
     * @return string
     */
    public function getGestionModelesFormulaire()
    {
        return $this->gestionModelesFormulaire;
    }

    /**
     * @param string $gestionModelesFormulaire
     */
    public function setGestionModelesFormulaire($gestionModelesFormulaire)
    {
        $this->gestionModelesFormulaire = $gestionModelesFormulaire;
    }

    /**
     * @return string
     */
    public function getGestionAdressesFacturationJal()
    {
        return $this->gestionAdressesFacturationJal;
    }

    /**
     * @param string $gestionAdressesFacturationJal
     */
    public function setGestionAdressesFacturationJal($gestionAdressesFacturationJal)
    {
        $this->gestionAdressesFacturationJal = $gestionAdressesFacturationJal;
    }

    /**
     * @return string
     */
    public function getAdministrerAdressesFacturationJal()
    {
        return $this->administrerAdressesFacturationJal;
    }

    /**
     * @param string $administrerAdressesFacturationJal
     */
    public function setAdministrerAdressesFacturationJal($administrerAdressesFacturationJal)
    {
        $this->administrerAdressesFacturationJal = $administrerAdressesFacturationJal;
    }

    /**
     * @return string
     */
    public function getRedactionDocumentsRedac()
    {
        return $this->redactionDocumentsRedac;
    }

    /**
     * @param string $redactionDocumentsRedac
     */
    public function setRedactionDocumentsRedac($redactionDocumentsRedac)
    {
        $this->redactionDocumentsRedac = $redactionDocumentsRedac;
    }

    /**
     * @return string
     */
    public function getValidationDocumentsRedac()
    {
        return $this->validationDocumentsRedac;
    }

    /**
     * @param string $validationDocumentsRedac
     */
    public function setValidationDocumentsRedac($validationDocumentsRedac)
    {
        $this->validationDocumentsRedac = $validationDocumentsRedac;
    }

    /**
     * @return string
     */
    public function getGestionMiseDispositionPiecesMarche()
    {
        return $this->gestionMiseDispositionPiecesMarche;
    }

    /**
     * @param string $gestionMiseDispositionPiecesMarche
     */
    public function setGestionMiseDispositionPiecesMarche($gestionMiseDispositionPiecesMarche)
    {
        $this->gestionMiseDispositionPiecesMarche = $gestionMiseDispositionPiecesMarche;
    }

    /**
     * @return string
     */
    public function getAnnuaireAcheteur()
    {
        return $this->annuaireAcheteur;
    }

    /**
     * @param string $annuaireAcheteur
     */
    public function setAnnuaireAcheteur($annuaireAcheteur)
    {
        $this->annuaireAcheteur = $annuaireAcheteur;
    }

    /**
     * @return string
     */
    public function getReprendreIntegralementArticle()
    {
        return $this->reprendreIntegralementArticle;
    }

    /**
     * @param string $reprendreIntegralementArticle
     */
    public function setReprendreIntegralementArticle($reprendreIntegralementArticle)
    {
        $this->reprendreIntegralementArticle = $reprendreIntegralementArticle;
    }

    /**
     * @return string
     */
    public function getAdministrerClauses()
    {
        return $this->administrerClauses;
    }

    /**
     * @param string $administrerClauses
     */
    public function setAdministrerClauses($administrerClauses)
    {
        $this->administrerClauses = $administrerClauses;
    }

    /**
     * @return string
     */
    public function getValiderClauses()
    {
        return $this->validerClauses;
    }

    /**
     * @param string $validerClauses
     */
    public function setValiderClauses($validerClauses)
    {
        $this->validerClauses = $validerClauses;
    }

    /**
     * @return string
     */
    public function getAdministrerCanevas()
    {
        return $this->administrerCanevas;
    }

    /**
     * @param string $administrerCanevas
     */
    public function setAdministrerCanevas($administrerCanevas)
    {
        $this->administrerCanevas = $administrerCanevas;
    }

    /**
     * @return string
     */
    public function getValiderCanevas()
    {
        return $this->validerCanevas;
    }

    /**
     * @param string $validerCanevas
     */
    public function setValiderCanevas($validerCanevas)
    {
        $this->validerCanevas = $validerCanevas;
    }

    /**
     * @return string
     */
    public function getAdministrerClausesEntiteAchats()
    {
        return $this->administrerClausesEntiteAchats;
    }

    /**
     * @param string $administrerClausesEntiteAchats
     */
    public function setAdministrerClausesEntiteAchats($administrerClausesEntiteAchats)
    {
        $this->administrerClausesEntiteAchats = $administrerClausesEntiteAchats;
    }

    /**
     * @return string
     */
    public function getGenererPiecesFormatOdt()
    {
        return $this->genererPiecesFormatOdt;
    }

    /**
     * @param string $genererPiecesFormatOdt
     */
    public function setGenererPiecesFormatOdt($genererPiecesFormatOdt)
    {
        $this->genererPiecesFormatOdt = $genererPiecesFormatOdt;
    }

    /**
     * @return string
     */
    public function getPublierVersionClausierEditeur()
    {
        return $this->publierVersionClausierEditeur;
    }

    /**
     * @param string $publierVersionClausierEditeur
     */
    public function setPublierVersionClausierEditeur($publierVersionClausierEditeur)
    {
        $this->publierVersionClausierEditeur = $publierVersionClausierEditeur;
    }

    /**
     * @return string
     */
    public function getAdministrerClausesEditeur()
    {
        return $this->administrerClausesEditeur;
    }

    /**
     * @param string $administrerClausesEditeur
     */
    public function setAdministrerClausesEditeur($administrerClausesEditeur)
    {
        $this->administrerClausesEditeur = $administrerClausesEditeur;
    }

    /**
     * @return string
     */
    public function getValiderClausesEditeur()
    {
        return $this->validerClausesEditeur;
    }

    /**
     * @param string $validerClausesEditeur
     */
    public function setValiderClausesEditeur($validerClausesEditeur)
    {
        $this->validerClausesEditeur = $validerClausesEditeur;
    }

    /**
     * @return string
     */
    public function getAdministrerCanevasEditeur()
    {
        return $this->administrerCanevasEditeur;
    }

    /**
     * @param string $administrerCanevasEditeur
     */
    public function setAdministrerCanevasEditeur($administrerCanevasEditeur)
    {
        $this->administrerCanevasEditeur = $administrerCanevasEditeur;
    }

    /**
     * @return string
     */
    public function getValiderCanevasEditeur()
    {
        return $this->validerCanevasEditeur;
    }

    /**
     * @param string $validerCanevasEditeur
     */
    public function setValiderCanevasEditeur($validerCanevasEditeur)
    {
        $this->validerCanevasEditeur = $validerCanevasEditeur;
    }

    /**
     * @return string
     */
    public function getDecisionSuiviSeul()
    {
        return $this->decisionSuiviSeul;
    }

    /**
     * @param string $decisionSuiviSeul
     */
    public function setDecisionSuiviSeul($decisionSuiviSeul)
    {
        $this->decisionSuiviSeul = $decisionSuiviSeul;
    }

    /**
     * @return string
     */
    public function getOuvrirCandidatureHorsLigne()
    {
        return $this->ouvrirCandidatureHorsLigne;
    }

    /**
     * @param string $ouvrirCandidatureHorsLigne
     */
    public function setOuvrirCandidatureHorsLigne($ouvrirCandidatureHorsLigne)
    {
        $this->ouvrirCandidatureHorsLigne = $ouvrirCandidatureHorsLigne;
    }

    /**
     * @return string
     */
    public function getOuvrirOffreHorsLigne()
    {
        return $this->ouvrirOffreHorsLigne;
    }

    /**
     * @param string $ouvrirOffreHorsLigne
     */
    public function setOuvrirOffreHorsLigne($ouvrirOffreHorsLigne)
    {
        $this->ouvrirOffreHorsLigne = $ouvrirOffreHorsLigne;
    }

    /**
     * @return string
     */
    public function getOuvrirOffreTechniqueHorsLigne()
    {
        return $this->ouvrirOffreTechniqueHorsLigne;
    }

    /**
     * @param string $ouvrirOffreTechniqueHorsLigne
     */
    public function setOuvrirOffreTechniqueHorsLigne($ouvrirOffreTechniqueHorsLigne)
    {
        $this->ouvrirOffreTechniqueHorsLigne = $ouvrirOffreTechniqueHorsLigne;
    }

    /**
     * @return string
     */
    public function getOuvrirAnonymatHorsLigne()
    {
        return $this->ouvrirAnonymatHorsLigne;
    }

    /**
     * @param string $ouvrirAnonymatHorsLigne
     */
    public function setOuvrirAnonymatHorsLigne($ouvrirAnonymatHorsLigne)
    {
        $this->ouvrirAnonymatHorsLigne = $ouvrirAnonymatHorsLigne;
    }

    /**
     * @return string
     */
    public function getEspaceCollaboratifGestionnaire()
    {
        return $this->espaceCollaboratifGestionnaire;
    }

    /**
     * @param string $espaceCollaboratifGestionnaire
     */
    public function setEspaceCollaboratifGestionnaire($espaceCollaboratifGestionnaire)
    {
        $this->espaceCollaboratifGestionnaire = $espaceCollaboratifGestionnaire;
    }

    /**
     * @return string
     */
    public function getEspaceCollaboratifContributeur()
    {
        return $this->espaceCollaboratifContributeur;
    }

    /**
     * @param string $espaceCollaboratifContributeur
     */
    public function setEspaceCollaboratifContributeur($espaceCollaboratifContributeur)
    {
        $this->espaceCollaboratifContributeur = $espaceCollaboratifContributeur;
    }

    /**
     * @return string
     */
    public function getGererOrganismes()
    {
        return $this->gererOrganismes;
    }

    /**
     * @param string $gererOrganismes
     */
    public function setGererOrganismes($gererOrganismes)
    {
        $this->gererOrganismes = $gererOrganismes;
    }

    /**
     * @return string
     */
    public function getGererAssociationsAgents()
    {
        return $this->gererAssociationsAgents;
    }

    /**
     * @param string $gererAssociationsAgents
     */
    public function setGererAssociationsAgents($gererAssociationsAgents)
    {
        $this->gererAssociationsAgents = $gererAssociationsAgents;
    }

    /**
     * @return string
     */
    public function getModuleRedactionUniquement()
    {
        return $this->moduleRedactionUniquement;
    }

    /**
     * @param string $moduleRedactionUniquement
     */
    public function setModuleRedactionUniquement($moduleRedactionUniquement)
    {
        $this->moduleRedactionUniquement = $moduleRedactionUniquement;
    }

    /**
     * @return string
     */
    public function getHistoriqueNavigationInscrits()
    {
        return $this->historiqueNavigationInscrits;
    }

    /**
     * @param string $historiqueNavigationInscrits
     */
    public function setHistoriqueNavigationInscrits($historiqueNavigationInscrits)
    {
        $this->historiqueNavigationInscrits = $historiqueNavigationInscrits;
    }

    /**
     * @return string
     */
    public function getTelechargerAccordsCadres()
    {
        return $this->telechargerAccordsCadres;
    }

    /**
     * @param string $telechargerAccordsCadres
     */
    public function setTelechargerAccordsCadres($telechargerAccordsCadres)
    {
        $this->telechargerAccordsCadres = $telechargerAccordsCadres;
    }

    /**
     * @return string
     */
    public function getCreerAnnonceDecisionResiliation()
    {
        return $this->creerAnnonceDecisionResiliation;
    }

    /**
     * @param string $creerAnnonceDecisionResiliation
     */
    public function setCreerAnnonceDecisionResiliation($creerAnnonceDecisionResiliation)
    {
        $this->creerAnnonceDecisionResiliation = $creerAnnonceDecisionResiliation;
    }

    /**
     * @return string
     */
    public function getCreerAnnonceSyntheseRapportAudit()
    {
        return $this->creerAnnonceSyntheseRapportAudit;
    }

    /**
     * @param string $creerAnnonceSyntheseRapportAudit
     */
    public function setCreerAnnonceSyntheseRapportAudit($creerAnnonceSyntheseRapportAudit)
    {
        $this->creerAnnonceSyntheseRapportAudit = $creerAnnonceSyntheseRapportAudit;
    }

    /**
     * @return string
     */
    public function getGererOperations()
    {
        return $this->gererOperations;
    }

    /**
     * @param string $gererOperations
     */
    public function setGererOperations($gererOperations)
    {
        $this->gererOperations = $gererOperations;
    }

    /**
     * @return string
     */
    public function getTelechargerSiretAcheteur()
    {
        return $this->telechargerSiretAcheteur;
    }

    /**
     * @param string $telechargerSiretAcheteur
     */
    public function setTelechargerSiretAcheteur($telechargerSiretAcheteur)
    {
        $this->telechargerSiretAcheteur = $telechargerSiretAcheteur;
    }

    /**
     * @return string
     */
    public function getGererReouverturesModification()
    {
        return $this->gererReouverturesModification;
    }

    /**
     * @param string $gererReouverturesModification
     */
    public function setGererReouverturesModification($gererReouverturesModification)
    {
        $this->gererReouverturesModification = $gererReouverturesModification;
    }

    /**
     * @return string
     */
    public function getAccederTousTelechargements()
    {
        return $this->accederTousTelechargements;
    }

    /**
     * @param string $accederTousTelechargements
     */
    public function setAccederTousTelechargements($accederTousTelechargements)
    {
        $this->accederTousTelechargements = $accederTousTelechargements;
    }

    /**
     * @return string
     */
    public function getCreerContrat()
    {
        return $this->creerContrat;
    }

    /**
     * @param string $creerContrat
     */
    public function setCreerContrat($creerContrat)
    {
        $this->creerContrat = $creerContrat;
    }

    /**
     * @return string
     */
    public function getModifierContrat()
    {
        return $this->modifierContrat;
    }

    /**
     * @param string $modifierContrat
     */
    public function setModifierContrat($modifierContrat)
    {
        $this->modifierContrat = $modifierContrat;
    }

    /**
     * @return string
     */
    public function getConsulterContrat()
    {
        return $this->consulterContrat;
    }

    /**
     * @param string $consulterContrat
     */
    public function setConsulterContrat($consulterContrat)
    {
        $this->consulterContrat = $consulterContrat;
    }

    /**
     * @return string
     */
    public function getGererNewsletterRedac()
    {
        return $this->gererNewsletterRedac;
    }

    /**
     * @param string $gererNewsletterRedac
     */
    public function setGererNewsletterRedac($gererNewsletterRedac)
    {
        $this->gererNewsletterRedac = $gererNewsletterRedac;
    }

    /**
     * @return string
     */
    public function getProfilRma()
    {
        return $this->profilRma;
    }

    /**
     * @param string $profilRma
     */
    public function setProfilRma($profilRma)
    {
        $this->profilRma = $profilRma;
    }

    /**
     * @return string
     */
    public function getAffectationVisionRma()
    {
        return $this->affectationVisionRma;
    }

    /**
     * @param string $affectationVisionRma
     */
    public function setAffectationVisionRma($affectationVisionRma)
    {
        $this->affectationVisionRma = $affectationVisionRma;
    }

    /**
     * @return string
     */
    public function getGererGabaritEditeur()
    {
        return $this->gererGabaritEditeur;
    }

    /**
     * @param string $gererGabaritEditeur
     */
    public function setGererGabaritEditeur($gererGabaritEditeur)
    {
        $this->gererGabaritEditeur = $gererGabaritEditeur;
    }

    /**
     * @return string
     */
    public function getGererGabarit()
    {
        return $this->gererGabarit;
    }

    /**
     * @param string $gererGabarit
     */
    public function setGererGabarit($gererGabarit)
    {
        $this->gererGabarit = $gererGabarit;
    }

    /**
     * @return string
     */
    public function getGererGabaritEntiteAchats()
    {
        return $this->gererGabaritEntiteAchats;
    }

    /**
     * @param string $gererGabaritEntiteAchats
     */
    public function setGererGabaritEntiteAchats($gererGabaritEntiteAchats)
    {
        $this->gererGabaritEntiteAchats = $gererGabaritEntiteAchats;
    }

    /**
     * @return string
     */
    public function getGererGabaritAgent()
    {
        return $this->gererGabaritAgent;
    }

    /**
     * @param string $gererGabaritAgent
     */
    public function setGererGabaritAgent($gererGabaritAgent)
    {
        $this->gererGabaritAgent = $gererGabaritAgent;
    }

    /**
     * @return string
     */
    public function getGererMessagesAccueil()
    {
        return $this->gererMessagesAccueil;
    }

    /**
     * @param string $gererMessagesAccueil
     */
    public function setGererMessagesAccueil($gererMessagesAccueil)
    {
        $this->gererMessagesAccueil = $gererMessagesAccueil;
    }

    /**
     * @return string
     */
    public function getGererOaGa()
    {
        return $this->gererOaGa;
    }

    /**
     * @param string $gererOaGa
     */
    public function setGererOaGa($gererOaGa)
    {
        $this->gererOaGa = $gererOaGa;
    }

    /**
     * @return string
     */
    public function getDeplacerService()
    {
        return $this->deplacerService;
    }

    /**
     * @param string $deplacerService
     */
    public function setDeplacerService($deplacerService)
    {
        $this->deplacerService = $deplacerService;
    }

    /**
     * @return string
     */
    public function getActiverVersionClausier()
    {
        return $this->activerVersionClausier;
    }

    /**
     * @param string $activerVersionClausier
     */
    public function setActiverVersionClausier($activerVersionClausier)
    {
        $this->activerVersionClausier = $activerVersionClausier;
    }

    /**
     * @return ?Agent
     */
    public function getAgent(): ?Agent
    {
        return $this->agent;
    }

    public function setAgent(Agent $agent)
    {
        $this->agent = $agent;
    }

    public function getInvitePermanentMonEntite(): string
    {
        return $this->invitePermanentMonEntite;
    }

    /**
     * @param $invitePermanentMonEntite
     */
    public function setInvitePermanentMonEntite($invitePermanentMonEntite)
    {
        $this->invitePermanentMonEntite = $invitePermanentMonEntite;
    }

    /**
     * @return invite_permanent_entite_dependante
     */
    public function getInvitePermanentEntiteDependante()
    {
        return $this->invitePermanentEntiteDependante;
    }

    /**
     * @return string
     */
    public function getExecVoirContratsEa()
    {
        return $this->execVoirContratsEa;
    }

    /**
     * @param string $execVoirContratsEa
     */
    public function setExecVoirContratsEa($execVoirContratsEa)
    {
        $this->execVoirContratsEa = $execVoirContratsEa;

        return $this;
    }

    /**
     * @return string
     */
    public function getExecVoirContratsEaDependantes()
    {
        return $this->execVoirContratsEaDependantes;
    }

    /**
     * @param string $execVoirContratsEaDependantes
     */
    public function setExecVoirContratsEaDependantes($execVoirContratsEaDependantes)
    {
        $this->execVoirContratsEaDependantes = $execVoirContratsEaDependantes;

        return $this;
    }

    /**
     * @return string
     */
    public function getExecVoirContratsOrganisme()
    {
        return $this->execVoirContratsOrganisme;
    }

    /**
     * @param string $execVoirContratsOrganisme
     */
    public function setExecVoirContratsOrganisme($execVoirContratsOrganisme)
    {
        $this->execVoirContratsOrganisme = $execVoirContratsOrganisme;

        return $this;
    }

    /**
     * Cette fonction permet de cloner les habilitations d'un agent et de les attribuer à l'agent dont l'identifiant
     * est passé en paramètre.
     *
     * @param int $idAgent identifiant de l'agent auquel va être rattaché le clone
     *
     * @return bool|HabilitationAgent false si le clone n'a pas réussi, sinon le nouvel objet HabilitationAgent créé
     *
     * @throws Exception
     */
    public function copier(int $idAgent): bool|\App\Entity\HabilitationAgent
    {
        $agentDestinationObject = $this->em->getRepository(Agent::class)->findOneBy(['id' => $idAgent]);

        if ($agentDestinationObject instanceof Agent) {
            $nouveauHabilitationAgent = clone $this;
            $nouveauHabilitationAgent->setAgent($agentDestinationObject);
            $this->em->persist($nouveauHabilitationAgent);
            $this->em->flush();

            $agentDestinationObject = null;
            unset($agentDestinationObject);

            return $nouveauHabilitationAgent;
        }

        return false;
    }

    public function isAccesWs(): bool
    {
        return $this->accesWs;
    }

    /**
     * @return $this
     */
    public function setAccesWs(bool $accesWs)
    {
        $this->accesWs = $accesWs;

        return $this;
    }

    /**
     * @param  $invitePermanentEntiteDependante
     */
    public function setInvitePermanentEntiteDependante($invitePermanentEntiteDependante)
    {
        $this->invitePermanentEntiteDependante = $invitePermanentEntiteDependante;
    }

    /**
     * @return invite_permanent_transverse
     */
    public function getInvitePermanentTransverse()
    {
        return $this->invitePermanentTransverse;
    }

    /**
     * @param  $invitePermanentTransverse
     */
    public function setInvitePermanentTransverse($invitePermanentTransverse)
    {
        $this->invitePermanentTransverse = $invitePermanentTransverse;
    }

    /**
     * @return bool
     */
    public function isEspaceDocumentaireConsultation()
    {
        return $this->espaceDocumentaireConsultation;
    }

    /**
     * @return $this
     */
    public function getEspaceDocumentaireConsultation()
    {
        return $this->espaceDocumentaireConsultation;
    }

    public function setEspaceDocumentaireConsultation(bool $espaceDocumentaireConsultation)
    {
        $this->espaceDocumentaireConsultation = $espaceDocumentaireConsultation;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAccesEchangeDocumentaire()
    {
        return $this->accesEchangeDocumentaire;
    }

    /**
     * @param bool $accesEchangeDocumentaire
     *
     * @return HabilitationAgent
     */
    public function setAccesEchangeDocumentaire($accesEchangeDocumentaire)
    {
        $this->accesEchangeDocumentaire = $accesEchangeDocumentaire;

        return $this;
    }

    public function getAdministrerOrganisme(): int
    {
        return $this->administrerOrganisme;
    }

    public function setAdministrerOrganisme(int $administrerOrganisme): HabilitationAgent
    {
        $this->administrerOrganisme = $administrerOrganisme;

        return $this;
    }

    /**
     * @return bool
     */
    public function isExecModificationContrat()
    {
        return $this->execModificationContrat;
    }

    public function setExecModificationContrat(string $execModificationContrat): HabilitationAgent
    {
        $this->execModificationContrat = $execModificationContrat;

        return $this;
    }

    public function getRecensementProgrammationStrategieAchat(): RecensementProgrammationStrategieAchat
    {
        return $this->recensementProgrammationStrategieAchat;
    }

    public function setRecensementProgrammationStrategieAchat(
        RecensementProgrammationStrategieAchat $recensementProgrammationStrategieAchat
    ): HabilitationAgent {
        $this->recensementProgrammationStrategieAchat = $recensementProgrammationStrategieAchat;

        return $this;
    }

    public function isGestionEnvol(): bool
    {
        return $this->gestionEnvol;
    }

    public function getGestionEnvol(): bool
    {
        return $this->gestionEnvol;
    }

    public function setGestionEnvol(bool $gestionEnvol): self
    {
        $this->gestionEnvol = $gestionEnvol;

        return $this;
    }

    /**
     * @return bool
     */
    public function isModuleAutoformation(): bool
    {
        return $this->moduleAutoformation;
    }

    /**
     * @param bool $moduleAutoformation
     * @return HabilitationAgent
     */
    public function setModuleAutoformation(bool $moduleAutoformation): self
    {
        $this->moduleAutoformation = $moduleAutoformation;

        return $this;
    }

    /**
     * @return bool
     */
    public function getModuleAutoformation(): bool
    {
        return $this->moduleAutoformation;
    }

    public function isGestionSpaserConsultations(): bool
    {
        return $this->gestionSpaserConsultations;
    }

    public function setGestionSpaserConsultations(bool $gestionSpaserConsultations): self
    {
        $this->gestionSpaserConsultations = $gestionSpaserConsultations;

        return $this;
    }

    public function isGestionValidationEco(): bool
    {
        return $this->gestionValidationEco;
    }

    public function setGestionValidationEco(bool $gestionValidationEco): self
    {
        $this->gestionValidationEco = $gestionValidationEco;

        return $this;
    }

    public function isGestionValidationSip(): bool
    {
        return $this->gestionValidationSip;
    }

    public function setGestionValidationSip(bool $gestionValidationSip): self
    {
        $this->gestionValidationSip = $gestionValidationSip;

        return $this;
    }

    public function isRattachementService(): bool
    {
        return $this->rattachementService;
    }

    public function setRattachementService(bool $rattachementService): self
    {
        $this->rattachementService = $rattachementService;

        return $this;
    }

    public function isDuplicationConsultations(): bool
    {
        return $this->duplicationConsultations;
    }

    public function setDuplicationConsultations(bool $duplicationConsultations): self
    {
        $this->duplicationConsultations = $duplicationConsultations;

        return $this;
    }

    public function isProjetAchatLancementSourcing(): bool
    {
        return $this->projetAchatLancementSourcing;
    }

    public function setProjetAchatLancementSourcing(bool $projetAchatLancementSourcing): self
    {
        $this->projetAchatLancementSourcing = $projetAchatLancementSourcing;

        return $this;
    }

    public function isProjetAchatInvalidation(): bool
    {
        return $this->projetAchatInvalidation;
    }

    public function setProjetAchatInvalidation(bool $projetAchatInvalidation): self
    {
        $this->projetAchatInvalidation = $projetAchatInvalidation;

        return $this;
    }

    public function isProjetAchatAnnulation(): bool
    {
        return $this->projetAchatAnnulation;
    }

    public function setProjetAchatAnnulation(bool $projetAchatAnnulation): self
    {
        $this->projetAchatAnnulation = $projetAchatAnnulation;

        return $this;
    }

    public function isLancementProcedure(): bool
    {
        return $this->lancementProcedure;
    }

    public function setLancementProcedure(bool $lancementProcedure): self
    {
        $this->lancementProcedure = $lancementProcedure;

        return $this;
    }

    public function isRecensementInvaliderProjetAchat(): bool
    {
        return $this->recensementInvaliderProjetAchat;
    }

    public function setRecensementInvaliderProjetAchat(bool $recensementInvaliderProjetAchat): self
    {
        $this->recensementInvaliderProjetAchat = $recensementInvaliderProjetAchat;

        return $this;
    }

    public function isRecensementAnnulerProjetAchat(): bool
    {
        return $this->recensementAnnulerProjetAchat;
    }

    public function setRecensementAnnulerProjetAchat(bool $recensementAnnulerProjetAchat): self
    {
        $this->recensementAnnulerProjetAchat = $recensementAnnulerProjetAchat;

        return $this;
    }

    public function isAdministrationDocumentsModeles(): bool
    {
        return $this->administrationDocumentsModeles;
    }

    public function setAdministrationDocumentsModeles(bool $administrationDocumentsModeles): self
    {
        $this->administrationDocumentsModeles = $administrationDocumentsModeles;

        return $this;
    }

    public function getSupprimerContrat(): ?bool
    {
        return $this->supprimerContrat;
    }

    public function setSupprimerContrat(bool $supprimerContrat): self
    {
        $this->supprimerContrat = $supprimerContrat;

        return $this;
    }
}
