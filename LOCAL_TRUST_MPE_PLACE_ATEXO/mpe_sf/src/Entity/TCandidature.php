<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;

/**
 * TCandidature.
 *
 * @ORM\Table(name="t_candidature")
 * @ORM\Entity(repositoryClass="App\Repository\TCandidatureRepository")
 */
class TCandidature
{
    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation", inversedBy="candidatures", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="consultation_id", referencedColumnName="id")
     */
    private ?Consultation $consultation = null;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme", inversedBy="candidatures")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private ?Organisme $organisme = null;

    /**
     * @ORM\Column(name="id_inscrit", type="integer", nullable=false)
     */
    private ?int $idInscrit = null;

    /**
     * @ORM\Column(name="id_entreprise", type="integer", nullable=false)
     */
    private ?int $idEntreprise = null;

    /**
     * @ORM\Column(name="id_etablissement", type="integer", nullable=false)
     */
    private ?int $idEtablissement = null;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private int $status = 99;

    /**
     * @ORM\Column(name="type_candidature", type="string", length=45)
     */
    private ?string $typeCandidature = null;

    /**
     * @ORM\Column(name="type_candidature_dume", type="string", length=45)
     */
    private ?string $typeCandidatureDume = null;

    /**
     *
     * @ORM\OneToOne(targetEntity="App\Entity\TDumeContexte", inversedBy="idCandidature")
     * @ORM\JoinColumn(name="id_dume_contexte", referencedColumnName="id")
     */
    private ?TDumeContexte $idDumeContexte = null;

    /**
     * @var Offre
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Offre")
     * @ORM\JoinColumn(name="id_offre", referencedColumnName="id")
     */
    private $idOffre;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\TListeLotsCandidature", mappedBy="candidature", cascade={"persist"})
     * @ORM\JoinColumn(name="id", referencedColumnName="id_candidature")
     */
    private Collection $lots;

    /**
     * @ORM\Column(name="role_inscrit", type="integer", nullable=false, options={"default" : "1"})
     */
    private int $roleInscrit = 1;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_derniere_validation_dume", type="datetime", nullable=true)*/
    private $dateDerniereValidationDume;

    public function __construct()
    {
        $this->lots = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Consultation
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    /**
     * @param Consultation $consultation
     */
    public function setConsultation($consultation)
    {
        $this->consultation = $consultation;
    }

    /**
     * @return Organisme
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param Organisme $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return int
     */
    public function getIdInscrit()
    {
        return $this->idInscrit;
    }

    /**
     * @param int $idInscrit
     */
    public function setIdInscrit($idInscrit)
    {
        $this->idInscrit = $idInscrit;
    }

    /**
     * @return int
     */
    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    /**
     * @param int $idEntreprise
     */
    public function setIdEntreprise($idEntreprise)
    {
        $this->idEntreprise = $idEntreprise;
    }

    /**
     * @return int
     */
    public function getIdEtablissement()
    {
        return $this->idEtablissement;
    }

    /**
     * @param int $idEtablissement
     */
    public function setIdEtablissement($idEtablissement)
    {
        $this->idEtablissement = $idEtablissement;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getTypeCandidature()
    {
        return $this->typeCandidature;
    }

    /**
     * @param string $typeCandidature
     */
    public function setTypeCandidature($typeCandidature)
    {
        $this->typeCandidature = $typeCandidature;
    }

    /**
     * @return string
     */
    public function getTypeCandidatureDume()
    {
        return $this->typeCandidatureDume;
    }

    /**
     * @param string $typeCandidatureDume
     */
    public function setTypeCandidatureDume($typeCandidatureDume)
    {
        $this->typeCandidatureDume = $typeCandidatureDume;
    }

    /**
     * @return TDumeContexte
     */
    public function getIdDumeContexte()
    {
        return $this->idDumeContexte;
    }

    /**
     * @param TDumeContexte $idDumeContexte
     */
    public function setIdDumeContexte($idDumeContexte)
    {
        $this->idDumeContexte = $idDumeContexte;
    }

    /**
     * @return mixed
     */
    public function getIdOffre()
    {
        return $this->idOffre;
    }

    /**
     * @param mixed $idOffre
     */
    public function setIdOffre($idOffre)
    {
        $this->idOffre = $idOffre;
    }

    /**
     * @return $this
     */
    public function addLots(TListeLotsCandidature $lot)
    {
        $this->lots[] = $lot;
        $lot->setCandidature($this);

        return $this;
    }

    public function removeLots(TListeLotsCandidature $lot)
    {
        $this->lots->removeElement($lot);
    }

    /**
     * @return ArrayCollection
     */
    public function getLots()
    {
        return $this->lots;
    }

    /**
     * @return int
     */
    public function getRoleInscrit()
    {
        return $this->roleInscrit;
    }

    /**
     * @param int $roleInscrit
     */
    public function setRoleInscrit($roleInscrit)
    {
        $this->roleInscrit = $roleInscrit;
    }

    /**
     * @return DateTime
     */
    public function getDateDerniereValidationDume()
    {
        return $this->dateDerniereValidationDume;
    }

    /**
     * @param DateTime $dateDerniereValidationDume
     */
    public function setDateDerniereValidationDume($dateDerniereValidationDume)
    {
        $this->dateDerniereValidationDume = $dateDerniereValidationDume;
    }
}
