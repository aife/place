<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Typeavis
 *
 * @ORM\Table(name="TypeAvis")
 * @ORM\Entity
 */
class Typeavis
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule_avis", type="string", length=100, nullable=false)
     */
    private $intituleAvis = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="intitule_avis_fr", type="string", length=100, nullable=true)
     */
    private $intituleAvisFr;

    /**
     * @var string|null
     *
     * @ORM\Column(name="intitule_avis_en", type="string", length=100, nullable=true)
     */
    private $intituleAvisEn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="intitule_avis_es", type="string", length=100, nullable=true)
     */
    private $intituleAvisEs;

    /**
     * @var string|null
     *
     * @ORM\Column(name="intitule_avis_su", type="string", length=100, nullable=true)
     */
    private $intituleAvisSu;

    /**
     * @var string|null
     *
     * @ORM\Column(name="intitule_avis_du", type="string", length=100, nullable=true)
     */
    private $intituleAvisDu;

    /**
     * @var string|null
     *
     * @ORM\Column(name="intitule_avis_cz", type="string", length=100, nullable=true)
     */
    private $intituleAvisCz;

    /**
     * @var string|null
     *
     * @ORM\Column(name="intitule_avis_ar", type="string", length=100, nullable=true)
     */
    private $intituleAvisAr;

    /**
     * @var string
     *
     * @ORM\Column(name="abbreviation", type="string", length=50, nullable=false)
     */
    private $abbreviation = '';

    /**
     * @var int
     *
     * @ORM\Column(name="id_type_avis_ANM", type="integer", nullable=false)
     */
    private $idTypeAvisAnm = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="intitule_avis_it", type="string", length=100, nullable=true)
     */
    private $intituleAvisIt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntituleAvis(): ?string
    {
        return $this->intituleAvis;
    }

    public function setIntituleAvis(string $intituleAvis): self
    {
        $this->intituleAvis = $intituleAvis;

        return $this;
    }

    public function getIntituleAvisFr(): ?string
    {
        return $this->intituleAvisFr;
    }

    public function setIntituleAvisFr(?string $intituleAvisFr): self
    {
        $this->intituleAvisFr = $intituleAvisFr;

        return $this;
    }

    public function getIntituleAvisEn(): ?string
    {
        return $this->intituleAvisEn;
    }

    public function setIntituleAvisEn(?string $intituleAvisEn): self
    {
        $this->intituleAvisEn = $intituleAvisEn;

        return $this;
    }

    public function getIntituleAvisEs(): ?string
    {
        return $this->intituleAvisEs;
    }

    public function setIntituleAvisEs(?string $intituleAvisEs): self
    {
        $this->intituleAvisEs = $intituleAvisEs;

        return $this;
    }

    public function getIntituleAvisSu(): ?string
    {
        return $this->intituleAvisSu;
    }

    public function setIntituleAvisSu(?string $intituleAvisSu): self
    {
        $this->intituleAvisSu = $intituleAvisSu;

        return $this;
    }

    public function getIntituleAvisDu(): ?string
    {
        return $this->intituleAvisDu;
    }

    public function setIntituleAvisDu(?string $intituleAvisDu): self
    {
        $this->intituleAvisDu = $intituleAvisDu;

        return $this;
    }

    public function getIntituleAvisCz(): ?string
    {
        return $this->intituleAvisCz;
    }

    public function setIntituleAvisCz(?string $intituleAvisCz): self
    {
        $this->intituleAvisCz = $intituleAvisCz;

        return $this;
    }

    public function getIntituleAvisAr(): ?string
    {
        return $this->intituleAvisAr;
    }

    public function setIntituleAvisAr(?string $intituleAvisAr): self
    {
        $this->intituleAvisAr = $intituleAvisAr;

        return $this;
    }

    public function getAbbreviation(): ?string
    {
        return $this->abbreviation;
    }

    public function setAbbreviation(string $abbreviation): self
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    public function getIdTypeAvisAnm(): ?int
    {
        return $this->idTypeAvisAnm;
    }

    public function setIdTypeAvisAnm(int $idTypeAvisAnm): self
    {
        $this->idTypeAvisAnm = $idTypeAvisAnm;

        return $this;
    }

    public function getIntituleAvisIt(): ?string
    {
        return $this->intituleAvisIt;
    }

    public function setIntituleAvisIt(?string $intituleAvisIt): self
    {
        $this->intituleAvisIt = $intituleAvisIt;

        return $this;
    }
}
