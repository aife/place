<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConsLotContrat.
 *
 * @ORM\Table(name="t_cons_lot_contrat")
 * @ORM\Entity(repositoryClass="App\Repository\ConsLotContratRepository")
 */
class ConsLotContrat
{
    /**
     *
     * @ORM\Column(name="id_cons_lot_contrat", type="integer", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $idConsLotContrat = null;

    /**
     * @ORM\Column(name="organisme", type="string", length=45, nullable=true)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(name="consultation_id", type="integer", nullable=true)
     */
    private ?int $consultationId = null;

    /**
     * @ORM\Column(name="lot", type="integer", nullable=true)
     */
    private ?int $lot = null;

    /**
     * @ORM\Column(name="id_contrat_titulaire", type="integer", nullable=false)
     */
    private ?int $idContratTitulaire = null;

    /**
     * @return int
     */
    public function getIdConsLotContrat()
    {
        return $this->idConsLotContrat;
    }

    public function setIdConsLotContrat(int $idConsLotContrat)
    {
        $this->idConsLotContrat = $idConsLotContrat;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return int
     */
    public function getConsultationId()
    {
        return $this->consultationId;
    }

    /**
     * @param int $consultationId
     */
    public function setConsultationId($consultationId)
    {
        $this->consultationId = $consultationId;
    }

    /**
     * @return int
     */
    public function getLot()
    {
        return $this->lot;
    }

    public function setLot(int $lot)
    {
        $this->lot = $lot;
    }

    /**
     * @return int
     */
    public function getIdContratTitulaire()
    {
        return $this->idContratTitulaire;
    }

    public function setIdContratTitulaire(int $idContratTitulaire)
    {
        $this->idContratTitulaire = $idContratTitulaire;
    }
}
