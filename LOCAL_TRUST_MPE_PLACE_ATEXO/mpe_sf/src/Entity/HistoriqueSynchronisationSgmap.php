<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Test.
 *
 * @ORM\Table(name="t_historique_synchronisation_SGMAP")
 * @ORM\Entity(repositoryClass="App\Repository\HistoriqueSynchronisationSgmapRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class HistoriqueSynchronisationSgmap
{
    /**
     *
     * @ORM\Column(name="id_historique", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $idHistorique = null;

    /**
     * @ORM\Column(name="id_objet", type="integer", nullable=true)
     */
    private ?int $idObjet = null;

    /**
     * @ORM\Column(name="type_objet", type="string", length=255, nullable=true)
     */
    private ?string $typeObjet = null;

    /**
     * @ORM\Column(name="code", type="string", length=14, nullable=true)
     */
    private ?string $code = null;

    /**
     * @ORM\Column(name="jeton", type="text", length=65535, nullable=true)
     */
    private ?string $jeton = null;

    /**
     * @ORM\Column(name="date_creation", type="string", length=20, nullable=true)
     */
    private ?string $dateCreation = null;

    /**
     * @return int
     */
    public function getIdHistorique()
    {
        return $this->idHistorique;
    }

    /**
     * @param int $idHistorique
     */
    public function setIdHistorique($idHistorique)
    {
        $this->idHistorique = $idHistorique;
    }

    /**
     * @return int
     */
    public function getIdObjet()
    {
        return $this->idObjet;
    }

    /**
     * @param int $idObjet
     */
    public function setIdObjet($idObjet)
    {
        $this->idObjet = $idObjet;
    }

    /**
     * @return string
     */
    public function getTypeObjet()
    {
        return $this->typeObjet;
    }

    /**
     * @param string $typeObjet
     */
    public function setTypeObjet($typeObjet)
    {
        $this->typeObjet = $typeObjet;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getJeton()
    {
        return $this->jeton;
    }

    /**
     * @param string $jeton
     */
    public function setJeton($jeton)
    {
        $this->jeton = $jeton;
    }

    /**
     * @return string
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param string $dateCreation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }

    /**
     * @ORM\PrePersist
     */
    public function createDate()
    {
        $this->dateCreation = date('Y-m-d H:i:s');
    }
}
