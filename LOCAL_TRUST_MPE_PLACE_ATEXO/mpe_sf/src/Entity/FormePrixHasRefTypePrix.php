<?php

namespace App\Entity;

use App\Repository\FormePrixHasRefTypePrixRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="t_forme_prix_has_ref_type_prix" )
 * @ORM\Entity(repositoryClass=FormePrixHasRefTypePrixRepository::class)
 */
class FormePrixHasRefTypePrix
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_forme_prix", type="integer")
     */
    private ?int $idFormePrix;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_type_prix", type="integer", nullable=false)
     */
    private ?int $idTypePrix;

    public function getIdFormePrix(): ?int
    {
        return $this->idFormePrix;
    }

    public function setIdFormePrix(?int $idFormePrix): self
    {
        $this->idFormePrix = $idFormePrix;

        return $this;
    }

    public function getIdTypePrix(): ?int
    {
        return $this->idTypePrix;
    }

    public function setIdTypePrix(?int $idTypePrix): self
    {
        $this->idTypePrix = $idTypePrix;

        return $this;
    }
}
