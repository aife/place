<?php

namespace App\Entity;

use DateTimeInterface;
use App\Repository\DerniersAppelsValidesWsSgmapDocumentsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="t_derniers_appels_valides_ws_sgmap_documents")
 * @ORM\Entity(repositoryClass=DerniersAppelsValidesWsSgmapDocumentsRepository::class)
 */
class DerniersAppelsValidesWsSgmapDocuments
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="bigint")
     */
    private ?string $identifiant;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $idTypeDocument;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $idEntreprise;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $idAgent;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $dateDernierAppelValide;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentifiant(): ?string
    {
        return $this->identifiant;
    }

    public function setIdentifiant(string $identifiant): self
    {
        $this->identifiant = $identifiant;

        return $this;
    }

    public function getIdTypeDocument(): ?int
    {
        return $this->idTypeDocument;
    }

    public function setIdTypeDocument(int $idTypeDocument): self
    {
        $this->idTypeDocument = $idTypeDocument;

        return $this;
    }

    public function getIdEntreprise(): ?int
    {
        return $this->idEntreprise;
    }

    public function setIdEntreprise(int $idEntreprise): self
    {
        $this->idEntreprise = $idEntreprise;

        return $this;
    }

    public function getIdAgent(): ?int
    {
        return $this->idAgent;
    }

    public function setIdAgent(int $idAgent): self
    {
        $this->idAgent = $idAgent;

        return $this;
    }

    public function getDateDernierAppelValide(): ?DateTimeInterface
    {
        return $this->dateDernierAppelValide;
    }

    public function setDateDernierAppelValide(DateTimeInterface $dateDernierAppelValide): self
    {
        $this->dateDernierAppelValide = $dateDernierAppelValide;

        return $this;
    }
}
