<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TTypeGroupementEntreprise.
 *
 * @ORM\Table(name="t_type_groupement_entreprise")
 * @ORM\Entity
 */
class TTypeGroupementEntreprise
{
    /**
     *
     * @ORM\Column(name="id_type_groupement", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $idTypeGroupement;

    /**
     * @ORM\Column(name="libelle_type_groupement", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private ?string $libelleTypeGroupement = null;

    /**
     * Get idTypeGroupement.
     *
     * @return int
     */
    public function getIdTypeGroupement()
    {
        return $this->idTypeGroupement;
    }

    /**
     * Set libelleTypeGroupement.
     *
     * @param string $libelleTypeGroupement
     *
     * @return TTypeGroupementEntreprise
     */
    public function setLibelleTypeGroupement($libelleTypeGroupement)
    {
        $this->libelleTypeGroupement = $libelleTypeGroupement;

        return $this;
    }

    /**
     * Get libelleTypeGroupement.
     *
     * @return string
     */
    public function getLibelleTypeGroupement()
    {
        return $this->libelleTypeGroupement;
    }
}
