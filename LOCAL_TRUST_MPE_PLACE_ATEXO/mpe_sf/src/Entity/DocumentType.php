<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\DocumentTypeRepository;

/**
 * DocumentType.
 *
 * @ORM\Table(name="t_document_type")
 * @ORM\Entity(repositoryClass=DocumentTypeRepository::class)
 */
class DocumentType
{
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\EntrepriseDocument", mappedBy="typeDocument")
     */
    private Collection $documents;

    /**
     *
     * @ORM\Column(name="id_type_document", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $idDocumentType;

    /**
     * @ORM\Column(name="nom_type_document", type="string", length=100, nullable=false)
     */
    private ?string $nomDocumentType = null;

    /**
     * @ORM\Column(name="code", type="string", length=50, nullable=true)
     */
    private ?string $code = null;

    /**
     * @ORM\Column(name="type_doc_entreprise_etablissement", type="string", nullable=true)
     */
    private ?string $typeDocEntrepriseEtablissement = null;

    /**
     * @ORM\Column(name="uri", type="string", length=100, nullable=true)
     */
    private ?string $uri = null;

    /**
     * @ORM\Column(name="params_uri", type="string", length=100, nullable=false)
     */
    private ?string $paramsUri = null;

    /**
     * @ORM\Column(name="class_name", type="string", length=100, nullable=false)
     */
    private ?string $className = null;

    /**
     * @ORM\Column(name="nature_document", type="string", nullable=false)
     */
    private string $natureDocument = '1';

    /**
     * Get idDocumentType.
     *
     * @return int
     */
    public function getIdDocumentType()
    {
        return $this->idDocumentType;
    }

    /**
     * Set nomDocumentType.
     *
     * @param string $nomDocumentType
     *
     * @return DocumentType
     */
    public function setNomDocumentType($nomDocumentType)
    {
        $this->nomDocumentType = $nomDocumentType;

        return $this;
    }

    /**
     * Get nomDocumentType.
     *
     * @return string
     */
    public function getNomDocumentType()
    {
        return $this->nomDocumentType;
    }

    /**
     * Entreprise constructor.
     */
    public function __construct()
    {
        $this->documents = new ArrayCollection();
    }

    /**
     * @return $this
     */
    public function addDocument(EntrepriseDocument $document)
    {
        $this->documents[] = $document;
        $document->setTypeDocument($this);

        return $this;
    }

    public function removeDocument(EntrepriseDocument $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * @return ArrayCollection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return DocumentType
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set typeDocEntrepriseEtablissement.
     *
     * @param string $typeDocEntrepriseEtablissement
     *
     * @return DocumentType
     */
    public function setTypeDocEntrepriseEtablissement($typeDocEntrepriseEtablissement)
    {
        $this->typeDocEntrepriseEtablissement = $typeDocEntrepriseEtablissement;

        return $this;
    }

    /**
     * Get typeDocEntrepriseEtablissement.
     *
     * @return string
     */
    public function getTypeDocEntrepriseEtablissement()
    {
        return $this->typeDocEntrepriseEtablissement;
    }

    /**
     * Set uri.
     *
     * @param string $uri
     *
     * @return DocumentType
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get uri.
     *
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Set paramsUri.
     *
     * @param string $paramsUri
     *
     * @return DocumentType
     */
    public function setParamsUri($paramsUri)
    {
        $this->paramsUri = $paramsUri;

        return $this;
    }

    /**
     * Get paramsUri.
     *
     * @return string
     */
    public function getParamsUri()
    {
        return $this->paramsUri;
    }

    /**
     * Set className.
     *
     * @param string $className
     *
     * @return DocumentType
     */
    public function setClassName($className)
    {
        $this->className = $className;

        return $this;
    }

    /**
     * Get className.
     *
     * @return string
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * Set natureDocument.
     *
     * @param string $natureDocument
     *
     * @return DocumentType
     */
    public function setNatureDocument($natureDocument)
    {
        $this->natureDocument = $natureDocument;

        return $this;
    }

    /**
     * Get natureDocument.
     *
     * @return string
     */
    public function getNatureDocument()
    {
        return $this->natureDocument;
    }
}
