<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Entity\Consultation;

use Doctrine\ORM\Mapping as ORM;

/**
 * EtatConsultation.
 *
 * @ORM\Table(name="EtatConsultation")
 * @ORM\Entity(repositoryClass="App\Repository\Consultation\EtatConsultationRepository")
 */
class EtatConsultation
{
    /**
     *
     * @ORM\Column(name="id_etat", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $idEtat;

    /**
     * @ORM\Column(name="code_etat", type="string", length=200, nullable=false)
     */
    private ?string $codeEtat = null;

    /**
     * @ORM\Column(name="abreviation_etat", type="string", length=20, nullable=false)
     */
    private ?string $abreviationEtat = null;

    /**
     * @ORM\Column(name="visible", type="integer", nullable=false)
     */
    private ?int $visible = null;

    public function getIdEtat(): int
    {
        return $this->idEtat;
    }

    public function getCodeEtat(): string
    {
        return $this->codeEtat;
    }

    public function setCodeEtat(string $codeEtat): EtatConsultation
    {
        $this->codeEtat = $codeEtat;
        return $this;
    }

    public function getAbreviationEtat(): string
    {
        return $this->abreviationEtat;
    }

    public function setAbreviationEtat(string $abreviationEtat): EtatConsultation
    {
        $this->abreviationEtat = $abreviationEtat;
        return $this;
    }

    public function getVisible(): int
    {
        return $this->visible;
    }

    public function setVisible(int $visible): EtatConsultation
    {
        $this->visible = $visible;
        return $this;
    }
}
