<?php

namespace App\Entity\Consultation;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * DonneeComplementaire.
 *
 * @ORM\Table(name="t_donnee_complementaire")
 * @ORM\Entity
 */
#[ApiResource(
    collectionOperations: [
        'get',
        'post',
    ],
    itemOperations: ['get'],
)]
class DonneeComplementaire
{
    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="CritereAttribution",mappedBy="donneeComplementaire", fetch="EAGER")
     * @ORM\JoinColumn(name="id_donnee_complementaire", referencedColumnName="id_donnee_complementaire")
     */
    private Collection $criteresAttribution;

    /**
     *
     * @ORM\Column(name="id_donnee_complementaire", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $idDonneeComplementaire = null;

    /**
     * @ORM\Column(name="id_tranche_type_prix", type="integer", nullable=true)
     */
    private ?int $idTrancheTypePrix = null;

    /**
     * @ORM\Column(name="id_forme_prix", type="integer", nullable=true)
     */
    private ?int $idFormePrix = null;

    /**
     * @ORM\Column(name="id_ccag_reference", type="integer", nullable=true)
     */
    private ?int $idCcagReference = null;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="reconductible", type="boolean", nullable=true)
     */
    private $reconductible;

    /**
     * @ORM\Column(name="nombre_reconductions", type="integer", nullable=true)
     */
    private ?int $nombreReconductions = null;

    /**
     * @ORM\Column(name="modalites_reconduction", type="string", length=1000, nullable=true)
     */
    private ?string $modalitesReconduction = null;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="variantes_autorisees", type="boolean", nullable=true)
     */
    private $variantesAutorisees;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="variante_exigee", type="boolean", nullable=true)
     */
    private $varianteExigee;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="variantes_techniques_obligatoires", type="boolean", nullable=true)
     */
    private $variantesTechniquesObligatoires;

    /**
     * @ORM\Column(name="variantes_techniques_description", type="string", length=100, nullable=true)
     */
    private ?string $variantesTechniquesDescription = null;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="decomposition_lots_techniques", type="boolean", nullable=true)
     */
    private $decompositionLotsTechniques;

    /**
     * @ORM\Column(name="id_duree_delai_description", type="integer", nullable=true)
     */
    private ?int $idDureeDelaiDescription = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estimation_pf_att_pressenti", type="decimal", precision=30, scale=2, nullable=true)
     */
    private $estimationPfAttPressenti;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estimation_bc_min_att_pressenti", type="decimal", precision=30, scale=2, nullable=true)
     */
    private $estimationBcMinAttPressenti;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estimation_bc_max_att_pressenti", type="decimal", precision=30, scale=2, nullable=true)
     */
    private $estimationBcMaxAttPressenti;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estimation_pf_tab_ouv_offre", type="decimal", precision=30, scale=2, nullable=true)
     */
    private $estimationPfTabOuvOffre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estimation_dqe_tab_ouv_offre", type="decimal", precision=30, scale=2, nullable=true)
     */
    private $estimationDqeTabOuvOffre;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="avis_attribution_present", type="boolean", nullable=true)
     */
    private $avisAttributionPresent = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="estimation_pf_preinscription", type="decimal", precision=30, scale=2, nullable=true)
     */
    private $estimationPfPreinscription;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estimation_bc_min_preinscription", type="decimal", precision=30, scale=2, nullable=true)
     */
    private $estimationBcMinPreinscription;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estimation_bc_max_preinscription", type="decimal", precision=30, scale=2, nullable=true)
     */
    private $estimationBcMaxPreinscription;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="estimation_date_valeur_preinscription", type="date", nullable=true)*/
    private $estimationDateValeurPreinscription;

    /**
     * @ORM\Column(name="lieu_execution", type="string", length=40, nullable=true)
     */
    private ?string $lieuExecution = null;

    /**
     * @ORM\Column(name="duree_marche", type="integer", nullable=true)
     */
    private ?int $dureeMarche = null;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="duree_date_debut", type="date", nullable=true)*/
    private $dureeDateDebut;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="duree_date_fin", type="date", nullable=true)*/
    private $dureeDateFin;

    /**
     * @ORM\Column(name="duree_description", type="string", length=200, nullable=true)
     */
    private ?string $dureeDescription = null;

    /**
     * @ORM\Column(name="id_choix_mois_jour", type="integer", nullable=true)
     */
    private ?int $idChoixMoisJour = null;

    /**
     * @ORM\Column(name="id_unite", type="integer", nullable=true)
     */
    private ?int $idUnite = null;

    /**
     * @ORM\Column(name="id_nb_candidats_admis", type="integer", nullable=true)
     */
    private ?int $idNbCandidatsAdmis = null;

    /**
     * @ORM\Column(name="nombre_candidats_fixe", type="integer", nullable=true)
     */
    private ?int $nombreCandidatsFixe = null;

    /**
     * @ORM\Column(name="nombre_candidats_min", type="integer", nullable=true)
     */
    private ?int $nombreCandidatsMin = null;

    /**
     * @ORM\Column(name="nombre_candidats_max", type="integer", nullable=true)
     */
    private ?int $nombreCandidatsMax = null;

    /**
     * @ORM\Column(name="delai_validite_offres", type="integer", nullable=true)
     */
    private ?int $delaiValiditeOffres = null;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="phase_successive", type="boolean", nullable=true)
     */
    private $phaseSuccessive;

    /**
     * @ORM\Column(name="id_groupement_attributaire", type="integer", nullable=true)
     */
    private ?int $idGroupementAttributaire = null;

    /**
     * @ORM\Column(name="id_critere_attribution", type="integer", nullable=true)
     */
    private ?int $idCritereAttribution = null;

    /**
     * @ORM\Column(name="type_prestation", type="integer", nullable=false, options={"default"="1"})
     */
    private string|int $typePrestation = '1';

    /**
     * @ORM\Column(name="delai_partiel", type="string", nullable=false)
     */
    private string $delaiPartiel = '0';

    /**
     * @ORM\Column(name="adresse_retrais_dossiers", type="text", length=0, nullable=true)
     */
    private ?string $adresseRetraisDossiers = null;

    /**
     * @ORM\Column(name="adresse_depot_offres", type="text", length=0, nullable=true)
     */
    private ?string $adresseDepotOffres = null;

    /**
     * @ORM\Column(name="lieu_ouverture_plis", type="text", length=0, nullable=true)
     */
    private ?string $lieuOuverturePlis = null;

    /**
     * @ORM\Column(name="pieces_dossier_admin", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierAdmin = null;

    /**
     * @ORM\Column(name="id_tr_pieces_dossier_admin", type="integer", nullable=true)
     */
    private ?int $idTrPiecesDossierAdmin = null;

    /**
     * @ORM\Column(name="pieces_dossier_tech", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierTech = null;

    /**
     * @ORM\Column(name="id_tr_pieces_dossier_tech", type="integer", nullable=true)
     */
    private ?int $idTrPiecesDossierTech = null;

    /**
     * @ORM\Column(name="pieces_dossier_additif", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierAdditif = null;

    /**
     * @ORM\Column(name="id_tr_pieces_dossier_additif", type="integer", nullable=true)
     */
    private ?int $idTrPiecesDossierAdditif = null;

    /**
     * @ORM\Column(name="id_tr_adresse_retrais_dossiers", type="integer", nullable=true)
     */
    private ?int $idTrAdresseRetraisDossiers = null;

    /**
     * @ORM\Column(name="id_tr_adresse_depot_offres", type="integer", nullable=true)
     */
    private ?int $idTrAdresseDepotOffres = null;

    /**
     * @ORM\Column(name="id_tr_lieu_ouverture_plis", type="integer", nullable=true)
     */
    private ?int $idTrLieuOuverturePlis = null;

    /**
     * @ORM\Column(name="caution_provisoire", type="string", length=255, nullable=true)
     */
    private ?string $cautionProvisoire = null;

    /**
     * @ORM\Column(name="prix_aquisition_plans", type="string", length=255, nullable=true)
     */
    private ?string $prixAquisitionPlans = null;

    /**
     * @ORM\Column(name="add_echantillon", type="string", length=255, nullable=true)
     */
    private ?string $addEchantillon = null;

    /**
     * @ORM\Column(name="id_tr_add_echantillon", type="integer", nullable=true,
     *     options={"comment"="Identifiant de la traduction de l'échantillon"})
     */
    private ?int $idTrAddEchantillon = null;

    /**
     * @ORM\Column(name="date_limite_echantillon", type="string", length=50, nullable=true)
     */
    private ?string $dateLimiteEchantillon = null;

    /**
     * @ORM\Column(name="add_reunion", type="string", length=255, nullable=true)
     */
    private ?string $addReunion = null;

    /**
     * @ORM\Column(name="id_tr_add_reunion", type="integer", nullable=true,
     *     options={"comment"="Identifiant de la traduction de la réunion"})
     */
    private ?int $idTrAddReunion = null;

    /**
     * @ORM\Column(name="date_reunion", type="string", length=50, nullable=true)
     */
    private ?string $dateReunion = null;

    /**
     * @ORM\Column(name="reunion", type="string", nullable=false)
     */
    private string $reunion = '0';

    /**
     * @ORM\Column(name="visites_lieux", type="string", nullable=false)
     */
    private string $visitesLieux = '0';

    /**
     * @ORM\Column(name="echantillon", type="string", nullable=false)
     */
    private string $echantillon = '0';

    /**
     * @ORM\Column(name="variantes", type="string", nullable=true, options={"fixed"=true})
     */
    private ?string $variantes = null;

    /**
     * @ORM\Column(name="variante_calcule", type="string", nullable=false)
     */
    private string $varianteCalcule = '0';

    /**
     * @ORM\Column(name="criteres_identiques", type="string", nullable=false,
     *     options={"comment"="permet de savoir si les critères sont identiques pour tous les lots"})
     */
    private string $criteresIdentiques = '0';

    /**
     * @ORM\Column(name="justification_non_alloti", type="string", length=255, nullable=true)
     */
    private ?string $justificationNonAlloti = null;

    /**
     * @ORM\Column(name="id_ccag_dpi", type="integer", nullable=true)
     */
    private ?int $idCcagDpi = null;

    /**
     * @ORM\Column(name="montant_marche", type="float", precision=10, scale=0, nullable=true)
     */
    private ?float $montantMarche = null;

    /**
     * @ORM\Column(name="mots_cles", type="string", length=255, nullable=true)
     */
    private ?string $motsCles = null;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="procedure_accord_marches_publics_omc", type="boolean", nullable=true)
     */
    private $procedureAccordMarchesPublicsOmc;

    /**
     * @ORM\Column(name="cautionnement_regime_financier", type="text", length=0, nullable=true)
     */
    private ?string $cautionnementRegimeFinancier = null;

    /**
     * @ORM\Column(name="modalites_financement_regime_financier", type="text", length=0, nullable=true)
     */
    private ?string $modalitesFinancementRegimeFinancier = null;

    /**
     * @ORM\Column(name="publication_montant_estimation", type="string", nullable=false,
     *     options={"comment"="publication du montant estime 0 => non, 1 => oui"})
     */
    private string $publicationMontantEstimation = '0';

    /**
     * @ORM\Column(name="valeur_montant_estimation_publiee", type="float", nullable=true)
     */
    private ?float $valeurMontantEstimationPubliee = null;

    /**
     * @ORM\Column(name="projet_finance_fonds_union_europeenne", type="string", nullable=false)
     */
    private string $projetFinanceFondsUnionEuropeenne = '0';

    /**
     * @ORM\Column(name="visite_obligatoire", type="boolean", nullable=true)
     */
    private ?bool $visiteObligatoire = null;

    /**
     * @ORM\Column(name="visite_description", type="string", length=100, nullable=true)
     */
    private ?string $visiteDescription = null;

    /**
     * @ORM\Column(name="catalogue_electronique", type="integer", length=1, nullable=true)
     */
    private ?int $catalogueElectronique = null;

    /**
     * @ORM\Column(name="attribution_sans_negociation", type="boolean", nullable=true)
     */
    private ?bool $attributionSansNegociation = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="variante_prestations_supp_eventuelles", type="boolean")
     */
    private $variantePrestationsSuppEventuelles = false;

    /**
     * @ORM\Column(name="identification_projet", type="text", nullable=false)
     */
    private string $identificationProjet = '';

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $activiteProfessionel;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $economiqueFinanciere;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $techniquesProfessionels;

    private ?bool $avecTranche = false;

    /**
     * @ORM\OneToOne(targetEntity=ConsultationComptePub::class, mappedBy="donneeComplementaire", cascade={"persist"})
     */
    private ?ConsultationComptePub $consultationComptePub = null;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Tranche", mappedBy="idDonneeComplementaire", fetch="EAGER")
     * @ORM\JoinColumn(name="id_donnee_complementaire", referencedColumnName="id_donnee_complementaire")
     */
    private Collection $tranches;


    /**
     * Les propriétés static suivant seront à mappé lors de la migration des reférentiel de consultation vers les
     * donnees-complémentaire
     */
    private ?string $informationComplementaire = null;
    private ?string $autresInformations = null;
    private ?string $modaliteVisiteLieuxReunion = null;
    private ?string $conditionParticipation = null;
    private ?string $autrePouvoirAdjudicateur = null;
    private ?string $formuleSignature = null;
    /**
     * Fin de la liste des propriétés static
     */

    public function __construct()
    {
        $this->criteresAttribution = new ArrayCollection();
        $this->formePrix = new ArrayCollection();
        $this->tranches = new ArrayCollection();
    }

    public function getIdDonneeComplementaire(): int
    {
        return $this->idDonneeComplementaire;
    }

    public function getIdTrancheTypePrix(): ?int
    {
        return $this->idTrancheTypePrix;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setIdTrancheTypePrix(?int $idTrancheTypePrix)
    {
        $this->idTrancheTypePrix = $idTrancheTypePrix;

        return $this;
    }

    public function getIdFormePrix(): ?int
    {
        return $this->idFormePrix;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setIdFormePrix(?int $idFormePrix)
    {
        $this->idFormePrix = $idFormePrix;

        return $this;
    }

    public function getIdCcagReference(): ?int
    {
        return $this->idCcagReference;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setIdCcagReference(?int $idCcagReference)
    {
        $this->idCcagReference = $idCcagReference;

        return $this;
    }

    public function getReconductible(): ?bool
    {
        return $this->reconductible;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setReconductible(?bool $reconductible)
    {
        $this->reconductible = $reconductible;

        return $this;
    }

    public function getNombreReconductions(): ?int
    {
        return $this->nombreReconductions;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setNombreReconductions(?int $nombreReconductions)
    {
        $this->nombreReconductions = $nombreReconductions;

        return $this;
    }

    public function getModalitesReconduction(): ?string
    {
        return $this->modalitesReconduction;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setModalitesReconduction(?string $modalitesReconduction)
    {
        $this->modalitesReconduction = $modalitesReconduction;

        return $this;
    }

    public function getVariantesAutorisees(): ?bool
    {
        return $this->variantesAutorisees;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setVariantesAutorisees(?bool $variantesAutorisees)
    {
        $this->variantesAutorisees = $variantesAutorisees;

        return $this;
    }

    public function getVarianteExigee(): ?bool
    {
        return $this->varianteExigee;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setVarianteExigee(?bool $varianteExigee)
    {
        $this->varianteExigee = $varianteExigee;

        return $this;
    }

    public function getVariantesTechniquesObligatoires(): ?bool
    {
        return $this->variantesTechniquesObligatoires;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setVariantesTechniquesObligatoires(?bool $variantesTechniquesObligatoires)
    {
        $this->variantesTechniquesObligatoires = $variantesTechniquesObligatoires;

        return $this;
    }

    public function getVariantesTechniquesDescription(): ?string
    {
        return $this->variantesTechniquesDescription;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setVariantesTechniquesDescription(?string $variantesTechniquesDescription)
    {
        $this->variantesTechniquesDescription = $variantesTechniquesDescription;

        return $this;
    }

    public function getDecompositionLotsTechniques(): ?bool
    {
        return $this->decompositionLotsTechniques;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setDecompositionLotsTechniques(?bool $decompositionLotsTechniques)
    {
        $this->decompositionLotsTechniques = $decompositionLotsTechniques;

        return $this;
    }

    public function getIdDureeDelaiDescription(): ?int
    {
        return $this->idDureeDelaiDescription;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setIdDureeDelaiDescription(?int $idDureeDelaiDescription)
    {
        $this->idDureeDelaiDescription = $idDureeDelaiDescription;

        return $this;
    }

    public function getEstimationPfAttPressenti(): ?string
    {
        return $this->estimationPfAttPressenti;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setEstimationPfAttPressenti(?string $estimationPfAttPressenti)
    {
        $this->estimationPfAttPressenti = $estimationPfAttPressenti;

        return $this;
    }

    public function getEstimationBcMinAttPressenti(): ?string
    {
        return $this->estimationBcMinAttPressenti;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setEstimationBcMinAttPressenti(?string $estimationBcMinAttPressenti)
    {
        $this->estimationBcMinAttPressenti = $estimationBcMinAttPressenti;

        return $this;
    }

    public function getEstimationBcMaxAttPressenti(): ?string
    {
        return $this->estimationBcMaxAttPressenti;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setEstimationBcMaxAttPressenti(?string $estimationBcMaxAttPressenti)
    {
        $this->estimationBcMaxAttPressenti = $estimationBcMaxAttPressenti;

        return $this;
    }

    public function getEstimationPfTabOuvOffre(): ?string
    {
        return $this->estimationPfTabOuvOffre;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setEstimationPfTabOuvOffre(?string $estimationPfTabOuvOffre)
    {
        $this->estimationPfTabOuvOffre = $estimationPfTabOuvOffre;

        return $this;
    }

    public function getEstimationDqeTabOuvOffre(): ?string
    {
        return $this->estimationDqeTabOuvOffre;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setEstimationDqeTabOuvOffre(?string $estimationDqeTabOuvOffre)
    {
        $this->estimationDqeTabOuvOffre = $estimationDqeTabOuvOffre;

        return $this;
    }

    public function getAvisAttributionPresent(): ?bool
    {
        return $this->avisAttributionPresent;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setAvisAttributionPresent(?bool $avisAttributionPresent)
    {
        $this->avisAttributionPresent = $avisAttributionPresent;

        return $this;
    }

    public function getEstimationPfPreinscription(): ?string
    {
        return $this->estimationPfPreinscription;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setEstimationPfPreinscription(?string $estimationPfPreinscription)
    {
        $this->estimationPfPreinscription = $estimationPfPreinscription;

        return $this;
    }

    public function getEstimationBcMinPreinscription(): ?string
    {
        return $this->estimationBcMinPreinscription;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setEstimationBcMinPreinscription(?string $estimationBcMinPreinscription)
    {
        $this->estimationBcMinPreinscription = $estimationBcMinPreinscription;

        return $this;
    }

    public function getEstimationBcMaxPreinscription(): ?string
    {
        return $this->estimationBcMaxPreinscription;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setEstimationBcMaxPreinscription(?string $estimationBcMaxPreinscription)
    {
        $this->estimationBcMaxPreinscription = $estimationBcMaxPreinscription;

        return $this;
    }

    public function getEstimationDateValeurPreinscription(): ?DateTime
    {
        return $this->estimationDateValeurPreinscription;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setEstimationDateValeurPreinscription(?DateTime $estimationDateValeurPreinscription)
    {
        $this->estimationDateValeurPreinscription = $estimationDateValeurPreinscription;

        return $this;
    }

    public function getLieuExecution(): ?string
    {
        return $this->lieuExecution;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setLieuExecution(?string $lieuExecution)
    {
        $this->lieuExecution = $lieuExecution;

        return $this;
    }

    public function getDureeMarche(): ?int
    {
        return $this->dureeMarche;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setDureeMarche(?int $dureeMarche)
    {
        $this->dureeMarche = $dureeMarche;

        return $this;
    }

    public function getDureeDateDebut(): ?DateTime
    {
        return $this->dureeDateDebut;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setDureeDateDebut(?DateTime $dureeDateDebut)
    {
        $this->dureeDateDebut = $dureeDateDebut;

        return $this;
    }

    public function getDureeDateFin(): ?DateTime
    {
        return $this->dureeDateFin;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setDureeDateFin(?DateTime $dureeDateFin)
    {
        $this->dureeDateFin = $dureeDateFin;

        return $this;
    }

    public function getDureeDescription(): ?string
    {
        return $this->dureeDescription;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setDureeDescription(?string $dureeDescription)
    {
        $this->dureeDescription = $dureeDescription;

        return $this;
    }

    public function getIdChoixMoisJour(): ?int
    {
        return $this->idChoixMoisJour;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setIdChoixMoisJour(?int $idChoixMoisJour)
    {
        $this->idChoixMoisJour = $idChoixMoisJour;

        return $this;
    }

    public function getIdUnite(): ?int
    {
        return $this->idUnite;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setIdUnite(?int $idUnite)
    {
        $this->idUnite = $idUnite;

        return $this;
    }

    public function getIdNbCandidatsAdmis(): ?int
    {
        return $this->idNbCandidatsAdmis;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setIdNbCandidatsAdmis(?int $idNbCandidatsAdmis)
    {
        $this->idNbCandidatsAdmis = $idNbCandidatsAdmis;

        return $this;
    }

    public function getNombreCandidatsFixe(): ?int
    {
        return $this->nombreCandidatsFixe;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setNombreCandidatsFixe(?int $nombreCandidatsFixe)
    {
        $this->nombreCandidatsFixe = $nombreCandidatsFixe;

        return $this;
    }

    public function getNombreCandidatsMin(): ?int
    {
        return $this->nombreCandidatsMin;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setNombreCandidatsMin(?int $nombreCandidatsMin)
    {
        $this->nombreCandidatsMin = $nombreCandidatsMin;

        return $this;
    }

    public function getNombreCandidatsMax(): ?int
    {
        return $this->nombreCandidatsMax;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setNombreCandidatsMax(?int $nombreCandidatsMax)
    {
        $this->nombreCandidatsMax = $nombreCandidatsMax;

        return $this;
    }

    public function getDelaiValiditeOffres(): ?int
    {
        return $this->delaiValiditeOffres;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setDelaiValiditeOffres(?int $delaiValiditeOffres)
    {
        $this->delaiValiditeOffres = $delaiValiditeOffres;

        return $this;
    }

    public function getPhaseSuccessive(): ?bool
    {
        return $this->phaseSuccessive;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setPhaseSuccessive(?bool $phaseSuccessive)
    {
        $this->phaseSuccessive = $phaseSuccessive;

        return $this;
    }

    public function getIdGroupementAttributaire(): ?int
    {
        return $this->idGroupementAttributaire;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setIdGroupementAttributaire(?int $idGroupementAttributaire)
    {
        $this->idGroupementAttributaire = $idGroupementAttributaire;

        return $this;
    }

    public function getIdCritereAttribution(): ?int
    {
        return $this->idCritereAttribution;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setIdCritereAttribution(?int $idCritereAttribution)
    {
        $this->idCritereAttribution = $idCritereAttribution;

        return $this;
    }

    public function getTypePrestation(): int
    {
        return $this->typePrestation;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setTypePrestation(int $typePrestation)
    {
        $this->typePrestation = $typePrestation;

        return $this;
    }

    public function getDelaiPartiel(): string
    {
        return $this->delaiPartiel;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setDelaiPartiel(string $delaiPartiel)
    {
        $this->delaiPartiel = $delaiPartiel;

        return $this;
    }

    public function getAdresseRetraisDossiers(): ?string
    {
        return $this->adresseRetraisDossiers;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setAdresseRetraisDossiers(?string $adresseRetraisDossiers)
    {
        $this->adresseRetraisDossiers = $adresseRetraisDossiers;

        return $this;
    }

    public function getAdresseDepotOffres(): ?string
    {
        return $this->adresseDepotOffres;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setAdresseDepotOffres(?string $adresseDepotOffres)
    {
        $this->adresseDepotOffres = $adresseDepotOffres;

        return $this;
    }

    public function getLieuOuverturePlis(): ?string
    {
        return $this->lieuOuverturePlis;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setLieuOuverturePlis(?string $lieuOuverturePlis)
    {
        $this->lieuOuverturePlis = $lieuOuverturePlis;

        return $this;
    }

    public function getPiecesDossierAdmin(): ?string
    {
        return $this->piecesDossierAdmin;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setPiecesDossierAdmin(?string $piecesDossierAdmin)
    {
        $this->piecesDossierAdmin = $piecesDossierAdmin;

        return $this;
    }

    public function getIdTrPiecesDossierAdmin(): ?int
    {
        return $this->idTrPiecesDossierAdmin;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setIdTrPiecesDossierAdmin(?int $idTrPiecesDossierAdmin)
    {
        $this->idTrPiecesDossierAdmin = $idTrPiecesDossierAdmin;

        return $this;
    }

    public function getPiecesDossierTech(): ?string
    {
        return $this->piecesDossierTech;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setPiecesDossierTech(?string $piecesDossierTech)
    {
        $this->piecesDossierTech = $piecesDossierTech;

        return $this;
    }

    public function getIdTrPiecesDossierTech(): ?int
    {
        return $this->idTrPiecesDossierTech;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setIdTrPiecesDossierTech(?int $idTrPiecesDossierTech)
    {
        $this->idTrPiecesDossierTech = $idTrPiecesDossierTech;

        return $this;
    }

    public function getPiecesDossierAdditif(): ?string
    {
        return $this->piecesDossierAdditif;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setPiecesDossierAdditif(?string $piecesDossierAdditif)
    {
        $this->piecesDossierAdditif = $piecesDossierAdditif;

        return $this;
    }

    public function getIdTrPiecesDossierAdditif(): ?int
    {
        return $this->idTrPiecesDossierAdditif;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setIdTrPiecesDossierAdditif(?int $idTrPiecesDossierAdditif)
    {
        $this->idTrPiecesDossierAdditif = $idTrPiecesDossierAdditif;

        return $this;
    }

    public function getIdTrAdresseRetraisDossiers(): ?int
    {
        return $this->idTrAdresseRetraisDossiers;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setIdTrAdresseRetraisDossiers(?int $idTrAdresseRetraisDossiers)
    {
        $this->idTrAdresseRetraisDossiers = $idTrAdresseRetraisDossiers;

        return $this;
    }

    public function getIdTrAdresseDepotOffres(): ?int
    {
        return $this->idTrAdresseDepotOffres;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setIdTrAdresseDepotOffres(?int $idTrAdresseDepotOffres)
    {
        $this->idTrAdresseDepotOffres = $idTrAdresseDepotOffres;

        return $this;
    }

    public function getIdTrLieuOuverturePlis(): ?int
    {
        return $this->idTrLieuOuverturePlis;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setIdTrLieuOuverturePlis(?int $idTrLieuOuverturePlis)
    {
        $this->idTrLieuOuverturePlis = $idTrLieuOuverturePlis;

        return $this;
    }

    public function getCautionProvisoire(): ?string
    {
        return $this->cautionProvisoire;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setCautionProvisoire(?string $cautionProvisoire)
    {
        $this->cautionProvisoire = $cautionProvisoire;

        return $this;
    }

    public function getPrixAquisitionPlans(): ?string
    {
        return $this->prixAquisitionPlans;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setPrixAquisitionPlans(?string $prixAquisitionPlans)
    {
        $this->prixAquisitionPlans = $prixAquisitionPlans;

        return $this;
    }

    public function getAddEchantillon(): ?string
    {
        return $this->addEchantillon;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setAddEchantillon(?string $addEchantillon)
    {
        $this->addEchantillon = $addEchantillon;

        return $this;
    }

    public function getIdTrAddEchantillon(): ?int
    {
        return $this->idTrAddEchantillon;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setIdTrAddEchantillon(?int $idTrAddEchantillon)
    {
        $this->idTrAddEchantillon = $idTrAddEchantillon;

        return $this;
    }

    public function getDateLimiteEchantillon(): ?string
    {
        return $this->dateLimiteEchantillon;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setDateLimiteEchantillon(?string $dateLimiteEchantillon)
    {
        $this->dateLimiteEchantillon = $dateLimiteEchantillon;

        return $this;
    }

    public function getAddReunion(): ?string
    {
        return $this->addReunion;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setAddReunion(?string $addReunion)
    {
        $this->addReunion = $addReunion;

        return $this;
    }

    public function getIdTrAddReunion(): ?int
    {
        return $this->idTrAddReunion;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setIdTrAddReunion(?int $idTrAddReunion)
    {
        $this->idTrAddReunion = $idTrAddReunion;

        return $this;
    }

    public function getDateReunion(): ?string
    {
        return $this->dateReunion;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setDateReunion(?string $dateReunion)
    {
        $this->dateReunion = $dateReunion;

        return $this;
    }

    public function getReunion(): string
    {
        return $this->reunion;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setReunion(string $reunion)
    {
        $this->reunion = $reunion;

        return $this;
    }

    public function getVisitesLieux(): string
    {
        return $this->visitesLieux;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setVisitesLieux(string $visitesLieux)
    {
        $this->visitesLieux = $visitesLieux;

        return $this;
    }

    public function getEchantillon(): string
    {
        return $this->echantillon;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setEchantillon(string $echantillon)
    {
        $this->echantillon = $echantillon;

        return $this;
    }

    public function getVariantes(): ?string
    {
        return $this->variantes;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setVariantes(?string $variantes)
    {
        $this->variantes = $variantes;

        return $this;
    }

    public function getVarianteCalcule(): string
    {
        return $this->varianteCalcule;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setVarianteCalcule(string $varianteCalcule)
    {
        $this->varianteCalcule = $varianteCalcule;

        return $this;
    }

    public function getCriteresIdentiques(): string
    {
        return $this->criteresIdentiques;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setCriteresIdentiques(string $criteresIdentiques)
    {
        $this->criteresIdentiques = $criteresIdentiques;

        return $this;
    }

    public function getJustificationNonAlloti(): ?string
    {
        return $this->justificationNonAlloti;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setJustificationNonAlloti(?string $justificationNonAlloti)
    {
        $this->justificationNonAlloti = $justificationNonAlloti;

        return $this;
    }

    public function getIdCcagDpi(): ?int
    {
        return $this->idCcagDpi;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setIdCcagDpi(?int $idCcagDpi)
    {
        $this->idCcagDpi = $idCcagDpi;

        return $this;
    }

    public function getMontantMarche(): ?float
    {
        return $this->montantMarche;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setMontantMarche(?float $montantMarche)
    {
        $this->montantMarche = $montantMarche;

        return $this;
    }

    public function getMotsCles(): ?string
    {
        return $this->motsCles;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setMotsCles(?string $motsCles)
    {
        $this->motsCles = $motsCles;

        return $this;
    }

    public function getProcedureAccordMarchesPublicsOmc(): ?bool
    {
        return $this->procedureAccordMarchesPublicsOmc;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setProcedureAccordMarchesPublicsOmc(?bool $procedureAccordMarchesPublicsOmc)
    {
        $this->procedureAccordMarchesPublicsOmc = $procedureAccordMarchesPublicsOmc;

        return $this;
    }

    public function getCautionnementRegimeFinancier(): ?string
    {
        return $this->cautionnementRegimeFinancier;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setCautionnementRegimeFinancier(?string $cautionnementRegimeFinancier)
    {
        $this->cautionnementRegimeFinancier = $cautionnementRegimeFinancier;

        return $this;
    }

    public function getModalitesFinancementRegimeFinancier(): ?string
    {
        return $this->modalitesFinancementRegimeFinancier;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setModalitesFinancementRegimeFinancier(?string $modalitesFinancementRegimeFinancier)
    {
        $this->modalitesFinancementRegimeFinancier = $modalitesFinancementRegimeFinancier;

        return $this;
    }

    public function getPublicationMontantEstimation(): string
    {
        return $this->publicationMontantEstimation;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setPublicationMontantEstimation(string $publicationMontantEstimation)
    {
        $this->publicationMontantEstimation = $publicationMontantEstimation;

        return $this;
    }

    public function getValeurMontantEstimationPubliee(): ?float
    {
        return $this->valeurMontantEstimationPubliee;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setValeurMontantEstimationPubliee(?float $valeurMontantEstimationPubliee)
    {
        $this->valeurMontantEstimationPubliee = $valeurMontantEstimationPubliee;

        return $this;
    }

    public function getProjetFinanceFondsUnionEuropeenne(): string
    {
        return $this->projetFinanceFondsUnionEuropeenne;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setProjetFinanceFondsUnionEuropeenne(string $projetFinanceFondsUnionEuropeenne)
    {
        $this->projetFinanceFondsUnionEuropeenne = $projetFinanceFondsUnionEuropeenne;

        return $this;
    }

    public function getIdentificationProjet(): string
    {
        return $this->identificationProjet;
    }

    /**
     * @return DonneeComplementaire
     */
    public function setIdentificationProjet(string $identificationProjet)
    {
        $this->identificationProjet = $identificationProjet;

        return $this;
    }

    public function getCriteresAttribution(): array
    {
        return $this->criteresAttribution->toArray();
    }

    public function getCriteresAttributionCollection(): Collection
    {
        return $this->criteresAttribution;
    }

    /**
     * @param $criteresAttribution
     */
    public function setCriteresAttribution($criteresAttribution): DonneeComplementaire
    {
        $this->criteresAttribution = $criteresAttribution;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getVisiteObligatoire(): ?bool
    {
        return $this->visiteObligatoire;
    }

    /**
     * @param bool|null $visiteObligatoire
     */
    public function setVisiteObligatoire(?bool $visiteObligatoire): void
    {
        $this->visiteObligatoire = $visiteObligatoire;
    }

    /**
     * @return string|null
     */
    public function getVisiteDescription(): ?string
    {
        return $this->visiteDescription;
    }

    /**
     * @param string|null $visiteDescription
     */
    public function setVisiteDescription(?string $visiteDescription): void
    {
        $this->visiteDescription = $visiteDescription;
    }

    /**
     * @return int|null
     */
    public function getCatalogueElectronique(): ?int
    {
        return $this->catalogueElectronique;
    }

    /**
     * @param int|null $catalogueElectronique
     */
    public function setCatalogueElectronique(?int $catalogueElectronique): void
    {
        $this->catalogueElectronique = $catalogueElectronique;
    }

    /**
     * @return bool|null
     */
    public function getAttributionSansNegociation(): ?bool
    {
        return $this->attributionSansNegociation;
    }

    /**
     * @param bool|null $attributionSansNegociation
     */
    public function setAttributionSansNegociation(?bool $attributionSansNegociation): void
    {
        $this->attributionSansNegociation = $attributionSansNegociation;
    }

    /**
     * @return bool
     */
    public function isVariantePrestationsSuppEventuelles(): bool
    {
        return $this->variantePrestationsSuppEventuelles;
    }

    /**
     * @param bool $variantePrestationsSuppEventuelles
     * @return DonneeComplementaire
     */
    public function setVariantePrestationsSuppEventuelles(
        bool $variantePrestationsSuppEventuelles
    ): DonneeComplementaire {
        $this->variantePrestationsSuppEventuelles = $variantePrestationsSuppEventuelles;

        return $this;
    }

    public function getActiviteProfessionel(): ?string
    {
        return $this->activiteProfessionel;
    }

    public function setActiviteProfessionel(?string $activiteProfessionel): self
    {
        $this->activiteProfessionel = $activiteProfessionel;

        return $this;
    }

    public function getEconomiqueFinanciere(): ?string
    {
        return $this->economiqueFinanciere;
    }

    public function setEconomiqueFinanciere(?string $economiqueFinanciere): self
    {
        $this->economiqueFinanciere = $economiqueFinanciere;

        return $this;
    }

    public function getTechniquesProfessionels(): ?string
    {
        return $this->techniquesProfessionels;
    }

    public function setTechniquesProfessionels(?string $techniquesProfessionels): self
    {
        $this->techniquesProfessionels = $techniquesProfessionels;

        return $this;
    }

    public function getAvecTranche(): ?bool
    {
        return $this->avecTranche;
    }

    public function setAvecTranche(?bool $avecTranche): DonneeComplementaire
    {
        $this->avecTranche = $avecTranche;

        return $this;
    }

    public function getConsultationComptePub(): ?ConsultationComptePub
    {
        return $this->consultationComptePub;
    }

    public function setConsultationComptePub(?ConsultationComptePub $consultationComptePub): self
    {
        $this->consultationComptePub = $consultationComptePub;

        return $this;
    }

    public function getInformationComplementaire(): ?string
    {
        return $this->informationComplementaire;
    }

    public function setInformationComplementaire(?string $informationComplementaire): self
    {
        $this->informationComplementaire = $informationComplementaire;

        return $this;
    }

    public function getAutresInformations(): ?string
    {
        return $this->autresInformations;
    }

    public function setAutresInformations(?string $autresInformations): self
    {
        $this->autresInformations = $autresInformations;

        return $this;
    }

    public function getModaliteVisiteLieuxReunion(): ?string
    {
        return $this->modaliteVisiteLieuxReunion;
    }

    public function setModaliteVisiteLieuxReunion(?string $modaliteVisiteLieuxReunion): self
    {
        $this->modaliteVisiteLieuxReunion = $modaliteVisiteLieuxReunion;

        return $this;
    }

    public function getConditionParticipation(): ?string
    {
        return $this->conditionParticipation;
    }

    public function setConditionParticipation(?string $conditionParticipation): self
    {
        $this->conditionParticipation = $conditionParticipation;

        return $this;
    }

    public function getAutrePouvoirAdjudicateur(): ?string
    {
        return $this->autrePouvoirAdjudicateur;
    }

    public function setAutrePouvoirAdjudicateur(?string $autrePouvoirAdjudicateur): self
    {
        $this->autrePouvoirAdjudicateur = $autrePouvoirAdjudicateur;

        return $this;
    }

    public function getFormuleSignature(): ?string
    {
        return $this->formuleSignature;
    }

    public function setFormuleSignature(?string $formuleSignature): self
    {
        $this->formuleSignature = $formuleSignature;

        return $this;
    }

    public function getFormePrix(): ?ArrayCollection
    {
        return $this->formePrix;
    }

    public function setFormePrix(ArrayCollection $formePrix): void
    {
        $this->formePrix = $formePrix;
    }

    public function getTranches(): Collection
    {
        return $this->tranches;
    }

    public function setTranches(Collection $tranches): void
    {
        $this->tranches = $tranches;
    }

}
