<?php

namespace App\Entity\Consultation;

use App\Repository\Consultation\NumerotationRefConsAutoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * NumerotationRefConsAuto.
 *
 * @ORM\Table(name="Numerotation_ref_cons_auto")
 * @ORM\Entity(repositoryClass=NumerotationRefConsAutoRepository::class)
 */
class NumerotationRefConsAuto
{
    /**
     *
     * @ORM\Column(type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $idAuto = null;

    /**
     * @ORM\Column(type="integer", unique=true, nullable=false)
     */
    private ?int $idConsAuto = null;

    /**
     * @ORM\Column(type="string", length=30, nullable=false)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column( type="integer", nullable=false)
     */
    private ?int $annee = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $serviceId = null;

    public function getIdAuto(): int
    {
        return $this->idAuto;
    }

    public function setIdAuto(int $idAuto): NumerotationRefConsAuto
    {
        $this->idAuto = $idAuto;

        return $this;
    }

    public function getIdConsAuto(): int
    {
        return $this->idConsAuto;
    }

    public function setIdConsAuto(int $idConsAuto): NumerotationRefConsAuto
    {
        $this->idConsAuto = $idConsAuto;

        return $this;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme): NumerotationRefConsAuto
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getAnnee(): int
    {
        return $this->annee;
    }

    public function setAnnee(int $annee): NumerotationRefConsAuto
    {
        $this->annee = $annee;

        return $this;
    }

    public function getServiceId(): ?int
    {
        return $this->serviceId;
    }

    public function setServiceId(?int $serviceId): NumerotationRefConsAuto
    {
        $this->serviceId = $serviceId;

        return $this;
    }
}
