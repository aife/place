<?php

namespace App\Entity\Consultation;

use App\Entity\Referentiel\Consultation\ClausesN2 as ReferentielClausesN2;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="consultation_clauses_n2")
 * @ORM\Entity(repositoryClass="App\Repository\Consultation\ClausesN2Repository")
 */
class ClausesN2
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation\ClausesN1", inversedBy="clausesN2")
     * @ORM\JoinColumn(nullable=false)
     */
    private ClausesN1 $clauseN1;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Consultation\ClausesN3", mappedBy="clauseN2", orphanRemoval=true)
     */
    private Collection $clausesN3;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referentiel\Consultation\ClausesN2")
     * @ORM\JoinColumn(nullable=false)
     */
    private ReferentielClausesN2 $referentielClauseN2;

    public function __construct()
    {
        $this->clausesN3 = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClauseN1(): ClausesN1
    {
        return $this->clauseN1;
    }

    public function setClauseN1(ClausesN1 $clauseN1): self
    {
        $this->clauseN1 = $clauseN1;

        return $this;
    }

    /**
     * @return Collection<int, ClausesN3>
     */
    public function getClausesN3(): Collection
    {
        return $this->clausesN3;
    }

    public function addClausesN3(ClausesN3 $clausesN3): self
    {
        if (!$this->clausesN3->contains($clausesN3)) {
            $this->clausesN3[] = $clausesN3;
            $clausesN3->setClauseN2($this);
        }

        return $this;
    }

    public function removeClausesN3(ClausesN3 $clausesN3): self
    {
        if ($this->clausesN3->removeElement($clausesN3)) {
            // set the owning side to null (unless already changed)
            if ($clausesN3->getClauseN2() === $this) {
                $clausesN3->setClauseN2(null);
            }
        }

        return $this;
    }

    public function getReferentielClauseN2(): ReferentielClausesN2
    {
        return $this->referentielClauseN2;
    }

    public function setReferentielClauseN2(ReferentielClausesN2 $referentielClauseN2): self
    {
        $this->referentielClauseN2 = $referentielClauseN2;

        return $this;
    }
}
