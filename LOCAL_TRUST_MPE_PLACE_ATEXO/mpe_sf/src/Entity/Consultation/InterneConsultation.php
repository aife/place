<?php

namespace App\Entity\Consultation;

use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Organisme;
use Doctrine\ORM\Mapping as ORM;

/**
 * InterneConsultation.
 *
 * @ORM\Table(name="InterneConsultation" )
 * @ORM\Entity(repositoryClass="App\Repository\Consultation\InterneConsultationRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class InterneConsultation
{
    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation")
     * @ORM\JoinColumn(name="consultation_id", referencedColumnName="id")
     */
    private $consultation;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="App\Entity\Agent")
     * @ORM\JoinColumn(name="interne_id", referencedColumnName="id")
     */
    private $agent;

    /**
     * @var Organisme
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private $organisme;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    public function setConsultation(Consultation $consultation)
    {
        $this->consultation = $consultation;
    }

    /**
     * @return int
     */
    public function getAgent()
    {
        return $this->agent;
    }

    public function setAgent(Agent $agent)
    {
        $this->agent = $agent;
    }

    /**
     * @return Organisme
     */
    public function getOrganisme(): Organisme
    {
        return $this->organisme;
    }

    /**
     * @param Organisme $organisme
     * @return InterneConsultation
     */
    public function setOrganisme(Organisme $organisme): InterneConsultation
    {
        $this->organisme = $organisme;

        return $this;
    }
}
