<?php

namespace App\Entity\Consultation;

use App\Entity\Referentiel\Consultation\ClausesN3 as ReferentielClausesN3;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="consultation_clauses_n3")
 * @ORM\Entity(repositoryClass="App\Repository\Consultation\ClausesN3Repository")
 */
class ClausesN3
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation\ClausesN2", inversedBy="clausesN3")
     * @ORM\JoinColumn(nullable=false)
     */
    private ClausesN2 $clauseN2;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referentiel\Consultation\ClausesN3")
     * @ORM\JoinColumn(nullable=false)
     */
    private ReferentielClausesN3 $referentielClauseN3;

    /**
     * @ORM\OneToMany(targetEntity=ClausesN4::class, mappedBy="clauseN3", orphanRemoval=true)
     */
    private Collection $clausesN4;

    public function __construct()
    {
        $this->clausesN4 = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClauseN2(): ClausesN2
    {
        return $this->clauseN2;
    }

    public function setClauseN2(ClausesN2 $clauseN2): self
    {
        $this->clauseN2 = $clauseN2;

        return $this;
    }

    public function getReferentielClauseN3(): ReferentielClausesN3
    {
        return $this->referentielClauseN3;
    }

    public function setReferentielClauseN3(ReferentielClausesN3 $referentielClauseN3): self
    {
        $this->referentielClauseN3 = $referentielClauseN3;

        return $this;
    }

    /**
     * @return Collection<int, ClausesN4>
     */
    public function getClausesN4(): Collection
    {
        return $this->clausesN4;
    }

    public function addClausesN4(ClausesN4 $clausesN4): self
    {
        if (!$this->clausesN4->contains($clausesN4)) {
            $this->clausesN4[] = $clausesN4;
            $clausesN4->setClauseN3($this);
        }

        return $this;
    }

    public function removeClausesN4(ClausesN4 $clausesN4): self
    {
        if ($this->clausesN4->removeElement($clausesN4) && ($clausesN4->getClauseN3() === $this)) {
            // set the owning side to null (unless already changed)
            $clausesN4->setClauseN3(null);
        }

        return $this;
    }
}
