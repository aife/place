<?php

namespace App\Entity\Consultation;

use App\Entity\Agent;
use App\Entity\Consultation;
use Doctrine\ORM\Mapping as ORM;

/**
 * ConsultationFavoris.
 *
 * @ORM\Table(name="consultation_favoris" )
 * @ORM\Entity(repositoryClass="App\Repository\Consultation\ConsultationFavorisRepository")
 */
class ConsultationFavoris
{
    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;


     /**
     * @var int
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation",  inversedBy="consultationFavoris")
     * @ORM\JoinColumn(name="id_consultation", referencedColumnName="id")
     */
    private $consultation;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="App\Entity\Agent",  inversedBy="consultationFavoris")
     * @ORM\JoinColumn(name="id_agent", referencedColumnName="id")
     */
    private $agent;

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    /**
     * @param int $consultation
     */
    public function setConsultation(Consultation $consultation)
    {
        $this->consultation = $consultation;
    }

    /**
     * @return int
     */
    public function getAgent()
    {
        return $this->agent;
    }

    public function setAgent(Agent $agent): ConsultationFavoris
    {
        $this->agent = $agent;

        return $this;
    }
}
