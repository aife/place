<?php

namespace App\Entity\Consultation;

use DateTimeInterface;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * AutrePieceConsultation.
 *
 * @ORM\Table(name="autre_piece_consultation" )
 * @ORM\Entity(repositoryClass="App\Repository\Consultation\AutrePieceConsultationRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class AutrePieceConsultation
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="App\Entity\BloborganismeFile")
     * @ORM\JoinColumn(name="blob_id", referencedColumnName="id")
     */
    private $blobId;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation", inversedBy="autresPieceConsultation")
     * @ORM\JoinColumn(name="consultation_id", referencedColumnName="id")
     */
    private $consultationId;

    /**
     * @ORM\Column(type="datetime", nullable = false)
     */
    private DateTimeInterface $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable = true)
     */
    private DateTimeInterface $updatedAt;

    public function __construct()
    {
        $this->createdAt = new DateTime('now');
        $this->updatedAt = new DateTime('now');
    }

    /**
     * Date de création.
     */
    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * Gets triggered only on insert.
     *
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->createdAt = new DateTime('now');
        $this->updatedAt = new DateTime('now');

        return $this;
    }

    public function getUpdatedAt(): DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * Gets triggered every time on update.
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new DateTime('now');

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getConsultationId()
    {
        return $this->consultationId;
    }

    /**
     * @param int $consultationId
     */
    public function setConsultationId($consultationId)
    {
        $this->consultationId = $consultationId;

        return $this;
    }

    /**
     * @return int
     */
    public function getBlobId()
    {
        return $this->blobId;
    }

    /**
     * @param int $blobId
     */
    public function setBlobId($blobId)
    {
        $this->blobId = $blobId;

        return $this;
    }
}
