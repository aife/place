<?php

namespace App\Entity\Consultation\Decision;

use Doctrine\ORM\Mapping as ORM;

/**
 * DecisionSelectionEntreprise.
 *
 * @ORM\Table(name="t_decision_selection_entreprise" )
 * @ORM\Entity(repositoryClass="App\Repository\Consultation\Decision\DecisionSelectionEntrepriseRepository")
 */
class DecisionSelectionEntreprise
{
    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var mixed
     * @ORM\Column(name="consultation_ref", type="integer", nullable=true)
     */
    private $consultationRef = null;

    /**
     * @var mixed
     * @ORM\Column(name="organisme", type="string", length=45,  nullable=true)
     */
    private $organisme = null;

    /**
     * @var mixed
     * @ORM\Column(name="service_id", type="integer",  nullable=true)
     */
    private $serviceId;

    /**
     * @var mixed
     * @ORM\Column(name="lot", type="integer", nullable=true)
     */
    private $lot;

    /**
     * @var mixed
     * @ORM\Column(name="id_entreprise", type="integer", nullable=true)
     */
    private $idEntreprise;

    /**
     * @var mixed
     * @ORM\Column(name="id_etablissement", type="integer", nullable=true)
     */
    private $idEtablissement;

    /**
     * @var mixed
     * @ORM\Column(name="id_offre", type="integer", nullable=true)
     */
    private $idOffre;

    /**
     * @var mixed
     * @ORM\Column(name="type_depot_reponse", type="string", length=1, nullable=true)
     */
    private $typeDepotReponse;

    /**
     * @var mixed
     * @ORM\Column(name="date_creation", type="datetime", nullable=true)
     */
    private $dateCreation;

    /**
     * @var mixed
     * @ORM\Column(name="date_modification", type="datetime", nullable=true)
     */
    private $dateModification;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="App\Entity\ContratTitulaire")
     * @ORM\JoinColumn(name="id_contact_contrat", referencedColumnName="id_contrat_titulaire", nullable=true)
     */
    private $idContratConstat;

    /**
     * @var mixed
     * @ORM\Column(name="consultation_id", type="integer", nullable=true)
     */
    private $consultationId;

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return DecisionSelectionEntreprise
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getConsultationRef()
    {
        return $this->consultationRef;
    }

    /**
     * @param mixed $consultationRef
     *
     * @return DecisionSelectionEntreprise
     */
    public function setConsultationRef($consultationRef = null)
    {
        $this->consultationRef = $consultationRef;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param mixed $organisme
     *
     * @return DecisionSelectionEntreprise
     */
    public function setOrganisme($organisme = null)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * @param mixed $serviceId
     *
     * @return DecisionSelectionEntreprise
     */
    public function setServiceId($serviceId = null)
    {
        $this->serviceId = $serviceId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLot()
    {
        return $this->lot;
    }

    /**
     * @param mixed $lot
     *
     * @return DecisionSelectionEntreprise
     */
    public function setLot($lot = null)
    {
        $this->lot = $lot;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    /**
     * @param mixed $idEntreprise
     *
     * @return DecisionSelectionEntreprise
     */
    public function setIdEntreprise($idEntreprise = null)
    {
        $this->idEntreprise = $idEntreprise;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdEtablissement()
    {
        return $this->idEtablissement;
    }

    /**
     * @param mixed $idEtablissement
     *
     * @return DecisionSelectionEntreprise
     */
    public function setIdEtablissement($idEtablissement = null)
    {
        $this->idEtablissement = $idEtablissement;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdOffre()
    {
        return $this->idOffre;
    }

    /**
     * @param mixed $idOffre
     *
     * @return DecisionSelectionEntreprise
     */
    public function setIdOffre($idOffre = null)
    {
        $this->idOffre = $idOffre;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTypeDepotReponse()
    {
        return $this->typeDepotReponse;
    }

    /**
     * @param mixed $typeDepotReponse
     *
     * @return DecisionSelectionEntreprise
     */
    public function setTypeDepotReponse($typeDepotReponse = null)
    {
        $this->typeDepotReponse = $typeDepotReponse;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param mixed $dateCreation
     *
     * @return DecisionSelectionEntreprise
     */
    public function setDateCreation($dateCreation = null)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }

    /**
     * @param mixed $dateModification
     *
     * @return DecisionSelectionEntreprise
     */
    public function setDateModification($dateModification = null)
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdContratConstat()
    {
        return $this->idContratConstat;
    }

    /**
     * @return DecisionSelectionEntreprise
     */
    public function setIdContratConstat(int $idContratConstat = null)
    {
        $this->idContratConstat = $idContratConstat;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getConsultationId()
    {
        return $this->consultationId;
    }

    /**
     * @param mixed $consultationId
     *
     * @return DecisionSelectionEntreprise
     */
    public function setConsultationId($consultationId = null)
    {
        $this->consultationId = $consultationId;

        return $this;
    }
}
