<?php

namespace App\Entity\Consultation;

use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Organisme;
use Doctrine\ORM\Mapping as ORM;

/**
 * InterneConsultationSuiviSeul.
 *
 * @ORM\Table(name="InterneConsultationSuiviSeul" )
 * @ORM\Entity(repositoryClass="App\Repository\Consultation\InterneConsultationSuiviSeulRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class InterneConsultationSuiviSeul
{
    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation")
     * @ORM\JoinColumn(name="consultation_id", referencedColumnName="id")
     */
    private $consultation;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="App\Entity\Agent")
     * @ORM\JoinColumn(name="interne_id", referencedColumnName="id")
     */
    private $agent;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    public function setConsultation(Consultation $consultation)
    {
        $this->consultation = $consultation;
    }

    /**
     * @return int
     */
    public function getAgent()
    {
        return $this->agent;
    }

    public function setAgent(Agent $agent)
    {
        $this->agent = $agent;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param Organisme $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }
}
