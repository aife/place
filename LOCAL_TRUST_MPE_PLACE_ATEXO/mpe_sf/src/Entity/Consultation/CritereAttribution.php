<?php

namespace App\Entity\Consultation;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Dto\Input\CritereAttributionInput;
use App\Dto\Output\CritereAttributionOutput;
use App\Entity\Lot;
use App\Repository\Consultation\CritereAttributionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * CritereAttribution.
 *
 * @ORM\Table(name="t_critere_attribution")
 * @ORM\Entity(repositoryClass=CritereAttributionRepository::class)
 */
#[ApiResource(
    collectionOperations: ['get', 'post'],
    itemOperations: ['get'],
    input: CritereAttributionInput::class,
    output: CritereAttributionOutput::class,
)]
class CritereAttribution
{
    /**
     * @ORM\ManyToOne(targetEntity="DonneeComplementaire", inversedBy="criteresAttribution", fetch="EAGER")
     * @ORM\JoinColumn(name="id_donnee_complementaire", referencedColumnName="id_donnee_complementaire")
     */
    private ?DonneeComplementaire $donneeComplementaire = null;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="SousCritereAttribution",mappedBy="critereAttribution", fetch="EAGER")
     * @ORM\JoinColumn(name="id_critere_attribution", referencedColumnName="id_critere_attribution")
     */
    private Collection $sousCriteres;

    /**
     *
     * @ORM\Column(name="id_critere_attribution", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="enonce", type="text", length=65535, nullable=true)
     */
    private ?string $enonce = null;

    /**
     * @ORM\Column(name="ordre", type="integer", nullable=false)
     */
    private string|int $ordre = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="ponderation", type="decimal", precision=5, scale=2, nullable=true, options={"default"="0.00"})
     */
    private $ponderation = '0.00';

    /**
     * @ORM\Column(name="id_donnee_complementaire", type="integer", nullable=true)
     */
    private ?int $idDonneeComplementaire = null;

    /**
     * @ORM\ManyToOne(targetEntity=Lot::class, inversedBy="critereAttribution")
     */
    private $lot;

    public function __construct()
    {
        $this->sousCriteres = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEnonce(): ?string
    {
        return $this->enonce;
    }

    public function setEnonce(?string $enonce): CritereAttribution
    {
        $this->enonce = $enonce;

        return $this;
    }

    public function getOrdre(): int
    {
        return $this->ordre;
    }

    public function setOrdre(int $ordre): CritereAttribution
    {
        $this->ordre = $ordre;

        return $this;
    }

    public function getPonderation(): ?string
    {
        return $this->ponderation;
    }

    public function setPonderation(?string $ponderation): CritereAttribution
    {
        $this->ponderation = $ponderation;

        return $this;
    }

    public function getIdDonneeComplementaire(): ?int
    {
        return $this->idDonneeComplementaire;
    }

    public function setIdDonneeComplementaire(?int $idDonneeComplementaire): CritereAttribution
    {
        $this->idDonneeComplementaire = $idDonneeComplementaire;

        return $this;
    }

    public function getSousCriteres(): array
    {
        return $this->sousCriteres->toArray();
    }

    /**
     * @param $sousCriteres
     */
    public function setSousCriteres($sousCriteres): CritereAttribution
    {
        $this->sousCriteres = $sousCriteres;

        return $this;
    }

    public function getSousCriteresCollection(): Collection
    {
        return $this->sousCriteres;
    }

    public function getDonneeComplementaire(): DonneeComplementaire
    {
        return $this->donneeComplementaire;
    }

    public function setDonneeComplementaire(DonneeComplementaire $donneeComplementaire): CritereAttribution
    {
        $this->donneeComplementaire = $donneeComplementaire;

        return $this;
    }

    public function getLot(): ?Lot
    {
        return $this->lot;
    }

    public function setLot(?Lot $lot): self
    {
        $this->lot = $lot;

        return $this;
    }
}
