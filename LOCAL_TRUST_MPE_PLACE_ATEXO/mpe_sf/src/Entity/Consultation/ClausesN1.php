<?php

namespace App\Entity\Consultation;

use App\Entity\Consultation;
use App\Entity\ContratTitulaire;
use App\Entity\Lot;
use App\Entity\Referentiel\Consultation\ClausesN1 as ReferentielClausesN1;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="consultation_clauses_n1")
 * @ORM\Entity(repositoryClass="App\Repository\Consultation\ClausesN1Repository")
 */
class ClausesN1
{
    public const CLAUSE_SOCIALES = 'clausesSociales';
    public const CLAUSES_ENVIRONNEMENTALES = 'clauseEnvironnementale';
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation", inversedBy="clausesN1")
     */
    private ?Consultation $consultation = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Lot", inversedBy="clausesN1")
     */
    private ?Lot $lot = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ContratTitulaire", inversedBy="clausesN1")
     * @ORM\JoinColumn(name="contrat_id", referencedColumnName="id_contrat_titulaire")
     */
    private ?ContratTitulaire $contrat = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Consultation\ClausesN2", mappedBy="clauseN1", orphanRemoval=true)
     */
    private Collection $clausesN2;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referentiel\Consultation\ClausesN1")
     * @ORM\JoinColumn(nullable=false)
     */
    private ReferentielClausesN1 $referentielClauseN1;

    public function __construct()
    {
        $this->clausesN2 = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getConsultation(): ?Consultation
    {
        return $this->consultation;
    }

    public function setConsultation(?Consultation $consultation): self
    {
        $this->consultation = $consultation;

        return $this;
    }

    public function getLot(): ?Lot
    {
        return $this->lot;
    }

    public function setLot(?Lot $lot): self
    {
        $this->lot = $lot;

        return $this;
    }

    /**
     * @return Collection<int, ClausesN2>
     */
    public function getClausesN2(): Collection
    {
        return $this->clausesN2;
    }

    public function addClausesN2(ClausesN2 $clausesN2): self
    {
        if (!$this->clausesN2->contains($clausesN2)) {
            $this->clausesN2[] = $clausesN2;
            $clausesN2->setClauseN1($this);
        }

        return $this;
    }

    public function removeClausesN2(ClausesN2 $clausesN2): self
    {
        if ($this->clausesN2->removeElement($clausesN2)) {
            // set the owning side to null (unless already changed)
            if ($clausesN2->getClauseN1() === $this) {
                $clausesN2->setClauseN1(null);
            }
        }

        return $this;
    }

    public function getReferentielClauseN1(): ReferentielClausesN1
    {
        return $this->referentielClauseN1;
    }

    public function setReferentielClauseN1(ReferentielClausesN1 $referentielClauseN1): self
    {
        $this->referentielClauseN1 = $referentielClauseN1;

        return $this;
    }

    public function getContrat(): ?ContratTitulaire
    {
        return $this->contrat;
    }

    public function setContrat(?ContratTitulaire $contrat): self
    {
        $this->contrat = $contrat;

        return $this;
    }
}
