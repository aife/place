<?php

namespace App\Entity\Consultation;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConsultationComptePub.
 *
 * @ORM\Table(name="t_consultation_compte_pub", indexes={@ORM\Index(name="id_donnees_complementaires",
 *     columns={"id_donnees_complementaires"}), @ORM\Index(name="id_compte_pub", columns={"id_compte_pub"})})
 * @ORM\Entity
 */
class ConsultationComptePub
{
    /**
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="id_donnees_complementaires", type="integer", nullable=false)
     */
    private ?int $idDonneesComplementaires = null;

    /**
     * @ORM\Column(name="organisme", type="string", length=30, nullable=false)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(name="id_compte_pub", type="integer", nullable=false)
     */
    private ?int $idComptePub = null;

    /**
     * @ORM\Column(name="boamp_login", type="string", length=100, nullable=false)
     */
    private string $boampLogin = '';

    /**
     * @ORM\Column(name="boamp_password", type="string", length=100, nullable=false)
     */
    private string $boampPassword = '';

    /**
     * @ORM\Column(name="boamp_mail", type="string", length=100, nullable=false)
     */
    private string $boampMail = '';

    /**
     * @ORM\Column(name="boamp_target", type="string", length=1, nullable=false, options={"fixed"=true})
     */
    private string $boampTarget = '0';

    /**
     * @ORM\Column(name="denomination", type="string", length=100, nullable=false)
     */
    private string $denomination = '';

    /**
     * @ORM\Column(name="prm", type="string", length=100, nullable=false)
     */
    private string $prm = '';

    /**
     * @ORM\Column(name="adresse", type="string", length=100, nullable=false)
     */
    private string $adresse = '';

    /**
     * @ORM\Column(name="cp", type="string", length=5, nullable=false)
     */
    private string $cp = '';

    /**
     * @ORM\Column(name="ville", type="string", length=100, nullable=false)
     */
    private string $ville = '';

    /**
     * @ORM\Column(name="url", type="string", length=100, nullable=false)
     */
    private string $url = '';

    /**
     * @ORM\Column(name="facture_denomination", type="string", length=100, nullable=true)
     */
    private ?string $factureDenomination = null;

    /**
     * @ORM\Column(name="facture_adresse", type="string", length=255, nullable=false)
     */
    private string $factureAdresse = '';

    /**
     * @ORM\Column(name="facture_cp", type="string", length=10, nullable=false)
     */
    private string $factureCp = '';

    /**
     * @ORM\Column(name="facture_ville", type="string", length=100, nullable=false)
     */
    private string $factureVille = '';

    /**
     * @ORM\Column(name="instance_recours_organisme", type="string", length=200, nullable=true)
     */
    private ?string $instanceRecoursOrganisme = null;

    /**
     * @ORM\Column(name="instance_recours_adresse", type="string", length=200, nullable=true)
     */
    private ?string $instanceRecoursAdresse = null;

    /**
     * @ORM\Column(name="instance_recours_cp", type="string", length=200, nullable=true)
     */
    private ?string $instanceRecoursCp = null;

    /**
     * @ORM\Column(name="instance_recours_ville", type="string", length=200, nullable=true)
     */
    private ?string $instanceRecoursVille = null;

    /**
     * @ORM\Column(name="instance_recours_url", type="text", length=65535, nullable=true)
     */
    private ?string $instanceRecoursUrl = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $email = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $telephone = null;

    /**
     * @ORM\OneToOne(targetEntity=DonneeComplementaire::class, inversedBy="consultationComptePub")
     * @ORM\JoinColumn(name="id_donnees_complementaires", referencedColumnName="id_donnee_complementaire")
     */
    private ?DonneeComplementaire $donneeComplementaire = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getIdDonneesComplementaires(): int
    {
        return $this->idDonneesComplementaires;
    }

    public function setIdDonneesComplementaires(int $idDonneesComplementaires): ConsultationComptePub
    {
        $this->idDonneesComplementaires = $idDonneesComplementaires;

        return $this;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme): ConsultationComptePub
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getIdComptePub(): int
    {
        return $this->idComptePub;
    }

    public function setIdComptePub(int $idComptePub): ConsultationComptePub
    {
        $this->idComptePub = $idComptePub;

        return $this;
    }

    public function getBoampLogin(): string
    {
        return $this->boampLogin;
    }

    public function setBoampLogin(string $boampLogin): ConsultationComptePub
    {
        $this->boampLogin = $boampLogin;

        return $this;
    }

    public function getBoampPassword(): string
    {
        return $this->boampPassword;
    }

    public function setBoampPassword(string $boampPassword): ConsultationComptePub
    {
        $this->boampPassword = $boampPassword;

        return $this;
    }

    public function getBoampMail(): string
    {
        return $this->boampMail;
    }

    public function setBoampMail(string $boampMail): ConsultationComptePub
    {
        $this->boampMail = $boampMail;

        return $this;
    }

    public function getBoampTarget(): string
    {
        return $this->boampTarget;
    }

    public function setBoampTarget(string $boampTarget): ConsultationComptePub
    {
        $this->boampTarget = $boampTarget;

        return $this;
    }

    public function getDenomination(): string
    {
        return $this->denomination;
    }

    public function setDenomination(string $denomination): ConsultationComptePub
    {
        $this->denomination = $denomination;

        return $this;
    }

    public function getPrm(): string
    {
        return $this->prm;
    }

    public function setPrm(string $prm): ConsultationComptePub
    {
        $this->prm = $prm;

        return $this;
    }

    public function getAdresse(): string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): ConsultationComptePub
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCp(): string
    {
        return $this->cp;
    }

    public function setCp(string $cp): ConsultationComptePub
    {
        $this->cp = $cp;

        return $this;
    }

    public function getVille(): string
    {
        return $this->ville;
    }

    public function setVille(string $ville): ConsultationComptePub
    {
        $this->ville = $ville;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): ConsultationComptePub
    {
        $this->url = $url;

        return $this;
    }

    public function getFactureDenomination(): ?string
    {
        return $this->factureDenomination;
    }

    public function setFactureDenomination(?string $factureDenomination): ConsultationComptePub
    {
        $this->factureDenomination = $factureDenomination;

        return $this;
    }

    public function getFactureAdresse(): string
    {
        return $this->factureAdresse;
    }

    public function setFactureAdresse(string $factureAdresse): ConsultationComptePub
    {
        $this->factureAdresse = $factureAdresse;

        return $this;
    }

    public function getFactureCp(): string
    {
        return $this->factureCp;
    }

    public function setFactureCp(string $factureCp): ConsultationComptePub
    {
        $this->factureCp = $factureCp;

        return $this;
    }

    public function getFactureVille(): string
    {
        return $this->factureVille;
    }

    public function setFactureVille(string $factureVille): ConsultationComptePub
    {
        $this->factureVille = $factureVille;

        return $this;
    }

    public function getInstanceRecoursOrganisme(): ?string
    {
        return $this->instanceRecoursOrganisme;
    }

    public function setInstanceRecoursOrganisme(?string $instanceRecoursOrganisme): ConsultationComptePub
    {
        $this->instanceRecoursOrganisme = $instanceRecoursOrganisme;

        return $this;
    }

    public function getInstanceRecoursAdresse(): ?string
    {
        return $this->instanceRecoursAdresse;
    }

    public function setInstanceRecoursAdresse(?string $instanceRecoursAdresse): ConsultationComptePub
    {
        $this->instanceRecoursAdresse = $instanceRecoursAdresse;

        return $this;
    }

    public function getInstanceRecoursCp(): ?string
    {
        return $this->instanceRecoursCp;
    }

    public function setInstanceRecoursCp(?string $instanceRecoursCp): ConsultationComptePub
    {
        $this->instanceRecoursCp = $instanceRecoursCp;

        return $this;
    }

    public function getInstanceRecoursVille(): ?string
    {
        return $this->instanceRecoursVille;
    }

    public function setInstanceRecoursVille(?string $instanceRecoursVille): ConsultationComptePub
    {
        $this->instanceRecoursVille = $instanceRecoursVille;

        return $this;
    }

    public function getInstanceRecoursUrl(): ?string
    {
        return $this->instanceRecoursUrl;
    }

    public function setInstanceRecoursUrl(?string $instanceRecoursUrl): ConsultationComptePub
    {
        $this->instanceRecoursUrl = $instanceRecoursUrl;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): ConsultationComptePub
    {
        $this->email = $email;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): ConsultationComptePub
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getDonneeComplementaire(): ?DonneeComplementaire
    {
        return $this->donneeComplementaire;
    }

    public function setDonneeComplementaire(?DonneeComplementaire $donneeComplementaire): self
    {
        $this->donneeComplementaire = $donneeComplementaire;

        return $this;
    }
}
