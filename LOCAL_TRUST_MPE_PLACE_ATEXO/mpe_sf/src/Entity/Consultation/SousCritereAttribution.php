<?php

namespace App\Entity\Consultation;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Dto\Input\SousCritereAttributionInput;
use App\Dto\Output\SousCritereAttributionOutput;
use App\Repository\Consultation\SousCritereAttributionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * SousCritereAttribution.
 *
 * @ORM\Table(name="t_sous_critere_attribution")
 * @ORM\Entity(repositoryClass=SousCritereAttributionRepository::class)
 */
#[ApiResource(
    collectionOperations: ['get', 'post'],
    itemOperations: ['get'],
    input: SousCritereAttributionInput::class,
    output: SousCritereAttributionOutput::class,
)]
class SousCritereAttribution
{
    /**
     * @ORM\ManyToOne(targetEntity="CritereAttribution", inversedBy="sousCriteres", fetch="EAGER")
     * @ORM\JoinColumn(name="id_critere_attribution", referencedColumnName="id_critere_attribution")
     */
    private ?CritereAttribution $critereAttribution = null;

    /**
     *
     * @ORM\Column(name="id_sous_critere_attribution", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $idSousCritereAttribution;

    /**
     * @ORM\Column(name="enonce", type="text", length=65535, nullable=true)
     */
    private ?string $enonce = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ponderation", type="decimal", precision=5, scale=2, nullable=true, options={"default"="0.00"})
     */
    private $ponderation = '0.00';

    /**
     * @ORM\Column(name="id_critere_attribution", type="integer", nullable=true)
     */
    private ?int $idCritereAttribution = null;

    public function getIdSousCritereAttribution(): int
    {
        return $this->idSousCritereAttribution;
    }

    public function getEnonce(): ?string
    {
        return $this->enonce;
    }

    public function setEnonce(?string $enonce): SousCritereAttribution
    {
        $this->enonce = $enonce;

        return $this;
    }

    public function getPonderation(): ?string
    {
        return $this->ponderation;
    }

    public function setPonderation(?string $ponderation): SousCritereAttribution
    {
        $this->ponderation = $ponderation;

        return $this;
    }

    public function getIdCritereAttribution(): ?int
    {
        return $this->idCritereAttribution;
    }

    public function setIdCritereAttribution(?int $idCritereAttribution): SousCritereAttribution
    {
        $this->idCritereAttribution = $idCritereAttribution;

        return $this;
    }

    public function getCritereAttribution(): CritereAttribution
    {
        return $this->critereAttribution;
    }

    public function setCritereAttribution(CritereAttribution $critereAttribution): SousCritereAttribution
    {
        $this->critereAttribution = $critereAttribution;

        return $this;
    }
}
