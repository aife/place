<?php

namespace App\Entity\Consultation;

use App\Entity\Consultation\ClausesN3 as ClausesN3;
use App\Entity\Referentiel\Consultation\ClausesN4 as ReferentielClausesN4;
use App\Repository\Consultation\ClausesN4Repository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="consultation_clauses_n4")
 * @ORM\Entity(repositoryClass=ClausesN4Repository::class)
 */
class ClausesN4
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $valeur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation\ClausesN3", inversedBy="clausesN4")
     * @ORM\JoinColumn(nullable=false)
     */
    private ClausesN3 $clauseN3;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referentiel\Consultation\ClausesN4")
     * @ORM\JoinColumn(nullable=false)
     */
    private ReferentielClausesN4 $referentielClauseN4;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValeur(): ?string
    {
        return $this->valeur;
    }

    public function setValeur(?string $valeur): self
    {
        $this->valeur = $valeur;

        return $this;
    }

    public function getClauseN3(): ?ClausesN3
    {
        return $this->clauseN3;
    }

    public function setClauseN3(?ClausesN3 $clauseN3): self
    {
        $this->clauseN3 = $clauseN3;

        return $this;
    }

    public function getReferentielClausesN4(): ?ReferentielClausesN4
    {
        return $this->referentielClauseN4;
    }

    public function setReferentielClausesN4(?ReferentielClausesN4 $referentielClauseN4): self
    {
        $this->referentielClauseN4 = $referentielClauseN4;

        return $this;
    }
}
