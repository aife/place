<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * TBourseCotraitance.
 *
 * @ORM\Table(name="t_bourse_cotraitance")
 * @ORM\Entity(repositoryClass="App\Repository\TBourseCotraitanceRepository")
 */
class TBourseCotraitance
{
    /**
     *
     * @ORM\Column(name="id_auto_BC", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation", inversedBy="bourses", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="reference_consultation", referencedColumnName="id")
     */
    private ?Consultation $consultation = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Entreprise", inversedBy="bourses", fetch="EAGER")
     * @ORM\JoinColumn(name="id_entreprise", referencedColumnName="id")
     */
    private ?Entreprise $entreprise = null;

    /**
     * @ORM\Column(name="id_etablissement_inscrite", type="integer", nullable=false)
     */
    private ?int $etablissementId = null;

    /**
     * @ORM\Column(name="nom_inscrit", type="string", length=255, nullable=false)
     */
    private ?string $nomInscrit = null;

    /**
     * @ORM\Column(name="prenom_inscrit", type="string", length=255, nullable=false)
     */
    private ?string $prenomInscrit = null;

    /**
     * @ORM\Column(name="adresse_inscrit", type="string", length=255, nullable=false)
     */
    private ?string $adresseInscrit = null;

    /**
     * @ORM\Column(name="adresse2_incsrit", type="string", length=255)
     */
    private string $adresse2Incsrit = '';

    /**
     * @ORM\Column(name="cp_inscrit", type="string", length=255, nullable=false)
     */
    private ?string $cpInscrit = null;

    /**
     * @ORM\Column(name="ville_inscrit", type="string", length=255, nullable=false)
     */
    private ?string $villeInscrit = null;

    /**
     * @ORM\Column(name="pays_inscrit", type="string", length=255)
     */
    private string $paysInscrit = '';

    /**
     * @ORM\Column(name="fonction_inscrit", type="string", length=255, nullable=false)
     */
    private ?string $fonctionInscrit = null;

    /**
     * @ORM\Column(name="email_inscrit", type="string", length=255, nullable=false)
     */
    private ?string $emailInscrit = null;

    /**
     * @ORM\Column(name="tel_fixe_inscrit", type="string", length=255)
     */
    private ?string $telFixeInscrit = '';

    /**
     * @ORM\Column(name="tel_mobile_inscrit", type="string", length=255)
     */
    private ?string $telMobileInscrit = '';

    /**
     * @ORM\Column(name="mandataire_groupement", type="integer", nullable=false)
     */
    private ?int $mandataireGroupement = null;

    /**
     * @ORM\Column(name="cotraitant_conjoint", type="integer", nullable=false)
     */
    private ?int $cotraitantConjoint = null;

    /**
     * @ORM\Column(name="cotraitant_solidaire", type="integer", nullable=false)
     */
    private ?int $cotraitantSolidaire = null;

    /**
     * @ORM\Column(name="desc_mon_apport_marche", type="text")
     */
    private string $descMonApportMarche = '';

    /**
     * @ORM\Column(name="desc_type_cotraitance_recherche", type="text")
     */
    private string $descTypeCotraitanceRecherche = '';

    /**
     * @ORM\Column(name="clause_social", type="integer", nullable=false)
     */
    private ?int $clauseSocial = null;

    /**
     * @ORM\Column(name="entreprise_adapte", type="integer", nullable=false)
     */
    private ?int $entrepriseAdapte = null;

    /**
     * @ORM\Column(name="long", type="float")
     */
    private int|float|null $long = 0;

    /**
     * @ORM\Column(name="lat", type="float")
     */
    private int|float|null $lat = 0;

    /**
     * @ORM\Column(name="maj_long_lat", type="datetime")
     */
    private string|DateTimeInterface|int|null $majLongLat = '';

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getEtablissementId()
    {
        return $this->etablissementId;
    }

    /**
     * @param int $etablissementId
     */
    public function setEtablissementId($etablissementId)
    {
        $this->etablissementId = $etablissementId;
    }

    /**
     * @return string
     */
    public function getNomInscrit()
    {
        return $this->nomInscrit;
    }

    /**
     * @param string $nomInscrit
     */
    public function setNomInscrit($nomInscrit)
    {
        $this->nomInscrit = $nomInscrit;
    }

    /**
     * @return string
     */
    public function getPrenomInscrit()
    {
        return $this->prenomInscrit;
    }

    /**
     * @param string $prenomInscrit
     */
    public function setPrenomInscrit($prenomInscrit)
    {
        $this->prenomInscrit = $prenomInscrit;
    }

    /**
     * @return string
     */
    public function getAdresseInscrit()
    {
        return $this->adresseInscrit;
    }

    /**
     * @param string $adresseInscrit
     */
    public function setAdresseInscrit($adresseInscrit)
    {
        $this->adresseInscrit = $adresseInscrit;
    }

    /**
     * @return string
     */
    public function getAdresse2Incsrit()
    {
        return $this->adresse2Incsrit;
    }

    /**
     * @param string $adresse2Incsrit
     */
    public function setAdresse2Incsrit($adresse2Incsrit)
    {
        $this->adresse2Incsrit = $adresse2Incsrit;
    }

    /**
     * @return string
     */
    public function getCpInscrit()
    {
        return $this->cpInscrit;
    }

    /**
     * @param string $cpInscrit
     */
    public function setCpInscrit($cpInscrit)
    {
        $this->cpInscrit = $cpInscrit;
    }

    /**
     * @return string
     */
    public function getVilleInscrit()
    {
        return $this->villeInscrit;
    }

    /**
     * @param string $villeInscrit
     */
    public function setVilleInscrit($villeInscrit)
    {
        $this->villeInscrit = $villeInscrit;
    }

    /**
     * @return string
     */
    public function getPaysInscrit()
    {
        return $this->paysInscrit;
    }

    /**
     * @param string $paysInscrit
     */
    public function setPaysInscrit($paysInscrit)
    {
        $this->paysInscrit = $paysInscrit;
    }

    /**
     * @return string
     */
    public function getFonctionInscrit()
    {
        return $this->fonctionInscrit;
    }

    /**
     * @param string $fonctionInscrit
     */
    public function setFonctionInscrit($fonctionInscrit)
    {
        $this->fonctionInscrit = $fonctionInscrit;
    }

    /**
     * @return string
     */
    public function getEmailInscrit()
    {
        return $this->emailInscrit;
    }

    /**
     * @param string $emailInscrit
     */
    public function setEmailInscrit($emailInscrit)
    {
        $this->emailInscrit = $emailInscrit;
    }

    /**
     * @return string
     */
    public function getTelFixeInscrit()
    {
        return $this->telFixeInscrit;
    }

    /**
     * @param string $telFixeInscrit
     */
    public function setTelFixeInscrit($telFixeInscrit)
    {
        $this->telFixeInscrit = $telFixeInscrit;
    }

    /**
     * @return string
     */
    public function getTelMobileInscrit()
    {
        return $this->telMobileInscrit;
    }

    /**
     * @param string $telMobileInscrit
     */
    public function setTelMobileInscrit($telMobileInscrit)
    {
        $this->telMobileInscrit = $telMobileInscrit;
    }

    /**
     * @return int
     */
    public function getMandataireGroupement()
    {
        return $this->mandataireGroupement;
    }

    /**
     * @param int $mandataireGroupement
     */
    public function setMandataireGroupement($mandataireGroupement)
    {
        $this->mandataireGroupement = $mandataireGroupement;
    }

    /**
     * @return int
     */
    public function getCotraitantConjoint()
    {
        return $this->cotraitantConjoint;
    }

    /**
     * @param int $cotraitantConjoint
     */
    public function setCotraitantConjoint($cotraitantConjoint)
    {
        $this->cotraitantConjoint = $cotraitantConjoint;
    }

    /**
     * @return int
     */
    public function getCotraitantSolidaire()
    {
        return $this->cotraitantSolidaire;
    }

    /**
     * @param int $cotraitantSolidaire
     */
    public function setCotraitantSolidaire($cotraitantSolidaire)
    {
        $this->cotraitantSolidaire = $cotraitantSolidaire;
    }

    /**
     * @return string
     */
    public function getDescMonApportMarche()
    {
        return $this->descMonApportMarche;
    }

    /**
     * @param string $descMonApportMarche
     */
    public function setDescMonApportMarche($descMonApportMarche)
    {
        $this->descMonApportMarche = $descMonApportMarche;
    }

    /**
     * @return string
     */
    public function getDescTypeCotraitanceRecherche()
    {
        return $this->descTypeCotraitanceRecherche;
    }

    /**
     * @param string $descTypeCotraitanceRecherche
     */
    public function setDescTypeCotraitanceRecherche($descTypeCotraitanceRecherche)
    {
        $this->descTypeCotraitanceRecherche = $descTypeCotraitanceRecherche;
    }

    /**
     * @return int
     */
    public function getClauseSocial()
    {
        return $this->clauseSocial;
    }

    /**
     * @param int $clauseSocial
     */
    public function setClauseSocial($clauseSocial)
    {
        $this->clauseSocial = $clauseSocial;
    }

    /**
     * @return int
     */
    public function getEntrepriseAdapte()
    {
        return $this->entrepriseAdapte;
    }

    /**
     * @param int $entrepriseAdapte
     */
    public function setEntrepriseAdapte($entrepriseAdapte)
    {
        $this->entrepriseAdapte = $entrepriseAdapte;
    }

    /**
     * @return int
     */
    public function getLong()
    {
        return $this->long;
    }

    /**
     * @param int $long
     */
    public function setLong($long)
    {
        $this->long = $long;
    }

    /**
     * @return int
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param int $lat
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * @return int
     */
    public function getMajLongLat()
    {
        return $this->majLongLat;
    }

    /**
     * @param int $majLongLat
     */
    public function setMajLongLat($majLongLat)
    {
        $this->majLongLat = $majLongLat;
    }

    public function setConsultation(Consultation $consultation)
    {
        $this->consultation = $consultation;
        $this->organisme = $consultation->getAcronymeOrg();
    }

    /**
     * @return Consultation
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    public function setEntreprise(Entreprise $entreprise)
    {
        $this->entreprise = $entreprise;
    }

    /**
     * @return Entreprise
     */
    public function getEntreprise(): Entreprise
    {
        return $this->entreprise;
    }
}
