<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class SsoAgent.
 *
 * @ORM\Table(name="sso_agent")
 * @ORM\Entity(repositoryClass="App\Repository\SsoAgentRepository")
 */
class SsoAgent
{
    /**
     *
     * @ORM\Column(name="id_sso", type="string", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private ?string $idSso = null;

    /**
     * @ORM\Column(name="id_agent", type="integer", nullable=false)
     */
    private ?int $idAgent = null;

    /**
     * @ORM\Column(name="organisme", type="string", nullable=false)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(name="service_id", type="integer", nullable=false)
     */
    private ?int $serviceId = null;

    /**
     * @ORM\Column(name="date_connexion", type="string", nullable=false)
     */
    private ?string $dateConnexion = null;

    /**
     * @ORM\Column(name="date_last_request", type="string", nullable=false)
     */
    private ?string $dateLastRequest = null;

    public function getIdSso(): string
    {
        return $this->idSso;
    }

    public function setIdSso(string $idSso)
    {
        $this->idSso = $idSso;
    }

    public function getIdAgent(): int
    {
        return $this->idAgent;
    }

    public function setIdAgent(int $idAgent)
    {
        $this->idAgent = $idAgent;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme)
    {
        $this->organisme = $organisme;
    }

    public function getServiceId(): int
    {
        return $this->serviceId;
    }

    public function setServiceId(int $serviceId)
    {
        $this->serviceId = $serviceId;
    }

    public function getDateConnexion(): string
    {
        return $this->dateConnexion;
    }

    public function setDateConnexion(string $dateConnexion)
    {
        $this->dateConnexion = $dateConnexion;
    }

    public function getDateLastRequest(): string
    {
        return $this->dateLastRequest;
    }

    public function setDateLastRequest(string $dateLastRequest)
    {
        $this->dateLastRequest = $dateLastRequest;
    }
}
