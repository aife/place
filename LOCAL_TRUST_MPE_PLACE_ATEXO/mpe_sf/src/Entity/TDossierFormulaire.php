<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TDossierFormulaire.
 *
 * @ORM\Table(name="t_dossier_formulaire", indexes={@ORM\Index(name="t_reponse_elec_form_t_dossier_form_id_reponse_elec_form_fk", columns={"id_reponse_elec_formulaire"})})
 * @ORM\Entity
 */
class TDossierFormulaire
{
    /**
     *
     * @ORM\Column(name="id_dossier_formulaire", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $idDossierFormulaire = null;

    /**
     * @ORM\Column(name="id_lot", type="integer", nullable=true)
     */
    private ?int $idLot = null;

    /**
     * @ORM\Column(name="type_enveloppe", type="integer", nullable=false)
     */
    private ?int $typeEnveloppe = null;

    /**
     * @ORM\Column(name="libelle_forrmulaire", type="string", length=255, nullable=false)
     */
    private ?string $libelleForrmulaire = null;

    /**
     * @ORM\Column(name="cle_externe_dispositif", type="integer", nullable=false)
     */
    private ?int $cleExterneDispositif = null;

    /**
     * @ORM\Column(name="cle_externe_dossier", type="integer", nullable=true)
     */
    private ?int $cleExterneDossier = null;

    /**
     * @ORM\Column(name="statut_validation", type="string", nullable=false)
     */
    private ?string $statutValidation = null;

    /**
     * @ORM\Column(name="date_creation", type="string", length=255, nullable=true)
     */
    private ?string $dateCreation = null;

    /**
     * @ORM\Column(name="date_modif", type="string", length=255, nullable=true)
     */
    private ?string $dateModif = null;

    /**
     * @ORM\Column(name="date_validation", type="string", length=255, nullable=true)
     */
    private ?string $dateValidation = null;

    /**
     * @ORM\Column(name="statut_generation_globale", type="integer", nullable=true)
     */
    private ?int $statutGenerationGlobale = null;

    /**
     * @var int
     *
     * @ORM\Column(name="type_reponse", type="integer", nullable=true)
     */
    private $typeReponse = '1';

    /**
     * @ORM\Column(name="cle_externe_formulaire", type="integer", nullable=true)
     */
    private ?int $cleExterneFormulaire = null;

    /**
     * @ORM\Column(name="formulaire_depose", type="string", nullable=false)
     */
    private string $formulaireDepose = '0';

    /**
     * @ORM\Column(name="id_dossier_pere", type="integer", nullable=true)
     */
    private ?int $idDossierPere = null;

    /**
     * @ORM\Column(name="reference_dossier_sub", type="string", length=255, nullable=true)
     */
    private ?string $referenceDossierSub = null;

    /**
     *
     * @ORM\ManyToOne(targetEntity="TReponseElecFormulaire", inversedBy="tdossierformulaires")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_reponse_elec_formulaire", referencedColumnName="id_reponse_elec_formulaire")
     * })
     */
    private ?TReponseElecFormulaire $tReponseElecFormulaire = null;

    public function __construct()
    {
        $this->tEditionFormulaires = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getIdDossierFormulaire()
    {
        return $this->idDossierFormulaire;
    }

    /**
     * @param int $idDossierFormulaire
     */
    public function setIdDossierFormulaire($idDossierFormulaire)
    {
        $this->idDossierFormulaire = $idDossierFormulaire;
    }

    /**
     * @return int
     */
    public function getIdLot()
    {
        return $this->idLot;
    }

    /**
     * @param int $idLot
     */
    public function setIdLot($idLot)
    {
        $this->idLot = $idLot;
    }

    /**
     * @return int
     */
    public function getTypeEnveloppe()
    {
        return $this->typeEnveloppe;
    }

    /**
     * @param int $typeEnveloppe
     */
    public function setTypeEnveloppe($typeEnveloppe)
    {
        $this->typeEnveloppe = $typeEnveloppe;
    }

    /**
     * @return string
     */
    public function getLibelleForrmulaire()
    {
        return $this->libelleForrmulaire;
    }

    /**
     * @param string $libelleForrmulaire
     */
    public function setLibelleForrmulaire($libelleForrmulaire)
    {
        $this->libelleForrmulaire = $libelleForrmulaire;
    }

    /**
     * @return int
     */
    public function getCleExterneDispositif()
    {
        return $this->cleExterneDispositif;
    }

    /**
     * @param int $cleExterneDispositif
     */
    public function setCleExterneDispositif($cleExterneDispositif)
    {
        $this->cleExterneDispositif = $cleExterneDispositif;
    }

    /**
     * @return int
     */
    public function getCleExterneDossier()
    {
        return $this->cleExterneDossier;
    }

    /**
     * @param int $cleExterneDossier
     */
    public function setCleExterneDossier($cleExterneDossier)
    {
        $this->cleExterneDossier = $cleExterneDossier;
    }

    /**
     * @return string
     */
    public function getStatutValidation()
    {
        return $this->statutValidation;
    }

    /**
     * @param string $statutValidation
     */
    public function setStatutValidation($statutValidation)
    {
        $this->statutValidation = $statutValidation;
    }

    /**
     * @return string
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param string $dateCreation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }

    /**
     * @return string
     */
    public function getDateModif()
    {
        return $this->dateModif;
    }

    /**
     * @param string $dateModif
     */
    public function setDateModif($dateModif)
    {
        $this->dateModif = $dateModif;
    }

    /**
     * @return string
     */
    public function getDateValidation()
    {
        return $this->dateValidation;
    }

    /**
     * @param string $dateValidation
     */
    public function setDateValidation($dateValidation)
    {
        $this->dateValidation = $dateValidation;
    }

    /**
     * @return int
     */
    public function getStatutGenerationGlobale()
    {
        return $this->statutGenerationGlobale;
    }

    /**
     * @param int $statutGenerationGlobale
     */
    public function setStatutGenerationGlobale($statutGenerationGlobale)
    {
        $this->statutGenerationGlobale = $statutGenerationGlobale;
    }

    /**
     * @return int
     */
    public function getTypeReponse()
    {
        return $this->typeReponse;
    }

    /**
     * @param int $typeReponse
     */
    public function setTypeReponse($typeReponse)
    {
        $this->typeReponse = $typeReponse;
    }

    /**
     * @return int
     */
    public function getCleExterneFormulaire()
    {
        return $this->cleExterneFormulaire;
    }

    /**
     * @param int $cleExterneFormulaire
     */
    public function setCleExterneFormulaire($cleExterneFormulaire)
    {
        $this->cleExterneFormulaire = $cleExterneFormulaire;
    }

    /**
     * @return string
     */
    public function getFormulaireDepose()
    {
        return $this->formulaireDepose;
    }

    /**
     * @param string $formulaireDepose
     */
    public function setFormulaireDepose($formulaireDepose)
    {
        $this->formulaireDepose = $formulaireDepose;
    }

    /**
     * @return int
     */
    public function getIdDossierPere()
    {
        return $this->idDossierPere;
    }

    /**
     * @param int $idDossierPere
     */
    public function setIdDossierPere($idDossierPere)
    {
        $this->idDossierPere = $idDossierPere;
    }

    /**
     * @return string
     */
    public function getReferenceDossierSub()
    {
        return $this->referenceDossierSub;
    }

    /**
     * @param string $referenceDossierSub
     */
    public function setReferenceDossierSub($referenceDossierSub)
    {
        $this->referenceDossierSub = $referenceDossierSub;
    }

    /**
     * @return TReponseElecFormulaire
     */
    public function getTReponseElecFormulaire()
    {
        return $this->tReponseElecFormulaire;
    }

    /**
     * @param TReponseElecFormulaire $tReponseElecFormulaire
     */
    public function setTReponseElecFormulaire($tReponseElecFormulaire)
    {
        $this->tReponseElecFormulaire = $tReponseElecFormulaire;
    }

    /**
     * @return mixed
     */
    public function getIdReponseElecFormulaire()
    {
        return $this->idReponseElecFormulaire;
    }

    /**
     * @param mixed $idReponseElecFormulaire
     */
    public function setIdReponseElecFormulaire($idReponseElecFormulaire)
    {
        $this->idReponseElecFormulaire = $idReponseElecFormulaire;
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id_reponse_elec_formulaire", type="integer", nullable=true)
     */
    private $idReponseElecFormulaire;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="TEditionFormulaire", mappedBy="tDossierFormulaire")
     * @ORM\JoinColumn(name="id_dossier_formulaire", referencedColumnName="id_dossier_formulaire")
     */
    private Collection $tEditionFormulaires;

    /**
     * @return ArrayCollection
     */
    public function getTEditionFormulaires()
    {
        return $this->tEditionFormulaires;
    }

    /**
     * @param ArrayCollection $tEditionFormulaires
     */
    public function setTEditionFormulaires($tEditionFormulaires)
    {
        $this->tEditionFormulaires = $tEditionFormulaires;
    }
}
