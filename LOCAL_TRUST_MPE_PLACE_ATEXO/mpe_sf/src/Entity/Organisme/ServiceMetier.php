<?php

namespace App\Entity\Organisme;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="Service_Mertier")
 * @ORM\Entity(repositoryClass="App\Repository\Organisme\ServiceMetierRepository")
 */
class ServiceMetier
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default":0})
     * @ORM\ManyToOne(targetEntity="App\Entity\Agent")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id = 0;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private ?string $sigle = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $denomination = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $urlAcces = null;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $logo = null;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $urlDeconnexion = null;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     */
    private int $ordre = 0;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id = 0)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getSigle()
    {
        return $this->sigle;
    }

    public function setSigle(string $sigle = null)
    {
        $this->sigle = $sigle;
    }

    /**
     * @return string
     */
    public function getDenomination()
    {
        return $this->denomination;
    }

    public function setDenomination(string $denomination = null)
    {
        $this->denomination = $denomination;
    }

    /**
     * @return string
     */
    public function getUrlAcces()
    {
        return $this->urlAcces;
    }

    public function setUrlAcces(string $urlAcces = null)
    {
        $this->urlAcces = $urlAcces;
    }

    public function getUrlDeconnexion(): string
    {
        return $this->urlDeconnexion;
    }

    public function setUrlDeconnexion(string $urlDeconnexion)
    {
        $this->urlDeconnexion = $urlDeconnexion;
    }

    public function getLogo(): string
    {
        return $this->logo;
    }

    public function setLogo(string $logo)
    {
        $this->logo = $logo;
    }

    public function getOrdre(): int
    {
        return $this->ordre;
    }

    public function setOrdre(int $ordre = 0)
    {
        $this->ordre = $ordre;
    }
}
