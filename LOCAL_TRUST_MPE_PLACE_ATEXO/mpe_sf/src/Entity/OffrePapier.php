<?php

namespace App\Entity;

use App\Repository\OffrePapierRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * OffrePapier.
 *
 * @ORM\Table(name="Offre_papier", indexes={@ORM\Index(name="consultation_id", columns={"consultation_id"}), @ORM\Index(name="Offre_papier_consultation", columns={"organisme", "consultation_id"})})
 * @ORM\Entity(repositoryClass=OffrePapierRepository::class)
 *
 */
class OffrePapier
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation", inversedBy="offresPapiers")
     * @ORM\JoinColumn(name="consultation_id", referencedColumnName="id")
     */
    private ?Consultation $consultation = null;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\EnveloppePapier", mappedBy="offre")
     * @ORM\JoinColumn(name="id", referencedColumnName="offre_papier_id")
     */
    private Collection $enveloppes;

    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     *
     * @ORM\Column(name="organisme", type="string", length=30, unique=true)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private string $organisme = '';

    /**
     * @ORM\Column(name="consultation_id", type="integer", nullable=false)
     */
    private string|int $consultationId = '0';

    /**
     * @ORM\Column(name="entreprise_id", type="integer", nullable=true)
     */
    private ?int $entrepriseId = null;

    /**
     * @ORM\Column(name="id_etablissement", type="integer", nullable=true)
     */
    private ?int $idEtablissement = null;

    /**
     * @ORM\Column(name="nom_entreprise", type="string", length=30, nullable=false)
     */
    private string $nomEntreprise = '';

    /**
     * @ORM\Column(name="date_depot", type="string", nullable=false)
     */
    private string|DateTime $dateDepot = '0000-00-00 00:00:00';

    /**
     * @ORM\Column(name="external_serial", type="string", length=8, nullable=true)
     */
    private ?string $externalSerial = null;

    /**
     * @ORM\Column(name="internal_serial", type="string", length=8, nullable=true)
     */
    private ?string $internalSerial = null;

    /**
     * @ORM\Column(name="offre_selectionnee", type="integer", nullable=false)
     */
    private string|int $offreSelectionnee = '0';

    /**
     * @ORM\Column(name="nom", type="string", length=80, nullable=true)
     */
    private ?string $nom = null;

    /**
     * @ORM\Column(name="prenom", type="string", length=80, nullable=true)
     */
    private ?string $prenom = null;

    /**
     * @ORM\Column(name="adresse", type="string", length=100, nullable=true)
     */
    private ?string $adresse = null;

    /**
     * @ORM\Column(name="adresse2", type="string", length=100, nullable=true)
     */
    private ?string $adresse2 = null;

    /**
     * @ORM\Column(name="telephone", type="string", length=20, nullable=true)
     */
    private ?string $telephone = null;

    /**
     * @ORM\Column(name="fax", type="string", length=30, nullable=true)
     */
    private ?string $fax = null;

    /**
     * @ORM\Column(name="code_postal", type="string", length=5, nullable=true)
     */
    private ?string $codePostal = null;

    /**
     * @ORM\Column(name="ville", type="string", length=50, nullable=true)
     */
    private ?string $ville = null;

    /**
     * @ORM\Column(name="pays", type="string", length=50, nullable=true)
     */
    private ?string $pays = null;

    /**
     * @ORM\Column(name="acronyme_pays", type="string", length=10, nullable=true)
     */
    private ?string $acronymePays = null;

    /**
     * @ORM\Column(name="siret", type="string", length=14, nullable=true)
     */
    private ?string $siret = null;

    /**
     * @ORM\Column(name="identifiant_national", type="string", length=20, nullable=true)
     */
    private ?string $identifiantNational = null;

    /**
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private ?string $email = null;

    /**
     * @ORM\Column(name="observation", type="text", length=65535, nullable=true)
     */
    private ?string $observation = null;

    /**
     * @ORM\Column(name="date_annulation", type="string", length=20, nullable=true)
     */
    private ?string $dateAnnulation = null;

    /**
     * @ORM\Column(name="depot_annule", type="string", nullable=false)
     */
    private string $depotAnnule = '0';

    /**
     * @ORM\Column(name="offre_variante", type="string", nullable=true)
     */
    private ?string $offreVariante = null;

    /**
     * @ORM\Column(name="statut_offre_papier", type="integer", nullable=false)
     */
    private string|int $statutOffrePapier = '1';

    /**
     * @ORM\Column(name="numero_reponse", type="integer", nullable=true)
     */
    private ?int $numeroReponse = null;

    /**
     * @ORM\Column(name="nom_agent_ouverture", type="string", length=100, nullable=true)
     */
    private ?string $nomAgentOuverture = null;

    /**
     * @ORM\Column(name="agent_id_ouverture", type="integer", nullable=true)
     */
    private ?int $agentIdOuverture = null;

    /**
     * @ORM\Column(name="dateheure_ouverture", type="string", length=20, nullable=false)
     */
    private string $dateheureOuverture = '0000-00-00 00:00:00';

    /**
     * Set id.
     *
     * @param int $id
     *
     * @return OffrePapier
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set organisme.
     *
     * @param string $organisme
     *
     * @return OffrePapier
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * Get organisme.
     *
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * Set consultationId.
     *
     * @param int $consultationId
     *
     * @return OffrePapier
     */
    public function setConsultationId($consultationId)
    {
        $this->consultationId = $consultationId;

        return $this;
    }

    /**
     * Get consultationId.
     *
     * @return int
     */
    public function getConsultationId()
    {
        return $this->consultationId;
    }

    /**
     * Set entrepriseId.
     *
     * @param int $entrepriseId
     *
     * @return OffrePapier
     */
    public function setEntrepriseId($entrepriseId)
    {
        $this->entrepriseId = $entrepriseId;

        return $this;
    }

    /**
     * Get entrepriseId.
     *
     * @return int
     */
    public function getEntrepriseId()
    {
        return $this->entrepriseId;
    }

    /**
     * Set idEtablissement.
     *
     * @param int $idEtablissement
     *
     * @return OffrePapier
     */
    public function setIdEtablissement($idEtablissement)
    {
        $this->idEtablissement = $idEtablissement;

        return $this;
    }

    /**
     * Get idEtablissement.
     *
     * @return int
     */
    public function getIdEtablissement()
    {
        return $this->idEtablissement;
    }

    /**
     * Set nomEntreprise.
     *
     * @param string $nomEntreprise
     *
     * @return OffrePapier
     */
    public function setNomEntreprise($nomEntreprise)
    {
        $this->nomEntreprise = $nomEntreprise;

        return $this;
    }

    /**
     * Get nomEntreprise.
     *
     * @return string
     */
    public function getNomEntreprise()
    {
        return $this->nomEntreprise;
    }

    /**
     * Set dateDepot.
     *
     * @param string $dateDepot
     *
     * @return OffrePapier
     */
    public function setDateDepot($dateDepot)
    {
        $this->dateDepot = $dateDepot;

        return $this;
    }

    /**
     * Get dateDepot.
     *
     * @return string
     */
    public function getDateDepot()
    {
        return $this->dateDepot;
    }

    /**
     * Set externalSerial.
     *
     * @param string $externalSerial
     *
     * @return OffrePapier
     */
    public function setExternalSerial($externalSerial)
    {
        $this->externalSerial = $externalSerial;

        return $this;
    }

    /**
     * Get externalSerial.
     *
     * @return string
     */
    public function getExternalSerial()
    {
        return $this->externalSerial;
    }

    /**
     * Set internalSerial.
     *
     * @param string $internalSerial
     *
     * @return OffrePapier
     */
    public function setInternalSerial($internalSerial)
    {
        $this->internalSerial = $internalSerial;

        return $this;
    }

    /**
     * Get internalSerial.
     *
     * @return string
     */
    public function getInternalSerial()
    {
        return $this->internalSerial;
    }

    /**
     * Set offreSelectionnee.
     *
     * @param int $offreSelectionnee
     *
     * @return OffrePapier
     */
    public function setOffreSelectionnee($offreSelectionnee)
    {
        $this->offreSelectionnee = $offreSelectionnee;

        return $this;
    }

    /**
     * Get offreSelectionnee.
     *
     * @return int
     */
    public function getOffreSelectionnee()
    {
        return $this->offreSelectionnee;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return OffrePapier
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom.
     *
     * @param string $prenom
     *
     * @return OffrePapier
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom.
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set adresse.
     *
     * @param string $adresse
     *
     * @return OffrePapier
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse.
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set adresse2.
     *
     * @param string $adresse2
     *
     * @return OffrePapier
     */
    public function setAdresse2($adresse2)
    {
        $this->adresse2 = $adresse2;

        return $this;
    }

    /**
     * Get adresse2.
     *
     * @return string
     */
    public function getAdresse2()
    {
        return $this->adresse2;
    }

    /**
     * Set telephone.
     *
     * @param string $telephone
     *
     * @return OffrePapier
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone.
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set fax.
     *
     * @param string $fax
     *
     * @return OffrePapier
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax.
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set codePostal.
     *
     * @param string $codePostal
     *
     * @return OffrePapier
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal.
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set ville.
     *
     * @param string $ville
     *
     * @return OffrePapier
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville.
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set pays.
     *
     * @param string $pays
     *
     * @return OffrePapier
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays.
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set acronymePays.
     *
     * @param string $acronymePays
     *
     * @return OffrePapier
     */
    public function setAcronymePays($acronymePays)
    {
        $this->acronymePays = $acronymePays;

        return $this;
    }

    /**
     * Get acronymePays.
     *
     * @return string
     */
    public function getAcronymePays()
    {
        return $this->acronymePays;
    }

    /**
     * Set siret.
     *
     * @param string $siret
     *
     * @return OffrePapier
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Get siret.
     *
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * Set identifiantNational.
     *
     * @param string $identifiantNational
     *
     * @return OffrePapier
     */
    public function setIdentifiantNational($identifiantNational)
    {
        $this->identifiantNational = $identifiantNational;

        return $this;
    }

    /**
     * Get identifiantNational.
     *
     * @return string
     */
    public function getIdentifiantNational()
    {
        return $this->identifiantNational;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return OffrePapier
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set observation.
     *
     * @param string $observation
     *
     * @return OffrePapier
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation.
     *
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set dateAnnulation.
     *
     * @param string $dateAnnulation
     *
     * @return OffrePapier
     */
    public function setDateAnnulation($dateAnnulation)
    {
        $this->dateAnnulation = $dateAnnulation;

        return $this;
    }

    /**
     * Get dateAnnulation.
     *
     * @return string
     */
    public function getDateAnnulation()
    {
        return $this->dateAnnulation;
    }

    /**
     * Set depotAnnule.
     *
     * @param string $depotAnnule
     *
     * @return OffrePapier
     */
    public function setDepotAnnule($depotAnnule)
    {
        $this->depotAnnule = $depotAnnule;

        return $this;
    }

    /**
     * Get depotAnnule.
     *
     * @return string
     */
    public function getDepotAnnule()
    {
        return $this->depotAnnule;
    }

    /**
     * Set offreVariante.
     *
     * @param string $offreVariante
     *
     * @return OffrePapier
     */
    public function setOffreVariante($offreVariante)
    {
        $this->offreVariante = $offreVariante;

        return $this;
    }

    /**
     * Get offreVariante.
     *
     * @return string
     */
    public function getOffreVariante()
    {
        return $this->offreVariante;
    }

    /**
     * Set statutOffrePapier.
     *
     * @param int $statutOffrePapier
     *
     * @return OffrePapier
     */
    public function setStatutOffrePapier($statutOffrePapier)
    {
        $this->statutOffrePapier = $statutOffrePapier;

        return $this;
    }

    /**
     * Get statutOffrePapier.
     *
     * @return int
     */
    public function getStatutOffrePapier()
    {
        return $this->statutOffrePapier;
    }

    /**
     * Set numeroReponse.
     *
     * @param int $numeroReponse
     *
     * @return OffrePapier
     */
    public function setNumeroReponse($numeroReponse)
    {
        $this->numeroReponse = $numeroReponse;

        return $this;
    }

    /**
     * Get numeroReponse.
     *
     * @return int
     */
    public function getNumeroReponse()
    {
        return $this->numeroReponse;
    }

    /**
     * Set nomAgentOuverture.
     *
     * @param string $nomAgentOuverture
     *
     * @return OffrePapier
     */
    public function setNomAgentOuverture($nomAgentOuverture)
    {
        $this->nomAgentOuverture = $nomAgentOuverture;

        return $this;
    }

    /**
     * Get nomAgentOuverture.
     *
     * @return string
     */
    public function getNomAgentOuverture()
    {
        return $this->nomAgentOuverture;
    }

    /**
     * Set agentIdOuverture.
     *
     * @param int $agentIdOuverture
     *
     * @return OffrePapier
     */
    public function setAgentIdOuverture($agentIdOuverture)
    {
        $this->agentIdOuverture = $agentIdOuverture;

        return $this;
    }

    /**
     * Get agentIdOuverture.
     *
     * @return int
     */
    public function getAgentIdOuverture()
    {
        return $this->agentIdOuverture;
    }

    /**
     * Set dateheureOuverture.
     *
     * @param string $dateheureOuverture
     *
     * @return OffrePapier
     */
    public function setDateheureOuverture($dateheureOuverture)
    {
        $this->dateheureOuverture = $dateheureOuverture;

        return $this;
    }

    /**
     * Get dateheureOuverture.
     *
     * @return string
     */
    public function getDateheureOuverture()
    {
        return $this->dateheureOuverture;
    }

    public function setConsultation(Consultation $consultation)
    {
        $this->consultation = $consultation;
    }

    /**
     * @return Consultation
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    /**
     * Consultation constructor.
     */
    public function __construct()
    {
        $this->enveloppes = new ArrayCollection();
    }

    /**
     * @return $this
     */
    public function addEnveloppe(EnveloppePapier $enveloppe)
    {
        $this->enveloppes[] = $enveloppe;
        $enveloppe->setOffre($this);

        return $this;
    }

    public function removeEnveloppe(EnveloppePapier $enveloppe)
    {
        $this->enveloppes->removeElement($enveloppe);
    }

    /**
     * @return ArrayCollection
     */
    public function getEnveloppes()
    {
        return $this->enveloppes;
    }
}
