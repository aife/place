<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\EntrepriseDocumentVersionRepository;

/**
 * EntrepriseDocumentVersion.
 *
 * @ORM\Table(name="t_entreprise_document_version", indexes={@ORM\Index(name="entreprise_document_version", columns={"id_document"})})
 * @ORM\Entity(repositoryClass=EntrepriseDocumentVersionRepository::class)
 */
class EntrepriseDocumentVersion
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EntrepriseDocument", inversedBy="versions")
     * @ORM\JoinColumn(name="id_document", referencedColumnName="id_document")
     */
    private ?EntrepriseDocument $document = null;

    /**
     *
     * @ORM\Column(name="id_version_document", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $idVersionDocument;

    /**
     * @ORM\Column(name="id_document", type="integer", nullable=false)
     */
    private ?int $idDocument = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_recuperation", type="datetime", nullable=false)*/
    private $dateRecuperation;

    /**
     * @ORM\Column(name="hash", type="string", length=500, nullable=false)
     */
    private ?string $hash = null;

    /**
     * @ORM\Column(name="id_blob", type="integer", nullable=false)
     */
    private ?int $idBlob = null;

    /**
     * @ORM\Column(name="taille_document", type="string", length=50, nullable=false)
     */
    private ?string $tailleDocument = null;

    /**
     * @ORM\Column(name="extension_document", type="string", length=50, nullable=false)
     */
    private ?string $extensionDocument = null;

    /**
     * Get idVersionDocument.
     *
     * @return int
     */
    public function getIdVersionDocument()
    {
        return $this->idVersionDocument;
    }

    /**
     * Set idDocument.
     *
     * @param int $idDocument
     *
     * @return EntrepriseDocumentVersion
     */
    public function setIdDocument($idDocument)
    {
        $this->idDocument = $idDocument;

        return $this;
    }

    /**
     * Get idDocument.
     *
     * @return int
     */
    public function getIdDocument()
    {
        return $this->idDocument;
    }

    /**
     * Set dateRecuperation.
     *
     * @param DateTime $dateRecuperation
     *
     * @return EntrepriseDocumentVersion
     */
    public function setDateRecuperation($dateRecuperation)
    {
        $this->dateRecuperation = $dateRecuperation;

        return $this;
    }

    /**
     * Get dateRecuperation.
     *
     * @return DateTime
     */
    public function getDateRecuperation()
    {
        return $this->dateRecuperation;
    }

    /**
     * Set hash.
     *
     * @param string $hash
     *
     * @return EntrepriseDocumentVersion
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash.
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set idBlob.
     *
     * @param int $idBlob
     *
     * @return EntrepriseDocumentVersion
     */
    public function setIdBlob($idBlob)
    {
        $this->idBlob = $idBlob;

        return $this;
    }

    /**
     * Get idBlob.
     *
     * @return int
     */
    public function getIdBlob()
    {
        return $this->idBlob;
    }

    /**
     * Set tailleDocument.
     *
     * @param string $tailleDocument
     *
     * @return EntrepriseDocumentVersion
     */
    public function setTailleDocument($tailleDocument)
    {
        $this->tailleDocument = $tailleDocument;

        return $this;
    }

    /**
     * Get tailleDocument.
     *
     * @return string
     */
    public function getTailleDocument()
    {
        return $this->tailleDocument;
    }

    /**
     * Set extensionDocument.
     *
     * @param string $extensionDocument
     *
     * @return EntrepriseDocumentVersion
     */
    public function setExtensionDocument($extensionDocument)
    {
        $this->extensionDocument = $extensionDocument;

        return $this;
    }

    /**
     * Get extensionDocument.
     *
     * @return string
     */
    public function getExtensionDocument()
    {
        return $this->extensionDocument;
    }

    /**
     * @return EntrepriseDocument
     */
    public function getDocument()
    {
        return $this->document;
    }

    public function setDocument(EntrepriseDocument $document)
    {
        $this->document = $document;
    }
}
