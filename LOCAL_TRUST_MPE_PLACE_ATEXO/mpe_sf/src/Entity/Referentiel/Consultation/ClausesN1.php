<?php

namespace App\Entity\Referentiel\Consultation;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use App\Dto\Output\Referentiel\ClausesN1Output;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="referentiel_consultation_clauses_n1")
 * @ORM\Entity(repositoryClass="App\Repository\Referentiel\Consultation\ClausesN1Repository")
 */
#[ApiResource(
    collectionOperations: ['get'],
    itemOperations: ['get'],
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: ClausesN1Output::class,
    routePrefix: '/referentiels'
)]
#[ApiFilter(
    BooleanFilter::class,
    properties: [
        'actif' => 'exact'
    ]
)]
class ClausesN1
{
    public const CLAUSES_SOCIALES = 'clausesSociales';
    public const CLAUSES_ENVIRONNEMENTALES = 'clauseEnvironnementale';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $label;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $tooltip;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $actif;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Referentiel\Consultation\ClausesN2",
     *     mappedBy="clauseN1", orphanRemoval=true)
     */
    private Collection $clausesN2;

    public function __construct()
    {
        $this->clausesN2 = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getTooltip(): ?string
    {
        return $this->tooltip;
    }

    public function setTooltip(?string $tooltip): self
    {
        $this->tooltip = $tooltip;

        return $this;
    }

    public function isActif(): bool
    {
        return $this->actif;
    }

    public function setActif(bool $actif): self
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * @return Collection<int, ClausesN2>
     */
    public function getClausesN2(): Collection
    {
        return $this->clausesN2;
    }

    public function addClausesN2(ClausesN2 $clausesN2): self
    {
        if (!$this->clausesN2->contains($clausesN2)) {
            $this->clausesN2[] = $clausesN2;
            $clausesN2->setClauseN1($this);
        }

        return $this;
    }

    public function removeClausesN2(ClausesN2 $clausesN2): self
    {
        if ($this->clausesN2->removeElement($clausesN2)) {
            // set the owning side to null (unless already changed)
            if ($clausesN2->getClauseN1() === $this) {
                $clausesN2->setClauseN1(null);
            }
        }

        return $this;
    }
}
