<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Entity\Referentiel\Consultation;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\Output\Referentiel\ClausesN4Output;
use App\Entity\Referentiel\Consultation\ClausesN3;
use App\Repository\Referentiel\Consultation\ClausesN4Repository;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="referentiel_consultation_clauses_n4")
 * @ORM\Entity(repositoryClass=ClausesN4Repository::class)
 */
#[ApiResource(
    collectionOperations: ['get'],
    itemOperations: ['get'],
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: ClausesN4Output::class,
    routePrefix: '/referentiels'
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'clauseN3' => 'exact'
    ]
)]
#[ApiFilter(
    BooleanFilter::class,
    properties: [
        'actif' => 'exact'
    ]
)]
class ClausesN4
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $label;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $tooltip;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $typeChamp;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $actif = true;

    /**
     * @ORM\ManyToOne(targetEntity=ClausesN3::class, inversedBy="clausesN4s")
     * @ORM\JoinColumn(nullable=false)
     */
    private ClausesN3 $clauseN3;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getTypeChamp(): ?string
    {
        return $this->typeChamp;
    }

    public function setTypeChamp(?string $typeChamp): self
    {
        $this->typeChamp = $typeChamp;

        return $this;
    }

    public function getTooltip(): ?string
    {
        return $this->tooltip;
    }

    public function setTooltip(?string $tooltip): self
    {
        $this->tooltip = $tooltip;

        return $this;
    }

    public function getActif(): ?bool
    {
        return $this->actif;
    }

    public function setActif(bool $actif): self
    {
        $this->actif = $actif;

        return $this;
    }

    public function getClauseN3(): ?ClausesN3
    {
        return $this->clauseN3;
    }

    public function setClauseN3(?ClausesN3 $clauseN3): self
    {
        $this->clauseN3 = $clauseN3;

        return $this;
    }
}
