<?php

namespace App\Entity\Referentiel\Consultation;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\Output\Referentiel\ClausesN3Output;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="referentiel_consultation_clauses_n3")
 * @ORM\Entity(repositoryClass="App\Repository\Referentiel\Consultation\ClausesN3Repository")
 */
#[ApiResource(
    collectionOperations: ['get'],
    itemOperations: ['get'],
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: ClausesN3Output::class,
    routePrefix: '/referentiels',
    attributes: ["pagination_items_per_page" => 20]
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'clausesN2' => 'exact'
    ]
)]
#[ApiFilter(
    BooleanFilter::class,
    properties: [
        'actif' => 'exact'
    ]
)]
class ClausesN3
{
    public const INSERTION_ACTIVITE_ECONOMIQUE = 'insertionActiviteEconomique';
    public const CLAUSE_SOCIALE_FORMATION_SCOLAIRE = 'clauseSocialeFormationScolaire';
    public const LUTTE_CONTRE_DISCRIMINATIONS = 'lutteContreDiscriminations';
    public const COMMERCE_EQUITABLE = 'commerceEquitable';
    public const ACHATS_ETHIQUES_TRACABILITE_SOCIALE = 'achatsEthiquesTracabiliteSociale';
    public const AUTRE_CLAUSE_SOCIALE = 'autreClauseSociale';
    public const CLAUSE_SOCIALE_RESERVE_ATELIER_PROTEGE = 'clauseSocialeReserveAtelierProtege';
    public const CLAUSE_SOCIALE_SIAE = 'clauseSocialeSIAE';
    public const CLAUSE_SOCIALE_EESS = 'clauseSocialeEESS';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $label;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $tooltip;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $actif;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Referentiel\Consultation\ClausesN2", mappedBy="clausesN3")
     */
    private Collection $clausesN2;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Referentiel\Consultation\ClausesN4", mappedBy="clauseN3")
     */
    private $clausesN4s;

    public function __construct()
    {
        $this->clausesN2 = new ArrayCollection();
        $this->clausesN4s = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getTooltip(): ?string
    {
        return $this->tooltip;
    }

    public function setTooltip(?string $tooltip): self
    {
        $this->tooltip = $tooltip;

        return $this;
    }

    public function isActif(): bool
    {
        return $this->actif;
    }

    public function setActif(bool $actif): self
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * @return Collection<int, ClausesN2>
     */
    public function getClausesN2(): Collection
    {
        return $this->clausesN2;
    }

    public function addClausesN2(ClausesN2 $clausesN2): self
    {
        if (!$this->clausesN2->contains($clausesN2)) {
            $this->clausesN2[] = $clausesN2;
            $clausesN2->addClausesN3($this);
        }

        return $this;
    }

    public function removeClausesN2(ClausesN2 $clausesN2): self
    {
        if ($this->clausesN2->removeElement($clausesN2)) {
            $clausesN2->removeClausesN3($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, ClausesN4>
     */
    public function getClausesN4s(): Collection
    {
        return $this->clausesN4s;
    }

    public function addClausesN4(ClausesN4 $clausesN4): self
    {
        if (!$this->clausesN4s->contains($clausesN4)) {
            $this->clausesN4s[] = $clausesN4;
            $clausesN4->setClauseN3($this);
        }

        return $this;
    }

    public function removeClausesN4(ClausesN4 $clausesN4): self
    {
        if ($this->clausesN4s->removeElement($clausesN4)) {
            // set the owning side to null (unless already changed)
            if ($clausesN4->getClauseN3() === $this) {
                $clausesN4->setClauseN3(null);
            }
        }

        return $this;
    }
}
