<?php

namespace App\Entity\Referentiel\Consultation;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\Output\Referentiel\ClausesN2Output;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="referentiel_consultation_clauses_n2")
 * @ORM\Entity(repositoryClass="App\Repository\Referentiel\Consultation\ClausesN2Repository")
 */
#[ApiResource(
    collectionOperations: ['get'],
    itemOperations: ['get'],
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: ClausesN2Output::class,
    routePrefix: '/referentiels'
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'clauseN1' => 'exact'
    ]
)]
#[ApiFilter(
    BooleanFilter::class,
    properties: [
        'actif' => 'exact'
    ]
)]
class ClausesN2
{
    public const CONDITION_EXECUTION = 'conditionExecution';
    public const SPECIFICATION_TECHNIQUE = 'specificationTechnique';
    public const CRITERE_ATTRIBUTION_MARCHE = 'critereAttributionMarche';
    public const MARCHE_RESERVE = 'marcheReserve';
    public const INSERTION = 'insertion';
    public const SPECIFICATIONS_TECHNIQUES = 'specificationsTechniques';
    public const CONDITIONS_EXECUTIONS = 'conditionsExecutions';
    public const CRITERES_SELECTIONS = 'criteresSelections';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $label;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $tooltip;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $actif;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $limitation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referentiel\Consultation\ClausesN1", inversedBy="clausesN2")
     * @ORM\JoinColumn(nullable=false)
     */
    private ClausesN1 $clauseN1;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Referentiel\Consultation\ClausesN3", inversedBy="clausesN2")
     * @ORM\JoinTable(name="referentiel_consultation_clauses_n2_clauses_n3")
     */
    private Collection $clausesN3;

    public function __construct()
    {
        $this->clausesN3 = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getTooltip(): ?string
    {
        return $this->tooltip;
    }

    public function setTooltip(?string $tooltip): self
    {
        $this->tooltip = $tooltip;

        return $this;
    }

    public function isActif(): bool
    {
        return $this->actif;
    }

    public function setActif(bool $actif): self
    {
        $this->actif = $actif;

        return $this;
    }

    public function getLimitation(): ?int
    {
        return $this->limitation;
    }

    public function setLimitation(?int $limitation): self
    {
        $this->limitation = $limitation;

        return $this;
    }

    public function getClauseN1(): ClausesN1
    {
        return $this->clauseN1;
    }

    public function setClauseN1(ClausesN1 $clauseN1): self
    {
        $this->clauseN1 = $clauseN1;

        return $this;
    }

    /**
     * @return Collection<int, ClausesN3>
     */
    public function getClausesN3(): Collection
    {
        return $this->clausesN3;
    }

    public function addClausesN3(ClausesN3 $clausesN3): self
    {
        if (!$this->clausesN3->contains($clausesN3)) {
            $this->clausesN3[] = $clausesN3;
        }

        return $this;
    }

    public function removeClausesN3(ClausesN3 $clausesN3): self
    {
        $this->clausesN3->removeElement($clausesN3);

        return $this;
    }
}
