<?php

namespace App\Entity\WS;

use App\Entity\Agent;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class WebService.
 *
 * @ORM\Table(name="web_service")
 * @ORM\Entity(repositoryClass="App\Repository\WS\WebServiceRepository")
 */
class WebService
{
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="nom_ws", type="string", nullable=false)
     */
    private ?string $nomWs = null;

    /**
     * Many Users have Many Agents.
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Agent", mappedBy="webservices")
     */
    private Collection $agents;

    /**
     * @var bool
     * @ORM\Column(name="uniquement_technique", type="boolean", nullable=false)
     */
    private $uniquementTechnique = true;

    /**
     * @ORM\Column(name="method_ws", type="string", nullable=false)
     */
    private ?string $methodWs = null;

    /**
     * @ORM\Column(name="route_ws", type="string", nullable=false)
     */
    private ?string $routeWs = null;

    public function __construct()
    {
        $this->agents = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return $this
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getNomWs(): string
    {
        return $this->nomWs;
    }

    /**
     * @return $this
     */
    public function setNomWs(string $nomWs)
    {
        $this->nomWs = $nomWs;

        return $this;
    }

    public function getAgents(): Collection
    {
        return $this->agents;
    }

    public function addAgent(Agent $agent)
    {
        if (!$this->agents->contains($agent)) {
            $this->agents[] = $agent;
        }

        return $this;
    }

    public function removeAgent(Agent $agent)
    {
        $this->agents->removeElement($agent);

        return $this;
    }


    public function isUniquementTechnique(): bool
    {
        return $this->uniquementTechnique;
    }

    public function setUniquementTechnique(bool $uniquementTechnique)
    {
        $this->uniquementTechnique = $uniquementTechnique;
    }

    public function getMethodWs(): string
    {
        return $this->methodWs;
    }

    public function setMethodWs(string $methodWs)
    {
        $this->methodWs = $methodWs;
    }

    public function getRouteWs(): string
    {
        return $this->routeWs;
    }

    public function setRouteWs(string $routeWs)
    {
        $this->routeWs = $routeWs;
    }
}
