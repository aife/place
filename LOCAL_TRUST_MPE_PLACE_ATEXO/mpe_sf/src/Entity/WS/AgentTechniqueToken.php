<?php

namespace App\Entity\WS;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\ApiPlatformCustom\ValidationTicketController;
use DateTime;
use App\Entity\Agent;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class WebService.
 *
 * @ORM\Table(name="agent_technique_token")
 * @ORM\Entity(repositoryClass="App\Repository\WS\AgentTechniqueTokenRepository")
 * @ORM\HasLifecycleCallbacks
 */
#[ApiResource(
    itemOperations: [
        'get' => [
            'method' => 'GET',
            'path' => '/validation-ticket/{ticket}',
            'controller' => ValidationTicketController::class,
            'read' => false,
            "openapi_context" => [
                "summary" => "récupération d'un JWT à partir d'un ticket V0",
                "description" => "## récupération d'un JWT à partir d'un ticket V0",
                "parameters" => [
                    [
                        "in" => "path",
                        "name" => "ticket",
                        "required" => true,
                    ],
                    [
                        "in" => "path",
                        "name" => "id",
                        "required" => false,
                    ]
                ]
            ]
        ]
    ]
)]
class AgentTechniqueToken
{
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Agent")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id")
     */
    private ?Agent $agent = null;

    /**
     * @ORM\Column(name="token", type="string")
     */
    private ?string $token = null;

    /**
     * @var datetime
     *
     * @ORM\Column(type="datetime")
     */
    protected $createAt;

    /**
     * @var datetime
     *
     * @ORM\Column(type="datetime")
     */
    protected $updateAt;

    /**
     * @var datetime
     *
     * @ORM\Column(type="datetime")
     */
    protected $dateExpiration;

    /**
     * Gets triggered only on insert.
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createAt = new DateTime('now');
        $this->updateAt = new DateTime('now');
    }

    /**
     * Gets triggered every time on update.
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updateAt = new DateTime('now');
    }

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return $this
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return $this
     */
    public function setToken(string $token)
    {
        $this->token = $token;

        return $this;
    }

    public function getAgent(): Agent
    {
        return $this->agent;
    }

    /**
     * @return $this
     */
    public function setAgent(Agent $agent)
    {
        $this->agent = $agent;

        return $this;
    }

    public function getDateExpiration(): datetime
    {
        return $this->dateExpiration;
    }

    public function setDateExpiration(datetime $dateExpiration)
    {
        $this->dateExpiration = $dateExpiration;
    }
}
