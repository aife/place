<?php

namespace App\Entity\WS;

use App\Entity\Organisme;
use App\Entity\Service;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class AgentTechniqueAssociation.
 *
 * @ORM\Table(name="agent_technique_association")
 * @ORM\Entity(repositoryClass="App\Repository\WS\AgentTechniqueAssociationRepository")*/
#[UniqueEntity(fields: ['idAgent', 'organisme', 'serviceId'])]
class AgentTechniqueAssociation
{
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id= null;
    /**
     * @ORM\Column(name="id_agent", type="integer")
     */
    private ?int $idAgent = null;
    /**
     * @ORM\Column(name="organisme", type="string", nullable=true, length=50)
     */
    private ?string $acronymeOrganisme = null;
    /**
     * @ORM\Column(name="service_id", type="integer", nullable=true)
     */
    private ?int $serviceId = null;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Service", inversedBy="agentTechAssotiations")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     */
    private ?Service $service = null;
    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme", inversedBy="agentTechAssociations")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private ?Organisme $organisme = null;
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return int
     */
    public function getIdAgent()
    {
        return $this->idAgent;
    }
    /**
     * @return AgentTechniqueAssociation
     */
    public function setIdAgent(int $idAgent)
    {
        $this->idAgent = $idAgent;

        return $this;
    }
    /**
     * @return string | null
     */
    public function getAcronymeOrganisme()
    {
        return $this->acronymeOrganisme;
    }
    public function setAcronymeOrganisme(string $acronymeOrganisme = null)
    {
        $this->acronymeOrganisme = $acronymeOrganisme;

        return $this;
    }
    /**
     * @return Organisme | null
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }
    /**
     * @return AgentTechniqueAssociation
     */
    public function setOrganisme(Organisme $organisme = null)
    {
        $this->organisme = $organisme;

        return $this;
    }
    /**
     * @return int|null
     */
    public function getServiceId(): ?int
    {
        return $this->serviceId;
    }
    /**
     * @param int|null $serviceId
     *
     * @return AgentTechniqueAssociation
     */
    public function setServiceId(int $serviceId = null): AgentTechniqueAssociation
    {
        $this->serviceId = $serviceId;

        return $this;
    }
    /**
     * @return Service| null
     */
    public function getService(): ?Service
    {
        return $this->service;
    }
    /**
     * @param Service | null $service
     *
     * @return AgentTechniqueAssociation
     */
    public function setService(Service $service = null)
    {
        $this->service = $service;
        if ($service) {
            $this->serviceId = $service->getId();
        } else {
            $this->serviceId = null;
        }

        return $this;
    }
}
