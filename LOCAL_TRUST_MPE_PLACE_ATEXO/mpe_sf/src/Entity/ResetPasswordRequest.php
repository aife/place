<?php

namespace App\Entity;

use App\Repository\ResetPasswordRequestRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="t_information_modification_password")
 * @ORM\Entity(repositoryClass=ResetPasswordRequestRepository::class)
 */
class ResetPasswordRequest
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $idUser = null;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private ?string $typeUser = null;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $email = null;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private ?string $dateDemandeModification = null;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private ?string $dateFinValidite = null;

    /**
     * @ORM\Column(type="string", length=256)
     */
    private ?string $jeton = null;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $modificationFaite = '0';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUser(): ?int
    {
        return $this->idUser;
    }

    public function setIdUser(int $idUser): self
    {
        $this->idUser = $idUser;
        return $this;
    }

    public function getTypeUser(): ?string
    {
        return $this->typeUser;
    }

    public function setTypeUser(string $typeUser): self
    {
        $this->typeUser = $typeUser;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function getDateDemandeModification(): ?string
    {
        return $this->dateDemandeModification;
    }

    public function setDateDemandeModification(string $dateDemandeModification): self
    {
        $this->dateDemandeModification = $dateDemandeModification;
        return $this;
    }

    public function getDateFinValidite(): ?string
    {
        return $this->dateFinValidite;
    }

    public function setDateFinValidite(string $dateFinValidite): self
    {
        $this->dateFinValidite = $dateFinValidite;
        return $this;
    }

    public function getJeton(): ?string
    {
        return $this->jeton;
    }

    public function setJeton(string $jeton): self
    {
        $this->jeton = $jeton;
        return $this;
    }

    public function getModificationFaite(): string
    {
        return $this->modificationFaite;
    }

    public function setModificationFaite(string $modificationFaite): self
    {
        $this->modificationFaite = $modificationFaite;
        return $this;
    }
}
