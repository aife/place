<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TDumeContexte.
 *
 * @ORM\Table(name="t_dume_contexte")
 * @ORM\Entity(repositoryClass="App\Repository\TDumeContexteRepository")
 */
class TDumeContexte
{
    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation", inversedBy="dumeContextes", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="consultation_id", referencedColumnName="id")
     */
    private ?Consultation $consultation = null;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme", inversedBy="dumeContextes")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private ?Organisme $organisme = null;

    /**
     * @ORM\Column(name="contexte_lt_dume_id", type="string", length=255)
     */
    private ?string $contexteLtDumeId = null;

    /**
     * @ORM\Column(name="type_dume", type="string", length=45)
     */
    private ?string $typeDume = null;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\TDumeNumero", mappedBy="idDumeContexte")
     * @ORM\JoinColumn(name="id", referencedColumnName="id_dume_contexte")
     */
    private Collection $dumeNumeros;

    public function __construct()
    {
        $this->dumeNumeros = new ArrayCollection();
    }

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\TCandidature", mappedBy="idDumeContexte")
     */
    private $idCandidature;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private int $status = 99;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_standard", type="boolean", nullable=false)
     */
    private $isStandard = true;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime", nullable=false)*/
    private $dateCreation;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_modification", type="datetime", nullable=false)*/
    private $dateModification;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Consultation
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    /**
     * @param Consultation $consultation
     */
    public function setConsultation($consultation)
    {
        $this->consultation = $consultation;
    }

    /**
     * @return Organisme
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param Organisme $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return string
     */
    public function getContexteLtDumeId()
    {
        return $this->contexteLtDumeId;
    }

    /**
     * @param string $contexteLtDumeId
     */
    public function setContexteLtDumeId($contexteLtDumeId)
    {
        $this->contexteLtDumeId = $contexteLtDumeId;
    }

    /**
     * @return string
     */
    public function getTypeDume()
    {
        return $this->typeDume;
    }

    /**
     * @param string $typeDume
     */
    public function setTypeDume($typeDume)
    {
        $this->typeDume = $typeDume;
    }

    /**
     * @return $this
     */
    public function addDumeNumeros(TDumeNumero $dumeNumero)
    {
        $this->dumeNumeros[] = $dumeNumero;
        $dumeNumero->setIdDumeContexte($this);

        return $this;
    }

    public function removeDumeNumeros(TDumeNumero $dumeNumero)
    {
        $this->dumeNumeros->removeElement($dumeNumero);
    }

    /**
     * @return ArrayCollection
     */
    public function getDumeNumeros()
    {
        return $this->dumeNumeros;
    }

    /**
     * @return mixed
     */
    public function getIdCandidature()
    {
        return $this->idCandidature;
    }

    /**
     * @param mixed $idCandidature
     */
    public function setIdCandidature($idCandidature)
    {
        $this->idCandidature = $idCandidature;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function isStandard()
    {
        return $this->isStandard;
    }

    /**
     * @param bool $isStandard
     */
    public function setIsStandard($isStandard)
    {
        $this->isStandard = $isStandard;
    }

    /**
     * @return DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    public function setDateCreation(DateTime $dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }

    /**
     * @return DateTime
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }

    public function setDateModification(DateTime $dateModification)
    {
        $this->dateModification = $dateModification;
    }
}
