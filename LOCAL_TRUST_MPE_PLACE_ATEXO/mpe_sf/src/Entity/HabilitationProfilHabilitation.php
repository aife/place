<?php

namespace App\Entity;

use App\Repository\HabilitationProfilHabilitationRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: HabilitationProfilHabilitationRepository::class)]
/**
 * @ORM\Entity(repositoryClass=HabilitationProfilHabilitationRepository::class)
 */
class HabilitationProfilHabilitation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    /**
     * @ORM\ManyToOne(targetEntity=HabilitationProfil::class)
     * @ORM\JoinColumn(nullable= false)
     */
    private ?HabilitationProfil $habilitationProfil = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    /**
     * @ORM\ManyToOne(targetEntity=ReferentielHabilitation::class)
     * @ORM\JoinColumn(nullable= false)
     */
    private ?ReferentielHabilitation $habilitation = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHabilitationProfil(): ?HabilitationProfil
    {
        return $this->habilitationProfil;
    }

    public function setHabilitationProfil(?HabilitationProfil $habilitationProfil): static
    {
        $this->habilitationProfil = $habilitationProfil;

        return $this;
    }

    public function getHabilitation(): ?ReferentielHabilitation
    {
        return $this->habilitation;
    }

    public function setHabilitation(?ReferentielHabilitation $habilitation): static
    {
        $this->habilitation = $habilitation;

        return $this;
    }
}
