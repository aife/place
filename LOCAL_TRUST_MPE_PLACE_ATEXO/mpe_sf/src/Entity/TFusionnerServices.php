<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * TFusionnerServices.
 *
 * @ORM\Table(name="t_fusionner_services")
 * @ORM\Entity
 */
class TFusionnerServices
{
    /**
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="id_service_source", type="integer", nullable=true)
     */
    private ?int $idServiceSource = null;

    /**
     * @ORM\Column(name="id_service_cible", type="integer", nullable=true)
     */
    private ?int $idServiceCible = null;

    /**
     * @ORM\Column(name="organisme", type="string", length=50, nullable=true)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(name="id_agent", type="integer", nullable=true)
     */
    private ?int $idAgent = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime", nullable=true)*/
    private $dateCreation;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_fusion", type="datetime", nullable=true)*/
    private $dateFusion;

    /**
     * @ORM\Column(name="donnees_fusionnees", type="string", nullable=true)
     */
    private ?string $donneesFusionnees = '0';

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIdServiceSource()
    {
        return $this->idServiceSource;
    }

    /**
     * @param int $idServiceSource
     */
    public function setIdServiceSource($idServiceSource)
    {
        $this->idServiceSource = $idServiceSource;
    }

    /**
     * @return int
     */
    public function getIdServiceCible()
    {
        return $this->idServiceCible;
    }

    /**
     * @param int $idServiceCible
     */
    public function setIdServiceCible($idServiceCible)
    {
        $this->idServiceCible = $idServiceCible;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param string $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return int
     */
    public function getIdAgent()
    {
        return $this->idAgent;
    }

    /**
     * @param int $idAgent
     */
    public function setIdAgent($idAgent)
    {
        $this->idAgent = $idAgent;
    }

    /**
     * @return DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param DateTime $dateCreation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }

    /**
     * @return DateTime
     */
    public function getDateFusion()
    {
        return $this->dateFusion;
    }

    /**
     * @param DateTime $dateFusion
     */
    public function setDateFusion($dateFusion)
    {
        $this->dateFusion = $dateFusion;
    }

    /**
     * @return string
     */
    public function getDonneesFusionnees()
    {
        return $this->donneesFusionnees;
    }

    /**
     * @param string $donneesFusionnees
     */
    public function setDonneesFusionnees($donneesFusionnees)
    {
        $this->donneesFusionnees = $donneesFusionnees;
    }
}
