<?php

namespace App\Entity\Inscrit;

use App\Repository\InscritHistoriqueRepository;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="InscritHistorique")
 * @ORM\Entity(repositoryClass="App\Repository\InscritHistoriqueRepository")
 */
class InscritHistorique
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(type="integer")
     */
    private int $idInscrit1;

    /**
     * @ORM\Column(type="integer")
     */
    private int $entrepriseId;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private string $inscrit1;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private string $mail1;

    /**
     * @ORM\Column(type="integer")
     */
    private int $profil1;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private string $inscrit2;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private string $mail2;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private string $date;

    /**
     * @ORM\Column(type="integer")
     */
    private int $action;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getIdInscrit1(): int
    {
        return $this->idInscrit1;
    }

    /**
     * @param int $idInscrit1
     * @return InscritHistorique
     */
    public function setIdInscrit1(int $idInscrit1): InscritHistorique
    {
        $this->idInscrit1 = $idInscrit1;

        return $this;
    }

    /**
     * @return int
     */
    public function getEntrepriseId(): int
    {
        return $this->entrepriseId;
    }

    /**
     * @param int $entrepriseId
     * @return InscritHistorique
     */
    public function setEntrepriseId(int $entrepriseId): InscritHistorique
    {
        $this->entrepriseId = $entrepriseId;

        return $this;
    }

    /**
     * @return string
     */
    public function getInscrit1(): string
    {
        return $this->inscrit1;
    }

    /**
     * @param string $inscrit1
     * @return InscritHistorique
     */
    public function setInscrit1(string $inscrit1): InscritHistorique
    {
        $this->inscrit1 = $inscrit1;

        return $this;
    }

    /**
     * @return string
     */
    public function getMail1(): string
    {
        return $this->mail1;
    }

    /**
     * @param string $mail1
     * @return InscritHistorique
     */
    public function setMail1(string $mail1): InscritHistorique
    {
        $this->mail1 = $mail1;

        return $this;
    }

    /**
     * @return int
     */
    public function getProfil1(): int
    {
        return $this->profil1;
    }

    /**
     * @param int $profil1
     * @return InscritHistorique
     */
    public function setProfil1(int $profil1): InscritHistorique
    {
        $this->profil1 = $profil1;

        return $this;
    }

    /**
     * @return string
     */
    public function getInscrit2(): string
    {
        return $this->inscrit2;
    }

    /**
     * @param string $inscrit2
     * @return InscritHistorique
     */
    public function setInscrit2(string $inscrit2): InscritHistorique
    {
        $this->inscrit2 = $inscrit2;

        return $this;
    }

    /**
     * @return string
     */
    public function getMail2(): string
    {
        return $this->mail2;
    }

    /**
     * @param string $mail2
     * @return InscritHistorique
     */
    public function setMail2(string $mail2): InscritHistorique
    {
        $this->mail2 = $mail2;

        return $this;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     * @return InscritHistorique
     */
    public function setDate(string $date): InscritHistorique
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return int
     */
    public function getAction(): int
    {
        return $this->action;
    }

    /**
     * @param int $action
     * @return InscritHistorique
     */
    public function setAction(int $action): InscritHistorique
    {
        $this->action = $action;

        return $this;
    }
}
