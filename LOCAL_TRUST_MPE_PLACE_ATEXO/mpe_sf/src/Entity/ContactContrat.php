<?php

namespace App\Entity;

use DateTime;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\Output\ContactContratOutput;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * ContactContrat.
 *
 * @ORM\Table(name="t_contact_contrat")
 * @ORM\Entity(repositoryClass="App\Repository\ContactContratRepository")
 */
#[ApiResource(
    collectionOperations: ['get'],
    itemOperations: ['get'],
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: ContactContratOutput::class,
    routePrefix: '/referentiels'
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'idContactContrat' => 'exact',
        'nom' => 'partial',
        'prenom' => 'partial',
    ]
)]
class ContactContrat
{
    /**
     *
     * @ORM\Column(name="id_contact_contrat", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $idContactContrat = null;

    /**
     *
     * @ORM\Column(name="nom", type="string", length=80, nullable=true)*/
    #[Groups('webservice')]
    private ?string $nom = null;

    /**
     *
     * @ORM\Column(name="prenom", type="string", length=80, nullable=true)*/
    #[Groups('webservice')]
    private ?string $prenom = null;

    /**
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)*/
    #[Groups('webservice')]
    private ?string $email = null;

    /**
     *
     * @ORM\Column(name="telephone", type="string", length=20, nullable=true)*/
    #[Groups('webservice')]
    private ?string $telephone = null;

    private $idTypeContrat;

    /**
     * @ORM\Column(name="id_etablissement", type="integer")
     */
    private ?int $idEtablissement = null;

    /**
     * @ORM\Column(name="id_entreprise", type="integer")
     */
    private ?int $idEntreprise = null;

    /**
     * @ORM\Column(name="id_inscrit", type="integer")
     */
    private ?int $idInscrit = null;

    /**
     * @var DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=true)*/
    private $createdAt;

    /**
     * @var DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)*/
    private $updatedAt;

    /**
     * ContactContrat constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt(new DateTime());
    }

    /**
     * @return string
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return int*/
    #[Groups('webservice')]
    public function getId()
    {
        return $this->idContactContrat;
    }

    /**
     * @return int
     */
    public function getIdContactContrat()
    {
        return $this->idContactContrat;
    }

    public function setIdContactContrat(int $idContactContrat)
    {
        $this->idContactContrat = $idContactContrat;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdExterne()
    {
        return $this->idContactContrat;
    }

    public function setIdExterne(int $idContactContrat)
    {
        $this->idContactContrat = $idContactContrat;
    }

    public function setFormePrix(string $formePrix)
    {
        $this->formePrix = $formePrix;
    }

    public function getIdTypeContrat()
    {
        return $this->idTypeContrat;
    }

    /**
     * @return int
     */
    public function getIdEtablissement()
    {
        return $this->idEtablissement;
    }

    public function setIdEtablissement(int $idEtablissement)
    {
        $this->idEtablissement = $idEtablissement;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getIdInscrit()
    {
        return $this->idInscrit;
    }

    public function setIdInscrit(int $idInscrit)
    {
        $this->idInscrit = $idInscrit;
    }

    /**
     * @return int
     */
    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    public function setIdEntreprise(int $idEntreprise)
    {
        $this->idEntreprise = $idEntreprise;
    }
}
