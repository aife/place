<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * InvitePermanentContrat.
 *
 * @ORM\Table(name="invite_permanent_contrat")
 * @ORM\Entity(repositoryClass="App\Repository\InvitePermanentContratRepository")
 */
class InvitePermanentContrat
{
    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;
    /**
     * @ORM\Column(name="organisme", type="string",length=256, nullable=true)
     */
    private ?string $organisme = null;
    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="App\Entity\Service")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id", nullable=true)
     */
    private $service;
    /**
     * @ORM\Column(name="lot", type="string",length=256, nullable=true)
     */
    private ?string $lot = null;

    /**
     * @ORM\Column(name="id_consultation", type="integer", nullable=true)
     */
    private ?int $idConsultation = null;

    /**
     * @ORM\Column(name="id_contrat_titulaire", type="integer", nullable=true)
     */
    private ?int $idContratTitulaire = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_decision", type="date",nullable=true)*/
    private $dateDecision;

    /**
     * @var DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=true)*/
    private $createdAt;

    /**
     * @var DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)*/
    private $updatedAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getOrganisme(): ?string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme): InvitePermanentContrat
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * @return int
     */
    public function getService(): ?int
    {
        return $this->service;
    }

    public function setService(int $service): InvitePermanentContrat
    {
        $this->service = $service;

        return $this;
    }

    /**
     * @return string
     */
    public function getLot(): ?string
    {
        return $this->lot;
    }

    public function setLot(string $lot): InvitePermanentContrat
    {
        $this->lot = $lot;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdConsultation(): ?int
    {
        return $this->idConsultation;
    }

    public function setIdConsultation(int $idConsultation): InvitePermanentContrat
    {
        $this->idConsultation = $idConsultation;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdContratTitulaire(): ?int
    {
        return $this->idContratTitulaire;
    }

    public function setIdContratTitulaire(int $idContratTitulaire): InvitePermanentContrat
    {
        $this->idContratTitulaire = $idContratTitulaire;

        return $this;
    }

    public function getDateDecision(): DateTime
    {
        return $this->dateDecision;
    }

    public function setDateDecision(DateTime $dateDecision): InvitePermanentContrat
    {
        $this->dateDecision = $dateDecision;

        return $this;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): InvitePermanentContrat
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTime $updatedAt): InvitePermanentContrat
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
