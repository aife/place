<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Entity\SuiviAcces;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="SuiviAcces")
 * @ORM\Entity(repositoryClass="App\Repository\SuiviAcces\SuiviAccesRepository")
 */
class SuiviAcces
{
    /**
     * @ORM\Column(name="id_auto", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $idAuto;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $idAgent = 0;

    /**
     * @ORM\Column(type="string", length=20, nullable=false)
     */
    private string $dateAcces;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $nom = null;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $prenom = null;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $email = null;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(type="integer", options={"unsigned"=true},  nullable=true)
     */
    private ?int $serviceId = null;

    /**
     * @return int
     */
    public function getIdAuto(): int
    {
        return $this->idAuto;
    }

    /**
     * @return int
     */
    public function getIdAgent(): int
    {
        return $this->idAgent;
    }

    /**
     * @param int $idAgent
     * @return SuiviAcces
     */
    public function setIdAgent(int $idAgent): SuiviAcces
    {
        $this->idAgent = $idAgent;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateAcces(): string
    {
        return $this->dateAcces;
    }

    /**
     * @param string $dateAcces
     * @return SuiviAcces
     */
    public function setDateAcces(string $dateAcces): SuiviAcces
    {
        $this->dateAcces = $dateAcces;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * @param string|null $nom
     * @return SuiviAcces
     */
    public function setNom(?string $nom): SuiviAcces
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    /**
     * @param string|null $prenom
     * @return SuiviAcces
     */
    public function setPrenom(?string $prenom): SuiviAcces
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return SuiviAcces
     */
    public function setEmail(?string $email): SuiviAcces
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrganisme(): ?string
    {
        return $this->organisme;
    }

    /**
     * @param string|null $organisme
     * @return SuiviAcces
     */
    public function setOrganisme(?string $organisme): SuiviAcces
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getServiceId(): ?int
    {
        return $this->serviceId;
    }

    /**
     * @param int|null $serviceId
     * @return SuiviAcces
     */
    public function setServiceId(?int $serviceId): SuiviAcces
    {
        $this->serviceId = $serviceId;

        return $this;
    }
}
