<?php

namespace App\Entity;

use DateTimeInterface;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * EnveloppePapier.
 *
 * @ORM\Table(name="Enveloppe_papier", indexes={@ORM\Index(name="offre_papier_id", columns={"offre_papier_id"})})
 * @ORM\Entity
 */
class EnveloppePapier
{
    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\OffrePapier", inversedBy="enveloppes")
     * @ORM\JoinColumn(name="offre_papier_id", referencedColumnName="id")
     */
    private ?OffrePapier $offre = null;

    /**
     *
     * @ORM\Column(name="id_enveloppe_papier", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $idEnveloppePapier = null;

    /**
     *
     * @ORM\Column(name="organisme", type="string", length=30)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private string $organisme = '';

    /**
     * @ORM\Column(name="offre_papier_id", type="integer", nullable=false)
     */
    private string|int $offrePapierId = '0';

    /**
     * @ORM\Column(name="statut", type="integer", nullable=false)
     */
    private string|int $statut = '0';

    /**
     * @ORM\Column(name="supprime", type="string", length=1, nullable=false)
     */
    private string $supprime = '0';

    /**
     * @ORM\Column(name="cryptage", type="string", length=1, nullable=false)
     */
    private string $cryptage = '1';

    /**
     * @ORM\Column(name="is_send", type="integer", nullable=false)
     */
    private string|int $isSend = '1';

    /**
     * @ORM\Column(name="type_env", type="integer", nullable=false)
     */
    private string|int $typeEnv = '0';

    /**
     * @ORM\Column(name="sous_pli", type="integer", nullable=false)
     */
    private string|int $sousPli = '0';

    /**
     * @ORM\Column(name="champs_optionnels", type="text", nullable=true)
     */
    private ?string $champsOptionnels = null;

    /**
     * @ORM\Column(name="agent_id_ouverture", type="integer", nullable=false)
     */
    private string|int $agentIdOuverture = '0';

    /**
     * @ORM\Column(name="dateheure_ouverture", type="datetime", nullable=false)
     */
    private string|DateTimeInterface|DateTime $dateheureOuverture = '0000-00-00 00:00:00';

    /**
     * @ORM\Column(name="statut_enveloppe", type="integer", nullable=false)
     */
    private string|int $statutEnveloppe = '1';

    /**
     * @ORM\Column(name="enveloppe_postule", type="string", nullable=false)
     */
    private string $enveloppePostule = '1';

    /**
     * @ORM\Column(name="nom_agent_ouverture", type="string", length=100, nullable=true)
     */
    private ?string $nomAgentOuverture = null;

    /**
     * CandidatureMps constructor.
     */
    public function __construct()
    {
        $this->dateheureOuverture = new DateTime();
    }

    /**
     * Set idEnveloppePapier.
     *
     * @param int $idEnveloppePapier
     *
     * @return EnveloppePapier
     */
    public function setIdEnveloppePapier($idEnveloppePapier)
    {
        $this->idEnveloppePapier = $idEnveloppePapier;

        return $this;
    }

    /**
     * Get idEnveloppePapier.
     *
     * @return int
     */
    public function getIdEnveloppePapier()
    {
        return $this->idEnveloppePapier;
    }

    /**
     * Set organisme.
     *
     * @param string $organisme
     *
     * @return EnveloppePapier
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * Get organisme.
     *
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * Set offrePapierId.
     *
     * @param int $offrePapierId
     *
     * @return EnveloppePapier
     */
    public function setOffrePapierId($offrePapierId)
    {
        $this->offrePapierId = $offrePapierId;

        return $this;
    }

    /**
     * Get offrePapierId.
     *
     * @return int
     */
    public function getOffrePapierId()
    {
        return $this->offrePapierId;
    }

    /**
     * Set statut.
     *
     * @param int $statut
     *
     * @return EnveloppePapier
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut.
     *
     * @return int
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set supprime.
     *
     * @param string $supprime
     *
     * @return EnveloppePapier
     */
    public function setSupprime($supprime)
    {
        $this->supprime = $supprime;

        return $this;
    }

    /**
     * Get supprime.
     *
     * @return string
     */
    public function getSupprime()
    {
        return $this->supprime;
    }

    /**
     * Set cryptage.
     *
     * @param string $cryptage
     *
     * @return EnveloppePapier
     */
    public function setCryptage($cryptage)
    {
        $this->cryptage = $cryptage;

        return $this;
    }

    /**
     * Get cryptage.
     *
     * @return string
     */
    public function getCryptage()
    {
        return $this->cryptage;
    }

    /**
     * Set isSend.
     *
     * @param int $isSend
     *
     * @return EnveloppePapier
     */
    public function setIsSend($isSend)
    {
        $this->isSend = $isSend;

        return $this;
    }

    /**
     * Get isSend.
     *
     * @return int
     */
    public function getIsSend()
    {
        return $this->isSend;
    }

    /**
     * Set typeEnv.
     *
     * @param int $typeEnv
     *
     * @return EnveloppePapier
     */
    public function setTypeEnv($typeEnv)
    {
        $this->typeEnv = $typeEnv;

        return $this;
    }

    /**
     * Get typeEnv.
     *
     * @return int
     */
    public function getTypeEnv()
    {
        return $this->typeEnv;
    }

    /**
     * Set sousPli.
     *
     * @param int $sousPli
     *
     * @return EnveloppePapier
     */
    public function setSousPli($sousPli)
    {
        $this->sousPli = $sousPli;

        return $this;
    }

    /**
     * Get sousPli.
     *
     * @return int
     */
    public function getSousPli()
    {
        return $this->sousPli;
    }

    /**
     * Set champsOptionnels.
     *
     * @param string $champsOptionnels
     *
     * @return EnveloppePapier
     */
    public function setChampsOptionnels($champsOptionnels)
    {
        $this->champsOptionnels = $champsOptionnels;

        return $this;
    }

    /**
     * Get champsOptionnels.
     *
     * @return string
     */
    public function getChampsOptionnels()
    {
        return $this->champsOptionnels;
    }

    /**
     * Set agentIdOuverture.
     *
     * @param int $agentIdOuverture
     *
     * @return EnveloppePapier
     */
    public function setAgentIdOuverture($agentIdOuverture)
    {
        $this->agentIdOuverture = $agentIdOuverture;

        return $this;
    }

    /**
     * Get agentIdOuverture.
     *
     * @return int
     */
    public function getAgentIdOuverture()
    {
        return $this->agentIdOuverture;
    }

    /**
     * Set dateheureOuverture.
     *
     * @param DateTime $dateheureOuverture
     *
     * @return EnveloppePapier
     */
    public function setDateheureOuverture($dateheureOuverture)
    {
        $this->dateheureOuverture = $dateheureOuverture;

        return $this;
    }

    /**
     * Get dateheureOuverture.
     *
     * @return DateTime
     */
    public function getDateheureOuverture()
    {
        return $this->dateheureOuverture;
    }

    /**
     * Set statutEnveloppe.
     *
     * @param int $statutEnveloppe
     *
     * @return EnveloppePapier
     */
    public function setStatutEnveloppe($statutEnveloppe)
    {
        $this->statutEnveloppe = $statutEnveloppe;

        return $this;
    }

    /**
     * Get statutEnveloppe.
     *
     * @return int
     */
    public function getStatutEnveloppe()
    {
        return $this->statutEnveloppe;
    }

    /**
     * Set enveloppePostule.
     *
     * @param string $enveloppePostule
     *
     * @return EnveloppePapier
     */
    public function setEnveloppePostule($enveloppePostule)
    {
        $this->enveloppePostule = $enveloppePostule;

        return $this;
    }

    /**
     * Get enveloppePostule.
     *
     * @return string
     */
    public function getEnveloppePostule()
    {
        return $this->enveloppePostule;
    }

    /**
     * Set nomAgentOuverture.
     *
     * @param string $nomAgentOuverture
     *
     * @return EnveloppePapier
     */
    public function setNomAgentOuverture($nomAgentOuverture)
    {
        $this->nomAgentOuverture = $nomAgentOuverture;

        return $this;
    }

    /**
     * Get nomAgentOuverture.
     *
     * @return string
     */
    public function getNomAgentOuverture()
    {
        return $this->nomAgentOuverture;
    }

    /**
     * @return OffrePapier
     */
    public function getOffre()
    {
        return $this->offre;
    }

    public function setOffre(OffrePapier $offre)
    {
        $this->offre = $offre;
    }
}
