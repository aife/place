<?php

namespace App\Entity;

use App\Repository\FluxRssRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="t_flux_rss" )
 * @ORM\Entity(repositoryClass=FluxRssRepository::class)
 */
class FluxRss
{
    /**
     * @var ?int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     **/
    private ?int $id;

    /**
     * @var string
     * @ORM\Column(name="tender_xml", type="text", nullable=false)
     */
    private string $tenderXml;

    /**
     * @var string
     * @ORM\Column(name="nom_fichier", type="string", length=256, nullable=false)
     */
    private string $nomFichier;

    /**
     * @var ?string
     * @ORM\Column(name="libelle", type="string", length=256, nullable=true)
     */
    private ?string $libelle;

    /**
     * @var ?string
     * @ORM\Column(name="module", type="string", length=256, nullable=true)
     */
    private ?string $module;

    /**
     * @var string
     * @ORM\Column(name="afficher_flux_rss", type="string", length=1, nullable=false)
     */
    private string $afficherFluxRss = '1';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): FluxRss
    {
        $this->id = $id;

        return $this;
    }

    public function getTenderXml(): string
    {
        return $this->tenderXml;
    }

    public function setTenderXml(string $tenderXml): FluxRss
    {
        $this->tenderXml = $tenderXml;

        return $this;
    }

    public function getNomFichier(): string
    {
        return $this->nomFichier;
    }

    public function setNomFichier(string $nomFichier): FluxRss
    {
        $this->nomFichier = $nomFichier;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): FluxRss
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getModule(): ?string
    {
        return $this->module;
    }

    public function setModule(?string $module): FluxRss
    {
        $this->module = $module;

        return $this;
    }

    public function getAfficherFluxRss(): string
    {
        return $this->afficherFluxRss;
    }

    public function setAfficherFluxRss(string $afficherFluxRss): FluxRss
    {
        $this->afficherFluxRss = $afficherFluxRss;

        return $this;
    }
}
