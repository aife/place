<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class EchangePieceJointe.
 *
 * @ORM\Table(name="EchangePieceJointe")
 * @ORM\Entity()
 */
class EchangePieceJointe
{
    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme", inversedBy="echangePieceJointes")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private ?Organisme $organisme = null;

    /**
     * @ORM\Column(name="id_message", type="integer", nullable=false)
     */
    private int $idMessage = 0;

    /**
     * @ORM\Column(name="nom_fichier", type="string", length=255, nullable=false)
     */
    private ?string $nomFichier = null;

    /**
     * @ORM\Column(name="extension", type="string", length=100, nullable=false)
     */
    private ?string $extension = null;

    /**
     * @ORM\Column(name="piece", type="integer", nullable=false)
     */
    private int $piece = 0;

    /**
     * @ORM\Column(name="horodatage", type="blob", nullable=false)
     */
    private ?string $horodatage = null;

    /**
     * @ORM\Column(name="untrusteddate", type="string", length=25, nullable=false)
     */
    private ?string $untrustedDate = null;

    /**
     * @ORM\Column(name="taille", type="string", length=25, nullable=false)
     */
    private int|string $taille = 0;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Organisme
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param Organisme $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    public function getIdMessage(): int
    {
        return $this->idMessage;
    }

    public function setIdMessage(int $idMessage)
    {
        $this->idMessage = $idMessage;
    }

    public function getNomFichier(): string
    {
        return $this->nomFichier;
    }

    public function setNomFichier(string $nomFichier)
    {
        $this->nomFichier = $nomFichier;
    }

    public function getExtension(): string
    {
        return $this->extension;
    }

    public function setExtension(string $extension)
    {
        $this->extension = $extension;
    }

    public function getPiece(): int
    {
        return $this->piece;
    }

    public function setPiece(int $piece)
    {
        $this->piece = $piece;
    }

    public function getHorodatage(): string
    {
        return $this->horodatage;
    }

    public function setHorodatage(string $horodatage)
    {
        $this->horodatage = $horodatage;
    }

    public function getUntrustedDate(): string
    {
        return $this->untrustedDate;
    }

    public function setUntrustedDate(string $untrustedDate)
    {
        $this->untrustedDate = $untrustedDate;
    }

    public function getTaille(): string
    {
        return $this->taille;
    }

    public function setTaille(string $taille)
    {
        $this->taille = $taille;
    }
}
