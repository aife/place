<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HistoriqueSuppressionAgent.
 *
 * @ORM\Table(name="Historique_suppression_agent")
 * @ORM\Entity(repositoryClass="App\Repository\HistoriqueSuppressionAgentRepository")
 */
class HistoriqueSuppressionAgent
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="id_agent_suppresseur", type="integer", length=8)
     */
    private int $idAgentSuppresseur = 0;

    /**
     * @ORM\Column(name="service_id", type="integer", nullable=true)
     */
    private ?int $serviceId = null;

    /**
     * @ORM\Column(name="nom",type="string", length=100)
     */
    private ?string $nom = null;

    /**
     * @ORM\Column(name="prenom",type="string", length=100)
     */
    private ?string $prenom = null;

    /**
     * @ORM\Column(name="email",type="string", length=100)
     */
    private ?string $email = null;

    /**
     * @ORM\Column(name="organisme",type="string", length=100)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(name="date_suppression",type="string", length=20)
     */
    private ?string $dateSuppression = null;

    /**
     * @ORM\Column(name="id_agent_supprime", type="integer", length=8)
     */
    private ?int $idAgentSupprime = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getIdAgentSuppresseur(): int
    {
        return $this->idAgentSuppresseur;
    }

    public function setIdAgentSuppresseur(int $idAgentSuppresseur): HistoriqueSuppressionAgent
    {
        $this->idAgentSuppresseur = $idAgentSuppresseur;

        return $this;
    }

    public function getServiceId(): int
    {
        return $this->serviceId;
    }

    public function setServiceId(int $serviceId): HistoriqueSuppressionAgent
    {
        $this->serviceId = $serviceId;

        return $this;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    public function setNom(string $nom): HistoriqueSuppressionAgent
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): HistoriqueSuppressionAgent
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail(string $email): HistoriqueSuppressionAgent
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme): HistoriqueSuppressionAgent
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateSuppression()
    {
        return $this->dateSuppression;
    }

    public function setDateSuppression(string $dateSuppression): HistoriqueSuppressionAgent
    {
        $this->dateSuppression = $dateSuppression;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdAgentSupprime()
    {
        return $this->idAgentSupprime;
    }

    public function setIdAgentSupprime(int $idAgentSupprime): HistoriqueSuppressionAgent
    {
        $this->idAgentSupprime = $idAgentSupprime;

        return $this;
    }
}
