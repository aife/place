<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class RelationEchange.
 *
 * @ORM\Table(name="Relation_Echange")
 * @ORM\Entity(repositoryClass="App\Repository\RelationEchangeRepository")
 */
class RelationEchange
{
    /**
     *
     * @ORM\Column(name="id_auto", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var Echange
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Echange", inversedBy="relationsEchange")
     * @ORM\JoinColumn(name="id_echange", referencedColumnName="id",nullable=true)
     */
    private $echange = 0;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme", inversedBy="relationEchanges")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private ?Organisme $organisme = null;

    /**
     * @ORM\Column(name="id_externe", type="integer", nullable=false)
     */
    private int $idExterne = 0;

    /**
     * @ORM\Column(name="type_relation", type="integer", nullable=false)
     */
    private int $typeRelation = 0;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_envoi", type="datetime", nullable=true)*/
    private $dateEnvoi = '0000-00-00 00:00:00';

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Echange
     */
    public function getEchange()
    {
        return $this->echange;
    }

    /**
     * @param Echange $echange
     */
    public function setEchange($echange)
    {
        $this->echange = $echange;
    }

    /**
     * @return Organisme
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param Organisme $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return int
     */
    public function getIdExterne()
    {
        return $this->idExterne;
    }

    /**
     * @param int $idExterne
     */
    public function setIdExterne($idExterne)
    {
        $this->idExterne = $idExterne;
    }

    /**
     * @return int
     */
    public function getTypeRelation()
    {
        return $this->typeRelation;
    }

    /**
     * @param int $typeRelation
     */
    public function setTypeRelation($typeRelation)
    {
        $this->typeRelation = $typeRelation;
    }

    /**
     * @return DateTime
     */
    public function getDateEnvoi()
    {
        return $this->dateEnvoi;
    }

    /**
     * @param DateTime $dateEnvoi
     */
    public function setDateEnvoi($dateEnvoi)
    {
        $this->dateEnvoi = $dateEnvoi;
    }
}
