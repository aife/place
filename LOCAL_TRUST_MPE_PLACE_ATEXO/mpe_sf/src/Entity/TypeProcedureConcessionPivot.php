<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TypeProcedureConcessionPivot.
 *
 * @ORM\Table(name="type_procedure_concession_pivot")
 * @ORM\Entity
 */
class TypeProcedureConcessionPivot
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="libelle", type="string", length=255, nullable=false)
     */
    private ?string $libelle = null;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ContratTitulaire", mappedBy="idTypeProcedureConcessionPivot")
     */
    private Collection $contratTitulaires;

    /**
     * TypeContratConcessionPivot constructor.
     */
    public function __construct()
    {
        $this->contratTitulaires = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param string $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * @param $contratTitulaire
     *
     * @return $this
     */
    public function addContratTitulaire($contratTitulaire)
    {
        $this->contratTitulaires[] = $contratTitulaire;

        return $this;
    }

    /**
     * @param $contratTitulaire
     */
    public function removeContratTitulaire($contratTitulaire)
    {
        $this->contratTitulaires->removeElement($contratTitulaire);
    }

    /**
     * @return ArrayCollection
     */
    public function getContratTitulaires()
    {
        return $this->contratTitulaires;
    }
}
