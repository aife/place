<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PanierEntreprise.
 *
 * @ORM\Table(name="Panier_Entreprise")
 * @ORM\Entity(repositoryClass="App\Repository\PanierEntrepriseRepository")
 */
class PanierEntreprise
{
    /**
     *
     * @ORM\Column(name="organisme", type="string", length=30, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private ?string $organisme = null;

    /**
     *
     * @ORM\Column(name="consultation_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private ?int $consultationId = null;

    /**
     *
     * @ORM\Column(name="id_entreprise", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private ?int $idEntreprise = null;

    /**
     *
     * @ORM\Column(name="id_inscrit", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private ?int $idInscrit = null;

    /**
     * @ORM\Column(name="date_ajout", type="string", length=200, nullable=true)
     */
    private ?string $dateAjout = null;

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param string $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return int
     */
    public function getConsultationId()
    {
        return $this->consultationId;
    }

    /**
     * @param int $consultationId
     */
    public function setConsultationId($consultationId)
    {
        $this->consultationId = $consultationId;
    }

    /**
     * @return int
     */
    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    /**
     * @param int $idEntreprise
     */
    public function setIdEntreprise($idEntreprise)
    {
        $this->idEntreprise = $idEntreprise;
    }

    /**
     * @return int
     */
    public function getIdInscrit()
    {
        return $this->idInscrit;
    }

    /**
     * @param int $idInscrit
     */
    public function setIdInscrit($idInscrit)
    {
        $this->idInscrit = $idInscrit;
    }

    /**
     * @return string
     */
    public function getDateAjout()
    {
        return $this->dateAjout;
    }

    /**
     * @param string $dateAjout
     */
    public function setDateAjout($dateAjout)
    {
        $this->dateAjout = $dateAjout;
    }
}
