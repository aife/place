<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Handler;

use App\Entity\DCE;
use App\Repository\ConsultationRepository;
use App\Service\AtexoUtil;
use App\Service\Consultation\UploadDceHelper;
use App\Utils\Filesystem\MountManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class ListDceFilesHandler
{
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag,
        private readonly ConsultationRepository $consultationRepository,
        private readonly UploadDceHelper $uploadDceHelper,
        private readonly AtexoUtil $atexoUtil
    ) {
    }

    public function handle(?string $filePath, ?int $consultationId): JsonResponse
    {
        try {
            $data = $this->getZipArchiveFileData($filePath, $consultationId);

            if (!isset($data['zipArchiveFilePath']) || !$data['zipArchiveFilePath']) {
                return new JsonResponse(
                    "Une erreur s'est produite lors du chargement des fichiers de la consultation",
                    Response::HTTP_NOT_FOUND
                );
            }

            $zipArchive = new \ZipArchive();

            $zipArchive->open($data['zipArchiveFilePath']);
            $files = [];

            for ($i = 0; $i < $zipArchive->numFiles; $i++) {
                $file = $zipArchive->statIndex($i);

                $fileName = $this->atexoUtil->replaceCharactersMsWordWithRegularCharacters($file['name']);
                $files[] = iconv('UTF-8', 'ASCII//TRANSLIT', $fileName);
            }

            $zipArchive->close();
        } catch (\Exception $exception) {
            $this->logger->error(
                sprintf('An error occured when reading the DCE document archive : %s', $exception->getMessage())
            );

            return new JsonResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $finalTree = $this->buildFinalTree($this->explodeTreePaths($files));

        return new JsonResponse(
            $this->addHeaderToDirectoryTree(
                $finalTree,
                $data['zipArchiveFilePath'],
                $data['zipRealName']
            )
        );
    }

    private function explodeTreePaths(array $paths, string $delimiter = '/'): array
    {
        $splitRE = '/' . preg_quote($delimiter, '/') . '/';

        $tree = [];

        foreach ($paths as $path) {
            // Get parent parts and the current leaf
            $parts = preg_split($splitRE, $path, -1, PREG_SPLIT_NO_EMPTY);
            $leafPart = array_pop($parts);

            // Build the parent structure
            $parentArr = &$tree;
            foreach ($parts as $part) {
                if (!isset($parentArr[$part])) {
                    $parentArr[$part] = [];
                } elseif (!is_array($parentArr[$part])) {
                    $parentArr[$part] = [];
                }

                $parentArr = &$parentArr[$part];
            }

            // Add the final part to the structure
            if (empty($parentArr[$leafPart])) {
                $parentArr[$leafPart] = $path;
            }
        }

        return $tree;
    }

    private function buildFinalTree(array $tree): array
    {
        $finalTree = [];

        foreach ($tree as $key => $branch) {
            if (is_array($branch)) {
                $finalTree[] = [
                    'name' => $key,
                    'tree' => $this->buildFinalTree($branch)
                ];
            } else {
                $finalTree[] = [
                    'name' => $key,
                ];
            }
        }

        return $finalTree;
    }

    private function addHeaderToDirectoryTree(
        array $directoryTree,
        string $completeFilePath,
        string $zipRealName
    ): array {
        return [
            'name' => $zipRealName,
            'path' => $completeFilePath,
            'tree' => $directoryTree,
        ];
    }

    /**
     * @throws \Exception
     */
    private function getZipArchiveFileData(string|null $filePath, int $consultationId): array
    {
        $uploadFilePath = sprintf('%suploadFile/', $this->parameterBag->get('COMMON_TMP'));

        $data = [];
        if ($consultationId) {
            $consultation = $this->consultationRepository->find($consultationId);
            $lastDce = null;

            $dceId = 0;
            foreach ($consultation->getDce() as $dce) {
                if ($dce->getId() > $dceId) {
                    $lastDce = $dce;
                    $dceId = $dce->getId();
                }
            }
            /**
             * @var DCE|null $lastDce
             */
            if ($lastDce) {
                $data = [
                    "zipArchiveFilePath" => $this->uploadDceHelper->getFileByBlobId($lastDce->getDce()),
                    "zipRealName"   => $lastDce->getNomFichier()
                ];
            } else {
                $this->logger->error(
                    sprintf("Le DCE n'a pas été trouvé pour la consultation %s", $consultationId)
                );
            }
        } else {
            $data = [
                "zipArchiveFilePath" => sprintf('%s/%s', $uploadFilePath, $filePath),
                "zipRealName"   => basename($filePath)
            ];
        }

        if (
            !$data["zipArchiveFilePath"]
            || realpath($data["zipArchiveFilePath"]) === false
            || str_starts_with($filePath, $uploadFilePath)
        ) {
            $this->logger->error(
                sprintf("Le fichier n'a pas été trouvé pour la consultation %s", $consultationId)
            );
        }

        return $data;
    }
}
