<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Handler;

use App\Factory\Document\DocumentFactory;
use App\Service\AntivirusScanner;
use App\Service\AtexoFichierOrganisme;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use TusPhp\Tus\Server as TusServer;

final class UploadDocumentHandler
{
    public const DISALLOWED_MIME_TYPE =  [
        'application/x-msdownload',
        'application/x-dosexec',
        'text/javascript',
        'application/vbscript',
        'application/sql',
        'application/x-javascript',
        'application/x-shockwave-flash',
        'application/x-java-archive',
    ];

    public const DISALLOWED_EXTENSION = [
        'php',
        'asp',
        'jsp',
        'ini',
        'sh',
        'bat',
        'ps1'
    ];

    public const TUS_EVENT_UPLOAD_COMPLETE = 'tus-server.upload.complete';

    public function __construct(
        private readonly TusServer $tusServer,
        private readonly LoggerInterface $logger,
        private readonly UrlGeneratorInterface $urlGenerator,
        private readonly AntivirusScanner $antivirusScanner,
        private readonly string $isClamavEnabled,
        private readonly AtexoFichierOrganisme $atexoBlobOrganisme,
        private readonly ParameterBagInterface $parameterBag,
    ) {
    }

    public function handle(string $requestMethod): BinaryFileResponse|JsonResponse|Response
    {
        try {
            $targetDirectory = sprintf(
                '%s/%s',
                rtrim($this->tusServer->getUploadDir(), '/'),
                $this->tusServer->getUploadKey()
            );

            if (Request::METHOD_POST === $requestMethod && !file_exists($targetDirectory)) {
                mkdir($targetDirectory, 0777, true);
            }

            $this->tusServer->setUploadDir($targetDirectory);
            $this->tusServer->setApiPath($this->urlGenerator->generate('agent_document_upload'));
            $this->tusServer->setMaxUploadSize($this->parameterBag->get('maxBytesFilesSize'));

            $this->tusServer->event()->addListener(self::TUS_EVENT_UPLOAD_COMPLETE, function ($event) {
                $file = $event->getFile();
                $filePath = $file->getFilePath();
                $this->isAuthorizedFile($file);

                if (filter_var($this->isClamavEnabled, FILTER_VALIDATE_BOOLEAN)) {
                    $isFileInfected = $this->antivirusScanner->scanFile($filePath);

                    if ($isFileInfected) {
                        $file->delete([$filePath], true);

                        throw new \Exception('File is infected');
                    }
                }
            });

            return $this->tusServer->serve();
        } catch (\Exception $exception) {
            $this->logger->error(
                sprintf('An error occured during document upload : %s', $exception->getMessage())
            );

            return new JsonResponse([
                'error' => $exception->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    private function isAuthorizedFile($file): void
    {
        $fileSize = $file->getFileSize();
        $filePath = $file->getFilePath();
        $fileMimeType = mime_content_type($filePath);
        $fileInfo = new File($filePath);
        $fileExtension = $fileInfo->getExtension();

        if ($this->isDisallowedMimeType($fileMimeType)) {
            $file->delete([$filePath], true);

            throw new \Exception(sprintf('Invalid file MIME type (%s)', $fileMimeType));
        }

        if (!$this->isAuthorizedFileSize($fileSize)) {
            $file->delete([$filePath], true);

            throw new \Exception(sprintf('Invalid file size (%s)', $fileSize));
        }

        if ($this->isDisallowedExtension($fileExtension)) {
            $file->delete([$filePath], true);
            throw new \Exception(sprintf('Invalid extension (%s)', $fileExtension));
        }
    }

    private function isAuthorizedFileSize(int $fileSize): bool
    {
        return $fileSize <= $this->parameterBag->get('maxBytesFilesSize');
    }

    private function isDisallowedMimeType(string $mimeType): bool
    {
        return in_array($mimeType, self::DISALLOWED_MIME_TYPE);
    }

    private function isDisallowedExtension(string $extension): bool
    {
        return in_array($extension, self::DISALLOWED_EXTENSION);
    }

    /**
     * @throws \Exception
     */
    public function createBlobFile(
        string $fileName,
        string $fileSystemPath,
        string $organismeAcronyme,
        string $documentType
    ) {
        $fileExtension = match ($documentType) {
            DocumentFactory::DOCUMENT_TYPE_DCE          => $this->parameterBag->get('EXTENSION_DCE'),
            DocumentFactory::DOCUMENT_TYPE_RC           => $this->parameterBag->get('EXTENSION_RC'),
            DocumentFactory::DOCUMENT_TYPE_AUTRE_PIECE  => '',
            default => throw new \Exception(sprintf('Invalid document type (%s)', $documentType)),
        };

        return $this->atexoBlobOrganisme->insertFile(
            $fileName,
            $fileSystemPath,
            $organismeAcronyme,
            null,
            $fileExtension
        );
    }
}
