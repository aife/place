<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class CertificatsController extends AbstractController
{
    /**
     * @Route("/agent/aide/certificats", name="certificats_")
     *
     * @return Response
     */
    public function indexCertificatsAction()
    {
        return $this->render('agent/aide/certificats.html.twig', [
            'isFullWidth' => false
        ]);
    }

    /**
     * @Route("/agent/aide/documents-de-reference", name="documents_reference_")
     *
     * @return Response
     */
    public function indexDocumentsReferenceAction()
    {
        return $this->render('agent/aide/documents-de-reference.html.twig', [
            'isFullWidth' => false
        ]);
    }

}
