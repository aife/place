<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use App\Entity\Agent;
use App\Entity\Agent\NotificationAgent;
use App\Service\Agent\AgentNotificationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Route(path: '/agent/notification', name: 'agent_notification_')]
class NotificationController extends AbstractController
{
    #[Route(path: '/count', name: 'count', options: ['expose' => true])]
    public function unreadCount(AgentNotificationService $notificationService) : JsonResponse
    {
        /** @var ?UserInterface $user */
        $user = $this->getUser();
        if ($user instanceof Agent) {
            return new JsonResponse($notificationService->countUnread($user));
        }
        return new JsonResponse(false);
    }
    #[Route(path: '/list', name: 'list', options: ['expose' => true])]
    public function list(AgentNotificationService $notificationService) : JsonResponse
    {
        /** @var ?UserInterface $user */
        $user = $this->getUser();
        if ($user instanceof Agent) {
            return new JsonResponse($this->renderView('agent/notification/list.html.twig', [
                'list' => $notificationService->listUnread($user),
            ]));
        }
        return new JsonResponse(false);
    }
    #[Route(path: '/{id}/read', name: 'read', options: ['expose' => true])]
    public function read(NotificationAgent $notification, AgentNotificationService $notificationService) : JsonResponse
    {
        $notificationService->listRead($notification);
        return new JsonResponse();
    }
    #[Route(path: '/{id}/disable', name: 'disable', options: ['expose' => true])]
    public function disable(NotificationAgent $notification, AgentNotificationService $notificationService) : JsonResponse
    {
        $notificationService->disableNotification($notification);
        return new JsonResponse();
    }
}
