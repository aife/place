<?php

namespace App\Controller\Agent;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\MessageCatalogueInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class TranslationController extends AbstractController
{
    #[Route(
        path: '/agent/translation/messages',
        name: 'agent_translation_messages',
        methods: ['GET']
    )]
    public function getAllTranslations(TranslatorInterface $translator): Response
    {
        /** @var MessageCatalogueInterface $catalogue */
        $catalogue = $translator->getCatalogue($translator->getLocale());
        $messages = $catalogue->all('messages');

        return $this->json($messages);
    }
}
