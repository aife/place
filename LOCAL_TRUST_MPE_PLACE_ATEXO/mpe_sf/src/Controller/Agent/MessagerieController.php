<?php

namespace App\Controller\Agent;

use App\Entity\OffrePapier;
use DateTime;
use DateTimeInterface;
use DateTimeZone;
use App\Controller\AtexoController;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Consultation\InterneConsultationSuiviSeul;
use App\Entity\DossierVolumineux\DossierVolumineux;
use App\Entity\HabilitationAgent;
use App\Entity\Messagerie\MailTemplate;
use App\Entity\Offre;
use App\Entity\QuestionDCE;
use App\Entity\RelationEchange;
use App\Exception\ClientWsException;
use App\Service\Agent\Habilitation;
use App\Service\AtexoConfiguration;
use App\Service\AtexoUtil;
use App\Service\Authorization\AuthorizationAgent;
use App\Service\CurrentUser;
use App\Service\Messagerie\MessagerieService;
use App\Service\Messagerie\WebServicesMessagerie;
use App\Service\SearchAgent;
use App\Utils\Encryption;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Exception;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use ReflectionException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Error\Error;

/**
 * Class MessagerieController.
 **/
#[Route(path: '/agent/messagerie', name: 'messagerie_')]
class MessagerieController extends AtexoController
{
    public final const ERROR_TECHNIQUE = 'Une erreur technique s\'est produite.';
    public final const ERROR_WS = 'Problème avec l\'api ou le WS.';
    public final const METHOD_INIT_SUIVI_TOKEN = 'initialisationSuiviToken';
    public final const METHOD_INIT_REDACTION_TOKEN = 'initialisationRedactionToken';
    public final const ALLOWED_FILTERS = [
        "reponse-attendue",
        "reponse-non-lue",
        "statut-non-delivre"
    ];
    /**
     * MessagerieController constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly SessionInterface $session,
        protected LoggerInterface $logger,
        protected TranslatorInterface $translator,
        private readonly MessagerieService $messagerieService,
        private readonly SearchAgent $searchAgent,
        private readonly AuthorizationAgent $perimetreAgent,
        private readonly Encryption $encryption,
        private readonly AtexoUtil $utilitaire,
        private readonly Security $security,
        private readonly WebServicesMessagerie $webServicesMessagerie,
        private AtexoConfiguration $atexoConfiguration,
        private CurrentUser $currentUser,
        private ParameterBagInterface $parameterBag
    ) {
    }

    /**
     * @param Request $request
     * @param AtexoConfiguration $atexoConfiguration
     * @param Habilitation $habilitation
     * @return Response
     * @throws Error
     */
    #[Route(path: '/redaction/{consultationId}', name: 'new_message', methods: ['GET'])]
    public function newMessageAction(Request $request, AtexoConfiguration $atexoConfiguration, Habilitation $habilitation) : Response
    {
        $titrePage = $this->translator->trans('NEW_MESSAGE');
        $consultationIdCrypte = $request->get('consultationId');
        $consultationId = $this->encryption->decryptId($consultationIdCrypte);
        if (!$this->perimetreAgent->isInTheScope($consultationId)) {
            $this->logger->warning(
                sprintf(
                    AuthorizationAgent::CONSULTATION_IS_NOT_IN_PERIMETRE,
                    $consultationId,
                    $this->getUser()->getId()
                )
            );
            return $this->redirectAccueil();
        } else {
            $reponse = [
                'token' => null,
                'context' => null,
                'error' => null,
                'status' => Response::HTTP_OK,
            ];
            $dossiersVolumineux = [];

            try {
                $consultation = $this->em
                    ->getRepository(Consultation::class)
                    ->find($consultationId);

                if (
                    $atexoConfiguration->hasConfigOrganisme($this->getUser()->getOrganisme(), 'moduleEnvol')
                    && $habilitation->checkHabilitation($this->getUser(), 'isGestionEnvol')
                    && $consultation->isEnvolActivation()
                ) {
                    $dossiersVolumineux = $this->em
                        ->getRepository(DossierVolumineux::class)
                        ->getDataForAgentMessecV2($this->getUser()->getId());
                }

                $reponse['token'] = $this->getTokenMessage($consultationId, [
                    'id_contrat' => $consultation->getIdContrat(),
                ], $dossiersVolumineux);

                $messagerieSuiviMessage = $this->generateUrl(
                    'messagerie_suivi_message',
                    ['consultationId' => $consultationIdCrypte],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );

                $asHabilitationEspaceDoc = $this->checkHabilitationEspaceDoc();

                $render = $this->render('messagerie/newMessage.html.twig', [
                    'token' => $reponse['token'],
                    'suivi_url' => $messagerieSuiviMessage,
                    'url_front' => $this->utilitaire
                        ->getPfUrlDomaine($this->parameterBag->get('URL_MESSAGERIE_SECURISEE')),
                    'consultation' => $consultation,
                    'titre_page' => $titrePage,
                    'espace_doc' => $asHabilitationEspaceDoc,
                ]);
            } catch (ClientWsException $clientException) {
                $this->logger(self::ERROR_WS, $clientException);
                $reponse['status'] = $clientException->getCode();
                $reponse['error'] = utf8_encode($clientException->getMessage());
                $reponse['token'] = null;
                $render = $this->render('messagerie/error.html.twig', $reponse);
            } catch (\Error $error) {
                $this->logger(self::ERROR_TECHNIQUE, $error);
                $reponse['status'] = 500;
                $reponse['error'] = $this->translator->trans('ERROR_TECHNIQUE');
                unset($reponse['token']);
                $render = $this->render('messagerie/error.html.twig', $reponse);
            }
        }
        return $render;
    }

    /**
     * @param $consultationId
     * @param array $optionalParameters
     * @param array $dossiersVolumineux
     * @return string
     * @throws Error
     * @throws \JsonException
     * @throws ReflectionException
     */
    public function getTokenMessage(
        $consultationId,
        $optionalParameters = [],
        array $dossiersVolumineux = []
    ): string {
        $questions = [];
        $id = $this->getConnectedUserId();
        $typeUser = $this->getConnectedUserType();
        $destinatairesPfDestinataire = $nomOrganisme = null;

        $consultation = $this->em
            ->getRepository(Consultation::class)
            ->find($consultationId);

        if ('agent' === $typeUser) {
            $agent = $this->em->getRepository(Agent::class)->find($id);
            $destinatairesPfDestinataire['destinatairePfDestinataire'] = $this
                ->getListDestinataires($agent, $consultation, $optionalParameters);
            $nomOrganisme = $agent->getOrganisme()->getDenominationOrg();
        }

        $cartouche = $this->messagerieService->getCartoucheContent($consultationId);

        $consultationIdCrypte = $this->encryption->cryptId($consultationId);
        $urlEntreprise = $this->parameterBag->get('URL_PF_DESTINATAIRE_VISUALISATION')
            . '/' . $consultationIdCrypte;

        $logo = null;
        if (!empty($this->parameterBag->get('SOURCE_IMAGE_BANDEAU'))) {
            $logo = $this->utilitaire->getPfUrlDomaine($this->parameterBag->get('PF_URL_REFERENCE'))
                . $this->parameterBag->get('SOURCE_IMAGE_BANDEAU');
        }

        $params = [
            'idObjetMetierPfEmetteur' => $consultationId,
            'idPfEmetteur' => $this->parameterBag->get('MESSAGERIE_ID_PF_EMETTEUR'),
            'urlPfEmetteur' => $this->parameterBag->get('PF_URL_MESSAGERIE_NEW_MESSAGE'),
            'urlPfDestinataire' => $this->parameterBag->get('PF_URL_MESSAGERIE_NEW_MESSAGE'),
            'emailExpediteur' => $this->parameterBag->get('PF_MAIL_FROM'),
            'nomPfEmetteur' => $this->parameterBag->get('MESSAGERIE_NOM_PF_EMETTEUR'),
            'destinatairesPfDestinataire' => $destinatairesPfDestinataire,
            'idObjetMetierPfDestinataire' => $consultationId,
            'cartouche' => $cartouche,
            'idPfDestinataire' => $this->parameterBag->get('MESSAGERIE_ID_PF_EMETTEUR'),
            'nomPfDestinataire' => $this->parameterBag->get('MESSAGERIE_ID_PF_EMETTEUR'),
            'logoSrc' => $logo,
            'signatureAvisPassage' => $this->parameterBag->get('PF_LONG_NAME'),
            'nomCompletExpediteur' => $nomOrganisme,
            'urlPfDestinataireVisualisation' => $urlEntreprise,
            'urlPfReponse' => $this->parameterBag->get('URL_PF_REPONSE'),
            'refObjetMetier' => $consultation->getReferenceUtilisateur(),
            'metaDonnees' => $this->messagerieService->getMetaDonnees($consultation),
            'dossiersVolumineux' => $dossiersVolumineux,
        ];

        if (array_key_exists('codeLien', $optionalParameters)) {
            $params['codeLien'] = $optionalParameters['codeLien'];
        }

        if (array_key_exists('offreId', $optionalParameters)) {
            if (array_key_exists('papier', $optionalParameters)) {
                $offre = $this->em
                    ->getRepository(OffrePapier::class)
                    ->find($optionalParameters['offreId']);

                $params['destinatairesPreSelectionnes']['destinatairePreSelectionne'] = [
                    [
                        'type' => 'Registre des dépôts',
                        'idEntrepriseDest' => $offre->getEntrepriseId(),
                        'mailContactDestinataire' => $offre->getEmail(),
                        'nomContactDest' => $offre->getNom().' '.$offre->getPrenom(),
                        'nomEntrepriseDest' => $offre->getNomEntreprise(),
                    ],
                ];
            } else {
                $offre = $this->em
                    ->getRepository(Offre::class)
                    ->find($optionalParameters['offreId']);

                $params['destinatairesPreSelectionnes']['destinatairePreSelectionne'] = [
                    [
                        'type' => 'Registre des dépôts',
                        'idContactDest' => $offre->getInscritId(),
                        'idEntrepriseDest' => $offre->getEntrepriseId(),
                        'mailContactDestinataire' => $offre->getMailsignataire(),
                        'nomContactDest' => $offre->getNomInscrit().' '.$offre->getPrenomInscrit(),
                        'nomEntrepriseDest' => $offre->getNomEntrepriseInscrit(),
                    ],
                ];
            }
        }

        if (array_key_exists('allRegistre', $optionalParameters)) {
            $params['destinatairesPreSelectionnes']['destinatairePreSelectionne'] =
                $destinatairesPfDestinataire['destinatairePfDestinataire'];
        }

        if (array_key_exists('questionId', $optionalParameters)) {
            $questionDce = $this->em
                ->getRepository(QuestionDCE::class)
                ->find($optionalParameters['questionId']);

            $questions[] = [
                'type' => 'Registre des questions',
                'idContactDest' => $questionDce->getInscrit()?->getId(),
                'idEntrepriseDest' => $questionDce->getIdEntreprise(),
                'mailContactDestinataire' => $questionDce->getEmail(),
                'nomContactDest' => $questionDce->getNom() . ' ' . $questionDce->getPrenom(),
                'nomEntrepriseDest' => $questionDce->getEntreprise(),
            ];
            $params['destinatairesPreSelectionnes']['destinatairePreSelectionne'] = $questions;
        }

        if (array_key_exists('invitationAConcourir', $optionalParameters)) {
            $contratId = $optionalParameters['id_contrat'] ?? 0;
            $destinatairesIssusPhasePrecedente = ($contratId > 0) ?
                $this->messagerieService->getContactForConsultationContract($contratId) :
                $this->messagerieService->getListeDestinatairesIssusPhasePrecedente($consultation);

            $params['destinatairesPreSelectionnes']['destinatairePreSelectionne'] = $destinatairesIssusPhasePrecedente;
        }

        return $this->webServicesMessagerie->getContent(self::METHOD_INIT_REDACTION_TOKEN, $params);
    }
    /**
     * @return array
     */
    public function getListDestinataires(Agent $agent, Consultation $consultation, $optionalParameters = [])
    {
        $consultationId = $consultation->getId();
        $contratId = $optionalParameters['id_contrat'] ?? 0;
        $organisme = $agent->getOrganisme()->getAcronyme();

        $listRegistreQuestions = $this->messagerieService
            ->getListRegistreQuestions($consultationId, $organisme);

        $listRegistreDepots = $this->messagerieService
            ->getListRegistreDepots($consultationId, $organisme);

        $listRegistreRetraits = $this->messagerieService
            ->getListRegistreRetraits($consultationId, $organisme);

        $destinatairesIssusPhasePrecedente = ($contratId > 0) ?
            $this->messagerieService->getContactForConsultationContract($contratId) :
            $this->messagerieService->getListeDestinatairesIssusPhasePrecedente($consultation);

        /**
         * @todo : code a revoir (refacto)
         */
        $destinataires = array_merge(
            $destinatairesIssusPhasePrecedente,
            $listRegistreRetraits,
            $listRegistreQuestions,
            $listRegistreDepots
        );

        $listeDestinataires = array_map(
            'unserialize',
            array_unique(array_map(
                'serialize',
                $destinataires
            ))
        );

        $destinataires = [];
        $i = 1;
        foreach ($listeDestinataires as $destinataire) {
            $destinataire['ordre'] = $i;
            ++$i;
            $destinataires[] = $destinataire;
        }

        return $destinataires;
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws ReflectionException
     * @throws \JsonException
     */
    #[Route(path: '/suivi/{consultationId}', name: 'suivi_message', methods: ['GET'])]
    public function suiviMessageAction(Request $request)
    {
        $reponse = [];
        $options = [];
        $titrePage = $this->translator->trans('SUIVI_MESSAGE');
        $consultationIdCrypte = $request->get('consultationId');
        $consultationId = $this->encryption->decryptId($consultationIdCrypte);
        $consultation = $this->em
            ->getRepository(Consultation::class)
            ->find($consultationId);
        $params = [
            'idObjetMetier' => $consultationId,
            'refObjetMetier' => $consultation->getReferenceUtilisateur(),
            'nomPfEmetteur' => $this->parameterBag->get('MESSAGERIE_NOM_PF_EMETTEUR'),
            'typePlateformeRecherche' => 'DESTINATAIRE',
        ];
        if (!$this->perimetreAgent->isInTheScope($consultationId)) {
            $this->logger->warning(
                sprintf(
                    AuthorizationAgent::CONSULTATION_IS_NOT_IN_PERIMETRE,
                    $consultationId,
                    $this->getUser()->getId()
                )
            );
            return $this->redirectAccueil();
        } else {
            try {
                $reponse['token'] =
                    $this->webServicesMessagerie->getContent(self::METHOD_INIT_SUIVI_TOKEN, $params);
                $envoyerMessage = $this->currentUser->checkHabilitation('EnvoyerMessage');

                //Invité ponctuel
                $suiviSeul = $this->em->getRepository(InterneConsultationSuiviSeul::class)->findOneBy([
                    'consultation' => $consultationId,
                    'agent' => $this->getConnectedUserId(),
                ]);
                $messagerieNewMessage = $messagerieModificationMessageBrouillon = null;
                if ($envoyerMessage && empty($suiviSeul)) {
                    $messagerieNewMessage = $this->generateUrl(
                        'messagerie_new_message',
                        ['consultationId' => $consultationIdCrypte],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    );

                    $messagerieModificationMessageBrouillon = $this->generateUrl(
                        'messagerie_modification_message_brouillon_consultation',
                        [
                            'consultationId' => $consultationIdCrypte,
                            'codeLien' => 'codeLien',
                        ],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    );

                    $messagerieModificationMessageBrouillon = str_replace(
                        'codeLien',
                        '',
                        $messagerieModificationMessageBrouillon
                    );
                }

                $render = $this->render('messagerie/suiviMessage.html.twig', [
                    'token' => $reponse['token'],
                    'url_nouveau_message' => $messagerieNewMessage,
                    'url_front' => $this->utilitaire
                        ->getPfUrlDomaine($this->parameterBag->get('URL_MESSAGERIE_SECURISEE')),
                    'consultation' => $consultation,
                    'titre_page' => $titrePage,
                    'url_modification_message' => $messagerieModificationMessageBrouillon,
                ]);
            } catch (ClientWsException $clientException) {
                $this->logger(self::ERROR_WS, $clientException);
                $reponse['status'] = $clientException->getCode();
                $reponse['error'] = utf8_encode($clientException->getMessage());
                $reponse['token'] = null;
                $render = $this->render('messagerie/error.html.twig', $reponse);
            } catch (\Error $error) {
                $this->logger(self::ERROR_TECHNIQUE, $error);
                $reponse['status'] = 500;
                $reponse['error'] = $this->translator->trans('ERROR_TECHNIQUE');
                unset($reponse['token']);
                $render = $this->render('messagerie/error.html.twig', $reponse);
            }
        }
        return $render;
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Error
     * @throws ReflectionException
     * @throws \JsonException
     */
    #[Route(path: '/redaction/modification-consultation/{consultationId}', name: 'new_message_modication_consultation', methods: ['GET'])]
    public function newMessageModiciationConsultationAction(Request $request)
    {
        $optionalParameters = [];
        $titrePage = $this->translator->trans('NEW_MESSAGE_MODIFICATION_CONSULTATION');
        $consultationIdCrypte = $request->get('consultationId');
        $consultationId = $this->encryption->decryptId($consultationIdCrypte);
        $organisme = null;
        if (null === $consultationId) {
            return $this->redirectAccueil();
        } else {
            $reponse = [
                'token' => null,
                'context' => null,
                'error' => null,
                'status' => Response::HTTP_OK,
            ];

            try {
                $optionalParameters['allRegistre'] = true;
                $reponse['token'] = $this->getTokenMessage(
                    $consultationId,
                    $optionalParameters
                );
                $messagerieSuiviMessage = $this->generateUrl(
                    'messagerie_suivi_message',
                    ['consultationId' => $consultationIdCrypte],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );

                $idTemplate = $this->em->getRepository(MailTemplate::class)
                    ->findOneBy([
                        'code' => $this->getParameter('CODE_MAIL_MODIFICATION_CONSULTATION'),
                    ]);

                $render = $this->getRender(
                    $reponse,
                    $idTemplate,
                    $consultationId,
                    $messagerieSuiviMessage,
                    $titrePage
                );
            } catch (ClientWsException $clientException) {
                $this->logger(self::ERROR_WS, $clientException);
                $reponse['status'] = $reponse['token']->getCode();
                $reponse['error'] = utf8_encode($reponse['token']->getMessage());
                $reponse['token'] = null;
                $render = $this->render('messagerie/error.html.twig', $reponse);
            } catch (\Error $error) {
                $this->logger(self::ERROR_TECHNIQUE, $error);
                $reponse['status'] = 500;
                $reponse['error'] = $this->translator->trans('ERROR_TECHNIQUE');
                unset($reponse['token']);
                $render = $this->render('messagerie/error.html.twig', $reponse);
            }
        }
        return $render;
    }

    /**
     * @param $reponse
     * @param $idTemplate
     * @param $consultationId
     * @param $messagerieSuiviMessage
     * @param $titrePage
     * @param array $option
     * @return Response
     */
    public function getRender($reponse, $idTemplate, $consultationId, $messagerieSuiviMessage, $titrePage, $option = [])
    {
        $asHabilitationEspaceDoc = $this->checkHabilitationEspaceDoc();

        $consultation = $this->em
            ->getRepository(Consultation::class)
            ->find($consultationId);
        $paramBase = [
            'token' => $reponse['token'],
            'suivi_url' => $messagerieSuiviMessage,
            'id_template_selectionne' => $idTemplate->getId(),
            'url_front' => $this->utilitaire
                ->getPfUrlDomaine($this->parameterBag->get('URL_MESSAGERIE_SECURISEE')),
            'consultation' => $consultation,
            'titre_page' => $titrePage,
            'espace_doc' => $asHabilitationEspaceDoc,
        ];
        $params = array_merge($paramBase, $option);

        return $this->render('messagerie/newMessage.html.twig', $params);
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Error
     * @throws ReflectionException
     * @throws \JsonException
     */
    #[Route(path: '/redaction/reponse-a-une-question/{consultationId}/{questionId}', name: 'reponse_question_consultation', methods: ['GET'])]
    public function reponseQuestionConsultationAction(Request $request)
    {
        $titrePage = $this->translator->trans('REPONSE_QUESTION_CONSULTATION');
        $consultationIdCrypte = $request->get('consultationId');
        $consultationId = $this->encryption->decryptId($consultationIdCrypte);
        $questionIdCrypte = $request->get('questionId');
        $questionId = $this->encryption->decryptId($questionIdCrypte);
        $organisme = null;
        if (!$this->getConnectedUserId() || null === $consultationId) {
            return $this->redirectAccueil();
        } else {
            $reponse = [
                'token' => null,
                'context' => null,
                'error' => null,
                'status' => Response::HTTP_OK,
            ];

            $params = [
                'questionId' => $questionId,
            ];

            try {
                $reponse['token'] = $this->getTokenMessage($consultationId, $params);
                $messagerieSuiviMessage = $this->generateUrl(
                    'messagerie_suivi_message',
                    ['consultationId' => $consultationIdCrypte],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );

                $idTemplate = $this->em->getRepository(MailTemplate::class)
                    ->findOneBy([
                        'code' => $this->getParameter('REPONSE_QUESTION'),
                    ]);

                $urlCallbackReponseAQuestion = $this->generateUrl(
                    'messagerie_callback_reponse_question_consultation',
                    ['consultationId' => $consultationIdCrypte, 'questionId' => $questionIdCrypte],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );
                $render = $this->getRender(
                    $reponse,
                    $idTemplate,
                    $consultationId,
                    $messagerieSuiviMessage,
                    $titrePage,
                    [
                        'url_callback_reponse_a_question' => $urlCallbackReponseAQuestion,
                    ]
                );
            } catch (ClientWsException $clientException) {
                $this->logger(self::ERROR_WS, $clientException);
                $reponse['status'] = $clientException->getCode();
                $reponse['error'] = utf8_encode($clientException->getMessage());
                $reponse['token'] = null;
                $render = $this->render('messagerie/error.html.twig', $reponse);
            } catch (\Error $error) {
                $this->logger(self::ERROR_TECHNIQUE, $error);
                $reponse['status'] = 500;
                $reponse['error'] = $this->translator->trans('ERROR_TECHNIQUE');
                unset($reponse['token']);
                $render = $this->render('messagerie/error.html.twig', $reponse);
            }
        }
        return $render;
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Error
     * @throws ReflectionException
     * @throws \JsonException
     */
    #[Route(path: '/redaction/invitation-a-concourir/{consultationId}', name: 'invitation_concourir_consultation', methods: ['GET'])]
    public function invitationConcourirConsultationAction(Request $request)
    {
        $titrePage = $this->translator->trans('INVITATION_CONCOURIR_CONSULTATION');
        $consultationIdCrypte = $request->get('consultationId');
        $consultationId = $this->encryption->decryptId($consultationIdCrypte);
        $organisme = null;
        $consultation = $this->em
            ->getRepository(Consultation::class)
            ->find($consultationId);
        if (!$this->getConnectedUserId() || null === $consultationId) {
            return $this->redirectAccueil();
        } else {
            $reponse = [
                'token' => null,
                'context' => null,
                'error' => null,
                'status' => Response::HTTP_OK,
            ];

            try {
                $params = [
                    'invitationAConcourir' => $consultationId,
                    'id_contrat' => $consultation->getIdContrat(),
                ];

                $reponse['token'] = $this->getTokenMessage($consultationId, $params);
                $messagerieSuiviMessage = $this->generateUrl(
                    'messagerie_suivi_message',
                    ['consultationId' => $consultationIdCrypte],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );
                $idTemplate = $this->em->getRepository(MailTemplate::class)
                    ->findOneBy([
                        'code' => $this->getParameter('INVITATION_CONCOURIR'),
                    ]);

                $render = $this->getRender(
                    $reponse,
                    $idTemplate,
                    $consultationId,
                    $messagerieSuiviMessage,
                    $titrePage
                );
            } catch (ClientWsException $clientException) {
                $this->logger(self::ERROR_WS, $clientException);
                $reponse['status'] = $clientException->getCode();
                $reponse['error'] = utf8_encode($clientException->getMessage());
                $reponse['token'] = null;
                $render = $this->render('messagerie/error.html.twig', $reponse);
            } catch (\Error $error) {
                $this->logger(self::ERROR_TECHNIQUE, $error);
                $reponse['status'] = 500;
                $reponse['error'] = $this->translator->trans('ERROR_TECHNIQUE');
                unset($reponse['token']);
                $render = $this->render('messagerie/error.html.twig', $reponse);
            }
        }
        return $render;
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Error
     * @throws ReflectionException
     * @throws \JsonException
     */
    #[Route(path: '/redaction/demande-complement/{consultationId}/{offreId}', name: 'demande_complement_consultation', methods: ['GET'])]
    public function demandeComplementConsultationAction(Request $request)
    {
        $titrePage = $this->translator->trans('DEMANDE_DE_COMPLEMENT');
        $consultationIdCrypte = $request->get('consultationId');
        $consultationId = $this->encryption->decryptId($consultationIdCrypte);
        $offreIdCrypte = $request->get('offreId');
        $offreId = $this->encryption->decryptId($offreIdCrypte);
        $organisme = null;
        if (!$this->getConnectedUserId() || null === $consultationId) {
            return $this->redirectAccueil();
        } else {
            $reponse = [
                'token' => null,
                'context' => null,
                'error' => null,
                'status' => Response::HTTP_OK,
            ];

            $params = [
                'offreId' => $offreId,
            ];

            try {
                $reponse['token'] = $this->getTokenMessage($consultationId, $params);
                $messagerieSuiviMessage = $this->generateUrl(
                    'messagerie_suivi_message',
                    ['consultationId' => $consultationIdCrypte],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );
                $idTemplate = $this->em->getRepository(MailTemplate::class)
                    ->findOneBy([
                        'code' => $this->getParameter('MESSEC_DEMANDE_COMPLEMENT'),
                    ]);
                $render = $this->getRender(
                    $reponse,
                    $idTemplate,
                    $consultationId,
                    $messagerieSuiviMessage,
                    $titrePage
                );
            } catch (ClientWsException $clientException) {
                $this->logger(self::ERROR_WS, $clientException);
                $reponse['status'] = $clientException->getCode();
                $reponse['error'] = utf8_encode($clientException->getMessage());
                $reponse['token'] = null;
                $render = $this->render('messagerie/error.html.twig', $reponse);
            } catch (\Error $error) {
                $this->logger(self::ERROR_TECHNIQUE, $error);
                $reponse['status'] = 500;
                $reponse['error'] = $this->translator->trans('ERROR_TECHNIQUE');
                unset($reponse['token']);
                $render = $this->render('messagerie/error.html.twig', $reponse);
            }
        }
        return $render;
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Error
     * @throws ReflectionException
     * @throws \JsonException
     */
    #[Route(path: '/redaction/courrier-notification/{consultationId}/{offreId}', name: 'courrier_notification_consultation', methods: ['GET'])]
    public function courrierNotificationConsultationAction(Request $request)
    {
        $titrePage = $this->translator->trans('COURRIER_DE_NOTIFICATION');
        $consultationIdCrypte = $request->get('consultationId');
        $consultationId = $this->encryption->decryptId($consultationIdCrypte);
        $offreIdCrypte = $request->get('offreId');
        $offreId = $this->encryption->decryptId($offreIdCrypte);
        $organisme = null;
        if (!$this->getConnectedUserId() || null === $consultationId) {
            return $this->redirectAccueil();
        } else {
            $reponse = [
                'token' => null,
                'context' => null,
                'error' => null,
                'status' => Response::HTTP_OK,
            ];

            $params = [
                'offreId' => $offreId,
            ];

            try {
                $reponse['token'] = $this->getTokenMessage($consultationId, $params);
                $messagerieSuiviMessage = $this->generateUrl(
                    'messagerie_suivi_message',
                    ['consultationId' => $consultationIdCrypte],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );
                $idTemplate = $this->em->getRepository(MailTemplate::class)
                    ->findOneBy([
                        'code' => $this->getParameter('COURRIER_NOTIFICATION'),
                    ]);
                $render = $this->getRender(
                    $reponse,
                    $idTemplate,
                    $consultationId,
                    $messagerieSuiviMessage,
                    $titrePage
                );
            } catch (ClientWsException $clientException) {
                $this->logger(self::ERROR_WS, $clientException);
                $reponse['status'] = $clientException->getCode();
                $reponse['error'] = utf8_encode($clientException->getMessage());
                $reponse['token'] = null;
                $render = $this->render('messagerie/error.html.twig', $reponse);
            } catch (\Error $error) {
                $this->logger(self::ERROR_WS, $error);
                $reponse['status'] = 500;
                $reponse['error'] = $this->translator->trans('ERROR_TECHNIQUE');
                unset($reponse['token']);
                $render = $this->render('messagerie/error.html.twig', $reponse);
            }
        }
        return $render;
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Error
     * @throws ReflectionException
     * @throws \JsonException
     */
    #[Route(path: '/redaction/courrier-rejet/{consultationId}/{offreId}', name: 'courrier_rejet_consultation', methods: ['GET'])]
    public function courrierNotificationRejetConsultationAction(Request $request)
    {
        $titrePage = $this->translator->trans('COURRIER_DE_NOTIFICATION_REJET');
        $consultationIdCrypte = $request->get('consultationId');
        $consultationId = $this->encryption->decryptId($consultationIdCrypte);
        $offreIdCrypte = $request->get('offreId');
        $offreId = $this->encryption->decryptId($offreIdCrypte);
        $organisme = null;
        if (!$this->getConnectedUserId() || null === $consultationId) {
            return $this->redirectAccueil();
        } else {
            $reponse = [
                'token' => null,
                'context' => null,
                'error' => null,
                'status' => Response::HTTP_OK,
            ];

            $params = [
                'offreId' => $offreId,
            ];

            try {
                $reponse['token'] = $this->getTokenMessage($consultationId, $params);
                $messagerieSuiviMessage = $this->generateUrl(
                    'messagerie_suivi_message',
                    ['consultationId' => $consultationIdCrypte],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );

                $idTemplate = $this->em->getRepository(MailTemplate::class)
                    ->findOneBy([
                        'code' => $this->getParameter('COURRIER_REJET'),
                    ]);

                $render = $this->getRender(
                    $reponse,
                    $idTemplate,
                    $consultationId,
                    $messagerieSuiviMessage,
                    $titrePage
                );
            } catch (ClientWsException $clientException) {
                $this->logger(self::ERROR_TECHNIQUE, $clientException);
                $reponse['status'] = $clientException->getCode();
                $reponse['error'] = utf8_encode($clientException->getMessage());
                unset($reponse['token']);
                $render = $this->render('messagerie/error.html.twig', $reponse);
            } catch (\Error $error) {
                $this->logger(self::ERROR_WS, $error);
                $reponse['status'] = 500;
                $reponse['error'] = $this->translator->trans('ERROR_TECHNIQUE');
                unset($reponse['token']);
                $render = $this->render('messagerie/error.html.twig', $reponse);
            }
        }
        return $render;
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Atexo_Consultation_Exception
     * @throws Error
     * @throws ReflectionException
     * @throws \JsonException
     */
    #[Route(path: '/redaction/modification-message/{consultationId}/{codeLien}', name: 'modification_message_brouillon_consultation', methods: ['GET'])]
    public function modificationMessageConsultationAction(Request $request)
    {
        $paramOptionnel = [];
        $titrePage = $this->translator->trans('MODIFICATION_BROUILLON_MESSAGE');
        $codeLien = $request->get('codeLien');
        $consultationIdCrypte = $request->get('consultationId');
        $consultationId = $this->encryption->decryptId($consultationIdCrypte);
        $auth = $this->perimetreAgent->isAuthorized($consultationId);
        $listRegistreQuestions = $listRegistreDepots = $listRegistreRetraits = null;
        if (false === $auth || !$this->getConnectedUserId() || null === $consultationId) {
            return $this->redirectAccueil();
        } else {
            $reponse = [
                'token' => null,
                'context' => null,
                'error' => null,
                'status' => Response::HTTP_OK,
            ];

            try {
                $paramOptionnel['codeLien'] = $codeLien;
                $reponse['token'] = $this->getTokenMessage(
                    $consultationId,
                    $paramOptionnel
                );

                $messagerieSuiviMessage = $this->generateUrl(
                    'messagerie_suivi_message',
                    ['consultationId' => $consultationIdCrypte],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );

                $consultation = $this->em
                    ->getRepository(Consultation::class)
                    ->find($consultationId);

                $asHabilitationEspaceDoc = $this->checkHabilitationEspaceDoc();

                $render = $this->render('messagerie/modificationMessage.html.twig', [
                    'token' => $reponse['token'],
                    'suivi_url' => $messagerieSuiviMessage,
                    'url_front' => $this->utilitaire
                        ->getPfUrlDomaine($this->parameterBag->get('URL_MESSAGERIE_SECURISEE')),
                    'consultation' => $consultation,
                    'titre_page' => $titrePage,
                    'espace_doc' => $asHabilitationEspaceDoc,
                ]);
            } catch (ClientWsException $clientException) {
                $this->logger(self::ERROR_WS, $clientException);
                $reponse['status'] = $clientException->getCode();
                $reponse['error'] = utf8_encode($clientException->getMessage());
                $reponse['token'] = null;
                $render = $this->render('messagerie/error.html.twig', $reponse);
            } catch (\Error $error) {
                $this->logger(self::ERROR_TECHNIQUE, $error);
                $reponse['status'] = 500;
                $reponse['error'] = $this->translator->trans('ERROR_TECHNIQUE');
                unset($reponse['token']);
                $render = $this->render('messagerie/error.html.twig', $reponse);
            }
        }
        return $render;
    }
    /**
     * Permet la redirection vers la page d'accueil en cas d'erreur.
     */
    public function redirectAccueil(): RedirectResponse
    {
        return $this->redirectToRoute('atexo_agent_accueil_index');
    }

    /**
     * Action appelée par MESSEC pour mettre à jour la date de réponse à question.
     * @param Request $request
     * @return JsonResponse
     * @throws Atexo_Consultation_Exception
     */
    #[Route(path: '/callback-reponse-a-une-question/{consultationId}/{questionId}', name: 'callback_reponse_question_consultation', methods: ['GET'])]
    public function callbackReponseQuestionAction(Request $request)
    {
        $consultationId = $this->encryption->decryptId($request->get('consultationId'));
        $questionId = $this->encryption->decryptId($request->get('questionId'));
        $timestampReponse = $request->get('dateReponse');
        $auth = $this->perimetreAgent->isAuthorized($consultationId);
        $questionDCE = $this->em->getRepository(QuestionDCE::class)->find($questionId);
        $relationEchangeAlreadyExist = $this->em->getRepository(RelationEchange::class)
            ->findOneBy(['idExterne' => $questionId]);
        $result = false;
        $statutCode = Response::HTTP_OK;
        $key = 'statut';
        try {
            if (
                false === $auth
                || !$this->getConnectedUserId()
                || null === $consultationId
            ) {
                $statutCode = Response::HTTP_FORBIDDEN;
            } elseif (
                $relationEchangeAlreadyExist instanceof RelationEchange
                || !$questionDCE instanceof QuestionDCE
                || empty($timestampReponse)
            ) {
                $statutCode = Response::HTTP_BAD_REQUEST;
            } else {
                if (
                    false !== DateTime::createFromFormat(DateTimeInterface::RFC3339_EXTENDED, $timestampReponse)
                ) {
                    $dateReponse = new DateTime($timestampReponse);
                    $timezone = $this->getParameter('TIMEZONE');
                    $dateReponse->setTimezone(new DateTimeZone($timezone));
                    $relationEchange = new RelationEchange();
                    $relationEchange->setOrganisme($questionDCE->getOrganisme());
                    $relationEchange->setIdExterne($questionDCE->getId());
                    $relationEchange->setTypeRelation($this->getParameter('TYPE_RELATION_REPONSE_QUESTION'));
                    $relationEchange->setDateEnvoi($dateReponse);
                    $this->em->persist($relationEchange);
                    $this->em->flush();
                    $result = true;
                } else {
                    $key = 'message';
                    $result = $this->translator->trans('ERROR_FORMAT_DATE');
                    $statutCode = Response::HTTP_INTERNAL_SERVER_ERROR;
                }
            }
        } catch (Exception $e) {
            $this->logger(self::ERROR_TECHNIQUE, $e);
            $key = 'message';
            $result = $e->getMessage();
            $statutCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        }
        return new JsonResponse([$key => $result], $statutCode);
    }

    /**
     * @param $consultationId
     * @return Response
     */
    #[Route(path: '/call-espcae-doc/{consultationId}', name: 'call_espace_doc', methods: ['GET'])]
    public function callEspaceDoc($consultationId)
    {
        return $this->render('messagerie/modal-espacedoc.html.twig', ['consultationId' => $consultationId]);
    }

    #[Route(path: '/suivi-transverse-des-courriers/{type}', name: 'suivi_transverse', methods: ['GET'])]
    public function suiviTransverse(ParameterBagInterface $parameterBag, AtexoUtil $atexoUtil, MessagerieService $messagerieService, string $type = null) : Response
    {
        $params = $messagerieService->initializeContentForSuiviToken($this->getUser());
        if (in_array($type, self::ALLOWED_FILTERS)) {
            $params['filtresAppliques'] = $this->webServicesMessagerie->getContent(
                'initParamsWidgetMessec',
                $atexoUtil->kebabCaseToCamelCase($type)
            );
        }
        $token = $this->webServicesMessagerie->getContent(self::METHOD_INIT_SUIVI_TOKEN, $params);
        $accessToken = $this->webServicesMessagerie->getContent('getToken');
        $contexte = ['token' => $token, 'access_token' => $accessToken];
        return $this->render(
            'messagerie/suivi_transverse.html.twig',
            [
                'url_front' => $atexoUtil->getPfUrlDomaine($parameterBag->get('URL_MESSAGERIE_SECURISEE')),
                'contexte'   => json_encode($contexte, JSON_THROW_ON_ERROR)
            ]
        );
    }

    /**
     * @param $message
     * @param $Exception
     * @return void
     */
    protected function logger($message, $Exception)
    {
        $this->logger->error($message, [
            'httpCode' => $Exception->getCode(),
            'message' => utf8_encode($Exception->getMessage()),
        ]);
    }
    public function checkHabilitationEspaceDoc(): bool
    {
        $asHabilitationEspaceDoc = false;
        $infoAgent = $this->em
            ->getRepository(Agent::class)
            ->findOneBy(['id' => $this->getConnectedUserId()]);
        if ($infoAgent) {
            $habilitationAgent = $this->em
                ->getRepository(HabilitationAgent::class)
                ->findOneBy(['agent' => $this->getConnectedUserId()]);
            $espaceDocumentaire = $this->atexoConfiguration
                ->hasConfigOrganisme($infoAgent->getOrganisme()->getAcronyme(), 'espaceDocumentaire');

            if (true === $espaceDocumentaire && true === $habilitationAgent->isEspaceDocumentaireConsultation()) {
                $asHabilitationEspaceDoc = true;
            }
        }

        return $asHabilitationEspaceDoc;
    }

    private function getConnectedUserId(): false|int
    {
        if ($this->security->getUser() instanceof Agent) {
            return $this->security->getUser()->getId();
        }
        return false;
    }

    private function getConnectedUserType()
    {
        if ($this->security->getUser() instanceof Agent) {
            return 'agent';
        }
        return 'entreprise';
    }

    /**
     * @Route("/redaction/courrier-notification-contrat/{consultationIdCrypte}/{contratIdCrypte}",
     *     name="courrier_notification_contrat",
     *     methods={"GET"})
     */
    public function courrierNotificationContrat(
        string $consultationIdCrypte,
        string $contratIdCrypte
    ): Response {
        $titrePage = $this->translator->trans('COURRIER_DE_NOTIFICATION');
        $consultationId = $this->encryption->decryptId($consultationIdCrypte);
        $params = [
            'contratId' => $contratIdCrypte
        ];

        $response = [
            'token' => null,
            'context' => null,
            'error' => null,
            'status' => Response::HTTP_OK
        ];

        try {
            $response['token'] = $this->getTokenMessage($consultationId, $params);

            $messagerieSuiviMessage = $this->generateUrl(
                'messagerie_suivi_message',
                ['consultationId' => $consultationIdCrypte],
                UrlGeneratorInterface::ABSOLUTE_URL
            );

            $mailTemplate = $this->em->getRepository(MailTemplate::class)->findOneBy(
                [
                    'code' => $this->getParameter('COURRIER_NOTIFICATION')
                ]
            );

            $render = $this->getRender(
                $response,
                $mailTemplate,
                $consultationId,
                $messagerieSuiviMessage,
                $titrePage
            );
        } catch (ClientWsException $clientException) {
            $this->logger(self::ERROR_WS, $clientException);
            $response['status'] = $clientException->getCode();
            $response['error'] = utf8_encode($clientException->getMessage());
            $response['token'] = null;

            $render = $this->render('messagerie/error.html.twig', $response);
        } catch (Exception $exception) {
            $this->logger(self::ERROR_WS, $exception->getMessage());
            $response['status'] = Response::HTTP_INTERNAL_SERVER_ERROR;
            $response['error'] = $this->translator->trans('ERROR_TECHNIQUE');
            unset($response['token']);

            $render = $this->render('messagerie/error.html.twig', $response);
        }

        return $render;
    }



    /**
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Error
     * @throws ReflectionException
     * @throws \JsonException
     */
    #[Route(path: '/redaction/courrier-notification-offre-papier/{consultationId}/{offreId}', name: 'courrier_notification_consultation_papier', methods: ['GET'])]
    public function courrierNotificationOffrePapierConsultationAction(Request $request)
    {
        $titrePage = $this->translator->trans('COURRIER_DE_NOTIFICATION');
        $consultationIdCrypte = $request->get('consultationId');
        $consultationId = $this->encryption->decryptId($consultationIdCrypte);
        $offreIdCrypte = $request->get('offreId');
        $offreId = $this->encryption->decryptId($offreIdCrypte);
        $organisme = null;
        if (!$this->getConnectedUserId() || null === $consultationId) {
            return $this->redirectAccueil();
        } else {
            $reponse = [
                'token' => null,
                'context' => null,
                'error' => null,
                'status' => Response::HTTP_OK,
            ];

            $params = [
                'offreId' => $offreId,
                'papier' => true,
            ];

            try {
                $reponse['token'] = $this->getTokenMessage($consultationId, $params);
                $messagerieSuiviMessage = $this->generateUrl(
                    'messagerie_suivi_message',
                    ['consultationId' => $consultationIdCrypte],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );
                $idTemplate = $this->em->getRepository(MailTemplate::class)
                    ->findOneBy([
                        'code' => $this->getParameter('COURRIER_NOTIFICATION'),
                    ]);
                $render = $this->getRender(
                    $reponse,
                    $idTemplate,
                    $consultationId,
                    $messagerieSuiviMessage,
                    $titrePage
                );
            } catch (ClientWsException $clientException) {
                $this->logger(self::ERROR_WS, $clientException);
                $reponse['status'] = $clientException->getCode();
                $reponse['error'] = utf8_encode($clientException->getMessage());
                $reponse['token'] = null;
                $render = $this->render('messagerie/error.html.twig', $reponse);
            } catch (\Error $error) {
                $this->logger(self::ERROR_WS, $error);
                $reponse['status'] = 500;
                $reponse['error'] = $this->translator->trans('ERROR_TECHNIQUE');
                unset($reponse['token']);
                $render = $this->render('messagerie/error.html.twig', $reponse);
            }
        }
        return $render;
    }
}
