<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\PieceGenereConsultation;
use App\Service\AgentTechniqueTokenService;
use App\Service\AtexoFichierOrganisme;
use App\Service\Blob\BlobOrganismeFileService;
use App\Service\EspaceDocumentaire\Authorization;
use App\Service\GenererDocumentModele;
use App\Service\WebServicesRedac;
use App\Utils\Encryption;
use App\Utils\Filesystem\MountManager;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Exception;
use Doctrine\DBAL\Exception;
use League\Flysystem\FileExistsException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class GenererDocumentModeleController.
 **/
#[Route(path: '/agent/espace-documentaire', name: 'agent_espace_documentaire_')]
class GenererDocumentModeleController extends AbstractController
{
    public function __construct(private readonly ParameterBagInterface $parameterBag)
    {
    }

    public const AGENT_DOCGEN = 'agent_docgen';

    #[Route(path: '/document/template', methods: ['GET'], name: 'document_template')]
    public function getTemplate(GenererDocumentModele $genererDocumentModele, WebServicesRedac $redac): Response
    {
        /** @var Agent $agent */
        $agent = $this->getUser();
        $organism = $agent?->getOrganisme()?->getAcronyme();
        $service = $agent?->getService()?->getId();

        return $this->json($redac->getContent(
            'getDocumentsList',
            $this->parameterBag->get('UID_PF_MPE'),
            $organism,
            $service
        ));
    }

    /**
     *
     * @throws Atexo_Consultation_Exception
     * @throws Exception
     * @throws FileExistsException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    #[Route(path: '/document/generate/{idConsultation}', methods: ['POST'], name: 'document_generate')]
    public function generate(
        string $idConsultation,
        TranslatorInterface $translator,
        Authorization $perimetreAgent,
        Request $request,
        GenererDocumentModele $genererDocumentModele
    ): JsonResponse {
        $auth = $perimetreAgent->isAuthorized($idConsultation);
        $documentName = $request->request->get('template');
        $fileCode = $request->request->get('fileCode');
        $fileExtension = $request->request->get('extension');
        $lotNumber = $request->request->get('lotNumber');
        $entrepriseId = $request->request->get('entrepriseId');

        /** @var Agent $agent */
        $agent = $this->getUser();

        if (!$auth) {
            throw new AccessDeniedException($translator->trans('TEXT_VOUS_ETES_PAS_CONNECTE'));
        }

        return $this->json(
            $genererDocumentModele->generate(
                $documentName,
                $idConsultation,
                $agent->getOrganisme(),
                $fileCode,
                $lotNumber,
                $entrepriseId,
                $fileExtension
            )
        );
    }
    #[Route(
        path: '/document/edition/{idConsultation}/{blobId}',
        methods: ['GET'],
        name: 'document_consultation_edition'
    )]
    public function edit(
        Authorization $perimetreAgent,
        Encryption $encryption,
        TranslatorInterface $translator,
        GenererDocumentModele $genererDocumentModele,
        ParameterBagInterface $parameterBag,
        string $idConsultation,
        string $blobId
    ): JsonResponse {
        $auth = $perimetreAgent->isAuthorized($idConsultation);

        /** @var Agent $agent */
        $agent = $this->getUser();

        $url = $parameterBag->get('GENDOC_BASE_URL') .  '/edition-en-ligne/';

        if (!$auth) {
            throw new AccessDeniedException($translator->trans('TEXT_VOUS_ETES_PAS_CONNECTE'));
        }

        $blobId = $encryption->decryptId($blobId);
        $document = $this->getDoctrine()->getRepository(PieceGenereConsultation::class)
            ->findOneBy([
                'blobId' => $blobId,
                'consultation' => $idConsultation,
            ]);

        if (!$document instanceof PieceGenereConsultation) {
            throw new FileNotFoundException($translator->trans('TEXT_FICHIER_NON_TROUVE'));
        }

        $agentTechnique = $this->getDoctrine()->getRepository(Agent::class)->findOneBy([
            'login' => self::AGENT_DOCGEN,
            'technique' => 1,
        ]);

        $ticket = (new AgentTechniqueTokenService($this->getDoctrine()->getManager()))
            ->generateTokenForAgentTechnique($agentTechnique);

        $token = $genererDocumentModele->getToken(
            $document,
            $agent,
            $agent->getAcronymeOrganisme(),
            $this->generateUrl(
                'agent_espace_documentaire_edit_file',
                [
                    'id' => $document->getId(),
                    'token' => $ticket
                ],
                UrlGenerator::ABSOLUTE_URL
            )
        );

        $url = $url . '?token=' . $token;

        return $this->json($url);
    }

    /**
     * @param PieceGenereConsultation $piece
     * @param GenererDocumentModele $genererDocumentModele
     * @param string $token
     * @return JsonResponse
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    #[Route(path: '/document-update/{id}/{token}', name: 'edit_file', methods: ['POST'])]
    public function update(
        PieceGenereConsultation $piece,
        GenererDocumentModele $genererDocumentModele,
        string $token
    ): JsonResponse {
        try {
            $isTokenValid = (new AgentTechniqueTokenService($this->getDoctrine()->getManager()))->getAgentFromToken($token);
            if (!$isTokenValid instanceof Agent) {
                return $this->json(
                    [
                        'error' => 'invalid token'
                    ]
                );
            }

            $genererDocumentModele->getEditedDocument($piece, $piece->getBlobId()->getOrganisme());
            return $this->json(
                [
                    'error' => '0'
                ]
            );
        } catch (\Exception $exception) {
            return $this->json(
                [
                    'error' => $exception->getMessage()
                ]
            );
        }
    }

    /**
     *
     * @throws Atexo_Consultation_Exception
     * @throws Exception
     */
    #[Route(path: '/document/consultation/{idConsultation}', methods: ['GET'], name: 'document_consultation')]
    public function getDocumentConsultationAction(
        TranslatorInterface $translator,
        Authorization $perimetreAgent,
        Encryption $encryption,
        MountManager $mountManager,
        GenererDocumentModele $genererDocumentModele,
        string $idConsultation
    ): JsonResponse {
        $result = [];
        $result['type'] = 'PIECESGENEREESCONSULTATION';
        $result['message'] = $translator->trans('NON_AUTORISE_RESSOURCE');
        $result['fichiers'] = [];
        $result['statut'] = 500;
        $auth = $perimetreAgent->isAuthorized($idConsultation);

        if (!$auth) {
            throw new AccessDeniedException($translator->trans('TEXT_VOUS_ETES_PAS_CONNECTE'));
        }

        $result['statut'] = 200;
        /** @var Agent $agent */
        $agent = $this->getUser();
        $pieceGenereConsultations =
            $this->getDoctrine()->getRepository(PieceGenereConsultation::class)
                ->findBy([
                    'consultation' => $idConsultation,
                ]);

        if (count($pieceGenereConsultations) > 0) {
            $result = $genererDocumentModele->getDocumentConsultations(
                $result,
                $pieceGenereConsultations,
                $encryption,
                $mountManager,
                $agent->getOrganisme()
            );
        }

        return $this->json($result);
    }

    /**
     *
     * @throws Atexo_Consultation_Exception
     * @throws Exception
     */
    #[Route(
        path: '/document/consultation/{idConsultation}/{blobId}',
        methods: ['DELETE'],
        name: 'document_consultation_delete'
    )]
    public function deleteDoccumentConsultationAction(
        TranslatorInterface $translator,
        Encryption $encryption,
        Authorization $perimetreAgent,
        AtexoFichierOrganisme $atexoBlobOrganisme,
        string $idConsultation,
        string $blobId
    ): JsonResponse {
        $result = [];
        $result['type'] = 'document consultation';
        $result['message'] = $translator->trans('NON_AUTORISE_RESSOURCE');
        $auth = $perimetreAgent->isAuthorized($idConsultation);

        if (!$auth) {
            throw new AccessDeniedException($translator->trans('TEXT_VOUS_ETES_PAS_CONNECTE'));
        }

        $result['statut'] = 500;
        $result['message'] = $translator->trans('ERROR_DELETE');
        $blobId = $encryption->decryptId($blobId);
        $autrePiece = $this->getDoctrine()->getRepository(PieceGenereConsultation::class)
            ->findOneBy([
                'blobId' => $blobId,
                'consultation' => $idConsultation,
            ]);

        if ($autrePiece) {
            $this->getDoctrine()->getManager()->remove($autrePiece);
            /** @var Agent $agent */
            $agent = $this->getUser();
            $atexoBlobOrganisme->deleteBlobFile($blobId, $agent->getOrganisme());
            $this->getDoctrine()->getManager()->flush();
            $result['statut'] = 200;
            $result['message'] = $translator->trans('DELETE_OK');
        }

        return $this->json($result);
    }
    #[Route(
        path: '/document/download-to-pdf/{id}/{blobId}',
        methods: ['GET'],
        name: 'document_consultation_download_to_pdf'
    )]
    public function downloadToPdf(
        Consultation $consultation,
        string $blobId,
        BlobOrganismeFileService $blobOrganismeFileService,
        TranslatorInterface $translator,
        Authorization $perimetreAgent,
        GenererDocumentModele $genererDocumentModele
    ): JsonResponse {
        if (!$perimetreAgent->isAuthorized($consultation->getId())) {
            throw new AccessDeniedException($translator->trans('TEXT_VOUS_ETES_PAS_CONNECTE'));
        }

        $fileInfos = $blobOrganismeFileService->getFile($blobId) ??
            throw new FileNotFoundException($translator->trans('TEXT_FICHIER_NON_TROUVE'));

        return $this->json($genererDocumentModele->convertFileToPdf(
            $consultation,
            $this->getUser()->getOrganisme(),
            $fileInfos
        ));
    }
}
