<?php

namespace App\Controller\Agent;

use App\Entity\TMesRecherches;
use App\Repository\TMesRecherchesRepository;
use App\Service\Contrat\LotContrat;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RechercheSauvegardeeController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ParameterBagInterface $parameterBag
    ) {
    }

    /**
     * @Route("/agent/recherches-sauvegardees", name="agent_recherches_sauvegardees")
     *
     * @return Response
     */
    public function __invoke(): Response
    {
        $list = [];
        $recherchesSauvegardees = $this->entityManager->getRepository(TMesRecherches::class)
            ->displayRechercheSauvegardees(
                $this->getUser()->getId(),
                $this->parameterBag->get('TYPE_CREATEUR_AGENT'),
                $this->parameterBag->get('TYPE_AVIS_CONSULTATION'),
                $this->parameterBag->get('NBRE_MAX_MENUS_RECHERCHES_FAVORITES'),
            );

        foreach ($recherchesSauvegardees as $recherche) {
            $list[] = [
                'id' => $recherche->getId(),
                'denomination' => $recherche->getDenomination(),
                'url' => $this->generateUrl('consultation_search_index') .
                    '?' . http_build_query($recherche->getCriteria())
            ];
        }

        return $this->render('agent/recherche-sauvegardee/mes-recherches.html.twig', [
            'isFullWidth' => false,
            'recherches' => $list
        ]);
    }

    #[Route(
        path: '/agent/recherches-sauvegardees/save',
        name: 'agent_recherches_sauvegardees_save',
        methods: ['POST'],
        options: ['expose' => true]
    )]
    public function saveRecherches(Request $request): JsonResponse
    {
        $searchName = $request->request->get('searchName');
        $queryStringToJSON = $request->request->get('queryStringToJSON');

        $recherche = new TMesRecherches();
        $recherche->setIdCreateur($this->getUser()->getId());
        $recherche->setTypeCreateur($this->parameterBag->get('TYPE_CREATEUR_AGENT'));
        $recherche->setTypeAvis($this->parameterBag->get('TYPE_AVIS_CONSULTATION'));
        $recherche->setDenomination($searchName);
        $recherche->setPeriodicite(7);
        $recherche->setIdInitial(0);
        $recherche->setFormat(1);
        $recherche->setRecherche(1);
        $recherche->setAlerte(0);
        $recherche->setCriteria($queryStringToJSON);
        $recherche->setHashedCriteria(md5(json_encode($queryStringToJSON)));
        $this->entityManager->persist($recherche);
        $this->entityManager->flush();

        return new JsonResponse('search saved');
    }

    #[Route(
        path: '/agent/recherches-sauvegardees/{id}/delete',
        name: 'agent_recherches_sauvegardees_delete',
        options: ['expose' => true]
    )]
    public function deleteRecherche(TMesRecherches $id): Response
    {
        $this->entityManager->remove($id);
        $this->entityManager->flush();

        return new Response(null, 204);
    }
}
