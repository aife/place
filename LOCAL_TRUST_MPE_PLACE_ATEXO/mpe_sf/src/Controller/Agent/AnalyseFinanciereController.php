<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\PieceGenereConsultation;
use App\Service\DocGen\AnalyseFinanciere;
use App\Service\DocGen\Docgen;
use App\Service\Offre\AnalyseFinanciereService;
use App\Utils\Encryption;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

#[Route(path: '/agent/analyse-financiere', name: 'analyse_financiere_')]
class AnalyseFinanciereController extends AbstractController
{
    final public const TEMPLATE_EXTENSION = '.xlsx';

    public function __construct(
        private readonly AnalyseFinanciereService $analyseFinanciere,
        private readonly Encryption $encryption
    ) {
    }

    /**
     * @param Request $request
     * @return BinaryFileResponse
     * @throws Atexo_Consultation_Exception
     */
    #[Route(path: '/direct-download-afi/{idAgent}/{idConsultation}/{blobId}', methods: ['GET'], name: 'download')]
    public function downloadAnalyseFinanciere(Request $request): BinaryFileResponse
    {
        if (empty($this->getUser()) || !$this->getUser() instanceof Agent) {
            throw new AccessDeniedException();
        }

        $idAgent = $this->encryption->decryptId($request->get('idAgent'));

        $consultationId = $request->get('idConsultation');
        $blobId = $request->get('blobId');

        return $this->analyseFinanciere->getAnnexeFinanciereFile($idAgent, $consultationId, $blobId);
    }

    #[Route(path: '/delete-afi-manuel/{idAgent}/{idConsultation}/{annexeFinanciereId}', methods: ['GET'], name: 'delete')]
    public function deleteAnalyseFinanciereManuel(Request $request): Response
    {
        if (empty($this->getUser()) || !$this->getUser() instanceof Agent) {
            throw new AccessDeniedException();
        }

        $annexeFinanciereId = $request->get('annexeFinanciereId');
        $consultationId = $request->get('idConsultation');

        $idAgent = $this->encryption->decryptId($request->get('idAgent'));

        $this->analyseFinanciere->removeAnnexeFinanciereManuel($idAgent, $consultationId, $annexeFinanciereId);

        return $this->redirectToRoute('analyse_offre_index', ['id' => $consultationId]);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    #[Route(path: '/annexe-financiere-tableau-comparaison/{id}/', methods: ['GET'], name: 'annexe_financiere-tableau-comparaison')]
    public function annexeFinanciereTableauComparaison(Request $request, Consultation $consultation, Docgen $docgen, ParameterBagInterface $parameterBag): Response
    {
        $organisme = $this->getUser()->getOrganisme();
        $url = $parameterBag->get('GENDOC_BASE_URL') . '/edition-en-ligne/';

        $pieceGeneree = $this->analyseFinanciere->analyzeAndNote(
            $this->getUser()->getId(),
            $consultation,
            $request->query->get('blobIds'),
            $request->query->get('fileName'),
            $organisme
        );

        $token = $docgen->getToken(
            $pieceGeneree,
            $this->getUser(),
            $organisme,
            self::TEMPLATE_EXTENSION,
            $this->generateUrl(
                'analyse_financiere_edit_file',
                ['id' => $pieceGeneree->getId()],
                UrlGenerator::ABSOLUTE_URL
            )
        );

        return new Response($url . '?token=' . $token);
    }

    /**
     * @throws FileExistsException
     * @throws FileNotFoundException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    #[Route(path: '/document-update/{id}', name: 'edit_file', methods: ['POST'])]
    public function edit(PieceGenereConsultation $piece, AnalyseFinanciere $analyseFinanciere): Response
    {
        $analyseFinanciere->getEditedDocument($piece, $piece->getBlobId()->getOrganisme());

        return new Response('OK');
    }
}
