<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use JsonException;
use ReflectionException;
use App\Controller\Api\EspaceDocumentaire\ApiEspaceDocumentaireController;
use App\Entity\Agent\NotificationAgent;
use App\Entity\Consultation;
use App\Entity\TDumeContexte;
use App\Service\Authorization\AuthorizationAgent;
use App\Service\Blob\BlobOrganismeFileService;
use App\Service\EspaceDocumentaire\Authorization;
use App\Service\EspaceDocumentaire\EspaceDocumentaireService;
use App\Service\EspaceDocumentaire\EspaceDocumentaireUtil;
use App\Service\SearchAgent;
use App\Utils\Encryption;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Exception;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route(path: '/agent', name: 'agent_')]
class EspaceDocumentaireController extends AbstractController
{
    private const DOWNLOADPLI = '1';
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly SearchAgent $searchAgent,
        private readonly SessionInterface $session,
        private readonly Authorization $perimetreAgent,
        protected LoggerInterface $logger,
        protected TranslatorInterface $translator,
        private BlobOrganismeFileService $blobOrganismeFileService,
        private Security $security,
        private ParameterBagInterface $parameterBag,
        private Encryption $encryption,
        private EspaceDocumentaireService $espaceDocumentaireService
    ) {
    }

    /**
     * @param EspaceDocumentaireUtil $espaceDocumentaireUtil
     * @param AuthorizationAgent $authorizationAgent
     * @param int $id
     * @return Response
     * @throws JsonException
     * @throws ReflectionException
     */
    #[Route(path: '/espace-documentaire-consultation/{id}', name: 'espace_documentaire_consultation')]
    public function indexAction(EspaceDocumentaireUtil $espaceDocumentaireUtil, AuthorizationAgent $authorizationAgent, int $id): Response
    {
        $auth = $authorizationAgent->isInTheScope($id);
        if (!$auth) {
            $this->logger->warning(
                sprintf(
                    'La consultation id %d n\'existe pas ou ne fait pas partie du perimètre de l\'agent %d ',
                    $id,
                    $this->getUser()->getId()
                )
            );
            return $this->redirectToRoute('atexo_agent_accueil_index');
        }
        $consultation = $this->em
            ->getRepository(Consultation::class)
            ->find($id);
        $agentId = $this->getUser()->getId();
        $encryptedAgentId = $this->encryption->cryptId($agentId);

        return $this->render('EspaceDocumentaire/espace-documentaire.html.twig', [
            'base_dir' => realpath($this->parameterBag->get('kernel.project_dir') . '/..'),
            'consultation' => $consultation,
            'dume' => $this->getInfoDume($consultation),
            'encryptedAgentId' => $encryptedAgentId,
            'auth' => $auth,
            'number_of_files_by_block' => $espaceDocumentaireUtil->getAllBlockCounterFiles(
                $consultation,
                $this->getUser()->getAcronymeOrganisme()
            ),
            'lang'  => json_encode($this->espaceDocumentaireService->getPieceModeleTranslations())
        ]);
    }

    public function getInfoDume($consultation, $contexte = null)
    {
        $infoDume = [];
        if (
            !($contexte instanceof TDumeContexte)
            && $consultation instanceof Consultation
            && '1' == $consultation->getDumeDemande()
        ) {
            $contexte = $this->getDoctrine()
                ->getRepository(TDumeContexte::class)
                ->getWaitingOrValidOrPublish(
                    $consultation->getId(),
                    $consultation->getAcronymeOrg(),
                    $this->parameterBag->get('TYPE_DUME_ACHETEUR'),
                    $this->parameterBag->get('STATUT_DUME_CONTEXTE_EN_ATTENTE_PUBLICATION'),
                    $this->parameterBag->get('STATUT_DUME_CONTEXTE_VALIDE'),
                    $this->parameterBag->get('STATUT_DUME_CONTEXTE_PUBLIE'),
                    $this->logger,
                    $this->translator->trans('MSG_ERREUR_TECHNIQUE'),
                    false
                );
        }
        if ($contexte instanceof TDumeContexte) {
            if ($contexte->isStandard()) {
                $infoDume['image'] = 'bundles/atexoconsultationdepot/assets/images/logo-dume-non-publie.png';
                $infoDume['message'] = $this->translator->trans('DEFINE_DUME_ACHETEUR_NON_PUBLIE');
            } else {
                $infoDume['image'] = 'bundles/atexoconsultationdepot/assets/images/logo-dume-publie.png';
                $infoDume['message'] = $this->translator->trans('DEFINE_DUME_ACHETEUR_PUBLIE');
            }
        }

        return $infoDume;
    }

    /**
     * @param Request $request
     * @return BinaryFileResponse|RedirectResponse
     */
    #[Route(path: '/download-archive/{idAgent}/{idConsultation}/{nomArchive}/{type}', methods: ['GET'], name: 'espace_documentaire_consultation_download')]
    public function downloadArchiveAction(Request $request)
    {
        $idAgent = $this->encryption->decryptId($request->get('idAgent'));
        $idConsultation = $this->encryption->decryptId($request->get('idConsultation'));
        $type = $request->get('type');
        $auth = $this->perimetreAgent->isAuthorized($idConsultation, (self::DOWNLOADPLI === $type));
        if (!$auth) {
            throw $this->createNotFoundException('Cette ressource n\'existe pas !');
        }
        $urlNotif = substr($request->server->get('REQUEST_URI'), 1);
        $notification = $this->em->getRepository(NotificationAgent::class)
            ->findOneBy(
                [
                    'urlNotification' => $urlNotif,
                    'idAgent' => $idAgent,
                ]
            );
        if (!$notification instanceof NotificationAgent) {
            throw $this->createNotFoundException('Cette ressource n\'existe pas !');
        }
        $filename = $request->get('nomArchive');
        $publicResourcesFolderPath = $this->parameterBag->get('PATH_ARCHIVE_ESPACE_DOCUMENTAIRE');
        if (!file_exists($publicResourcesFolderPath . $filename)) {
            $url = '/index.php?page=Agent.TableauDeBord&id=' . $idConsultation . '&ArchiveExpirer';

            return $this->redirect($url);
        }
        $consultation = $this->em->getRepository(Consultation::class)
            ->find($idConsultation);
        $suffix = $consultation->getReferenceUtilisateur() . '_' . date('His') . '.zip';
        $archiveName = $this->translator->trans('DOCUMENTS') . '_' . $suffix;
        if (self::DOWNLOADPLI === $type) {
            $archiveName = $this->translator->trans('REPONSES') . '_' . $suffix;
        }
        $response = new BinaryFileResponse($publicResourcesFolderPath . $filename);
        return $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $archiveName
        );
    }

    /**
     * @param Request $request
     * @return BinaryFileResponse
     * @throws Atexo_Consultation_Exception
     */
    #[Route(path: '/direct-download/{idAgent}/{idConsultation}/{typeAndBlobId}', methods: ['GET'], name: 'espace_documentaire_consultation_direct_download')]
    public function directDownloadAction(Request $request)
    {
        $idAgent = $this->encryption->decryptId($request->get('idAgent'));
        $consultationId = $request->get('idConsultation');
        $authAgent = $this->perimetreAgent->isAgentAuthorized($idAgent);
        $authConsultation = $this->perimetreAgent->isAuthorized($consultationId);
        if (!$authAgent || !$authConsultation) {
            throw $this->createNotFoundException('Cette ressource n\'existe pas !');
        }
        $blobParameters = explode('-', (string) $request->get('typeAndBlobId'));
        $espaceDocumentaireService = $this->espaceDocumentaireService;
        $delete = false;
        if ('DCE' == $blobParameters[0]) {
            $reponse = $espaceDocumentaireService->getFileIntoDce($blobParameters[1], $blobParameters[2]);
            $delete = true;
        } else {
            $reponse = $this->blobOrganismeFileService->getFile($blobParameters[1]);
        }
        if (!file_exists($reponse['realPath'])) {
            throw $this->createNotFoundException('Le chemin de cette ressource n\'existe pas !');
        }
        $response = new BinaryFileResponse($reponse['realPath']);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $reponse['name']
        );
        $response->deleteFileAfterSend($delete);
        return $response;
    }

    protected function forwardApiEspaceDocumentaire(
        Request $request,
        ?EspaceDocumentaireUtil $espaceDocumentaireUtil,
        string $function
    ): Response {
        return $this->forward(
            ApiEspaceDocumentaireController::class . '::' . $function,
            [
                'request' => $request,
                'espaceDocumentaireUtil' => $espaceDocumentaireUtil,
            ]
        );
    }

    #[Route(path: '/espace-documentaire/dce/autres-pieces/{idConsultation}.{format}', name: 'espace_documentaire_autres_pieces', defaults: ['format' => 'json'], methods: ['GET'])]
    public function getAutresPiecesAction(Request $request, EspaceDocumentaireUtil $espaceDocumentaireUtil): Response
    {
        return $this->forwardApiEspaceDocumentaire($request, $espaceDocumentaireUtil, __FUNCTION__);
    }

    #[Route(
        path: '/espace-documentaire/autres-pieces-consultation/{idConsultation}.{format}',
        name: 'espace_documentaire_autre_piece_consultation',
        defaults: ['format' => 'json'],
        methods: ['GET']
    )]
    public function getAutresPiecesConsultationAction(Request $request, EspaceDocumentaireUtil $espaceDocumentaireUtil): Response
    {
        return $this->forwardApiEspaceDocumentaire($request, $espaceDocumentaireUtil, __FUNCTION__);
    }

    #[Route(path: '/espace-documentaire/dce/{idConsultation}.{format}', name: 'espace_documentaire_dce', defaults: ['format' => 'json'], methods: ['GET'])]
    public function getDceAction(Request $request, EspaceDocumentaireUtil $espaceDocumentaireUtil): Response
    {
        return $this->forwardApiEspaceDocumentaire($request, $espaceDocumentaireUtil, __FUNCTION__);
    }

    #[Route(path: '/espace-documentaire/dume/dume-acheteur/{idConsultation}.{format}', name: 'espace_documentaire_dume_acheteur', defaults: ['format' => 'json'], methods: ['GET'])]
    public function getDUMEAcheteurAction(Request $request): Response
    {
        return $this->forwardApiEspaceDocumentaire($request, null, __FUNCTION__);
    }

    #[Route(
        path: '/espace-documentaire/pieces-du-depots/{idOffre}/{idConsultation}.{format}',
        name: 'espace_documentaire_pli-dume-enveloppe',
        defaults: ['format' => 'json'],
        methods: ['GET']
    )]
    public function getPiecesConsultationAction(Request $request, EspaceDocumentaireUtil $espaceDocumentaireUtil): Response
    {
        return $this->forwardApiEspaceDocumentaire($request, $espaceDocumentaireUtil, __FUNCTION__);
    }

    #[Route(path: '/espace-documentaire/publicite/{idConsultation}.{format}', name: 'espace_documentaire_publicite', defaults: ['format' => 'json'], methods: ['GET'])]
    public function getPiecesDePubliciteAction(Request $request, EspaceDocumentaireUtil $espaceDocumentaireUtil): Response
    {
        return $this->forwardApiEspaceDocumentaire($request, $espaceDocumentaireUtil, __FUNCTION__);
    }

    #[Route(
        path: '/espace-documentaire/pieces-plis-attributaires/{idOffre}/{idConsultation}.{format}',
        name: 'espace_documentaire_pieces_plis_attributaires',
        defaults: ['format' => 'json'],
        methods: ['GET']
    )]
    public function getPiecesPlisAttributaires(Request $request, EspaceDocumentaireUtil $espaceDocumentaireUtil): Response
    {
        return $this->forwardApiEspaceDocumentaire($request, $espaceDocumentaireUtil, __FUNCTION__);
    }

    #[Route(path: '/espace-documentaire/plis/{idConsultation}.{format}', name: 'espace_documentaire_plis', defaults: ['format' => 'json'], methods: ['GET'])]
    public function getPlisAction(Request $request, EspaceDocumentaireUtil $espaceDocumentaireUtil): Response
    {
        return $this->forwardApiEspaceDocumentaire($request, $espaceDocumentaireUtil, __FUNCTION__);
    }

    #[Route(
        path: '/espace-documentaire/plis-attributaires/{idConsultation}.{format}',
        name: 'espace_documentaire_plis_attributaires',
        defaults: ['format' => 'json'],
        methods: ['GET']
    )]
    public function getPlisDesAttributaires(Request $request): Response
    {
        return $this->forwardApiEspaceDocumentaire($request, null, __FUNCTION__);
    }

    #[Route(path: '/espace-documentaire/rc/{idConsultation}.{format}', name: 'espace_documentaire_rc', defaults: ['format' => 'json'], methods: ['GET'])]
    public function getRcAction(Request $request, EspaceDocumentaireUtil $espaceDocumentaireUtil): Response
    {
        return $this->forwardApiEspaceDocumentaire($request, $espaceDocumentaireUtil, __FUNCTION__);
    }

    #[Route(
        path: '/espace-documentaire/autres-pieces-consultation/{idConsultation}.{format}',
        name: 'espace_documentaire_ajout_autre_piece_consultation',
        defaults: ['format' => 'json'],
        methods: ['POST']
    )]
    public function postAutresPiecesConsultationAction(Request $request, EspaceDocumentaireUtil $espaceDocumentaireUtil): Response
    {
        return $this->forwardApiEspaceDocumentaire($request, $espaceDocumentaireUtil, __FUNCTION__);
    }

    #[Route(path: '/espace-documentaire/create-download/{idConsultation}.{format}', name: 'create_download', defaults: ['format' => 'json'], methods: ['POST'])]
    public function createDownloadAction(Request $request): Response
    {
        return $this->forwardApiEspaceDocumentaire($request, null, __FUNCTION__);
    }

    #[Route(
        path: '/espace-documentaire/autres-pieces-consultation/{idConsultation}/{blobId}.{format}',
        name: 'delete_autre_piece_consultation',
        defaults: ['format' => 'json'],
        methods: ['DELETE']
    )]
    public function deleteAutresPiecesConsultationAction(Request $request): Response
    {
        return $this->forwardApiEspaceDocumentaire($request, null, __FUNCTION__);
    }
}
