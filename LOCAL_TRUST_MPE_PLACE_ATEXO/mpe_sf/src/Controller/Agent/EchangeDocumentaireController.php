<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use App\Entity\ConsLotContrat;
use App\Entity\Consultation;
use App\Entity\ContratTitulaire;
use App\Entity\EchangeDocumentaire\EchangeDoc;
use App\Entity\EchangeDocumentaire\EchangeDocApplicationClient;
use App\Entity\EchangeDocumentaire\EchangeDocHistorique;
use App\Entity\EchangeDocumentaire\EchangeDocTypePieceActes;
use App\Entity\EchangeDocumentaire\EchangeDocTypePieceStandard;
use App\Entity\Entreprise;
use App\Message\GenerateArchive;
use App\Repository\ReferentielSousTypeParapheurRepository;
use App\Service\Authorization\AuthorizationAgent;
use App\Service\ContratService;
use App\Service\EchangeDocumentaire\EchangeDocumentaireService;
use App\Service\EspaceDocumentaire\EspaceDocumentaireService;
use App\Service\EspaceDocumentaire\EspaceDocumentaireUtil;
use App\Service\Messagerie\MessagerieService;
use App\Service\SearchAgent;
use App\Service\WebServices\WebServicesExec;
use App\Utils\Encryption;
use Doctrine\ORM\EntityManagerInterface;
use Matrix\Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route(path: '/agent/echange-documentaire', name: 'echange_documentaire_')]
class EchangeDocumentaireController extends AbstractController
{
    final public const RELANCE_STATUT = 'A_ENVOYER';
    final public const ECHANGES_DOCUMENTS_MODULE_CHECK = [
        'EchangesDocuments' => true,
    ];

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly SessionInterface $session,
        protected LoggerInterface $logger,
        protected TranslatorInterface $translator,
        private readonly MessagerieService $messagerieService,
        private readonly SearchAgent $searchAgent,
        private readonly AuthorizationAgent $perimetreAgent,
        private readonly Encryption $encryption,
        private readonly EchangeDocumentaireService $edService,
        private readonly ParameterBagInterface $parameterBag,
        private readonly ContratService $contratService,
        private readonly ReferentielSousTypeParapheurRepository $referentielparapheurRepository,
        private readonly EspaceDocumentaireService $espaceDocumentaireService,
        private readonly WebServicesExec $webServicesExec
    ) {
    }

    #[Route(path: '/{consultationId}', name: 'index', methods: ['GET', 'POST'])]
    public function index(Request $request): Response
    {
        $encryptedId = $request->get('consultationId');
        $consultationId = $this->encryption->decryptId($encryptedId);
        if (!$this->perimetreAgent->isAuthorized($consultationId, self::ECHANGES_DOCUMENTS_MODULE_CHECK)) {
            return $this->getErrorResponse();
        }
        $consultation = $this->em
            ->getRepository(Consultation::class)
            ->find($consultationId);
        $titrePage = $this->translator->trans('SUIVI_DES_ECHANGES');
        return $this->render('echangeDocumentaire/index.html.twig', [
            'consultation' => $consultation,
            'titre_page' => $titrePage,
            'encryptedId' => $encryptedId,
            'echanges' => null,
        ]);
    }
    private function getErrorResponse()
    {
        $reponse = [];
        $reponse['status'] = 404;
        $reponse['error'] = $this->translator->trans('PAS_AUTHORISER');

        return $this->render('generique/erreur/error.html.twig', $reponse);
    }

    #[Route(path: '/{consultationId}/new', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request, EspaceDocumentaireUtil $espaceDocumentaireUtil): Response
    {
        $encryptedId = $request->get('consultationId');
        $consultationId = $this->encryption->decryptId($encryptedId);
        if (!$this->perimetreAgent->isAuthorized($consultationId, self::ECHANGES_DOCUMENTS_MODULE_CHECK)) {
            return $this->getErrorResponse();
        }
        $consultation = $this->em
            ->getRepository(Consultation::class)
            ->find($consultationId)
        ;

        $encryptedAgentId = $this->encryption->cryptId($this->getUser()->getId());
        $applicationClients = $this->edService->getAuthorizedApplicationClient();
        $titrePage = $this->translator->trans('CREATION_ECHANGE');

        if ($this->parameterBag->get('ACTIVE_EXEC_V2')) {
            $query = [
                "page" => WebServicesExec::NB_PAGE,
                "size" => WebServicesExec::NB_ITEMS,
                "idExternesConsultation" => $consultationId,
            ];
            $contratsExec = $this->webServicesExec->getContent('getContrats', $query);
            $contracts = $contratsExec['content'] ?? [];
        } else {
            // TODO : EXEC - A supprimer quand le module EXEC sera activé
            $contracts = $this->em->getRepository(ConsLotContrat::class)->getListContrat($consultationId);
            foreach ($contracts as &$contract) {
                $contract['montantContrat'] = $this->displayMontant($contract['idContratTitulaire']);
            }
        }

        return $this->render('echangeDocumentaire/new.html.twig', [
            'consultation' => $consultation,
            'titre_page' => $titrePage,
            'encryptedId' => $encryptedId,
            'base_dir' => realpath($this->parameterBag->get('kernel.project_dir') . '/..'),
            'dume' => null,
            'encryptedAgentId' => $encryptedAgentId,
            'applicationsClients' => $applicationClients,
            'contracts' => $contracts,
            'number_of_files_by_block' => $espaceDocumentaireUtil->getAllBlockCounterFiles(
                $consultation,
                $this->getUser()->getAcronymeOrganisme()
            ),
            'sousTypeParapheurs' => $this->referentielparapheurRepository->findAll(),
            'lang'  => json_encode($this->espaceDocumentaireService->getPieceModeleTranslations())
        ]);
    }

    #[Route(path: '/{consultationId}/save', name: 'save', methods: ['GET', 'POST'])]
    public function save(Request $request, MessageBusInterface $bus): Response
    {
        $statutCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        $isSaved = false;
        $encryptedId = $request->get('consultationId');
        $consultationId = $this->encryption->decryptId($encryptedId);
        if ($this->perimetreAgent->isAuthorized($consultationId, self::ECHANGES_DOCUMENTS_MODULE_CHECK)) {
            $data = $request->request->all()['data'];
            if ($this->isValidData($data)) {
                try {
                    $echangeDoc = $this->edService->createEchangeDocumentaire($data, $consultationId);
                    if ($echangeDoc instanceof EchangeDoc) {
                        $isSaved = true;
                        $statutCode = Response::HTTP_CREATED;

                        if ($echangeDoc->getEchangeDocApplicationClient()->getEchangeDocApplication()->isEnvoiDic()) {
                            $bus->dispatch(new GenerateArchive($echangeDoc));
                        }
                    }
                } catch (\Exception $e) {
                    $this->logger->error($e->getMessage());
                }
            }
        }
        return new JsonResponse($isSaved, $statutCode);
    }

    private function isValidData($data)
    {
        if (empty($data['application']) || empty($data['objet'])) {
            return false;
        }

        if (empty($data['fichierPrincipaux'])) {
            $echangeDocApplicationClient = $this->em->getRepository(EchangeDocApplicationClient::class)
                ->find($data['application']);

            if (!$echangeDocApplicationClient->getEchangeDocApplication()->isEnvoiDic()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return JsonResponse
     * @throws Exception
     */
    #[Route(path: '/update-statut-echange', name: 'put_statut_echange', methods: ['PUT'])]
    public function putStatutEchangeAction(Request $request)
    {
        $echangeId = $request->get('idEchange');
        $echangeDoc = $this->em
            ->getRepository(EchangeDoc::class)
            ->find($echangeId);
        if ($echangeDoc instanceof EchangeDoc) {
            try {
                $this->edService->changeStatut(
                    $echangeDoc,
                    $this->getUser(),
                    self::RELANCE_STATUT
                );
            } catch (Exception $exception) {
                $messageError = "Impossible de sauvegarder soit le changement de statut 
                 soit d'écire dans la table d'historique d'échange pour l'échange docu. suivant : "
                    . $echangeId;
                $this->logger->error($messageError . 'Stack : ' . $exception->getMessage());
                throw new Exception($messageError);
            }
        } else {
            $this->logger->error("L'echange documentaire avec l'id : " . $echangeId . "n'existe pas ");
            throw new Exception("L'echange documentaire n'existe pas");
        }
        return $this->json(['statut' => true]);
    }

    /**
     * @return JsonResponse
     */
    #[Route(path: '/modal-detail-echange/{idEchange}', name: 'modal_detail_echange', methods: ['GET'])]
    public function getDetailEchangeAction(Request $request)
    {
        $id = $this->encryption->decryptId($request->get('idEchange'));
        $echange = $this->em
            ->getRepository(EchangeDoc::class)
            ->find($id);
        $render = $this->render('echangeDocumentaire/bloc/erreur.html.twig', [
            'ERREUR' => 'ERREUR_ECHANGE_ABSENT',
        ]);
        if ($echange instanceof EchangeDoc) {
            $echangeHistoriques = $this->em
                ->getRepository(EchangeDocHistorique::class)
                ->findBy([
                    'echangeDoc' => $id,
                ]);

            $render = $this->render('echangeDocumentaire/bloc/modalDetailEchange.html.twig', [
                'echange' => $this->enrichirUnEchangeAvecDonneesContrat($echange),
                'echangeHistoriques' => $echangeHistoriques,
            ]);
        }
        return $render;
    }

    #[Route(path: '/filter-echange/{consultationId}', name: 'ajax_filter_echange', methods: ['GET'])]
    public function ajaxTableauSuiviEchange(Request $request)
    {
        $encryptedId = $request->get('consultationId');
        $consultationId = $this->encryption->decryptId($encryptedId);
        if (!$this->perimetreAgent->isAuthorized($consultationId, self::ECHANGES_DOCUMENTS_MODULE_CHECK)) {
            return $this->getErrorResponse();
        }
        $paramTabs = $request->query->all();
        $echanges = $this->em
            ->getRepository(EchangeDoc::class)
            ->filterEchange($consultationId, $paramTabs)
        ;

        return $this->render('echangeDocumentaire/bloc/listeEchanges.html.twig', [
            'echanges' => $this->enrichirDesEchangesAvecDonneesContrat($echanges),
        ]);
    }

    /**
     * @param $echanges
     *
     * @return mixed
     */
    private function enrichirDesEchangesAvecDonneesContrat($echanges)
    {
        //add montant contrat dans les échanges et titulaire
        foreach ($echanges as $key => $echange) {
            $echanges[$key] = $this->enrichirUnEchangeAvecDonneesContrat($echange);
        }

        return $echanges;
    }

    /**
     * @param $echange
     *
     * @return mixed
     */
    private function enrichirUnEchangeAvecDonneesContrat(?EchangeDoc $echange)
    {
        if ($this->parameterBag->get('ACTIVE_EXEC_V2')) {
            if ($echange->getUuidExterneExec()) {
                try {
                    $contratsExec = $this->webServicesExec->getContent(
                        'getContratByUuid',
                        $echange->getUuidExterneExec()
                    );

                    if ($contratsExec) {
                        $echange->setNumeroContrat($contratsExec['numero'] ?? null);
                        $echange->setMontantContrat($contratsExec['montant'] ?? null);
                        $echange->setRaisonSocialeTitulaire(
                            $contratsExec['etablissementTitulaire']['fournisseur']['raisonSociale'] ?? null
                        );
                    }
                } catch (\Exception $e) {
                    $this->logger->error($e->getMessage());
                }
            }
        } else {
            // TODO : EXEC - A supprimer quand le module EXEC sera activé
            if (!is_null($echange->getContratTitulaire())) {
                $echange->setMontantContrat($this->displayMontant($echange->getContratTitulaire()));
            }
            if ($echange->getContratTitulaire() instanceof ContratTitulaire) {
                $titulaire = $this->em
                    ->getRepository(Entreprise::class)
                    ->find($echange->getContratTitulaire()->getIdTitulaire());
                if ($titulaire instanceof Entreprise) {
                    $echange->setRaisonSocialeTitulaire($titulaire->getNom());
                }
            }
        }

        return $echange;
    }

    #[Route(path: '/get-file-echange/{echangeDocBlobId}/{blobId}', name: 'get_file_echange', methods: ['GET'])]
    public function getFileAction(Request $request): \Exception|StreamedResponse
    {
        $encryptedEchangeDocBlobId = $request->get('echangeDocBlobId');
        $echangeDocBlobId = $this->encryption->decryptId($encryptedEchangeDocBlobId);
        $encryptedBlobId = $request->get('blobId');
        $blobId = $this->encryption->decryptId($encryptedBlobId);
        try {
            $result = $this->edService->getFile($request, $echangeDocBlobId, $blobId);
        } catch (\Exception $exception) {
            $result = $exception;
        }
        return $result;
    }

    /**
     * @return Response
     */
    #[Route(
        path: '/get-cheminement-pastell/{consultationId}/{applicationDistante}',
        name: 'get_cheminement_pastell',
        methods: ['GET']
    )]
    public function ajaxCheminementPastell(Request $request)
    {
        $encryptedId = $request->get('consultationId');
        $consultationId = $this->encryption->decryptId($encryptedId);
        if (!$this->perimetreAgent->isAuthorized($consultationId, self::ECHANGES_DOCUMENTS_MODULE_CHECK)) {
            return $this->getErrorResponse();
        }
        $applicationDistante = $request->get('applicationDistante');
        $echangeDocApplicationClient = $this->em
            ->getRepository(EchangeDocApplicationClient::class)
            ->findOneBy(['id' => $applicationDistante]);
        $isEchangeDocApplicationClient = false;
        if ($echangeDocApplicationClient instanceof EchangeDocApplicationClient) {
            $isEchangeDocApplicationClient = true;
            if (
                !$echangeDocApplicationClient->isCheminementSignature()
                && !$echangeDocApplicationClient->isCheminementGed()
                && !$echangeDocApplicationClient->isCheminementSae()
                && !$echangeDocApplicationClient->isCheminementTdt()
            ) {
                $isEchangeDocApplicationClient = false;
            }
        }
        return $this->render('echangeDocumentaire/bloc/cheminement-pastell.html.twig', [
            'echangeDocApplicationClient' => $echangeDocApplicationClient,
            'isEchangeDocApplicationClient' => $isEchangeDocApplicationClient,
        ]);
    }

    /**
     * @param $idContratTitulaire
     *
     * @return mixed
     */
    public function displayMontant($idContratTitulaire)
    {
        return $this->contratService->getMontantMarche($idContratTitulaire);
    }

    /**
     * @param $idEchangeDocApplicationClient
     * @return JsonResponse
     */
    #[Route(
        path: '/ajax-typage-document/{idEchangeDocApplicationClient}',
        name: 'ajax_typage_document',
        methods: ['GET']
    )]
    public function ajaxGetTypageDocument($idEchangeDocApplicationClient, Request $request)
    {
        $typage = [];
        $echangeDocApplicationClient = $this->em
            ->getRepository(EchangeDocApplicationClient::class)
            ->findOneBy(['id' => $idEchangeDocApplicationClient]);
        if ($echangeDocApplicationClient instanceof EchangeDocApplicationClient) {
            $echangeDocApplication = $echangeDocApplicationClient->getEchangeDocApplication();

            $flux = EchangeDocTypePieceStandard::class;
            if ($echangeDocApplication->isFluxActes()) {
                $flux = EchangeDocTypePieceActes::class;
            }

            $typage = $this->em
                ->getRepository($flux)
                ->findAllToArray();
        }
        return new JsonResponse($typage);
    }
}
