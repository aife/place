<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use App\Service\WebServices\WebServicesExec;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class TokenController extends AbstractController
{
    #[Route(path: '/agent/token-user-connected', name: 'token_user_connect', methods: ['GET'])]
    public function getTokenUserConnected(JWTTokenManagerInterface $tokenManager): JsonResponse
    {
        return $this->json($tokenManager->create($this->getUser()));
    }

    #[Route(path: '/agent/exec/token-user-connected', name: 'exec_token_user_connect', methods: ['GET'])]
    public function getExecTokenUserConnected(WebServicesExec $webServicesExec): JsonResponse
    {
        return $this->json($webServicesExec->getContent('getToken'));
    }
}
