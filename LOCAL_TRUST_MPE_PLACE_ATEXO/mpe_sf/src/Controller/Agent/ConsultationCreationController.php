<?php

namespace App\Controller\Agent;

use App\Entity\ConfigurationOrganisme;
use App\Entity\Consultation;
use App\Entity\LtReferentiel;
use App\Handler\UploadDocumentHandler;
use App\Repository\ConfigurationClientRepository;
use App\Repository\ConfigurationOrganismeRepository;
use App\Repository\Procedure\TypeProcedureOrganismeRepository;
use App\Service\Consultation\ConsultationService;
use App\Service\CurrentUser;
use App\Service\WebservicesMpeConsultations;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\ConcentrateurManager;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ConsultationCreationController extends AbstractController
{
    public function __construct(
        private readonly CurrentUser $currentUser,
        private readonly TypeProcedureOrganismeRepository $typeProcedureOrganismeRepository,
        private readonly AuthorizationCheckerInterface $authorizationChecker,
        private readonly EntityManagerInterface $entityManager,
        private readonly WebservicesMpeConsultations $wsConsultations,
    ) {
    }

    #[Route(
        path: '/agent/consultation/creation',
        name: 'agent_consultation_creation',
        methods: ['GET']
    )]
    #[Security("is_granted('CONSULTATION_CREATE') or is_granted('CONSULTATION_SUITE_CREATE')")]
    public function creation(Request $request, UploadDocumentHandler $uploadDocumentHandler): Response
    {
        $contratId = $request->query->get('contratId');
        $contratUuid = $request->query->get('contratUuid');
        $consultationParentId = $request->query->get('consultationParentId');

        $uploadDocumentHandler->handle($request);

        return $this->render('agent/consultation-elaboration/index.html.twig', [
            'isFullWidth'           => true,
            'contratId'             => $contratId,
            'contratUuid'           => $contratUuid,
            'consultationParentId'  => $consultationParentId,
            'organismAcronym'       => $this->getUser()->getOrganisme()->getAcronyme(),
        ]);
    }

    #[Route(
        path: '/agent/consultation/modification/{uuid}',
        name: 'agent_consultation_modification',
        options: ['expose' => true],
        methods: ['GET']
    )]
    #[Security("is_granted('CONSULTATION_UPDATE', consultation)")]
    public function modification(Consultation $consultation): Response
    {
        $isSimplifie = $this->typeProcedureOrganismeRepository->findOneBy([
            'idTypeProcedure' => $consultation->getIdTypeProcedureOrg(),
            'organisme' => $consultation->getAcronymeOrg()
        ])?->getProcedureSimplifie();

        if ($isSimplifie) {
            return $this->redirectToRoute('atexo_consultation_simplifie_edit', ['id' => $consultation->getId()]);
        }
        return $this->render('agent/consultation-elaboration/index.html.twig', [
            'isFullWidth'      => true,
            'consultationId'   => $consultation->getId(),
            'organismAcronym'  => $this->getUser()->getOrganisme()->getAcronyme(),
            'consultationUuid'  => $consultation->getUuid(),
        ]);
    }

    #[Route(
        path: '/agent/consultation/modification/{uuid}/publicite',
        name: 'agent_consultation_publicite',
        options: ['expose' => true],
        methods: ['GET']
    )]
    public function publicite(
        Consultation $consultation,
        ParameterBagInterface $parameterBag,
        ConcentrateurManager $concentrateur,
        LoggerInterface $publiciteLogger
    ): Response {
        $error = false;
        $accesToken = null;
        try {
            $accesToken = $concentrateur->addAgent($consultation);
            $concentrateur->configAnnonces($consultation, $accesToken, []);
        } catch (\Exception $exception) {
            $publiciteLogger->error(
                sprintf(
                    'Probleme lors recuperation du token Concentrateur : %s - %s',
                    $exception->getMessage(),
                    $exception->getTraceAsString()
                )
            );
            $error = true;
        }

        return $this->render('agent/consultation-elaboration/publicite.html.twig', [
            'isFullWidth'       => true,
            'consultationId'    => $consultation->getId(),
            'organismAcronym'   => $this->getUser()->getOrganisme()->getAcronyme(),
            'urlPf'             => $parameterBag->get('PF_URL_REFERENCE'),
            'uidPf'             => $parameterBag->get('UID_PF_MPE'),
            'traductions'       => $concentrateur->getPubTranslate(),
            'accesToken'        => $accesToken,
            'error'             => $error,
            'consultationUuid'  => $consultation->getUuid(),
        ]);
    }

    #[Route(
        path: '/agent/consultation/parameters',
        name: 'agent_consultation_parameters',
        methods: ['GET']
    )]
    public function getParameters(
        ParameterBagInterface $parameterBag,
        EntityManagerInterface $entityManager,
        ConfigurationOrganismeRepository $configurationOrganismeRepository,
        ConfigurationClientRepository $configurationClientRepository
    ): Response {
        $configurationOrganime = $configurationOrganismeRepository
            ->findOneBy(['organisme' => $this->getUser()->getAcronymeOrganisme()]);

        $baseDceEnabled = $configurationOrganime->getBaseDce() === '1';
        $limitDefaultHour = $configurationOrganime
            ->isHeureLimiteDeRemiseDePlisParDefaut() ?? ($parameterBag->get('HEURE_CLOTURE_DEFAUT') ?? '17:00');

        $codesNacres = $entityManager
            ->getRepository(LtReferentiel::class)
            ->findBy(['codeLibelle' => 'NOMENCLATURE_ACHAT']);

        $activerOptionCarteAccueil = $configurationClientRepository
            ->findOneBy(['parameter' => 'ACTIVER_OPTION_CARTE_PAGE_ACCUEIL']);
        $surchargeCarte = !empty($activerOptionCarteAccueil) && $activerOptionCarteAccueil->getValue() == '0';
        if ($surchargeCarte) {
            $disableNationalMap = $configurationClientRepository
                ->findOneBy(['parameter' => 'MASQUER_CARTE_NATIONAL_IDENTIFICATION']);
        }

        $poursuivreAffichageOptions = json_decode(
            str_replace(
                "'",
                '"',
                $parameterBag->get('NBR_JOURS_POURSUIVRE_AFFICHAGE')
            )
        );

        $isTimeZoneActive = $this->getUser()->getOrganisme()?->getActivationFuseauHoraire() === '1';
        if (!empty($this->getUser()->getService())) {
            $isTimeZoneActive = $this->getUser()->getService()?->getActivationFuseauHoraire() === '1';
        }
        $localTimeZoneEnabled = $configurationOrganime->getFuseauHoraire() === '1' && $isTimeZoneActive;
        $maxBytesFilesSize = $parameterBag->get('maxBytesFilesSize');

        return $this->json([
            'baseDceEnabled'                => $baseDceEnabled,
            'codesNacres'                   => !empty($codesNacres),
            'delaiDLRO'                     => $parameterBag->get('DELAI_DLRO'),
            'surchargeCarte'                => $surchargeCarte,
            'disableNationalMap'            => !empty($disableNationalMap) && $disableNationalMap->getValue() == '1',
            'execContratEnabled'            => !empty($parameterBag->get('ACTIVE_EXEC_V2')),
            'poursuivreAffichageOptions'    => $poursuivreAffichageOptions,
            'localTimeZoneEnabled'          => $localTimeZoneEnabled,
            'maxBytesFilesSize'             => $maxBytesFilesSize,
            'limitDefaultDate'              => [
                'hours'     => explode(':', $limitDefaultHour)[0],
                'minutes'   => explode(':', $limitDefaultHour)[1]
            ]
        ]);
    }

    #[Route(
        path: '/agent/consultation/lieu/map',
        name: 'agent_consultation_lieu_map',
        methods: ['GET']
    )]
    public function getLieuMapData(ParameterBagInterface $parameterBag, EntityManagerInterface $entityManager): Response
    {
        if (!is_file('themes/map/MapData.json')) {
            return $this->json(['error' => 'File MapData does not exist'], Response::HTTP_NOT_FOUND);
        }

        $file = file_get_contents('themes/map/MapData.json');
        $originalFile = file_get_contents('../legacy/protected/themes/map/MapData.json');

        $maps = [
            'map'           => json_decode($file, true),
            'originalMap'   => json_decode($originalFile, true),
        ];

        return $this->json($maps);
    }
}
