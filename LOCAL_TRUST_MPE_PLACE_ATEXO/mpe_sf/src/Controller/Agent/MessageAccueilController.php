<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use App\Entity\MessageAccueil;
use App\Form\Agent\Enum\MessageAccueilInfo;
use App\Form\Agent\MessageAccueilEntreprise;
use App\Form\Agent\MessageAccueilAgent;
use App\Form\Agent\MessageAccueilAgentConnecte;
use App\Repository\MessageAccueilRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/agent', name: 'message_accueil_')]
class MessageAccueilController extends AbstractController
{
    public MessageAccueilInfo $destinataire = MessageAccueilInfo::DESTINATAIRE;
    public MessageAccueilInfo $authentifier = MessageAccueilInfo::AUTHENTIFIER;

    #[Route('/message/accueil', name: 'index')]
    public function index(): Response
    {
        $messageAccueil = new MessageAccueil();

        $formEntreprise = $this->createForm(MessageAccueilEntreprise::class, $messageAccueil);
        $formMessageAccueilAgent = $this->createForm(MessageAccueilAgent::class, $messageAccueil);
        $formAgentConnecte = $this->createForm(MessageAccueilAgentConnecte::class, $messageAccueil);

        return $this->render('message_accueil/index.html.twig', [
            'controller_name' => 'MessageAccueilController',
            'formAgentConnecte' => $formAgentConnecte->createView(),
            'agentConnected' => [
                'destinataire' => $this->destinataire->authenticatedAgent(),
                'authentifier' => $this->authentifier->authenticatedAgent(),
            ],
            'formAccueilAgent' => $formMessageAccueilAgent->createView(),
            'agentNonConnected' => [
                'destinataire' => $this->destinataire->unauthenticatedAgent(),
                'authentifier' => $this->authentifier->unauthenticatedAgent(),
            ],
            'formEntreprise' => $formEntreprise->createView(),
            'entreprise' => [
                'destinataire' => $this->destinataire->companies(),
                'authentifier' => $this->authentifier->companies(),
            ],
        ]);
    }

    #[Route('/message/accueil/save', name: 'save')]
    public function save(Request $request, MessageAccueilRepository $messageAccueilRepository): Response
    {
        $destinataire = $request->request->get('destinataire');
        $authentifier = $request->request->get('authentifier');
        $data = $request->request->get('data');

        $typeMessage = $request->request->get('selectedValueTypeMessage');
        $updateMessage = $messageAccueilRepository->updateMessageAccueil(
            $typeMessage,
            $data,
            $destinataire,
            $authentifier
        );

        return new Response($updateMessage);
    }
}
