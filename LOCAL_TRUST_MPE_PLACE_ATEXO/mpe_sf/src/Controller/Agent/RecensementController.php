<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use App\Service\WebServicesRecensement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;

class RecensementController extends AbstractController
{
    #[Route(path: '/agent/recensement/nukema', name: 'agent_recensement_nukema')]
    public function authAction(Request $request, WebServicesRecensement $client)
    {
        return $this->redirect($client->getContent('getUrlRecensement', $this->getUser()));
    }
}
