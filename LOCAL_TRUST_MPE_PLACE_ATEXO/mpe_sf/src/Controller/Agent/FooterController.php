<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use App\Service\AtexoUtil;
use App\Entity\Partenaire\Partenaire;
use App\Serializer\CertificatsAcrgsNormalizer;
use App\Service\ClientWs\ClientWsV2;
use App\Service\Footer\ConditionsUtilisation;
use App\Service\Footer\PrerequisTechnique;
use Application\Service\Atexo\Atexo_Config;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use SimpleXMLElement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route(path: '/{calledFrom}/footer', name: 'ecrans_', requirements: ['calledFrom' => 'entreprise|agent'])]
class FooterController extends AbstractController
{
    private const ENTREPRISE = 'entreprise';

    private Serializer $serializer;

    /**
     * FooterController constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(
        protected EntityManagerInterface $entityManager,
        private ParameterBagInterface $parameterBag,
        private AtexoUtil $atexoUtil
    ) {
        $normalizer = new CertificatsAcrgsNormalizer(new ObjectNormalizer());
        $this->serializer = new Serializer([$normalizer], [new XmlEncoder()]);
    }

    #[Route(path: '/info-site', name: 'info_site')]
    public function infoSite(string $calledFrom): Response
    {
        $info = [];
        $realisationEditoriale = $this->encodeUTF8(
            html_entity_decode($this->getParameter('INFOSITE_REALISATION_EDITORIALE'))
        );
        $directeurPublication = $this->encodeUTF8(
            html_entity_decode($this->getParameter('INFOSITE_DIRECTEUR_PUBLICATION'))
        );
        $info['PF_LONG_NAME_INFOSITE'] = $this->encodeUTF8($this->getParameter('PF_LONG_NAME_INFOSITE'));
        $info['INFOSITE'] = $this->encodeUTF8(html_entity_decode($this->getParameter('INFOSITE')));
        $info['INFOSITE_REALISATION_EDITORIALE'] = $this->encodeUTF8($realisationEditoriale);
        $info['INFOSITE_DIRECTEUR_PUBLICATION'] = $this->encodeUTF8($directeurPublication);

        $version = $this->getParameter('APP_BASE_ROOT_DIR') . '.version';
        $content = file($version);
        $info['MPE_VERSION'] = $this->encodeUTF8(str_replace('version=', '', $content['0']));
        $info['MPE_VERSION_DATE'] = $this->encodeUTF8(str_replace('date=', '', $content['1']));

        if (self::ENTREPRISE === $calledFrom) {
            return $this->render('ecrans-entreprise/info-site.html.twig', ['info' => $info]);
        }

        return $this->render('ecrans-agent/info-site.html.twig', ['info' => $info]);
    }

    #[Route(path: '/nos-partenaires', name: 'nos_partenaires')]
    public function nosPartenaires(string $calledFrom): Response
    {
        $partenaires = $this->entityManager->getRepository(Partenaire::class)->findAll();
        $twig = 'ecrans-agent/nos-partenaires.html.twig';
        if (self::ENTREPRISE === $calledFrom) {
            $twig = 'ecrans-entreprise/nos-partenaires.html.twig';
        }

        return $this->render($twig, ['partenaires' => $partenaires]);
    }

    #[Route(path: '/conditions-utilisation', name: 'condition_generales_utilisation')]
    public function conditionsGeneralesUtilisation(string $calledFrom, ConditionsUtilisation $cguService): Response
    {
        $twig = 'ecrans-agent/cgu.html.twig';
        if (self::ENTREPRISE === $calledFrom) {
            $twig = 'ecrans-entreprise/cgu.html.twig';
        }

        $visibiliteBlocCguSpecifique = $cguService->visibiliteBlocCguSpecifique($calledFrom);
        $libelleBlocCguSpecifique = $cguService->getLibelleBlocCguSpecifique($calledFrom);

        return $this->render($twig, [
            'visibilite_bloc_cgu_specifique' => $visibiliteBlocCguSpecifique,
            'libelle_bloc_cgu_specifique' => $libelleBlocCguSpecifique
        ]);
    }

    #[Route(path: '/accessibilite', name: 'accessibilite')]
    public function accessibilite(string $calledFrom): Response
    {
        $info = [];
        $info['PF_LONG_NAME'] = html_entity_decode($this->getParameter('PF_LONG_NAME'));
        $twig = 'ecrans-agent/accessibilite.html.twig';
        if (self::ENTREPRISE === $calledFrom) {
            $twig = 'ecrans-entreprise/accessibilite.html.twig';
        }

        return $this->render($twig, ['info' => $info]);
    }

    #[Route(path: '/prerequis-techniques', name: 'prerequis_techniques')]
    public function prerequisTechniques(string $calledFrom, PrerequisTechnique $prerequisTechnique): Response
    {
        $twig = 'ecrans-agent/prerequis-techniques.html.twig';
        if (self::ENTREPRISE === $calledFrom) {
            $twig = 'ecrans-entreprise/prerequis-techniques.html.twig';
        }

        return $this->render($twig, [
                'called_from' => $calledFrom,
                'mode_serveur_crypto' => $prerequisTechnique->getModeServeurCrypto($calledFrom),
                'isFullWidth' => true,
            ]);
    }

    #[Route(path: '/diagnostic-poste', name: 'diagnostic_poste')]
    public function diagnosticPoste(
        string $calledFrom,
        TranslatorInterface $translator,
        ParameterBagInterface $parameterBag
    ): Response {
        $translations = [
            'en_ligne' => $translator->trans('EN_LIGNE'),
            'hors_ligne' => $translator->trans('HORS_LIGNE'),
            'etat_assistant' => $translator->trans('ETAT_ASSISTANT_MARCHES_PUBLICS'),
            'lance_assistant' => $translator->trans('ASSISTANT_MARCHES_PUBLICS_LANCE'),
            'non_lance_assistant' => $translator->trans('ASSISTANT_MARCHES_PUBLICS_NON_LANCE'),
            'afficher_plus_informations' => $translator->trans('AFFICHER_PLUS_INFORMATIONS'),
        ];

        $urlCentralReferentiel = $parameterBag->get('URL_CENTRALE_REFERENTIELS');
        if (Atexo_Config::getParameter('SPECIFIQUE_PLACE')) {
            $urlCentralReferentiel = $parameterBag->get('URL_DOCS');
        }

        $urls = [
            'crypto' =>  $parameterBag->get('URL_CRYPTO'),
            'url_central_referentiel' => $urlCentralReferentiel,
            'windows' => $parameterBag->get('LIEN_TELECHARGEMENT_CLIENT_LOURD_WINDOWS'),
            'linux' => $parameterBag->get('LIEN_TELECHARGEMENT_CLIENT_LOURD_LINUX'),
            'macos' => $parameterBag->get('LIEN_TELECHARGEMENT_CLIENT_LOURD_MACOS'),
        ];

        return $this->render('ecrans-entreprise/diagnostic-poste.html.twig', [
            'called_from' => $calledFrom,
            'urls' => $urls,
            'url_crypto' => $parameterBag->get('URL_CRYPTO'),
            'plateform' => $parameterBag->get('UID_PF_MPE'),
            'translations' => base64_encode(json_encode($translations, JSON_THROW_ON_ERROR)),
        ]);
    }

    #[Route(path: '/test-config', name: 'test_config')]
    public function testConfig(): RedirectResponse
    {
        $test = $this->atexoUtil->getPfUrlDomaine(
            $this->parameterBag->get('URL_CRYPTO_OUTIL_TEST')
        );

        return new RedirectResponse($test);
    }

    #[Route(path: '/info-rgs', name: 'info_rgs')]
    public function infoRgs(string $calledFrom): Response
    {
        $twig = 'ecrans-agent/info-rgs.html.twig';
        if (self::ENTREPRISE === $calledFrom) {
            $twig = 'ecrans-entreprise/info-rgs.html.twig';
        }

        return $this->render($twig);
    }

    #[Route(path: '/info-acrgs', name: 'info_acrgs')]
    public function infoAcrgs(string $calledFrom): Response
    {
        $cacheDataFile = $this->getParameter('DOCUMENT_ROOT_DIR') . '/protected/var/cache/liste-acrgs.xml';
        /*checker si le xml est a jour*/
        if (!$this->isXmlUpToDate($cacheDataFile)) {
            try {
                $client = new ClientWsV2($this->parameterBag, ['verify' => false]);
                $resp = $client->get($this->getParameter('LISTE_AC_RGS_TSL_FR'));
                $xml_str = '';
                if($resp) {
                    $xml_str = $resp->getBody();
                }
                $xml = new SimpleXMLElement($xml_str);
                if (!empty($xml)) {
                    file_put_contents($cacheDataFile, $this->serializer->serialize($xml, 'xml'));
                    $data = $this->serializer->decode(file_get_contents($cacheDataFile), 'xml');
                }
            } catch (\Exception $e) {

            }
        }

        $twig = 'ecrans-agent/liste-acrgs.html.twig';
        if (self::ENTREPRISE === $calledFrom) {
            $twig = 'ecrans-entreprise/liste-acrgs.html.twig';
        }

        return $this->render($twig, ['data' => $data]);
    }

    #[Route(path: '/download-certificat/{indexPere}/{index}', name: 'download_certificat')]
    public function downloadCertificatAction($indexPere, $index)
    {
        $rootDir = $this->getParameter('DOCUMENT_ROOT_DIR');
        $cacheDataFile = $rootDir . '/protected/var/cache/liste-acrgs.xml';
        $data = $this->serializer->decode(file_get_contents($cacheDataFile), 'xml');
        $nameCertif = $data[$indexPere]['ACFilles'][$index]['CN'] . '.cer';
        $certificat = $data[$indexPere]['ACFilles'][$index]['Certificate'];

        $certif = $rootDir . '/protected/var/cache/certificat.cer';
        file_put_contents($certif, $certificat);

        $file = new File($certif);

        return $this->file($file, $nameCertif);
    }

    private function isXmlUpToDate($cacheDataFile)
    {
        if (
            !file_exists($cacheDataFile) || strtotime('now') >=
            strtotime('+' . $this->getParameter('NBR_JOURS_AFFICHAGE_RGS') . ' DAY', filemtime($cacheDataFile))
        ) {
            return false;
        }
        if (empty(file_get_contents($cacheDataFile))) {
            return false;
        }

        return true;
    }

    private function encodeUTF8(string $string): false|string
    {
        $encoding = mb_detect_encoding($string, 'utf-8', true);
        if ($encoding === false) {
            $string = utf8_encode($string);
        }
        return $string;
    }

    #[Route(path: '/protection-donnees', name: 'protection_donnees')]
    public function protectionDonneesAction($calledFrom)
    {
        if ($calledFrom === self::ENTREPRISE) {
            return $this->render('ecrans-entreprise/protection-donnees.html.twig');
        }
        return $this->render('ecrans-agent/protection-donnees.html.twig');
    }

    /**
     * @param string $calledFrom
     * @return Response
     */
    #[Route(path: '/plan-du-site', name: 'plan_site')]
    public function planSite(string $calledFrom)
    {
        $data = ['called' => $calledFrom];

        if ($calledFrom === self::ENTREPRISE) {
            return $this->render('ecrans-entreprise/plan-site.html.twig', $data);
        }

        return $this->render('ecrans-agent/plan-site.html.twig', $data);
    }
}
