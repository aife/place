<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use App\Entity\ConfigurationPlateforme;
use App\Service\DossierVolumineux\DossierVolumineuxService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route(path: '/{calledFrom}/aide', name: 'aides_', requirements: ['calledFrom' => 'entreprise|agent'])]
class AideController extends AbstractController
{
    private const ENTREPRISE = 'entreprise';

    /**
     * AideController constructor.
     */
    public function __construct(
        private readonly TranslatorInterface $translator,
        private readonly EntityManagerInterface $em,
        private SessionInterface $session,
        private ParameterBagInterface $parameterBag
    ) {
    }

    #[Route(path: '/assistance-telephonique', name: 'assistance_telephonique')]
    public function assistanceTelephonique($calledFrom)
    {
        $locale = $this->session->get('_locale') ?? $this->parameterBag->get('locale');
        $twig = 'ecrans-agent/assistance-telephonique.html.twig';
        $info = [];
        $info['utah'] = $this->getParameter('UTILISER_UTAH');
        $info['hours'] = $this->getParameter('DISPLAY_CRENEAUX_HORAIRES');
        $info['outils'] = $this->getParameter('URL_DOCS_OUTILS') . $this->getParameter('DOCS_OUTILS');
        $val = 'goToUtah()';
        $info['retour'] = str_replace(
            'FUNCTION_ONCLICK',
            $val,
            $this->translator->trans('TEXT_AIDE_ASSISTANCE_ENTREPRISE_UTAH', [], null, $locale)
        );
        if (self::ENTREPRISE === $calledFrom) {
            $twig = 'ecrans-entreprise/assistance-telephonique.html.twig';
        }
        return $this->render($twig, ['info' => $info]);
    }

    #[Route(path: '/outils-informatiques', name: 'outils_informatiques')]
    public function outilsInformatiques(string $calledFrom, DossierVolumineuxService $dossierVolService): Response
    {
        $module = $this->em->getRepository(ConfigurationPlateforme::class)->getConfigurationPlateforme();

        $info = [];
        $info['outils'] = $this->getParameter('URL_DOCS_OUTILS');
        $info['windows'] = $this->getParameter('URL_UTILITAIRE_DOWNLOAD_DV_WINDOWS');
        $info['mac'] = $this->getParameter('URL_UTILITAIRE_DOWNLOAD_DV_MAC');
        $info['linux'] = $this->getParameter('URL_UTILITAIRE_DOWNLOAD_DV_LINUX');
        $info['sign'] = $module->getMasquerAtexoSign();
        $info['dossier'] = $dossierVolService->isEnable();
        $info['horodatage'] = $module->getTelechargementOutilVerifHorodatage();
        $twig = 'ecrans-agent/outils-informatiques.html.twig';

        if (self::ENTREPRISE === $calledFrom) {
            $twig = 'ecrans-entreprise/outils-informatiques.html.twig';
        }

        return $this->render($twig, ['info' => $info]);
    }

    #[Route(path: '/dechiffrement-hors-ligne', name: 'dechiffrement_hors_ligne')]
    public function dechiffrementHorsLigne(string $calledFrom): Response
    {
        $twig = 'ecrans-agent/dechiffrement-hors-ligne.html.twig';
        return $this->render($twig);
    }
}
