<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Entity\Agent;
use App\Service\AtexoAlertes;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * Administration des organismes.
     *
     * @return RedirectResponse
     */
    #[Route(path: '/administration')]
    public function administration()
    {
        return $this->redirect('/?page=Administration.Home', Response::HTTP_MOVED_PERMANENTLY);
    }
}
