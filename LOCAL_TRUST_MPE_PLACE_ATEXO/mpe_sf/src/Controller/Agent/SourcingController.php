<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use App\Service\WebServicesSourcing;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SourcingController extends AbstractController
{
    #[Route(path: '/agent/sourcing/nukema', name: 'agent_sourcing_nukema')]
    public function authAction(Request $request, WebServicesSourcing $client)
    {
        return $this->redirect($client->getContent('getUrlSourcing', $this->getUser()));
    }
}
