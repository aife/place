<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent\Organisme;

use App\Entity\Organisme;
use App\Form\Agent\FieldType\Organisme\OrganismeRechercheType;
use App\Model\OrganismeModel;
use App\Service\AtexoConfiguration;
use App\Service\CurrentUser;
use App\Service\Organisme\SearchOrganismeTools;
use App\Service\Paginator;
use App\Service\WebServices\Organisme\WebservicesOrgansimes;
use App\Traits\BasicControllerTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SearchController
 */
#[Route(path: '/agent/organisme/recherche', name: 'organisme_search_')]
class SearchController extends AbstractController
{
    use BasicControllerTrait;

    public function __construct(
        private readonly CurrentUser $currentUser,
        private readonly EntityManagerInterface $entityManager,
        private readonly WebservicesOrgansimes $wsOrganismes,
    ) {
    }

    #[Route(path: '', name: 'index')]
    public function index(
        Request $request,
        LoggerInterface $logger,
        AtexoConfiguration $config,
        Paginator $paginator,
        SearchOrganismeTools $tools
    ): Response {
        //check habilitation
        if (
            !$this->currentUser->checkHabilitation('HyperAdmin')
            || (!$config->isModuleEnabled('GestionOrganismes', null)
                || !$this->currentUser->checkHabilitation('GererOrganismes'))
        ) {
            return $this->redirectToRoute('atexo_agent_accueil_index');
        }

        $organismeModels = $this->entityManager->getRepository(Organisme::class)->getOrganismsForSearchPage();

        $form = $this->createForm(OrganismeRechercheType::class, null, ['organismes' => $organismeModels]);
        $form->handleRequest($request);
        $organismes = new ArrayCollection();

        if ($form->isSubmitted()) {
            try {
                $data = $tools->mapFilters($form->getData());
                if ($request->query->has('page')) {
                    $data['page'] = ($request->query->get('page') > 0) ? $request->query->get('page') : 1 ;
                }

                if ($request->query->has('itemsPerPage')) {
                    $data['itemsPerPage'] = $request->query->get('itemsPerPage');
                }

                $wsResponse = $this->wsOrganismes->getContent('searchOrganismes', $data);
                $paginator = $this->handlePaginator($request, $paginator, $wsResponse);

                if ($data['page'] > $paginator['last'] && '' !== $paginator['last']) {
                    return $this->redirect($paginator['goto'] . $paginator['last']);
                }

                /** @var OrganismeModel[] $organismes */
                $organismes = $tools->getOrganismes($wsResponse);
            } catch (\Exception $e) {
                $logger->error('Erreur à la récupération des organismes via API Platform');
                $logger->error('Erreur : ' . $e->getMessage() . ' ' . $e->getTraceAsString());
                $this->addFlash('error', 'Erreur lors de la récupération des organismes.');
            }
        }

        return $this->render('agent/administration/organisme/index.html.twig', [
            'isFullWidth'       => true,
            'form'              => $form->createView(),
            'organismes'        => $organismes,
            'searchLaunched'    => $request->query->get('search') !== null,
            'paginator'         => $paginator
        ]);
    }
}
