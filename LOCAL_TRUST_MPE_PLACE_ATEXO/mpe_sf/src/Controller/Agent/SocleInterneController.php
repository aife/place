<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route(path: '/agent/socle-interne', name: 'atexo_agent_accueil_socle_interne_')]
class SocleInterneController extends AbstractController
{
    private const AGENT_AUTHENTIFIE_SOCLE_INTERNE = "/?page=Agent.AccueilAgentAuthentifieSocleinterne";
    #[Route(path: '/', name: 'index')]
    public function index(Request $request) : Response
    {
        return $this->redirect(self::AGENT_AUTHENTIFIE_SOCLE_INTERNE);
    }
}
