<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserWidgetController extends AbstractController
{
    public function userWidget(): Response
    {
        return $this->render('agent/user-widget.html.twig');
    }
}
