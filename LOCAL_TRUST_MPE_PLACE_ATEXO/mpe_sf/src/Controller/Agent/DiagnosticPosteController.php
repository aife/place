<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use App\Service\AtexoConfiguration;
use Application\Service\Atexo\Atexo_Config;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;


#[Route(path: '/agent/diagnostic-poste', name: 'dignostic_poste_')]
class DiagnosticPosteController extends AbstractController
{
    public function __construct(
        private readonly AtexoConfiguration $configuration
    )
    {

    }

    #[Route(path: '/', name: 'index')]
    public function indexAction(
        TranslatorInterface $translator,
        ParameterBagInterface $parameterBag
    ): Response
    {
        $user = $this->getUser();
        $confOrganisme = $this->configuration->getConfigurationOrganisme($user->getOrganisme());

        $calledFrom = 'agent';
        $translations = [
            'en_ligne' => $translator->trans('EN_LIGNE'),
            'hors_ligne' => $translator->trans('HORS_LIGNE'),
            'etat_assistant' => $translator->trans('ETAT_ASSISTANT_MARCHES_PUBLICS'),
            'lance_assistant' => $translator->trans('ASSISTANT_MARCHES_PUBLICS_LANCE'),
            'non_lance_assistant' => $translator->trans('ASSISTANT_MARCHES_PUBLICS_NON_LANCE'),
            'afficher_plus_informations' => $translator->trans('AFFICHER_PLUS_INFORMATIONS'),
        ];

        $urlCentralReferentiel = $parameterBag->get('URL_CENTRALE_REFERENTIELS');
        if (Atexo_Config::getParameter('SPECIFIQUE_PLACE')) {
            $urlCentralReferentiel = $parameterBag->get('URL_DOCS');
        }

        $urls = [
            'crypto' =>  $parameterBag->get('URL_CRYPTO'),
            'url_central_referentiel' => $urlCentralReferentiel,
            'windows' => $parameterBag->get('LIEN_TELECHARGEMENT_CLIENT_LOURD_WINDOWS'),
            'linux' => $parameterBag->get('LIEN_TELECHARGEMENT_CLIENT_LOURD_LINUX'),
            'macos' => $parameterBag->get('LIEN_TELECHARGEMENT_CLIENT_LOURD_MACOS'),
        ];

        return $this->render('agent/diagnostic-poste/index.html.twig', [
            'called_from' => $calledFrom,
            'urls' => $urls,
            'url_crypto' => $parameterBag->get('URL_CRYPTO'),
            'plateform' => $parameterBag->get('UID_PF_MPE'),
            'translations' => base64_encode(json_encode($translations, JSON_THROW_ON_ERROR)),
            'mamp' => $confOrganisme->isActiverMonAssistantMarchesPublics(),
            'isFullWidth' => false,
        ]);


    }
}
