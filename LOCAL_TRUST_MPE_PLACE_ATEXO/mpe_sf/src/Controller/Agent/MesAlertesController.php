<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use App\Entity\Agent;
use App\Form\Agent\MesAlertesType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route(
    path: '/agent/mes-alertes',
    name: 'agent_mes_alertes',
    methods: [Request::METHOD_GET, Request::METHOD_POST]
)]
class MesAlertesController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        private readonly TranslatorInterface $translator
    ) {
    }

    public function __invoke(Request $request): Response
    {
        /** @var Agent $agent */
        $agent = $this->getUser();

        $form = $this->createForm(MesAlertesType::class, $agent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('success', $this->translator->trans('MES_ALERTES_MAJ_SUCCES'));
        }

        return $this->render('agent/mes-alertes/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
