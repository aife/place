<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent\Consultation;

use App\Entity\Consultation;
use App\Service\Consultation\DuplicationConsultation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/agent/consultations/{consultation}/duplication/', name: 'agent_consultations_duplication')]
class DuplicationConsultationController extends AbstractController
{
    public function __construct(
        private readonly DuplicationConsultation $duplicationConsultation
    ) {
    }

    public function __invoke(Request $request, Consultation $consultation): Response
    {
        $duplicatedConsultationId = $this->duplicationConsultation->duplicate($consultation, $request);

        return $this->redirectToRoute('consultation_search_index', [
            'id' => [$consultation->getId(), $duplicatedConsultationId],
            'search' => true
        ]);
    }
}
