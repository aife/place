<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent\Consultation;

use App\Entity\Lot;
use App\Entity\Referentiel\Consultation\ClausesN1;
use DateTimeInterface;
use Exception;
use App\Doctrine\Extension\ConsultationIdExtension;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Form\Agent\ConsultationRechercheType;
use App\Service\Paginator;
use App\Service\Agent\ConsultationSimplifieeService;
use App\Service\Consultation\ConsultationService;
use App\Service\Consultation\SearchAgentService;
use App\Service\Perimeter\PerimetreVisionService;
use App\Service\WebservicesMpeConsultations;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;
use Symfony\Component\HttpClient\Exception\ServerException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SearchController.*/
#[Route(path: '/agent/consultation/recherche', name: 'consultation_search_')]
class SearchController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ConsultationSimplifieeService $consultationSimplifieeService,
        private readonly WebservicesMpeConsultations $wsConsultations,
        private readonly PerimetreVisionService $perimetreVisionService,
    ) {
    }

    #[Route(path: '', name: 'index', options: ['expose' => true])]
    public function index(
        Request $request,
        ConsultationService $consultationService,
        LoggerInterface $logger,
        Paginator $paginator
    ): Response {
        /** @var Agent $agent */
        $agent = $this->getUser();
        // On pourrait mettre en cache cette donnée
        $typesProcedure = $this->entityManager->getRepository(TypeProcedureOrganisme::class)
            ->findBy(['organisme' => $agent->getAcronymeOrganisme()], ['libelleTypeProcedure' => 'asc']);
        $formDataInit = $this->prepareFormDataInit(
            $typesProcedure,
            $request,
            $this->consultationSimplifieeService
        );
        $form = $this->createForm(ConsultationRechercheType::class, null, $formDataInit);
        $form->handleRequest($request);
        $consultations = new ArrayCollection();
        if ($form->isSubmitted()) {
            try {
                $data = $this->mapFilters($form->getData(), $request);
                if ($request->query->has('page')) {
                    $data['page'] = ($request->query->get('page') > 0) ? $request->query->get('page') : 1 ;
                }

                if ($request->query->has('itemsPerPage')) {
                    $data['itemsPerPage'] = $request->query->get('itemsPerPage');
                }
                $wsResponse = $this->wsConsultations->getContent('searchConsultations', $data);

                $idsList = [];
                $listWithStatuses = [];
                $paginator = $paginator->getPagination($wsResponse->{'hydra:view'}, $request->getRequestUri());
                $paginator['total'] = $wsResponse->{'hydra:totalItems'};

                if ($data['page'] > $paginator['last'] && '' !== $paginator['last']) {
                    return $this->redirect($paginator['goto'] . $paginator['last']);
                }

                foreach ($wsResponse->{'hydra:member'} as $item) {
                    $idsList[] = $item->{'id'};
                    $listWithStatuses[(string)$item->{'id'}] = explode(',', (string) $item->{'statutCalcule'});
                }

                /** @var Consultation[] $consultations */
                $consultations = $this->entityManager->getRepository(Consultation::class)
                    ->findBy(['id' => $idsList], ['datefin' => 'DESC']);

                foreach ($consultations as $consultation) {
                    $consultation->pictograms = [];
                    $consultation->isFavori = $consultationService
                        ->isFavoriByAgent($consultation, $agent);
                    $consultation->statutCalcule = $listWithStatuses[(string)$consultation->getId()];
                    $consultation->typeProcedureOrg =
                        $this->entityManager->getRepository(TypeProcedureOrganisme::class)->findOneBy(
                            [
                                'idTypeProcedure' => $consultation->getIdTypeProcedureOrg(),
                                'organisme' => $consultation->getAcronymeOrg()
                            ]
                        );

                    $consultation->pictograms = $this->getPictograms($consultation);
                }
            } catch (ServerException) {
                $logger->error('Erreur à la récupération des consultations via API Platform');
                $this->addFlash('error', 'Erreur lors de la récupération des consultations.');
            }
        }

        return $this->render('consultation/Agent/search/index.html.twig', [
            'isFullWidth'       => true,
            'form'              => $form->createView(),
            'consultations'     => $consultations,
            'displayCriteria'   => $this->displayCriteria($request),
            'searchLaunched'    => $request->query->get('search') !== null,
            'paginator'         => $paginator
        ]);
    }

    protected function getPictograms($consultation)
    {
        $pictograms = [];
        $lotSearch = $this->entityManager->getRepository(Lot::class)
            ->findOneBy(
                ['consultation' => $consultation->getId()]
            );

        if ($lotSearch) {
            $clauseSearch = $this->entityManager->getRepository(Consultation\ClausesN1::class)
                ->findBy(['lot' => $lotSearch->getId()]);
        } else {
            $clauseSearch = $this->entityManager->getRepository(Consultation\ClausesN1::class)
                ->findBy(['consultation' => $consultation->getId()]);
        }

        foreach ($clauseSearch as $clause) {
            if ($clause->getReferentielClauseN1()->getSlug() ==  ClausesN1::CLAUSES_SOCIALES) {
                $pictograms[] = [
                    'image' => '/themes/images/picto-clause-sociale.gif',
                    'translation' => 'CONSIDERATIONS_SOCIALES'
                ];
            }

            if ($clause->getReferentielClauseN1()->getSlug() == ClausesN1::CLAUSES_ENVIRONNEMENTALES) {
                $pictograms[] = [
                    'image' =>  '/themes/images/picto-clause-environnementale.gif',
                    'translation' => 'CONSIDERATIONS_ENVIRONNEMENTALES'
                ];
            }
        }

        return $pictograms;
    }

    #[Route(path: '/statistiques', name: 'stats', methods: ['GET'])]
    public function getStatistics(Request $request, SearchAgentService $searchAgentService): JsonResponse
    {
        session_write_close();
        /** @var Agent $agent */
        $agent = $this->getUser();
        $typesProcedure = $this->entityManager->getRepository(TypeProcedureOrganisme::class)
            ->findBy(['organisme' => $agent->getAcronymeOrganisme()]);
        $formDataInit = $this->prepareFormDataInit(
            $typesProcedure,
            $request,
            $this->consultationSimplifieeService
        );

        $formDataInit['isStat'] = true;
        $form = $this->createForm(ConsultationRechercheType::class, null, $formDataInit);
        $form->handleRequest($request);
        $data = $this->mapFilters($form->getData(), $request);

        // On fait ceci car on souhaite que StatutCalcule soit l'un des premiers paramètres
        krsort($data);
        if (!$data['groupBy']) {
            throw new ParameterNotFoundException(
                'Wrong groupBy parameter value'
            );
        }
        $wsResponse = $this->wsConsultations->getContent('searchConsultations', $data);
        $response = $searchAgentService->getDataWidgetsStats($wsResponse, $data);
        return $this->json($response);
    }

    protected function mapFilters(array $data, ?Request $request = null): array
    {
        if (isset($data['keyWord'])) {
            $data['search_full'] = explode(' ', (string) $data['keyWord']);
            unset($data['keyWord']);
        }

        if (isset($data['typeProcedureOrganisme'])) {
            $data['idTypeProcedureOrg'] = $data['typeProcedureOrganisme']->getIdTypeProcedure();
        }

        if (isset($data['statut'])) {
            $data['statutCalcule'] = $data['statut'];
            unset($data['statut']);
        }

        if (isset($data['form_lieuxExecution'])) {
            $data['lieuExecution'] = explode(',', trim($data['form_lieuxExecution'], ','));
            unset($data['form_lieuxExecution']);
            unset($data['form_lieuxExecutionSelect']);
        }

        $dateFields = ['dlro' => 'datefin', 'miseEnLigne' => 'dateMiseEnLigneCalcule'];
        $dateFieldsSuffixes = ['Avant' => 'before', 'Apres' => 'after'];

        foreach ($dateFields as $dateField => $apiField) {
            foreach ($dateFieldsSuffixes as $dateFieldsSuffix => $apiFieldSuffix) {
                $fullDateField = $dateField . $dateFieldsSuffix;
                if (isset($data[$fullDateField]) && $data[$fullDateField] instanceof DateTimeInterface) {
                    $data[$apiField][$apiFieldSuffix] = $data[$fullDateField]->format('Y-m-d');
                    unset($data[$fullDateField]);
                }
            }
        }

        $data['idCreateur'] = $request->query->get('idCreateur') ?? null;

        if (
            ($ids = $request->query->get('id'))
            && is_array($ids)
            && count($ids) > 0
        ) {
            $data['id'] = $ids;
        }

        $data[ConsultationIdExtension::IDS_ONLY] = 1;

        return $data;
    }

    /**
     * @return array[]
     * @throws Exception
     */
    protected function prepareFormDataInit(
        array $typesProcedure,
        Request $request,
        ConsultationSimplifieeService $consultationSimplifieeService,
    ): array {
        $formDataInit = [
            'types_procedures' => $typesProcedure,
            'rma' => $request->query->get('rma')
        ];

        if ($request->query->has('form_lieuxExecution')) {
            $lieuxExecutionIds = trim($request->query->get('form_lieuxExecution'), ',');
            $formDataInit['lieuxExecution'] = $consultationSimplifieeService->idsToGeoN2($lieuxExecutionIds);
            $formDataInit['lieuxExecutionIds'] = $lieuxExecutionIds;
        }

        return $formDataInit;
    }

    private function displayCriteria(Request $request): bool
    {
        $queryParams = $request->query->all();

        if (!isset($queryParams['search'])) {
            return true;
        }

        foreach ($queryParams as $key => $value) {
            if (!in_array($key, ['keyWord', 'statut', 'search', 'page', 'itemsPerPage']) && !empty($value)) {
                return true;
            }
        }

        return false;
    }

    #[Route(path: '/agents-referent', name: 'agents_referent', methods: ['GET'])]
    public function agentsReferent(): JsonResponse
    {
        session_write_close();
        return $this->json($this->perimetreVisionService->getAgentsCreateurInPerimeterVision());
    }
}
