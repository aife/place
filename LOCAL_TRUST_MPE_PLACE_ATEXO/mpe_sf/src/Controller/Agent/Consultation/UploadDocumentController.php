<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent\Consultation;

use App\Handler\UploadDocumentHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route(
    path: '/agent/document/upload',
    name: 'agent_document_upload',
    options: ['expose' => true],
    methods: [Request::METHOD_POST]
)]
#[Route(
    path: '/agent/document/upload/{token?}',
    name: 'agent_document_upload_token',
    requirements: ['token' => '.+'],
    methods: [
        Request::METHOD_POST,
        Request::METHOD_GET,
        Request::METHOD_PATCH,
        Request::METHOD_DELETE,
        Request::METHOD_HEAD,
    ]
)]
#[IsGranted('ROLE_AGENT')]
class UploadDocumentController extends AbstractController
{
    public function __invoke(Request $request, UploadDocumentHandler $uploadDocumentHandler)
    {
        return $uploadDocumentHandler->handle($request->getMethod());
    }
}
