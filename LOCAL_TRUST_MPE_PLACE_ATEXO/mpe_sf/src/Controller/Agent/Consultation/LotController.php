<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent\Consultation;

use App\Entity\ConfigurationPlateforme;
use App\Entity\Consultation;
use App\Entity\Lot;
use App\Service\Authorization\AuthorizationAgent;
use App\Service\CpvService;
use App\Service\LotCandidature;
use App\Service\LotService;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class LotController.
 **/
#[Route(path: '/agent/consultation/lot', name: 'atexo_lot')]
class LotController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $em
    ) {
    }

    private function readFirstTabExcelFile($excel)
    {
        $sheet = $excel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        return [
            'sheet' => $sheet,
            'highestRow' => $highestRow,
            'highestColumn' => $highestColumn,
        ];
    }

    private function checkFieldOui1OrNon2($field)
    {
        return ('OUI' == mb_strtoupper($field)) ? 1 : 2;
    }

    private function checkFieldOui1OrNon0($field)
    {
        return ('OUI' == mb_strtoupper($field)) ? 1 : 0;
    }

    private function transformValue($field)
    {
        $tab = [];
        $explode = explode(' - ', (string) $field);
        $result = 0;
        if (0 != $explode[0]) {
            $tab[] = $explode[0];
            $result = serialize($tab);
        }

        return $result;
    }

    /**
     * @param int $numLot
     * @param int $idConsultation
     * @param LotCandidature $lotCandidature
     * @return JsonResponse
     */
    #[Route(path: '/{numLot}/candidature/{idConsultation}', name: '_candidature')]
    public function getLotsCandidatureByLot(int $numLot, int $idConsultation, LotCandidature $lotCandidature)
    {
        return $this->json($lotCandidature->getNamesAndIdsForCandidats($numLot, $idConsultation));
    }

    #[Route(path: '/import_lot', name: 'import_lot')]
    public function importLotAction(
        Request $request,
        LotService $serviceLot,
        TranslatorInterface $translator,
        CpvService $cpvService,
        AuthorizationAgent $authAgent
    ) : Response {
        $message = [];
        $message['message'] = $translator->trans('VEUILLEZ_COMPLETER_MODELE_IMPORT_LOT');
        $message['put'] = $translator->trans('DEPOSER_VOTRE_FICHIER');
        $message['import'] = $translator->trans('DEFINE_IMPORTER_LOT');
        $message['file'] = $translator->trans('DEFINE_TEXT_FICHIER');
        $message['select'] = $translator->trans('SELECT_VOTRE_FICHIER');
        $rangeCpv = 4;
        $endOfLife = 0;
        $lots = [];
        $dataLots = [];
        $dataInsert = 0;
        $codeProb = -1;
        $messageProb = [];
        $badInsert = [];
        $badInsertCpvForCategory = [];
        $refresh = $request->query->get('idButtonRefresh');
        $urlModele = $this->getParameter('URL_DOCS_MODELE_IMPORT_LOTS');
        $consultationObj = null;
        $form = $this->createFormBuilder()
            ->add('attachement', FileType::class)
            ->getForm();
        $consultation = $request->query->get('consultation');
        $organisme = $request->query->get('acronyme');
        $configurationPlateforme = $this->em->getRepository(ConfigurationPlateforme::class)->getConfigurationPlateforme();

        if (!isset($consultation)) {
            throw new AccessDeniedException('Access Denied.');
        }

        if (!$authAgent->isInTheScope((int) $consultation)) {
            throw new AccessDeniedException('Access Denied.');
        }

        if (isset($consultation) && isset($organisme)) {
            $consultationObj = $this->em
                ->getRepository(Consultation::class)
                ->findOneBy([
                    'organisme' => $organisme,
                    'id' => $consultation,
                ]);
        }
        $form->handleRequest($request);
        $data = $form['attachement']->getData();
        if (
            !empty($data)
        ) {
            $codeProb = 0;
            // check excel format
            $inputFileType = IOFactory::identify($data);
            $extFileInfo = pathinfo($data->getClientOriginalName(), PATHINFO_EXTENSION);

            // if extension not valid with the format
            if (
                !(
                (
                    'Xlsx' == $inputFileType &&
                    'xlsx' == $extFileInfo
                ) ||
                (
                    'Xls' == $inputFileType &&
                    'xls' == $extFileInfo
                )
                )
            ) {
                $codeProb = 1;

                $messageProb['format_fichier_non_respecte'] = $translator->trans('FORMAT_FICHIER_NON_RESPECTE');
                $messageProb['format_ecceptes'] = $translator->trans('FORMATS_ACCEPTES');
                $messageProb['format_xlsx'] = $translator->trans('FORMAT_XLSX');
                $messageProb['format_xls'] = $translator->trans('FORMAT_XLS');

                return $this->render('consultation/Agent/import_lot.html.twig', [
                    'form' => $form->createView(),
                    'message' => $message,
                    'code_prob' => $codeProb,
                    'message_prob' => $messageProb,
                    'refresh' => $refresh,
                    'url_modele' => $urlModele,
                ]);
            }

            // read ExcelFile
            $objReader = IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($data);

            $excelInfo = $this->readFirstTabExcelFile($objPHPExcel);

            for ($row = 2; $row <= $excelInfo['highestRow']; ++$row) {
                // Read a row of data into an array
                $rowData = $excelInfo['sheet']->rangeToArray(
                    'A' . $row . ':' . $excelInfo['highestColumn'] . $row,
                    null,
                    true,
                    false
                );

                $lots[] = $rowData[0][0];
            }

            $listLot = $this->getDoctrine()
                ->getRepository(Lot::class)
                ->findLots($consultation, $organisme, $lots);

            foreach ($listLot as $item) {
                $dataLots[] = $item['lot'];
            }

            $entetes = [
                'number',
                'name',
                'description',
                'category',
                'code_cpv',
                'code_cpv_2',
                'code_cpv_3',
                'code_cpv_4',
                'social_condition',
                'execution_social_condition',
                'clause_specification_technique',
                'attribution_social_condition',
                'esat_ea_social_condition',
                'siae_social_condition',
                'eess_social_condition',
                'marche_insertion',
                'environment',
                'technical_environment',
                'execution_environment',
                'attribution_environment',
            ];

            //  Loop through each row of the worksheet in turn
            for ($row = 2; $row <= $excelInfo['highestRow']; ++$row) {
                // Read a row of data into an array
                $rowData = $excelInfo['sheet']->rangeToArray(
                    'A' . $row . ':' . $excelInfo['highestColumn'] . $row,
                    null,
                    true,
                    false
                );
                //  Insert row data array into your database of choice here
                ++$endOfLife;

                $myData = array_slice($rowData[0], 0, sizeof($entetes));
                $myData = array_combine($entetes, $myData);

                if ($myData['number'] === null) {
                    continue;
                }
                
                // If one field mandatory is null
                if (
                    null == $myData['number'] ||
                    null == $myData['name'] ||
                    null == $myData['description'] ||
                    null == $myData['category'] ||
                    null == $myData['code_cpv'] ||
                    null == $myData['social_condition'] ||
                    null == $myData['environment'] ||
                    in_array($myData['number'], $dataLots)
                ) {
                    $badInsert[] = $myData['number'];
                } elseif (!$cpvService->isValidCpv($myData['code_cpv'], $myData['category'])) {
                    $badInsertCpvForCategory[] = $myData['number'];
                } else {
                    $codeCpv2 = '#';
                    $myData['category'] = match (strtoupper(trim($myData['category']))) {
                        $this->getParameter('PRESTATION_TRAVAUX') => $this->getParameter('TYPE_PRESTATION_TRAVAUX'),
                        $this->getParameter('PRESTATION_FOURNITURES') => $this->getParameter(
                            'TYPE_PRESTATION_FOURNITURES'
                        ),
                        $this->getParameter('PRESTATION_SERVICES') => $this->getParameter('TYPE_PRESTATION_SERVICES'),
                    };

                    for ($i = 2; $i < $rangeCpv; ++$i) {
                        if (null != $myData['code_cpv_' . $i]) {
                            $codeCpv2 .= $myData['code_cpv_' . $i] . '#';
                        }
                    }

                    $myData['code_cpv_2'] = $codeCpv2;

                    //Clause sociale
                    $myData['social_condition'] = $this->checkFieldOui1OrNon2($myData['social_condition']);
                    $myData['execution_social_condition'] = $this->transformValue(
                        $myData['execution_social_condition']
                    );
                    $myData['clause_specification_technique'] = $this->transformValue(
                        $myData['clause_specification_technique']
                    );
                    $myData['attribution_social_condition'] = $this->transformValue(
                        $myData['attribution_social_condition']
                    );
                    $myData['esat_ea_social_condition'] = $this->checkFieldOui1OrNon0(
                        $myData['esat_ea_social_condition']
                    );
                    $myData['siae_social_condition'] = $this->checkFieldOui1OrNon0($myData['siae_social_condition']);
                    $myData['eess_social_condition'] = $this->checkFieldOui1OrNon0($myData['eess_social_condition']);
                    $myData['marche_insertion'] = $this->checkFieldOui1OrNon0($myData['marche_insertion']);

                    //Clause Environnement
                    $myData['environment'] = $this->checkFieldOui1OrNon2($myData['environment']);
                    $myData['technical_environment'] = $this->checkFieldOui1OrNon0($myData['technical_environment']);
                    $myData['execution_environment'] = $this->checkFieldOui1OrNon0($myData['execution_environment']);
                    $myData['attribution_environment'] = $this->checkFieldOui1OrNon0(
                        $myData['attribution_environment']
                    );

                    $data = [
                        'acronyme' => $organisme,
                        'consultation' => $consultation,
                        'row' => $myData,
                    ];

                    $serviceLot->importLot($data);
                    ++$dataInsert;
                }

                if (0 == count($badInsert) && 0 === count($badInsertCpvForCategory) && $dataInsert > 0) {
                    $codeProb = 0;
                    $messageProb['lot_correctement_importes'] = $translator->trans('LOT_CORRECTEMENT_IMPORTES');
                }
            }

            if (0 == count($badInsert) && 0 === count($badInsertCpvForCategory) && 0 == $dataInsert) {
                $codeProb = 3;
                $messageProb['aucune_donnee_trouvee'] = $translator->trans('AUCUNE_DONNEE_TROUVEE');
            }
        }
        if ((count($badInsert) > 0 || count($badInsertCpvForCategory) > 0) && $dataInsert >= 0) {
            $codeProb = 2;
            $xMessage = '';

            for ($i = 0; $i < count($badInsert); ++$i) {
                $xMessage .= $translator->trans('LOTS_NUMBER') . $badInsert[$i] . ', '; // 'lot n° '
            }

            foreach ($badInsertCpvForCategory as $numLot) {
                $xMessage .= $translator->trans('LOTS_NUMBER') . $numLot . ' ('
                    . $translator->trans('CPV_PRINCIPAL_INCOHERENT') . '), ';
            }

            $xMessage = trim($xMessage, ', ');

            $messageProb['n_lots_importes'] = $translator->trans('N_LOTS_IMPORTES', ['%number%' => $dataInsert]);
            $messageProb['lots_suivants_avec_des_erreurs'] = $translator->trans('LOTS_SUIVANTS_AVEC_DES_ERREURS');
            $messageProb['liste_des_lots'] = $xMessage;
            $messageProb['veuillez_verifier_le_format_du_fichier'] = $translator->trans(
                'VEUILLEZ_VERIFIER_LE_FORMAT_DU_FICHIER'
            );
            $messageProb['aucun_numero_lot_en_double'] = $translator->trans('AUCUN_NUMERO_LOT_EN_DOUBLE');
        }
        if (!$serviceLot->canImport($consultation)) {
            $codeProb = 4;
            $messageProb['import_lots_non_autorisee'] = $translator->trans('IMPORT_LOTS_NON_AUTORISEE');
        }
        return $this->render('consultation/Agent/import_lot.html.twig', [
            'form' => $form->createView(),
            'message' => $message,
            'codeProb' => $codeProb,
            'messageProb' => $messageProb,
            'refresh' => $refresh,
            'urlModele' => $urlModele,
            'consultationObj' => $consultationObj,
            'configurationPlateforme' => $configurationPlateforme,
        ]);
    }

    private function isValidCpv($data)
    {
        $categoryUpper = strtoupper($data['category']);
        $prefix = intval(substr($data['code_cpv'], 0, 2));

        $isTravaux = ($this->getParameter('PRESTATION_TRAVAUX') === $categoryUpper && 45 === $prefix);
        $isFournitures = ($this->getParameter('PRESTATION_FOURNITURES') === $categoryUpper
            && ($prefix < 45 || 48 === $prefix));
        $isServices = ($this->getParameter('PRESTATION_SERVICES') === $categoryUpper && $prefix >= 49);

        if ($isTravaux || $isFournitures || $isServices) {
            return true;
        }

        return false;
    }
}
