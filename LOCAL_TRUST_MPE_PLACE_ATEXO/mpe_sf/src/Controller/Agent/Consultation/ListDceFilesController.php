<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent\Consultation;

use App\Handler\ListDceFilesHandler;
use App\Service\Authorization\AuthorizationAgent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

#[Route(
    path: '/agent/document/dce/file-descriptor',
    name: 'agent_document_dce_file_descriptor',
    options: ['expose' => true],
    methods: [Request::METHOD_GET]
)]
#[IsGranted('ROLE_AGENT')]
class ListDceFilesController extends AbstractController
{
    public function __invoke(
        Request $request,
        ListDceFilesHandler $listDceFilesHandler,
        AuthorizationAgent $authorizationAgent
    ): JsonResponse {
        $consultationId = $request->query->get('consultationId');

        if (!empty($consultationId)) {
            if (!$authorizationAgent->isInTheScope((int) $consultationId)) {
                throw new AccessDeniedException('Access Denied.');
            }
        }

        return $listDceFilesHandler->handle(
            utf8_encode(base64_decode($request->query->get('filePath'))),
            (int) $consultationId
        );
    }
}
