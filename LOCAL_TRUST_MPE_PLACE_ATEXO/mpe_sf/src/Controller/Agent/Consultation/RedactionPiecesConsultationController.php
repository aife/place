<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent\Consultation;

use App\Entity\Consultation;
use App\Service\Authorization\AuthorizationAgent;
use App\Service\CurrentUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ConsultationController.*/
#[Route(path: '/agent/consultations/redaction-pieces', name: 'agent_consultations_redaction_pieces_')]
class RedactionPiecesConsultationController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly AuthorizationAgent $authorizationAgent,
        private readonly CurrentUser $currentUser,
        private readonly TranslatorInterface $translator
    ) {
    }
    #[Route(path: '/{id}', methods: ['GET'], name: 'get_by_id')]
    public function getById(Request $request, string $id) : Response
    {
        if (
            !$this->authorizationAgent->isInTheScope((int)$id)
            || !$this->currentUser->checkHabilitation('RedactionDocumentsRedac')
        ) {
            $this->addFlash('error', $this->translator->trans('ACCES_NON_AUTORISE_A_LA_CONSULTATION'));
            return $this->redirectToRoute('atexo_agent_accueil_index');
        }
        $consultation = $this->entityManager->getRepository(Consultation::class)
            ->findOneBy(['id' => $id]);
        return $this->render(
            'consultation/Agent/redaction-pieces.html.twig',
            ['isFullWidth' => true, 'consultationId' => $id, 'reference' => $consultation->getReferenceUtilisateur()]
        );
    }
}
