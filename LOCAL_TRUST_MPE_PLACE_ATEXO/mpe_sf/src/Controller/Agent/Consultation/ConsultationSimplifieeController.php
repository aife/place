<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent\Consultation;

use App\Controller\AtexoController;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\TypeContrat;
use App\Form\Agent\ConsultationSimplifieeType;
use App\Form\Agent\FieldType\ProcedureType;
use App\Service\Agent\ConsultationSimplifieeService;
use App\Service\Consultation\ConsultationService;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Service\Agent\HistorisationConsultationSimplifieeService;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * Class ConsultationSimplifieeController.
 **/
#[Route(path: '/agent/consultation/simplifiee', name: 'atexo_consultation_simplifie_')]
class ConsultationSimplifieeController extends AtexoController
{
    public function __construct(
        private readonly ConsultationSimplifieeService $consultationSimplifieeService,
        private readonly TranslatorInterface $translator,
        private readonly HistorisationConsultationSimplifieeService $historisationConsultationSimplifieeService,
        private readonly ConsultationService $consultationService,
        private readonly SessionInterface $session
    ) {
    }

    /**
     * @Security("is_granted('CS_CREATE')")
     *
     * @throws Exception
     */
    #[Route(path: '/creer', name: 'create')]
    public function create(Request $request): Response
    {
        /** @var Agent $agent */
        $agent = $this->getUser();
        $consultation = $this->consultationSimplifieeService->initConsultation($agent);
        // Lieux d'exécution
        $lieux = $this->consultationSimplifieeService->getSelectedGeoN2($agent);
        $form = $this->createForm(ConsultationSimplifieeType::class, $consultation, [
            'lieux' => $lieux['list'],
            'ids' => $lieux['ids'],
        ]);
        $form->handleRequest($request);
        // Selectionne le type de procédure en cas de reload
        $typeProcedureOrganismeSelected = '';
        $typeProcedureOrganisme = $consultation->getTypeProcedureOrganisme();
        if ($typeProcedureOrganisme) {
            $typeProcedureOrganismeSelected = $typeProcedureOrganisme->concatTypeProcedureOrganisme();
        }
        if ($form->isSubmitted()) {
            $lieuxExecutionIsValid = $this->isLieuxExecutionValid($lieux['ids'], $form);
            if ($form->isValid() && $lieuxExecutionIsValid) {
                $consultation->setLieuExecution($lieux['ids']);
                $consultation = $this->consultationSimplifieeService->setDateFinFromDateAndHour($form, $consultation);
                $consultation->setAgentCreateur($this->getUser());
                $this->consultationSimplifieeService->initDefaultValues($consultation, $agent);
                $this->consultationService->setInvitePonctuelForInitialization($consultation, $agent);
                $consultation->setIdTypeProcedureOrg($consultation->getTypeProcedureOrganisme()->getIdTypeProcedure());
                if (
                    false === $this->consultationSimplifieeService->createDce(
                        $form['dce_filepath']->getData(),
                        $consultation,
                        $agent
                    )
                ) {
                    $this->addFlash('danger', $this->translator->trans('CONSULTATION_FORM_DCE_ERROR'));
                }

                return $this->saveAndRedirect($consultation, $form, $request->getLocale());
            }

            $this->addFlash('danger', $this->translator->trans('CONSULTATION_FORM_ERREUR'));
        }
        return $this->render('consultation/Agent/simplifiee.html.twig', [
            'form' => $form->createView(),
            'typeProcedureOrganismeSelected' => $typeProcedureOrganismeSelected,
            'dce_uploaded' => $this->consultationSimplifieeService->getUploadedDce($form['dce_filepath']->getData()),
        ]);
    }

    /**
     * @Security("is_granted('CS_EDIT', consultation)")
     *
     * @throws Exception
     */
    #[Route(path: '/{id}/modifier', name: 'edit')]
    public function edit(Request $request, Consultation $consultation): Response
    {
        /** @var Agent $agent */
        $agent = $this->getUser();
        $typeProcedureOrganisme = $this->getDoctrine()->getRepository(TypeProcedureOrganisme::class)->findOneBy(
            [
                'idTypeProcedure' => $consultation->getIdTypeProcedureOrg(),
                'organisme' => $consultation->getAcronymeOrg()
            ]
        );
        $consultation->setTypeProcedureOrganisme($typeProcedureOrganisme);
        // Initialise les 2 champs issues de la date de fin
        $consultation->setDatefinDate($consultation->getDatefin())->setDatefinHeure($consultation->getDatefin());
        $form = $this->createForm(ConsultationSimplifieeType::class, $consultation, [
            'lieux' => $this->consultationSimplifieeService->idsToGeoN2($consultation->getLieuExecution()),
            'ids' => $consultation->getLieuExecution(),
        ]);
        $form->handleRequest($request);
        // Selectionne le type de procédure en cas de reload
        $typeProcedureOrganismeSelected = '';
        $typeProcedureOrganisme = $consultation->getTypeProcedureOrganisme();
        if ($typeProcedureOrganisme) {
            $typeProcedureOrganismeSelected = $typeProcedureOrganisme->concatTypeProcedureOrganisme();
        }
        if ($form->isSubmitted()) {
            $lieuxIds = $this->session->get('lieux');
            $lieuxExecutionIsValid = $this->isLieuxExecutionValid($lieuxIds, $form);
            $em = $this->getDoctrine()->getManager();
            if ($form->isValid() && $lieuxExecutionIsValid) {
                $consultation->setLieuExecution($lieuxIds);
                $consultation = $this->consultationSimplifieeService->setDateFinFromDateAndHour($form, $consultation);
                $em->flush();
                if (
                    false === $this->consultationSimplifieeService->createDce(
                        $form['dce_filepath']->getData(),
                        $consultation,
                        $agent
                    )
                ) {
                    $this->addFlash('danger', $this->translator->trans('CONSULTATION_FORM_DCE_ERROR'));
                }

                return $this->saveAndRedirect($consultation, $form, $request->getLocale());
            }

            $em->clear(Consultation::class);
            $this->addFlash('danger', $this->translator->trans('CONSULTATION_FORM_ERREUR'));
        }
        $this->session->set('lieux', $consultation->getLieuExecution());
        return $this->render('consultation/Agent/simplifiee.html.twig', [
            'form' => $form->createView(),
            'typeProcedureOrganismeSelected' => $typeProcedureOrganismeSelected,
            'dce' => $this->consultationSimplifieeService->getDce($consultation),
            'dce_url' => $this->consultationSimplifieeService->getDceDownloadURL($consultation),
            'dce_uploaded' => $this->consultationSimplifieeService->getUploadedDce($form['dce_filepath']->getData()),
            'actionType' => "update",
        ]);
    }

    /**
     * Recharge la liste des type de procédure en AJAX en fonction du type de contrat.
     **/
    #[Route(path: '/updateTypeProcedure', name: 'update_type_procedure', options: ['expose' => true])]
    public function updateTypeProcedure(Request $request): Response
    {
        $typeProcedures = [];
        $disabled = false;

        $typeContratId = $request->request->get('typeContratId');
        if (false === empty($typeContratId)) {
            $em = $this->getDoctrine()->getManager();
            $organisme = $this->getUser()->getOrganisme();
            $typeContrat = $em->getRepository(TypeContrat::class)->findOneBy([
                'idTypeContrat' => $request->request->get('typeContratId'),
            ]);
            $typeProcedures = $em->getRepository(TypeProcedureOrganisme::class)
                ->procedureSimplifieesWithOraganismeAndTypeContrat($organisme, $typeContrat, $this->getUser());
            if ('update' === $request->request->get('actionType')) {
                $disabled = true;
            }
        }
        $typeProcedureOrganismeDiabled = [
            'disabled' => $disabled,
            'choices' => $typeProcedures,
        ];
        $form = $this->createFormBuilder()
            ->add(
                'typeProcedureOrganisme',
                ProcedureType::class,
                $typeProcedureOrganismeDiabled
            )
        ;
        return $this->render('consultation/Agent/AjaxForm/procedure_type.html.twig', [
            'form' => $form->getForm()->createView(),
        ]);
    }

    /**
     * Selectionne le type d'accès en AJAX en fonction du type de procedure.
     **/
    #[Route(path: '/updateTypeAcces', name: 'update_type_acces', options: ['expose' => true])]
    public function updateTypeAcces(Request $request): Response
    {
        $typeAcces = null;
        $ids = explode('_', (string) $request->request->get('typeProcedure'));
        if (count($ids)) {
            $idTypeProcedure = (int) $ids[0];
            $typeAcces = $this->consultationSimplifieeService->getTypeAccesFromTypeProcedure(
                $idTypeProcedure,
                Atexo_CurrentUser::getOrganismAcronym()
            );
        }
        return new JsonResponse($typeAcces);
    }

    private function saveAndRedirect(Consultation $consultation, FormInterface $form, string $locale): Response
    {
        $em = $this->getDoctrine()->getManager();
        $modification = false;
        if ($form->get('saveAndQuitte') === $form->getClickedButton()) {
            $response = $this->redirect(
                "/?page=Agent.TableauDeBord&id={$consultation->getId()}"
                . "&save=true&noCheckedEnv=0&traductionOgl"
            );
        } elseif ($form->get('saveAndValidate') === $form->getClickedButton()) {
            $consultation->setEtatEnAttenteValidation('1')
                ->setNumeroPhase(ConsultationSimplifieeService::PHASE_VALIDATION);
            $em->persist($consultation);
            $em->flush();
            $this->consultationSimplifieeService->sendMailAlert($consultation, $locale);
            $response = $this->redirect(
                "/?page=Agent.TableauDeBord&id={$consultation->getId()}"
                . "&save=true&noCheckedEnv=0&traductionOgl"
            );
        } else {
            $modification = true;
            $this->addFlash('success', $this->translator->trans('CONSULTATION_FORM_OK'));
            $response = $this->redirectToRoute('atexo_consultation_simplifie_edit', [
                'id' => $consultation->getId(),
            ]);
        }
        $this->historisationConsultationSimplifieeService->saveHistory($modification, $consultation, $this->getUser());

        return $response;
    }

    private function isLieuxExecutionValid(?string $ids, FormInterface $form): bool
    {
        $lieuxExecutionIsValid = null !== $ids
            && '' !== $ids
            && 0 < count(array_filter(explode(',', $ids)));
        if (!$lieuxExecutionIsValid) {
            $errorMessage = $this->translator->trans(
                'consultation.simplifiee.form.error.blank',
                [],
                'validators'
            );
            $form->get('lieuxExecutionSelect')->addError(new FormError($errorMessage));
        }
        return $lieuxExecutionIsValid;
    }
}
