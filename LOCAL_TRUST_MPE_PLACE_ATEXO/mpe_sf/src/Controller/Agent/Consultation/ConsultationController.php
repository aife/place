<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent\Consultation;

use App\Entity\Agent;
use App\Entity\Consultation;
use App\Service\AtexoFavoris;
use App\Service\LotCandidature;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ConsultationController.*/
#[Route(path: '/agent/consultations', name: 'agent_consultations_')]
class ConsultationController extends AbstractController
{
    public function __construct(private readonly LotCandidature $lotCandidature)
    {
    }

    #[Route(path: '/{id}/favori', methods: ['PUT'], name: 'update_favori')]
    public function updateFavori(Request $request, AtexoFavoris $atexoFavoris, int $id): Response
    {
        /** @var Agent $agent */
        $agent = $this->getUser();

        //TODO check agent perimetre sur la consultation
        $body = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        if ($body['favori'] === true) {
            $atexoFavoris->addFavoris($id, $agent);
        } else {
            $atexoFavoris->deleteFavoris($id, $agent);
        }

        return $this->json(['success' => 'success'], 200);
    }

    #[Route(path: '/{consultation}/candidature', name: 'lot_candidature')]
    public function getCandidaturesByLot(Consultation $consultation): JsonResponse
    {
        return $this->json(
            $this->lotCandidature->getLotsCandidature($consultation->getId())
        );
    }
}
