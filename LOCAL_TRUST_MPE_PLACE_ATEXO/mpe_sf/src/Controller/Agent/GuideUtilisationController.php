<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GuideUtilisationController extends AbstractController
{
    /**
     * @Route("/agent/aide/guide-utilisation", name="guide_utilisation_")
     *
     * @return Response
     */
    public function indexGuideUtilisationAction()
    {
        return $this->render('agent/aide/guide-utilisation.html.twig', [
            'isFullWidth' => false
        ]);
    }

    /**
     * @Route("/agent/aide/assistance", name="assistance_")
     *
     * @return Response
     */
    public function indexGuideAssistanceAction()
    {
        $info = [];
        $info['outils'] = $this->getParameter('URL_DOCS_OUTILS') . $this->getParameter('DOCS_OUTILS');
        return $this->render('agent/aide/assistance.html.twig', [
            'isFullWidth' => false,
            'info' => $info
        ]);
    }

     /**
     * @Route("/agent/aide/glossaire", name="glossaire_")
     *
     * @return Response
     */
    public function indexGlossaireAction()
    {
        return $this->render('agent/aide/glossaire.html.twig', [
            'isFullWidth' => true
        ]);
    }

    /**
     * @Route("/agent/aide/outils-de-formation", name="outils_formation_")
     *
     * @return Response
     */
    public function indexOutilsFormationAction()
    {
        return $this->render('agent/aide/outils-de-formation.html.twig', [
            'isFullWidth' => false
        ]);
    }

     /**
     * @Route("/agent/aide/outils-de-formation-informatiques", name="outils_informatiques_")
     *
     * @return Response
     */
    public function indexOutilsINformatiquesAction()
    {
        return $this->render('agent/aide/outils-informatiques.html.twig', [
            'isFullWidth' => false
        ]);
    }
}
