<?php

namespace App\Controller\Agent;

use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Enum\Habilitation\HabilitationSlug;
use App\Form\Agent\Habilitation\HabilitationAgentType;
use App\Form\Agent\Habilitation\HabilitationAgentUnifiedType;
use App\Form\Agent\Habilitation\Section\Legacy\HabilitationAdministrationMetierLegacyType;
use App\Form\Agent\Habilitation\Section\Legacy\HabilitationAnnuaireLegacyType;
use App\Form\Agent\Habilitation\Section\Legacy\HabilitationArchivesLegacyType;
use App\Form\Agent\Habilitation\Section\Legacy\HabilitationCreerConsultationLegacyType;
use App\Form\Agent\Habilitation\Section\Legacy\HabilitationDocumentsModelesLegacyType;
use App\Form\Agent\Habilitation\Section\Legacy\HabilitationEspaceCollaboratifLegacyType;
use App\Form\Agent\Habilitation\Section\Legacy\HabilitationGestionAutresAnnoncesLegacyType;
use App\Form\Agent\Habilitation\Section\Legacy\HabilitationGestionConsultationLegacyType;
use App\Form\Agent\Habilitation\Section\Legacy\HabilitationGestionContratLegacyType;
use App\Form\Agent\Habilitation\Section\Legacy\HabilitationGestionOperationsLegacyType;
use App\Form\Agent\Habilitation\Section\Legacy\HabilitationGestionPerimetreLegacyType;
use App\Form\Agent\Habilitation\Section\Legacy\HabilitationNewsletterLegacyType;
use App\Form\Agent\Habilitation\Section\Legacy\HabilitationParametrageServiceLegacyType;
use App\Form\Agent\Habilitation\Section\Legacy\HabilitationRedactionPiecesMarcheLegacyType;
use App\Form\Agent\Habilitation\Section\Legacy\HabilitationResponsableMinisterielAchatLegacyType;
use App\Form\Agent\Habilitation\Section\Legacy\HabilitationSocietesExcluesLegacyType;
use App\Form\Agent\Habilitation\Section\Legacy\HabilitationSpaserLegacyType;
use App\Form\Agent\Habilitation\Section\Legacy\RecensementProgrammationStrategieAchatType;
use App\Form\Agent\Habilitation\Section\New\HabilitationCreerConsultationNewType;
use App\Form\Agent\Habilitation\Section\New\HabilitationCreerSuiteConsultationNewType;
use App\Form\Agent\Habilitation\Section\New\HabilitationGestionPerimetreNewType;
use App\Service\Agent\HabilitationTypeProcedureService;
use App\Entity\Agent\Habilitations\RecensementProgrammationStrategieAchat;
use App\Service\AtexoConfiguration;
use App\Service\InvitePermanentTransverseService;
use App\Service\ServiceService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/agent/habilitations', name: 'agent_habilitation_')]
class HabilitationController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ParameterBagInterface $parameterBag,
        private readonly AtexoConfiguration $atexoConfiguration,
        private HabilitationTypeProcedureService $habilitationTypeProcedureService,
        private readonly ServiceService $serviceService,
        private readonly InvitePermanentTransverseService $invitePermanentTransverseService
    ) {
    }

    private $mode = 0;

    #[Route(path: '/{id}', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, int $id): Response
    {
        $close = false;
        $agent = $this->entityManager->getRepository(Agent::class)->find($id);

        $mode = $request->get('mode', 0);
        $habilitationForm = $this->createForm(
            HabilitationAgentUnifiedType::class,
            null
        );

        $habilitationForm->handleRequest($request);

        if ($habilitationForm->isSubmitted() && $habilitationForm->isValid()) {
            $habilitationAgent = $agent->getHabilitation();
            $this->handleSections($request, $habilitationAgent);
            $recensement = $this->processRecensementLegacy($request);
            $habilitationAgent->setRecensementProgrammationStrategieAchat($recensement);
            $this->processNewHabilitations($request, $habilitationAgent->getAgent());
            $close = 'close' === $request->request->get('close');

            $this->entityManager->flush();
        }

        return $this->render('agent/habilitation/index.html.twig', [
            'agent' => $agent,
            'form' => $habilitationForm->createView(),
            'design' => '',
            'close' => $close,
            'mode' => $mode,
        ]);
    }

    #[Route(path: '/{id}/load-section/{sectionName}', name: 'load_section', methods: ['GET'])]
    public function loadSection(Request $request, int $id, string $sectionName): Response
    {
        $form = null;

        $this->mode = $request->get('mode', 0);
        switch ($sectionName) {
            case 'creerConsultationSection':
                return $this->createCreerConsultationSectionForm($id);
            case 'creerSuiteConsultationSection':
                return $this->createCreerSuiteConsultationSectionForm($id);
            case 'gestionConsultationSection':
                return $this->createGestionConsultationSectionForm($id);
            case 'gestionContratSection':
                return $this->createGestionContratSectionForm($id);
            case 'gestionPerimetreInterventionSection':
                return $this->createGestionPerimetreInterventionSectionForm($id);
            case 'recensementSection':
                return $this->createRecensementSectionForm($id);
            case 'redactionPieceMarcheSection':
                return $this->createRedactionPieceMarcheSectionForm($id);
            case 'annuaireSection':
                return $this->createAnnuaireSectionForm($id);
            case 'gestionAutresAnnoncesSection':
                return $this->createGestionAutresAnnoncesSectionForm($id);
            case 'gestionOperationsSection':
                return $this->createGestionOperationsSectionForm($id);
            case 'administrationMetierSection':
                return $this->createAdministrationMetierSectionForm($id);
            case 'archiveSection':
                return $this->createArchiveSectionForm($id);
            case 'parametrageServiceSection':
                return $this->createParametrageServiceSectionForm($id);
            case 'newsletterSection':
                return $this->createNewsletterSectionForm($id);
            case 'societesExcluesSection':
                return $this->createSocietesExcluesSectionForm($id);
            case 'espaceCollaboratifSection':
                return $this->createEspaceCollaboratifSectionForm($id);
            case 'responsableMinisterielAchatSection':
                return $this->createResponsableMinisterielSectionForm($id);
            case 'spaserSection':
                return $this->createSpaserSectionForm($id);
            case 'documentsModelesSection':
                return $this->createDocumentsModelesSectionForm($id);
            default:
                break;
        }

        return new Response(null, Response::HTTP_BAD_REQUEST);
    }

    protected function createCreerConsultationSectionForm(int $idAgent): Response
    {
        if ($this->parameterBag->get('ACTIVE_HABILITATION_V2')) {
            $agent = $this->entityManager->getRepository(Agent::class)->find($idAgent);
            $form = $this->createForm(
                HabilitationCreerConsultationNewType::class,
                null,
                [
                    'agent' => $agent,
                    'mode' => $this->mode,
                ]
            );

            return $this->render('agent/habilitation/sections/new/_creer-consultation.html.twig', [
                'form' => $form->createView(),
                'sectionName' => 'creerConsultationSection',
                'mode' => $this->mode,
            ]);
        }

        $habilitationAgent = $this->entityManager->getRepository(HabilitationAgent::class)->find($idAgent);
        $form = $this->createForm(HabilitationCreerConsultationLegacyType::class, $habilitationAgent, [
            'mode' => $this->mode
        ]);

        return $this->render('agent/habilitation/sections/legacy/_creer-consultation.html.twig', [
            'form' => $form->createView(),
            'sectionName' => 'creerConsultationSection',
            'mode' => $this->mode,
        ]);
    }

    protected function createGestionConsultationSectionForm(int $idAgent): Response
    {
        $habilitationAgent = $this->entityManager->getRepository(HabilitationAgent::class)->find($idAgent);
        $form = $this->createForm(HabilitationGestionConsultationLegacyType::class, $habilitationAgent, [
            'mode' => $this->mode
        ]);

        return $this->render('agent/habilitation/sections/legacy/_gestion-consultation.html.twig', [
            'form' => $form->createView(),
            'sectionName' => 'gestionConsultationSection',
        ]);
    }

    protected function createGestionContratSectionForm(int $idAgent): Response
    {
        $agent = $this->entityManager->getRepository(Agent::class)->find($idAgent);
        if (!$this->atexoConfiguration->hasConfigOrganisme($agent->getOrganisme(), 'moduleExec')) {
            return new Response('', Response::HTTP_OK);
        }

        $habilitationAgent = $this->entityManager->getRepository(HabilitationAgent::class)->find($idAgent);
        $form = $this->createForm(HabilitationGestionContratLegacyType::class, $habilitationAgent, [
            'mode' => $this->mode
        ]);

        return $this->render('agent/habilitation/sections/legacy/_gestion-contrats.html.twig', [
            'form' => $form->createView(),
            'sectionName' => 'gestionContratSection',
        ]);
    }

    protected function createGestionPerimetreInterventionSectionForm(int $idAgent): Response
    {
        if ($this->parameterBag->get('ACTIVE_HABILITATION_V2')) {
            $agent = $this->entityManager->getRepository(Agent::class)->find($idAgent);

            $services = $this->serviceService->getServiceArborescence($agent);

            $form = $this->createForm(
                HabilitationGestionPerimetreNewType::class,
                null,
                [
                    'agent' => $agent,
                    'mode' => $this->mode,
                ]
            );

            return $this->render('agent/habilitation/sections/new/_gestion-perimetre-intervention.html.twig', [
                'form' => $form->createView(),
                'sectionName' => 'gestionPerimetreInterventionSection',
                'serviceTree' => $services,
                'mode' => $this->mode,
            ]);
        }

        $habilitationAgent = $this->entityManager->getRepository(HabilitationAgent::class)->find($idAgent);
        $form = $this->createForm(HabilitationGestionPerimetreLegacyType::class, $habilitationAgent, [
                'mode' => $this->mode,
            ]);

        return $this->render('agent/habilitation/sections/legacy/_gestion-perimetre-intervention.html.twig', [
            'form' => $form->createView(),
            'sectionName' => 'gestionPerimetreInterventionSection',
            'mode' => $this->mode,
        ]);
    }

    protected function createRedactionPieceMarcheSectionForm(int $idAgent): Response
    {
        $agent = $this->entityManager->getRepository(Agent::class)->find($idAgent);
        if (!$this->atexoConfiguration->hasConfigOrganisme($agent->getOrganisme(), 'interfaceModuleRsem')) {
            return new Response('', Response::HTTP_OK);
        }

        $habilitationAgent = $this->entityManager->getRepository(HabilitationAgent::class)->find($idAgent);
        $form = $this->createForm(HabilitationRedactionPiecesMarcheLegacyType::class, $habilitationAgent, [
            'mode' => $this->mode
        ]);

        return $this->render('agent/habilitation/sections/legacy/_redaction.html.twig', [
            'form' => $form->createView(),
            'sectionName' => 'redactionPieceMarcheSection',
        ]);
    }

    protected function createAnnuaireSectionForm(int $idAgent): Response
    {
        $habilitationAgent = $this->entityManager->getRepository(HabilitationAgent::class)->find($idAgent);
        $form = $this->createForm(HabilitationAnnuaireLegacyType::class, $habilitationAgent, [
            'mode' => $this->mode
        ]);

        return $this->render('agent/habilitation/sections/legacy/_annuaires.html.twig', [
            'form' => $form->createView(),
            'sectionName' => 'annuaireSection',
        ]);
    }

    protected function createGestionAutresAnnoncesSectionForm(int $idAgent): Response
    {
        $habilitationAgent = $this->entityManager->getRepository(HabilitationAgent::class)->find($idAgent);
        $form = $this->createForm(HabilitationGestionAutresAnnoncesLegacyType::class, $habilitationAgent, [
            'mode' => $this->mode
        ]);

        return $this->render('agent/habilitation/sections/legacy/_gestion-autres-annonces.html.twig', [
            'form' => $form->createView(),
            'sectionName' => 'gestionAutresAnnoncesSection',
        ]);
    }

    protected function createGestionOperationsSectionForm(int $idAgent): Response
    {
        $agent = $this->entityManager->getRepository(Agent::class)->find($idAgent);
        if (!$this->atexoConfiguration->hasConfigOrganisme($agent->getOrganisme(), 'gestionOperations')) {
            return new Response('', Response::HTTP_OK);
        }

        $habilitationAgent = $this->entityManager->getRepository(HabilitationAgent::class)->find($idAgent);
        $form = $this->createForm(HabilitationGestionOperationsLegacyType::class, $habilitationAgent, [
            'mode' => $this->mode
        ]);

        return $this->render('agent/habilitation/sections/legacy/_gestion-operations.html.twig', [
            'form' => $form->createView(),
            'sectionName' => 'gestionOperationsSection',
        ]);
    }

    protected function createAdministrationMetierSectionForm(int $idAgent): Response
    {
        $habilitationAgent = $this->entityManager->getRepository(HabilitationAgent::class)->find($idAgent);
        $form = $this->createForm(HabilitationAdministrationMetierLegacyType::class, $habilitationAgent, [
            'mode' => $this->mode
        ]);

        return $this->render('agent/habilitation/sections/legacy/_administration-metier.html.twig', [
            'form' => $form->createView(),
            'sectionName' => 'administrationMetierSection',
        ]);
    }

    protected function createArchiveSectionForm(int $idAgent): Response
    {
        $habilitationAgent = $this->entityManager->getRepository(HabilitationAgent::class)->find($idAgent);
        $form = $this->createForm(HabilitationArchivesLegacyType::class, $habilitationAgent, [
            'mode' => $this->mode
        ]);

        return $this->render('agent/habilitation/sections/legacy/_archive.html.twig', [
            'form' => $form->createView(),
            'sectionName' => 'archiveSection',
        ]);
    }

    protected function createParametrageServiceSectionForm(int $idAgent): Response
    {
        $habilitationAgent = $this->entityManager->getRepository(HabilitationAgent::class)->find($idAgent);
        $form = $this->createForm(HabilitationParametrageServiceLegacyType::class, $habilitationAgent, [
            'mode' => $this->mode
        ]);

        return $this->render('agent/habilitation/sections/legacy/_parametrage-service.html.twig', [
            'form' => $form->createView(),
            'sectionName' => 'parametrageServiceSection',
        ]);
    }

    protected function createNewsletterSectionForm(int $idAgent): Response
    {
        if (!$this->atexoConfiguration->hasConfigPlateforme('gestionNewsletter')) {
            return new Response('', Response::HTTP_OK);
        }

        $habilitationAgent = $this->entityManager->getRepository(HabilitationAgent::class)->find($idAgent);
        $form = $this->createForm(HabilitationNewsletterLegacyType::class, $habilitationAgent, [
            'mode' => $this->mode
        ]);

        return $this->render('agent/habilitation/sections/legacy/_gestion-newsletter.html.twig', [
            'form' => $form->createView(),
            'sectionName' => 'newsletterSection',
        ]);
    }

    protected function createSocietesExcluesSectionForm(int $idAgent): Response
    {
        if (!$this->atexoConfiguration->hasConfigPlateforme('menuAgentSocietesExclues')) {
            return new Response('', Response::HTTP_OK);
        }

        $habilitationAgent = $this->entityManager->getRepository(HabilitationAgent::class)->find($idAgent);
        $form = $this->createForm(HabilitationSocietesExcluesLegacyType::class, $habilitationAgent, [
            'mode' => $this->mode
        ]);

        return $this->render('agent/habilitation/sections/legacy/_societes-exclues.html.twig', [
            'form' => $form->createView(),
            'sectionName' => 'societesExcluesSection',
        ]);
    }

    protected function createEspaceCollaboratifSectionForm(int $idAgent): Response
    {
        $agent = $this->entityManager->getRepository(Agent::class)->find($idAgent);
        if (!$this->atexoConfiguration->hasConfigOrganisme($agent->getOrganisme(), 'espaceCollaboratif')) {
            return new Response('', Response::HTTP_OK);
        }

        $habilitationAgent = $this->entityManager->getRepository(HabilitationAgent::class)->find($idAgent);
        $form = $this->createForm(HabilitationEspaceCollaboratifLegacyType::class, $habilitationAgent, [
            'mode' => $this->mode
        ]);

        return $this->render('agent/habilitation/sections/legacy/_espace-collaboratif.html.twig', [
            'form' => $form->createView(),
            'sectionName' => 'espaceCollaboratifSection',
        ]);
    }

    protected function createResponsableMinisterielSectionForm(int $idAgent): Response
    {
        $agent = $this->entityManager->getRepository(Agent::class)->find($idAgent);
        if (!$this->atexoConfiguration->hasConfigOrganisme($agent->getOrganisme(), 'profilRma')) {
            return new Response('', Response::HTTP_OK);
        }

        $habilitationAgent = $this->entityManager->getRepository(HabilitationAgent::class)->find($idAgent);
        $form = $this->createForm(HabilitationResponsableMinisterielAchatLegacyType::class, $habilitationAgent, [
            'mode' => $this->mode
        ]);

        return $this->render('agent/habilitation/sections/legacy/_responsableMinisterielAchat.html.twig', [
            'form' => $form->createView(),
            'sectionName' => 'responsableMinisterielAchatSection',
        ]);
    }

    protected function createRecensementSectionForm(int $idAgent): Response
    {
        if (!$this->atexoConfiguration->hasConfigurationOrganisme('module_recensement_programmation')) {
            return new Response('', Response::HTTP_OK);
        }
        $habilitationAgent = $this->entityManager->getRepository(HabilitationAgent::class)->find($idAgent);
        $form = $this->createForm(
            RecensementProgrammationStrategieAchatType::class,
            $habilitationAgent->getRecensementProgrammationStrategieAchat(),
            [
            'mode' => $this->mode
            ]
        );

        return $this->render('agent/habilitation/sections/legacy/_recensement.html.twig', [
            'form' => $form->createView(),
            'sectionName' => 'recensementSection',
        ]);
    }

    protected function createSpaserSectionForm(int $idAgent): Response
    {
        $agent = $this->entityManager->getRepository(Agent::class)->find($idAgent);
        if (
            !$this->atexoConfiguration->hasConfigOrganisme(
                $agent->getOrganisme(),
                'accesModuleSpaser',
                'is'
            )
        ) {
            return new Response('', Response::HTTP_OK);
        }

        $habilitationAgent = $this->entityManager->getRepository(HabilitationAgent::class)->find($idAgent);
        $form = $this->createForm(HabilitationSpaserLegacyType::class, $habilitationAgent, [
            'mode' => $this->mode
        ]);

        return $this->render('agent/habilitation/sections/legacy/_spaser.html.twig', [
            'form' => $form->createView(),
            'sectionName' => 'spaserSection',
        ]);
    }

    protected function createDocumentsModelesSectionForm(int $idAgent): Response
    {
        $agent = $this->entityManager->getRepository(Agent::class)->find($idAgent);
        if (
            !$this->atexoConfiguration->hasConfigOrganisme(
                $agent->getOrganisme(),
                'moduleAdministrationDocument',
                'is'
            )
        ) {
            return new Response('', Response::HTTP_OK);
        }

        $habilitationAgent = $this->entityManager->getRepository(HabilitationAgent::class)->find($idAgent);
        $form = $this->createForm(HabilitationDocumentsModelesLegacyType::class, $habilitationAgent, [
            'mode' => $this->mode
        ]);
        $template = 'agent/habilitation/sections/legacy/_documents-modeles.html.twig';


        return $this->render($template, [
            'form' => $form->createView(),
            'sectionName' => 'documentsModelesSection',
        ]);
    }

    private function processNewHabilitations(Request $request, $agent)
    {
        if (!$this->parameterBag->get('ACTIVE_HABILITATION_V2')) {
            return;
        }

        $datas = $request->request->all();

        $this->processNewCreerConsultation($datas, $agent);
        $this->processNewCreerSuiteConsultation($datas, $agent);
        $this->processNewGestionPerimetre($datas, $agent);
    }

    private function processNewCreerConsultation(array $datas, Agent $agent)
    {
        // On clean les habilitation déjà présentes
        foreach (HabilitationSlug::creerConsultationSection() as $slug) {
            $this->habilitationTypeProcedureService->deleteHabilitationsBySlug($agent, $slug);
        }
        $this->entityManager->flush();

        $habilitationDatas = $datas[HabilitationCreerConsultationNewType::BLOCK_NAME];

        // On enregistre les nouvelles habilitations
        foreach ($habilitationDatas as $slug => $data) {
            if (is_array($data)) {
                foreach ($data as $typeProcedureId => $checked) {
                    $this->habilitationTypeProcedureService->createHabilitation(
                        $agent,
                        $slug,
                        $typeProcedureId
                    );
                }
            } else {
                $this->habilitationTypeProcedureService->createHabilitation(
                    $agent,
                    $slug
                );
            }
        }

        if (!empty($habilitationDatas)) {
            $this->habilitationTypeProcedureService->createHabilitation(
                $agent,
                HabilitationSlug::CREER_CONSULTATION()
            );
        }

        $this->entityManager->flush();
    }

    private function processRecensementLegacy($request)
    {
        $requestDatas = $request->request->all();
        $recencementProgrammationStrategieAchat = new RecensementProgrammationStrategieAchat();

        if (!array_key_exists('recensement_programmation_strategie_achat', $requestDatas)) {
            return $recencementProgrammationStrategieAchat;
        }

        $recensementData = $requestDatas['recensement_programmation_strategie_achat'];

        foreach ($recensementData as $key => $value) {
            $setter = 'set' . ucfirst($key);
            if (method_exists($recencementProgrammationStrategieAchat, $setter)) {
                call_user_func([$recencementProgrammationStrategieAchat, $setter], true);
            }
        }

        return $recencementProgrammationStrategieAchat;
    }

    private function handleSections(Request $request, $habilitationAgent)
    {
        $sections = [
            HabilitationGestionConsultationLegacyType::class,
            HabilitationCreerConsultationLegacyType::class,
            HabilitationGestionContratLegacyType::class,
            HabilitationGestionPerimetreLegacyType::class,
            HabilitationRedactionPiecesMarcheLegacyType::class,
            HabilitationAnnuaireLegacyType::class,
            HabilitationGestionAutresAnnoncesLegacyType::class,
            HabilitationGestionOperationsLegacyType::class,
            HabilitationAdministrationMetierLegacyType::class,
            HabilitationArchivesLegacyType::class,
            HabilitationParametrageServiceLegacyType::class,
            HabilitationNewsletterLegacyType::class,
            HabilitationSocietesExcluesLegacyType::class,
            HabilitationEspaceCollaboratifLegacyType::class,
            HabilitationSpaserLegacyType::class,
            HabilitationDocumentsModelesLegacyType::class,
        ];

        foreach ($sections as $section) {
            $habilitationForm = $this->createForm($section, $habilitationAgent);
            if (array_key_exists($habilitationForm->getName(), $request->request->all())) {
                $habilitationForm->submit($request->request->all($habilitationForm->getName()));
            }
        }
    }

    protected function createCreerSuiteConsultationSectionForm(int $idAgent): Response
    {
        if (!$this->parameterBag->get('ACTIVE_HABILITATION_V2')) {
            return new Response('', Response::HTTP_OK);
        }

        $agent = $this->entityManager->getRepository(Agent::class)->find($idAgent);
        $form = $this->createForm(
            HabilitationCreerSuiteConsultationNewType::class,
            null,
            [
                'agent' => $agent,
                'mode' => $this->mode,
            ]
        );

        return $this->render('agent/habilitation/sections/new/_creer-suite-consultation.html.twig', [
            'form' => $form->createView(),
            'mode' => $this->mode,
            'sectionName' => 'creerSuiteConsultationSection',
        ]);
    }

    private function processNewCreerSuiteConsultation(array $datas, Agent $agent)
    {
        // On clean les habilitation déjà présentes
        foreach (HabilitationSlug::creerSuiteConsultationSection() as $slug) {
            $this->habilitationTypeProcedureService->deleteHabilitationsBySlug($agent, $slug);
        }
        $this->entityManager->flush();

        $habilitationDatas = $datas[HabilitationCreerSuiteConsultationNewType::BLOCK_NAME];

        // On enregistre les nouvelles habilitations
        foreach ($habilitationDatas as $slug => $data) {
            if (is_array($data)) {
                foreach ($data as $typeProcedureId => $checked) {
                    $this->habilitationTypeProcedureService->createHabilitation(
                        $agent,
                        $slug,
                        $typeProcedureId
                    );
                }
            } else {
                $this->habilitationTypeProcedureService->createHabilitation(
                    $agent,
                    $slug
                );
            }
        }

        if (!empty($habilitationDatas)) {
            $this->habilitationTypeProcedureService->createHabilitation(
                $agent,
                HabilitationSlug::CREER_SUITE_CONSULTATION()
            );
        }

        $this->entityManager->flush();
    }

    private function processNewGestionPerimetre(array $datas, Agent $agent)
    {
        // On clean les habilitation déjà présentes
        foreach (HabilitationSlug::gestionPerimetreSection() as $slug) {
            $this->habilitationTypeProcedureService->deleteHabilitationsBySlug($agent, $slug);
        }
        $this->invitePermanentTransverseService->removeInvitePermanentTransverseByAgent($agent);
        $this->entityManager->flush();

        $habilitationDatas = $datas[HabilitationGestionPerimetreNewType::BLOCK_NAME];

        // On enregistre les nouvelles habilitations
        foreach ($habilitationDatas as $slug => $data) {
            $this->habilitationTypeProcedureService->createHabilitation(
                $agent,
                $slug
            );

            if ($slug === HabilitationSlug::INVITE_PERMANENT_TRANSVERSE() && !empty($data['services'])) {
                $this->invitePermanentTransverseService->createInvitePermanentTransverse($agent, $data['services']);
                $this->habilitationTypeProcedureService->createHabilitation(
                    $agent,
                    HabilitationSlug::INVITE_PERMANENT_TRANSVERSE()
                );
            }
        }

        $this->entityManager->flush();
    }
}
