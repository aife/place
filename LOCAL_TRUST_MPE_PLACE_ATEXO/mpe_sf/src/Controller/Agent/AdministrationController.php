<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/agent/administration', name: 'agent_administration_')]
class AdministrationController extends AbstractController
{
    /**
     * @return Response
     */
    #[Route(path: '/', name: 'index')]
    public function indexAction()
    {
        $this->denyAccessUnlessGranted('ROLE_AGENT');
        return $this->render('agent/administration/index.html.twig', [
            'isFullWidth' => true
        ]);
    }
}
