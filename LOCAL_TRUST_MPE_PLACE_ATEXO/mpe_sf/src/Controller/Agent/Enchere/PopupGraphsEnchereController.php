<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent\Enchere;

use App\Entity\Agent;
use App\Service\Enchere\EnchereService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PopupGraphsEnchereController.
 **/
#[Route(path: '/agent/enchere', name: 'atexo_enchère_')]
class PopupGraphsEnchereController extends AbstractController
{
    public function __construct(private readonly EnchereService $enchereService)
    {
    }
    /**
     * @throws Exception
     */
    #[Route(path: '/graph-suivi-prix/{idEnchere}', name: 'graph_prix')]
    public function graphSuiviPrix(Request $request) : Response
    {
        $agent = $this->getUser();
        if (!($agent instanceof Agent)) {
            return $this->response->redirect('agent');
        }
        $encheres = $this->enchereService->getData($request->get('idEnchere'));
        return $this->render('enchere/graph.html.twig', [
            'encheres' => $encheres,
            'TYPE' => 'SUIVI_PRIX'
        ]);
    }
    /**
     * @throws Exception
     */
    #[Route(path: '/graph-suivi-note/{idEnchere}', name: 'graph_note')]
    public function graphSuiviNote(Request $request) : Response
    {
        $agent = $this->getUser();
        if (!($agent instanceof Agent)) {
            return $this->response->redirect('agent');
        }
        $encheres = $this->enchereService->getData($request->get('idEnchere'), 'notes');
        return $this->render('enchere/graph.html.twig', [
            'encheres' => $encheres,
            'TYPE' => 'SUIVI_NOTE'
        ]);
    }
}
