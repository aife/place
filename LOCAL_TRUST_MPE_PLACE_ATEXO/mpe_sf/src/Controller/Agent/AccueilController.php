<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use App\Entity\MessageAccueil;
use App\Repository\MessageAccueilRepository;
use App\Service\Agent\AccountsAssocService;
use App\Service\Agent\AccueilService;
use App\Service\AgentService;
use App\Service\AtexoConfiguration;
use App\Service\AtexoMenu;
use App\Service\CurrentUser;
use App\Service\Messagerie\MessagerieService;
use App\Service\Messagerie\WebServicesMessagerie;
use ReflectionException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route(path: '/agent', name: 'atexo_agent_accueil_')]
class AccueilController extends AbstractController
{
    private const TYPE_ASSOCIATION = 2;

    #[Route(path: '/', name: 'index')]
    public function index(ParameterBagInterface $parameterBag, AtexoMenu $atexo, Request $request) : Response
    {
        $session = $request->getSession();

        $longName = $parameterBag->get('PF_LONG_NAME');

        $encoding = mb_detect_encoding($longName, 'utf-8', true);
        if ($encoding === false) {
            $longName = utf8_encode($longName);
        }

        if (!$session->get('account_login_chosen')) {
            if ($atexo->isAuthentificationAgentMultiOrganismes() && count($atexo->getComptesAgentsAssocies()) > 0) {
                $session->set('account_login_chosen', true);
                return $this->redirectToRoute('atexo_agent_accueil_account_choice_login');
            }
        }

        return $this->render('agent/accueil/index.html.twig', [
            'clientName' => $longName,
            'isFullWidth' => true,
        ]);
    }

    public function agentBlock(AtexoConfiguration $atexoConfiguration, AgentService $agentService): Response
    {
        $configurationPlateforme = $atexoConfiguration->getConfigurationPlateforme();

        $labelService = $agentService->getLabelService($this->getUser());

        return $this->render('agent/accueil/_agent.html.twig', [
            'display_comptes_associes' => $configurationPlateforme
                ->getAuthentificationAgentMultiOrganismes() === '0' ? 'd-none' : '',
            'labelService' => $labelService
        ]);
    }

    public function informationsBlock(): Response
    {
        /** @var MessageAccueilRepository $homeMessage */
        $homeMessage = $this->getDoctrine()->getManager()->getRepository(MessageAccueil::class)->findOneBy(
            [
                'destinataire' => 'agent',
                'authentifier' => '1',
            ]
        );

        return $this->render('agent/accueil/_informations.html.twig', [
            'messageAccueil' => $homeMessage && $homeMessage->getContenu() ? $homeMessage : null
        ]);
    }

    /**
     * @param CurrentUser $currentUser
     * @param AccueilService $accueilService
     * @return Response
     * @throws \JsonException
     * @throws ReflectionException
     */
    public function statisticsBlock(CurrentUser $currentUser, AccueilService $accueilService, AtexoConfiguration $atexoConfiguration): Response
    {
        $configurationPlateforme = $atexoConfiguration->getConfigurationPlateforme();
        return $this->render('agent/accueil/_statistics.html.twig', [
            'createConsultationRight' => $accueilService->checkHabilitationForMenu($currentUser),
            'stats' => $accueilService->getStatistics(),
            'has_habilitation_suivi_message' => ($currentUser->checkHabilitation('suivreMessage')
                && true === $configurationPlateforme->isMessagerieV2())
        ]);
    }

    #[Route(path: '/choix-compte', name: 'account_choice_login')]
    public function accountChoiceLogin(AccountsAssocService $accountsAssocService, Request $request) : Response
    {
        $user = $this->getUser();
        $session = $request->getSession();
        $accountsAssoc = $accountsAssocService->listAccountsAssoc($user, self::TYPE_ASSOCIATION);

        if (
            (empty($accountsAssoc) && !$session->get('accounts_agent_assoc'))
        ) {
            return $this->redirectToRoute('atexo_agent_accueil_index');
        }

        if (!$session->get('accounts_agent_assoc')) {
            $session->set('accounts_agent_assoc', $accountsAssoc);
        }

        return $this->render(
            'agent/accueil/accountChoiceLogin.html.twig',
            ['accounts' => $accountsAssoc]
        );
    }

    /**
     * @param MessagerieService $messagerieService
     * @param WebServicesMessagerie $webServicesMessagerie
     * @return Response
     */
    #[Route(path: '/home/messec/dashboard', name: 'messec_dashboard', methods: ['GET'])]
    public function getMessecDashboard(MessagerieService $messagerieService, WebServicesMessagerie $webServicesMessagerie,
        Request $request) : Response
    {
        $params = $messagerieService->initializeContentForSuiviToken();
        $token = $webServicesMessagerie->getContent('initialisationSuiviToken', $params);

        // On libère le fichier de session pour éviter les accès concurrents.
        $request->getSession()->save();

        $response = $webServicesMessagerie->getContent('getSuiviCount', $token);
        $response = json_decode($response, true, 512, JSON_THROW_ON_ERROR);

        return $this->json([
            'nombreMessageNonLu'            => $response['nombreMessageNonLu'],
            'nombreMessageNonDelivre'       => $response['nombreMessageNonDelivre'],
            'nombreMessageEnAttenteReponse' => $response['nombreMessageEnAttenteReponse'],
            'nombreMessages'                => $response['nombreMessages']
        ]);
    }
}
