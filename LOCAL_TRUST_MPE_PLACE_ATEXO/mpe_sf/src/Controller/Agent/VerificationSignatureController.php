<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use App\Form\VerifierSignatureForm;
use App\Service\AtexoConfiguration;
use App\Service\CertificateTransformer;
use Atexo\CryptoBundle\AtexoCrypto;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Cache\CacheItemPoolInterface;
use Twig\Environment;
use AtexoCrypto\Dto\InfosSignature;

class VerificationSignatureController extends AbstractController
{
    public function __construct(private CacheItemPoolInterface $cache, private ParameterBagInterface $parameterBag)
    {
    }

    #[Route("/entreprise/verification-signature", name: "verification_signature_entreprise")]
    #[Route("/agent/verification-signature", name: "verification_signature")]
    public function indexAction(
        Request $request,
        AtexoCrypto $atexoCrypto,
        TranslatorInterface $translator,
        AtexoConfiguration $config
    ): Response {
        $form = $this->createForm(VerifierSignatureForm::class);
        $form->handleRequest($request);
        $verificationResult = null;
        $cacheHash = '';
        $fileName = '';
        $jetonName = '';

        if ($form->isSubmitted() && $form->isValid()) {
            $fileToVerify = $form->get('fileToVerify')->getData();
            if ($fileToVerify) {
                $fileName = $fileToVerify->getClientOriginalName();
                $jetonName = $fileToVerify->getClientOriginalName();
                $fileExtension = $fileToVerify->guessExtension();
                if ($this->parameterBag->get('PDF_EXTENTION') === $fileExtension) {
                    $verificationResult = $atexoCrypto->verifierSignaturePdf(
                        $fileToVerify->getPathName(),
                        $this->parameterBag->get('PADES_SIGNATURE')
                    );
                }
            }
            $signingFile = $form->get('signingFile')->getData();
            if ($this->isValidSigningFile($signingFile, $fileToVerify, $verificationResult)) {
                $jetonName = $signingFile->getClientOriginalName();
                $verificationResult = $atexoCrypto->verifierSignatureHash(
                    sha1_file($fileToVerify->getPathName()),
                    base64_encode(trim(file_get_contents($signingFile->getPathName()))),
                    $this->parameterBag->get('INCONNUE_SIGNATURE'),
                    hash_file('sha256', $fileToVerify->getPathName())
                );
            }
            if (null === $verificationResult) {
                $form->get('fileToVerify')->addError(
                    new FormError($translator->trans('DEFINE_TEXT_FICHIERS_A_VERIFIER_ERREUR_CERTIFICAT'))
                );
            }
            $verificationResult = CertificateTransformer::transform(
                $fileName,
                $jetonName,
                $verificationResult,
                $config->isModuleEnabled('SurchargeReferentiels', null)
            );

            $cacheHash = $this->saveCache($verificationResult, $fileToVerify);
        }

        return $this->render('agent/verification-signature/index.html.twig', [
            'form' => $form->createView(),
            'certificates' =>  $verificationResult,
            'cacheHash' => $cacheHash,
            'isFullWidth' => true
        ]);
    }

    #[Route("/entreprise/verification-signature/download/{hash}", name: "verification_signature_entreprise_download")]
    #[Route("/agent/verification-signature/download/{hash}", name: "verification_signature_download")]
    public function reportVerification(string $hash, Environment $twig, Pdf $pdfGenerator): PdfResponse
    {
        $infosSignature = unserialize($this->cache->getItem($hash)->get());
        $html = $twig->render('pdf/verifySignature.html.twig', ['certificates' => $infosSignature]);

        return new PdfResponse($pdfGenerator->getOutputFromHtml($html), 'Rapport_verification_signature.pdf');
    }

    private function saveCache(?array $verificationResult, mixed $fileToVerify): string
    {
        if (null !== $verificationResult) {
            $cacheHash = hash_file('sha256', $fileToVerify->getPathName()) . uniqid();
            $item = $this->cache->getItem($cacheHash);
            $item->set(serialize($verificationResult));
            $item->expiresAfter(60 * 60 * 2);
            $this->cache->save($item);

            return $cacheHash;
        }

        return '';
    }

    private function isValidSigningFile(
        ?UploadedFile $signingFile,
        ?UploadedFile $fileToVerify,
        array | InfosSignature | null $verificationResult
    ): bool {
        return $signingFile
            && $fileToVerify
            && (
                $this->parameterBag->get('PDF_EXTENTION') !== $fileToVerify->guessExtension()
                || ($this->parameterBag->get(
                    'PDF_EXTENTION'
                ) === $fileToVerify->guessExtension() && $verificationResult === null));
    }
}
