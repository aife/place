<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use App\Service\EspaceDocumentaire\EspaceDocumentaireService;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Exception;
use League\Flysystem\FileExistsException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use League\Flysystem\FileNotFoundException;
use App\Entity\Consultation;
use App\Entity\PieceGenereConsultation;
use App\Form\Offre\ImportAnnexeFinanciereType;
use App\Service\DonneeComplementaire\DonneeComplementaireService;
use App\Service\Offre\AnalyseFinanciereService;
use App\Service\DocGen\AnalyseOffre;
use App\Service\EspaceDocumentaire\Authorization;
use App\Service\EspaceDocumentaire\EspaceDocumentaireUtil;
use App\Utils\Encryption;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGenerator;

#[Route(path: '/agent/analyse-offre', name: 'analyse_offre_')]
class AnalyseOffreController extends AbstractController
{
    final public const MSG_404 = 'Cette ressource n\'existe pas !';
    final public const LIBELLE_CRITERE_ATTRIBUTION_ORDRE_PRIO_DESC = 'LIBELLE_CRITERE_ATTRIBUTION_ORDRE_PRIO_DESC';

    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly EspaceDocumentaireService $espaceDocumentaireService,
    ) {
    }

    /**
     * @param Request $request
     * @param Consultation $consultation
     * @param Authorization $perimetreAgent
     * @param AnalyseOffre $analyseOffre
     * @param EspaceDocumentaireUtil $espaceDocumentaireUtil
     * @param Encryption $encrypte
     * @param EntityManagerInterface $entityManager
     * @param AnalyseFinanciereService $analyseFinanciereService
     * @param DonneeComplementaireService $donneeComplementaireService
     * @return Response
     * @throws Atexo_Consultation_Exception
     * @throws FileExistsException
     */
    #[Route(path: '/{id}', name: 'index')]
    public function index(Request $request, Consultation $consultation, Authorization $perimetreAgent, AnalyseOffre $analyseOffre, EspaceDocumentaireUtil $espaceDocumentaireUtil, Encryption $encrypte, EntityManagerInterface $entityManager, AnalyseFinanciereService $analyseFinanciereService, DonneeComplementaireService $donneeComplementaireService): Response
    {
        $auth = $perimetreAgent->isAuthorized($consultation->getId());
        $path = $this->generateUrl('analyse_offre_open_file', ['id' => $consultation->getId()]);
        $messageEchec = 'TABLEAU_ANALISE_CRITERE_VIDE';

        if (!$auth) {
            throw $this->createNotFoundException(self::MSG_404);
        }

        if (!$analyseOffre->criteresAttributionExists($consultation)) {
            $path = '#';
        }

        $form = $this->createForm(ImportAnnexeFinanciereType::class);

        if (!$consultation->isAlloti()) {
            $form->remove('numeroLot');
        }

        $form->handleRequest($request);

        $forceModal = false;

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $data = $form->getData();
                $annexeFinanciere = $analyseFinanciereService->add($data, $consultation);
                $entityManager->persist($annexeFinanciere);
                $entityManager->flush();

                return $this->redirectToRoute('analyse_offre_index', ['id' => $consultation->getId()]);
            }

            $forceModal = true;
        }

        $criteriaAttributionInfos = $donneeComplementaireService->getCriteriaAttributionInfos($consultation);
        if ($criteriaAttributionInfos['typeCriteriaAttribution']['libelle'] == self::LIBELLE_CRITERE_ATTRIBUTION_ORDRE_PRIO_DESC) {
            $messageEchec = 'TABLEAU_ANALYSE_CRITERE_ORDRE_PRIO_DESC';
        }

        $nbDocs = $espaceDocumentaireUtil->piecesGenereesApartirModele($consultation);


        return $this->render('analyseOffre/index.html.twig', [
            'forceModal' => $forceModal,
            'form' => $form->createView(),
            'consultation' => $consultation,
            'path_opening_analysis_file' => $path,
            'message_echec' => $messageEchec,
            'base_dir' => realpath($this->parameterBag->get('kernel.project_dir') . '/..'),
            'step' => '',
            'encryptedAgentId' => $encrypte->cryptId($this->getUser()->getId()),
            'number_docs' => $nbDocs,
            'annexeFinanciereReponses' => $analyseFinanciereService->getAnnexeFinanciereReponse($consultation),
            'type_criteria_attribution' => $criteriaAttributionInfos['typeCriteriaAttribution']['libelle'],
            'label_criteria_attribution' => $criteriaAttributionInfos['labelCriteriaAttribution'],
            'list_criterias_attribution' => $criteriaAttributionInfos['listCriteriaAttribution'],
            'info_lot_for_front' => $criteriaAttributionInfos['infoLotFront'],
            'identical_bareme' => $criteriaAttributionInfos['identicalBareme'],
            'number_of_files_by_block' => ['pieces_modele' => $nbDocs],
            'lang' => json_encode($this->espaceDocumentaireService->getPieceModeleTranslations()),
        ]);
    }

    /**
     * @param Consultation $consultation
     * @param Authorization $perimetreAgent
     * @param AnalyseOffre $analyseOffre
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @return Response
     * @throws Atexo_Consultation_Exception
     * @throws ClientExceptionInterface
     * @throws FileExistsException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    #[Route(path: '/ouverture-fichier/{id}', name: 'open_file', methods: ['GET'])]
    public function openFile(Consultation $consultation, Authorization $perimetreAgent, AnalyseOffre $analyseOffre, Request $request, ParameterBagInterface $parameterBag): Response
    {
        $auth = $perimetreAgent->isAuthorized($consultation->getId());
        if (!$auth) {
            throw $this->createNotFoundException(self::MSG_404);
        }

        $url = $parameterBag->get('GENDOC_BASE_URL') . '/edition-en-ligne/';
        $organisme = $this->getUser()->getOrganisme();
        $pieceGeneree = $analyseOffre->getPieceGeneree($consultation, $organisme);

        $token = $analyseOffre->getToken(
            $pieceGeneree,
            $this->getUser(),
            $organisme,
            $this->generateUrl('analyse_offre_edit_file', ['id' => $pieceGeneree->getId()], UrlGenerator::ABSOLUTE_URL)
        );

        $url = $url . '?token=' . $token;

        return $this->redirect($url);
    }

    /**
     * @throws FileExistsException
     * @throws FileNotFoundException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    #[Route(path: '/document-update/{id}', name: 'edit_file', methods: ['POST'])]
    public function edit(PieceGenereConsultation $piece, AnalyseOffre $analyseOffre): Response
    {
        $analyseOffre->getEditedDocument($piece, $piece->getBlobId()->getOrganisme());

        return new Response('OK');
    }
    #[Route(path: '/critere-attribution/definir', name: 'define_criteria_attribution', methods: ['POST', 'GET'], options: ['expose' => true])]
    public function ajaxDefineCriteriaAttribution(
        Request $request,
        DonneeComplementaireService $donneeComplementaireService
    ): JsonResponse {
        $data = $request->request->get('dataAjax');
        try {
            $donneeComplementaireService->saveCriteriaAttribution($data);
            return new JsonResponse(['message' => 'Criteria attribution created'], Response::HTTP_CREATED);
        } catch (NotFoundHttpException $e) {
            return new JsonResponse(['message' => 'Error : ' . $e->getMessage()], $e->getCode());
        }
    }
}
