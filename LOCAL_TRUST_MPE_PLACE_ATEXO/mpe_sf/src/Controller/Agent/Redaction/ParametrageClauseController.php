<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent\Redaction;

use App\Service\WebServicesRedaction;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/agent', name: 'parametrage_redac_clauses_')]
class ParametrageClauseController extends AbstractController
{
    public function __construct(
        private WebServicesRedaction $webServicesRedaction
    ) {
    }

    #[Route('/redaction/parametrage/clauses', name: 'index')]
    public function index(): Response
    {
        $auth = $this->webServicesRedaction->getAuthResponse($this->getUser());
        $identifiant = $this->webServicesRedaction->initialiseContext($this->getUser(), $auth['token']);

        return $this->render('agent/accueil/parametrageRedactionClauses.html.twig', [
            'isFullWidth'       => true,
            'identifiant'       => $identifiant['identifiant']
        ]);
    }
}
