<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent\Redaction;

use App\Service\WebServicesRedaction;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/agent/redaction/auth', name: 'atexo_agent_redaction_auth', methods: [Request::METHOD_GET])]
#[IsGranted('ROLE_AGENT')]
class AuthController extends AbstractController
{
    public function __invoke(WebServicesRedaction $webServicesRedaction): JsonResponse
    {
        try {
            return new JsonResponse($webServicesRedaction->getAuthResponse($this->getUser()));
        } catch (\Exception $exception) {
            return new JsonResponse(['error' => $exception->getMessage()], 400);
        }
    }
}
