<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent\Redaction;

use App\Repository\ConsultationRepository;
use App\Service\WebServicesRedaction;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use TusPhp\Tus\Server;

#[Route(path: '/agent/redaction/download', name: 'atexo_agent_redaction_download', methods: [Request::METHOD_POST])]
#[IsGranted('ROLE_AGENT')]
class DownloadController extends AbstractController
{
    public function __invoke(
        Request $request,
        WebServicesRedaction $webServicesRedaction,
        ConsultationRepository $consultationRepository,
        Server $tusServer
    ): JsonResponse {
        try {
            $redacAuthToken = $request->request->get('redacAuthToken');
            $fileId = $request->request->get('fileId');
            $consultationOrganism = $request->request->get('consultationOrganism');
            $consultationId = $request->request->get('consultationId');
            $fileName = $request->request->get('fileName');

            if (!$consultationId || !$consultationOrganism || !$redacAuthToken || !$fileId || !$fileName) {
                return new JsonResponse(['error' => 'Missing route parameters'], Response::HTTP_BAD_REQUEST);
            }

            $consultation = $consultationRepository->find($consultationId);

            $blobFile = $webServicesRedaction->downloadRedacDocument(
                $fileId,
                $consultation->getDatefin(),
                $consultationOrganism,
                $redacAuthToken
            );

            $targetDirectory = sprintf(
                '%s/%s',
                rtrim($tusServer->getUploadDir(), '/'),
                Uuid::uuid4()->toString(),
            );

            if (!file_exists($targetDirectory)) {
                mkdir($targetDirectory, 0777, true);
            }

            $pathTmp = sprintf('%s/%s', $targetDirectory, $fileName);

            if (!file_put_contents($pathTmp, $blobFile)) {
                throw new \Exception('Erreur lors de l\'enregistrement du fichier depuis Redac');
            };

            return new JsonResponse(['filepath' => sprintf('%s/%s', basename(dirname($pathTmp)), $fileName)]);
        } catch (\Exception $exception) {
            return new JsonResponse(['error' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
