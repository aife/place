<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent\Redaction;

use App\Service\WebServicesRedaction;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/agent/redaction/list-files', name: 'atexo_agent_redaction_list_files', methods: [Request::METHOD_POST])]
#[IsGranted('ROLE_AGENT')]
class ListFilesController extends AbstractController
{
    public function __invoke(Request $request, WebServicesRedaction $webServicesRedaction): JsonResponse
    {
        try {
            $consultationId = $request->request->get('consultationId');
            $consultationOrganism = $request->request->get('consultationOrganism');
            $redacAuthToken = $request->request->get('redacAuthToken');

            if (!$consultationId || !$consultationOrganism || !$redacAuthToken) {
                return new JsonResponse(['error' => 'Missing route parameters'], Response::HTTP_BAD_REQUEST);
            }

            return new JsonResponse($webServicesRedaction->getListeDocument(
                $consultationId,
                $consultationOrganism,
                $redacAuthToken
            ));
        } catch (\Exception $exception) {
            return new JsonResponse(['error' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
