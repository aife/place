<?php

namespace App\Controller\Agent;

use Exception;
use App\Service\Agent\StatisticsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class StatisticsController, permet d'intéragir avec l'API de statistiques BI
 **/
#[Route(path: '/agent/statistiques', name: 'statistics_')]
class StatisticsController extends AbstractController
{
    /**
     * @throws Exception*/
    #[Route(path: '/reports', name: 'reports', methods: ['GET'])]
    public function getReports(StatisticsService $statisticsService) : JsonResponse
    {
        $currentUser = $this->getUser();
        $reports = $statisticsService->getContent('getReports', $currentUser);

        $hasStatistics = !empty($reports);
        $error = null;
        if (!empty($reports['error'])) {
            $hasStatistics = false;
            $error = $reports['error'];
        }

        return $this->json(['hasStatistics' => $hasStatistics, 'reports' => $reports, 'error' => $error]);
    }

    /**
     * @throws Exception*/
    #[Route(path: '/reports/{id}', name: 'reports_id')]
    public function getReportById(Request $request, StatisticsService $statisticsService, string $id) : Response
    {
        $currentUser = $this->getUser();

        $apiUrl = $statisticsService->getContent('getApiUrl');
        $context = $statisticsService->getContent('createContext', $currentUser);
        $accessToken = $statisticsService->getContent('getAgentAccessToken');
        $reports = $statisticsService->getContent('getReports', $currentUser);
        $reports = array_column($reports, 'name', 'id');

        return $this->render(
            'ecrans-agent/statistics.html.twig',
            [
                'reportId'      => $id,
                'reportName'    => $reports[$id] ?? '',
                'apiUrl'        => $apiUrl,
                'context'       => $context,
                'accessToken'   => $accessToken,
                'isFullWidth'   => true
            ]
        );
    }
}
