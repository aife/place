<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use App\Model\ConsultationForm;
use App\Model\ConsultationInput;
use App\Model\ConsultationModule;
use App\Repository\Procedure\ProcedureEquivalenceRepository;
use App\Service\Procedure\ProcedureRuleInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;

class ParametersProceduresController extends AbstractController
{
    public function __construct(
        private ProcedureEquivalenceRepository $equivalenceRepository,
        #[TaggedIterator(ProcedureRuleInterface::class)] private iterable $rulesServices
    ) {
    }

    public function __invoke(string $id)
    {
        $organism = $this->getUser()?->getOrganisme();

        $procedureEquivalence = $this->equivalenceRepository->findOneBy([
            'idTypeProcedure' => $id,
            'organisme' => $organism?->getAcronyme()
        ]);

        $form = new ConsultationForm();

        foreach ($this->rulesServices as $ruleService) {
            $form = $ruleService->apply($form, $procedureEquivalence, $organism);
        }

        return $form;
    }
}
