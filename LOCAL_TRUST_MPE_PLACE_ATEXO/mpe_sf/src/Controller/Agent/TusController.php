<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Agent;

use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use TusPhp\Tus\Server as TusServer;

class TusController extends AbstractController
{
    #[Route(path: '/agent/dce/tus', name: 'agent_dce_tus_upload', methods: ['POST'], options: ['expose' => true])]
    #[Route(path: '/agent/dce/tus/{token?}', name: 'tus_token', methods: ['GET', 'POST', 'HEAD', 'PATCH', 'DELETE'], requirements: ['token' => '.+'])]
    public function dceUploadServer(TusServer $server, LoggerInterface $logger, TranslatorInterface $translator) : Response
    {
        $this->denyAccessUnlessGranted('ROLE_AGENT');
        try {
            $uploadKey = $server->getUploadKey();
            $uploadDir = $server->getUploadDir();

            $targetDir = rtrim($uploadDir, '/').'/'.$uploadKey;
            if (!file_exists($targetDir)) {
                mkdir($targetDir, 0777, true);
            }
            $server->setUploadDir($targetDir);
            $server->setApiPath($this->generateUrl('agent_dce_tus_upload'));

            return $server->serve();
        } catch (Exception $e) {
            $logger->error($e->getMessage());

            return new Response($translator->trans('CONSULTATION_FORM_DCE_ERROR'), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
