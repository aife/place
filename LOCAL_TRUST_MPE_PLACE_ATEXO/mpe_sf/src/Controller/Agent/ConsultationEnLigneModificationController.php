<?php

namespace App\Controller\Agent;

use App\Entity\Consultation;
use App\Utils\Encryption;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConsultationEnLigneModificationController extends AbstractController
{
    #[Route(
        path: '/agent/consultation/modification-en-ligne/{uuid}',
        name: 'agent_consultation_modification_en_ligne',
        options: ['expose' => true],
        methods: ['GET']
    )]
    #[Security("is_granted('ONLINE_CONSULTATION_UPDATE', consultation)")]
    public function modification(Consultation $consultation, Encryption $encryption): Response
    {
        return $this->render('agent/consultation-ligne-modification/index.html.twig', [
            'isFullWidth'               => true,
            'consultationId'            => $consultation->getId(),
            'encryptedConsultationId'   => $encryption->cryptId($consultation->getId())
        ]);
    }
}
