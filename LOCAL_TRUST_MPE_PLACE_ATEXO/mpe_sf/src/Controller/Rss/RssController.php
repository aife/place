<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Rss;

use App\Repository\FluxRssRepository;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class RssController
 *
 * @package App\Controller\Rss
 */
class RssController extends AbstractController
{
    public function __construct(
        private readonly FluxRssRepository $fluxRssRepository
    ) {
    }

    /**
     * @param ParameterBagInterface $parameterBag
     * @param string                $fileName
     * @return BinaryFileResponse
     * @throws Exception
     */
    #[Route(path: '/agent/rss-download/{fileName}', methods: ['GET'], name: 'rss_download_file')]
    #[Route(path: '/{fileName}', name: 'rss')]
    public function download(ParameterBagInterface $parameterBag, string $fileName)
    {
        $fluxRss = $this->fluxRssRepository->findBy(['nomFichier' => $fileName]);

        if (empty($fluxRss)) {
            throw new Exception('Le fichier ' . $fileName . ' n\'existe pas');
        }

        $pathFile = $parameterBag->get('DIR_FLUX_RSS') . $fileName ;
        $filesystem = new Filesystem();

        if (!$filesystem->exists($pathFile)) {
            throw new Exception('Le fichier ' . $fileName . ' n\'existe pas');
        }

        return $this->file($pathFile);
    }
}
