<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\ApiPlatformCustom;

use App\Entity\Consultation;
use App\Factory\Document\DocumentFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

final class CreateAutrePiece extends AbstractController
{
    public function __construct(private readonly DocumentFactory $documentFactory)
    {
    }

    /**
     * @throws \Exception
     */
    public function __invoke(Request $request, Consultation $consultation): void
    {
        if (!$request->request->has('path')) {
            throw new \InvalidArgumentException('path field is required');
        }
        $this->documentFactory->createOrUpdateDocumentByType(
            $consultation,
            $this->getUser(),
            $request->toArray(),
            DocumentFactory::DOCUMENT_TYPE_AUTRE_PIECE
        );
    }
}
