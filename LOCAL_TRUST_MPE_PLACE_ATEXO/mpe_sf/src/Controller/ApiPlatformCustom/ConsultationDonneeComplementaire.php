<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\ApiPlatformCustom;

use App\Entity\ReferentielConsultation;
use App\Entity\Tranche;
use App\Repository\ReferentielConsultationRepository;
use App\Service\ConsultationReferentielService;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Consultation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ConsultationDonneeComplementaire extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ReferentielConsultationRepository $referentielConsultationRepository,
        private readonly ConsultationReferentielService $referentielService,
    ) {
    }

    public function __invoke(Consultation $consultation)
    {
        $tranche = $this->entityManager->getRepository(Tranche::class)->findBy([
            'idDonneeComplementaire' => $consultation?->getDonneeComplementaire()?->getIdDonneeComplementaire()
        ]);

        if (!($donneeComplementaire = $consultation->getDonneeComplementaire())) {
            return null;
        }

        $donneeComplementaire->setAvecTranche(!empty($tranche));

        $referentiels = $this->referentielConsultationRepository->findBy(
            [
                'consultation' => $consultation->getId(),
                'lot' => ReferentielConsultation::REFERENTIEL_CONSULTATION_ONLY,
            ]
        );

        if ($referentiels) {
            $donneeComplementaire = $this->referentielService->getReferentielValuesSettedInDonneeComplementaire(
                $referentiels,
                $donneeComplementaire
            );
        }

        return $donneeComplementaire;
    }
}
