<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\ApiPlatformCustom;

use App\Entity\Lot;
use App\Entity\Tranche;
use App\Repository\ReferentielConsultationRepository;
use App\Service\ConsultationReferentielService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LotDonneeComplementaire extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ReferentielConsultationRepository $referentielConsultationRepository,
        private readonly ConsultationReferentielService $referentielService,
    ) {
    }

    public function __invoke(Lot $lot)
    {
        $tranche = $this->entityManager->getRepository(Tranche::class)->findBy([
            'idDonneeComplementaire' => $lot?->getDonneeComplementaire()?->getIdDonneeComplementaire()
        ]);

        $donneeComplementaire = $lot->getDonneeComplementaire();
        if (empty($donneeComplementaire)) {
            return null;
        }
        $donneeComplementaire->setAvecTranche(!empty($tranche));

        $referentiels = $this->referentielConsultationRepository->findBy(
            [
                'consultation' => $lot->getConsultation()?->getId(),
                'lot' => $lot->getLot(),
            ]
        );
        if ($referentiels) {
            $donneeComplementaire = $this->referentielService->getReferentielValuesSettedInDonneeComplementaire(
                $referentiels,
                $donneeComplementaire,
                $lot->getLot()
            );
        }

        return $donneeComplementaire;
    }
}
