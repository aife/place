<?php

namespace App\Controller\ApiPlatformCustom;

use App\Entity\Consultation;
use App\Repository\CertificatChiffrementRepository;
use App\Service\ConsultationCertificats\ConsultationCertificatsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ConsultationCertificats extends AbstractController
{
    public function __construct(
        protected readonly ConsultationCertificatsService $consultationCertificatsService
    ) {
    }

    /**
     * @param Consultation $consultation
     * @return array
     */
    public function __invoke(Consultation $consultation): array
    {
        return $this->consultationCertificatsService->getList($consultation);
    }
}
