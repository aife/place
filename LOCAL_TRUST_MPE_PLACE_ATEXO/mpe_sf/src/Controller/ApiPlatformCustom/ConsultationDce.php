<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\ApiPlatformCustom;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\Consultation;
use App\Entity\MediaUuid;
use App\Repository\BloborganismeFileRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ConsultationDce extends AbstractController
{
    public function __construct(
        private readonly BloborganismeFileRepository $blobRepository,
        private readonly IriConverterInterface $iriConverter,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function __invoke(Consultation $consultation): array
    {
        $listDce = $this->blobRepository->getDceByConsultation($consultation->getId());
        $listRc = $this->blobRepository->getRcByConsultation($consultation->getId());
        $listDume = $this->blobRepository->getDumeByConsultation($consultation->getId());
        $listAutrePiece = $this->blobRepository->getAutrePieceByConsultation($consultation->getId());
        $blobList = array_merge($listDce, $listRc, $listDume, $listAutrePiece);

        $data = [];
        foreach ($blobList as $key => $blob) {
            $media = new MediaUuid();
            $media->setMedia($blob['blob']);
            $media->setCreatedAt(new \DateTime());
            $this->entityManager->persist($media);
            $data[$key]['id'] = $this->iriConverter->getIriFromItem($media);
            $data[$key]['name'] = $blob['blob']->getName();
            $data[$key]['fileName'] = $blob['nomFichier'] ?? '';
            $data[$key]['type'] = $blob['type'];
            $data[$key]['dateCreation'] = $blob['dateCreation'];
            $data[$key]['dateModification'] = $blob['dateModification'];
            $data[$key]['hash'] = $blob['blob']->getHash();
            $data[$key]['taille'] = $blob['taille'];
            $data[$key]['technicalId'] = $blob['technicalId'];
        }

        $this->entityManager->flush();

        return $data;
    }
}
