<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\ApiPlatformCustom;

use App\Entity\Entreprise;
use App\Service\Attestation\AttestationCoffreFortEntreprise;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AttestationCoffreFort extends AbstractController
{
    public function __construct(
        private readonly AttestationCoffreFortEntreprise $attestationCoffreFortEntreprise
    ) {
    }

    public function __invoke(Entreprise $entreprise): array
    {
        return $this->attestationCoffreFortEntreprise->getAttestations($entreprise);
    }
}
