<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\ApiPlatformCustom;

use App\Repository\LotRepository;
use App\Repository\ReferentielConsultationRepository;
use App\Service\ConsultationReferentielService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Consultation;

class ConsultationReferentielController extends AbstractController
{
    public function __construct(
        private readonly ReferentielConsultationRepository $referentielConsultationRepository,
        private readonly ConsultationReferentielService $referentielService,
    ) {
    }

    public function __invoke(Consultation $consultation): array
    {
        $referentiels = $this->referentielConsultationRepository->findByConsultation($consultation->getId());

        return $this->referentielService->getListReferentielsConsultation($referentiels, $consultation);
    }
}
