<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\ApiPlatformCustom;

use App\Entity\Entreprise;
use App\Service\Attestation\AttestationApiEntreprise;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AttestationFiscales extends AbstractController
{
    public function __construct(
        private readonly AttestationApiEntreprise $attestationApiEntreprise
    ) {
    }

    public function __invoke(Entreprise $data)
    {
        return $this->attestationApiEntreprise->getAttestations($data);
    }
}
