<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\ApiPlatformCustom;

use App\Entity\BlobFichier;
use App\Service\Blob\BlobFileService;
use League\Flysystem\FileNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class DownloadBlobFile extends AbstractController
{
    private const REAL_PATH = 'realPath';

    public function __invoke(BlobFichier $blob, BlobFileService $blobFileService): BinaryFileResponse
    {
        $data = $blobFileService->getNameAndRealPath($blob);

        if (!file_exists($data[self::REAL_PATH])) {
            throw new FileNotFoundException('Le fichier n\'existe pas sur le serveur');
        }

        $response = new BinaryFileResponse($data[self::REAL_PATH]);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $data['name']
        );

        $response->deleteFileAfterSend(false);

        return $response;
    }
}
