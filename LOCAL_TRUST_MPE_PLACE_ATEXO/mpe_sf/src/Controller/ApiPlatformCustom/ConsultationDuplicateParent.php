<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\ApiPlatformCustom;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\Consultation;
use App\Service\Consultation\DuplicationConsultation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ConsultationDuplicateParent extends AbstractController
{
    public function __construct(
        private readonly IriConverterInterface $iriConverter,
        private readonly DuplicationConsultation $duplicationConsultation,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function __invoke(Request $request, Consultation $consultation): Response
    {
        $consultationParent = $consultation->getReferenceConsultationParent();
        if (empty($consultationParent)) {
            return new Response('No consultation parent', Response::HTTP_BAD_REQUEST);
        }

        if (!empty($consultationParent->getDonneeComplementaire())) {
            $donneeComplementaireCopyIri = $this->duplicationConsultation->duplicateDonneeComplementaire(
                $consultationParent->getDonneeComplementaire()
            );
            /** @var Consultation\DonneeComplementaire $donneeComplementaireCopy */
            $donneeComplementaireCopy = $this->iriConverter->getItemFromIri($donneeComplementaireCopyIri);
            if (
                !empty($consultation->getDonneeComplementaire())
                && !empty($consultation->getDonneeComplementaire()->getMontantMarche())
            ) {
                $donneeComplementaireCopy
                    ->setMontantMarche($consultation->getDonneeComplementaire()->getMontantMarche());
            }
            $consultation->setDonneeComplementaire($donneeComplementaireCopy);
            $this->entityManager->flush();
        }
        if ($consultation->isAlloti()) {
            $selectedLots = $request->request->get('lots');
            $parentLots = $consultationParent->getLots();
            $lotsToDuplicate = new ArrayCollection();
            foreach ($parentLots as $parentLot) {
                if (in_array($parentLot->getId(), (array)$selectedLots)) {
                    $lotsToDuplicate[] = $parentLot;
                }
            }

            if (!empty($lotsToDuplicate)) {
                $this->duplicationConsultation->duplicateLots(
                    $lotsToDuplicate,
                    $this->iriConverter->getIriFromItem($consultation),
                    $request
                );
            }
        }

        $response = new Response();
        $response->setStatusCode(Response::HTTP_NO_CONTENT);

        return $response;
    }
}
