<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\ApiPlatformCustom;

use App\Entity\WS\AgentTechniqueToken;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ValidationTicketController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        private JWTTokenManagerInterface $tokenManager
    ) {
    }

    public function __invoke(string $ticket)
    {

        $userToken = $this->em->getRepository(AgentTechniqueToken::class)->checkToken($ticket);
        if ($userToken) {
            $token = $this->tokenManager->create($userToken->getAgent());

            return  $token;
        }

        return null;
    }
}
