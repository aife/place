<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\ApiPlatformCustom;

use App\Entity\Consultation;
use App\Entity\LtReferentiel;
use App\Entity\ReferentielConsultation;
use App\Repository\LtReferentielRepository;
use App\Repository\ReferentielConsultationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class CreateConsultationReferentielController extends AbstractController
{
    public function __construct(
        private ReferentielConsultationRepository $referentielConsultationRepository,
        private LtReferentielRepository $ltReferentielRepository
    ) {
    }

    public function __invoke(Consultation $consultation, Request $request): array
    {
        $referentiels = $request->request->all();
        $list = [];
        foreach ($referentiels as $key => $referentiel) {
            $ltReferentiel = $this->ltReferentielRepository->findOneBy(['codeLibelle' => $key]);
            if ($ltReferentiel instanceof LtReferentiel) {
                $referentielConsultation = new ReferentielConsultation();
                $referentielConsultation->setConsultation($consultation)
                    ->setLtReferentiel($ltReferentiel)
                    ->setValeurPrincipaleLtReferentiel($referentiel['valeurPrincipale'])
                    ->setValeurSecondaireLtReferentiel($referentiel['valeurSecondaire'] ?? '')
                    ->setOrganisme($this->getUser()?->getOrganisme()?->getAcronyme());
                $this->referentielConsultationRepository->add($referentielConsultation);
                $list['list'][$key] = $referentiel;
            }
        }

        return $list;
    }
}
