<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\ApiPlatformCustom;

use App\Entity\Consultation;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Service\Agent\ConsultationSimplifieeService;
use App\Service\Consultation\ConsultationValidationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ConsultationAttenteValidation extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ConsultationSimplifieeService $consultationSimplifieeService,
        private readonly ConsultationValidationService $consultationValidationService
    ) {
    }

    public function __invoke(Request $request, Consultation $consultation): Response
    {
        $errors = $this->consultationValidationService->consultationValidate($consultation);

        if (!empty($errors)) {
            return new JsonResponse($errors, Response::HTTP_BAD_REQUEST);
        }

        $consultation->setEtatEnAttenteValidation('1')
            ->setNumeroPhase(ConsultationSimplifieeService::PHASE_VALIDATION);
        $this->entityManager->flush();

        $typeProcedureOrganisme = $this->entityManager->getRepository(TypeProcedureOrganisme::class)->findOneBy(
            [
                'idTypeProcedure' => $consultation->getIdTypeProcedureOrg(),
            ]
        );
        $consultation->setTypeProcedureOrganisme($typeProcedureOrganisme);

        $this->consultationSimplifieeService->sendMailAlert($consultation, $request->getLocale());
        
        $response = new Response();
        $response->setStatusCode(Response::HTTP_NO_CONTENT);

        return $response;
    }
}
