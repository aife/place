<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\ApiPlatformCustom;

use App\Entity\Consultation;
use App\Event\ModificationDceEvent;
use App\Factory\Document\DocumentFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Event\PublicationConsultationEvent;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

final class CreateOrUpdateDceDocument extends AbstractController
{
    public function __construct(
        private readonly DocumentFactory $documentFactory,
        private readonly EventDispatcherInterface $dispatcher,
    ) {
    }

    /**
     * @throws \Exception
     */
    public function __invoke(Request $request, Consultation $consultation): void
    {
        $this->documentFactory->createOrUpdateDocumentByType(
            $consultation,
            $this->getUser(),
            $request->toArray(),
            DocumentFactory::DOCUMENT_TYPE_DCE
        );
        if ($consultation->getDateMiseEnLigneCalcule()) {
            $event = new ModificationDceEvent($consultation->getId(), (int)$consultation->getCodeExterne());
            $this->dispatcher->dispatch($event, ModificationDceEvent::class);
        }
    }
}
