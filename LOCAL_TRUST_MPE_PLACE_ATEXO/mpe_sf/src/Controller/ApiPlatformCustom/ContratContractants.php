<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\ApiPlatformCustom;

use App\Entity\ContratTitulaire;
use App\Entity\Etablissement;
use App\Repository\ContratTitulaireRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class ContratContractants
{
    public function __construct(
        private ContratTitulaireRepository $contratTitulaireRepository,
        private EntityManagerInterface $entityManager
    ) {
    }

    public function __invoke(ContratTitulaire $contratTitulaire): array
    {
        $list = [];
        if ($contratTitulaire->getIdTitulaireEtab()) {
            $etablisment = $this->entityManager->getRepository(Etablissement::class)
                ->find($contratTitulaire->getIdTitulaireEtab());
            $entreprise = $etablisment?->getEntreprise();
            $list[] = $entreprise?->getNom() . ' (' . $etablisment?->getCodePostal()
                . ' - ' . $etablisment->getVille() . ')';
        } else {
            $contratTitulaires = $this->contratTitulaireRepository->findBy(
                ['idContratMulti' => $contratTitulaire->getIdContratTitulaire()]
            );

            foreach ($contratTitulaires as $contratTitulaire) {
                $etablisment = $this->entityManager->getRepository(Etablissement::class)
                    ->find($contratTitulaire->getIdTitulaireEtab());
                $entreprise = $etablisment?->getEntreprise();
                $list[] = $entreprise?->getNom() . ' (' . $etablisment?->getCodePostal()
                    . ' - ' . $etablisment->getVille() . ')';
            }
        }

        return $list;
    }
}
