<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\ApiPlatformCustom;

use App\Entity\Agent;
use App\Service\Perimeter\PerimetreVisionService;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class PerimeterVisionController
{
    public function __construct(
        private readonly PerimetreVisionService $perimetreVisionService,
    ) {
    }

    public function __invoke(Agent $agent): array
    {
        return $this->perimetreVisionService->getServicesInPerimeterVision($agent);
    }
}
