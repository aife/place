<?php

/*
 * Controller TokenController : classe de gestion du token : generation et acces a l'application via le token
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @since ESR2016
 * @copyright Atexo 2016
 */

namespace App\Controller;

use App\Entity\AccessToken;
use App\Entity\Agent;
use App\Entity\Inscrit;
use App\Service\Authentication;
use DateTime;
use Exception;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class TokenController extends AtexoController
{
    private $accesTokenObject;

    /**
     * TokenController constructor.
     *
     * @param LoggerInterface $loggerAccesToken
     * @param LoggerInterface $loggerGenerateToken
     * @param SessionInterface $session
     * @param ParameterBagInterface $parameterBag
     * @param Authentication $authentication
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function __construct(
        private LoggerInterface $loggerAccesToken,
        private LoggerInterface $loggerGenerateToken,
        private SessionInterface $session,
        private ParameterBagInterface $parameterBag,
        private Authentication $authentication
    ) {
    }

    /**
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getAccesTokenObject()
    {
        return $this->accesTokenObject;
    }

    /**
     * @param mixed $accesTokenObject
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function setAccesTokenObject($accesTokenObject)
    {
        $this->accesTokenObject = $accesTokenObject;
    }

    /**
     * Permet de creer un token et le retourne.
     *
     * @Method("POST")
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016*/
    #[Route(path: '/token/generate', name: 'generate_token')]
    public function generateTokenAction(Request $request): Response
    {
        try {
            $this->loggerGenerateToken->info('Debut generation du token');
            $accesToken = new AccessToken();

            if ('json' != $request->getContentType()) {
                $this->loggerGenerateToken->error('Content Type is not application/json.');

                return new Response(
                    json_encode(['error' => 'Content Type is not application/json.'], JSON_THROW_ON_ERROR)
                );
            }

            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

            if (empty($data) || !is_array($data)) {
                $this->loggerGenerateToken->error('The context data are empty.');

                return new Response(json_encode(['error' => 'The context data are empty.'], JSON_THROW_ON_ERROR));
            }

            $accesToken->setData(json_encode($data, JSON_THROW_ON_ERROR));
            $accesToken->setIp($request->getClientIp());
            $accesToken->setUserId($data['id']);
            $accesToken->setUserType(('entreprise' == $data['type_user']) ? $this->getParameter('type_user_entreprise') : $this->parameterBag->get('type_user_agent'));
            $this->loggerGenerateToken->info('id_user = ' . $data['id'] . ' , type_user = ' . $data['type_user']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($accesToken);
            $em->flush();

            $this->loggerGenerateToken->info('Token genere avec succes : ' . $accesToken->getId());

            return new Response(json_encode(['token' => $accesToken->getId()], JSON_THROW_ON_ERROR));
        } catch (Exception $e) {
            $this->loggerGenerateToken->error(
                'The context data are empty.' . $e->getMessage() . PHP_EOL
                . 'Trace: ' . $e->getTraceAsString()
            );

            return new Response(json_encode(['error' => $e->getMessage()], JSON_THROW_ON_ERROR));
        } finally {
            $this->loggerGenerateToken->info('Fin generation du token');
        }
    }

    /**
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016*/
    #[Route(path: '/token/acces/{token}', name: 'acces_token')]
    public function accesTokenAction($token, Request $request)
    {
        $repository = null;
        $authResult = null;
        $this->loggerAccesToken->info("Debut acces a l'application via le token : $token");

        //Recuperation objet 't_acces_token' en base de donnees
        $tAccesToken = $this->getDoctrine()->getManager()
            ->getRepository(AccessToken::class)->findOneBy(['id' => $token]);
        if (!$tAccesToken) {
            $log = "The token '$token' was not found in the database (table t_acces_token)";
            $this->loggerAccesToken->error($log);
            throw new Exception($log);
        }

        if ($this->isTokenExpired($tAccesToken)) {
            $log = 'The token ' . $tAccesToken->getId() . ' is expired.';
            $this->loggerAccesToken->error($log);
            throw new Exception($log);
        }

        $this->setAccesTokenObject($tAccesToken);

        //Recuperation du repository de l'utilisateur
        if ($this->getAccesTokenObject()->getUserType() == $this->getParameter('type_user_agent')) {
            $this->loggerAccesToken->info("Recuperation du repository de l'utilisateur : " . Agent::class);
            $repository = $this->getDoctrine()->getManager()->getRepository(Agent::class);
        } elseif ($this->getAccesTokenObject()->getUserType() == $this->getParameter('type_user_entreprise')) {
            $this->loggerAccesToken->info("Recuperation du repository de l'utilisateur : Inscrit");
            $repository = $this->getDoctrine()->getManager()->getRepository(Inscrit::class);
        }

        //Recuperation de l'utilisateur lie au token en base de donnees
        $this->loggerAccesToken->info(
            "Recuperation de l'utilisateur lie au token en base de donnees : id => " .
            $this->getAccesTokenObject()->getUserId()
        );

        if (null != $this->getAccesTokenObject()->getUserId()) {
            $userToken = $repository->findOneById($this->getAccesTokenObject()->getUserId());
            if (empty($userToken)) {
                $log = 'The user (id = ' . $this->getAccesTokenObject()->getUserId() . ') does not exist in database';
                $this->loggerAccesToken->error($log);
                throw new Exception($log);
            }

            //Authentification de l'utilisateur en base de donnees
            $authResult = $this->authentication->authenticateUser($userToken);
        }

        $contexteAuthentification = $tAccesToken->getData();

        //Mettre le token en session de l'utilisateur
        $this->session->set('token_contexte_authentification', $token);
        $this->session->set('contexte_authentification', $contexteAuthentification);
        $this->session->set('_locale', $contexteAuthentification['langue']);
        $this->session->set(
            'contexte_authentification_' . $contexteAuthentification['reference_consultation'],
            $contexteAuthentification
        );
        $this->session->set(
            'token_contexte_authentification_' . $contexteAuthentification['reference_consultation'],
            $token
        );
        $request->setLocale($contexteAuthentification['langue']);

        if (null != $this->getAccesTokenObject()->getUserId()) {
            if (true === $authResult) {
                //Redirection de l'utilisateur vers la page souhaitee
                return $this->redirectAfterLogin();
            }

            $this->loggerAccesToken->info("Fin acces a l'application via le token : $token");
        } else {
            return $this->getRedirectResponse();
        }
    }

    /**
     * Permet de rediriger l'utilisateur apres l'authentification.
     *
     * @throws Exception
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function redirectAfterLogin()
    {
        return $this->getRedirectResponse();
    }

    /**
     * Permet de verifier si le jeton est expire.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    private function isTokenExpired(AccessToken $tAccesTokenObjet): bool
    {
        try {
            $this->loggerAccesToken->info(
                "Debut verification de la validite du jeton d'authentification : " . $tAccesTokenObjet->getId()
            );

            $dateCreation = $tAccesTokenObjet->getCreatedAt();
            $diffDays = date_diff($dateCreation, new DateTime())->format('%h');
            $delai = $this->parameterBag->get('delai_expiration_token_authentification');
            $this->loggerAccesToken->info(
                '[date de creation du jeton = ' . $dateCreation->format('y-m-d h-i-s')
                . '] , [difference date (mn) = ' . $diffDays * 60 . " ] , [delai d'expiration en (mn) = $delai]"
            );

            if (($diffDays * 60) > $delai) {
                $this->loggerAccesToken->info('Le jeton est EXPIRE.');

                return true;
            }
            $this->loggerAccesToken->info('Le jeton est VALIDE.');

            return false;
        } catch (Exception $e) {
            $messageErreur = 'ERREUR lors de la verification de la valide du jeton '
                . $tAccesTokenObjet->getId() . PHP_EOL . ' ';
            $messageErreur .= 'Message erreur : ' . $e->getMessage() . PHP_EOL . 'Trace : ' . $e->getTraceAsString();
            $this->loggerAccesToken->error($messageErreur);

            return false;
        }
    }

    /**
     * Permet de deconnecter l'utilisateur.
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016*/
    #[Route(path: '/token/disconnect', name: 'disconnect_token')]
    public function disconnectUser()
    {
        $this->authentication->disconnectUser();

        return new RedirectResponse($this->getParameter('URL_MPE') . '?page=Entreprise.EntrepriseHome');
    }

    /**
     * @return RedirectResponse
     * @throws \JsonException
     * @throws Exception
     */
    protected function getRedirectResponse(): RedirectResponse
    {
        $data = json_decode($this->getAccesTokenObject()->getData(), null, 512, JSON_THROW_ON_ERROR);

        if (!is_object($data)) {
            $log = 'The context data are not valid.' . PHP_EOL . 'Data = ' . $this->getAccesTokenObject()->getData();
            $this->loggerAccesToken->error($log);
            throw new Exception($log);
        }

        $routeName = $data->route;

        $this->loggerAccesToken->info("Redirection vers la route (route_name) :  '$routeName'");

        return new RedirectResponse($this->generateUrl($routeName, ['id' => $data->reference_consultation]));
    }
}
