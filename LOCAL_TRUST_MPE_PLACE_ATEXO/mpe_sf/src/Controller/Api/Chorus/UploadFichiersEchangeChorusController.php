<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Api\Chorus;

use App\Service\AgentTechniqueTokenService;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\Api\StatistiqueService;
use App\Service\AtexoAgent;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\AtexoUtil;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use Application\Service\Atexo\Atexo_Config;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use AtexoCrypto\Exception\ServiceException;
use DateTime;
use App\Controller\Api\AbstractWebserviceController;
use App\Entity\BloborganismeFile;
use App\Entity\Chorus\ChorusEchange;
use App\Entity\Chorus\ChorusFicheModificative;
use App\Entity\Chorus\ChorusFicheModificativePj;
use App\Service\File\FileHelper;
use Atexo\CryptoBundle\AtexoCrypto;
use AtexoCrypto\Dto\InfosHorodatage;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use PHPUnit\Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use ZipArchive;
use App\Service\AtexoFichierOrganisme;

/**
 * Class UploadFichiersEchangeChorusController
 **/
#[Route(path: '/api/{version}/echanges-chorus/upload/{idEchangeChorus}', name: 'api_echanges_chorus_upload')]
class UploadFichiersEchangeChorusController extends AbstractWebserviceController
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private ApiSerializer $apiSerializer,
        private AtexoAgent $agent,
        private AtexoEntreprise $atexoEntreprise,
        private AtexoEtablissement $atexoEtablissement,
        private AtexoInscrit $atexoInscrit,
        private OrganismeService $organismeService,
        private ModificationContratService $modificationContratService,
        private AtexoUtil $atexoUtil,
        private LoggerInterface $logger,
        private ParameterBagInterface $parameterBag,
        private AtexoFichierOrganisme $atexoFichierOrganisme,
        private AtexoCrypto $atexoCrypto
    ) {
        parent::__construct(
            $entityManager,
            $agentTechniqueTokenService,
            $apiSerializer,
            $agent,
            $atexoEntreprise,
            $atexoEtablissement,
            $atexoInscrit,
            $organismeService,
            $modificationContratService,
            $atexoUtil
        );
    }

    /**
     * @Operation(
     *     tags={"/echanges-chorus"},
     *     summary="Créer un échange chorus",
     *     @SWG\Parameter(
     *         name="file",
     *         in="formData",
     *         description="fichier que l'on souhaite envoyer",
     *         required=false,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     * POST Route annotation.
     * @Rest\View()
     * @Rest\Post()
     *
     * @param Request $request
     * @param FileHelper $fileHelper
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function postChorusEchange(Request $request, FileHelper $fileHelper): Response
    {
        $idEchangeChorus = $request->get('idEchangeChorus');

        try {
            /**
             * @var ChorusEchange $echangeChorus
             */
            $echangeChorus = $this->entityManager
                ->getRepository(ChorusEchange::class)
                ->find($idEchangeChorus);

            if (!$echangeChorus) {
                throw new InvalidArgumentException('Echange Chorus n\' a pas été trouvé');
            }

            $chorusFicheModificative = $this->entityManager
                ->getRepository(ChorusFicheModificative::class)
                ->findOneBy(['idEchange' => $idEchangeChorus]);

            if (!$chorusFicheModificative instanceof ChorusFicheModificative) {
                throw new InvalidArgumentException('Aucune fiche modificatif trouvé.');
            }
        } catch (\Exception $exception) {
            $response = new Response('Error : ' . $exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
            $this->logger->error(
                'Error lors de l\'appel du WS : /api/{version}/echanges-chorus/upload/{idEchangeChorus}' .
                PHP_EOL .
                "Erreur: " .
                $exception->getMessage() .
                PHP_EOL .
                "Trace: " .
                $exception->getTraceAsString()
            );

            return $response->send();
        }

        try {
            $this->removeOldChorusFicheModificativePj($chorusFicheModificative, $fileHelper);
            $files = $request->files->all();
            $file = array_shift($files);
            if (is_array($file)) {
                $file = $file[0];
            }

            if (!$file instanceof UploadedFile) {
                throw new InvalidArgumentException('No file found');
            }

            $organisme = $echangeChorus->getOrganisme();
            $tmpFilePath = $file->getPathname();
            $tmpMpe = $this->parameterBag->get('COMMON_TMP');

            $blobs = [];
            $zip = new ZipArchive();
            if ($zip->open($tmpFilePath) === true) {
                for ($i = 0; $i < $zip->numFiles; $i++) {
                    $filename = $zip->getNameIndex($i);
                    $fileinfo = pathinfo($filename);
                    $fileExtract = $tmpMpe . 'tmp_' . date('Ymdhis') . '_' . $fileinfo['basename'];
                    copy("zip://" . $tmpFilePath . "#" . $filename, $fileExtract);
                    $dir = $this->atexoFichierOrganisme->moveTmpFile($fileExtract, $request);
                    $blobs[$i]['id'] = $this->atexoFichierOrganisme->insertFile(
                        $filename,
                        $dir,
                        $organisme
                    );
                    $this->setChorusFicheModificativePj(
                        $filename,
                        $blobs[$i]['id'],
                        $organisme,
                        $chorusFicheModificative,
                        $fileHelper
                    );
                    unlink($fileExtract);
                }
                $zip->close();
                unlink($tmpFilePath);
            }
            $response = $this->getWithPaginate($request, $this->prepareData($blobs));
        } catch (\Exception $exception) {
            $response = new Response('Error : ' . $exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
            $this->logger->error(
                'Error lors de l\'appel du WS : /api/{version}/echanges-chorus/upload/{idEchangeChorus}' .
                PHP_EOL .
                "Erreur technique sur les pièces jointes: " .
                $exception->getMessage() .
                PHP_EOL .
                "Trace: " .
                $exception->getTraceAsString()
            );

            $echangeChorus->setStatutEchange(Atexo_Config::getParameter('STATUT_ECHANGE_CHORUS_REJETE_NON_ENVOYE'));
            $this->entityManager->persist($echangeChorus);
            $this->entityManager->flush();
        }

        return $response->send();
    }

    private function prepareData(array $blobs): array
    {
        foreach ($blobs as $key => $value) {
            $blobs['documents-chorus'][]['document-chorus'] = $blobs[$key];
            unset($blobs[$key]);
        }

        return $blobs;
    }

    /**
     * @return void
     * @throws ServiceException
     */
    private function setChorusFicheModificativePj(
        string $nameFile,
        string $idBlob,
        string $organisme,
        ChorusFicheModificative $chorusFicheModificative,
        FileHelper $fileHelper
    ) {
        try {
            $blobPath = $fileHelper->getFileByBlobId($idBlob);
            $taille = $this->atexoFichierOrganisme->getFileSize($idBlob, $organisme);
            $ficheModificatifPj = new ChorusFicheModificativePj();
            $hash256 = hash_file("sha256", $blobPath);
            $infosHorodatage = $this->atexoCrypto->demandeHorodatage($hash256);

            if ($infosHorodatage instanceof InfosHorodatage) {
                $info = "Demande d'horodatage OK." . PHP_EOL;
                $info .= "Infos demande d'horodatage : " . print_r($infosHorodatage, true);
                $this->logger->info($info);
                $untrusteddateOffre = new DateTime(
                    date('Y-m-d H:i:s', $infosHorodatage->getHorodatage())
                );
                $ficheModificatifPj->setHorodatage($infosHorodatage->getJetonHorodatageBase64());
                $ficheModificatifPj->setUntrusteddate($untrusteddateOffre->format('Y-m-d H:i:s'));
            }
            $ficheModificatifPj->setFichier($idBlob);
            $ficheModificatifPj->setIdFicheModificative($chorusFicheModificative);
            $ficheModificatifPj->setNomFichier($nameFile);
            $ficheModificatifPj->setTaille($taille);
            $this->entityManager->persist($ficheModificatifPj);
            $this->entityManager->flush();
        } catch (Exception $exception) {
            $this->logger->error(
                'Error lors de l\'enregistrement : ' .
                PHP_EOL .
                "Erreur technique sur les pièces jointes: " .
                $exception->getMessage() .
                PHP_EOL .
                "Trace: " .
                $exception->getTraceAsString()
            );
        }
    }

    private function removeOldChorusFicheModificativePj(ChorusFicheModificative $chorusFicheModificative, FileHelper $fileHelper)
    {
        $allFicheModificativePj = $this->entityManager
            ->getRepository(ChorusFicheModificativePj::class)
            ->findBy(['idFicheModificative' => $chorusFicheModificative->getIdFicheModificative()]);

        if (!$allFicheModificativePj) {
            return true;
        }

        /**
         * @var ChorusFicheModificativePj $ficheMpdificativePj
         */
        foreach ($allFicheModificativePj as $ficheMpdificativePj) {
            try {
                $idFichier = $ficheMpdificativePj->getFichier();
                $blobPath = $fileHelper->getFileByBlobId($idFichier);

                if (!is_null($blobPath)) {
                    unlink($blobPath);
                }

                $this->entityManager->remove($ficheMpdificativePj);
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }
        }

        return true;
    }
}
