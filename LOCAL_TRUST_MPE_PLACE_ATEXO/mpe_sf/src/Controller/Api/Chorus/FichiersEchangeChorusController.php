<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Api\Chorus;

use App\Service\AgentTechniqueTokenService;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\Api\StatistiqueService;
use App\Service\AtexoAgent;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\AtexoUtil;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Exception;
use App\Controller\Api\AbstractWebserviceController;
use App\Entity\BloborganismeFile;
use App\Entity\Chorus\ChorusEchange;
use App\Entity\Chorus\ChorusFicheModificative;
use App\Entity\Chorus\ChorusFicheModificativePj;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use ZipArchive;
use App\Service\AtexoFichierOrganisme;

/**
 * Class FichiersEchangeChorusController*/
#[Route(path: '/api/{version}/echanges-chorus/download-zip/{idEchangeChorus}', name: 'api_echanges_chorus_download_zip')]
class FichiersEchangeChorusController extends AbstractWebserviceController
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private ApiSerializer $apiSerializer,
        private AtexoAgent $agent,
        private AtexoEntreprise $atexoEntreprise,
        private AtexoEtablissement $atexoEtablissement,
        private AtexoInscrit $atexoInscrit,
        private OrganismeService $organismeService,
        private ModificationContratService $modificationContratService,
        private AtexoUtil $atexoUtil,
        private LoggerInterface $logger,
        private ParameterBagInterface $parameterBag
    ) {
        parent::__construct(
            $entityManager,
            $agentTechniqueTokenService,
            $apiSerializer,
            $agent,
            $atexoEntreprise,
            $atexoEtablissement,
            $atexoInscrit,
            $organismeService,
            $modificationContratService,
            $atexoUtil
        );
    }

    /**
     * @Operation(
     *     tags={"/echanges-chorus"},
     *     summary="download les pièce de l'échange + l'acte modificatif",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     * @Rest\Get()
     * @Rest\View()
     *
     * @return Response
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function getDownloadZipEchangeAction(Request $request)
    {
        try {
            $idEchangeChorus = $request->get('idEchangeChorus');
            $chorusFicheModificative = $this->entityManager
                ->getRepository(ChorusFicheModificative::class)
                ->findOneBy([
                    'idEchange' => $idEchangeChorus
                ]);


            if (!$chorusFicheModificative instanceof ChorusFicheModificative) {
                throw new InvalidArgumentException('Echange Chorus n\' a pas été trouvé');
            }

            $chorusFicheModificativePj = $this->entityManager
                ->getRepository(ChorusFicheModificativePj::class)
                ->findBy([
                    'idFicheModificative' => $chorusFicheModificative->getIdFicheModificative()
                ]);

            $zip = new ZipArchive();
            $tmp = $this->parameterBag->get('COMMON_TMP') . '/ZipEchange' . date('Ymdhis') . '.zip';
            if ($zip->open($tmp, ZipArchive::CREATE) === true) {
                foreach ($chorusFicheModificativePj as $fichier) {
                    $dataFile = $this->getFile($fichier->getFichier());
                    if (array_key_exists('path', $dataFile)) {
                        $zip->addFile($dataFile['path'], $dataFile['name']);
                    }
                }

                if ($chorusFicheModificative->getIdBlobFicheModificative() !== null) {
                    $idBlob = $chorusFicheModificative->getIdBlobFicheModificative();
                    if ($idBlob) {
                        $dataFile = $this->getFile($idBlob);
                        if (array_key_exists('path', $dataFile)) {
                            $zip->addFile($dataFile['path'], $dataFile['name']);
                        }
                    }
                }
                $zip->close();
            }

            if (!file_exists($tmp)) {
                $response = new Response('', Response::HTTP_NO_CONTENT);
            } else {
                $response = new BinaryFileResponse($tmp);
                $response->setContentDisposition(
                    ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                    'ZipEchange' . date('Ymdhis') . '.zip'
                );
            }
        } catch (Exception $exception) {
            $error = 'Problème avec le WS : echanges-chorus/download-zip' .
                PHP_EOL .
                "Erreur: " .
                $exception->getMessage();
            $response = new Response($error, Response::HTTP_INTERNAL_SERVER_ERROR);
            $this->logger->error(
                $error .
                PHP_EOL .
                "Trace: " .
                $exception->getTraceAsString()
            );
        }
        return $response;
    }

    /**
     * @param array $blobs
     */
    private function getFile($idBlob): array
    {
        $dataFile = [];
        $blobOrganismeFile = $this->entityManager->getRepository(BloborganismeFile::class)->find($idBlob);
        if ($blobOrganismeFile instanceof BloborganismeFile) {
            $dir = $this->parameterBag->get('BASE_ROOT_DIR') .
                $blobOrganismeFile->getOrganisme() .
                "/files/";
            $filename = $blobOrganismeFile->getId() . '-0';
            if (!file_exists($dir . $filename)) {
                $chemin = null;
                if ($blobOrganismeFile->getChemin() !== null) {
                    $chemin = $blobOrganismeFile->getChemin();
                    $dir = $this->parameterBag->get('BASE_ROOT_DIR') .
                        $blobOrganismeFile->getChemin();
                }

                if (!file_exists($dir . $filename)) {
                    $mnts = $this->getParameter('DOCUMENT_ROOT_DIR_MULTIPLE');
                    if ($mnts !== '') {
                        $arrayMnts = explode(';', (string) $mnts);
                        foreach ($arrayMnts as $mnt) {
                            $dir = $mnt . $chemin;
                            if (file_exists($dir . $filename)) {
                                break;
                            }
                        }
                    }
                }
            }
            $dataFile['path'] = $dir . $filename;
            $dataFile['name'] = $blobOrganismeFile->getName();
        }


        return $dataFile;
    }
}
