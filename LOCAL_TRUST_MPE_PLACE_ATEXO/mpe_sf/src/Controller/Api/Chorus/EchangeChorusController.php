<?php

namespace App\Controller\Api\Chorus;

use App\Service\AgentTechniqueTokenService;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\Api\StatistiqueService;
use App\Service\AtexoAgent;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\AtexoUtil;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use App\Controller\Api\AbstractWebserviceController;
use App\Entity\Chorus\ChorusEchange;
use App\Service\AtexoEchangeChorus;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ApiEchangeChorus
 **/
#[Route(path: '/api/{version}/echanges-chorus.{format}', name: 'api_echanges_chorus')]
class EchangeChorusController extends AbstractWebserviceController
{
    public function __construct(
        private EntityManagerInterface $em,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private ApiSerializer $apiSerializer,
        private AtexoAgent $agent,
        private AtexoEntreprise $atexoEntreprise,
        private AtexoEtablissement $atexoEtablissement,
        private AtexoInscrit $atexoInscrit,
        private OrganismeService $organismeService,
        private ModificationContratService $modificationContratService,
        private AtexoUtil $atexoUtil,
        private LoggerInterface $logger,
        private AtexoEchangeChorus $echangeChorus
    ) {
        parent::__construct(
            $em,
            $agentTechniqueTokenService,
            $apiSerializer,
            $agent,
            $atexoEntreprise,
            $atexoEtablissement,
            $atexoInscrit,
            $organismeService,
            $modificationContratService,
            $atexoUtil
        );
        $this->setEchangeChorus($this->echangeChorus);
    }

    /**
     * @Operation(
     *     tags={"/echanges-chorus"},
     *     summary="Retourner les échanges chorus",
     *     @SWG\Parameter(
     *         name="limit",
     *         in="body",
     *         description="Nombre d'éléments par page",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="body",
     *         description="Numéro de page - Entier supérieur à 0",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="lastUpdateTimestamp",
     *         in="body",
     *         description="Dernière modification - Timestamp",
     *         required=false,
     *         @SWG\Schema(type="timestamp")
     *     ),
     *     @SWG\Parameter(
     *         name="module",
     *         in="body",
     *         description="nom du module appelant (exec,redac, ...)",
     *         required=false,
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * @Rest\Get()
     * @Rest\View()
     */
    public function listAction(Request $request): void
    {
        $response = $this->getWithPaginate($request, ChorusEchange::class);
        $response->send();
    }

    /**
     * @Operation(
     *     tags={"/echanges-chorus"},
     *     summary="Créer un échange chorus",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * POST Route annotation.
     * @Rest\View()
     * @Rest\Post()
     *
     * @return Response
     * @throws Exception
     */
    public function postChorusEchange(Request $request): Response
    {
        return $this->createEntity($request, AtexoEchangeChorus::class, $this->logger);
    }

    /**
     * @Operation(
     *     tags={"/echanges-chorus"},
     *     summary="modifier un échange chorus",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * PUT Route annotation.
     * @Rest\View()
     * @Rest\Put()
     *
     * @return Response
     * @throws Exception
     */
    public function putChorusEchange(Request $request): Response
    {
        return $this->updateEntity($request, AtexoEchangeChorus::class, $this->logger);
    }
}
