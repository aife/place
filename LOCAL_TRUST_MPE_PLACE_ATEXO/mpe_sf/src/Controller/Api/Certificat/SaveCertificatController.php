<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Api\Certificat;

use App\Service\Api\Serializer as ApiSerializer;
use App\Service\AtexoAgent;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\AtexoUtil;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use Exception;
use JsonException;
use App\Controller\Api\AbstractWebserviceController;
use App\Entity\Agent;
use App\Entity\CertificatPermanent;
use App\Exception\ApiProblemException;
use App\Service\AgentTechniqueTokenService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SaveCertificat.
 **/
#[Route(path: '/', name: 'atexo_api_mamp')]
class SaveCertificatController extends AbstractWebserviceController
{
    private const AGENT_TECHNIQUE_LOGIN = 'mamp';
    private const NOM_WS = 'POST_MAMP';

    public function __construct(
        private EntityManagerInterface $em,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private ApiSerializer $apiSerializer,
        private AtexoAgent $agent,
        private AtexoEntreprise $atexoEntreprise,
        private AtexoEtablissement $atexoEtablissement,
        private AtexoInscrit $atexoInscrit,
        private OrganismeService $organismeService,
        private ModificationContratService $modificationContratService,
        private AtexoUtil $atexoUtil,
        private LoggerInterface $logger
    ) {
        parent::__construct(
            $em,
            $agentTechniqueTokenService,
            $apiSerializer,
            $agent,
            $atexoEntreprise,
            $atexoEtablissement,
            $atexoInscrit,
            $organismeService,
            $modificationContratService,
            $atexoUtil
        );
    }

    /**
     * @Rest\Post("cryptographie/mpeMock/dual-key")
     */
    public function dualKey(Request $request): Response
    {
        $this->logger->info('WS CRYPTOGRAPHIE/mpeMock/dual-key');
        $agentTechnique = $this->agentTechniqueTokenService->getAgentFromToken($request->get('ticket'));
        $agentId = $request->get('agentId');
        $this->logger->info('Récuprération de l\'agent technique');
        $result = [];
        $result['code'] = Response::HTTP_UNAUTHORIZED;
        if (!($agentTechnique instanceof Agent) || self::AGENT_TECHNIQUE_LOGIN != $agentTechnique->getLogin()) {
            $result['message'] = 'Ticket invalide';
            return $this->getResponse($result);
        }
        if (!$this->agentTechniqueTokenService->isTokenCanAccessWs($agentTechnique, self::NOM_WS)) {
            $result['code'] = Response::HTTP_UNAUTHORIZED;
            $result['message'] = 'Agent Technique non autorisé d\'accéder à ce WS';
            return $this->getResponse($result);
        }
        $this->logger->info('Agent technique OK');
        $data = json_decode($request->getContent(), true);
        if (!isset($data['nomCleChiffrement']) ||
            !isset($data['estCleChiffrementSecours']) ||
            !isset($data['certificat']) ||
            !isset($data['cnCleChiffrement'])
        ) {
            $result['message'] =
                'Le json doit contenir ces quatres params: nomCleChiffrement, certificat, estCleChiffrementSecours,cnCleChiffrement';
            return $this->getResponse($result);
        }
        $this->logger->info('Récupération des données');
        $nomCleChiffrement = $data['nomCleChiffrement'];
        $estCleChiffrementSecours = $data['estCleChiffrementSecours'];
        $certificat = $data['certificat'];
        $cnCleChiffrement = $data['cnCleChiffrement'];
        if (!$certificat ||
            !is_string($nomCleChiffrement) ||
            !is_bool($estCleChiffrementSecours) ||
            !is_string($cnCleChiffrement)
        ) {
            $result['message'] = 'Les données sont erronées';
            return $this->getResponse($result);
        }
        $this->logger->info('Données OK');

        $agent = $this->getEntityManager()->getRepository(Agent::class)->find($agentId);
        if (!($agent instanceof Agent)) {
            $result['message'] = 'Agent Non Trouvé';
            return $this->getResponse($result);
        }
        $certificatPermanent = new CertificatPermanent();
        $certificatPermanent->setId(
            $this->getEntityManager()->getRepository(CertificatPermanent::class)->getHighestId()
            + 1
        );
        $certificatPermanent->setCertificat(
            "-----BEGIN CERTIFICATE-----\n" . $certificat . "\n-----END CERTIFICATE-----"
        );
        $certificatPermanent->setIdAgent($agent->getId());
        $certificatPermanent->setOrganisme($agent->getOrganisme());
        if ($request->query->has('serviceId')) {
            $certificatPermanent->setServiceId($request->get('serviceId'));
        } else {
            $certificatPermanent->setServiceId($agent->getServiceId());
        }
        $certificatPermanent->setTitre($cnCleChiffrement);
        $certificatPermanent->setNom($nomCleChiffrement);
        if ($estCleChiffrementSecours) {
            $certificatPermanent->setMasterKey(1);
        } else {
            $certificatPermanent->setMasterKey(0);
        }
        try {
            $this->logger->info('Enregistrement du nouveau certificat');
            $this->getEntityManager()->persist($certificatPermanent);
            $this->getEntityManager()->flush();
            $result['code'] = Response::HTTP_OK;
            $result['message'] = 'Certificat créé avec succès';
        } catch (Exception $e) {
            $erreur = 'Erreur : ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString();
            $this->logger->error(
                "Erreur lors de l'enregistrement de la certificat permanente : WS MAMP, " .
                PHP_EOL . $erreur
            );
            throw $e;
        }

        return $this->getResponse($result);
    }
    /**
     * @param $result
     * @throws JsonException
     */
    protected function getResponse($result): Response
    {
        return new Response(json_encode($result, JSON_THROW_ON_ERROR), $result['code'], [self::CONTENT_TYPE_KEY => 'application/json',]);
    }
}
