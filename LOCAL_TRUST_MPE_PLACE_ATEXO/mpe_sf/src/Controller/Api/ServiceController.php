<?php

namespace App\Controller\Api;

use App\Service\AgentTechniqueTokenService;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\AtexoAgent;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\AtexoUtil;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use DateTime;
use App\Entity\AffiliationService;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Entity\TFusionnerServices;
use App\Exception\ApiProblemDbException;
use App\Exception\ApiProblemException;
use App\Exception\ApiProblemInvalidArgumentException;
use App\Exception\ApiProblemInvalidTokenException;
use App\Exception\ApiProblemNotFoundException;
use App\Exception\ApiProblemUnauthorizedException;
use App\Exception\ApiProblemValidationException;
use App\Exception\ApiProblemValidationSchemaException;
use App\Form\Model\ServiceType;
use App\Model\ApiProblem;
use App\Model\ServiceModel;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ServiceController.*/
#[Route(path: '/api/{version}', name: 'atexo_api_', defaults: ['format' => 'xml'], requirements: ['version' => '^(?:(?!v2).)+$'])]
class ServiceController extends AbstractWebserviceController
{
    public function __construct(
        private EntityManagerInterface $em,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private ApiSerializer $apiSerializer,
        private AtexoAgent $agent,
        private AtexoEntreprise $atexoEntreprise,
        private AtexoEtablissement $atexoEtablissement,
        private AtexoInscrit $atexoInscrit,
        private OrganismeService $organismeService,
        private ModificationContratService $modificationContratService,
        private AtexoUtil $atexoUtil,
    ) {
        parent::__construct(
            $em,
            $agentTechniqueTokenService,
            $apiSerializer,
            $agent,
            $atexoEntreprise,
            $atexoEtablissement,
            $atexoInscrit,
            $organismeService,
            $modificationContratService,
            $atexoUtil
        );
    }

    /**
     * @Operation(
     *     tags={"/services"},
     *     summary="Créer un service",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     * @Rest\View()
     * @Rest\Post("/services.{format}", name="services_post")
     *
     * @return Response
     */
    public function newAction(Request $request, LoggerInterface $logger)
    {
        $serviceModel = new ServiceModel();
        $form = $this->createForm(ServiceType::class, $serviceModel);
        $form->submit($this->decodeRequestBody($request));

        if (!$form->isValid()) {
            throw new ApiProblemValidationException($this->getErrorsFromForm($form));
        }

        if (!$this->isAgentTechniqueGranted($request, $serviceModel)) {
            $organisme = !empty($serviceModel->organisme) ? $serviceModel->organisme : null;
            throw new ApiProblemUnauthorizedException($this->getMessageErrorForAccessDenied($organisme));
        }

        $this->validateOrganisme($serviceModel->organisme);
        $service = $this->hydrateEntityService($serviceModel);
        $serviceModel->id = $service->getId();
        $serviceModel->dateCreation = $service->getDateCreation();
        $serviceModel->dateModification = $service->getDateModification();

        try {
            if (!empty($serviceModel->idParent)) {
                $serviceParent = $this
                    ->getDoctrine()
                    ->getRepository(Service::class)
                    ->findOneBy([
                        'id' => $serviceModel->idParent,
                        'organisme' => $service->getOrganisme(),
                    ]);


                if (empty($serviceParent)) {
                    $serviceParent = $this
                        ->getDoctrine()
                        ->getRepository(Service::class)
                        ->findOneBy([
                            'oldId' => $serviceModel->idParent,
                            'organisme' => $service->getOrganisme(),
                        ]);
                }

                if (!empty($serviceParent)) {
                    $affiliation = $this
                        ->getDoctrine()
                        ->getRepository(AffiliationService::class)
                        ->findOneBy([
                            'organisme' => $serviceParent->getOrganisme(),
                            'serviceParentId' => $serviceParent->getId(),
                            'serviceId' => $service->getId(),
                        ]);
                    if (empty($affiliation)) {
                        $affiliation = new AffiliationService();
                        $affiliation->setOrganisme($serviceParent->getOrganisme());
                        $affiliation->setServiceParentId($serviceParent);
                        $affiliation->setServiceId($service);
                        $this->getDoctrine()->getManager()->persist($affiliation);
                    }
                }
            }
            $this->getDoctrine()->getManager()->persist($service);
            $this->getDoctrine()->getManager()->flush();
        } catch (Exception $exception) {
            $logger->error($exception->getMessage());
            throw new ApiProblemDbException();
        }

        return $this->createServiceApiResponse($serviceModel, $request->get('format', 'xml'), 201);
    }
    private function isAgentTechniqueGranted(Request $request, $serviceRequested)
    {
        $agentTechniqueService = $this->agentTechniqueTokenService;
        $token = $request->get('token') ?? $request->get('ticket') ?? null;
        if (null === $token) {
            throw new ApiProblemInvalidTokenException("Erreur lors de la récupération du token!");
        }

        $currentAgent = $agentTechniqueService->getAgentFromToken($token);
        $flux = [];
        $flux['acronymeOrganisme'] = !empty($serviceRequested->organisme) ? $serviceRequested->organisme : null;
        $flux['service']['id'] = !empty($serviceRequested->idParent) ? $serviceRequested->idParent : 0;

        return $agentTechniqueService->isGrantedFromAgentWs($currentAgent, $flux);
    }
    private function createCheminService($service)
    {
        $slash = ' / ';
        $chemin = null;
        $organisme = $this
            ->getDoctrine()
            ->getRepository(Organisme::class)
            ->findOneByAcronyme([
                'acronyme' => $service->organisme,
            ]);

        if (!empty($service->idParent)) {
            $serviceFuturParent = $this
                ->getDoctrine()
                ->getRepository(Service::class)
                ->findOneBy([
                    'id' => $service->idParent,
                    'organisme' => $service->organisme,
                ]);


            if (!empty($serviceFuturParent) && !empty($serviceFuturParent->getId())) {
                $idParent = $serviceFuturParent->getId();
                $chemin = $serviceFuturParent->getSigle().$slash;
                do {
                    $ancestors = $this->getDoctrine()->getRepository(Service::class)
                        ->getServiceParent(
                            $idParent,
                            $service->organisme
                        );

                    $idParent = $ancestors?->getId();
                    if ($idParent) {
                        $chemin = $ancestors->getSigle().$slash.$chemin;
                    }
                } while (!empty($idParent));
            }
        }

        $chemin = $organisme->getDenominationOrg().$slash.$chemin.$service->sigle.' - '.$service->libelle;
        return $chemin;
    }
    private function decodeRequestBody(Request $request)
    {
        if ('xml' == $request->get('format', 'xml')) {
            $xml = simplexml_load_string(($request->getContent()));
            $json = json_encode($xml->envoi->service, JSON_THROW_ON_ERROR);
            $json = str_replace('{}', '""', $json);
            $service = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
        } else {
            $service = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        }

        if (!$this->validateFlux($request->getContent(), $request->get('format'), 'service')) {
            throw new ApiProblemValidationSchemaException($request->get('format'));
        }

        if (null === $service) {
            $problem = new ApiProblem(
                400,
                ApiProblem::TYPE_INVALID_REQUEST_BODY_FORMAT
            );
            throw new ApiProblemException($problem, 1);
        }

        return $service;
    }
    private function hydrateEntityService($serviceModel, $serviceToUpdate = null)
    {
        if (empty($serviceToUpdate)) {
            $serviceToUpdate = new Service();
            $lastService = $this
                ->getDoctrine()
                ->getRepository(Service::class)
                ->findOneBy([], ['id' => 'DESC']);
            $serviceToUpdate->setId($lastService->getId() + 1);
        }

        $now = new DateTime();
        $date = $now->format('Y-m-d H:i:s');
        $serviceToUpdate->setDateModification($date);
        if (empty($serviceToUpdate->getDateCreation())) {
            $serviceToUpdate->setDateCreation($date);
        }

        $serviceToUpdate->setLibelle($serviceModel->libelle);
        $sigle = $serviceModel->sigle ?? '';

        $serviceToUpdate->setSigle($sigle);
        $serviceToUpdate->setFormeJuridique($serviceModel->formeJuridique);
        $serviceToUpdate->setFormeJuridiqueCode($serviceModel->formeJuridiqueCode);
        $serviceToUpdate->setIdExterne($serviceModel->idExterne);
        $serviceToUpdate->setComplement($serviceModel->complement);
        $serviceToUpdate->setSiren($serviceModel->siren);
        $serviceToUpdate->setIdEntite($serviceModel->idEntite);
        $serviceToUpdate->setCheminComplet($this->createCheminService($serviceModel));
        if (!empty($serviceModel->email)) {
            $serviceToUpdate->setMail($serviceModel->email);
        }

        $organisme = $this
            ->getDoctrine()
            ->getRepository(Organisme::class)
            ->findOneByAcronyme($serviceModel->organisme);

        if (!$organisme) {
            throw new ApiProblemNotFoundException();
        }
        $serviceToUpdate->setOrganisme($organisme);

        return $serviceToUpdate;
    }
    protected function createServiceApiResponse($data, $format, $statusCode = 200)
    {
        $content = null;
        if ('xml' == $format) {
            $content = $this->renderView('api/service.xml.twig', ['service' => $data]);
        } else {
            $content = json_encode(['mpe' => ['reponse' => ['statutReponse' => 'OK', 'service' => $data]]], JSON_THROW_ON_ERROR);
        }

        return new Response($content, $statusCode, [
            'Content-Type' => 'application/'.$format,
        ]);
    }
    /**
     * @Operation(
     *     tags={"/services"},
     *     summary="Mettre à jour un service",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     * @Rest\View()
     * @Rest\Put("/services.{format}", name="services_put")
     *
     * @return Response
     *
     * @throws Exception
     */
    public function updateAction(Request $request)
    {
        $serviceModel = new ServiceModel();
        $form = $this->createForm(ServiceType::class, $serviceModel);
        $form->submit($this->decodeRequestBody($request));

        if (!$form->isValid()) {
            throw new ApiProblemValidationException($this->getErrorsFromForm($form));
        }

        $this->validateOrganisme($serviceModel->organisme);
        $service = $this->getDoctrine()
            ->getRepository(Service::class)
            ->findOneBy([
                'organisme' => $serviceModel->organisme,
                'id' => $serviceModel->id,
            ]);

        if (!$service) {
            throw new ApiProblemNotFoundException();
        }

        $service = $this->hydrateEntityService($serviceModel, $service);
        if (!$this->isAgentTechniqueGranted($request, $serviceModel)) {
            $organisme = !empty($serviceModel->organisme) ? $serviceModel->organisme : null;
            $serviceId = !empty($serviceModel->id) ? $serviceModel->id : null;
            throw new ApiProblemUnauthorizedException($this->getMessageErrorForAccessDenied($organisme, $serviceId));
        }

        $serviceModel->id = $service->getId();
        $serviceModel->dateCreation = $service->getDateCreation();
        $serviceModel->dateModification = $service->getDateModification();

        try {
            $this->updateAffiliation($serviceModel, $service);

            return $this->createServiceApiResponse($serviceModel, $request->get('format', 'xml'), 200);
        } catch (\Exception) {
            throw new ApiProblemDbException();
        }
    }
    /**
     * @Operation(
     *     tags={"/services"},
     *     summary="Lister les services",
     *     @SWG\Parameter(
     *         name="limit",
     *         in="body",
     *         description="Nombre d'éléments par page",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="body",
     *         description="Numéro de page - Entier supérieur à 0",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="lastUpdateTimestamp",
     *         in="body",
     *         description="Dernière modification - Timestamp",
     *         required=false,
     *         @SWG\Schema(type="timestamp")
     *     ),
     *     @SWG\Parameter(
     *         name="module",
     *         in="body",
     *         description="nom du module appelant (exec,redac, autres)",
     *         required=false,
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * @Rest\Get("/services.{format}", name="services_get_list")
     *
     * @throws Exception
     */
    public function listAction(Request $request)
    {
        $agentTechniqueService = $this->agentTechniqueTokenService;
        $agentTechnique = $agentTechniqueService->getAgentFromToken($request->get('ticket'));
        $orgServicesAllowed = $agentTechniqueService->getAllOrganismesServicesAllowed($agentTechnique);
        $response = $this->getWithPaginate($request, Service::class, $orgServicesAllowed);
        return $response->send();
    }
    /**
     * @Operation(
     *     tags={"/services"},
     *     summary="Récupérer un service",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * @Rest\Get("/services/{id}.{format}", name="services_get")
     * @Rest\View()
     */
    public function getServiceAction(Request $request): Response
    {
        $agentTechniqueService = $this->agentTechniqueTokenService;
        $agentTechnique = $agentTechniqueService->getAgentFromToken($request->get('ticket'));
        $orgServicesAllowed = $agentTechniqueService->getAllOrganismesServicesAllowed($agentTechnique);
        $response = $this->getWithPaginate($request, Service::class, $orgServicesAllowed);

        return $response->send();
    }
    private function updateAffiliation(ServiceModel $inputService, Service $serviceToUpdate)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            /**
             * @var AffiliationService $affiliation
             */

            $affiliation = $em->getRepository(AffiliationService::class)
                ->findOneBy([
                    'organisme' => $serviceToUpdate->getOrganisme(),
                    'serviceId' => $serviceToUpdate->getId(),
                ]);

            /**
             * @var Service $serviceFuturParent
             */
            $serviceFuturParent = $this
                ->getDoctrine()
                ->getRepository(Service::class)
                ->findOneBy([
                    'id' => $inputService->idParent,
                    'organisme' => $inputService->organisme,
                ]);
            if (!$serviceFuturParent && $affiliation) {
                //Si une affiliation existe et que l'on a plus de parent, on supprime la relation
                $em->getRepository(AffiliationService::class)->removeAffiliation(
                    $serviceToUpdate->getId(),
                    $serviceToUpdate->getOrganisme()
                );
            } elseif ($serviceFuturParent && $affiliation) {
                //Si une affiliation existe et que l'on a un parent, on met à jour la relation
                $em->getRepository(AffiliationService::class)->updateAffiliation(
                    $serviceToUpdate->getId(),
                    $serviceToUpdate->getOrganisme(),
                    $inputService->idParent
                );
            } elseif ($serviceFuturParent && !$affiliation) {
                //Si une affiliation n'existe pas et que l'on a un parent, on créé la relation
                $affiliationN = new AffiliationService();
                $affiliationN->setOrganisme($serviceFuturParent->getOrganisme());
                $affiliationN->setServiceParentId($serviceFuturParent);
                $affiliationN->setServiceId($serviceToUpdate);

                $em->persist($affiliationN);
                $em->flush();
            }

            $this->getDoctrine()
                ->getRepository(Service::class)
                ->thisUpdate($serviceToUpdate);

            return true;
        } catch (Exception) {
            throw new ApiProblemDbException();
        }
    }
    /**
     * @Operation(
     *     tags={"/services"},
     *     summary="Déplacer un service",
     *     @SWG\Parameter(
     *         name="deServiceId",
     *         in="body",
     *         description="Service Id source",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="versServiceId",
     *         in="body",
     *         description="Service Id destination",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="organisme",
     *         in="body",
     *         description="Organisme des services",
     *         required=false,
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * @Rest\Patch("/services.{format}", name="services_patch")
     *
     * @return Response
     */
    public function patchServicesAction(Request $request, LoggerInterface $logger)
    {
        $organisme = $request->get('organisme');
        $deServiceId = $request->get('deServiceId');
        $versServiceId = $request->get('versServiceId');

        $serviceModel = new ServiceModel();
        $serviceModel->id = $versServiceId;
        $serviceModel->organisme = $organisme;
        if (!$this->isAgentTechniqueGranted($request, $serviceModel)) {
            throw new ApiProblemUnauthorizedException($this->getMessageErrorForAccessDenied($organisme, $versServiceId));
        }

        $this->validateOrganisme($organisme);
        $this->validateService($deServiceId, $organisme, 'deServiceId');
        $this->validateService($versServiceId, $organisme, 'versServiceId');

        $this->fusionner($deServiceId, $versServiceId, $organisme, $logger);
        $format = $request->get('format', 'xml');
        $content = $this->getRetour($format, 'OK');

        return new Response($content, Response::HTTP_OK, [
            'Content-Type' => 'application/'.$format,
        ]);
    }
    private function fusionner($deServiceId, $versServiceId, $organisme, $logger)
    {
        try {
            $fusionService = $this->getDoctrine()->getRepository(TFusionnerServices::class)
                ->findOneBy([
                    'idServiceSource' => $deServiceId,
                    'idServiceCible' => $versServiceId,
                    'organisme' => $organisme,
                    'donneesFusionnees' => 0,
                ]);
            if (!$fusionService instanceof TFusionnerServices) {
                $fusionService = new TFusionnerServices();
                $fusionService->setIdServiceSource($deServiceId);
                $fusionService->setIdServiceCible($versServiceId);
                $fusionService->setOrganisme($organisme);
                $fusionService->setDateCreation(new DateTime());
                $em = $this->getDoctrine()->getManager();
                $em->persist($fusionService);
                $em->flush();
            }
        } catch (Exception $e) {
            $logger->error($e->getMessage().' trace '.$e->getTraceAsString());
            throw new ApiProblemDbException($e->getMessage().' trace '.$e->getTraceAsString());
        }
    }
    /**
     * @param $organisme
     *
     * @throws Exception
     */
    public function validateOrganisme($organisme)
    {
        if (empty($organisme)) {
            throw new ApiProblemInvalidArgumentException('organisme manquant');
        }
        $orgObj = $this->getDoctrine()->getRepository(Organisme::class)->findOneBy(['acronyme' => $organisme]);
        if (!($orgObj instanceof Organisme)) {
            throw new ApiProblemInvalidArgumentException('Aucun organisme avec acronyme '.$organisme);
        }
    }
    /**
     * @param $service
     * @param $organisme
     * @param $msg
     *
     * @throws Exception
     */
    public function validateService($service, $organisme, $msg)
    {
        if (empty($service)) {
            throw new ApiProblemInvalidArgumentException($msg . 'manquant');
        }
        $serviceObj = $this->getDoctrine()
            ->getRepository(Service::class)
            ->findOneBy(['acronymeOrg' => $organisme, 'id' => $service]);
        if (!$serviceObj instanceof Service) {
            throw new ApiProblemInvalidArgumentException('Aucun service avec id ' . $service . ' acronyme '.$organisme);
        }
    }
    /**
     * @param $mode
     * @param $statut
     * @param string $msg
     *
     * @return string
     */
    public function getRetour($mode, $statut, $msg = '')
    {
        $message = null;
        if ('json' == $mode) {
            $response = json_encode(['mpe' => ['reponse' => ['statutReponse' => $statut, 'message' => $msg]]], JSON_THROW_ON_ERROR);
        } else {
            $message = '';
            if ($msg) {
                $message = '<message>'.$msg.'</message>';
            } else {
                $message = '<message>Le déplacement de service sera traité de manière asynchrone.</message>';
            }
            $response = <<<XML
<mpe xmlns="http://www.atexo.com/epm/xml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.atexo.com/epm/xml/ATEXO%20-%20LOCAL%20TRUST%20MPE%20-%20WebServices.xsd">
<reponse statutReponse="$statut">
$message
</reponse>
</mpe>
XML;
        }

        return $response;
    }
}
