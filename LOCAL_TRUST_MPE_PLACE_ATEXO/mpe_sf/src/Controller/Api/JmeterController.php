<?php

namespace App\Controller\Api;

use App\Service\AgentTechniqueTokenService;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\AtexoAgent;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\AtexoUtil;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use App\Exception\ApiProblemInvalidArgumentException;
use App\Exception\ApiProblemNotFoundException;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class JmeterController.
 **/
#[Route(path: '/api/{version}/monitoring/jmeter', name: 'atexo_api_jmeter', requirements: ['version' => '^(?:(?!v2).)+$'])]
class JmeterController extends AbstractWebserviceController
{
    public function __construct(
        private EntityManagerInterface $em,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private ApiSerializer $apiSerializer,
        private AtexoAgent $agent,
        private AtexoEntreprise $atexoEntreprise,
        private AtexoEtablissement $atexoEtablissement,
        private AtexoInscrit $atexoInscrit,
        private OrganismeService $organismeService,
        private ModificationContratService $modificationContratService,
        private AtexoUtil $atexoUtil,
    ) {
        parent::__construct(
            $em,
            $agentTechniqueTokenService,
            $apiSerializer,
            $agent,
            $atexoEntreprise,
            $atexoEtablissement,
            $atexoInscrit,
            $organismeService,
            $modificationContratService,
            $atexoUtil
        );
    }

    /**
     * @Operation(
     *     tags={"/jmeter"},
     *     summary="Récupération du scénario JMeter",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     * @return Response
     */
    #[Route(path: '/{scenario}', name: 'atexo_api_jmeter_get', methods: ['GET'])]
    public function getAction(Request $request)
    {
        $scenario = $request->get('scenario');
        if (empty($scenario)) {
            throw new ApiProblemInvalidArgumentException();
        }
        try {
            return $this->createApiResponse(
                [],
                'xml',
                'api/jmeter/'.$scenario.'.jmx.xml'
            );
        } catch (Exception) {
            throw new ApiProblemNotFoundException();
        }
    }
    /**
     * @Operation(
     *     tags={"/jmeter"},
     *     summary="Récupération du hash du scénario JMeter",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     * @return Response
     */
    #[Route(path: '/hash/{scenario}.{format}', name: 'atexo_api_jmeter_hash', defaults: ['format' => 'xml'], methods: ['GET'])]
    public function hashAction(Request $request)
    {
        $scenario = $request->get('scenario');
        if (empty($scenario)) {
            throw new ApiProblemInvalidArgumentException();
        }
        try {
            $xml = $this->renderView('api/jmeter/'.$scenario.'.jmx.xml');
            $hash = hash('sha256', $xml);

            return $this->createApiResponse(
                ['hash' => $hash],
                $request->get('format', 'xml'),
                'api/jmeter/hash.xml.twig'
            );
        } catch (Exception) {
            throw new ApiProblemNotFoundException();
        }
    }
}
