<?php

namespace App\Controller\Api;

use App\Service\AgentTechniqueTokenService;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\AtexoAgent;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoUtil;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use App\Entity\Inscrit;
use App\Exception\ApiProblemValidationException;
use App\Service\Api\StatistiqueService;
use App\Service\AtexoInscrit;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class InscritController.
 **/
#[Route(path: '/api/{version}/inscrits.{format}', name: 'atexo_api_inscrits', requirements: ['version' => '^(?:(?!v2).)+$'])]
class InscritController extends AbstractWebserviceController
{
    public function __construct(
        private EntityManagerInterface $em,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private ApiSerializer $apiSerializer,
        private AtexoAgent $agent,
        private AtexoEntreprise $atexoEntreprise,
        private AtexoEtablissement $atexoEtablissement,
        private AtexoInscrit $atexoInscrit,
        private OrganismeService $organismeService,
        private ModificationContratService $modificationContratService,
        private AtexoUtil $atexoUtil,
        private StatistiqueService $statistiqueService
    ) {
        parent::__construct(
            $em,
            $agentTechniqueTokenService,
            $apiSerializer,
            $agent,
            $atexoEntreprise,
            $atexoEtablissement,
            $atexoInscrit,
            $organismeService,
            $modificationContratService,
            $atexoUtil
        );
    }

    /**
     * @Operation(
     *     tags={"/inscrits"},
     *     summary="Lister les inscrits",
     *     @SWG\Parameter(
     *         name="limit",
     *         in="body",
     *         description="Nombre d'éléments par page",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="body",
     *         description="Numéro de page - Entier supérieur à 0",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="lastUpdateTimestamp",
     *         in="body",
     *         description="Dernière modification - Timestamp",
     *         required=false,
     *         @SWG\Schema(type="timestamp")
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * @Rest\Get()
     * @Rest\View()
     */
    public function listAction(Request $request): Response
    {
        $response = $this->getWithPaginate($request, Inscrit::class);

        return $response->send();
    }
    /**
     * @Operation(
     *     tags={"/inscrits"},
     *     summary="Créer un inscrit",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * POST Route annotation.
     *
     * @Rest\View()
     * @Rest\Post()
     *
     * @return Response
     * @throws Exception
     */
    public function postInscritAction(Request $request, LoggerInterface $logger)
    {
        $echangesInterfaces = null;

        $statistique = $this->statistiqueService;
        $ticket = AbstractWebserviceController::getToken($request);
        $tiersInformations = $this->getTiersInformations($ticket);
        $params = [
            'interface' => (isset($tiersInformations)) ? $tiersInformations->getLogin() : null,
            'service' => 'Inscrit',
            'webserviceBatch' => 'WS-Inscrit-création',
            'status' => StatistiqueService::INIT,
        ];

        $statistique->create($request, $params);

        $result = $this->createEntity($request, AtexoInscrit::class, $logger, $echangesInterfaces, 'inscrit');

        if (is_array($result)) {
            $params['status'] = StatistiqueService::ECHEC;
            $statistique->create($request, $params);
            throw new ApiProblemValidationException($result);
        }

        $params['status'] = StatistiqueService::SUCCESS;
        $statistique->create($request, $params);

        return $result;
    }
    /**
     * @Operation(
     *     tags={"/inscrits"},
     *     summary="Mettre à jour un inscrit",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * PUT Route annotation.
     * @Rest\View()
     * @Rest\Put()
     *
     * @return Response
     * @throws Exception
     */
    public function putInscritAction(Request $request, LoggerInterface $logger)
    {
        $echangesInterfaces = null;

        $statistique = $this->statistiqueService;
        $ticket = AbstractWebserviceController::getToken($request);
        $tiersInformations = $this->getTiersInformations($ticket);
        $params = [
            'interface' => (isset($tiersInformations)) ? $tiersInformations->getLogin() : null,
            'service' => 'Inscrit',
            'webserviceBatch' => 'WS-Inscrit-modification',
            'status' => StatistiqueService::INIT,
        ];

        $statistique->create($request, $params);

        $result = $this->updateEntity($request, AtexoInscrit::class, $logger, $echangesInterfaces, 'inscrit');

        if (is_array($result)) {
            $params['status'] = StatistiqueService::ECHEC;
            $statistique->create($request, $params);
            throw new ApiProblemValidationException($result);
        }
        $params['status'] = StatistiqueService::SUCCESS;
        $statistique->create($request, $params);

        return $result;
    }
    /**
     * @Operation(
     *     tags={"/inscrits"},
     *     summary="WS : Désactive un inscrit",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * Delete Route annotation.
     * @Rest\View()
     * @Rest\Delete()
     *
     * @return Response
     * @throws Exception
     */
    public function deleteIncrit(Request $request, LoggerInterface $logger)
    {
        $echangesInterfaces = null;

        $statistique = $this->statistiqueService;
        $ticket = AbstractWebserviceController::getToken($request);
        $tiersInformations = $this->getTiersInformations($ticket);
        $params = [
            'interface' => (isset($tiersInformations)) ? $tiersInformations->getLogin() : null,
            'service' => 'Inscrit',
            'webserviceBatch' => 'WS-Inscrit-delete',
            'status' => StatistiqueService::INIT,
        ];

        $statistique->create($request, $params);

        $result = $this->deleteInscrit(
            $request,
            AtexoInscrit::class,
            $logger,
            $echangesInterfaces,
            'inscrit'
        );

        if (is_array($result)) {
            $params['status'] = StatistiqueService::ECHEC;
            $statistique->create($request, $params);
            throw new ApiProblemValidationException($result);
        }
        $params['status'] = StatistiqueService::SUCCESS;
        $statistique->create($request, $params);

        return $result;
    }
}
