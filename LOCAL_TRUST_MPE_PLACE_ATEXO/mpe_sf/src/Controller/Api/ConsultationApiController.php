<?php

namespace App\Controller\Api;

use DateTime;
use App\Controller\AtexoController;
use App\Entity\Consultation;
use App\Entity\Offre;
use App\Entity\Telechargement;
use App\Entity\TelechargementAnonyme;
use App\Entity\WS\AgentTechniqueToken;
use App\Repository\WS\AgentTechniqueTokenRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ConsultationApiController.
 **/
#[Route(path: '/api/consultation', name: 'atexo_api')]
class ConsultationApiController extends AtexoController
{
    /**
     * @Rest\View()
     * @Rest\Get("/get_consultation")
     *
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    public function getConsultationAction(Request $request): JsonResponse|View
    {
        $token = $request->query->get('token_permission');

        if (!isset($token)) {
            return $this->consultationUnauthorized();
        }

        /** @var AgentTechniqueTokenRepository $agentTechniqueTokenRepository */
        $agentTechniqueTokenRepository = $this->getDoctrine()->getRepository(AgentTechniqueToken::class);
        $check = $agentTechniqueTokenRepository->checkToken($token);

        if (false != $check) {
            $datemin = new DateTime($_GET['datemin']);
            $datemin->setTime('00', '00', '00');
            $datemax = new DateTime($_GET['datemax']);
            $datemax->setTime('00', '00', '00');
            $formatted_result = [];

            $formatted_result['datemin'] = $datemin->format('D-d-m');
            $formatted_result['monthmin'] = $datemin->format('M-Y');
            $formatted_result['yearmin'] = $datemin->format('Y');
            $formatted_result['dateminInfo'] = $datemin->format('Y-m-d H:i:s');
            $formatted_result['datemaxInfo'] = $datemax->format('Y-m-d H:i:s');

            $formatted_result['publish_consultation'] = number_format($this->getDoctrine()
                ->getRepository(Consultation::class)
                ->findPublishedConsultations($datemin, $datemax), 0, ',', ' ');

            $formatted_result['publish_lot'] = number_format($this->getDoctrine()
                ->getRepository(Consultation::class)
                ->findPublishedLots($datemin, $datemax), 0, ',', ' ');

            $formatted_result['public_consultations_online'] = number_format($this->getDoctrine()
                ->getRepository(Consultation::class)
                ->findOnlinePublicConsultations($datemin, $datemax), 0, ',', ' ');

            $formatted_result['restraint_consultations_online'] = number_format($this->getDoctrine()
                ->getRepository(Consultation::class)
                ->findOnlineRestrictedConsultations($datemin, $datemax), 0, ',', ' ');

            $formatted_result['close_consultations'] = number_format($this->getDoctrine()
                ->getRepository(Consultation::class)
                ->findClosedConsultations($datemin, $datemax), 0, ',', ' ');

            $formatted_result['download_dce_authentificated'] = number_format($this->getDoctrine()
                ->getRepository(Telechargement::class)
                ->findDownloadDceAuthenticated($datemin, $datemax), 0, ',', ' ');

            $formatted_result['weight_download_authentificated'] = number_format(round($this->getDoctrine()
                ->getRepository(Telechargement::class)
                ->findWeightDownloadAuthenticated($datemin, $datemax)), 0, ',', ' ');

            $formatted_result['download_dce_anonymous'] = number_format($this->getDoctrine()
                ->getRepository(TelechargementAnonyme::class)
                ->findAnonymousDownloadedDce($datemin, $datemax), 0, ',', ' ');

            $formatted_result['weight_download_anonymous'] = number_format(round($this->getDoctrine()
                ->getRepository(TelechargementAnonyme::class)
                ->findAnonymousDownloadedWeight($datemin, $datemax)), 0, ',', ' ');

            $formatted_result['reply_elec'] = number_format($this->getDoctrine()
                ->getRepository(Offre::class)
                ->findDigitalAnswer($datemin, $datemax), 0, ',', ' ');

            $formatted_result['weight_reply'] = number_format(round($this->getDoctrine()
                ->getRepository(Offre::class)
                ->findDigitalWeight($datemin, $datemax)), 0, ',', ' ');

            return new JsonResponse($formatted_result);
        }

        return $this->consultationUnauthorized();
    }
    /**
     * @return View
     */
    protected function consultationUnauthorized()
    {
        return View::create(['message' => 'Unauthorized Token'], Response::HTTP_UNAUTHORIZED);
    }
}
