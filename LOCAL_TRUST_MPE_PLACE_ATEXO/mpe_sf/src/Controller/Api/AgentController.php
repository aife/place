<?php

/**
 * Created by PhpStorm.
 * User: atexo
 * Date: 06/12/2018
 * Time: 11:34.
 */

namespace App\Controller\Api;

use App\Service\Api\Serializer as ApiSerializer;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\AtexoUtil;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use App\Service\Api\Serializer;
use App\Entity\Agent;
use App\Exception\ApiProblemInternalException;
use App\Exception\ApiProblemInvalidSsoException;
use App\Exception\ApiProblemInvalidTokenException;
use App\Exception\ApiProblemUnauthorizedException;
use App\Exception\ApiProblemValidationException;
use App\Service\AgentService;
use App\Service\AgentTechniqueTokenService;
use App\Service\Api\StatistiqueService;
use App\Service\AtexoAgent;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AgentController.
 **/

#[Route(
    path: '/api/{version}/agents.{format}',
    name: 'atexo_api_agents',
    requirements: ["version" => "^(?:(?!v2).)+$"],
    defaults: ['format' => 'xml']
)]
class AgentController extends AbstractWebserviceController
{
    final const STATUT_KEY = 'status';

    public function __construct(
        private EntityManagerInterface $em,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private ApiSerializer $apiSerializer,
        private AtexoAgent $agent,
        private AtexoEntreprise $atexoEntreprise,
        private AtexoEtablissement $atexoEtablissement,
        private AtexoInscrit $atexoInscrit,
        private OrganismeService $organismeService,
        private ModificationContratService $modificationContratService,
        private AtexoUtil $atexoUtil,
        private LoggerInterface $webservicesLogger,
        private AgentService $agentService,
        private Serializer $serializer,
        private StatistiqueService $statistiqueService,
        private ParameterBagInterface $parameterBag,
    ) {
        parent::__construct(
            $em,
            $agentTechniqueTokenService,
            $apiSerializer,
            $agent,
            $atexoEntreprise,
            $atexoEtablissement,
            $atexoInscrit,
            $organismeService,
            $modificationContratService,
            $atexoUtil
        );
    }

    /**
     * @Operation(
     *     tags={"/agents"},
     *     summary="Lister les agents",
     *     @SWG\Parameter(
     *         name="limit",
     *         in="body",
     *         description="Nombre d'éléments par page",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="body",
     *         description="Numéro de page - Entier supérieur à 0",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="lastUpdateTimestamp",
     *         in="body",
     *         description="Dernière modification - Timestamp",
     *         required=false,
     *         @SWG\Schema(type="timestamp")
     *     ),
     *     @SWG\Parameter(
     *         name="module",
     *         in="body",
     *         description="nom du module appelant (exec,redac, autres)",
     *         required=false,
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     * @Rest\Get()
     */
    public function listAction(Request $request): Response
    {
        try {
            $agent = $this->agentTechniqueTokenService->getAgentFromToken($request->get('ticket'));
            $orgServicesAllowed = $this->agentTechniqueTokenService->getAllOrganismesServicesAllowed($agent);
            $response = $this->getWithPaginate($request, Agent::class, $orgServicesAllowed);
            return $response->send();
        } catch (Exception $e) {
            throw $e;
        }
    }
    /**
     * @Operation(
     *     tags={"/agents/habilitations"},
     *     summary="Récupérer les habilitations des agents",
     *     @SWG\Parameter(
     *         name="sso",
     *         in="body",
     *         description="Sso d'identification de l'agent",
     *         required=false,
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     * @Rest\Get("/habilitations")
     *
     * @return Response
     */
    public function habilitationsAction(Request $request)
    {
        $format = $request->get('format', 'xml');
        $idAgent = $this->agentService->getIdAgentFromIdSso($request->get('sso'), $this->parameterBag->get('SERVICE_METIER_EXEC'));
        if (!$idAgent) {
            throw new ApiProblemInvalidSsoException();
        }
        $this->agentService->destroySso($request->get('sso'), $this->parameterBag->get('SERVICE_METIER_EXEC'));
        $agent = $this->agentService->getAgent($idAgent);
        if (!$agent) {
            throw new ApiProblemInvalidSsoException();
        }

        if (empty($this->agentTechniqueTokenService)) {
            throw new ApiProblemInternalException();
        }

        $token = $request->get('token') ?? $request->get('ticket') ?? null;
        if (null === $token) {
            throw new ApiProblemInvalidTokenException("Erreur lors de la récupération du token!");
        }

        /** @var Agent $currentAgent */
        $currentAgent = $this->agentTechniqueTokenService->getAgentFromToken($token);
        $nodeFlux = [];
        $nodeFlux['acronymeOrganisme'] = !empty($agent->getOrganisme()) ? $agent->getOrganisme()->getAcronyme() : null;
        $serviceKey = 'service';
        $nodeFlux[$serviceKey]['id'] = !empty($currentAgent->getService()) ? $currentAgent->getService()->getId() : null;
        if (!$this->agentTechniqueTokenService->isGrantedFromAgentWs($currentAgent, $nodeFlux)) {
            throw new ApiProblemUnauthorizedException($this->getMessageErrorForAccessDenied($nodeFlux['acronymeOrganisme'], $nodeFlux[$serviceKey]['id']));
        }
        $serializerService = $this->serializer;
        $response = $serializerService->simpleSerialize($agent, 'agentSSO', 'ws-habilitation', $format);

        return new Response($response, Response::HTTP_OK, [
            'Content-Type' => 'application/' . $format,
        ]);
    }
    /**
     * @Operation(
     *     tags={"/agents"},
     *     summary="Créer un agent",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * POST Route annotation.
     * @Rest\View()
     * @Rest\Post()
     *
     * @return Response
     * @throws Exception
     */
    public function postAgentAction(Request $request)
    {
        $echangesInterfaces = null;

        $statistique = $this->statistiqueService;
        $ticket = AbstractWebserviceController::getToken($request);
        $tiersInformations = $this->getTiersInformations($ticket);
        $params = $this->setParams($tiersInformations, 'WS-Agent-création');

        $statistique->create($request, $params);

        $result = $this->createEntity(
            $request,
            AtexoAgent::class,
            $this->webservicesLogger,
            $echangesInterfaces,
            self::AGENT
        );

        if (is_array($result)) {
            $params[self::STATUT_KEY] = StatistiqueService::ECHEC;
            $statistique->create($request, $params);
            throw new ApiProblemValidationException($result);
        }
        $params[self::STATUT_KEY] = StatistiqueService::SUCCESS;
        $statistique->create($request, $params);

        return $result;
    }
    /**
     * @Operation(
     *     tags={"/agents"},
     *     summary="Mettre à jour un agent",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * POST Route annotation.
     * @Rest\View()
     * @Rest\Put()
     *
     * @return Response
     * @throws Exception
     */
    public function putAgentAction(Request $request)
    {
        $echangesInterfaces = null;

        $statistique = $this->statistiqueService;
        $ticket = AbstractWebserviceController::getToken($request);
        $tiersInformations = $this->getTiersInformations($ticket);
        $params = $this->setParams($tiersInformations, 'WS-Agent-modification');

        $statistique->create($request, $params);

        $result = $this->updateEntity(
            $request,
            AtexoAgent::class,
            $this->webservicesLogger,
            $echangesInterfaces,
            self::AGENT
        );
        if (is_array($result)) {
            $params[self::STATUT_KEY] = StatistiqueService::ECHEC;
            $statistique->create($request, $params);
            throw new ApiProblemValidationException($result);
        }
        $params[self::STATUT_KEY] = StatistiqueService::SUCCESS;
        $statistique->create($request, $params);

        return $result;
    }
    /**
     * @Operation(
     *     tags={"/agents"},
     *     summary="supprimer un agent",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * @Rest\View()
     * @Rest\Delete()
     *
     * @return Response
     * @throws Exception
     */
    public function deleteAgentAction(Request $request)
    {
        $echangesInterfaces = null;

        $params = self::statistique($request);

        $result = $this->deleteEntity(
            $request,
            AtexoAgent::class,
            $this->webservicesLogger,
            $echangesInterfaces,
            self::AGENT
        );

        $params[self::STATUT_KEY] = StatistiqueService::SUCCESS;

        self::statistique($request, $params);

        return $result;
    }
    public function statistique($request, $params = false)
    {
        try {
            $statistique = $this->statistiqueService;
            if (false === $params) {
                $ticket = self::getToken($request);
                $tiersInformations = $this->getTiersInformations($ticket);
                $params = $this->setParams($tiersInformations, 'WS-Agent-modification');
            }
            $statistique->create($request, $params);

            return $params;
        } catch (Exception $e) {
            throw $e;
        }
    }
    /**
     * @param $tiersInformations
     * @param $batch
     *
     * @return array
     */
    public function setParams($tiersInformations, $batch)
    {
        $params = [
            'interface' => (isset($tiersInformations)) ? $tiersInformations->getLogin() : null,
            'service' => 'Agent',
            'webserviceBatch' => $batch,
            self::STATUT_KEY => StatistiqueService::INIT,
        ];

        return $params;
    }
}
