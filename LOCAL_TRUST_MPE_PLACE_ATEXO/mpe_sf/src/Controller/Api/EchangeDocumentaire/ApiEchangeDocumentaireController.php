<?php

namespace App\Controller\Api\EchangeDocumentaire;

use App\Service\AgentTechniqueTokenService;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\Api\StatistiqueService;
use App\Service\AtexoAgent;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use Doctrine\DBAL\Exception;
use App\Controller\Api\AbstractWebserviceController;
use App\Entity\EchangeDocumentaire\EchangeDoc;
use App\Entity\EchangeDocumentaire\EchangeDocBlob;
use App\Entity\EchangeDocumentaire\EchangeDocHistorique;
use App\Exception\ApiProblemInternalException;
use App\Exception\ApiProblemInvalidArgumentException;
use App\Serializer\EchangeDocumentaire\EchangeDocNormalizer;
use App\Service\AtexoUtil;
use App\Service\EchangeDocumentaire\EchangeDocumentaireService;
use App\Service\EspaceDocumentaire\Authorization;
use App\Utils\Encryption;
use App\Utils\Filesystem\MountManager;
use App\Utils\Zip;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Swagger\Annotations as SWG;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class EchangeDocumentaireController.
 **/
#[Route(
    path: '/api/{version}/echange-documentaire',
    name: 'api_echange_documentaire_',
    requirements: ["version" => "^(?:(?!v2).)+$"]
)]
class ApiEchangeDocumentaireController extends AbstractWebserviceController
{
    private const ID_ECHANGE = 'idEchange';
    private const TICKET = 'ticket';
    private const CODE_STATUT = 'codeStatut';
    private const CHEMIN = 'chemin';
    private const CODE_STATUT_ARRAY = ['ENVOYE', 'ECHEC', 'REJETE', 'RECEPTIONNE', 'A_ENVOYER'];
    /**
     * ApiEchangeDocumentaireController constructor.
     *
     * @param EntityManager $entityManager
     * @param Session       $session
     */
    public function __construct(
        private Authorization $perimetreAgent,
        private EntityManagerInterface $entityManager,
        private SessionInterface $session,
        private Encryption $encryption,
        private TranslatorInterface $translator,
        private Zip $zip,
        private AtexoUtil $atexoUtil,
        private LoggerInterface $logger,
        private MountManager $mountManager,
        private EchangeDocumentaireService $documentaireService,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private ApiSerializer $apiSerializer,
        private AtexoAgent $agent,
        private AtexoEntreprise $atexoEntreprise,
        private AtexoEtablissement $atexoEtablissement,
        private AtexoInscrit $atexoInscrit,
        private OrganismeService $organismeService,
        private ModificationContratService $modificationContratService,
        private StatistiqueService $statistiqueService,
        private EchangeDocNormalizer $echangeDocNormalizer
    ) {
        parent::__construct(
            $entityManager,
            $agentTechniqueTokenService,
            $apiSerializer,
            $agent,
            $atexoEntreprise,
            $atexoEtablissement,
            $atexoInscrit,
            $organismeService,
            $modificationContratService,
            $atexoUtil
        );
    }
    /**
     * @Operation(
     *     tags={"/echange-documentaire"},
     *     summary="",
     *     @SWG\Parameter(
     *         name="idConsultation",
     *         in="body",
     *         description="Identifiant technique de la consultation de rattachement de l'échange.",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="body",
     *         description="Identifiant technique de l'échange.",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="organisme",
     *         in="body",
     *         description="Organisme de la consultation de rattachement de l'échange.",
     *         required=false,
     *         @SWG\Schema(type="timestamp")
     *     ),
     *     @SWG\Parameter(
     *         name="statut",
     *         in="body",
     *         description="Statut de l'échange.",
     *         required=false,
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Parameter(
     *         name="dateModificationMin",
     *         in="body",
     *         description="Date minimum de dernière modification de l'échange (EX: YYYY-MM-DD)",
     *         required=false,
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Parameter(
     *         name="dateModificationMax",
     *         in="body",
     *         description="Date maximum de dernière modification de l'échange (EX: YYYY-MM-DD)",
     *         required=false,
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Parameter(
     *         name="codeApplication",
     *         in="body",
     *         description="Code de l'application progiciel de l'échange.",
     *         required=false,
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Parameter(
     *         name="codeApplicationClient",
     *         in="body",
     *         description="Code de l'application client de l'échange.",
     *         required=false,
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="body",
     *         description="Nombre d'éléments par page",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="body",
     *         description="Numéro de page - Entier supérieur à 0",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     **/
    #[Route(path: '/echange.{format}', name: 'getEchanges', defaults: ['format' => 'json'], methods: ['GET'])]
    public function getEchangesAction(Request $request): Response
    {
        $paramTabs = $request->query->all();
        if (array_key_exists(self::TICKET, $paramTabs)) {
            unset($paramTabs[self::TICKET]);
        }
        $response = $this->getWithPaginate($request, EchangeDoc::class, [], $paramTabs);
        return $response->send();
    }
    /**
     * @Operation(
     *     tags={"/echange-documentaire"},
     *     summary="Ce Web service permet de lister le contenu de l'échange avec son historique.",
     *     @SWG\Parameter(
     *         name="idEchange",
     *         in="body",
     *         description="Identifiant de l'échange.",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     **/
    #[Route(path: '/echange-historique.{format}', name: 'getEchangeHistorique', defaults: ['format' => 'json'], methods: ['GET'])]
    public function getEchangeHistoriqueAction(Request $request): Response
    {
        //check if echangeId exists in WS
        if (!$request->query->has(self::ID_ECHANGE)) {
            throw new ApiProblemInvalidArgumentException('Le paramètre idEchange est obligatoire.');
        }
        $echangeId = $request->query->get(self::ID_ECHANGE);
        //check if echangeId exists in Database
        $echange = $this->getDoctrine()->getRepository(EchangeDoc::class)->find($echangeId);
        if (empty($echange)) {
            throw new ApiProblemInvalidArgumentException("L'échange Id renseigné n'existe pas.");
        }
        $historique = $this->getDoctrine()->getRepository(EchangeDocHistorique::class)
            ->getHistorique($echangeId);
        if (empty($historique)) {
            throw new ApiProblemInvalidArgumentException('Pas de historique pour cet échange.');
        }
        $response = $this->getWithPaginate($request, $this->echangeDocNormalizer->adresserDonneesHistorique($historique));
        return $response->send();
    }

    /**
     * @Operation(
     *     tags={"/echange-documentaire"},
     *     summary="Ce Web service a comme rôle de retourner un document, de manière unitaire, mis à disposition dans le cadre d'un échange.",
     *     @SWG\Parameter(
     *         name="id",
     *         in="body",
     *         description="Identifiant de l'échange documentaire blob.",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="idEchange",
     *         in="body",
     *         description="Identifiant de l'échange documentaire auquel le document est joint.",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     * @param Request $request
     *
     * @return StreamedResponse
     * @throws ApiProblemInvalidArgumentException|\Exception
     */
    #[Route(path: '/document.{format}', name: 'getDocument', defaults: ['format' => 'json'], methods: ['GET'])]
    public function getDocumentAction(Request $request): StreamedResponse
    {
        //check if id & echangeId exists in WS
        if (null === $request->get('id') || null === $request->get(self::ID_ECHANGE)) {
            throw new ApiProblemInvalidArgumentException('Les paramètres id et idEchange sont obligatoires.');
        }
        $id = $request->get('id');
        $echangeId = $request->get(self::ID_ECHANGE);
        //check if documentId & echangeId exists in DB
        $checkDocumentIdAndEchangeId = $this->entityManager->getRepository(EchangeDocBlob::class)
            ->findOneBy(['id' => $id, 'echangeDoc' => $echangeId]);
        if (empty($checkDocumentIdAndEchangeId)) {
            throw new ApiProblemInvalidArgumentException('Ce couple id/idEchange n\'existe pas.');
        }
        try {
            return $this->documentaireService->getFile($request, $id, $echangeId);
        } catch (Exception $e) {
            throw new ApiProblemInvalidArgumentException('Un probléme est survenu.');
        }
    }

    /**
     * @Operation(
     *     tags={"/echange-documentaire"},
     *     summary="Ce Web service a comme rôle de pouvoir mettre à jour le statut d'un échange.",
     *     @SWG\Parameter(
     *         name="idEchange",
     *         in="body",
     *         description="Identifiant de l'échange.",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="codeStatut",
     *         in="body",
     *         description="Code du statut à dans lequel l'échange doit passer.",
     *         required=false,
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Parameter(
     *         name="messageFonctionnel",
     *         in="body",
     *         description="Dans le Body: Message fonctionnel qui sera affiché dans l'IHM du détail de l'échange. (Longeur maximum = 500 caractéres)",
     *         required=false,
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Parameter(
     *         name="messageTechnique",
     *         in="body",
     *         description="Dans le Body: Message technique pour troubleshooting et analyse. (Longeur maximum = 1000 caractéres)",
     *         required=false,
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     **/
    #[Route(path: '/update-statut-echange.{format}', name: 'updateStatutEchange', defaults: ['format' => 'json'], methods: ['PUT'])]
    public function updateStatutEchangeAction(Request $request): Response
    {
        //check if idEchange & codeStatut exists in WS
        if ('' === $request->get(self::ID_ECHANGE) || '' === $request->get(self::CODE_STATUT)) {
            throw new ApiProblemInvalidArgumentException('Les paramètres idEchange et codeStatut sont obligatoires.');
        }
        //check if echangeId exists in Database
        $echangeId = $request->get(self::ID_ECHANGE);
        $echange = $this->getDoctrine()->getRepository(EchangeDoc::class)->find($echangeId);
        if (!($echange instanceof EchangeDoc)) {
            throw new ApiProblemInvalidArgumentException("L'échange Id renseigné n'existe pas.");
        }
        //check if codeStatut In [ENVOYE, ECHEC, REJETE, RECEPTIONNE, A_ENVOYER]
        $codeStatut = strtoupper($request->get(self::CODE_STATUT));
        if (!in_array($codeStatut, self::CODE_STATUT_ARRAY)) {
            throw new ApiProblemInvalidArgumentException('codeStatut : la valeur fournie ne correspond à aucun statut existant.');
        }
        //check if codeStatut est le même dans l'echange
        if ($codeStatut === $echange->getStatut()) {
            throw new ApiProblemInvalidArgumentException("codeStatut : la valeur fournie correspond au statut actuel de l'échange.");
        }
        try {
            $messageFonctionnel = $request->request->get('messageFonctionnel');
            $messageTechnique = $request->request->get('messageTechnique');

            $agent = $this->getCurrentAgent($request);
            $this->documentaireService->changeStatut(
                $echange,
                $agent,
                $codeStatut,
                $messageFonctionnel,
                $messageTechnique
            );
        } catch (\Exception) {
            throw new ApiProblemInvalidArgumentException("Une erreur est survenue lors de la mise à jour de l'échange.");
        }
        $response = $this->getWithPaginate($request, [
            'message' => 'La mise à jour du statut de l\'échange a été fait avec succés.',
        ]);
        return $response->send();
    }

    /**
     * @param $request
     *
     * @return mixed
     */
    private function getCurrentAgent($request)
    {
        $agentTechniqueTokenService = $this->agentTechniqueTokenService;
        if (empty($agentTechniqueTokenService)) {
            throw new ApiProblemInternalException();
        }

        return $agentTechniqueTokenService->getAgentFromToken($request->get(self::TICKET));
    }
}
