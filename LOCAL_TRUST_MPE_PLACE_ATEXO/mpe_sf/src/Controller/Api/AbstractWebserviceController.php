<?php

namespace App\Controller\Api;

use App\Service\AgentTechniqueTokenService;
use App\Service\AtexoEchangeChorus;
use App\Service\AtexoUtil;
use Exception;
use App\Service\AtexoAgent;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\OrganismeService;
use App\Service\ModificationContrat\ModificationContratService;
use ReflectionClass;
use App\Entity\Agent;
use App\Entity\WS\AgentTechniqueToken;
use App\Entity\WS\WebService;
use App\Exception\ApiProblemDbException;
use App\Exception\ApiProblemInternalException;
use App\Exception\ApiProblemInvalidArgumentException;
use App\Exception\ApiProblemInvalidTokenException;
use App\Exception\ApiProblemUnauthorizedException;
use App\Exception\ApiProblemValidationException;
use App\Exception\ApiProblemValidationSchemaException;
use App\Exception\DataNotFoundException;
use App\Serializer\AgentNormalizer;
use App\Serializer\EntrepriseNormalizer;
use App\Serializer\EchangeChorusNormalizer;
use App\Serializer\EtablissementNormalizer;
use App\Serializer\InscritNormalizer;
use App\Serializer\ServiceNormalizer;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\AtexoEntreprise;
use DateTime;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class AbstractWebserviceController.
 */
abstract class AbstractWebserviceController extends AbstractController
{
    final public const AGENT = 'agent';
    final public const CONTENT_TYPE_KEY = 'Content-Type';
    final public const FLUX_MPE = "</mpe>\n";
    private ?AtexoEchangeChorus $echangeChorus;

    public const SERVICE_PROPERTY_NAME = [
        AtexoAgent::class => 'agent',
        AtexoEntreprise::class => 'atexoEntreprise',
        AtexoEtablissement::class => 'atexoEtablissement',
        AtexoInscrit::class => 'atexoInscrit',
        OrganismeService::class => 'organismeService',
        ModificationContratService::class => 'modificationContratService',
        AtexoEchangeChorus::class => 'echangeChorus',
    ];

    /**
     * @var Agent
     */
    private $currentAgentTechnique;

    /**
     * @var WebService
     */
    private $currentWebservice;

    public function __construct(
        private EntityManagerInterface $em,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private ApiSerializer $serializerService,
        private AtexoAgent $agent,
        private AtexoEntreprise $atexoEntreprise,
        private AtexoEtablissement $atexoEtablissement,
        private AtexoInscrit $atexoInscrit,
        private OrganismeService $organismeService,
        private ModificationContratService $modificationContratService,
        private AtexoUtil $atexoUtil
    ) {
    }

    protected bool $needsHabilitation = true;

    /**
     * @return mixed|null
     */
    public function getFormat(Request $request)
    {
        return $request->get('format');
    }

    /**
     * @param $format
     *
     * @return string
     */
    public function getErrorFlux($format)
    {
        return 'Flux ' . $format . ' manquant';
    }

    /**
     * @param $nodeFlux
     *
     * @return string
     */
    public function getInfoNodeFlux($nodeFlux)
    {
        return 'flux : ' . var_export($nodeFlux, true);
    }

    /**
     * @param $result
     *
     * @return string
     */
    public function getInfoResult($result)
    {
        return 'result : ' . $result;
    }

    /**
     * @param $format
     *
     * @return string
     */
    public function getContentType($format)
    {
        return 'application/' . $format;
    }

    /**
     * @param $request
     *
     * @return mixed
     */
    public function getRequestToken(Request $request)
    {
        return $request->get('ticket');
    }

    /**
     * @param $nodeFlux
     *
     * @return string
     */
    private function getErrorNodeFlux($nodeFlux)
    {
        return 'flux : ' . print_r($nodeFlux, true);
    }

    /**
     * @return AtexoEchangeChorus|null
     */
    public function getEchangeChorus(): ?AtexoEchangeChorus
    {
        return $this->echangeChorus;
    }

    /**
     * @param AtexoEchangeChorus|null $echangeChorus
     * @return AbstractWebserviceController
     */
    public function setEchangeChorus(?AtexoEchangeChorus $echangeChorus): AbstractWebserviceController
    {
        $this->echangeChorus = $echangeChorus;

        return $this;
    }

    /**
     * @param $entity
     * @param array $orgServicesAllowed
     * @param array $tabParams
     */
    public function getWithPaginate(
        Request $request,
        $entity,
        $orgServicesAllowed = [],
        $tabParams = []
    ): Response|StreamedResponse {
        $format = $this->getFormat($request);
        $idEntityToSearch = $request->get('id', null);
        $numeroPage = $request->get('page', 1);
        if (!is_numeric($numeroPage) || $numeroPage < 1) {
            $numeroPage = 1;
        }
        $nombreElementsParPage = $request->get('limit', 100);
        if (!is_numeric($nombreElementsParPage) || $nombreElementsParPage < 1) {
            $nombreElementsParPage = 100;
        }
        $timestamp = $request->get('lastUpdateTimestamp');
        if (!is_numeric($timestamp) || $timestamp < 1) {
            $timestamp = 1;
        }
        if ('json' != $format) {
            $format = 'xml';
        }

        $module = $request->get('module', false);


        $tabParams['statut'] =  $request->get('statut', false);
        $tabParams['disableFiltering'] =  $request->get('disableFiltering', false);

        $response = new StreamedResponse();
        $response->headers->set(self::CONTENT_TYPE_KEY, $this->getContentType($format));
        $dateModification = new DateTime();
        $dateModification->setTimestamp($timestamp);
        $serializerService = $this->serializerService;
        if (is_array($entity)) {
            $entityClass = $entity;
            $is_array = true;
        } else {
            $entityClass = $this->getEntityManager()->getRepository($entity);
            $is_array = false;
        }

        $response->setCallback(function () use (
            $format,
            $numeroPage,
            $nombreElementsParPage,
            $dateModification,
            $entityClass,
            $serializerService,
            $module,
            $is_array,
            $orgServicesAllowed,
            $tabParams,
            $idEntityToSearch
        ) {
            $serializerService->constructResponseWS(
                $format,
                $numeroPage,
                $nombreElementsParPage,
                $dateModification,
                $entityClass,
                $module,
                $is_array,
                $orgServicesAllowed,
                $tabParams,
                $idEntityToSearch
            );
        });

        return $response;
    }

    /**
     * @param $mode
     * @param $numeroPage
     * @param $nombreElementsParPage
     * @param $dateModification
     * @param $nodeClass
     */
    public function constructResponseWS(
        $mode,
        $numeroPage,
        $nombreElementsParPage,
        $dateModification,
        $nodeClass,
        $is_array
    ) {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new DateTimeNormalizer(), new ObjectNormalizer($classMetadataFactory)];
        $serializer = new Serializer($normalizers, $encoders);

        $this->initHeadContentResponse($mode, $nodeClass);

        $this->constructContentResponse(
            $mode,
            $serializer,
            $numeroPage,
            $nombreElementsParPage,
            $dateModification,
            $nodeClass
        );

        $this->initFooterContentResponse($mode, $nodeClass);
    }

    /**
     * @param $mode
     * @param $nodeClass
     */
    public function initHeadContentResponse($mode, $nodeClass)
    {
        if ('xml' == $mode) {
            echo "<?xml version=\"1.0\"?>\n";
            echo "<mpe>\n";
            echo '<reponse statutReponse="OK">';
            echo '<' . $nodeClass->headWs . ">\n";
        } else {
            echo '{"' . $nodeClass->headWs . '":[';
        }
    }

    /**
     * @param $mode
     * @param $serializer
     * @param $numeroPage
     * @param $nombreElementsParPage
     * @param $dateModification
     */
    public function constructContentResponse(
        $mode,
        $serializer,
        $numeroPage,
        $nombreElementsParPage,
        $dateModification,
        $nodeClass
    ) {
        $count = 1;
        foreach (
            $nodeClass->findAllByParamWs(
                $nombreElementsParPage,
                $numeroPage,
                $dateModification,
                $mode
            ) as $node
        ) {
            if (1 != $count && 'json' == $mode) {
                echo ',';
            }
            echo $nodeClass->getNode($node, $serializer, $mode) . "\n";

            if (0 === $count % 5 && 1 !== $count) {
                ob_flush();
                flush();
            }
            ++$count;
        }
    }

    /**
     * @param $mode
     * @param $nodeClass
     */
    public function initFooterContentResponse($mode, $nodeClass)
    {
        if ('xml' == $mode) {
            echo '</' . $nodeClass->headWs . ">\n";
            echo "</reponse>\n";
            echo self::FLUX_MPE;
        } else {
            echo ']}';
        }
    }

    /**
     * @param $serviceName
     * @param $logger
     * @param $entityInterfaces
     *
     * @return Response
     *
     * @throws Exception
     */
    public function createEntity(Request $request, $serviceName, $logger, $echangesInterfaces = null, $entity = '')
    {
        $format = $this->getFormat($request);
        if ('json' != $format) {
            $format = 'xml';
        }

        $flux = $request->getContent();
        if (empty($flux)) {
            throw new ApiProblemInvalidArgumentException($this->getErrorFlux($format));
        }

        if (!$this->validateFlux($flux, $format, $entity)) {
            throw new ApiProblemValidationSchemaException($format);
        }

        $nodeFlux = $this->constructRequestCreate($flux, $format, $serviceName);
        if (self::AGENT == $entity) {
            $agentTechniqueTokenService = $this->agentTechniqueTokenService;
            if (empty($agentTechniqueTokenService)) {
                throw new ApiProblemInternalException();
            }
            if (!$agentTechniqueTokenService->isGrantedFromAgentWs($this->currentAgentTechnique, $nodeFlux)) {
                $acronyme = $nodeFlux['acronymeOrganisme'];
                $organisme = !empty($acronyme) ? $acronyme : null;
                $nodeFluxId = $nodeFlux['service']['id'];
                $service = $nodeFluxId ?? null;
                throw new ApiProblemUnauthorizedException($this->getMessageErrorForAccessDenied($organisme, $service));
            }
        }

        $service = $this->{self::SERVICE_PROPERTY_NAME[$serviceName]};
        $logger->info($this->getErrorNodeFlux($nodeFlux));
        $messagesErreur = $service->validateInfo($nodeFlux);

        if (empty($messagesErreur)) {
            $node = $service->createEntity($nodeFlux);
            $result = $this->constructResponseCreate($format, $service, $node, $logger);
            $logger->info($this->getInfoResult($result));
            $response = new Response($result, Response::HTTP_CREATED);
            $response->headers->set(self::CONTENT_TYPE_KEY, $this->getContentType($format));

            return $response;
        } else {
            return $this->json($messagesErreur, Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param $flux
     * @param $format
     * @param $serviceName
     *
     * @return mixed|null
     */
    public function constructRequestCreate($flux, $format, $serviceName)
    {
        $entityTags = [
            AtexoAgent::class => self::AGENT,
            AtexoEntreprise::class => 'entreprise',
            AtexoEtablissement::class => 'etablissement',
            AtexoInscrit::class => 'inscrit',
            OrganismeService::class => 'organisme',
            ModificationContratService::class => 'modification',
            \App\Service\AtexoEchangeChorus::class => 'echangeChorus',
        ];

        if (!array_key_exists($serviceName, $entityTags)) {
            throw new ApiProblemInvalidArgumentException('Le service' . $serviceName . 'n\'est pas défini');
        }

        $entity = null;
        if ('xml' == $format) {
            libxml_use_internal_errors(true);
            $xml = simplexml_load_string($flux);
            if (!$xml) {
                throw new ApiProblemInvalidArgumentException(
                    'Problème de sérialisation: le contenu envoyé n\'est pas en format XML.'
                );
            }

            $json = json_encode($xml->envoi->{$entityTags[$serviceName]}, JSON_THROW_ON_ERROR);
            $json = str_replace('{}', '""', $json);
            $entity = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
        } else {
            $entity = json_decode($flux, true, 512, JSON_THROW_ON_ERROR);

            if (empty($entity)) {
                throw new ApiProblemInvalidArgumentException(
                    'Problème de sérialisation: le contenu envoyé n\'est pas en format JSON.'
                );
            }
        }

        $serviceObject = $this->{self::SERVICE_PROPERTY_NAME[$serviceName]};

        return isset($entity[AtexoEntreprise::ADRESSE]) && is_countable($entity[AtexoEntreprise::ADRESSE])
                && (method_exists($serviceObject, 'explodeDataAdress')) ?
                    $serviceObject->explodeDataAdress($entity) : $entity ;
    }

    /**
     * @param $format
     * @param $service
     * @param $node
     *
     * @return string
     *
     * @throws Exception
     */
    public function constructResponseCreate($format, $service, $node, $logger)
    {
        try {
            $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
            $encoders = [new XmlEncoder(), new JsonEncoder()];

            $normalizers = [
                new DateTimeNormalizer(),
                new ServiceNormalizer(new ObjectNormalizer($classMetadataFactory)),
                new AgentNormalizer(
                    new ObjectNormalizer($classMetadataFactory),
                    new ServiceNormalizer(new ObjectNormalizer($classMetadataFactory))
                ),
                new EntrepriseNormalizer(
                    new ObjectNormalizer($classMetadataFactory),
                    new EtablissementNormalizer(new ObjectNormalizer($classMetadataFactory))
                ),
                new EchangeChorusNormalizer(
                    new ObjectNormalizer($classMetadataFactory),
                    $this->getDoctrine()->getManager(),
                    $logger
                ),
                new EtablissementNormalizer(new ObjectNormalizer($classMetadataFactory)),
                new InscritNormalizer(new ObjectNormalizer($classMetadataFactory)),
                new ObjectNormalizer($classMetadataFactory),
            ];
            $serializer = new Serializer($normalizers, $encoders);
            $result = '';
            if ('xml' == $format) {
                $result .= "<?xml version=\"1.0\"?>\n";
                $result .= $this->getNodeMpe();
                $result .= '<reponse statutReponse="OK">';
            } else {
                $nodeRf = new ReflectionClass($node::class);
                $entityName = $nodeRf->getShortName();
                $result .= '{"mpe":{"reponse":{"statutReponse":"OK","' . $entityName . '":';
            }

            $result .= $service->getNode($node, $serializer, $format);

            if ('xml' == $format) {
                $result .= "</reponse>\n";
                $result .= self::FLUX_MPE;
            } else {
                $result .= '}}}';
            }

            return $result;
        } catch (Exception) {
            throw new ApiProblemInvalidArgumentException('Format incorrect');
        }
    }

    /**
     * @return string
     */
    public function getNodeMpe()
    {
        return <<<EOD
            <mpe xmlns="http://www.atexo.com/epm/xml" 
               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
               xsi:schemaLocation="http://www.atexo.com/epm/xml/ATEXO%20-%20LOCAL%20TRUST%20MPE%20-%20WebServices.xsd">
        EOD;
    }

    /**
     * @param $serviceName
     * @param $logger
     * @param $echangesInterfaces
     * @param string $entity
     *
     * @return Response
     *
     * @throws Exception
     */
    public function updateEntity(Request $request, $serviceName, $logger, $echangesInterfaces = null, $entity = '')
    {
        try {
            $format = $this->getFormat($request);
            if ('json' != $format) {
                $format = 'xml';
            }

            $flux = $request->getContent();

            if (empty($flux)) {
                throw new ApiProblemInvalidArgumentException($this->getErrorFlux($format));
            }

            if (!$this->validateFlux($flux, $format, $entity)) {
                throw new ApiProblemValidationSchemaException($format);
            }

            $nodeFlux = $this->constructRequestCreate($flux, $format, $serviceName);
            if (self::AGENT == $entity) {
                $agentTechniqueTokenService = $this->agentTechniqueTokenService;
                if (empty($agentTechniqueTokenService)) {
                    throw new ApiProblemInternalException();
                }
                if (!$agentTechniqueTokenService->isGrantedFromAgentWs($this->currentAgentTechnique, $nodeFlux)) {
                    $acronyme = $nodeFlux['acronymeOrganisme'];
                    $organisme = !empty($acronyme) ? $acronyme : null;
                    $nodeFluxId = $nodeFlux['service']['id'];
                    $service = $nodeFluxId ?? null;
                    throw new ApiProblemUnauthorizedException(
                        $this->getMessageErrorForAccessDenied($organisme, $service)
                    );
                }
            }
            $service = $this->{self::SERVICE_PROPERTY_NAME[$serviceName]};
            $logger->info($this->getErrorNodeFlux($nodeFlux));
            $messagesErreur = $service->validateInfo($nodeFlux, true);

            if (empty($messagesErreur)) {
                $node = $service->updateEntity($nodeFlux);
                $result = $this->constructResponseCreate($format, $service, $node, $logger);

                $logger->info($this->getInfoResult($result));
                $response = new Response();
                $response->headers->set(self::CONTENT_TYPE_KEY, $this->getContentType($format));
                $response->setContent($result);

                return $response;
            } else {
                return $messagesErreur;
            }
        } catch (ApiProblemUnauthorizedException | DataNotFoundException $e) {
            throw $e;
        } catch (Exception $e) {
            $logger->error('Erreur  updateEntity ' . $serviceName . ' : ' . $e->getMessage() . ' ' .
                $e->getTraceAsString());
            throw $e;
        }
    }

    /**
     * Permet la récupération des erreurs d'un formulaire.
     *
     * @return array
     */
    protected function getErrorsFromForm(FormInterface $form)
    {
        $errors = [];
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }

        return $errors;
    }

    /**
     * Vérification de l'auhentification via bearer.
     *
     * @return bool
     */
    protected function checkToken(Request $request)
    {
        $token = $this->extractToken($request->headers->get('Authorization'));
        //Si le token n'est pas présent
        if (
            !$token || !$this->getEm()->getRepository(AgentTechniqueToken::class)
                ->checkToken($token)
        ) {
            throw new ApiProblemInvalidTokenException();
        }

        return true;
    }

    public function getEm()
    {
        if (isset($this->em)) {
            return $this->em;
        }

        return $this->getDoctrine()->getManager();
    }

    /**
     * Extraction du token situé dans le header Authorization.
     *
     * @param $authorizationHeader
     *
     * @return bool
     */
    private function extractToken($authorizationHeader)
    {
        $headerParts = explode(' ', (string) $authorizationHeader);
        if (!(2 === count($headerParts) && 0 === strcasecmp($headerParts[0], 'Bearer'))) {
            return false;
        }

        return $headerParts[1];
    }

    /**
     * @param $mode
     * @param $service
     * @param $node
     *
     * @return Response
     *
     * @throws Exception
     */
    public function constructResponseTableauBord($mode, $flux)
    {
        try {
            $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
            $encoders = [new XmlEncoder(), new JsonEncoder()];
            $normalizers = [new DateTimeNormalizer(), new ObjectNormalizer($classMetadataFactory)];
            $serializer = new Serializer($normalizers, $encoders);
            $result = '';
            $fluxEncode = $serializer->encode($flux, $mode);
            if ('json' != $mode) {
                $fluxEncode = str_replace("\n", '', $fluxEncode);
                $fluxEncode = str_replace(
                    '?><response>',
                    "?>\n<mpe>\n<reponse statutReponse=\"OK\">",
                    $fluxEncode
                );
                $fluxEncode = str_replace('</response>', '</reponse>', $fluxEncode);
            }
            $result .= $fluxEncode;
            if ('json' != $mode) {
                $result .= self::FLUX_MPE;
            }

            $response = new Response();
            $response->headers->set(self::CONTENT_TYPE_KEY, 'application/' . $mode);
            $response->setContent($result);

            return $response;
        } catch (Exception) {
            throw new Exception('Format incorrect', 412);
        }
    }

    /**
     * Création de la réponse en fonction de la data reçu et du format.
     *
     * @param $data
     * @param $format
     * @param $file
     *
     * @return Response
     */
    protected function createApiResponse($data, $format, $file)
    {
        if ('xml' == $format) {
            $data = $this->renderView($file, $data);
            $formatHeader = 'application/xml';
        } else {
            $data = json_encode($data, JSON_THROW_ON_ERROR);
            $formatHeader = 'application/json';
        }

        return new Response($data, Response::HTTP_OK, [
            self::CONTENT_TYPE_KEY => $formatHeader,
        ]);
    }

    /**
     * Création de la réponse en fonction de la data reçu et du format.
     *
     * @param $flux
     * @param $format
     * @param string $entity
     *
     * @return mixed
     */
    protected function validateFlux($flux, $format, $entity = '')
    {
        if ($this->getParameter('ENABLE_XSD_SCHEMA_VALIDATION')) {
            $atexoUtil = $this->atexoUtil;
            if ('json' === $format) {
                $arrayJson = json_decode($flux, true, 512, JSON_THROW_ON_ERROR);
                if ($entity) {
                    $array = ['envoi' => [$entity => $arrayJson]];
                } else {
                    $array = ['envoi' => $arrayJson];
                }
                $xmlFlux = $atexoUtil->arrayToXml($array, '<mpe/>');
            } else {
                $xmlFlux = $flux;
            }
            $xmlFlux = str_replace(
                '<mpe>',
                '<mpe xmlns="http://www.atexo.com/epm/xml"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:schemaLocation="http://www.atexo.com/epm/xml xsd/mpe.xsd">',
                $xmlFlux
            );

            return $atexoUtil->isXmlValide($xmlFlux, $this->getParameter('PATH_FILE_XSD_WS'));
        }

        return true;
    }

    public function isNeedsHabilitation(): bool
    {
        return $this->needsHabilitation;
    }

    public function setNeedsHabilitation(bool $needsHabilitation)
    {
        $this->needsHabilitation = $needsHabilitation;
    }

    /**
     * Get Tiers Information from SsoTiers and Tiers.
     *
     * @param $ticket
     *
     * @return mixed
     */
    public function getTiersInformations($ticket)
    {
        $em = $this->em;
        $agentTechniqueTokenRepository = $em->getRepository(AgentTechniqueToken::class);
        $tiers = null;
        if (!empty($ticket)) {
            $agentTechniqueToken = $agentTechniqueTokenRepository->findOneBy(['token' => $ticket]);
            if (!empty($agentTechniqueToken)) {
                $idAgent = $agentTechniqueToken->getAgent()->getId();
                $agentRepository = $em->getRepository(Agent::class);
                $tiers = $agentRepository->find($idAgent);
            }
        }

        return $tiers;
    }

    /**
     * @return \Doctrine\Common\Persistence\ManagerRegistry
     */
    public function getEntityManager()
    {
        if (!empty($this->em)) {
            return $this->em;
        }

        return $this->getDoctrine();
    }

    /**
     * @param $request
     *
     * @return mixed|null
     */
    public static function getToken($request)
    {
        $ticketKey = 'ticket';
        $token = $request->query->get($ticketKey);
        if (empty($token)) {
            $token = $request->get($ticketKey);
        }
        if (empty($token)) {
            $token = $request->get('token');
        }
        if (empty($token)) {
            $headerParts = explode(' ', (string) $request->headers->get('Authorization'));
            if (!(2 === count($headerParts) && 0 === strcasecmp($headerParts[0], 'Bearer'))) {
                $token = null;
            } else {
                $token = $headerParts[1];
            }
        }

        return $token;
    }

    public function deleteEntity(Request $request, $serviceName, $logger, $echangesInterfaces, $entity = '')
    {
        try {
            $format = $this->getFormat($request);

            $flux = $request->getContent();

            if (empty($flux)) {
                throw new ApiProblemInvalidArgumentException($this->getErrorFlux($format));
            }

            if (!$this->validateFlux($flux, $format, $entity)) {
                throw new ApiProblemValidationSchemaException($format);
            }

            $nodeFlux = $this->constructRequestCreate($flux, $format, $serviceName);
            $service = $this->{self::SERVICE_PROPERTY_NAME[$serviceName]};
            $logger->info($this->getInfoNodeFlux($nodeFlux));
            $messagesErreur = $service->validateInfo($nodeFlux, true);

            if (empty($messagesErreur)) {
                $node = $service->deleteEntity($nodeFlux);
                $result = $this->constructResponseCreate($format, $service, $node, $logger);
                $logger->info($this->getInfoResult($result));
                $response = new Response();
                $response->headers->set(self::CONTENT_TYPE_KEY, $this->getContentType($format));
                $response->setContent($result);

                return $response;
            } else {
                $dbExecption = new ApiProblemValidationException($messagesErreur);
                $logger->error('Erreur  updateEntity ' . $serviceName . ' : ' . $dbExecption->getMessage() . ' ' .
                    $dbExecption->getTraceAsString());

                throw $dbExecption;
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $serviceName
     * @param $logger
     * @param $echangesInterfaces
     * @param string $entity
     *
     * @return Response
     *
     * @throws Exception
     */
    public function deleteInscrit(Request $request, $serviceName, $logger, $echangesInterfaces, $entity = '')
    {
        $messagesErreur = [];
        try {
            $format = $this->getFormat($request);

            $flux = $request->getContent();

            if (empty($flux)) {
                throw new ApiProblemInvalidArgumentException($this->getErrorFlux($format));
            }

            if (!$this->validateFlux($flux, $format, $entity)) {
                throw new ApiProblemValidationSchemaException($format);
            }

            $nodeFlux = $this->constructRequestCreate($flux, $format, $serviceName);
            $service = $this->{self::SERVICE_PROPERTY_NAME[$serviceName]};
            $logger->info($this->getInfoNodeFlux($nodeFlux));

            $node = $service->deleteEntity($nodeFlux);
            $result = $this->constructResponseCreate($format, $service, $node, $logger);
            $logger->info($this->getInfoResult($result));
            $response = new Response();
            $response->headers->set(self::CONTENT_TYPE_KEY, $this->getContentType($format));
            $response->setContent($result);

            return $response;
        } catch (Exception) {
            $messagesErreur[] = "Impossible de désactiver l'inscrit.";
            $dbExecption = new ApiProblemValidationException($messagesErreur);
            $logger->error('Erreur  deleteEntity ' . $serviceName . ' : ' . $dbExecption->getMessage() . ' ' .
                $dbExecption->getTraceAsString());

            throw $dbExecption;
        }
    }

    /**
     * @param $currentAgentTechnique
     */
    public function setCurrentAgentTechnique($currentAgentTechnique)
    {
        $this->currentAgentTechnique = $currentAgentTechnique;
    }

    /**
     * @param $currentWebservice
     */
    public function setCurrentWebservice($currentWebservice)
    {
        $this->currentWebservice = $currentWebservice;
    }

    /**
     * @return string
     */
    public function getMessageErrorForAccessDenied($organisme = null, $service = null)
    {
        $nomApplication = $this->currentAgentTechnique->getLogin();
        $nomWs = $this->currentWebservice->getNomWs();
        $message = "L’application $nomApplication n’est pas habilité à consommer le WS  $nomWs";
        if (!empty($organisme)) {
            $message = "L'application $nomApplication ne peut pas utiliser le WS $nomWs sur l’organisme $organisme";
            if (!is_null($service)) {
                $message .= " / service $service.";
            }
        }

        return $message;
    }
}
