<?php

namespace App\Controller\Api;

use App\Service\AgentTechniqueTokenService;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\AtexoAgent;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\AtexoUtil;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use Exception;
use App\Exception\ApiProblemDbException;
use App\Exception\ApiProblemInvalidArgumentException;
use App\Exception\ApiProblemXsdException;
use App\Service\Api\StatistiqueService;
use App\Service\StatistiqueService\Api;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SupervisionInterfaceController.
 */
class SupervisionInterfacesController extends AbstractWebserviceController
{
    public function __construct(
        private EntityManagerInterface $em,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private ApiSerializer $apiSerializer,
        private AtexoAgent $agent,
        private AtexoEntreprise $atexoEntreprise,
        private AtexoEtablissement $atexoEtablissement,
        private AtexoInscrit $atexoInscrit,
        private OrganismeService $organismeService,
        private ModificationContratService $modificationContratService,
        private AtexoUtil $atexoUtil,
        private LoggerInterface $logger,
        private StatistiqueService $statistiqueService
    ) {
        parent::__construct(
            $em,
            $agentTechniqueTokenService,
            $apiSerializer,
            $agent,
            $atexoEntreprise,
            $atexoEtablissement,
            $atexoInscrit,
            $organismeService,
            $modificationContratService,
            $atexoUtil
        );
    }

    /**
     * @var StatistiqueService
     * @Operation(
     *     tags={"/statistiques"},
     *     summary="Liste les statistiques",
     *     @SWG\Parameter(
     *         name="createdDate",
     *         in="body",
     *         description="Statistiques du jour pour cette date",
     *         required=false,
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * POST Route annotation.
     * @Rest\View()
     * @Rest\GET("/api/{version}/statistiques/interfaces.{format}", requirements={"version": "^(?:(?!v2).)+$"})
     *
     * @return JsonResponse
     *
     * @throws Exception
     */
    public function recoveryIndicatorsAction(Request $request)
    {
        try {
            $content = $this->statistiqueService->get($request);
            $response = new Response();
            $response->setContent($content);

            return $response;
        } catch (ApiProblemXsdException $e) {
            throw $e;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            throw new ApiProblemDbException($e->getMessage());
        }
    }

    /**
     * @var StatistiqueService
     * @Operation(
     *     tags={"/statistiques"},
     *     summary="Créer ou updater une statistique",
     *     @SWG\Parameter(
     *         name="interface",
     *         in="formData",
     *         description="Nom de l'interface (ex: Plis)",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="service",
     *         in="formData",
     *         description="nome du service (ex: WS-pli-get)",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="webserviceBatch",
     *         in="formData",
     *         description="Nom du webservice ou du batch",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="statut",
     *         in="formData",
     *         description="Statut du service (ok si l'action mpe s'est bien effectuée)",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="poids",
     *         in="formData",
     *         description="poids en octets",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     * POST Route annotation.
     * @Rest\View()
     * @Rest\POST("/api/{version}/statistiques/interfaces.{format}", requirements={"version": "^(?:(?!v2).)+$"})
     *
     * @return Response
     *
     * @throws Exception
     */
    public function postStatistiqueAction(Request $request)
    {
        $mandatories = ['interface', 'service', 'webserviceBatch', 'statut'];
        foreach ($mandatories as $mandatory) {
            if (empty($request->get($mandatory))) {
                $msg = "Le paramètre `$mandatory` est manquant !";
                $this->logger->error($msg);
                throw new ApiProblemInvalidArgumentException($msg);
            }
        }
        try {
            $content = $this->statistiqueService->create($request);

            $this->statistiqueService->validate($content, $request);

            $response = new Response();
            $response->setContent($content);

            return $response;
        } catch (ApiProblemXsdException $e) {
            throw $e;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            throw new ApiProblemDbException($e->getMessage());
        }
    }
}
