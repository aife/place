<?php

namespace App\Controller\Api;

use App\Service\AgentTechniqueTokenService;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\AtexoAgent;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\AtexoUtil;
use App\Service\ModificationContrat\ModificationContratService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use App\Entity\Organisme;
use App\Service\Api\DeserializerService;
use App\Service\Api\Serializer;
use App\Service\OrganismeService;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OrganismeController.
 **/
#[Route(
    path: '/api/{version}/organismes.{format}',
    name: 'atexo_api_organismes',
    requirements: ['version' => '^(?:(?!v2).)+$']
)]
class OrganismeController extends AbstractWebserviceController
{
    public function __construct(
        private EntityManagerInterface $em,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private ApiSerializer $apiSerializer,
        private AtexoAgent $agent,
        private AtexoEntreprise $atexoEntreprise,
        private AtexoEtablissement $atexoEtablissement,
        private AtexoInscrit $atexoInscrit,
        private OrganismeService $organismeService,
        private ModificationContratService $modificationContratService,
        private AtexoUtil $atexoUtil,
        private DeserializerService $deserializerService,
        private LoggerInterface $logger,
        private Serializer $serializerService
    ) {
        parent::__construct(
            $em,
            $agentTechniqueTokenService,
            $apiSerializer,
            $agent,
            $atexoEntreprise,
            $atexoEtablissement,
            $atexoInscrit,
            $organismeService,
            $modificationContratService,
            $atexoUtil
        );
    }

    /**
     * @Operation(
     *     tags={"/organismes"},
     *     summary="Lister les organismes",
     *     @SWG\Parameter(
     *         name="limit",
     *         in="body",
     *         description="Nombre d'éléments par page",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="body",
     *         description="Numéro de page - Entier supérieur à 0",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="module",
     *         in="body",
     *         description="nom du module appelant (exec,redac, autres)",
     *         required=false,
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * @Rest\Get()
     * @Rest\View()
     */
    public function listAction(Request $request): Response
    {
        $response = $this->getWithPaginate($request, Organisme::class);

        return $response->send();
    }
    /**
     * @Operation(
     *     tags={"/organismes"},
     *     summary="Créer un organisme",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     * @Rest\View()
     * @Rest\Post()
     */
    public function newAction(Request $request)
    {
        try {
            return $this->dispatch($request);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            throw $e;
        }
    }
    /**
     * @Operation(
     *     tags={"/organismes"},
     *     summary="Modifie un organisme",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     * @Rest\View()
     * @Rest\Put()
     */
    public function editAction(Request $request)
    {
        try {
            return $this->dispatch($request);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            throw $e;
        }
    }
    private function dispatch(Request $request)
    {
        $organisme = null;
        try {
            $method = $request->getMethod();
            $format = $request->get('format');
            $this->validateFlux($request->getContent(), $format, 'organisme');
            $cleanedData = $this->constructRequestCreate($request->getContent(), $format, OrganismeService::class);

            $organismeReceived = $this->deserializerService->deserializeOrganisme($cleanedData, $format);

            if ('POST' == $method) {
                $organisme = $this->organismeService->create($organismeReceived);
            } elseif ('PUT' == $method) {
                $organisme = $this->organismeService->update($organismeReceived);
            }

            $organismeToSend = $this->serializerService
                ->simpleSerialize($organisme, 'organisme', 'webservice', $format);

            return $this->send($organismeToSend, $format);
        } catch (Exception $e) {
            throw $e;
        }
    }
    private function send($response, $format)
    {
        return new Response($response, Response::HTTP_OK, [
            'Content-Type' => 'application/' . $format,
        ]);
    }
}
