<?php

namespace App\Controller\Api;

use App\Service\AgentTechniqueTokenService;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\AtexoAgent;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\AtexoUtil;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use App\Entity\Entreprise;
use App\Exception\ApiProblemValidationException;
use App\Service\Api\StatistiqueService;
use App\Service\AtexoEntreprise;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class EntrepriseController.
 **/
#[Route(
    path: '/api/{version}/entreprises.{format}',
    name: 'atexo_api_entreprises',
    requirements: ["version" => "^(?:(?!v2).)+$"],
    defaults: ['format' => 'xml']
)]
class EntrepriseController extends AbstractWebserviceController
{
    public function __construct(
        private EntityManagerInterface $em,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private ApiSerializer $apiSerializer,
        private AtexoAgent $agent,
        private AtexoEntreprise $atexoEntreprise,
        private AtexoEtablissement $atexoEtablissement,
        private AtexoInscrit $atexoInscrit,
        private OrganismeService $organismeService,
        private ModificationContratService $modificationContratService,
        private AtexoUtil $atexoUtil,
        private StatistiqueService $statistiqueService
    ) {
        parent::__construct(
            $em,
            $agentTechniqueTokenService,
            $apiSerializer,
            $agent,
            $atexoEntreprise,
            $atexoEtablissement,
            $atexoInscrit,
            $organismeService,
            $modificationContratService,
            $atexoUtil
        );
    }

    /**
     * @Operation(
     *     tags={"/entreprises"},
     *     summary="Lister les entreprises",
     *     @SWG\Parameter(
     *         name="limit",
     *         in="body",
     *         description="Nombre d'éléments par page",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="body",
     *         description="Numéro de page - Entier supérieur à 0",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="lastUpdateTimestamp",
     *         in="body",
     *         description="Dernière modification - Timestamp",
     *         required=false,
     *         @SWG\Schema(type="timestamp")
     *     ),
     *     @SWG\Parameter(
     *         name="module",
     *         in="body",
     *         description="nom du module appelant (exec,redac, autres)",
     *         required=false,
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     * @Rest\Get()
     * @Rest\View()
     */
    public function listAction(Request $request): Response
    {
        $response = $this->getWithPaginate($request, Entreprise::class);

        return $response->send();
    }

    /**
     * @Operation(
     *     tags={"/entreprises"},
     *     summary="Créer une entreprise",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     * POST Route annotation.
     * @Rest\View()
     * @Rest\Post()
     *
     * @return Response
     * @throws Exception
     */
    public function postEntrepriseAction(Request $request, LoggerInterface $logger)
    {
        $echangesInterfaces = null;

        $statistique = $this->statistiqueService;
        $ticket = AbstractWebserviceController::getToken($request);
        $tiersInformations = $this->getTiersInformations($ticket);
        $params = [
            'interface' => (isset($tiersInformations)) ? $tiersInformations->getLogin() : null,
            'service' => 'Entreprise',
            'webserviceBatch' => 'WS-Entreprise-création',
            'status' => StatistiqueService::INIT,
        ];

        $statistique->create($request, $params);
        $result = $this->createEntity($request, AtexoEntreprise::class, $logger, $echangesInterfaces, 'entreprise');

        if (is_array($result)) {
            $params['status'] = StatistiqueService::ECHEC;
            $statistique->create($request, $params);
            throw new ApiProblemValidationException($result);
        }

        $params['status'] = StatistiqueService::SUCCESS;
        $statistique->create($request, $params);

        return $result;
    }

    /**
     * @Operation(
     *     tags={"/entreprises"},
     *     summary="Mettre à jour une entreprise",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     * PUT Route annotation.
     * @Rest\View()
     * @Rest\Put()
     *
     * @return Response
     * @throws Exception
     */
    public function putEntrepriseAction(Request $request, LoggerInterface $logger)
    {
        $echangesInterfaces = null;

        $statistique = $this->statistiqueService;
        $ticket = AbstractWebserviceController::getToken($request);
        $tiersInformations = $this->getTiersInformations($ticket);
        $params = [
            'interface' => (isset($tiersInformations)) ? $tiersInformations->getLogin() : null,
            'service' => 'Entreprise',
            'webserviceBatch' => 'WS-Entreprise-modification',
            'status' => StatistiqueService::INIT,
        ];

        $statistique->create($request, $params);
        $result = $this->updateEntity($request, AtexoEntreprise::class, $logger, $echangesInterfaces, 'entreprise');
        if (is_array($result)) {
            $params['status'] = StatistiqueService::ECHEC;
            $statistique->create($request, $params);
            throw new ApiProblemValidationException($result);
        }
        $params['status'] = StatistiqueService::SUCCESS;
        $statistique->create($request, $params);

        return $result;
    }
}
