<?php

namespace App\Controller\Api\EspaceDocumentaire;

use App\Exception\InvalidArgumentException;
use App\Message\GenerationArchiveDocumentaire;
use App\Service\AgentTechniqueTokenService;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\AtexoAgent;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use Doctrine\DBAL\Exception;
use App\Service\AtexoConfiguration;
use Doctrine\ORM\OptimisticLockException;
use App\Service\AtexoFichierOrganisme;
use App\Controller\Api\AbstractWebserviceController;
use App\Entity\Avis;
use App\Entity\BloborganismeFile;
use App\Entity\Complement;
use App\Entity\Consultation;
use App\Entity\Consultation\AutrePieceConsultation;
use App\Entity\DCE;
use App\Entity\Offre;
use App\Entity\RG;
use App\Entity\TCandidature;
use App\Entity\TDumeContexte;
use App\Entity\TDumeNumero;
use App\Service\AtexoUtil;
use App\Service\EspaceDocumentaire\Authorization;
use App\Service\EspaceDocumentaire\EspaceDocumentaireUtil;
use App\Utils\Encryption;
use App\Utils\Filesystem\MountManager;
use App\Utils\Zip;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Exception;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Swagger\Annotations as SWG;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class EspaceDocumentaireController.
 **/
#[Route(path: '/api/{version}/espace-documentaire', name: 'api_espace_documentaire_')]
class ApiEspaceDocumentaireController extends AbstractWebserviceController
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private ApiSerializer $apiSerializer,
        private AtexoAgent $agent,
        private AtexoEntreprise $atexoEntreprise,
        private AtexoEtablissement $atexoEtablissement,
        private AtexoInscrit $atexoInscrit,
        private OrganismeService $organismeService,
        private ModificationContratService $modificationContratService,
        private Authorization $perimetreAgent,
        private SessionInterface $session,
        private Encryption $encryption,
        private TranslatorInterface $translator,
        private Zip $zip,
        private AtexoUtil $atexoUtil,
        private MountManager $mountManager,
        private LoggerInterface $logger,
        private Security $security,
        private AtexoConfiguration $atexoConfiguration,
        private AtexoFichierOrganisme $atexoFichierOrganisme,
        private ParameterBagInterface $parameterBag
    ) {
        parent::__construct(
            $entityManager,
            $agentTechniqueTokenService,
            $apiSerializer,
            $agent,
            $atexoEntreprise,
            $atexoEtablissement,
            $atexoInscrit,
            $organismeService,
            $modificationContratService,
            $atexoUtil
        );
    }

    /**
     * @Operation(
     *     tags={"/espace-documentaire"},
     *     summary="Service de récupération du dernier RC sur la consultation, ce service n'est disponible que
        s'il y a une session agent ouverte.",
     *     @SWG\Response(
     *              response="200",
     *              description="Returned when successful"
     *          )
     *     )
     *
     * @throws Atexo_Consultation_Exception
     * @throws Exception
     */
    #[Route(path: '/rc/{idConsultation}.{format}', name: 'rc', defaults: ['format' => 'json'], methods: ['GET'])]
    public function getRcAction(Request $request, EspaceDocumentaireUtil $espaceDocumentaireUtil): Response
    {
        $idConsultation = $request->get('idConsultation');
        $auth = $this->perimetreAgent->isAuthorized($idConsultation);
        $reglementConsultation = $this->atexoConfiguration
            ->hasConfigPlateforme('reglementConsultation');
        $message = $this->translator->trans('NON_AUTORISE_RESSOURCE');
        $status = '403';
        $nom = null;
        $poids = null;
        $idHash = null;
        $id = null;
        if (true === $auth && true == $reglementConsultation) {
            $organisme = $this->security->getUser()
                ? $this->security->getUser()->getOrganisme()
                : $this->session->get('contexte_authentification')['organisme'];
            $reglement = $this->entityManager->getRepository(RG::class)
                ->getReglement($idConsultation, $organisme);
            $message = $this->translator->trans('DEFINE_AUCUN_FICHIER_RENSEIGNE');
            $status = 'warning';
            if ($reglement instanceof RG) {
                $status = 'ok';
                $id = $reglement->getRg();
                $idHash = $this->encryption->cryptId($id);
                $nom = $reglement->getNomFichier();
                $poids = $this->mountManager
                    ->getFileSize($id, $organisme, $this->entityManager);
            }
        }
        $extension = $espaceDocumentaireUtil->getFileExtension($nom);
        $array = [
            'type' => 'RC',
            'message' => $message,
            'nom' => $nom,
            'poids' => $poids,
            'id' => $idHash,
            'status' => $status,
            'extension' => $extension,
        ];
        if ($id && $nom) {
            unset($array['message']);
        } else {
            unset($array['nom'], $array['id'], $array['poids']);
        }
        $response = $this->getWithPaginate($request, $array);
        return $response->send();
    }
    /**
     * @Operation(
     *     tags={"/espace-documentaire"},
     *     summary="Service de récupèration des autres pièces sur la consultation, ce service n'est disponible que
        s'il y a une session agent ouverte.",
     *     @SWG\Response(
     *              response="200",
     *              description="Returned when successful"
     *          )
     *     )
     *
     * @throws Atexo_Consultation_Exception
     * @throws Exception
     */
    #[Route(path: '/dce/autres-pieces/{idConsultation}.{format}', name: 'autres_pieces', defaults: ['format' => 'json'], methods: ['GET'])]
    public function getAutresPiecesAction(Request $request, EspaceDocumentaireUtil $espaceDocumentaireUtil): Response
    {
        $idConsultation = $request->get('idConsultation');
        $auth = $this->perimetreAgent->isAuthorized($idConsultation);
        $message = $this->translator->trans('NON_AUTORISE_RESSOURCE');
        $status = 'error';
        $nom = null;
        $poids = null;
        $idHash = null;
        $id = null;
        if (true === $auth) {
            $organisme = $this->security->getUser()
                ? $this->security->getUser()->getOrganisme()
                : $this->session->get('contexte_authentification')['organisme'];
            $complement = $this->entityManager->getRepository(Complement::class)
                ->getLastComplement($idConsultation, $organisme);

            $message = $this->translator->trans('DEFINE_AUCUN_FICHIER_RENSEIGNE');
            $status = 'warning';
            if ((is_countable($complement) ? count($complement) : 0) > 0) {
                $complement = $complement[0];
                $status = 'ok';
                $id = $complement['complement'];
                $idHash = $this->encryption->cryptId($complement['complement']);
                $nom = $complement['nomFichier'];
                $poids = $this->mountManager
                    ->getFileSize($id, $organisme, $this->entityManager);
            }
        }
        $extension = $espaceDocumentaireUtil->getFileExtension($nom);
        $array = [
            'type' => 'AUTRESPIECES',
            'message' => $message,
            'nom' => $nom,
            'poids' => $poids,
            'id' => $idHash,
            'status' => $status,
            'extension' => $extension,
        ];
        if ($id && $nom) {
            unset($array['message']);
        } else {
            unset($array['nom'], $array['id'], $array['poids']);
        }
        $response = $this->getWithPaginate($request, $array);
        return $response->send();
    }
    /**
     * @Operation(
     *     tags={"/espace-documentaire"},
     *     summary="Retourne la liste des plis (des dépots) de la consultation",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     * @throws Atexo_Consultation_Exception
     * @throws Exception
     */
    #[Route(path: '/plis/{idConsultation}.{format}', name: 'plis', defaults: ['format' => 'json'], methods: ['GET'])]
    public function getPlisAction(Request $request, EspaceDocumentaireUtil $espaceDocumentaireUtil): Response
    {
        $result = [];
        $result['type'] = 'DEPOT';
        $result['message'] = $this->translator->trans('NON_AUTORISE_RESSOURCE');
        $idConsultation = $request->get('idConsultation');
        $auth = $this->perimetreAgent->isAuthorized($idConsultation);
        if (true === $auth) {
            $organisme = $this->security->getUser()
                ? $this->security->getUser()->getOrganisme()
                : $this->session->get('contexte_authentification')['organisme'];

            $offres = $this->entityManager->getRepository(Offre::class)
                ->getAllDepotFromConsultationId($idConsultation);

            if ((is_countable($offres) ? count($offres) : 0) > 0) {
                $consultation = $this->entityManager->getRepository(Consultation::class)->find($idConsultation);
                if (empty($consultation)) {
                    throw new NotFoundHttpException('La consultation avec l\'id:' . $idConsultation . 'n\'existe pas');
                }
                foreach ($offres as $key => $offre) {
                    $result['plis'][$key]['entreprise'] = $offre['nomEntrepriseInscrit'];
                    $result['plis'][$key]['pli'] = 'El ' . $offre['numeroReponse'];
                    $result['plis'][$key]['offre'] = $this->encryption->cryptId($offre['id']);
                    $result['plis'][$key]['is_selectable'] = (1 !== $offre['statutOffres']) ? 'false' : 'true';
                    $result['plis'][$key]['counter'] =
                        $espaceDocumentaireUtil->getCounterFilesByIdOffre($consultation, $organisme, $offre['id']);
                }
                unset($result['message']);
            } else {
                $result['message'] = $this->translator->trans('PAS_DE_DEPOT');
            }
        }
        $response = $this->getWithPaginate($request, $result);
        return $response->send();
    }
    /**
     * @Operation(
     *     tags={"/espace-documentaire"},
     *     summary="Service de récupèration du dernier dce uploadé sur la consultation, ce service n'est disponible
        s'il y a une session agent ouverte.",
     *     @SWG\Response(
     *              response="200",
     *              description="Returned when successful"
     *          )
     *     )
     *
     * @throws Atexo_Consultation_Exception
     * @throws Exception
     */
    #[Route(path: '/dce/{idConsultation}.{format}', name: 'dce', defaults: ['format' => 'json'], methods: ['GET'])]
    public function getDceAction(Request $request, EspaceDocumentaireUtil $espaceDocumentaireUtil): Response
    {
        $idConsultation = $request->get('idConsultation');
        $auth = $this->perimetreAgent->isAuthorized($idConsultation);
        $message = $this->translator->trans('NON_AUTORISE_RESSOURCE');
        $status = 'error';
        $nom = $poids = $idHash = $id = null;
        $liste = $arbo = [];
        if (true === $auth) {
            $organisme = $this->security->getUser()
                ? $this->security->getUser()->getOrganisme()
                : $this->session->get('contexte_authentification')['organisme'];
            $dce = $this->entityManager->getRepository(DCE::class)
                ->getDce($idConsultation, $organisme);
            $message = $this->translator->trans('DEFINE_AUCUN_FICHIER_RENSEIGNE');
            $status = 'warning';
            if ($dce instanceof DCE) {
                $status = 'ok';
                $id = $dce->getDce();
                $idHash = $this->encryption->cryptId($id);
                $nom = $dce->getNomDce();
                $mount = $this->mountManager;
                $poids = $mount->getFileSize($id, $organisme, $this->entityManager);
                $r = $mount->getAbsolutePath($id, $organisme, $this->entityManager);
                $index = 0;
                $arbo = $this->zip->getArrayOrderedFilesForZip($r, $index, 'DCE_' . $id);
            }
        }
        $extension = $espaceDocumentaireUtil->getFileExtension($nom);
        $array = [
            'type' => 'DCE',
            'message' => $message,
            'nom' => $nom,
            'poids' => $poids,
            'id' => $idHash,
            'status' => $status,
            'extension' => $extension,
            'listes' => $arbo,
        ];
        if ($id && $nom) {
            unset($array['message']);
        } else {
            unset($array['nom'], $array['id'], $array['poids'], $array['listes']);
        }
        $response = $this->getWithPaginate($request, $array);
        return $response->send();
    }
    /**
     * @Operation(
     *     tags={"/espace-documentaire"},
     *     summary="retourner la liste des plis sur la consultations",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     * @throws Atexo_Consultation_Exception
     * @throws Exception
     */
    #[Route(path: '/dume/dume-acheteur/{idConsultation}.{format}', name: 'dume-acheteur', defaults: ['format' => 'json'], methods: ['GET'])]
    public function getDUMEAcheteurAction(Request $request): Response
    {
        $result = [];
        $result['type'] = 'DUMEACHETEUR';
        $result['message'] = $this->translator->trans('NON_AUTORISE_RESSOURCE');
        $idConsultation = $request->get('idConsultation');
        $auth = $this->perimetreAgent->isAuthorized($idConsultation);
        if (true === $auth) {
            $organisme = $this->security->getUser()
                ? $this->security->getUser()->getOrganisme()
                : $this->session->get('contexte_authentification')['organisme'];

            $contexte = $this->entityManager->getRepository(TDumeContexte::class)->getLastTDumeContext(
                $idConsultation,
                $organisme,
                'demande'
            );

            if ($contexte) {
                $dumeEntreprise = $this->entityManager->getRepository(TDumeNumero::class)->findOneBy(
                    [
                        'idDumeContexte' => $contexte->getId(),
                    ]
                );
                if ($dumeEntreprise instanceof TDumeNumero) {
                    $result['fichiers'][0]['nom'] = $dumeEntreprise->getNumeroDumeNational() . '.pdf';

                    $result['fichiers'][0]['poids'] = $this->mountManager
                        ->getFileSize($dumeEntreprise->getBlobId(), $organisme, $this->entityManager);

                    $result['fichiers'][0]['id'] = $this->encryption->cryptId(
                        $dumeEntreprise->getBlobId()
                    );
                    $result['fichiers'][0]['format'] = 'PDF';
                    $result['fichiers'][0]['extension'] = 'pdf';

                    $result['fichiers'][1]['nom'] = $dumeEntreprise->getNumeroDumeNational() . '.xml';

                    $blodIdDume = 0;
                    $poids = 0;
                    if (!empty($dumeEntreprise->getBlobIdXml())) {
                        $blodIdDume = $dumeEntreprise->getBlobIdXml();
                        $poids = $this->mountManager
                            ->getFileSize($blodIdDume, $organisme, $this->entityManager);
                    }
                    $result['fichiers'][1]['poids'] = $poids;
                    $result['fichiers'][1]['id'] = $this->encryption->cryptId(
                        $blodIdDume
                    );
                    $result['fichiers'][1]['format'] = 'XML';
                    $result['fichiers'][1]['extension'] = 'xml';
                    unset($result['message']);
                } else {
                    $result['message'] = $this->translator->trans('MSG_ERREUR_REFERENCE_DUME_INVALIDE');
                }
            } else {
                $result['message'] = $this->translator->trans('DUME_NON_PUBLIE');
            }
        }

        $response = $this->getWithPaginate($request, $result);

        return $response->send();
    }

    /**
     * @Operation(
     *     tags={"/espace-documentaire"},
     *     summary="retourne les pièces de consultation: dume entreprise et les enveloppes :
         candidatures,offres et anonymats ",
     *     @SWG\Response(
     *              response="200",
     *              description="Returned when successful"
     *          )
     *     )
     *
     * @return void
     * @throws Atexo_Consultation_Exception
     * @throws Exception
     */
    #[Route(path: '/pieces-du-depots/{idOffre}/{idConsultation}.{format}', name: 'pli-dume-enveloppe', defaults: ['format' => 'json'], methods: ['GET'])]
    public function getPiecesConsultationAction(Request $request, EspaceDocumentaireUtil $espaceDocumentaireUtil): Response
    {
        $result = [];
        $idConsultation = $request->get('idConsultation');
        $auth = $this->perimetreAgent->isAuthorized($idConsultation);
        $result['type'] = 'PLI';
        $idOffre = $this->encryption->decryptId($request->get('idOffre'));
        $result['offre'] = $idOffre;
        $result['message'] = $this->translator->trans('NON_AUTORISE_RESSOURCE');
        if (true === $auth) {
            $organisme = $this->security->getUser()
                ? $this->security->getUser()->getOrganisme()
                : $this->session->get('contexte_authentification')['organisme'];
            $alloti = '0';
            $consultation = $this->entityManager->getRepository(Consultation::class)->find($idConsultation);
            if (empty($consultation)) {
                throw new NotFoundHttpException('La consultation avec l\'id:' . $idConsultation . 'n\'existe pas');
            }
            if ($consultation->getAlloti()) {
                $alloti = '1';
            }
            $result['alloti'] = $alloti;

            $contexte = $this->entityManager->getRepository(TCandidature::class)->getIdContexte(
                $idOffre,
                $organisme,
                $idConsultation,
                'dume',
                '99',
                'online',
                'reponse'
            );

            if (isset($contexte) && !empty($contexte[0])) {
                $idContexte = $contexte[0]['id'];

                $dumeEntreprise = $this->entityManager->getRepository(TDumeNumero::class)->findOneBy(
                    [
                        'idDumeContexte' => $idContexte,
                    ]
                );

                $result['listes']['dume_entreprise'][0]['nom'] = $dumeEntreprise->getNumeroDumeNational() . '.pdf';

                $result['listes']['dume_entreprise'][0]['poids'] = $this->mountManager
                    ->getFileSize($dumeEntreprise->getBlobId(), $organisme, $this->entityManager);

                $result['listes']['dume_entreprise'][0]['id_blob'] = $this->encryption->cryptId(
                    $dumeEntreprise->getBlobId()
                );

                $result['listes']['dume_entreprise'][0]['type'] = 'PDF';
                $result['listes']['dume_entreprise'][0]['extension'] = 'pdf';

                $result['listes']['dume_entreprise'][1]['nom'] = $dumeEntreprise->getNumeroDumeNational() . '.xml';

                $poids = 0;
                $blodIdDume = 0;
                if (!empty($dumeEntreprise->getBlobIdXml())) {
                    $blodIdDume = $dumeEntreprise->getBlobIdXml();
                    $poids = $this->mountManager
                        ->getFileSize($blodIdDume, $organisme, $this->entityManager);
                }
                $result['listes']['dume_entreprise'][1]['poids'] = $poids;

                $result['listes']['dume_entreprise'][1]['id_blob'] = $this->encryption->cryptId(
                    $blodIdDume
                );
                $result['listes']['dume_entreprise'][1]['type'] = 'XML';
                $result['listes']['dume_entreprise'][1]['extension'] = 'xml';
            } else {
                $result['message'] = $this->translator->trans('AUCUN_PLIS');
            }

            if ('0' === $alloti) {
                $result = $espaceDocumentaireUtil->getListeEnveloppeNonAlloti($idOffre, $result);
            } else {
                $result = $espaceDocumentaireUtil->getListeEnveloppeAlloti($idOffre, $result, $consultation);
            }
        } else {
            $result['message'] = $this->translator->trans('PAS_DE_DEPOT');
        }

        $response = $this->getWithPaginate($request, $result);

        return $response->send();
    }

    /**
     * @Operation(
     *     tags={"/espace-documentaire"},
     *     summary="Retourne l'ensemble des fichiers joints ayant été ajoutés
        comme avis de publicité au format libre",
     *     @SWG\Response(
     *              response="200",
     *              description="Returned when successful"
     *          )
     *     )
     *
     * @throws Atexo_Consultation_Exception
     * @throws Exception
     */
    #[Route(path: '/publicite/{idConsultation}.{format}', name: 'publicite', defaults: ['format' => 'json'], methods: ['GET'])]
    public function getPiecesDePubliciteAction(Request $request, EspaceDocumentaireUtil $espaceDocumentaireUtil): Response
    {
        $result = [];
        $result['type'] = 'PUBLICITE';
        $result['message'] = $this->translator->trans('NON_AUTORISE_RESSOURCE');
        $idConsultation = $request->get('idConsultation');
        $auth = $this->perimetreAgent->isAuthorized($idConsultation);
        if (true === $auth) {
            $organisme = $this->security->getUser()
                ? $this->security->getUser()->getOrganisme()
                : $this->session->get('contexte_authentification')['organisme'];
            $listeAvis = $this->entityManager->getRepository(Avis::class)
                ->getAvisFilesAvailable($idConsultation, $organisme);

            if (false !== $listeAvis && (is_countable($listeAvis) ? count($listeAvis) : 0) > 0) {
                foreach ($listeAvis as $key => $avis) {
                    $extension = $espaceDocumentaireUtil->getFileExtension($avis['nomFichier']);
                    $result['fichiers'][$key]['nom'] = $avis['nomFichier'];
                    $result['fichiers'][$key]['extension'] = $extension;
                    $result['fichiers'][$key]['poids'] = $this->mountManager
                        ->getFileSize($avis['avis'], $organisme, $this->entityManager);

                    $result['fichiers'][$key]['id'] = $this->encryption->cryptId($avis['avis']);
                }
                unset($result['message']);
            } else {
                $result['message'] = $this->translator->trans('AUCUN_AVIS');
            }
        }
        $response = $this->getWithPaginate($request, $result);
        return $response->send();
    }
    /**
     * @Operation(
     *     tags={"/espace-documentaire"},
     *     summary="Retourne l'ensemble des autres pièces  de la consultation",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     * @return void
     * @throws Atexo_Consultation_Exception
     * @throws Exception
     */
    #[Route(path: '/autres-pieces-consultation/{idConsultation}.{format}', name: 'autre_piece_consultation', defaults: ['format' => 'json'], methods: ['GET'])]
    public function getAutresPiecesConsultationAction(Request $request, EspaceDocumentaireUtil $espaceDocumentaireUtil): Response
    {
        $result = [];
        $result['type'] = 'AUTRESPIECESCONSULTATION';
        $result['message'] = $this->translator->trans('NON_AUTORISE_RESSOURCE');
        $result['fichiers'] = [];
        $idConsultation = $request->get('idConsultation');
        $auth = $this->perimetreAgent->isAuthorized($idConsultation);
        $organisme = $this->security->getUser()
            ? $this->security->getUser()->getOrganisme()
            : $this->session->get('contexte_authentification')['organisme'];
        if (true === $auth) {
            $autrePieceConsultations = $this->entityManager->getRepository(AutrePieceConsultation::class)
                ->findBy([
                    'consultationId' => $idConsultation,
                ]);

            if (count($autrePieceConsultations) > 0) {
                $result = $espaceDocumentaireUtil->getAutrePieceConsultations(
                    $result,
                    $autrePieceConsultations,
                    $organisme
                );
            } else {
                $result['message'] = $this->translator->trans('AUCUNE_AUTRE_PIECE_CONSULTATION');
            }
        }
        $response = $this->getWithPaginate($request, $result);
        return $response->send();
    }
    /**
     * @Operation(
     *     tags={"/espace-documentaire"},
     *     summary="ajout d'une autre pièce de la consultation",
     *     @SWG\Parameter(
     *         name="file[]",
     *         in="formData",
     *         description="fichier que l'on souhaite envoyer",
     *         required=false,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     * @return void
     * @throws Atexo_Consultation_Exception
     * @throws Exception
     * @throws OptimisticLockException
     */
    #[Route(path: '/autres-pieces-consultation/{idConsultation}.{format}', name: 'ajout_autre_piece_consultation', defaults: ['format' => 'json'], methods: ['POST'])]
    public function postAutresPiecesConsultationAction(Request $request, EspaceDocumentaireUtil $espaceDocumentaireUtil): Response
    {
        $result = [];
        $result['type'] = 'AUTRES_PIECES_CONSULTATION';
        $result['message'] = $this->translator->trans('NON_AUTORISE_RESSOURCE');
        $idConsultation = $request->get('idConsultation');
        $auth = $this->perimetreAgent->isAuthorized($idConsultation);
        if ($auth) {
            if ($request->files && $idConsultation) {
                $files = $request->files->all();
                $file = array_shift($files);
                if (is_array($file)) {
                    $file = $file[0];
                }
                if (!$file instanceof UploadedFile) {
                    throw new InvalidArgumentException('No file found');
                }

                $atexoBlobOrganisme = $this->atexoFichierOrganisme;
                $organisme = $this->security->getUser()
                    ? $this->security->getUser()->getOrganisme()
                    : $this->session->get('contexte_authentification')['organisme'];

                $tmpFilePath = $file->getPathname();
                $atexoBlobOrganisme->moveTmpFile($tmpFilePath, $request);

                $idBlob = $atexoBlobOrganisme->insertFile(
                    $file->getClientOriginalName(),
                    $file->getPathName(),
                    $organisme
                );

                if ($idBlob) {
                    $blobOrganisme = $this->entityManager->getRepository(BloborganismeFile::class)
                        ->findOneBy(['id' => $idBlob]);
                    $consultation = $this->entityManager->getRepository(Consultation::class)
                        ->findOneBy(['id' => $idConsultation]);
                    $autrePieceConsultation = new AutrePieceConsultation();
                    $autrePieceConsultation->setBlobId($blobOrganisme);
                    $autrePieceConsultation->setConsultationId($consultation);
                    $this->entityManager->persist($autrePieceConsultation);
                    $this->entityManager->flush();

                    $result['message'] = $this->translator->trans('SUCCES_UPLOAD_AUTRE_PIECE');
                    $result['statut'] = 200;

                    $result['fichier'] = [];
                    $result['fichier']['id'] = $this->encryption->cryptId($idBlob);
                    $result['fichier']['nom'] = $blobOrganisme->getName();
                    $extension = $espaceDocumentaireUtil->getFileExtension($blobOrganisme->getName());
                    $result['fichier']['extension'] = $extension;
                    $result['fichier']['poids'] = $this->mountManager
                        ->getFileSize($blobOrganisme->getId(), $organisme, $this->entityManager);
                    $result['fichier']['created_at'] = $autrePieceConsultation->getCreatedAt()->format('d/m/Y - G:i');
                    $result['fichier']['uploaded'] = true;
                } else {
                    $result['message'] = $this->translator->trans('ERROR_UPLOAD_AUTRE_PIECE');
                    $result['statut'] = 500;
                }
            } else {
                $result['statut'] = 500;
                $result['message'] = $this->translator->trans('ERROR_UPLOAD_AUTRE_PIECE');
            }
        }
        $response = $this->getWithPaginate($request, $result);
        return $response->send();
    }
    /**
     * @Operation(
     *     tags={"/espace-documentaire"},
     *     summary="Suppression d'une autre pièce de la consultation",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     * @throws Atexo_Consultation_Exception
     * @throws Exception
     * @throws OptimisticLockException
     */
    #[Route(path: '/autres-pieces-consultation/{idConsultation}/{blobId}.{format}', name: 'delete_autre_piece_consultation', defaults: ['format' => 'json'], methods: ['DELETE'])]
    public function deleteAutresPiecesConsultationAction(Request $request): Response
    {
        $result = [];
        $result['type'] = 'autre pièce consultation';
        $result['message'] = $this->translator->trans('NON_AUTORISE_RESSOURCE');
        $blobId = $this->encryption->decryptId($request->get('blobId'));
        $idConsultation = $request->get('idConsultation');
        $auth = $this->perimetreAgent->isAuthorized($idConsultation);
        if ($auth) {
            $autrePiece = $this->entityManager->getRepository(AutrePieceConsultation::class)
                ->findOneBy([
                    'blobId' => $blobId,
                    'consultationId' => $idConsultation,
                ]);

            if ($autrePiece) {
                $this->entityManager->remove($autrePiece);
                $atexoBlobOrganisme = $this->atexoFichierOrganisme;
                $organisme = $this->security->getUser()
                    ? $this->security->getUser()->getOrganisme()
                    : $this->session->get('contexte_authentification')['organisme'];
                $atexoBlobOrganisme->deleteBlobFile($blobId, $organisme);
                $this->entityManager->flush();
                $result['statut'] = 200;
                $result['message'] = $this->translator->trans('DELETE_OK');
            } else {
                $result['statut'] = 403;
                $result['message'] = $this->translator->trans('ERROR_DELETE');
            }
        }
        $response = $this->getWithPaginate($request, $result);
        return $response->send();
    }
    /**
     * @Operation(
     *     tags={"/espace-documentaire"},
     *     summary="Retourne la liste des plis du ou des attributaires",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     * @throws Atexo_Consultation_Exception
     * @throws Exception
     */
    #[Route(path: '/plis-attributaires/{idConsultation}.{format}', name: 'plis-attributaires', defaults: ['format' => 'json'], methods: ['GET'])]
    public function getPlisDesAttributaires(Request $request): Response
    {
        $result = [];
        $idConsultation = $request->get('idConsultation');
        $auth = $this->perimetreAgent->isAuthorized($idConsultation);
        $result['type'] = 'ATTRIBUTAIRES';
        $result['message'] = $this->translator->trans('NON_AUTORISE_RESSOURCE');
        if (true === $auth) {
            $statutOffres = $this->parameterBag->get('STATUT_ENV_BROUILLON');
            $offres = $this->entityManager->getRepository(Offre::class)
                ->getAttributaire($idConsultation, $statutOffres);

            if ((is_countable($offres) ? count($offres) : 0) > 0) {
                foreach ($offres as $key => $offre) {
                    $result['plis'][$key]['entreprise'] = $offre['nomEntrepriseInscrit'];
                    $result['plis'][$key]['pli'] = 'El ' . $offre['numeroReponse'];
                    $result['plis'][$key]['offre'] = $this->encryption->cryptId($offre['id']);
                }
                unset($result['message']);
            } else {
                $result['message'] = $this->translator->trans('AUCUN_ATTRIBUTAIRE');
            }
        } else {
            $result['message'] = $this->translator->trans('AUCUN_ATTRIBUTAIRE');
        }
        $response = $this->getWithPaginate($request, $result);
        return $response->send();
    }

    /**
     * @Operation(
     *     tags={"/espace-documentaire"},
     *     summary="Retourne la liste des  piéces d'un plis de l'attributaires",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     * @param Request $request
     * @param EspaceDocumentaireUtil $espaceDocumentaireUtil
     * @return Response
     */
    #[Route(path: '/pieces-plis-attributaires/{idOffre}/{idConsultation}.{format}', name: 'pieces_plis_attributaires', defaults: ['format' => 'json'], methods: ['GET'])]
    public function getPiecesPlisAttributaires(Request $request, EspaceDocumentaireUtil $espaceDocumentaireUtil): Response
    {
        $result = [];
        $idConsultation = $request->get('idConsultation');
        $idOffre = $this->encryption->decryptId($request->get('idOffre'));
        $auth = $this->perimetreAgent->isAuthorized($idConsultation);
        $result['type'] = 'PLI_ATTRIBUTAIRE';
        $result['offre'] = $idOffre;
        $result['message'] = $this->translator->trans('NON_AUTORISE_RESSOURCE');
        if (true === $auth) {
            $organisme = $this->security->getUser()
                ? $this->security->getUser()->getOrganisme()
                : $this->session->get('contexte_authentification')['organisme'];
            $consultation = $this->entityManager->getRepository(Consultation::class)->find($idConsultation);

            $result = $espaceDocumentaireUtil->getPiecesDesAttributaires($consultation, $organisme, $idOffre);
        } else {
            $result['message'] = $this->translator->trans('PAS_DE_DEPOT');
        }
        $response = $this->getWithPaginate($request, $result);
        return $response->send();
    }
    /**
     * @Operation(
     *     tags={"/espace-documentaire"},
     *     summary="Création du message messenger de téléchargement",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     */
    #[Route(path: '/create-download/{idConsultation}.{format}', name: 'create_download', defaults: ['format' => 'json'], methods: ['POST'])]
    public function createDownloadAction(Request $request, MessageBusInterface $bus): Response
    {
        $result = [];
        $result['type'] = 'CREATEDOWNLOAD';
        $result['message'] = $this->translator->trans('NON_AUTORISE_RESSOURCE');
        $idConsultation = $request->get('idConsultation');
        $auth = $this->perimetreAgent->isAuthorized($idConsultation);
        if ($auth) {
            $datas = $request->request->all();
            $res = $this->atexoUtil->getArrayArbo($datas['data']);

            if (is_array($res) && !empty($res)) {
                $idAgent = $this->security->getUser()
                    ? $this->security->getUser()->getId()
                    : $this->session->get('contexte_authentification')['id'];
                $this->logger->info('lancement message GenerationArchiveDocumentaire pour $idConsultation : ' .
                $idConsultation, [$idConsultation, $idAgent, base64_encode(json_encode($res, JSON_THROW_ON_ERROR))]);
                $bus->dispatch(new GenerationArchiveDocumentaire(
                    $idConsultation,
                    $idAgent,
                    base64_encode(json_encode($res, JSON_THROW_ON_ERROR))
                ));

                $result['message'] = 'OK';
                $result['statut'] = 200;
            } else {
                $result['statut'] = 500;
                $result['message'] = 'KO';
            }
        }

        $response = $this->getWithPaginate($request, $result);

        return $response->send();
    }
}
