<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Api;

use App\Service\AtexoFichier;
use App\Controller\AtexoController;
use App\Entity\DossierVolumineux\DossierVolumineux;
use App\Entity\WS\AgentTechniqueToken;
use App\Service\DossierVolumineux\DossierVolumineuxService;
use DateTime;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DossierVolumineuxController.
 **/
#[Route(
    path: '/api/{version}/dossier-volumineux',
    name: 'atexo_dossier_volumineux',
    requirements: ["version" => "^(?:(?!v2).)+$"]
)]
class DossierVolumineuxWsController extends AtexoController
{
    public function __construct(
        private AtexoFichier $atexoFichier,
        private DossierVolumineuxService $dossierVolumineuxService,
        private ParameterBagInterface $parameterBag
    ) {
    }

    /**
     * @Operation(
     *     tags={"/envol/"},
     *     summary="Dossier Volumineux: Upload le fichier de log",
     *     @SWG\Parameter(
     *         name="logFile",
     *         in="formData",
     *         description="Contenue du fichier de log",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * @Rest\View()
     * @ParamConverter("dossierVolumineux", options={"mapping": {"uuidTechnique": "uuidTechnique"}})
     *
     *
     * @return Response
     */
    #[Route(path: '/upload/logfile/{uuidTechnique}/{HASHMD5}/{ticket}', methods: ['POST'], name: 'atexo_dossier_volumineux_logfile')]
    public function uploadFileLogAction(Request $request, DossierVolumineux $dossierVolumineux = null)
    {
        $response = $this->checkDossierVolumineux($dossierVolumineux);
        if (true === $response) {
            $response = $this->uploadFile($request, $dossierVolumineux, 'logFile');
        }
        return $response;
    }
    public function checkDossierVolumineux(DossierVolumineux $dossierVolumineux = null): bool|JsonResponse
    {
        $code = Response::HTTP_PRECONDITION_FAILED;
        if (null !== $dossierVolumineux) {
            return true;
        } else {
            $json = [
                'error_code' => $code,
                'error_message' => "L'uuid technique n'est pas valide.",
            ];
            $response = new JsonResponse($json, $code);
            $response->headers->set('Content-type', 'application/json');
        }

        return $response;
    }
    /**
     * @return JsonResponse
     */
    public function uploadFile(Request $request, DossierVolumineux $dossierVolumineux, string $prefix)
    {
        $statutCode = null;
        try {
            $em = $this->getDoctrine()->getManager();
            $ticket = $request->get('ticket');

            $check = $em
                ->getRepository(AgentTechniqueToken::class)
                ->checkToken($ticket);

            $statutCode = Response::HTTP_OK;
            if (false == $check) {
                $statutCode = Response::HTTP_UNAUTHORIZED;
                throw new Exception('Invalid ticket');
            }

            $contenu = $request->get($prefix);
            $md5 = $request->get('HASHMD5');

            if ($md5 !== md5($contenu)) {
                $statutCode = Response::HTTP_PRECONDITION_FAILED;
                throw new Exception('Invalid MD5');
            }

            try {
                $this->atexoFichier
                    ->insertDossierVolumineuxFile($contenu, $dossierVolumineux, $prefix);
            } catch (Exception $exception) {
                $statutCode = Response::HTTP_INTERNAL_SERVER_ERROR;
                throw $exception;
            }

            $json = ['uuid' => $dossierVolumineux->getUuidReference()];
            $response = new JsonResponse($json);
            $response->headers->set('Content-type', 'application/json');
        } catch (Exception $exception) {
            $json = [
                'error_code' => $statutCode,
                'error_message' => $exception->getMessage(),
            ];
            $response = new JsonResponse($json, $statutCode);
            $response->headers->set('Content-type', 'application/json');
        }

        return $response;
    }
    /**
     * @Operation(
     *     tags={"/envol/"},
     *     summary="Dossier Volumineux: Upload le fichier descripteur",
     *     @SWG\Parameter(
     *         name="descripteurFile",
     *         in="formData",
     *         description="Contenue du fichier descripteur",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * @Rest\View()
     * @ParamConverter("dossierVolumineux", options={"mapping": {"uuidTechnique": "uuidTechnique"}})
     *
     *
     * @return Response
     */
    #[Route(path: '/upload/descripteurfile/{uuidTechnique}/{HASHMD5}/{ticket}', methods: ['POST'])]
    public function uploadFileDescripteurAction(Request $request, DossierVolumineux $dossierVolumineux = null)
    {
        $response = $this->checkDossierVolumineux($dossierVolumineux);
        if (true === $response) {
            $response = $this->uploadFile($request, $dossierVolumineux, 'descripteurFile');
        }
        return $response;
    }
    /**
     * @Operation(
     *     tags={"/envol/"},
     *     summary="Mise à jour des métadata d'un dossier volumineux",
     *     @SWG\Parameter(
     *         name="statut",
     *         in="formData",
     *         description="Statut de l'upload (valeur: init, incomplet ou complet)",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="date",
     *         in="formData",
     *         description="Date de l'envoie des metadata",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="taille",
     *         in="formData",
     *         description="Taille du répertoire",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="nom",
     *         in="formData",
     *         description="nom du répertoire",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * @Rest\View()
     * @ParamConverter("dossierVolumineux", options={"mapping": {"uuidTechnique": "uuidTechnique"}})
     *
     *
     * @return Response
     */
    #[Route(path: '/upload/metadata/{uuidTechnique}/{ticket}', methods: ['POST'], name: 'dossier_volumineux_metadata')]
    public function uploadMetadataAction(Request $request, DossierVolumineux $dossierVolumineux = null)
    {
        $statutCode = null;
        try {
            $response = $this->checkDossierVolumineux($dossierVolumineux);
            if (true === $response) {
                $ticket = $request->get('ticket');

                $check = $this->getDoctrine()
                    ->getRepository(AgentTechniqueToken::class)
                    ->checkToken($ticket);

                $statutCode = Response::HTTP_OK;
                if (false == $check) {
                    $statutCode = Response::HTTP_UNAUTHORIZED;
                    throw new Exception('Invalid ticket');
                }
                $tabStatut = ['incomplet', 'complet'];
                if (!in_array($request->get('statut'), $tabStatut)) {
                    $statutCode = Response::HTTP_PRECONDITION_FAILED;
                    throw new Exception('Invalid statut');
                }

                if (is_null($request->get('taille'))) {
                    $statutCode = Response::HTTP_PRECONDITION_FAILED;
                    throw new Exception('Invalid taille');
                }

                if (is_null($request->get('nom'))) {
                    $statutCode = Response::HTTP_PRECONDITION_FAILED;
                    throw new Exception('Invalid nom');
                }

                if (is_null($request->get('nom'))) {
                    $statutCode = Response::HTTP_PRECONDITION_FAILED;
                    throw new Exception('Invalid nom');
                }

                $em = $this->getDoctrine()->getManager();
                $datetime = new DateTime($request->get('date'));
                $dossierVolumineux->setDateModification($datetime);
                $dossierVolumineux->setTaille($request->get('taille'));
                $dossierVolumineux->setNom($request->get('nom'));
                $dossierVolumineux->setStatut($request->get('statut'));
                $activeDossierVolumineux = 'complet' == $request->get('statut') ? 1 : 0;
                $dossierVolumineux->setActif($activeDossierVolumineux);

                $em->persist($dossierVolumineux);
                $em->flush();

                $json = ['uuid' => $dossierVolumineux->getUuidReference()];
                $response = new JsonResponse($json);
                $response->headers->set('Content-type', 'application/json');
            }
        } catch (Exception $exception) {
            if (Response::HTTP_OK === $statutCode) {
                $statutCode = Response::HTTP_PRECONDITION_FAILED;
            }
            $json = [
                'error_code' => $statutCode,
                'error_message' => $exception->getMessage(),
            ];
            $response = new JsonResponse($json, $statutCode);
            $response->headers->set('Content-type', 'application/json');
        }
        return $response;
    }
    #[Route(path: '/upload/')]
    public function upload() : Response
    {
        /** @var DossierVolumineuxService $service */
        $service = $this->dossierVolumineuxService;
        if (false === $service->isEnable()) {
            return $this->redirectAccueil();
        }
        try {
            $dossierVolumineux = $service->createDossierVolumineux();
        } catch (Exception $e) {
            return new Response($e->getMessage());
        }
        $params = [
            'reference' => $dossierVolumineux->getUuidReference(),
            'serverLogin' => $this->parameterBag->get('LOGIN_TIERS_TUS'),
            'serverPassword' => $this->parameterBag->get('PWD_TIERS_TUS'),
            'serverURL' => $this->parameterBag->get('PF_URL'),
            'statut' => '0',
        ];
        try {
            $responseTus = $service->callTusSession($params, $dossierVolumineux->getUuidTechnique());
            $uuidSessionTus = $responseTus['uuid'];
            $url = $this->parameterBag->get('URL_CLIENT_TUS');
            $url .= 'index.html?uuid=';
            $url .= $uuidSessionTus;

            return $this->redirect($url);
        } catch (Exception $e) {
            return new Response($e->getMessage());
        }
    }
    /**
     * @ParamConverter("dossierVolumineux", options={"mapping": {"uuidReference": "uuidReference"}})*/
    #[Route(path: '/event/{uuidReference}')]
    public function event(DossierVolumineux $dossierVolumineux) : Response
    {
        /** @var DossierVolumineuxService $service */
        $service = $this->dossierVolumineuxService;
        if (false === $service->checkAccess($dossierVolumineux)) {
            return $this->redirectAccueil();
        }
        $file = $service->getBlogFilePath($dossierVolumineux);
        return $this->file($this->getParameter('DIR_DOSSIERS_VOLUMINEUX').$file);
    }
}
