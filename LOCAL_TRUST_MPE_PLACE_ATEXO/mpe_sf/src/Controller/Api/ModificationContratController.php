<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Api;

use App\Exception\ApiProblemInvalidArgumentException;
use App\Service\AgentTechniqueTokenService;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\AtexoAgent;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\AtexoUtil;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Exception\ValidatorException;

/**
 * Class ModificationContratController.
 **/
#[Route(path: '/api/{version}/contrats', name: 'api_modification_contrat_', requirements: ['version' => '^(?:(?!v2).)+$'])]
class ModificationContratController extends AbstractWebserviceController
{
    public function __construct(
        private EntityManagerInterface $em,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private ApiSerializer $apiSerializer,
        private AtexoAgent $agent,
        private AtexoEntreprise $atexoEntreprise,
        private AtexoEtablissement $atexoEtablissement,
        private AtexoInscrit $atexoInscrit,
        private OrganismeService $organismeService,
        private ModificationContratService $modificationContratService,
        private AtexoUtil $atexoUtil,
    ) {
        parent::__construct(
            $em,
            $agentTechniqueTokenService,
            $apiSerializer,
            $agent,
            $atexoEntreprise,
            $atexoEtablissement,
            $atexoInscrit,
            $organismeService,
            $modificationContratService,
            $atexoUtil
        );
    }

    /**
     * @Operation(
     *     tags={"/modifications"},
     *     summary="Ce Web service a comme rôle de pouvoir mettre à jour Une modification de contrat.",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     * @return Response
     *
     * @throws Exception
     */
    #[Route(path: '/modifications.{format}', name: 'insertModificationContrat', defaults: ['format' => 'json'], methods: ['POST'])]
    public function insert(Request $request, LoggerInterface $logger)
    {
        try {
            return $this->createEntity($request, ModificationContratService::class, $logger);
        } catch (ValidatorException $e) {
            $logger->error(__METHOD__.' : '.$e->getMessage());
            throw new ApiProblemInvalidArgumentException($e->getMessage());
        } catch (Exception $e) {
            $msg = "Une erreur est survenue lors de l'insertion de la modification de contrat.".$e->getMessage();
            $logger->error(__METHOD__.' : '.$msg);
            throw new ApiProblemInvalidArgumentException("Une erreur est survenue lors de l'insertion de la modification de contrat.".$e->getMessage());
        }
    }
}
