<?php

namespace App\Controller\Api;

use DateTime;
use Exception;
use SimpleXMLElement;
use App\Controller\AtexoController;
use App\Entity\Agent;
use App\Entity\Tiers;
use App\Entity\WS\AgentTechniqueToken;
use App\Repository\WS\AgentTechniqueTokenRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ConsultationApiController.
 **/
#[Route(path: '/indicateur_metier', name: 'atexo_indicateur_metier')]
class IndicateurMetierController extends AtexoController
{
    /**
     * @param $min
     * @param $max
     * @param $token
     */
    private function getIndicateurMetier($min, $max, $token): bool|string
    {
        $datemin = new DateTime($min);
        $datemax = new DateTime($max);

        $url = $this->getParameter('PF_URL_REFERENCE').
            'app.php/api/consultation/get_consultation'.
            '?datemin='.$datemin->format('Y-m-d').
            '&datemax='.$datemax->format('Y-m-d').
            '&token_permission='.$token;

        return file_get_contents($url);
    }
    /**
     * @return Response
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>*/
    #[Route(path: '/rapport_prod', name: 'atexo_indicateur_metier_index')]
    public function indexAction(Request $request)
    {
        set_time_limit(0);
        //date => day by day
        $datemin = new DateTime();
        $datemin->modify('-1 day');
        $datemin->setTime('00', '00', '00');
        $datemax = new DateTime();
        $datemax->setTime('00', '00', '00');
        $datemaxCurrentMonthYear = new DateTime();
        $datemaxCurrentMonthYear->setTime('00', '00', '00');
        //Création du token
        $token = $this->getToken();
        /** @var AgentTechniqueTokenRepository $agentTechniqueTokenRepository */
        $agentTechniqueTokenRepository = $this->getDoctrine()->getRepository(AgentTechniqueToken::class);
        $check = $agentTechniqueTokenRepository->checkToken($token);
        $user = $_GET['token_user'];
        $id = $this->getDoctrine()
            ->getRepository(Agent::class)
            ->getIdAgentHash($this->getParameter('INDICATEUR_METIER_SALT'), $user);
        $hyper_admin = $this->getDoctrine()
            ->getRepository(Agent::class)
            ->getIdAgentHyperAdmin($id);
        if (0 == $hyper_admin['hyper_admin'] || null === $id) {
            return $this->render('Default/unauthorized.html.twig', []);
        }
        if ($check && 1 == $hyper_admin['hyper_admin']) {
            $j1 = $this->getIndicateurMetier($datemin->format('Y-m-d'), $datemax->format('Y-m-d'), $token);
            $datemin->modify('-1 day');
            $datemax->modify('-1 day');

            $j2 = $this->getIndicateurMetier($datemin->format('Y-m-d'), $datemax->format('Y-m-d'), $token);
            $datemin->modify('-1 day');
            $datemax->modify('-1 day');

            $j3 = $this->getIndicateurMetier($datemin->format('Y-m-d'), $datemax->format('Y-m-d'), $token);
            $datemin->modify('-1 day');
            $datemax->modify('-1 day');

            $j4 = $this->getIndicateurMetier($datemin->format('Y-m-d'), $datemax->format('Y-m-d'), $token);
            $datemin->modify('-1 day');
            $datemax->modify('-1 day');

            $j5 = $this->getIndicateurMetier($datemin->format('Y-m-d'), $datemax->format('Y-m-d'), $token);
            $datemin->modify('-1 day');
            $datemax->modify('-1 day');

            $j6 = $this->getIndicateurMetier($datemin->format('Y-m-d'), $datemax->format('Y-m-d'), $token);
            $datemin->modify('-1 day');
            $datemax->modify('-1 day');

            $j7 = $this->getIndicateurMetier($datemin->format('Y-m-d'), $datemax->format('Y-m-d'), $token);
            $datemin->modify('-1 day');
            $datemax->modify('-1 day');

            $j8 = $this->getIndicateurMetier($datemin->format('Y-m-d'), $datemax->format('Y-m-d'), $token);

            // date => month by month
            $datemin = new DateTime();
            $datemin->modify('first day of this month');
            $datemin->setTime('00', '00', '00');
            $datemax = new DateTime();
            $datemax->modify('+1 month');
            $datemax->modify('first day of this month');

            $mec = $this->getIndicateurMetier(
                $datemin->format('Y-m-d'),
                $datemaxCurrentMonthYear->format('Y-m-d'),
                $token
            );
            $datemin->modify('-1 month');
            $datemax->modify('-1 month');

            $m1 = $this->getIndicateurMetier($datemin->format('Y-m-d'), $datemax->format('Y-m-d'), $token);
            $datemin->modify('-1 month');
            $datemax->modify('-1 month');

            $m2 = $this->getIndicateurMetier($datemin->format('Y-m-d'), $datemax->format('Y-m-d'), $token);
            $datemin->modify('-1 month');
            $datemax->modify('-1 month');

            $m3 = $this->getIndicateurMetier($datemin->format('Y-m-d'), $datemax->format('Y-m-d'), $token);
            $datemin->modify('-9 month');
            $datemax->modify('-9 month');

            $m12 = $this->getIndicateurMetier($datemin->format('Y-m-d'), $datemax->format('Y-m-d'), $token);

            // date => year by year
            $datemin = new DateTime();
            $datemin->setTime('00', '00', '00');
            $datemax = new DateTime();
            $datemax->modify('+1 year');

            $aec = $this->getIndicateurMetier(
                $datemin->format('Y-01-01'),
                $datemaxCurrentMonthYear->format('Y-m-d'),
                $token
            );
            $datemin->modify('-1 year');
            $datemax->modify('-1 year');

            $a1 = $this->getIndicateurMetier($datemin->format('Y-01-01'), $datemax->format('Y-01-01'), $token);
            $datemin->modify('-1 year');
            $datemax->modify('-1 year');

            $a2 = $this->getIndicateurMetier($datemin->format('Y-01-01'), $datemax->format('Y-01-01'), $token);

            $response = $this->render('Default/indicateur_metier.html.twig', [
                'j1' => json_decode($j1, true, 512, JSON_THROW_ON_ERROR),
                'j2' => json_decode($j2, true, 512, JSON_THROW_ON_ERROR),
                'j3' => json_decode($j3, true, 512, JSON_THROW_ON_ERROR),
                'j4' => json_decode($j4, true, 512, JSON_THROW_ON_ERROR),
                'j5' => json_decode($j5, true, 512, JSON_THROW_ON_ERROR),
                'j6' => json_decode($j6, true, 512, JSON_THROW_ON_ERROR),
                'j7' => json_decode($j7, true, 512, JSON_THROW_ON_ERROR),
                'j8' => json_decode($j8, true, 512, JSON_THROW_ON_ERROR),
                'mec' => json_decode($mec, true, 512, JSON_THROW_ON_ERROR),
                'm1' => json_decode($m1, true, 512, JSON_THROW_ON_ERROR),
                'm2' => json_decode($m2, true, 512, JSON_THROW_ON_ERROR),
                'm3' => json_decode($m3, true, 512, JSON_THROW_ON_ERROR),
                'm12' => json_decode($m12, true, 512, JSON_THROW_ON_ERROR),
                'aec' => json_decode($aec, true, 512, JSON_THROW_ON_ERROR),
                'a1' => json_decode($a1, true, 512, JSON_THROW_ON_ERROR),
                'a2' => json_decode($a2, true, 512, JSON_THROW_ON_ERROR),
            ]);

            $response->setSharedMaxAge(3600);

            return $response;
        } else {
            return $this->render('Default/unauthorized.html.twig', []);
        }
    }
    /**
     * @return mixed
     */
    public function getToken()
    {
        $options = [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => '',
            CURLOPT_USERAGENT => 'spider',
            CURLOPT_AUTOREFERER => true,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYPEER => false,
        ];

        $tiersRepository = $this->getDoctrine()->getRepository(Tiers::class);
        $tier = $tiersRepository->findOneBy(['idTiers' => 1]);

        $ch = curl_init(
            $this->getParameter('PF_URL_REFERENCE').
            'api.php/ws/authentification/connexion/'.$tier->getLogin().'/'.$tier->getPassword()
        );

        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        $err = curl_errno($ch);
        $errmsg = curl_error($ch);
        $header = curl_getinfo($ch);
        curl_close($ch);

        $header['errno'] = $err;
        $header['errmsg'] = $errmsg;
        $header['content'] = $content;
        if ($err) {
            $this->getLogger('authentication')->Error(sprintf('Problème lors de la recupèration du token %s', $errmsg));
            throw new Exception(sprintf('Problème lors de la recupèration du token %s', $errmsg));
        }

        $parser = new SimpleXMLElement($content);

        return $parser[0];
    }
}
