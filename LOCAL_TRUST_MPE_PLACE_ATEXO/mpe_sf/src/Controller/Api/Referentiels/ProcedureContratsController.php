<?php

/** MPE */

namespace App\Controller\Api\Referentiels;

use App\Controller\Api\AbstractWebserviceController;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\TypeContrat;
use App\Service\AgentTechniqueTokenService;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\Api\StatistiqueService;
use App\Service\AtexoAgent;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\AtexoUtil;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\MpeSecurity;
use App\Service\OrganismeService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ProcedureContratsController.
 **/
#[Route(path: '/api/{version}/referentiels/procedureContrats', name: 'atexo_api_referentiels_procedure_contrats', requirements: ['version' => '^(?:(?!v2).)+$'])]
class ProcedureContratsController extends AbstractWebserviceController
{
    public final const ORGANISME = 'organisme';

    public function __construct(
        private EntityManagerInterface $em,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private ApiSerializer $apiSerializer,
        private AtexoAgent $agent,
        private AtexoEntreprise $atexoEntreprise,
        private AtexoEtablissement $atexoEtablissement,
        private AtexoInscrit $atexoInscrit,
        private OrganismeService $organismeService,
        private ModificationContratService $modificationContratService,
        private TranslatorInterface $translator,
        private AtexoUtil $atexoUtil
    ) {
        parent::__construct(
            $em,
            $agentTechniqueTokenService,
            $apiSerializer,
            $agent,
            $atexoEntreprise,
            $atexoEtablissement,
            $atexoInscrit,
            $organismeService,
            $modificationContratService,
            $atexoUtil
        );
    }

    /**
     * @Operation(
     *     tags={"/referentiels"},
     *     summary="Return referentiel",
     *     @SWG\Parameter(
     *         name="organisme",
     *         in="body",
     *         description="Filtre par organisme",
     *         required=false,
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Parameter(
     *         name="exec",
     *         in="body",
     *         description="Filtre par module exec",
     *         required=false,
     *         @SWG\Schema(type="boolean")
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * @Rest\View()
     * @Rest\Get()
     *
     * @return Response
     */
    public function getReferentielsAction(Request $request, LoggerInterface $logger)
    {
        try {
            $atexoUtil = $this->atexoUtil;
            $trans = $this->translator;
            $procedures = $this->getDoctrine()
                ->getRepository(TypeProcedureOrganisme::class)
                ->getTypeProcedureDistinctWithCodesOrganisme(
                    $atexoUtil,
                    $request->query->get('organisme')
                );
            $logger->info('Recuperation des procedures OK ');
            $contrats = $this->getDoctrine()
                ->getRepository(TypeContrat::class)
                ->getAllContratTypeWithCodesProcedure(
                    $atexoUtil,
                    $trans,
                    $request->query->get('organisme')
                );
            $logger->info('Recuperation des contrats OK');
            $logger->info('generation de la reponse');

            return $this->streamResponse($procedures, $contrats);
        } catch (Exception $e) {
            $params = ['error' => $e->getMessage()];
            $logger->error($e->getMessage() . ' trace ' . $e->getTraceAsString());
            $response = new Response();
            $response->headers->set('Content-Type', 'application/xml');
            $response->setContent($this->renderView('synchro/erreur.xml.twig', $params));

            return $response;
        }
    }
    /**
     * @param $procedures
     * @param $contrats
     *
     * @return StreamedResponse
     */
    public function streamResponse($procedures, $contrats)
    {
        $response = new StreamedResponse();
        $response->headers->set('Content-Type', 'application/xml');
        $response->setCallback(function () use ($procedures, $contrats) {
            $streamCount = 50;
            $this->printf('<?xml version="1.0" encoding="UTF-8"?>');
            echo $this->getNodeMpe();
            $this->printf('<reponse>');
            $this->printf('<referentiels>');
            $this->printf('<procedures>');
            foreach ($procedures as $procedure) {
                $this->printf('<procedure>');
                $this->printf('<codeExterne><![CDATA[%s]]></codeExterne>', $procedure->getIdExterne());
                $this->printf('<libelle><![CDATA[%s]]></libelle>', $procedure->getLibelleTypeProcedure());
                $this->printf('<abreviation><![CDATA[%s]]></abreviation>', $procedure->getAbbreviation());
                $this->printf('<associationOrganisme>');
                $count = 1;
                foreach ($procedure->getOrganismes() as $codeOrg) {
                    $this->printf('<codeOrganisme><![CDATA[%s]]></codeOrganisme>', $codeOrg[self::ORGANISME]);
                    if (0 === $count % $streamCount && 1 !== $count) {
                        ob_flush();
                        flush();
                    }
                    ++$count;
                }
                $this->printf('</associationOrganisme>');
                $this->printf('</procedure>');
            }
            $this->printf('</procedures>');
            $this->printf('<contrats>');
            foreach ($contrats as $contrat) {
                $this->printf('<contrat>');
                $this->printf('<codeExterne><![CDATA[%s]]></codeExterne>', $contrat->getIdExterne());
                $this->printf('<libelle><![CDATA[%s]]></libelle>', $contrat->getLibelleTypeContrat());
                $this->printf('<abreviation><![CDATA[%s]]></abreviation>', $contrat->getAbreviationTypeContrat());
                $this->printf('<associationProcedures>');
                $count = 1;
                foreach ($contrat->getProcedureCodes() as $codeProcedure) {
                    $this->printf('<associationProcedure>');
                    $this->printf('<codeOrganisme><![CDATA[%s]]></codeOrganisme>', $codeProcedure['id_externe']);
                    $this->printf('<codeExterneProcedure><![CDATA[%s]]></codeExterneProcedure>', $codeProcedure[self::ORGANISME]);
                    $this->printf('</associationProcedure>');
                    if (0 === $count % $streamCount && 1 !== $count) {
                        ob_flush();
                        flush();
                    }
                    ++$count;
                }
                $this->printf('</associationProcedures>');
                $this->printf('</contrat>');
            }
            $this->printf('</contrats>');
            $this->printf('</referentiels>');
            $this->printf('</reponse>');
            $this->printf('</mpe>');
        });

        return $response->send();
    }
    /**
     * @param $str
     * @param $value
     */
    public function printf($str, $value = null)
    {
        if (empty($value)) {
            printf("$str\n", '');
        } else {
            printf("$str\n", $value);
        }
    }
}
