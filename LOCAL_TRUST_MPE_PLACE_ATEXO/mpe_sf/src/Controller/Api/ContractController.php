<?php

namespace App\Controller\Api;

use App\Service\AgentTechniqueTokenService;
use App\Entity\ContratTitulaire;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\Api\StatistiqueService;
use App\Service\AtexoAgent;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\AtexoUtil;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Nelmio\ApiDocBundle\Annotation\Model;
use Psr\Log\LoggerInterface;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ContractController.
 **/
//#[Route(
//    path: '/api/{version}/donnees-essentielles/contrat',
//    name: 'atexo_api_donnees_essentielles',
//    requirements: ["version" => "^(?:(?!v2).)+$"]
//)]

#[Route(path: '/api/{version}', name: 'atexo_api_', defaults: ['format' => 'xml'], requirements: ['version' => '^(?:(?!v2).)+$'])]
class ContractController extends AbstractWebserviceController
{
    public function __construct(
        private EntityManagerInterface $em,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private ApiSerializer $apiSerializer,
        private AtexoAgent $agent,
        private AtexoEntreprise $atexoEntreprise,
        private AtexoEtablissement $atexoEtablissement,
        private AtexoInscrit $atexoInscrit,
        private OrganismeService $organismeService,
        private ModificationContratService $modificationContratService,
        private AtexoUtil $atexoUtil,
    ) {
        parent::__construct(
            $em,
            $agentTechniqueTokenService,
            $apiSerializer,
            $agent,
            $atexoEntreprise,
            $atexoEtablissement,
            $atexoInscrit,
            $organismeService,
            $modificationContratService,
            $atexoUtil
        );
    }

    /**
     * @Operation(
     *     tags={"/contrats"},
     *     summary="Lister les contrats",
     *     @SWG\Parameter(
     *         name="limit",
     *         in="body",
     *         description="Nombre d'éléments par page",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="body",
     *         description="Numéro de page - Entier supérieur à 0",
     *         required=false,
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="lastUpdateTimestamp",
     *         in="body",
     *         description="Dernière modification - Timestamp",
     *         required=false,
     *         @SWG\Schema(type="timestamp")
     *     ),
     *     @SWG\Parameter(
     *         name="module",
     *         in="body",
     *         description="nom du module appelant (exec,redac, autres)",
     *         required=false,
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * @Rest\Get("/contrats.{format}", name="contracts_get_list")
     * @Rest\View()
     */
    public function listAction(Request $request)
    {
        $agentTechniqueService = $this->agentTechniqueTokenService;
        $agentTechnique = $agentTechniqueService->getAgentFromToken($request->get('ticket'));
        $orgServicesAllowed = $agentTechniqueService->getAllOrganismesServicesAllowed($agentTechnique);
        $response = $this->getWithPaginate($request, ContratTitulaire::class, $orgServicesAllowed);

        return $response->send();
    }

    /**
     * @Operation(
     *     tags={"/contrats"},
     *     summary="Récupérer un contrat",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     * @Rest\Get("/contrats/{id}.{format}", name="contracts_get")
     * @Rest\View()
     */
    public function getContratAction(Request $request)
    {
        $agentTechniqueService = $this->agentTechniqueTokenService;
        $agentTechnique = $agentTechniqueService->getAgentFromToken($request->get('ticket'));
        $orgServicesAllowed = $agentTechniqueService->getAllOrganismesServicesAllowed($agentTechnique);
        $response = $this->getWithPaginate($request, ContratTitulaire::class, $orgServicesAllowed);

        return $response->send();
    }
}
