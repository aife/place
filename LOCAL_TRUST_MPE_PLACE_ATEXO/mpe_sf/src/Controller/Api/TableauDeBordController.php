<?php

namespace App\Controller\Api;

use App\Service\AgentTechniqueTokenService;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\AtexoAgent;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\AtexoUtil;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use App\Service\Api\TableauDeBordService;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TableauBordController.
 **/
#[Route(path: '/api/{version}/tableau-de-bord', name: 'atexo_api_tableaudebord', requirements: ['version' => '^(?:(?!v2).)+$'])]
class TableauDeBordController extends AbstractWebserviceController
{
    public function __construct(
        private EntityManagerInterface $em,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private ApiSerializer $apiSerializer,
        private AtexoAgent $agent,
        private AtexoEntreprise $atexoEntreprise,
        private AtexoEtablissement $atexoEtablissement,
        private AtexoInscrit $atexoInscrit,
        private OrganismeService $organismeService,
        private ModificationContratService $modificationContratService,
        private AtexoUtil $atexoUtil,
        private TableauDeBordService $tableauDeBordService
    ) {
        parent::__construct(
            $em,
            $agentTechniqueTokenService,
            $apiSerializer,
            $agent,
            $atexoEntreprise,
            $atexoEtablissement,
            $atexoInscrit,
            $organismeService,
            $modificationContratService,
            $atexoUtil
        );
    }

    /**
     * @Operation(
     *     tags={"/tableau-de-bord"},
     *     summary="Tableau de bord : consultations en cours",
     *     @SWG\Parameter(
     *         name="loginAgent",
     *         in="body",
     *         description="Login de l'agent",
     *         required=false,
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     * @return Response
     */
    #[Route(path: '/consultations-en-cours.{format}', name: 'atexo_api_tb_consultationencours', defaults: ['format' => 'xml'], methods: ['GET'])]
    public function consultationsEnCoursAction(Request $request)
    {
        /**
         * @var TableauDeBordService $tableauDeBordService
         */
        $tableauDeBordService = $this->tableauDeBordService;
        $consultationsEncours = $tableauDeBordService->getConsultationsEnCours($request);
        return $this->createApiResponse(
            $consultationsEncours,
            $request->get('format', 'xml'),
            'api/consultationsEnCours.xml.twig'
        );
    }
    /**
     * @Operation(
     *     tags={"/tableau-de-bord"},
     *     summary="Tableau de bord : questions",
     *     @SWG\Parameter(
     *         name="loginAgent",
     *         in="body",
     *         description="Login de l'agent",
     *         required=false,
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     * @return Response
     */
    #[Route(path: '/questions.{format}', name: 'atexo_api_tb_questions', defaults: ['format' => 'xml'], methods: ['GET'])]
    public function questionsAction(Request $request)
    {
        /**
         * @var TableauDeBordService $tableauDeBordService
         */
        $tableauDeBordService = $this->tableauDeBordService;
        $questions = $tableauDeBordService->getQuestions($request);
        return $this->createApiResponse(
            $questions,
            $request->get('format', 'xml'),
            'api/questions.xml.twig'
        );
    }
    /**
     * @Operation(
     *     tags={"/tableau-de-bord"},
     *     summary="Tableau de bord : contrats",
     *     @SWG\Parameter(
     *         name="loginAgent",
     *         in="body",
     *         description="Login de l'agent",
     *         required=false,
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     * @return Response
     *
     * @throws Exception
     */
    #[Route(path: '/contrats.{format}', name: 'atexo_api_tb_contrats', defaults: ['format' => 'xml'], methods: ['GET'])]
    public function contratsAction(Request $request)
    {
        /**
         * @var TableauDeBordService $tableauDeBordService
         */
        $tableauDeBordService = $this->tableauDeBordService;
        $contrats = $tableauDeBordService->getContrats($request);
        return $this->createApiResponse(
            $contrats,
            $request->get('format', 'xml'),
            'api/contrats.xml.twig'
        );
    }
}
