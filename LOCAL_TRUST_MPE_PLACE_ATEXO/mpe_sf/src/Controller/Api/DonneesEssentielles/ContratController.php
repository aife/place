<?php

namespace App\Controller\Api\DonneesEssentielles;

use App\Service\AgentTechniqueTokenService;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\Api\StatistiqueService;
use App\Service\AtexoAgent;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\AtexoUtil;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use App\Service\WebServices\WebServicesExec;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use DateTime;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use App\Controller\Api\AbstractWebserviceController;
use App\Model\DonneesEssentielles\Marches;
use App\Entity\SsoTiers;
use App\Exception\ApiProblemException;
use App\Exception\ApiProblemInvalidTokenException;
use App\Model\ApiProblem;
use App\Service\ContratService;
use App\Utils\Utils;
use FOS\RestBundle\Controller\Annotations as Rest;
use GoetasWebservices\Xsd\XsdToPhpRuntime\Jms\Handler\BaseTypesHandler;
use GoetasWebservices\Xsd\XsdToPhpRuntime\Jms\Handler\XmlSchemaDateHandler;
use JMS\Serializer\Handler\DateHandler;
use JMS\Serializer\Handler\HandlerRegistryInterface;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\DonneesEssentielles\Marche;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use App\Model\DonneesEssentielles\Marche\TitulairesAType\TitulaireAType;

/**
 * Class ContratController.
 **/
#[Route(
    path: '/api/{version}/donnees-essentielles/contrat',
    name: 'atexo_api_donnees_essentielles',
    requirements: ["version" => "^(?:(?!v2).)+$"]
)]
class ContratController extends AbstractWebserviceController
{
    private $service;

    public function __construct(
        private EntityManagerInterface $em,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private ApiSerializer $apiSerializer,
        private AtexoAgent $agent,
        private AtexoEntreprise $atexoEntreprise,
        private AtexoEtablissement $atexoEtablissement,
        private AtexoInscrit $atexoInscrit,
        private OrganismeService $organismeService,
        private ModificationContratService $modificationContratService,
        private AtexoUtil $atexoUtil,
        private StatistiqueService $statistiqueService,
        private ContratService $contratService,
        private ParameterBagInterface $parameterBag
    ) {
        parent::__construct(
            $em,
            $agentTechniqueTokenService,
            $apiSerializer,
            $agent,
            $atexoEntreprise,
            $atexoEtablissement,
            $atexoInscrit,
            $organismeService,
            $modificationContratService,
            $atexoUtil
        );
    }

    /**
     * @Operation(
     *     tags={"/donnees-essentielles/contrat"},
     *     summary="Return all contracts in the given format",
     *     @SWG\Parameter(
     *         name="format",
     *         in="formData",
     *         description="format expected for response",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="date_notif_min",
     *         in="formData",
     *         description="minimum notification date of objects to be returned",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="date_notif_max",
     *         in="formData",
     *         description="maximum notification date of objects to be returned",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="type_donnees_essentielles",
     *         in="formData",
     *         description="essential data type of objects to be returned",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="siren",
     *         in="formData",
     *         description="siren of objects to be returned",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="complement",
     *         in="formData",
     *         description="complement of objects to be returned",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="date_derniere_modif",
     *         in="formData",
     *         description="minimum modification date of objects to be returned",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="statut_publication_sn",
     *         in="formData",
     *         description="statut publication au sn (0: non publié ; 1: publié)",
     *         required=false,
     *         type="boolean"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * @Rest\View()
     * @Rest\Post("/format-pivot")
     *
     * @param string $format
     * @param null   $essentialDataType
     * @param string $siren
     * @param string $complement
     * @param string $dateDerniereModif
     *
     * @return Response
     *
     * @throws Exception
     */
    public function formatPivotAction(Request $request)
    {
        return $this->getResponse($request, false);
    }

    /**
     * Récupération des données essentielles au format pivot ou étendu.
     *
     * @param $request
     * @param $isExtendedFormat
     *
     * @return Response
     */
    public function getResponse($request, $isExtendedFormat)
    {
        $parameters = $this->getFormatedParameters($request);
        $contratService = $this->getService();

        $agentTechniqueTokenService = $this->agentTechniqueTokenService;
        $agentTechnique = $agentTechniqueTokenService->getAgentFromToken($request->get('token'));
        $orgServicesAllowed = $agentTechniqueTokenService->getAllOrganismesServicesAllowed($agentTechnique);
        $contrats = $contratService->getContracts($parameters, $orgServicesAllowed);
        $marches = $this->getMarchesFromContrats($contrats, $isExtendedFormat, $parameters['statutPublicationSn']);
        $formatKey = 'format';
        $serializedData = $this->serializeMarches($marches, $parameters[$formatKey]);

        return $this->getFormatedResponse($serializedData, $parameters[$formatKey]);
    }

    /**
     * Vérifie la validité d'un ticket (anciennement token).
     *
     * @param $ticket
     */
    public function isValidTicket($ticket)
    {
        $check = $this->getEm()
            ->getRepository(SsoTiers::class)
            ->checkToken($ticket);

        if (false === $check) {
            throw new ApiProblemInvalidTokenException();
        }
    }

    /**
     * @param mixed $em
     */
    public function setEm($em)
    {
        $this->em = $em;
    }

    /**
     * Formatage des paramètres dans un tableau pour faciliter et factoriser le traitement.
     *
     * @param $request
     *
     * @return array
     *
     * @throws Exception
     */
    protected function getFormatedParameters($request)
    {
        $siren = null;
        $complement = null;
        $dateNotifMin = null;
        $dateNotifMax = null;
        $dateDerniereModif = null;
        $dateFormat = 'd-m-Y';
        $limit = $this->parameterBag->get('DONNEES_ESSENTIELLES_LIMIT');
        if (null === $request->request->get('date_notif_min')) {
            $dateNotifMin = new DateTime('now');
            $dateNotifMin->modify('-4 year');
        } else {
            $limit = null;
            $dateNotifMin = $request->request->get('date_notif_min');
            if (false === (new Utils())->validateDate($dateNotifMin, $dateFormat)) {
                $problem = new ApiProblem(
                    400,
                    ApiProblem::TYPE_INVALID_ARGUMENT
                );
                throw new ApiProblemException($problem);
            }

            $dateNotifMin = new DateTime($dateNotifMin);
            if ($dateNotifMin > new DateTime('now')) {
                $dateNotifMin = null;
            }
        }

        if (null === $request->request->get('date_notif_max')) {
            $dateNotifMax = new DateTime('now');
        } else {
            $limit = null;
            $dateNotifMax = $request->request->get('date_notif_max');
            if (false === (new Utils())->validateDate($dateNotifMax, $dateFormat)) {
                $problem = new ApiProblem(
                    400,
                    ApiProblem::TYPE_INVALID_ARGUMENT
                );
                throw new ApiProblemException($problem);
            }
            $dateNotifMax = new DateTime($dateNotifMax);
            if ($dateNotifMax > new DateTime('now')) {
                $dateNotifMax = new DateTime('now');
            }
        }

        if (null !== $request->request->get('siren')) {
            $siren = $request->request->get('siren');

            if (null !== $request->request->get('complement')) {
                $complement = $request->request->get('complement');
            }
        }

        if (null !== $request->request->get('date_derniere_modification')) {
            $limit = null;
            $dateDerniereModif = $request->request->get('date_derniere_modification');
            if (false === (new Utils())->validateDate($dateDerniereModif, $dateFormat)) {
                $problem = new ApiProblem(
                    400,
                    ApiProblem::TYPE_INVALID_ARGUMENT
                );
                throw new ApiProblemException($problem);
            }
            $dateDerniereModif = new DateTime($dateDerniereModif);
        }

        $statutPublicationSn = $request->request->get('statut_publication_sn');
        $formatKey = 'format';
        $format = $request->request->get($formatKey, 'xml');
        $exclusionOrganisme = $this->parameterBag->get('exclusionOrganisme');

        $parameters = [
            $formatKey => $format,
            'dateNotifMin' => $dateNotifMin,
            'dateNotifMax' => $dateNotifMax,
            'siren' => $siren,
            'complement' => $complement,
            'dateDerniereModif' => $dateDerniereModif,
            'statutPublicationSn' => $statutPublicationSn,
            'exclusionOrganisme' => $exclusionOrganisme,
            'limit' => $limit,
        ];

        return $parameters;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        if (empty($this->service)) {
            return $this->contratService;
        }

        return $this->service;
    }

    /**
     * @param mixed $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * Récupération des marchés à partir d'une liste de contrats.
     *
     * @param $contrats
     * @param $isExtendedFormat
     *
     * @return Marches
     */
    protected function getMarchesFromContrats($contrats, $isExtendedFormat, $statutPublicationSn)
    {
        $marches = new Marches([]);

        foreach ($contrats as $contrat) {
            $data = $this->backUntoTheFlow($statutPublicationSn, $contrat);
            if (count($data) > 0) {
                $lists = $this->getService()->listMarcheForContrat($data, $isExtendedFormat, $statutPublicationSn);

                foreach ($lists as $list) {
                    $this->getService()->displayMontant($data['contrat'], $list);
                    $marches->addMarche($list);
                }
            }
        }

        return $marches;
    }

    /**
     * @param int $sn
     */
    protected function backUntoTheFlow(?int $sn, array $contrats): array
    {
        /**
         * @var ContratTitulaire $contrat
         */
        $contrat = $contrats['contrat'];
        $avenants = $contrats['modifications'] ?? [];
        $data = [];
        if ($sn === 0) {
            if (
                ($contrat->getStatutPublicationSn() === 0 && empty($avenants))
                || ($contrat->getStatutPublicationSn() === 1 && is_countable($avenants) && count($avenants) > 0)
            ) {
                $data = $contrats;
            } elseif (
                ($contrat->getStatutPublicationSn() === 0)
                && (is_countable($avenants) && count($avenants) > 0)
            ) {
                unset($contrats['modifications']);
                $data = $contrats;
            }
        } else {
            $data = $contrats;
        }

        return $data;
    }

    /**
     * Sérialisation des objets marches.
     *
     * @param $marches
     * @param $format
     *
     * @return mixed|string
     */
    protected function serializeMarches($marches, $format)
    {
        $serializerBuilder = SerializerBuilder::create();
        $serializerBuilder->setPropertyNamingStrategy(new IdenticalPropertyNamingStrategy());
        $serializerBuilder->addMetadataDir(
            __DIR__ . '/../../../Model/DonneesEssentielles/Metadata',
            'App\Model\DonneesEssentielles'
        );
        $serializerBuilder->configureHandlers(function (HandlerRegistryInterface $handler) use ($serializerBuilder) {
            $serializerBuilder->addDefaultHandlers();
            $handler->registerSubscribingHandler(new DateHandler('Y-m-d'));
            $handler->registerSubscribingHandler(new BaseTypesHandler()); // XMLSchema List handling
            $handler->registerSubscribingHandler(new XmlSchemaDateHandler()); // XMLSchema date handling
        });

        $serializer = $serializerBuilder->build();

        return $serializer->serialize($marches, $format);
    }

    /**
     * Création de l'objet response au bon format.
     *
     * @param $data
     * @param $format
     *
     * @return Response
     */
    protected function getFormatedResponse($data, $format)
    {
        $response = new Response();
        $response->setContent($data);
        $response->headers->set('Content-Type', 'application/' . $format);

        return $response;
    }

    /**
     * @Operation(
     *     tags={"/donnees-essentielles/contrat"},
     *     summary="Return all extended contracts in the given format",
     *     @SWG\Parameter(
     *         name="format",
     *         in="formData",
     *         description="format expected for response",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="date_notif_min",
     *         in="formData",
     *         description="minimum notification date of objects to be returned",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="date_notif_max",
     *         in="formData",
     *         description="maximum notification date of objects to be returned",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="type_donnees_essentielles",
     *         in="formData",
     *         description="essential data type of objects to be returned",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="siren",
     *         in="formData",
     *         description="siren of objects to be returned",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="complement",
     *         in="formData",
     *         description="complement of objects to be returned",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="date_derniere_modif",
     *         in="formData",
     *         description="minimum modification date of objects to be returned",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="statut_publication_sn",
     *         in="formData",
     *         description="statut publication au sn (0: non publié ; 1: publié)",
     *         required=false,
     *         type="boolean"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * @Rest\View()
     * @Rest\Post("/format-etendu")
     *
     * @param string $format
     * @param string $essentialDataType
     * @param string $siren
     * @param string $complement
     * @param string $dateDerniereModif
     *
     * @return Response
     *
     * @throws Exception
     */
    public function formatEtenduAction(
        Request $request,
        WebServicesExec $webServicesExec,
        DenormalizerInterface $normalizer
    ) {
        //return $this->getResponse($request, true);

        $parameters = $this->getFormatedParametersExec($request);

        $contrats = $webServicesExec->getContent('getContrats', $parameters, true);

        $marches = new Marches([]);

        $this->getService()->normalizeForExec($contrats, $normalizer, $marches);

        $serializedData = $this->serializeMarches($marches, 'xml');

        return $this->getFormatedResponse($serializedData, 'xml');
    }

    protected function getFormatedParametersExec(Request $request): array
    {
        $parameters = $this->getFormatedParameters($request);

        if (array_key_exists('dateNotifMin', $parameters)) {
            $parameters['dateNotificationMin'] = $parameters['dateNotifMin']->format('Y-m-d');
            unset($parameters['dateNotifMin']);
        }
        if (array_key_exists('dateNotifMax', $parameters)) {
            $parameters['dateNotificationMax'] = $parameters['dateNotifMax']->format('Y-m-d');
            unset($parameters['dateNotifMax']);
        }

        $parameters['itemPerPage'] = $parameters['limit'];
        if ($parameters['dateDerniereModif']) {
            $parameters['dateModification'] = $parameters['dateDerniereModif'];
        }
        $agentTechniqueTokenService = $this->agentTechniqueTokenService;
        $agentTechnique = $agentTechniqueTokenService->getAgentFromToken($request->get('token'));
        $orgServicesAllowed = $agentTechniqueTokenService->getAllOrganismesServicesAllowed($agentTechnique);
        $parameters['defenseOuSecurite'] = false;
        if (!empty($orgServicesAllowed)) {
            $parameters['idServices'] = [];
            foreach ($orgServicesAllowed as $orgServiceAllowed) {
                $parameters['idExternesService'] = array_merge($parameters['idServices'], $orgServiceAllowed);
            }
            $parameters['acronymesOrganisme'] = array_keys($orgServicesAllowed);
        }

        return $parameters;
    }
}
