<?php

namespace App\Controller\Api\DonneesEssentielles;

use App\Service\AgentTechniqueTokenService;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\Api\StatistiqueService;
use App\Service\AtexoAgent;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\AtexoUtil;
use App\Service\ContratService;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use App\Entity\ContratTitulaire;
use App\Model\DonneesEssentielles\Marches;
use App\Entity\MarchePublie;
use App\Entity\Service;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ContratTokenlessController.
 */
class ContratTokenlessController extends ContratController
{
    public function __construct(
        private EntityManagerInterface $em,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private ApiSerializer $apiSerializer,
        private AtexoAgent $agent,
        private AtexoEntreprise $atexoEntreprise,
        private AtexoEtablissement $atexoEtablissement,
        private AtexoInscrit $atexoInscrit,
        private OrganismeService $organismeService,
        private ModificationContratService $modificationContratService,
        private AtexoUtil $atexoUtil,
        private StatistiqueService $statistiqueService,
        private ContratService $contratService,
        private ParameterBagInterface $parameterBag
    ) {
        parent::__construct(
            $em,
            $agentTechniqueTokenService,
            $apiSerializer,
            $agent,
            $atexoEntreprise,
            $atexoEtablissement,
            $atexoInscrit,
            $organismeService,
            $modificationContratService,
            $atexoUtil,
            $statistiqueService,
            $contratService,
            $parameterBag
        );

        $this->needsHabilitation = false;
    }

    /**
     * @Method("GET")
     * @param string $prefix
     * @param $idEntitePublique
     * @param $idEntiteAchat
     * @param $inclureDescendances
     * @param $annee
     * @param $nomAttributaire
     * @param $idCategorie
     * @param $montantMin
     * @param $montantMax
     * @param $lieuxExecution
     * @param $codesCpv
     * @param $dateNotifMin
     * @param $dateNotifMax
     * @param $motsCles
     * @param int $concession
     * @return Response
     */
    #[Route(path: '/api/v1/donnees-essentielles/contrat/{prefix}/{idEntitePublique}/{idEntiteAchat}/{inclureDescendances}/{annee}/{nomAttributaire}/{idCategorie}/{montantMin}/{montantMax}/{lieuxExecution}/{codesCpv}/{dateNotifMin}/{dateNotifMax}/{motsCles}/{concession}', name: 'atexo_api_donnees_essentielles_xml_extraire_criteres', defaults: ['prefix' => null], requirements: ['prefix' => 'xml-extraire-criteres|extraire-criteres'])]
    public function extraireAvecCriteres($idEntitePublique, $idEntiteAchat, $inclureDescendances, $annee, $nomAttributaire, $idCategorie, $montantMin, $montantMax, $lieuxExecution, $codesCpv, $dateNotifMin, $dateNotifMax, $motsCles, $concession = 0, $prefix = null)
    {
        $entityPurchaseArray = [];
        try {
            $services = $this->em
                ->getRepository(Service::class)
                ->findBy(
                    ['organisme' => $idEntitePublique],
                    ['cheminComplet' => 'ASC']
                );

            if ($inclureDescendances) {
                if ($idEntiteAchat) {
                    $unserializedServices = unserialize($idEntiteAchat);
                    if (is_array($unserializedServices) && count($unserializedServices) > 1) {
                        $entityPurchaseArray = $unserializedServices;
                    } else {
                        $entityPurchaseArray = $this->getService()->getSubServices(
                            $idEntiteAchat,
                            $idEntitePublique,
                            true
                        );
                        $entityPurchaseArray[] = $idEntiteAchat;
                    }
                } else {
                    $entityPurchaseArray = [];

                    foreach ($services as $service) {
                        $entityPurchaseArray[] = $service->getId();
                    }

                    $entityPurchaseArray[] = $idEntiteAchat;
                }
            } else {
                $entityPurchaseArray[] = $idEntiteAchat;
            }

            $listeMarchesPublies = $this->em
                ->getRepository(MarchePublie::class)
                ->getListeMarchesPublies(
                    $idEntitePublique,
                    $entityPurchaseArray,
                    $annee,
                    (bool) $idEntiteAchat
                );
            $marches = new Marches([]);
            if ($listeMarchesPublies && is_array($listeMarchesPublies)) {
                $arrayIdsServices = [];

                foreach ($listeMarchesPublies as $marchePublie) {
                    $arrayIdsServices[] = $marchePublie->getserviceId();
                }

                $contrats = $this->em
                    ->getRepository(ContratTitulaire::class)
                    ->getMarchesByCriteria(
                        $idEntitePublique,
                        $arrayIdsServices,
                        $annee,
                        $nomAttributaire,
                        $idCategorie,
                        $montantMin,
                        $montantMax,
                        $lieuxExecution,
                        $codesCpv,
                        $dateNotifMin,
                        $dateNotifMax,
                        $motsCles,
                         $this->parameterBag->get('HTTP_ENCODING'),
                        $concession
                    );

                if ('xml-extraire-criteres' === $prefix) {
                    foreach ($contrats as $contrat) {
                        $marche = $this->getService()->getFormatPivot($contrat['contrat']);
                        $this->getService()->displayMontant($contrat['contrat'], $marche);
                        if (isset($contrat['modifications'])) {
                            $marche = $this->getService()->setModificationContratForMarche(
                                $contrat['modifications'],
                                $marche
                            );
                        }
                        if ('1' == $concession) {
                            $marche = $this->getService()->setMarcheOrConcession($contrat['contrat'], $marche);
                        }
                        $marches->addMarche($marche);
                    }
                } else {
                    foreach ($contrats as $contrat) {
                        $lists = $this->getService()->listMarcheForContratCriteres($contrat, false);
                        if (!is_null($lists)) {
                            foreach ($lists as $list) {
                                $marches->addMarche($list);
                            }
                        }
                    }
                }
            }

            $newXml = $this->serializeMarches($marches, 'xml');

            return $this->getFormatedResponse($newXml, 'xml');
        } catch (Exception $exception) {
            $response = new Response();
            $response->headers->set('Content-Type', 'xml');
            $response->headers->set('Content-Disposition', 'attachment; filename=marches.xml');
            $response->setContent('<erreur>'.$exception->getMessage().'</erreur>');

            return $response;
        }
    }
}
