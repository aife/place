<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Admin;

use App\Entity\Organisme;
use App\Entity\OrganismeServiceMetier;
use App\Form\Admin\OrganismeType;
use App\Service\Admin\HandleUploadLogoOrganisme;
use App\Service\Admin\ServiceMetierOrganisme;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/admin', name: 'admin_')]
class OrganismeController extends AbstractController
{
    #[Route('/organisme/gestion', name: 'gestion_organisme')]
    public function gestion(
        Request $request,
        ParameterBagInterface $parameterBag,
        EntityManagerInterface $entityManager,
        HandleUploadLogoOrganisme $handleUploadLogoOrganisme,
        ServiceMetierOrganisme $serviceMetierOrganisme
    ): Response {
        /** @var Organisme $organisme */
        $organisme = $this->getUser()->getOrganisme();

        $path = $parameterBag->get('BASE_ROOT_DIR')
            . '/'
            . $organisme->getAcronyme()
            . $parameterBag->get('PATH_ORGANISME_IMAGE')
        ;

        $form = $this->createForm(OrganismeType::class, $organisme, [
            'directory_logo' => $path
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $handleUploadLogoOrganisme->upload($form, $path);
            $entityManager->persist($organisme);
            $entityManager->flush();
        }

        return $this->render('admin/organisme/index.html.twig', [
            'form' => $form->createView(),
            'acronyme' => $organisme->getAcronyme(),
            'path_picture' => $path,
            'services' => $serviceMetierOrganisme->getAccessibleServices($organisme, false)
        ]);
    }
}
