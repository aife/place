<?php

namespace App\Controller\SuperAdmin;

use Knp\Component\Pager\Paginator;
use App\Controller\AtexoController;
use App\Entity\ConfigurationMessagesTraduction;
use App\Entity\HabilitationAdministrateur;
use App\Form\TraductionType;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/superadmin/traduction', name: 'superadmin_traduction_')]
class ConfigTraductionController extends AtexoController
{
    /**
     * ConfigBddController constructor.
     */
    public function __construct(protected EntityManagerInterface $em, private PaginatorInterface $paginator)
    {
    }
    #[Route(path: '/', name: 'index', methods: ['GET', 'POST'])]
    public function index(Request $request) : Response
    {
        if (
            !$this->getUser()->hasHabilitationAdministrateur(
                HabilitationAdministrateur::ACCESS_ADMINISTRATION_PANELS
            )
        ) {
            return $this->redirectToRoute('superadmin_interface_suivi');
        }

        $term = $request->request->get('term');
        if (!empty($term)) {
            $page = 1;
        } else {
            $page = $request->query->get('page', 1);
        }
        $parametres = $this->em->getRepository(ConfigurationMessagesTraduction::class)->msgTradConfigFiltre($term);
        $limiteParPage = 15;
        $paginator = $this->paginator;
        $pagination = $paginator->paginate(
            $parametres,
            $page,
            $limiteParPage
        );
        return $this->render('superadmin/traduction/index.html.twig', [
            'traductions' => $pagination,
        ]);
    }
    #[Route(path: '/new', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request) : Response
    {
        $traduction = new ConfigurationMessagesTraduction();
        $form = $this->createForm(TraductionType::class, $traduction);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($traduction);
            $this->em->flush();

            return $this->redirectToRoute('superadmin_traduction_index');
        }
        return $this->render('superadmin/traduction/new.html.twig', [
            'traduction' => $traduction,
            'form' => $form->createView(),
        ]);
    }
    #[Route(path: '/{id}/edit', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ConfigurationMessagesTraduction $traduction) : Response
    {
        $form = $this->createForm(TraductionType::class, $traduction);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $this->redirectToRoute('superadmin_traduction_index');
        }
        return $this->render('superadmin/traduction/edit.html.twig', [
            'traduction' => $traduction,
            'form' => $form->createView(),
        ]);
    }
    #[Route(path: '/{id}', name: 'delete', methods: ['DELETE'])]
    public function delete(Request $request, ConfigurationMessagesTraduction $traduction) : Response
    {
        if ($this->isCsrfTokenValid('delete' . $traduction->getSource(), $request->request->get('_token'))) {
            $this->em->remove($traduction);
            $this->em->flush();
        }
        return $this->redirectToRoute('superadmin_traduction_index');
    }
}
