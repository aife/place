<?php

namespace App\Controller\SuperAdmin;

use DateTime;
use App\Controller\AtexoController;
use App\Entity\ConfigurationClient;
use App\Entity\HabilitationAdministrateur;
use App\Form\ConfigClientType;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/superadmin/configsclients', name: 'superadmin_configsclients_')]
class ConfigClientController extends AtexoController
{
    /**
     * ConfigClientController constructor.
     */
    public function __construct(protected EntityManagerInterface $em, private PaginatorInterface $paginator)
    {
    }

    #[Route(path: '/', name: 'index', methods: ['GET', 'POST'])]
    public function index(Request $request) : Response
    {
        if (
            !$this->getUser()->hasHabilitationAdministrateur(
                HabilitationAdministrateur::ACCESS_ADMINISTRATION_PANELS
            )
        ) {
            return $this->redirectToRoute('superadmin_interface_suivi');
        }

        $term = $request->request->get('term');
        if (!empty($term)) {
            $page = 1;
        } else {
            $page = $request->query->get('page', 1);
        }
        $limiteParPage = $request->query->get('limite', 15);
        $parametres = $this->em->getRepository(ConfigurationClient::class)->paramConfigFiltre($term);
        $paginator = $this->paginator;
        $pagination = $paginator->paginate(
            $parametres,
            $page,
            $limiteParPage
        );
        return $this->render(
            'superadmin/configclient/index.html.twig',
            [
                'configsclients' => $pagination,
            ]
        );
    }
    #[Route(path: '/new', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request) : Response
    {
        $configClient = new ConfigurationClient();
        $configClient->setUpdatedAt(new DateTime());
        $form = $this->createForm(ConfigClientType::class, $configClient);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $a = new DateTime('NOW');

            $configClient->setUpdatedAt($a);
            $this->em->persist($configClient);
            $this->em->flush();

            return $this->redirectToRoute('superadmin_configsclients_index');
        }
        return $this->render(
            'superadmin/configclient/new.html.twig',
            [
                'configClient' => $configClient,
                'form' => $form->createView(),
            ]
        );
    }
    #[Route(path: '/{id}/edit', name: 'edit', methods: ['GET', 'POST'])]
    public function editConfigsClients(Request $request, ConfigurationClient $configClient) : Response
    {
        $form = $this->createForm(ConfigClientType::class, $configClient);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $configClient->setUpdatedAt(new DateTime());
            $this->em->persist($configClient);
            $this->em->flush();

            return $this->redirectToRoute('superadmin_configsclients_index');
        }
        return $this->render(
            'superadmin/configclient/edit.html.twig',
            [
                'configClient' => $configClient,
                'form' => $form->createView(),
            ]
        );
    }
    #[Route(path: '/{id}', name: 'delete', methods: ['DELETE'])]
    public function delete(Request $request, ConfigurationClient $configClient) : Response
    {
        if ($this->isCsrfTokenValid('delete' . $configClient->getParameter(), $request->request->get('_token'))) {
            $this->em->remove($configClient);
            $this->em->flush();
        }
        return $this->redirectToRoute('superadmin_configsclients_index');
    }
}
