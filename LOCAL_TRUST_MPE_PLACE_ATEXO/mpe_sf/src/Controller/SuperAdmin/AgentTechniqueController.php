<?php

namespace App\Controller\SuperAdmin;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormInterface;
use Exception;
use DateTime;
use Doctrine\ORM\Exception\ORMException;
use App\Controller\AtexoController;
use App\Entity\Agent;
use App\Entity\HabilitationAdministrateur;
use App\Entity\HabilitationAgent;
use App\Entity\Service;
use App\Entity\WS\AgentTechniqueAssociation;
use App\Form\AgentTechAssociationType;
use App\Form\AgentType;
use App\Form\Habilitation\HabilitationAgentType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

#[Route(path: '/superadmin/agent-technique', name: 'superadmin_agenttechnique_')]
class AgentTechniqueController extends AtexoController
{
    final public const HABILITATIONS_AGENT = [
        [
            'id'        => 'creer_consultation',
            'label'     => 'Créer une consultation',
            'method'    => 'CreerConsultation'
        ],
        [
            'id'        => 'modifier_consultation_avant_validation',
            'label'     => 'Modifier une consultation avant validation',
            'method'    => 'ModifierConsultationAvantValidation'
        ]
    ];

    /**
     * ConfigClientController constructor.
     */
    public function __construct(
        protected EntityManagerInterface $em,
        private PaginatorInterface $paginator,
        private ParameterBagInterface $parameterBag,
        private UserPasswordHasherInterface $passwordHasher
    ) {
    }

    #[Route(path: '/', name: 'administration', methods: ['GET', 'POST'])]
    public function administrationAction(Request $request): Response
    {
        if (
            !$this->getUser()->hasHabilitationAdministrateur(
                HabilitationAdministrateur::ACCESS_ADMINISTRATION_PANELS
            )
        ) {
            return $this->redirectToRoute('superadmin_interface_suivi');
        }

        $dql = 'SELECT a FROM ' . Agent::class . ' as a where a.technique = 1';
        $query = $this->em->createQuery($dql);

        $limiteParPage = 15;
        $paginator = $this->paginator;
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            $limiteParPage
        );

        return $this->render('superadmin/agents/listes.html.twig', ['agents' => $pagination]);
    }
    #[Route(path: '/new', name: 'administration_new', methods: ['GET', 'POST'])]
    public function administrationAgentNewAction(Request $request): Response
    {
        $agent = new Agent();
        $form = $this->save($agent, $request, true);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('superadmin_agenttechnique_administration');
        }

        return $this->render('superadmin/agents/new.html.twig', ['agent' => $agent, 'form' => $form->createView()]);
    }

    /**
     * @param bool $new
     * @return FormInterface
     * @throws Exception
     */
    protected function save(Agent $agent, Request $request, $new = false)
    {
        $dateTime = new DateTime();
        $oldPassWord = $agent->getPassword();
        $options = [];
        $options['new'] = $new;
        $form = $this->createForm(AgentType::class, $agent, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($agent->getOrganisme()) {
                if ($agent->getService()) {
                    $agent->setServiceId($agent->getService()->getId());
                } else {
                    $agent->setService(null);
                }
            } else {
                $agent->setOrganisme(null);
                $agent->setService(null);
            }
            $agent->setIdProfilSocleExterne(0);
            $agent->setTechnique(true);

            if (null === $agent->getDateCreation()) {
                $date = $dateTime->format('Y:m:d H:i:s');
                $agent->setDateCreation($date);
            }

            if (null === $agent->getDateModification()) {
                $date = $dateTime->format('Y:m:d H:i:s');
                $agent->setDateModification($date);
            }

            if ('**********' != $agent->getPassword()) {
                $agent->setPassword($this->passwordHasher->hashPassword($agent, $agent->getPassword()));
            } else {
                $agent->setPassword($oldPassWord);
            }
            $agent->setTypeHash('ARGON2');
            $this->em->persist($agent);
            $this->em->flush();
            if (true === $new) {
                $habiltationAgent = new HabilitationAgent();
                $habiltationAgent->setAgent($agent);
                $this->em->persist($habiltationAgent);
            }

            $this->em->flush();
        }

        return $form;
    }
    #[Route(
        path: '/edit/{idAgent}',
        name: 'administration_edit',
        requirements: ['idAgent' => '\d+'],
        methods: ['GET', 'POST']
    )]
    public function administrationAgentEditAction(Request $request): Response
    {
        $idAgent = $request->get('idAgent');
        $agent = $this->em->getRepository(Agent::class)->findOneBy(['id' => $idAgent, 'technique' => 1]);
        if (0 === $agent->getServiceId()) {
            $agent->setService(null);
        }
        $form = $this->save($agent, $request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('superadmin_agenttechnique_administration');
        }

        return $this->render(
            'superadmin/agents/edit.html.twig',
            [
                'agent' => $agent,
                'form' => $form->createView(),
            ]
        );
    }
    #[Route(path: '/list-service', name: 'administration_liste_service', methods: ['GET'])]
    public function listServiceOfOrganismeAction(Request $request)
    {
        // Get Entity manager and repository
        $serviceRepository = $this->em->getRepository(Service::class);

        // Search the service that belongs to the organisme with the given id as GET parameter "acronyme"
        $services = $serviceRepository->createQueryBuilder('s')
            ->where('s.organisme = :organisme')
            ->setParameter('organisme', $request->get('organisme'))
            ->getQuery()
            ->getResult();

        // Serialize into an array the data that we need, in this case only name and id
        // Note: you can use a serializer as well, for explanation purposes, we'll do it manually
        $responseArray = [];
        foreach ($services as $service) {
            $responseArray[] = [
                'id' => $service->getId(),
                'name' => $service->getLibelle(),
            ];
        }

        // Return array with structure of the service of the providen organisme
        return new JsonResponse($responseArray);
    }

    #[Route(
        path: '/changer-statut-agent/{idAgent}',
        name: 'administration_change_agent_status',
        requirements: ['idAgent' => '\d+'],
        methods: ['GET']
    )]
    public function changeAgentStatusAction(Request $request): RedirectResponse|Response
    {
        $idAgent = $request->get('idAgent');
        $agent = $this->em->getRepository(Agent::class)->findOneBy(['id' => $idAgent, 'technique' => 1]);
        $agent->switchActif();
        $this->em->flush();
        if (false === $request->isXmlHttpRequest()) {
            $response = $this->redirectToRoute('superadmin_agenttechnique_administration');
        } else {
            $response = new Response(
                json_encode(
                    [
                        'status' => $agent->getActif(),
                    ],
                    JSON_THROW_ON_ERROR
                )
            );
            $response->headers->set('Content-Type', 'application/json');
        }

        return $response;
    }

    #[Route(
        path: '/habilitation/{idAgent}',
        name: 'administration_habilitation_agent',
        requirements: ['idAgent' => '\d+'],
        methods: ['GET', 'POST']
    )]
    public function habilitationAgentAction(Request $request): RedirectResponse|Response
    {
        $idAgent = $request->get('idAgent');
        $agent = $this->em->getRepository(Agent::class)->findOneBy(['id' => $idAgent, 'technique' => 1]);
        $habilitationAgent = $this->em->getRepository(
            HabilitationAgent::class
        )->findOneBy(
            ['agent' => $idAgent]
        );

        $newHabilitations = self::HABILITATIONS_AGENT;
        foreach ($newHabilitations as $key => $habilitation) {
            $method = "get{$habilitation['method']}";
            $newHabilitations[$key]['checked'] = !empty($habilitationAgent?->$method());
        }

        if (null === $habilitationAgent) {
            $habilitationAgent = new HabilitationAgent();
            $habilitationAgent->setAgent($agent);
        }
        $form = $this->createForm(HabilitationAgentType::class, $agent);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $habilitationAgent->setAccesWs(false);
            if ((is_countable($agent->getWebservices()) ? count($agent->getWebservices()) : 0) > 0) {
                $habilitationAgent->setAccesWs(true);
            }

            $this->em->persist($habilitationAgent);
            $this->em->persist($agent);
            $this->em->flush();

            return $this->redirectToRoute('superadmin_agenttechnique_administration');
        }

        return $this->render(
            'superadmin/agents/habilitation.html.twig',
            [
                'form'                  => $form->createView(),
                'newHabilitations'      => $newHabilitations,
                'agent'                 => $agent
            ]
        );
    }

    /**
     * Habilitations utilisées pour Api Platform
     **/
    #[Route(
        path: '/habilitation-agent/{idAgent}',
        name: 'administration_update_habilitations',
        requirements: ['idAgent' => '\d+'],
        methods: ['POST']
    )]
    public function updateHabilitations(Request $request, CsrfTokenManagerInterface $csrfTokenManager): RedirectResponse
    {
        $token = new CsrfToken(
            'superadmin_agenttechnique_administration_update_habilitations',
            $request->query->get('_csrf_token')
        );

        if (!$csrfTokenManager->isTokenValid($token)) {
            throw $this->createAccessDeniedException('Token CSRF invalide');
        }

        $idAgent = $request->get('idAgent');
        $agent = $this->em->getRepository(Agent::class)->findOneBy(['id' => $idAgent, 'technique' => 1]);
        $habilitationAgent = $this->em->getRepository(HabilitationAgent::class)->findOneBy(
            ['agent' => $idAgent]
        );
        if (empty($habilitationAgent)) {
            $habilitationAgent = new HabilitationAgent();
            $habilitationAgent->setAgent($agent);
        }
        foreach (self::HABILITATIONS_AGENT as $habilitation) {
            $method = "set{$habilitation['method']}";
            $habilitationAgent->$method(empty($request->request->get($habilitation['id'])) ? '0' : '1');
        }

        $this->em->persist($habilitationAgent);
        $this->em->flush();

        return $this->redirectToRoute('superadmin_agenttechnique_administration');
    }

    /**
     * @return Response
     * @throws ORMException
     */
    #[Route(
        path: '/association/{idAgent}',
        name: 'administration_association',
        requirements: ['idAgent' => '\d+'],
        methods: ['GET', 'POST']
    )]
    public function associationAgentAction(Request $request)
    {
        $idAgent = $request->get('idAgent');
        $agent = $this->em->getRepository(Agent::class)->findOneBy(['id' => $idAgent, 'technique' => 1]);
        if ($agent instanceof Agent) {
            $msg = '';
            $agentAssoc = new AgentTechniqueAssociation();
            $agentAssoc->setIdAgent($idAgent);
            $form = $this->createForm(AgentTechAssociationType::class, $agentAssoc);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $association = $this->em->getRepository(AgentTechniqueAssociation::class)->findOneBy(
                    [
                        'idAgent' => $idAgent,
                        'acronymeOrganisme' => $agentAssoc->getOrganisme()->getAcronyme(),
                        'serviceId' => $agentAssoc->getServiceId(),
                    ]
                );
                if ($association) {
                    $msg = 'ASSOCIATION_DEJA_EXISTE';
                } else {
                    $this->em->persist($agentAssoc);
                    $this->em->flush();
                }
            }
            // @todo: Revoir cette requête pour éviter l'injection SQL
            $dql = 'SELECT a FROM ' . AgentTechniqueAssociation::class . ' as a where a.idAgent = ' . $idAgent;
            $query = $this->em->createQuery($dql);

            $limiteParPage = 15;
            $paginator = $this->paginator;
            $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                $limiteParPage
            );

            return $this->render(
                'superadmin/agents/listes.association.agent.html.twig',
                [
                    'associations' => $pagination,
                    'message' => $msg,
                    'form' => $form->createView(),
                ]
            );
        } else {
            return $this->administrationAction($request);
        }
    }

    /**
     * @return JsonResponse
     */
    #[Route(
        path: '/association/delete',
        name: 'administration_association_delete',
        requirements: ['idAssociation' => '\d+', 'idAgent' => '\d+'],
        options: ['expose' => true],
        methods: ['DELETE']
    )]
    public function deleteAssociationAgentAction(Request $request)
    {
        $idAssociation = $request->get('idAssociation');
        $idAgent = $request->get('idAgent');
        $association = $this->em->getRepository(AgentTechniqueAssociation::class)->find($idAssociation);
        if ($association) {
            $this->em->remove($association);
            $this->em->flush();
        }

        return new JsonResponse(
            [
                'url' => $this->generateUrl(
                    'superadmin_agenttechnique_administration_association',
                    ['idAgent' => $idAgent]
                )
            ]
        );
    }
}
