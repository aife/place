<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller;

use Exception;
use App\Entity\Inscrit;
use App\Service\RgpdService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RgpdController extends AbstractController
{
    /**
     *
     * @return Response
     */
    #[Route(path: '/entreprise/rgpd')]
    #[Route(path: '/agent/rgpd')]
    public function rgpdView(): Response
    {
        return $this->render(
            'rgpd.html.twig',
            ['withCommunication' => (($this->getUser()) && $this->getUser() instanceof Inscrit)]
        );
    }

    /**
     *
     * @param Request $request
     * @param RgpdService $rgpdService
     * @return JsonResponse
     * @throws Exception
     */
    #[Route(path: '/entreprise/rgpd/save', name: 'entreprise_post_rgpd', methods: ['POST'])]
    #[Route(path: '/agent/rgpd/save', name: 'agent_post_rgpd', methods: ['POST'])]
    public function postEntrepriseRgpd(Request $request, RgpdService $rgpdService): JsonResponse
    {
        try {
            $rgpdService->saveRgpd($request);
            return $this->json(['status' => 'OK']);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            throw $e;
        }
    }
}
