<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Commun;

use App\Repository\FluxRssRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RssController
 * @package Atexo\AppBundle\Controller\Rss
 */
class RssController extends AbstractController
{
    public function __construct(
        private readonly FluxRssRepository $fluxRssRepository
    ) {
    }

    #[Route(path: '/rss.xml', name: 'rss_standard')]
    #[Route(path: '/rss-MPS.xml', name: 'rss_mps')]
    #[Route(path: '/rss-AA.xml', name: 'rss_aa')]
    #[Route(path: '/rss-API.xml', name: 'rss_api')]
    #[Route(path: '/rssFn.xml', name: 'rss_fn')]
    #[Route(path: '/rssTr.xml', name: 'rss_tr')]
    #[Route(path: '/rssSr.xml', name: 'rss_sr')]
    #[Route(path: '/rssOnline.xml', name: 'rss_online')]
    #[Route(path: '/rssCS.xml', name: 'rss_cs')]
    #[Route(path: '/rssCE.xml', name: 'rss_ce')]
    #[Route(path: '/rssAP.xml', name: 'rss_ap')]
    #[Route(path: '/agent/rss-download/{fileName}', methods: ['GET'], name: 'rss_download_file')]
    public function rssAction(Request $request): Response
    {
        $filename = ltrim($request->getPathInfo(), '/');
        $fluxRss = $this->fluxRssRepository->findBy(['nomFichier' => $filename]);

        if (empty($fluxRss)) {
            throw new Exception('Le fichier ' . $filename . ' n\'existe pas');
        }

        $file = $this->getParameter('DIR_FLUX_RSS') . $request->getPathInfo();

        return $this->file($file, null, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
