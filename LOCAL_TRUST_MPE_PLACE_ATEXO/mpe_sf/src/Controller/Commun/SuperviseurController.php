<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Commun;

use App\Service\Superviseur\SuperviseurFacade;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class SuperviseurController extends AbstractController
{
    /**
     * SuperviseurController constructor.
     */
    public function __construct(private readonly SuperviseurFacade $superviseurFacade)
    {
    }

    #[Route(path: '/url-de-vie', name: 'supervisor-home')]
    public function indexAction(ParameterBagInterface $parameterBag)
    {
        $maxParallelProcesses = $parameterBag->get('max_parallel_processes');
        $pollingInterval = $parameterBag->get('polling_interval');
        $result = $this->superviseurFacade->getResult($maxParallelProcesses, $pollingInterval);
        return new JsonResponse($result);
    }
}
