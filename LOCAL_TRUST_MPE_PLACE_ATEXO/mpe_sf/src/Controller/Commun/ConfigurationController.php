<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Commun;

use App\Service\AtexoConfiguration;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ConfigurationController extends AbstractController
{
    /**
     * @return Response
     */
    #[Route(path: '/config/{module}', name: 'App\Service\AtexoConfiguration')]
    public function indexAction($module, AtexoConfiguration $atexoConfiguration)
    {
        echo 'Module platforme '.$module.($atexoConfiguration->hasConfigPlateforme($module) ? ' ' : ' non ').'activé<br/><br>';
        echo 'Module organisme a1a '.$module.($atexoConfiguration->hasConfigOrganisme('a1a', $module) ? ' ' : ' non ').'activé<br/><br>';
        return $this->render('Default/index.html.twig');
    }
}
