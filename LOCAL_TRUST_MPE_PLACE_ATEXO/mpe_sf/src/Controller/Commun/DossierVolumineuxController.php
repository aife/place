<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Commun;

use Doctrine\ORM\Exception\ORMException;
use App\Controller\AtexoController;
use App\Entity\DossierVolumineux\DossierVolumineux;
use App\Entity\EchangeDestinataire;
use App\Service\AtexoUtil;
use App\Service\CurrentUser;
use App\Service\DossierVolumineux\DossierVolumineuxService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Exception;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class DossierVolumineuxController.
 */
class DossierVolumineuxController extends AtexoController
{
    /**
     * @var sent to navigator
     */
    private ? int $code = null;

    /**
     * @var sent to navigator
     */
    private $content;

    public function __construct(private TranslatorInterface $translator, private AtexoUtil $atexoUtil, private ParameterBagInterface $parameterBag)
    {
    }

    /**
     * @ParamConverter("dossierVolumineux", options={"mapping": {"uuidReference": "uuidReference"}})
     * @ParamConverter("echangeDestinataire", options={"mapping": {"num_ar": "uid", "orgAccronyme": "organisme"}})*/
    #[Route(path: '/entreprise/dossier-volumineux/download/{uuidReference}', name: 'dossier_volumineux_entreprise_download_tokenless')]
    #[Route(path: '/entreprise/dossier-volumineux/download/{uuidReference}/mail/{orgAccronyme}/{num_ar}', name: 'dossier_volumineux_download_entreprise_from_mail')]
    #[Route(path: '/agent/dossier-volumineux/download/{uuidReference}', name: 'dossier_volumineux_agent_download_tokenless')]
    #[Route(path: '/agent/dossier-volumineux/download/{uuidReference}/mail/{orgAccronyme}/{num_ar}', name: 'dossier_volumineux_download_agent_from_mail')]
    public function downloadClientDossierVolumineuxAction(DossierVolumineux $dossierVolumineux, LoggerInterface $dossierVolumineuxLogger, DossierVolumineuxService $service, AtexoUtil $atexoUtil, Request $request, EchangeDestinataire $echangeDestinataire = null) : RedirectResponse|Response
    {
        try {
            if (false === $service->isAuthorizedToDownload($dossierVolumineux, $echangeDestinataire)) {
                throw new AccessDeniedHttpException();
            }

            return $this->downloadDossierVolumineux(
                $dossierVolumineux,
                $dossierVolumineuxLogger,
                $service,
                $atexoUtil,
                $request
            );
        } catch (Exception $exception) {
            $dossierVolumineuxLogger->error($exception->getMessage());
            $this->addFlash(
                'error',
                'Vous ne pouvez pas télécharger ce dossier volumineux !'
            );
            $dossierVolumineuxLogger->error($exception->getMessage());

            return $this->redirectToRoute($this->getRedirectRoute($request));
        }
    }

    /**
     * @return string
     */
    public function getRedirectRoute(Request $request)
    {
        $route = $request->attributes->get('_route');
        $userType = 'entreprise';
        if (str_contains((string) $route, 'agent')) {
            $userType = 'agent';
        }

        return sprintf('dossier_volumineux_%s_gestion', $userType);
    }

    /**
     * @param $dossierVolumineux
     */
    private function downloadDossierVolumineux(
        $dossierVolumineux,
        LoggerInterface $dossierVolumineuxLogger,
        DossierVolumineuxService $service,
        AtexoUtil $atexoUtil,
        Request $request
    ): RedirectResponse|Response {
        try {
            $service->checkServiceDossierVolumineux();

            $actif = '0';

            if ($dossierVolumineux->isActif()) {
                $actif = '1';
            }

            $pfUrl =
                $atexoUtil->getPfUrlDomaine($this->parameterBag->get('PF_URL_REFERENCE'));
            $params = [
                'reference' => $dossierVolumineux->getUuidReference(),
                'nomDossierVolumineux' => $dossierVolumineux->getNom(),
                'serverLogin' => $this->parameterBag->get('LOGIN_TIERS_TUS'),
                'serverPassword' => $this->parameterBag->get('PWD_TIERS_TUS'),
                'serverURL' => $pfUrl,
                'statut' => $actif,
            ];

            $callback = $service->callTusSession($params, $dossierVolumineux->getUuidTechnique());

            $response = new Response();
            $response->headers->set('Cache-Control', 'private');
            $response->headers->set('Content-type', 'application/' .
                $this->getParameter('DOSSIER_VOLUMINEUX_APS_EXTENSION'));
            $response->headers->set('Content-Disposition', 'attachment; filename="' .
                $this->getParameter('DOSSIER_VOLUMINEUX_APS') .
                $dossierVolumineux->getUuidReference() .
                $this->getParameter('DOSSIER_VOLUMINEUX_APS_EXTENSION') .
                '";');
            $response->sendHeaders();
            $response->setContent(json_encode($callback, JSON_THROW_ON_ERROR));

            return $response;
        } catch (Exception $e) {
            return $this->tusError($e, $dossierVolumineuxLogger, $request);
        }
    }

    /**
     * @param $e
     *
     * @return RedirectResponse
     */
    public function tusError($e, LoggerInterface $dossierVolumineuxLogger, Request $request)
    {
        $this->addFlash(
            'error',
            "Il y a un problème avec le serveur d'upload !"
        );
        $dossierVolumineuxLogger->error($e->getMessage());

        return $this->redirectToRoute($this->getRedirectRoute($request));
    }

    #[Route(path: '/entreprise/dossier-volumineux/gestion/', name: 'dossier_volumineux_entreprise_gestion', defaults: ['userType' => 'entreprise'])]
    #[Route(path: '/entreprise/dossier-volumineux/gestion/{userType}', defaults: ['userType' => 'entreprise'])]
    #[Route(path: '/agent/dossier-volumineux/gestion/', name: 'dossier_volumineux_agent_gestion', defaults: ['userType' => 'agent'])]
    #[Route(path: '/agent/dossier-volumineux/gestion/{userType}', defaults: ['userType' => 'agent'])]
    public function gestion(DossierVolumineuxService $service, CurrentUser $currentUser, Request $request, string $userType = 'entreprise') : Response
    {
        if (false === $service->isEnable()) {
            return $this->redirectAccueil();
        }
        $params = [];
        $params['withoutFooter'] = true;
        $params['controllerName'] = $userType . '/dossier-volumineux';
        $params['userType'] = $userType;
        $params['breadcrumb'] = $this->getBreadCrumb();
        $params['downloadActionName'] = sprintf('dossier_volumineux_%s_download_tokenless', $userType);
        $params['tpl'] = 'dossierVolumineux/gestion-table.html.twig';
        if ('entreprise' === $userType) {
            $params['tpl'] = 'dossierVolumineux/gestion.html.twig';
        }

        if ('agent' === $userType && $currentUser->isConnected()) {
            return $this->renderGestion($currentUser, $params, $service);
        } elseif ('entreprise' === $userType && $currentUser->isInscrit() && $currentUser->isConnected()) {
            return $this->renderGestion($currentUser, $params, $service);
        } else {
            return $this->renderGestion($currentUser, $params, $service, false);
        }
    }

    /**
     * @return RedirectResponse
     */
    #[Route(path: '/dossier-volumineux/download/{uuidReference}', name: 'dossier_volumineux_download_by_uuid_reference')]
    public function downloadByUuidReference(string $uuidReference, LoggerInterface $dossierVolumineuxLogger, DossierVolumineuxService $service, AtexoUtil $atexoUtil, Request $request) : Response
    {
        // On supprime ce controle temporairement MPE-18757
        //        if (!str_starts_with($uuidReference, 'A-')) {
        //            // Les dossiers volumineux dont la référence ne commence pas par 'A-' ne sont
        //            // pas accessibles par ce controller.
        //            throw new AccessDeniedException();
        //        }
        return $this->downloadDossierVolumineux(
            $service->getDossierVolumineuxByUuid($uuidReference),
            $dossierVolumineuxLogger,
            $service,
            $atexoUtil,
            $request
        );
    }

    /**
     * @param $params
     * @param bool $connected
     *
     * @return Response
     */
    public function renderGestion(
        CurrentUser $currentUser,
        $params,
        DossierVolumineuxService $service,
        $connected = true
    ) {
        $dossiersVolumineux = $service->findAll($currentUser);
        $datas = [];
        foreach ($dossiersVolumineux as $dossierVolumineux) {
            $pathFile = $this->getParameter('DIR_DOSSIERS_VOLUMINEUX') . $service->getBlogFilePath($dossierVolumineux);
            $downloadable = false;
            if (file_exists($pathFile) && !empty($dossierVolumineux->getBlobLogfile())) {
                $downloadable = true;
            }

            $paramsDV = [
                'reference' => $dossierVolumineux->getUuidReference(),
                'nomDossierVolumineux' => $dossierVolumineux->getNom(),
                'serverLogin' => $this->parameterBag->get('LOGIN_TIERS_TUS'),
                'serverPassword' => $this->parameterBag->get('PWD_TIERS_TUS'),
                'serverURL' => $this->atexoUtil->getPfUrlDomaine($this->parameterBag->get('PF_URL_REFERENCE')),
                'statut' => $dossierVolumineux->isActif() ? '1' : '0',
            ];

            $callback = $service->callTusSession($paramsDV, $dossierVolumineux->getUuidTechnique());


            $dossierVolumineux->callback = $callback['uuid'];
            $dossierVolumineux->downloadable = $downloadable;
            $datas[] = $dossierVolumineux;
        }
        $params['datas'] = $datas;
        $params['connected'] = $connected;
        if (true !== $connected) {
            $translator = $this->translator;
            $this->addFlash(
                'error',
                $translator->trans('DOSSIER_VOLUMINEUX_GESTION_CONNECTION_ERROR')
            );
        }

        return $this->render('Default-App/index.html.twig', $params);
    }

    /**
     * @ParamConverter("dossierVolumineux", options={"mapping": {"uuidReference": "uuidReference"}})
     *
     * @throws ORMException
     */
    #[Route(path: '/entreprise/dossier-volumineux/toggle-statut/{uuidReference}')]
    #[Route(path: '/agent/dossier-volumineux/toggle-statut/{uuidReference}')]
    public function toggleStatut(DossierVolumineuxService $service, TranslatorInterface $translator, DossierVolumineux $dossierVolumineux = null) : Response
    {
        $access = $this->checkAccess($service, $dossierVolumineux);

        if (true === $access) {
            if (true === $res = $service->toggleStatut($dossierVolumineux)) {
                $ret = [
                    'code' => Response::HTTP_OK,
                    'message' => 'Le statut a été modifié',
                ];
                $this->setCode(Response::HTTP_OK);
            } else {
                $ret = [
                    'error_code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                    'error_message' => $translator->trans('DOSSIER_VOLUMINEUX_GESTION_STATUT_ERROR'),
                ];
                $this->setCode(Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        } else {
            $ret = $this->getContent();
        }

        $response = new JsonResponse($ret, $this->getCode());
        $response->headers->set('Content-type', 'application/json');

        return $response;
    }

    /**
     * @return bool
     */
    public function checkAccess(DossierVolumineuxService $service, DossierVolumineux $dossierVolumineux = null)
    {
        $access = true;
        $code = null;
        $content = null;
        if (empty($dossierVolumineux)) {
            $code = Response::HTTP_NOT_FOUND;
            $content = 'Not Found';
            $access = false;
        } elseif (false === $service->isEnable()) {
            $content = 'Dossier Volumineux est desactivé!';
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $access = false;
        } elseif (false === $service->checkAccess($dossierVolumineux)) {
            $content = 'Accès refusé!';
            $code = Response::HTTP_UNAUTHORIZED;
            $access = false;
        }
        if (false === $access) {
            $ret = [
                'error_code' => $code,
                'error_message' => $content,
            ];
            $this->setContent($ret);
            $this->setCode($code);
        }

        return $access;
    }

    /**
     * @return array
     */
    public function getBreadcrumb()
    {
        return ['DOSSIER_VOLUMINEUX_GESTION_TITLE'];
    }

    /**
     * @return RedirectResponse
     */
    public function redirectAccueil()
    {
        return $this->redirect($this->getParameter('PF_URL') . 'entreprise');
    }

    /**
     * remplace le troisieme segment d'url par celui gestion.
     *
     * @return RedirectResponse
     */
    public function redirectGestion(Request $request)
    {
        $url = $request->server->get('REQUEST_URI');
        $pattern = '/\/(.*)\/(.*)\/(.*)\//i';
        $replacement = '/${1}/${2}/gestion/';
        $url = preg_replace($pattern, $replacement, $url);

        return $this->redirect($url);
    }

    #[Route(path: '/entreprise/dossier-volumineux/upload/')]
    #[Route(path: '/agent/dossier-volumineux/upload/', name: 'dossier_volumineux_agent_upload')]
    public function upload(Request $request, LoggerInterface $dossierVolumineuxLogger, CurrentUser $currentUser, DossierVolumineuxService $service, ParameterBagInterface $parameterBag) : Response
    {
        $this->forward('App\Controller\DossierVolumineuxController::gestion', []);

        if (false === $currentUser->isConnected()) {
            return $this->redirectGestion($request);
        }

        if (false === $service->isEnable() && false === $currentUser->isConnected()) {
            return $this->redirectAccueil();
        }
        try {
            $dossierVolumineux = $service->createDossierVolumineux();
        } catch (Exception $e) {
            return new Response($e->getMessage());
        }
        $pfUrl =
            $this->atexoUtil->getPfUrlDomaine($parameterBag->get('PF_URL_REFERENCE'));
        $params = [
            'reference' => $dossierVolumineux->getUuidReference(),
            'nomDossierVolumineux' => 'INIT_NOM',
            'serverLogin' => $parameterBag->get('LOGIN_TIERS_TUS'),
            'serverPassword' => $parameterBag->get('PWD_TIERS_TUS'),
            'serverURL' => $pfUrl,
            'statut' => '0',
        ];

        try {
            $responseTus = $service->callTusSession($params, $dossierVolumineux->getUuidTechnique());
            $uuidSessionTus = $responseTus['uuid'];
            $url = $parameterBag->get('URL_CLIENT_TUS');
            $url .= 'index.html?uuid=';
            $url .= $uuidSessionTus;
            $url .= '&from=';
            $url .= ($currentUser->isAgent()) ? 'agent' : 'entreprise';

            return $this->redirect($url);
        } catch (Exception $e) {
            return $this->tusError($e, $dossierVolumineuxLogger, $request);
        }
    }

    /**
     * @ParamConverter("dossierVolumineux", options={"mapping": {"uuidReference": "uuidReference"}})*/
    #[Route(path: '/entreprise/dossier-volumineux/event/{uuidReference}')]
    #[Route(path: '/agent/dossier-volumineux/event/{uuidReference}')]
    public function event(DossierVolumineux $dossierVolumineux, LoggerInterface $dossierVolumineuxLogger, DossierVolumineuxService $service, Request $request) : Response
    {
        if (false === $service->checkAccess($dossierVolumineux)) {
            return $this->redirectAccueil();
        }
        try {
            $file = $service->getBlogFilePath($dossierVolumineux);

            return $this->file($this->getParameter('DIR_DOSSIERS_VOLUMINEUX') . $file);
        } catch (Exception $e) {
            $dossierVolumineuxLogger->error($e->getMessage());

            $this->addFlash(
                'error',
                "Le fichier n'est pas disponible !"
            );

            return $this->redirectToRoute($this->getRedirectRoute($request));
        }
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }
}
