<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Commun;

use Exception;
use DateTime;
use App\Controller\AtexoController;
use App\Entity\Consultation;
use App\Service\InscritOperationsTracker;
use App\Service\ObscureDataManagement;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class InscritOperationTrackerController de tracage des operations de l'inscrit.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @since ESR2016
 *
 * @copyright Atexo 2016
 */
class InscritOperationTrackerController extends AtexoController
{
    public function __construct(
        private readonly InscritOperationsTracker $inscritOperationTracker,
        private readonly LoggerInterface $logger,
        private readonly ObscureDataManagement $obscureDataManagement,
        private readonly TranslatorInterface $translator
    ) {
    }

    /**
     * Route pour tracer les operations de l'inscrit.
     *
     * @Method({"POST"})
     * @ParamConverter("consultation", options={"mapping": {"consultation_id": "id", "consultation_org": "organisme"}})
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016*/
    #[Route(path: '/entreprise/operation/tracking/{consultation_id}/{consultation_org}', requirements: ['consultation_id' => '\d+'], name: 'traking_inscrit_operation', options: ['expose' => true])]
    public function trackInscritOperation(Consultation $consultation, Request $request)
    {
        $timestampDebutActionClient = null;
        $data = $request->get('dataTracking');
        $errors = [];
        $fichier = '';
        $fichierSource = '';
        $dateDebutActionClient = null;
        try {
            $typeEnveloppe = $this->obscureDataManagement->getDataFromObscuredValue(
                $data['typeEnveloppe'],
                $data['uid_response']
            );
            $typeFichier = $this->obscureDataManagement->getDataFromObscuredValue(
                $data['typeFichier'],
                $data['uid_response']
            );
            $offreId = $this->obscureDataManagement->getDataFromObscuredValue($data['offre_id'], $data['uid_response']);
            if (!empty($data['cleDescription'])) {
                $cleDescription = $data['cleDescription'];
            } elseif (!empty($data['action'])) {
                $cleDescription = $this->inscritOperationTracker->getCleDescription($data['action'], $typeFichier);
            } else {
                throw new Exception('La cle de la description est obligatoire.');
            }
            if (array_key_exists('dateDebutAction', $data)) {
                if (count(explode('}', (string) $data['dateDebutAction'])) >= 2) {
                    $cleanDateDebutAction = explode('}', (string) $data['dateDebutAction']);
                    $data['dateDebutAction'] = $cleanDateDebutAction[0];
                }
                $dateDebutActionClient = new DateTime($data['dateDebutAction']); //Date envoyee par le poste client
            }
            if (array_key_exists('debutActionTimestampMillisecond', $data)) {
                $timestampDebutActionClient = (float) $data['debutActionTimestampMillisecond']; //timestamp de debut
                // de l'action (coté client) en millisecond
            }
            $dateDebutAction = new DateTime();
            if (array_key_exists('fichiers', $data)) {
                $fichier = $data['fichiers'];
            }
            if (array_key_exists('fichierSource', $data)) {
                $fichierSource = $data['fichierSource'];
            }

            [$idValeurReferentielle, $description] = $this->inscritOperationTracker
                ->getInfosValeursReferentielles($cleDescription);
            $description ??= '';
            if (empty($fichierSource)) {
                $this->inscritOperationTracker->completeDescription(
                    $description,
                    $fichier,
                    $typeEnveloppe,
                    $typeFichier
                );
            } else {
                $this->inscritOperationTracker->completeDescription(
                    $description,
                    $fichier,
                    $typeEnveloppe,
                    $typeFichier,
                    $fichierSource,
                );
            }

            $this->inscritOperationTracker->tracerOperationsInscrit(
                $dateDebutAction,
                new DateTime(),
                $idValeurReferentielle,
                $description,
                $consultation,
                '',
                false,
                null,
                1,
                $dateDebutActionClient,
                $offreId,
                $timestampDebutActionClient
            );
        } catch (Exception $e) {
            $erreur = 'Error tracking inscrit operation. Exception : ' . $e->getMessage() . PHP_EOL .
                'Trace : ' . $e->getTraceAsString();
            $this->logger->error($erreur);
            $errors[] = $erreur;
        }
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        if (!empty($errors)) {
            $response->setContent(json_encode(['errors' => $errors], JSON_THROW_ON_ERROR));

            return $response;
        } else {
            $response->setContent(json_encode([
                'dataTracking' => $data,
            ], JSON_THROW_ON_ERROR));

            return $response;
        }
    }
}
