<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Commun;

use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\GeolocalisationN2;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/geolocalisation-n2', name: 'geolocalisation_n2_')]
class GeolocalisationN2Controller extends AbstractController
{
    /**
     * @return JsonResponse
     */
    #[Route(path: '/pays-etrangers', options: ['expose' => true], name: 'foreign_countries', methods: ['GET'])]
    public function getForeignCountriesAction()
    {
        $countries = $this->getDoctrine()
            ->getRepository(GeolocalisationN2::class)
            ->getForeignCountries();

        return $this->json($countries);
    }
}
