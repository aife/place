<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Commun;

use App\Service\Contrat\LotContrat;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use App\Controller\AtexoController;
use App\Entity\ContratTitulaire;
use App\Service\ContratService;
use GoetasWebservices\Xsd\XsdToPhpRuntime\Jms\Handler\BaseTypesHandler;
use GoetasWebservices\Xsd\XsdToPhpRuntime\Jms\Handler\XmlSchemaDateHandler;
use JMS\Serializer\Handler\DateHandler;
use JMS\Serializer\Handler\HandlerRegistryInterface;
use JMS\Serializer\SerializerBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ContratTitulaireController.
 **/
#[Route(path: '/agent/contrat', name: 'atexo_entreprise_contrat_titulaire')]
class ContratTitulaireController extends AtexoController
{
    public function __construct(private ContratService $contratService)
    {
    }
    /**
     * @Method("GET")
     * @param int $uuid
     * @return Response
     */
    #[Route(path: '/export/{uuid}', name: 'atexo_entreprise_contrat_titulaire_index')]
    public function exportContratAction($uuid)
    {
        $contrat = $this->getDoctrine()
            ->getRepository(ContratTitulaire::class)
            ->findOneBy(['uuid' => $uuid]);
        $marche = $this->contratService->getFormatPivot($contrat);
        $newXml = $this->serializeMarches($marche, 'xml');
        $response = new Response();
        $response->headers->set('Content-Type', 'xml');
        $response->headers->set('Content-Disposition', 'attachment; filename=contrat.xml');
        $response->setContent($newXml);
        return $response;
    }
    /**
     * @Method("GET")
     * @param int $idContrat
     * @return Response
     */
    #[Route(path: '/export-etendu/{idContrat}', requirements: ['idContrat' => '\d+'], name: 'atexo_entreprise_contrat_titulaire_etendu_index')]
    public function exportContratEtenduAction($idContrat)
    {
        $contrat = $this->getDoctrine()
            ->getRepository(ContratTitulaire::class)
            ->find($idContrat);
        $marche = $this->contratService->getFormatExtendu($contrat);
        $newXml = $this->serializeMarches($marche, 'xml');
        $response = new Response();
        $response->headers->set('Content-Type', 'xml');
        $response->headers->set('Content-Disposition', 'attachment; filename=contrat.xml');
        $response->setContent($newXml);
        return $response;
    }

    /**
     * @Route("/lot/{idConsultation}",
     *     requirements={"idConsultation" = "\d+"},
     *     options={"expose"=true},
     *     name="_lot_attribution")
     * @param int $idConsultation
     * @param LotContrat $lotContratService
     * @return JsonResponse
     */
    public function getLotContratAttribution(int $idConsultation, LotContrat $lotContratService): JsonResponse
    {
        $data = $lotContratService->getByIdConsultation($idConsultation);

        return $this->json($data);
    }

    protected function serializeMarches($marches, $format)
    {
        $serializerBuilder = SerializerBuilder::create();
        $serializerBuilder->setPropertyNamingStrategy(new IdenticalPropertyNamingStrategy());
        $serializerBuilder->addMetadataDir(
            __DIR__ . '/../Entity/DonneesEssentielles/Metadata',
            'App\Model\DonneesEssentielles'
        );
        $serializerBuilder->configureHandlers(function (HandlerRegistryInterface $handler) use ($serializerBuilder) {
            $serializerBuilder->addDefaultHandlers();
            $handler->registerSubscribingHandler(new DateHandler('Y-m-d'));
            $handler->registerSubscribingHandler(new BaseTypesHandler()); // XMLSchema List handling
            $handler->registerSubscribingHandler(new XmlSchemaDateHandler()); // XMLSchema date handling
        });

        $serializer = $serializerBuilder->build();

        $data = $serializer->serialize($marches, $format);

        return $data;
    }
}
