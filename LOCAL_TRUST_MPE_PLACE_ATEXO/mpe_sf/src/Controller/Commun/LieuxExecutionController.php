<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Commun;

use App\Form\Agent\FieldType\LieuxExecutionSelectType;
use App\Service\Agent\ConsultationSimplifieeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/lieux-execution', name: 'lieux_execution_')]
class LieuxExecutionController extends AbstractController
{
    public function __construct(
        private readonly ConsultationSimplifieeService $consultationSimplifieeService
    ) {
    }

    #[Route(path: '/updateListeLieux', name: 'update_liste_lieux', options: ['expose' => true])]
    public function updateListeLieux(Request $request): Response
    {
        $data = $this->consultationSimplifieeService->idsToGeoN2($request->request->get('ids'));
        $form = $this->createFormBuilder()->add('lieuxExecutionSelect', LieuxExecutionSelectType::class, [
            'choices' => $data,
        ]);

        return $this->render('consultation/Agent/AjaxForm/lieux_execution_select_type.html.twig', [
            'form' => $form->getForm()->createView(),
        ]);
    }
}
