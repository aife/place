<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Commun;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class LanguageController extends AbstractController
{
    #[Route(path: 'language/{_locale}', name: 'switcher-language', defaults: ['_locale' => 'fr'], requirements: ['_locale' => 'fr|en|es|du|it|su|ar'], options: ['expose' => true])]
    public function switchLanguage(Request $request, string $_locale) : RedirectResponse
    {
        if (null != $_locale) {
            $request->getSession()->set('_locale', $_locale);

            $contexte = $request->getSession()->get('contexte_authentification');
            $contexte['lang'] = $_locale;
            $request->getSession()->set('contexte_authentification', $contexte);
        }
        $url = $request->headers->get('referer');
        if (empty($url)) {
            $url = $this->generateUrl('homepage');
        }
        return $this->redirect($url);
    }
}
