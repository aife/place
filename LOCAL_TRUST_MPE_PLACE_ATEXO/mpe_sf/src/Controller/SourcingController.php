<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller;

use App\Service\WebServicesSourcing;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SourcingController extends AbstractController
{
    #[Route(path: '/agent/sourcing/nukema', name: 'agent_sourcing_nukema')]
    public function authAction(Request $request, WebServicesSourcing $client)
    {
        if (!$this->getUser()) {
            throw $this->createNotFoundException('Cette ressource n\'existe pas !');
        }
        return $this->redirect($client->getContent('getUrlSourcing', $this->getUser()));
    }

    #[Route(path: '/agent/sourcing/base-dce', name: 'agent_sourcing_base_dce')]
    #[IsGranted('ROLE_AGENT')]
    public function redirectToBaseDce(Request $request, WebServicesSourcing $client): RedirectResponse
    {
        $baseDceUrlParameters = [
            'county' => $request->query->get('countries'),
            'nature' => $request->query->get('category'),
        ];

        return new RedirectResponse(
            $client->getContent(
                'getBaseDceUrl',
                $this->getUser(),
                $baseDceUrlParameters
            )
        );
    }
}
