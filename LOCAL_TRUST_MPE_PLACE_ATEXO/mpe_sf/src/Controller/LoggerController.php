<?php

namespace App\Controller;

use App\Entity\SsoTiers;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\TokenNotFoundException;

class LoggerController extends AbstractController
{
    #[Route(path: '/logger/{token}', name: 'logger', methods: ['GET'])]
    public function indexAction(Request $request, ParameterBagInterface $parameterBag)
    {
        $token = $request->get('token');
        $check = $this->getDoctrine()
            ->getRepository(SsoTiers::class)
            ->checkToken($token, $parameterBag->get('FONCTIONNALITE_TOKEN'));
        //5
        if (false === $check) {
            throw new TokenNotFoundException();
        }
        //LOG_DIR
        $directoryLog = $this->scanAllDir($parameterBag->get('LOG_DIR'));
        return $this->render(
            'Logger/logger.html.twig',
            ['directoryLog' => $directoryLog]
        );
    }

    protected function scanAllDir($dir)
    {
        $result = [];
        foreach (scandir($dir) as $filename) {
            if ('.' === $filename[0]) {
                continue;
            }
            $filePath = $dir.'/'.$filename;
            if (is_dir($filePath)) {
                foreach ($this->scanAllDir($filePath) as $childFilename) {
                    $result[] = $filename.'/'.$childFilename;
                }
            } else {
                $result[] = $filename;
            }
        }

        return $result;
    }

    #[Route(path: '/download-logger', name: 'downloadLogger', methods: ['POST'])]
    public function downloadLogAction(Request $request, ParameterBagInterface $parameterBag)
    {
        if ('POST' === $request->getMethod()) {
            $samplePdf = new File($parameterBag->get('LOG_DIR').'/'.$request->request->get('file'));

            return $this->file($samplePdf);
        }
    }
}
