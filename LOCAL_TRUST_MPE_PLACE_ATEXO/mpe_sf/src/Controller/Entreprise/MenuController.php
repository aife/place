<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise;

use App\Service\Configuration;
use App\Service\Entreprise\Menu;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class MenuController extends CommonController
{
    private readonly array $url_redirect;

    public function __construct(Menu $menu, Configuration $configuration, ContainerBagInterface $container)
    {
        parent::__construct($menu, $configuration, $container);

        $this->url_redirect = [
            'SocleExterneEntreprise' => 'URL_INDEX_SSO_ENTREPRISE',
            'SocleExternePpp' => 'URL_PPP_INSCRIPTION',
        ];
    }

    /**
     * @param $url_id
     */
    #[Route(path: '/entreprise/{url_id}/external-connect', methods: 'GET')]
    public function socleExterneConnect($url_id) : RedirectResponse
    {
        if (array_key_exists($url_id, $this->url_redirect)) {
            return $this->redirect($this->getParameter('URL_INDEX_SSO_ENTREPRISE'));
        }
        return $this->redirectToRoute('accueil_entreprise');
    }
}
