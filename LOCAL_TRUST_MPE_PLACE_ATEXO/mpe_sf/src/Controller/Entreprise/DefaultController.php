<?php

namespace App\Controller\Entreprise;

use App\Service\Entreprise\EntrepriseVerificationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController.
 */
class DefaultController extends AbstractController
{
    /**
     * DefaultController constructor.
     */
    public function __construct(private readonly EntrepriseVerificationService $entrepriseVerificationService)
    {
    }

    #[Route(path: '/entreprise/verification')]
    public function verificationAction()
    {
        return $this->render(
            'Entreprise/verification.html.twig',
            $this->entrepriseVerificationService->getParamsModal()
        );
    }
}
