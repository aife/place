<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise\Consultation;

use App\Entity\TBourseCotraitance;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/entreprise/consultation')]
class ConsultationDeleteController extends ConsultationIndexDepotController
{
    /**
     * @Method({"GET"})*/
    #[Route(path: '/delete/', name: 'atexo_consultation_delete')]
    public function deleteConsultationAction() : Response
    {
        $idConsultation = $this->request->query->get('id_consultation');
        $idEntreprise = $this->request->query->get('id_entreprise');
        if (isset($idConsultation) && isset($idEntreprise)) {
            $data = $this->em->getRepository(TBourseCotraitance::class)
                ->findOneBy(array(
                    'consultation' => $idConsultation,
                    'entreprise' => $idEntreprise,
                ));

            if ($data) {
                $this->em->remove($data);
                $this->em->flush();
            }
        }
        return $this->redirectToRoute(
            'atexo_consultation_index',
            array('id' => $idConsultation)
        );
    }
}
