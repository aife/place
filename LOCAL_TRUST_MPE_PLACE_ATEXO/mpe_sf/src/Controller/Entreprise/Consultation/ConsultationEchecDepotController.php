<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise\Consultation;

use Exception;
use DateTime;
use App\Entity\Consultation;
use App\Entity\Offre;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/entreprise/consultation')]
class ConsultationEchecDepotController extends ConsultationIndexDepotController
{
    /**
     * Permet d'afficher le message d'echec de depot.
     *
     * @Route("/depot/{consultation_id}/{offre_id}/echecDepot", requirements={"consultation_id" = "\d+"},
     *                                                      name="atexo_consultation_echec_depot")
     * @ParamConverter("consultation", options={"mapping": {"consultation_id": "id"}})
     * @ParamConverter("offre", options={"mapping": {"offre_id": "id"}})
     *
     * @param Consultation $consultation : objet consultation
     * @param Offre        $offre        : objet Offre
     *
     * @return Response
     * @throws Exception
     */
    #[Route(path: '/depot/{consultation_id}/{offre_id}/echecDepot', requirements: ['consultation_id' => '\d+'], name: 'atexo_consultation_echec_depot')]
    public function echecDepotAction(Consultation $consultation, Offre $offre)
    {
        if (
            null !== $this->getUser() &&
            $this->currentUser->isInscrit() &&
            $this->getUser()->getId() === $offre->getInscritId()
        ) {
            $liensRetour = $this->parameterBag->get('URL_LT_MPE_SF') . DIRECTORY_SEPARATOR . 'consultation' . DIRECTORY_SEPARATOR . $consultation->getId();

            [$idValeurReferentielle, $description] = $this->inscritOperationTracker->getInfosValeursReferentielles('DESCRIPTION40');
            $this->inscritOperationTracker->tracerOperationsInscrit(new DateTime(), new DateTime(), $idValeurReferentielle, $description, $consultation, '', false, null, 1, null, $offre->getId());

            return $this->render('consultation/consultation-echec-depot.html.twig', [
                'consultation' => $consultation,
                'lienRetour' => $liensRetour,
            ]);
        } else {
            throw new Exception('Vous n\'avez pas le droit d\'accéder à cette page.');
        }
    }
}
