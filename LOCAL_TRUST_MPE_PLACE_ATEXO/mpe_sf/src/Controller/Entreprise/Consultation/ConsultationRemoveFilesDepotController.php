<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise\Consultation;

use App\Service\Consultation\ConsultationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/entreprise/consultation')]
class ConsultationRemoveFilesDepotController extends ConsultationIndexDepotController
{
    /**
     * @Method({"DELETE"})
     *
     * @param string              $id                  ID de la consultation (valeur obscurcie)
     * @param string              $idFichier           ID du fichier a retirer de la consultation (valeur obscurcie)
     * @param string              $uidResponse         Unique ID de la reponse
     * @param ConsultationService $consultationService Consultation service
     *
     * @return Response
     */
    #[Route(path: '/depot/{id}/retirer/{idFichier}/{uidResponse}', name: 'atexo_consultation_depot_retirer_fichier')]
    public function retirerFichierAction($id, $idFichier, $uidResponse, ConsultationService $consultationService)
    {
        $id = $this->obscureDataManagement->getDataFromObscuredValue($id, $uidResponse);
        $idFichier = $this->obscureDataManagement->getDataFromObscuredValue($idFichier, $uidResponse);
        if (!empty($idFichier) && !empty($id)) {
            $this->depotLogger->info('supprimé fichier retirerFichierAction : id fichier ==> '.$idFichier);
            $consultationService->removeEnveloppeFichier($idFichier);
        }
        $return = [
            $this->generateUrl('atexo_consultation_depot_retirer_fichier', [
                'id' => $this->obscureDataManagement->getObscureData($id, $uidResponse),
                'idFichier' => $this->obscureDataManagement->getObscureData($idFichier, $uidResponse),
                'uidResponse' => $uidResponse,
            ]) => 'true',
        ];
        return $this->json($return);
    }
}
