<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise\Consultation;

use Twig\Error\Error;
use App\Service\Consultation\ConsultationService;
use App\Utils\Encryption;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/entreprise/consultation')]
class ConsultationShowContenusTransmisController extends ConsultationIndexDepotController
{
    /**
     * @Method({"POST"})
     *
     * @return Response
     * @throws Error
     */
    #[Route(path: '/contenus_transmis', name: 'atexo_consultation_contenus_transmis_depot')]
    public function showContenusTransmisConsultationAction(Encryption $encryption, ConsultationService $consultationService) : Response
    {
        if (!$this->request->request->get('consultation')) {
            throw new BadRequestHttpException(Response::HTTP_BAD_REQUEST);
        }

        $type = ConsultationService::CONSULTATION_TYPE_CLOTURES;
        $consultation = $consultationService->getConsultation(
            $encryption->decryptId($this->request->request->get('consultation')),
            $type
        );

        $htmlContent = $this->infoRecapService->getDepotContenusTransmis(
            $consultation
        );

        $responseContent = json_encode([
            'content' => $htmlContent,
        ], JSON_THROW_ON_ERROR);

        return new Response($responseContent);
    }
}
