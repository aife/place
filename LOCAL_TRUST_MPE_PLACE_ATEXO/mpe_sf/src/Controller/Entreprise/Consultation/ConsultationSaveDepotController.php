<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise\Consultation;

use App\Message\AskDumePublicationGroupementEntreprise;
use App\Message\DumePublication;
use App\Service\CurrentUser;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use App\Service\ObscureDataManagement;
use App\Service\AtexoFichierSignature;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Service\InscritOperationsTracker;
use App\Service\AtexoUtil;
use App\Service\AtexoTelechargementConsultation;
use App\Security\UidManagement;
use App\Service\Entreprise\EntrepriseVerificationService;
use App\Service\Dume\DumeService;
use App\Service\AtexoDumeService;
use App\Service\GeolocalisationService;
use App\Service\InfoRecapService;
use App\Service\CandidatureService;
use App\Service\DocumentService;
use App\Utils\Filesystem\MountManager;
use App\Service\Consultation\ConsultationDepotService;
use App\Service\Consultation\ConsultationUploadDepotService;
use DateTime;
use App\Service\AtexoEnveloppeFichier;
use App\Entity\CandidatureMps;
use App\Entity\Consultation;
use App\Entity\Enveloppe;
use App\Entity\EnveloppeFichier;
use App\Entity\PanierEntreprise;
use App\Entity\TCandidature;
use App\Entity\TDumeNumero;
use App\Entity\TGroupementEntreprise;
use App\Entity\TListeLotsCandidature;
use App\Entity\TMembreGroupementEntreprise;
use App\Entity\TNumeroReponse;
use App\Event\DepotValidationSynchronizeDocumentsEvent;
use App\Event\ResponseEvents;
use App\Event\ValidateResponseEvent;
use App\Listener\ResponseListener;
use App\Service\AtexoCandidatureMps;
use App\Service\AtexoChiffrementOffre;
use App\Service\AtexoConfiguration;
use App\Service\AtexoGroupement;
use App\Service\AtexoHorodatageOffre;
use App\Service\Consultation\ConsultationSaveDepotService;
use App\Service\Consultation\ConsultationService;
use App\Service\DossierVolumineux\DossierVolumineuxService;
use App\Service\Handler\EmailAgentHandler;
use App\Service\PlateformeVirtuelle\Context;
use Doctrine\DBAL\Exception as DBALException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\Exception\ORMException;
use Exception;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/entreprise/consultation')]
class ConsultationSaveDepotController extends ConsultationIndexDepotController
{
    public function __construct(
        RequestStack $request,
        EntityManagerInterface $em,
        LoggerInterface $logger,
        ObscureDataManagement $obscureDataManagement,
        AtexoConfiguration $moduleStateChecker,
        AtexoFichierSignature $atexoSignatureFiles,
        TranslatorInterface $translator,
        InscritOperationsTracker $inscritOperationTracker,
        AtexoUtil $atexoUtil,
        AtexoTelechargementConsultation $atexoTelechargementConsultation,
        UidManagement $uidManagement,
        EntrepriseVerificationService $entrepriseVerificationService,
        DumeService $dumeService,
        AtexoDumeService $atexoDumeService,
        GeolocalisationService $geolocalisationService,
        InfoRecapService $infoRecapService,
        CandidatureService $candidatureService,
        DocumentService $documentService,
        MountManager $mountManager,
        DossierVolumineuxService $dossiersVolumineuxService,
        ConsultationDepotService $consultationDepotService,
        ConsultationUploadDepotService $consultationUploadDepotService,
        private Context $context,
        private AtexoHorodatageOffre $atexoHorodatageOffre,
        private ResponseListener $responseListener,
        private AtexoChiffrementOffre $atexoChiffrementOffre,
        private EmailAgentHandler $emailAgentHandler,
        private TokenStorageInterface $usageTrackingTokenStorage,
        private AtexoGroupement $atexoGroupement,
        private AtexoEnveloppeFichier $atexoEnveloppeFichier,
        private AtexoCandidatureMps $atexoCandidatureMps,
        ParameterBagInterface $parameterBag,
        CurrentUser $currentUser
    ) {
        parent::__construct(
            $request,
            $em,
            $logger,
            $obscureDataManagement,
            $moduleStateChecker,
            $atexoSignatureFiles,
            $translator,
            $inscritOperationTracker,
            $atexoUtil,
            $atexoTelechargementConsultation,
            $uidManagement,
            $entrepriseVerificationService,
            $dumeService,
            $atexoDumeService,
            $geolocalisationService,
            $infoRecapService,
            $candidatureService,
            $documentService,
            $mountManager,
            $dossiersVolumineuxService,
            $consultationDepotService,
            $consultationUploadDepotService,
            $usageTrackingTokenStorage,
            $parameterBag,
            $currentUser
        );
    }

    /**
     * @param int $id ID de la consultation
     * @throws Exception*/
    #[Route(
        path: '/depot/{id}/enregistrer',
        name: 'atexo_consultation_depot_enregistrer',
        requirements: ['id' => '\d+'],
        options: ['expose' => true],
        methods: ['POST']
    )]
    public function enregistrerAction(
        int $id,
        Request $request,
        DossierVolumineuxService $dossiersVolumineuxService,
        ConsultationService $consultationService,
        ConsultationSaveDepotService $consultationSaveDepotService,
        MessageBusInterface $bus
    ): Response {
        $errors = [];
        $reponse = $request->get('reponse');
        $uidResponse = $reponse['uid_response'];
        $validationAnticipee = $reponse['validationAnticipee'];
        $offreId = $this->obscureDataManagement->getDataFromObscuredValue($reponse['offre_id'], $uidResponse);
        $dossiersVolumineux =
            json_decode($reponse['dossiersVolumineux'], null, 512, JSON_THROW_ON_ERROR);
        try {
            $this->em->beginTransaction();
            $this->offre = $consultationService->getOffre($offreId, $uidResponse);
            $this->consultation =
                $consultationService->getConsultation($id, ConsultationService::CONSULTATION_TYPE_CLOTURES, true);
            $emailAR = $reponse['adresse_ar'];

            if ($this->offre->getStatutOffres() == $this->getParameter('STATUT_ENV_BROUILLON')) {
                if (1 == $validationAnticipee) {
                    [$idValeurReferentielle, $description] =
                        $this->inscritOperationTracker->getInfosValeursReferentielles('DESCRIPTION41');
                    $this->inscritOperationTracker->tracerOperationsInscrit(
                        new DateTime(),
                        new DateTime(),
                        $idValeurReferentielle,
                        $description,
                        $this->consultation,
                        '',
                        false,
                        null,
                        1,
                        null,
                        $this->offre->getId()
                    );
                    $consultationService->validerFichiersTraitement($id, $request, false, $this->offre);
                }

                /** @var Context $contextService */
                $contextService = $this->context;
                $pfv = $contextService->getCurrentPlateformeVirtuelle();
                $this->offre->setPlateformeVirtuelle($pfv);

                $this->offre->setEnvoiComplet(1);
                $this->offre->setDateDepot(new DateTime());

                $dateDebutValidationOffre = $this->session->get(
                    $this->session->get('token_contexte_authentification_' . $id) .
                    "/$uidResponse/infos_validation_depot_" . $id . $offreId . '/date_debut'
                );

                $dateDebutValidationOffre ??= new DateTime();

                if (!empty($emailAR)) {
                    $this->offre->setMailSignataire($emailAR);
                }

                foreach ($this->offre->getEnveloppes() as $enveloppe) {
                    if ($enveloppe instanceof Enveloppe) {
                        $nbre = $this->em
                            ->getRepository(EnveloppeFichier::class)
                            ->getNombreEnveloppeFichier($enveloppe->getIdEnveloppeElectro());

                        $info = "Nombre de fichiers dans l'enveloppe : Offre => " . $this->offre->getId() .
                            " | Enveloppe => " . $enveloppe->getIdEnveloppeElectro() . " | Nombre Fichiers => " . $nbre;
                        $this->depotLogger->info($info);
                        if ($nbre == 0) {
                            $info = "Suppression de l'enveloppe vide : Offre => " .
                                $this->offre->getId() . " | Enveloppe => " . $enveloppe->getIdEnveloppeElectro();
                            $this->depotLogger->info($info);
                            $this->em->remove($enveloppe);
                            $this->offre->removeEnveloppe($enveloppe);
                        } elseif (!empty($dossiersVolumineux[$enveloppe->getSousPli()])) {
                            $dossiersVolumineuxService->setDossierVolumineuxFromEnveloppe(
                                $enveloppe,
                                $dossiersVolumineux[$enveloppe->getSousPli()]
                            );
                        }
                    }
                }

                if ($this->offre->getEnveloppes()->isEmpty()) {
                    $erreur = "[uid_response = $uidResponse] Une erreur est survenu lors du dépot de votre offre : 
                        Il n'y a pas des enveloppes pour l'offre => " . $this->offre->getId();
                    $this->depotLogger->error($erreur);
                    $errors[] = $erreur;
                } else {
                    $this->ajouterEnveloppeFictive($uidResponse, $consultationService);
                    $this->gestionCandidatureMps($uidResponse, $id);


                    $configGroupement = $this->moduleStateChecker->hasConfigPlateforme("groupement");

                    if ($configGroupement) {
                        $atexoGroupement = $this->atexoGroupement;
                        $atexoGroupement->saveGroupementEntreprise(
                            $this->request,
                            $offreId,
                            $this->consultation,
                            $uidResponse
                        );
                    }

                    if ($this->moduleStateChecker->hasConfigPlateforme('interfaceModuleSub')) {
                        $consultationService->createEnveloppeDossierFormulaire($this->offre);
                    }


                    if ($this->consultation->getChiffrementOffre() == '1') {
                        $this->offre->setStatutOffres($this->getParameter('STATUT_ENV_EN_ATTENTE_CHIFFREMENT'));
                    } else {
                        $this->offre->setStatutOffres($this->getParameter('STATUT_ENV_FERMETURE_EN_ATTENTE'));
                    }
                    $this->atexoHorodatageOffre
                        ->demanderHorodaterOffre($this->offre, $request);
                    if (!$this->offre->getNumeroReponse()) {
                        $tNumeroReponse = $this->em->getRepository(TNumeroReponse::class)->
                            geTNumReponseFromConsultation(
                                $this->consultation->getId(),
                                $this->consultation->getOrganisme()
                            );
                        $nbReponses = $tNumeroReponse->getNumeroReponse() + 1;
                        $tNumeroReponse->setNumeroReponse($nbReponses);
                        $this->em->persist($tNumeroReponse);
                        $this->offre->setNumeroReponse($nbReponses);
                    }

                    //Part select lot
                    if ($this->consultation->getAlloti()) {
                        $listLotsCandidature = $this->em
                            ->getRepository(TListeLotsCandidature::class)
                            ->getListeLotByUserAndConsultationAndWithCandidature(
                                $this->user,
                                $this->consultation,
                                $this->getParameter('STATUT_ENV_BROUILLON')
                            );

                        foreach ($listLotsCandidature as $value) {
                            $value->setStatus($this->getParameter('STATUT_ENV_FERME'));
                            $this->em->persist($value);
                        }
                    }

                    $candidature = $this->em
                        ->getRepository(TCandidature::class)
                        ->findOneBy([
                            'organisme' => $this->consultation->getAcronymeOrg(),
                            'consultation' => $this->consultation->getId(),
                            'idInscrit' => $this->user->getId(),
                            'idEntreprise' => $this->user->getEntrepriseId(),
                            'idEtablissement' => $this->user->getIdEtablissement(),
                            'status' => $this->getParameter('STATUT_ENV_BROUILLON')
                        ]);

                    if (!$candidature instanceof TCandidature) {
                        return $this->getError($id);
                    }

                    $candidature->setStatus($this->getParameter('STATUT_ENV_FERME'));
                    $candidature->setIdOffre($this->offre);

                    if ($candidature->getIdDumeContexte()) {
                        $candidature->getIdDumeContexte()->setStatus(
                            $this->getParameter('STATUT_DUME_CONTEXTE_VALIDE')
                        );
                        $candidature->setDateDerniereValidationDume(new DateTime());
                    }

                    $this->em->persist($candidature);

                    if (
                        $candidature->getIdDumeContexte() &&
                        $candidature->getTypeCandidature() == $this->parameterBag->get('TYPE_CANDIDATUE_DUME') &&
                        $candidature->getTypeCandidatureDume() ==
                        $this->parameterBag->get('TYPE_CANDIDATUE_DUME_ONLINE')
                    ) {
                        $tGroupementEntreprise = $this->em
                            ->getRepository(TGroupementEntreprise::class)
                            ->findOneBy([
                                'candidature' => $candidature->getId()
                            ]);
                        $validationDumeEntreprise = true;
                        if ($tGroupementEntreprise) {
                            $mandataire = $this->em
                                ->getRepository(TMembreGroupementEntreprise::class)
                                ->findOneBy([
                                    'idGroupementEntreprise' => $tGroupementEntreprise->getIdGroupementEntreprise(),
                                    'idRoleJuridique' => $this->parameterBag->get('ID_ROLE_JURIDIQUE_MANDATAIRE')
                                ]);

                            if ($mandataire instanceof TMembreGroupementEntreprise) {
                                $validationDumeEntreprise = false;
                                $numeroSnMandataire = $this->em
                                    ->getRepository(TDumeNumero::class)
                                    ->findOneBy([
                                        'idDumeContexte' => $candidature->getIdDumeContexte()->getId()
                                    ]);

                                $mandataire->setNumeroSn($numeroSnMandataire->getNumeroDumeNational());

                                $this->em->persist($mandataire);
                                //$errors est mis à jour si une erreur intervient dans la fonction
                                $this->depotLogger->info('lancement message DumePublication 
                                    de type DumePublication en mode publication');
                                $bus->dispatch(new AskDumePublicationGroupementEntreprise(
                                    $tGroupementEntreprise->getIdGroupementEntreprise()
                                ));
                            }
                        }
                        if ($validationDumeEntreprise) {
                            $this->depotLogger->info('lancement message DumePublication 
                                    de type DumePublication en mode publication');
                            $bus->dispatch(new DumePublication(
                                'entreprise',
                                false,
                                $candidature->getIdDumeContexte()->getContexteLtDumeId()
                            ));
                        }
                    }

                    /**
                     * @info envoi des mails aux agents et à l'inscrit
                     */
                    $idCandidatureMps = ($this->session->get(
                        $this->session->get('token_contexte_authentification_' . $id)
                            . "/$uidResponse/type_candidature_mps"
                    ) == 1) ?
                        $this->session->get($this->session->get('token_contexte_authentification_' . $id)
                        . "/$uidResponse/id_candidature_mps") ?: 0 : 0;
                    $eventDispatcher = new EventDispatcher();
                    $listener = $this->responseListener;

                    $errors = $consultationSaveDepotService->addJobEmailAR(
                        $this->consultation->getId(),
                        $this->offre->getId(),
                        $this->user->getId(),
                        $idCandidatureMps
                    );

                    /**
                     * @info Synchroniser les documents entreprise/etablissement de l'inscrit
                     */
                    if ($this->getParameter('synchro_document_entreprise')) {
                        $synchronizeDocumentsevent = new DepotValidationSynchronizeDocumentsEvent(
                            $this->consultation,
                            $this->offre->getEntrepriseId(),
                            $this->offre->getIdEtablissement(),
                            $this->offre->getId()
                        );

                        $eventDispatcher->addListener(
                            ResponseEvents::ON_DEPOTVALIDATION_SYNCHRONIZE_DOCUMENTS,
                            [
                                $listener,
                                ResponseEvents::CALLBACK[ResponseEvents::ON_DEPOTVALIDATION_SYNCHRONIZE_DOCUMENTS]
                            ]
                        );

                        $eventDispatcher->dispatch(
                            $synchronizeDocumentsevent,
                            ResponseEvents::ON_DEPOTVALIDATION_SYNCHRONIZE_DOCUMENTS
                        );
                    }

                    //Ajout de la consultation au panier de l'entreprise
                    if ($this->moduleStateChecker->hasConfigPlateforme('panierEntreprise')) {
                        $panierRepository = $this->em
                            ->getRepository(PanierEntreprise::class);

                        $panierEntreprise = $panierRepository
                            ->getPanierEntreprise(
                                $this->consultation->getOrganisme(),
                                $this->consultation->getId(),
                                $this->offre->getEntrepriseId(),
                                $this->getUser()->getId()
                            );

                        if (!$panierEntreprise) {
                            $panierRepository->addConsultationToPanierEntreprise(
                                $this->consultation->getOrganisme(),
                                $this->consultation->getId(),
                                $this->offre->getEntrepriseId(),
                                $this->getUser()->getId()
                            );
                        }
                    }

                    //Redirection vers la page de confirmation
                    $dateDepot = $this->offre->getUntrusteddate();
                    //Permet de supprimer un eventuel message deja enregistre dans le flashbag
                    $this->session->getFlashBag()->get('info-depot-confirmation');
                    $this->session->getFlashBag()->add('info-depot-confirmation', $dateDepot->format("d/m/Y H:i:s"));

                    $idValeurReferentielle = $this->session->get(
                        $this->session->get('token_contexte_authentification_' . $id) .
                        "/$uidResponse/infos_validation_depot_" . $id . $offreId .
                        '/id_valeur_referentielle_info_tracker'
                    );
                    $description = $this->session->get(
                        $this->session->get('token_contexte_authentification_' . $id) .
                        "/$uidResponse/infos_validation_depot_" . $id . $offreId .
                        '/description_info_tracker'
                    );
                    $detailsFiles = $this->session->get(
                        $this->session->get('token_contexte_authentification_' . $id) .
                        "/$uidResponse/infos_validation_depot_" . $id . $offreId .
                        '/detail_files_info_tracker'
                    );
                    $this->inscritOperationTracker->tracerOperationsInscrit(
                        $dateDebutValidationOffre,
                        new DateTime(),
                        $idValeurReferentielle,
                        $description,
                        $this->consultation,
                        $detailsFiles,
                        false,
                        null,
                        1,
                        null,
                        $this->offre->getId(),
                        null,
                        false
                    );
                }
                $this->em->commit();

                if (!$this->offre->getEnveloppes()->isEmpty()) {
                    // changer extension des fichiers de brouillon à en attente chiffrement
                    try {
                        $extension = $this->getParameter('EXTENSION_ATTENTE_CHIFFREMENT');
                        if ($this->consultation->getChiffrementOffre() === '0') {
                            $extension = $this->getParameter('EXTENSION_NON_CHIFFRE');
                        }

                        $this->atexoEnveloppeFichier
                            ->changeExtensionFichiersOffre(
                                $offreId,
                                $this->offre->getOrganisme(),
                                $extension
                            );
                    } catch (Exception $e) {
                        $erreur = "[uid_response = $uidResponse] "
                            . "Une erreur est survenu lors de modification des extensions des fichiers de l'offre :  "
                            . $offreId
                            . "  pour la consultation=> "
                            . $this->consultation->getReference()
                            . " Erreur => "
                            . $e->getMessage()
                            . "   "
                            . $e->getTraceAsString();

                        $this->depotLogger->error($erreur);
                    }
                    /**
                     * @info Chiffrement des fichiers
                     */
                    if ($this->consultation->getChiffrementOffre() !== '0') {
                        $this->atexoChiffrementOffre->demanderChiffrementOffre($this->offre);
                    } else {
                        $this->offre->setStatutOffres($this->getParameter('STATUT_ENV_FERME'));
                    }
                }
            } else {
                $erreur = "[uid_response = $uidResponse]  erreur lors du depot de l'offre. Erreur : " .
                    $this->offre->getId() . " le statuts de l'offre est invalide =>  " .
                    $this->offre->getStatutOffres();
                $this->depotLogger->error($erreur);
                $errors[] = $erreur;
            }
        } catch (Exception $e) {
            $erreur = "[uid_response = $uidResponse] Une erreur est survenu lors du dépot de votre offre :  "
                . $offreId
                . "  pour la consultation=> "
                . $this->consultation->getId()
                . " Erreur => "
                . $e->getMessage()
                . "   "
                . $e->getTraceAsString();

            $this->depotLogger->error($erreur);
            $errors[] = $erreur;

            if ($e instanceof DBALException) {
                $this->em->rollback();
            }
        }
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        if (!empty($errors)) {
            $response->setContent(json_encode([
                'redirect' => $this->generateUrl('atexo_consultation_echec_depot', [
                    'consultation_id' => $id,
                    'offre_id' => $this->offre->getId()
                ])
            ], JSON_THROW_ON_ERROR));
        } else {
            $date = new DateTime();
            [$idValeurReferentielle, $description] =
                $this->inscritOperationTracker->getInfosValeursReferentielles('DESCRIPTION39');
            $description = str_replace('[_nom_page_]', "Confirmation dépôt", $description);
            $lienDownload = "index.php?page=Agent.DownloadRecapReponse&idOffre=" . $this->offre->getId() .
                "&orgAcronyme=" . $this->consultation->getOrganisme();
            $this->inscritOperationTracker->tracerOperationsInscrit(
                $date,
                $date,
                $idValeurReferentielle,
                $description,
                $this->consultation,
                '',
                false,
                $lienDownload,
                1,
                null,
                $this->offre->getId()
            );

            $response->setContent(json_encode([
                'redirect' => $this->generateUrl('atexo_consultation_depot_confirmation', [
                    'consultation_id' => $id,
                    'offre_id' => $this->offre->getId(),
                    'uidResponse' => $uidResponse
                ])
            ], JSON_THROW_ON_ERROR));
        }
        return $response;
    }

    /**
     * Verifie si une enveloppe de candidature fictive
     * doit etre ajouter avant l'enregistrement de l'offre.
     */
    protected function ajouterEnveloppeFictive($uidResponse, ConsultationService $consultationService)
    {
        if (
            $this->consultation->getEnvCandidature() &&
            !$this->offre->hasEnveloppeCandidature($this->getParameter('TYPE_ENV_CANDIDATURE'))
        ) {
            $consultationService->createEnveloppe(
                $uidResponse,
                $this->getParameter('TYPE_ENV_CANDIDATURE'),
                0,
                $this->consultation->getAcronymeOrg(),
                true,
                true,
                $this->offre
            );
        }
        if (
            $this->consultation->getEnvOffre() &&
            !$this->offre->hasEnveloppeOffre($this->getParameter('TYPE_ENV_OFFRE'))
        ) {
            $lots = [];
            if ($this->consultation->getAlloti()) {
                $listLotsCandidature = $this->em
                    ->getRepository(TListeLotsCandidature::class)
                    ->getListeLotByUserAndConsultationAndWithCandidature(
                        $this->user,
                        $this->consultation,
                        $this->getParameter('STATUT_ENV_BROUILLON')
                    );

                foreach ($listLotsCandidature as $value) {
                    $lots[] = $value->getNumLot();
                }
            } else {
                $lots[] = '0';
            }
            foreach ($lots as $key => $lot) {
                $consultationService->createEnveloppe(
                    $uidResponse,
                    $this->getParameter('TYPE_ENV_OFFRE'),
                    $lot,
                    $this->consultation->getAcronymeOrg(),
                    true,
                    true,
                    $this->offre
                );
            }
        }
    }

    /**
     * gestion candidature MPS interne ou externe.
     *
     * @param $uidResponse
     * @param $consultationId :ID de la consultation
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function gestionCandidatureMps($uidResponse, $consultationId)
    {
        $this->saveCandidatureMps($uidResponse, $consultationId);
        $this->candidatureExterne($uidResponse, $consultationId);
    }

    /**
     * @param $uidResponse
     *
     * @throws OptimisticLockException
     */
    public function candidatureExterne($uidResponse, $consultationId)
    {
        $idCandidatureMps = $this->session->get(
            $this->session->get('token_contexte_authentification_' . $consultationId) .
            "/$uidResponse/id_candidature_mps"
        );

        $typeCandidature = $this->session->get(
            $this->session->get('token_contexte_authentification_' . $consultationId) .
            "/$uidResponse/type_candidature_mps"
        );

        if (2 == $typeCandidature) {
            $this->offre->setCandidatureIdExterne($idCandidatureMps);
            $this->depotLogger->info("saved CandidatureIdExterne $idCandidatureMps offre " . $this->offre->getId());
            $this->em->persist($this->offre);
            $this->em->flush($this->offre);
            $this->depotLogger->info(
                'code pin consultation ' . $this->consultation->getId() .
                ' avant recuperation ' . $this->consultation->getPinApiSgmapMps()
            );
        }
    }

    /**
     * save candidature MPS.
     *
     * @param $uidResponse
     * @param $consultationId
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function saveCandidatureMps($uidResponse, $consultationId)
    {
        $candidatureMPS = $this->consultationDepotService->getCandidatureMps(
            $uidResponse,
            $consultationId,
            $this->consultation,
            $this->getUser()
        );

        if ($candidatureMPS instanceof CandidatureMps) {
            $candidatureMPS->setIdOffre($this->offre->getId());
            $this->em->persist($candidatureMPS);
            $this->em->flush();
        }
    }

    private function getError($id) {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $date = new DateTime();
        [$idValeurReferentielle, $description] =
            $this->inscritOperationTracker->getInfosValeursReferentielles('DESCRIPTION39');
        $description = str_replace('[_nom_page_]', "Confirmation dépôt", $description);
        $lienDownload = "index.php?page=Agent.DownloadRecapReponse&idOffre=" . $this->offre->getId() .
            "&orgAcronyme=" . $this->consultation->getOrganisme();
        $this->inscritOperationTracker->tracerOperationsInscrit(
            $date,
            $date,
            $idValeurReferentielle,
            $description,
            $this->consultation,
            '',
            false,
            $lienDownload,
            1,
            null,
            $this->offre->getId()
        );

        return $response->setContent(json_encode([
            'redirect' => $this->generateUrl('atexo_consultation_echec_depot', [
                'consultation_id' => $id,
                'offre_id' => $this->offre->getId()
            ])
        ], JSON_THROW_ON_ERROR));

    }
}
