<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise\Consultation;

use App\Service\AtexoUtil;
use App\Controller\AtexoController;
use App\Entity\EnveloppeFichier;
use App\Service\AtexoFichierOrganisme;
use App\Service\AtexoSignature;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DownloadController de telechargement de l'outil de signature
 */
class ConsultationDownloadSignatureController extends AtexoController
{
    public function __construct(private AtexoUtil $atexoUtil)
    {
    }
    /**
     * Permet de telecharger l'outil signature.
     *
     * @return Response
     */
    #[Route(path: '/download/outilSignature/', name: 'atexo_download_outil_signature')]
    public function downloadOutilSignatureAction(AtexoSignature $atexoSignature)
    {
        $contentJnlp = $atexoSignature->getJnlpToSignDocument();
        $fileName = $this->getParameter('NOM_FICHIER_WEBSTART_SIGNATURE');
        $extension = 'application/x-java-jnlp-file';
        return $this->downloadFile($contentJnlp, $fileName, $extension);
    }

    /**
     * Permet de telecharger le fichier signature.
     *
     * @ParamConverter("fichierEnveloppe", options={"mapping": {"id_fichier": "idFichier"}})
     *
     * @return string
     */
    #[Route(path: '/download/signature/{id_fichier}', requirements: ['id_fichier' => '\d+'], name: 'atexo_download_fichier_signature')]
    public function downloadFichierSignatureAction(EnveloppeFichier $fichierEnveloppe, AtexoFichierOrganisme $atexoBlobOrganisme, LoggerInterface $logger)
    {
        $fileName = $this->getDoctrine()
            ->getRepository(EnveloppeFichier::class)
            ->setLogger($logger)
            ->getNomFichierSignature($fichierEnveloppe);
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);
        $content = $atexoBlobOrganisme->getContentSignatureFichier($fichierEnveloppe);
        if ('' === $content) {
            $content   = $fichierEnveloppe->getSignatureFichier();
        }
        $fileName = $this->atexoUtil->OterAccents($fileName);
        return $this->downloadFile(base64_decode($content), $fileName, $extension);
    }
}
