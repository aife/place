<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise\Consultation;

use Exception;
use DateTime;
use App\Entity\Consultation;
use App\Entity\Inscrit;
use App\Entity\Lot;
use App\Entity\Organisme;
use App\Entity\TCandidature;
use App\Entity\TDumeContexte;
use App\Entity\TListeLotsCandidature;
use App\Exception\ConsultationNotFoundException;
use App\Service\Consultation\ConsultationDepotService;
use App\Service\Consultation\ConsultationService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\Exception\ORMException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/entreprise/consultation')]
class ConsultationDumeController extends ConsultationIndexDepotController
{
    /**
     * Permet d'afficher le message de confirmation de dume cotraitant.
     *
     * @ParamConverter("consultation", options={"mapping": {"consultation_id": "id"}})
     *
     * @param Consultation $consultation : objet   consultation
     * @param int          $dumeId       : integer dumeId
     *
     * @return Response
     * @throws Exception
     */
    #[Route(path: '/{consultation_id}/confirmation/dume/{dume_id}', requirements: ['consultation_id' => '\d+'], name: 'atexo_consultation_depot_dume_cotraitant_confirmation')]
    public function confirmationDumeAction(Consultation $consultation, $dumeId)
    {
        $tDume = $this->em
            ->getRepository(TDumeContexte::class)
            ->findOneBy(['id' => $dumeId, 'consultation' => $consultation]);
        if (
            null !== $this->getUser() &&
            $this->currentUser->isInscrit() &&
            ($tDume instanceof TDumeContexte)
        ) {
            //Récupérer le TDumeNumero lié au contexteDume
            $tDumeNumero = $this->dumeService->getDumeNumeroFromIdDumeContexte($tDume->getId());

            if ($tDumeNumero) {
                $lieuxExecutions = $this->geolocalisationService->getLieuxExecution($consultation);

                $liensRetour = $this->getParameter('URL_MPE') . 'index.php?page=Entreprise.EntrepriseDetailsConsultation&id=' . $consultation->getId() . '&orgAcronyme=' . $consultation->getOrganisme();

                $lienDetailLots = ($consultation->getAlloti()) ? $this->getParameter('URL_MPE') . '?page=Entreprise.PopUpDetailLots&orgAccronyme=' . $consultation->getOrganisme() . '&id=' . $consultation->getId() : '';

                $params = [
                    'consultation' => $consultation,
                    'lieuxExecutions' => $lieuxExecutions,
                    'lienRetour' => $liensRetour,
                    'lienDetailLots' => $lienDetailLots,
                    'dume' => $this->dumeService->getInfoDume($consultation),
                    'dumeNumero' => $tDumeNumero->getNumeroDumeNational(),
                ];
            } else {
                throw new Exception('Vous n\'avez pas le droit d\'accéder à cette page.');
            }
        } else {
            throw new Exception('Vous n\'avez pas le droit d\'accéder à cette page.');
        }
        return $this->render(
            'consultation/consultation-depot-dume-cotraitant-confirmation.html.twig',
            $params
        );
    }
    /**
     * Vérifie que le dume est valide.
     *
     *
     */
    #[Route(path: '/verifier/validation/Dume', options: ['expose' => true], name: 'atexo_consultation_verifier_validation_Dume')]
    public function verifierValiderDumeAction(Request $request) : Response
    {
        try {
            $urlRedirection = '';
            $afficherModal = false;
            $idCandidature = $request->get('idCandidature');
            $candidature = $this->em
                ->getRepository(TCandidature::class)
                ->findOneBy([
                    'id' => $idCandidature,
                ]);
            if ($candidature instanceof TCandidature && $candidature->getIdDumeContexte() && $candidature->getConsultation()) {
                if (1 == $candidature->getRoleInscrit()) {
                    $urlRedirection = $this->generateUrl('atexo_consultation_index', ['id' => $candidature->getConsultation()->getId()]) . '#depot';
                } else {
                    if (2 == $candidature->getRoleInscrit()) {
                        $clientDume = $this->atexoDumeService
                            ->createConnexionLtDume();

                        $validateDume = $clientDume->entrepriseService()->checkAccess(
                            $candidature->getIdDumeContexte()->getContexteLtDumeId()
                        );
                        if ($validateDume->getStatut() == $this->parameterBag->get('STATUT_VALIDATION_DUME_OK')) {
                            $dumeNumeroContext = $this->em
                                ->getRepository(TDumeContexte::class)
                                ->findOneBy([
                                    'id' => $candidature->getIdDumeContexte()->getId(),
                                ]);
                            if ($dumeNumeroContext instanceof TDumeContexte) {
                                $dumeNumeroContext->setStatus($this->getParameter('STATUT_DUME_CONTEXTE_VALIDE'));
                                $candidature->setStatus($this->getParameter('STATUT_ENV_FERME'));
                                $candidature->setDateDerniereValidationDume(new DateTime());
                                $listLotsCandidature = $this->em
                                    ->getRepository(TListeLotsCandidature::class)
                                    ->findBy([
                                        'consultation' => $candidature->getConsultation(),
                                        'organisme' => $candidature->getOrganisme(),
                                        'idInscrit' => $candidature->getIdInscrit(),
                                        'idEntreprise' => $candidature->getIdEntreprise(),
                                        'idEtablissement' => $candidature->getIdEtablissement(),
                                        'candidature' => $candidature,
                                        'status' => $this->getParameter('STATUT_ENV_BROUILLON'),
                                    ]);
                                if (is_array($listLotsCandidature) && count($listLotsCandidature)) {
                                    foreach ($listLotsCandidature as $value) {
                                        $value->setStatus($this->getParameter('STATUT_ENV_FERME'));
                                        $this->em->persist($value);
                                    }
                                }
                                $this->em->persist($dumeNumeroContext);
                                $this->em->persist($candidature);
                                $this->em->flush();
                                $urlRedirection = $this->generateUrl('atexo_consultation_depot_dume_cotraitant_confirmation', ['consultation_id' => $candidature->getConsultation()->getId(), 'dume_id' => $dumeNumeroContext->getId()]);
                            }
                        } else {
                            $afficherModal = true;
                        }
                    }
                }
            }

            return new Response(
                json_encode([
                    'urlRedirection' => $urlRedirection,
                    'afficherModal' => $afficherModal,
                ], JSON_THROW_ON_ERROR)
            );
        } catch (Exception $e) {
            return new Response(
                json_encode([
                    'message' => $e->getMessage(),
                    'error' => 1,
                ], JSON_THROW_ON_ERROR)
            );
        }
    }
    /**
     *
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    #[Route(path: '/depot/dume', name: 'atexo_consultation_dume_depot', methods: ['GET'])]
    public function dumeDepotConsultationAction(ConsultationService $consultationService, ConsultationDepotService $consultationDepotService) : RedirectResponse|Response
    {
        $idContextDume = [];
        $paramsConnected = [];
        $lotsNum = null;
        $lots = [];
        $idContextDume['idContexteOE'] = -1;
        $idConsultation = $this->request->query->get('id_consultation');
        $numSN = null;
        if ($this->request->query->get('numSn')) {
            $numSN = $this->request->query->get('numSn');
        }
        $acronymeOrg = $this->request->query->get('acronyme');
        $accessToken = false;
        $first = false;
        //Check all parameters are defined and not empty
        if (
            isset($idConsultation) &&
            isset($acronymeOrg) &&
            !empty($idConsultation) &&
            !empty($acronymeOrg)
        ) {
            $alloti = $this->request->query->get('alloti');

            $consultation = $this->em
                ->getRepository(Consultation::class)
                ->find($idConsultation);

            $organisme = $this->em
                ->getRepository(Organisme::class)
                ->find($acronymeOrg);

            //Check if consultation is alloti
            if (1 == $alloti) {
                //Check parameter lotsNum to retrieve some information to lt_dume
                $lotsNum = $this->request->query->get('lotsNum');
                if (
                    isset($lotsNum) &&
                    !empty($lotsNum)
                ) {
                    $dataLots = $this->em
                        ->getRepository(Lot::class)
                        ->getIntituleAndNumLot(
                            $idConsultation,
                            $acronymeOrg,
                            $lotsNum
                        );

                    foreach ($dataLots as $value) {
                        $lotDume = $this->atexoDumeService
                            ->createLotDume($value['num_lot'], $value['intitule_lot']);

                        $lots[] = $lotDume;
                    }
                } elseif (isset($idConsultation) && !empty($idConsultation)) {
                    return $this->redirectToRoute('atexo_consultation_index', ['id' => $idConsultation], 301);
                } else {
                    throw new ConsultationNotFoundException(404, 'Consultation non disponible');
                }
            }

            $siretAndTypeOE = $consultationService->getSiretAndTypeOE($this->user);

            $siret = $siretAndTypeOE['siret'];
            $typeOE = $siretAndTypeOE['typeOE'];
            $etablissement = $siretAndTypeOE['etablissement'];

            $plateforme = $this->getParameter('UID_PF_MPE');

            $candidature = $consultationDepotService->updateTypeCandidatureAndRelationListLotCandidature(
                $consultation,
                $this->user,
                $this->request->query->get('phase'),
                $this->request->query->get('choice')
            );

            //Appel lt_dume
            try {
                $inscrit = $this->em->getRepository(Inscrit::class)->find($this->user->getId());

                $dumeContexteAcheteur = $this->dumeService->getDumeContexteAcheteurPublie(
                    $idConsultation,
                    $acronymeOrg
                );

                $dumeContexteEntrepriseFinish = $this->dumeService->getDumeContexteEntrepriseExistant(
                    $idConsultation,
                    $acronymeOrg,
                    $this->user,
                    $etablissement
                );

                $dumeContexteEntreprise = $this->dumeService->getDumeContexteEntrepriseBrouillon(
                    $idConsultation,
                    $acronymeOrg,
                    $this->user,
                    $etablissement
                );

                if (!empty($dumeContexteEntrepriseFinish)) {
                    $candidatureEntreprise = $this->em
                        ->getRepository(TCandidature::class)
                        ->findBy([
                            'consultation' => $idConsultation,
                            'organisme' => $acronymeOrg,
                            'idEntreprise' => $this->user->getEntrepriseId(),
                            'idEtablissement' => $etablissement,
                            'typeCandidature' => $this->parameterBag->get('TYPE_CANDIDATUE_DUME'),
                            'typeCandidatureDume' => $this->parameterBag->get('TYPE_CANDIDATUE_DUME_ONLINE'),
                            'idDumeContexte' => $dumeContexteEntrepriseFinish[0]['id'],
                        ]);

                    if (empty($candidatureEntreprise)) {
                        $first = true;
                    }
                }

                $clientDume = $this->atexoDumeService
                    ->createConnexionLtDume();

                $metadataDume = $this->atexoDumeService
                    ->createMetadataLtDume(
                        $plateforme,
                        $idConsultation,
                        $dumeContexteAcheteur[0]['contexteLtDumeId'],
                        $this->user->getId(),
                        $siret,
                        $typeOE,
                        $first
                    );

                $contact = $this->atexoDumeService
                    ->createContactDume($inscrit);

                $consultationDume = $this->atexoDumeService
                    ->createConsultationDume($lots, $metadataDume, $contact, $alloti);

                if (empty($dumeContexteEntreprise)) {
                    $idContextDumeJson = $clientDume->entrepriseService()->createDume($consultationDume);

                    $idContextDume = json_decode($idContextDumeJson, true, 512, JSON_THROW_ON_ERROR);

                    $dumeContexteCreated = $this->dumeService->createDumeContexte(
                        $consultation,
                        $organisme,
                        $idContextDume['idContexteOE']
                    );

                    $this->dumeService->createDumeNumero($dumeContexteCreated, $idContextDume['numeroSN']);

                    if (($dumeContexteCreated instanceof TDumeContexte) && ($candidature instanceof TCandidature)) {
                        $candidature->setIdDumeContexte($dumeContexteCreated);
                        $this->em->persist($candidature);
                        $this->em->flush();
                    }

                    if ($numSN) {
                        try {
                            $dumeContexteEntrepriseSecondDepot = $this->dumeService->getDumeContexteEntrepriseBrouillon(
                                $idConsultation,
                                $acronymeOrg,
                                $this->user,
                                $etablissement
                            );

                            $clientDume->entrepriseService()->createDume(
                                $consultationDume,
                                $dumeContexteEntrepriseSecondDepot[0]['contexteLtDumeId'],
                                $numSN
                            );
                        } catch (Exception $exc) {
                            if ($this->depotLogger) {
                                $this->depotLogger->error('Dume : Erreur lors de la fusion. Message : ' . $exc->getMessage());
                            }
                        }
                    }
                } else {
                    $idContextDume['idContexteOE'] = $clientDume->entrepriseService()->updateDume(
                        $consultationDume,
                        $dumeContexteEntreprise[0]['contexteLtDumeId']
                    );
                }

                $paramsConnected['idContextDume'] = $idContextDume['idContexteOE'];

                $accessToken = $clientDume->entrepriseService()->getClientAccessToken($clientDume, $idContextDume['idContexteOE']);
            } catch (Exception) {
                $paramsConnected['validateDume'] = $this->getParameter('LT_DUME_DOWN');

                if (empty($dumeContexteAcheteur)) {
                    $this->session->getFlashBag()->add('error', $this->translator->trans('TEXT_LT_DUME_NE_REPOND_PAS'));

                    return $this->redirectToRoute(
                        'atexo_consultation_index',
                        [
                            'id' => $idConsultation,
                        ],
                        301
                    );
                }
            }
        } elseif (isset($idConsultation) && !empty($idConsultation)) {
            return $this->redirectToRoute('atexo_consultation_index', ['id' => $idConsultation], 301);
        } else {
            throw new ConsultationNotFoundException(404, 'Consultation non disponible');
        }
        $lienDetailLots = ($consultation->getAlloti()) ? $this->getParameter('URL_MPE') . '?page=Entreprise.PopUpDetailLots&orgAccronyme=' . $consultation->getOrganisme() . '&id=' . $consultation->getId() : '';
        $msgBoutonDume = (($candidature instanceof TCandidature) && '2' == $candidature->getRoleInscrit()) ? $this->translator->trans('TEXT_ENREGISTER_DUME_ET_VALIDER_CANDIDATURE') : $this->translator->trans('TEXT_ENREGISTER_DUME_ET_RETOUR_A_LA_PAGE_PRECEDENTE');
        $params = [
            'consultation' => $consultation,
            'lienDetailLots' => $lienDetailLots,
            'inscrit' => $this->user,
            'url_dume' => $this->getParameter('URL_PF_DUME'),
            'idContextDume' => $idContextDume,
            'accessToken' => $accessToken,
            'msgBoutonDume' => $msgBoutonDume,
            'idCandidature' => $candidature->getId(),
            'dume' => $this->dumeService->getInfoDume($consultation),
        ];
        if (isset($lotsNum) && !empty($lotsNum)) {
            $params['lotsNum'] = $lotsNum;
        }
        $params = array_merge($paramsConnected, $params);
        return $this->render('consultation/dume.html.twig', $params);
    }
    /**
     * @Method({"POST"})
     *
     * @return Response
     */
    #[Route(path: '/validate_dume_select', name: 'atexo_consultation_validate_dume_select')]
    public function getDumeValidate()
    {
        $paramsConnected = [];
        $info = '';
        try {
            $clientDume = $this->atexoDumeService
                ->createConnexionLtDume();

            $clientDume->AuthentificationService()->login(
                $this->parameterBag->get('LOGIN_DUME_API'),
                $this->parameterBag->get('PASSWORD_DUME_API'),
                $this->parameterBag->get('TYPE_DUME_OE')
            );
        } catch (Exception) {
            $info = $this->getParameter('LT_DUME_DOWN');
        }
        $candidature = $this->em
            ->getRepository(TCandidature::class)
            ->findOneBy([
                'organisme' => $_POST['id_organisme'],
                'consultation' => $_POST['id_consultation'],
                'idInscrit' => $_POST['id_inscrit'],
                'idEntreprise' => $_POST['id_entreprise'],
                'idEtablissement' => $_POST['id_etablissement'],
                'status' => $this->getParameter('STATUT_ENV_BROUILLON'),
            ]);
        if (null !== $candidature) {
            $paramsConnected['candidature'] = $candidature;

            // Appel webservice validation lt_dume
            if (
                null !== $candidature->getIdDumeContexte() &&
                null !== $candidature->getIdDumeContexte()->getContexteLtDumeId()
            ) {
                try {
                    $clientDume = $this->atexoDumeService
                        ->createConnexionLtDume();

                    $validateDume = $clientDume->entrepriseService()->checkAccess(
                        $candidature->getIdDumeContexte()->getContexteLtDumeId()
                    );

                    if ('NON_VALIDE' == $validateDume->getStatut()) {
                        $info = $this->getParameter('NOK');
                    } elseif ('VALIDE' == $validateDume->getStatut()) {
                        $info = $this->getParameter('OK');
                    }
                } catch (Exception) {
                    $info = $this->getParameter('LT_DUME_DOWN');
                }
            }
        }
        return new Response(
            json_encode(
                [
                    'message' => $info,
                    'default' => [
                        'OK' => $info = $this->getParameter('OK'),
                        'NOK' => $info = $this->getParameter('NOK'),
                        'LT_DUME_DOWN' => $info = $this->getParameter('LT_DUME_DOWN'),
                    ],
                ], JSON_THROW_ON_ERROR
            )
        );
    }
}
