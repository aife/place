<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise\Consultation;

use App\Entity\Consultation;
use App\Entity\Inscrit;
use App\Entity\Lot;
use App\Entity\Organisme;
use App\Entity\TCandidature;
use App\Entity\TListeLotsCandidature;
use App\Exception\ConsultationNotFoundException;
use App\Service\Consultation\ConsultationService;
use Doctrine\ORM\Exception\ORMException;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/entreprise/consultation')]
class ConsultationAddLotToDepotController extends ConsultationIndexDepotController
{
    /**
     * @Method({"POST"})*/
    #[Route(path: '/depot/lot', name: 'atexo_consultation_ajout_lot_depot')]
    public function addLotDepotConsultationAction(ConsultationService $consultationService)
    {
        $consultation = $this->request->request->get('id_consultation');
        $organisme = $this->request->request->get('id_organisme');
        $idDumeContexte = $this->request->request->get('id_dume_contexte');
        $first = false;
        if (isset($consultation) &&
            isset($organisme)
        ) {
            $listLotPost = $this->request->request->get('list_lot');

            $listeLotCandidatureRepository = $this->em
                ->getRepository(TListeLotsCandidature::class);

            $listLot = $listeLotCandidatureRepository->findBy([
                'consultation' => $consultation,
                'organisme' => $organisme,
                'idInscrit' => $this->user->getId(),
                'idEntreprise' => $this->user->getEntrepriseId(),
                'idEtablissement' => $this->user->getIdEtablissement(),
                'status' => $this->getParameter('STATUT_ENV_BROUILLON'),
            ]);

            if (0 != count($listLot)) {
                foreach ($listLot as $value) {
                    $listeLotCandidature = $this->em
                        ->getRepository(TListeLotsCandidature::class)
                        ->findOneBy([
                            'consultation' => $consultation,
                            'organisme' => $organisme,
                            'idInscrit' => $this->user->getId(),
                            'idEntreprise' => $this->user->getEntrepriseId(),
                            'idEtablissement' => $this->user->getIdEtablissement(),
                            'numLot' => $value->getNumLot(),
                            'status' => $this->getParameter('STATUT_ENV_BROUILLON'),
                        ]);

                    $this->em->remove($listeLotCandidature);
                }

                $this->em->flush();
            }

            if (isset($listLotPost) && isset($this->user) && !empty($listLotPost)) {
                foreach ($listLotPost as $value) {
                    $listeLotCandidature = $this->addListeLotCandidature(
                        $consultation,
                        $organisme,
                        $value
                    );

                    $this->em->persist($listeLotCandidature);
                }

                $this->em->flush();
            }

            if (isset($idDumeContexte) && -1 != $idDumeContexte) {
                $lots = [];
                if (isset($listLotPost) && !empty($listLotPost)) {
                    $lotsString = '';

                    foreach ($listLotPost as $lot) {
                        $lotsString .= $lot.',';
                    }

                    $lotsString = trim($lotsString, ',');

                    $dataLots = $this->em
                        ->getRepository(Lot::class)
                        ->getIntituleAndNumLot(
                            $consultation,
                            $organisme,
                            $lotsString
                        );

                    foreach ($dataLots as $value) {
                        $lotDume = $this->atexoDumeService
                            ->createLotDume($value['num_lot'], $value['intitule_lot']);

                        $lots[] = $lotDume;
                    }
                }

                $siretAndTypeOE = $consultationService->getSiretAndTypeOE($this->user);

                $siret = $siretAndTypeOE['siret'];
                $typeOE = $siretAndTypeOE['typeOE'];
                $etablissement = $siretAndTypeOE['etablissement'];

                $plateforme = $this->getParameter('UID_PF_MPE');

                //Appel lt_dume
                try {
                    $dumeContexteAcheteur = $this->dumeService->getDumeContexteAcheteurPublie(
                        $consultation,
                        $organisme
                    );

                    $dumeContexteEntrepriseFinish = $this->dumeService->getDumeContexteEntrepriseExistant(
                        $consultation,
                        $organisme,
                        $this->user,
                        $etablissement
                    );
                    $dumeContexteEntreprise = $this->dumeService->getDumeContexteEntrepriseBrouillon(
                        $consultation,
                        $organisme,
                        $this->user,
                        $etablissement
                    );

                    $inscrit = $this->em->getRepository(Inscrit::class)->find($this->user->getId());

                    $candidatureUser = [];

                    if (empty($dumeContexteEntrepriseFinish)) {
                        $candidatureUser = $this->em
                            ->getRepository(TCandidature::class)
                            ->findBy([
                                'consultation' => $consultation,
                                'organisme' => $organisme,
                                'idInscrit' => $this->user->getId(),
                                'idEntreprise' => $this->user->getEntrepriseId(),
                                'idEtablissement' => $etablissement,
                                'typeCandidature' => $this->parameterBag->get('TYPE_CANDIDATUE_DUME'),
                                'typeCandidatureDume' => $this->parameterBag->get('TYPE_CANDIDATUE_DUME_ONLINE'),
                                'idDumeContexte' => $dumeContexteEntrepriseFinish->getId(),
                            ]);
                    }

                    if (empty($candidatureUser)) {
                        $first = true;
                    }

                    $clientDume = $this->atexoDumeService
                        ->createConnexionLtDume();

                    $metadataDume = $this->atexoDumeService
                        ->createMetadataLtDume(
                            $plateforme,
                            $consultation,
                            $dumeContexteAcheteur[0]['contexteLtDumeId'],
                            $this->user->getId(),
                            $siret,
                            $typeOE,
                            $first
                        );

                    $contact = $this->atexoDumeService
                        ->createContactDume($inscrit);

                    $consultationDume = $this->atexoDumeService
                        ->createConsultationDume($lots, $metadataDume, $contact, 1);

                    if (!empty($dumeContexteEntreprise)) {
                        $clientDume->entrepriseService()->updateDume(
                            $consultationDume,
                            $dumeContexteEntreprise[0]['contexteLtDumeId']
                        );
                    }
                } catch (Exception) {
                    if (empty($dumeContexteAcheteur)) {
                        $this->session->getFlashBag()->add('error', $this->translator->trans('TEXT_LT_DUME_NE_REPOND_PAS'));

                        return $this->redirectToRoute(
                            'atexo_consultation_index',
                            [
                                'id' => $consultation,
                            ],
                            301
                        );
                    }
                }
            }
        }
        return $this->render('Default-App/index.html.twig');
    }
    /**
     * @param $cons
     * @param $org
     * @param $numLot
     *
     *
     * @throws ORMException|Exception
     */
    private function addListeLotCandidature($cons, $org, $numLot): TListeLotsCandidature
    {
        $listeLotCandidature = new TListeLotsCandidature();

        $consultation = $this->em
            ->getRepository(Consultation::class)
            ->find($cons);
        $listeLotCandidature->setConsultation($consultation);

        if (!$consultation instanceof Consultation) {
            $this->depotLogger->error("Consultation not found");
            throw new ConsultationNotFoundException(404, 'Consultation non disponible');
        }

        $organisme = $this->em
            ->getRepository(Organisme::class)
            ->find($org);

        if (!$organisme instanceof Organisme) {
            $this->depotLogger->error("Organism not found");
            throw new Exception('Organisme non disponible');
        }

        $listeLotCandidature->setOrganisme($organisme);
        $listeLotCandidature->setIdInscrit($this->user->getId());
        $listeLotCandidature->setIdEntreprise($this->user->getEntrepriseId());
        $listeLotCandidature->setIdEtablissement($this->user->getIdEtablissement());
        $listeLotCandidature->setNumLot($numLot);

        // Trouver le TCandidature à rattacher
        $candidature = $this->em
            ->getRepository(TCandidature::class)
            ->findOneBy([
                'organisme' => $org,
                'consultation' => $cons,
                'idInscrit' => $this->user->getId(),
                'idEntreprise' => $this->user->getEntrepriseId(),
                'idEtablissement' => $this->user->getIdEtablissement(),
                'status' => $this->getParameter('STATUT_ENV_BROUILLON'),
            ]);

        if (null != $candidature) {
            $listeLotCandidature->setCandidature($candidature);
        } else {
            $candidature = $this->candidatureService->createCandidatureFromConsultation($consultation, $organisme);
            $this->em->persist($candidature);

            $listeLotCandidature->setCandidature($candidature);
        }

        return $listeLotCandidature;
    }
}
