<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise\Consultation;

use App\Doctrine\Extension\ConsultationIdExtension;
use App\Entity\Consultation;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\Typeavis;
use App\Entity\TypeProcedure;
use App\Form\Entreprise\ConsultationRechercheType;
use App\Service\Paginator;
use App\Service\Agent\ConsultationSimplifieeService;
use App\Service\WebservicesMpeConsultations;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\Exception\ServerException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SearchController.
 * @Route("/entreprise/consultation/recherche", name="entreprise_consultation_search_")
 */
class SearchController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private ConsultationSimplifieeService $consultationSimplifieeService,
        private WebservicesMpeConsultations $wsConsultations,
    ) {
    }

    /**
     * @Route("", name="index")
     */
    public function index(
        Request $request,
        LoggerInterface $logger,
        Paginator $paginator
    ): Response {
        $typesProcedure = $this->entityManager->getRepository(TypeProcedure::class)
            ->findAll();

        $formDataInit = $this->prepareFormDataInit($typesProcedure, $request, $this->consultationSimplifieeService);

        //TODO autre form
        $form = $this->createForm(ConsultationRechercheType::class, null, $formDataInit);
        $form->handleRequest($request);
        $consultations = new ArrayCollection();

        if ($form->isSubmitted()) {
            try {
                $data = $this->mapFilters($form->getData());
                if ($request->query->has('page')) {
                    $data['page'] = ($request->query->get('page') > 0) ? $request->query->get('page') : 1 ;
                }

                if ($request->query->has('itemsPerPage')) {
                    $data['itemsPerPage'] = $request->query->get('itemsPerPage');
                }
                $wsResponse = $this->wsConsultations->getContent('searchConsultations', $data, false);

                $idsList = [];
                $paginator = $paginator->getPagination($wsResponse->{'hydra:view'}, $request->getRequestUri());
                $paginator['total'] = $wsResponse->{'hydra:totalItems'};

                if ($data['page'] > $paginator['last'] && '' !== $paginator['last']) {
                    return $this->redirect($paginator['goto'] . $paginator['last']);
                }

                foreach ($wsResponse->{'hydra:member'} as $item) {
                    $idsList[] = $item->{'id'};
                }

                /** @var Consultation[] $consultations */
                $consultations = $this->entityManager->getRepository(Consultation::class)
                    ->findBy(['id' => $idsList], ['datefin' => 'DESC']);

                foreach ($consultations as $consultation) {
                    $consultation->typeProcedureOrg =
                        $this->entityManager->getRepository(TypeProcedureOrganisme::class)->findOneBy(
                            [
                                'idTypeProcedure' => $consultation->getIdTypeProcedureOrg(),
                                'organisme' => $consultation->getAcronymeOrg()
                            ]
                        );
                }
            } catch (ServerException $e) {
                $logger->error('Erreur à la récupération des consultations via API Platform');
                $this->addFlash('error', 'Erreur lors de la récupération des consultations.');
            }
        }

        return $this->render('consultation/Entreprise/search/index.html.twig', [
            'isFullWidth'       => true,
            'form'              => $form->createView(),
            'consultations'     => $consultations,
            'displayCriteria'   => $this->displayCriteria($request),
            'searchLaunched'    => $request->query->get('search') !== null,
            'paginator'         => $paginator
        ]);
    }

    /**
     * @Route("/restreinte", name="index_restreinte")
     */
    public function indexConsultationRestreinte(
        Request $request,
        LoggerInterface $logger,
        Paginator $paginator
    ): Response {
        $consultations = new ArrayCollection();

        if (!empty($request->query->get('reference')) && !empty($request->query->get('code'))) {
            try {
                $wsResponse = $this->wsConsultations->getContent(
                    'searchConsultations',
                    ['reference' => $request->query->get('reference'), 'codeProcedureRestreinte' => $request->query->get('code')],
                    false
                );

                $idsList = [];
                $paginator = $paginator->getPagination($wsResponse->{'hydra:view'}, $request->getRequestUri());
                $paginator['total'] = $wsResponse->{'hydra:totalItems'};

                foreach ($wsResponse->{'hydra:member'} as $item) {
                    $idsList[] = $item->{'id'};
                }

                /** @var Consultation[] $consultations */
                $consultations = $this->entityManager->getRepository(Consultation::class)
                    ->findBy(['id' => $idsList], ['datefin' => 'DESC']);

                foreach ($consultations as $consultation) {
                    $consultation->typeProcedureOrg =
                        $this->entityManager->getRepository(TypeProcedureOrganisme::class)->findOneBy(
                            [
                                'idTypeProcedure' => $consultation->getIdTypeProcedureOrg(),
                                'organisme' => $consultation->getAcronymeOrg()
                            ]
                        );
                }
            } catch (ServerException $e) {
                $logger->error('Erreur à la récupération des consultations via API Platform');
                $this->addFlash('error', 'Erreur lors de la récupération des consultations.');
            }
        }

        return $this->render('consultation/Entreprise/search/index-restreinte.html.twig', [
            'fullWidth'       => true,
            'consultations'     => $consultations,
//            'searchLaunched'    => $request->query->get('search') !== null,
            'paginator'         => $paginator
        ]);
    }

    protected function mapFilters($data): array
    {
        if (isset($data['keyWord'])) {
            $data['search_full'] = explode(' ', $data['keyWord']);
            unset($data['keyWord']);
        }

        if (isset($data['typeProcedureOrganisme'])) {
            $data['idTypeProcedureOrg'] = $data['typeProcedureOrganisme']->getIdTypeProcedure();
        }
        if (isset($data['typeAvis']) && $data['typeAvis']) {
            $data['idTypeAvis'] = $data['typeAvis']->getId();
        }

        if (isset($data['form_lieuxExecution'])) {
            $data['lieuExecution'] = explode(',', trim($data['form_lieuxExecution'], ','));
            unset($data['form_lieuxExecution']);
            unset($data['form_lieuxExecutionSelect']);
        }

        if (isset($data['form_CPV'])) {
            $data['codesCPV'] = explode(',', trim($data['form_CPV'], ','));
        }

        $dateFields = ['dlro' => 'datefin', 'miseEnLigne' => 'dateMiseEnLigneCalcule'];
        $dateFieldsSuffixes = ['Avant' => 'before', 'Apres' => 'after'];

        foreach ($dateFields as $dateField => $apiField) {
            foreach ($dateFieldsSuffixes as $dateFieldsSuffix => $apiFieldSuffix) {
                $fullDateField = $dateField . $dateFieldsSuffix;
                if (isset($data[$fullDateField]) && $data[$fullDateField] instanceof \DateTimeInterface) {
                    $data[$apiField][$apiFieldSuffix] = $data[$fullDateField]->format('Y-m-d');
                    unset($data[$fullDateField]);
                }
            }
        }

        $data[ConsultationIdExtension::IDS_ONLY] = 1;

        return $data;
    }

    protected function prepareFormDataInit(
        array $typesProcedure,
        Request $request,
        ConsultationSimplifieeService $consultationSimplifieeService
    ): array {
        $formDataInit = [
            'types_procedures' => $typesProcedure,
        ];

        $typesAvis = $this->entityManager->getRepository(Typeavis::class)->findAll();
        $formDataInit['types_avis'] = $typesAvis;

        if ($request->query->has('form_lieuxExecution')) {
            $lieuxExecutionIds = trim($request->query->get('form_lieuxExecution'), ',');
            $formDataInit['lieuxExecution'] = $consultationSimplifieeService->idsToGeoN2($lieuxExecutionIds);
            $formDataInit['lieuxExecutionIds'] = $lieuxExecutionIds;
        }

        return $formDataInit;
    }

    private function displayCriteria(Request $request): bool
    {
        $queryParams = $request->query->all();

        if (!isset($queryParams['search'])) {
            return true;
        }

        foreach ($queryParams as $key => $value) {
            if (!in_array($key, ['keyWord', 'statut', 'search', 'page', 'itemsPerPage']) && !empty($value)) {
                return true;
            }
        }

        return false;
    }
}
