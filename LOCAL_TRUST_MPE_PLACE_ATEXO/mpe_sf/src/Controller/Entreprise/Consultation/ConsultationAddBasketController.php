<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise\Consultation;

use App\Service\CurrentUser;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use App\Service\ObscureDataManagement;
use App\Service\AtexoConfiguration;
use App\Service\AtexoFichierSignature;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Service\InscritOperationsTracker;
use App\Service\AtexoUtil;
use App\Service\AtexoTelechargementConsultation;
use App\Security\UidManagement;
use App\Service\Entreprise\EntrepriseVerificationService;
use App\Service\Dume\DumeService;
use App\Service\AtexoDumeService;
use App\Service\GeolocalisationService;
use App\Service\InfoRecapService;
use App\Service\CandidatureService;
use App\Service\DocumentService;
use App\Utils\Filesystem\MountManager;
use App\Service\DossierVolumineux\DossierVolumineuxService;
use App\Service\Consultation\ConsultationDepotService;
use App\Service\Consultation\ConsultationUploadDepotService;
use App\Entity\Consultation;
use App\Entity\PanierEntreprise;
use App\Exception\ConsultationNotFoundException;
use App\Repository\PanierEntrepriseRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Route(path: '/entreprise/consultation')]
class ConsultationAddBasketController extends ConsultationIndexDepotController
{
    public function __construct(
        RequestStack $request,
        ContainerInterface $container,
        EntityManagerInterface $em,
        LoggerInterface $logger,
        ObscureDataManagement $obscureDataManagement,
        AtexoConfiguration $moduleStateChecker,
        AtexoFichierSignature $atexoSignatureFiles,
        TranslatorInterface $translator,
        InscritOperationsTracker $inscritOperationTracker,
        AtexoUtil $atexoUtil,
        AtexoTelechargementConsultation $atexoTelechargementConsultation,
        UidManagement $uidManagement,
        EntrepriseVerificationService $entrepriseVerificationService,
        DumeService $dumeService,
        AtexoDumeService $atexoDumeService,
        GeolocalisationService $geolocalisationService,
        InfoRecapService $infoRecapService,
        CandidatureService $candidatureService,
        DocumentService $documentService,
        MountManager $mountManager,
        DossierVolumineuxService $dossiersVolumineuxService,
        ConsultationDepotService $consultationDepotService,
        ConsultationUploadDepotService $consultationUploadDepotService,
        TokenStorageInterface $usageTrackingTokenStorage,
        ParameterBagInterface $parameterBag,
        CurrentUser $currentUser
    ) {
        parent::__construct(
            $request,
            $em,
            $logger,
            $obscureDataManagement,
            $moduleStateChecker,
            $atexoSignatureFiles,
            $translator,
            $inscritOperationTracker,
            $atexoUtil,
            $atexoTelechargementConsultation,
            $uidManagement,
            $entrepriseVerificationService,
            $dumeService,
            $atexoDumeService,
            $geolocalisationService,
            $infoRecapService,
            $candidatureService,
            $documentService,
            $mountManager,
            $dossiersVolumineuxService,
            $consultationDepotService,
            $consultationUploadDepotService,
            $usageTrackingTokenStorage,
            $parameterBag,
            $currentUser
        );
    }
    #[Route(path: '/ajout/panier', name: 'atexo_consultation_ajout_panier', methods: ['GET'])]
    public function addBasketConsultationAction(): Response
    {
        $message = '';
        /** @var PanierEntrepriseRepository $panierRepository */
        $panierRepository = $this->em->getRepository(PanierEntreprise::class);
        $idConsultation = $this->request->query->get('id_consultation');
        $idEntreprise = $this->request->query->get('id_entreprise');
        if (isset($idConsultation) && isset($idEntreprise)) {
            $consultation = $this->em->getRepository(Consultation::class)
                ->findOneBy(array('id' => $idConsultation));

            if (!$consultation instanceof Consultation) {
                $this->depotLogger->error("Consultation not found, reference : " . $idConsultation);
                throw new ConsultationNotFoundException(404, 'Consultation non disponible');
            }

            $message = $this->translator
                ->trans(
                    'SF_MESSAGE_CONSULTATION_EXISTE_DEJA_DANS_VOTRE_PANIER',
                    array('%consultation%' => $consultation->getReferenceUtilisateur())
                );
            /** @var UserInterface|object|null $user */
            $user = $this->getUser();
            $data = $panierRepository->getPanierEntreprise(
                $consultation->getOrganisme(),
                $consultation->getId(),
                $this->request->query->get('id_entreprise'),
                $user->getId()
            );

            if (!$data) {
                $panierRepository->addConsultationToPanierEntreprise(
                    $consultation->getOrganisme(),
                    $consultation->getId(),
                    $this->request->query->get('id_entreprise'),
                    $user->getId()
                );

                $message = $this->translator
                    ->trans(
                        'SF_CONFIRMATION_ENREGISTREMENT_CONSULTATION_PANIER',
                        array('%consultation%' => $consultation->getReferenceUtilisateur())
                    );
            }
        }
        return $this->render('consultation/consultation-panier-confirmation.html.twig', array(
            'message' => $message,
            'url_panier' => $this->getParameter('MENU_URL_PANIER_ENTREPRISE'),
            'id_consultation' => $idConsultation,
        ));
    }
}
