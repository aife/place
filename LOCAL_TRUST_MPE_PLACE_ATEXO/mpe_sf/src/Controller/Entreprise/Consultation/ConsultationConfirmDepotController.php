<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise\Consultation;

use Exception;
use App\Service\AtexoConfiguration;
use DateTime;
use App\Entity\Consultation;
use App\Entity\EnveloppeFichier;
use App\Entity\Inscrit;
use App\Entity\Offre;
use App\Entity\TCandidature;
use App\Entity\TDumeNumero;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/entreprise/consultation')]
class ConsultationConfirmDepotController extends ConsultationIndexDepotController
{
    /**
     * Permet d'afficher le message de confirmation
     *
     * @ParamConverter("consultation", options={"mapping": {"consultation_id": "id"}})
     * @ParamConverter("offre", options={"mapping": {"offre_id": "id"}})
     *
     * @param Consultation $consultation : objet consultation
     * @param Offre        $offre        : objet Offre
     * @param string       $uidResponse  : unique ID du depot de l'entreprise
     *
     * @return Response
     * @throws Exception
     */
    #[Route(path: '/depot/{consultation_id}/{offre_id}/confirmation/{uidResponse}', requirements: ['consultation_id' => '\d+'], name: 'atexo_consultation_depot_confirmation')]
    public function confirmationAction(Consultation $consultation, Offre $offre, string $uidResponse)
    {
        if (
            $this->getUser() !== null &&
            $this->currentUser->isInscrit() &&
            $this->getUser()->getId() === $offre->getInscritId()
        ) {
            $uidConfirmation = "confirmation_$uidResponse";
            $lieuxExecutions = $this->geolocalisationService->getLieuxExecution($consultation);

            $inscritDepotOffre = $this
                ->em
                ->getRepository(Inscrit::class)
                ->find($offre->getInscritId());

            $entreprise = $inscritDepotOffre->getEntreprise();

            $liensRetour = $this->parameterBag->get('URL_LT_MPE_SF') . DIRECTORY_SEPARATOR . 'consultation' . DIRECTORY_SEPARATOR . $consultation->getId();

            $listeFichiers = $this
                ->getDoctrine()
                ->getRepository(EnveloppeFichier::class)
                ->setLogger($this->depotLogger)
                ->getListeFichiersDepot($offre, $this->getParameter('type_enveloppe'));

            $lienDetailLots = ($consultation->getAlloti()) ? $this->getParameter('URL_MPE') . "?page=Entreprise.PopUpDetailLots&orgAccronyme=" . $consultation->getOrganisme() . "&id=" . $consultation->getId() : "";
            /** @var TCandidature $candidature */
            $candidature = $this->em
                ->getRepository(TCandidature::class)
                ->findOneBy(['idOffre' => $offre->getId()]);

            $paramsCandidature = ['candidature' => $candidature];

            if (
                $candidature->getTypeCandidature() === $this->getParameter('TYPE_CANDIDATUE_DUME') &&
                $candidature->getTypeCandidatureDume() === $this->getParameter('TYPE_CANDIDATUE_DUME_ONLINE')
            ) {
                $dumeContexte = $this->dumeService->getDumeContexteFromId($candidature->getIdDumeContexte());

                $dumeNumero = $this->dumeService->getDumeNumeroFromIdDumeContexte($dumeContexte->getId());

                $paramsCandidature['numeroSN'] = $dumeNumero->getNumeroDumeNational();

                $configGroupement = $this->moduleStateChecker
                    ->hasConfigPlateforme("groupement");
                $idEntreprise = $this->user->getEntrepriseId();
                $idOffre = $offre->getId();
                if ($configGroupement && $idEntreprise) {
                    $listeDumeGroupement = $this
                        ->em
                        ->getRepository(TDumeNumero::class)
                        ->getDumeNumeroFromGroupement($idOffre, $idEntreprise);
                    if ($listeDumeGroupement) {
                        $paramsCandidature['dumeGroupements'] = $listeDumeGroupement;
                    }
                }
            }
            $date = new DateTime();
            [$idValeurReferentielle, $description] =
                $this->inscritOperationTracker->getInfosValeursReferentielles('DESCRIPTION20');
            $lienDownload = "index.php?page=Agent.DownloadRecapReponse&idOffre=" . $offre->getId() . "&orgAcronyme=" . $consultation->getOrganisme();
            $this->inscritOperationTracker->tracerOperationsInscrit(
                $date,
                $date,
                $idValeurReferentielle,
                $description,
                $consultation,
                '',
                false,
                $lienDownload,
                1,
                null,
                $offre->getId()
            );
            $paramsExternalLayout = [
                'consultation' => $consultation,
                'offre' => $offre,
                'lieuxExecutions' => $lieuxExecutions,
                'user' => $inscritDepotOffre,
                'entreprise' => $entreprise,
                'lienRetour' => $liensRetour,
                'listeFichiers' => $listeFichiers,
                'lienDetailLots' => $lienDetailLots,
                'uid_response' => $uidConfirmation,
                'dume' => $this->dumeService->getInfoDume($consultation),
            ];

            $params = array_merge($paramsExternalLayout, $paramsCandidature);

            return $this->render(
                'consultation/consultation-depot-confirmation.html.twig',
                $params
            );
        } else {
            throw new Exception('Vous n\'avez pas le droit d\'accéder à cette page.');
        }
    }
}
