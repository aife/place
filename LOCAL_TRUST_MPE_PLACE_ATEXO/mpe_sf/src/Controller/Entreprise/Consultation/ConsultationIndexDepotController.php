<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise\Consultation;

use App\Service\CurrentUser;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Controller\AtexoController;
use App\Entity\Consultation;
use App\Entity\Offre;
use App\Exception\UidNotFoundException;
use App\Security\UidManagement;
use App\Service\AtexoConfiguration;
use App\Service\AtexoDumeService;
use App\Service\AtexoFichierSignature;
use App\Service\AtexoGroupement;
use App\Service\AtexoTelechargementConsultation;
use App\Service\AtexoUtil;
use App\Service\CandidatureService;
use App\Service\Consultation\ConsultationDepotService;
use App\Service\Consultation\ConsultationService;
use App\Service\Consultation\ConsultationUploadDepotService;
use App\Service\DocumentService;
use App\Service\DossierVolumineux\DossierVolumineuxService;
use App\Service\Dume\DumeService;
use App\Service\Entreprise\EntrepriseVerificationService;
use App\Service\GeolocalisationService;
use App\Service\InfoRecapService;
use App\Service\InscritOperationsTracker;
use App\Service\ObscureDataManagement;
use App\Utils\Filesystem\MountManager;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Stringable;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route(path: '/entreprise/consultation')]
class ConsultationIndexDepotController extends AtexoController
{
    protected Consultation $consultation;
    protected Offre $offre;
    protected ?Request $request = null;
    /** @var string|Stringable|UserInterface */
    protected $user;
    protected SessionInterface $session;
    protected array $typeEnveloppe = [
        1 => ['id' => 1, 'libelle' => 'DEFINE_DOSSIER_CANDIDATURE', 'acronyme' => 'CANDIDATURE'],
        2 => [
            'id'                    => 2,
            'libelle'               => 'DEFINE_DOSSIER_OFFRES',
            'acronyme'              => 'OFFRE',
            'libelleFormulaire'     => 'DEFINE_FORMULAIRE_OFFRE'
        ],
        3 => ['id' => 3, 'libelle' => 'DOSSIER_OFFRE_ANONYMAT', 'acronyme' => 'ANONYMAT', 'libelleFormulaire' => ''],
    ];

    protected array $typePiece = [
        'PRI' => ['id' => 3, 'libelle' => 'Pièce libre'],
        'ACE' => ['id' => 4, 'libelle' => 'Acte d\'engagement'],
        'AFI' => ['id' => 5, 'libelle' => 'Annexe financière'],
        'SIG' => ['id' => 99, 'libelle' => 'Signature'],
    ];

    /**
     * ConsultationIndexDepotController constructor.
     */
    public function __construct(
        RequestStack $request,
        protected EntityManagerInterface $em,
        protected LoggerInterface $depotLogger,
        protected ObscureDataManagement $obscureDataManagement,
        protected AtexoConfiguration $moduleStateChecker,
        protected AtexoFichierSignature $atexoSignatureFiles,
        protected TranslatorInterface $translator,
        protected InscritOperationsTracker $inscritOperationTracker,
        protected AtexoUtil $atexoUtil,
        protected AtexoTelechargementConsultation $atexoTelechargementConsultation,
        protected UidManagement $uidManagement,
        protected EntrepriseVerificationService $entrepriseVerificationService,
        /**
         * Nouveau Service Dume permettant la refacto de code et l'utilisation de test unitiaire
         * On migrera au fur et à mesure AtexoDumeService vers ce service.
         */
        protected DumeService $dumeService,
        /**
         * Service Dume Historique.
         */
        protected AtexoDumeService $atexoDumeService,
        protected GeolocalisationService $geolocalisationService,
        protected InfoRecapService $infoRecapService,
        protected CandidatureService $candidatureService,
        protected DocumentService $documentService,
        protected MountManager $mountManager,
        private readonly DossierVolumineuxService $dossiersVolumineuxService,
        protected ConsultationDepotService $consultationDepotService,
        protected ConsultationUploadDepotService $consultationUploadDepotService,
        private TokenStorageInterface $usageTrackingTokenStorage,
        protected ParameterBagInterface $parameterBag,
        protected CurrentUser $currentUser
    ) {
        $this->request = $request->getCurrentRequest();
        $this->session = $this->request->getSession();
        $this->user = $this->usageTrackingTokenStorage->getToken()->getUser();
    }

    /**
     * @param int $id ID de la consultation
     * @throws UidNotFoundException
     * @throws Exception
     */
    #[Route(
        path: '/depot/{id}',
        name: 'atexo_consultation_depot_index',
        requirements: ['id' => '\d+'],
        methods: ['GET']
    )]
    public function index(int $id, AtexoGroupement $atexoGroupement, ConsultationService $consultationService, ConsultationDepotService $consultationDepotService): Response
    {
        $dateDebutValidation = new DateTime();
        $user = $this->getUser();
        $phase = $this->request->query->get('phase', '');
        $choice = $this->request->query->get('choice', '');
        $uidResponse = $this->session->get('uid' . $id);

        $dlroExpire = $this->checkDLRO($id);
        if ($dlroExpire) {
            return $this->redirectToRoute(
                'atexo_consultation_dlro_depassee',
                [
                    'consultation_id' =>$id,
                ]
            );
        }

        $consultation = $consultationService->getConsultation($id);
        $consultationDepotService->checkRepondreApresCloture($consultation);
        $consultationDepotService->checkConsultationRestreinte($consultation);
        $offre = $consultationDepotService->createOffre($user, $consultation, $uidResponse);
        $msgSelectionLot = $this->translator->trans('DEFINE_SELECTION_LOTS_DEPOT_OFFRE');
        $consultationDepotService->updateTypeCandidatureAndRelationListLotCandidature(
            $consultation,
            $user,
            $phase,
            $choice
        );
        [$enveloppes, $arrayDossiers, $fromCandidatureMPS, $msgSelectionLot] = $consultationDepotService->getConsultationEnveloppesLots(
            $user,
            $uidResponse,
            $id,
            $consultation,
            $msgSelectionLot
        );
        [$candidatureDume, $candidatureGroupement] = $consultationDepotService->getConsultationCandidature(
            $phase,
            $choice,
            $consultation,
            $user,
            $offre
        );
        $lienDetailLots =
            ($consultation->getAlloti()) ? $this->getParameter('URL_MPE') .
            "?page=Entreprise.PopUpDetailLots&orgAccronyme=" .
            $consultation->getOrganisme() . "&id=" . $consultation->getId()
            : "";
        $lieuxExecutions = $this->geolocalisationService->getLieuxExecution($consultation);
        $typeEnveloppeLot = [];
        if ($consultation->getEnvOffre()) {
            $typeEnveloppeLot[] = $this->typeEnveloppe[2];
        }
        if ($consultation->getEnvAnonymat()) {
            $typeEnveloppeLot[] = $this->typeEnveloppe[3];
        }
        [$lots, $msgSelectionLot] = $consultationDepotService->getConsultationLots(
            $consultation,
            $user,
            $msgSelectionLot
        );
        $paramsCons = [
            'lots' => $lots,
            'consultation' => $consultation,
            'lienDetailLots' => $lienDetailLots,
            'lieuxExecutions' => $lieuxExecutions,
            'inscrit' => $user,
            'enveloppes' => $enveloppes,
            'typeEnveloppeLot' => $typeEnveloppeLot,
            'offre' => $offre,
            'fromCandidatureMPS' => $fromCandidatureMPS,
            'uid_response' => $uidResponse,
            'dossiersSub' => $arrayDossiers,
            'interfaceModuleSub' => $this->moduleStateChecker->hasConfigPlateforme('interfaceModuleSub'),
            'msgSelectionLot' => $msgSelectionLot,
            'dume' => $this->dumeService->getInfoDume($consultation),
            'timePing' => $this->parameterBag->get('TIME_AJAX_PING_DEPOT')
        ];
        $params = array_merge(
            $paramsCons,
            $consultationDepotService->initGroupement($uidResponse, $id, $atexoGroupement),
            $candidatureDume,
            $candidatureGroupement,
            $consultationDepotService->getDossierVolumineux($consultation)
        );
        $response = $this->render('consultation/index.html.twig', $params);
        $response->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate');
        $consultationDepotService->addTraceInscrit($dateDebutValidation, $consultation, $offre);
        return $response;
    }

    #[Route(path: '/depot/modeCandidatureChanged', options: ['expose' => true], name: 'atexo_consultation_mode_candidature_changed', methods: ['POST'])]
    public function modeCandidatureChanged(Request $request): Response
    {
        $error = 0;
        try {
            $this->candidatureService->changeTypeCandidatureStandard($request);
        } catch (Exception $e) {
            $error = 1;
            $erreur = 'Erreur : ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString();
            $this->depotLogger->error(
                "Erreur lors de l'enregistrement de la candidature : 
                function modeCandidatureChanged, " . PHP_EOL . $erreur
            );
        }
        return new Response(
            json_encode([
                'error' => $error,
                'message' => $this->translator->trans('SERVICE_ACTUELLEMENT_INDISPONIBLE'),
            ], JSON_THROW_ON_ERROR)
        );
    }

    #[Route(path: '/depot/modeCandidatureDumeChanged', options: ['expose' => true], name: 'atexo_consultation_mode_candidature_dume_changed', methods: ['POST'])]
    public function modeCandidatureDumeChanged(Request $request): Response
    {
        $error = 0;
        try {
            $this->candidatureService->changeTypeCandidatureDume($request);
        } catch (Exception $e) {
            $error = 1;
            $erreur = 'Erreur : ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString();
            $this->depotLogger->error(
                "Erreur lors de l'enregistrement de la candidature : 
                function modeCandidatureDumeChanged, " . PHP_EOL . $erreur
            );
        }
        return new Response(
            json_encode([
                'error' => $error,
                'message' => $this->translator->trans('SERVICE_ACTUELLEMENT_INDISPONIBLE'),
            ], JSON_THROW_ON_ERROR)
        );
    }
    #[Route(path: '/depot/roleInscritChanged', options: ['expose' => true], name: 'atexo_consultation_role_inscrit_changed', methods: ['POST'])]
    public function roleInscritChanged(Request $request): Response
    {
        $error = 0;
        $message = '';
        try {
            if (!is_object($this->user)) {
                $message = $this->translator->trans('TEXT_PERMISSION_NON_ACCORDEE');
                throw new AuthenticationCredentialsNotFoundException(
                    $this->translator->trans('TEXT_PERMISSION_NON_ACCORDEE')
                );
            }
            $this->candidatureService->changeRoleInscrit($request);
            $message = 'OK';
        } catch (Exception $e) {
            $error = 1;
            $erreur = 'Erreur : ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString();
            $message = $message ?: $this->translator->trans('SERVICE_ACTUELLEMENT_INDISPONIBLE');
            $this->depotLogger->error(
                "Erreur lors de l'enregistrement de la candidature : function roleInscritChanged, " . PHP_EOL . $erreur
            );
        }
        return new Response(
            json_encode([
                'error' => $error,
                'message' => $message,
            ], JSON_THROW_ON_ERROR)
        );
    }
    /**
     * @return JsonResponse
     */
    #[Route(path: '/ping', options: ['expose' => true], name: 'atexo_consultation_depot_ping', methods: ['GET'])]
    public function pingDepotAction(): Response
    {
        $json = ['status' => 'KO'];
        if (null !== $this->getUser() && $this->currentUser->isInscrit()) {
            $json['status'] = 'OK';
        }
        return new JsonResponse($json);
    }


    public function checkDLRO($consultationId): bool {
        $consultation = $this->em->getRepository(Consultation::class)
            ->findOneBy(
                [
                    'id' => $consultationId
                ]
            );


        $dlroExprie = false;
        if ($consultation->getDatefin() < new DateTime('now')
            && (string) $consultation->getIdTypeAvis() === $this->parameterBag->get('TYPE_AVIS_CONSULTATION')) {
            $dlroExprie = true;
        }
        return $dlroExprie;
    }
}
