<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise\Consultation;

use Exception;
use DateTime;
use App\Entity\Consultation;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

#[Route(path: '/entreprise/consultation')]
class ConsultationDLRODepasseeController extends ConsultationIndexDepotController
{
    /**
     * Permet d'afficher une message lors de l'accés a une consultation qui a une fin de DLRO.
     *
     * @ParamConverter("consultation", options={"mapping": {"consultation_id": "id"}})
     * @param Consultation $consultation : objet consultation
     *
     * @return Response
     * @throws Exception
     */
    #[Route(path: '/depot/{consultation_id}/dlro-depassee', requirements: ['consultation_id' => '\d+'], name: 'atexo_consultation_dlro_depassee')]
    public function dlroDepasseAction(Consultation $consultation)
    {

        if (
            null !== $this->getUser() &&
            $this->currentUser->isInscrit()
        ) {
            $liensRetour = $this->parameterBag->get('URL_LT_MPE_SF') . DIRECTORY_SEPARATOR . 'consultation' . DIRECTORY_SEPARATOR . $consultation->getId();

            $detail = "L'entreprise suivante "
                . $this->getUser()->getEntreprise()->getNom()
                . " a tenté d'accéder au dépôt après la DLRO.";
            [$idValeurReferentielle, $description] = $this->inscritOperationTracker->getInfosValeursReferentielles('DESCRIPTION40');
            $description = $detail;
            $this->inscritOperationTracker
                ->tracerOperationsInscrit(
                    new DateTime(),
                    new DateTime(),
                    $idValeurReferentielle,
                    $description,
                    $consultation,
                    $detail,
                    false,
                    null,
                    1,
                    new DateTime('now'),
                    null
                );

            return $this->render('consultation/consultation-dlro-depassee-depot.html.twig', [
                'consultation' => $consultation,
                'lienRetour' => $liensRetour,
            ]);
        } else {
            throw new Exception('Vous n\'avez pas le droit d\'accéder à cette page.');
        }
    }
}
