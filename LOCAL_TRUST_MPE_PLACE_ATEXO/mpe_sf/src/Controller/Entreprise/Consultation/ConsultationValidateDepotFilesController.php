<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise\Consultation;

use Exception;
use App\Service\Consultation\ConsultationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/entreprise/consultation')]
class ConsultationValidateDepotFilesController extends ConsultationIndexDepotController
{
    /**
     * @Method({"POST"})
     *
     * @param int                 $id                  ID de la consultation
     *                                                 Details retours infos signatures:
     *                                                 Valide => 0
     *                                                 Expire => 2
     *                                                 PasEncoreValide => 5
     *                                                 Inconnu => 1
     *
     * @return Response
     */
    #[Route(path: '/depot/{id}/enregistrer/valider/fichiers', requirements: ['id' => '\d+'], name: 'atexo_consultation_depot_enregistrer_valider_fichiers', options: ['expose' => true])]
    public function validerFichiersAction(int $id, Request $request, ConsultationService $consultationService)
    {
        $errors = [];
        $reponse = $request->get('reponse');
        $uidResponse = $reponse['uid_response'];
        $infoFiles = [];
        try {
            $infoFiles = $consultationService->validerFichiersTraitement($id, $request, true);
        } catch (Exception $e) {
            $erreur = "[uid_response = $uidResponse] Depot - Erreur validation des fichiers : ".$e->getMessage();
            $this->depotLogger->error($erreur);
            $errors[] = $erreur;
        }
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        if (!empty($errors)) {
            $response->setContent(json_encode(['errors' => $errors], JSON_THROW_ON_ERROR));
        } else {
            $response->setContent(json_encode(['infoFiles' => $infoFiles], JSON_THROW_ON_ERROR));
        }
        return $response;
    }
}
