<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise\Consultation;

use App\Entity\Avis;
use App\Entity\AvisPub;
use App\Entity\Consultation;
use App\Service\Consultation\ConsultationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/entreprise/consultation')]
class ConsultationAvisController extends ConsultationIndexDepotController
{
    /**
     * Permet de telecharger un avis de publicite de la consultation
     *
     * @Method({"GET"})
     *
     * @param int                 $id
     * @param int                 $idAvis
     * @return Response
     */
    #[Route(path: '/{id}/avis/{idAvis}/telechargement/{idTypeAvis}', name: 'atexo_consultation_avis_telechargement')]
    public function downloadAvisAction($id, $idAvis, $idTypeAvis, ConsultationService $consultationService)
    {

        $this->consultation = $consultationService->getConsultation($id, $idTypeAvis);
        if ($this->consultation instanceof Consultation) {
            $avis = $this
                ->em
                ->getRepository(Avis::class)
                ->findOneBy([
                    'consultationId' => $this->consultation->getId(),
                    'organisme' => $this->consultation->getOrganisme(),
                    'id' => $idAvis,
                ]);

            if ($avis instanceof Avis) {
                //Nom du fichier joint
                $nomFichier = "";
                if ($this->moduleStateChecker->hasConfigPlateforme('PubliciteOpoce')) {
                    $critere = [
                        'organisme' => $this->consultation->getOrganisme()->getAcronyme(),
                        'consultationId' => $this->consultation->getId(),
                    ];
                    //Début récuperation de l'identifiant de l'avis global
                    if ($avis->getType() === $this->getParameter('TYPE_FORMAT_PDF_TED')) {
                        $critere['idAvisPdfOpoce'] = $avis->getId();
                        $nomFichier = 'Avis_TED_' . "Annonce_Presse_" . $this->consultation->getId() . "_";
                    } elseif ($avis->getType() === $this->getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT')) {
                        $critere['idAvisPortail'] = $avis->getId();
                        $nomFichier = 'Avis_presse_' . "Annonce_Presse_" . $this->consultation->getId() . "_" ;
                    }
                    $avisPub = $this
                        ->em
                        ->getRepository(AvisPub::class)
                        ->findOneBy($critere);

                    $idAvisPub = '';
                    if ($avisPub instanceof AvisPub) {
                        $idAvisPub = $avisPub->getId();
                    }
                    $nomFichier .= $idAvisPub . ".pdf";
                } else {
                    $nomFichier = $avis->getNomFichier();
                }

                $fileContent = $this->mountManager->getFileContent($avis->getAvis(), $avis->getOrganisme(), $this->em);
                if (false !== $fileContent) {
                    $response = new Response($fileContent);
                    $filenameFallback = preg_replace('#^.*\.#', md5($nomFichier) . '.', $nomFichier);
                    $disposition = $response->headers->makeDisposition(
                        ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                        $nomFichier,
                        $filenameFallback
                    );
                    $response->headers->set('Content-Disposition', $disposition);

                    return $response;
                }
            }
        }
        return new Response();
    }
}
