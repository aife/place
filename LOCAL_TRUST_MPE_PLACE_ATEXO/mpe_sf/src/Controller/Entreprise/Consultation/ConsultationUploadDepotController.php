<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise\Consultation;

use Doctrine\DBAL\ConnectionException;
use Exception;
use DateTime;
use App\Entity\BloborganismeFile;
use App\Entity\Consultation;
use App\Entity\EnveloppeFichier;
use App\Entity\Offre;
use App\Exception\ConsultationNotFoundException;
use App\Service\AtexoFichierOrganisme;
use App\Service\Consultation\ConsultationService;
use App\Service\ObscureDataManagement;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/entreprise/consultation')]
class ConsultationUploadDepotController extends ConsultationIndexDepotController
{
    /**
     * @Method({"POST"})
     *
     * @param $id                    :ID de la consultation
     *
     * @throws ConnectionException
     */
    #[Route(path: '/depotUpload/{id}/upload', requirements: ['id' => '\d+'], name: 'atexo_consultation_depot_upload')]
    public function uploadAction($id, Request $request, LoggerInterface $depotLogger, EntityManagerInterface $em, AtexoFichierOrganisme $atexoBlobOrganisme, ObscureDataManagement $obscureDataManagement, ConsultationService $consultationService) : JsonResponse|Response
    {
        $typeEnveloppe = null;
        $typeFichier = null;
        $offreId = null;
        $enveloppeId = null;
        $fichierId = null;
        $filesCollections = null;
        $enveloppeFichier = null;
        $consultation = null;
        try {
            $uidResponse = $request->get('uid_response');
            $files = $request->files->all();
            /** @var UploadedFile $file */
            $file = array_shift($files);
            if (empty($file)) {
                $depotLogger->error(" Ajout d'un fichier impossible fichier non reçu");

                return new Response("Ajout d'un fichier impossible fichier non reçu", Response::HTTP_INTERNAL_SERVER_ERROR);
            }
            if (!empty($file->getError())) {
                $depotLogger->error(" Ajout d'un fichier impossible code erreur " . $file->getError() . ': uidResponse = ' . $uidResponse . ' nom fichier =' . $file->getClientOriginalName() . ' chemin fichier ' . $file->getPathname());

                return new Response("Ajout d'un fichier impossible code erreur", Response::HTTP_INTERNAL_SERVER_ERROR);
            }
            if (null === $this->getUser()) {
                $ip = var_export($request->getClientIps(), true);
                $serverId = $request->cookies->get('SERVERID');
                $phpSessionId = $request->cookies->get('PHPSESSID');

                $depotLogger->error(" Ajout d'un fichier impossible session perdue ip client : " . $ip . ', uidResponse = ' . $uidResponse . ', nom fichier =' . $file->getClientOriginalName() . ' chemin fichier ' . $file->getPathname() . ' serveur id :' . $serverId . ' PHPSESSID :' . $phpSessionId);

                return new Response("Ajout d'un fichier impossible session perdue", Response::HTTP_INTERNAL_SERVER_ERROR);
            }
            $consultation = $consultationService->getConsultation($id, ConsultationService::CONSULTATION_TYPE_CLOTURES, true);
            $preperedData = $this->prepareFiles($consultation, $uidResponse, $file, $request, $atexoBlobOrganisme, $obscureDataManagement, $consultationService, $depotLogger, $em);
            if ($preperedData instanceof Response) {
                return $preperedData;
            }
            [$enveloppe, $typeEnveloppe, $offreId, $enveloppeId, $acronymeEnveloppe] = $preperedData;
            [$filesCollections, $typeFichier, $fichierId] = $this->saveDepos(
                $consultation,
                $uidResponse,
                $request,
                $file,
                $enveloppe,
                $typeEnveloppe,
                $acronymeEnveloppe,
                $atexoBlobOrganisme,
                $obscureDataManagement,
                $em,
                $depotLogger
            );

            return $this->json($filesCollections);
        } catch (Exception $e) {
            $error = "[uid_response = $uidResponse] Erreur lors de l'upload du fichier : [refCons = $id], [typeEnveloppe = $typeEnveloppe], [typeFichier = $typeFichier], [offreId = $offreId], [enveloppeId = $enveloppeId], [fichierId = $fichierId], [filesCollections = " . var_export($filesCollections, true) . ']';
            $tabErreurWarning = [
                'Warning: sha1_file(): Filename cannot be empty',
                'chemin du fichier incorrecte',
            ];
            $stack = $error . PHP_EOL . 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            if (in_array($e->getMessage(), $tabErreurWarning)) {
                $depotLogger->warning($stack);
            } else {
                $depotLogger->error($stack);
            }

            if (
                isset($enveloppeFichier) &&
                $enveloppeFichier instanceof EnveloppeFichier &&
                !empty($enveloppeFichier->getIdFichier())
            ) {
                $depotLogger->info('supprimé fichier du a une execpetion au niveau de uploadAction : id fichier ==> ' . $enveloppeFichier->getIdFichier());
                $consultationService->removeEnveloppeFichier($enveloppeFichier->getIdFichier(), $consultation);
            }

            if ($em->getConnection()->isTransactionActive()) {
                $em->getConnection()->rollBack();
            }
            return new Response($error, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Permet de verifier la signature.
     *
     * @param $enveloppeFichierSource
     * @param $contentSignature
     * @param $idBlobSignature
     * @param $idFichierSignature
     * @param $uidResponse
     * @param string $pdfPath
     *
     * @return array
     */
    protected function verifierSignature(
        $enveloppeFichierSource,
        $contentSignature,
        $idBlobSignature,
        $idFichierSignature,
        $uidResponse,
        $pdfPath
    ) {
        $infosSignatures = [];
        try {
            if ($enveloppeFichierSource instanceof EnveloppeFichier) {
                /*
                 * @todo Attention utiliser dans Chorus pour le stockage en zip de la signature
                 * fichier MPE : PopupChorusSelectedFiles::
                 */
                $enveloppeFichierSource->setIdBlobSignature($idBlobSignature);
                $enveloppeFichierSource->setIdFichierSignature($idFichierSignature);

                try {
                    $tryVerification = $this->getParameter('try_verification_signature');
                } catch (InvalidArgumentException $e) {
                    $tryVerification = 3;
                }
                $verificationSuccess = false;

                do {
                    $this->depotLogger->info(
                        'la vérification de la signature pour le fichier =>  ' . $enveloppeFichierSource->getIdFichier() . ' tryVerification =>' . $tryVerification
                    );

                    $infosSignatureJson = $this->atexoSignatureFiles->verifierSignature($enveloppeFichierSource, $pdfPath);
                    $infosSignatures = json_decode($infosSignatureJson, true, 512, JSON_THROW_ON_ERROR);

                    if (is_array($infosSignatures) && !empty($infosSignatures)) {
                        $verificationSuccess = true;
                    }

                    --$tryVerification;
                } while ($tryVerification && !$verificationSuccess);

                if (
                    is_array($infosSignatures) &&
                    !empty($infosSignatures) &&
                    array_key_exists('signatureValide', $infosSignatures) &&
                    null !== $infosSignatures['signatureValide'] &&
                    array_key_exists('emetteur', $infosSignatures) &&
                    null !== $infosSignatures['emetteur'] &&
                    array_key_exists('periodiciteValide', $infosSignatures) &&
                    null !== $infosSignatures['periodiciteValide']
                ) {
                    if ($this->moduleStateChecker->hasConfigPlateforme('surchargeReferentiels')) {
                        $repertoiresChaineCertification = $infosSignatures['repertoiresChaineCertification'];
                        if (array_key_exists('statut', $repertoiresChaineCertification[0])) {
                            $statutReferentielCertificat = match ($repertoiresChaineCertification[0]['statut']) {
                                $this->getParameter('statut_referentiel_certificat_ok') => 0,
                                $this->getParameter('statut_referentiel_certificat_ko') => 2,
                                $this->getParameter('statut_referentiel_certificat_unknown') => 1,
                                default => 1,
                            };

                            $enveloppeFichierSource->setStatutReferentielCertificat($statutReferentielCertificat);
                        } else {
                            $enveloppeFichierSource->setStatutReferentielCertificat(1);
                        }

                        if (array_key_exists('nom', $repertoiresChaineCertification[0])) {
                            $enveloppeFichierSource->setNomReferentielFonctionnel($repertoiresChaineCertification[0]['nom']);
                        } else {
                            $enveloppeFichierSource->setNomReferentielFonctionnel($this->translator->trans('DEFINE_CERTIFICAT_INCONNU'));
                        }
                    }

                    if (
                        !empty($infosSignatures) &&
                        array_key_exists('chaineDeCertificationValide', $infosSignatures) &&
                        array_key_exists('absenceRevocationCRL', $infosSignatures) &&
                        array_key_exists('periodiciteValide', $infosSignatures)
                    ) {
                        $resultValidation = $infosSignatures['chaineDeCertificationValide'] . '-' . $infosSignatures['absenceRevocationCRL'] . '-' . ((true == $infosSignatures['periodiciteValide']) ? '0' : '2');
                    } else {
                        $resultValidation = '3-3-3';
                    }

                    $enveloppeFichierSource->setVerificationCertificat($resultValidation);
                    $infosSignatures['name'] = $enveloppeFichierSource->getNomFichier();
                    $infosSignatures['isSignaturePADES'] = ('pades' == strtolower($enveloppeFichierSource->getTypeSignatureFichier()));
                    //Mise a jour des infos signature de l'offre
                    $enveloppeFichierSource->setSignatureInfos(json_encode($infosSignatures, JSON_THROW_ON_ERROR));
                    $enveloppeFichierSource->setSignatureInfosDate(new DateTime());

                    if (array_key_exists('typeSignature', $infosSignatures)) {
                        $enveloppeFichierSource->setTypeSignatureFichier($infosSignatures['typeSignature']);
                    }
                } else {
                    if ($enveloppeFichierSource->getIdFichier() != $idFichierSignature) {
                        $enveloppeFichierSource->setVerificationCertificat('3-3-3');
                        $enveloppeFichierSource->setTypeSignatureFichier('INCONNUE');
                    } else {
                        $enveloppeFichierSource->setIdBlobSignature(null);
                        $enveloppeFichierSource->setIdFichierSignature(null);
                        $enveloppeFichierSource->setTypeSignatureFichier('');
                    }
                }

                $this->em->persist($enveloppeFichierSource);
                $this->em->flush();
            }
        } catch (Exception $e) {
            $error = "[uid_response = $uidResponse] Message: " . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString();
            $this->depotLogger->error('Erreur : [fichierId=' . $enveloppeFichierSource->getIdFichier() . "], [contentSignature=$contentSignature], [idBlobSignature=$idBlobSignature], [$idFichierSignature=$idFichierSignature] " . PHP_EOL . $error);
        }

        return $infosSignatures;
    }
    private function prepareFiles($consultation, $uidResponse, $file, $request, $atexoBlobOrganisme, $obscureDataManagement, ConsultationService $consultationService, $logger, $em)
    {
        try {
            $tmpFilePath = $file->getPathname();
            $atexoBlobOrganisme->moveTmpFile($tmpFilePath, $request);
        } catch (Exception $exception) {
            $logger->error(sprintf('Problème lors de deplacement de fichier à uploader dans le dossier tmp:  %s ', $exception->getMessage()));

            return new Response('Problème lors de deplacement de fichier à uploader dans le dossier tmp', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $em->getConnection()->beginTransaction();

        $typeEnveloppe = $obscureDataManagement
            ->getDataFromObscuredValue(
                $request->get('type_enveloppe'),
                $uidResponse
            );

        $offreId = $obscureDataManagement->getDataFromObscuredValue($request->get('offre_id'), $uidResponse);
        $enveloppeIdObscurci = $request->get('enveloppe');
        $enveloppeIdObscurciExplode = explode('_', (string) $enveloppeIdObscurci);

        $acronymeEnveloppe = $obscureDataManagement
            ->getDataFromObscuredValue(
                array_pop($enveloppeIdObscurciExplode),
                $uidResponse
            );

        //Debut reconstitution enveloppe_id en clair
        $enveloppeIdObscurciExplodeTiret = explode('-', $enveloppeIdObscurciExplode[1]);

        $numeroLot = $obscureDataManagement
            ->getDataFromObscuredValue(
                array_pop($enveloppeIdObscurciExplodeTiret),
                $uidResponse
            );

        $enveloppeId = 'env_lot-' . $numeroLot . '_' . $acronymeEnveloppe;
        //Fin reconstitution enveloppe_id en clair
        $lot = preg_match('/.*-([0-9]+)_.*/', $enveloppeId, $matches) ? array_pop($matches) : 0;

        if (empty($offreId)) {
            throw new Exception('id Offre est perdu');
        }
        /** @var Offre offre */
        $this->offre = $consultationService->getOffre($offreId, $uidResponse);
        if ($this->offre->getStatutOffres() != $this->getParameter('STATUT_ENV_BROUILLON')) {
            $msg = " Ajout d'un fichier impossible : offre:  idOffre = " . $this->offre->getId() . ' a un statut invalide :' . $this->offre->getStatutOffres();
            $logger->error($msg);

            return new Response($msg, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $enveloppe = $this->consultationUploadDepotService->getEnveloppe(
            $uidResponse,
            $offreId,
            $typeEnveloppe,
            $lot,
            $consultation->getAcronymeOrg()
        );

        if (!$enveloppe) {
            $enveloppe = $consultationService->createEnveloppe(
                $uidResponse,
                $typeEnveloppe,
                $lot,
                $consultation->getAcronymeOrg(),
                false,
                true,
                $this->offre
            );
        }

        return [$enveloppe, $typeEnveloppe, $offreId, $enveloppeId, $acronymeEnveloppe];
    }
    private function saveDepos(Consultation $consultation, $uidResponse, $request, $file, $enveloppe, $typeEnveloppe, $acronymeEnveloppe, $atexoBlobOrganisme, $obscureDataManagement, $em, $depotLogger)
    {
        $typeFichier = $request->get('type_fichier') ? $obscureDataManagement->getDataFromObscuredValue($request->get('type_fichier'), $uidResponse) : 'PRI';
        $fichierId = $request->get('fichier') ? $obscureDataManagement->getDataFromObscuredValue($request->get('fichier'), $uidResponse) : null;
        $filesCollections = empty($request->get('files_collections')) ? [] : (array) json_decode($request->get('files_collections'), null, 512, JSON_THROW_ON_ERROR);

        $enveloppeFichier = new EnveloppeFichier();
        $enveloppeFichier->setUidResponse($uidResponse);
        $enveloppeFichier->setEnveloppe($enveloppe);
        $enveloppeFichier->setOrganisme($consultation->getAcronymeOrg());
        $enveloppeFichier->setNumOrdreFichier(count($filesCollections) + 1);
        $enveloppeFichier->setTypeFichier($typeFichier);
        $enveloppeFichier->setTypePiece($this->typePiece[$typeFichier]['id']);
        $enveloppeFichier->setTailleFichier($file->getSize());
        $nomFichier = utf8_decode($file->getClientOriginalName());
        $nomFichier = $this->atexoUtil->replaceCharactersMsWordWithRegularCharacters($nomFichier);
        $nomFichier = utf8_encode($nomFichier);
        $enveloppeFichier->setNomFichier($nomFichier);
        $enveloppeFichier->setHash(sha1_file($file->getPathname()));
        $enveloppeFichier->setHash256(hash_file('sha256', $file->getPathname()));
        $enveloppeFichier->setIdTypePiece(0);
        $enveloppeFichier->setIsHash(0);
        $enveloppeFichier->setVerificationCertificat('');

        $blob = $atexoBlobOrganisme->insertFile(
            $file->getClientOriginalName(),
            $file->getPathname(),
            $consultation->getAcronymeOrg(),
            null,
            $this->getParameter('EXTENSION_BROUILLON')
        );
        $blob = $em->getRepository(BloborganismeFile::class)
            ->findOneById($blob);
        $enveloppeFichier->setBlob($blob);
        $em->persist($enveloppeFichier);

        $enveloppe->addFichierEnveloppe($enveloppeFichier);

        $em->persist($enveloppe);
        $em->flush();

        $infosSignature = [];

        $extentionFile = strtolower($this->atexoUtil->getExtension($file->getClientOriginalName()));
        $pdfPath = '';
        $contentSignature = '';
        if ('SIG' == $typeFichier || ('pdf' == $extentionFile && 'SIG' != $typeFichier)) {
            $idBlobSignature = $blob->getId();

            if ('pdf' == $extentionFile && 'SIG' != $typeFichier) {
                $fichierSource = $enveloppeFichier;
                $pdfPath = $file->getPathname();
                $fichierSource->setTypeSignatureFichier('PADES');
            } else {
                $fichierSource = $em
                    ->getRepository(EnveloppeFichier::class)
                    ->findOneByIdFichier($fichierId);

                $contentSignature = base64_encode(file_get_contents($file->getPathname()));
            }

            if (
                '1' === $consultation->getSignatureOffre()
                || '2' === $consultation->getSignatureOffre()
            ) {
                try {
                    $infosSignature = $this->verifierSignature(
                        $fichierSource,
                        $contentSignature,
                        $idBlobSignature,
                        $enveloppeFichier->getIdFichier(),
                        $uidResponse,
                        $pdfPath
                    );
                } catch (\Exception $ex) {
                    $depotLogger->error(
                        ' Ajout d\'un fichier impossible fichier. Message : '
                        . $ex->getMessage()
                        . '. Trace : '
                        . $ex->getTraceAsString()
                    );
                }
            }
        }

        $data = [
            'name' => $file->getClientOriginalName(),
            'size' => $file->getSize(),
            'id_blob' => $obscureDataManagement->getObscureData($blob->getid(), $uidResponse),
            'position' => $enveloppeFichier->getNumOrdreFichier(),
            'offre' => $obscureDataManagement
                ->getObscureData($this->offre->getId(), $uidResponse),
            'organisme' => $obscureDataManagement
                ->getObscureData($consultation->getOrganisme()->getAcronyme(), $uidResponse),
            'numlot' => $obscureDataManagement
                ->getObscureData($enveloppe->getSousPli(), $uidResponse),
            'lot' => $enveloppe->getSousPli(),
            'enveloppe' => $obscureDataManagement
                ->getObscureData($enveloppe->getIdEnveloppeElectro(), $uidResponse),
            'type_enveloppe' => $obscureDataManagement
                ->getObscureData($typeEnveloppe, $uidResponse),
            'acronyme_enveloppe' => $obscureDataManagement
                ->getObscureData($acronymeEnveloppe, $uidResponse),
            'fichier' => $obscureDataManagement
                ->getObscureData($enveloppeFichier->getIdFichier(), $uidResponse),
            'infos_certificat' => $infosSignature,
            'deleteType' => 'DELETE',
            'deleteUrl' => $this->generateUrl('atexo_consultation_depot_retirer_fichier', [
                'id' => $obscureDataManagement->getObscureData($consultation->getId(), $uidResponse),
                'idFichier' => $obscureDataManagement
                    ->getObscureData($enveloppeFichier->getIdFichier(), $uidResponse),
                'uidResponse' => $uidResponse,
            ]),
            'uid_response' => $uidResponse,
            'errorDoublon' => $this->isDoublonExist($request, $file),
        ];

        if (!empty($fichierId) && 'SIG' == $typeFichier) {
            $data['fichier_source'] = $fichierId;
        } elseif ('SIG' != $typeFichier && 'pdf' == $extentionFile) {
            $data['fichier_source'] = $enveloppeFichier->getIdFichier();
        }

        $filesCollections['files'][] = $data;

        $em->flush();
        $em->getConnection()->commit();

        return [$filesCollections, $typeFichier, $fichierId];
    }
    private function isDoublonExist($request, $file)
    {
        // code pour gérer les doublons
        $listeFichierEnveloppe = $request->get('names_all_files');

        $errorDoublon = false;
        $nameFile = utf8_encode($this->atexoUtil->replaceCharactersMsWordWithRegularCharacters(utf8_decode($file->getClientOriginalName())));

        // code pour gérer les doublons
        if (is_array($listeFichierEnveloppe)) {
            foreach ($listeFichierEnveloppe as $value) {
                if ($nameFile == $value) {
                    $errorDoublon = true;
                }
            }
        }
        return $errorDoublon;
    }
}
