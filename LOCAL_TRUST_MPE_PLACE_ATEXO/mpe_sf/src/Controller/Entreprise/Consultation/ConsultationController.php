<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise\Consultation;

use App\Service\Publicite\EchangeConcentrateur;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use App\Service\AtexoUtil;
use App\Controller\AtexoController;
use App\Entity\Agent;
use App\Entity\AnnonceBoamp;
use App\Entity\Avis;
use App\Entity\ConfigurationPlateforme;
use App\Entity\Consultation;
use App\Entity\Echange;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\FormXmlDestinataireOpoce;
use App\Entity\Inscrit;
use App\Entity\Lot;
use App\Entity\Offre;
use App\Entity\QuestionDCE;
use App\Entity\TBourseCotraitance;
use App\Entity\TCandidature;
use App\Entity\Telechargement;
use App\Entity\TGroupementEntreprise;
use App\Entity\TListeLotsCandidature;
use App\Entity\TMembreGroupementEntreprise;
use App\Entity\TSupportAnnonceConsultation;
use App\Entity\TSupportPublication;
use App\Exception\ConsultationNotAuthorizedException;
use App\Exception\ConsultationNotFoundException;
use App\Security\UidManagement;
use App\Service\AtexoConfiguration;
use App\Service\AtexoDumeService;
use App\Service\Authentication;
use App\Service\configurationPlatformService;
use App\Service\Consultation\ConsultationService;
use App\Service\CurrentUser;
use App\Service\DocumentService;
use App\Service\Dume\DumeService;
use App\Service\Entreprise\EntrepriseVerificationService;
use App\Service\GeolocalisationService;
use App\Service\InscritOperationsTracker;
use App\Service\Messagerie\MessagerieService;
use App\Service\Messagerie\WebServicesMessagerie;
use App\Utils\Encryption;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use GuzzleHttp\Exception\ClientException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Error\Error;

#[Route(path: '/entreprise/consultation')]
class ConsultationController extends AtexoController
{
    public const ANCHOR_MESSAGERIE = 'messagerie';
    public const ANCHOR_QUESTION = 'question';

    protected ?Consultation $consultation = null;

    protected $user = null;

    protected $offre;

    protected bool $cloturer = false;

    /**
     * ConsultationIndexController constructor.
     */
    public function __construct(
        protected SessionInterface $session,
        protected configurationPlatformService $configurationPlatformService,
        protected AtexoDumeService $atexoDumeService,
        protected DumeService $dumeService,
        protected LoggerInterface $logger,
        protected TranslatorInterface $translator,
        protected EntityManagerInterface $em,
        protected DocumentService $documentService,
        protected InscritOperationsTracker $inscritOperationTracker,
        protected AtexoConfiguration $moduleStateChecker,
        protected WebServicesMessagerie $webServicesMessagerie,
        private Authentication $authentication,
        private PaginatorInterface $paginator,
        private Encryption $encryption,
        private AtexoUtil $atexoUtil,
        private MessagerieService $messagerieService,
        private ParameterBagInterface $parameterBag,
        private readonly EchangeConcentrateur $echangeConcentrateur
    ) {
    }

    /**
     * @throws Exception
     */
    #[Route(path: '/{id}', name: 'atexo_consultation_index', requirements: ['id' => '\d+'], methods: ['GET'])]
    public function consultationIndexAction(
        Request $request,
        int $id,
        ConsultationService $consultationService,
        EntrepriseVerificationService $entrepriseVerificationService,
        GeolocalisationService $geolocalisationService,
        UidManagement $uidManagement,
        CurrentUser $user,
        InscritOperationsTracker $inscritOperationsTracker,
        SessionInterface $session
    ): Response {
        $dateDebutValidation = new DateTime();
        $this->user = $user->getCurrentUser();
        if ($this->user instanceof Agent) {
            $this->authentication->disconnectUser();

            return new RedirectResponse($this->getParameter('URL_MPE') . '?page=Entreprise.EntrepriseHome');
        }
        try {
            $this->consultation = $consultationService->getConsultation(
                $id,
                $consultationService->clotureConsultation($request)
            );
        } catch (ConsultationNotFoundException) {
            $this->addFlash('error', $this->translator->trans('ACCES_NON_AUTORISE_A_LA_CONSULTATION'));

            return $this->render('consultation/consultation.html.twig', ['displayErrorMessage' => true]);
        }
        $this->setConsultationInfoInSession($request, $id);
        $this->checkAcessProcedureRestreinte($request);
        $params = $this->getParamConsultationForTemplate(
            $id,
            $request,
            $geolocalisationService,
            $this->user,
            $uidManagement,
            $session
        );
        // encart verification entreprise
        $paramsVerif = $entrepriseVerificationService->getParamsDepot();
        $params = array_merge($params, $paramsVerif);
        $this->setTraceInscrit($inscritOperationsTracker, $dateDebutValidation);
        return $this->render('consultation/consultation.html.twig', $params);
    }

    private function setConsultationInfoInSession(
        Request $request,
        int $id
    ): ?Consultation {
        $codeAcces = $request->query->get(
            'code',
            $this->get('session')->get('contexte_authentification_' . $id)['codeAcces']
        );
        $orgAcronyme = $request->query->get('orgAcronyme', '');

        //Gestion accès consultation restreinte au panier (après déco/reco)
        $contexteAuthentification = $this->session->get('contexte_authentification');
        if (!empty($codeAcces)) {
            $contexteAuthentification['codeAcces'] = $codeAcces;
        } elseif ($this->session->get('codeAcces')) {
            $contexteAuthentification['codeAcces'] = $this->session->get('codeAcces');
        }

        $token = $this->session->get('token_contexte_authentification');
        $contexteAuthentification['organisme_consultation'] = $orgAcronyme;
        $contexteAuthentification['reference_consultation'] = $id;
        $contexteAuthentification['reference_utilisateur_consultation'] =
            $this->consultation->getReferenceUtilisateur();
        $this->session->set('token_contexte_authentification_' . $id, $token);

        $this->session->set('contexte_authentification_' . $id, $contexteAuthentification);

        return $this->consultation;
    }

    private function checkAcessProcedureRestreinte(Request $request): void
    {
        $criteriaCons = null;
        if (!empty($this->session->get('criteriaCons'))) {
            $criteriaCons = $this->session->get('criteriaCons');
        } elseif (!empty($this->session->get('contexte_authentification')['criteriaCons'])) {
            $criteriaCons = $this->session->get('contexte_authentification')['criteriaCons'];
        }

        if (
            $this->consultation->getCodeProcedure() !== $request->query->get('code') &&
               $this->consultation->getCodeProcedure() !== $this->session->get('contexte_authentification_'
                   . $this->consultation->getId())['codeAcces'] &&
            ($this->getParameter('type_acces_procedure')['restreinte'] === $this->consultation->getTypeAcces() &&
                (empty($criteriaCons) ||
                    $this->consultation->getCodeProcedure()
                    !== $criteriaCons->getCodeAccesProcedureRestreinte()))
        ) {
            throw new ConsultationNotAuthorizedException(
                401,
                $this->translator->trans('ACCES_NON_AUTORISE_A_LA_CONSULTATION')
            );
        }
    }

    /**
     * @throws Error
     */
    private function getParamConsultationForTemplate(
        int $id,
        Request $request,
        GeolocalisationService $geolocalisationService,
        ?UserInterface $user,
        UidManagement $uidManagement,
        SessionInterface $session
    ): array {
        $consultationInfo = [];
        $isEnabled = $this->em->getRepository(ConfigurationPlateforme::class)
            ->getConfigurationPlateforme();
        $consultationInfo['alloti'] = $this->consultation->getAlloti();
        $consultationInfo['phase'] = $this->consultation->getEnvCandidature();
        $consultationInfo['mps'] = $this->consultation->getMarchePublicSimplifie();
        $consultationInfo['dume'] = $this->consultation->getDumeDemande();

        $contexte = null;
        [$paramsConnected, $contexte, $paramsContexte] = $this->getDumeContext();
        [$paramsConnected, $tvaIntracommunautaire] =
            $this->getConsultationLotInfo($request, $paramsConnected['groupement'], $paramsConnected);
        $listePub = $this->getLienPubInfo();
        [$listePubLibre, $typeFormatAvis] = $this->getLienPubLibreInfo();
        $paramsGroupement = $this->getParamGroupement();
        $tabDossierVolumineux = $this->getDossierVolumineux();
        $orgs = $session->get('organismes_eligibles') ?? [];
        $this->cloturer = $this->checkConsultationCloturer($this->consultation->getId(), $orgs);
        [$actionPossible, $liensConsult] = $this->getActionPossible($tabDossierVolumineux);
        $paramMessecV2 = $this->getMessecVersion();
        $lieuxExecutions = $geolocalisationService->getLieuxExecution($this->consultation);
        $lienDetailLots = ($this->consultation->getAlloti())
            ? $this->getParameter('URL_MPE') . '?page=Entreprise.PopUpDetailLots&orgAccronyme='
                . $this->consultation->getOrganisme() . '&id=' . $this->consultation->getId()
            : '';
        $messageBourse = $this->getMessageInscriptionBourseCotraitance();

        $paramsCons = [
            'consultation' => $this->consultation,
            'lienDetailLots' => $lienDetailLots,
            'lieuxExecutions' => $lieuxExecutions,
            'inscrit' => $user,
            'offre' => $this->consultation->getOffres(),
            'uid_response' => $uidManagement->generateUid($id),
            'interfaceModuleSub' => $this->moduleStateChecker->hasConfigPlateforme('interfaceModuleSub'),
            'messageBourse' => $messageBourse,
            'consultationInfo' => $consultationInfo,
            'liensConsult' => $liensConsult,
            'listePub' => $listePub,
            'listePubLibre' => $listePubLibre,
            'typeFormatAvis' => $typeFormatAvis,
            'isEnabled' => $isEnabled,
            'tvaIntracommunautaire' => $tvaIntracommunautaire,
            'dume' => $this->dumeService->getInfoDume($this->consultation, $contexte),
            'actionPossible' => $actionPossible,
            'paramMessecV2' => $paramMessecV2,
            'cloturer' => $this->cloturer,
        ];

        return array_merge($paramsConnected, $paramsContexte, $paramsGroupement, $paramsCons);
    }

    /**
     *
     * @throws Exception
     */
    private function getDumeContext(): array
    {
        $paramsConnected = [];
        $paramsContexte = [];
        $contexte = null;
        if ($this->consultation->getDumeDemande()) {
            try {
                if (1 === (int)$this->configurationPlatformService->getConfigurationPlateforme()->getInterfaceDume()) {
                    $clientDume = $this->atexoDumeService
                        ->createConnexionLtDume();

                    $clientDume->AuthentificationService()->login(
                        $this->parameterBag->get('LOGIN_DUME_API'),
                        $this->parameterBag->get('PASSWORD_DUME_API'),
                        $this->parameterBag->get('TYPE_DUME_OE')
                    );
                }
            } catch (Exception) {
                $paramsConnected['validateDume'] = $this->getParameter('LT_DUME_DOWN');
            }

            try {
                $contexte = $this->dumeService->getDumeContexteWaitingOrValidOrPublish($this->consultation);

                $paramsContexte = [
                    'isStandard' => $contexte->isStandard(),
                    'status' => $contexte->getStatus(),
                    'contexteLtDumeId' => $contexte->getContexteLtDumeId(),
                ];
            } catch (Exception $ex) {
                $id = $this->consultation->getId();
                $this->logger->error("[reference_consultation = $id] - Erreur :" . $ex->getMessage());
                throw $ex;
            }
        }

        return [$paramsConnected, $contexte, $paramsContexte];
    }

    /**
     * @param $dataGroupement
     * @param $paramsConnected
     */
    private function getConsultationLotInfo(Request $request, $dataGroupement, $paramsConnected): array
    {
        $lotJson = $this->em->getRepository(Lot::class)
            ->getLotsFormatJson($this->consultation->getId(), $this->consultation->getAcronymeOrg());
        $tvaIntracommunautaire = '';
        if (is_object($this->user)) {
            $idLots = $this->getLotsCandidatures($this->em);

            $candidature = $this->getCandidatureInfo($this->em);

            [$pagination, $messagesPagination, $piecesPagination] = $this->getEchangesInfo($this->em, $request);

            $paramsConnected = array_merge(
                $paramsConnected,
                [
                    'listLotJson' => $lotJson,
                    'idLots' => $idLots,
                    'pagination' => $pagination,
                    'messagesPagination' => $messagesPagination,
                    'piecesPagination' => $piecesPagination,
                ]
            );

            $dataGroupement['groupement'] = null;
            $dataGroupement['membres'] = [];

            [$etablissement, $tvaIntracommunautaire] = $this->getEtablissementInfo($this->em);

            if (null !== $candidature) {
                $paramsConnected['candidature'] = $candidature;
                $paramsConnected = $this->getGroupementInfo($candidature, $dataGroupement, $paramsConnected);
                $paramsConnected =
                    $this->checkDumeValidation($candidature, $paramsConnected, $etablissement, $tvaIntracommunautaire);
            }
        } else {
            $paramsConnected = [
                'listLotJson' => $lotJson,
            ];
        }

        return [$paramsConnected, $tvaIntracommunautaire, $dataGroupement];
    }

    private function getLotsCandidatures(EntityManagerInterface $em): array
    {
        $idLots = [];

        $listeLotsCandidatures = $em
            ->getRepository(TListeLotsCandidature::class)
            ->getListeLotByUserAndConsultation(
                $this->user,
                $this->consultation,
                $this->getParameter('STATUT_ENV_BROUILLON')
            );

        foreach ($listeLotsCandidatures as $value) {
            $idLots[] = $value->getNumLot();
        }

        return $idLots;
    }

    private function getCandidatureInfo(EntityManagerInterface $em): ?object
    {
        return $em
            ->getRepository(TCandidature::class)
            ->findOneBy([
                'organisme' => $this->consultation->getAcronymeOrg(),
                'consultation' => $this->consultation->getId(),
                'idInscrit' => $this->user->getId(),
                'idEntreprise' => $this->user->getEntrepriseId(),
                'idEtablissement' => $this->user->getIdEtablissement(),
                'status' => $this->getParameter('STATUT_ENV_BROUILLON'),
            ]);
    }

    private function getEchangesInfo(EntityManagerInterface $em, Request $request): array
    {
        $questionsQuery = $em
            ->getRepository(QuestionDCE::class)
            ->getQuestionsWithResponseDate(
                $this->consultation->getId(),
                $this->consultation->getOrganisme(),
                $this->user->getId(),
                $this->user->getEntreprise()->getId()
            );

        $paginator = $this->paginator;
        $pagination = $paginator->paginate(
            $questionsQuery,
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );
        $pagination->setParam('_fragment', self::ANCHOR_QUESTION);

        $messagesQuery = $em
            ->getRepository(Echange::class)
            ->getMessagesWithDestinataires(
                $this->consultation->getId(),
                $this->consultation->getOrganisme(),
                $this->user->getEmail()
            );

        $messagesPagination = $paginator->paginate(
            $messagesQuery,
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );
        $messagesPagination->setParam('_fragment', self::ANCHOR_MESSAGERIE);

        $piecesQuery = $em
            ->getRepository(Telechargement::class)
            ->getDownloadsByInscritAndConsultation(
                $this->consultation->getId(),
                $this->consultation->getOrganisme(),
                $this->user->getEntreprise()->getId(),
                $this->user->getId()
            );

        $piecesPagination = $paginator->paginate(
            $piecesQuery,
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return [$pagination, $messagesPagination, $piecesPagination];
    }

    private function getGroupementInfo(object $candidature, array $dataGroupement, array $paramsConnected): array
    {
        //Information du groupement
        $groupement = $this->em
            ->getRepository(TGroupementEntreprise::class)
            ->findOneBy([
                'candidature' => $candidature,
            ]);

        $idRoleJuridiqueSousTraitant = 3;
        $dataGroupement['groupement'] = $groupement;

        if (null !== $groupement) {
            foreach ($groupement->getMembresGroupement() as $key => $membre) {
                if ($membre->getIdRoleJuridique()->getIdRoleJuridique() !== $idRoleJuridiqueSousTraitant) {
                    $dataGroupement['membres'][$key] = $this->em->getRepository(TMembreGroupementEntreprise::class)
                        ->findBy([
                            'idMembreGroupementEntreprise' => $membre->getIdMembreGroupementEntreprise(),
                        ]);

                    $dataGroupement['membres'][$key]['sousMembres'] =
                        $this->em->getRepository(TMembreGroupementEntreprise::class)
                        ->findBy([
                            'idMembreParent' => $membre->getIdMembreGroupementEntreprise(),
                        ]);
                }
            }
        }

        $paramsConnected['groupement'] = $dataGroupement;

        return $paramsConnected;
    }

    private function getEtablissementInfo(EntityManagerInterface $em): array
    {
        $tvaIntracommunautaire = '';
        $etablissement = $em
            ->getRepository(Inscrit::class)
            ->find($this->user->getId())
            ->getIdEtablissement();

        if (null !== $etablissement) {
            $tvaIntracommunautaire = $em
                ->getRepository(Etablissement::class)
                ->findOneBy(['idEtablissement' => $etablissement])
                ->getTvaIntracommunautaire();
        }

        return [$etablissement, $tvaIntracommunautaire];
    }

    /**
     * @param $etablissement
     * @param $tvaIntracommunautaire
     */
    private function checkDumeValidation(
        object $candidature,
        array $paramsConnected,
        $etablissement,
        $tvaIntracommunautaire
    ): array {
        $dumeInProgress = null;

        // Appel webservice validation lt_dume
        if (
            null !== $candidature->getIdDumeContexte()
            && null !== $candidature->getIdDumeContexte()->getContexteLtDumeId()
        ) {
            try {
                $clientDume = $this->atexoDumeService
                    ->createConnexionLtDume();

                $validateDume = $clientDume->entrepriseService()->checkAccess(
                    $candidature->getIdDumeContexte()->getContexteLtDumeId()
                );

                $paramsConnected['validateDume'] = $validateDume->getStatut();
                $paramsConnected['idDumeContexte'] = $candidature->getIdDumeContexte();
            } catch (Exception) {
                $paramsConnected['validateDume'] = $this->getParameter('LT_DUME_DOWN');
            }
        }

        /**
         * @var Entreprise $entreprise
         */
        $entreprise = $this->em
            ->getRepository(Entreprise::class)
            ->find($this->user->getEntrepriseId());

        $codeEtablissement = $this->em
            ->getRepository(Etablissement::class)
            ->findOneBy(['idEtablissement' => $etablissement])
            ->getCodeEtablissement();

        $dumeInProgress = $this->em
            ->getRepository(TCandidature::class)
            ->getDumeInProgress(
                $this->consultation->getId(),
                $this->consultation->getOrganisme()->getAcronyme(),
                [
                    $this->parameterBag->get('STATUT_DUME_CONTEXTE_BROUILLON'),
                ],
                $this->parameterBag->get('TYPE_CANDIDATUE_DUME'),
                $this->parameterBag->get('TYPE_CANDIDATUE_DUME_ONLINE'),
                $entreprise->getSiren(),
                $codeEtablissement,
                $tvaIntracommunautaire,
                $this->user
            );
        //gestion du multiDepot DUME + popin d'alertissement dans la vue
        $paramsConnected['multiDepot'] = ['isDumePublier' => false];
        if ($this->user) {
            $paramsConnected['multiDepot'] = $this->dumeService->getDepotDumeValid(
                true,
                $entreprise,
                $codeEtablissement,
                $tvaIntracommunautaire,
                $this->consultation,
                $this->user
            );
        }

        $paramsConnected['dumeInProgress'] = $dumeInProgress;

        return $paramsConnected;
    }

    private function getLienPubInfo(): array
    {
        $listeAvis = [];
        if ($this->moduleStateChecker->hasConfigPlateforme('publicite')) {
            try {
                $accessToken = $this->echangeConcentrateur->getToken();
                if (!empty($accessToken)) {
                    $listeAvis = $this->echangeConcentrateur->getConsultationAll($accessToken, $this->consultation->getId());
                }
            } catch (\Exception) {
                $this->addFlash('error-publicite', $this->translator->trans('SERVICE_PUBLICITE_INDISPONIBLE'));
            }
        }

        return $listeAvis;
    }

    private function getLienPubLibreInfo(): array
    {
        //Lien AVIS
        $listeAvisPub = $this
            ->em
            ->getRepository(Avis::class)
            ->findBy([
                'consultationId' => $this->consultation->getId(),
                'organisme' => $this->consultation->getOrganisme(),
                'statut' => $this->parameterBag->get('DESTINATAIRE_PUBLIE'),
            ], [
                'datePub' => 'DESC',
            ]);
        $listePub = [];
        /** @var Avis $avis */
        foreach ($listeAvisPub as $avis) {
            $urlAvis = '';
            if (
                $avis->getType() === $this->parameterBag->get('TYPE_FORMAT_LIBRE_FICHIER_JOINT')
                || $avis->getType() === $this->parameterBag->get('TYPE_FORMAT_PDF_TED')
                || $avis->getType() === $this->parameterBag->get('TYPE_FORMAT_AVIS_IMPORTES')
                || $avis->getType() === $this->parameterBag->get('TYPE_FORMAT_PDF_TED_HORS_PF')
            ) {
                if ($avis->getTypeDocGenere() === $this->parameterBag->get('TYPE_DOC_GENERE_AVIS')) {
                    if (!$this->moduleStateChecker->hasConfigPlateforme('publiciteOpoce')) {
                        $libelleAvis =
                            $this->translator->trans('TEXT_FICHIER_JOINT_AVIS_PUBLICITE')
                            . ' - ' . strtoupper($avis->getLangue());
                    }
                } else {
                    if ($avis->getType() === $this->parameterBag->get('TYPE_FORMAT_PDF_TED_HORS_PF')) {
                        $libelleAvis = $this->translator->trans('TEXT_AVIS_EUROPEEN_FICHIER_JOINT');
                    } else {
                        $libelleAvis = $this->translator->trans('TEXT_FICHIER_JOINT_AVIS_COMPLEMENTAIRE');
                    }
                }
                $typeAvis = $this->parameterBag->get('TYPE_FORMAT_LIBRE_FICHIER_JOINT');
            } else {
                $libelleAvis = $this->translator->trans('TEXT_URL_ACCES_DIRECT');
                $typeAvis = $this->parameterBag->get('TYPE_FORMAT_LIBRE_URL');
                $urlAvis = $avis->getUrl();
            }
            $listePub[] = [
                'url' => $urlAvis,
                'libelle' => $libelleAvis,
                'type' => $typeAvis,
                'id' => $avis->getId(),
            ];
        }

        //Lien BOAMP
        $boamps = $this->em
            ->getRepository(AnnonceBoamp::class)
            ->findBy([
                'consultationId' => $this->consultation->getId(),
                'organisme' => $this->consultation->getOrganisme(),
            ]);

        /** @var AnnonceBoamp $boamp */
        foreach ($boamps as $boamp) {
            if (!empty($boamp->getLienBoamp())) {
                $listePub[] = [
                    'url' => $boamp->getLienBoamp(),
                    'libelle' => $this->translator->trans('NOM_LIEN_AVIS_BOAMP'),
                    'type' => $this->parameterBag->get('TYPE_FORMAT_URL_BOAMP'),
                ];
            }
        }

        $typeFormatAvis = [
            'TYPE_FORMAT_LIBRE_FICHIER_JOINT' => $this->getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT'),
            'TYPE_FORMAT_LIBRE_URL' => $this->getParameter('TYPE_FORMAT_LIBRE_URL'),
            'TYPE_FORMAT_URL_BOAMP' => $this->getParameter('TYPE_FORMAT_URL_BOAMP'),
            'TYPE_FORMAT_URL_TED' => $this->getParameter('TYPE_FORMAT_URL_TED'),
        ];

        return [$listePub, $typeFormatAvis];
    }

    private function getParamGroupement(): array
    {
        $paramsGroupement = [
            'idTypeGroupementSolidaire' => $this->parameterBag->get('ID_TYPE_GROUPEMENT_SOLIDAIRE'),
            'idTypeGroupementConjointNonSolidaire' =>
                $this->parameterBag->get('ID_TYPE_GROUPEMENT_CONJOINT_NON_SOLIDAIRE'),
            'idTypeGroupementConjointSolidaire' =>
                $this->parameterBag->get('ID_TYPE_GROUPEMENT_CONJOINT_SOLIDAIRE'),
            'idRoleJuridiqueMandataire' => $this->parameterBag->get('ID_ROLE_JURIDIQUE_MANDATAIRE'),
        ];

        return $paramsGroupement;
    }

    private function getDossierVolumineux(): array
    {
        $tabDossierVolumineux = [];
        if ($this->consultation->getDossierVolumineux()) {
            $tabDossierVolumineux['lienConsul'] = $this->getParameter('URL_MPE') .
                'index.php?page=Entreprise.EntrepriseDemandeTelechargementDce&id=' .
                htmlspecialchars($this->consultation->getId()) .
                '&orgAcronyme=' .
                htmlspecialchars($this->consultation->getAcronymeOrg());

            $tabDossierVolumineux['tailleConsul'] = $this->translator->trans('DEFINE_DOSSIER_CONSULTATION');
        }

        return $tabDossierVolumineux;
    }

    private function getActionPossible(array $tabDossierVolumineux): array
    {
        $now = new DateTime();
        $dateFinPoursuivreAffichage = (new DateTime())->setTimestamp($this->consultation->getDateFinUnix());
        $actionPossible = (
            $this->consultation->getIdTypeAvis() == $this->getParameter('TYPE_AVIS_CONSULTATION')
            && !$this->consultation->getConsultationAnnulee()
            && (
                (
                    $this->moduleStateChecker->hasConfigPlateforme('entrepriseRepondreConsultationApresCloture')
                    && $dateFinPoursuivreAffichage >= $now
                )
                || $this->consultation->getDatefin() > $now
            )
        );
        $liensConsult = [];
        if ($actionPossible) {
            $liensConsult = [
                'complement' => $this->documentService->getLienComplement($this->consultation),
                'rg' => $this->documentService->getLienRg($this->consultation),
                'dce' => $this->documentService->getLienDCE($this->consultation),
                'dume' => $this->atexoDumeService->getDume($this->consultation),
                'dossierVolumineux' => $tabDossierVolumineux,
            ];
            $this->cloturer = false;
        }

        if ($this->cloturer) {
            $liensConsult = [
                'complement' => $this->documentService->getLienComplement($this->consultation),
                'rg' => $this->documentService->getLienRg($this->consultation),
                'dce' => $this->documentService->getLienDCE($this->consultation),
                'dume' => $this->atexoDumeService->getDume($this->consultation),
                'dossierVolumineux' => $tabDossierVolumineux,
            ];
        }

        return [$actionPossible, $liensConsult];
    }

    /**
     * @throws Error
     */
    private function getMessecVersion(): array
    {
        $paramMessecV2 = [];
        if (2 === $this->consultation->getVersionMessagerie()) {
            $paramMessecV2 = $this->getTokenMessage();
        }

        return $paramMessecV2;
    }

    /**
     * @todo refacturer dans un service
     */
    private function getTokenMessage(): array
    {
        $reponse = [];
        $tab = [];
        if ($this->user) {
            $messagerieService = $this->messagerieService;
            $params = [
                'idObjetMetier' => $this->consultation->getId(),
                'refObjetMetier' => $this->consultation->getReferenceUtilisateur(),
                'mailDestinataire' => $this->user->getEmail(),
                'metaDonnees' => $messagerieService->getMetaDonnees($this->consultation),
            ];

            $reponse['token'] = $this->webServicesMessagerie->getContent(
                'initialisationSuiviToken',
                $params
            );
            if (empty($reponse['token'])) {
                $reponse['status'] = Response::HTTP_INTERNAL_SERVER_ERROR;
                $tab = $reponse;
            } else {
                $consultationIdCrypte = $this->encryption->cryptId($this->consultation->getId());
                $messagerieEntrepriseVisualisationMessage = $this->generateUrl(
                    'messagerie_entreprise_visualisation_message',
                    ['consultationId' => $consultationIdCrypte],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );

                $tab = [
                    'token' => $reponse['token'],
                    'url_visualisation_message' => $messagerieEntrepriseVisualisationMessage . '?codelien=',
                    'url_front' => $this->atexoUtil
                        ->getPfUrlDomaine($this->parameterBag->get('URL_MESSAGERIE_SECURISEE')),
                ];
            }
        }

        return $tab;
    }

    /**
     * Permet de retourner le message selon le cas.
     */
    private function getMessageInscriptionBourseCotraitance(): array
    {
        $locale = $this->session->get('_locale') ?? $this->parameterBag->get('locale');
        $message = '';
        $nomEse = '';
        $isInGroupement = 0;
        $data = [
            'message' => $message,
            'cas' => 'cas0',
            'image' => '',
        ];

        $countGroupement = $this->em->getRepository(TBourseCotraitance::class)
            ->getCountGroupement($this->consultation->getId());

        if (is_object($this->user)) {
            $entreprise = $this->em->getRepository(Entreprise::class)
                ->findOneBy(['id' => $this->user->getEntrepriseId()]);

            if ($entreprise instanceof Entreprise) {
                $nomEse = $entreprise->getNom();
            }

            $isInGroupement = $this->em->getRepository(TBourseCotraitance::class)
                ->isInGroupement($this->consultation->getId(), $this->user->getEntrepriseId());
        }

        if (0 === $countGroupement) {
            $data = [
                'message' => $this->translator
                    ->trans('DEFINE_AUCUNE_ESE_INSCRITE_BOURSE_COTRAITANCE_CONSULTATION', [], null, $locale),
                'cas' => 'cas1',
                'image' => 'icone-cotraitance-1.png',
            ];
        } elseif (1 == $countGroupement && 1 == $isInGroupement) {
            $data = [
                'message' => '<strong>' . $nomEse . ' </strong>' . $this->translator->trans(
                    'DEFINE_SEUL_MON_ESE_INSCRITE_BOURSE_COTRAITANCE_CONSULTATION',
                    [],
                    null,
                    $locale
                ),
                'cas' => 'cas2',
                'image' => 'icone-cotraitance-2.png',
            ];
        } elseif ($countGroupement > 1 && 1 == $isInGroupement) {
            $message = '<a title="Voir le détail" href="/?page=Entreprise.EntrepriseRechercheBourseCotraitance&id='
                . $this->consultation->getId() . '&orgAcronyme=' . $this->consultation->getAcronymeOrg() . '&GME">
                        <strong>' . $countGroupement . ' '
                . $this->translator->trans('TEXT_ENTREPRISES_INSCRITES', [], null, $locale) . ' </strong>
                    </a> ' .
                $this->translator->trans('DEFINE_INSCRITE_BOURSE_COTRAITANCE_Y_COMPRIS_MON_ESE', [], null, $locale) .
                '<strong> ' . $nomEse . ' </strong>';

            $data = [
                'message' => $message,
                'cas' => 'cas3',
                'image' => 'icone-cotraitance-4.png',
            ];
        } elseif ($countGroupement >= 1 && 0 == $isInGroupement) {
            if (is_object($this->user)) {
                $message = '<span title="'
                    . $this->translator->trans('TEXT_SINSCRIRE_POUR_VISUALISER_DETAIL', [], null, $locale) . '">
                    <strong>' . $countGroupement . ' '
                    . $this->translator->trans('DEFINE_ESE_INSCRITE') . '</strong></span>
                    <br/><strong>' . $nomEse . ' </strong>'
                    . $this->translator->trans(
                        'DEFINE_MON_ESE_NON_INSCRITE_BOURSE_COTRAITANCE_CONSULTATION',
                        [],
                        null,
                        $locale
                    );
            } else {
                $message = '<span title="'
                    . $this->translator->trans(
                        'TEXT_SINSCRIRE_POUR_VISUALISER_DETAIL',
                        [],
                        null,
                        $locale
                    ) . '"><strong> ' .
                    $countGroupement . ' ' . $this->translator->trans('DEFINE_ESE_INSCRITE', [], null, $locale) .
                    '</strong></span>';
            }

            $data = [
                'message' => $message,
                'cas' => 'cas4',
                'image' => 'icone-cotraitance-3.png',
            ];
        }

        return $data;
    }

    /**
     *
     * @throws NonUniqueResultException
     */
    private function checkConsultationCloturer(int $consultationId, array $orgEligible): bool
    {
        if (array_key_exists('clotures', $_GET)) {
            if ($this->user) {
                $consultation = $this->getDoctrine()->getRepository(Consultation::class)->
                    getClosedConsultations(
                        $consultationId,
                        $this->user->getId(),
                        $orgEligible
                    );
                if ($consultation instanceof Consultation) {
                    return $this->cloturer = true;
                }
            } else {
                $this->logger->notice('User not connected attempt to access a page with param : clotures');
            }
        }

        return $this->cloturer = false;
    }

    /**
     * @throws Exception
     */
    private function setTraceInscrit(
        InscritOperationsTracker $inscritOperationsTracker,
        DateTime $dateDebutValidation
    ): void {
        //Debut tracer operations de l'inscrit
        [$idValeurReferentielle, $description] =
            $this->inscritOperationTracker->getInfosValeursReferentielles('DESCRIPTION4');
        if ($this->consultation instanceof Consultation) {
            $description = str_replace(
                '[__ref_utilisateur__]',
                $this->consultation->getReferenceUtilisateur(),
                $description
            );
        }
        $idOffre = ($this->consultation->getOffres() instanceof Offre) ? $this->offre->getId() : null;
        $inscritOperationsTracker->tracerOperationsInscrit(
            $dateDebutValidation,
            new DateTime(),
            $idValeurReferentielle,
            $description,
            $this->consultation,
            '',
            false,
            null,
            1,
            null,
            $idOffre
        );
        //Fin tracer operations de l'inscrit
    }
}
