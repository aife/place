<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise\Consultation;

use DateTime;
use Exception;
use App\Entity\Consultation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/entreprise/consultation')]
class ConsultationDownloadDumeController extends ConsultationIndexDepotController
{
    /**
     * Permet de télécharger le DUME (xml ou pdf) pour les groupements.
     *
     * @Method({"GET"})
     *
     * @param $id
     * @param $idNumero
     * @param $format
     * @return BinaryFileResponse
     */
    #[Route(path: '/contenus_transmis/dume/telechargement/file/{id}/{idNumero}/{format}', name: 'atexo_consultation_contenus_transmis_dume_telechargement_file')]
    public function downloadDUMEById($id, $idNumero, $format)
    {
        return $this->getBinaryResponse($this->dumeService->downloadDume($format, $id, $idNumero));
    }
    /**
     * @param $fileArray
     *
     * @return BinaryFileResponse
     */
    public function getBinaryResponse($fileArray)
    {
        // prepare BinaryFileResponse
        $response = new BinaryFileResponse($fileArray['filePath']);
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $fileArray['dumeNumero'].'.'.explode('/', (string) $response->getFile()->getMimeType())[1]
        );

        return $response;
    }
    /**
     * Permet de telecharger le XML DUME d'une offre (contenus transmis).
     *
     * @Method({"GET"})
     *
     * @param $id
     * @return BinaryFileResponse
     */
    #[Route(path: '/contenus_transmis/dume/telechargement/xml/{id}', name: 'atexo_consultation_contenus_transmis_dume_telechargement_xml')]
    public function downloadXmlDUME($id)
    {
        return $this->getBinaryResponse($this->dumeService->downloadDume('xml', $id));
    }
    /**
     * Permet de telecharger le PDF DUME d'une offre (contenus transmis).
     *
     * @Method({"GET"})
     *
     * @param $id
     * @return BinaryFileResponse
     */
    #[Route(path: '/contenus_transmis/dume/telechargement/pdf/{id}', name: 'atexo_consultation_contenus_transmis_dume_telechargement_pdf')]
    public function downloadPdfDUME($id)
    {
        return $this->getBinaryResponse($this->dumeService->downloadDume('pdf', $id));
    }
    /**
     * Permet de telecharger le PDF DUME d'une offre (contenus transmis).
     *
     * @Method({"GET"})
     *
     * @param $idNumero
     * @param $idConsultation
     * @param $acronymeOrganisme
     * @param $format
     * @return BinaryFileResponse
     */
    #[Route(path: '/contenus_transmis/dume/telechargement/zip/{idNumero}/{idConsultation}/{acronymeOrganisme}/{format}', name: 'atexo_consultation_contenus_transmis_dume_telechargement_zip')]
    public function downloadZipDUMEById($idNumero, $idConsultation, $acronymeOrganisme, $format)
    {
        try {
            $archiveInfo = $this->atexoDumeService
                ->downloadZipDUMEById($idNumero, $acronymeOrganisme, $format);
            if (is_array($archiveInfo) && count($archiveInfo)) {
                // prepare BinaryFileResponse
                $response = new BinaryFileResponse($archiveInfo['archiveName']);
                $response->trustXSendfileTypeHeader();
                $response->setContentDisposition(
                    ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                    $archiveInfo['downloadName'].'.'.explode('/', (string) $response->getFile()->getMimeType())[1]
                );
                $consultation = $this->em->getRepository(Consultation::class)->findOneByReferenceAnOrganisme($idConsultation, $acronymeOrganisme);
                if ($consultation) {
                    //Début d'enregistrement du trace
                    [$idValeurReferentielle, $description] = $this->inscritOperationTracker->getInfosValeursReferentielles('DESCRIPTION44');
                    $this->inscritOperationTracker->tracerOperationsInscrit(new DateTime(), new DateTime(), $idValeurReferentielle, $description, $consultation, '', false, null, 1, null, null);
                    //Fin d'enregistrement du trace
                }

                return $response;
            }
        } catch (Exception $e) {
            $this->depotLogger->info(' Erreur lors de telechargement du Zip DUME Acheteur '.$e->getMessage().' '.$e->getTraceAsString());
        }
        return $this->redirectToRoute(
            'atexo_consultation_index',
            ['id' => $idConsultation]
        );
    }
}
