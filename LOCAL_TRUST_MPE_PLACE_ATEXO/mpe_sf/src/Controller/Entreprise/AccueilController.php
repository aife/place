<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise;

use App\Service\AtexoEntreprise;
use App\Service\AtexoInscrit;
use App\Service\Entreprise\Accueil;
use App\Service\Entreprise\Menu;
use App\Utils\Utils;
use Application\Service\Atexo\Atexo_Config;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccueilController extends CommonController
{
    public $pageTitle = null;

    #[Route(path: '/entreprise', name: 'accueil_entreprise')]
    public function accueil(Accueil $accueil, Menu $menu, AtexoEntreprise $atexoEntreprise) : Response
    {
        $info = [];
        $parameters = [
            'MENU_CSS_TEST_CONFIG',
            'ACTIVER_OPTION_CARTE_PAGE_ACCUEIL',
        ];
        $parameters = array_merge($parameters, $this->getParameters());
        foreach ($parameters as $parameter) {
            $info[strtolower($parameter)] = $this->getParameter($parameter);
        }
        $accueilEntreprisePersonnalise = $this->getConfigurationPlateforme()->getAccueilEntreprisePersonnalise();
        $info['url_standard_connexion'] = $menu->getUrlStandardConnexion();
        $info['url_inscription'] = $menu->getUrlInscription();
        $info['lblMsgAccueil'] = $accueil->getMsgAccueil();
        $info['lblMsgDesign'] = $accueil->getMsgDesign();
        $info['categorie'] = ['DEFINE_TRAVAUX', 'DEFINE_FOURNITURES', 'DEFINE_SERVICES'];
        $info['accueil_entreprise_personnalise'] = $accueilEntreprisePersonnalise;
        $info['socle_url_connect'] = 'SocleExternePpp';
        $info['liens_menu_personnalise'] = $menu->getLiensMenuEntreprisePersonalise();
        $twigTemplate = 'entreprise/accueil/accueil.html.twig';
        $homeMessages = $atexoEntreprise->getHomeBasicMessages();

        $info['displayDematBlock'] = $this->getParameter('AFFICHER_BLOC_DEMATERIALISATION_ACCUEIl_ENTREPRISE');
        $info['homeMessage'] = $homeMessages['homeMessage'];
        $info['truncatedHomeMessage'] = $homeMessages['homeMessage'];
        $info['langMessages'] = $homeMessages['langMessages'];
        if (!$accueilEntreprisePersonnalise) {
            $twigTemplate = 'entreprise/accueil/accueil-basic.html.twig';
        }
        return $this->render($twigTemplate, ['info' => $info, 'fullWidth' => $accueilEntreprisePersonnalise]);
    }

    #[Route(path: '/entreprise/activateInscrit/{uid}', name: 'entreprise_activate_inscrit')]
    public function activateInscrit(string $uid, AtexoInscrit $atexoInscrit) : Response
    {
        $message = $atexoInscrit->activateInscritByUid($uid);
        return $this->render(
            'entreprise/accueil/activate-inscrit.html.twig',
            ['message' => $message, 'fullWidth' => true]
        );
    }
}
