<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise;

use App\Controller\AtexoController;
use App\Entity\ConfigurationPlateforme;
use App\Service\Configuration;
use App\Service\Entreprise\Menu;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;


class CommonController extends AbstractController
{
    private $configurationPlateforme;
    private $parameters;

    public function __construct(Configuration $configuration)
    {
        $this->initParameters();
        $this->configurationPlateforme = $configuration->getConfigurationPlateforme();
    }

    protected function getConfigurationPlateforme()
    {
        return $this->configurationPlateforme;
    }

    protected function getParameters() : array
    {
        return $this->parameters;
    }

    private function initParameters() : void
    {
        $this->parameters = [
            'PF_URL_ENTREPRISE',
            'PF_URL',
            'MSG_ACCUEIL_BTN_JE_M_INSCRIS_AFFICHAGE',
            'ACTIVER_FC_AUTHENTIFICATION',
            'URL_FRANCECONNECT_CONCENTRATEUR_LOGIN',
            'URL_FRANCECONNECT_DETAIL',
            'URL_DEMARCHE_INSCRIPTION_ENTREPRISE',
            'MENU_URL_MPS',
            'MENU_CSS_GUIDE',
            'MENU_CSS_OUTILS',
            'MENU_CSS_RECHERCHE_SIGNER',
            'MENU_CSS_RECHERCHE_AVIS_MARCHE',
            'MENU_CSS_CAHIER_CHARGE',
            'MENU_CSS_PANIER_ENTREPRISE',
            'MENU_CSS_ENCHERES',
            'MENU_CSS_MPS',
            'MENU_CSS_BOURCE_COTRAITANCE',
            'URL_LT_MPE_SF',
        ];
    }
}
