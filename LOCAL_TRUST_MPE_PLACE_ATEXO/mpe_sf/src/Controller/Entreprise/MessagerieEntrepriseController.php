<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise;

use App\Service\AtexoUtil;
use Exception;
use App\Controller\AtexoController;
use App\Entity\Consultation;
use App\Entity\ContratTitulaire;
use App\Entity\DossierVolumineux\DossierVolumineux;
use App\Entity\Inscrit;
use App\Exception\ClientWsException;
use App\Service\AtexoConfiguration;
use App\Service\ClientWs\ClientWsV2;
use App\Service\Messagerie\MessagerieService;
use App\Service\Messagerie\WebServicesMessagerie;
use App\Service\Perimeter\Perimeter;
use App\Utils\Encryption;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class MessagerieEntrepriseController.
 **/
#[Route(path: '/entreprise/messagerie', name: 'messagerie_entreprise_')]
class MessagerieEntrepriseController extends AtexoController
{
    public const ERROR_TECHNIQUE = 'Une erreur technique s\'est produite.';
    public const ERROR_WS = 'Problème avec l\'api ou le WS.';

    public function __construct(
        private EntityManagerInterface $em,
        private SessionInterface $session,
        protected LoggerInterface $logger,
        protected TranslatorInterface $translator,
        private MessagerieService $messagerieService,
        private Encryption $encryption,
        private Perimeter $perimeter,
        private AtexoUtil $atexoUtil,
        private ParameterBagInterface $parameterBag,
        private ClientWsV2 $clientWs
    ) {
    }

    /**
     * @throws Exception
     */
    #[Route(path: '/visualisation/{consultationId}', name: 'visualisation_message', methods: ['GET'])]
    public function visualisationMessageAction(Request $request, AtexoConfiguration $atexoConfiguration, WebServicesMessagerie $webServicesMessagerie): Response
    {
        $params = [];
        $reponse = [];
        $consultationIdCrypte = $request->get('consultationId');
        $consultationId = $this->encryption->decryptId($consultationIdCrypte);
        $consultation = $this->em
            ->getRepository(Consultation::class)
            ->find($consultationId);

        if(!$consultation instanceof Consultation) {
            $reponse = [];
            $twig = 'messagerie/error-consultation-introuvable.html.twig';
        } else {
            $params['codeLien'] = $request->query->get('codelien');
            $params['idObjetMetier'] = $consultationId;
            $params['refObjetMetier'] = $consultation->getReferenceUtilisateur();
            $params['metaDonnees'] = $this->messagerieService->getMetaDonnees($consultation);

            if ($request->query->get('contratId')) {
                $contratId = $this->encryption->decryptId($request->query->get('contratId'));
                $contratTitulaire = $this->em->getRepository(ContratTitulaire::class)->findOneBy([
                    'idContratTitulaire' => $contratId,
                ]);
                if (
                    $contratTitulaire instanceof ContratTitulaire
                    && $contratTitulaire->getDateNotification() === null
                    && $contratTitulaire->getStatut() === (int)$this->parameterBag->get('STATUT_NOTIFICATION_CONTRAT')
                ) {
                    $this->setNotificationContrat($contratTitulaire);
                }
            }

            if ($this->getUser() instanceof Inscrit) {
                if ($consultation->isEnvolActivation()) {
                    $params['dossiersVolumineux'] = $this->em
                        ->getRepository(DossierVolumineux::class)
                        ->getDataForInscritMessecV2($this->getUser()->getId());
                }

                $params['mailDestinataire'] = $this->getUser()->getEmail();
            }
            try {
                $listeEmailAgent = $this->perimeter->getListEmailAgent($consultation);
                if (count($listeEmailAgent) > 0) {
                    $params['emailsAlerteReponse'] = $listeEmailAgent;
                }
                $reponse['token'] = $webServicesMessagerie->getContent('initialisationSuiviToken', $params);
                $reponse['titrePage'] = $this->translator->trans('visualisationMessage');
                $reponse['url_front'] = $this->atexoUtil
                    ->getPfUrlDomaine($this->parameterBag->get('URL_MESSAGERIE_SECURISEE'));
                $reponse['idConsultation'] = $consultationId;
                $reponse['organisme'] = $consultation->getAcronymeOrg();

                $twig = 'messagerie/entreprise/detail.html.twig';
            } catch (Exception $e) {
                $this->logger(self::ERROR_TECHNIQUE, $e);
                $reponse['status'] = $e->getCode();
                $reponse['error'] = utf8_encode($e->getMessage());
                $reponse['token'] = null;
                $twig = 'messagerie/error.html.twig';
            }
        }

        return $this->render($twig, $reponse);
    }

    /**
     * @param $message
     * @param $Exception
     */
    protected function logger($message, $Exception)
    {
        $this->logger->error($message, [
            'httpCode' => $Exception->getCode(),
            'message' => utf8_encode($Exception->getMessage()),
        ]);
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    private function setNotificationContrat(ContratTitulaire $contratTitulaire)
    {
        $dateTime = new \DateTime();
        $contratTitulaire->setDateNotification($dateTime);
        $contratTitulaire->setDateModification($dateTime);
        $contratTitulaire->setStatutContrat($this->parameterBag->get('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE'));
        $this->em->persist($contratTitulaire);
        $this->em->flush();
    }

    /**
     * @Route("/visualisation-contrat", name="visualisation_contrat", methods={"GET"})
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function messageAction(Request $request, WebServicesMessagerie $webServicesMessagerie)
    {
        $reponse = [];
        $params['codeLien'] = $request->query->get('codelien');

        if ($this->getUser() instanceof Inscrit) {
            $params['mailDestinataire'] = $this->getUser()->getEmail();
        }

        try {
            $reponse['token'] = $webServicesMessagerie->getContent('initialisationSuiviToken', $params);
            $reponse['titrePage'] = $this->translator->trans('visualisationMessage');
            $reponse['url_front'] = $this->atexoUtil
                ->getPfUrlDomaine($this->parameterBag->get('URL_MESSAGERIE_SECURISEE'));

            $tabApiExec = [
                'acte' => 'exec-api/api/actes/notification/',
                'contrat' => 'exec-api/api/contrat/notification/'
            ];


            foreach ($tabApiExec as $query => $route) {
                $this->postApiExec($request, $query, $route, $webServicesMessagerie);
            }

            $twig = 'messagerie/entreprise/message.html.twig';
        } catch (ClientWsException $e) {
            $this->logger(self::ERROR_TECHNIQUE, $e);
            $reponse['status'] = $e->getCode();
            $reponse['error'] = utf8_encode($e->getMessage());
            $reponse['token'] = null;
            $twig = 'messagerie/error.html.twig';
        }
        return $this->render($twig, $reponse);
    }

    /**
     * @param Request $request
     * @param $query | value = 'acte' ou 'contrat'
     * @param $route | value = api/actes/notification/' OU 'api/contrat/notification/'
     * @param WebServicesMessagerie $webServicesMessagerie
     */
    protected function postApiExec(
        Request $request,
        string $query,
        string $route,
        WebServicesMessagerie $webServicesMessagerie
    ): void {
        if ($request->query->get($query)) {
            try {
                $urlExec =
                    $this->parameterBag->get('PF_URL_REFERENCE')
                    . $route
                    . $request->query->get($query);
                $result = $this->clientWs->post($urlExec);
                $this->logger->info(
                    "Synchro EXEC : url Post " . $urlExec . " result " .  $result->getBody()->getContents()
                );
            } catch (\Exception $exception) {
                $message = self::ERROR_TECHNIQUE . ' | Query : ' . $query . ' | API : ' . $route;
                $this->logger($message, $exception);
            }
        }
    }
}
