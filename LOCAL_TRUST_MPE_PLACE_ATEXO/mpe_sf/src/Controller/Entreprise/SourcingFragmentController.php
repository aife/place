<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise;

use App\Entity\Inscrit;
use App\Service\WebServicesSourcing;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SourcingFragmentController extends AbstractController
{
    #[Route(path: '/entreprise/sourcing-fragment', name: 'sourcing_fragment')]
    public function index(WebServicesSourcing $sourcing, LoggerInterface $log)
    {
        $isConnected = false;
        $isRegistered = false;
        try {
            if ($this->getUser() instanceof Inscrit) {
                $isConnected = true;
                $response = $sourcing->getContent('isCompanyRegistered', $this->getUser()->getEmail());
                $isRegistered = !empty($response->email);
            }
        } catch (\Exception $exception) {
            $log->error(sprintf('Service sourcing returned error: %s', $exception->getMessage()));
            $isRegistered = true;
        }

        return $this->render('ecrans-entreprise/sourcing-fragment.html.twig', [
            'isConnected' => $isConnected,
            'isRegistered' => $isRegistered
        ]);
    }
}
