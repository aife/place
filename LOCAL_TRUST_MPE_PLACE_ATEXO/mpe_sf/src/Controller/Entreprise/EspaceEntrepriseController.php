<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class EspaceEntrepriseController.
 **/
#[Route(path: '/espace-entreprise', name: 'espace_entreprise_')]
class EspaceEntrepriseController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly SessionInterface $session,
        private readonly LoggerInterface $logger,
        private readonly TranslatorInterface $translator
    ) {
    }

    #[Route(path: '/search', name: 'search', methods: ['POST', 'GET'])]
    public function searchAction(Request $request)
    {
        if ($request->getMethod() == 'GET') {
            $key = $request->query->get('keyWord');
            $categorie = $request->query->get('categorie');
            $localisations = null;
        } else {
            $key = $request->request->get('keyWord');
            $categorie = $request->request->get('categorie');
            $localisations = is_array($request->request->get('localisations')) ?
                implode(',', $request->request->get('localisations')) : '';
        }
        $queryData = [
            "keyWord" => $key,
            "categorie" => $categorie,
            "localisations" => $localisations
        ];
        $urlSearchPrado = '/?page=Entreprise.EntrepriseAdvancedSearch&searchAnnCons&' . http_build_query($queryData);
        return $this->redirect($urlSearchPrado);
    }
}
