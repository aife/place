<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise;

use App\Entity\Inscrit;
use App\Entity\Inscrit\InscritHistorique;
use App\Repository\InscritHistoriqueRepository;
use App\Service\CurrentUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class EvenementUtilisateurAdminController extends AbstractController
{
    public function __construct(
        private readonly InscritHistoriqueRepository $inscritHistoriqueRepository,
        private readonly ParameterBagInterface $parameterBag,
        private readonly TranslatorInterface $translator
    ) {
    }

    /**
     * @param CurrentUser $currentUser
     * @param string $userType
     * @return Response|RedirectResponse
     */
    #[Route(
        path: '/entreprise/evenements-admin',
        name: 'evenement_admin_entreprise',
        defaults: ['userType' => 'entreprise']
    )]
    public function listEvenementAdmin(
        CurrentUser $currentUser,
        string $userType = 'entreprise'
    ): Response|RedirectResponse {
        $params = [];
        $params['connected'] = $currentUser->isConnected();
        $params['breadcrumb'] = 'TEXT_LISTE_EVENEMENTS_UTILISATEURS';
        $params['tpl'] = 'Inscrit/evenement-utilisateurs.html.twig';

        if (!$currentUser->isConnected()) {
            $translator = $this->translator;
            $this->addFlash(
                'error',
                $translator->trans('ACCES_NON_AUTORISE')
            );
            return $this->render('Default-App/index.html.twig', $params);
        }

        /** @var Inscrit $user */
        $user = $currentUser->getCurrentUser();
        $params['datas'] = $this->inscritHistoriqueRepository
            ->findBy(['entrepriseId' => $user->getEntreprise()->getId()]);

        return $this->render('Default-App/index.html.twig', $params);
    }
}
