<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise;

use App\Service\AtexoEntreprise;
use App\Service\AtexoInscrit;
use App\Service\Entreprise\EntrepriseAPIService;
use App\Service\Entreprise\SocietesExcluesService;
use App\Service\ExcelService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

class EntrepriseController extends AbstractController
{
    /**
     * Création d'une entreprise, de ses établissements et du compte inscrit lié
     **/
    #[Route(path: '/entreprises', methods: ['POST'], name: 'create-entreprise')]
    public function create(Request $request, AtexoEntreprise $atexoEntreprise, AtexoInscrit $atexoInscrit) : Response
    {
        $body = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $control = $atexoEntreprise->controlEntrepriseCreationFromInscription($body);
        if (!empty($control)) {
            return $this->json(['error' => $control], Response::HTTP_BAD_REQUEST);
        }
        $control = $atexoInscrit->controlInscritCreationFromInscription($body);
        if (!empty($control)) {
            return $this->json(['error' => $control], Response::HTTP_BAD_REQUEST);
        }
        $selectedEstablishment = 0;
        foreach ($body['etablissements'] as $key => $establishment) {
            if ($establishment['selected'] === true) {
                $selectedEstablishment = $key;
            }
        }
        $creation = $atexoEntreprise->createFromInscription($body);
        $inscrit = $atexoInscrit->createFromInscription(
            $body['inscrit'],
            $creation['entreprise'],
            $creation['establishments'][$selectedEstablishment] ?? $creation['establishments'][0]
        );
        $atexoInscrit->sendCreationEmail($inscrit, $creation['entreprise']);
        $atexoInscrit->sendCreationEmailForAdmins($inscrit, $creation['entreprise']);
        if ($atexoInscrit->isValidationRequiredForRegistration()) {
            return $this->json(['success' => 'success'], 201);
        }
        return $atexoInscrit->logIn($request, $inscrit);
    }

    #[Route(path: '/url-de-vie', name: 'supervisor-home')]
    public function zabbixAction()
    {
        $facade = $this->get('atexo_superviseur_services.facade');

        $result = $facade->getResult();
        $json = new JsonResponse($result);
        return $json;
    }

    /**
     * @param int $id ID de la consultation
     *
     * @return Response
     */
    #[Route(path: '/consultation/{id}', requirements: ['id' => '\d+'], name: 'atexo_consultation_url_rewriting')]
    public function urlConsultationRewritingAction(Request $request, $id)
    {
        $query = array_merge(['id' => $id], $request->query->all());
        return $this->redirectToRoute('atexo_consultation_index', $query);
    }

    /**
     * @param int $consultationId ID de la consultation
     *
     * @return Response
     */
    #[Route(path: '/messagerie-entreprise/visualisation/{consultationId}', requirements: ['id' => '\d+'], name: 'messagerie_entreprise_visualisation_url_rewriting')]
    public function urlMessagerieVisualisationRewritingAction(Request $request, $consultationId)
    {
        $query = array_merge(['consultationId' => $consultationId], $request->query->all());
        return $this->redirectToRoute('messagerie_entreprise_visualisation_message', $query, 302);
    }

    #[Route(path: '/entrepriseAPI/{siren}/{siret}', methods: ['GET'], name: 'entreprise-api', requirements: ['siren' => '\d{9}', 'siret' => '\d{5}'])]
    public function getEntrepriseByAPI(Request $request, EntrepriseAPIService $entrepriseAPIService, int $siren, int $siret = null) : Response
    {
        $entreprise = $entrepriseAPIService->getEntreprise($siren, $siret);
        return $this->json(['entreprise' => $entreprise]);
    }

    #[Route(path: '/entreprise/download/societes-exclues', name: 'download_societes_exclues')]
    public function downloadSocietesExclues(SocietesExcluesService $societesExcluesService) : BinaryFileResponse|RedirectResponse
    {
        $file = $societesExcluesService->getFileSocietesExclues();
        if ($file) {
            return $this->file(
                $file['dirname'].'/'.$file['basename'],
                $file['filename'].ExcelService::EXCEL_EXTENSION,
                ResponseHeaderBag::DISPOSITION_INLINE
            );
        }
        return $this->redirectToRoute('accueil_entreprise');
    }
}
