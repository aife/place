<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise;

use App\Service\CurrentUser;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Exception;
use App\Entity\TRoleJuridique;
use App\Service\AtexoConfiguration;
use App\Entity\TTypeGroupementEntreprise;
use DateTime;
use App\Controller\AtexoController;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\TCandidature;
use App\Entity\TDumeContexte;
use App\Entity\TDumeNumero;
use App\Entity\TGroupementEntreprise;
use App\Entity\TMembreGroupementEntreprise;
use App\Service\AtexoDumeService;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoGroupement;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class GroupementController.
 **/
#[Route(path: '/entreprise')]
class GroupementController extends AtexoController
{
    private bool $shouldRemoveGroupement = false;

    public function __construct(
        private SessionInterface $session,
        private TranslatorInterface $translator,
        private AtexoDumeService $atexoDumeService,
        private AtexoGroupement $atexoGroupement,
        private AtexoConfiguration $atexoConfiguration,
        private AtexoEntreprise $atexoEntreprise,
        private AtexoEtablissement $atexoEtablissement,
        private CurrentUser $currentUser,
        private ParameterBagInterface $parameterBag
    ) {
    }

    /**
     * @throws Exception
     */
    #[Route(path: '/test/groupement/{uid}', name: 'atexo_groupement_afficher_groupement')]
    public function afficherAction($uid, Request $request): Response
    {
        $params = $this->getParamsConfig();
        $session = $request->getSession();
        $session->set($session->get('token_contexte_authentification') . "/$uid/groupement", null);
        $groupement = null;
        $params['groupement'] = $groupement;

        return $this->render('groupement/index.html.twig', $params);
    }

    /**
     * Permet d'ajouter un co-traitant.
     *
     * @throws Exception
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since develop
     *
     * @copyright Atexo 2016*/
    #[Route(path: '/groupement/ajout/coTraitant', options: ['expose' => true], name: 'atexo_groupement_ajout_cotraitant')]
    public function ajouterCoTraitantAction(Request $request): Response
    {
        $params = $this->getParamsConfig();
        $uidResponse = $request->get('uid_response');
        $consultationId = $request->get('refConsultation');

        $em = $this->getDoctrine()->getManager();
        $groupementService = $this->atexoGroupement;
        $infoGroupement =
            $groupementService->getGroupement($this->session->get('contexte_authentification_' . $consultationId));
        $groupement = $infoGroupement['groupement'];
        $candidature = $infoGroupement['candidature'];

        $membreGroupement = $em->getRepository(TMembreGroupementEntreprise::class)
            ->findOneBy([
                'idGroupementEntreprise' => $groupement->getIdGroupementEntreprise(),
            ]);

        if (!$membreGroupement instanceof TMembreGroupementEntreprise) {
            $idEntreprise = $this->currentUser->getIdEntreprise();
            $entreprise = $this->getDoctrine()
                ->getRepository(Entreprise::class)->find($idEntreprise);

            $idEtablissement = $this->currentUser->getIdEtablissement();
            $etablissement =
                $this->getDoctrine()
                    ->getRepository(Etablissement::class)->find($idEtablissement);

            $membreMandataire = new TMembreGroupementEntreprise();
            $membreMandataire->setIdEntreprise($idEntreprise);
            $membreMandataire->setEntreprise($entreprise);
            $membreMandataire->setIdEtablissement($idEtablissement);
            $membreMandataire->setEtablissement($etablissement);
            $membreMandataire->setIdGroupementEntreprise($groupement->getIdGroupementEntreprise());
            $membreMandataire->setGroupement($groupement);

            $idRoleJurdiqueMandataire = $this->parameterBag->get('ID_ROLE_JURIDIQUE_MANDATAIRE');
            $role = $em->getReference(TRoleJuridique::class, $idRoleJurdiqueMandataire);
            $membreMandataire->setIdRoleJuridique($role);

            $em->persist($membreMandataire);

            $groupement->addMembresGroupement($membreMandataire);

            $em->persist($groupement);
            $em->flush();
        }

        if ($groupement instanceof TGroupementEntreprise) {
            $siren = $request->get('siren');
            $siret = $request->get('siret');
            $pays = $request->get('pays');
            $sirenEtranger = $request->get('sirenEtranger');

            $idRoleJurdiqueCoTraitement = $this->parameterBag->get('ID_ROLE_JURIDIQUE_CO_TRAITANT');
            $msgErreur = 'ENTREPRISE_N_EXISTE_PAS';

            if (!empty($siren) && !empty($siret)) {
                $msgErreur = $this->ajouterMembreAuGroupement($groupement, $idRoleJurdiqueCoTraitement, $siren, $siret);
            } elseif (!empty($sirenEtranger) && !empty($pays)) {
                $msgErreur = $this->ajouterMembreAuGroupement(
                    $groupement,
                    $idRoleJurdiqueCoTraitement,
                    '',
                    '',
                    null,
                    [
                    'country' => $pays,
                    'siren' => $sirenEtranger,
                    ]
                );
            }

            if ('' === $msgErreur) {
                $dataGroupement = $this->prepareGroupementData($groupement);

                $this->session->set($this->session
                        ->get('token_contexte_authentification_' . $consultationId) . "/$uidResponse/groupement", $groupement);

                $params['groupement'] = $dataGroupement;
                $params['candidature'] = $candidature;
                $view = $this->renderView('groupement/groupement-tableau.html.twig', $params);

                return new Response(json_encode(['div' => 'layerRepeaterGrouepement', 'view' => $view, 'code' => '200'], JSON_THROW_ON_ERROR));
            } else {
                // Afficher Erreur
                $params['message'] = $this->translator->trans($msgErreur);
                $view = $this->renderView('groupement/groupement-erreur.html.twig', $params);

                return new Response(json_encode(['div' => 'panelMessage', 'view' => $view, 'code' => '500'], JSON_THROW_ON_ERROR));
            }
        }

        return new Response();
    }

    /**
     * @param $idRoleJuridique
     * @param array $foreignCountry
     *
     * @return string
     */
    public function ajouterMembreAuGroupement(
        TGroupementEntreprise $groupement,
        $idRoleJuridique,
        string $siren,
        string $siret,
        int $idEntrepriseParent = null,
        $foreignCountry = []
    ) {
        $em = $this->getDoctrine()->getManager();
        $msgErreur = '';
        $membreExiste = false;

        if (0 == count($foreignCountry)) {
            $entreprise = $this->getDoctrine()->getRepository(Entreprise::class)
            ->getEntrepriseBySiren($siren);
        } else {
            $entreprise = $this->getDoctrine()->getRepository(Entreprise::class)
                ->getEntrepriseBySirenEtrangerAndCountry($foreignCountry);
        }

        if ($entreprise instanceof Entreprise) {
            $membreExiste = $em->getRepository(TMembreGroupementEntreprise::class)
                ->isMembreGroupementEntreprise($groupement, $entreprise->getId());
        } else {
            $entreprise = new Entreprise();
            $entreprise->setSiren($siren);
        }

        if (!$membreExiste) {
            $coTraitant = $this->getCoSousTraitant($idRoleJuridique, $entreprise, $siret, $groupement);

            if ($coTraitant instanceof TMembreGroupementEntreprise) {
                if ($idRoleJuridique == $this->parameterBag->get('ID_ROLE_JURIDIQUE_CO_TRAITANT')) {
                    $groupement->addMembresGroupement($coTraitant);
                } elseif ($idRoleJuridique == $this->parameterBag->get('ID_ROLE_JURIDIQUE_SOUS_TRAITANT')) {
                    $membreParent = $em->getRepository(TMembreGroupementEntreprise::class)
                        ->findOneBy([
                            'idGroupementEntreprise' => $groupement->getIdGroupementEntreprise(),
                            'idEntreprise' => $idEntrepriseParent,
                        ]);

                    if ($membreParent instanceof TMembreGroupementEntreprise) {
                        $coTraitant->setIdMembreParent($membreParent);
                    }
                }

                $em->persist($coTraitant);
                $em->persist($groupement);
                $em->flush();
            } elseif (!($entreprise instanceof Entreprise && $entreprise->getId())) {
                $msgErreur = 'ENTREPRISE_N_EXISTE_PAS';
            }
        } else {
            $msgErreur = 'SIRET_EXISTE_DEJA';
        }

        return $msgErreur;
    }

    /**
     * @param $idRoleJuridique
     * @param $siret
     * @param $groupement
     *
     * @return TMembreGroupementEntreprise|null
     */
    public function getCoSousTraitant(
        $idRoleJuridique,
        Entreprise &$entreprise,
        $siret,
        $groupement
    ) {
        $etablissement = null;
        $membreGroupement = null;

        if (!$entreprise->getId()) {
            $configSgmap = $this->atexoConfiguration->hasConfigPlateforme('synchronisationSgmap');
            $configSyncroCreation = $this->parameterBag->get('ACTIVER_SYNCHRONISATION_SGMAP_LORS_DE_CREATION_ENTREPRISE');

            if ($configSgmap && $configSyncroCreation) {
                $atexoEntreprise = $this->atexoEntreprise;
                $entreprise = $atexoEntreprise->synchroEntrepriseAvecApiGouvEntreprise($entreprise->getSiren());

                if ($entreprise instanceof Entreprise) {
                    if ($entreprise->getNicsiege() != $siret) {
                        $atexoEtablissement = $this->atexoEtablissement;
                        $etablissement =
                            $atexoEtablissement->synchroEtablissementAvecApiGouvEntreprise($entreprise->getSiren() . $siret);

                        if ($etablissement instanceof Etablissement) {
                            $entreprise->addEtablissement($etablissement);
                        }
                    }
                }
            }
        }

        if ($entreprise instanceof Entreprise && $entreprise->getId()) {
            if (!$etablissement instanceof Etablissement) {
                $etablissement = $this->getDoctrine()
                    ->getRepository(Etablissement::class)
                    ->getEtablissementByCodeAndIdEntreprise($siret, $entreprise->getId());

                if (!$etablissement instanceof Etablissement) {
                    $etablissement = $this->getDoctrine()
                        ->getRepository(Etablissement::class)
                        ->getEtablissementSiegeByIdEntreprise($entreprise->getId());
                }
            }

            $membreGroupement = new TMembreGroupementEntreprise();
            $membreGroupement->setIdEntreprise($entreprise->getId());
            $membreGroupement->setEntreprise($entreprise);

            if ($etablissement instanceof Etablissement) {
                $membreGroupement->setIdEtablissement($etablissement->getIdEtablissement());
                $membreGroupement->setEtablissement($etablissement);
            }

            $role = $this->getDoctrine()->getManager()->getReference(TRoleJuridique::class, $idRoleJuridique);
            $membreGroupement->setIdRoleJuridique($role);
            $membreGroupement->setIdGroupementEntreprise($groupement->getIdGroupementEntreprise());
            $membreGroupement->setGroupement($groupement);
        }

        return $membreGroupement;
    }

    /**
     * Permet d'ajouter un sous-traitant.
     *
     * @throws Exception
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since develop
     *
     * @copyright Atexo 2016*/
    #[Route(path: '/groupement/ajout/sousTraitant', options: ['expose' => true], name: 'atexo_groupement_ajouter_soustraitant')]
    public function ajouterSousTraitantAction(Request $request): Response
    {
        $params = $this->getParamsConfig();

        $uidResponse = $request->get('uid_response');
        $consultationId = $request->get('refConsultation');
        $idEntrepriseParent = $request->get('idParent');
        $siren = $request->get('siren');
        $siret = $request->get('siret');
        $groupementService = $this->atexoGroupement;
        $foreignCountry = [];
        if ($request->get('country')) {
            $foreignCountry = [
                'country' => $request->get('country'),
                'siren' => $request->get('siren'),
            ];
        }
        $infoGroupement =
            $groupementService
                ->getGroupement($this->session->get('contexte_authentification_' . $consultationId));
        $groupement = $infoGroupement['groupement'];
        $candidature = $infoGroupement['candidature'];

        if ($groupement instanceof TGroupementEntreprise) {
            if ($idEntrepriseParent) {
                $idRoleJuridique = $this->parameterBag->get('ID_ROLE_JURIDIQUE_SOUS_TRAITANT');
                $msgErreur = $this->ajouterMembreAuGroupement(
                    $groupement,
                    $idRoleJuridique,
                    $siren,
                    $siret,
                    $idEntrepriseParent,
                    $foreignCountry
                );
                if ('' === $msgErreur) {
                    //todo Voir si c'est utile, j'avais des erreurs lors de son utilisation
                    //$this->supprimerSousTraitant($groupement->getMembreGroupementByKey($idEntrepriseParent), $idEntreprise);

                    $dataGroupement = $this->prepareGroupementData($groupement);
                    $this->session->set(
                        $this->session
                            ->get('token_contexte_authentification_' . $consultationId) . "/$uidResponse/groupement",
                        $groupement
                    );
                    $params['groupement'] = $dataGroupement;
                    $params['candidature'] = $candidature;
                    $view = $this->renderView('groupement/groupement-tableau.html.twig', $params);

                    return new Response(
                        json_encode(['div' => 'layerRepeaterGrouepement', 'view' => $view, 'code' => '200'], JSON_THROW_ON_ERROR)
                    );
                } else {
                    // Afficher Erreur
                    $params['message'] = $this->translator->trans($msgErreur);
                    $view = $this->renderView('groupement/groupement-erreur.html.twig', $params);

                    return new Response(
                        json_encode(['div' => 'panelMessage', 'view' => $view, 'code' => '500'], JSON_THROW_ON_ERROR)
                    );
                }
            }
        }

        return new Response();
    }

    /**
     * Permet de supprimer un sous traitant.
     *
     * @param $idEntrepriseSousTraitant
     * @return void
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2016-develop
     * @copyright Atexo 2016*/
    public function supprimerSousTraitant(TMembreGroupementEntreprise $membreParent, $idEntrepriseSousTraitant)
    {
        $em = $this->getDoctrine()->getManager();

        $sousTraitant = $em->getRepository(TMembreGroupementEntreprise::class)
            ->findOneBy([
                'idMembreParent' => $membreParent->getIdMembreGroupementEntreprise(),
                'idEntreprise' => $idEntrepriseSousTraitant,
            ]);

        if ($sousTraitant instanceof TMembreGroupementEntreprise) {
            $em->remove($sousTraitant);
            $em->flush();
        }
    }

    /**
     * Permet de supprimer un sous traitant.
     *
     * @param $idEntrepriseCoTraitant
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function supprimerCoTraitant(TGroupementEntreprise $groupement, $idEntrepriseCoTraitant)
    {
        $em = $this->getDoctrine()->getManager();

        $membreParent = $em->getRepository(TMembreGroupementEntreprise::class)
            ->findOneBy([
                'idGroupementEntreprise' => $groupement->getIdGroupementEntreprise(),
                'idEntreprise' => $idEntrepriseCoTraitant,
            ]);

        $sousTraitants = $em->getRepository(TMembreGroupementEntreprise::class)
            ->findBy([
                'idGroupementEntreprise' => $groupement->getIdGroupementEntreprise(),
                'idMembreParent' => $membreParent->getIdMembreGroupementEntreprise(),
            ]);

        foreach ($sousTraitants as $sousTraitant) {
            $em->remove($sousTraitant);
            $em->flush();
        }

        $em->remove($membreParent);
        $em->flush();
    }

    /**
     * Permet de supprimer un membre.
     *
     * @throws Exception
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016*/
    #[Route(path: '/groupement/supprimer/membre', options: ['expose' => true], name: 'atexo_groupement_supprimer_membre')]
    public function deleteMenbre(Request $request): Response
    {
        $params = $this->getParamsConfig();

        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();

        $uidResponse = $request->get('uidResponse');
        $idEntreprise = $request->get('id');
        $consultationId = $request->get('refConsultation');
        //MALIK
        $type = $request->get('type');
        $groupementService = $this->atexoGroupement;
        $infoGroupement =
            $groupementService
                ->getGroupement($this->session->get('contexte_authentification_' . $consultationId));
        $groupement = $infoGroupement['groupement'];
        $candidature = $infoGroupement['candidature'];

        if ($groupement instanceof TGroupementEntreprise) {
            if ('cotraitant' == $type) {
                $this->supprimerCoTraitant($groupement, $idEntreprise);
            } elseif ('soustraitant' == $type) {
                $idParent = $request->get('idParent');
                $membre = $em->getRepository(TMembreGroupementEntreprise::class)
                    ->findOneBy([
                        'idGroupementEntreprise' => $groupement->getIdGroupementEntreprise(),
                        'idEntreprise' => $idParent,
                    ]);

                if ($membre instanceof TMembreGroupementEntreprise) {
                    $this->supprimerSousTraitant($membre, $idEntreprise);
                }
            }
        }

        $dataGroupement = $this->prepareGroupementData($groupement);
        $this->session->set($this->session
                ->get('token_contexte_authentification_' . $consultationId) . "/$uidResponse/groupement", $groupement);
        $params['groupement'] = $dataGroupement;
        $params['candidature'] = $candidature;
        $view = $this->renderView('groupement/groupement-tableau.html.twig', $params);

        return new Response(json_encode(
            [
            'div' => 'layerRepeaterGrouepement',
            'view' => $view,
            'cacheDiv' => 'panelMessage',
            ],
            JSON_THROW_ON_ERROR
        ));
    }

    /**
     * Permet d'initialiser les paramettres du twig.
     *
     * @return array paramettre
     *
     * @throws Exception
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016*/
    public function getParamsConfig()
    {
        $params = [];
        $params['idTypeGroupementSolidaire'] = $this->parameterBag->get('ID_TYPE_GROUPEMENT_SOLIDAIRE');
        $params['idTypeGroupementConjointNonSolidaire'] =
            $this->parameterBag->get('ID_TYPE_GROUPEMENT_CONJOINT_NON_SOLIDAIRE');
        $params['idTypeGroupementConjointSolidaire'] =
            $this->parameterBag->get('ID_TYPE_GROUPEMENT_CONJOINT_NON_SOLIDAIRE');
        $params['idRoleJuridiqueMandataire'] = $this->parameterBag->get('ID_ROLE_JURIDIQUE_MANDATAIRE');
        $params['groupement'] = null;

        return $params;
    }

    /**
     * Permet de type du groupement.
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016*/
    #[Route(path: '/groupement/changeType', options: ['expose' => true], name: 'atexo_groupement_change_type')]
    public function updateTypeGroupement(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();

        $idType = $request->get('idTypeGroupement');
        $uidResponse = $request->get('uidResponse');
        $consultationId = $request->get('refConsultation');

        try {
            $this->session->set(
                $this->session
                    ->get('token_contexte_authentification_' . $consultationId) . "/$uidResponse/idTypeGroupement",
                $idType
            );
        } catch (Exception) {
        }
        $groupementService = $this->atexoGroupement;
        $groupement =
            $groupementService
                ->getGroupement($this->session->get('contexte_authentification_' . $consultationId))['groupement'];
        if ($groupement instanceof TGroupementEntreprise) {
            $type = $this->getDoctrine()->getManager()->getReference(TTypeGroupementEntreprise::class, $idType);
            $groupement->setIdTypeGroupement($type);

            $em->persist($groupement);
            $em->flush();
        }

        try {
            $this->session->set(
                $this->session->get('token_contexte_authentification_' . $consultationId) .
                "/$uidResponse/groupement",
                $groupement
            );
        } catch (Exception) {
        }

        return new Response(json_encode(['div' => '', 'view' => ''], JSON_THROW_ON_ERROR));
    }

    /**
     * Permet d'enregister le choix au niveau de la session.
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since develop
     *
     * @copyright Atexo 2016*/
    #[Route(path: '/groupement/declarer', options: ['expose' => true], name: 'atexo_groupement_declarer')]
    public function declarerGroupementAction(Request $request): Response
    {
        $session = $request->getSession();
        $declarer = $request->get('declarerGroupement');
        $consultationId = $request->get('refConsultation');
        $uidResponse = $request->get('uid_response');
        $session->set(
            $session->get('token_contexte_authentification_' . $consultationId) . "/$uidResponse/declarerGroupement",
            $declarer
        );

        return new Response($declarer);
    }

    /**
     * Permet d'enregister le choix au niveau de la session.
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since develop
     *
     * @copyright Atexo 2016*/
    #[Route(path: '/groupement/creerGroupement', options: ['expose' => true], name: 'atexo_groupement_oui')]
    public function creerGroupementAction(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $groupementOui = $request->get('groupementOui');
        $uidResponse = $request->get('uid_response');
        $consultationId = $request->get('refConsultation');

        try {
            $this->session->set(
                $this->session
                    ->get('token_contexte_authentification_' . $consultationId) . "/$uidResponse/groupementOui",
                $groupementOui
            );
        } catch (Exception) {
        }
        $groupementService = $this->atexoGroupement;
        $candidature = $groupementService->getCandidature($this->session
            ->get('contexte_authentification_' . $consultationId));

        //création du groupement en DB avec son role solidaire par défaut
        $groupement = new TGroupementEntreprise();

        $type = $em->getReference(
            TTypeGroupementEntreprise::class,
            $this->parameterBag->get('ID_TYPE_GROUPEMENT_SOLIDAIRE')
        );

        $groupement->setIdTypeGroupement($type);
        $groupement->setCandidature($candidature);
        $groupement->setDateCreation(new DateTime());
        $em->persist($groupement);
        $em->flush();

        return new Response($groupement->getIdGroupementEntreprise());
    }

    /**
     * Vérifie que la référence DUME existe bien et corresponde à la bonne entreprise.
     **/
    #[Route(path: '/groupement/validerReferenceDume', options: ['expose' => true], name: 'atexo_groupement_valider_reference_dume')]
    public function validerRefDumeAction(Request $request): Response
    {
        try {
            $refDume = $request->get('refDume');
            $idMembreGroupement = $request->get('idMembreGroupement');

            $dumeNumero = $this->getDoctrine()
                ->getRepository(TDumeNumero::class)
                ->findOneBy([
                    'numeroDumeNational' => $refDume,
                ]);

            if (null == $dumeNumero) {
                throw new Exception($this->translator->trans('MSG_ERREUR_REFERENCE_DUME_INVALIDE'));
            }

            $dumeContexte = $this->getDoctrine()
                ->getRepository(TDumeContexte::class)
                ->find($dumeNumero->getIdDumeContexte());

            if (
                null == $dumeContexte ||
                (
                    null != $dumeContexte &&
                    (
                        $dumeContexte->getTypeDume() != $this->parameterBag->get('TYPE_DUME_OE') ||
                        $dumeContexte->getStatus() != $this->parameterBag->get('STATUT_DUME_CONTEXTE_VALIDE')
                    )
                )
            ) {
                throw new Exception($this->translator->trans('MSG_ERREUR_REFERENCE_DUME_DEJA_UTILISEE'));
            }

            $candidature = $this->getDoctrine()
                ->getRepository(TCandidature::class)
                ->findOneBy([
                    'consultation' => $dumeContexte->getConsultation(),
                    'organisme' => $dumeContexte->getOrganisme(),
                    'status' => $dumeContexte->getStatus(),
                    'idDumeContexte' => $dumeContexte->getId(),
                ]);

            $tMembreGroupementEntreprise = $this->getDoctrine()
                ->getRepository(TMembreGroupementEntreprise::class)
                ->findOneBy(['idMembreGroupementEntreprise' => $idMembreGroupement]);

            if (null == $candidature || null == $tMembreGroupementEntreprise) {
                throw new Exception($this->translator->trans('MSG_ERREUR_REFERENCE_DUME_INVALIDE'));
            }

            if ($candidature->getIdEntreprise() != $tMembreGroupementEntreprise->getIdEntreprise()) {
                throw new Exception($this->translator->trans('MSG_ERREUR_REFERENCE_DUME_INVALIDE'));
            }

            $clientDume = $this->atexoDumeService
                ->createConnexionLtDume();

            $validateDume = $clientDume->entrepriseService()->checkAccess(
                $candidature->getIdDumeContexte()->getContexteLtDumeId()
            );

            if ($validateDume->getStatut() != $this->parameterBag->get('STATUT_VALIDATION_DUME_OK')) {
                throw new Exception($this->translator->trans('MSG_ERREUR_REFERENCE_DUME_INVALIDE'));
            }

            $membreGroupement = $this->getDoctrine()
                ->getRepository(TMembreGroupementEntreprise::class)
                ->findOneBy([
                    'idMembreGroupementEntreprise' => $idMembreGroupement,
                ]);

            $membreGroupement->setNumeroSN($refDume);
            $candidature->setStatus($this->parameterBag->get('STATUT_DUME_CONTEXTE_VALIDE'));
            $candidature->setDateDerniereValidationDume(new DateTime());
            $dumeContexte->setStatus($this->parameterBag->get('STATUT_DUME_CONTEXTE_VALIDE'));

            $em = $this->getDoctrine()->getManager();

            $em->persist($membreGroupement);
            $em->persist($candidature);
            $em->persist($dumeContexte);
            $em->flush();

            $view = $this->getRenderedView($membreGroupement->getIdGroupementEntreprise());

            return new Response(
                json_encode([
                    'div' => 'layerRepeaterGrouepement',
                    'view' => $view,
                    'cacheDiv' => 'panelMessage',
                    'message' => $this->translator->trans('MSG_SUCCESS_REFERENCE_DUME_ENREGISTREE'),
                ], JSON_THROW_ON_ERROR)
            );
        } catch (Exception $e) {
            return new Response(
                json_encode([
                    'message' => $e->getMessage(),
                    'error' => 1,
                ], JSON_THROW_ON_ERROR)
            );
        }
    }

    /**
     * Permet de préparer les données avant de les envoyer à la vue afin de construire le tableau des groupements.
     *
     * @param $groupement
     *
     * @return array
     */
    public function prepareGroupementData($groupement)
    {
        $dataGroupement = [];
        $idRoleJuridiqueSousTraitant = $this->parameterBag->get('ID_ROLE_JURIDIQUE_SOUS_TRAITANT');
        $dataGroupement['groupement'] = $groupement;

        if (null !== $groupement) {
            $em = $this->getDoctrine()->getManager();

            foreach ($groupement->getMembresGroupement() as $key => $membre) {
                if ($membre->getIdRoleJuridique()->getIdRoleJuridique() != $idRoleJuridiqueSousTraitant) {
                    $dataGroupement['membres'][$key] = $em->getRepository(TMembreGroupementEntreprise::class)
                        ->findBy([
                            'idMembreGroupementEntreprise' => $membre->getIdMembreGroupementEntreprise(),
                        ]);

                    $dataGroupement['membres'][$key]['sousMembres'] = $em->getRepository(TMembreGroupementEntreprise::class)
                        ->findBy([
                            'idMembreParent' => $membre->getIdMembreGroupementEntreprise(),
                        ]);
                }
            }
        }

        return $dataGroupement;
    }

    /**
     * Suppression du groupement et de ses membres.
     **/
    #[Route(path: '/groupement/supprimerGroupement', options: ['expose' => true], name: 'atexo_groupement_supprimer_groupement')]
    public function supprimerGroupementAction(Request $request): Response
    {
        $this->shouldRemoveGroupement = true;

        return $this->supprimerMembresGroupementAction($request);
    }

    /**
     * Suppression des membres du groupement.
     **/
    #[Route(path: '/groupement/supprimerMembresGroupement', options: ['expose' => true], name: 'atexo_groupement_supprimer_membres_groupement')]
    public function supprimerMembresGroupementAction(Request $request): Response
    {
        try {
            $idGroupementEntreprise = $request->get('idGroupementEntreprise');

            $groupementService = $this->atexoGroupement;

            $groupementService->supprimerMembresGroupement($idGroupementEntreprise);

            if ($this->shouldRemoveGroupement) {
                $groupementService->supprimerGroupement($idGroupementEntreprise);
            }

            $view = $this->getRenderedView($idGroupementEntreprise);

            return new Response(
                json_encode([
                    'div' => 'layerRepeaterGrouepement',
                    'view' => $view,
                    'cacheDiv' => 'panelMessage',
                    'message' => $this->translator->trans('MSG_SUCCESS_REFERENCE_DUME_ENREGISTREE'),
                ], JSON_THROW_ON_ERROR)
            );
        } catch (Exception $e) {
            return new Response(
                json_encode([
                    'message' => $e->getMessage(),
                    'error' => 1,
                ], JSON_THROW_ON_ERROR)
            );
        }
    }

    /**
     * @param $idGroupementEntreprise
     *
     * @return string
     */
    private function getRenderedView($idGroupementEntreprise)
    {
        $params = [];
        $groupement = $this->getDoctrine()
            ->getRepository(TGroupementEntreprise::class)
            ->findOneBy([
                'idGroupementEntreprise' => $idGroupementEntreprise,
            ]);

        $dataGroupement = $this->prepareGroupementData($groupement);

        $params['groupement'] = $dataGroupement;
        $params['idRoleJuridiqueMandataire'] = $this->parameterBag->get('ID_ROLE_JURIDIQUE_MANDATAIRE');
        $params['idTypeGroupementSolidaire'] = $this->parameterBag->get('ID_TYPE_GROUPEMENT_SOLIDAIRE');
        $params['idTypeGroupementConjointNonSolidaire'] =
            $this->parameterBag->get('ID_TYPE_GROUPEMENT_CONJOINT_NON_SOLIDAIRE');
        $params['idTypeGroupementConjointSolidaire'] =
            $this->parameterBag->get('ID_TYPE_GROUPEMENT_CONJOINT_SOLIDAIRE');

        return $this->renderView('groupement/groupement-tableau.html.twig', $params);
    }
}
