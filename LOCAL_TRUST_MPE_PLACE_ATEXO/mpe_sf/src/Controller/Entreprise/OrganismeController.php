<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Entreprise;

use App\Service\App\GraphicChart;
use App\Service\OrganismeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class OrganismeController extends AbstractController
{

    /**
     * @param Request $request
     * @param OrganismeService $organismeService
     * @param $organismAcronym
     */
    #[Route(path: 'bascule-organisme/{organismAcronym}', name: 'switcher-organism', requirements: ['organismAcronym' => '[a-zA-Z0-9_-]+'])]
    public function switchOrganism(Request $request, OrganismeService $organismeService, $organismAcronym) : RedirectResponse
    {
        $redirectUrl = $request->headers->get('referer', $this->generateUrl('accueil_entreprise'));
        $response = $this->redirect($redirectUrl);

        if ($organismeService->exists($organismAcronym)) {
            $response->headers->clearCookie(GraphicChart::NO_DESIGN_COOKIE);
            $response->headers->setCookie(Cookie::create(GraphicChart::DESIGN_COOKIE, $organismAcronym));
            $response->headers->setCookie(Cookie::create('selectedorg', $organismAcronym));
        }

        return $response;
    }
}
