<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Spaser;

use App\Entity\Consultation;
use App\Entity\ContratTitulaire;
use App\Service\Spaser\WebServicesSpaser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: 'agent/spaser/objet-metier', name: 'agent_spaser_objet_metier_')]
class SpaserController extends AbstractController
{
    public function __construct(private readonly WebServicesSpaser $webServicesSpaser)
    {
    }

    #[Route(path: '/consultation/{id}', name: 'consultation', methods: ['GET'])]
    public function consultation(Consultation $consultation): RedirectResponse
    {
        return $this->redirect(
            $this->webServicesSpaser->getContent(
                'getSpaserAuthUrl',
                $this->getUser(),
                WebServicesSpaser::TYPE_ROUTE_SAISIE_INDICATEUR,
                $consultation
            )
        );
    }

    #[Route(path: '/contrat/{id}', name: 'contrat', methods: ['GET'])]
    public function contrat(ContratTitulaire $contratTitulaire): RedirectResponse
    {
        return $this->redirect(
            $this->webServicesSpaser->getContent(
                'getSpaserAuthUrl',
                $this->getUser(),
                WebServicesSpaser::TYPE_ROUTE_SAISIE_INDICATEUR,
                $contratTitulaire
            )
        );
    }

    #[Route(path: '/transverse', name: 'transverse', methods: ['GET'])]
    public function transverse(): RedirectResponse
    {
        return $this->redirect(
            $this->webServicesSpaser->getContent(
                'getSpaserAuthUrl',
                $this->getUser(),
                WebServicesSpaser::TYPE_ROUTE_SAISIE_INDICATEUR_TRANSVERSE
            ),
        );
    }

    #[Route(path: '/dashboard', name: 'dashboard', methods: ['GET'])]
    public function dashboard(): RedirectResponse
    {
        return $this->redirect(
            $this->webServicesSpaser->getContent(
                'getSpaserAuthUrl',
                $this->getUser(),
                WebServicesSpaser::TYPE_ROUTE_DASHBOARD
            )
        );
    }

    #[Route(path: '/admin-spaser', name: 'admin_spaser', methods: ['GET'])]
    public function adminSpaser(): RedirectResponse
    {
        return $this->redirect(
            $this->webServicesSpaser->getContent(
                'getSpaserAuthUrl',
                $this->getUser(),
                WebServicesSpaser::TYPE_ROUTE_ADMIN_SPASER
            )
        );
    }
}
