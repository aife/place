<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller;

use App\Entity\ModuleSatelliteMonitoring;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MonitoringController extends AbstractController
{
    #[Route(path: '/healthcheck', name: 'services_healthcheck')]
    public function index(EntityManagerInterface $em)
    {
        $services = $em->getRepository(ModuleSatelliteMonitoring::class)->findAll();
        $result = [];
        foreach ($services as $service) {
            $info = $service->getJsonResponse();
            (key($info) === 'externalModule') ?
                $result[key($info)][] = $info[key($info)] :
                $result[key($info)] = $info[key($info)];
        }

        return $this->json($result);
    }
}
