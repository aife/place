<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller;

use App\Entity\ConfigurationPlateforme;
use App\Security\CertificateAuthenticator;
use App\Security\ExternalSsoAuthenticator;
use App\Security\GuardManager;
use App\Security\OpenidKeycloakAuthenticator;
use App\Security\OpenidMicrosoftAuthenticator;
use App\Security\ShibbolethAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use App\Entity\MessageAccueil;
use App\Repository\MessageAccueilRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class SecurityController extends AtexoController
{
    public function __construct(
        private readonly AuthorizationCheckerInterface $authorizationChecker,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    /**
     * @author Salwa DRISSI <salwa.drissit@atexo.com>*/
    #[Route(path: '/login', options: ['expose' => true], name: 'mpe_non_auth')]
    public function login(Request $request): Response
    {
        if ($request->isXmlHttpRequest()) {
            return new JsonResponse('', Response::HTTP_UNAUTHORIZED);
        }
        return new RedirectResponse(
            $this->getParameter('URL_MPE') .
            '?page=Entreprise.EntrepriseHome'
        );
    }

    #[Route(path: '/entreprise/login', name: 'mpe_entreprise_auth')]
    public function loginEntreprise(Request $request, GuardManager $guardManager): Response
    {
        $securityContext = $this->authorizationChecker;
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirect('/entreprise');
        }
        if (OpenidKeycloakAuthenticator::class === $guardManager->getMainGuard($request)) {
            return $this->redirectToRoute('mpe_inscrit_auth_keycloak');
        }
        if (ExternalSsoAuthenticator::class === $guardManager->getMainGuard($request)) {
            return $this->redirectToRoute('mpe_inscrit_auth_sso');
        }
        return $this->redirect($request->server->get('HTTP_REFERER', '/entreprise'));
    }

    #[Route(path: '/agent/login', name: 'mpe_agent_auth')]
    public function loginAgent(Request $request, GuardManager $guardManager): Response
    {
        $securityContext = $this->authorizationChecker;
        $configurationPlateforme = $this->entityManager->getRepository(ConfigurationPlateforme::class)
            ->getConfigurationPlateforme();

        if ($securityContext->isGranted('ROLE_ENTREPRISE')) {
            return $this->redirect('/entreprise');
        }
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirect('/?page=Agent.AgentHome');
        }
        if (OpenidKeycloakAuthenticator::class === $guardManager->getMainGuard($request)) {
            return $this->redirectToRoute('mpe_agent_auth_keycloak');
        }
        if (OpenidMicrosoftAuthenticator::class === $guardManager->getMainGuard($request)) {
            return $this->redirectToRoute('mpe_agent_auth_microsoft');
        }
        if (ExternalSsoAuthenticator::class === $guardManager->getMainGuard($request)) {
            return $this->redirectToRoute('mpe_agent_auth_sso');
        }

        if (ShibbolethAuthenticator::class === $guardManager->getMainGuard($request)) {
            return $this->redirectToRoute('mpe_agent_auth_sso_shibboleth');
        }

        $loginTemplate = 'security/login.html.twig';
        $parameters = [
            'message_accueil' => $this->getMessageAccueil(),
        ];
        if ('1' === $configurationPlateforme->getAuthenticateAgentSaml()) {
            $parameters['login_by_saml'] = true;
        } elseif (CertificateAuthenticator::class === $guardManager->getMainGuard($request)) {
            $parameters['login_by_certif'] = true;
        } else {
            $parameters['form'] = $this->getForm()->createView();
        }
        return $this->render($loginTemplate, $parameters);
    }

    #[Route(path: '/admin/login', name: 'mpe_admin_auth')]
    public function loginAdmin(Request $request, ParameterBagInterface $parameterBag): Response
    {
        $securityContext = $this->authorizationChecker;
        if ($securityContext->isGranted('ROLE_ENTREPRISE')) {
            return $this->redirect('/entreprise');
        }
        if ($securityContext->isGranted('ROLE_AGENT')) {
            return $this->redirect('/agent');
        }
        if ($securityContext->isGranted('ROLE_ADMIN')) {
            return $parameterBag->get('GESTION_ORGANISME_PAGE_SYMFONY') ?
                $this->redirectToRoute('admin_gestion_organisme')
                :
                $this->redirect('/?page=Administration.GestionOrganisme');
        }
        if ($securityContext->isGranted('ROLE_SUPER_ADMIN')) {
            return $this->redirectToRoute('superadmin_configsclients_index');
        }
        return $this->render(
            'security/login.html.twig',
            [
                'form' => $this->getForm()->createView(),
            ]
        );
    }

    #[Route(path: '/entreprise/logout', name: 'mpe_logout_entreprise')]
    public function logoutInscrit(Request $request): RedirectResponse
    {
        return $this->redirectToRoute('accueil_entreprise');
    }

    #[Route(path: '/agent/logout', name: 'mpe_logout_agent')]
    public function logoutAgent(Request $request): RedirectResponse
    {
        return $this->redirect($request->server->get('HTTP_REFERER', '/'));
    }

    #[Route(path: '/admin/logout', name: 'mpe_logout_admin')]
    public function logoutAdmin(Request $request): RedirectResponse
    {
        return $this->redirect($request->server->get('HTTP_REFERER', '/'));
    }

    #[Route(path: '/agent/login/microsoft', name: 'mpe_agent_auth_microsoft')]
    public function loginAgentMicrosoft(ClientRegistry $clientRegistry): RedirectResponse
    {
        return $clientRegistry->getClient('microsoft_agent')->redirect(['openid'], []);
    }

    #[Route(path: '/login/microsoft/check', name: 'mpe_auth_microsoft_check')]
    public function loginMicrosoftCheck(ClientRegistry $clientRegistry)
    {
    }

    #[Route(path: '/superadmin/login', name: 'mpe_superadmin_auth', methods: ['GET', 'POST'])]
    public function loginSuperAdminAction(): Response
    {
        $securityContext = $this->authorizationChecker;
        if ($securityContext->isGranted('ROLE_SUPER_ADMIN')) {
            return $this->redirectToRoute('superadmin_configsclients_index');
        }
        return $this->render(
            'security/login.html.twig',
            [
                'form' => $this->getForm()->createView(),
            ]
        );
    }

    #[Route(path: '/agent/login/keycloak', name: 'mpe_agent_auth_keycloak')]
    public function loginAgentKeycloak(ClientRegistry $clientRegistry): RedirectResponse
    {
        return $clientRegistry->getClient('keycloak')->redirect(['openid'], []);
    }

    #[Route(path: '/inscrit/login/keycloak', name: 'mpe_inscrit_auth_keycloak')]
    public function loginInscritKeycloak(ClientRegistry $clientRegistry): RedirectResponse
    {
        return $clientRegistry->getClient('keycloak')->redirect(['openid'], []);
    }

    #[Route(path: '/login/keycloak/check', name: 'mpe_auth_keycloak_check')]
    public function loginKeycloakCheck(ClientRegistry $clientRegistry)
    {
    }

    #[Route(path: '/agent/login/sso', name: 'mpe_agent_auth_sso')]
    public function loginAgentSso(ParameterBagInterface $parameterBag): RedirectResponse
    {
        return $this->redirectToRoute('mpe_auth_sso_check');
    }

    #[Route(path: '/agent/login/sso/shibboleth', name: 'mpe_agent_auth_sso_shibboleth')]
    public function loginAgentSsoShibboleth(ParameterBagInterface $parameterBag): RedirectResponse
    {
        return $this->redirectToRoute('mpe_auth_sso_check_shibboleth');
    }

    #[Route(path: '/inscrit/login/sso', name: 'mpe_inscrit_auth_sso')]
    public function loginInscritSso(ParameterBagInterface $parameterBag): RedirectResponse
    {
        return $this->redirectToRoute('mpe_auth_sso_check');
    }

    #[Route(path: '/login/sso/check', name: 'mpe_auth_sso_check')]
    public function loginSsoCheck(TranslatorInterface $translator)
    {
        // L'action du contrôleur peut être vide : elle ne sera jamais exécutée.
        // Le traitement se fait au niveau du guard ExternalSsoAuthenticator
        $this->addFlash('error', $translator->trans('CONNEXION_IMPOSSIBLE'));
        return new RedirectResponse('/');
    }

    #[Route(path: '/login/sso/shibboleth/check', name: 'mpe_auth_sso_check_shibboleth')]
    public function loginSsoShibbolethCheck(TranslatorInterface $translator)
    {
        // L'action du contrôleur peut être vide : elle ne sera jamais exécutée.
        // Le traitement se fait au niveau du guard ShibbolethAuthenticator
        $this->addFlash('error', $translator->trans('CONNEXION_IMPOSSIBLE'));
        return new RedirectResponse('/');
    }

    protected function getForm(): FormInterface
    {
        return $this->createFormBuilder()
            ->add('_username', TextType::class, ['attr' => ['placeholder' => 'LOGIN']])
            ->add('_password', PasswordType::class, ['attr' => ['placeholder' => 'PASSWORD']])
            ->add('login', SubmitType::class, ['label' => 'TEXT_CONNECTION'])
            ->getForm();
    }

    protected function getMessageAccueil(): ?MessageAccueil
    {
        /** @var MessageAccueilRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository(MessageAccueil::class);
        $messageAccueil = $repo->getMessageAccueilAgentNonAuthentifie();

        return $messageAccueil && $messageAccueil->getContenu() ? $messageAccueil : null;
    }

    #[Route(path: '/agent/login-by-certificate', name: 'login-by-certificate')]
    public function index(Request $request, TranslatorInterface $translator)
    {
        // L'action du contrôleur peut être vide : elle ne sera jamais exécutée.
        // Le traitement se fait au niveau du guard CertificateAuthenticator
        $this->addFlash('error', $translator->trans('AUCUN_CERTIFICAT_TRANSMIS'));
        return new RedirectResponse('/agent');
    }

    #[Route(path: '/saml/error', name: 'saml_error')]
    public function samlError(Request $request, TranslatorInterface $translator, LoggerInterface $logger)
    {
        $session = $request->getSession();
        $authErrorKey = Security::AUTHENTICATION_ERROR;
        if (null !== $session && $session->has($authErrorKey)) {
            $error = $session->get($authErrorKey);
            $logger->error($error->getMessage());

            $session->remove($authErrorKey);
            $this->addFlash('error', $translator->trans('SSO_SAML_ERROR'));
        }
        return new RedirectResponse('/agent/login');
    }
}
