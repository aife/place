<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller;

use App\Controller\AtexoController;
use App\Service\CurrentUser;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Application\Service\Atexo\Atexo_Config;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class OutilsSignature.
 */
class OutilsSignatureController extends AtexoController
{
    public function __construct(
        private TranslatorInterface $translator,
        private ParameterBagInterface $parameterBag,
        private readonly CurrentUser $currentUser
    ) {
    }

    #[Route(path: '/agent/signer-document', name: 'agent_signer_document')]
    #[Route(path: '/entreprise/signer-document', name: 'entreprise_signer_document')]
    public function signerDocumentAction()
    {
        $isAgent = $this->currentUser->isAgent();
        $translator = $this->translator;
        $translations = [
            'en_ligne' => $translator->trans('EN_LIGNE'),
            'hors_ligne' => $translator->trans('HORS_LIGNE'),
            'etat_assistant' => $translator->trans('ETAT_ASSISTANT_MARCHES_PUBLICS'),
            'lance_assistant' => $translator->trans('ASSISTANT_MARCHES_PUBLICS_LANCE'),
            'non_lance_assistant' => $translator->trans('ASSISTANT_MARCHES_PUBLICS_NON_LANCE'),
            'afficher_plus_informations' => $translator->trans('AFFICHER_PLUS_INFORMATIONS'),
        ];

        $urlCentralReferentiel = $this->parameterBag->get('URL_CENTRALE_REFERENTIELS');
        if (Atexo_Config::getParameter('SPECIFIQUE_PLACE')) {
            $urlCentralReferentiel = $this->parameterBag->get('URL_DOCS');
        }

        $urls = [
            'crypto' => $this->parameterBag->get('URL_CRYPTO') . '/signature/',
            'url_central_referentiel' => $urlCentralReferentiel,
            'windows' => $this->parameterBag->get('LIEN_TELECHARGEMENT_CLIENT_LOURD_WINDOWS'),
            'linux' => $this->parameterBag->get('LIEN_TELECHARGEMENT_CLIENT_LOURD_LINUX'),
            'macos' => $this->parameterBag->get('LIEN_TELECHARGEMENT_CLIENT_LOURD_MACOS'),
        ];

        $template = 'outils-signature/signer-document-entreprise.html.twig';

        if ($isAgent) {
            $template = 'outils-signature/signer-document-agent.html.twig';
        }

        return $this->render($template, [
            'path_crypto' => $this->parameterBag->get('URL_CRYPTO'),
            'plateform' => $this->parameterBag->get('UID_PF_MPE'),
            'translations' => base64_encode(json_encode($translations, JSON_THROW_ON_ERROR)),
            'urls' => $urls,
        ]);
    }
}
