<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\ValidationEcoSip;

use App\Service\Agent\ValidationEcoSip;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: 'agent/annonces', name: 'agent_annonces_')]
class ValidationController extends AbstractController
{
    public function __construct(
        private ValidationEcoSip $validationEcoSip
    ) {
    }

    #[Route(path: '/validation-eco', name: 'validation_eco', methods: ['GET'])]
    public function getEco(): Response
    {
        return $this->render(
            'agent/validation-eco-sip/validation.html.twig',
            $this->validationEcoSip->getConnectionParams(
                'agent/annonces/validation-eco',
                $this->getUser(),
                'eco',
            )
        );
    }
    #[Route(path: '/validation-sip', name: 'validation_sip', methods: ['GET'])]
    public function getSip(): Response
    {
        return $this->render(
            'agent/validation-eco-sip/validation.html.twig',
            $this->validationEcoSip->getConnectionParams(
                'agent/annonces/validation-sip',
                $this->getUser(),
                'sip'
            )
        );
    }
}
