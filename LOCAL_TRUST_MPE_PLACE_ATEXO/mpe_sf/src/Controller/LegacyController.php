<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller;

use Prado\TApplication;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;

class LegacyController extends AbstractController
{
    public function __construct(private readonly SessionInterface $session)
    {
    }

    public function loadLegacyScript(): StreamedResponse
    {
        // MPE-17182 : Hack pour avoir le cookie PHPSESSID côté Prado
        $this->session->set('startsession', 'please');

        return new StreamedResponse(
            function () {
                $application = new TApplication('../legacy/protected');
                $application->run();
            }
        );
    }
}
