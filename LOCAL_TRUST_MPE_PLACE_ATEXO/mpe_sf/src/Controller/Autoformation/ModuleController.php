<?php

namespace App\Controller\Autoformation;

use App\Entity\Autoformation\ModuleAutoformation;
use App\Entity\Autoformation\Rubrique;
use App\Service\Autoformation\ModuleAutoformationModule;
use App\Service\Autoformation\ModuleAutoformationRubrique;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route(
    path: '/{_type}/module-autoformation/module',
    name: 'module_autoformation_module_',
    requirements: ["_type" => "agent|entreprise"]
)]
class ModuleController extends AbstractController
{
    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    /**
     * @param Request $request
     * @param ModuleAutoformationModule $moduleAutoformationModule
     * @return JsonResponse
     */
    #[Route(path: '/ajouter/rubrique/{id}', options: ['expose' => true], name: 'create')]
    public function create(
        Rubrique $rubrique,
        Request $request,
        ModuleAutoformationModule $moduleAutoformationModule
    ): JsonResponse {
        $content = $moduleAutoformationModule->create(
            $rubrique,
            $request->request->all()
        );
        $this->addFlash('success', $this->translator->trans('MODULE_CREE'));
        return $this->json(
            $content
        );
    }
    /**
     * @param Request $request
     * @param ModuleAutoformationModule $moduleAutoformationModule
     * @return JsonResponse
     */
    #[Route(path: '/modifier/{id}', options: ['expose' => true], name: 'update')]
    public function update(
        Request $request,
        ModuleAutoformation $module,
        ModuleAutoformationModule $moduleAutoformationModule
    ): JsonResponse {
        return $this->json(
            $moduleAutoformationModule->update(
                $module,
                $request->request->all()
            )
        );
    }

    /**
     * @param ModuleAutoformation $moduleAutoformation
     * @return JsonResponse
     */
    #[Route(path: '/supprimer/{id}', options: ['expose' => true], name: 'remove')]
    public function remove(ModuleAutoformation $moduleAutoformation)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($moduleAutoformation);
        $em->flush();

        $this->addFlash('success', $this->translator->trans('MODULE_SUPPRIME'));

        return $this->json(['success' => true]);
    }
}
