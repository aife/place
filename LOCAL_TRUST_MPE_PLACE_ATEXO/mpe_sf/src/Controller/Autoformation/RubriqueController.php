<?php

namespace App\Controller\Autoformation;

use Exception;
use App\Entity\Autoformation\Rubrique;
use App\Service\Autoformation\ModuleAutoformationRubrique;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route(
    path: '/{_type}/module-autoformation/rubrique',
    name: 'module_autoformation_rubrique_',
    requirements: ["_type" => "agent|entreprise"]
)]
class RubriqueController extends AbstractController
{
    /**
     * @param Request $request
     * @return Response
     */
    #[Route(path: '/', options: ['expose' => true], name: 'entreprise')]
    public function entreprise(Request $request): Response
    {
        $rubriques = $this->getDoctrine()->getRepository(Rubrique::class)
            ->findBy([
                'isAgent' => false
            ]);

        return $this->render('module_autoformation/entreprise.html.twig', [
            'rubriques' => $rubriques,
            'locale' => $request->getLocale(),
            'show_btn' => false,
            'page' => 'entreprise'
        ]);
    }

    /**
     * @param Request $request
     * @param ModuleAutoformationRubrique $moduleAutoformationRubrique
     * @return JsonResponse
     */
    #[Route(path: '/ajouter', options: ['expose' => true], name: 'create')]
    public function create(Request $request, ModuleAutoformationRubrique $moduleAutoformationRubrique): JsonResponse
    {
        return $this->json(
            $moduleAutoformationRubrique->create(
                json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR)
            )
        );
    }

    /**
     * @param Request $request
     * @param Rubrique $rubrique
     * @param ModuleAutoformationRubrique $moduleAutoformationRubrique
     * @return JsonResponse
     * @throws Exception
     */
    #[Route(path: '/modifier/{id}', options: ['expose' => true], name: 'update')]
    public function update(
        Request $request,
        Rubrique $rubrique,
        ModuleAutoformationRubrique $moduleAutoformationRubrique
    ): JsonResponse {
        return $this->json(
            $moduleAutoformationRubrique->update(
                $rubrique,
                json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR)
            )
        );
    }

    /**
     * @param Request $request
     * @param Rubrique $rubrique
     * @return JsonResponse
     */
    #[Route(path: '/{id}/nombre-de-module', options: ['expose' => true], name: 'count_number_module')]
    public function count(Rubrique $rubrique): JsonResponse
    {
        return  $this->json([
            'total' => count($rubrique->getModuleAutoformations())
        ]);
    }
}
