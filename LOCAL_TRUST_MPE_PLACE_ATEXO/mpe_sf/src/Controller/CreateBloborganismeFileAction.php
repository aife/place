<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller;

use App\Entity\BloborganismeFile;
use App\Entity\MediaUuid;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

final class CreateBloborganismeFileAction extends AbstractController
{
    public function __invoke(Request $request, EntityManagerInterface $entityManager): MediaUuid
    {
        /**
         * @var UploadedFile $uploadedFile
         */
        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }
        $mediaObject = new BloborganismeFile();

        $mediaObject->file = $uploadedFile;
        $mediaObject->setName($uploadedFile->getClientOriginalName());
        $mediaObject->setOrganisme($this->getUser()->getOrganisme()->getAcronyme());
        $mediaObject->setStatutSynchro(0);
        $entityManager->persist($mediaObject);

        $media = new MediaUuid();
        $media->setMedia($mediaObject);
        $media->setCreatedAt(new \DateTime());
        $entityManager->persist($media);

        return $media;
    }
}