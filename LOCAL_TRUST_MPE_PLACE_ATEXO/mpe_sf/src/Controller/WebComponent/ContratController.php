<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\WebComponent;

use App\Entity\ContratTitulaire;
use App\Service\AgentService;
use App\Service\CurrentUser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: 'agent/execution', name: 'agent_web_component_contrat_')]
class ContratController extends AbstractController
{
    final public const TYPE_SAD = 'SAD';
    final public const TYPE_AC_MONO_ATTRIBUTRAIRE = 'AMO';
    final public const TYPE_AC_MULTI_ATTRIBUTRAIRE = 'AMU';
    final public const TYPE_AC_BON_COMMANDE_MONO_ATTRIBUTRAIRE = 'ABCMO';

    final public const TYPES_CONTRAT = [
        self::TYPE_SAD,
        self::TYPE_AC_MONO_ATTRIBUTRAIRE,
        self::TYPE_AC_MULTI_ATTRIBUTRAIRE,
        self::TYPE_AC_BON_COMMANDE_MONO_ATTRIBUTRAIRE,
    ];

    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly AgentService $agentService,
        private readonly CurrentUser $currentUser,
    ) {
    }

    #[Route(path: '/contrats', name: 'list', methods: ['GET'])]
    public function list(Request $request): Response
    {
        return $this->render(
            'webcomponent/contrat/list.html.twig',
            [
                'pf_uid'        => $this->parameterBag->get('UID_PF_MPE'),
                'sso'           => $this->agentService->getSsoForSocle($this->currentUser),
                'filtres'       => '',
                'isRecherche'   => $request->get('advanceSearch') ?? false,
                'isFullWidth'   => true,
            ]
        );
    }

    #[Route(path: '/contrats/types/ac-sad', name: 'list_type_sad', methods: ['GET'])]
    public function sad(Request $request): Response
    {
        $filtres['typesContrat'] = self::TYPES_CONTRAT;

        return $this->render(
            'webcomponent/contrat/list.html.twig',
            [
                'pf_uid'        => $this->parameterBag->get('UID_PF_MPE'),
                'sso'           => $this->agentService->getSsoForSocle($this->currentUser),
                'filtres'       => json_encode($filtres, JSON_THROW_ON_ERROR),
                'isRecherche'   => true,
                'isFullWidth'   => true,
            ]
        );
    }

    #[Route(path: '/contrats/keyword/{keyword}', name: 'list_keyword', methods: ['GET'])]
    public function keyword(Request $request, string $keyword = null): Response
    {
        $filtres = [];

        if ($keyword) {
            $filtres['motsCles'] = $keyword;
        }

        return $this->render(
            'webcomponent/contrat/list.html.twig',
            [
                'pf_uid'        => $this->parameterBag->get('UID_PF_MPE'),
                'sso'           => $this->agentService->getSsoForSocle($this->currentUser),
                'filtres'       => $filtres ? json_encode($filtres, JSON_THROW_ON_ERROR) : '',
                'isRecherche'   => true,
                'isFullWidth'   => true,
            ]
        );
    }

    #[Route(path: '/contrat/{contrat}', name: 'get', methods: ['GET'])]
    public function ficheContrat(ContratTitulaire $contrat): Response
    {
        return $this->render(
            'webcomponent/contrat/fiche.html.twig',
            [
                'pf_uid'        => $this->parameterBag->get('UID_PF_MPE'),
                'sso'           => $this->agentService->getSsoForSocle($this->currentUser),
                'idContrat'     => $contrat->getId(),
                'isFullWidth'   => true,
            ]
        );
    }

    #[Route(path: '/creer', name: 'create', methods: ['GET'])]
    public function create(Request $request): Response
    {
        return $this->render(
            'webcomponent/contrat/create.html.twig',
            [
                'pf_uid'        => $this->parameterBag->get('UID_PF_MPE'),
                'sso'           => $this->agentService->getSsoForSocle($this->currentUser),
                'isFullWidth'   => true,
            ]
        );
    }

    #[Route(path: '/fournisseurs', name: 'fournisseur', methods: ['GET'])]
    public function fournisseurs(Request $request): Response
    {
        return $this->render(
            'webcomponent/contrat/fournisseur.html.twig',
            [
                'pf_uid'        => $this->parameterBag->get('UID_PF_MPE'),
                'sso'           => $this->agentService->getSsoForSocle($this->currentUser),
                'isFullWidth'   => true,
            ]
        );
    }

    #[Route(path: '/fiche-fournisseur/{uuidExec}', name: 'fournisseur', methods: ['GET'])]
    public function fournisseurByUuid(Request $request, string $uuidExec): Response
    {
        return $this->render(
            'webcomponent/contrat/fournisseur.html.twig',
            [
                'pf_uid'        => $this->parameterBag->get('UID_PF_MPE'),
                'uuid_exec'     => sprintf("uuid=%s", $uuidExec),
                'sso'           => $this->agentService->getSsoForSocle($this->currentUser),
                'isFullWidth'   => true,
            ]
        );
    }

    #[Route(path: '/sso-socle', name: 'sso_socle', methods: ['GET'])]
    public function sso(Request $request): Response
    {
        return new JsonResponse(
            [
                'sso' => $this->agentService->getSsoForSocle($this->currentUser),
            ]
        );
    }
}
