<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Tncp;

use DateTime;
use App\Entity\Agent;
use App\Entity\HabilitationAdministrateur;
use App\Form\Tncp\InterfaceSuiviSearchType;
use App\Message\Tncp\Producer\MessageSuiviConsultation;
use App\Service\Paginator;
use App\Service\WebservicesInterfaceSuivi;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use App\Entity\InterfaceSuivi;
use Symfony\Component\Routing\Annotation\Route;

class InterfaceSuiviController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ParameterBagInterface $parameterBag,
        private readonly JWTTokenManagerInterface $tokenManager,
        private readonly LoggerInterface $logger
    ) {
    }

    #[Route(path: '/superadmin/interface-suivi', name: 'superadmin_interface_suivi', methods: ['POST', 'GET'])]
    public function index(Request $request, WebservicesInterfaceSuivi $interfaceSuivi, Paginator $paginator): Response
    {
        $form = $this->createForm(InterfaceSuiviSearchType::class);
        $form->handleRequest($request);
        $data = $form->getData();
        $data['page'] = $request->query->get('page');
        $data = $this->mapFilters($data);
        $wsResponse = $interfaceSuivi->getContent('searchMessages', $data);
        $uri = $form->isSubmitted() ? $request->getRequestUri() : $request->getRequestUri() . '?' ;
        $paginator = $paginator->getPagination($wsResponse->{'hydra:view'}, $uri);
        $paginator['total'] = $wsResponse->{'hydra:totalItems'};
        $messages = $wsResponse->{'hydra:member'};
        return $this->render('superadmin/interface_suivi/index.html.twig', [
            'messages' => $messages,
            'form' => $form->createView(),
            'paginator' => $paginator
        ]);
    }

    #[Route(path: '/superadmin/interface-suivi/republish', name: 'superadmin_interface_suivi_republish', methods: ['POST'])]
    public function rePublish(Request $request, MessageBusInterface $messageBus): Response
    {
        if (
            !$this->getUser()->hasHabilitationAdministrateur(
                HabilitationAdministrateur::ACCESS_INTERFACE_SUIVI_WRITE
            )
        ) {
            return $this->redirectToRoute('superadmin_interface_suivi');
        }

        $messages = $request->request->get('messages');
        foreach ($messages as $messageId) {
            $message = $this->entityManager->getRepository(InterfaceSuivi::class)->find($messageId);
            if (! $message instanceof InterfaceSuivi) {
                $this->logger->error(sprintf('Message with ID %s not found.', $messageId));

                continue;
            }

            if (InterfaceSuivi::ACTION_PUBLICATION_ANNONCE === $message->getAction()) {
                $this->logger->info(sprintf('Message with ID %s not allowed to re-publish.', $messageId));

                continue;
            }
            $agentTechnique = $this->entityManager->getRepository(Agent::class)->findOneBy([
                'login' => $this->parameterBag->get('AGENT_TECHNIQUE_TNCP')
            ]);
            $messageToPublish = MessageSuiviConsultation::normalize(
                $message,
                $this->parameterBag->get('PF_URL_REFERENCE'),
                $this->parameterBag->get('UID_PF_MPE'),
                $agentTechnique ? $this->tokenManager->create($agentTechnique) : ''
            );
            $messageBus->dispatch(new Envelope($messageToPublish));
            $new = clone $message;
            $new->setStatut(InterfaceSuivi::STATUS_EN_ATTENTE);
            $new->setDateCreation(new DateTime());
            $new->setDateModification(new DateTime());

            $this->entityManager->persist($new);
            $this->entityManager->flush();
        }
        return $this->redirectToRoute('superadmin_interface_suivi');
    }

    private function mapFilters(array $data): array
    {
        if (isset($data['dateEnvoi'])) {
            $dateRange = explode('-', (string) $data['dateEnvoi']);

            // Pour avoir les flux d'un jour donné, il faut ajouter 1 jour à la date "before".
            $dateFormat = 'd/m/Y';
            $dateBefore = \DateTime::createFromFormat($dateFormat, trim($dateRange[1]));
            $dateBefore->add(\DateInterval::createFromDateString('1 day'));
            $data['dateCreation']['after'] = str_replace('/', '-', trim($dateRange[0]));
            $data['dateCreation']['strictly_before'] = str_replace('/', '-', $dateBefore->format($dateFormat));
            unset($data['dateEnvoi']);
        }

        return $data;
    }
}
