<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller;

use App\Entity\Agent;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class ErrorController extends AbstractController
{
    #[Route(path: '/redirect-user', name: 'redirect-user')]
    public function redirectUser(ParameterBagInterface $parameterBag) : RedirectResponse
    {
        $baseUrl = $parameterBag->get('PF_URL');
        if ($this->getUser() instanceof Agent) {
            return $this->redirect($baseUrl . 'agent/accueil');
        }
        return $this->redirect($baseUrl);
    }
}
