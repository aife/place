<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller;

use Application\Library\Toro\RestServer;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Admissibilite;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_AnnonceService;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Archive;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Archives;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Authentification;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Consultation;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_ContactsIdEse;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_ContactsSiret;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Decision;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Document;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_DocumentsSynchro;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Echange;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Echanges;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_EntrepriseByRaisonSociale;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_EntrepriseBySiren;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Fichier;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Organisme;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Organismes;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Pli;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_RegistreDepot;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_RegistreDepots;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_RegistreQuestion;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_RegistreQuestions;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_RegistreRetraitDce;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_RegistreRetraitDces;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_TableauDeBord;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_WebService;
use Prado\TShellApplication;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;

class LegacyApiController extends AbstractController
{
    #[Route(path: '/ws/{wildcard}', name: 'legacy_ws', requirements: ['wildcard' => '.*'])]
    public function loadLegacyApiScript() : StreamedResponse
    {
        return new StreamedResponse(
            function () {
                error_reporting(E_ERROR | E_PARSE);
                ini_set('display_errors', '0');

                $application = new TShellApplication('../legacy/protected');
                $application->run();

                RestServer::serve($this->getRoutes());
            }
        );
    }

    /**
     * @return string[]
     */
    protected function getRoutes(): array
    {
        $routes = [
            'authentification/connexion/:alpha/:alpha' => Atexo_Rest_Action_Authentification::class,
            'annonceservice/create?ticket=:alpha' => Atexo_Rest_Action_AnnonceService::class,
            'annonceservice/update?ticket=:alpha' => Atexo_Rest_Action_AnnonceService::class,
            'annonceservice/get/:alpha/:number?ticket=:alpha' => Atexo_Rest_Action_AnnonceService::class,
            'consultation/create?ticket=:alpha' => Atexo_Rest_Action_Consultation::class,
            'consultation/update?ticket=:alpha' => Atexo_Rest_Action_Consultation::class,
            'consultation/get/:alpha/:number?ticket=:alpha' => Atexo_Rest_Action_Consultation::class,
            'tableaudebord?ticket=:alpha' => Atexo_Rest_Action_TableauDeBord::class,
            'registre/questions/:number?ticket=:alpha' => Atexo_Rest_Action_RegistreQuestions::class,
            'registre/question/:number?ticket=:alpha' => Atexo_Rest_Action_RegistreQuestion::class,
            'registre/retraitdces/:number?ticket=:alpha' => Atexo_Rest_Action_RegistreRetraitDces::class,
            'registre/retraitdce/:number?ticket=:alpha' => Atexo_Rest_Action_RegistreRetraitDce::class,
            'registre/depots/:number?ticket=:alpha' => Atexo_Rest_Action_RegistreDepots::class,
            'registre/depot/:number?ticket=:alpha' => Atexo_Rest_Action_RegistreDepot::class,
            'registre/depot/update?ticket=:alpha' => Atexo_Rest_Action_RegistreDepot::class,
            'registre/pli/:number?ticket=:alpha' => Atexo_Rest_Action_Pli::class,
            'registre/pli/:string/:number?ticket=:alpha' => Atexo_Rest_Action_Pli::class,
            'contactsBySiret/get/:number?ticket=:alpha' => Atexo_Rest_Action_ContactsSiret::class,
            'contactsByEntrepriseId/get/:number?ticket=:alpha' => Atexo_Rest_Action_ContactsIdEse::class,
            'entrepriseByRaisonSociale/get/:base64?ticket=:alpha' => Atexo_Rest_Action_EntrepriseByRaisonSociale::class,
            'entrepriseBySiren/get/:number?ticket=:alpha' => Atexo_Rest_Action_EntrepriseBySiren::class,
            'archive/listeid/:number?ticket=:alpha' => Atexo_Rest_Action_Archives::class,
            'archive/fichier/:alpha/:number?ticket=:alpha' => Atexo_Rest_Action_Archive::class,
            'organismes?ticket=:alpha' => Atexo_Rest_Action_Organismes::class,
            'organismes?ticket=:alpha&withLogo=:alpha' => Atexo_Rest_Action_Organismes::class,
            'organisme/:alpha?ticket=:alpha' => Atexo_Rest_Action_Organisme::class,
            'organisme/:alpha?ticket=:alpha&withLogo=:alpha' => Atexo_Rest_Action_Organisme::class,
            'document/:alpha/:alpha/:alpha?ticket=:alpha' => Atexo_Rest_Action_Document::class,
            'document/:alpha/:alpha/:alpha/:alpha?ticket=:alpha' => Atexo_Rest_Action_Document::class,
            'document/:alpha/:alpha/:alpha/:alpha/:alpha?ticket=:alpha' => Atexo_Rest_Action_Document::class,
            'depot/admissibilite/:alpha/:alpha/:number?ticket=:alpha' => Atexo_Rest_Action_Admissibilite::class,
            'depot/decision/:alpha/:alpha/:number?ticket=:alpha' => Atexo_Rest_Action_Decision::class,
            'offre/admissibilite/:alpha/:alpha/:number?ticket=:alpha' => Atexo_Rest_Action_Admissibilite::class,
            'offre/decision/:alpha/:alpha/:number?ticket=:alpha' => Atexo_Rest_Action_Decision::class,
            'registre/echanges/:alpha/:number?ticket=:alpha' => Atexo_Rest_Action_Echanges::class,
            'registre/echange/:alpha/:number?ticket=:alpha' => Atexo_Rest_Action_Echange::class,
            'fichier/:alpha/:number/:alpha?ticket=:alpha' => Atexo_Rest_Action_Fichier::class,
            'fichier/:alpha/:number/:alpha/:number?ticket=:alpha' => Atexo_Rest_Action_Fichier::class,
            'document/:alpha/:alpha/:alpha/:alpha?ticket=:alpha&typeChecksum=:alpha' =>
                Atexo_Rest_Action_Document::class,
            'document/:alpha/:alpha/:alpha?ticket=:alpha&typeChecksum=:alpha' => Atexo_Rest_Action_Document::class,
            'document/:alpha/:alpha/:alpha?ticket=:alpha&chiffrer=:alpha' => Atexo_Rest_Action_Document::class,
            'document/:alpha/:alpha/:alpha?ticket=:alpha&chiffrer=:alpha&withFile=:alpha' =>
                Atexo_Rest_Action_Document::class,
            'document/:alpha/:alpha/:alpha/:alpha?ticket=:alpha&withFile=:alpha' => Atexo_Rest_Action_Document::class,
            'document/:alpha/:alpha/:alpha?ticket=:alpha&withFile=:alpha' => Atexo_Rest_Action_Document::class,
            'document/:alpha/:alpha/:alpha?ticket=:alpha&typeChecksum=:alpha&chiffrer=:alpha' =>
                Atexo_Rest_Action_Document::class,
            'document/:alpha/:alpha/:alpha?ticket=:alpha&typeChecksum=:alpha&chiffrer=:alpha&withFile=:alpha' =>
                Atexo_Rest_Action_Document::class,
            'document/:alpha/:alpha/:alpha/:alpha?ticket=:alpha&typeChecksum=:alpha&withFile=:alpha' =>
                Atexo_Rest_Action_Document::class,
            'document/:alpha/:alpha/:alpha?ticket=:alpha&typeChecksum=:alpha&withFile=:alpha' =>
                Atexo_Rest_Action_Document::class,
            'document/synchro/:alpha/:all?type=:alpha' => Atexo_Rest_Action_DocumentsSynchro::class,
            'document/synchro/:alpha/:all/:alpha?type=:alpha' => Atexo_Rest_Action_DocumentsSynchro::class,
            ':alpha/:alpha?ticket=:alpha' => Atexo_Rest_Action_WebService::class,
        ];

        $routesKeys = array_keys($routes);

        array_walk($routesKeys, function (&$item) {
            $item = '/api.php/ws/' . $item;
        });

        return array_combine($routesKeys, $routes);
    }
}
