<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Ecrans;

use App\Service\Footer\Footer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class FooterSharedController extends AbstractController
{

    /**
     * @param string $calledFrom
     * @param string $productName
     * @param Footer $footerService
     * @return Response
     */
    #[Route(path: '/footer/{calledFrom}/{productName}', name: 'footer_share', methods: ['GET'], requirements: ['productName' => 'redac|faq|exec'])]
    public function index(string $calledFrom, string $productName, Footer $footerService) : Response
    {
        $content = $footerService->getTemplate($productName, $calledFrom);

        $textResponse = new Response($content, Response::HTTP_OK);
        $textResponse->headers->set('Content-Type', 'text/plain');

        return $textResponse;
    }
}
