<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller\Ecrans;

use App\Service\Header\Header;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class HeaderSharedController extends AbstractController
{
    /**
     * @param string $productName
     * @param Header $headerService
     * @return Response
     */
    #[Route(path: '/header/{productName}', name: 'header_share', methods: ['GET'], requirements: ['productName' => 'redac|faq|exec'])]
    public function index(string $productName, Header $headerService)
    {
        $content = $headerService->getTemplate($productName);

        $textResponse = new Response($content, Response::HTTP_OK);
        $textResponse->headers->set('Content-Type', 'text/plain');

        return $textResponse;
    }
}
