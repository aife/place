<?php

namespace App\Controller;

use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Entity\Agent;
use App\Form\ChangePasswordFormType;
use App\Form\ResetPasswordRequestFormType;
use App\Service\ResetPasswordRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route(path: '/{calledFrom}/reset-password', name: 'atexo_', requirements: ['calledFrom' => 'entreprise|agent'])]
class ResetPasswordController extends AbstractController
{
    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    #[Route(path: '/', name: 'forgot_password_request')]
    public function request(Request $request, ResetPasswordRequest $resetPasswordRequest, string $calledFrom): Response
    {
        $form = $this->createForm(ResetPasswordRequestFormType::class);
        $form->handleRequest($request);
        $template = 'reset_password/request.html.twig';
        if ('entreprise' === $calledFrom) {
            $template = 'reset_password/entreprise-request.html.twig';
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $resetPasswordRequest->processSendingPasswordResetEmail($form->get('email')->getData(), $calledFrom);

            return $this->render($template);
        }

        return $this->render($template, [
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/reset/{token}', name: 'reset_password')]
    public function reset(
        Request $request,
        UserPasswordHasherInterface $passwordEncoder,
        ResetPasswordRequest $resetPasswordRequest,
        string $calledFrom,
        string $token
    ): Response {
        $resetPassword = $resetPasswordRequest->getResetPasswordRequestByJeton($token);
        $user = $resetPasswordRequest->validateTokenAndFetchUser($resetPassword);

        if ($resetPassword->getModificationFaite() === '1') {
            return $this->redirect('/entreprise');
        }

        if (null === $user) {
            $template = 'reset_password/reset-token-expired.html.twig';
            if ('entreprise' === $calledFrom) {
                $template = 'reset_password/entreprise-reset-token-expired.html.twig';
            }

            return $this->render($template);
        }
        $form = $this->createForm(ChangePasswordFormType::class, null, ['userEmail' => $user->getEmail()]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $encodedPassword = $passwordEncoder->hashPassword(
                $user,
                $form->get('plainPassword')->getData()
            );

            if ($user instanceof Agent) {
                $user->setPassword($encodedPassword);
            } else {
                $user->setMdp($encodedPassword);
            }

            $user->setTentativesMdp(0);
            $resetPasswordRequest->validateAllResetPasswordRequest($resetPassword->getIdUser());
            $this->getDoctrine()->getManager()->flush();

            if ('entreprise' === $calledFrom) {
                return $this->render('reset_password/success-entreprise-reset.html.twig', [
                    'link' => $this->generateUrl('accueil_entreprise'),
                ]);
            }

            $this->addFlash('success', $this->translator->trans('MOT_DE_PASSE_REINITIALISE_SUCCES'));

            return $this->redirectToRoute('atexo_agent_accueil_index');
        }

        $template = 'reset_password/reset.html.twig';
        if ('entreprise' === $calledFrom) {
            $template = 'reset_password/entreprise-reset.html.twig';
        }

        return $this->render($template, [
            'form' => $form->createView(),
        ]);
    }
}
