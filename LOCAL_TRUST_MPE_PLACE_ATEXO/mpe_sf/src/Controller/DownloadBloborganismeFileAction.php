<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Controller;

use App\Entity\MediaUuid;
use App\Service\Blob\BlobOrganismeFileService;
use League\Flysystem\FileNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Mime\MimeTypeGuesserInterface;

class DownloadBloborganismeFileAction extends AbstractController
{
    private const REAL_PATH = 'realPath';

    public function __invoke(
        MediaUuid $blob,
        BlobOrganismeFileService $blobOrganismeFileService,
        MimeTypeGuesserInterface $mimeTypeGuesser
    ): BinaryFileResponse {
        $data = $blobOrganismeFileService->getNameAndRealPath($blob->getMedia());

        if (!file_exists($data[self::REAL_PATH]) || time() > $blob->getCreatedAt()->getTimestamp() + MediaUuid::EXPIRED_DOWNLOAD_TIME) {
            throw new FileNotFoundException('Le fichier n\'existe pas sur le serveur');
        }

        $response = new BinaryFileResponse($data[self::REAL_PATH]);

        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $data['name']);
        $response->headers->set('Content-Type', $mimeTypeGuesser->guessMimeType($data[self::REAL_PATH]));
        $response->headers->set('Content-Length', filesize($data[self::REAL_PATH]));

        return $response;
    }
}
