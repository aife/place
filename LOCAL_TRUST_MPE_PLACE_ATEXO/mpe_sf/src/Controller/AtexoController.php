<?php

/*
 * Controller AtexoController
 *
 * @author Anas Rhorbal <anas.rhorbal@atexo.com>
 * @author Oumar KONATE <oumar.konate@atexo.com>
 */

namespace App\Controller;

use App\Traits\ConfigProxyTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class AtexoController extends AbstractController
{
    use ConfigProxyTrait;

    /**
     * Permet de telecharger.
     *
     * @param $content
     * @param $fileName
     * @param $type
     * @return Response
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @since ESR2016
     * @copyright Atexo 2016
     */
    public function downloadFile($content, $fileName, $type)
    {
        $response = new Response($content);
        $d = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $fileName);
        $response->headers->set('Content-Disposition', $d);
        $response->headers->set('Content-type', $type);

        return $response;
    }
}
