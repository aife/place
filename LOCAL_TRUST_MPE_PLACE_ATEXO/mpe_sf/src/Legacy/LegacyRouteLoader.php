<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Legacy;

use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class LegacyRouteLoader extends Loader
{
    /**
     * Ajoute la route /?page=... dans la liste des routers SF
     * qui lancera l'appel à App\Controller\LegacyController::loadLegacyScript
     *
     * @param mixed $resource
     * @param null $type
     */
    public function load($resource, $type = null): RouteCollection
    {
        $collection = new RouteCollection();

        $collection->add(
            'app.legacy.prado',
            new Route(
                '/',
                [
                    '_controller' => 'App\Controller\LegacyController::loadLegacyScript',
                ],
                [
                    'page' => '[a-zA-Z0-9]+.[a-zA-Z0-9]+',
                ]
            )
        );

        return $collection;
    }

    public function supports($resource, $type = null): bool
    {
        return 'legacy' === $type;
    }
}
