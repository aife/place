<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataProvider;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryResultCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryResultItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGenerator;
use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\Exception\RuntimeException;
use App\Doctrine\Extension\ConsultationIdExtension;
use App\Doctrine\Extension\ConsultationStatutExtension;
use App\Entity\Consultation;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

final class ConsultationDataProvider extends AbstractDataProvider implements
    ItemDataProviderInterface,
    CollectionDataProviderInterface,
    RestrictedDataProviderInterface
{
    public function __construct(
        private readonly ManagerRegistry $managerRegistry,
        private readonly iterable $itemExtensions = [],
        private readonly iterable $collectionExtensions = []
    ) {
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Consultation::class === $resourceClass;
    }

    /**
     * {@inheritdoc}
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        /** @var EntityManagerInterface $manager */
        $manager = $this->managerRegistry->getManagerForClass($resourceClass);

        $identifiers = ["id" => $id];

        $fetchData = $context['fetch_data'] ?? true;
        if (!$fetchData) {
            return $manager->getReference($resourceClass, $identifiers);
        }

        $repository = $manager->getRepository($resourceClass);
        if (!method_exists($repository, 'createQueryBuilder')) {
            throw new RuntimeException('The repository class must have a "createQueryBuilder" method.');
        }

        $queryBuilder = $repository->createQueryBuilder('o');
        $queryNameGenerator = new QueryNameGenerator();
        $doctrineClassMetadata = $manager->getClassMetadata($resourceClass);

        $this->addWhereForIdentifiers($identifiers, $queryBuilder, $doctrineClassMetadata);

        foreach ($this->itemExtensions as $extension) {
            $extension->applyToItem(
                $queryBuilder,
                $queryNameGenerator,
                $resourceClass,
                $identifiers,
                $operationName,
                $context
            );

            if (
                $extension instanceof QueryResultItemExtensionInterface
                && $extension->supportsResult($resourceClass, $operationName, $context)
            ) {
                return $extension->getResult($queryBuilder, $resourceClass, $operationName, $context);
            }
        }

        $result = $queryBuilder->getQuery()->getOneOrNullResult();

        if (
            $result
            && is_array($result)
            && count($result) === 2
            && ($consultation = $result[0])
            && isset($result[ConsultationStatutExtension::PROPERTY_NAME])
        ) {
            $statutCalcule = $result[ConsultationStatutExtension::PROPERTY_NAME];
            /** @var Consultation $consultation */
            $consultation->setCalculatedStatus($statutCalcule);

            return $consultation;
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): iterable
    {
        /** @var EntityManagerInterface $manager */
        $manager = $this->managerRegistry->getManagerForClass($resourceClass);

        $repository = $manager->getRepository($resourceClass);
        if (!method_exists($repository, 'createQueryBuilder')) {
            throw new RuntimeException('The repository class must have a "createQueryBuilder" method.');
        }

        $queryBuilder = $repository->createQueryBuilder('o');
        $queryNameGenerator = new QueryNameGenerator();

        foreach ($this->collectionExtensions as $extension) {
            $extension->applyToCollection($queryBuilder, $queryNameGenerator, $resourceClass, $operationName, $context);

            if (
                $extension instanceof QueryResultCollectionExtensionInterface
                && $extension->supportsResult($resourceClass, $operationName, $context)
            ) {
                $paginatedResults = $extension->getResult($queryBuilder, $resourceClass, $operationName, $context);

                return $this->getTransformedResults($paginatedResults);
            }
        }

        $hydrationMode = ConsultationIdExtension::shouldReturnOnlyIds($context)
            ? AbstractQuery::HYDRATE_ARRAY : AbstractQuery::HYDRATE_OBJECT;

        return $this->getTransformedResults($queryBuilder->getQuery()->getResult($hydrationMode));
    }

    /**
     * @param Paginator|array|mixed $paginatedResults
     * @return mixed
     */
    public function getTransformedResults($paginatedResults)
    {
        foreach ($paginatedResults as &$item) {
            $statutCalcule = $item[ConsultationStatutExtension::PROPERTY_NAME] ?? null;
            if (
                $item
                && is_array($item)
                && count($item) === 2
                && ($item[0] instanceof Consultation)
            ) {
                /** @var Consultation $consultation */
                $item[0]->setCalculatedStatus($statutCalcule);
                $item = $item[0];
            }
        }

        return $paginatedResults;
    }
}
