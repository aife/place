<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Entreprise;

use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\Typeavis;
use App\Entity\TypeProcedure;
use App\Filter\GroupBySearchFilter;
use App\Form\Agent\FieldType\LieuxExecutionSelectType;
use App\Form\Agent\FieldType\LieuxExecutionType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConsultationRechercheType extends AbstractType
{
    public function __construct(
        private ParameterBagInterface $parameterBag
    ) {
    }

    public function getBlockPrefix()
    {
        return '';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->setMethod(Request::METHOD_GET)
            ->add('keyWord', TextType::class, ['label' => 'DEFINE_MOTS_CLES'])
            ->add('referenceUtilisateur', TextType::class, ['label' => 'REFERENCE'])
            ->add('typeProcedureOrganisme', ChoiceType::class, [
                'choices' => $options['types_procedures'],
                'choice_value' => 'id_type_procedure',
                'choice_label' => function (?TypeProcedure $typeProcedure) {
                    return $typeProcedure ? $typeProcedure->getLibelleTypeProcedure() : '';
                },
                'placeholder' => '',
                'multiple' => false,
                'expanded' => false,
            ])
            ->add('typeAvis', ChoiceType::class, [
                'choices'       => $options['types_avis'],
                'choice_value'  => 'id',
                'choice_label'  => function (?Typeavis $typeAvis) {
                    return $typeAvis ? $typeAvis->getIntituleAvis() : '';
                },
                'placeholder'   => false,
                'multiple'      => false,
                'expanded'      => false,
                'preferred_choices' => function ($choice, $key, $value) {
                    return $value == $this->parameterBag->get('TYPE_AVIS_CONSULTATION');
                },
            ])
            ->add(
                'categorie',
                ChoiceType::class,
                [
                    'choices' => [
                        'DEFINE_TRAVAUX' => $this->parameterBag->get('TYPE_PRESTATION_TRAVAUX'),
                        'DEFINE_FOURNITURES' => $this->parameterBag->get('TYPE_PRESTATION_FOURNITURES'),
                        'DEFINE_SERVICES' => $this->parameterBag->get('TYPE_PRESTATION_SERVICES'),
                    ],
                    'choice_attr' => [
                        'DEFINE_TRAVAUX' => ['data-icon' => 'la la-wrench'],
                        'DEFINE_FOURNITURES' => ['data-icon' => 'ft-shopping-cart'],
                        'DEFINE_SERVICES' => ['data-icon' => 'ft-briefcase'],
                    ],
                    'multiple' => true,
                    'expanded' => true,
                    'attr' => [
                        'class' => 'btn-group btn-group-toggle d-flex flex-column flex-md-row',
                        'data-toggle' => 'buttons',
                        'role' => 'group',
                    ],
                ]
            )
            ->add(
                'form_lieuxExecutionSelect',
                LieuxExecutionSelectType::class,
                ['choices' => $options['lieuxExecution']]
            )
            ->add('form_CPV', HiddenType::class)
            ->add('form_lieuxExecution', LieuxExecutionType::class, ['data' => $options['lieuxExecutionIds']])
            ->add('dlroApres', DateType::class, ['widget' => 'single_text'])
            ->add('dlroAvant', DateType::class, ['widget' => 'single_text'])
            ->add('miseEnLigneApres', DateType::class, ['widget' => 'single_text'])
            ->add('miseEnLigneAvant', DateType::class, ['widget' => 'single_text'])
            ->add('clauseSociale', ChoiceType::class, [
                'choices' => [
                    'TEXT_INDIFFERENT' => null,
                    'TEXT_OUI' => '1',
                    'TEXT_NON' => '2'
                ],
                'multiple' => false,
                'expanded' => false,
            ])
            ->add('clauseSocialeAteliersProteges', ChoiceType::class, [
                'choices' => [
                    'TEXT_INDIFFERENT' => null,
                    'TEXT_OUI' => '1',
                    'TEXT_NON' => '2'
                ],
                'multiple' => false,
                'expanded' => false,
            ])
            ->add('clauseSocialeSiae', ChoiceType::class, [
                'choices' => [
                    'TEXT_INDIFFERENT' => null,
                    'TEXT_OUI' => '1',
                    'TEXT_NON' => '2'
                ],
                'multiple' => false,
                'expanded' => false,
            ])
            ->add('clauseSocialeEss', ChoiceType::class, [
                'choices' => [
                    'TEXT_INDIFFERENT' => null,
                    'TEXT_OUI' => '1',
                    'TEXT_NON' => '2'
                ],
                'multiple' => false,
                'expanded' => false,
            ])
            ->add('socialeCommerceEquitable', ChoiceType::class, [
                'choices' => [
                    'TEXT_INDIFFERENT' => null,
                    'TEXT_OUI' => '1',
                    'TEXT_NON' => '2'
                ],
                'multiple' => false,
                'expanded' => false,
            ])
            ->add('socialeInsertionActiviterEconomique', ChoiceType::class, [
                'choices' => [
                    'TEXT_INDIFFERENT' => null,
                    'TEXT_OUI' => '1',
                    'TEXT_NON' => '2'
                ],
                'multiple' => false,
                'expanded' => false,
            ])
            ->add('clauseEnvironnementale', ChoiceType::class, [
                'choices' => [
                    'TEXT_INDIFFERENT' => null,
                    'TEXT_OUI' => '1',
                    'TEXT_NON' => '2'
                ],
                'multiple' => false,
                'expanded' => false,
            ])
            ->add('search', SubmitType::class, [
                'label' => 'RECHERCHER',
                'icon_before' => 'fa fa-search',
                'attr' => [
                    'class' => 'btn btn-primary',
                ],
            ])
            ->add('id', HiddenType::class)
        ;

        if ($options['isStat']) {
            $builder
                ->add(
                    'groupBy',
                    ChoiceType::class,
                    [
                        'choices' => GroupBySearchFilter::GROUP_BY_ACCEPT
                    ]
                )
                ->add('pagination', TextType::class, ['empty_data' => false]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'error_mapping' => [
                    'form_lieuExecution' => 'form_lieuExecutionSelect',
                ],
                'attr'              => [
                    'novalidate'    => 'novalidate',
                ],
                'lieux' => [],
                'ids' => [],
                'types_procedures'  => new ArrayCollection(),
                'types_avis'        => new ArrayCollection(),
                'lieuxExecution'    => new ArrayCollection(),
                'lieuxExecutionIds' => '',
                'csrf_protection'   => false,
                'isStat'            => false
            ]
        );
    }
}
