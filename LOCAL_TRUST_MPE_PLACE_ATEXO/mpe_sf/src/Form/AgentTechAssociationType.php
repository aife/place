<?php

namespace App\Form;

use App\Entity\Organisme;
use App\Entity\Service;
use App\Entity\WS\AgentTechniqueAssociation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AgentTechAssociationType extends AbstractType
{
    /**
     * The Type requires the EntityManager as argument in the constructor. It is autowired
     * in Symfony 3.
     */
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'onPreSetData']);
        $builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'onPreSubmit']);
    }

    protected function addElements(FormInterface $form, Organisme $organisme = null)
    {
        // 4. Add the province element
        $form->add('organisme', EntityType::class, [
            'class' => Organisme::class,
            'placeholder' => 'Sélectionner ...',
        ]);

        // Service empty, unless there is a selected Organisme (Edit View)
        $services = [];

        // If there is a Organisme stored in the Agent entity, load the Service of it
        if ($organisme) {
            // Fetch Service of the City if there's a selected Organisme
            $serviceRepo = $this->em->getRepository(Service::class);

            $services = $serviceRepo->createQueryBuilder('s')
                ->where('s.organisme = :organisme')
                ->setParameter('organisme', $organisme->getAcronyme())
                ->getQuery()
                ->getResult();
        }

        // Add the Neighborhoods field with the properly data
        $form->add('service', EntityType::class, [
            'required' => false,
            'class' => Service::class,
            'choices' => $services,
            'choice_label' => 'libelle',
            'placeholder' => 'Sélectionner ...',
        ]);
    }

    public function onPreSubmit(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

        $organisme = $this->em->getRepository(Organisme::class)->find($data['organisme']);

        $this->addElements($form, $organisme);
    }

    public function onPreSetData(FormEvent $event)
    {
        $agent = $event->getData();
        $form = $event->getForm();

        $organisme = $agent->getOrganisme() ?: null;
        $this->addElements($form, $organisme);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AgentTechniqueAssociation::class,
        ]);
    }
}
