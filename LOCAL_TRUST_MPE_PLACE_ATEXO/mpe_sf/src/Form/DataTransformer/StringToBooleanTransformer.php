<?php

namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class StringToBooleanTransformer implements DataTransformerInterface
{
    public function transform($value)
    {
        if ($value === null) {
            return null;
        }

        return $value === '1' || $value === 1 || $value === true;
    }

    public function reverseTransform($value)
    {
        if ($value === null) {
            return null;
        }

        return $value === '1' || $value === 1 || $value === true ? '1' : '0';
    }
}
