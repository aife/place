<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\DataTransformer;

use App\Entity\CategorieINSEE;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class CategorieInseeToIdTransformer implements DataTransformerInterface
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function transform($idCategorieInsee): ?CategorieINSEE
    {
        // no issue number? It's optional, so that's ok
        if (!$idCategorieInsee) {
            return null;
        }

        $categorieINSEE = $this->entityManager
            ->getRepository(CategorieINSEE::class)
            // query for the issue with this id
            ->find($idCategorieInsee)
        ;

        if (null === $categorieINSEE) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'An CategorieINSEE with number "%s" does not exist!',
                $categorieINSEE
            ));
        }

        return $categorieINSEE;
    }

    public function reverseTransform($categorieINSEE): ?string
    {
        return $categorieINSEE->getId();
    }
}
