<?php

namespace App\Form;

use App\Entity\ConfigurationMessagesTraduction;
use App\Entity\Langue;
use App\Repository\LangueRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TraductionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('source', null, ['label' => 'Clé'])
            ->add('target', null, ['label' => 'Traduction'])
            ->add('langue', EntityType::class, [
                'class' => Langue::class,
                'query_builder' => fn(LangueRepository $repo) => $repo->createQueryBuilder('l')
                    ->orderBy('l.langue', 'ASC'),
                'choice_label' => 'langue',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ConfigurationMessagesTraduction::class,
        ]);
    }
}
