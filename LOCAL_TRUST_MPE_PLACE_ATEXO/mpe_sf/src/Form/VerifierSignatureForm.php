<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

class VerifierSignatureForm extends AbstractType
{
    public function __construct(private TranslatorInterface $translator)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('fileToVerify', FileType::class, [
            'attr' => [
                'class' => 'filestyle',
                'data-text' => $this->translator->trans('TELECHARGER_DOCUMENT_VERIFICATION'),
                'data-btnClass' => 'btn-primary text-white',
                'data-buttonBefore' => 'true'
            ],
            'constraints' => [new NotBlank(['message' => $this->translator->trans('DEFINE_TEXT_FICHIERS_A_VERIFIER')])]
        ])->add('signingFile', FileType::class, [
                'attr' => [
                    'class' => 'filestyle',
                    'data-text' => $this->translator->trans('TELECHARGER_DOCUMENT_ASSOCIE'),
                    'data-btnClass' => 'btn-primary text-white',
                    'data-buttonBefore' => 'true'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([]);
    }
}
