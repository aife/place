<?php

namespace App\Form\Habilitation;

use App\Entity\Agent;
use App\Entity\WS\WebService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HabilitationAgentType extends AbstractType
{
    /**
     * The Type requires the EntityManager as argument in the constructor. It is autowired
     * in Symfony 3.
     */
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $webservices = $this->em->getRepository(WebService::class)->findAll();
        $builder->add('webservices', EntityType::class, [
                'class' => WebService::class,
                'required' => false,
                'choices' => $webservices,
                'choice_label' => 'nom_ws',
               'multiple' => true,
               'expanded' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Agent::class,
        ]);
    }
}
