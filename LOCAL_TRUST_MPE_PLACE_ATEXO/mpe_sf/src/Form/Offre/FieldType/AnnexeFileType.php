<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Offre\FieldType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

class AnnexeFileType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'label' => false,
            'required' => true,
            'constraints' => [
                new NotBlank(['message' => 'analyse-offre.annexe-financiere.form.error.blank']),
                new File([
                    'mimeTypes' => ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
                    'mimeTypesMessage' => 'analyse-offre.annexe-financiere.form.error.xlsx',
                ]),
            ],
            'row_attr' => ['class' => 'custom-file-input'],
            'attr' => ['accept' => '.xlsx'],
        ]);
    }

    public function getParent(): string
    {
        return FileType::class;
    }
}
