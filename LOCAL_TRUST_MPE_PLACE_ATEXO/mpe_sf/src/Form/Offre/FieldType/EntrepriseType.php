<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Offre\FieldType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class EntrepriseType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'label' => false,
            'required' => true,
            'attr' => [
                'class' => 'form-control',
            ],
            'constraints' => [
                new NotBlank(['message' => 'analyse-offre.annexe-financiere.form.error.blank']),
            ],
        ]);
    }

    public function getParent(): string
    {
        return TextType::class;
    }
}
