<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Offre;

use App\Form\Offre\FieldType\AnnexeFileType;
use App\Form\Offre\FieldType\DateRemiseDateType;
use App\Form\Offre\FieldType\DateRemiseHeureType;
use App\Form\Offre\FieldType\EntrepriseType;
use App\Form\Offre\FieldType\NumeroLotType;
use App\Form\Offre\FieldType\NumeroPliType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImportAnnexeFinanciereType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('entreprise', EntrepriseType::class)
            ->add('numeroPli', NumeroPliType::class)
            ->add('numeroLot', NumeroLotType::class)
            ->add('dateRemiseDate', DateRemiseDateType::class)
            ->add('dateRemiseHeure', DateRemiseHeureType::class)
            ->add('importFichier', AnnexeFileType::class)
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => 'TEXT_VALIDER',
                    'attr' => [
                        'class' => 'btn btn-primary',
                    ],
                ]
            )
        ;
    }

    public function getBlockPrefix(): string
    {
        return 'form';
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'attr' => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }
}
