<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Tncp;

use App\Entity\InterfaceSuivi;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InterfaceSuiviSearchType extends AbstractType
{
    public function getBlockPrefix()
    {
        return '';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('uuid', TextType::class, ['attr' => ['class' => 'form-control', 'name' => 'uuid']])
            ->add('flux', ChoiceType::class, [
                'attr' => ['class' => 'select2 form-control'],
                'choices' => [
                    InterfaceSuivi::TNCP_INTERFACE => InterfaceSuivi::TNCP_INTERFACE,
                    InterfaceSuivi::ANNONCE_TNCP_INTERFACE => InterfaceSuivi::ANNONCE_TNCP_INTERFACE,
                    InterfaceSuivi::TNCP_INTERFACE_INPUT => InterfaceSuivi::TNCP_INTERFACE_INPUT,
                ],
                'multiple' => true,
                ])
            ->add('dateEnvoi', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('consultation', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('idObjetDestination', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('action', ChoiceType::class, [
                'attr' => ['class' => 'select2 form-control'],
                'multiple' => true,
                'choices' => [
                    InterfaceSuivi::ACTION_PUBLICATION_CONSULTATION => InterfaceSuivi::ACTION_PUBLICATION_CONSULTATION,
                    InterfaceSuivi::ACTION_MODIFICATION_DATE_LIMITE_REMISE_PLI =>
                    InterfaceSuivi::ACTION_MODIFICATION_DATE_LIMITE_REMISE_PLI,
                    InterfaceSuivi::ACTION_MODIFICATION_DCE => InterfaceSuivi::ACTION_MODIFICATION_DCE,
                    InterfaceSuivi::ACTION_PUBLICATION_ANNONCE => InterfaceSuivi::ACTION_PUBLICATION_ANNONCE,
                    InterfaceSuivi::ACTION_RECUPERATION_NOTIFICATION => InterfaceSuivi::ACTION_RECUPERATION_NOTIFICATION,
                    InterfaceSuivi::ACTION_INTERGRATION_CONSULTATIONS_TNCP => InterfaceSuivi::ACTION_INTERGRATION_CONSULTATIONS_TNCP,
                ]
            ])
            ->add('statut', ChoiceType::class, [
                'attr' => ['class' => 'select2 form-control'],
                'multiple' => true,
                'choices' => [
                    InterfaceSuivi::STATUS_EN_ATTENTE_TEXT => InterfaceSuivi::STATUS_EN_ATTENTE,
                    InterfaceSuivi::STATUS_EN_COURS_TEXT => InterfaceSuivi::STATUS_EN_ATTENTE,
                    InterfaceSuivi::STATUS_FINI_TEXT => InterfaceSuivi::STATUS_FINI,
                    InterfaceSuivi::STATUS_ERREUR_TEXT => InterfaceSuivi::STATUS_ERREUR,
                ]
            ])
            ->add('etape', ChoiceType::class, [
                'attr' => ['class' => 'select2 form-control'],
                'multiple' => true,
                'choices' => [
                    InterfaceSuivi::ETAPE_AJOUT_CONSULTATION => InterfaceSuivi::ETAPE_AJOUT_CONSULTATION,
                    InterfaceSuivi::ETAPE_AJOUT_DCE => InterfaceSuivi::ETAPE_AJOUT_DCE,
                    InterfaceSuivi::ETAPE_AJOUT_LOT => InterfaceSuivi::ETAPE_AJOUT_LOT,
                    InterfaceSuivi::ETAPE_VALIDATION_CONSULTATION => InterfaceSuivi::ETAPE_VALIDATION_CONSULTATION,
                    InterfaceSuivi::ETAPE_ABONNEMENT_CONSULTATION => InterfaceSuivi::ETAPE_ABONNEMENT_CONSULTATION,
                    InterfaceSuivi::ETAPE_RECUPERATION_CONSULTATIONS_TNCP =>
                        InterfaceSuivi::ETAPE_RECUPERATION_CONSULTATIONS_TNCP,
                    InterfaceSuivi::ETAPE_ANNONCE_SYNCHRO_CONSULTATION =>
                        InterfaceSuivi::ETAPE_ANNONCE_SYNCHRO_CONSULTATION,
                ]
            ])
            ->add('annonce', TextType::class, ['attr' => ['class' => 'form-control']])
            ->setMethod(Request::METHOD_GET);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['csrf_protection' => false, 'translation_domain' => false]);
    }
}
