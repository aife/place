<?php

namespace App\Form\Model;

use App\Model\ServiceModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ServiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', TextType::class)
            ->add('idParent', TextType::class)
            ->add('idExterneParent', TextType::class)
            ->add('idExterne', TextType::class)
            ->add('libelle', TextType::class)
            ->add('acronymeOrganisme', TextType::class, [
            'property_path' => 'organisme',
            ])
            ->add('sigle', TextType::class)
            ->add('email', TextType::class)
            ->add('formeJuridique', TextType::class)
            ->add('formeJuridiqueCode', TextType::class)
            ->add('siren', TextType::class)
            ->add('complement', TextType::class)
            ->add('dateModification', TextType::class)
            ->add('dateCreation', TextType::class)
            ->add('idEntite', TextType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ServiceModel::class,
            'is_edit' => false,
            'csrf_protection' => false,
        ]);
    }
}
