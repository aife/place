<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Admin;

use App\Entity\CategorieINSEE;
use App\Entity\Organisme;
use App\Form\DataTransformer\CategorieInseeToIdTransformer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;

class OrganismeType extends AbstractType
{
    public function __construct(private CategorieInseeToIdTransformer $transformer)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('sigle', TextType::class, [
                'label' => 'DEFINE_SIGLE'
            ])
            ->add('typeArticleOrg', ChoiceType::class, [
                'choices'  => [
                    'Le' => 1,
                    'La' => 2,
                    'L\'' => 3,
                ],
                'constraints' => [
                    new NotBlank()
                ],
                'placeholder' => ' ',
                'label' => 'ARTICLE_CONSULTATION',
                'required' => true
            ])
            ->add('denominationOrg', TextType::class, [
                'label' => 'DENOMINATION'
            ])
            ->add('descriptionOrg', TextareaType::class, [
                'attr' => ['cols' => '60', 'rows' => '3'],
                'label' => 'DEFINE_DESCRIPTION'
            ])
            ->add('categorieInsee', EntityType::class, [
                'class' => CategorieINSEE::class,
                'choice_label' => function ($category) {
                    /** @var CategorieINSEE $category */
                    return $category->getId() . ' - ' . $category->getLibelle();
                },
                'placeholder' => '-- Choisissez une catégorie --',
                'label' => 'TEXT_CATEGORIE_JURIDIQUE'
            ])
            ->add('sousTypeOrganisme', ChoiceType::class, [
                'label' => 'Sous-catégorie d\'entité',
                'choices' => [
                    'ELIGIBLES_SEUIL_ETAT' => 1,
                    'ELIGIBLES_SEUIL_COLLECTIVITES' => 2,
                ],
                'expanded' => true,
                'multiple' => false,
                'required' => true,
            ])
            ->add('email', EmailType::class, [
                'label' => 'TEXT_E_MAIL',
                'required' => false,
            ])
            ->add('url', UrlType::class, [
                'label' => 'TEXT_SITE_INTERNET',
                'empty_data' => '',
                'required' => false,
            ])
            ->add('adresse', TextType::class)
            ->add('adresse2', TextType::class, [
                'label' => 'TEXT_ADRESSE_SUITE',
                'empty_data' => '',
                'required' => false,
            ])
            ->add('cp', TextType::class, [
                'label' => 'TEXT_CP'
            ])
            ->add('ville', TextType::class)
            ->add('pays', TextType::class)
            ->add('tel', TelType::class, [
                'label' => 'TEXT_TEL',
                'empty_data' => '',
                'required' => false,
            ])
            ->add('telecopie', TextType::class, [
                'label' => 'TEXT_FAX',
                'empty_data' => '',
                'required' => false,
            ])
            ->add('siren', TextType::class, [
                'label' => 'TEXT_SIREN',
            ])
            ->add('complement', TextType::class, [
                'label' => 'TEXT_SIRET',
                'attr' => ['maxlength' => 5]
            ])
            ->add('idEntite', NumberType::class, [
                'label' => 'TEXT_ID_ENTITE',
                'required' => false,
            ])
            ->add('logoSmall', FileType::class, [
                'label' => 'TEXT_LOGO_ORGANISME_PETIT',
                'mapped' => false,
                'required' => false,
                'attr' => ['style' => 'opacity: initial', 'data-size' => 'small', 'data-path' => $options['directory_logo']],
                'constraints' => [
                    new Image([
                        'maxHeight' => 50,
                        'maxWidth' => 50,
                        'mimeTypes' => ['image/jpeg'],
                    ])
                ],
            ])
            ->add('logoBig', FileType::class, [
                'label' => 'TEXT_LOGO_ORGANISME_GRAND',
                'mapped' => false,
                'required' => false,
                'attr' => ['style' => 'opacity: initial', 'data-size' => 'big', 'data-path' => $options['directory_logo']],
                'constraints' => [
                    new Image([
                        'maxHeight' => 92,
                        'maxWidth' => 100,
                        'mimeTypes' => ['image/jpeg'],
                    ])
                ],
            ])
            ;

        $builder->get('categorieInsee')
            ->addModelTransformer($this->transformer);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Organisme::class,
            'directory_logo' => ''
        ]);
    }
}
