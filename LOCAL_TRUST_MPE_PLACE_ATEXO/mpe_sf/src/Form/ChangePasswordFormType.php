<?php

namespace App\Form;

use App\Validator\EqualToNoCaseSensitive;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\EqualTo;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class ChangePasswordFormType extends AbstractType
{
    public const EMAIL_INVALID_MESSAGE = 'reset-password.form.error.email-invalid';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'attr' => [
                    'placeholder' => 'ADRESSE_ELECTRONIQUE',
                    'class' => 'form-control',
                    'aria-describedby' => "mail"
                ],
                'label' => false,
                'constraints' => [
                    new Email([
                        'message' => self::EMAIL_INVALID_MESSAGE
                    ]),
                    new NotBlank([
                        'message' => self::EMAIL_INVALID_MESSAGE
                    ]),
                    new EqualToNoCaseSensitive([
                        'message' => self::EMAIL_INVALID_MESSAGE,
                        'value' => $options['userEmail'] ?? '',
                    ])
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => [
                    'attr' => [
                        'placeholder' => 'TEXT_NOUVEAU_MOT_PASSE',
                        'class' => 'form-control',
                        'aria-describedby' => "password"
                    ],
                    'constraints' => [
                        new Regex([
                            'pattern' => '/(?=.{8,})((?=.*\d)(?=.*[a-z])(?=.*[A-Z])'
                                            . '|(?=.*\d)(?=.*[a-zA-Z])(?=.*[\W_])'
                                            . '|(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_])).*/',
                            'message' => 'reset-password.form.error.password-format',
                            'match' => true
                        ])
                    ],
                    'label' => 'New password',
                ],
                'second_options' => [
                    'attr' => [
                        'placeholder' => 'TEXT_CONFIRME_MOT_PASSE_SANS_POINTS',
                        'class' => 'form-control',
                        'aria-describedby' => "passwordconfirmation"
                    ],
                    'label' => 'Repeat Password',
                ],
                'invalid_message' => 'reset-password.form.error.not-same-password',
                // Instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'userEmail' => null,
            ]
        );
    }
}
