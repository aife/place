<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent;

use App\Entity\Agent;
use App\Entity\Organisme;
use App\Form\Agent\DataTransformer\MesAlertesTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Service\AtexoConfiguration;

class MesAlertesType extends AbstractType
{
    public function __construct(
        private TranslatorInterface $translator,
        private MesAlertesTransformer $transformer,
        private readonly AtexoConfiguration $atexoConfiguration
    ) {
    }

    public function getBlockPrefix(): string
    {
        return '';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Agent $agent */
        $agent = $builder->getData();
        /** @var Organisme $organisme */
        $organisme = $agent?->getOrganisme();

        $fields = [
            'alerteReponseElectronique' => [
                'label' => 'TEXT_INFORMATION_RECEPTION_ELECTRONIQUE_CONSULTATION_SYNCHRONE',
                'title' => 'TEXT_INFORMATION_RECEPTION_ELECTRONIQUE',
                'modules' => [
                    'recherchesFavorites',
                ],
            ],
            'alerteClotureConsultation' => [
                'label' => 'TEXT_INFORMATION_CLOTURE_CONSULTATION_SUIVIE_DEPASSEMENT',
                'title' => 'TEXT_INFORMATION_CLOTURE_CONSULTATION_SUIVIE',
                'modules' => [
                    'recherchesFavorites',
                ],
            ],
            'alerteReceptionMessage' => [
                'label' => 'TEXT_INFORMATION_RECEPTION_MESSAGE_SOLLICITE',
                'title' => 'TEXT_INFORMATION_RECEPTION_MESSAGE',
                'modules' => [
                    'recherchesFavorites',
                ],
            ],
            'alerteQuestionEntreprise' => [
                'label' => 'TEXT_INFORMATION_RECEPTION_QUESTION_POSEE',
                'title' => 'TEXT_INFORMATION_RECEPTION_QUESTION_POSEE',
                'modules' => [
                    'recherchesFavorites',
                ],
            ],
            'alertePublicationBoamp' => [
                'label' => 'TEXT_INFORMATION_PUBLICATION_BOAMP_CONSULTATION_SUIVIE',
                'title' => 'TEXT_INFORMATION_PUBLICATION_BOAMP_CONSULTATION_SUIVIE',
                'modules' => [
                    'recherchesFavorites',
                    'publiciteFormatXml',
                ],
            ],
            'alerteEchecPublicationBoamp' => [
                'label' => 'TEXT_INFORMATION_ECHEC_PUBLICATION_BOAMP_CONSULTATION_SUIVIE',
                'title' => 'TEXT_INFORMATION_ECHEC_PUBLICATION_BOAMP_CONSULTATION_SUIVIE',
                'modules' => [
                    'recherchesFavorites',
                    'publiciteFormatXml',
                ],
            ],
            'alerteValidationConsultation' => [
                'label' => 'TEXT_INFORMATION_CONSULTATION_EN_ATTENTE_VALIDATION',
                'title' => 'TEXT_INFORMATION_CONSULTATION_EN_ATTENTE_VALIDATION',
                'modules' => [
                    'recherchesFavorites',
                ],
            ],
            'alerteMesConsultations' => [
                'label' => 'TEXT_MES_CONSULTATIONS',
                'title' => 'TEXT_MES_CONSULTATIONS',
                'modules' => [
                    'recherchesFavorites',
                ],
            ],
            'alerteConsultationsMonEntite' => [
                'label' => 'TEXT_MES_CONSULTATIONS_MON_ENTITE',
                'title' => 'TEXT_MES_CONSULTATIONS_MON_ENTITE',
                'modules' => [
                    'recherchesFavorites',
                ],
            ],
            'alerteConsultationsDesEntitesDependantes' => [
                'label' => 'TEXT_MES_CONSULTATIONS_DES_ENTITE_DEPENDENTE',
                'title' => 'TEXT_MES_CONSULTATIONS_DES_ENTITE_DEPENDENTE',
                'modules' => [
                    'recherchesFavorites',
                    'organisationCentralisee',
                ],
            ],
            'alerteConsultationsMesEntitesTransverses' => [
                'label' => 'TEXT_MES_CONSULTATIONS_MES_ENTITE_ASSOCIEES_TRANSVERSALE',
                'title' => 'TEXT_MES_CONSULTATIONS_MES_ENTITE_ASSOCIEES_TRANSVERSALE',
                'modules' => [
                    'recherchesFavorites',
                    'organisationCentralisee',
                ],
            ],
            'alerteCreationModificationAgent' => [
                'label' => 'TEXT_CREATION_MODIFICATION_COMPTE_AGENT',
                'title' => 'TEXT_CREATION_MODIFICATION_COMPTE_AGENT',
                'modules' => [],
            ],
            'alerteChorus' => [
                'label' => 'DEFINE_ALERTES_CHORUS',
                'title' => 'DEFINE_ALERTES_CHORUS',
                'modules' => [
                    'chorus',
                ],
            ],
            'alerteConsultationsInvitePonctuel' => [
                'label'   => 'TEXT_MES_CONSULTATIONS_INVITE_PONCTUEL',
                'title'   => 'TEXT_MES_CONSULTATIONS_INVITE_PONCTUEL',
                'modules' => [],
            ],
        ];

        $builder->add(
            'id',
            HiddenType::class
        );

        foreach ($fields as $fieldName => $field) {
            $modules = $field['modules'];

            if (!empty($modules)) {
                $result = true;

                foreach ($modules as $module) {
                    switch ($module) {
                        case 'recherchesFavorites':
                            $result &= $this->atexoConfiguration->isModuleEnabled('RecherchesFavorites', $organisme);
                            break;
                        case 'publiciteFormatXml':
                            $result &= $this->atexoConfiguration->isModuleEnabled('PubliciteFormatXml', $organisme);
                            break;
                        case 'organisationCentralisee':
                            if (!$organisme instanceof Organisme) {
                                $result &= false;
                            } else {
                                $result &= $this->atexoConfiguration
                                    ->isModuleEnabled(
                                        'OrganisationCentralisee',
                                        $organisme
                                    );
                            }
                            break;
                        case 'chorus':
                            $result &= $this->atexoConfiguration->isChorusAccessActivate($agent);
                            break;
                        default:
                            break;
                    }
                }

                if (!$result) {
                    continue;
                }
            }

            $builder->add(
                $fieldName,
                CheckboxType::class,
                [
                    'label'     => $this->translator->trans($field['label']),
                    'required'  => false,
                    'attr'      => [
                        'title' => $this->translator->trans($field['title']),
                    ],
                ]
            )
                ->get($fieldName)
                ->addModelTransformer($this->transformer);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Agent::class,
        ]);
    }
}
