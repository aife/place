<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent;

use App\Entity\Agent;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Filter\GroupBySearchFilter;
use App\Form\Agent\FieldType\LieuxExecutionSelectType;
use App\Form\Agent\FieldType\LieuxExecutionType;
use App\Service\configurationPlatformService;
use Application\Service\Atexo\Atexo_Util;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConsultationRechercheType extends AbstractType
{
    public function __construct(
        private readonly ParameterBagInterface $parameterBag
    ) {
    }

    public function getBlockPrefix()
    {
        return '';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $choices = [
            'DEFINE_ELABORATION' => $this->parameterBag->get('STATUS_ELABORATION'),
            'DEFINE_ATTENTE_VALIDATION' => $this->parameterBag->get('STATUS_PREPARATION'),
            'DEFINE_CONSULTATION' => $this->parameterBag->get('STATUS_CONSULTATION'),
            'DEFINE_OUVERTURE_ANALYSE' => $this->parameterBag->get('STATUS_OUVERTURE_ANALYSE'),
            'DEFINE_DECISION' => $this->parameterBag->get('STATUS_DECISION'),
        ];

        $builder
            ->setMethod(Request::METHOD_GET)
            ->add('keyWord', TextType::class, ['label' => 'DEFINE_MOTS_CLES'])
            ->add('typeProcedureOrganisme', ChoiceType::class, [
                'choices' => $options['types_procedures'],
                'choice_value' => 'id_type_procedure',
                'choice_label' => fn(?TypeProcedureOrganisme $typeProcedure) => $typeProcedure ? $typeProcedure->getLibelleTypeProcedure() : '',
                'placeholder' => '',
                'multiple' => false,
                'expanded' => false,
            ])
            ->add(
                'statut',
                ChoiceType::class,
                [
                    'choices' => $options['rma'] == '1' ? array_diff_key($choices, array_flip(['DEFINE_ELABORATION', 'DEFINE_ATTENTE_VALIDATION'])) : $choices,
                    'choice_attr' => [
                        'DEFINE_ELABORATION' => ['data-icon' => 'ft-edit-1'],
                        'DEFINE_ATTENTE_VALIDATION' => ['data-icon' => 'ft-check-circle'],
                        'DEFINE_CONSULTATION' => ['data-icon' => 'ft-globe'],
                        'DEFINE_OUVERTURE_ANALYSE' => ['data-icon' => 'ft-bar-chart-2'],
                        'DEFINE_DECISION' => ['data-icon' => 'ft-award'],
                    ],
                    'multiple' => true,
                    'expanded' => true,
                    'attr' => [
                        'class' => 'btn-group btn-group-toggle d-flex flex-column flex-md-row',
                        'data-toggle' => 'buttons',
                        'role' => 'group',
                    ],
                ]
            )
            ->add(
                'categorie',
                ChoiceType::class,
                [
                    'choices' => [
                        'DEFINE_TRAVAUX' => $this->parameterBag->get('TYPE_PRESTATION_TRAVAUX'),
                        'DEFINE_FOURNITURES' => $this->parameterBag->get('TYPE_PRESTATION_FOURNITURES'),
                        'DEFINE_SERVICES' => $this->parameterBag->get('TYPE_PRESTATION_SERVICES'),
                    ],
                    'choice_attr' => [
                        'DEFINE_TRAVAUX' => ['data-icon' => 'la la-wrench'],
                        'DEFINE_FOURNITURES' => ['data-icon' => 'ft-shopping-cart'],
                        'DEFINE_SERVICES' => ['data-icon' => 'ft-briefcase'],
                    ],
                    'multiple' => true,
                    'expanded' => true,
                    'attr' => [
                        'class' => 'btn-group btn-group-toggle d-flex flex-column flex-md-row',
                        'data-toggle' => 'buttons',
                        'role' => 'group',
                    ],
                ]
            );

        $configurationOrganismeService = Atexo_Util::getSfContainer()->get(configurationPlatformService::class);
        if ($configurationOrganismeService->moduleIsEnabled('lieuxExecutionCarte', '1')) {
            $builder->add(
                'form_lieuxExecutionSelect',
                LieuxExecutionSelectType::class,
                ['choices' => $options['lieuxExecution']]
            )
                ->add(
                    'form_lieuxExecution',
                    LieuxExecutionType::class,
                    ['data' => $options['lieuxExecutionIds']]
                );
        }
        $builder->add('dlroApres', DateType::class, ['widget' => 'single_text'])
                ->add('dlroAvant', DateType::class, ['widget' => 'single_text'])
                ->add('miseEnLigneApres', DateType::class, ['widget' => 'single_text'])
                ->add('miseEnLigneAvant', DateType::class, ['widget' => 'single_text']);


        if ($this->parameterBag->get('ACTIVER_LISTE_AGENTS_TABLEAU_BORD')) {
            $builder->add('idCreateur', ChoiceType::class, [
                    'choice_value' => 'id',
                    'placeholder' => '',
                    'multiple' => false,
                    'expanded' => false,
                    'attr' => [
                        'class' => 'selectAgentReferent',
                    ],
                ]);
        }

        $builder->add('search', SubmitType::class, [
                'label' => 'RECHERCHER',
                'icon_before' => 'fa fa-search',
                'attr' => [
                    'class' => 'btn btn-primary',
                ],
            ])
            ->add('id', HiddenType::class)
        ;

        if ($options['isStat']) {
            $builder
                ->add(
                    'groupBy',
                    ChoiceType::class,
                    [
                        'choices' => GroupBySearchFilter::GROUP_BY_ACCEPT
                    ]
                )
                ->add('pagination', TextType::class, ['empty_data' => false]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'error_mapping' => [
                    'form_lieuExecution' => 'form_lieuExecutionSelect',
                ],
                'attr' => [
                    'novalidate' => 'novalidate',
                ],
                'lieux' => [],
                'ids' => [],
                'types_procedures' => new ArrayCollection(),
                'lieuxExecution' => new ArrayCollection(),
                'lieuxExecutionIds' => '',
                'csrf_protection' => false,
                'isStat' => false,
                'agents_createur' => new ArrayCollection(),
                'rma' => '',
            ]
        );
    }
}
