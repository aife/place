<?php

namespace App\Form\Agent;

use App\Entity\MessageAccueil;
use App\Form\Agent\Enum\MessageAccueilInfo;
use App\Form\Agent\Trait\TypeMessage;
use App\Repository\MessageAccueilRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MessageAccueilAgentConnecte extends AbstractType
{
    use TypeMessage;

    public MessageAccueilInfo $destinataire = MessageAccueilInfo::DESTINATAIRE;
    public MessageAccueilInfo $authentifier = MessageAccueilInfo::AUTHENTIFIER;

    public function __construct(private readonly MessageAccueilRepository $messageAccueilRepository)
    {
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $selectedTypeMessage = $this->messageAccueilRepository->getTypeMessage(
            $this->destinataire->authenticatedAgent(),
            $this->authentifier->authenticatedAgent()
        );

        $builder
            ->add('typeMessage', ChoiceType::class, [
                'choices' => array_flip($this->getTypeMessage()),
                'attr' => ['class' => "form-select mt-2"],
                'data' => $selectedTypeMessage,
            ])
            ->add('agentAuthenticated', CKEditorType::class, [
                'data' => $this->messageAccueilRepository->getContenu(
                    $this->destinataire->authenticatedAgent(),
                    $this->authentifier->authenticatedAgent()
                ),
                'mapped' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MessageAccueil::class,
        ]);
    }
}
