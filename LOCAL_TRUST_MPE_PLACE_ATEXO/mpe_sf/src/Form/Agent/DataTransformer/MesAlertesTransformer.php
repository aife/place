<?php

namespace App\Form\Agent\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class MesAlertesTransformer implements DataTransformerInterface
{
    public function transform($value): bool
    {
        return boolval(intval($value));
    }

    public function reverseTransform($value): string
    {
        if (empty($value)) {
            return '0';
        }

        return strval($value);
    }
}
