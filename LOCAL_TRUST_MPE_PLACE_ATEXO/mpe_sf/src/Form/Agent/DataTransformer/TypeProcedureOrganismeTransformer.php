<?php

namespace App\Form\Agent\DataTransformer;

use App\Entity\Procedure\TypeProcedureOrganisme;
use Application\Service\Atexo\Atexo_CurrentUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class TypeProcedureOrganismeTransformer implements DataTransformerInterface
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    public function transform($value): string
    {
        if ($value instanceof TypeProcedureOrganisme) {
            return $value->concatTypeProcedureOrganisme();
        }

        return '';
    }

    public function reverseTransform($value): ?TypeProcedureOrganisme
    {
        $explode = explode('_', (string) $value);

        if (!is_countable($explode)) {
            return null;
        }

        $typeProcedureOrganisme = $this->entityManager
            ->getRepository(TypeProcedureOrganisme::class)
            ->findOneBy([
                'idTypeProcedure' => (int) $explode[0],
                'organisme' => Atexo_CurrentUser::getOrganismAcronym(),
            ]);

        if (null === $typeProcedureOrganisme) {
            throw new TransformationFailedException(
                sprintf('Le couple typeProcedure / Organisme "%s" n\'existe pas', $value)
            );
        }

        return $typeProcedureOrganisme;
    }
}
