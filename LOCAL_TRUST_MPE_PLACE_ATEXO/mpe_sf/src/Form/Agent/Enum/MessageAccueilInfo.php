<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Enum;

enum MessageAccueilInfo
{
    case DESTINATAIRE;
    case AUTHENTIFIER;

    public function authenticatedAgent(): string
    {
        return match ($this) {
            MessageAccueilInfo::DESTINATAIRE => 'agent',
            MessageAccueilInfo::AUTHENTIFIER => 1,
        };
    }

    public function unauthenticatedAgent(): string
    {
        return match ($this) {
            MessageAccueilInfo::DESTINATAIRE => 'agent',
            MessageAccueilInfo::AUTHENTIFIER => 0,
        };
    }

    public function companies(): string
    {
        return match ($this) {
            MessageAccueilInfo::DESTINATAIRE => 'entreprise',
            MessageAccueilInfo::AUTHENTIFIER => '',
        };
    }
}
