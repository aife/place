<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Trait;

trait TypeMessage
{
    public function getTypeMessage(): array
    {
        return [
            'info' => 'Informatif (bleu)',
            'avertissement' => 'Avertissement (orange)',
            'erreur' => 'Erreur (rouge)'
        ];
    }
}
