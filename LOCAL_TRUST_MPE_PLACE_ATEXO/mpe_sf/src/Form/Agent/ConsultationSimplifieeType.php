<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent;

use App\Entity\Consultation;
use App\Entity\ProcedureEquivalence;
use App\Form\Agent\FieldType\AnnonceType;
use App\Form\Agent\FieldType\CategorieConsultationType;
use App\Form\Agent\FieldType\CodeProcedureType;
use App\Form\Agent\FieldType\CommentaireInterneType;
use App\Form\Agent\FieldType\DateFinDateType;
use App\Form\Agent\FieldType\DateFinHeureType;
use App\Form\Agent\FieldType\DCEAccessType;
use App\Form\Agent\FieldType\DCEType;
use App\Form\Agent\FieldType\IntituleType;
use App\Form\Agent\FieldType\LieuxExecutionSelectType;
use App\Form\Agent\FieldType\LieuxExecutionType;
use App\Form\Agent\FieldType\MarcheType;
use App\Form\Agent\FieldType\ObjetType;
use App\Form\Agent\FieldType\ProcedureType;
use App\Form\Agent\FieldType\ReferenceType;
use App\Form\Agent\Validator\Constraints\ConsultationSimplifiee;
use Datetime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConsultationSimplifieeType extends AbstractType
{
    public function __construct(private EntityManagerInterface $em)
    {
    }

    public function onPreSubmit(FormEvent $event): void
    {
        $form = $event->getForm();
        $data = (array) $event->getData();

        if (array_key_exists('typeProcedureOrganismeHidden', $data)) {
            $data['typeProcedureOrganisme'] = $data['typeProcedureOrganismeHidden'];
        }

        if (array_key_exists('typeProcedureOrganisme', $data)) {
            $typeProcedureOrganisme = explode('_', $data['typeProcedureOrganisme']);
            $procedureEquivalence = $this->em->getRepository(ProcedureEquivalence::class)->findOneBy(
                [
                    'idTypeProcedure' => $typeProcedureOrganisme[0],
                    'organisme' => $typeProcedureOrganisme[1],
                ]
            );

            if ($procedureEquivalence instanceof ProcedureEquivalence) {
                $procedurePublicite = $procedureEquivalence->getProcedurePublicite();
                $procedureRestreint = $procedureEquivalence->getProcedureRestreinte();
                if ($procedurePublicite === '1' || $procedureRestreint === '1') {
                    $form->add(
                        'typeAcces',
                        DCEAccessType::class,
                        [
                            'constraints' => [],
                        ]
                    );
                    if ($procedurePublicite === '1') {
                        $data['typeAcces'] = Consultation::TYPE_ACCES_PUBLIC;
                    }
                    if ($procedureRestreint === '1') {
                        $data['typeAcces'] = Consultation::TYPE_ACCES_RESTREINT;
                    }
                    $event->setData($data);
                }
            }
        }
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $typeMarcheDiabled = [];
        $typeProcedureOrganismeDiabled = ['choices' => []];
        if (null !== $options['data']->getId()) {
            $typeMarcheDiabled =  ['disabled' => true];
            $typeProcedureOrganismeDiabled = ['disabled' => true, 'choices' => []];
        }

        $builder
            ->add('avisType', AnnonceType::class, ['disabled' => true])
            ->add('typeMarche', MarcheType::class, $typeMarcheDiabled)
            ->add('typeProcedureOrganisme', ProcedureType::class, $typeProcedureOrganismeDiabled)
            ->add('categorieConsultation', CategorieConsultationType::class)
            ->add('referenceUtilisateur', ReferenceType::class)
            ->add('intitule', IntituleType::class)
            ->add('objet', ObjetType::class)
            ->add('champSuppInvisible', CommentaireInterneType::class, ['required' => false])
            ->add('lieuxExecutionSelect', LieuxExecutionSelectType::class, ['choices' => $options['lieux']])
            ->add('lieuExecution', LieuxExecutionType::class, ['data' => $options['ids']])
            ->add('datefinDate', DateFinDateType::class)
            ->add('datefinHeure', DateFinHeureType::class)
            ->add('dce', DCEType::class, ['mapped' => false])
            ->add('dce_filepath', HiddenType::class, ['mapped' => false])
            ->add('typeAcces', DCEAccessType::class)
            ->add('codeProcedureInput', CodeProcedureType::class)
            ->add('codeProcedure', HiddenType::class)
            ->add('actionType', HiddenType::class, [
                'mapped' => false,
                'attr' => [
                            'data-ajax-custom' => 'actionType',
                        ],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'TEXT_ENREGISTER',
                'icon_before' => 'fa fa-save',
                'attr' => [
                    'class' => 'btn btn-primary',
                ],
            ])
            ->add('saveAndQuitte', SubmitType::class, [
                'label' => 'TEXT_ENREGISTER_QUITTER',
                'icon_before' => 'fa fa-sign-out-alt',
                'attr' => [
                    'class' => 'btn btn-primary',
                ],
            ])
            ->add('saveAndValidate', SubmitType::class, [
                'label' => 'TEXT_DEMANDE_VALIDATION',
                'icon_before' => 'fa fa-check-square',
                'attr' => [
                    'class' => 'btn btn-success',
                    'data-toggle' => 'modal',
                    'data-target' => '#modaleSucces',
                ],
            ])
        ;
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $consultation =  $event->getData();
            $form = $event->getForm();
            if (null !== $consultation->getId()) {
                $idTypeProcedureOrganismeSelected = $consultation->getTypeProcedureOrganisme()->getIdTypeProcedure();
                $organisme = $consultation->getOrganisme()->getAcronyme();
                $form->add(
                    'typeProcedureOrganismeHidden',
                    HiddenType::class,
                    [
                        'data' => $idTypeProcedureOrganismeSelected.'_'.$organisme,
                        'mapped' => false,
                    ]
                );
            }
        });
        $builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'onPreSubmit']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Consultation::class,
            'attr' => [
                'novalidate' => 'novalidate',
            ],
            'lieux' => [],
            'ids' => [],
            'date' => new DateTime(),
            'constraints' => [
                new ConsultationSimplifiee(),
            ],
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'form';
    }
}
