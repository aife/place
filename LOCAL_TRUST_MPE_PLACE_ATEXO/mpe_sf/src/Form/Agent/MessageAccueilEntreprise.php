<?php

namespace App\Form\Agent;

use App\Entity\MessageAccueil;
use App\Form\Agent\Enum\MessageAccueilInfo;
use App\Form\Agent\Trait\TypeMessage;
use App\Repository\MessageAccueilRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MessageAccueilEntreprise extends AbstractType
{
    use TypeMessage;

    public MessageAccueilInfo $destinataire = MessageAccueilInfo::DESTINATAIRE;

    public function __construct(private readonly MessageAccueilRepository $messageAccueilRepository)
    {
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $selectedTypeMessage = $this->messageAccueilRepository->getTypeMessage($this->destinataire->companies());

        $builder
            ->add('typeMessage', ChoiceType::class, [
                'attr' => ['class' => "form-select mt-2"],
                'choices' => array_flip($this->getTypeMessage()),
                'data' => $selectedTypeMessage,
            ])
            ->add('companies', CKEditorType::class, [
                'data' => $this->messageAccueilRepository->getContenu($this->destinataire->companies()),
                'mapped' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MessageAccueil::class,
        ]);
    }
}
