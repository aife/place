<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\New;

use App\Entity\Agent;
use App\Service\Agent\HabilitationTypeProcedureService;
use App\Service\AtexoConfiguration;
use App\Service\Procedure\TypeProcedureService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractHabilitationNewType extends AbstractType
{
    public const BLOCK_NAME = '';
    protected ?Agent $agent = null;
    protected ?int $mode = null;


    public function __construct(
        protected readonly TranslatorInterface $translator,
        protected readonly AtexoConfiguration $atexoConfiguration,
        protected readonly TypeProcedureService $typeProcedureService,
        protected readonly HabilitationTypeProcedureService $habilitationTypeProcedureService,
        protected readonly ParameterBagInterface $parameterBag
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->agent = $options['agent'];
        $this->mode = $options['mode'];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'block_name' => static::BLOCK_NAME,
            'agent' => null,
            'mode' => null
        ]);
    }
}
