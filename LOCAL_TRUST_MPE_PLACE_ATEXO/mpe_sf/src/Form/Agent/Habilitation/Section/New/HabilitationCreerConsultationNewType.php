<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\New;

use App\Entity\HabilitationAgent;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Enum\Habilitation\HabilitationSlug;
use App\Service\Agent\HabilitationTypeProcedureService;
use App\Service\AtexoConfiguration;
use App\Service\Procedure\TypeProcedureService;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class HabilitationCreerConsultationNewType extends AbstractHabilitationNewType
{
    public const BLOCK_NAME = 'habilitation_creer_consultation_new';
    private $sections = [
        'gerer_mapa_inferieur_montant' => [
            'label' => 'TEXT_MAPA_INFERIEUR_90000',
        ],
        'gerer_mapa_superieur_montant' => [
            'label' => 'TEXT_MAPA_SUPERIEUR_90000',
        ],
        'administrer_procedures_formalisees' => [
            'label' => 'TEXT_PROCEDURES_FORMALISEES',
        ],
        'administrer_procedures_simplifiees' => [
            'label' => 'TEXT_PROCEDURES_SIMPLIFIEES',
        ],
    ];

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $this->deactivateFields();

        $this->buildSections($builder);
        $habilitation = $this->habilitationTypeProcedureService->getHabilitationsBySlug(
            $this->agent,
            HabilitationSlug::CREER_CONSULTATION_TRANSVERSE()
        );

        $builder
            ->add('creer_consultation_transverse', CheckboxType::class, [
                'label' => $this->translator->trans('TEXT_CREER_CONSULTATION_TRANSVERSE'),
                'disabled' => $this->mode == 2 ? true : false,
                'required' => false,
                'data' => !empty($habilitation),
                'attr' => [
                    'data-style' => 'right',
                    'data-habilitation-slug' => 'creer_consultation_transverse',
                ],
            ]);
    }

    private function buildSections(FormBuilderInterface $builder)
    {
        foreach ($this->sections as $section => $data) {
            $sectionBuilder = $builder
                ->create($section, FormType::class, [
                    'label' => $this->translator->trans($data['label']),
                    'required' => false,
                    'mapped' => false,
                    'attr' => [
                        'data-habilitation-slug' => $section,
                    ],
                ]);

            $this->buildTypeProcedure($section, $sectionBuilder);

            if (count($sectionBuilder->all()) > 0) {
                $builder->add($sectionBuilder);
            }
        }
    }

    private function buildTypeProcedure(
        string $section,
        FormBuilderInterface $sectionBuilder
    ) {
        $typeProcedures = [];
        $organisme = $this->agent->getOrganisme();

        $habilitations = $this->habilitationTypeProcedureService->getHabilitationsBySlug(
            $this->agent,
            $section
        );

        $typeProcedureIdsHabilited = array_map(function ($habilitation) {
            return $habilitation->getTypeProcedure();
        }, $habilitations);

        switch ($section) {
            case HabilitationSlug::GERER_MAPA_INFERIEUR_MONTANT():
                $typeProcedures = $this->typeProcedureService->getTypeProcedureMapaInfByOrganisme(
                    $organisme->getAcronyme()
                );
                break;
            case HabilitationSlug::GERER_MAPA_SUPERIEUR_MONTANT():
                $typeProcedures = $this->typeProcedureService->getTypeProcedureMapaSupByOrganisme(
                    $organisme->getAcronyme()
                );
                break;
            case HabilitationSlug::ADMINISTRER_PROCEDURES_FORMALISEES():
                $typeProcedures = $this->typeProcedureService->getTypeProcedureFormaliseeByOrganisme(
                    $organisme->getAcronyme()
                );
                break;
            case HabilitationSlug::ADMINISTRER_PROCEDURES_SIMPLIFIEES():
                $typeProcedures = $this->typeProcedureService->getTypeProcedureSimplifieeByOrganisme(
                    $organisme->getAcronyme()
                );
                break;
            default:
                break;
        }

        /** @var TypeProcedureOrganisme $typeProcedure */
        foreach ($typeProcedures as $typeProcedure) {
            $sectionBuilder
                ->add($typeProcedure->getIdTypeProcedure(), CheckboxType::class, [
                    'label' => html_entity_decode($typeProcedure->getLibelleTypeProcedure()),
                    'required' => false,
                    'disabled' => $this->mode == 2 ? true : false,
                    'data' => in_array($typeProcedure->getIdTypeProcedure(), $typeProcedureIdsHabilited),
                    'attr' => [
                        'data-style' => 'right',
                        'data-habilitation-slug' => $section,
                    ],
                ]);
        }
    }

    private function deactivateFields()
    {
        $fieldsToRemove = [];

        if (!$this->atexoConfiguration->hasConfigPlateforme('procedureAdaptee')) {
            $fieldsToRemove[] = 'gerer_mapa_inferieur_montant';
            $fieldsToRemove[] = 'gerer_mapa_superieur_montant';
        }

        foreach ($fieldsToRemove as $fieldToRemove) {
            unset($this->sections[$fieldToRemove]);
        }
    }
}
