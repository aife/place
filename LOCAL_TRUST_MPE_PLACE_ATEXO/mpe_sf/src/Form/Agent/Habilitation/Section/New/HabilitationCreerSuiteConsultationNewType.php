<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\New;

use App\Entity\HabilitationAgent;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Enum\Habilitation\HabilitationSlug;
use App\Service\Agent\HabilitationTypeProcedureService;
use App\Service\AtexoConfiguration;
use App\Service\Procedure\TypeProcedureService;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class HabilitationCreerSuiteConsultationNewType extends AbstractHabilitationNewType
{
    public const BLOCK_NAME = 'habilitation_creer_suite_consultation_new';

    private $sections = [
        'gerer_mapa_inferieur_montant_suite' => [
            'label' => 'TEXT_MAPA_INFERIEUR_90000',
        ],
        'gerer_mapa_superieur_montant_suite' => [
            'label' => 'TEXT_MAPA_SUPERIEUR_90000',
        ],
        'administrer_procedures_formalisees_suite' => [
            'label' => 'TEXT_PROCEDURES_FORMALISEES',
        ],
    ];

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $this->mode = $options['mode'];
        $this->deactivateFields();

        $this->buildSections($builder);
    }

    private function buildSections(FormBuilderInterface $builder)
    {
        foreach ($this->sections as $section => $data) {
            $sectionBuilder = $builder
                ->create($section, FormType::class, [
                    'label' => $this->translator->trans($data['label']),
                    'disabled' => $this->mode == 2 ? true : false,
                    'required' => false,
                    'mapped' => false,
                    'attr' => [
                        'data-habilitation-slug' => $section,
                    ],
                ]);

            $this->buildTypeProcedure($section, $sectionBuilder);

            if (count($sectionBuilder->all()) > 0) {
                $builder->add($sectionBuilder);
            }
        }
    }

    private function buildTypeProcedure(
        string $section,
        FormBuilderInterface $sectionBuilder
    ) {
        $typeProcedures = [];
        $organisme = $this->agent->getOrganisme();

        $habilitations = $this->habilitationTypeProcedureService->getHabilitationsBySlug(
            $this->agent,
            $section
        );

        $typeProcedureIdsHabilited = array_map(function ($habilitation) {
            return $habilitation->getTypeProcedure();
        }, $habilitations);

        switch ($section) {
            case HabilitationSlug::GERER_MAPA_INFERIEUR_MONTANT_SUITE():
                $typeProcedures = $this->typeProcedureService->getTypeProcedureMapaInfByOrganisme(
                    $organisme->getAcronyme()
                );
                break;
            case HabilitationSlug::GERER_MAPA_SUPERIEUR_MONTANT_SUITE():
                $typeProcedures = $this->typeProcedureService->getTypeProcedureMapaSupByOrganisme(
                    $organisme->getAcronyme()
                );
                break;
            case HabilitationSlug::ADMINISTRER_PROCEDURES_FORMALISEES_SUITE():
                $typeProcedures = $this->typeProcedureService->getTypeProcedureFormaliseeByOrganisme(
                    $organisme->getAcronyme()
                );
                break;
            default:
                break;
        }

        /** @var TypeProcedureOrganisme $typeProcedure */
        foreach ($typeProcedures as $typeProcedure) {
            $sectionBuilder
                ->add($typeProcedure->getIdTypeProcedure(), CheckboxType::class, [
                    'label' => html_entity_decode($typeProcedure->getLibelleTypeProcedure()),
                    'required' => false,
                    'data' => in_array($typeProcedure->getIdTypeProcedure(), $typeProcedureIdsHabilited),
                    'attr' => [
                        'data-style' => 'right',
                        'data-habilitation-slug' => $section,
                    ],
                ]);
        }
    }

    private function deactivateFields()
    {
        $fieldsToRemove = [];

        if (!$this->atexoConfiguration->hasConfigPlateforme('procedureAdaptee')) {
            $fieldsToRemove[] = 'gerer_mapa_inferieur_montant_suite';
            $fieldsToRemove[] = 'gerer_mapa_superieur_montant_suite';
        }

        foreach ($fieldsToRemove as $fieldToRemove) {
            unset($this->sections[$fieldToRemove]);
        }
    }
}
