<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\New;

use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Enum\Habilitation\HabilitationSlug;
use App\Service\Agent\HabilitationTypeProcedureService;
use App\Service\AtexoConfiguration;
use App\Service\Procedure\TypeProcedureService;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class HabilitationGestionPerimetreNewType extends AbstractHabilitationNewType
{
    public const BLOCK_NAME = 'habilitation_gestion_perimetre_new';
    private $fields = [
        'invite_permanent_mon_entite' => [
            'label' => 'TEXT_INVITE_PERMANENT_MON_ENTITE',
        ],
        'invite_permanent_entite_dependante' => [
            'label' => 'TEXT_INVITE_PERMANENT_ENTITE_DEPENDANTE',
        ],
        'invite_permanent_transverse' => [
            'label' => 'TEXT_INVITE_PERMANENT_TRANSVERSE',
        ],
    ];

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $agent = $options['agent'];
        $mode = $options['mode'];

        $this->deactivateFields($agent);

        foreach ($this->fields as $field => $datas) {
            $habilitation = $this->habilitationTypeProcedureService->getHabilitationsBySlug(
                $agent,
                $field
            );
            $builder
                ->add($field, CheckboxType::class, [
                    'label' => $this->translator->trans($datas['label']),
                    'data' => !empty($habilitation),
                    'disabled' => $mode == 2 ? true : false,
                    'required' => false,
                    'attr' => [
                        'data-style' => 'right',
                        'data-habilitation-slug' => $field,
                    ],
                ]);
        }
    }

    private function deactivateFields(Agent $agent)
    {
        $fieldsToRemove = [];

        if (!$this->atexoConfiguration->hasConfigOrganisme($agent->getOrganisme(), 'organisationCentralisee')) {
            $fieldsToRemove[] = HabilitationSlug::INVITE_PERMANENT_ENTITE_DEPENDANTE();
            $fieldsToRemove[] = HabilitationSlug::INVITE_PERMANENT_TRANSVERSE();
        }

        if ($this->parameterBag->get('SPECIFIQUE_PLACE')) {
            $fieldsToRemove[] = HabilitationSlug::INVITE_PERMANENT_ENTITE_DEPENDANTE();
            $fieldsToRemove[] = HabilitationSlug::INVITE_PERMANENT_TRANSVERSE();
        }

        foreach ($fieldsToRemove as $fieldToRemove) {
            unset($this->fields[$fieldToRemove]);
        }
    }
}
