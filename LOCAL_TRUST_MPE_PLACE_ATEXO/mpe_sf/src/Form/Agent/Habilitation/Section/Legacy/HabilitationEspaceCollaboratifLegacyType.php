<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\Legacy;

use App\Entity\Agent;

class HabilitationEspaceCollaboratifLegacyType extends AbstractHabilitationLegacyType
{
    protected $fields = [
        'espaceCollaboratifGestionnaire' => 'LEGACY_HABILITATION_GESTIONNAIRE_ESPACE_COLLABORATIF',
        'espaceCollaboratifContributeur' => 'LEGACY_HABILITATION_GESTIONNAIRE_ESPACE_CONTRIBUTEUR',
    ];

    protected $section = 'espaceCollaboratifSection';
}
