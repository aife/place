<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\Legacy;

class HabilitationResponsableMinisterielAchatLegacyType extends AbstractHabilitationLegacyType
{
    protected $fields = [
        'profilRma' => 'TEXT_VISION_RESPONSABLE_MINISTERIEL_ACHAT',
    ];

    protected $section = 'responsableMinisterielAchatSection';
}
