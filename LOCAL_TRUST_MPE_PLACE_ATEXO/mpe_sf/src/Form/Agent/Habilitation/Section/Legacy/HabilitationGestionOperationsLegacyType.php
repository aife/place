<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\Legacy;

class HabilitationGestionOperationsLegacyType extends AbstractHabilitationLegacyType
{
    protected $fields = [
        'gererOperations' => 'DEFINE_GESTION_OPERATIONS',
    ];

    protected $section = 'gestionOperationsSection';
}
