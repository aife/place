<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\Legacy;

use App\Entity\Agent;

class HabilitationDocumentsModelesLegacyType extends AbstractHabilitationLegacyType
{
    protected $fields = [
        'administrationDocumentsModeles' => 'TEXT_ADMINISTRATION_DOCUMENTS_MODELES',
    ];

    protected $section = 'documentsModelesSection';
}
