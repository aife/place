<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\Legacy;

use App\Entity\Agent;

class HabilitationGestionPerimetreLegacyType extends AbstractHabilitationLegacyType
{
    protected $fields = [
        'invitePermanentMonEntite'         => 'TEXT_INVITE_PERMANENT_MON_ENTITE',
        'invitePermanentEntiteDependante'  => 'TEXT_INVITE_PERMANENT_ENTITE_DEPENDANTE',
        'invitePermanentTransverse'        => 'TEXT_INVITE_PERMANENT_TRANSVERSE',
    ];

    protected $section = 'gestionPerimetreInterventionSection';

    protected function deactivateFields(Agent $agent)
    {
        $fieldsToRemove = [];

        if (!$this->atexoConfiguration->hasConfigOrganisme($agent->getOrganisme(), 'organisationCentralisee')) {
            $fieldsToRemove[] = 'invitePermanentEntiteDependante';
            $fieldsToRemove[] = 'invitePermanentTransverse';
        }

        if ($this->parameterBag->get('SPECIFIQUE_PLACE')) {
            $fieldsToRemove[] = 'invitePermanentEntiteDependante';
            $fieldsToRemove[] = 'invitePermanentTransverse';
            $fieldsToRemove[] = 'reprendreIntegralementArticle';
        }

        foreach ($fieldsToRemove as $fieldToRemove) {
            unset($this->fields[$fieldToRemove]);
        }
    }
}
