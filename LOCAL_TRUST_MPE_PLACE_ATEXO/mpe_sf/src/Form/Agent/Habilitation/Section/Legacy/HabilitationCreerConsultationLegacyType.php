<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\Legacy;

use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HabilitationCreerConsultationLegacyType extends AbstractHabilitationLegacyType
{
    protected $fields = [
        'gererMapaInferieurMontant'        => 'TEXT_MAPA_INFERIEUR_90000',
        'gererMapaSuperieurMontant'        => 'TEXT_MAPA_SUPERIEUR_90000',
        'administrerProceduresFormalisees' => 'TEXT_PROCEDURES_FORMALISEES',
        'creerConsultationTransverse'      => 'TEXT_CREER_CONSULTATION_TRANSVERSE',
    ];

    protected $section = 'creerConsultationSection';

    protected function deactivateFields(Agent $agent)
    {
        $fieldsToRemove = [];

        if (!$this->atexoConfiguration->hasConfigPlateforme('procedureAdaptee')) {
            $fieldsToRemove[] = 'gererMapaInferieurMontant';
            $fieldsToRemove[] = 'gererMapaSuperieurMontant';
        }

        foreach ($fieldsToRemove as $fieldToRemove) {
            unset($this->fields[$fieldToRemove]);
        }
    }
}
