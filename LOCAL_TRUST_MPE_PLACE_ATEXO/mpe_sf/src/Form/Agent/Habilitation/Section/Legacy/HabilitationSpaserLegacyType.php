<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\Legacy;

class HabilitationSpaserLegacyType extends AbstractHabilitationLegacyType
{
    protected $fields = [
        'gestionSpaserConsultations' => 'TEXT_GESTION_SPASER',
    ];

    protected $section = 'spaserSection';
}
