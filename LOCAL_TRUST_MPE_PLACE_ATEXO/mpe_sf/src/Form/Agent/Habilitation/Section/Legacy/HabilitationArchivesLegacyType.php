<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\Legacy;

use App\Entity\Agent;

class HabilitationArchivesLegacyType extends AbstractHabilitationLegacyType
{
    protected $fields = [
        'gererArchives'              => 'TEXT_GERER_ARCHIVES',
        'downloadArchives'           => 'TELECHARGEMENT_DES_ARCHIVES',
        'accederTousTelechargements' => 'TEXT_ACCEDER_TOUS_TELECHARGEMENTS',
    ];

    protected $section = 'archiveSection';

    protected function deactivateFields(Agent $agent)
    {
        $fieldsToRemove = [];

        if (
            !$this->atexoConfiguration->hasConfigOrganisme($agent->getOrganisme(), 'archivageConsultationSurPf')
        ) {
            $fieldsToRemove[] = 'downloadArchives';
        }

        foreach ($fieldsToRemove as $fieldToRemove) {
            unset($this->fields[$fieldToRemove]);
        }
    }
}
