<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\Legacy;

use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Form\DataTransformer\StringToBooleanTransformer;
use App\Service\Agent\HabilitationTypeProcedureService;
use App\Service\AtexoConfiguration;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractHabilitationLegacyType extends AbstractType
{
    protected $fields = [];
    protected $section = '';

    protected $mode;
    public function __construct(
        protected readonly AtexoConfiguration $atexoConfiguration,
        protected readonly ParameterBagInterface $parameterBag,
        private TranslatorInterface $translator,
        protected readonly HabilitationTypeProcedureService $habilitationTypeProcedureService
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $habilitationAgent = $options['data'];
        $this->mode = $options['mode'];
        $agent = $habilitationAgent->getAgent();
        $this->deactivateFields($agent);

        $this->addSection($this->section, $this->fields, $builder);
    }

    protected function addSection($section, $fields, $builder)
    {
        foreach ($fields as $field => $label) {
            $builder
                ->add($field, CheckboxType::class, [
                    'label' => $this->translator->trans($label),
                    'disabled' => $this->mode == 2 ? true : false,
                    'required' => false,
                    'attr' => [
                        'data-style' => 'right',
                        'data-section' => $section,
                    ],
                ])
                ->get($field)
                ->addModelTransformer(new StringToBooleanTransformer());
        }
    }

    protected function deactivateFields(Agent $agent)
    {
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => HabilitationAgent::class,
            'mode' => 0,
        ]);
    }
}
