<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\Legacy;

use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Enum\Habilitation\HabilitationSlug;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HabilitationGestionConsultationLegacyType extends AbstractHabilitationLegacyType
{
    protected $fields = [
        'creerSuiteConsultation' => 'DEFINE_CREER_UNE_SUITE',
        'duplicationConsultations' => 'DUPLIQUER_CONSULTATION',
        'publierConsultation' => 'TEXT_GERER_PUBLICITE',
        'envoyerPublicite' => 'DEFINE_ENVOYER_PUBLICITE',
        'modifierConsultationAvantValidation' => 'TEXT_MODIFIER_CONSULTATION_AVANT_VALIDATION',
        'validationSimple' => 'TEXT_VALIDATION_SIMPLE',
        'validationIntermediaire' => 'TEXT_VALIDATION_INTERMEDIAIRE',
        'validationFinale' => 'TEXT_VALIDATION_FINALE',
        'modifierConsultationMapaInferieurMontantApresValidation' =>
            'TEXT_MODIFIER_CONSULTATION_MAPA_INFERIEUR_MONTANT_APRES_VALIDATION',
        'modifierConsultationMapaSuperieurMontantApresValidation' =>
            'TEXT_MODIFIER_CONSULTATION_MAPA_SUPERIEUR_MONTANT_APRES_VALIDATION',
        'modifierConsultationProceduresFormaliseesApresValidation' =>
            'TEXT_MODIFIER_CONSULTATION_PROCEDURES_FORMALISEES_APRES_VALIDATION',
        'espaceDocumentaireConsultation' => 'TEXT_GERER_ESPACE_DOCUMENTAIRE',
        'accesEchangeDocumentaire' => 'TEXT_GERER_ECHANGES_DOCUMENTAIRES',
        'accesRegistreRetraitsPapier' => 'TEXT_GERER_REGISTRE_RETRAITS',
        'accesRegistreQuestionsPapier' => 'TEXT_GERER_REGISTRE_QUESTIONS',
        'accesRegistreDepotsPapier' => 'TEXT_GERER_REGISTRE_DEPOTS',
        'suiviSeulRegistreRetraitsPapier' => 'TEXT_SUIVI_SEUL_REGISTRE_RETRAITS',
        'suiviSeulRegistreQuestionsPapier' => 'TEXT_SUIVI_SEUL_REGISTRE_QUESTIONS',
        'suiviSeulRegistreDepotsPapier' => 'TEXT_SUIVI_SEUL_REGISTRE_DEPOTS',
        'accesReponses' => 'TEXT_ACCES_REPONSES',
        'telechargementGroupeAnticipePlisChiffres' => 'TEXT_TELECHARGEMENT_GROUPE_ANTICIPE_PLIS_CHIFFRES',
        'telechargementUnitairePlisChiffres' => 'TEXT_TELECHARGEMENT_UNITAIRES_PLIS_CHIFFRES',
        'ouvrirCandidatureEnLigne' => 'TEXT_OUVERTURE_CONDIDATURE_LIGNES',
        'ouvrirCandidatureADistance' => 'TEXT_OUVERTURE_CANDIDATURE_HORS_LIGNE',
        'ouvrirCandidatureHorsLigne' => 'TEXT_OUVERTURE_CANDIDATURE_HORS_LIGNE',
        'ouvrirOffreTechniqueEnLigne' => 'TEXT_OUVERTURE_OFFRES_TECHNIQUE_EN_LIGNE',
        'ouvrirOffreTechniqueADistance' => 'TEXT_OUVERTURE_OFFRES_TECHNIQUE_A_DISTANCE',
        'ouvrirOffreTechniqueHorsLigne' => 'TEXT_OUVERTURE_OFFRES_TECHNIQUE_HORS_LIGNE',
        'ouvrirOffreEnLigne' => 'TEXT_OUVERTURE_OFFRES_LIGNE',
        'ouvrirOffreADistance' => 'TEXT_OUVERTURE_OFFRES_DISTANCE',
        'ouvrirOffreHorsLigne' => 'TEXT_OUVERTURE_OFFRES_HORS_LIGNE',
        'ouvrirAnonymatEnLigne' => 'TEXT_OUVRIR_ENVELOPPES_ANONYMES_LIGNE',
        'ouvrirAnonymatADistance' => 'TEXT_OUVRIR_ENVELOPPES_ANONYMES_DSITANCE',
        'ouvrirAnonymatHorsLigne' => 'TEXT_OUVRIR_ENVELOPPES_ANONYMES_HORS_LIGNE',
        'refuserEnveloppe' => 'REFUSER_ENVELOPPE',
        'restaurerEnveloppe' => 'RESTAURER_ENVELOPPE',
        'gererAdmissibilite' => 'GERE_ADMISSIBILITE',
        'gererEncheres' => 'TEXT_DEFINIR_ENCHERE',
        'suivreEncheres' => 'TEXT_SUIVRE_ENCHERE',
        'gererReouverturesModification' => 'GERER_REOUVERTURES_MODIFICATION',
        'attributionMarche' => 'TEXT_RENSEIGNER_DECISION',
        'decisionSuiviSeul' => 'DEFINE_SUIVI_SEUL_LA_DECISION',
        'resultatAnalyse' => 'RESULTAT_ANALYSE',
        'annulerConsultation' => 'ANNULER_CONSULTATION',
        'importerEnveloppe' => 'IMPORTER_ENVELOPPE',
        'envoyerMessage' => 'TEXT_ENVOYER_MESSAGE',
        'suivreMessage' => 'SUIVI_MESSAGE',
        'gestionMiseDispositionPiecesMarche' => 'TEXT_MISE_DISPOSITION_PIECES_MARCHE',
        'historiqueNavigationInscrits' => 'DEFINE_ACCES_HISTORIQUE_ENTREPRISES',
    ];


    protected $section = 'gestionConsultationSection';

    protected function deactivateFields(Agent $agent)
    {
        $fieldsToRemove = [];

        if (!$this->atexoConfiguration->hasConfigPlateforme('procedureAdaptee')) {
            $fieldsToRemove[] = 'modifierConsultationMapaInferieurMontantApresValidation';
            $fieldsToRemove[] = 'modifierConsultationMapaSuperieurMontantApresValidation';
        }

        if (
            !$this->atexoConfiguration->hasConfigOrganisme(
                $agent->getOrganisme(),
                'espaceDocumentaire',
                'is'
            )
        ) {
            $fieldsToRemove[] = 'espaceDocumentaireConsultation';
            $fieldsToRemove[] = 'accesEchangeDocumentaire';
        }

        if (!$this->atexoConfiguration->hasConfigPlateforme('annulerConsultation')) {
            $fieldsToRemove[] = 'annulerConsultation';
        }

        if (
            !$this->atexoConfiguration->hasConfigOrganisme(
                $agent->getOrganisme(),
                'miseDispositionPiecesMarche'
            )
        ) {
            $fieldsToRemove[] = 'gestionMiseDispositionPiecesMarche';
        }

        if (
            !$this->atexoConfiguration->hasConfigOrganisme(
                $agent->getOrganisme(),
                'echangesDocuments',
                'is'
            )
        ) {
            $fieldsToRemove[] = 'accesEchangeDocumentaire';
        }

        if (!$this->atexoConfiguration->hasConfigPlateforme('enveloppeOffreTechnique')) {
            $fieldsToRemove[] = 'ouvrirOffreTechniqueEnLigne';
            $fieldsToRemove[] = 'ouvrirOffreTechniqueHorsLigne';
        }

        if (!$this->atexoConfiguration->hasConfigPlateforme('enveloppeAnonymat')) {
            $fieldsToRemove[] = 'ouvrirAnonymatEnLigne';
        }

        if (!$this->atexoConfiguration->hasConfigOrganisme($agent->getOrganisme(), 'modeApplet')) {
            $fieldsToRemove[] = 'ouvrirOffreTechniqueADistance';
            $fieldsToRemove[] = 'ouvrirCandidatureADistance';
            $fieldsToRemove[] = 'ouvrirOffreADistance';
            $fieldsToRemove[] = 'ouvrirAnonymatADistance';
        }

        if (!$this->atexoConfiguration->hasConfigOrganisme($agent->getOrganisme(), 'encheres')) {
            $fieldsToRemove[] = 'gererEncheres';
            $fieldsToRemove[] = 'suivreEncheres';
        }

        if (!$this->atexoConfiguration->hasConfigPlateforme('interfaceModuleSub')) {
            $fieldsToRemove[] = 'gererReouverturesModification';
        }

        if (!$this->atexoConfiguration->hasConfigPlateforme('resultatAnalyseAvantDecision')) {
            $fieldsToRemove[] = 'resultatAnalyse';
        }


        if (!$this->atexoConfiguration->hasConfigOrganisme($agent->getOrganisme(), 'importerEnveloppe')) {
            $fieldsToRemove[] = 'importerEnveloppe';
        }

        // Suppression des champs gérés par la gestion V2 des habilitations
        if ($this->parameterBag->get('ACTIVE_HABILITATION_V2')) {
            if (
                $this->habilitationTypeProcedureService->isHabilitationManaged(
                    HabilitationSlug::CREER_SUITE_CONSULTATION()
                )
            ) {
                $fieldsToRemove[] = 'creerSuiteConsultation';
            }
        }

        foreach ($fieldsToRemove as $fieldToRemove) {
            unset($this->fields[$fieldToRemove]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'block_name' => 'gestion_consultation',
            'data_class' => HabilitationAgent::class,
            'mode' => 0,
        ]);
    }
}
