<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\Legacy;

use App\Entity\Agent;

class HabilitationAnnuaireLegacyType extends AbstractHabilitationLegacyType
{
    protected $fields = [
        'suiviEntreprise'     => 'DEFINE_TEXT_VISUALISER_ENTREPRISES',
        'gererLesEntreprises' => 'LEGACY_HABILITATION_GERER_LES_ENTRERISES',
        'annuaireAcheteur'    => 'ANNUAIRE_ACHETEUR_PUBLIC',
    ];

    protected $section = 'annuaireSection';

    protected function deactivateFields(Agent $agent)
    {
        $fieldsToRemove = [];

        if (!$this->atexoConfiguration->hasConfigPlateforme('gestionEntrepriseParAgent')) {
            $fieldsToRemove[] = 'gererLesEntreprises';
        }
        if (!$this->atexoConfiguration->hasConfigPlateforme('annuaireAcheteursPublics')) {
            $fieldsToRemove[] = 'annuaireAcheteur';
        }

        foreach ($fieldsToRemove as $fieldToRemove) {
            unset($this->fields[$fieldToRemove]);
        }
    }
}
