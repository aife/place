<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\Legacy;

use App\Entity\Agent;

class HabilitationRedactionPiecesMarcheLegacyType extends AbstractHabilitationLegacyType
{
    protected $fields = [
        'reprendreIntegralementArticle'  => 'TEXT_REPRENDRE_INTEGRALEMENT_ARTICLE',
        'redactionDocumentsRedac'        => 'TEXT_REDACTION_PIECES',
        'validationDocumentsRedac'       => 'TEXTE_VALIDER_DOCUMENTS_REDAC',
        'administrerClausesEntiteAchats' => 'TEXT_ADMINISTER_CLAUSES_ENTITE_ACHAT',
        'genererPiecesFormatOdt'         => 'TEXT_GENERER_PIECES_FORMAT_ODT',
        'gererGabaritEntiteAchats'       => 'ADMINISTRER_LES_GABARITS_ENTITE_ACHAT',
        'gererGabaritAgent'              => 'ADMINISTRER_LES_GABARITS_AGENT',
    ];

    protected $section = 'redactionPieceMarcheSection';

    protected function deactivateFields(Agent $agent)
    {
        $fieldsToRemove = [];

        if ($this->parameterBag->get('SPECIFIQUE_PLACE')) {
            $fieldsToRemove[] = 'reprendreIntegralementArticle';
        }

        foreach ($fieldsToRemove as $fieldToRemove) {
            unset($this->fields[$fieldToRemove]);
        }
    }
}
