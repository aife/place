<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\Legacy;

use App\Entity\Agent;

class HabilitationGestionAutresAnnoncesLegacyType extends AbstractHabilitationLegacyType
{
    protected $fields = [
        'creerAnnonceInformation'          => 'TEXT_CREER_ANNONCE_INFORMATION',
        'creerAnnonceAttribution'          => 'TEXT_CREER_ANNONCE_ATTRIBUTION',
        'creerAnnonceExtraitPv'            => 'TEXT_CREER_ANNONCE_EXTRAIT_PV',
        'creerAnnonceRapportAchevement'    => 'TEXT_CREER_ANNONCE_RAPPORT_ACHEVEMENT',
        'creerAnnonceDecisionResiliation'  => 'TEXT_CREER_DECISION_RESILIATION',
        'creerAvisProgrammePrevisionnel'   => 'TEXT_CREER_ANNONCE_PROGRAMME_PREVISIONNEL',
        'creerAnnonceSyntheseRapportAudit' => 'TEXT_CREER_ANNONCE_SYNTHESE_RAPPORT_AUDIT',
    ];

    protected $section = 'gestionAutresAnnoncesSection';

    protected function deactivateFields(Agent $agent)
    {
        $fieldsToRemove = [];

        if (!$this->atexoConfiguration->hasConfigPlateforme('autreAnnonceExtraitPv')) {
            $fieldsToRemove[] = 'creerAnnonceExtraitPv';
        }

        if (!$this->atexoConfiguration->hasConfigPlateforme('autreAnnonceRapportAchevement')) {
            $fieldsToRemove[] = 'creerAnnonceRapportAchevement';
        }

        if (!$this->atexoConfiguration->hasConfigPlateforme('autreAnnonceDecisionResiliation')) {
            $fieldsToRemove[] = 'creerAnnonceDecisionResiliation';
        }

        if (!$this->atexoConfiguration->hasConfigPlateforme('autreAnnonceProgrammePrevisionnel')) {
            $fieldsToRemove[] = 'creerAvisProgrammePrevisionnel';
        }

        if (!$this->atexoConfiguration->hasConfigPlateforme('autreAnnonceSyntheseRapportAudit')) {
            $fieldsToRemove[] = 'creerAnnonceSyntheseRapportAudit';
        }

        foreach ($fieldsToRemove as $fieldToRemove) {
            unset($this->fields[$fieldToRemove]);
        }
    }
}
