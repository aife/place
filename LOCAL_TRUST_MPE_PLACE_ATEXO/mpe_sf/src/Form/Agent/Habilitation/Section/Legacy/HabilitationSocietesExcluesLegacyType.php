<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\Legacy;

class HabilitationSocietesExcluesLegacyType extends AbstractHabilitationLegacyType
{
    protected $fields = [
        'porteeSocietesExclues'               => 'TEXT_CREER_SOCIETES_EXCLUES',
        'modifierSocietesExclues'             => 'TEXT_MODIFIER_SOCIETES_EXCLUES',
        'supprimerSocietesExclues'            => 'TEXT_SUPPRIMER_SOCIETES_EXCLUES',
        'porteeSocietesExcluesTousOrganismes' => 'TEXT_PORTEE_SOCIETES_EXCLUES_TOUS_ORGANISMES',
    ];

    protected $section = 'societesExcluesSection';
}
