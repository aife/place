<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\Legacy;

use App\Entity\Agent;

class HabilitationGestionContratLegacyType extends AbstractHabilitationLegacyType
{
    protected $fields = [
        'creerContrat'                  => 'TEXT_CREER_CONTRATS',
        'modifierContrat'               => 'MODIFIER_LES_CONTRATS',
        'supprimerContrat'              => 'SUPPRIMER_LES_CONTRATS',
        'consulterContrat'              => 'TEXT_CONSULTER_CONTRATS',
        'execVoirContratsEa'            => 'EXEC_VOIR_CONTRATS_EA',
        'execVoirContratsEaDependantes' => 'EXEC_VOIR_CONTRATS_EA_DEPENDANTES',
        'execVoirContratsOrganisme'     => 'EXEC_VOIR_CONTRATS_ORGANISME',
    ];

    protected $section = 'gestionContratSection';

    protected function deactivateFields(Agent $agent)
    {
        $fieldsToRemove = [];

        if (!$this->atexoConfiguration->hasConfigOrganisme($agent->getOrganisme(), 'organisationCentralisee')) {
            $fieldsToRemove[] = 'execVoirContratsEaDependantes';
            $fieldsToRemove[] = 'execVoirContratsOrganisme';
        }

        foreach ($fieldsToRemove as $fieldToRemove) {
            unset($this->fields[$fieldToRemove]);
        }
    }
}
