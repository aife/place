<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\Legacy;

use App\Entity\Agent;

class HabilitationAdministrationMetierLegacyType extends AbstractHabilitationLegacyType
{
    protected $fields = [
        'supprimerEnveloppe'         => 'TEXT_SUPPRIMER_PLIS_REFUSES',
        'publicationMarches'         =>
            'MENU_AGENT_ADMINISTRATION_ADMINISTRATION_METIER_PUBLICATION_DONNEES_ESSENTIELLES_MARCHES_CONCLUS',
        'listeMarchesNotifies'       => 'LEGACY_HABILITATION_LISTE_MARCHES_NOTIFIES',
        'gererStatistiquesMetier'    => 'TEXT_ACCEDER_STAT_METIER',
        'suiviFluxChorusTransversal' => 'DEFINE_SUIVI_FLUX_CHORUS_TRANSVERSAUX',
    ];

    protected $section = 'administrationMetierSection';

    protected function deactivateFields(Agent $agent)
    {
        $fieldsToRemove = [];

        if (
            !$this->atexoConfiguration->hasConfigPlateforme('article133GenerationPf') ||
            !$this->parameterBag->get('AFFICHER_LISTE_MARCHES_CONCLUS')
        ) {
            $fieldsToRemove[] = 'publicationMarches';
            $fieldsToRemove[] = 'listeMarchesNotifies';
        }

        if (
            !$this->atexoConfiguration->hasConfigOrganisme($agent->getOrganisme(), 'exportMarchesNotifies')
        ) {
            $fieldsToRemove[] = 'listeMarchesNotifies';
        }
        if (
            !$this->atexoConfiguration->isChorusAccessActivate($agent)
        ) {
            $fieldsToRemove[] = 'suiviFluxChorusTransversal';
        }

        foreach ($fieldsToRemove as $fieldToRemove) {
            unset($this->fields[$fieldToRemove]);
        }
    }
}
