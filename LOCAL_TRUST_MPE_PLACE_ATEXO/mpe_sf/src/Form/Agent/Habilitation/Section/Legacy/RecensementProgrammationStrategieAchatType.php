<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\Legacy;

use App\Entity\Agent\Habilitations\RecensementProgrammationStrategieAchat;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class RecensementProgrammationStrategieAchatType extends AbstractType
{
    public function __construct(
        private TranslatorInterface $translator,
    ) {
    }

    protected $fields = [
        'besoinUnitaireConsultation'             => 'TEXT_BESOIN_UNITAIRE_CONSULTATION',
        'besoinUnitaireCreationModification'     => 'TEXT_BESOIN_UNITAIRE_CREATION_MODIFICATION',
        'demandeAchatConsultation'               => 'TEXT_DEMANDE_ACHAT_CONSULTATION',
        'demandeAchatCreationModification'       => 'TEXT_DEMANDE_ACHAT_CREATION_MODIFICATION',
        'projetAchatConsultation'                => 'TEXT_PROJET_ACHAT_CONSULTATION',
        'projetAchatCreationModification'        => 'TEXT_PROJET_ACHAT_CREATION_MODIFICATION',
        'validationOpportunite'                  => 'TEXT_VALIDATION_OPPORTUNITE',
        'validationAchat'                        => 'TEXT_VALIDATION_ACHAT',
        'validationBudget'                       => 'TEXT_VALIDATION_BUDGET',
        'validerProjetAchat'                     => 'TEXT_VALIDER_PROJET_ACHAT',
        'strategieAchatGestion'                  => 'TEXT_STRATEGIE_ACHAT_GESTION',
        'recensementProgrammationAdministration' => 'TEXT_RECENSEMENT_PROGRAMMATION_ADMINISTRATION',
    ];

    protected $section = 'recensementProgrammationStrategieAchatSection';

    protected $mode;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->mode = $options['mode'];
        $this->addSection($builder);
    }

    protected function addSection($builder)
    {
        foreach ($this->fields as $field => $label) {
            $builder
                ->add($field, CheckboxType::class, [
                    'label' => $this->translator->trans($label),
                    'required' => false,
                    'disabled' =>  $this->mode == 2 ? true : false,
                    'attr' => [
                        'data-style' => 'right',
                        'data-section' => $this->section,
                    ],
                ]);
        }
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'block_name' => 'recensement_programmation_strategie_achat',
            'data_class' => RecensementProgrammationStrategieAchat::class,
            'mode' => 0,
        ]);
    }
}
