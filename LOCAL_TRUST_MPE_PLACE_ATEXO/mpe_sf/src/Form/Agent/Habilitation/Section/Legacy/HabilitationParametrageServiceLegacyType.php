<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\Legacy;

use App\Entity\Agent;

class HabilitationParametrageServiceLegacyType extends AbstractHabilitationLegacyType
{
    protected $fields = [
        'gererMonService' => 'GERER_MON_SERVICE',
        'gestionAgentPole' => 'TEXT_GERER_ENTITE_ACHAT',
        'gestionAgents' => 'TEXT_GERER_AGENTS',
        'gestionHabilitations' => 'TEXT_GERER_HABILITATIONS',
        'gestionCompteBoamp' => 'TEXT_GERER_CPT_BOAMP',
        'gestionCompteJal' => 'GERER_COMPTES_JAL',
        'gestionAdressesFacturationJal' => 'TEXT_GERER_LES_ADRESSES_FACTURATION_JAL',
        'gestionBiCles' => 'TEXT_DEFINE_GERER_CLES_CHIFFREMENT',
        'gestionFournisseursEnvoisPostaux' => 'TEXT_FOURNISSEUR_DOC',
        'gestionCentralePub' => 'TEXT_GESTION_CENTRALES_PUBLICATION',
        'gestionCompteGroupeMoniteur' => 'TEXT_GERER_COMPTES_MONITEUR',
        'gererAdressesService' => 'LEGACY_HABILITATION_GERER_ADRESSES_SERVICES',
        'gestionCertificatsAgent' => 'DEFINE_GERER_CERTIFICAT_AGENT',
        'gestionMandataire' => 'DEFINE_GERER_MANDATAIRE',
    ];

    protected $section = 'parametrageServiceSection';

    protected function deactivateFields(Agent $agent)
    {
        $fieldsToRemove = [];

        if (!$this->atexoConfiguration->hasConfigPlateforme('gererMonService')) {
            $fieldsToRemove[] = 'gererMonService';
        }

        if ($this->atexoConfiguration->hasConfigPlateforme('socleInterne')) {
            $fieldsToRemove[] = 'gestionAgentPole';
            $fieldsToRemove[] = 'gestionAgents';
        }

        if ($this->atexoConfiguration->hasConfigPlateforme('socleExternePpp')) {
            $fieldsToRemove[] = 'gestionAgents';
        }

        if (!$this->atexoConfiguration->hasConfigPlateforme('publiciteFormatXml')) {
            $fieldsToRemove[] = 'gestionCompteBoamp';
            $fieldsToRemove[] = 'gestionCompteGroupeMoniteur';
        }

        if (!$this->atexoConfiguration->hasConfigPlateforme('gestionAdressesFacturationJal')) {
            $fieldsToRemove[] = 'gestionAdressesFacturationJal';
        }

        if (!$this->atexoConfiguration->hasConfigOrganisme($agent->getOrganisme(), 'centralePublication')) {
            $fieldsToRemove[] = 'gestionCentralePub';
        }

        if (!$this->atexoConfiguration->hasConfigPlateforme('gererAdressesService')) {
            $fieldsToRemove[] = 'gererAdressesService';
        }

        if (!$this->atexoConfiguration->hasConfigPlateforme('gererCertificatsAgent')) {
            $fieldsToRemove[] = 'gestionCertificatsAgent';
        }

        if (!$this->atexoConfiguration->hasConfigOrganisme($agent->getOrganisme(), 'gestionMandataire')) {
            $fieldsToRemove[] = 'gestionMandataire';
        }

        foreach ($fieldsToRemove as $fieldToRemove) {
            unset($this->fields[$fieldToRemove]);
        }
    }
}
