<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\Habilitation\Section\Legacy;

use App\Entity\Agent;

class HabilitationNewsletterLegacyType extends AbstractHabilitationLegacyType
{
    protected $fields = [
        'gererNewsletter'       => 'TEXT_GERER_NEWSLETTER',
        'gererNewsletterRedac'  => 'TEXT_GERER_NEWSLETTER_REDAC',
    ];

    protected $section = 'newsletterSection';
}
