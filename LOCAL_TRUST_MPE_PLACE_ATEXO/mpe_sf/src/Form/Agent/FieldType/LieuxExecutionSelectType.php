<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\FieldType;

use Exception;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LieuxExecutionSelectType extends AbstractType
{
    /**
     * @throws Exception
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'label' => 'TEXT_LIEU_EXECUTION',
            'multiple' => true,
            'disabled' => true,
            'mapped' => false,
            'choice_translation_domain' => false,
            'attr' => [
                'class' => 'select2-icons form-control formLieuxExecution',
            ],
        ]);
    }

    public function getParent(): string
    {
        return ChoiceType::class;
    }
}
