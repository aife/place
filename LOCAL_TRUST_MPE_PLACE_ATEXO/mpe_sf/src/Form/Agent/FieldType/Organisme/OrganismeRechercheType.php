<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\FieldType\Organisme;

use App\Model\OrganismeModel;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrganismeRechercheType extends AbstractType
{
    public function __construct()
    {
    }

    public function getBlockPrefix()
    {
        return '';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->setMethod(Request::METHOD_GET)
            ->add('keyword', TextType::class, [
                'label' => 'DEFINE_MOTS_CLES',
            ])
            ->add('entitePublique', ChoiceType::class, [
                'choices' => $options['organismes'],
                'choice_value' => 'id',
                'choice_label' => fn (?OrganismeModel $organismeModel)
                => $organismeModel ? $organismeModel->getName() : '',
                'placeholder' => '',
                'multiple' => false,
                'expanded' => false,
                'attr' => [
                    'name' => 'entitePublique',
                ],
            ])
            ->add('search', SubmitType::class, [
                'label' => 'RECHERCHER',
                'icon_before' => 'fa fa-search',
                'block_name' => 'search',
                'attr' => [
                    'class' => 'btn btn-primary',
                    'name' => 'search',
                    'id' => 'search',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'attr' => [
                'novalidate' => 'novalidate',
            ],
            'organismes' => new ArrayCollection(),
            'csrf_protection' => false,
        ]);
    }
}
