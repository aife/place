<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\FieldType;

use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Form\Agent\DataTransformer\TypeProcedureOrganismeTransformer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProcedureType extends AbstractType
{
    public function __construct(private readonly TypeProcedureOrganismeTransformer $transformer)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->resetViewTransformers()
            ->addViewTransformer($this->transformer);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'class'                     => TypeProcedureOrganisme::class,
            'choice_translation_domain' => false,
            'label'                     => 'DEFINE_TYPE_PROCEDURE',
            'placeholder'               => 'TEXT_SELECTION_LISTE',
            'choice_value'              => fn (?TypeProcedureOrganisme $entity) => $entity ? $entity->concatTypeProcedureOrganisme() : '',
            'attr'                      => [
                'data-ajax-custom'          => 'procedureType',
            ],
            'constraints'               => [
                new NotBlank(['message'     => 'consultation.simplifiee.form.error.blank']),
            ],
            'invalid_message'           => 'consultation.simplifiee.form.error.blank',
            'disabled'                  => false
        ]);
    }

    public function getParent(): string
    {
        return EntityType::class;
    }
}
