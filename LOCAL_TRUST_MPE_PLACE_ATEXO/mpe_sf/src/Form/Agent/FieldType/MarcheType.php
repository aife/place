<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\FieldType;

use App\Entity\TypeContrat;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class MarcheType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'class' => TypeContrat::class,
            'choice_translation_domain' => true,
            'label' => 'DEFINE_TYPE_CONTRAT',
            'placeholder' => 'TEXT_SELECTION_LISTE',
            'attr' => [
                'data-ajax-custom' => 'typeContrat',
            ],
            'constraints' => [
                new NotBlank(['message' => 'consultation.simplifiee.form.error.blank']),
            ],
        ]);
    }

    public function getParent(): string
    {
        return EntityType::class;
    }
}
