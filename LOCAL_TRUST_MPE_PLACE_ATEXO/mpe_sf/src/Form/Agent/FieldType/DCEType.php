<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\FieldType;

use App\Entity\DCE;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class DCEType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'class' => DCE::class,
            'label' => 'DEFINE_DOCUMENTS_JOINT_DCE',
            'help' => 'TEXT_COMPRESSE_OBLIGATOIRE',
            'help_html' => true,
            'required' => false,
            'constraints' => [
                new File([
                    'mimeTypes' => ['application/zip'],
                    'mimeTypesMessage' => 'consultation.simplifiee.form.error.zip',
                ]),
            ],
            'row_attr' => ['class' => 'uploader'],
            'attr' => ['accept' => '.zip'],
        ]);
    }

    public function getParent(): string
    {
        return FileType::class;
    }
}
