<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\FieldType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class DateFinDateType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'required' => true,
            'widget' => 'single_text',
            'format' => 'yyyy-MM-dd',
            'constraints' => [
                new NotBlank(['message' => 'consultation.simplifiee.form.error.date']),
            ],
        ]);
    }

    public function getParent(): string
    {
        return DateType::class;
    }
}
