<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\FieldType;

use App\Entity\Agent;
use App\Entity\Organisme;
use App\Form\Agent\Validator\Constraints\ReferenceUtilisateur;
use App\Service\AtexoConfiguration;
use App\Service\CurrentUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraints\NotBlank;

class ReferenceType extends AbstractType
{
    private ?bool $isModulesNumerotationRefConsEnabled = null;

    /**
     * ReferenceType constructor.
     *
     * @param CurrentUser $currentUser
     */
    public function __construct(AtexoConfiguration $atexoConfiguration, Security $security)
    {
        $organisme = null;
        if ($security->getUser() instanceof Agent) {
            $organisme = $security->getUser()->getOrganisme();
        }
        if ($organisme instanceof Organisme) {
            $this->isModulesNumerotationRefConsEnabled = $atexoConfiguration->isModuleEnabled(
                'NumerotationRefCons',
                $organisme,
                'get'
            );
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $placeholder = '';
        $constraints = [
            new ReferenceUtilisateur(),
            new NotBlank(['message' => 'consultation.simplifiee.form.error.blank'])
        ];

        if ($this->isModulesNumerotationRefConsEnabled) {
            $placeholder = 'TEXT_GENEREE_ENREGISTREMENT';
            $constraints = [];
        }

        $resolver->setDefaults([
            'empty_data' => '',
            'label' => 'REFERENCE_DE_LA_CONSULTATION',
            'disabled' => $this->isModulesNumerotationRefConsEnabled,
            'attr' => [
                'placeholder' => $placeholder,
            ],
            'help' => 'DEFINE_MIN_CINQ_CARACTERES',
            'constraints' => $constraints,
        ]);
    }

    public function getParent(): string
    {
        return TextType::class;
    }
}
