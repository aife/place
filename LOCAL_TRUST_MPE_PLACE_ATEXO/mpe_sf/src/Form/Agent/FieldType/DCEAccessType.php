<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\FieldType;

use App\Entity\Consultation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class DCEAccessType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'label' => 'DEFINE_INFORMATIONS_DCE',
            'choices' => [
                'TEXT_PHASE_ACCES_PUBLIC' => Consultation::TYPE_ACCES_PUBLIC,
                'TEXT_PHASE_ACCES_RESTREINT' => Consultation::TYPE_ACCES_RESTREINT,
            ],
            'expanded' => true,
            'constraints' => [
                new NotBlank(['message' => 'consultation.simplifiee.form.error.blank']),
            ],
        ]);
    }

    public function getParent(): string
    {
        return ChoiceType::class;
    }
}
