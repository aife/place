<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\FieldType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ObjetType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'empty_data' => '',
            'label' => 'DEFINE_OBJET_CONSULTATION',
            'constraints' => [
                new NotBlank(['message' => 'consultation.simplifiee.form.error.blank']),
            ],
        ]);
    }

    public function getParent(): string
    {
        return TextareaType::class;
    }
}
