<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\FieldType;

use App\Entity\AvisType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class AnnonceType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'class' => AvisType::class,
            'choice_translation_domain' => false,
            'label' => 'TYPE_ANNONCE',
            'placeholder' => 'TEXT_SELECTION_LISTE',
            'constraints' => [
                new NotBlank(['message' => 'consultation.simplifiee.form.error.blank']),
            ],
        ]);
    }

    public function getParent(): string
    {
        return EntityType::class;
    }
}
