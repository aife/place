<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form\Agent\FieldType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class IntituleType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'empty_data' => '',
            'label' => 'DEFINE_INTITULE_CONSULTATION',
            'constraints' => [
                new NotBlank(['message' => 'consultation.simplifiee.form.error.blank']),
            ],
        ]);
    }

    public function getParent(): string
    {
        return TextType::class;
    }
}
