<?php

namespace App\Form\Agent\Validator\Constraints;

use App\Entity\Consultation;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ConsultationSimplifieeValidator extends ConstraintValidator
{
    /**
     * @param Consultation $consultation
     */
    public function validate($consultation, Constraint $constraint): void
    {
        // Si le type d'accès est à 2 (privé) et le code non rempli
        if (
            Consultation::TYPE_ACCES_RESTREINT === $consultation->getTypeAcces()
            && empty($consultation->getCodeProcedure())
        ) {
            $this->context->buildViolation('CHAMP_OBLIGATOIRE')
                ->setTranslationDomain('messages')
                ->atPath('codeProcedureInput')
                ->addViolation();
        }
    }
}
