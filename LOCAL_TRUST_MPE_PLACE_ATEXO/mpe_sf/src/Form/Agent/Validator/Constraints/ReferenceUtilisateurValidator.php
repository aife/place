<?php

namespace App\Form\Agent\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ReferenceUtilisateurValidator extends ConstraintValidator
{
    /**
     * @param string $reference
     */
    public function validate($reference, Constraint $constraint): void
    {
        if (preg_match('/[^a-z_\-0-9]/i', $reference)
            || strlen($reference) < 5
            || strlen($reference) > 32
        ) {
            $this->context->buildViolation('consultation.simplifiee.form.error.format')
                ->addViolation();
        }
    }
}
