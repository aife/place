<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Traits\Entity;

use App\Entity\Configuration\PlateformeVirtuelle;
use Doctrine\ORM\Mapping as ORM;

/**
 * Trait PlateformeVirtuelleLinkTrait.
 */
trait PlateformeVirtuelleLinkTrait
{
    /**
     * @var PlateformeVirtuelle
     * @ORM\ManyToOne(targetEntity=PlateformeVirtuelle::class)
     * @ORM\JoinColumn(name="plateforme_virtuelle_id", referencedColumnName="id")
     */
    private $plateformeVirtuelle;

    public function getPlateformeVirtuelle(): ?PlateformeVirtuelle
    {
        return $this->plateformeVirtuelle;
    }

    public function setPlateformeVirtuelle(?PlateformeVirtuelle $plateformeVirtuelle): void
    {
        $this->plateformeVirtuelle = $plateformeVirtuelle;
    }
}
