<?php

namespace App\Traits;

use Atexo_Config;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Laminas\Http\Client\Adapter\Curl;

trait ConfigProxyTrait
{
    /**
     * Permettre d'ajouter la configuration d'un Proxy au client LaminasHttp.
     */
    public function getConfigProxyForLaminasHttpClient(ContainerInterface $container): array|bool
    {
        if ('' != $container->getParameter('URL_PROXY')) {
            $configProxy = [
                'adapter' => Curl::class,
                'curloptions' => [
                    CURLOPT_SSL_VERIFYHOST => 0,
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_PROXY => $container->getParameter('URL_PROXY')
                        . ':'
                        . $container->getParameter('PORT_PROXY'),
                ],
            ];

            return $configProxy;
        }

        return false;
    }

    public function getConfigProxyContext($container)
    {
        $opts = null;
        if ('' != $container->getParameter('URL_PROXY')) {
            $opts = [
                'http' => [
                    'proxy' => $container->getParameter('URL_PROXY') . ':' . $container->getParameter('PORT_PROXY'),
                ],
            ];
        }

        $noSsl = ['ssl' => [
            'verify_peer' => false,
            'verify_peer_name' => false,
        ]];

        if (is_array($opts)) {
            $context = [...$opts, ...$noSsl];
        } else {
            $context = $noSsl;
        }

        return stream_context_create($context);
    }

    /**
     * Permettre d'ajouter la configuration d'un Proxy au client Guzzle.
     *
     * @param ContainerInterface $container container
     *
     *
     * @author Grégory CHEVRET <gregory.chevret@atexo.com>
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public static function getConfigProxyForGuzzle(ContainerInterface $container = null, $params = []): array|bool
    {
        $configProxy = false;
        if (!is_null($container)) {
            if ('' != $container->getParameter('URL_PROXY')) {
                $configProxy = ['proxy' => $container->getParameter('URL_PROXY')
                    . ':' . $container->getParameter('PORT_PROXY'), ];
            }
        } elseif (key_exists('URL_PROXY', $params) && '' != $params['URL_PROXY']) {
            $configProxy = ['proxy' => $params['URL_PROXY']
                . ':' . $params['PORT_PROXY'], ];
        }

        return $configProxy;
    }

    /**
     * Permettre d'ajouter la configuration d'un proxy
     * aux fonctions stream : file_get_content, fopen, etc...
     *
     * @param ContainerInterface $container container
     *
     * @return resource|null
     *
     * @author Grégory CHEVRET <gregory.chevret@atexo.com>
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function getConfigProxyForStream(ContainerInterface $container)
    {
        if ('' != $container->getParameter('URL_PROXY')) {
            $configProxy = [
                'http' => [
                    'proxy' => $container->getParameter('URL_PROXY')
                        . ':' . $container->getParameter('PORT_PROXY'),
                ],
            ];

            return stream_context_create($configProxy);
        }

        return null;
    }

    /**
     * Permettre d'ajouter la configuration d'un Proxy au client Guzzle.
     *
     *
     * @author Grégory CHEVRET <gregory.chevret@atexo.com>
     * @deprecated
     */
    public function getConfigProxyForGuzzlePrado(): array|bool
    {
        if ('' != Atexo_Config::getParameter('URL_PROXY')) {
            $configProxy = [
                'proxy' => Atexo_Config::getParameter('URL_PROXY') . ':' . Atexo_Config::getParameter('PORT_PROXY')
            ];

            return $configProxy;
        }

        return false;
    }
}
