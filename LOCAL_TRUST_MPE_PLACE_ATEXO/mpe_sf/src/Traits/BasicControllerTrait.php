<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Traits;

use App\Service\AbstractService;
use App\Service\Paginator;
use Symfony\Component\HttpFoundation\Request;

trait BasicControllerTrait
{
    public function handlePaginator(Request $request, Paginator $paginator, $wsResponse): array
    {
        $paginator = $paginator->getPagination($wsResponse->{'hydra:view'}, $request->getRequestUri());
        $paginator['total'] = $wsResponse->{'hydra:totalItems'};

        return $paginator;
    }
}
