<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Traits;

use App\Entity\ModificationContrat;
use Symfony\Component\Serializer\Serializer;

trait ApiTrait
{
    /**
     * @param ModificationContrat $entity
     * @param $mode
     */
    public function getNode($entity, Serializer $serializer, string $mode): bool|float|int|string
    {
        $nodeNameArray = explode('\\', $entity::class);
        $nodeName = array_pop($nodeNameArray);
        $normalized = $serializer->normalize($entity, $mode, ['groups' => ['webservice']]);
        $content = $serializer->serialize($normalized, $mode);
        if ('xml' == $mode) {
            $content = str_replace('<?xml version="1.0"?>', '', $content);
            $content = str_replace("\n", '', $content);
            $content = str_replace('response>', $nodeName.'>', $content);
        }

        return $content;
    }
}
