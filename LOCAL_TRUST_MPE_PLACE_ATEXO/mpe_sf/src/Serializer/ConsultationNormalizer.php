<?php

namespace App\Serializer;

use App\Entity\ConsLotContrat;
use App\Entity\Consultation;
use App\Exception\ApiProblemInvalidArgumentException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ConsultationNormalizer extends AbstractApiPlatformDisabledNormalizer implements ContextAwareNormalizerInterface
{
    public function __construct(
        private readonly ObjectNormalizer $normalizer,
        private readonly EntityManagerInterface $em
    ){
    }

    public function normalize($object, $format = null, array $context = [])
    {
        if (!$object instanceof Consultation) {
            throw new ApiProblemInvalidArgumentException("l'objet n'est pas une instance de Consultation");
        }

        $data = $this->normalizer->normalize($object, $format, $context);
        $data['id'] = $object->getId();
        $data['reference'] = $object->getReferenceUtilisateur();
        $data['intitule'] = $object->getIntitule();
        $data['objet'] = $object->getObjet();
        $data['organisme'] = $object->getAcronymeOrg();
        $data['typeProcedure'] = $object->getTypeProcedure()?->getAbbreviation();
        $data['naturePrestation'] = $object->getNaturePrestation();
        $data['idDirectionService'] = $object->getServiceId();
        $data['numeroProjetAchat']= $object->getNumeroProjetAchat();
        $data['decisionAttribution']= $object->getDateDecision();

        if (empty($data['formePrix'])) {
            unset($data['formePrix']);
        }

        $data['numeroLot'] = 0;

        if ($object->getAlloti()) {
            $contratTitulaire = $context['contratTitulaire'];
            $consLotContrat = $this->em->getRepository(ConsLotContrat::class)->findOneBy(['idContratTitulaire' => $contratTitulaire->getIdContratTitulaire()]);
            if ($consLotContrat instanceof ConsLotContrat) {
                $data['numeroLot'] = $consLotContrat->getLot();
            }
        }

        return $data;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return parent::supportsNormalization($data, $format, $context) && $data instanceof Consultation;
    }
}
