<?php

namespace App\Serializer;

use DateTime;
use App\Entity\Agent;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class AgentNormalizer extends AbstractApiPlatformDisabledNormalizer implements ContextAwareNormalizerInterface
{
    public function __construct(private readonly ObjectNormalizer $normalizer, private readonly ServiceNormalizer $serviceNormalizer)
    {
    }

    public function normalize($agent, $format = null, array $context = [])
    {
        /** @var Agent $agent */
        if (empty($agent->getTelephone())) {
            $agent->setTelephone(null);
        }
        if (empty($agent->getFax())) {
            $agent->setFax(null);
        }

        $data = $this->normalizer->normalize($agent, $format, $context);

        if (in_array('ws-habilitation', $context['groups'])) {
            return $this->getWsHabilitationContext($agent, $data);
        }

        if (!empty($agent->getDateCreation())) {
            $data['dateCreation'] = new DateTime($agent->getDateCreation());
        }

        if (!empty($agent->getDateModification())) {
            $data['dateModification'] = new DateTime($agent->getDateModification());
        }
        if (!empty($agent->getServiceId())) {
            $data['service'] = $this->serviceNormalizer->normalize($agent->getService(), $format, $context);
        }

        return $data;
    }

    private function getWsHabilitationContext($agent, $data)
    {
        $data['identifiant'] = $agent->getLogin();
        $data['habilitations'] = $this->getHabilitationsActives($agent->getHabilitation());

        return $data;
    }

    private function getHabilitationsActives($habillitations)
    {
        $getters = array_filter(get_class_methods($habillitations), fn($method) => str_starts_with($method, 'get'));
        $habilitationsActives = [];
        foreach ($getters as $getter) {
            if ($habillitations->$getter() && 'IdAgent' !== $habillitations->$getter()) {
                $habilitationsActives['habilitation'][] = strtoupper(substr($getter, 3));
            }
        }

        return $habilitationsActives;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return parent::supportsNormalization($data, $format, $context) && $data instanceof Agent;
    }
}
