<?php

namespace App\Serializer;

use App\Doctrine\Extension\ConsultationIdExtension;
use App\Entity\Consultation;
use App\Exception\ApiProblemInvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class ConsultationIdsOnlyNormalizer implements ContextAwareNormalizerInterface
{
    public function normalize($object, $format = null, array $context = [])
    {
        if (!$object instanceof Consultation) {
            throw new ApiProblemInvalidArgumentException("l'objet n'est pas une instance de Consultation");
        }

        return ['id' => $object->getId(), 'statutCalcule' => $object->getCalculatedStatus()];
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return ConsultationIdExtension::shouldReturnOnlyIds($context) && $data instanceof Consultation;
    }
}
