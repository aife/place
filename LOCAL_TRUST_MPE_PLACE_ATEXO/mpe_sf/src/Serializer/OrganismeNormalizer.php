<?php

namespace App\Serializer;

use App\Entity\ConfigurationOrganisme;
use App\Entity\Organisme;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class OrganismeNormalizer extends AbstractApiPlatformDisabledNormalizer implements ContextAwareNormalizerInterface
{
    public function __construct(
        private readonly ObjectNormalizer $normalizer,
        private readonly ContainerInterface $container,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function normalize($organisme, $format = null, array $context = [])
    {
        $data = $this->normalizer->normalize($organisme, $format, $context);
        $data['denomination'] = $organisme->getDenominationOrg();
        $data['nic'] = $organisme->getComplement();
        $adresse = [];
        $adresse['rue'] = $organisme->getAdresse();
        $adresse['codePostal'] = $organisme->getCp();
        $adresse['ville'] = $organisme->getVille();
        $adresse['pays'] = $organisme->getPays();
        $data['adresse'] = $adresse;
        $data['description'] = null;

        $configurationOrganisme = $this->entityManager
            ->getRepository(ConfigurationOrganisme::class)
            ->findOneBy(
                ['organisme' => $organisme->getAcronyme()]
            );

        if ($configurationOrganisme instanceof ConfigurationOrganisme) {
            $data['echangesChorus'] = $configurationOrganisme->getInterfaceChorusPmi();
        }

        $data['logo'] = [];

        $cheminLogo = $this->container->getParameter('BASE_ROOT_DIR')
            . $organisme->getAcronyme()
            . $this->container->getParameter('PATH_ORGANISME_IMAGE')
            . 'logo-organisme-grand.jpg';

        if (is_file($cheminLogo)) {
            $data['logo'] = [];
            $data['logo']['taille'] = filesize($cheminLogo);
            $data['logo']['md5Checksum'] = md5_file($cheminLogo);
            $data['logo']['nom'] = 'logo-organisme-grand.jpg';
        }

        return $data;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return parent::supportsNormalization($data, $format, $context) && $data instanceof Organisme;
    }
}
