<?php

namespace App\Serializer;

use DateTime;
use App\Entity\ContactContrat;
use App\Exception\ApiProblemInvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ContactNormalizer extends AbstractApiPlatformDisabledNormalizer implements NormalizerInterface
{
    public function __construct(private readonly ObjectNormalizer $normalizer)
    {
    }

    public function normalize($contact, $format = null, array $context = [])
    {
        if (!$contact instanceof ContactContrat) {
            throw new ApiProblemInvalidArgumentException("l'objet n'est pas une instance de ContactContrat");
        }

        $data = $this->normalizer->normalize($contact, $format, $context);

        $data['inscrit'] = false;
        if (!empty($contact->getIdInscrit())) {
            $data['inscrit'] = true;
        }
        $data['idEtablissement'] = $contact->getIdEtablissement();
        $data['idEntreprise'] = $contact->getIdEntreprise();
        if ($contact->getCreatedAt() instanceof DateTime) {
            $data['dateCreation'] = $contact->getCreatedAt();
        }
        if ($contact->getUpdatedAt() instanceof DateTime) {
            $data['dateModification'] = $contact->getUpdatedAt();
        }

        return $data;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return parent::supportsNormalization($data, $format, $context) && $data instanceof ContactContrat;
    }
}
