<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Serializer\Denormalizer;

use App\Model\DonneesEssentielles\Marche\ContactTitulaireType;
use App\Model\DonneesEssentielles\Marche\TitulairesAType\TitulaireAType;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class TypeTitulaireDenormalizer implements DenormalizerInterface
{
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        $contact = new ContactTitulaireType();
        $contact->setNom($data['nom'])
            ->setPrenom($data['prenom'])
            ->setEmail($data['email']);

        $titulaire = new TitulaireAType();
        $titulaire->setTypeIdentifiant($data['typeIdentifiantEntreprise'])
        ->setDenominationSociale($data['denominationSociale'])
        ->setContact($contact);

        return $titulaire;
    }

    public function supportsDenormalization($data, string $type, string $format = null)
    {
        return TitulaireAType::class === $type;
    }
}
