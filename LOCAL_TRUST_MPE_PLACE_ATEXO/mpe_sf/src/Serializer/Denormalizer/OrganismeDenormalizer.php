<?php

namespace App\Serializer\Denormalizer;

use App\Entity\Organisme;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;

/**
 * Class PieceJointeDenormalizer.
 */
class OrganismeDenormalizer extends AbstractApiPlatformDisabledNormalizer implements ContextAwareDenormalizerInterface
{
    /**
     * @param mixed  $data
     * @param string $type
     * @param null   $format
     *
     * @return array|object
     */
    public function denormalize($data, $type, $format = null, array $context = []): array|object
    {
        $organisme = new Organisme();
        if (!empty($data['acronyme'])) {
            $organisme->setAcronyme($data['acronyme']);
        }
        if (!empty($data['denomination'])) {
            $organisme->setDenominationOrg($data['denomination']);
        } else {
            //on ne peut pas créer un organisme avec denominationOrg=null
            $organisme->setDenominationOrg('');
        }
        if (!empty($data['sigle'])) {
            $organisme->setSigle($data['sigle']);
        }
        if (!empty($data['description'])) {
            $organisme->setDescriptionOrg($data['description']);
        }
        if (!empty($data['categorieInsee'])) {
            $organisme->setCategorieInsee($data['categorieInsee']);
        }
        if (!empty($data['siren'])) {
            $organisme->setSiren($data['siren']);
        }
        if (!empty($data['nic'])) {
            $organisme->setComplement($data['nic']);
        }

        if (!empty($data['adresse'])) {
            $adresse = $data['adresse'];
            if (!empty($adresse['rue'])) {
                $organisme->setAdresse($adresse['rue']);
            }
            if (!empty($adresse['codePostal'])) {
                $organisme->setCp($adresse['codePostal']);
            }
            if (!empty($adresse['ville'])) {
                $organisme->setVille($adresse['ville']);
            }
            if (!empty($adresse['pays'])) {
                $organisme->setPays($adresse['pays']);
            }
        }

        if (!empty($data['article'])) {
            $organisme->setTypeArticleOrg($data['article']);
        }
        if (!empty($data['email'])) {
            $organisme->setEmail($data['email']);
        }
        if (!empty($data['url'])) {
            $organisme->setUrl($data['url']);
        }
        if (!empty($data['tel'])) {
            $organisme->setTel($data['tel']);
        }
        if (!empty($data['telecopie'])) {
            $organisme->setTelecopie($data['telecopie']);
        }
        if (!empty($data['idExterne'])) {
            $organisme->setIdExterne($data['idExterne']);
        }

        return $organisme;
    }

    /**
     * @param mixed  $data
     * @param string $type
     * @param null   $format
     */
    public function supportsDenormalization($data, $type, $format = null, array $context = []): bool
    {
        return parent::supportsNormalization($data, $format, $context) && Organisme::class === $type;
    }
}
