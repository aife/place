<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Serializer\Api;

use App\Service\ContratService;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use App\Entity\ContratTitulaire;

class ContratTitulaireNormalizer implements ContextAwareNormalizerInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;

    public function __construct(
        private ContratTitulaireNormalizerHelper $helper,
        private ContratService $contratService
    ) {
    }

    private const ALREADY_CALLED = 'ENTITIES_NORMALIZER_ALREADY_CALLED';

    public function supportsNormalization($data, string $format = null, array $context = [])
    {
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        return $context['api-platform']
            && 'get_place' === $context['collection_operation_name']
            && $data instanceof ContratTitulaire;
    }

    public function normalize($object, string $format = null, array $context = [])
    {
        $context[self::ALREADY_CALLED] = true;
        $data = $this->normalizer->normalize($object, $format, $context);
        $market = $this->contratService->getFormatExtendu($object);
        $normalizedMarket = $this->normalizer->normalize($market, $format, $context);

        $data = $this->helper->addExtraData($normalizedMarket, $data, $object);

        return array_merge($data, $this->helper->formatClauses($object));
    }
}
