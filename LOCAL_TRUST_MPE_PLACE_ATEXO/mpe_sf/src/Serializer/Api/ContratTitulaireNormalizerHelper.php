<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Serializer\Api;

use App\Entity\ContratTitulaire;
use App\Entity\Inscrit;
use App\Entity\Referentiel\Consultation\ClausesN1;
use App\Repository\InscritRepository;
use App\Repository\Referentiel\Consultation\ClausesN1Repository;

class ContratTitulaireNormalizerHelper
{
    public function __construct(
        private ClausesN1Repository $clausesRepository,
        private InscritRepository $inscritRepository
    ) {
    }
    public function formatClauses(ContratTitulaire $contratTitulaire): array
    {
        $data = [];
        $clausesValues = $this->getClausesValues($contratTitulaire);
        $clauses = $this->clausesRepository->findAll();
        foreach ($clauses as $clause) {
            foreach ($clause->getClausesN2() as $clauseN2) {
                if (0 === $clauseN2->getClausesN3()->count()) {
                    $data[$clause->getSlug()][$clauseN2->getSlug()] = isset(
                        $clausesValues[$clause->getSlug()][$clauseN2->getSlug()]
                    );

                    continue;
                }
                foreach ($clauseN2->getClausesN3() as $clauseN3) {
                    $data[$clause->getSlug()][$clauseN2->getSlug()][$clauseN3->getSlug()] = isset(
                        $clausesValues[$clause->getSlug()][$clauseN2->getSlug()][$clauseN3->getSlug()]
                    );
                }
            }
        }
        
        if (array_key_exists('clauseEnvironnementale', $data)) {
            $data['clausesEnvironnementales'] = $data['clauseEnvironnementale'];
            unset($data['clauseEnvironnementale']);
        }

        return $data;
    }

    public function addExtraData(
        array $normalizedMarket,
        array $data,
        ContratTitulaire $object
    ): array {
        $data['idMarche'] = $normalizedMarket['id'];
        $data['typeContrat'] = $object->getIdTypeContrat();
        $data['lieuExecutions'] = $normalizedMarket['lieuExecution'];
        $data['nature'] = $normalizedMarket['nature'];
        $data['marcheInnovant'] = $object->isMarcheInnovant() ? '1' : '0';
        $data['idChapeauAcSad'] = $normalizedMarket['donneesComplementaires']
        ['contratsChapeaux']['contratChapeauAcSad']['idTechniqueContratChapeauAcSad'];
        $data['idChapeauMultiAttributaire'] = $normalizedMarket['donneesComplementaires']['contratsChapeaux']
        ['contratChapeauMultiAttributaires']['idTechniqueContratChapeauMultiAttributaires'];
        $data['naturePassation'] = $normalizedMarket['donneesComplementaires']['naturePassation'];
        $data['procedurePassation'] = $normalizedMarket['procedure'];
        $data['nbTotalPropositionsRecu'] = $object->getNbTotalPropositionsLot();
        $data['consultation'] = $normalizedMarket['donneesComplementaires']['consultation'];
        $data['lots'] = $normalizedMarket['donneesComplementaires']['lot'];
        $data['oldIdService'] = $object->getOldServiceId();
        $data['acheteur'] = $normalizedMarket['acheteur'];
        $data['titulaire'] = $normalizedMarket['titulaires'];
        $data['publicationContrat'] = $object->getPublicationContrat();
        if (isset($data['acheteur']['accessChorus'])) {
            $data['acheteur']['accesChorus'] = $data['acheteur']['accessChorus'];
            unset($data['acheteur']['accessChorus']);
        }

        if (isset($data['titulaire'][0]['id'])) {
            $data['titulaire']['denominationSociale'] = $data['titulaire'][0]['denominationSociale'];
            $data['titulaire']['typeIdentifiantEntreprise'] = $data['titulaire'][0]['typeIdentifiant'];
            $data['titulaire']['nom'] = $object->getContact()->getNom();
            $data['titulaire']['prenom'] = $object->getContact()->getPrenom();
            $data['titulaire']['email'] = $object->getContact()->getEmail();
            $data['titulaire']['telephone'] = $object->getContact()->getTelephone();
            if ($object->getContact()->getIdInscrit()) {
                /* @var Inscrit $inscrit */
                $inscrit = $this->inscritRepository->find($object->getContact()->getIdInscrit());
                $data['titulaire']['identifiantTechnique'] = $inscrit->getId();
                $data['titulaire']['login'] = $inscrit->getLogin();
                $data['titulaire']['adresse'] = $inscrit->getAdresse();
                $data['titulaire']['actif'] = $inscrit->getBloque() ? 0 : 1;
                $data['titulaire']['siren'] = $inscrit->getEntreprise()->getSiren();
                $data['titulaire']['codeEtablissement'] = $inscrit->getEtablissement()->getCodeEtablissement();
                if ($inscrit->getEntreprise()->getSirenetranger()) {
                    $data['titulaire']['identifiantNationalEntreprise'] = $inscrit->getEntreprise()->getSirenetranger();
                }
                $data['titulaire']['etablissement'] = $inscrit->getEtablissement()->getIdEtablissement();
                $data['titulaire']['entreprise'] = $inscrit->getEntreprise()->getId();
                $data['titulaire']['dateModificationRgpd'] = $inscrit->getDateValidationRgpd();
                $rgpd = [];

                if ($inscrit->isRgpdCommunicationPlace()) {
                    $rgpd[] = 'COMMUNICATION_PLACE';
                }

                if ($inscrit->getRgpdCommunication()) {
                    $rgpd[] = 'COMMUNICATION_SIA';
                }

                if ($inscrit->isRgpdEnquete()) {
                    $rgpd[] = 'COMMUNICATION_ENQUETE';
                }

                $data['titulaire']['rgpd'] = $rgpd;
            }
            unset($data['titulaire'][0]);
        }

        unset($data['clauses']);

        if (isset($normalizedMarket['dateSignature'])) {
            $data['dateSignature'] = $normalizedMarket['dateSignature'];
        }

        unset($data['idChapeau']);

        if (isset($data['cpv']['codeSecondaire1']) && '' !== $data['cpv']['codeSecondaire1']) {
            $codeSeondaires = explode('#', $data['cpv']['codeSecondaire1']);
            unset($data['cpv']['codeSecondaire1']);
            $key = 1;
            foreach ($codeSeondaires as $codeSeondaire) {
                if ('' !== $codeSeondaire) {
                    $data['cpv']['codeSecondaire' . $key] = $codeSeondaire;
                    $key++;
                }
            }
        }

        return $data;
    }

    private function getClausesValues(ContratTitulaire $contratTitulaire): array
    {
        $clausesValues = [];
        foreach ($contratTitulaire->getClausesN1() as $clause) {
            foreach ($clause->getClausesN2() as $clauseN2) {
                if (0 === $clauseN2->getClausesN3()->count()) {
                    $clausesValues[$clause->getReferentielClauseN1()->getSlug()]
                    [$clauseN2->getReferentielClauseN2()->getSlug()] = true;
                }
                foreach ($clauseN2->getClausesN3() as $clauseN3) {
                    $clausesValues[$clause->getReferentielClauseN1()->getSlug()]
                    [$clauseN2->getReferentielClauseN2()->getSlug()]
                    [$clauseN3->getReferentielClauseN3()->getSlug()] = true;
                }
            }
        }

        return $clausesValues;
    }
}
