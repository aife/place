<?php

namespace App\Serializer;

use DateTime;
use App\Entity\Entreprise;
use App\Exception\ApiProblemInvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class EntrepriseNormalizer extends AbstractApiPlatformDisabledNormalizer implements NormalizerInterface
{
    public function __construct(private readonly ObjectNormalizer $normalizer, private readonly EtablissementNormalizer $normalizerEtablissement)
    {
    }

    public function normalize($entreprise, $format = null, array $context = [])
    {
        if (!$entreprise instanceof Entreprise) {
            throw new ApiProblemInvalidArgumentException("l'objet n'est pas une instance de Entreprise");
        }
        $data = $this->normalizer->normalize($entreprise, $format, $context);

        $rue = $data['adresse'];
        unset($data['adresse']);
        $data['adresse']['ville'] = !empty($data['ville']) ? $data['ville'] : '';
        $data['adresse']['rue'] = $rue;
        $data['adresse']['codePostal'] = $data['codepostal'];
        $data['adresse']['pays'] = $data['pays'];
        $data['adresse']['acronymePays'] = $entreprise->getAcronymePays();
        $data['formeJuridique'] = $entreprise->getFormejuridique();
        $data['codeAPE'] = $entreprise->getCodeape();
        $data['libelleAPE'] = $entreprise->getLibelleApe();
        if (!empty($entreprise->getDateModification())) {
            $data['dateModification'] = new DateTime($entreprise->getDateModification());
            if (empty($data['dateModification'])) {
                unset($data['dateModification']);
            }
        }

        if (!empty($entreprise->getDateCreation())) {
            $data['dateCreation'] = new DateTime($entreprise->getDateCreation());
            if (empty($data['dateCreation'])) {
                unset($data['dateCreation']);
            }
        }

        if (is_numeric($entreprise->getCapitalSocial())) {
            $data['capitalSocial'] = $entreprise->getCapitalSocial();
        }
        //A RETIRER SI BESOIn
        $data['raisonSociale'] = $entreprise->getNom();

        if (!empty($entreprise->getEmail())) {
            $data['email'] = $entreprise->getEmail();
        }

        // Modifier les SSS si besoin
        $etablissements = $entreprise->getEtablissements();
        foreach ($etablissements as $etablissement) {
            $data['etablisssements']['etablissement'][] = $this->normalizerEtablissement->normalize($etablissement, $format, $context);
        }

        unset($data['codepostal']);
        unset($data['pays']);
        unset($data['ville']);

        return $data;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return parent::supportsNormalization($data, $format, $context) && $data instanceof Entreprise;
    }
}
