<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Serializer;

abstract class AbstractApiPlatformDisabledNormalizer
{
    public final const CALLED_BY_API_PLATFORM = 'api-platform';

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return empty($context[self::CALLED_BY_API_PLATFORM]);
    }
}
