<?php

namespace App\Serializer\Messec;

use App\Model\Messec\Email;
use App\Model\Messec\Message;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class EmailDenormalizer.
 */
class EmailDenormalizer implements DenormalizerInterface
{
    /**
     * EmailDenormalizer constructor.
     */
    public function __construct(private readonly ObjectNormalizer $delegate, private readonly MessageDenormalizer $messageDenormalizer)
    {
    }

    /**
     * @param mixed  $data
     * @param string $type
     * @param null   $format
     */
    public function denormalize($data, $type, $format = null, array $context = []): array|object
    {
        $emails = [];
        if (is_array($data)) {
            foreach ($data as $dataEmail) {
                if (isset($dataEmail['message'])) {
                    $dataEmail['message'] = $this->messageDenormalizer->denormalize(
                        $dataEmail['message'],
                        Message::class,
                        $format
                    );
                }
                if (!empty($dataEmail['reponse'])) {
                    $dataEmail['reponse'] = $this->denormalize([$dataEmail['reponse']], $type, $format);
                }
                $emails[] = $this->delegate->denormalize($dataEmail, $type, $format);
            }
        }
        if (1 == count($emails)) {
            $emails = $emails[0];
        }

        return $emails;
    }

    /**
     * @param mixed  $data
     * @param string $type
     * @param null   $format
     *
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        if (Email::class != $type) {
            return false;
        }

        return true;
    }
}
