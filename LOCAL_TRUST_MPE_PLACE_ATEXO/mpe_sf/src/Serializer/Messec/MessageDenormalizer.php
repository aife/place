<?php

namespace App\Serializer\Messec;

use App\Model\Messec\Message;
use App\Model\Messec\PieceJointe;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class MessageDenormalizer.
 */
class MessageDenormalizer implements DenormalizerInterface
{
    /**
     * MessageDenormalizer constructor.
     */
    public function __construct(private readonly ObjectNormalizer $delegate, private readonly PieceJointeDenormalizer $pieceJointeDenormalizer)
    {
    }

    /**
     * @param mixed  $data
     * @param string $type
     * @param null   $format
     */
    public function denormalize($data, $type, $format = null, array $context = []): array|Message
    {
        $message = new Message();
        if (!empty($data)) {
            if (isset($data['piecesJointes'])) {
                $tempPJs = [];
                foreach ($data['piecesJointes'] as $pieceJointe) {
                    $tempPJs[] = $this->pieceJointeDenormalizer->denormalize($pieceJointe, PieceJointe::class, $format);
                }
                $data['piecesJointes'] = $tempPJs;
            }
            $message = $this->delegate->denormalize($data, $type, $format);
        }

        return $message;
    }

    /**
     * @param mixed  $data
     * @param string $type
     * @param null   $format
     *
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        if (Message::class != $type) {
            return false;
        }

        return true;
    }
}
