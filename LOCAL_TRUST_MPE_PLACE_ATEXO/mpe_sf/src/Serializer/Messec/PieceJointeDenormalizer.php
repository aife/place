<?php

namespace App\Serializer\Messec;

use App\Model\Messec\PieceJointe;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class PieceJointeDenormalizer.
 */
class PieceJointeDenormalizer implements DenormalizerInterface
{
    /**
     * PieceJointeDenormalizer constructor.
     */
    public function __construct(private readonly ObjectNormalizer $delegate)
    {
    }

    /**
     * @param mixed  $data
     * @param string $type
     * @param null   $format
     */
    public function denormalize($data, $type, $format = null, array $context = []): array|PieceJointe
    {
        $pieceJointe = new PieceJointe();
        if (!empty($data)) {
            $pieceJointe = $this->delegate->denormalize($data, $type, $format);
        }

        return $pieceJointe;
    }

    /**
     * @param mixed  $data
     * @param string $type
     * @param null   $format
     *
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        if (PieceJointe::class != $type) {
            return false;
        }

        return true;
    }
}
