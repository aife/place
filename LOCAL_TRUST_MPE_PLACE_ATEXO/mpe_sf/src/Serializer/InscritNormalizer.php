<?php
/**
 * Created by PhpStorm.
 * User: Nahed BEN SALEM
 * Date: 06/09/19
 * Time: 11:45.
 */

namespace App\Serializer;

use DateTime;
use App\Entity\Inscrit;
use App\Exception\ApiProblemInvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class InscritNormalizer extends AbstractApiPlatformDisabledNormalizer implements ContextAwareNormalizerInterface
{
    public function __construct(private readonly ObjectNormalizer $normalizer)
    {
    }

    public function normalize($inscrit, $format = null, array $context = [])
    {
        if (!$inscrit instanceof Inscrit) {
            throw new ApiProblemInvalidArgumentException("L'objet n'est pas une instance d'Inscrit");
        }

        $data = $this->normalizer->normalize($inscrit, $format, $context);

        $data['id'] = $inscrit->getId();
        $data['idEtablissement'] = $inscrit->getIdEtablissement();
        $data['login'] = $inscrit->getLogin();
        $data['motDePasse'] = hash('sha256', $inscrit->getMdp());
        $data['typeHash'] = $inscrit->getTypeHash();
        $data['email'] = $inscrit->getEmail();
        $data['nom'] = $inscrit->getNom();
        $data['prenom'] = $inscrit->getPrenom();

        if (!empty($inscrit->getDateModification())) {
            $data['dateModification'] = new DateTime($inscrit->getDateModification());
            if (empty($data['dateModification'])) {
                unset($data['dateModification']);
            }
        }

        if (!empty($inscrit->getDateCreation())) {
            $data['dateCreation'] = new DateTime($inscrit->getDateCreation());
            if (empty($data['dateCreation'])) {
                unset($data['dateCreation']);
            }
        }

        $adresse = [];
        $adresse['rue'] = $inscrit->getAdresse();
        $adresse['codePostal'] = $inscrit->getCodePostal();
        $adresse['ville'] = $inscrit->getVille();
        $adresse['pays'] = $inscrit->getPays();
        $data['adresse'] = $adresse;
        $data['telephone'] = $inscrit->getTelephone();
        $data['actif'] = !$inscrit->getBloque();
        $data['siret'] = $inscrit->getSiret();
        $data['inscritAnnuaireDefense'] = $inscrit->getInscritAnnuaireDefense();

        return $data;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return parent::supportsNormalization($data, $format, $context) && $data instanceof Inscrit;
    }
}
