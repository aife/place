<?php

/**
 * Created by PhpStorm.
 * User: nouass
 * Date: 21/03/19
 * Time: 09:32.
 */

namespace App\Serializer;

use App\Entity\Entreprise;
use App\Entity\Etablissement;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class EtablissementsNormalizer extends AbstractApiPlatformDisabledNormalizer implements NormalizerInterface
{
    public function __construct(private readonly ObjectNormalizer $normalizer)
    {
    }

    public function normalize($etablissements, $format = null, array $context = [])
    {
        $newData = [];
        $newData['etablissement'] = [];
        foreach ($etablissements as $key => $etablissement) {
            $newData['etablissement'][$key]['id'] = $etablissement->getIdEtablissement();
            $newData['etablissement'][$key]['siret'] = $etablissement->getSiret();
            $newData['etablissement'][$key]['idEntreprise'] = $etablissement->getIdEntreprise();
            $newData['etablissement'][$key]['siege'] = $etablissement->getEstSiege();
            $newData['etablissement'][$key]['dateCreation'] = $etablissement->getDateCreation();
            $newData['etablissement'][$key]['dateModification'] = $etablissement->getDateModification();
            $newData['etablissement'][$key]['adresse']['rue'] = $etablissement->getAdresse();
            $newData['etablissement'][$key]['adresse']['codePostal'] = $etablissement->getCodePostal();
            $newData['etablissement'][$key]['adresse']['ville'] = $etablissement->getVille();
            $newData['etablissement'][$key]['adresse']['pays'] = $etablissement->getPays();
        }

        return $newData;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return
            parent::supportsNormalization($data, $format, $context)
            && ($data instanceof Entreprise || $data instanceof Etablissement)
            ;
    }
}
