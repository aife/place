<?php

namespace App\Serializer;

use App\Entity\Entreprise;
use App\Entity\Etablissement;
use DateTime;
use App\Entity\Consultation;
use App\Entity\ContratTitulaire;
use App\Entity\GeolocalisationN2;
use App\Entity\InvitationConsultationTransverse;
use App\Entity\ModificationContrat;
use App\Entity\Service;
use App\Entity\TrancheArticle133;
use App\Entity\ValeurReferentiel;
use App\Exception\ApiProblemInvalidArgumentException;
use App\Model\TypePrestation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ContratTitulaireNormalizer extends AbstractApiPlatformDisabledNormalizer implements ContextAwareNormalizerInterface
{
    final public const PUBLIE = 'PUBLIE';
    final public const NON_PUBLIE = 'NON_PUBLIE';
    final public const EN_COURS = 'EN_COURS';
    final public const STATUT_PUBLICATION_SN = 'statut';
    final public const DONNEES_ESSENTIELLES = 'donneesEssentielles';

    public function __construct(
        private readonly ObjectNormalizer $normalizer,
        private readonly ConsultationNormalizer $normalizerConsultation,
        private readonly TypeContratNormalizer $normalizeTypeContrat,
        private readonly ContainerInterface $container,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function normalize($contratTitulaire, $format = null, array $context = [])
    {
        if (!$contratTitulaire instanceof ContratTitulaire) {
            throw new ApiProblemInvalidArgumentException("l'objet n'est pas une instance de ContratTitulaire");
        }

        $data = $this->normalizer->normalize($contratTitulaire, $format, $context);
        $data['id'] = $contratTitulaire->getIdContratTitulaire();
        $data['numero'] = $contratTitulaire->getNumero();
        $data['uuid'] = $contratTitulaire->getUuid();
        $data['idOffre'] = $contratTitulaire->getIdOffre();
        $data['statutEJ'] = $contratTitulaire->getStatutEJ();
        $data['lienAcSad'] = $contratTitulaire->getLienACSAD();

        if ($contratTitulaire->getNumEJ()) {
            $data['numEj'] = $contratTitulaire->getNumEJ();
        }

        if (!is_null($contratTitulaire->getIdTitulaireEtab())) {
            $etab = $this->entityManager
                         ->getRepository(Etablissement::class)
                         ->findOneBy(['idEtablissement' => $contratTitulaire->getIdTitulaireEtab()]);
            if (!is_null($etab?->getEntreprise()?->getCategorieEntreprise())) {
                $data['entreprise.categorie'] = $etab->getEntreprise()->getCategorieEntreprise();
            }
        }

        $data['numeroLong'] = $contratTitulaire->getNumeroLong();
        $data['referenceLibre'] = $contratTitulaire->getReferenceLibre();
        $data['objet'] = $contratTitulaire->getObjet();
        $serviceId = null;
        if (true === (bool) getenv('SPECIFIQUE_PLACE')) {
            if ($contratTitulaire->getIdService()) {
                /**
                 * @var Service $service
                 */
                $service = $this->entityManager
                    ->getRepository(Service::class)
                    ->find($contratTitulaire->getIdService());
                $serviceId = $service->getOldId() ?? $service->getId();
            }
        } else {
            $serviceId =  $contratTitulaire->getServiceId();
        }
        $data['idService'] = $serviceId;
        $data['idServiceSynchroEXEC'] = $contratTitulaire->getServiceId();
        $data['idCreateur'] = $contratTitulaire->getIdCreateur();
        if (!empty($contratTitulaire->getFormePrix())) {
            $data['formePrix'] = $contratTitulaire->getFormePrix();
            $data['modaliteRevisionPrix'] = $contratTitulaire->getFormePrix();
        }

        $defenseOuSecurite = null;
        if ($contratTitulaire->getMarcheDefense() == 2) {
            $defenseOuSecurite = 0;
        }
        if ($contratTitulaire->getMarcheDefense() == 1) {
            $defenseOuSecurite = 1;
        }
        $data['defenseOuSecurite'] = $defenseOuSecurite;
        $data['datePrevisionnelleNotification'] = $contratTitulaire->getDatePrevueNotification();
        $data['datePrevisionnelleFinMarche'] = $contratTitulaire->getDatePrevueFinContrat();
        $data['datePrevisionnelleFinMaximaleMarche'] = $contratTitulaire->getDatePrevueMaxFinContrat();
        $data['dureeMaximaleMarche'] = $contratTitulaire->getDureeInitialeContrat();
        $data['dateDebutExecution'] = $contratTitulaire->getDateDebutExecution();

        //Gestion des contrats qui vient d'une consultation
        if ($contratTitulaire->getLieuExecution() !== null) {
            $lieuExecutions = explode(',', $contratTitulaire->getLieuExecution());

            if (is_countable($lieuExecutions)) {
                $data['lieuExecutions'] = '';
                $lieux = $this->entityManager
                    ->getRepository(GeolocalisationN2::class)->getDenominationLieuxExecution($lieuExecutions);
                if (is_countable($lieux)) {
                    foreach ($lieux as $key => $lieu) {
                        $data['lieuExecutions'] .= $lieu . ', ';
                    }
                    $data['lieuExecutions'] = substr($data['lieuExecutions'], 0, -2);
                }
                if (empty($data['lieuExecutions'])) {
                    $data['lieuExecutions'] = $contratTitulaire->getNomLieuPrincipalExecution();
                }
            }
        } else {
            //Gestion des lieux dans le cas de création d'un contrat from scratch
            $data['lieuExecutions'] = $contratTitulaire->getNomLieuPrincipalExecution();
        }

        $data['decisionAttribution'] = $contratTitulaire->getDateAttribution();
        $data['intitule'] = $contratTitulaire->getIntitule();

        if ($contratTitulaire->getIdTrancheBudgetaire() !== null) {
            $trancheBudgetaire = $this->entityManager
                ->getRepository(TrancheArticle133::class)
                ->find($contratTitulaire->getIdTrancheBudgetaire());
            if ($trancheBudgetaire) {
                $data['trancheBudgetaire'] = $trancheBudgetaire->getLibelleTrancheBudgetaire();
            }
        }

        $ccag = $this->entityManager
            ->getRepository(ValeurReferentiel::class)
            ->findOneBy(
                [
                    'id' => $contratTitulaire->getCcagApplicable(),
                    'idReferentiel' => $this->container->getParameter('REFERENTIEL_CCAG_REFERENCE'),
                ]
            );
        $data['ccagApplicable'] = '';

        if ($ccag) {
            $data['ccagApplicable'] = $ccag->getLibelleValeurReferentielFr();
        }


        $data['defenseOuSecurite'] = $contratTitulaire->getMarcheDefense();
        $data['marcheInnovant'] = $contratTitulaire->isMarcheInnovant();
        $data['publicationDonneesEssentielles'] = $contratTitulaire->getPublicationContrat() === 0;
        $data['datePrevisionnelleNotification'] = $contratTitulaire->getDatePrevueNotification();
        $data['datePrevisionnelleFinMarche'] = $contratTitulaire->getDatePrevueFinContrat();
        $data['datePrevisionnelleFinMaximaleMarche'] = $contratTitulaire->getDatePrevueMaxFinContrat();
        $data['dureeMaximaleMarche'] = $contratTitulaire->getDureeInitialeContrat();
        $data['dateDebutExecution'] = $contratTitulaire->getDateDebutExecution();

        $consultation = $this->entityManager
            ->getRepository(Consultation::class)
            ->findOneBy(['id' => $contratTitulaire->getConsultation()]);
        if ($consultation) {
            $data['numeroProjetAchat'] = $consultation->getNumeroProjetAchat() ?? '';
        }

        $data['decisionAttribution'] = $contratTitulaire->getDateAttribution();
        $data['intitule'] = $contratTitulaire->getIntitule();

        if ($contratTitulaire->getIdTrancheBudgetaire() !== null) {
            $trancheBudgetaire = $this->entityManager
                ->getRepository(TrancheArticle133::class)
                ->find($contratTitulaire->getIdTrancheBudgetaire());
            if ($trancheBudgetaire) {
                $data['trancheBudgetaire'] = $trancheBudgetaire->getLibelleTrancheBudgetaire();
            }
        }

        $ccag = $this->entityManager
            ->getRepository(ValeurReferentiel::class)
            ->findOneBy(
                [
                    'id' => $contratTitulaire->getCcagApplicable(),
                    'idReferentiel' => $this->container->getParameter('REFERENTIEL_CCAG_REFERENCE'),
                ]
            );
        $data['ccagApplicable'] = '';

        if ($ccag) {
            $data['ccagApplicable'] = $ccag->getLibelleValeurReferentielFr();
        }
        $data['idAttributeur'] = null;
        if ($data['horsPassation']) {
            $data['idAttributeur'] = $contratTitulaire->getIdAgent();
        }

        if (!empty($contratTitulaire->getConsultation())) {
            $context['contratTitulaire'] = $contratTitulaire;
            $data['consultation'] = $this->normalizerConsultation->normalize(
                $contratTitulaire->getConsultation(),
                $format,
                $context
            );
        }

        if (!empty($contratTitulaire->getTypeContrat())) {
            $data['typeContrat'] = $this->normalizeTypeContrat->normalize(
                $contratTitulaire->getTypeContrat(),
                $format,
                $context
            );
        }

        if ($contratTitulaire->getDateModification() instanceof DateTime) {
            $data['dateModification'] = $contratTitulaire->getDateModification();
        }

        if ($contratTitulaire->getDateCreation() instanceof DateTime) {
            $data['dateCreation'] = $contratTitulaire->getDateCreation();
        }

        if ($contratTitulaire->getDateNotification() instanceof DateTime) {
            $data['dateNotification'] = $contratTitulaire->getDateNotification();
        }
        if ($contratTitulaire->getDateFin() instanceof DateTime) {
            $data['dateFin'] = $contratTitulaire->getDateFin();
        }

        if ($contratTitulaire->getDateMaxFin() instanceof DateTime) {
            $data['dateMaxFin'] = $contratTitulaire->getDateMaxFin();
        }

        $data['statut'] = $this->getStatut($contratTitulaire->getStatutContrat());
        $data['naturePrestation'] = $this->getCategorie($contratTitulaire->getCategorie());
        $data['idTitulaire'] = $contratTitulaire->getIdTitulaire();
        $data['idChapeau'] = $contratTitulaire->getIdChapeau();
        $data['chapeau'] = empty($contratTitulaire->getChapeau()) ? true : false;
        $data['idEtablissementTitulaire'] = $contratTitulaire->getIdTitulaireEtab();
        if (!empty($contratTitulaire->getMontant())) {
            $data['montant'] = $contratTitulaire->getMontant();
        } elseif (!empty($contratTitulaire->getMontantMaxEstime())) {
            $data['montant'] = $contratTitulaire->getMontantMaxEstime();
        }

        $data['idContact'] = $contratTitulaire->getIdContact();
        $data[self::DONNEES_ESSENTIELLES][self::STATUT_PUBLICATION_SN] =
        match ($contratTitulaire->getStatutPublicationSn()) {
            1 => self::PUBLIE,
            2 => self::NON_PUBLIE,
            default => self::EN_COURS,
        }
        ;

        $data[self::DONNEES_ESSENTIELLES]['datePublication'] = $contratTitulaire->getDatePublicationSN();
        $data[self::DONNEES_ESSENTIELLES]['dateModification'] = $contratTitulaire->getDateModificationSN();
        $data[self::DONNEES_ESSENTIELLES]['erreurPublication'] = $contratTitulaire->getErreurSn();
        $data[self::DONNEES_ESSENTIELLES]['idContrat'] = $contratTitulaire->getId();


        if (!empty($contratTitulaire->getCodeCpv1())) {
            $data['cpv'] = [];
            $data['cpv']['codePrincipal'] = $contratTitulaire->getCodeCpv1();
            if (!empty($contratTitulaire->getCodeCpv2())) {
                $data['cpv']['codeSecondaire1'] = $contratTitulaire->getCodeCpv2();
            }
        }

        if (!empty($contratTitulaire->getContratTitulaireFavori())) {
            $data['favoris'] = [];
            foreach ($contratTitulaire->getContratTitulaireFavori() as $favori) {
                $data['favoris']['idAgent'][] = $favori->getIdAgent();
            }
        }

        $entiteEligible = $this->entityManager
            ->getRepository(InvitationConsultationTransverse::class)
            ->findIfContratExists($contratTitulaire->getIdContratTitulaire());
        $prefix = ($format === 'xml') ? '@' : '';

        if (!empty($entiteEligible)) {
            $data['contratTransverse'][$prefix . 'value'] = 'true';

            foreach ($entiteEligible as $key => $value) {
                $data['contratTransverse']['entiteEligible'][$key][$prefix . 'id'] = $value['id'];
                $data['contratTransverse']['entiteEligible'][$key][$prefix . 'acronyme'] = $value['acronyme'];
                $data['contratTransverse']['entiteEligible'][$key][$prefix . 'denomination'] =
                    $value['denominationOrg'];
            }
        } else {
            $data['contratTransverse'][$prefix . 'value'] = 'false';
        }

        if ($contratTitulaire->getIdTitulaire()) {
            $entreprise = $this->entityManager
                ->getRepository(Entreprise::class)->find($contratTitulaire->getIdTitulaire());
            if ($entreprise instanceof Entreprise) {
                $data['entreprise'] = $this->mapEntrepriseData($entreprise);
            }
        }

        if ($contratTitulaire->getIdTitulaireEtab()) {
            $etablissement = $this->entityManager
                ->getRepository(Etablissement::class)->find($contratTitulaire->getIdTitulaireEtab());
            if ($etablissement instanceof Etablissement) {
                $data['etablissement'] = $this->mapEtablissementData($etablissement);
            }
        }

        $modifications = $this->entityManager
            ->getRepository(ModificationContrat::class)
            ->findIfContratExists($contratTitulaire->getIdContratTitulaire());
        if (!empty($modifications)) {
            $data['modifications'][$prefix . 'donneesEssentielles'] = "true";
            foreach ($modifications as $key => $modification) {
                if ($modification instanceof ModificationContrat) {
                    $data['modifications'][self::DONNEES_ESSENTIELLES][$key][self::STATUT_PUBLICATION_SN] =
                    match ($modification->getStatutPublicationSn()) {
                        1 => self::PUBLIE,
                        2 => self::NON_PUBLIE,
                        default => self::EN_COURS,
                    }
                    ;
                    $data['modifications'][self::DONNEES_ESSENTIELLES][$key]['idModification']
                        = $modification->getId();
                    $data['modifications'][self::DONNEES_ESSENTIELLES][$key]['datePublication']
                        = $modification->getDatePublicationSN();
                    $data['modifications'][self::DONNEES_ESSENTIELLES][$key]['dateModification']
                        = $modification->getDateModificationSN();
                    $data['modifications'][self::DONNEES_ESSENTIELLES][$key]['erreurPublication']
                        = $modification->getErreurSn();
                    $data['modifications'][self::DONNEES_ESSENTIELLES][$key]['idContrat']
                        = $modification->getIdContratTitulaire()->getId();
                }
            }
        }

        return $data;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return parent::supportsNormalization($data, $format, $context) && $data instanceof ContratTitulaire;
    }

    /**
     * Récupération du libellé statut.
     *
     * @param $statut
     *
     * @return string
     *
     */
    public function getStatut($statut)
    {
        $lib = '';
        $lib = match ((string)$statut) {
            $this->container->getParameter('STATUT_DECISION_CONTRAT') => 'STATUT_DECISION_CONTRAT',
            $this->container->getParameter('STATUT_DONNEES_CONTRAT_A_SAISIR') => 'STATUT_DONNEES_CONTRAT_A_SAISIR',
            $this->container->getParameter('STATUT_NUMEROTATION_AUTONOME') => 'STATUT_NUMEROTATION_AUTONOME',
            $this->container->getParameter('STATUT_NOTIFICATION_CONTRAT') => 'STATUT_NOTIFICATION_CONTRAT',
            $this->container->getParameter(
                'STATUT_NOTIFICATION_CONTRAT_EFFECTUEE'
            ) => 'STATUT_NOTIFICATION_CONTRAT_EFFECTUEE',
            default => $lib,
        };

        return $lib;
    }

    /**
     * Récupération du libellé de la catégorie.
     *
     * @param $idCategorie
     *
     * @return string
     */
    public function getCategorie($idCategorie)
    {
        return match ($idCategorie) {
            (int) $this->container->getParameter('TYPE_PRESTATION_TRAVAUX') => TypePrestation::TYPE_PRESTATION_TRAVAUX,
            (int) $this->container->getParameter(
                'TYPE_PRESTATION_FOURNITURES'
            ) => TypePrestation::TYPE_PRESTATION_FOURNITURES,
            (int) $this->container->getParameter('TYPE_PRESTATION_SERVICES') => TypePrestation::TYPE_PRESTATION_SERVICES,
            default => '',
        };
    }

    /**
     * @return array<string, array<string, string>|int|string|null> $entreprise
     */
    private function mapEntrepriseData(Entreprise $entreprise): array
    {
        $data = [];
        $data ['id'] = $entreprise->getId();
        $data ['siren'] = $entreprise->getSiren();
        $data ['telephone'] = $entreprise->getTelephone();
        $data ['siteInternet'] = $entreprise->getSiteInternet() ?: null;
        $data ['idExterne'] = $entreprise->getIdExterne();
        $data ['adresse']['ville'] = $entreprise->getVilleadresse();
        $data ['adresse']['rue'] = $entreprise->getAdresse();
        $data ['adresse']['codePostal'] = $entreprise->getCodepostal();
        $data ['adresse']['pays'] = $entreprise->getPaysadresse();
        $data ['adresse']['acronymePays'] = $entreprise->getPaysenregistrement();
        $data ['formeJuridique'] = $entreprise->getFormejuridique() ?: null;
        $data ['codeAPE'] = $entreprise->getCodeape() ?: null;
        $data ['libelleAPE'] = $entreprise->getLibelleApe();
        $data ['dateModification'] = $entreprise->getDateModification();
        $data ['dateCreation'] = $entreprise->getDateCreation();
        $data ['capitalSocial'] = $entreprise->getCapitalSocial();
        $data ['raisonSociale'] = $entreprise->getNom() ?: null;

        return $data;
    }

    /**
     * @return array<string, array<string,string>|\DateTime|int|string|null> $etablissement
     */
    private function mapEtablissementData(Etablissement $etablissement): array
    {
        $data = [];
        $data['id'] = $etablissement->getIdEtablissement();
        $data['idExterne'] = $etablissement->getIdExterne();
        $data['siege'] = $etablissement->getEstSiege();
        $data['siret'] = $etablissement->getCodeEtablissement();
        $data['dateCreation'] = $etablissement->getDateCreation();
        $data['dateModification'] = $etablissement->getDateModification();
        $data['adresse']['rue'] = $etablissement->getAdresse();
        $data['adresse']['codePostal'] = $etablissement->getCodePostal();
        $data['adresse']['ville'] = $etablissement->getVille();
        $data['adresse']['pays'] = $etablissement->getPays();

        return $data;
    }
}
