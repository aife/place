<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Serializer\Messenger\Tncp;

use App\Message\Tncp\Consumer\AnnonceSuiviMessage;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\MessageDecodingFailedException;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;

class AnnonceSuiviJsonMessageSerializer implements SerializerInterface
{
    private const DATE_PUBLICATION = 'date_publication';
    private const URL_BOAMP = 'urlBOAMP';
    private const URL_PDF = 'urlPDF';

    public function decode(array $encodedEnvelope): Envelope
    {
        $stamps = [];
        $headers = $encodedEnvelope['headers'];
        if (isset($headers['stamps'])) {
            $stamps = unserialize($headers['stamps']);
        }

        $body = $encodedEnvelope['body'];
        $data = json_decode($body, true);
        if (null === $data) {
            throw new MessageDecodingFailedException('Invalid JSON');
        }
        $datePublication = isset($data[self::DATE_PUBLICATION]) && !empty($data[self::DATE_PUBLICATION]) ? new \DateTime($data[self::DATE_PUBLICATION]) : null;
        $urlBoamp = isset($data[self::URL_BOAMP]) && !empty($data[self::URL_BOAMP]) ? $data[self::URL_BOAMP] : null;
        $urlPdf = isset($data[self::URL_PDF]) && !empty($data[self::URL_PDF]) ? $data[self::URL_PDF] : null;

        $message = new AnnonceSuiviMessage(
            $data['uuid'],
            $data['consultation']['id'],
            $data['consultation']['reference'],
            $data['statut'],
            new \DateTime($data['date_verification']),
            $data['type'],
            $data['organisme'],
            $datePublication,
            $urlPdf,
            $urlBoamp
        );

        return new Envelope($message, $stamps);
    }

    public function encode(Envelope $envelope): array
    {
        throw new \Exception('Transport & serializer not meant for sending messages');
    }
}
