<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Serializer\Messenger\Tncp;

use DateTime;
use Exception;
use App\Message\Tncp\Consumer\MessageSuiviConsultationInput;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\MessageDecodingFailedException;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;

class SuiviConsultationJsonMessageSerializer implements SerializerInterface
{
    public function decode(array $encodedEnvelope): Envelope
    {
        $stamps = [];
        $headers = $encodedEnvelope['headers'];
        if (isset($headers['stamps'])) {
            $stamps = unserialize($headers['stamps']);
        }

        $body = $encodedEnvelope['body'];
        $data = json_decode($body, true);
        if (null === $data) {
            throw new MessageDecodingFailedException('Invalid JSON');
        }

        $message = new MessageSuiviConsultationInput(
            $data['uuid'],
            $data['flux'],
            $data['type'],
            $data['statut'],
            $data['idObjetSource'] ?? null,
            (int) $data['idObjetDestination'],
            $data['message'] ?? null,
            $data['uuidPlateforme'],
            $data['messageDetails'] ?? null,
            new DateTime($data['dateEnvoi']),
            $data['etape'] ?? null
        );

        return new Envelope($message, $stamps);
    }

    public function encode(Envelope $envelope): array
    {
        throw new Exception('Transport & serializer not meant for sending messages');
    }
}
