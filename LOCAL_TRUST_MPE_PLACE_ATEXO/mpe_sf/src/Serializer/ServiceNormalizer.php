<?php

namespace App\Serializer;

use App\Entity\AffiliationService;
use DateTime;
use App\Entity\Service;
use App\Exception\ApiProblemInvalidArgumentException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ServiceNormalizer extends AbstractApiPlatformDisabledNormalizer implements ContextAwareNormalizerInterface
{
    public function __construct(private readonly ObjectNormalizer $normalizer)
    {
    }

    public function normalize($service, $format = null, array $context = [])
    {
        if (!$service instanceof Service) {
            throw new ApiProblemInvalidArgumentException("l'objet n'est pas une instance de Service");
        }
        $data = $this->normalizer->normalize($service, $format, $context);
        $data['acronymeOrganisme'] = $service->getAcronymeOrg();
        $data['email'] = $service->getMail();
        if (!empty($service->getDateCreation())) {
            $data['dateCreation']
                = new DateTime($service->getDateCreation());
        }
        if (!empty($service->getDateModification())) {
            $data['dateModification']
                = new DateTime($service->getDateModification());
        }
        if (true === (bool) getenv('SPECIFIQUE_PLACE')) {
            $data['id'] = $service->getOldId() ?? $service->getId();
            $data['idExterne'] = $service->getOldId() ?? $service->getId();
            $data['idParent'] = $service->getOldIdParent() ?? $service->getIdParent();
            $data['idExterneParent'] = $service->getOldIdParent() ?? $service->getIdExterneParent();
        } else {
            $data['idParent'] = $service->getIdParent();
            $data['idExterneParent'] = $service->getIdExterneParent();
        }

        // Champ ajouté dans le cadre de la synchro EXEC-MPE
        $data['idSynchroExec'] = $service->getId();
        $data['accesChorus'] = $service->getAccesChorus();

        return $data;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return parent::supportsNormalization($data, $format, $context) && $data instanceof Service;
    }
}
