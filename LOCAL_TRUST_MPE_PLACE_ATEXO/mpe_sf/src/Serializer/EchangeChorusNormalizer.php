<?php

namespace App\Serializer;

use ArrayObject;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use App\Entity\Chorus\ChorusEchange;
use App\Entity\Chorus\ChorusGroupementAchat;
use App\Entity\Chorus\ChorusOrganisationAchat;
use App\Entity\ContratTitulaire;
use App\Exception\ApiProblemInvalidArgumentException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class EchangeChorusNormalizer
 * @package App\Serializer
 */
class EchangeChorusNormalizer implements NormalizerInterface
{
    /**
     * EchangeChorusNormalizer constructor.
     * @param ObjectNormalizer $normalizer
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface $logger
     */
    public function __construct(
        private readonly ObjectNormalizer $normalizer,
        private EntityManagerInterface $entityManager,
        private LoggerInterface $logger
    ) {
    }

    /**
     * @param mixed $chorusEchange
     * @param null $format
     * @return array|ArrayObject|bool|float|int|mixed|string|null
     * @throws ExceptionInterface
     */
    public function normalize($chorusEchange, $format = null, array $context = [])
    {
        if (!$chorusEchange instanceof ChorusEchange) {
            throw new ApiProblemInvalidArgumentException("l'objet n'est pas une instance de chorus echange");
        }

        $data = $this->normalizer->normalize($chorusEchange, $format, $context);
        if (null !== $chorusEchange->getIdGa()) {
            $chorusGroupementAchat = $this->entityManager
                ->getRepository(ChorusGroupementAchat::class)->findOneBy([
                    'id' =>  $chorusEchange->getIdGa(),
                    'organisme' =>  $chorusEchange->getOrganisme(),
                    'actif' => 1
                ]);

            if ($chorusGroupementAchat instanceof ChorusGroupementAchat) {
                $data['groupementAchat'] = $chorusGroupementAchat->getLibelle();
            } else {
                $this->logger->error(
                    'Groupement Achat manquant pour l\'échange chorus : ' . $chorusEchange->getId()
                );
            }
        }

        if (null !== $chorusEchange->getIdOa()) {
            $chorusOrganisationAchat = $this->entityManager
                ->getRepository(ChorusOrganisationAchat::class)->findOneBy([
                    'id' =>   $chorusEchange->getIdOa(),
                    'organisme' =>  $chorusEchange->getOrganisme()
                ]);
            if ($chorusOrganisationAchat instanceof ChorusOrganisationAchat) {
                $data['organisationAchat'] = $chorusOrganisationAchat->getLibelle();
            } else {
                $this->logger->error(
                    'Organisation Achat manquant pour l\'échange chorus : ' . $chorusEchange->getId()
                );
            }
        }

        $data['idContrat'] = $chorusEchange->getIdDecision();

        $data['retour_chorus'] = [
            'code' => $chorusEchange->getRetourChorus(),
            'libelle' => $chorusEchange->getRetourChorusLibelle(),
        ];

        $data['statut_echange'] = [
            'code' => $chorusEchange->getStatutEchange(),
            'libelle' => $chorusEchange->getStatutEchangeLibelle(),
        ];

        $data['message_erreur'] = $chorusEchange->getErreurRejet();

        return $data;
    }

    /**
     * @param mixed $data
     * @param null $format
     */
    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof ChorusEchange;
    }
}
