<?php

namespace App\Serializer;

use App\Entity\TypeContrat;
use App\Exception\ApiProblemInvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class TypeContratNormalizer extends AbstractApiPlatformDisabledNormalizer implements ContextAwareNormalizerInterface
{
    public function __construct(private readonly ObjectNormalizer $normalizer)
    {
    }

    public function normalize($typeContrat, $format = null, array $context = [])
    {
        if (!$typeContrat instanceof TypeContrat) {
            throw new ApiProblemInvalidArgumentException("l'objet n'est pas une instance de TypeContrat");
        }

        $data = $this->normalizer->normalize($typeContrat, $format, $context);
        $data['associationProcedures'] = null;

        return $data;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return parent::supportsNormalization($data, $format, $context) && $data instanceof TypeContrat;
    }
}
