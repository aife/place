<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Serializer\EchangeDocumentaire;

use App\Entity\Consultation;
use App\Entity\ContratTitulaire;
use App\Entity\Entreprise;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use App\Service\ContratService;
use App\Service\WebServices\WebServicesExec;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class EchangeDocConsLotContratNormalizer extends AbstractApiPlatformDisabledNormalizer implements ContextAwareNormalizerInterface
{
    public final const CONSULTATION_CONTRAT_LIE_ID = 'id';
    public final const CONSULTATION_CONTRAT_LIE_NUMERO = 'numero';
    public final const CONSULTATION_CONTRAT_ATTRIBUTAIRE = 'attributaire';
    public final const CONSULTATION_CONTRAT_MONTANT = 'montant';
    public final const CONSULTATION_CONTRAT_LIE_UUID = 'uuid';

    /**
     * EchangeDocConsLotContratNormalizer constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ContratService $contratService,
        private readonly ParameterBagInterface $parameterBag,
        private readonly LoggerInterface $logger,
        private readonly WebServicesExec $webServicesExec,
    ) {
    }

    /**
     * @param mixed $echangeDoc
     * @param null  $format
     *
     * @return array|bool|float|int|string|null
     */
    public function normalize($echangeDoc, $format = null, array $context = [])
    {
        $attributaire = null;
        $data = [];

        if ($this->parameterBag->get('ACTIVE_EXEC_V2')) {
            if ($echangeDoc->getUuidExterneExec()) {
                $data = [
                    self::CONSULTATION_CONTRAT_LIE_UUID => $echangeDoc->getUuidExterneExec(),
                ];
            }
        } else {
            $contratLie = $echangeDoc->getContratTitulaire();
            if ($contratLie instanceof ContratTitulaire) {
                $idContratTitulaire = $contratLie->getIdContratTitulaire();
                $titulaire = $this->em
                    ->getRepository(Entreprise::class)
                    ->find($contratLie->getIdTitulaire());
                if ($titulaire instanceof Entreprise) {
                    $attributaire = $titulaire->getNom();
                }

                $data = [
                    self::CONSULTATION_CONTRAT_LIE_ID => $idContratTitulaire,
                    self::CONSULTATION_CONTRAT_ATTRIBUTAIRE => $attributaire ?? '',
                    self::CONSULTATION_CONTRAT_LIE_NUMERO => $contratLie->getNumeroContrat(),
                    self::CONSULTATION_CONTRAT_MONTANT => $this->contratService->getMontantMarche($idContratTitulaire),
                ];
            }
        }

        return $data;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return parent::supportsNormalization($data, $format, $context) && $data instanceof Consultation;
    }
}
