<?php

namespace App\Serializer\EchangeDocumentaire;

/**
 * Class EchangeDocNormalizer.
 */
class EchangeDocNormalizer
{
    /**
     * @param $historiques
     *
     * @return array
     */
    public function adresserDonneesHistorique($historiques)
    {
        $data = [];
        foreach ($historiques as $key => $historique) {
            $data['historique'][$historique['id']] = $historique;
        }

        return $data;
    }
}
