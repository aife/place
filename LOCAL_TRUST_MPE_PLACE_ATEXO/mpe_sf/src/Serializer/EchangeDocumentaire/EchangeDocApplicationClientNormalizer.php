<?php

namespace App\Serializer\EchangeDocumentaire;

use App\Entity\EchangeDocumentaire\EchangeDoc;
use App\Entity\EchangeDocumentaire\EchangeDocApplicationClient;
use App\Exception\ApiProblemInvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class EchangeDocApplicationClientNormalizer implements NormalizerInterface
{
    final public const ECHANGE_DOC_APPLICATION = 'echangeDocApplication';
    final public const ECHANGE_DOC_APPLICATION_CLIENT = 'echangeDocApplicationClient';
    final public const ECHANGE_DOC_CHEMINEMENT = 'cheminement';
    final public const ECHANGE_DOC_SIGNATURE = 'signature';
    final public const ECHANGE_DOC_TDT = 'tdt';
    final public const ECHANGE_DOC_GED = 'ged';
    final public const ECHANGE_DOC_SAE = 'sae';
    final public const ECHANGE_DOC_CLASSIFICATIONS = 'classifications';
    final public const ECHANGE_DOC_CLASSIFICATION_1 = 'classification1';
    final public const ECHANGE_DOC_CLASSIFICATION_2 = 'classification2';
    final public const ECHANGE_DOC_CLASSIFICATION_3 = 'classification3';
    final public const ECHANGE_DOC_CLASSIFICATION_4 = 'classification4';
    final public const ECHANGE_DOC_CLASSIFICATION_5 = 'classification5';
    final public const CODE = 'code';
    final public const LIBELLE = 'libelle';
    final public const ENVOI_DIC = 'envoiDic';
    final public const MULTI_DOCS_PRINCIPAUX = 'multiDocsPrincipaux';

    public function __construct(
        private readonly ObjectNormalizer $normalizer,
        private readonly EchangeDocApplicationNormalizer $normalizerEchangeDocApplication
    ) {
    }

    public function normalize($echangeDocApplicationClient, $format = null, array $context = [])
    {
        if (!$echangeDocApplicationClient instanceof EchangeDocApplicationClient) {
            throw new ApiProblemInvalidArgumentException("l'objet n'est pas une instance de EchangeDocApplicationClient");
        }

        $data = $this->normalizer->normalize($echangeDocApplicationClient, $format, $context);
        $echangeDocApplication = $this->normalizerEchangeDocApplication->normalize(
            $echangeDocApplicationClient->getEchangeDocApplication(),
            $format,
            $context
        );

        $data[self::ECHANGE_DOC_APPLICATION][self::CODE] = $echangeDocApplication[self::CODE];
        $data[self::ECHANGE_DOC_APPLICATION][self::LIBELLE] = $echangeDocApplication[self::LIBELLE];
        $data[self::ECHANGE_DOC_APPLICATION][self::ENVOI_DIC] = $echangeDocApplication[self::ENVOI_DIC];

        $data[self::ECHANGE_DOC_APPLICATION_CLIENT][self::CODE] = $echangeDocApplicationClient->getCode();
        $data[self::ECHANGE_DOC_APPLICATION_CLIENT][self::LIBELLE] = $echangeDocApplicationClient->getLibelle();
        $data[self::ECHANGE_DOC_APPLICATION_CLIENT][self::ECHANGE_DOC_CLASSIFICATIONS] = [
            self::ECHANGE_DOC_CLASSIFICATION_1 => $context[EchangeDocumentaireNormalizer::SOUS_TYPE_PARAPHEUR] ?? null,
            self::ECHANGE_DOC_CLASSIFICATION_2 => $echangeDocApplicationClient->getClassification2(),
            self::ECHANGE_DOC_CLASSIFICATION_3 => $echangeDocApplicationClient->getClassification3(),
            self::ECHANGE_DOC_CLASSIFICATION_4 => $echangeDocApplicationClient->getClassification4(),
            self::ECHANGE_DOC_CLASSIFICATION_5 => $echangeDocApplicationClient->getClassification5(),
        ];
        $data[self::ECHANGE_DOC_APPLICATION_CLIENT][self::MULTI_DOCS_PRINCIPAUX] = (string) $echangeDocApplicationClient->isMultiDocsPrincipaux();

        $echangeDocs = $echangeDocApplicationClient->getEchangeDocs();
        foreach ($echangeDocs as $echangeDoc) {
            if ($echangeDoc instanceof EchangeDoc) {
                $data[self::ECHANGE_DOC_CHEMINEMENT] = $this->getCheminement($echangeDoc);
            }
        }

        return $data;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof EchangeDocApplicationClient;
    }

    /**
     * Get Cheminement.
     *
     * @return array
     */
    protected function getCheminement(EchangeDoc $echangeDoc)
    {
        $data = [
            self::ECHANGE_DOC_SIGNATURE => (true === $echangeDoc->isCheminementSignature()) ? 1 : 0,
            self::ECHANGE_DOC_TDT => (true === $echangeDoc->isCheminementTdt()) ? 1 : 0,
            self::ECHANGE_DOC_GED => (true === $echangeDoc->isCheminementGed()) ? 1 : 0,
            self::ECHANGE_DOC_SAE => (true === $echangeDoc->isCheminementSae()) ? 1 : 0,
        ];

        return $data;
    }
}
