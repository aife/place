<?php

namespace App\Serializer\EchangeDocumentaire;

use App\Entity\BloborganismeFile;
use App\Exception\ApiProblemInvalidArgumentException;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class BlobOrganismeEchangeNormalizer extends AbstractApiPlatformDisabledNormalizer implements NormalizerInterface
{
    public function __construct(private readonly ObjectNormalizer $normalizer)
    {
    }

    public function normalize($blobOrganisme, $format = null, array $context = [])
    {
        if (!$blobOrganisme instanceof BloborganismeFile) {
            throw new ApiProblemInvalidArgumentException("l'objet n'est pas une instance de BloborganismeFile");
        }

        $id = $blobOrganisme->getId();
        $data = $this->normalizer->normalize($blobOrganisme, $format, $context);
        $data['id'] = $id;
        $data['name'] = $blobOrganisme->getName();

        return $data;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return parent::supportsNormalization($data, $format, $context) && $data instanceof BloborganismeFile;
    }
}
