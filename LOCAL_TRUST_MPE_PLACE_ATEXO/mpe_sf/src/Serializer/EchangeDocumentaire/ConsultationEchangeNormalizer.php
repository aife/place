<?php

namespace App\Serializer\EchangeDocumentaire;

use App\Entity\Consultation;
use App\Exception\ApiProblemInvalidArgumentException;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ConsultationEchangeNormalizer extends AbstractApiPlatformDisabledNormalizer implements ContextAwareNormalizerInterface
{
    public function __construct(private readonly ObjectNormalizer $normalizer)
    {
    }

    public function normalize($consultation, $format = null, array $context = [])
    {
        if (!$consultation instanceof Consultation) {
            throw new ApiProblemInvalidArgumentException("l'objet n'est pas une instance de Consultation");
        }

        $data = $this->normalizer->normalize($consultation, $format, $context);
        $data['organisme'] = $consultation->getAcronymeOrg();
        $data['naturePrestation'] = $consultation->getNaturePrestation();
        $data['typeProcedure'] = $consultation->getTypeProcedure()?->getAbbreviation();
        $data['id'] = $consultation->getId();
        $data['reference'] = $consultation->getReferenceUtilisateur();
        $data['idDirectionService'] = $consultation->getServiceId();
        $data['intitule'] = $consultation->getIntitule();
        $data['objet'] = $consultation->getObjet();
        $data['contrat'] = $consultation->getTypeMarche()->getIdTypeContrat();
        $data['idEntite'] = $this->getIdEntite($consultation);

        return $data;
    }

    public function supportsNormalization($data, $format = null, array $context = []) : bool
    {
        return parent::supportsNormalization($data, $format, $context) && $data instanceof Consultation;
    }

    /**
     * Get IdEntity for given consultation.
     *
     * @return string
     */
    protected function getIdEntite(Consultation $consultation)
    {
        $idEntity = null;

        if (null !== $consultation->getService() &&
            0 !== $consultation->getService()->getId() &&
            !empty($consultation->getService()->getIdEntite())
        ) {
            $idEntity = $consultation->getService()->getIdEntite();
        } else {
            $idEntity = $consultation->getOrganisme()->getIdEntite();
        }

        return $idEntity;
    }
}
