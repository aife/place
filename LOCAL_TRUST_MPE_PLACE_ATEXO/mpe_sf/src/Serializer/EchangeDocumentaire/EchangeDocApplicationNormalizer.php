<?php

namespace App\Serializer\EchangeDocumentaire;

use App\Entity\EchangeDocumentaire\EchangeDocApplication;
use App\Exception\ApiProblemInvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class EchangeDocApplicationNormalizer implements NormalizerInterface
{
    public function __construct(private readonly ObjectNormalizer $normalizer)
    {
    }

    public function normalize($echangeDocApplication, $format = null, array $context = [])
    {
        if (!$echangeDocApplication instanceof EchangeDocApplication) {
            throw new ApiProblemInvalidArgumentException("l'objet n'est pas une instance de EchangeDocApplication");
        }

        $data = $this->normalizer->normalize($echangeDocApplication, $format, $context);
        $data['code'] = $echangeDocApplication->getCode();
        $data['libelle'] = $echangeDocApplication->getLibelle();
        $data['envoiDic'] = $echangeDocApplication->isEnvoiDic();

        return $data;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof EchangeDocApplication;
    }
}
