<?php

namespace App\Serializer\EchangeDocumentaire;

use Exception;
use App\Entity\EchangeDocumentaire\EchangeDoc;
use App\Exception\ApiProblemInvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class EchangeDocumentaireNormalizer implements NormalizerInterface
{
    final public const ECHANGE_DOC_DOCUMENTS_GROUP = 'documents';
    final public const ECHANGE_DOC_DOCUMENT = 'document';
    final public const SOUS_TYPE_PARAPHEUR = 'sousTypeParapheur';

    public function __construct(private readonly ObjectNormalizer $normalizer, private readonly ConsultationEchangeNormalizer $normalizerConsultation, private readonly EchangeDocBlobEchangeNormalizer $normalizerBlobOrganisme, private readonly EchangeDocApplicationClientNormalizer $normalizerEchangeDocApplicationClient, private readonly EchangeDocConsLotContratNormalizer $consLotContratNormalizer)
    {
    }

    /**
     * @param mixed $echangeDocumentaires
     * @param null  $format
     *
     * @return array|bool|float|int|mixed|string|null
     *
     * @throws Exception
     */
    public function normalize($echangeDocumentaires, $format = null, array $context = [])
    {
        if (!$echangeDocumentaires instanceof EchangeDoc) {
            throw new ApiProblemInvalidArgumentException("l'objet n'est pas une instance d'echange documentaire");
        }
        $data = $this->normalizer->normalize($echangeDocumentaires, $format, $context);

        $data['id'] = $echangeDocumentaires->getId();
        $data['objet'] = $echangeDocumentaires->getObjet();
        $data['description'] = $echangeDocumentaires->getDescription();

        $context[self::SOUS_TYPE_PARAPHEUR] = $echangeDocumentaires->getReferentielSousTypeParapheur()?->getCode();

        if (!empty($echangeDocumentaires->getEchangeDocApplicationClient())) {
            $data['applicationDistante'] = $this->normalizerEchangeDocApplicationClient->normalize(
                $echangeDocumentaires->getEchangeDocApplicationClient(),
                $format,
                $context
            );
        }
        $data['statut'] = $echangeDocumentaires->getStatut();

        $data['agentCreateur'] = [
            'id' => $echangeDocumentaires->getAgent()?->getId(),
            'nom' => $echangeDocumentaires->getAgent()?->getPrenom() . ' ' . $echangeDocumentaires->getAgent()?->getNom(),
        ];

        $createAt = $echangeDocumentaires->getCreatedAt();
        $data['dateCreation'] = $createAt->format('c');
        $updateAt = $echangeDocumentaires->getUpdatedAt();
        $data['dateModification'] = $updateAt->format('c');

        if (!empty($echangeDocumentaires->getConsultation())) {
            $data['consultation'] = $this->normalizerConsultation->normalize(
                $echangeDocumentaires->getConsultation(),
                $format,
                $context
            );

            $contratLie = $this->consLotContratNormalizer->normalize(
                $echangeDocumentaires
            );
            $data['contratLie'] = (empty($contratLie)) ? null : $contratLie;
        }

        if (!empty($echangeDocumentaires->getEchangeDocBlobs())) {
            foreach ($echangeDocumentaires->getEchangeDocBlobs() as $collection) {
                $data[self::ECHANGE_DOC_DOCUMENTS_GROUP][self::ECHANGE_DOC_DOCUMENT][]
                    = $this->normalizerBlobOrganisme->normalize(
                        $collection,
                        $format,
                        $context
                    );
            }
        }

        return $data;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof EchangeDoc;
    }
}
