<?php

namespace App\Serializer\EchangeDocumentaire;

use App\Entity\EchangeDocumentaire\EchangeDocBlob;
use App\Entity\EchangeDocumentaire\EchangeDocTypePieceActes;
use App\Entity\EchangeDocumentaire\EchangeDocTypePieceStandard;
use App\Exception\ApiProblemInvalidArgumentException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class EchangeDocBlobEchangeNormalizer implements NormalizerInterface
{
    /** @var string */
    private const DOCUMENT_PRINCIPAL = 'documentPrincipal';

    /** @var string */
    private const ANNEXES = 'annexes';

    /** @var string */
    private const ID_ANNEXES = 'idAnnexe';

    /** @var string */
    private const PRIMO_SIGNATURE = 'idPrimoSignature';

    /** @var string */
    private const CATEGORIE_PIECE = 'categoriePiece';

    /** @var string */
    private const ECHANGE_DOC = 'echangeDoc';

    /** @var string */
    private const ECHANGE_DOC_PRINCIPAL = 'docPrincipal';

    public function __construct(private readonly ObjectNormalizer $normalizer, private readonly BlobOrganismeEchangeNormalizer $normalizerBlobOrganisme, private readonly EntityManagerInterface $em)
    {
    }

    /**
     * @param mixed $echangeDocBlob
     * @param null  $format
     *
     * @return array|bool|float|int|mixed|string|null
     */
    public function normalize($echangeDocBlob, $format = null, array $context = [])
    {
        if (!$echangeDocBlob instanceof EchangeDocBlob) {
            throw new ApiProblemInvalidArgumentException("l'objet n'est pas une instance de EchangeDocBlob");
        }

        $data = $this->normalizer->normalize($echangeDocBlob, $format, $context);
        $blob = $this->normalizerBlobOrganisme->normalize(
            $echangeDocBlob->getBlobOrganisme(),
            $format,
            $context
        );

        $data['id'] = $echangeDocBlob->getId();
        $data['nom'] = $blob['name'];

        if (null !== $echangeDocBlob->getChemin()) {
            $chemin = explode('/', $echangeDocBlob->getChemin());
            $index = count($chemin) - 1;
            $data['nom'] = $chemin[$index];
        }

        $data['taille'] = $echangeDocBlob->getPoids();
        $data['checksum'] = $echangeDocBlob->getChecksum();
        $data['type'] = $this->getType($echangeDocBlob);
        $data['relations'] = $this->getRelations($echangeDocBlob);

        return $data;
    }

    /**
     * @param mixed $data
     * @param null  $format
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof EchangeDocBlob;
    }

    protected function getRelations(EchangeDocBlob $echangeDocBlob): array
    {
        $data = [
            self::DOCUMENT_PRINCIPAL => $this->isDocumentPrincipal($echangeDocBlob->getCategoriePiece()),
            self::ANNEXES => [],
            self::PRIMO_SIGNATURE => null,
        ];

        if (1 === $data[self::DOCUMENT_PRINCIPAL]) {
            $data[self::ANNEXES] = $this->getAnnexes($echangeDocBlob);
            $data[self::PRIMO_SIGNATURE] = $this->getPrimoSignature($echangeDocBlob);
        }

        return $data;
    }

    protected function getPrimoSignature(EchangeDocBlob $echangeDocBlob): ?int
    {
        $primoSignature = $this->em
            ->getRepository(EchangeDocBlob::class)
            ->findOneBy([
                self::CATEGORIE_PIECE => EchangeDocBlob::FICHIER_JETON,
                self::ECHANGE_DOC => $echangeDocBlob->getEchangeDoc(),
                self::ECHANGE_DOC_PRINCIPAL => $echangeDocBlob,
            ]);

        if ($primoSignature instanceof EchangeDocBlob) {
            return $primoSignature->getId();
        }

        return null;
    }

    protected function getAnnexes(EchangeDocBlob $echangeDocBlob): array
    {
        $annexes = $this->em
            ->getRepository(EchangeDocBlob::class)
            ->findBy([
                self::CATEGORIE_PIECE => EchangeDocBlob::FICHIER_ANNEXE,
                self::ECHANGE_DOC => $echangeDocBlob->getEchangeDoc(),
                self::ECHANGE_DOC_PRINCIPAL => $echangeDocBlob,
            ]);

        $data = [];
        foreach ($annexes as $annexe) {
            $data[self::ID_ANNEXES][] = $annexe->getId();
        }

        return $data;
    }

    protected function isDocumentPrincipal(int $value): int
    {
        $documentPrincipal = 0;
        if (EchangeDocBlob::FICHIER_PRINCIPAL === $value) {
            $documentPrincipal = 1;
        }

        return $documentPrincipal;
    }

    /**
     * @return array
     */
    protected function getType(EchangeDocBlob $echangeDocBlob)
    {
        $data = null;

        $acte = $echangeDocBlob->getEchangeTypeActes();
        $standard = $echangeDocBlob->getEchangeTypeStandard();
        if (
            $acte instanceof EchangeDocTypePieceActes &&
            !$standard instanceof EchangeDocTypePieceStandard
        ) {
            $data = [
                'code' => $acte->getCode(),
                'libelle' => $acte->getLibelle(),
            ];
        } elseif (
            !$acte instanceof EchangeDocTypePieceActes &&
            $standard instanceof EchangeDocTypePieceStandard
        ) {
            $data = [
                'code' => $standard->getCode(),
                'libelle' => $standard->getLibelle(),
            ];
        }

        return $data;
    }
}
