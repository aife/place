<?php

namespace App\Serializer;

use App\Exception\ApiProblemInvalidArgumentException;
use phpseclib\File\X509;
use SimpleXMLElement;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CertificatsAcrgsNormalizer implements NormalizerInterface
{
    private const INDEX = 'Index';
    private const CERTIF_POLICIES = 'CertificatPolicies';
    private const POLICY_IDENT = 'PolicyIdentifier';
    private const TBS_CERTIF = 'tbsCertificate';
    private const ACFILLES = 'ACFilles';

    public function __construct(private readonly ObjectNormalizer $normalizer)
    {
    }

    private function findElement($box, $element)
    {
        foreach ($element as $element) {
            if ('fr' == $element->attributes('xml', true)->lang) {
                $box = (string) $element;
            }
        }
        if (empty($box)) {
            $box = (string) $element[0];
        }

        return $box;
    }

    private function infoX509($x509, $method, $id, $box)
    {
        $element = $x509->$method($id);
        if (array_key_exists(0, $element)) {
            $box = (string) $element[0];
        } else {
            $box = ''; // A verifier
        }

        return $box;
    }

    private function getCrlDistributionPoints($x509, $ACFille)
    {
        $CrlDistributionPoints = $x509->_getExtension('id-ce-cRLDistributionPoints');
        $ACFille = [];
        if (!empty($CrlDistributionPoints) && is_array($CrlDistributionPoints)) {
            foreach ($CrlDistributionPoints as $CrlDistributionPoint) {
                if (array_key_exists('uniformResourceIdentifier', $CrlDistributionPoint)) {
                    $ACFille[] = $CrlDistributionPoint['distributionPoint']['fullName'][0]['uniformResourceIdentifier'];
                }
            }
        } else {
            $ACFille[] = ''; //A verifier
        }

        return $ACFille;
    }

    private function getCertificatPolicies($x509, $ACFille, $CertificatPoliciesURIDefault)
    {
        $CertificatPolicies = $x509->_getExtension('id-ce-certificatePolicies');
        $ACFille[self::CERTIF_POLICIES] = [];
        if (!empty($CertificatPolicies) && is_array($CertificatPolicies)) {
            foreach ($CertificatPolicies as $CertificatPolicy) {
                if (!empty($CertificatPolicy['policyQualifiers'][0]['qualifier']['ia5String'])) {
                    $ACFille[self::CERTIF_POLICIES]['URI'] =
                        $CertificatPolicy['policyQualifiers'][0]['qualifier']['ia5String'];
                } else {
                    $ACFille[self::CERTIF_POLICIES]['URI'] = $CertificatPoliciesURIDefault;
                }
                $ACFille[self::CERTIF_POLICIES][self::POLICY_IDENT] =
                    $CertificatPolicy[self::POLICY_IDENT];
                if (array_key_exists($ACFille[self::CERTIF_POLICIES][self::POLICY_IDENT], $x509->oids)) {
                    $ACFille[self::CERTIF_POLICIES][self::POLICY_IDENT] .=
                        '('.$x509->oids[$ACFille[self::CERTIF_POLICIES][self::POLICY_IDENT]].')';
                } elseif ($value = array_search($ACFille[self::CERTIF_POLICIES][self::POLICY_IDENT], $x509->oids)) {
                    $ACFille[self::CERTIF_POLICIES][self::POLICY_IDENT] .= '('.$value.')';
                }
            }
        } else {
            $ACFille[self::CERTIF_POLICIES]['URI'] = $CertificatPoliciesURIDefault; //A verifier
            $ACFille[self::CERTIF_POLICIES][self::POLICY_IDENT] = '';
        }
    }

    private function fillACFille($trustServiceProvider, $cptACFille, $ACRacine)
    {
        foreach ($trustServiceProvider->TSPServices->TSPService as $service) {
            $ACFille = [];
            $ACFille['typeIdentifier'] = (string) $service->ServiceInformation->ServiceTypeIdentifier;
            $ACFille['ServiceName'] = $this->findElement(
                $ACFille['ServiceName'],
                $service->ServiceInformation->ServiceName->Name
            );
            $CertificatPoliciesURIDefault = null;
            $CertificatPoliciesURIDefault = $this->findElement(
                $CertificatPoliciesURIDefault,
                $service->ServiceInformation->TSPServiceDefinitionURI->URI
            );

            $X509Certificate = '-----BEGIN CERTIFICATE-----'.PHP_EOL.
                ((string) $service->ServiceInformation->ServiceDigitalIdentity->DigitalId->X509Certificate).
                PHP_EOL.'-----END CERTIFICATE-----'.PHP_EOL;
            $ACFille['Certificate'] = $X509Certificate;
            $x509 = new X509();
            $x509->loadX509($X509Certificate);
            $ACFille['SerialNumber'] = (string) $x509->currentCert[self::TBS_CERTIF]['serialNumber'];
            $ACFille['ValidityNotBefore'] = date(
                'd/m/Y',
                strtotime($x509->currentCert[self::TBS_CERTIF]['validity']['notBefore']['utcTime'])
            );
            $ACFille['ValidityNotAfter'] = date(
                'd/m/Y',
                strtotime($x509->currentCert[self::TBS_CERTIF]['validity']['notAfter']['utcTime'])
            );
            $ACFille['CN'] = $this->infoX509($x509, 'getDNProp', 'CN', $ACFille['CN']);
            $ACFille['CommonName'] =
                $this->infoX509($x509, 'getIssuerDNProp', 'id-at-commonName', $ACFille['CommonName']);
            $ACFille['CrlDistributionPoints'] =
                $this->getCrlDistributionPoints($x509, $ACFille['CrlDistributionPoints']);
            $ACFille[self::CERTIF_POLICIES] =
                $this->getCertificatPolicies($x509, $ACFille[self::CERTIF_POLICIES], $CertificatPoliciesURIDefault);

            $ACFille['IndexPere'] = $ACRacine[self::INDEX];
            $ACFille[self::INDEX] = $ACRacine[self::INDEX]."_ACFille_$cptACFille";
            $ACRacine[self::ACFILLES][$ACFille[self::INDEX]] = $ACFille;

            ++$cptACFille;
        }

        return $ACRacine[self::ACFILLES];
    }

    private function fillData($data, $cptACRac, $xml)
    {
        foreach ($xml->TrustServiceProviderList->TrustServiceProvider as $trustServiceProvider) {
            $ACRacine = [];
            $ACRacine['TSPName'] = $this->findElement(
                $ACRacine['TSPName'],
                $trustServiceProvider->TSPInformation->TSPName->Name
            );
            $ACRacine['TSPTradeName'] = $this->findElement(
                $ACRacine['TSPTradeName'],
                $trustServiceProvider->TSPInformation->TSPTradeName->Name
            );
            foreach ($trustServiceProvider->TSPInformation->TSPAddress->ElectronicAddress->URI as $uri) {
                if (preg_match('#http#', (string) $uri)) {
                    if ('fr' == $uri->attributes('xml', true)->lang ||
                        ('en' == $uri->attributes('xml', true)->lang && !array_key_exists('InformationURI', $ACRacine))
                    ) {
                        $ACRacine['InformationURI'] = (string) $uri;
                    }
                }
            }
            $ACRacine[self::INDEX] = "ACR_$cptACRac";
            $ACRacine[self::ACFILLES] = $this->fillACFille($trustServiceProvider, 0, $ACRacine);
            $data[$ACRacine[self::INDEX]] = $ACRacine;
            ++$cptACRac;
        }

        return $data;
    }

    public function normalize($xml, $format = null, array $context = [])
    {
        if (!$xml instanceof SimpleXMLElement) {
            throw new ApiProblemInvalidArgumentException("l'objet n'est pas une instance de SimpleXmlElement");
        }
        $data = [];
        $data = $this->fillData($data, 0, $xml);

        return $data;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof SimpleXMLElement;
    }
}
