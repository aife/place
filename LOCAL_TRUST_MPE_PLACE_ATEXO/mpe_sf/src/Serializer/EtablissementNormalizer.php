<?php

/**
 * Created by PhpStorm.
 * User: nouass
 * Date: 21/03/19
 * Time: 09:32.
 */

namespace App\Serializer;

use DateTime;
use App\Entity\Etablissement;
use App\Exception\ApiProblemInvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class EtablissementNormalizer extends AbstractApiPlatformDisabledNormalizer implements NormalizerInterface
{
    public function __construct(private readonly ObjectNormalizer $normalizer)
    {
    }

    public function normalize($etablissement, $format = null, array $context = [])
    {
        if (!$etablissement instanceof Etablissement) {
            throw new ApiProblemInvalidArgumentException("l'objet n'est pas une instance de Etablissement");
        }

        $data = $this->normalizer->normalize($etablissement, $format, $context);

        $data['id'] = $etablissement->getIdEtablissement();
        $data['siege'] = $etablissement->getEstSiege();
        $data['siret'] = $etablissement->getEntreprise()->getSiren() . $etablissement->getCodeEtablissement();

        if ($etablissement->getDateCreation() instanceof DateTime) {
            $data['dateCreation']
                = $etablissement->getDateCreation();
        }
        if ($etablissement->getDateModification() instanceof DateTime) {
            $data['dateModification']
                = $etablissement->getDateModification();
        }
        $adresse = [];
        $adresse['rue'] = $etablissement->getAdresse();
        $adresse['codePostal'] = $etablissement->getCodePostal();
        $adresse['ville'] = $etablissement->getVille();
        $adresse['pays'] = $etablissement->getPays();
        $data['adresse'] = $adresse;
        $data['idEntreprise'] = $etablissement->getIdEntreprise();

        return $data;
    }

    public function supportsNormalization($data, $format = null, array $context = []): bool
    {
        return parent::supportsNormalization($data, $format, $context) && $data instanceof Etablissement;
    }
}
