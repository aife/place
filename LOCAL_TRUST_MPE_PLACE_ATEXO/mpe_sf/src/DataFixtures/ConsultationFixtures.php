<?php

namespace App\DataFixtures;

use App\Entity\Consultation;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Entity\TypeContrat;
use App\Entity\TypeContratEtTypeProcedure;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ConsultationFixtures extends AbstractFixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public final const COUNT = 0;
    public final const FIXTURE_PREFIX = 'consultation-';

    private ?array $availableTypesContratEtProcedure = null;

    protected $data = [
        [
            'service_fixture' => ServiceFixtures::SERVICE_HPS_SR,
            'reference_utilisateur' => 'HPS_SR_REF000001',
            'intitule' => 'Construction d\'une école élémentaire',
            'objet' => 'Construction d\'une école élémentaire composée de 15 classes de 30 élèves.',
        ],
    ];

    public function getConsultation(array $data = [])
    {
        $faker = $this->getFaker();

        if (isset($data['service_fixture'])) {
            /** @var Service $service */
            $service = $this->getReference(ServiceFixtures::FIXTURE_PREFIX.$data['service_fixture']);
        } else {
            $service = $this->getReference(
                Service::class.'_'.$faker->numberBetween(1, ServiceFixtures::COUNT)
            );
        }

        /** @var Organisme $organisme */
        $organisme = $this->manager->getRepository(Organisme::class)->findOneBy(
            [
                'acronyme' => $service->getOrganisme()->getAcronyme(),
            ]
        );

        if (isset($data['reference_utilisateur'])) {
            $referenceUtilisateur = $data['reference_utilisateur'];
        } else {
            $referenceUtilisateur = $faker->unique()->bothify('???##?#?####');
        }

        $existingConsultation = $this->manager->getRepository(Consultation::class)->findOneBy(
            [
                'organisme' => $organisme->getAcronyme(),
                'referenceUtilisateur' => $referenceUtilisateur,
            ]
        );

        if ($existingConsultation) {
            /** @var Consultation $consultation */
            $consultation = $existingConsultation;
        } else {
            $consultation = new Consultation();
        }

//        $consultation->setService($service);
//        $consultation->setOrganisme($organisme);
        $consultation->setAcronymeOrg($organisme->getAcronyme());
        $consultation->setServiceId($service->getId());
        $consultation->setServiceAssocieId($service->getId());
        $consultation->setReferenceUtilisateur($referenceUtilisateur);

        /** @var TypeContratEtTypeProcedure $typeContratEtProcedure */
        $typeContratEtProcedure = $faker->randomElement($this->getAvailableTypesContratEtProcedure($organisme));

        /** @var ?TypeContrat $typeContrat */
        $typeContrat = $this->manager->getRepository(TypeContrat::class)
            ->find($typeContratEtProcedure->getIdTypeContrat());
        if ($typeContrat) {
            $consultation->setTypeMarche($typeContrat);
        }
        $consultation->setTypeProcedure($typeContratEtProcedure->getIdTypeProcedure());
        $consultation->setIdTypeProcedure($typeContratEtProcedure->getIdTypeProcedure());

        $consultation->setCategorie($faker->randomElement(['1', '2', '3']));

        $consultation->setReponseElectronique(1);
        $consultation->setNumProcedure(1);
        $consultation->setLieuExecution('254,260,302,224');
        $consultation->setTypeMiseEnLigne(1);
        $consultation->setEtatApprobation($faker->optional(0.1, 1)->randomElement([0, 1]));
        $consultation->setEtatEnAttenteValidation($faker->optional(0.1, 1)->randomElement([0, 1]));
        $consultation->setEtatValidation($faker->optional(0.1, 1)->randomElement([0, 1]));
        $consultation->setCodeCpv1(
            $faker->randomElement(['32342400', '24951000', '30122200', '30145100', '30192126', '30194320'])
        );
        $consultation->setIntitule($faker->sentence());
        $consultation->setObjet($faker->paragraph(1, false));

        $consultation->setDatedebut($faker->dateTimeBetween('-1 year', '+1 year'));
        $consultation->setDatefin($faker->dateTimeBetween('-1 year', '+1 year'));
        $consultation->setDateFinAffichage(
            ($faker->dateTimeBetween('-1 year', '+1 year'))->format(self::DATETIME_FORMAT)
        );
        $consultation->setDatevalidation(
            ($faker->dateTimeBetween('-1 year', '+1 year'))->format(self::DATETIME_FORMAT)
        );
        $consultation->setDatemiseenligne($faker->dateTimeBetween('-1 year', '+1 year'));
        $consultation->setDateMiseEnLigneCalcule($faker->dateTimeBetween('-1 year', '+1 year'));
        $consultation->setDatefinSad($faker->dateTimeBetween('-1 year', '+1 year'));
        $consultation->setDateFinLocale(
            ($faker->dateTimeBetween('-1 year', '+1 year'))->format(self::DATETIME_FORMAT)
        );

        $consultation = $this->setDataFromProperties($consultation, $data);

        return $consultation;
    }

    protected function loadData()
    {
        $this->createMany(
            Consultation::class,
            self::COUNT,
            fn($index) => $this->getConsultation()
        );
    }

    public function loadPreparedData(ObjectManager $manager)
    {
        return;
        foreach ($this->data as $consultationData) {
            /** @var Consultation $consultation */
            $consultation = $this->getConsultation($consultationData);

            $manager->persist($consultation);

            $reference = self::FIXTURE_PREFIX.$consultation->getReferenceUtilisateur();
            $this->addReference($reference, $consultation);
        }
    }

    public function getAvailableTypesContratEtProcedure(Organisme $organisme)
    {
        if (!$this->availableTypesContratEtProcedure) {
            $this->availableTypesContratEtProcedure = $this->manager->getRepository(
                TypeContratEtTypeProcedure::class
            )->findBy(
                [
                    'organisme' => $organisme->getAcronyme(),
                ]
            );
        }

        return $this->availableTypesContratEtProcedure;
    }

    public function getDependencies()
    {
        return [
            AgentFixtures::class,
        ];
    }

    public static function getGroups(): array
    {
        return ['all', 'consultation'];
    }
}
