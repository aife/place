<?php

namespace App\DataFixtures;

use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class HabilitationAgentFixtures extends AbstractFixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public function loadPreparedData(ObjectManager $manager)
    {
        $agentFixtures = new AgentFixtures($this->parameterBag);

        $agentData = $agentFixtures->getData();

        foreach ($agentData as $data) {
            $reference = AgentFixtures::FIXTURE_PREFIX . $this->getLogin($data['prenom'], $data['nom']);

            $this->manager->persist($this->getHabilitationAgent($reference));
        }
    }

    protected function loadData()
    {
        for ($i = 1; $i <= AgentFixtures::COUNT; ++$i) {
            $reference = Agent::class . '_' . $i;

            $this->manager->persist($this->getHabilitationAgent($reference));
        }
    }

    protected function getHabilitationAgent($reference)
    {
        /** @var Agent $agent */
        $agent = $this->getReference($reference);

        $existingHabilitations = $this->manager->getRepository(HabilitationAgent::class)->findOneBy(
            [
                'agent' => $agent->getId(),
            ]
        );

        if ($existingHabilitations) {
            $habilitations = $existingHabilitations;
        } else {
            $habilitations = new HabilitationAgent();
        }

        $habilitations->setAccesReponses('1');
        $habilitations->setCreerConsultation('1');
        $habilitations->setCreerConsultationTransverse('1');
        $habilitations->setCreerSuiteConsultation('1');
        $habilitations->setModifierConsultation('1');
        $habilitations->setModifierConsultationAvantValidation('1');
        $habilitations->setModifierConsultationMapaInferieurMontantApresValidation('1');
        $habilitations->setModifierConsultationMapaSuperieurMontantApresValidation('1');
        $habilitations->setModifierConsultationProceduresFormaliseesApresValidation('1');
        $habilitations->setPublierConsultation('1');
        $habilitations->setSuivreConsultation('1');
        $habilitations->setSuivreConsultationPole('1');
        $habilitations->setAdministrerOrganisme('1');
        $habilitations->setGererMapaInferieurMontant('1');
        $habilitations->setGererMapaSuperieurMontant('1');
        $habilitations->setAdministrerProceduresFormalisees('1');

        $habilitations->setAgent($agent);

        return $habilitations;
    }

    public function getDependencies()
    {
        return [
            AgentFixtures::class,
        ];
    }

    public static function getGroups(): array
    {
        return ['all', 'agent'];
    }
}
