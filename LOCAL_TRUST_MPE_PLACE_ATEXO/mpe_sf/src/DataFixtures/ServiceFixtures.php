<?php

namespace App\DataFixtures;

use DateTime;
use App\Entity\Organisme;
use App\Entity\Service;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ServiceFixtures extends AbstractFixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public final const COUNT = 50;
    public final const FIXTURE_PREFIX = 'service-';
    public final const SERVICE_MAQ_DF = OrganismeFixtures::ORG_MAQ.'-DF';
    public final const SERVICE_MAQ_DA = OrganismeFixtures::ORG_MAQ.'-DA';
    public final const SERVICE_MTI_DE = OrganismeFixtures::ORG_MTI.'-DE';
    public final const SERVICE_MTI_SMOD = OrganismeFixtures::ORG_MTI.'-SMOD';
    public final const SERVICE_HPS_SR = OrganismeFixtures::ORG_HPS.'-SR';
    public final const SERVICE_HRE_SP = OrganismeFixtures::ORG_HRE.'-SP';
    public final const SERVICE_LTR_SA = OrganismeFixtures::ORG_LTR.'-SA';
    public final const SERVICE_MLV_DO = OrganismeFixtures::ORG_MLV.'-DO';

    protected $data = [
        [
            'org' => OrganismeFixtures::ORG_MAQ,
            'libelle' => 'Direction financière',
            'sigle' => self::SERVICE_MAQ_DF,
            'adresse' => '59 rue Colbert',
            'cp' => '33000',
            'ville' => 'Bordeaux',
            'siren' => '110006012',
            'complement' => '00014',
        ],
        [
            'org' => OrganismeFixtures::ORG_MAQ,
            'libelle' => 'Direction des achats',
            'sigle' => self::SERVICE_MAQ_DA,
            'adresse' => '12 boulevard Vauban',
            'cp' => '59000',
            'ville' => 'Lille',
            'siren' => '444914568',
            'complement' => '00019',
        ],
        [
            'org' => OrganismeFixtures::ORG_MTI,
            'libelle' => 'Direction de l\'eau',
            'sigle' => self::SERVICE_MTI_DE,
            'adresse' => '11 rue de Groussay',
            'cp' => '78120',
            'ville' => 'Rambouillet',
            'siren' => '110006012',
            'complement' => '00014',
        ],
        [
            'org' => OrganismeFixtures::ORG_MTI,
            'libelle' => 'Service de la modernisation',
            'sigle' => self::SERVICE_MTI_SMOD,
            'adresse' => '1, Rue de Rivoli',
            'cp' => '75001',
            'ville' => 'Paris',
            'siren' => '130006786',
            'complement' => '00011',
        ],
        [
            'org' => OrganismeFixtures::ORG_HPS,
            'libelle' => 'Service de réanimation',
            'sigle' => self::SERVICE_HPS_SR,
            'adresse' => '47-83 Boulevard de l\'Hôpital',
            'cp' => '75013',
            'ville' => 'Paris',
            'siren' => '130014202',
            'complement' => '00019',
        ],
        [
            'org' => OrganismeFixtures::ORG_HRE,
            'libelle' => 'Service de pédiatrie',
            'sigle' => self::SERVICE_HRE_SP,
            'adresse' => '56 rue des Cordelières',
            'cp' => '59000',
            'ville' => 'Lille',
            'siren' => '152000428',
            'complement' => '00028',
        ],
        [
            'org' => OrganismeFixtures::ORG_LTR,
            'libelle' => 'Service des Achats',
            'sigle' => self::SERVICE_LTR_SA,
            'adresse' => '123 rue des bleuets',
            'cp' => '44000',
            'ville' => 'Nantes',
            'siren' => '183500040',
            'complement' => '00013',
        ],
        [
            'org' => OrganismeFixtures::ORG_MLV,
            'libelle' => 'Direction des opérations',
            'sigle' => self::SERVICE_MLV_DO,
            'adresse' => '84 rue du Louvre',
            'cp' => '75001',
            'ville' => 'Paris',
            'siren' => '130013683',
            'complement' => '00011',
        ],
    ];

    protected function loadData()
    {
        $this->createMany(
            Service::class,
            self::COUNT,
            function ($index) {
                $faker = $this->getFaker();
                $siret = str_replace(' ', '', $faker->siret);
                $siren = substr($siret, 0, 9);
                $complement = substr($siret, -5);

                /** @var Organisme $organisme */
                $organisme = $this->getReference(
                    Organisme::class.'_'.$faker->numberBetween(1, OrganismeFixtures::COUNT)
                );

                $sigle = strtoupper($faker->unique()->lexify('????'));

                $existingService = $this->manager->getRepository(Service::class)->findOneBy(
                    [
                        'organisme' => $organisme->getAcronyme(),
                        'sigle' => $sigle,
                    ]
                );

                if ($existingService) {
                    $service = $existingService;
                } else {
                    $service = new Service();
                    $service->setId($index + 1_000_000);
                }

                $service->setSigle($sigle);
                $service->setLibelle($faker->company);
                $service->setSiren($siren);
                $service->setComplement($complement);
                $service->setAdresse($faker->streetAddress);
                $service->setCp(str_replace(' ', '', $faker->postcode));
                $service->setVille($faker->city);
                $service->setPays('FRANCE');

                $service->setTypeService('2');
                $service->setDateCreation((new DateTime())->format(self::DATETIME_FORMAT));
                $service->setDateModification((new DateTime())->format(self::DATETIME_FORMAT));
                $service->setIdExterne(0);
                $service->setFormeJuridique('Établissement public');
                $service->setFormeJuridiqueCode('7389');

                $service->setOrganisme($organisme);

                return $service;
            }
        );
    }

    public function loadPreparedData(ObjectManager $manager)
    {
        foreach ($this->data as $serviceData) {
            $existingService = $manager->getRepository(Service::class)->findOneBy(
                [
                    'organisme' => $serviceData['org'],
                    'sigle' => $serviceData['sigle'],
                ]
            );

            if ($existingService) {
                $service = $existingService;
            } else {
                $service = new Service();
            }

            $service->setTypeService('2');
            $service->setDateCreation((new DateTime())->format(self::DATETIME_FORMAT));
            $service->setDateModification((new DateTime())->format(self::DATETIME_FORMAT));
            $service->setIdExterne(0);
            $service->setFormeJuridique('Établissement public');
            $service->setFormeJuridiqueCode('7389');

            $service = $this->setDataFromProperties($service, $serviceData);

            $service->setOrganisme($this->getReference(OrganismeFixtures::FIXTURE_PREFIX.$serviceData['org']));

            $manager->persist($service);

            $this->addReference(self::FIXTURE_PREFIX.$serviceData['sigle'], $service);
        }
    }

    public function getDependencies()
    {
        return [
            OrganismeFixtures::class,
        ];
    }

    public static function getGroups(): array
    {
        return ['all', 'organismes', 'services'];
    }
}
