<?php

namespace App\DataFixtures;

use DateTime;
use App\Entity\Agent;
use App\Entity\Organisme;
use App\Entity\Service;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class AgentFixtures extends AbstractFixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public final const COUNT = 50;
    public final const FIXTURE_PREFIX = 'agent-';

    protected $data = [
        [
            'org' => OrganismeFixtures::ORG_MAQ,
            'service' => ServiceFixtures::SERVICE_MAQ_DA,
            'prenom' => 'Michel',
            'nom' => 'Parcel',
        ],
        [
            'org' => OrganismeFixtures::ORG_MAQ,
            'service' => ServiceFixtures::SERVICE_MAQ_DF,
            'prenom' => 'Mireille',
            'nom' => 'Pichon',
        ],
        [
            'org' => OrganismeFixtures::ORG_MTI,
            'service' => ServiceFixtures::SERVICE_MTI_SMOD,
            'prenom' => 'Marcel',
            'nom' => 'Durand',
        ],
        [
            'org' => OrganismeFixtures::ORG_MTI,
            'service' => ServiceFixtures::SERVICE_MTI_DE,
            'prenom' => 'Danielle',
            'nom' => 'Sartin',
        ],
        [
            'org' => OrganismeFixtures::ORG_HPS,
            'service' => ServiceFixtures::SERVICE_HPS_SR,
            'prenom' => 'Amélie',
            'nom' => 'Tétinger',
        ],
        [
            'org' => OrganismeFixtures::ORG_HRE,
            'service' => ServiceFixtures::SERVICE_HRE_SP,
            'prenom' => 'Maurice',
            'nom' => 'Poulain',
        ],
        [
            'org' => OrganismeFixtures::ORG_LTR,
            'service' => ServiceFixtures::SERVICE_LTR_SA,
            'prenom' => 'Martin',
            'nom' => 'Pageot',
        ],
        [
            'org' => OrganismeFixtures::ORG_MLV,
            'service' => ServiceFixtures::SERVICE_MLV_DO,
            'prenom' => 'Romain',
            'nom' => 'Dugeon',
        ],
    ];

    protected function loadData()
    {
        $this->createMany(
            Agent::class,
            self::COUNT,
            function ($index) {
                $faker = $this->getFaker();
                $prenom = $faker->firstName;
                $nom = $faker->lastName;
                $login = $this->getLogin($prenom, $nom);
                $password = $this->getEncryptedPassword();

                /** @var Organisme $organisme */
                $organisme = $this->getReference(
                    Organisme::class.'_'.$faker->numberBetween(1, OrganismeFixtures::COUNT)
                );
                /** @var Service $service */
                $service = $this->getReference(Service::class.'_'.$faker->numberBetween(1, ServiceFixtures::COUNT));

                $existingAgent = $this->manager->getRepository(Agent::class)->findOneBy(
                    [
                        'organisme' => $organisme->getAcronyme(),
                        'login' => $login,
                    ]
                );

                if ($existingAgent) {
                    $agent = $existingAgent;
                } else {
                    $agent = new Agent();
                }

                $agent = $this->prepareAgent($agent, $login, $password);

                $agent->setOrganisme($organisme);
                $agent->setService($service);

                return $agent;
            }
        );
    }

    public function loadPreparedData(ObjectManager $manager)
    {
        foreach ($this->data as $agentData) {
            $login = $this->getLogin($agentData['prenom'], $agentData['nom']);
            $password = $this->getEncryptedPassword();

            $existingAgent = $manager->getRepository(Agent::class)->findOneBy(
                [
                    'organisme' => $agentData['org'],
                    'login' => $login,
                ]
            );

            if ($existingAgent) {
                /** @var Agent $agent */
                $agent = $existingAgent;
            } else {
                $agent = new Agent();
            }

            $agent = $this->prepareAgent($agent, $login, $password);

            $agent = $this->setDataFromProperties($agent, $agentData);

            $agent->setOrganisme($this->getReference(OrganismeFixtures::FIXTURE_PREFIX.$agentData['org']));
            $agent->setService($this->getReference(ServiceFixtures::FIXTURE_PREFIX.$agentData['service']));

            $manager->persist($agent);

            $reference = self::FIXTURE_PREFIX.$this->getLogin($agentData['prenom'], $agentData['nom']);
            $this->addReference($reference, $agent);
        }
    }

    public function prepareAgent($agent, $login, $password)
    {
        $agent->setLogin($login);
        $agent->setEmail($login.'.agent@atexo.com');
        $agent->setMdp($password);
        $agent->setPassword($password);
        $agent->setTypeHash('SHA256');
        $agent->setDateCreation((new DateTime())->format(self::DATETIME_FORMAT));
        $agent->setDateModification((new DateTime())->format(self::DATETIME_FORMAT));
        $agent->setActif(1);
        $agent->setAlerteValidationConsultation('1');
        $agent->setAlerteQuestionEntreprise('1');
        $agent->setAlerteReceptionMessage('1');
        $agent->setAlerteReponseElectronique('1');
        $agent->setAlerteClotureConsultation('1');

        return $agent;
    }

    public function getDependencies()
    {
        return [
            OrganismeFixtures::class,
            ServiceFixtures::class,
        ];
    }

    public static function getGroups(): array
    {
        return ['all', 'agent'];
    }
}
