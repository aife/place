<?php

namespace App\DataFixtures;

use App\Entity\Configuration\PlateformeVirtuelle;
use App\Entity\Configuration\PlateformeVirtuelleOrganisme;
use App\Entity\Organisme;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PlateformeVirtuelleFixtures extends AbstractFixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public final const FIXTURE_PREFIX = 'plateformeVirtuelle-';

    public final const DATA = [
        [
            'code' => 'achats-hopitaux',
            'name' => 'Achats Hôpitaux',
            'associated_orgs' => [
                OrganismeFixtures::ORG_HPS,
                OrganismeFixtures::ORG_HRE,
            ],
        ],
        [
            'code' => 'ma-region',
            'name' => 'Ma Région',
            'associated_orgs' => [
                OrganismeFixtures::ORG_HRE,
                OrganismeFixtures::ORG_LTR,
            ],
        ],
    ];

    protected function loadData()
    {
    }

    public function load(ObjectManager $manager)
    {
        foreach (self::DATA as $pfvData) {
            $domain = $pfvData['domain'] ?? $pfvData['code'].'.local-trust.com';

            $existingPfv = $manager->getRepository(PlateformeVirtuelle::class)->findOneBy(
                [
                    'domain' => $domain,
                ]
            );

            if ($existingPfv) {
                /** @var PlateformeVirtuelle $pfv */
                $pfv = $existingPfv;
            } else {
                $pfv = new PlateformeVirtuelle();
            }

            $pfv->setDomain($domain);
            $pfv->setName($pfvData['name']);
            $pfv->setCodeDesign($pfvData['code']);
            $pfv->setProtocole('https');
            $pfv->setNoReply('no-reply@'.$domain);
            $pfv->setFromPfName($pfvData['name']);

            $manager->persist($pfv);

            $this->addReference(self::FIXTURE_PREFIX.$domain, $pfv);

            ////            $increment = 1;
//
//            foreach ($pfvData['associated_orgs'] as $associatedOrg) {
////                $plateforme = $this->getReference(self::FIXTURE_PREFIX . $domain);
//                $plateforme = $pfv;
//                $organisme = $this->getReference(OrganismeFixtures::FIXTURE_PREFIX . $associatedOrg);
//
//                $existingPfvOrg = $manager->getRepository(PlateformeVirtuelleOrganisme::class)->findOneBy(
//                    [
//                        'plateforme' => $plateforme,
//                        'organisme' => $organisme,
//                    ]
//                );
//
//                if ($existingPfvOrg) {
//                    /** @var PlateformeVirtuelleOrganisme $pfvOrg */
//                    $pfvOrg = $existingPfvOrg;
//                } else {
//                    $pfvOrg = new PlateformeVirtuelleOrganisme();
//                }
//
////                $pfvOrg->setId(100000 + $increment);
//                $pfvOrg->setPlateforme($plateforme);
//                $pfvOrg->setOrganisme($organisme);
//
//                $manager->persist($pfvOrg);
////                $increment++;
//            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            OrganismeFixtures::class,
        ];
    }

    public static function getGroups(): array
    {
        return ['all', 'organismes', 'services'];
    }
}
