<?php

namespace App\DataFixtures;

use App\Entity\Organisme;
use App\Entity\TypeContratEtTypeProcedure;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectManager;

class TypeContratTypeProcedureFixtures extends AbstractFixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public function loadPreparedData(ObjectManager $manager)
    {
        $organismeFixtures = new OrganismeFixtures($this->parameterBag);
        $organismeData = $organismeFixtures->getData();

        $typeContratTypeProcedures = $this->getTypeContratTypeProcedureReference();

        foreach ($organismeData as $data) {
            $reference = OrganismeFixtures::FIXTURE_PREFIX.$data['acronyme'];

            foreach ($typeContratTypeProcedures as $typeContratTypeProcedure) {
                $this->manager->persist($this->getTypeContratTypeProcedure($reference, $typeContratTypeProcedure));
            }
        }
    }

    protected function loadData()
    {
        $typeContratTypeProcedures = $this->getTypeContratTypeProcedureReference();

        for ($i = 1; $i <= OrganismeFixtures::COUNT; ++$i) {
            $reference = Organisme::class.'_'.$i;

            foreach ($typeContratTypeProcedures as $typeContratTypeProcedure) {
                $this->manager->persist($this->getTypeContratTypeProcedure($reference, $typeContratTypeProcedure));
            }
        }
    }

    public function getTypeContratTypeProcedureReference()
    {
        /** @var Organisme $refOrganisme */
        $refOrganisme = $this->manager->getRepository(Organisme::class)->findOneBy(
            [
                // On prend les mêmes données qu'un organisme de référence.
                'acronyme' => OrganismeFixtures::REFERENCE_ORGANISME_ACRONYME,
            ]
        );

        /** @var EntityRepository $repo */
        $repo = $this->manager->getRepository(TypeContratEtTypeProcedure::class);

        return $repo->findBy(
            [
                'organisme' => $refOrganisme->getAcronyme(),
            ]
        );
    }

    protected function getTypeContratTypeProcedure(
        $reference,
        TypeContratEtTypeProcedure $typeContratEtTypeProcedureRef
    ) {
        /** @var Organisme $organisme */
        $organisme = $this->getReference($reference);

        $existingTypeContratEtTypeProcedure = $this->manager->getRepository(
            TypeContratEtTypeProcedure::class
        )->findOneBy(
            [
                'idTypeContrat' => $typeContratEtTypeProcedureRef->getIdTypeContrat(),
                'idTypeProcedure' => $typeContratEtTypeProcedureRef->getIdTypeProcedure(),
                'organisme' => $organisme->getAcronyme(),
            ]
        );

        if ($existingTypeContratEtTypeProcedure) {
            /** @var TypeContratEtTypeProcedure $typeContratEtTypeProcedure */
            $typeContratEtTypeProcedure = $existingTypeContratEtTypeProcedure;
        } else {
            $typeContratEtTypeProcedure = clone $typeContratEtTypeProcedureRef;
            $typeContratEtTypeProcedure->setIdTypeProcedure($typeContratEtTypeProcedure);
            $typeContratEtTypeProcedure->setOrganisme($organisme->getAcronyme());
        }

        return $typeContratEtTypeProcedure;
    }

    public function getDependencies()
    {
        return [
            TypeProcedureOrganismeFixtures::class,
        ];
    }

    public static function getGroups(): array
    {
        return ['all', 'organismes'];
    }
}
