<?php

namespace App\DataFixtures;

use App\Entity\Organisme;
use App\Entity\Procedure\TypeProcedureOrganisme;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectManager;

class TypeProcedureOrganismeFixtures extends AbstractFixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public function loadPreparedData(ObjectManager $manager)
    {
        $organismeFixtures = new OrganismeFixtures($this->parameterBag);
        $organismeData = $organismeFixtures->getData();

        $typesProcedures = $this->getTypesProceduresReference();

        foreach ($organismeData as $data) {
            $reference = OrganismeFixtures::FIXTURE_PREFIX.$data['acronyme'];

            foreach ($typesProcedures as $typeProcedure) {
                $this->manager->persist($this->getTypeProcedureOrganisme($reference, $typeProcedure));
            }
        }
    }

    protected function loadData()
    {
        $typesProcedures = $this->getTypesProceduresReference();

        for ($i = 1; $i <= OrganismeFixtures::COUNT; ++$i) {
            $reference = Organisme::class.'_'.$i;

            foreach ($typesProcedures as $typeProcedure) {
                $this->manager->persist($this->getTypeProcedureOrganisme($reference, $typeProcedure));
            }
        }
    }

    public function getTypesProceduresReference()
    {
        /** @var Organisme $refOrganisme */
        $refOrganisme = $this->manager->getRepository(Organisme::class)->findOneBy(
            [
                // On prend les mêmes données qu'un organisme de référence.
                'acronyme' => OrganismeFixtures::REFERENCE_ORGANISME_ACRONYME,
            ]
        );

        /** @var EntityRepository $repo */
        $repo = $this->manager->getRepository(TypeProcedureOrganisme::class);

        return $repo->findBy(
            [
                'organisme' => $refOrganisme->getAcronyme(),
            ]
        );
    }

    protected function getTypeProcedureOrganisme($reference, TypeProcedureOrganisme $typeProcedureReference)
    {
        /** @var Organisme $organisme */
        $organisme = $this->getReference($reference);

        $existingTypeProcedureOrganisme = $this->manager->getRepository(TypeProcedureOrganisme::class)->findOneBy(
            [
                'idTypeProcedure' => $typeProcedureReference->getIdTypeProcedure(),
                'organisme' => $organisme->getAcronyme(),
            ]
        );

        if ($existingTypeProcedureOrganisme) {
            /** @var TypeProcedureOrganisme $typeProcedureOrganisme */
            $typeProcedureOrganisme = $existingTypeProcedureOrganisme;
        } else {
            $typeProcedureOrganisme = clone $typeProcedureReference;
            $typeProcedureOrganisme->setIdTypeProcedure($typeProcedureReference->getIdTypeProcedure());
            $typeProcedureOrganisme->setOrganisme($organisme->getAcronyme());
        }

        return $typeProcedureOrganisme;
    }

    public function getDependencies()
    {
        return [
            OrganismeFixtures::class,
        ];
    }

    public static function getGroups(): array
    {
        return ['all', 'organismes'];
    }
}
