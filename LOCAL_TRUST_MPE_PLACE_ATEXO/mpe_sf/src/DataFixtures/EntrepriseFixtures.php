<?php

namespace App\DataFixtures;

use DateTime;
use App\Entity\Entreprise;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class EntrepriseFixtures extends AbstractFixture implements FixtureGroupInterface
{
    public final const COUNT = 20;
    public final const FIXTURE_PREFIX = 'entreprise-';

    protected $data = [
        [
            'siren' => '392500138',
            'nom' => 'Pros du bâtiment BTP',
            'adresse' => '32 Grand Rue',
            'codepostal' => '27200',
            'villeadresse' => 'Vernon',
            'paysadresse' => 'FRANCE',
        ],
        [
            'siren' => '889029245',
            'nom' => 'Société de BTP',
            'adresse' => '90 rue Emile Zola',
            'codepostal' => '33000',
            'villeadresse' => 'Bordeaux',
            'paysadresse' => 'FRANCE',
        ],
        [
            'siren' => '414850636',
            'nom' => 'Les maçons de la Cannebière',
            'adresse' => '44 rue de la Cannebière',
            'codepostal' => '13007',
            'villeadresse' => 'Marseille',
            'paysadresse' => 'FRANCE',
        ],
    ];

    protected function loadData()
    {
        $this->createMany(
            Entreprise::class,
            self::COUNT,
            function ($index) {
                $faker = $this->getFaker();
                $siret = str_replace(' ', '', $faker->siret);
                $siren = substr($siret, 0, 9);

                $existingEntreprise = $this->manager->getRepository(Entreprise::class)->findOneBy(
                    [
                        'siren' => $siren,
                    ]
                );

                if ($existingEntreprise) {
                    /** @var Entreprise $entreprise */
                    $entreprise = $existingEntreprise;
                } else {
                    $entreprise = new Entreprise();
                }

                $entreprise->setSiren($siren);
                $entreprise->setNom($faker->company);
                $entreprise->setAdresse($faker->streetAddress);
                $entreprise->setCodepostal(str_replace(' ', '', $faker->postcode));
                $entreprise->setVilleadresse($faker->city);
                $entreprise->setPaysadresse('France');

                $entreprise->setDateCreation((new DateTime())->format(self::DATETIME_FORMAT));
                $entreprise->setDateModification((new DateTime())->format(self::DATETIME_FORMAT));
                $entreprise->setIdExterne(0);
                $entreprise->setFormejuridique('SARL');
                $entreprise->setPaysenregistrement('FR');
                $entreprise->setCapitalSocial('N.C.');

                return $entreprise;
            }
        );
    }

    public function loadPreparedData(ObjectManager $manager)
    {
        foreach ($this->data as $entrepriseData) {
            $existingEntreprise = $manager->getRepository(Entreprise::class)->findOneBy(
                [
                    'siren' => $entrepriseData['siren'],
                ]
            );

            if ($existingEntreprise) {
                /** @var Entreprise $entreprise */
                $entreprise = $existingEntreprise;
            } else {
                $entreprise = new Entreprise();
            }

            $entreprise->setDateCreation((new DateTime())->format(self::DATETIME_FORMAT));
            $entreprise->setDateModification((new DateTime())->format(self::DATETIME_FORMAT));
            $entreprise->setIdExterne(0);
            $entreprise->setFormejuridique('SARL');
            $entreprise->setPaysenregistrement('FR');
            $entreprise->setCapitalSocial('N.C.');

            $entreprise = $this->setDataFromProperties($entreprise, $entrepriseData);

            $manager->persist($entreprise);

            $this->addReference(self::FIXTURE_PREFIX.$entrepriseData['siren'], $entreprise);
        }
    }

    public static function getGroups(): array
    {
        return ['all', 'entreprises'];
    }
}
