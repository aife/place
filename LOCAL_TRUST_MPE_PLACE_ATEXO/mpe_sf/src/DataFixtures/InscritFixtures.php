<?php

namespace App\DataFixtures;

use DateTime;
use Exception;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\Inscrit;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class InscritFixtures extends AbstractFixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public final const COUNT = 100;
    public final const FIXTURE_PREFIX = 'inscrit-';

    protected $data = [
        [
            'siren' => '392500138',
            'codeEtablissement' => '00012',
            'prenom' => 'Marc',
            'nom' => 'Passet',
            'adresse' => '32 Grand Rue',
            'codepostal' => '27200',
            'villeadresse' => 'Vernon',
            'paysadresse' => 'FRANCE',
        ],
        [
            'siren' => '889029245',
            'codeEtablissement' => '00012',
            'prenom' => 'André',
            'nom' => 'Duchemin',
            'adresse' => '90 rue Emile Zola',
            'codepostal' => '33000',
            'villeadresse' => 'Bordeaux',
            'paysadresse' => 'FRANCE',
        ],
        [
            'siren' => '414850636',
            'codeEtablissement' => '00021',
            'prenom' => 'Martine',
            'nom' => 'Pertuis',
            'adresse' => '44 rue de la Cannebière',
            'codepostal' => '13007',
            'villeadresse' => 'Marseille',
            'paysadresse' => 'FRANCE',
        ],
    ];

    protected function loadData()
    {
        $this->createMany(
            Inscrit::class,
            self::COUNT,
            function ($index) {
                $faker = $this->getFaker();
                $prenom = $faker->firstName;
                $nom = $faker->lastName;
                $login = $this->getLogin($prenom, $nom);
                $password = $this->getEncryptedPassword();
                $email = $login.'.entreprise@atexo.com';

                /** @var Entreprise $entreprise */
                $entreprise = $this->getReference(
                    Entreprise::class.'_'.$faker->numberBetween(1, EntrepriseFixtures::COUNT)
                );
                /** @var Etablissement $etablissement */
                $etablissement = $this->getReference(
                    Etablissement::class.'_'.$faker->numberBetween(1, EtablissementFixtures::COUNT)
                );

                $existingInscrit = $this->manager->getRepository(Inscrit::class)->findOneBy(
                    [
                        'email' => $email,
                    ]
                );

                if ($existingInscrit) {
                    /** @var Inscrit $inscrit */
                    $inscrit = $existingInscrit;
                } else {
                    $inscrit = new Inscrit();
                }

                $inscrit->setLogin($login);
                $inscrit->setMdp($password);
                $inscrit->setTypeHash('SHA256');
                $inscrit->setEmail($email);
                $inscrit->setDateCreation((new DateTime())->format(self::DATETIME_FORMAT));
                $inscrit->setDateModification((new DateTime())->format(self::DATETIME_FORMAT));
                $inscrit->setIdExterne(0);
                $inscrit->setInscritAnnuaireDefense('0');
                $inscrit->setSiret($etablissement->getCodeEtablissement());

                $inscrit->setEntreprise($entreprise);
                $inscrit->setEtablissement($etablissement);

                //                $inscrit->setEntrepriseId($entreprise->getId());
                //                $inscrit->setIdEtablissement($etablissement->getIdEtablissement());

                return $inscrit;
            }
        );
    }

    public function loadPreparedData(ObjectManager $manager)
    {
        foreach ($this->data as $inscritData) {
            $login = $this->getLogin($inscritData['prenom'], $inscritData['nom']);
            $password = $this->getEncryptedPassword();
            $email = $login.'.entreprise@atexo.com';

            /** @var Entreprise $entreprise */
            $entreprise = $manager->getRepository(Entreprise::class)->findOneBy(
                [
                    'siren' => $inscritData['siren'],
                ]
            );

            if (!$entreprise) {
                throw new Exception('Aucune entreprise avec le SIREN '.$inscritData['siren']);
            }

            /** @var Etablissement $etablissement */
            $etablissement = $manager->getRepository(Etablissement::class)->findOneBy(
                [
                    'entreprise' => $entreprise,
                    'codeEtablissement' => $inscritData['codeEtablissement'],
                ]
            );

            if (!$etablissement) {
                throw new Exception('Aucun établissement avec le code_etablissement '.$inscritData['codeEtablissement']);
            }

            $existingInscrit = $manager->getRepository(Inscrit::class)->findOneBy(
                [
                    'email' => $email,
                ]
            );

            if ($existingInscrit) {
                /** @var Inscrit $inscrit */
                $inscrit = $existingInscrit;
            } else {
                $inscrit = new Inscrit();
            }

            $inscrit->setLogin($login);
            $inscrit->setMdp($password);
            $inscrit->setTypeHash('SHA256');
            $inscrit->setEmail($email);
            $inscrit->setDateCreation((new DateTime())->format(self::DATETIME_FORMAT));
            $inscrit->setDateModification((new DateTime())->format(self::DATETIME_FORMAT));
            $inscrit->setIdExterne(0);
            $inscrit->setInscritAnnuaireDefense('0');
            $inscrit->setSiret($inscritData['codeEtablissement']);

            $inscrit = $this->setDataFromProperties($inscrit, $inscritData);

            $inscrit->setEntreprise(
                $this->getReference(EntrepriseFixtures::FIXTURE_PREFIX.$entreprise->getSiren())
            );
            $inscrit->setEtablissement(
                $this->getReference(
                    EtablissementFixtures::FIXTURE_PREFIX.$entreprise->getSiren()
                    .$etablissement->getCodeEtablissement()
                )
            );

            $manager->persist($inscrit);

            $this->addReference(
                self::FIXTURE_PREFIX.$email,
                $inscrit
            );
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            EntrepriseFixtures::class,
        ];
    }

    public static function getGroups(): array
    {
        return ['all', 'entreprises'];
    }
}
