<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataFixtures;

use Faker\Generator;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

abstract class AbstractFixture extends Fixture
{
    /**
     * Nombre d'entités à générer (valeur par défaut).
     */
    public const COUNT = 20;

    final public const DATETIME_FORMAT = 'Y-m-d H:i:s';

    private ?Generator $faker = null;

    protected ?ObjectManager $manager = null;

    protected $data = [];

    public function __construct(protected ParameterBagInterface $parameterBag)
    {
    }

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->loadPreparedData($manager);
        $this->loadData();
        $this->manager->flush();
    }

    protected function createMany(string $className, int $count, callable $factory)
    {
        for ($i = 1; $i <= $count; ++$i) {
            $entity = $factory($i);
            $this->manager->persist($entity);
            $this->addReference($className.'_'.$i, $entity);

            if (0 == $i % 500) {
                $this->manager->flush();
            }
        }
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getFaker()
    {
        if (!$this->faker) {
            $this->faker = Factory::create('fr_FR');
            $this->faker->seed(2021); // Permet de générer toujours le même jeu de données
        }

        return $this->faker;
    }

    public function getLogin($prenom, $nom)
    {
        return str_replace(
            ' ',
            '',
            strtolower(iconv('UTF-8', 'ASCII//TRANSLIT', $prenom.'.'.$nom))
        );
    }

    public function getEncryptedPassword($password = 'convergence')
    {
        return hash('sha256', $this->parameterBag->get('SALT_POUR_MOT_DE_PASSE').$password);
    }

    public function getSetterNameFromProperty($propertyName)
    {
        $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $propertyName)));

        return 'set'.$str;
    }

    public function setDataFromProperties($entity, $dataArray)
    {
        foreach (array_keys($dataArray) as $property) {
            $setter = $this->getSetterNameFromProperty($property);

            if (method_exists($entity, $setter)) {
                $entity->$setter($dataArray[$property]);
            }
        }

        return $entity;
    }
}
