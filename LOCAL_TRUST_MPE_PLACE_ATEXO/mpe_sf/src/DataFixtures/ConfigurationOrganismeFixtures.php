<?php

namespace App\DataFixtures;

use App\Entity\ConfigurationOrganisme;
use App\Entity\Organisme;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectManager;

class ConfigurationOrganismeFixtures extends AbstractFixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public function loadPreparedData(ObjectManager $manager)
    {
        $organismeFixtures = new OrganismeFixtures($this->parameterBag);
        $organismeData = $organismeFixtures->getData();

        $configurationOrganismeReference = $this->getConfigurationOrganismeReference();

        foreach ($organismeData as $data) {
            $reference = OrganismeFixtures::FIXTURE_PREFIX.$data['acronyme'];

            $this->manager->persist($this->getConfigurationOrganisme($reference, $configurationOrganismeReference));
        }
    }

    protected function loadData()
    {
        $configurationOrganismeReference = $this->getConfigurationOrganismeReference();

        for ($i = 1; $i <= OrganismeFixtures::COUNT; ++$i) {
            $reference = Organisme::class.'_'.$i;

            $this->manager->persist($this->getConfigurationOrganisme($reference, $configurationOrganismeReference));
        }
    }

    public function getConfigurationOrganismeReference()
    {
        /** @var Organisme $refOrganisme */
        $refOrganisme = $this->manager->getRepository(Organisme::class)->findOneBy(
            [
                // On prend les mêmes données qu'un organisme de référence.
                'acronyme' => OrganismeFixtures::REFERENCE_ORGANISME_ACRONYME,
            ]
        );

        /** @var EntityRepository $repo */
        $repo = $this->manager->getRepository(ConfigurationOrganisme::class);

        $configurationOrganismeReference = $repo->find(
            [
                'organisme' => $refOrganisme->getAcronyme(),
            ]
        );

        return $configurationOrganismeReference ?: new ConfigurationOrganisme();
    }

    protected function getConfigurationOrganisme(
        $reference,
        ConfigurationOrganisme $configurationOrganismeReference
    ) {
        /** @var Organisme $organisme */
        $organisme = $this->getReference($reference);

        $existingConfigurationOrganisme = $this->manager->getRepository(
            ConfigurationOrganisme::class
        )->find(
            [
                'organisme' => $organisme->getAcronyme(),
            ]
        );

        if ($existingConfigurationOrganisme) {
            /** @var ConfigurationOrganisme $configurationOrganisme */
            $configurationOrganisme = $existingConfigurationOrganisme;
        } else {
            $configurationOrganisme = clone $configurationOrganismeReference;
            $configurationOrganisme->setOrganisme($organisme->getAcronyme());
        }

        return $configurationOrganisme;
    }

    public function getDependencies()
    {
        return [
            OrganismeFixtures::class,
        ];
    }

    public static function getGroups(): array
    {
        return ['all', 'organismes'];
    }
}
