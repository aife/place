<?php

namespace App\DataFixtures;

use App\Entity\Organisme;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class OrganismeFixtures extends AbstractFixture implements FixtureGroupInterface
{
    public final const COUNT = 20;
    public final const REFERENCE_ORGANISME_ACRONYME = 'e3r';

    public final const FIXTURE_PREFIX = 'organisme-';
    public final const ORG_MAQ = 'qa-maq';
    public final const ORG_MTI = 'qa-mti';
    public final const ORG_HPS = 'qa-hps';
    public final const ORG_HRE = 'qa-hre';
    public final const ORG_LTR = 'qa-ltr';
    public final const ORG_MLV = 'qa-mlv';

    protected $data = [
        [
            'acronyme' => self::REFERENCE_ORGANISME_ACRONYME,
            'sigle' => 'DEMO',
            'denomination_org' => 'Organisme de démonstration ATEXO',
            'adresse' => '3, rue Michel-Ange',
            'cp' => '75794',
            'ville' => 'Paris',
        ],
        [
            'acronyme' => self::ORG_MAQ,
            'sigle' => 'MAQ',
            'denomination_org' => 'Ministère de l\'Assurance Qualité',
            'adresse' => '59 rue Colbert',
            'cp' => '33000',
            'ville' => 'Bordeaux',
        ],
        [
            'acronyme' => self::ORG_MTI,
            'sigle' => 'MTI',
            'denomination_org' => 'Ministère des tests informatiques',
            'adresse' => '62 boulevard Voltaire',
            'cp' => '69003',
            'ville' => 'Lyon',
        ],
        [
            'acronyme' => self::ORG_HPS,
            'sigle' => 'HPS',
            'denomination_org' => 'Hôpital de la Pitié-Salpêtrière',
            'adresse' => '47-83 Boulevard de l\'Hôpital',
            'cp' => '75013',
            'ville' => 'Paris',
        ],
        [
            'acronyme' => self::ORG_HRE,
            'sigle' => 'HRE',
            'denomination_org' => 'Hôpital régional',
            'adresse' => '56 rue des Cordelières',
            'cp' => '59000',
            'ville' => 'Lille',
        ],
        [
            'acronyme' => self::ORG_LTR,
            'sigle' => 'LTR',
            'denomination_org' => 'Lycée technique régional public du BTP',
            'adresse' => '123 rue des bleuets',
            'cp' => '44000',
            'ville' => 'Nantes',
        ],
        [
            'acronyme' => self::ORG_MLV,
            'sigle' => 'MLV',
            'denomination_org' => 'Musée du Louvre',
            'adresse' => '84 rue du Louvre',
            'cp' => '75001',
            'ville' => 'Paris',
        ],
    ];

    protected function loadData()
    {
        $this->createMany(
            Organisme::class,
            self::COUNT,
            function ($index) {
                $faker = $this->getFaker();

                $acronym = $faker->bothify('??##??');
                $siren = str_replace(' ', '', $faker->siren);

                $organisme = $this->manager->getRepository(Organisme::class)->findOneBy(
                    ['acronyme' => $acronym]
                );

                if (!$organisme) {
                    $organisme = new Organisme();
                }

                $organisme->setAcronyme($acronym);
                $organisme->setSiren($siren);
                $organisme->setDenominationOrg($faker->company);
                $organisme->setAdresse($faker->streetAddress);
                $organisme->setCp(str_replace(' ', '', $faker->postcode));
                $organisme->setVille($faker->city);
                $organisme->setPays('France');
                $organisme->setActive(true);
                $organisme->setCategorieInsee('7.1.13');

                return $organisme;
            }
        );
    }

    public function loadPreparedData(ObjectManager $manager)
    {
        foreach ($this->data as $orgData) {
            $organisme = $manager->getRepository(Organisme::class)->findOneBy(
                ['acronyme' => $orgData['acronyme']]
            );

            if (!$organisme) {
                $organisme = new Organisme();
            }

            $organisme = $this->setDataFromProperties($organisme, $orgData);

            $organisme->setActive(true);
            $organisme->setCategorieInsee('7.1.13');

            $manager->persist($organisme);

            $this->addReference(self::FIXTURE_PREFIX.$orgData['acronyme'], $organisme);
        }
    }

    public static function getGroups(): array
    {
        return ['all', 'organismes'];
    }
}
