<?php

namespace App\DataFixtures;

use DateTime;
use Exception;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class EtablissementFixtures extends AbstractFixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public final const COUNT = 30;
    public final const FIXTURE_PREFIX = 'etablissement-';

    protected $data = [
        [
            'siren' => '392500138',
            'codeEtablissement' => '00012',
            'adresse' => '32 Grand Rue',
            'codepostal' => '27200',
            'ville' => 'Vernon',
            'pays' => 'FRANCE',
        ],
        [
            'siren' => '889029245',
            'codeEtablissement' => '00012',
            'adresse' => '90 rue Emile Zola',
            'codepostal' => '33000',
            'ville' => 'Bordeaux',
            'pays' => 'FRANCE',
        ],
        [
            'siren' => '414850636',
            'codeEtablissement' => '00021',
            'adresse' => '44 rue de la Cannebière',
            'codepostal' => '13007',
            'ville' => 'Marseille',
            'pays' => 'FRANCE',
        ],
    ];

    protected function loadData()
    {
        $this->createMany(
            Etablissement::class,
            self::COUNT,
            function ($index) {
                $faker = $this->getFaker();
                $siret = str_replace(' ', '', $faker->siret);
                $complement = substr($siret, -5);

                /** @var Entreprise $entreprise */
                $entreprise = $this->getReference(
                    Entreprise::class.'_'.$faker->numberBetween(1, EntrepriseFixtures::COUNT)
                );

                $existingEtablissement = $this->manager->getRepository(Etablissement::class)->findOneBy(
                    [
                        'entreprise' => $entreprise,
                        'codeEtablissement' => $complement,
                    ]
                );

                if ($existingEtablissement) {
                    /** @var Etablissement $etablissement */
                    $etablissement = $existingEtablissement;
                } else {
                    $etablissement = new Etablissement();
                }

                $etablissement->setCodeEtablissement($complement);
                $etablissement->setAdresse($faker->streetAddress);
                $etablissement->setCodepostal(str_replace(' ', '', $faker->postcode));
                $etablissement->setVille($faker->city);
                $etablissement->setPays('France');

                $etablissement->setEstSiege('1');
                $etablissement->setDateCreation(new DateTime());
                $etablissement->setDateModification(new DateTime());
                $etablissement->setIdExterne(0);

                $etablissement->setEntreprise($entreprise);

                return $etablissement;
            }
        );
    }

    public function loadPreparedData(ObjectManager $manager)
    {
        foreach ($this->data as $etablissementData) {
            /** @var Entreprise $entreprise */
            $entreprise = $manager->getRepository(Entreprise::class)->findOneBy(
                [
                    'siren' => $etablissementData['siren'],
                ]
            );

            if (!$entreprise) {
                throw new Exception('Aucune entreprise avec le SIREN '.$etablissementData['siren']);
            }

            $existingEtablissement = $manager->getRepository(Etablissement::class)->findOneBy(
                [
                    'entreprise' => $entreprise,
                    'codeEtablissement' => $etablissementData['codeEtablissement'],
                ]
            );

            if ($existingEtablissement) {
                /** @var Etablissement $etablissement */
                $etablissement = $existingEtablissement;
            } else {
                $etablissement = new Etablissement();
            }

            $etablissement->setEstSiege('1');
            $etablissement->setDateCreation(new DateTime());
            $etablissement->setDateModification(new DateTime());
            $etablissement->setIdExterne(0);

            $etablissement = $this->setDataFromProperties($etablissement, $etablissementData);

            $etablissement->setEntreprise(
                $this->getReference(EntrepriseFixtures::FIXTURE_PREFIX.$entreprise->getSiren())
            );

            $manager->persist($etablissement);

            $this->addReference(
                self::FIXTURE_PREFIX.$entreprise->getSiren().$etablissementData['codeEtablissement'],
                $etablissement
            );
        }
    }

    public function getDependencies()
    {
        return [
            EntrepriseFixtures::class,
        ];
    }

    public static function getGroups(): array
    {
        return ['all', 'entreprises'];
    }
}
