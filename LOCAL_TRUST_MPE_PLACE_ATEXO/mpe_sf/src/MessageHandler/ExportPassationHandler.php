<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\MessageHandler;

use App\Command\Dume\AskDumePublicationAbstractCommand;
use App\Command\Statistique\PassationCommand;
use App\Message\DumePublication;
use App\Message\ExportPassation;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class ExportPassationHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag
    ) {
    }

    public function __invoke(ExportPassation $exportPassation): void
    {
        $cmdBase = PassationCommand::getDefaultName();

        $commande = [
            $this->parameterBag->get('APP_BASE_ROOT_DIR') . 'mpe_sf/bin/console',
            $cmdBase,
            '--export', $exportPassation->getExport(),
            '--organisme', $exportPassation->getAcronyme()
        ];

        try {
            $process = new Process($commande);
            $process->setTimeout(null);
            $process->setIdleTimeout(null);
            $process->mustRun();
        } catch (ProcessFailedException $exception) {
            $this->logger->critical($exception->getMessage());
            throw $exception;
        }
    }
}
