<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\MessageHandler;

use App\Message\AskDumePublicationGroupementEntreprise;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class AskDumePublicationGroupementEntrepriseHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag
    ) {
    }

    public function __invoke(AskDumePublicationGroupementEntreprise $askDumePublicationGroupementEntreprise): void
    {
        $commande = [
            $this->parameterBag->get('APP_BASE_ROOT_DIR') . 'mpe_sf/bin/console',
            'dume:entreprise:groupement-to-publish',
            $askDumePublicationGroupementEntreprise->getIdGroupementEntreprise()
        ];

        try {
            $this->logger->info('Lancement commande : mpe:update:marchePublie avec les parametres suivants', $commande);
            $process = new Process($commande);
            $process->mustRun();

            $outputs = explode('#', $process->getOutput());
            $this->logger->info(
                'L\'execution de la commande mpe:update:marchePublie est terminée ',
                $outputs
            );
        } catch (ProcessFailedException $exception) {
            $this->logger->critical($exception->getMessage());
            throw $exception;
        }
    }
}
