<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\MessageHandler;

use App\Message\SendEmailAR;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class SendEmailARHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag
    ) {
    }

    public function __invoke(SendEmailAR $sendEmailAR): void
    {
        $commande = [
            $this->parameterBag->get('APP_BASE_ROOT_DIR') . 'mpe_sf/bin/console',
            'mpe:depot:email-ar',
            $sendEmailAR->getReferenceConsultation(),
            $sendEmailAR->getIdOffre(),
            $sendEmailAR->getIdInscrit(),
            $sendEmailAR->getIdCandidatureMps()
        ];

        try {
            $this->logger->info('Lancement commande : mpe:depot:email-ar
             avec les parametres suivants', $commande);
            $process = new Process($commande);
            $process->mustRun();

            $outputs = explode('#', $process->getOutput());
            $this->logger->info(
                'L\'execution de la commande mpe:depot:email-ar est terminée ',
                $outputs
            );
        } catch (ProcessFailedException $exception) {
            $this->logger->critical($exception->getMessage());
            throw $exception;
        }
    }
}
