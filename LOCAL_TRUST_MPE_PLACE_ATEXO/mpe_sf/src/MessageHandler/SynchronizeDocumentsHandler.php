<?php

namespace App\MessageHandler;

use App\Message\SynchronizeDocumentsMessage;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class SynchronizeDocumentsHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag
    ) {
    }

    public function __invoke(SynchronizeDocumentsMessage $synchronizeDocumentsMessage)
    {
        $commande = [
            $this->parameterBag->get('APP_BASE_ROOT_DIR') . 'mpe_sf/bin/console',
            'synchronize:documents',
            $synchronizeDocumentsMessage->getReferenceConsultation(),
            $synchronizeDocumentsMessage->getIdEntreprise(),
            $synchronizeDocumentsMessage->getIdEtablissement(),
            $synchronizeDocumentsMessage->getIdOffre()
        ];

        try {
            $this->logger->info('Lancement commande : synchronize:documents
             avec les parametres suivants', $commande);
            $process = new Process($commande);
            $process->mustRun();

            $outputs = explode('#', $process->getOutput());
            $this->logger->info(
                'L\'execution de la commande synchronize:documents est terminée ',
                $outputs
            );
        } catch (ProcessFailedException $exception) {
            $this->logger->critical($exception->getMessage());
            throw $exception;
        }
    }
}
