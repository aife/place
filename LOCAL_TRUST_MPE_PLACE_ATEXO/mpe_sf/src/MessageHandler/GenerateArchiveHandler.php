<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\MessageHandler;

use App\Entity\EchangeDocumentaire\EchangeDoc;
use App\Entity\EchangeDocumentaire\EchangeDocBlob;
use App\Entity\EchangeDocumentaire\EchangeDocTypePieceStandard;
use App\Message\GenerateArchive;
use App\Service\AtexoFichierOrganisme;
use App\Service\EchangeDocumentaire\EchangeDocumentaireService;
use Doctrine\ORM\EntityManagerInterface;
use League\Flysystem\FileNotFoundException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class GenerateArchiveHandler implements MessageHandlerInterface
{
    private const EXTENSION = '.zip';
    private const COMMAND_PRADO_GENERATION_DIC = 'GetPathDic';

    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly AtexoFichierOrganisme $atexoBlobOrganisme,
        private readonly EchangeDocumentaireService $echangeDocumentaireService,
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $archivageLogger
    ) {
    }

    public function __invoke(GenerateArchive $generateArchive): void
    {
        try {
            $process = new Process([
                $this->parameterBag->get('APP_BASE_ROOT_DIR') . 'mpe_sf/bin/console',
                'mpe:prado:cli',
                self::COMMAND_PRADO_GENERATION_DIC,
                'lots=' . $generateArchive->getLot(),
                'consultation_id=' . $generateArchive->getConsultation()->getId(),
                'acronyme_organisme=' . $generateArchive->getConsultation()->getAcronymeOrg(),
                '--no-lock'
            ]);

            $process->setTimeout(null);
            $process->setIdleTimeout(null);

            $this->archivageLogger->info('Lancement de la commande de génération du Dic pour l\'id consultation = ' .
                $generateArchive->getConsultation()->getId());
            $process->mustRun();
            $this->archivageLogger->info(
                'L\'execution de la commande de génération du Dic est terminée pour l\'id consultation = ' .
                $generateArchive->getConsultation()->getId()
            );

            $outputs = explode('#', $process->getOutput());
            $pathTmpDic = is_iterable($outputs) ? $this->getPathTmpDic($outputs) : null;

            if (!$pathTmpDic) {
                $this->archivageLogger->critical('The Dic zip file does not exist');
                throw new FileNotFoundException('The Dic zip file does not exist');
            }

            $tmpFileDic = new UploadedFile(
                $pathTmpDic,
                $generateArchive->getConsultation()->getIntitule() . '_DAC' . self::EXTENSION
            );
            $this->atexoBlobOrganisme->moveTmpFile($pathTmpDic);

            $idBlob = $this->atexoBlobOrganisme->insertFile(
                $tmpFileDic->getClientOriginalName(),
                $tmpFileDic->getPathName(),
                $generateArchive->getConsultation()->getAcronymeOrg(),
                null,
                self::EXTENSION
            );

            $echangeDocBlob = $this->echangeDocumentaireService->newEchangeDocBlob(
                '',
                EchangeDocBlob::FICHIER_PRINCIPAL,
                ['idBlob' => $idBlob, 'chemin' => null]
            );
            $echangeDocTypePieceStandard = $this->entityManager
                ->getRepository(EchangeDocTypePieceStandard::class)->findOneBy(['code' => 'DAC']);
            $echangeDocBlob->setEchangeTypeStandard($echangeDocTypePieceStandard);

            // Récupération de l'échange pour que Doctrine le connaisse à cause de l'asynchrone
            $echangeDoc = $this->entityManager->getRepository(EchangeDoc::class)
                ->find($generateArchive->getEchangeDoc()->getId());

            $echangeDoc->setStatut($this->parameterBag->get('ECHANGE_DOC_STATUT_A_ENVOYER'));
            $echangeDocBlob->setEchangeDoc($echangeDoc);

            $this->entityManager->persist($echangeDocBlob);
            $this->entityManager->flush();
        } catch (ProcessFailedException $exception) {
            $this->archivageLogger->critical($exception->getMessage());
            throw $exception;
        }
    }

    private function getPathTmpDic(array $outputs): ?string
    {
        $pathTmpDic = null;

        foreach ($outputs as $output) {
            if (preg_match('#(^' . $this->parameterBag->get('COMMON_TMP') . ')(.)+(\.zip$)#i', (string) $output)) {
                $pathTmpDic = $output;

                break;
            }
        }

        return  $pathTmpDic;
    }
}
