<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\MessageHandler;

use App\Message\CommandeMessenger;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class CommandeMessengerHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag
    ) {
    }

    public function __invoke(CommandeMessenger $commandeMessenger): int
    {
        $commande = [
            $this->parameterBag->get('APP_BASE_ROOT_DIR') . 'mpe_sf/bin/console',
            $commandeMessenger->getCommande()
        ];

        if (!empty($commandeMessenger->getParams())) {
            $commande = array_merge($commande, $commandeMessenger->getParams());
        }

        try {
            $this->logger->info('');
            $process = new Process($commande);
            $process->mustRun();

            $outputs = explode('#', $process->getOutput());
            $this->logger->info('Commande est terminee');
            $this->logger->info(
                'L\'execution de la commande ' . $commandeMessenger->getCommande() . ' est terminée ',
                $outputs
            );
            return 0;
        } catch (ProcessFailedException $exception) {
            $this->logger->critical($exception->getMessage());
            return 1;
        }
    }
}
