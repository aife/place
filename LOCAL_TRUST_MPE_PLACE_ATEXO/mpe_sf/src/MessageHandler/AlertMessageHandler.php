<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\MessageHandler;

use App\Entity\Consultation;
use App\Message\AlertMessage;
use App\Service\Configuration\PlateformeVirtuelleService;
use App\Service\Helper\EmailHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class AlertMessageHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly MailerInterface $mailer,
        private readonly ParameterBagInterface $parameterBag,
        private readonly EmailHelper $helper,
        private readonly PlateformeVirtuelleService $plateformeVirtuelle,
        private readonly TranslatorInterface $translator,
        private EntityManagerInterface $entityManager
    ) {
    }

    public function __invoke(AlertMessage $alertMessage): void
    {
        $idsList = [];

        foreach ($alertMessage->getConsultations() as $consultation) {
            $idsList[] = $consultation->id;
        }

        $consultationsCollection = $this->entityManager->getRepository(Consultation::class)->findBy(['id' => $idsList]);

        $context = $this->getContext($consultationsCollection);
        $emailCriteria['criteria'] = $this->helper->buildHtmlCriteriaForEmail(
            json_decode($alertMessage->getAlerts()[0]['criteria'], true, 512, JSON_THROW_ON_ERROR)
        );

        foreach ($alertMessage->getAlerts() as $alert) {
            $context = array_merge($context, $this->getAlertContext($alert));
            $typeAvis = $alert['typeAvis'] === $this->parameterBag->get('TYPE_AVIS_CONSULTATION')
                ? $this->translator->trans('TEXT_NOUVELLE_CONSULTATION')
                : $this->translator->trans('TEXT_NOUVELLE_ANNONCE');
            $subject = $context['pfLongName'] . ' - ' . $typeAvis . ' : ' . count($alertMessage->getConsultations())
                .  ' - ' . $alert['denomination'];
            $email = new TemplatedEmail();
            $email->from($context['from'])->to($alert['email'])
                ->subject($subject)
                ->htmlTemplate('email/alert-entreprise.html.twig')
                ->textTemplate('email/alert-entreprise.txt.twig')
                ->context(array_merge($context, $emailCriteria));

            $this->mailer->send($email);
        }
    }

    private function getAlertContext(array $alert): array
    {
        $context = [];
        $context['alertName'] = $alert['denomination'];
        if (null !== $alert['platformeVirtuelleId']) {
            $platform = $this->plateformeVirtuelle->getPlateformeVirtuelleById($alert['platformeVirtuelleId']);
            $context['from'] = $platform->getNoReply();
            $context['pfLongName'] = $platform->getFromPfName();
            $context['pfUrlReference'] = $platform->getProtocole() . '://' . $platform->getDomain();
            $context['imageBandeau'] = $this->helper->getImageBandeau($platform->getCodeDesign());
            $context['printPdfUrl'] = $context['pfUrlReference'] . '?page=Entreprise.GetMailFormatPdf&amp;idAlerte='
                . 'alerteId_' . uniqid();
        }

        return $context;
    }

    private function getContext(iterable $consultations): array
    {
        $context = [];
        $context['titleColor'] = $this->parameterBag->get('MAIL_COULEUR_TITRE');
        $context['imageBandeau'] = $this->helper->getImageBandeau();
        $context['urlImageBandeau'] = $this->helper->getUrlImageBandeau();
        $context['pfLongName'] = $this->parameterBag->get('PF_LONG_NAME');
        $context['from'] = $this->parameterBag->get('PF_MAIL_FROM');
        $context['pfUrlReference'] = $this->parameterBag->get('PF_URL_REFERENCE');
        $context['printPdfUrl'] = $context['pfUrlReference'] . '?page=Entreprise.GetMailFormatPdf&amp;idAlerte='
            . 'alerteId_' . uniqid();
        $context['consultations'] = $consultations;

        return $context;
    }
}
