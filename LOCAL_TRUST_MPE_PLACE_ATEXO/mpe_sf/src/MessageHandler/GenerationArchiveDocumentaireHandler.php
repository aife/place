<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\MessageHandler;

use App\Message\GenerationArchiveDocumentaire;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class GenerationArchiveDocumentaireHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag
    ) {
    }

    public function __invoke(GenerationArchiveDocumentaire $generationArchiveDocumentaire): void
    {
        $commande = [
            $this->parameterBag->get('APP_BASE_ROOT_DIR') . 'mpe_sf/bin/console',
            'mpe:generation-archive-documentaire',
            $generationArchiveDocumentaire->getConsultationId(),
            $generationArchiveDocumentaire->getAgentId(),
            $generationArchiveDocumentaire->getJson()
        ];

        if (null !== $generationArchiveDocumentaire->getType()) {
            $commande[] = $generationArchiveDocumentaire->getType();
        }

        if (null !== $generationArchiveDocumentaire->getSupprimerApres()) {
            $commande[] = $generationArchiveDocumentaire->getSupprimerApres();
        }

        try {
            $this->logger->info('Lancement commande : mpe:generation-archive-documentaire
             avec les parametres suivants', $commande);
            $process = new Process($commande);
            $process->mustRun();
        } catch (ProcessFailedException $exception) {
            $this->logger->critical($exception->getMessage());
            throw $exception;
        }
    }
}
