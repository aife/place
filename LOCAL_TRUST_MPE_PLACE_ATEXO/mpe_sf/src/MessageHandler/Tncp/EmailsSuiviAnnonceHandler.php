<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\MessageHandler\Tncp;

use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Message\Tncp\Consumer\AnnonceSuiviMessage;
use App\Message\Tncp\Consumer\SendEmailAnnonceSuiviMessage;
use App\Repository\AgentRepository;
use App\Repository\Consultation\ConsultationFavorisRepository;
use App\Repository\ConsultationRepository;
use App\Service\Agent\Guests;
use App\Service\Helper\EmailHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class EmailsSuiviAnnonceHandler implements MessageHandlerInterface
{
    public function __construct(
        private ConsultationRepository $consultationRepository,
        private Guests $agentGuests,
        private AgentRepository $agentRepository,
        private MailerInterface $mailer,
        private TranslatorInterface $translator,
        private ParameterBagInterface $parameterBag,
        private ConsultationFavorisRepository $favorisRepository,
        private EntityManagerInterface $entityManager,
        private EmailHelper $helper
    ) {
    }

    public function __invoke(SendEmailAnnonceSuiviMessage $message)
    {
        $consultation = $this->consultationRepository->find($message->getConsultationId());
        $isPublished = AnnonceSuiviMessage::STATUS_PUBLISHED === $message->getStatus();
        $alertType = $isPublished ? AgentRepository::ALERTE_PUBLICATION_BOAMP : AgentRepository::ALERTE_PUBLICATION_BOAMP_FAILED;
        $idAgents = $this->agentGuests->getAllGuestsByConsulation($consultation);
        $agents = $this->agentRepository->getAgentsByAlerte($idAgents, $alertType);
        $from = $this->parameterBag->get('PF_MAIL_FROM');
        $idAgentsInConsultationFavoris = $this->favorisRepository->getAgentsByConsultationAndAlertType(
            $consultation->getId(),
            $alertType
        );

        $idAgentsInConsultationFavoris = array_column($idAgentsInConsultationFavoris, 'id');
        $platformName = $this->parameterBag->get('PF_LONG_NAME');
        $design = null;
        if (null !== ($platformVirtual = $consultation->getPlateformeVirtuelle())) {
            $from = $platformVirtual->getNoReply();
            $platformName = $platformVirtual->getFromPfName();
            $design = $platformVirtual->getCodeDesign();
        }

        $subject = $isPublished
            ? $this->translator->trans('PUBLICATION_BOAMP_EFFECTIVE')
            : $this->translator->trans('PUBLICATION_BOAMP_ECHEC');

        $typePrcedure = $this->entityManager->getRepository(TypeProcedureOrganisme::class)
            ->findOneBy([
                'idTypeProcedure' => $consultation->getIdTypeProcedure(),
                'organisme' => $consultation->getOrganisme()->getAcronyme()
            ])->getLibelleTypeProcedure();

        foreach ($agents as $agent) {
            if ($agent->getEmail() && !in_array($agent->getId(), $idAgentsInConsultationFavoris)) {
                $email = new TemplatedEmail();
                $email->from($from)->to($agent->getEmail())
                    ->subject(
                        sprintf(
                            '%s  -  %s : %s',
                            $subject,
                            $this->translator->trans('TEXT_REF'),
                            $consultation->getReferenceUtilisateur()
                        )
                    )
                    ->htmlTemplate('publicite/mailPublicationBoamp.html.twig')
                    ->context([
                        'consultation' => $consultation,
                        'agent' => $agent,
                        'platformName' => $platformName,
                        'isPublished' => $isPublished,
                        'typePrcedure' => $typePrcedure,
                        'imageBandeau' => $this->helper->getImageBandeau($design)
                    ]);

                $this->mailer->send($email);
            }
        }
    }
}
