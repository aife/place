<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\MessageHandler\Tncp;

use App\Entity\AnnonceBoamp;
use App\Entity\AnnonceSuivi;
use App\Entity\Consultation;
use App\Entity\InterfaceSuivi;
use App\Entity\TDumeContexte;
use App\Message\Tncp\Consumer\AnnonceSuiviMessage;
use App\Message\Tncp\Consumer\SendEmailAnnonceSuiviMessage;
use App\Repository\AnnonceSuiviRepository;
use App\Repository\ConsultationRepository;
use App\Repository\InterfaceSuiviRepository;
use App\Service\AtexoConfiguration;
use App\Service\MessageBusService;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class AnnonceSuiviHandler implements MessageHandlerInterface
{
    public function __construct(
        private InterfaceSuiviRepository $interfaceSuiviRepository,
        private ConsultationRepository $consultationRepository,
        private EntityManagerInterface $entityManager,
        private ParameterBagInterface $parameterBag,
        private AtexoConfiguration $configuration,
        private MessageBusService $dume,
        private MessageBusInterface $messageBus
    ) {
    }

    public function __invoke(AnnonceSuiviMessage $message)
    {
        $annonceSuivi = $this->interfaceSuiviRepository->findOneByUuid($message->getUuidPublicite());
        $consultation = $this->consultationRepository->find($message->getConsultationId());
        $oldDateMiseEnligne = $consultation->getDateMiseEnLigneCalcule();

        $newAnnonce = new InterfaceSuivi();
        $newAnnonce->setUuid(Uuid::fromString($message->getUuidPublicite()))
            ->setFlux(InterfaceSuivi::ANNONCE_TNCP_INTERFACE)
            ->setAction(InterfaceSuivi::ACTION_PUBLICATION_ANNONCE)
            ->setEtape(InterfaceSuivi::ETAPE_ANNONCE_SYNCHRO_CONSULTATION)
            ->setConsultation($consultation)
            ->setStatut(InterfaceSuivi::STATUS_EN_COURS)
            ->setDateCreation(new \DateTime())
            ->setDateModification(new \DateTime());

        if (!($annonceBoamp = $annonceSuivi?->getAnnonce()) instanceof AnnonceBoamp) {
            $annonceBoamp = new AnnonceBoamp();
            $annonceBoamp->setConsultationId($message->getConsultationId());
            $annonceBoamp->setStatutDestinataire($message->getStatus());
            $annonceBoamp->setOrganisme($message->getOrganisme());
            $annonceBoamp->setDateEnvoi($message->getDateVerification()->format('Y-m-d H:i:s'));
            $annonceBoamp->setTypeBoamp($message->getTypeBoamp());
            $this->entityManager->persist($annonceBoamp);
        }
        $newAnnonce->setAnnonce($annonceBoamp);

        if (AnnonceSuiviMessage::STATUS_PUBLISHED === $message->getStatus()) {
            $annonceBoamp->setDatePub($message->getDatePublication()->format('Y-m-d H:i:s'));
            $this->messageBus->dispatch(
                new SendEmailAnnonceSuiviMessage(
                    AnnonceSuiviMessage::STATUS_PUBLISHED,
                    $message->getConsultationId()
                )
            );
            $newAnnonce->setStatut(InterfaceSuivi::STATUS_FINI);
        }

        if (
            in_array($message->getStatus(), [
            AnnonceSuiviMessage::STATUT_RX,
            AnnonceSuiviMessage::STATUT_RG,
            AnnonceSuiviMessage::STATUT_RE ])
        ) {
            $newAnnonce->setStatut(InterfaceSuivi::STATUS_ERREUR);
            $this->messageBus->dispatch(
                new SendEmailAnnonceSuiviMessage(
                    AnnonceSuiviMessage::STATUS_ERROR,
                    $message->getConsultationId()
                )
            );
        }

        if ($message->getUrlBoamp()) {
            $annonceBoamp->setLienBoamp($message->getUrlBoamp());
        }

        if ($message->getUrlPdf()) {
            $annonceBoamp->setLienPdf($message->getUrlPdf());
        }

        $this->entityManager->persist($newAnnonce);
        $this->updateConsultationDateMiseEnLigneCalcule($consultation, $annonceBoamp);
        $this->entityManager->flush();

        if (
            $this->configuration->isModuleEnabled('InterfaceDume')
            && (1 === (int)$consultation->getDumeDemande())
            && (
                $oldDateMiseEnligne !== $consultation->getDateMiseEnLigneCalcule()
                && !empty($consultation->getDateMiseEnLigneCalcule())
            )
        ) {
            $dumeContext = $this->entityManager->getRepository(TDumeContexte::class)->getTDumeContext(
                $consultation->getId(),
                $message->getOrganisme(),
                $this->parameterBag->get('TYPE_DUME_ACHETEUR')
            );
            $this->dume->publishDumePublication('acheteur', true, $dumeContext->getId());
        }
    }

    private function updateConsultationDateMiseEnLigneCalcule(Consultation $consultation, AnnonceBoamp $annonceBoamp): void
    {
        if (
            (int) $this->parameterBag->get('MISE_EN_LIGNE_APRES_VALIDATION_ET_ENVOI_BOAMP')
            === $consultation->getRegleMiseEnLigne()
            && null === $consultation->getDateMiseEnLigneCalcule()
        ) {
            $dateMiseEnLigne = match (true) {
                $annonceBoamp->getDateEnvoi() <= $consultation->getDatevalidation() => new \DateTime($consultation->getDatevalidation()),
                $consultation->getDatevalidation() > "0000-00-00" => new \DateTime($annonceBoamp->getDateEnvoi()),
                default => null
            };
            $consultation->setDateMiseEnLigneCalcule($dateMiseEnLigne);
        }

        if (
            (int) $this->parameterBag->get('MISE_EN_LIGNE_APRES_VALIDATION_ET_PUBLICATION_BOAMP')
            === $consultation->getRegleMiseEnLigne()
            && null === $consultation->getDateMiseEnLigneCalcule()
            && AnnonceSuiviMessage::STATUS_PUBLISHED === $annonceBoamp->getStatutDestinataire()
        ) {
            $dateMiseEnLigne = match (true) {
                $annonceBoamp->getDatePub() <= $consultation->getDatevalidation() => new \DateTime($consultation->getDatevalidation()),
                $consultation->getDatevalidation() > "0000-00-00" => new \DateTime($annonceBoamp->getDatePub()),
                default => null
            };
            $consultation->setDateMiseEnLigneCalcule($dateMiseEnLigne);
        }
    }
}
