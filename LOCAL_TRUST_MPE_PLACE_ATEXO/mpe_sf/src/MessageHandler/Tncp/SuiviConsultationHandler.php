<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\MessageHandler\Tncp;

use App\Entity\Consultation;
use App\Entity\InterfaceSuivi;
use App\Message\Tncp\Consumer\MessageSuiviConsultationInput;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SuiviConsultationHandler implements MessageHandlerInterface
{
    public function __construct(private readonly EntityManagerInterface $entityManager, private readonly LoggerInterface $logger)
    {
    }

    public function __invoke(MessageSuiviConsultationInput $suiviConsultation)
    {
        $consultation = $this->entityManager->getRepository(Consultation::class)->find(
            $suiviConsultation->getIdObjetSource()
        );

        if (! $consultation instanceof Consultation) {
            $this->logger->error(
                sprintf('Consultation with ID %s not found.', $suiviConsultation->getIdObjetSource())
            );

            return;
        }

        $status = match ($suiviConsultation->getStatut()) {
            InterfaceSuivi::STATUS_EN_ATTENTE_TEXT => InterfaceSuivi::STATUS_EN_ATTENTE,
            InterfaceSuivi::STATUS_EN_COURS_TEXT => InterfaceSuivi::STATUS_EN_COURS,
            InterfaceSuivi::STATUS_FINI_TEXT => InterfaceSuivi::STATUS_FINI,
            InterfaceSuivi::STATUS_ERREUR_TEXT => InterfaceSuivi::STATUS_ERREUR,
            default => 0
        };
        $interfaceSuivi = new InterfaceSuivi();
        $interfaceSuivi->setUuid(Uuid::fromString($suiviConsultation->getUuid()));
        $interfaceSuivi->setFlux($suiviConsultation->getFlux());
        $interfaceSuivi->setAction($suiviConsultation->getType());
        $interfaceSuivi->setStatut($status);
        $interfaceSuivi->setIdObjetDestination($suiviConsultation->getIdObjetDestination());
        $interfaceSuivi->setMessage($suiviConsultation->getMessage());
        $interfaceSuivi->setConsultation($consultation);
        $interfaceSuivi->setDateCreation($suiviConsultation->getDate());
        $interfaceSuivi->setDateModification($suiviConsultation->getDate());
        $interfaceSuivi->setEtape($suiviConsultation->getEtape());
        $interfaceSuivi->setMessageDetails($suiviConsultation->getMessageDetails());

        $this->entityManager->persist($interfaceSuivi);
        $this->entityManager->flush();
        $this->logger->info(
            sprintf(
                'New suivi added for consultation %s with action %s and status %s.',
                $suiviConsultation->getIdObjetSource(),
                $suiviConsultation->getType(),
                $suiviConsultation->getStatut()
            )
        );
    }
}
