<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\MessageHandler;

use App\Command\Dume\AskDumePublicationAbstractCommand;
use App\Message\DumePublication;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class DumePublicationHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag
    ) {
    }

    public function __invoke(DumePublication $dumePublication): void
    {
        $cmdBase = AskDumePublicationAbstractCommand::CMD_PREFIX;
        $type = $dumePublication->getType() == AskDumePublicationAbstractCommand::TYPE_ENTREPRISE
            ? AskDumePublicationAbstractCommand::TYPE_ENTREPRISE
            : AskDumePublicationAbstractCommand::TYPE_ACHETEUR;
        $etat = $dumePublication->isPublish()
            ? 'publish'
            : 'valid';

        $commande = [
            $this->parameterBag->get('APP_BASE_ROOT_DIR') . 'mpe_sf/bin/console',
            $cmdBase . ':' . $type . ':' . $etat
        ];

        if ($dumePublication->getContexteLtDumeId() !== null) {
            $commande[] = $dumePublication->getContexteLtDumeId();
        }

        try {
            $this->logger->info('Lancement commande : ' . $cmdBase . ':' . $type . ':' . $etat .
                ' avec les parametres suivants', $commande);
            $process = new Process($commande);
            $process->mustRun();

            $outputs = explode('#', $process->getOutput());
            $this->logger->info(
                'L\'execution de la commande ' . $cmdBase . ':' . $type . ':' . $etat . ' est terminée',
                $outputs
            );
        } catch (ProcessFailedException $exception) {
            $this->logger->critical($exception->getMessage());
            throw $exception;
        }
    }
}
