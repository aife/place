<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\EventListener\App;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use App\Service\App\GraphicChart;

/**
 * Class DesignCookieListener.
 *
 * @author Sébastien Lepers <sebastien.lepers@atexo.com>
 */
class DesignCookieListener// TODO: implements EventSubscriberInterface
{
    /**
     * DesignCookieListener constructor.
     */
    public function __construct(protected GraphicChart $graphicChart)
    {
    }

    public function onKernelRequest(RequestEvent $event)
    {
        if (!$event->isMainRequest()) {
            // don't do anything if it's not the master request
            return;
        }

        $this->graphicChart->setDesignCookie();
    }

//    /**
//     * @return array<string, mixed>
//     */
//    public static function getSubscribedEvents(): array
//    {
//        return [KernelEvents::REQUEST => ''];
//    }
}
