<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\EventListener;

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Exception;
use App\Controller\Api\AbstractWebserviceController;
use App\Entity\Agent;
use App\Entity\WS\AgentTechniqueToken;
use App\Entity\WS\WebService;
use App\Exception\ApiProblemException;
use App\Exception\ApiProblemInvalidTokenException;
use App\Exception\ApiProblemUnauthorizedException;
use App\Model\ApiProblem;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;
use Twig\Environment;

class ApiSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly Environment $twig,
        private readonly EntityManagerInterface $em,
        private readonly RouterInterface $router
    ) {
    }

    /**
     * On déclenche une exception propre pour nos webservices.
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function onKernelException(ExceptionEvent $event)
    {
        // seulement pour les exceptions dont l'url est /api mais pas /api/v2
        if (
            !str_starts_with($event->getRequest()->getPathInfo(), '/api')
            || str_starts_with($event->getRequest()->getPathInfo(), '/api/v2/')
        ) {
            return;
        }

        $e = $event->getThrowable();

        $statusCode = $e instanceof HttpExceptionInterface ? $e->getStatusCode() : 500;

        if ($statusCode >= 500) {
            return;
        }

        $this->logException($e);

        if ($e instanceof ApiProblemException) {
            $apiProblem = $e->getApiProblem();
        } else {
            $apiProblem = new ApiProblem(
                $statusCode
            );

            /*
             * If it's an HttpException message (e.g. for 404, 403),
             * we'll say as a rule that the exception message is safe
             * for the client. Otherwise, it could be some sensitive
             * low-level exception, which should *not* be exposed
             */
            if ($e instanceof HttpExceptionInterface) {
                $apiProblem->set('detail', $e->getMessage());
            }
        }

        $data = $apiProblem->toArray();
        //On créé une url vers le type de l'erreur (norme RFC mais ça peut ne pas être utile)
        if ('about:blank' != $data['type']) {
            $data['type'] = 'http://localhost:8000/docs/errors#' . $data['type'];
        }

        $response = null;

        if ('xml' == $event->getRequest()->get('format')) {
            $data = $this->twig->render('api/error.xml.twig', ['error' => $data]);
            $formatHeader = 'application/problem+xml';
        } else {
            $data['statutReponse'] = 'KO';
            $data = json_encode(['mpe' => ['reponse' => $data]], JSON_THROW_ON_ERROR);

            $formatHeader = 'application/problem+json';
        }

        $response = new Response(
            $data,
            $apiProblem->getStatusCode()
        );
        $response->headers->set('Content-Type', $formatHeader);
        $event->setResponse($response);
    }

    /**
     * On vérifie que le token est valide avant le chargement de la classe.
     */
    public function onKernelController(ControllerEvent $event)
    {
        // seulement pour les exceptions dont l'url est /api mais pas /api/v2
        if (
            (!str_starts_with($event->getRequest()->getPathInfo(), '/api')
            || str_starts_with($event->getRequest()->getPathInfo(), '/api/v2/'))
            && !str_starts_with($event->getRequest()->getPathInfo(), '/token/generate')
        ) {
            return;
        }
        if (str_starts_with($event->getRequest()->getPathInfo(), '/token/generate')) {
            $this->logger->warning('WS "/token/generate" called');
        }

        $controller = $event->getController();

        /*
         * $controller passed can be either a class or a Closure.
         * This is not usual in Symfony but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controller)) {
            return;
        }

        if (
            $controller[0] instanceof AbstractWebserviceController
            || str_starts_with($event->getRequest()->getPathInfo(), '/token/generate')
        ) {
            if (false !== $controller[0]->isNeedsHabilitation()) {
                if (!$this->isSession($event) && !$this->checkBearerToken($event) && !$this->checkGetToken($event)) {
                    $invalidTokenMessage =
                       ApiProblem::TYPE_UNAUTHORIZED
                       . ' for path '
                       . $event->getRequest()->getRequestURI()
                    ;

                    throw new ApiProblemInvalidTokenException($invalidTokenMessage);
                }
                $msg = '';
                if (!$this->isGranted($event, $controller[0], $msg)) {
                    throw new ApiProblemUnauthorizedException($msg);
                }
            }
        }
    }

    private function isGranted($event, &$abstractWebserviceController, &$msg = '')
    {
        $granted = false;
        $methode = $event->getRequest()->getMethod();
        $route = $event->getRequest()->attributes->get('_route');
        $routeInformation = $this->router->getRouteCollection()->get($route);

        $webservice = $this->em->getRepository(WebService::class)->findOneBy(
            [
            'methodWs' => $methode,
            'routeWs' => $routeInformation->getPath(),
            ]
        );

        if (null !== $webservice) {
            $agent = $this->getAgent($event);

            if (empty($agent)) {
                $token = $this->getTokent($event);
                $agentTechniqueToken = $this->em->getRepository(AgentTechniqueToken::class)->findOneBy(
                    [
                        'token' => $token,
                    ]
                );
                $agent = $agentTechniqueToken->getAgent();
            }
            if (!$agent) {
                $msg = 'Agent non trouvé !';

                return false;
            }
            $agent = $this->em->getRepository(Agent::class)->find($agent->getId());
            $hasHabilitationWs = $agent->getWebservices()->contains($webservice);

            if (
                str_starts_with($event->getRequest()->getPathInfo(), '/token/generate')
                && false === $webservice->isUniquementTechnique()
            ) {
                $granted = true;
            } elseif (
                (true === $agent->isTechnique() && $hasHabilitationWs)
                || false === $webservice->isUniquementTechnique()
            ) {
                $granted = true;
                $abstractWebserviceController->setCurrentAgentTechnique($agent);
                $abstractWebserviceController->setCurrentWebservice($webservice);
            } else {
                $msg = 'L’application ' . $agent->getNom() . ' n’est pas habilité à consommer le WS '
                    . $webservice->getNomWs();
            }
        }

        return $granted;
    }

    /**
     * Fonction de récupération du token utilisable dans cette classe.
     *
     * @param  $event
     *
     * @return bool|mixed|null
     */
    public function getTokent($event)
    {
        $token = null;
        if ($this->checkBearerToken($event)) {
            $token = $this->extractToken($event->getRequest()->headers->get('Authorization'));
        } elseif ($this->checkGetToken($event)) {
            $token = $event->getRequest()->query->get('ticket');
            if (empty($token)) {
                $token = $event->getRequest()->get('ticket');
            }
            if (empty($token)) {
                $token = $event->getRequest()->get('token');
            }
        }

        return $token;
    }

    private function checkBearerToken($event)
    {
        $token = $this->extractToken($event->getRequest()->headers->get('Authorization'));
        if ($this->checkToken($token)) {
            return true;
        }

        return false;
    }

    private function checkGetToken($event)
    {
        $token = $event->getRequest()->query->get('ticket');
        if (empty($token)) {
            $token = $event->getRequest()->get('ticket');
        }
        if (empty($token)) {
            $token = $event->getRequest()->get('token');
        }

        if ($this->checkToken($token)) {
            return true;
        }

        return false;
    }

    private function checkToken($token)
    {
        if (empty($token) || empty($this->em->getRepository(AgentTechniqueToken::class)->checkToken($token))) {
            return false;
        }

        return true;
    }

    private function isSession(ControllerEvent $event): bool
    {
        return $this->getAgent($event) instanceof Agent;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }

    /**
     *  Logger correctement les erreurs en fonction du type.
     */
    private function logException(Exception $exception)
    {
        $message = sprintf(
            'Uncaught PHP Exception %s: "%s" at %s line %s',
            $exception::class,
            $exception->getMessage(),
            $exception->getFile(),
            $exception->getLine()
        );
        $isCritical = !$exception instanceof HttpExceptionInterface || $exception->getStatusCode() >= 500;
        $context = ['exception' => $exception];
        if ($isCritical) {
            $this->logger->critical($message, $context);
        } else {
            $this->logger->error($message, $context);
        }
    }

    private function extractToken($authorizationHeader)
    {
        $headerParts = explode(' ', (string) $authorizationHeader);
        if (!(2 === count($headerParts) && 0 === strcasecmp($headerParts[0], 'Bearer'))) {
            return false;
        }

        return $headerParts[1];
    }

    private function getAgent(ControllerEvent $event): ?Agent
    {
        $request = $event->getRequest();
        if (!$request->hasPreviousSession()) {
            return null;
        }
        $user =  null;
        $sessionAttributes = $request->getSession()->all();

        if (
            !empty($sessionAttributes) &&
            (!empty($sessionAttributes['_security_agents']) || !empty($sessionAttributes['_security_agents_saml']))
        ) {
            // On est malheureusement obligé de vérifier la présence des infos de session de plusieurs
            // firewalls de sécurité SF.
            $sessionAttribute = $sessionAttributes['_security_agents'] ?? $sessionAttributes['_security_agents_saml'];

            /** @var AbstractToken $postAuthenticationGuardToken */
            $postAuthenticationGuardToken = unserialize($sessionAttribute);
            $user = $postAuthenticationGuardToken->getUser();
        }

        return ($user instanceof Agent) ? $user : null;
    }
}
