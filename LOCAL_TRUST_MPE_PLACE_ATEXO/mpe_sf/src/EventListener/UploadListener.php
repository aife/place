<?php

namespace App\EventListener;

use Oneup\UploaderBundle\Event\PostPersistEvent;

class UploadListener
{
    public function onUpload(PostPersistEvent $event)
    {
        $file = $event->getFile();
        $response = $event->getResponse();

        $url = '...';
        $deleteUrl = '...';

        $response['files'] = [
            'name' => $file->getFilename(),
            'size' => $file->getSize(),
            'url' => $url,
            'deleteUrl' => $deleteUrl,
            'deleteType' => 'DELETE',
        ];
    }
}
