<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Message;

class DeleteArchive
{
    public function __construct(private readonly string $pathArchive)
    {
    }

    /**
     * @return string
     */
    public function getPathArchive(): string
    {
        return $this->pathArchive;
    }
}
