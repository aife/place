<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Message\Tncp\Consumer;

class AnnonceSuiviMessage
{
    public const STATUS_PUBLISHED = 'PU';
    public const STATUS_ERROR = 'ERROR';
    public const STATUT_AV = 'AV';
    public const STATUT_RG = 'RG';
    public const STATUT_RX = 'RX';
    public const STATUT_RE = 'RE';
    public const STATUT_CP = 'CP';
    public const STATUT_AP = 'AP';
    public function __construct(
        private string $uuidPublicite,
        private int $consultationId,
        private string $consultationReference,
        private string $status,
        private \DateTime $dateVerification,
        private string $typeBoamp,
        private string $organisme,
        private ?\DateTime $datePublication = null,
        private ?string $urlPdf = null,
        private ?string $urlBoamp = null
    ) {
    }

    public function getUuidPublicite(): string
    {
        return $this->uuidPublicite;
    }

    public function getConsultationId(): int
    {
        return $this->consultationId;
    }

    public function getConsultationReference(): string
    {
        return $this->consultationReference;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getDatePublication(): ?\DateTime
    {
        return $this->datePublication;
    }

    public function getDateVerification(): \DateTime
    {
        return $this->dateVerification;
    }

    public function getTypeBoamp(): string
    {
        return $this->typeBoamp;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function getUrlPdf(): ?string
    {
        return $this->urlPdf;
    }

    public function getUrlBoamp(): ?string
    {
        return $this->urlBoamp;
    }
}
