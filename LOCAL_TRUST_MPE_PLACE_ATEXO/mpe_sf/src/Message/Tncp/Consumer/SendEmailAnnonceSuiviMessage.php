<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Message\Tncp\Consumer;

class SendEmailAnnonceSuiviMessage
{
    public function __construct(private string $status, private int $consultationId)
    {
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getConsultationId(): int
    {
        return $this->consultationId;
    }
}
