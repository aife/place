<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Message\Tncp\Consumer;

use DateTimeInterface;

class MessageSuiviConsultationInput
{
    public function __construct(
        private readonly string $uuid,
        private readonly string $flux,
        private readonly string $type,
        private readonly string $statut,
        private readonly ?int $idObjetSource,
        private readonly int $idObjetDestination,
        private readonly ?string $message,
        private readonly string $uuidPlateforme,
        private readonly ?string $messageDetails,
        private readonly DateTimeInterface $date,
        private readonly ?string $etape
    ) {
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getFlux(): string
    {
        return $this->flux;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getStatut(): string
    {
        return $this->statut;
    }

    public function getIdObjetSource(): ?int
    {
        return $this->idObjetSource;
    }

    public function getIdObjetDestination(): int
    {
        return $this->idObjetDestination;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function getUuidPlateforme(): string
    {
        return $this->uuidPlateforme;
    }

    public function getMessageDetails(): ?string
    {
        return $this->messageDetails;
    }

    public function getDate(): DateTimeInterface
    {
        return $this->date;
    }

    public function getEtape(): ?string
    {
        return $this->etape;
    }
}
