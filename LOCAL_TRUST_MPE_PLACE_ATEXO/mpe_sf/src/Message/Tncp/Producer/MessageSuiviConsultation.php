<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Message\Tncp\Producer;

use DateTimeInterface;
use DateTime;
use App\Entity\InterfaceSuivi;

class MessageSuiviConsultation
{
    public function __construct(
        private readonly string $uuid,
        private readonly string $flux,
        private readonly string $type,
        private readonly ?int $idObjetSource,
        private readonly int $idObjetDestination,
        private readonly string $url,
        private readonly string $uuidPlateforme,
        private readonly string $token,
        private readonly DateTimeInterface $dateEnvoi,
        private readonly ?string $etape = null
    ) {
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getFlux(): string
    {
        return $this->flux;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getIdObjetSource(): ?int
    {
        return $this->idObjetSource;
    }

    public function getIdObjetDestination(): int
    {
        return $this->idObjetDestination;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getUuidPlateforme(): string
    {
        return $this->uuidPlateforme;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getDateEnvoi(): DateTimeInterface
    {
        return $this->dateEnvoi;
    }

    public function getEtape(): ?string
    {
        return $this->etape;
    }

    public static function normalize(InterfaceSuivi $suivi, string $url, string $uuid, string $token): self
    {
        return new self(
            $suivi->getUuid(),
            $suivi->getFlux(),
            $suivi->getAction(),
            $suivi->getConsultation()?->getId(),
            (int) $suivi->getIdObjetDestination(),
            $url,
            $uuid,
            $token,
            new DateTime(),
            $suivi->getEtape()
        );
    }
}
