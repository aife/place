<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Message;

class UpdateMarchePublie
{
    public function __construct(private readonly ?string $annee = null)
    {
    }

    /**
     * @return string|null
     */
    public function getAnnee(): ?string
    {
        return $this->annee;
    }
}
