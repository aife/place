<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Message;

class SendEmailAR
{
    public function __construct(
        private readonly string $referenceConsultation,
        private readonly string $idOffre,
        private readonly string $idInscrit,
        private readonly string $idCandidatureMps
    ) {
    }

    /**
     * @return string
     */
    public function getReferenceConsultation(): string
    {
        return $this->referenceConsultation;
    }

    /**
     * @return string
     */
    public function getIdOffre(): string
    {
        return $this->idOffre;
    }

    /**
     * @return string
     */
    public function getIdInscrit(): string
    {
        return $this->idInscrit;
    }

    /**
     * @return string
     */
    public function getIdCandidatureMps(): string
    {
        return $this->idCandidatureMps;
    }
}
