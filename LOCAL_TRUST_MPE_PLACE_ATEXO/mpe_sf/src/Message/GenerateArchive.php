<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Message;

use App\Entity\Consultation;
use App\Entity\EchangeDocumentaire\EchangeDoc;

class GenerateArchive
{
    public function __construct(
        private readonly EchangeDoc $echangeDoc,
        private readonly ?string $lot = null
    ) {
    }

    public function getConsultation(): Consultation
    {
        return $this->echangeDoc->getConsultation();
    }

    public function getEchangeDoc(): EchangeDoc
    {
        return $this->echangeDoc;
    }

    public function getLot(): ?string
    {
        return $this->lot;
    }
}
