<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Message;

class AlertMessage
{
    public function __construct(private readonly array $consultations, private readonly array $alerts)
    {
    }

    public function getConsultations(): array
    {
        return $this->consultations;
    }

    public function getAlerts(): array
    {
        return $this->alerts;
    }
}
