<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Message;

class SynchronizeDocumentsMessage
{
    public function __construct(
        private readonly string $referenceConsultation,
        private readonly string $idEntreprise,
        private readonly ?string $idEtablissement,
        private readonly ?string $idOffre
    ) {
    }

    /**
     * @return string
     */
    public function getReferenceConsultation(): string
    {
        return $this->referenceConsultation;
    }

    /**
     * @return string
     */
    public function getIdEntreprise(): string
    {
        return $this->idEntreprise;
    }

    /**
     * @return string|null
     */
    public function getIdEtablissement(): ?string
    {
        return $this->idEtablissement;
    }

    /**
     * @return string|null
     */
    public function getIdOffre(): ?string
    {
        return $this->idOffre;
    }
}
