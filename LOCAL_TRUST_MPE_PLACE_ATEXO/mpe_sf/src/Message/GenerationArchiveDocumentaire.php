<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Message;

class GenerationArchiveDocumentaire
{
    public function __construct(
        private readonly string $consultationId,
        private readonly string $agentId,
        private readonly string $json,
        private readonly ?string $type = null,
        private readonly ?string $supprimerApres = null,
    ) {
    }

    /**
     * @return string
     */
    public function getConsultationId(): string
    {
        return $this->consultationId;
    }

    /**
     * @return string
     */
    public function getAgentId(): string
    {
        return $this->agentId;
    }

    /**
     * @return string
     */
    public function getJson(): string
    {
        return $this->json;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @return string|null
     */
    public function getSupprimerApres(): ?string
    {
        return $this->supprimerApres;
    }
}
