<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Message;

class AskDumePublicationGroupementEntreprise
{
    public function __construct(
        private readonly string $idGroupementEntreprise,
    ) {
    }

    /**
     * @return string
     */
    public function getIdGroupementEntreprise(): string
    {
        return $this->idGroupementEntreprise;
    }
}
