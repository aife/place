<?php

namespace App\Message;

class CommandeMessenger
{
    public function __construct(
        private readonly string $commande,
        private readonly ?array $params = null
    ) {
    }

    /**
     * @return string
     */
    public function getCommande(): string
    {
        return $this->commande;
    }

    /**
     * @return array|null
     */
    public function getParams(): ?array
    {
        return $this->params;
    }
}
