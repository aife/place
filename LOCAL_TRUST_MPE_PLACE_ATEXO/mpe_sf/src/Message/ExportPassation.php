<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Message;

class ExportPassation
{
    public function __construct(
        private readonly string $export,
        private readonly string $acronyme
    ) {
    }

    /**
     * @return string
     */
    public function getExport(): string
    {
        return $this->export;
    }

    /**
     * @return string
     */
    public function getAcronyme(): string
    {
        return $this->acronyme;
    }
}
