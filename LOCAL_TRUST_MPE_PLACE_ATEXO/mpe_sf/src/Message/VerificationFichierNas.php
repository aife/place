<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Message;

class VerificationFichierNas
{
    public function __construct(private readonly string $delay)
    {
    }

    /**
     * @return string
     */
    public function getDelay(): string
    {
        return $this->delay;
    }
}
