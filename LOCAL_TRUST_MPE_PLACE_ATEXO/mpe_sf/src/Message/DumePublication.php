<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Message;

class DumePublication
{
    public function __construct(
        private readonly string $type,
        private readonly bool $isPublish = false,
        private readonly ?string $contexteLtDumeId = null
    ) {
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function isPublish(): bool
    {
        return $this->isPublish;
    }

    /**
     * @return string|null
     */
    public function getContexteLtDumeId(): ?string
    {
        return $this->contexteLtDumeId;
    }
}
