<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\OpenApi;
use ApiPlatform\Core\OpenApi\Model;

final class JwtDecorator implements OpenApiFactoryInterface
{
    public function __construct(
        private OpenApiFactoryInterface $decorated
    ) {
    }

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);
        $schemas = $openApi->getComponents()->getSchemas();

        $schemas['Token'] = new \ArrayObject([
                                                 'type' => 'object',
                                                 'properties' => [
                                                     'token' => [
                                                         'type' => 'string',
                                                         'readOnly' => true,
                                                     ],
                                                     'refresh_token' => [
                                                         'type' => 'string',
                                                         'readOnly' => true,
                                                     ],
                                                 ],
                                             ]);
        $schemas['Credentials'] = new \ArrayObject([
                                                       'type' => 'object',
                                                       'properties' => [
                                                           'login' => [
                                                               'type' => 'string',
                                                               'example' => 'admin_e3r',
                                                           ],
                                                           'password' => [
                                                               'type' => 'string',
                                                               'example' => 'apassword',
                                                           ],
                                                       ],
                                                   ]);
        $schemas['RefreshToken'] = new \ArrayObject([
                                                       'type' => 'object',
                                                       'properties' => [
                                                           'refresh_token' => [
                                                               'type' => 'string',
                                                               'example' => '72b9b1fa8fb6f0de7be0bea3208a...',
                                                           ],
                                                       ],
                                                   ]);

        $pathItem = new Model\PathItem(
            ref:  'JWT Token',
            post: new Model\Operation(
                operationId: 'postCredentialsItem',
                tags:        ['Token'],
                responses:   [
                               '200' => [
                                   'description' => 'Get JWT token',
                                   'content' => [
                                       'application/json' => [
                                           'schema' => [
                                               '$ref' => '#/components/schemas/Token',
                                           ],
                                       ],
                                   ],
                               ],
                               '401' => [
                                   'description' => 'Invalid credentials'
                               ],
                           ],
                summary:     'Get JWT token to login.',
                requestBody: new Model\RequestBody(
                    description: 'Generate new JWT Token',
                    content:     new \ArrayObject([
                                         'application/json' => [
                                             'schema' => [
                                                 '$ref' => '#/components/schemas/Credentials',
                                             ],
                                         ],
                                     ]),
                ),
            ),
        );
        $openApi->getPaths()->addPath('/api/v2/token', $pathItem);

        $pathItem = new Model\PathItem(
            ref:  'JWT Token',
            post: new Model\Operation(
                operationId: 'postCredentialsItemInscrit',
                tags:        ['Token'],
                responses:   [
                    '200' => [
                        'description' => 'Get JWT token for Inscrit',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/Token',
                                ],
                            ],
                        ],
                    ],
                    '401' => [
                        'description' => 'Invalid credentials'
                    ],
                ],
                summary:     'Get JWT token to login for admin.',
                requestBody: new Model\RequestBody(
                    description: 'Generate new JWT Token for admin',
                    content:     new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/Credentials',
                            ],
                        ],
                    ]),
                ),
            ),
        );
        $openApi->getPaths()->addPath('/api/v2/token-enterprise', $pathItem);

        $pathItem = new Model\PathItem(
            ref:  'JWT Token',
            post: new Model\Operation(
                operationId: 'postCredentialsItemAdmin',
                tags:        ['Token'],
                responses:   [
                    '200' => [
                        'description' => 'Get JWT token for admin',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/Token',
                                ],
                            ],
                        ],
                    ],
                    '401' => [
                        'description' => 'Invalid credentials'
                    ],
                ],
                summary:     'Get JWT token to login for admin.',
                requestBody: new Model\RequestBody(
                    description: 'Generate new JWT Token for admin',
                    content:     new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/Credentials',
                            ],
                        ],
                    ]),
                ),
            ),
        );
        $openApi->getPaths()->addPath('/api/v2/token-admin', $pathItem);

        $pathItem = new Model\PathItem(
            ref:  'Refresh JWT Token',
            post: new Model\Operation(
                operationId: 'postRefreshTokenItem',
                tags:        ['Token'],
                responses:   [
                               '200' => [
                                   'description' => 'Get new JWT token from refresh token',
                                   'content' => [
                                       'application/json' => [
                                           'schema' => [
                                               '$ref' => '#/components/schemas/Token',
                                           ],
                                       ],
                                   ],
                               ],
                               '401' => [
                                   'description' => 'Authentication exception occurred'
                               ],
                           ],
                summary:     'Refresh JWT token.',
                requestBody: new Model\RequestBody(
                    description: 'Generate new JWT Token from refresh token',
                    content:     new \ArrayObject([
                                         'application/json' => [
                                             'schema' => [
                                                 '$ref' => '#/components/schemas/RefreshToken',
                                             ],
                                         ],
                                     ]),
                ),
            ),
        );
        $openApi->getPaths()->addPath('/api/v2/token/refresh', $pathItem);

        return $openApi;
    }
}
