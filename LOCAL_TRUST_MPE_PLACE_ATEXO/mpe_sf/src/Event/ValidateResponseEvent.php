<?php

namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;
use App\Entity\Consultation;
use App\Entity\Inscrit;
use App\Entity\Offre as Response;

/**
 * Business event ==> Event to dispatch once a candidate valid his response.
 *
 * @author Mohamed BLAL
 *
 * Class ValidateResponseEvent
 */
class ValidateResponseEvent extends Event implements EventInterface
{
    public function __construct(private readonly Consultation $consultation, private readonly Response $response, private readonly Inscrit $candidate, private readonly int $idCandidatureMps)
    {
    }

    public function getConsultation()
    {
        return $this->consultation;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return Inscrit
     */
    public function getCandidate()
    {
        return $this->candidate;
    }

    /**
     * @return int
     */
    public function getIdCandidatureMps()
    {
        return $this->idCandidatureMps;
    }
}
