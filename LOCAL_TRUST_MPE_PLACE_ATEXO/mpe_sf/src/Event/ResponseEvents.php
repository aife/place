<?php

namespace App\Event;

/**
 * Mohamed BLAL.
 *
 * Class ResponseEvents
 */
final class ResponseEvents
{
    // events
    public const ON_RESPONSE_VALIDATION = 'response.validate';
    public const ON_PDF_ACKNOWLEDGMENT_GENERATION = 'response.pdfAR.generated';
    public const ON_DEPOTVALIDATION_SYNCHRONIZE_DOCUMENTS = 'depot.validate.synchronize.documents';

    // events callback
    public const CALLBACK = [
        self::ON_RESPONSE_VALIDATION => 'onValidateResponse',
        self::ON_PDF_ACKNOWLEDGMENT_GENERATION => 'onPdfAcknowledgmentGeneration',
        self::ON_DEPOTVALIDATION_SYNCHRONIZE_DOCUMENTS => 'onDepotValidationSynchronizeDocuments',
    ];
}
