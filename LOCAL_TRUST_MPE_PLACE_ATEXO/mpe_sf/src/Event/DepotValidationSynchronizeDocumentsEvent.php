<?php

namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;
use App\Entity\Consultation;

/**
 * Class DepotValidationSynchronizeDocumentsEvent evenement de synchronisation des documents SGMAP.
 *
 * @author    Oumar KONATE <oumar.konate@atexo.com>
 *
 * @since     ESR2016
 *
 * @copyright Atexo 2016
 */
class DepotValidationSynchronizeDocumentsEvent extends Event
{
    /**
     * DepotValidationSynchronizeDocumentsEvent constructor.
     *
     * @param $consultation
     */
    public function __construct(protected $consultation, protected int $idEntreprise, protected int $idEtablissement, protected int $idOffre)
    {
    }

    /**
     * @return Consultation
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    /**
     * @return int
     */
    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    /**
     * @return int
     */
    public function getIdEtablissement()
    {
        return $this->idEtablissement;
    }

    /**
     * @return int
     */
    public function getIdOffre()
    {
        return $this->idOffre;
    }

    /**
     * @param int $idOffre
     */
    public function setIdOffre($idOffre)
    {
        $this->idOffre = $idOffre;
    }
}
