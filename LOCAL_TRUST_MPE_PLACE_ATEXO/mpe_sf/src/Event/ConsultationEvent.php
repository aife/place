<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;

abstract class ConsultationEvent extends Event
{
    public function __construct(private readonly int $idConsultation, private readonly int $idExternal)
    {
    }

    public function getIdConsultation(): int
    {
        return $this->idConsultation;
    }

    public function getIdExternal(): int
    {
        return $this->idExternal;
    }
}
