<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Factory\Document;

use App\Entity\Complement;
use App\Entity\Consultation;

class AutrePiece extends UploadDocumentAbstract
{
    public function save(
        $agent,
        Consultation $consultation,
        $blobFileId,
        $fileSystemPath,
        $fileTimestamp,
        $fileName,
        $stockedFileName
    ): void {
        $oldAutresPieces = $this->retrieveLastSavedAutresPieces($consultation);
        $oldAutresPieces?->setStatut($this->parameterBag->get('REMPLACEMENT_FILE'));

        $autresPieces = new Complement();

        $autresPieces->setAgentId($agent->getId());
        $autresPieces->setConsultationId($consultation->getId());
        $autresPieces->setConsultation($consultation);
        $autresPieces->setComplement($blobFileId);

        $autresPieces->setHorodatage($fileTimestamp?->getJetonHorodatageBase64() ?? time());
        $autresPieces->setUntrusteddate((new \DateTime())->setTimestamp($fileTimestamp?->getHorodatage() ?? time()));

        $autresPieces->setOrganisme($consultation->getOrganisme());
        $autresPieces->setStatut($this->parameterBag->get('AJOUT_FILE'));
        $autresPieces->setNomFichier($fileName);

        $this->entityManager->persist($autresPieces);
        $this->entityManager->flush();
    }
}
