<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Factory\Document;

use App\Entity\Agent;
use App\Entity\Complement;
use App\Entity\Consultation;
use App\Entity\DCE;
use App\Entity\RG;
use App\Handler\UploadDocumentHandler;
use App\Service\AtexoFichierOrganisme;
use App\Service\AtexoUtil;
use App\Service\Consultation\UploadDceHelper;
use Atexo\CryptoBundle\AtexoCrypto;
use AtexoCrypto\Dto\InfosHorodatage;
use Doctrine\ORM\EntityManagerInterface;
use League\Flysystem\FileExistsException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

abstract class UploadDocumentAbstract
{
    private string $realFileName = "";
    private string $stockedFileName = "";

    public function __construct(
        protected readonly EntityManagerInterface $entityManager,
        protected readonly ParameterBagInterface $parameterBag,
        private readonly AtexoCrypto $cryptoService,
        protected readonly UploadDocumentHandler $uploadDocumentHandler,
        protected readonly AtexoFichierOrganisme $atexoBlobOrganisme,
        protected readonly LoggerInterface $logger,
        protected readonly LoggerInterface $cryptoLogger,
        protected readonly UploadDceHelper $uploadDceHelper,
        protected readonly AtexoUtil $atexoUtil
    ) {
    }

    /**
     * @throws FileExistsException
     * @throws \Exception
     */
    public function createOrUpdateDocument(
        Consultation $consultation,
        Agent $agent,
        array $request,
        string $documentType
    ): void {
        $path = null;

        if (array_key_exists('file', $request)) {
            $filePath = $this->createFileOnTmp($request);
        } else {
            if (array_key_exists('path', $request)) {
                $path = $request['path'];
            }

            if (!empty($request['name'])) {
                $filePath = $this->createEmptyZipPath($request['name']);
            } else {
                $filePath = $this->getFilePath($path, $consultation);
            }
        }

        if ($documentType === DocumentFactory::DOCUMENT_TYPE_DCE) {
            $this->manageFileDescriptor($request, $filePath);
        }

        $organism = $agent?->getOrganisme()?->getAcronyme() ?? $consultation?->getOrganisme()?->getAcronyme();

        $fileTimestamp = $this->fileTimestamp($filePath);

        $movedFilePath = $this->moveFileOnNasTmpDir($filePath);
        $this->stockedFileName = basename($movedFilePath);
        $blobFileId = $this->uploadDocumentHandler->createBlobFile(
            $this->realFileName,
            $movedFilePath,
            $organism,
            $documentType
        );

        $this->save(
            $agent,
            $consultation,
            $blobFileId,
            $movedFilePath,
            $fileTimestamp,
            $this->realFileName,
            $this->stockedFileName
        );
    }

    /**
     * @throws FileExistsException
     */
    private function getFilePath(?string $filePath, ?Consultation $consultation = null): string
    {
        if (!$filePath) {
            $lastDce = $this->retrieveLastSavedDce($consultation);
            $filePath = $this->uploadDceHelper->getFileByBlobId($lastDce->getDce());
            $this->realFileName = $lastDce->getNomFichier();
            $this->stockedFileName = basename($filePath);

            return $this->moveFileOnNasTmpDir($filePath);
        }
        $this->realFileName = basename($filePath);

        return $this->getAddedFilePath($filePath);
    }

    private function getAddedFilePath(?string $filePath): string
    {
        $fileTmpSystemPath = sprintf('/uploadFile/%s', $filePath);

        return sprintf('%s%s', $this->parameterBag->get('COMMON_TMP'), $fileTmpSystemPath);
    }

    private function createEmptyZipPath(string $name): string
    {
        $this->realFileName = $name;
        $targetDirectory = $this->parameterBag->get('COMMON_TMP') . '/uploadFile/' . uniqid();
        mkdir($targetDirectory, 0777, true);

        return "{$targetDirectory}/{$name}";
    }

    protected function retrieveLastSavedDce(Consultation $consultation): DCE|null
    {
        $lastDce = null;

        foreach ($consultation->getDce() as $dce) {
            if ($this->parameterBag->get('AJOUT_FILE') === $dce->getStatus()) {
                $lastDce = $dce;
            }
        }

        return $lastDce;
    }

    protected function retrieveLastSavedRC(Consultation $consultation): RG|null
    {
        $lastRC = null;

        foreach ($consultation->getRC() as $rc) {
            if ($this->parameterBag->get('AJOUT_FILE') === $rc->getStatut()) {
                $lastRC = $rc;
            }
        }

        return $lastRC;
    }
    protected function retrieveLastSavedAutresPieces(Consultation $consultation): Complement|null
    {
        $lastAutresPieces = null;

        foreach ($consultation->getAutresPieces() as $autresPiece) {
            /** @var Complement $autresPiece */
            if ($this->parameterBag->get('AJOUT_FILE') === $autresPiece->getStatut()) {
                $lastAutresPieces = $autresPiece;
                break;
            }
        }

        return $lastAutresPieces;
    }

    /**
     * @throws FileExistsException
     * @throws \Exception
     */
    private function moveFileOnNasTmpDir(string $fileSystemPathComplete): string
    {
        $movedFilePath = $this->uploadDceHelper->moveTmpFile($fileSystemPathComplete);

        if (false === $movedFilePath) {
            $this->logger->error('Impossible de déplacer le fichier ' . $fileSystemPathComplete);

            throw new \Exception('Impossible de déplacer le fichier ' . $fileSystemPathComplete);
        }

        return $movedFilePath;
    }

    /**
     * @throws \Exception
     */
    private function manageFileDescriptor(array $fileDescriptor, ?string $filePath): void
    {
        if (empty($filePath)) {
            throw new \Exception('filePath is required ');
        }

        $zipArchive = new \ZipArchive();
        $zipArchive->open($filePath, \ZipArchive::CREATE);

        for ($i = 0; $i < $zipArchive->numFiles; $i++) {
            $fname = $zipArchive->getNameIndex($i);
            $fname_new = $this->atexoUtil->replaceCharactersMsWordWithRegularCharacters($fname);
            $fname_new = iconv('UTF-8', 'ASCII//TRANSLIT', $fname_new);
            $zipArchive->renameIndex($i, $fname_new);
        }

        foreach ($fileDescriptor['added'] as $addedFile) {
            $this->addZipFile($zipArchive, $addedFile);
        }

        foreach ($fileDescriptor['removed'] as $removedFile) {
            $this->removeZipFile($zipArchive, $removedFile['fileRealPath']);
        }

        $zipArchive->close();
    }

    /**
     * @throws FileExistsException
     */
    private function addZipFile(\ZipArchive $zipArchive, array $filePath): void
    {
        if (!empty($filePath['fileRealPath'])) {
            $entryName = $filePath['fileRealPath'] . '/' . basename($filePath['fileTmpPath']);
        } else {
            $entryName = basename($filePath['fileTmpPath']);
        }
        $zipArchive->addFile($this->getAddedFilePath($filePath['fileTmpPath']), $entryName);
    }

    private function removeZipFile(\ZipArchive $zipArchive, string $filepath): void
    {
        $zipArchive->deleteName($filepath);
    }

    private function fileTimestamp(string $filePath): ?InfosHorodatage
    {
        try {
            return $this->cryptoService->demandeHorodatage(hash_file('sha256', $filePath));
        } catch (\Exception $exception) {
            $this->cryptoLogger->error(
                'Erreur lors de la demande d\'horodatage de DCE : ' . $filePath,
                [$exception->getMessage()]
            );
        }

        return null;
    }

    abstract public function save(
        $agent,
        Consultation $consultation,
        $blobFileId,
        $fileSystemPath,
        $fileTimestamp,
        $fileName,
        $stockedFileName
    );

    protected function createFileOnTmp(array $request): string
    {
        $this->realFileName = $request['name'];
        $targetDirectory = $this->parameterBag->get('COMMON_TMP') . '/uploadFile/' . uniqid();
        mkdir($targetDirectory, 0777, true);
        file_put_contents($targetDirectory . '/' . $request['name'], base64_decode($request['file']));

        return $targetDirectory . '/' . $request['name'];
    }
}
