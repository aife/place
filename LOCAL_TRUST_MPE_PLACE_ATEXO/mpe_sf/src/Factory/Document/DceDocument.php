<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Factory\Document;

use App\Entity\Consultation;
use App\Entity\DCE;

final class DceDocument extends UploadDocumentAbstract
{
    public function save(
        $agent,
        Consultation $consultation,
        $blobFileId,
        $fileSystemPath,
        $fileTimestamp,
        $fileName,
        $stockedFileName
    ): void {
        $oldDce = $this->retrieveLastSavedDce($consultation);
        $oldDce?->setStatus($this->parameterBag->get('REMPLACEMENT_FILE'));
        $dce = new DCE();

        $dce->setAgentId($agent->getId());
        $dce->setConsultationId($consultation->getId());
        $dce->setConsultation($consultation);
        $dce->setDce($blobFileId);

        $dce->setHorodatage($fileTimestamp?->getJetonHorodatageBase64() ?? time());
        $dce->setUntrusteddate((new \DateTime())->setTimestamp($fileTimestamp?->getHorodatage() ?? time()));

        $dce->setOrganisme($consultation->getOrganisme());
        $dce->setStatus($this->parameterBag->get('AJOUT_FILE'));
        $dce->setNomDce($fileName);
        $dce->setNomFichier($fileName);
        $dce->setTailleDce($this->atexoBlobOrganisme->getFileSize($blobFileId, $consultation->getOrganisme()));

        $this->entityManager->persist($dce);
        $this->entityManager->flush();
    }
}
