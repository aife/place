<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Factory\Document;

use App\Entity\Agent;
use App\Entity\Consultation;

interface DocumentFactoryInterface
{
    public function createOrUpdateDocumentByType(
        Consultation $consultation,
        Agent $agent,
        array $request,
        string $documentType
    );
}
