<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Factory\Document;

use App\Entity\Agent;
use App\Entity\Consultation;

final class DocumentFactory implements DocumentFactoryInterface
{
    public const DOCUMENT_TYPE_DCE = 'DCE';
    public const DOCUMENT_TYPE_RC = 'RC';
    public const DOCUMENT_TYPE_AUTRE_PIECE = 'AUTRE_PIECE';

    public function __construct(
        private readonly DceDocument $dceDocument,
        private readonly RcDocument $rcDocument,
        private readonly AutrePiece $autrePiece
    ) {
    }

    /**
     * @throws \Exception
     */
    public function createOrUpdateDocumentByType(
        Consultation $consultation,
        Agent $agent,
        array $request,
        string $documentType
    ): void {
        try {
            match ($documentType) {
                self::DOCUMENT_TYPE_DCE => $this->dceDocument->createOrUpdateDocument(
                    $consultation,
                    $agent,
                    $request,
                    self::DOCUMENT_TYPE_DCE
                ),
                self::DOCUMENT_TYPE_RC => $this->rcDocument->createOrUpdateDocument(
                    $consultation,
                    $agent,
                    $request,
                    self::DOCUMENT_TYPE_RC
                ),
                self::DOCUMENT_TYPE_AUTRE_PIECE => $this->autrePiece->createOrUpdateDocument(
                    $consultation,
                    $agent,
                    $request,
                    self::DOCUMENT_TYPE_AUTRE_PIECE
                ),
                default => throw new \InvalidArgumentException(sprintf('Invalid document type (%s)', $documentType)),
            };
        } catch (\Exception $exception) {
            throw new \Exception(
                sprintf('Error while creating document (%s)', $exception->getMessage())
            );
        }
    }
}
