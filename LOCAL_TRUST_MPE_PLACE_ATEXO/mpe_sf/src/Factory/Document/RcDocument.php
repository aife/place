<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Factory\Document;

use App\Entity\Consultation;
use App\Entity\RG;

class RcDocument extends UploadDocumentAbstract
{
    public function save(
        $agent,
        Consultation $consultation,
        $blobFileId,
        $fileSystemPath,
        $fileTimestamp,
        $fileName,
        $stockedFileName
    ): void {
        $oldRC = $this->retrieveLastSavedRC($consultation);
        $oldRC?->setStatut($this->parameterBag->get('REMPLACEMENT_FILE'));

        $rc = new RG();

        $rc->setAgentId($agent->getId());
        $rc->setConsultationId($consultation->getId());
        $rc->setConsultation($consultation);
        $rc->setRg($blobFileId);

        $rc->setHorodatage($fileTimestamp?->getJetonHorodatageBase64() ?? time());
        $rc->setUntrusteddate((new \DateTime())->setTimestamp($fileTimestamp?->getHorodatage() ?? time()));

        $rc->setOrganisme($consultation->getOrganisme());
        $rc->setStatut($this->parameterBag->get('AJOUT_FILE'));
        $rc->setNomFichier($fileName);

        $this->entityManager->persist($rc);
        $this->entityManager->flush();
    }
}
