<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Model;

class ConsultationForm
{
    public bool|ConsultationModule $allotissement;
    public bool|ConsultationModule $publicite;
    public bool|ConsultationModule $redac;
    public bool|ConsultationModule $cpv;
    public bool|ConsultationModule $dume;
    public bool|ConsultationModule $typeProcedureDume;
    public bool|ConsultationModule $formulaireDume;
    public bool|ConsultationModule $acces;
    public ConsultationModule $reglementConsultation;
    public ConsultationModule $autresPiecesConsultation;
    public ConsultationModule $dcePartialDownload;
    public bool $valeurEstimee;
    public bool $jo;
    public bool $lieuxExecution;
    public bool $codeNuts;
    public bool $clauses;
    public bool $justificationNonAlloti;
    public bool $groupBuyers;
    public bool $procedureOpen;
    public bool $referenceGeneree;
    public bool $adresseRetraisDossiers;
    public bool $adresseDepotOffres;
    public bool $lieuOuverturePlis;
    public bool $numeroProjetAchat;
    public bool $entiteAdjudicatrice;
    public bool $autresInformations;
    public bool $visiteLieuxReunion;
    public bool $attestationConsultation;
    public bool|ConsultationModule $accesConsultationEntrepriseApresDate;
}
