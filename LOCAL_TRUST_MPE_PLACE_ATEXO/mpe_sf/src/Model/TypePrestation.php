<?php

namespace App\Model;

class TypePrestation
{
    final const TYPE_PRESTATION_TRAVAUX = 'TRAVAUX';
    final const TYPE_PRESTATION_FOURNITURES = 'FOURNITURES';
    final const TYPE_PRESTATION_SERVICES = 'SERVICES';

    public final const TYPE_PRESTATIONS = [
        1 => self::TYPE_PRESTATION_TRAVAUX,
        2 => self::TYPE_PRESTATION_FOURNITURES,
        3 => self::TYPE_PRESTATION_SERVICES
    ];
}
