<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Model;

class ConsultationInput
{
    public bool $selected;
    public bool $display;
    public bool $fixed;
    public bool $mandatory;
    public ?array $parameters;
}
