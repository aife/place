<?php

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class ServiceModel
{
    public int $id;

    public $idExterne;

    public int $idParent;

    public $idExterneParent;

    /**
     * @Assert\NotBlank(message="Please enter a libelle")
     */
    public string $libelle;

    /**
     * @Assert\NotBlank(message="Please enter an organisme")
     */
    public string $organisme;

    public string $sigle;

    public string $email;

    /**
     * @Assert\NotBlank(message="Please enter a forme Juridique")
     */
    public string $formeJuridique;

    /**
     * @Assert\NotBlank(message="Please enter a sigle")
     */
    public string $formeJuridiqueCode;

    public $siren;

    public $complement;

    public $dateCreation;

    public $dateModification;

    public $idEntite;
}
