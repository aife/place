<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Model;

class ConsultationModule
{
    public ConsultationInput $yes;
    public ConsultationInput $no;
}
