<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Model;

class OrganismeModel
{
    public function __construct(
        public int $id,
        public string $name,
        public string $acronyme,
        public string $sigle,
        public string $serviceAccessible,
    ) {
        if (empty($this->name)) {
            $this->name = $this->acronyme;
        }
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id = 0): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAcronyme(): string
    {
        return $this->acronyme;
    }

    public function setAcronyme(string $acronyme): self
    {
        $this->acronyme = $acronyme;

        return $this;
    }

    public function getSigle(): string
    {
        return $this->sigle;
    }

    public function setSigle(string $sigle): self
    {
        $this->sigle = $sigle;

        return $this;
    }

    public function getServiceAccessible(): string
    {
        return $this->serviceAccessible;
    }

    public function setServiceAccessible(string $serviceAccessible): self
    {
        $this->serviceAccessible = $serviceAccessible;

        return $this;
    }
}
