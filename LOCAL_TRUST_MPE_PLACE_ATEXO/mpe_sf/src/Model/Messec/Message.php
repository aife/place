<?php

namespace App\Model\Messec;

/**
 * Class Message.
 */
class Message
{
    private $id;
    private $piecesJointes;
    private $identifiantPfEmetteur;
    private $identifiantPfDestinataire;
    private $nomPfEmetteur;
    private $nomPfDestinataire;
    private $urlPfEmetteur;
    private $urlPfDestinataire;
    private $urlPfEmetteurVisualisation;
    private $urlPfDestinataireVisualisation;
    private $urlPfReponse;
    private $identifiantObjetMetierPlateFormeDestinataire;
    private $identifiantObjetMetierPlateFormeEmetteur;
    private $identifiantSousObjetMetier;
    private $referenceObjetMetier;
    private $typeMessage;
    private $masquerOptionsEnvoi;
    private $cartouche;
    private $signatureAvisPassage;
    private $nomCompletExpediteur;
    private $emailExpediteur;
    private $emailReponseExpediteur;
    private $emailContactDestinatairePfEmetteur;
    private $idMsgInitial;
    private $contenu;
    private $contenuHTML;
    private $contenuText;
    private $objet;
    private $objetText;
    private $reponseAttendue;
    private $statut;
    private $typesInterdits;
    private $destsPfEmettreur;
    private $destsPfDestinataire;
    private $destinatairesPreSelectionnes;
    private $destinatairesBrouillon;
    private $emailsAlerteReponse;
    private $dateReponseAttendue;
    private $reponseLectureBloque;
    private $dateReponse;
    private $dateReponseAsDate;
    private $destinatairesNotifies;
    private $maxTailleFichiers;
    private $templateMail;
    private $templateFige;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return Message
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getPiecesJointes()
    {
        return $this->piecesJointes;
    }

    public function setPiecesJointes($piecesJointes)
    {
        $this->piecesJointes = $piecesJointes;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdentifiantPfEmetteur()
    {
        return $this->identifiantPfEmetteur;
    }

    /**
     * @param mixed $identifiantPfEmetteur
     *
     * @return Message
     */
    public function setIdentifiantPfEmetteur($identifiantPfEmetteur)
    {
        $this->identifiantPfEmetteur = $identifiantPfEmetteur;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdentifiantPfDestinataire()
    {
        return $this->identifiantPfDestinataire;
    }

    /**
     * @param mixed $identifiantPfDestinataire
     *
     * @return Message
     */
    public function setIdentifiantPfDestinataire($identifiantPfDestinataire)
    {
        $this->identifiantPfDestinataire = $identifiantPfDestinataire;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNomPfEmetteur()
    {
        return $this->nomPfEmetteur;
    }

    /**
     * @param mixed $nomPfEmetteur
     *
     * @return Message
     */
    public function setNomPfEmetteur($nomPfEmetteur)
    {
        $this->nomPfEmetteur = $nomPfEmetteur;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNomPfDestinataire()
    {
        return $this->nomPfDestinataire;
    }

    /**
     * @param mixed $nomPfDestinataire
     *
     * @return Message
     */
    public function setNomPfDestinataire($nomPfDestinataire)
    {
        $this->nomPfDestinataire = $nomPfDestinataire;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrlPfEmetteur()
    {
        return $this->urlPfEmetteur;
    }

    /**
     * @param mixed $urlPfEmetteur
     *
     * @return Message
     */
    public function setUrlPfEmetteur($urlPfEmetteur)
    {
        $this->urlPfEmetteur = $urlPfEmetteur;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrlPfDestinataire()
    {
        return $this->urlPfDestinataire;
    }

    /**
     * @param mixed $urlPfDestinataire
     *
     * @return Message
     */
    public function setUrlPfDestinataire($urlPfDestinataire)
    {
        $this->urlPfDestinataire = $urlPfDestinataire;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrlPfEmetteurVisualisation()
    {
        return $this->urlPfEmetteurVisualisation;
    }

    /**
     * @param mixed $urlPfEmetteurVisualisation
     *
     * @return Message
     */
    public function setUrlPfEmetteurVisualisation($urlPfEmetteurVisualisation)
    {
        $this->urlPfEmetteurVisualisation = $urlPfEmetteurVisualisation;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrlPfDestinataireVisualisation()
    {
        return $this->urlPfDestinataireVisualisation;
    }

    /**
     * @param mixed $urlPfDestinataireVisualisation
     *
     * @return Message
     */
    public function setUrlPfDestinataireVisualisation($urlPfDestinataireVisualisation)
    {
        $this->urlPfDestinataireVisualisation = $urlPfDestinataireVisualisation;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrlPfReponse()
    {
        return $this->urlPfReponse;
    }

    /**
     * @param mixed $urlPfReponse
     *
     * @return Message
     */
    public function setUrlPfReponse($urlPfReponse)
    {
        $this->urlPfReponse = $urlPfReponse;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdentifiantObjetMetierPlateFormeDestinataire()
    {
        return $this->identifiantObjetMetierPlateFormeDestinataire;
    }

    /**
     * @param mixed $identifiantObjetMetierPlateFormeDestinataire
     *
     * @return Message
     */
    public function setIdentifiantObjetMetierPlateFormeDestinataire($identifiantObjetMetierPlateFormeDestinataire)
    {
        $this->identifiantObjetMetierPlateFormeDestinataire = $identifiantObjetMetierPlateFormeDestinataire;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdentifiantObjetMetierPlateFormeEmetteur()
    {
        return $this->identifiantObjetMetierPlateFormeEmetteur;
    }

    /**
     * @param mixed $identifiantObjetMetierPlateFormeEmetteur
     *
     * @return Message
     */
    public function setIdentifiantObjetMetierPlateFormeEmetteur($identifiantObjetMetierPlateFormeEmetteur)
    {
        $this->identifiantObjetMetierPlateFormeEmetteur = $identifiantObjetMetierPlateFormeEmetteur;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdentifiantSousObjetMetier()
    {
        return $this->identifiantSousObjetMetier;
    }

    /**
     * @param mixed $identifiantSousObjetMetier
     *
     * @return Message
     */
    public function setIdentifiantSousObjetMetier($identifiantSousObjetMetier)
    {
        $this->identifiantSousObjetMetier = $identifiantSousObjetMetier;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getReferenceObjetMetier()
    {
        return $this->referenceObjetMetier;
    }

    /**
     * @param mixed $referenceObjetMetier
     *
     * @return Message
     */
    public function setReferenceObjetMetier($referenceObjetMetier)
    {
        $this->referenceObjetMetier = $referenceObjetMetier;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTypeMessage()
    {
        return $this->typeMessage;
    }

    /**
     * @param mixed $typeMessage
     *
     * @return Message
     */
    public function setTypeMessage($typeMessage)
    {
        $this->typeMessage = $typeMessage;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMasquerOptionsEnvoi()
    {
        return $this->masquerOptionsEnvoi;
    }

    /**
     * @param mixed $masquerOptionsEnvoi
     *
     * @return Message
     */
    public function setMasquerOptionsEnvoi($masquerOptionsEnvoi)
    {
        $this->masquerOptionsEnvoi = $masquerOptionsEnvoi;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCartouche()
    {
        return $this->cartouche;
    }

    /**
     * @param mixed $cartouche
     *
     * @return Message
     */
    public function setCartouche($cartouche)
    {
        $this->cartouche = $cartouche;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSignatureAvisPassage()
    {
        return $this->signatureAvisPassage;
    }

    /**
     * @param mixed $signatureAvisPassage
     *
     * @return Message
     */
    public function setSignatureAvisPassage($signatureAvisPassage)
    {
        $this->signatureAvisPassage = $signatureAvisPassage;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNomCompletExpediteur()
    {
        return $this->nomCompletExpediteur;
    }

    /**
     * @param mixed $nomCompletExpediteur
     *
     * @return Message
     */
    public function setNomCompletExpediteur($nomCompletExpediteur)
    {
        $this->nomCompletExpediteur = $nomCompletExpediteur;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmailExpediteur()
    {
        return $this->emailExpediteur;
    }

    /**
     * @param mixed $emailExpediteur
     *
     * @return Message
     */
    public function setEmailExpediteur($emailExpediteur)
    {
        $this->emailExpediteur = $emailExpediteur;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmailReponseExpediteur()
    {
        return $this->emailReponseExpediteur;
    }

    /**
     * @param mixed $emailReponseExpediteur
     *
     * @return Message
     */
    public function setEmailReponseExpediteur($emailReponseExpediteur)
    {
        $this->emailReponseExpediteur = $emailReponseExpediteur;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmailContactDestinatairePfEmetteur()
    {
        return $this->emailContactDestinatairePfEmetteur;
    }

    /**
     * @param mixed $emailContactDestinatairePfEmetteur
     *
     * @return Message
     */
    public function setEmailContactDestinatairePfEmetteur($emailContactDestinatairePfEmetteur)
    {
        $this->emailContactDestinatairePfEmetteur = $emailContactDestinatairePfEmetteur;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdMsgInitial()
    {
        return $this->idMsgInitial;
    }

    /**
     * @param mixed $idMsgInitial
     *
     * @return Message
     */
    public function setIdMsgInitial($idMsgInitial)
    {
        $this->idMsgInitial = $idMsgInitial;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * @param mixed $contenu
     *
     * @return Message
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContenuHTML()
    {
        return $this->contenuHTML;
    }

    /**
     * @param mixed $contenuHTML
     *
     * @return Message
     */
    public function setContenuHTML($contenuHTML)
    {
        $this->contenuHTML = $contenuHTML;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContenuText()
    {
        return $this->contenuText;
    }

    /**
     * @param mixed $contenuText
     *
     * @return Message
     */
    public function setContenuText($contenuText)
    {
        $this->contenuText = $contenuText;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getObjet()
    {
        return $this->objet;
    }

    /**
     * @param mixed $objet
     *
     * @return Message
     */
    public function setObjet($objet)
    {
        $this->objet = $objet;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getObjetText()
    {
        return $this->objetText;
    }

    /**
     * @param mixed $objetText
     *
     * @return Message
     */
    public function setObjetText($objetText)
    {
        $this->objetText = $objetText;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getReponseAttendue()
    {
        return $this->reponseAttendue;
    }

    /**
     * @param mixed $reponseAttendue
     *
     * @return Message
     */
    public function setReponseAttendue($reponseAttendue)
    {
        $this->reponseAttendue = $reponseAttendue;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @param mixed $statut
     *
     * @return Message
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTypesInterdits()
    {
        return $this->typesInterdits;
    }

    /**
     * @param mixed $typesInterdits
     *
     * @return Message
     */
    public function setTypesInterdits($typesInterdits)
    {
        $this->typesInterdits = $typesInterdits;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDestsPfEmettreur()
    {
        return $this->destsPfEmettreur;
    }

    /**
     * @param mixed $destsPfEmettreur
     *
     * @return Message
     */
    public function setDestsPfEmettreur($destsPfEmettreur)
    {
        $this->destsPfEmettreur = $destsPfEmettreur;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDestsPfDestinataire()
    {
        return $this->destsPfDestinataire;
    }

    /**
     * @param mixed $destsPfDestinataire
     *
     * @return Message
     */
    public function setDestsPfDestinataire($destsPfDestinataire)
    {
        $this->destsPfDestinataire = $destsPfDestinataire;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDestinatairesPreSelectionnes()
    {
        return $this->destinatairesPreSelectionnes;
    }

    /**
     * @param mixed $destinatairesPreSelectionnes
     *
     * @return Message
     */
    public function setDestinatairesPreSelectionnes($destinatairesPreSelectionnes)
    {
        $this->destinatairesPreSelectionnes = $destinatairesPreSelectionnes;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDestinatairesBrouillon()
    {
        return $this->destinatairesBrouillon;
    }

    /**
     * @param mixed $destinatairesBrouillon
     *
     * @return Message
     */
    public function setDestinatairesBrouillon($destinatairesBrouillon)
    {
        $this->destinatairesBrouillon = $destinatairesBrouillon;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmailsAlerteReponse()
    {
        return $this->emailsAlerteReponse;
    }

    /**
     * @param mixed $emailsAlerteReponse
     *
     * @return Message
     */
    public function setEmailsAlerteReponse($emailsAlerteReponse)
    {
        $this->emailsAlerteReponse = $emailsAlerteReponse;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateReponseAttendue()
    {
        return $this->dateReponseAttendue;
    }

    /**
     * @param mixed $dateReponseAttendue
     *
     * @return Message
     */
    public function setDateReponseAttendue($dateReponseAttendue)
    {
        $this->dateReponseAttendue = $dateReponseAttendue;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getReponseLectureBloque()
    {
        return $this->reponseLectureBloque;
    }

    /**
     * @param mixed $reponseLectureBloque
     *
     * @return Message
     */
    public function setReponseLectureBloque($reponseLectureBloque)
    {
        $this->reponseLectureBloque = $reponseLectureBloque;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateReponse()
    {
        return $this->dateReponse;
    }

    /**
     * @param mixed $dateReponse
     *
     * @return Message
     */
    public function setDateReponse($dateReponse)
    {
        $this->dateReponse = $dateReponse;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateReponseAsDate()
    {
        return $this->dateReponseAsDate;
    }

    /**
     * @param mixed $dateReponseAsDate
     *
     * @return Message
     */
    public function setDateReponseAsDate($dateReponseAsDate)
    {
        $this->dateReponseAsDate = $dateReponseAsDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDestinatairesNotifies()
    {
        return $this->destinatairesNotifies;
    }

    /**
     * @param mixed $destinatairesNotifies
     *
     * @return Message
     */
    public function setDestinatairesNotifies($destinatairesNotifies)
    {
        $this->destinatairesNotifies = $destinatairesNotifies;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMaxTailleFichiers()
    {
        return $this->maxTailleFichiers;
    }

    /**
     * @param mixed $maxTailleFichiers
     *
     * @return Message
     */
    public function setMaxTailleFichiers($maxTailleFichiers)
    {
        $this->maxTailleFichiers = $maxTailleFichiers;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTemplateMail()
    {
        return $this->templateMail;
    }

    /**
     * @param mixed $templateMail
     *
     * @return Message
     */
    public function setTemplateMail($templateMail)
    {
        $this->templateMail = $templateMail;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTemplateFige()
    {
        return $this->templateFige;
    }

    /**
     * @param mixed $templateFige
     *
     * @return Message
     */
    public function setTemplateFige($templateFige)
    {
        $this->templateFige = $templateFige;

        return $this;
    }
}
