<?php

namespace App\Model\Messec;

/**
 * Class Email.
 */
class Email
{
    private ?int $id = null;

    private ?Message $message = null;
    private $typeDestinataire;
    private $identifiantObjetMetier;
    private $identifiantEntreprise;
    private $identifiantContact;
    private $nomEntreprise;
    private $nomContact;
    private $email;
    private $statutEmail;
    private $codeLien;
    private $dateDemandeEnvoi;
    private $dateEnvoi;
    private $dateAR;
    private $nombreEssais;
    private $erreur;
    private $identifiantEntrepriseAR;
    private $identifiantContactAR;
    private $nomEntrepriseAR;
    private $nomContactAR;
    private $emailContactAR;
    private $reponse;
    private $toutesLesReponses;
    private $dateModificationBrouillon;
    private $connecte;
    private $dateRelance;
    private $dateEchec;
    private $favori;
    private $dateDemandeEnvoiAsDate;
    private $dateRelanceAsDate;
    private $dateEchecAsDate;
    private $dateEnvoiAsDate;
    private $dateARAsDate;
    private $dateModificationBrouillonAsDate;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Email
    {
        $this->id = $id;

        return $this;
    }

    public function getMessage(): Message
    {
        return $this->message;
    }

    public function setMessage(Message $message): Email
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTypeDestinataire()
    {
        return $this->typeDestinataire;
    }

    /**
     * @param mixed $typeDestinataire
     *
     * @return Email
     */
    public function setTypeDestinataire($typeDestinataire)
    {
        $this->typeDestinataire = $typeDestinataire;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdentifiantObjetMetier()
    {
        return $this->identifiantObjetMetier;
    }

    /**
     * @param mixed $identifiantObjetMetier
     *
     * @return Email
     */
    public function setIdentifiantObjetMetier($identifiantObjetMetier)
    {
        $this->identifiantObjetMetier = $identifiantObjetMetier;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdentifiantEntreprise()
    {
        return $this->identifiantEntreprise;
    }

    /**
     * @param mixed $identifiantEntreprise
     *
     * @return Email
     */
    public function setIdentifiantEntreprise($identifiantEntreprise)
    {
        $this->identifiantEntreprise = $identifiantEntreprise;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdentifiantContact()
    {
        return $this->identifiantContact;
    }

    /**
     * @param mixed $identifiantContact
     *
     * @return Email
     */
    public function setIdentifiantContact($identifiantContact)
    {
        $this->identifiantContact = $identifiantContact;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNomEntreprise()
    {
        return $this->nomEntreprise;
    }

    /**
     * @param mixed $nomEntreprise
     *
     * @return Email
     */
    public function setNomEntreprise($nomEntreprise)
    {
        $this->nomEntreprise = $nomEntreprise;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNomContact()
    {
        return $this->nomContact;
    }

    /**
     * @param mixed $nomContact
     *
     * @return Email
     */
    public function setNomContact($nomContact)
    {
        $this->nomContact = $nomContact;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     *
     * @return Email
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatutEmail()
    {
        return $this->statutEmail;
    }

    /**
     * @param mixed $statutEmail
     *
     * @return Email
     */
    public function setStatutEmail($statutEmail)
    {
        $this->statutEmail = $statutEmail;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeLien()
    {
        return $this->codeLien;
    }

    /**
     * @param mixed $codeLien
     *
     * @return Email
     */
    public function setCodeLien($codeLien)
    {
        $this->codeLien = $codeLien;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateDemandeEnvoi()
    {
        return $this->dateDemandeEnvoi;
    }

    /**
     * @param mixed $dateDemandeEnvoi
     *
     * @return Email
     */
    public function setDateDemandeEnvoi($dateDemandeEnvoi)
    {
        $this->dateDemandeEnvoi = $dateDemandeEnvoi;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateEnvoi()
    {
        return $this->dateEnvoi;
    }

    /**
     * @param mixed $dateEnvoi
     *
     * @return Email
     */
    public function setDateEnvoi($dateEnvoi)
    {
        $this->dateEnvoi = $dateEnvoi;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateAR()
    {
        return $this->dateAR;
    }

    /**
     * @param mixed $dateAR
     *
     * @return Email
     */
    public function setDateAR($dateAR)
    {
        $this->dateAR = $dateAR;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNombreEssais()
    {
        return $this->nombreEssais;
    }

    /**
     * @param mixed $nombreEssais
     *
     * @return Email
     */
    public function setNombreEssais($nombreEssais)
    {
        $this->nombreEssais = $nombreEssais;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getErreur()
    {
        return $this->erreur;
    }

    /**
     * @param mixed $erreur
     *
     * @return Email
     */
    public function setErreur($erreur)
    {
        $this->erreur = $erreur;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdentifiantEntrepriseAR()
    {
        return $this->identifiantEntrepriseAR;
    }

    /**
     * @param mixed $identifiantEntrepriseAR
     *
     * @return Email
     */
    public function setIdentifiantEntrepriseAR($identifiantEntrepriseAR)
    {
        $this->identifiantEntrepriseAR = $identifiantEntrepriseAR;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdentifiantContactAR()
    {
        return $this->identifiantContactAR;
    }

    /**
     * @param mixed $identifiantContactAR
     *
     * @return Email
     */
    public function setIdentifiantContactAR($identifiantContactAR)
    {
        $this->identifiantContactAR = $identifiantContactAR;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNomEntrepriseAR()
    {
        return $this->nomEntrepriseAR;
    }

    /**
     * @param mixed $nomEntrepriseAR
     *
     * @return Email
     */
    public function setNomEntrepriseAR($nomEntrepriseAR)
    {
        $this->nomEntrepriseAR = $nomEntrepriseAR;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNomContactAR()
    {
        return $this->nomContactAR;
    }

    /**
     * @param mixed $nomContactAR
     *
     * @return Email
     */
    public function setNomContactAR($nomContactAR)
    {
        $this->nomContactAR = $nomContactAR;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmailContactAR()
    {
        return $this->emailContactAR;
    }

    /**
     * @param mixed $emailContactAR
     *
     * @return Email
     */
    public function setEmailContactAR($emailContactAR)
    {
        $this->emailContactAR = $emailContactAR;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getReponse()
    {
        return $this->reponse;
    }

    /**
     * @param mixed $reponse
     *
     * @return Email
     */
    public function setReponse($reponse)
    {
        $this->reponse = $reponse;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getToutesLesReponses()
    {
        return $this->toutesLesReponses;
    }

    /**
     * @param mixed $toutesLesReponses
     *
     * @return Email
     */
    public function setToutesLesReponses($toutesLesReponses)
    {
        $this->toutesLesReponses = $toutesLesReponses;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateModificationBrouillon()
    {
        return $this->dateModificationBrouillon;
    }

    /**
     * @param mixed $dateModificationBrouillon
     *
     * @return Email
     */
    public function setDateModificationBrouillon($dateModificationBrouillon)
    {
        $this->dateModificationBrouillon = $dateModificationBrouillon;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getConnecte()
    {
        return $this->connecte;
    }

    /**
     * @param mixed $connecte
     *
     * @return Email
     */
    public function setConnecte($connecte)
    {
        $this->connecte = $connecte;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateRelance()
    {
        return $this->dateRelance;
    }

    /**
     * @param mixed $dateRelance
     *
     * @return Email
     */
    public function setDateRelance($dateRelance)
    {
        $this->dateRelance = $dateRelance;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateEchec()
    {
        return $this->dateEchec;
    }

    /**
     * @param mixed $dateEchec
     *
     * @return Email
     */
    public function setDateEchec($dateEchec)
    {
        $this->dateEchec = $dateEchec;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFavori()
    {
        return $this->favori;
    }

    /**
     * @param mixed $favori
     *
     * @return Email
     */
    public function setFavori($favori)
    {
        $this->favori = $favori;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateDemandeEnvoiAsDate()
    {
        return $this->dateDemandeEnvoiAsDate;
    }

    /**
     * @param mixed $dateDemandeEnvoiAsDate
     *
     * @return Email
     */
    public function setDateDemandeEnvoiAsDate($dateDemandeEnvoiAsDate)
    {
        $this->dateDemandeEnvoiAsDate = $dateDemandeEnvoiAsDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateRelanceAsDate()
    {
        return $this->dateRelanceAsDate;
    }

    /**
     * @param mixed $dateRelanceAsDate
     *
     * @return Email
     */
    public function setDateRelanceAsDate($dateRelanceAsDate)
    {
        $this->dateRelanceAsDate = $dateRelanceAsDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateEchecAsDate()
    {
        return $this->dateEchecAsDate;
    }

    /**
     * @param mixed $dateEchecAsDate
     *
     * @return Email
     */
    public function setDateEchecAsDate($dateEchecAsDate)
    {
        $this->dateEchecAsDate = $dateEchecAsDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateEnvoiAsDate()
    {
        return $this->dateEnvoiAsDate;
    }

    /**
     * @param mixed $dateEnvoiAsDate
     *
     * @return Email
     */
    public function setDateEnvoiAsDate($dateEnvoiAsDate)
    {
        $this->dateEnvoiAsDate = $dateEnvoiAsDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateARAsDate()
    {
        return $this->dateARAsDate;
    }

    /**
     * @param mixed $dateARAsDate
     *
     * @return Email
     */
    public function setDateARAsDate($dateARAsDate)
    {
        $this->dateARAsDate = $dateARAsDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateModificationBrouillonAsDate()
    {
        return $this->dateModificationBrouillonAsDate;
    }

    /**
     * @param mixed $dateModificationBrouillonAsDate
     *
     * @return Email
     */
    public function setDateModificationBrouillonAsDate($dateModificationBrouillonAsDate)
    {
        $this->dateModificationBrouillonAsDate = $dateModificationBrouillonAsDate;

        return $this;
    }
}
