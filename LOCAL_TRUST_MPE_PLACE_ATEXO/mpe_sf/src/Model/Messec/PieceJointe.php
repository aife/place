<?php

namespace App\Model\Messec;

/**
 * Class PieceJointe.
 */
class PieceJointe
{
    private $id;
    private $idExterne;
    private $reference;
    private $nom;
    private $chemin;
    private $taille;
    private $tailleFormatee;
    private $dateCreation;
    private $contentType;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return PieceJointe
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdExterne()
    {
        return $this->idExterne;
    }

    /**
     * @param mixed $idExterne
     *
     * @return PieceJointe
     */
    public function setIdExterne($idExterne)
    {
        $this->idExterne = $idExterne;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param mixed $reference
     *
     * @return PieceJointe
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     *
     * @return PieceJointe
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getChemin()
    {
        return $this->chemin;
    }

    /**
     * @param mixed $chemin
     *
     * @return PieceJointe
     */
    public function setChemin($chemin)
    {
        $this->chemin = $chemin;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTaille()
    {
        return $this->taille;
    }

    /**
     * @param mixed $taille
     *
     * @return PieceJointe
     */
    public function setTaille($taille)
    {
        $this->taille = $taille;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTailleFormatee()
    {
        return $this->tailleFormatee;
    }

    /**
     * @param mixed $tailleFormatee
     *
     * @return PieceJointe
     */
    public function setTailleFormatee($tailleFormatee)
    {
        $this->tailleFormatee = $tailleFormatee;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param mixed $dateCreation
     *
     * @return PieceJointe
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * @param mixed $contentType
     *
     * @return PieceJointe
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;

        return $this;
    }
}
