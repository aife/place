<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Model\DonneesEssentielles\Marche;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class representing DonneesComplementaires.
 */
class DonneesComplementaires
{
    /**
     * @property string $procedurePassation
     */
    private $procedurePassation = null;

    /**
     * @property string $naturePassation
     *
     * @Serializer\XmlElement(cdata=false)
     */
    private $naturePassation = null;

    /**
     * @property bool $accordCadreAvecMarcheSubsequent
     */
    private $accordCadreAvecMarcheSubsequent = null;

    /**
     * @property string $idAccordCadre
     */
    private $idAccordCadre = null;

    /**
     * @property string $siretPAAccordCadre
     */
    private $siretPAAccordCadre = null;

    /**
     * @property string $codesCPVSec
     *
     * @Serializer\XmlElement(cdata=false)
     */
    private $codesCPVSec = null;

    /**
     * @property int $nbTotalPropositionsRecu
     */
    private $nbTotalPropositionsRecu = null;

    /**
     * @property int $nbTotalPropositionsDemat
     */
    private $nbTotalPropositionsDemat = null;

    /**
     * @property bool $clauseEnvironnementale
     */
    private $clauseEnvironnementale = null;

    /**
     * @property bool $clauseSociale
     */
    private $clauseSociale = null;

    /**
     * @var \stdClass[] $clauses
     */
    private array $clauses = [];

    /**
     * @return null
     */
    public function getClauseSociale()
    {
        return $this->clauseSociale;
    }

    /**
     * @param $clauseSociale
     *
     * @return $this
     */
    public function setClauseSociale($clauseSociale)
    {
        $this->clauseSociale = $clauseSociale;

        return $this;
    }

    /**
     * @property string $ccagReference
     *
     * @Serializer\XmlElement(cdata=false)
     */
    private $ccagReference = null;

    /**
     * @property ConsultationAType $consultation
     */
    private $consultation = null;

    /**
     * @property LotAType $lot
     */
    private $lot = null;

    /**
     * @property NumerosContratType $numerosContratType
     */
    private $numerosContrat = null;

    /**
     * @property ContratsChapeauxType $contratsChapeaux
     */
    private $contratsChapeaux;

    /**
     * @property string $numeroEJ
     *
     * @Serializer\XmlElement(cdata=false)
     */
    private $numeroEJ = null;

    /**
     * @property \App\Model\DonneesEssentielles\Marche\AchatResponsableType $achatResponsable
     */
    private $achatResponsable = null;

    /**
     * @return mixed
     */
    public function getProcedurePassation()
    {
        return $this->procedurePassation;
    }

    /**
     * @param mixed $procedurePassation
     *
     * @return self
     */
    public function setProcedurePassation($procedurePassation)
    {
        $this->procedurePassation = $procedurePassation;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNaturePassation()
    {
        return $this->naturePassation;
    }

    /**
     * @param mixed $naturePassation
     *
     * @return self
     */
    public function setNaturePassation($naturePassation)
    {
        $this->naturePassation = $naturePassation;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAccordCadreAvecMarcheSubsequent()
    {
        return $this->accordCadreAvecMarcheSubsequent;
    }

    /**
     * @param mixed $accordCadreAvecMarcheSubsequent
     *
     * @return self
     */
    public function setAccordCadreAvecMarcheSubsequent($accordCadreAvecMarcheSubsequent)
    {
        $this->accordCadreAvecMarcheSubsequent = $accordCadreAvecMarcheSubsequent;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdAccordCadre()
    {
        return $this->idAccordCadre;
    }

    /**
     * @param mixed $idAccordCadre
     *
     * @return self
     */
    public function setIdAccordCadre($idAccordCadre)
    {
        $this->idAccordCadre = $idAccordCadre;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiretPAAccordCadre()
    {
        return $this->siretPAAccordCadre;
    }

    /**
     * @param mixed $siretPAAccordCadre
     *
     * @return self
     */
    public function setSiretPAAccordCadre($siretPAAccordCadre)
    {
        $this->siretPAAccordCadre = $siretPAAccordCadre;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodesCPVSec()
    {
        return $this->codesCPVSec;
    }

    /**
     * @param mixed $codesCPVSec
     *
     * @return self
     */
    public function setCodesCPVSec($codesCPVSec)
    {
        $this->codesCPVSec = $codesCPVSec;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNbTotalPropositionsRecu()
    {
        return $this->nbTotalPropositionsRecu;
    }

    /**
     * @param mixed $nbTotalPropositionsRecu
     *
     * @return self
     */
    public function setNbTotalPropositionsRecu($nbTotalPropositionsRecu)
    {
        $this->nbTotalPropositionsRecu = $nbTotalPropositionsRecu;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNbTotalPropositionsDemat()
    {
        return $this->nbTotalPropositionsDemat;
    }

    /**
     * @param mixed $nbTotalPropositionsDemat
     *
     * @return self
     */
    public function setNbTotalPropositionsDemat($nbTotalPropositionsDemat)
    {
        $this->nbTotalPropositionsDemat = $nbTotalPropositionsDemat;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClauseEnvironnementale()
    {
        return $this->clauseEnvironnementale;
    }

    /**
     * @param mixed $clauseEnvironnementale
     *
     * @return self
     */
    public function setClauseEnvironnementale($clauseEnvironnementale)
    {
        $this->clauseEnvironnementale = $clauseEnvironnementale;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCcagReference()
    {
        return $this->ccagReference;
    }

    /**
     * @param $ccagReference
     *
     * @return self
     */
    public function setCcagReference($ccagReference)
    {
        $this->ccagReference = $ccagReference;

        return $this;
    }

    /**
     * Gets as consultation.
     *
     * @return ConsultationAType
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    /**
     * Sets a new consultation.
     *
     * @return self
     */
    public function setConsultation(ConsultationAType $consultation)
    {
        $this->consultation = $consultation;

        return $this;
    }

    /**
     * @return LotAType
     */
    public function getLot()
    {
        return $this->lot;
    }

    /**
     * @param LotAType $lot
     * @return self
     */
    public function setLot($lot)
    {
        $this->lot = $lot;

        return $this;
    }

    /**
     * @return NumerosContratType
     */
    public function getNumerosContrat()
    {
        return $this->numerosContrat;
    }

    /**
     * @param NumerosContratType $numerosContrat
     */
    public function setNumerosContrat($numerosContrat)
    {
        $this->numerosContrat = $numerosContrat;

        return $this;
    }

    /**
     * @return ContratsChapeauxType
     */
    public function getContratsChapeaux()
    {
        return $this->contratsChapeaux;
    }

    /**
     * @param ContratsChapeauxType $contratsChapeaux
     */
    public function setContratsChapeaux($contratsChapeaux)
    {
        $this->contratsChapeaux = $contratsChapeaux;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumeroEJ()
    {
        return $this->numeroEJ;
    }

    /**
     * @param mixed $numeroEJ
     *
     * @return DonneesComplementaires
     */
    public function setNumeroEJ($numeroEJ)
    {
        $this->numeroEJ = $numeroEJ;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAchatResponsable()
    {
        return $this->achatResponsable;
    }

    /**
     * @param mixed $achatResponsable
     *
     * @return DonneesComplementaires
     */
    public function setAchatResponsable($achatResponsable)
    {
        $this->achatResponsable = $achatResponsable;

        return $this;
    }

    /**
     * @param \stdClass[] $clauses
     * @return $this
     */
    public function setClauses(array $clauses): self
    {
        $this->clauses = $clauses;

        return $this;
    }

    /**
     * @return \stdClass[]
     */
    public function getClauses(): array
    {
        return $this->clauses;
    }
}
