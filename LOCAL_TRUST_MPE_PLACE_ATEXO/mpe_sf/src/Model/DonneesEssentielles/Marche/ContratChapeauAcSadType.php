<?php

/**
 * Created by PhpStorm.
 * User: atexo
 * Date: 30/05/2019
 * Time: 10:52.
 */

namespace App\Model\DonneesEssentielles\Marche;

/**
 * Class representing ContratChapeauAcSadType.
 */
class ContratChapeauAcSadType
{
    /**
     * @property int $idTechniqueContratChapeauAcSad
     */
    private string $idTechniqueContratChapeauAcSad = '';

    /**
     * @property int $idContratChapeauAcSad
     */
    private string $idContratChapeauAcSad = '';

    /**
     * @property string $referenceLibreContratChapeauAcSad
     */
    private ?string $referenceLibreContratChapeauAcSad = '';

    /**
     * @property string $numeroCourtContratChapeauAcSad
     */
    private ?string $numeroCourtContratChapeauAcSad = '';

    /**
     * @property string $numeroLongContratChapeauAcSad
     */
    private ?string $numeroLongContratChapeauAcSad = '';

    /**
     * @return int
     */
    public function getIdTechniqueContratChapeauAcSad()
    {
        return $this->idTechniqueContratChapeauAcSad;
    }

    /**
     * @param int $idTechniqueContratChapeauAcSad
     */
    public function setIdTechniqueContratChapeauAcSad($idTechniqueContratChapeauAcSad)
    {
        $this->idTechniqueContratChapeauAcSad = $idTechniqueContratChapeauAcSad;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdContratChapeauAcSad()
    {
        return $this->idContratChapeauAcSad;
    }

    /**
     * @param int $idContratChapeauAcSad
     */
    public function setIdContratChapeauAcSad($idContratChapeauAcSad)
    {
        $this->idContratChapeauAcSad = $idContratChapeauAcSad;

        return $this;
    }

    /**
     * @return string
     */
    public function getReferenceLibreContratChapeauAcSad()
    {
        return $this->referenceLibreContratChapeauAcSad;
    }

    /**
     * @param string $referenceLibreContratChapeauAcSad
     */
    public function setReferenceLibreContratChapeauAcSad($referenceLibreContratChapeauAcSad)
    {
        $this->referenceLibreContratChapeauAcSad = $referenceLibreContratChapeauAcSad;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumeroCourtContratChapeauAcSad()
    {
        return $this->numeroCourtContratChapeauAcSad;
    }

    /**
     * @param string $numeroCourtContratChapeauAcSad
     */
    public function setNumeroCourtContratChapeauAcSad($numeroCourtContratChapeauAcSad)
    {
        $this->numeroCourtContratChapeauAcSad = $numeroCourtContratChapeauAcSad;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumeroLongContratChapeauAcSad()
    {
        return $this->numeroLongContratChapeauAcSad;
    }

    /**
     * @param string $numeroLongContratChapeauAcSad
     */
    public function setNumeroLongContratChapeauAcSad($numeroLongContratChapeauAcSad)
    {
        $this->numeroLongContratChapeauAcSad = $numeroLongContratChapeauAcSad;

        return $this;
    }
}
