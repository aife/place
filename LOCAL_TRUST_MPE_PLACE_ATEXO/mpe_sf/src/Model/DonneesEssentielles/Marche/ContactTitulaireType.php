<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Model\DonneesEssentielles\Marche;

class ContactTitulaireType
{
    private ?int $id = null;

    private ?string $nom = null;

    private ?string $prenom = null;

    private ?string $email = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): ContactTitulaireType
    {
        $this->id = $id;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): ContactTitulaireType
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): ContactTitulaireType
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): ContactTitulaireType
    {
        $this->email = $email;

        return $this;
    }
}
