<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Model\DonneesEssentielles\Marche;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class representing ConsultationAType.
 */
class ConsultationAType
{
    /**
     * @property string $id
     */
    private $id = null;

    /**
     * @property string $numero
     *
     * @Serializer\XmlElement(cdata=false)
     */
    private $numero = null;

    /**
     * @property string $codeExterne
     */
    private $codeExterne = null;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $numero
     *
     * @return self
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeExterne()
    {
        return $this->codeExterne;
    }

    /**
     * @param mixed $codeExterne
     *
     * @return ConsultationAType
     */
    public function setCodeExterne($codeExterne)
    {
        $this->codeExterne = $codeExterne;

        return $this;
    }
}
