<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Model\DonneesEssentielles\Marche;

use App\Model\DonneesEssentielles\Marche\TitulairesAType\TitulaireAType;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class representing TitulairesAType.
 */
class TitulairesAType
{
    /**
     * @property TitulaireAType[] $titulaire
     * @Serializer\XmlList(inline=true, entry="titulaire")*/
    private array $titulaire = [
    ];

    /**
     * @property string $typeIdentifiant
     *
     * @Serializer\XmlElement(cdata=false)
     */
    private $typeIdentifiant = null;

    /**
     * @property string $id
     */
    private $id = null;

    /**
     * @property string $denominationSociale
     */
    private $denominationSociale = null;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTypeIdentifiant()
    {
        return $this->typeIdentifiant;
    }

    /**
     * @param mixed $typeIdentifiant
     */
    public function setTypeIdentifiant($typeIdentifiant)
    {
        $this->typeIdentifiant = $typeIdentifiant;
    }

    /**
     * Adds as titulaire.
     *
     *@return self
     */
    public function addToTitulaire(TitulaireAType $titulaire)
    {
        $this->titulaire[] = $titulaire;

        return $this;
    }

    /**
     * isset titulaire.
     *
     *
     * @return bool
     */
    public function issetTitulaire(bool|string|int|float $index)
    {
        return isset($this->titulaire[$index]);
    }

    /**
     * unset titulaire.
     *
     *
     * @return void
     */
    public function unsetTitulaire(bool|string|int|float $index)
    {
        unset($this->titulaire[$index]);
    }

    /**
     * Gets as titulaire.
     *
     * @return TitulaireAType[]
     */
    public function getTitulaire()
    {
        return $this->titulaire;
    }

    /**
     * Sets a new titulaire.
     *
     * @param TitulaireAType[] $titulaire
     * @return self
     */
    public function setTitulaire(array $titulaire)
    {
        $this->titulaire = $titulaire;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDenominationSociale()
    {
        return $this->denominationSociale;
    }

    /**
     * @param mixed $denominationSociale
     */
    public function setDenominationSociale($denominationSociale)
    {
        $this->denominationSociale = $denominationSociale;
    }
}
