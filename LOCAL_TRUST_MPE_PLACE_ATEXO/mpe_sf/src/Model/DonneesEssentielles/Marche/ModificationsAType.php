<?php

namespace App\Model\DonneesEssentielles\Marche;

use App\Model\DonneesEssentielles\Marche\ModificationsAType\ModificationAType;
use DateTime;
use App\Model\DonneesEssentielles\Marche\ModificationsAType\ModificationAType\TitulairesAType\TitulaireAType;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class representing ModificationsAType.
 */
class ModificationsAType
{
    /**
     * @property int $id
     */
    private $id;

    /**
     * @property ModificationAType[] $modification
     *
     * @Serializer\XmlList(inline=true, entry="modification")*/
    private array $modification = [
    ];

    /**
     * @property string $objetModification
     */
    private $objetModification = null;

    /**
     * @property DateTime $dateSignatureModification
     */
    private $dateSignatureModification = null;

    /**
     * @property DateTime $datePublicationDonneesModification
     */
    private $datePublicationDonneesModification = null;

    /**
     * @property int $dureeMois
     */
    private $dureeMois = null;

    /**
     * @property float $montant
     */
    private $montant = null;

    /**
     * @property float $valeurGlobale
     */
    private $valeurGlobale = null;

    /**
     * @property TitulaireAType[] $titulaires
     *
     * @Serializer\XmlList(inline=true, entry="titulaires")*/
    private $titulaires = null;

    /**
     * @property float $modifications
     */
    public $modifications = null;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getModifications()
    {
        return $this->modifications;
    }

    /**
     * @param mixed $modifications
     */
    public function setModifications($modifications)
    {
        $this->modifications = $modifications;
    }

    /**
     * @return mixed
     */
    public function getTitulaires()
    {
        return $this->titulaires;
    }

    /**
     * @param mixed $titulaires
     */
    public function setTitulaires($titulaires)
    {
        $this->titulaires = $titulaires;
    }

    /**
     * @return mixed
     */
    public function getObjetModification()
    {
        return $this->objetModification;
    }

    /**
     * @param mixed $objetModification
     */
    public function setObjetModification($objetModification)
    {
        $this->objetModification = $objetModification;
    }

    /**
     * @return mixed
     */
    public function getDateSignatureModification()
    {
        return $this->dateSignatureModification;
    }

    /**
     * @param mixed $dateSignatureModification
     */
    public function setDateSignatureModification($dateSignatureModification)
    {
        $this->dateSignatureModification = $dateSignatureModification;
    }

    /**
     * @return mixed
     */
    public function getDatePublicationDonneesModification()
    {
        return $this->datePublicationDonneesModification;
    }

    /**
     * @param mixed $datePublicationDonneesModification
     */
    public function setDatePublicationDonneesModification($datePublicationDonneesModification)
    {
        $this->datePublicationDonneesModification = $datePublicationDonneesModification;
    }

    /**
     * @return mixed
     */
    public function getDureeMois()
    {
        return $this->dureeMois;
    }

    /**
     * @param mixed $dureeMois
     */
    public function setDureeMois($dureeMois)
    {
        $this->dureeMois = $dureeMois;
    }

    /**
     * @return mixed
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * @param mixed $montant
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;
    }

    /**
     * Adds as modification.
     *
     * @return self
     */
    public function addToModification(ModificationAType $modification)
    {
        $this->modification[] = $modification;

        return $this;
    }

    /**
     * isset modification.
     *
     *
     * @return bool
     */
    public function issetModification(bool|string|int|float $index)
    {
        return isset($this->modification[$index]);
    }

    /**
     * unset modification.
     *
     *
     * @return void
     */
    public function unsetModification(bool|string|int|float $index)
    {
        unset($this->modification[$index]);
    }

    /**
     * Gets as modification.
     *
     * @return ModificationAType[]
     */
    public function getModification()
    {
        return $this->modification;
    }

    /**
     * Sets a new modification.
     *
     * @param ModificationAType[] $modification
     *
     * @return self
     */
    public function setModification(array $modification)
    {
        $this->modification = $modification;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValeurGlobale()
    {
        return $this->valeurGlobale;
    }

    /**
     * @param mixed $valeurGlobale
     */
    public function setValeurGlobale($valeurGlobale)
    {
        $this->valeurGlobale = $valeurGlobale;
    }
}
