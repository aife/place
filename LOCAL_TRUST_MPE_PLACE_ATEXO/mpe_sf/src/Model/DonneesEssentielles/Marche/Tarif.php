<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Model\DonneesEssentielles\Marche;

use JMS\Serializer\Annotation as Serializer;

class Tarif
{
    /**
     * @Serializer\XmlElement(cdata=false)
     */
    private string $intituleTarif = 'intitule tarif';

    /**
     * @Serializer\XmlElement(cdata=false)
     */
    private string $tarif = 'tarif';

    /**
     * @return mixed
     */
    public function getIntituleTarif()
    {
        return $this->intituleTarif;
    }

    /**
     * @param mixed $intituleTarif
     */
    public function setIntituleTarif($intituleTarif)
    {
        $this->intituleTarif = $intituleTarif;
    }

    /**
     * @return mixed
     */
    public function getTarif()
    {
        return $this->tarif;
    }

    /**
     * @param mixed $tarif
     */
    public function setTarif($tarif)
    {
        $this->tarif = $tarif;
    }
}
