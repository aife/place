<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Model\DonneesEssentielles\Marche;

use JMS\Serializer\Annotation as Serializer;

class DonneeAnnuelle
{
    /**
     * @Serializer\XmlElement(cdata=false)
     */
    private string $datePublicationDonneesExecution = '10-10-2018';

    /**
     * @Serializer\XmlElement(cdata=false)
     */
    private $depensesInvestissement = null;

    private $tarifs = null;

    /**
     * @return null
     */
    public function getDepensesInvestissement()
    {
        return $this->depensesInvestissement;
    }

    /**
     * @param null $depensesInvestissement
     */
    public function setDepensesInvestissement($depensesInvestissement)
    {
        $this->depensesInvestissement = $depensesInvestissement;
    }

    /**
     * @return null
     */
    public function getTarifs()
    {
        return $this->tarifs;
    }

    /**
     * @param null $tarifs
     */
    public function setTarifs($tarifs)
    {
        $this->tarifs = $tarifs;
    }

    public function getDatePublicationDonneesExecution(): string
    {
        return $this->datePublicationDonneesExecution;
    }

    public function setDatePublicationDonneesExecution(string $datePublicationDonneesExecution)
    {
        $this->datePublicationDonneesExecution = $datePublicationDonneesExecution;
    }
}
