<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Model\DonneesEssentielles\Marche\AchatResponsable;

use JMS\Serializer\Annotation\XmlAttribute;

/**
 * Class representing ConsiderationsEnvironnementalesType.
 */
class ConsiderationsEnvironnementalesType
{
    /** @XmlAttribute */
    private $value = null;

    /**
     * @property ClausesEnvironnementalesSpecificationTechniqueType $clausesEnvironnementalesSpecificationTechnique
     */
    private $clausesEnvironnementalesSpecificationTechnique = null;

    /**
     * @property ClausesEnvironnementalesConditionExecutionType $clausesEnvironnementalesConditionExecution
     */
    private $clausesEnvironnementalesConditionExecution = null;

    /**
     * @property CriteresEnvironnementauxAttributionType $criteresEnvironnementauxAttribution
     */
    private $criteresEnvironnementauxAttribution = null;

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     *
     * @return ConsiderationsEnvironnementalesType
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClausesEnvironnementalesSpecificationTechnique()
    {
        return $this->clausesEnvironnementalesSpecificationTechnique;
    }

    /**
     * @param mixed $clausesEnvironnementalesSpecificationTechnique
     *
     * @return ConsiderationsEnvironnementalesType
     */
    public function setClausesEnvironnementalesSpecificationTechnique($clausesEnvironnementalesSpecificationTechnique)
    {
        $this->clausesEnvironnementalesSpecificationTechnique = $clausesEnvironnementalesSpecificationTechnique;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClausesEnvironnementalesConditionExecution()
    {
        return $this->clausesEnvironnementalesConditionExecution;
    }

    /**
     * @param mixed $clausesEnvironnementalesConditionExecution
     *
     * @return ConsiderationsEnvironnementalesType
     */
    public function setClausesEnvironnementalesConditionExecution($clausesEnvironnementalesConditionExecution)
    {
        $this->clausesEnvironnementalesConditionExecution = $clausesEnvironnementalesConditionExecution;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCriteresEnvironnementauxAttribution()
    {
        return $this->criteresEnvironnementauxAttribution;
    }

    /**
     * @param mixed $criteresEnvironnementauxAttribution
     *
     * @return ConsiderationsEnvironnementalesType
     */
    public function setCriteresEnvironnementauxAttribution($criteresEnvironnementauxAttribution)
    {
        $this->criteresEnvironnementauxAttribution = $criteresEnvironnementauxAttribution;

        return $this;
    }
}
