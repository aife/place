<?php

namespace App\Model\DonneesEssentielles\Marche\AchatResponsable;

/**
 * Class representing AchatResponsableType.
 */
class AchatResponsableType
{
    /**
     * @property ConsiderationsSocialesType $considerationsSociales
     */
    private $considerationsSociales = null;

    /**
     * @property ConsiderationsEnvironnementalesType $considerationsEnvironnementales
     */
    private $considerationsEnvironnementales = null;

    /**
     * @return mixed
     */
    public function getConsiderationsSociales()
    {
        return $this->considerationsSociales;
    }

    /**
     * @param mixed $considerationsSociales
     *
     * @return AchatResponsableType
     */
    public function setConsiderationsSociales($considerationsSociales)
    {
        $this->considerationsSociales = $considerationsSociales;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getConsiderationsEnvironnementales()
    {
        return $this->considerationsEnvironnementales;
    }

    /**
     * @param mixed $considerationsEnvironnementales
     *
     * @return AchatResponsableType
     */
    public function setConsiderationsEnvironnementales($considerationsEnvironnementales)
    {
        $this->considerationsEnvironnementales = $considerationsEnvironnementales;

        return $this;
    }
}
