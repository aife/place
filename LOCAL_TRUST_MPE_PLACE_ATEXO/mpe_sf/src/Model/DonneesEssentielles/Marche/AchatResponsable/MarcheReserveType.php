<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Model\DonneesEssentielles\Marche\AchatResponsable;

use JMS\Serializer\Annotation\XmlAttribute;

/**
 * Class representing MarcheReserveType.
 */
class MarcheReserveType
{
    /** @XmlAttribute */
    private $ESAT_EA = null;

    /** @XmlAttribute */
    private $SIAE = null;

    /** @XmlAttribute */
    private $EESS = null;

    /**
     * @return mixed
     */
    public function getESATEA()
    {
        return $this->ESAT_EA;
    }

    /**
     * @param mixed $ESAT_EA
     *
     * @return MarcheReserveType
     */
    public function setESATEA($ESAT_EA)
    {
        $this->ESAT_EA = $ESAT_EA;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSIAE()
    {
        return $this->SIAE;
    }

    /**
     * @param mixed $SIAE
     *
     * @return MarcheReserveType
     */
    public function setSIAE($SIAE)
    {
        $this->SIAE = $SIAE;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEESS()
    {
        return $this->EESS;
    }

    /**
     * @param mixed $EESS
     *
     * @return MarcheReserveType
     */
    public function setEESS($EESS)
    {
        $this->EESS = $EESS;

        return $this;
    }
}
