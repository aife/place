<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Model\DonneesEssentielles\Marche\AchatResponsable;

use JMS\Serializer\Annotation\XmlAttribute;

/**
 * Class representing ClausesEnvironnementalesSpecificationTechniqueType.
 */
class ClausesEnvironnementalesSpecificationTechniqueType
{
    public function __construct(
        /** @XmlAttribute */
        private $value = null
    )
    {
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     *
     * @return ClausesEnvironnementalesSpecificationTechniqueType
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }
}
