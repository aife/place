<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Model\DonneesEssentielles\Marche\AchatResponsable;

use JMS\Serializer\Annotation\XmlAttribute;

/**
 * Class representing ConsiderationsSocialesType.
 */
class ConsiderationsSocialesType
{
    /** @XmlAttribute */
    private $value = null;

    /**
     * @property ClausesSocialesConditionExecutionType $clausesSocialesConditionExecution
     */
    private $clausesSocialesConditionExecution = null;

    /**
     * @property ClausesSocialesSpecificationTechniqueType $clausesSocialesSpecificationTechnique
     */
    private $clausesSocialesSpecificationTechnique = null;

    /**
     * @property CriteresSociauxAttributionType $criteresSociauxAttribution
     */
    private $criteresSociauxAttribution = null;

    /**
     * @property MarcheReserveType $marcheReserve
     */
    private $marcheReserve = null;

    /**
     * @property ObjetInsertionType $objetInsertion
     */
    private $objetInsertion = null;

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     *
     * @return ConsiderationsSocialesType
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClausesSocialesConditionExecution()
    {
        return $this->clausesSocialesConditionExecution;
    }

    /**
     * @param mixed $clausesSocialesConditionExecution
     *
     * @return ConsiderationsSocialesType
     */
    public function setClausesSocialesConditionExecution($clausesSocialesConditionExecution)
    {
        $this->clausesSocialesConditionExecution = $clausesSocialesConditionExecution;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClausesSocialesSpecificationTechnique()
    {
        return $this->clausesSocialesSpecificationTechnique;
    }

    /**
     * @param mixed $clausesSocialesSpecificationTechnique
     *
     * @return ConsiderationsSocialesType
     */
    public function setClausesSocialesSpecificationTechnique($clausesSocialesSpecificationTechnique)
    {
        $this->clausesSocialesSpecificationTechnique = $clausesSocialesSpecificationTechnique;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCriteresSociauxAttribution()
    {
        return $this->criteresSociauxAttribution;
    }

    /**
     * @param mixed $criteresSociauxAttribution
     *
     * @return ConsiderationsSocialesType
     */
    public function setCriteresSociauxAttribution($criteresSociauxAttribution)
    {
        $this->criteresSociauxAttribution = $criteresSociauxAttribution;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMarcheReserve()
    {
        return $this->marcheReserve;
    }

    /**
     * @param mixed $marcheReserve
     *
     * @return ConsiderationsSocialesType
     */
    public function setMarcheReserve($marcheReserve)
    {
        $this->marcheReserve = $marcheReserve;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getObjetInsertion()
    {
        return $this->objetInsertion;
    }

    /**
     * @param mixed $objetInsertion
     *
     * @return ConsiderationsSocialesType
     */
    public function setObjetInsertion($objetInsertion)
    {
        $this->objetInsertion = $objetInsertion;

        return $this;
    }
}
