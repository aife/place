<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Model\DonneesEssentielles\Marche\AchatResponsable;

use JMS\Serializer\Annotation\XmlAttribute;
use JMS\Serializer\Annotation\XmlValue;

/**
 * Class representing ClausesSocialesConditionExecutionType.
 */
class ClausesSocialesConditionExecutionType
{
    /** @XmlAttribute */
    private $value = null;

    /** @XmlValue */
    private $content = null;

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     *
     * @return ClausesSocialesConditionExecutionType
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     *
     * @return ClausesSocialesConditionExecutionType
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }
}
