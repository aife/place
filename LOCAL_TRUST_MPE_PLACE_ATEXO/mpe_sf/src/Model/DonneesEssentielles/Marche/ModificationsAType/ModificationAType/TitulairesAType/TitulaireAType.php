<?php

namespace App\Model\DonneesEssentielles\Marche\ModificationsAType\ModificationAType\TitulairesAType;

/**
 * Class representing TitulaireAType.
 */
class TitulaireAType
{
    /**
     * @property string $typeIdentifiant
     */
    private $typeIdentifiant = null;

    /**
     * @property string $id
     */
    private $id = null;

    /**
     * @property string $denominationSociale
     */
    private $denominationSociale = null;

    /**
     * Gets as typeIdentifiant.
     *
     * @return string
     */
    public function getTypeIdentifiant()
    {
        return $this->typeIdentifiant;
    }

    /**
     * Sets a new typeIdentifiant.
     *
     * @param string $typeIdentifiant
     *
     * @return self
     */
    public function setTypeIdentifiant($typeIdentifiant)
    {
        $this->typeIdentifiant = $typeIdentifiant;

        return $this;
    }

    /**
     * Gets as id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets a new id.
     *
     * @param string $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets as denominationSociale.
     *
     * @return string
     */
    public function getDenominationSociale()
    {
        return $this->denominationSociale;
    }

    /**
     * Sets a new denominationSociale.
     *
     * @param string $denominationSociale
     *
     * @return self
     */
    public function setDenominationSociale($denominationSociale)
    {
        $this->denominationSociale = $denominationSociale;

        return $this;
    }
}
