<?php

namespace App\Model\DonneesEssentielles\Marche\ModificationsAType;

use DateTime;
use App\Model\DonneesEssentielles\Marche\ModificationsAType\ModificationAType\TitulairesAType\TitulaireAType;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class representing ModificationAType.
 */
class ModificationAType
{
    /**
     * @property int $id
     */
    private $id;

    /**
     * @property string $objetModification
     */
    private $objetModification = null;

    /**
     * @property DateTime $dateSignatureModification
     */
    private $dateSignatureModification = null;

    /**
     * @property DateTime $datePublicationDonneesModification
     */
    private $datePublicationDonneesModification = null;

    /**
     * @property int $dureeMois
     */
    private $dureeMois = null;

    /**
     * @property float $montant
     */
    private $montant = null;

    /**
     * @property float $valeurGlobale
     */
    private $valeurGlobale = null;

    /**
     * @return mixed
     */
    public function getModifications()
    {
        return $this->modifications;
    }

    /**
     * @param mixed $modifications
     */
    public function setModifications($modifications)
    {
        $this->modifications = $modifications;
    }

    /**
     * @property float $modifications
     */
    private $modifications = null;

    /**
     * @property TitulaireAType[] $titulaires
     *
     * @Serializer\XmlList(inline=true, entry="titulaires")*/
    private $titulaires = null;

    public function getId()
    {
        return $this->id;
    }

    /**
     * Gets as objetModification.
     *
     * @return string
     */
    public function getObjetModification()
    {
        return $this->objetModification;
    }

    /**
     * Sets a new objetModification.
     *
     * @param string $objetModification
     *
     * @return self
     */
    public function setObjetModification($objetModification)
    {
        $this->objetModification = $objetModification;

        return $this;
    }

    /**
     * Gets as dateSignatureModification.
     *
     * @return DateTime
     */
    public function getDateSignatureModification()
    {
        return $this->dateSignatureModification;
    }

    /**
     * Sets a new dateSignatureModification.
     *
     * @return self
     */
    public function setDateSignatureModification(DateTime $dateSignatureModification)
    {
        $this->dateSignatureModification = $dateSignatureModification;

        return $this;
    }

    /**
     * Gets as datePublicationDonneesModification.
     *
     * @return DateTime
     */
    public function getDatePublicationDonneesModification()
    {
        return $this->datePublicationDonneesModification;
    }

    /**
     * Sets a new datePublicationDonneesModification.
     *
     * @return self
     */
    public function setDatePublicationDonneesModification(DateTime $datePublicationDonneesModification)
    {
        $this->datePublicationDonneesModification = $datePublicationDonneesModification;

        return $this;
    }

    /**
     * Gets as dureeMois.
     *
     * @return int
     */
    public function getDureeMois()
    {
        return $this->dureeMois;
    }

    /**
     * Sets a new dureeMois.
     *
     * @param int $dureeMois
     *
     * @return self
     */
    public function setDureeMois($dureeMois)
    {
        $this->dureeMois = $dureeMois;

        return $this;
    }

    /**
     * Gets as montant.
     *
     * @return float
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Sets a new montant.
     *
     * @param float $montant
     *
     * @return self
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Adds as titulaire.
     *
     * @return self
     */
    public function addToTitulaires(TitulaireAType $titulaire)
    {
        $this->titulaires[] = $titulaire;

        return $this;
    }

    /**
     * isset titulaires.
     *
     *
     * @return bool
     */
    public function issetTitulaires(bool|string|int|float $index)
    {
        return isset($this->titulaires[$index]);
    }

    /**
     * unset titulaires.
     *
     *
     * @return void
     */
    public function unsetTitulaires(bool|string|int|float $index)
    {
        unset($this->titulaires[$index]);
    }

    /**
     * Gets as titulaires.
     *
     * @return TitulaireAType[]
     */
    public function getTitulaires()
    {
        return $this->titulaires;
    }

    /**
     * Sets a new titulaires.
     *
     * @param TitulaireAType[] $titulaires
     *
     * @return self
     */
    public function setTitulaires(array $titulaires)
    {
        $this->titulaires = $titulaires;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValeurGlobale()
    {
        return $this->valeurGlobale;
    }

    /**
     * @param mixed $valeurGlobale
     */
    public function setValeurGlobale($valeurGlobale)
    {
        $this->valeurGlobale = $valeurGlobale;
    }
}
