<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Model\DonneesEssentielles\Marche;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class representing LotAType.
 */
class LotAType
{
    /**
     * @property string $id
     *
     * @Serializer\XmlElement(cdata=false)
     */
    private $intituleLot = null;

    /**
     * @property string $numero
     */
    private $numeroLot = null;

    /**
     * @return mixed
     */
    public function getIntituleLot()
    {
        return $this->intituleLot;
    }

    /**
     * @param mixed $intituleLot
     *
     * @return self
     */
    public function setIntituleLot($intituleLot)
    {
        $this->intituleLot = $intituleLot;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumeroLot()
    {
        return $this->numeroLot;
    }

    /**
     * @param mixed $numeroLot
     *
     * @return self
     */
    public function setNumeroLot($numeroLot)
    {
        $this->numeroLot = $numeroLot;

        return $this;
    }
}
