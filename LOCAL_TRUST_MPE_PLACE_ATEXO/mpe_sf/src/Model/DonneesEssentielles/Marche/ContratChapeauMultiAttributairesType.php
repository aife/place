<?php

/**
 * Created by PhpStorm.
 * User: atexo
 * Date: 30/05/2019
 * Time: 10:50.
 */

namespace App\Model\DonneesEssentielles\Marche;

/**
 * Class representing ContratChapeauMultiAttributairesType.
 */
class ContratChapeauMultiAttributairesType
{
    /**
     * @property int $idTechniqueContratChapeauMultiAttributaires
     */
    private string $idTechniqueContratChapeauMultiAttributaires = '';

    /**
     * @property string $referenceLibreChapeauMultiAttributaires
     */
    private string $referenceLibreChapeauMultiAttributaires = '';

    /**
     * @property string $numeroCourtContratChapeauMultiAttributaires
     */
    private string $numeroCourtContratChapeauMultiAttributaires = '';

    /**
     * @return int
     */
    public function getIdTechniqueContratChapeauMultiAttributaires()
    {
        return $this->idTechniqueContratChapeauMultiAttributaires;
    }

    /**
     * @param int $idTechniqueContratChapeauMultiAttributaires
     */
    public function setIdTechniqueContratChapeauMultiAttributaires($idTechniqueContratChapeauMultiAttributaires)
    {
        $this->idTechniqueContratChapeauMultiAttributaires = $idTechniqueContratChapeauMultiAttributaires;

        return $this;
    }

    /**
     * @return string
     */
    public function getReferenceLibreChapeauMultiAttributaires()
    {
        return $this->referenceLibreChapeauMultiAttributaires;
    }

    /**
     * @param string $referenceLibreChapeauMultiAttributaires
     */
    public function setReferenceLibreChapeauMultiAttributaires($referenceLibreChapeauMultiAttributaires)
    {
        $this->referenceLibreChapeauMultiAttributaires = $referenceLibreChapeauMultiAttributaires;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumeroCourtContratChapeauMultiAttributaires()
    {
        return $this->numeroCourtContratChapeauMultiAttributaires;
    }

    /**
     * @param string $numeroCourtContratChapeauMultiAttributaires
     */
    public function setNumeroCourtContratChapeauMultiAttributaires($numeroCourtContratChapeauMultiAttributaires)
    {
        $this->numeroCourtContratChapeauMultiAttributaires = $numeroCourtContratChapeauMultiAttributaires;

        return $this;
    }
}
