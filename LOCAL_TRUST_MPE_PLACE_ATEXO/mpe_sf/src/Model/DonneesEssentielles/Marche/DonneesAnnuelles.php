<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Model\DonneesEssentielles\Marche;

use JMS\Serializer\Annotation as Serializer;

class DonneesAnnuelles
{
    /**
     * @Serializer\XmlList(inline=true, entry="donneesAnnuelles")*/
    private array $donneesAnnuelles = [];

    /**
     * @return int
     */
    public function getDonneesAnnuelle()
    {
        return $this->donneesAnnuelles;
    }

    /**
     * @param int $donneesAnnuelle
     */
    public function setDonneesAnnuelle($donneesAnnuelle)
    {
        $this->donneesAnnuelles[] = $donneesAnnuelle;
    }
}
