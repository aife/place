<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Model\DonneesEssentielles\Marche;

use JMS\Serializer\Annotation as Serializer;

class Tarifs
{
    /**
     * @Serializer\XmlList(inline=true, entry="tarif")*/
    private array $tarif = [];

    /**
     * @return null
     */
    public function getTarif()
    {
        return $this->tarif;
    }

    /**
     * @param null $tarif
     */
    public function setTarif($tarif)
    {
        $this->tarif[] = $tarif;
    }
}
