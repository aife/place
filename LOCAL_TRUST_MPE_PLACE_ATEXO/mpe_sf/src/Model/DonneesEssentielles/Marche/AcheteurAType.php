<?php

namespace App\Model\DonneesEssentielles\Marche;

/**
 * Class representing AcheteurAType.
 */
class AcheteurAType
{
    /**
     * @property string $id
     */
    private $id = null;

    /**
     * @property string $nom
     */
    private $nom = null;

    /**
     * @property int $idOrganisme
     */
    private $idOrganisme = null;

    /**
     * @property string $libelleOrganisme
     */
    private $libelleOrganisme = null;

    /**
     * @property string $idEntityAchat
     */
    private $idEntiteAchat = null;

    /**
     * @property string $libelleEntiteAchat
     */
    private $libelleEntiteAchat = null;

    /**
     * @property string $accesChorus
     */
    private $accesChorus = null;

    /**
     * @property int $idAgent
     */
    private $idAgent = null;

    /**
     * @property string $nomAgent
     */
    private $nomAgent = null;

    /**
     * @property string $prenomAgent
     */
    private $prenomAgent = null;

    /**
     * @property string $loginAgent
     */
    private $loginAgent = null;
    /**
     * @property string $emailAgent
     */
    private $emailAgent = null;

    private ?string $oldIdEntiteAchat = null;

    /**
     *
     * Gets as nom
     *
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     *
     * Sets a new nom
     *
     * @param string|null $nom
     * @return $this
     */
    public function setNom(?string $nom): self
    {
        $this->nom = $nom;
        return $this;
    }

    /**
     *
     * Gets as id.
     *
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     *
     * Sets a new id.
     *
     * @param string|null $id
     *
     * @return $this
     */
    public function setId(?string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getIdAgent(): ?int
    {
        return $this->idAgent;
    }

    /**
     * @return $this
     */
    public function setIdAgent(?int $idAgent): self
    {
        $this->idAgent = $idAgent;
        return $this;
    }

    public function getNomAgent(): ?string
    {
        return $this->nomAgent;
    }

    /**
     * @return $this
     */
    public function setNomAgent(?string $nomAgent): self
    {
        $this->nomAgent = $nomAgent;
        return $this;
    }

    public function getPrenomAgent(): ?string
    {
        return $this->prenomAgent;
    }

    /**
     * @return $this
     */
    public function setPrenomAgent(?string $prenomAgent): self
    {
        $this->prenomAgent = $prenomAgent;
        return $this;
    }

    public function getEmailAgent(): ?string
    {
        return $this->emailAgent;
    }

    /**
     * @return string
     */
    public function setEmailAgent(?string $emailAgent): self
    {
        $this->emailAgent = $emailAgent;
        return $this;
    }

    public function getLoginAgent(): ?string
    {
        return $this->loginAgent;
    }

    /**
     * @return $this
     */
    public function setLoginAgent(?string $loginAgent): self
    {
        $this->loginAgent = $loginAgent;
        return $this;
    }

    public function getLibelleOrganisme(): string
    {
        return $this->libelleOrganisme;
    }

    /**
     * @param string $organisme
     * @return $this
     */
    public function setLibelleOrganisme(?string $libelleOrganisme): self
    {
        $this->libelleOrganisme = $libelleOrganisme;
        return $this;
    }

    public function getIdOrganisme(): int
    {
        return $this->idOrganisme;
    }

    /**
     * @return $this
     */
    public function setIdOrganisme(int $idOrganisme): self
    {
        $this->idOrganisme = $idOrganisme;
        return $this;
    }

    /**
     *
     * Id can be empty, so we cast into a string
     * Gets as nom.
     *
     */
    public function getIdEntiteAchat(): null|string|int
    {
        return $this->idEntiteAchat;
    }

    /**
     *
     * Id can be empty, so we cast into a string
     *
     * @param null|string|int $idIdentiteAchat
     * @return $this
     */
    public function setIdEntiteAchat(null|string|int $idEntiteAchat): self
    {
        $this->idEntiteAchat = $idEntiteAchat;
        return $this;
    }

    public function getLibelleEntiteAchat(): ?string
    {
        return $this->libelleEntiteAchat;
    }

    /**
     * @return $this
     */
    public function setLibelleEntiteAchat(?string $libelleEntiteAchat): self
    {
        $this->libelleEntiteAchat = $libelleEntiteAchat;
        return $this;
    }

    public function getAccessChorus(): ?string
    {
        return $this->accesChorus;
    }

    /**
     * Sets a new nom.
     */
    public function setAccessChorus(?string $acessChorus): self
    {
        $this->accesChorus = $acessChorus;
        return $this;
    }

    public function getOldIdEntiteAchat(): ?string
    {
        return $this->oldIdEntiteAchat;
    }

    public function setOldIdEntiteAchat(?string $oldIdEntiteAchat): self
    {
        $this->oldIdEntiteAchat = $oldIdEntiteAchat;

        return $this;
    }
}
