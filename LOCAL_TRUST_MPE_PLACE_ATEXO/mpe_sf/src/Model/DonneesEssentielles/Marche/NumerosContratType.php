<?php

/**
 * Created by PhpStorm.
 * User: atexo
 * Date: 28/05/2019
 * Time: 10:13.
 */

namespace App\Model\DonneesEssentielles\Marche;

/**
 * Class representing NumerosContratType.
 */
class NumerosContratType
{
    /**
     * @property int $idTechnique
     */
    private int|string $idTechnique = '';

    /**
     * @property string $referenceLibre
     */
    private string $referenceLibre = '';

    /**
     * @property string $numeroCourt
     */
    private ?string $numeroCourt = '';

    /**
     * @property string $numeroLong
     */
    private ?string $numeroLong = '';

    /**
     * @return int
     */
    public function getIdTechnique()
    {
        return $this->idTechnique;
    }

    /**
     * @param int $idTechnique
     */
    public function setIdTechnique($idTechnique)
    {
        $this->idTechnique = $idTechnique;

        return $this;
    }

    /**
     * @return string
     */
    public function getReferenceLibre()
    {
        return $this->referenceLibre;
    }

    /**
     * @param string $referenceLibre
     */
    public function setReferenceLibre($referenceLibre)
    {
        $this->referenceLibre = $referenceLibre;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumeroCourt()
    {
        return $this->numeroCourt;
    }

    /**
     * @param string $numeroCourt
     */
    public function setNumeroCourt($numeroCourt)
    {
        $this->numeroCourt = $numeroCourt;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumeroLong()
    {
        return $this->numeroLong;
    }

    /**
     * @param string $numeroLong
     */
    public function setNumeroLong($numeroLong)
    {
        $this->numeroLong = $numeroLong;

        return $this;
    }
}
