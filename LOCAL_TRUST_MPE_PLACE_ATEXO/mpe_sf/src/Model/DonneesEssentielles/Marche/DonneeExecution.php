<?php

namespace App\Model\DonneesEssentielles\Marche;

class DonneeExecution
{
    private $donneesExecution = null;

    /**
     * @return null
     */
    public function getDonneeExecutions()
    {
        return $this->donneesExecution;
    }

    /**
     * @param null $donneeExecutions
     */
    public function setDonneeExecutions($donneeExecutions)
    {
        $this->donneesExecution = $donneeExecutions;
    }
}
