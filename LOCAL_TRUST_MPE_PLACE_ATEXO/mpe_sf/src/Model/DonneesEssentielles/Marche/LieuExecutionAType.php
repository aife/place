<?php

namespace App\Model\DonneesEssentielles\Marche;

/**
 * Class representing LieuExecutionAType.
 */
class LieuExecutionAType
{
    /**
     * @property int $id
     */
    private $id;

    /**
     * @property string $code
     */
    private $code = null;

    /**
     * @property string $typeCode
     */
    private $typeCode = null;

    /**
     * @property string $nom
     */
    private $nom = null;

    public function getId()
    {
        return $this->id;
    }

    /**
     * Gets as code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Sets a new code.
     *
     * @param string $code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Gets as typeCode.
     *
     * @return string
     */
    public function getTypeCode()
    {
        return $this->typeCode;
    }

    /**
     * Sets a new typeCode.
     *
     * @param string $typeCode
     *
     * @return self
     */
    public function setTypeCode($typeCode)
    {
        $this->typeCode = $typeCode;

        return $this;
    }

    /**
     * Gets as nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Sets a new nom.
     *
     * @param string $nom
     *
     * @return self
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }
}
