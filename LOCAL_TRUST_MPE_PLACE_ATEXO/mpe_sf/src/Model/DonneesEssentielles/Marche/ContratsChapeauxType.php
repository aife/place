<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

/**
 * Created by PhpStorm.
 * User: atexo
 * Date: 30/05/2019
 * Time: 10:50.
 */

namespace App\Model\DonneesEssentielles\Marche;

/**
 * Class representing ContratsChapeauxType.
 */
class ContratsChapeauxType
{
    /**
     * @property ContratChapeauMultiAttributairesType $contratChapeauMultiAttributaires
     */
    private $contratChapeauMultiAttributaires;

    /**
     * @property ContratChapeauAcSadType $contratChapeauAcSad
     */
    private $contratChapeauAcSad;

    /**
     * @return ContratChapeauMultiAttributairesType
     */
    public function getContratChapeauMultiAttributaires()
    {
        return $this->contratChapeauMultiAttributaires;
    }

    /**
     * @param ContratChapeauMultiAttributairesType $contratChapeauMultiAttributaires
     */
    public function setContratChapeauMultiAttributaires($contratChapeauMultiAttributaires)
    {
        $this->contratChapeauMultiAttributaires = $contratChapeauMultiAttributaires;

        return $this;
    }

    /**
     * @return ContratChapeauAcSadType
     */
    public function getContratChapeauAcSad()
    {
        return $this->contratChapeauAcSad;
    }

    /**
     * @param ContratChapeauAcSadType $contratChapeauAcSad
     */
    public function setContratChapeauAcSad($contratChapeauAcSad)
    {
        $this->contratChapeauAcSad = $contratChapeauAcSad;

        return $this;
    }
}
