<?php

namespace App\Model\DonneesEssentielles\Marche;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\XmlAttribute;

/**
 * Class EntiteEligible.
 */
class ContratTransverse
{
    /**
     * @property int $id
     */
    private $id;

    /** @XmlAttribute */
    private $value;

    /**
     * @property EntiteEligible $entiteEligible
     *
     * @Serializer\XmlList(inline=true, entry="entiteEligible")
     */
    private $entiteEligible = null;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getEntiteEligible()
    {
        return $this->entiteEligible;
    }

    /**
     * @param EntiteEligible $entitesEligibles
     */
    public function setEntiteEligible(array $entiteEligible)
    {
        $this->entiteEligible = $entiteEligible;

        return $this;
    }

    public function isValue(): bool
    {
        return $this->value;
    }

    public function setValue(bool $value)
    {
        $this->value = $value;

        return $this;
    }
}
