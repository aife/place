<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Model\DonneesEssentielles;

use DateTime;
use App\Model\DonneesEssentielles\Marche\DonneesComplementaires;
use App\Model\DonneesEssentielles\Marche\AcheteurAType;
use App\Model\DonneesEssentielles\Marche\ContratTransverse;
use App\Model\DonneesEssentielles\Marche\LieuExecutionAType;
use App\Model\DonneesEssentielles\Marche\ModificationsAType\ModificationAType;
use App\Model\DonneesEssentielles\Marche\TitulairesAType\TitulaireAType;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class representing Marche.
 *
 * @Serializer\XmlRoot("marche")
 */
class Marche
{
    /**
     * L'identifiant de contrat de concession est composé de trois parties:
     *  - l'année de notification (4 caractères),
     *  - le numéro d'ordre interne propre à l'acheteur public (1 à 10 caractères alphanumériques)
     *  - le numéro d'ordre de la modification (deux caractères, 00 si pas de modification, 01 si une modification, etc.).
     *
     *  Exemples :
     *  - 201872300 (2018 723 00)
     *  - 2019000111202 (2019 00001112 02)
     *
     * @property string $id
     */
    private $id = null;

    /**
     * @property AcheteurAType $acheteur
     */
    private $acheteur = null;

    /**
     * @property string $nature
     */
    private $nature = null;

    /**
     * @return mixed
     */
    public function getTypeContrat()
    {
        return $this->typeContrat;
    }

    /**
     * @param $typeContrat
     *
     * @return $this
     */
    public function setTypeContrat($typeContrat)
    {
        $this->typeContrat = $typeContrat;

        return $this;
    }

    /**
     * @property string $typeContrat
     *
     * @Serializer\XmlElement(cdata=false)
     */
    private $typeContrat = null;

    /**
     * @property string $objet
     */
    private $objet = null;

    /**
     * @property string $codeCPV
     */
    private $codeCPV = null;

    /**
     * @property string $procedure
     */
    private $procedure = null;

    /**
     * @property LieuExecutionAType $lieuExecution
     */
    private $lieuExecution = null;

    /**
     * @property int $dureeMois
     */
    private $dureeMois = null;

    /**
     * @property DateTime $dateNotification
     */
    private $dateNotification = null;

    /**
     * @property DateTime $datePublicationDonnees
     */
    private $datePublicationDonnees = null;

    /**
     * @property DateTime $dateTransmissionDonneesEtalab
     */
    private $dateTransmissionDonneesEtalab = null;

    /**
     * @property float $montant
     */
    private $montant = null;

    /**
     * @property float $valeurGlobale
     */
    private $valeurGlobale = null;

    /**
     * @property float $montantSubventionPublique
     */
    private $montantSubventionPublique = null;

    /**
     * @property string $formePrix
     */
    private $formePrix = null;

    private ?ContratTransverse $contratTransverse = null;

    /**
     * @return ContratTransverse
     */
    public function getContratTransverse()
    {
        return $this->contratTransverse;
    }

    /**
     * @param ContratTransverse $contratTransverse
     */
    public function setContratTransverse($contratTransverse)
    {
        $this->contratTransverse = $contratTransverse;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMontantSubventionPublique()
    {
        return $this->montantSubventionPublique;
    }

    /**
     * @param mixed $montantSubventionPublique
     */
    public function setMontantSubventionPublique($montantSubventionPublique)
    {
        $this->montantSubventionPublique = $montantSubventionPublique;
    }

    /**
     * @property TitulaireAType[] $titulaires
     * @Serializer\XmlList(inline=true)*/
    private $titulaires = null;

    /**
     * @property ModificationAType[] $modifications
     */
    private $modifications = null;

    /**
     * @property DonneesComplementaires $donneesComplementaires
     * @Serializer\XmlList(inline=true)*/
    private $donneesComplementaires = null;

    /**
     * @property string dateDebutExecution
     */
    private $dateDebutExecution = null;

    /**
     * @property string donneesExecution
     */
    private $donneesExecution = null;

    /**
     * @property string procedureType
     */
    private $procedureType = null;

    /**
     * @property string modification
     */
    private $modification = null;

    /**
     * @property string tarifType
     */
    private $tarifType = null;

    /**
     * @property string donneesAnnuellesType
     */
    private $donneesAnnuellesType = null;

    /**
     * @property string $dateSignature
     *
     * @Serializer\XmlElement(cdata=false)
     */
    private $dateSignature = null;

    /**
     * @property string $uuid
     *
     * @Serializer\XmlElement(cdata=false)
     */
    private $uuid = null;

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param $uuid
     *
     * @return $this
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDonneesExecution()
    {
        return $this->donneesExecution;
    }

    /**
     * @param mixed $donneesExecution
     */
    public function setDonneesExecution($donneesExecution)
    {
        $this->donneesExecution = $donneesExecution;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProcedureType()
    {
        return $this->procedureType;
    }

    /**
     * @param mixed $procedureType
     */
    public function setProcedureType($procedureType)
    {
        $this->procedureType = $procedureType;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getModification()
    {
        return $this->modification;
    }

    /**
     * @param mixed $modification
     */
    public function setModification($modification)
    {
        $this->modification = $modification;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTarifType()
    {
        return $this->tarifType;
    }

    /**
     * @param mixed $tarifType
     */
    public function setTarifType($tarifType)
    {
        $this->tarifType = $tarifType;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDonneesAnnuellesType()
    {
        return $this->donneesAnnuellesType;
    }

    /**
     * @param mixed $donneesAnnuellesType
     */
    public function setDonneesAnnuellesType($donneesAnnuellesType)
    {
        $this->donneesAnnuellesType = $donneesAnnuellesType;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateDebutExecution()
    {
        return $this->dateDebutExecution;
    }

    /**
     * @param mixed $dateDebutExecution
     */
    public function setDateDebutExecution($dateDebutExecution)
    {
        $this->dateDebutExecution = $dateDebutExecution;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateSignature()
    {
        return $this->dateSignature;
    }

    /**
     * @param mixed $dateSignature
     *
     * @return self
     */
    public function setDateSignature($dateSignature)
    {
        $this->dateSignature = $dateSignature;

        return $this;
    }

    /**
     * Gets as id.
     *
     * L'identifiant de contrat de concession est composé de trois parties:
     *  - l'année de notification (4 caractères),
     *  - le numéro d'ordre interne propre à l'acheteur public (1 à 10 caractères alphanumériques)
     *  - le numéro d'ordre de la modification (deux caractères, 00 si pas de modification, 01 si une modification, etc.)
     *
     *  Exemples :
     *  - 201872300 (2018 723 00)
     *  - 2019000111202 (2019 00001112 02)
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets a new id.
     *
     * L'identifiant de contrat de concession est composé de trois parties:
     *  - l'année de notification (4 caractères),
     *  - le numéro d'ordre interne propre à l'acheteur public (1 à 10 caractères alphanumériques)
     *  - le numéro d'ordre de la modification (deux caractères, 00 si pas de modification, 01 si une modification, etc.)
     *
     *  Exemples :
     *  - 201872300 (2018 723 00)
     *  - 2019000111202 (2019 00001112 02)
     *
     * @param string $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets as acheteur.
     *
     * @return AcheteurAType
     */
    public function getAcheteur()
    {
        return $this->acheteur;
    }

    /**
     * Sets a new acheteur.
     *
     * @return self
     */
    public function setAcheteur(AcheteurAType $acheteur)
    {
        $this->acheteur = $acheteur;

        return $this;
    }

    /**
     * Gets as nature.
     *
     * @return string
     */
    public function getNature()
    {
        return $this->nature;
    }

    /**
     * Sets a new nature.
     *
     * @param string $nature
     *
     * @return self
     */
    public function setNature($nature)
    {
        $this->nature = $nature;

        return $this;
    }

    /**
     * Gets as objet.
     *
     * @return string
     */
    public function getObjet()
    {
        return $this->objet;
    }

    /**
     * Sets a new objet.
     *
     * @param string $objet
     *
     * @return self
     */
    public function setObjet($objet)
    {
        $this->objet = $objet;

        return $this;
    }

    /**
     * Gets as codeCPV.
     *
     * @return string
     */
    public function getCodeCPV()
    {
        return $this->codeCPV;
    }

    /**
     * Sets a new codeCPV.
     *
     * @param string $codeCPV
     *
     * @return self
     */
    public function setCodeCPV($codeCPV)
    {
        $this->codeCPV = $codeCPV;

        return $this;
    }

    /**
     * Gets as procedure.
     *
     * @return string
     */
    public function getProcedure()
    {
        return $this->procedure;
    }

    /**
     * Sets a new procedure.
     *
     * @param string $procedure
     *
     * @return self
     */
    public function setProcedure($procedure)
    {
        $this->procedure = $procedure;

        return $this;
    }

    /**
     * Gets as lieuExecution.
     *
     * @return LieuExecutionAType
     */
    public function getLieuExecution()
    {
        return $this->lieuExecution;
    }

    /**
     * Sets a new lieuExecution.
     *
     * @return self
     */
    public function setLieuExecution(LieuExecutionAType $lieuExecution)
    {
        $this->lieuExecution = $lieuExecution;

        return $this;
    }

    /**
     * Gets as dureeMois.
     *
     * @return int
     */
    public function getDureeMois()
    {
        return $this->dureeMois;
    }

    /**
     * Sets a new dureeMois.
     *
     * @param int $dureeMois
     *
     * @return self
     */
    public function setDureeMois($dureeMois)
    {
        $this->dureeMois = $dureeMois;

        return $this;
    }

    /**
     * Gets as dateNotification.
     *
     * @return DateTime
     */
    public function getDateNotification()
    {
        return $this->dateNotification;
    }

    /**
     * Sets a new dateNotification.
     *
     * @return self
     */
    public function setDateNotification(DateTime $dateNotification)
    {
        $this->dateNotification = $dateNotification;

        return $this;
    }

    /**
     * Gets as datePublicationDonnees.
     *
     * @return DateTime
     */
    public function getDatePublicationDonnees()
    {
        return $this->datePublicationDonnees;
    }

    /**
     * Sets a new datePublicationDonnees.
     *
     * @return self
     */
    public function setDatePublicationDonnees(DateTime $datePublicationDonnees)
    {
        $this->datePublicationDonnees = $datePublicationDonnees;

        return $this;
    }

    /**
     * Gets as dateTransmissionDonneesEtalab.
     *
     * @return DateTime
     */
    public function getDateTransmissionDonneesEtalab()
    {
        return $this->dateTransmissionDonneesEtalab;
    }

    /**
     * Sets a new dateTransmissionDonneesEtalab.
     *
     * @return self
     */
    public function setDateTransmissionDonneesEtalab(DateTime $dateTransmissionDonneesEtalab)
    {
        $this->dateTransmissionDonneesEtalab = $dateTransmissionDonneesEtalab;

        return $this;
    }

    /**
     * Gets as montant.
     *
     * @return float
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Sets a new montant.
     *
     * @param float $montant
     *
     * @return self
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Gets as formePrix.
     *
     * @return string
     */
    public function getFormePrix()
    {
        return $this->formePrix;
    }

    /**
     * Sets a new formePrix.
     *
     * @param string $formePrix
     *
     * @return self
     */
    public function setFormePrix($formePrix)
    {
        $this->formePrix = $formePrix;

        return $this;
    }

    /**
     * Adds as titulaire.
     *
     * @return self
     */
    public function addToTitulaires(TitulaireAType $titulaire)
    {
        $this->titulaires[] = $titulaire;

        return $this;
    }

    /**
     * isset titulaires.
     *
     *
     * @return bool
     */
    public function issetTitulaires(bool|string|int|float $index)
    {
        return isset($this->titulaires[$index]);
    }

    /**
     * unset titulaires.
     *
     *
     * @return void
     */
    public function unsetTitulaires(bool|string|int|float $index)
    {
        unset($this->titulaires[$index]);
    }

    /**
     * Gets as titulaires.
     *
     * @return TitulaireAType[]
     */
    public function getTitulaires()
    {
        return $this->titulaires;
    }

    /**
     * Sets a new titulaires.
     *
     * @param TitulaireAType[] $titulaires
     * @return self
     */
    public function setTitulaires(array $titulaires)
    {
        $this->titulaires = $titulaires;

        return $this;
    }

    /**
     * Adds as modification.
     *
     * @return self
     */
    public function addToModifications(ModificationAType $modification)
    {
        $this->modifications[] = $modification;

        return $this;
    }

    /**
     * isset modifications.
     *
     *
     * @return bool
     */
    public function issetModifications(bool|string|int|float $index)
    {
        return isset($this->modifications[$index]);
    }

    /**
     * unset modifications.
     *
     *
     * @return void
     */
    public function unsetModifications(bool|string|int|float $index)
    {
        unset($this->modifications[$index]);
    }

    /**
     * Gets as modifications.
     *
     * @return ModificationAType[]
     */
    public function getModifications()
    {
        return $this->modifications;
    }

    /**
     * Sets a new modifications.
     *
     * @param ModificationAType[] $modifications
     *
     * @return self
     */
    public function setModifications(array $modifications)
    {
        $this->modifications = $modifications;

        return $this;
    }

    /**
     * Gets as donneesComplementaires.
     *
     * @return DonneesComplementaires
     */
    public function getDonneesComplementaires()
    {
        return $this->donneesComplementaires;
    }

    /**
     * Sets a new donneesComplementaires.
     *
     * @return self
     */
    public function setDonneesComplementaires(DonneesComplementaires $donneesComplementaires)
    {
        $this->donneesComplementaires = $donneesComplementaires;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValeurGlobale()
    {
        return $this->valeurGlobale;
    }

    /**
     * @param mixed $valeurGlobale
     */
    public function setValeurGlobale($valeurGlobale)
    {
        $this->valeurGlobale = $valeurGlobale;
    }
}
