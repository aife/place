<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Model\DonneesEssentielles;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Marches.
 * @Serializer\XmlRoot("marches")
 */
class Marches
{
    /**
     * Marches constructor.
     * @param Marche[] $marches*/
    public function __construct(
        /**
         * @Serializer\XmlList(inline=true, entry = "marche")
         */
        private array $marches
    )
    {
    }

    /**
     * @return Marche[]
     */
    public function getMarches()
    {
        return $this->marches;
    }

    /**
     * @param Marche $marche
     */
    public function addMarche($marche)
    {
        $this->marches[] = $marche;
    }

    /**
     * @param Marche[] $marches
     */
    public function setMarches($marches)
    {
        $this->marches = $marches;
    }
}
