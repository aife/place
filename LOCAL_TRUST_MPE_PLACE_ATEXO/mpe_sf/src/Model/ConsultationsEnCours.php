<?php

namespace App\Model;

/**
 * Class ConsultationsEnCours.
 */
class ConsultationsEnCours
{
    public $elaboration;

    public $attenteValidation;

    public $consultation;

    public $ouvertureAnalyse;

    public $decision;

    public function __construct($consultationEnCours, public $url)
    {
        $this->elaboration = $consultationEnCours['elaboration'];
        $this->attenteValidation = $consultationEnCours['attenteValidation'];
        $this->consultation = $consultationEnCours['consultation'];
        $this->ouvertureAnalyse = $consultationEnCours['ouvertureAnalyse'];
        $this->decision = $consultationEnCours['decision'];
    }
}
