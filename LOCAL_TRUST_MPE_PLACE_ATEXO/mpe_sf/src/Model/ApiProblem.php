<?php

namespace App\Model;

use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;

/**
 * A wrapper for holding data to be used for a application/problem+json response.
 */
class ApiProblem
{
    final const TYPE_VALIDATION_ERROR = 'validation_error';
    final const TYPE_INVALID_REQUEST_BODY_FORMAT = 'invalid_body_format';
    final const TYPE_INVALID_TOKEN = 'invalid_token';
    final const TYPE_INVALID_SSO = 'invalid_sso';
    final const TYPE_ERROR_DB = 'error_db';
    final const TYPE_ERROR_INTERNAL = 'internal';
    final const TYPE_NOT_FOUND = 'not_found';
    final const TYPE_INVALID_ARGUMENT = 'invalid_argument';
    final const TYPE_XSD_ERROR = 'xsd_error';
    final const TYPE_OBJECT_ALREADY_EXIST = 'object_already_exist';
    final const TYPE_UNAUTHORIZED = 'unauthorized';
    final const TYPE_FORBIDDEN = 'forbidden';
    final const TYPE_ORGANISME_POOL = 'organisme_pool';
    private static array $titles = [
        self::TYPE_VALIDATION_ERROR => 'There was a validation error',
        self::TYPE_INVALID_REQUEST_BODY_FORMAT => 'Invalid format sent',
        self::TYPE_INVALID_TOKEN => 'Invalid token sent',
        self::TYPE_INVALID_SSO => 'Invalid sso sent',
        self::TYPE_ERROR_DB => 'Internal error from DB issue',
        self::TYPE_NOT_FOUND => 'Object not found',
        self::TYPE_INVALID_ARGUMENT => 'Invalid argument',
        self::TYPE_XSD_ERROR => 'Xsd error',
        self::TYPE_OBJECT_ALREADY_EXIST => 'object already exist',
        self::TYPE_UNAUTHORIZED => 'unauthorized',
        self::TYPE_ERROR_INTERNAL => 'Internal error',
        self::TYPE_FORBIDDEN => 'forbidden',
        self::TYPE_ORGANISME_POOL => 'Aucun organisme disponible. Veuillez contacter le service client.',
    ];

    private $type;

    private $title;

    private array $extraData = [];

    public function __construct(private $statusCode, $type = null)
    {
        if (null === $type) {
            // no type? The default is about:blank and the title should
            // be the standard status code message
            $type = 'about:blank';
            $title = Response::$statusTexts[$statusCode] ?? 'Unknown status code :(';
        } else {
            if (!isset(self::$titles[$type])) {
                throw new InvalidArgumentException('No title for type ' . $type);
            }

            $title = self::$titles[$type];
        }

        $this->type = $type;
        $this->title = $title;
    }

    public function toArray()
    {
        return array_merge(
            $this->extraData,
            [
                'status' => $this->statusCode,
                'type' => $this->type,
                'title' => $this->title,
            ]
        );
    }

    public function set($name, $value)
    {
        if (is_array($value)) {
            $this->extraData[$name] = $value;
        } else {
            $this->extraData[$name] = [$value];
        }
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function get(string $name): ?array
    {
        if (!isset($this->extraData[$name])) {
            return null;
        }

        return $this->extraData[$name];
    }
}
