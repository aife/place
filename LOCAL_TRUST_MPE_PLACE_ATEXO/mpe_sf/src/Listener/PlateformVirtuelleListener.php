<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Listener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use App\Entity\Configuration\PlateformeVirtuelleOrganisme;

/**
 * Class PlateformVirtuelleListener
 */
class PlateformVirtuelleListener implements EventSubscriberInterface
{
    public function __construct(
        private readonly SessionInterface $session,
        private readonly EntityManagerInterface $em,
        private readonly ParameterBagInterface $param
    ) {
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        if (!$event->isMainRequest()) {
            return;
        }
        $request = $event->getRequest();
        if (!$request->hasPreviousSession()) {
            return;
        }
        $domaine = $request->server->get('SERVER_NAME') ?? $this->param->get('PF_URL_REFERENCE');

        if (! $this->session->get('organismes_eligibles_cached')) {
            $acronymOrgas = $this->em->getRepository(PlateformeVirtuelleOrganisme::class)
                ->getListAcronymeOrgEligible($domaine);
            $this->session->set('organismes_eligibles_cached', 'true');
            $this->session->set('organismes_eligibles', $acronymOrgas);
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [['onKernelRequest', 15]],
        ];
    }
}
