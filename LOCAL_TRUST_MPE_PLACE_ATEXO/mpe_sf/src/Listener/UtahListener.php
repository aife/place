<?php

namespace App\Listener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Exception;
use Symfony\Component\HttpKernel\KernelEvents;
use App\Service\Utah\UtahService;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 * Class UtahListener
 */
class UtahListener implements EventSubscriberInterface
{
    public const PARAM_OK = 'OK';

    /**
     * UtahListener constructor.
     */
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly UtahService $utahService,
        private readonly LoggerInterface $logger
    ) {
    }

    public function utah(RequestEvent $event): void
    {
        $request = $event->getRequest();
        if (
            self::PARAM_OK !== $request->get('Utah')
            && !$request->hasPreviousSession()
        ) {
            return;
        }

        try {
            //Permet d'initialiser le composant "AtexoUtah"
            if ($this->parameterBag->get('UTILISER_FAQ') || $this->parameterBag->get('UTILISER_UTAH')) {
                $this->utahService->setRequest($request);
                $this->utahService->initialiser();

                if (
                    $request->get('javaVersion') !== null
                    && $request->get('Utah') !== null
                    && self::PARAM_OK == $request->get('Utah')
                ) {
                    $this->utahService->setCallFrom($request->get('calledFrom'));
                    $this->utahService->accederFormulaireUtah(
                        $event,
                        $request->get('javaVersion')
                    );
                }
            }
        } catch (Exception $e) {
            $this->logger->error(
                'Erreur lors de la creation des contextes UTAH: '
                . PHP_EOL
                . 'Exception: '
                . $e->getMessage()
                . PHP_EOL
                . 'Trace: '
                . $e->getTraceAsString()
            );
        }
    }
    /**
     * @return array<string, mixed>
     */
    public static function getSubscribedEvents(): array
    {
        return [KernelEvents::REQUEST => 'utah'];
    }
}
