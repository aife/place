<?php

namespace App\Listener;

use App\Event\DepotValidationSynchronizeDocumentsEvent;
use App\Event\ValidateResponseEvent;
use App\Service\Handler\EmailAgentHandler;
use App\Service\Handler\EmailCompanyHandler;
use App\Service\SynchronizeDocumentsHandler;

/**
 * Mohamed BLAL.
 *
 * Class ResponseListener
 */
class ResponseListener
{
    /**
     * ResponseListener constructor.
     */
    public function __construct(
        private readonly EmailCompanyHandler $emailCompanyHandler,
        private readonly EmailAgentHandler $emailAgentHandler,
        private readonly SynchronizeDocumentsHandler $synchronizeDocumentsHandler
    ) {
    }

    /**
     * Cette Fonction s'éxcute au moment de la validation de la réponse, responsable de l'envoi de mail.
     */
    public function onValidateResponse(ValidateResponseEvent $event)
    {
        $this->emailCompanyHandler->handle($event);
        $this->emailAgentHandler->handle($event);
    }

    /**
     * Permt d'initialialiser la synchronisation des documents de l'entreprise/etablissement
     * Methode executee au moment de validation du depot de l'entreprise a la consultation.
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function onDepotValidationSynchronizeDocuments(DepotValidationSynchronizeDocumentsEvent $event)
    {
        $this->synchronizeDocumentsHandler->synchronizeDocuments($event);
    }
}
