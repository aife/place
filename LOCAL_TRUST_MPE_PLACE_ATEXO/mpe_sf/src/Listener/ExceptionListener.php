<?php

namespace App\Listener;

use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Exception;
use App\Exception\ApiProblemException;
use App\Traits\ConfigProxyTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ExceptionListener
{
    use ConfigProxyTrait;

    public function __construct(private $templating, private TranslatorInterface $translator, private readonly LoggerInterface $logger)
    {
    }

    public function onKernelException(ExceptionEvent $event)
    {
        $e = $event->getThrowable();
        if ($e instanceof ApiProblemException) {
            return;
        }
        $message = $event->getThrowable()->getMessage();

        // On logge l'exception
        $e = $event->getThrowable();
        $this->logException($e);

        $position = stripos($message, 'No route found for');
        if (false !== $position) {
            $message = $this->translator->trans('CONSULTATION_NON_DISPO');
        }
        $event->setResponse($this->templating->renderResponse(
            'Exception/error.html.twig.twig',
            ['message' => $message, ]
        ));
    }

    /**
     * Adapted from the core Symfony exception handling in ExceptionListener.
     */
    private function logException(Exception $exception)
    {
        $message = sprintf(
            'Uncaught PHP Exception %s: "%s" at %s line %s',
            $exception::class,
            $exception->getMessage(),
            $exception->getFile(),
            $exception->getLine()
        );
        $isCritical = !$exception instanceof HttpExceptionInterface || $exception->getStatusCode() >= 500;
        $context = ['exception' => $exception];
        if ($isCritical) {
            $this->logger->critical($message, $context);
        } else {
            $this->logger->error($message, $context);
        }
    }
}
