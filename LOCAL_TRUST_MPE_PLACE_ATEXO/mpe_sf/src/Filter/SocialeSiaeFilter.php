<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Filter;

class SocialeSiaeFilter extends AbstractClauseN3Filter
{
    public final const PROPERTY_NAME = 'clauseSocialeSiae';
    public final const CLAUSE_N3_ID = 8;
    public final const PROPERTY_DESCRIPTION = 'Filter by Sociale Siae (1 or 2 : is Sociale Siae = 1, 
    is not Sociale Siae = 2).';
}
