<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;

class AbstractClauseN1Filter extends AbstractFilter
{
    public const PROPERTY_NAME = '';
    public const IS_N1_CLAUSE = 1;
    public const IS_NOT_N1_CLAUSE = 2;
    public const CLAUSE_N1_ID = 0;
    public const PROPERTY_DESCRIPTION = '';

    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ) {
        $logicExpressions = [];
        if (
            $property !== static::PROPERTY_NAME
            || !in_array((int) $value, [static::IS_N1_CLAUSE, static::IS_NOT_N1_CLAUSE])
        ) {
            return;
        }

        $exprBuilder = $queryBuilder->expr();
        $alias = $queryBuilder->getRootAliases()[0];
        $aliasCnl1 = $queryNameGenerator->generateJoinAlias('CNL1');
        $aliasCn1 = $queryNameGenerator->generateJoinAlias('CN1');
        $aliasLot = $queryNameGenerator->generateJoinAlias('lot');

        if (1 === (int) $value) {
            $logicExpressions[] = $exprBuilder->in($alias . '.id', sprintf("SELECT IDENTITY($aliasLot.consultation)
             FROM App\Entity\Lot $aliasLot WHERE $aliasLot.id IN (
                SELECT IDENTITY($aliasCnl1.lot) FROM App\Entity\Consultation\ClausesN1 $aliasCnl1 
                WHERE $aliasCnl1.lot IS NOT NULL AND $aliasCnl1.referentielClauseN1 = %s)", static::CLAUSE_N1_ID));

            $logicExpressions[] = $exprBuilder->in($alias . '.id', sprintf("SELECT IDENTITY($aliasCn1.consultation)
            FROM App\Entity\Consultation\ClausesN1 $aliasCn1 WHERE $aliasCn1.consultation IS NOT NULL AND 
            $aliasCn1.referentielClauseN1 = %s", static::CLAUSE_N1_ID));
            $queryBuilder->andWhere($queryBuilder->expr()->orX(...$logicExpressions));
        } else {
            $logicExpressions[] = $exprBuilder->notIn($alias . '.id', sprintf("SELECT IDENTITY($aliasLot.consultation)
             FROM App\Entity\Lot $aliasLot WHERE $aliasLot.id IN (
                SELECT IDENTITY($aliasCnl1.lot) FROM App\Entity\Consultation\ClausesN1 $aliasCnl1 
                WHERE $aliasCnl1.lot IS NOT NULL AND $aliasCnl1.referentielClauseN1 = %s)", static::CLAUSE_N1_ID));

            $logicExpressions[] = $exprBuilder->notIn($alias . '.id', sprintf("SELECT IDENTITY($aliasCn1.consultation)
            FROM App\Entity\Consultation\ClausesN1 $aliasCn1 WHERE $aliasCn1.consultation IS NOT NULL AND 
            $aliasCn1.referentielClauseN1 = %s", static::CLAUSE_N1_ID));
            $queryBuilder->andWhere($queryBuilder->expr()->andX(...$logicExpressions));
        }
    }

    public function getDescription(string $resourceClass): array
    {
        return [
            static::PROPERTY_NAME => [
                'property' => static::PROPERTY_NAME,
                'type' => 'int',
                'required' => false,
                'is_collection' => false,
                'description' => static::PROPERTY_DESCRIPTION,
            ]
        ];
    }
}
