<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Filter;

class ClauseEnvironnementaleFilter extends AbstractClauseN1Filter
{
    public final const PROPERTY_NAME = 'clauseEnvironnementale';
    public final const CLAUSE_N1_ID = 2;
    public final const PROPERTY_DESCRIPTION = 'Filter by clause Environnementale (1 or 2 : is Environnementale = 1, is not
                    Environnementale = 2).';
}
