<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Lot;
use Doctrine\ORM\QueryBuilder;

class SocialeCommerceEquitableFilter extends SearchFilter
{
    public final const PROPERTY_NAME = 'socialeCommerceEquitable';
    public final const IS_SOCIALE_COMMERCE = 1;
    public final const IS_NOT_SOCIALE_COMMERCE = 2;

    protected function filterProperty(
        string $property,
               $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ) {
        $logicExpressions = [];
        if (
            $property !== self::PROPERTY_NAME
            || !in_array((int) $value, [self::IS_SOCIALE_COMMERCE, self::IS_NOT_SOCIALE_COMMERCE])
        ) {
            return;
        }

        $exprBuilder = $queryBuilder->expr();
        $alias = $queryBuilder->getRootAliases()[0];
        $queryBuilder->leftJoin(Lot::class,
            'CL',
            'WITH',
            $alias . '.id = CL.consultation AND (CL.clauseSpecificationTechnique like \'%:1:"4"%\' 
            OR CL.clauseSocialeConditionExecution  like \'%:1:"4"%\' OR CL.clauseSocialeInsertion like \'%:1:"4"%\')');
        if (self::IS_SOCIALE_COMMERCE === (int) $value) {
            $logicExpressions[] = $exprBuilder->like($alias.'.clauseSpecificationTechnique','\'%:1:"4"%\'');
            $logicExpressions[] = $exprBuilder->like($alias.'.clauseSocialeConditionExecution','\'%:1:"4"%\'');
            $logicExpressions[] = $exprBuilder->like($alias.'.clauseSocialeInsertion','\'%:1:"4"%\'');
            $logicExpressions[] = $exprBuilder->isNotNull('CL.id');
            $queryBuilder->andWhere($queryBuilder->expr()->orX(...$logicExpressions));
        } else {
            $logicExpressions[] = $exprBuilder->notLike($alias.'.clauseSpecificationTechnique','\'%:1:"4"%\'');
            $logicExpressions[] = $exprBuilder->notLike($alias.'.clauseSocialeConditionExecution','\'%:1:"4"%\'');
            $logicExpressions[] = $exprBuilder->notLike($alias.'.clauseSocialeInsertion','\'%:1:"4"%\'');
            $logicExpressions[] = $exprBuilder->isNull('CL.id');
            $queryBuilder->andWhere($queryBuilder->expr()->andX(...$logicExpressions));
        }

        $queryBuilder->addGroupBy($alias . '.id');
    }

    public function getDescription(string $resourceClass): array
    {
        return [
            self::PROPERTY_NAME => [
                'property' => self::PROPERTY_NAME,
                'type' => 'int',
                'required' => false,
                'is_collection' => false,
                'description' => 'Filter by Sociale Commerce Equitable (1 or 2 : is Sociale Commerce Equitable = 1, is not
                    Sociale Commerce Equitable = 2).',
            ]
        ];
    }
}
