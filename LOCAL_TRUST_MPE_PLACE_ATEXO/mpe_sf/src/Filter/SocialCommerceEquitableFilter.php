<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;

class SocialCommerceEquitableFilter extends AbstractClauseN3Filter
{
    public final const PROPERTY_NAME = 'socialeCommerceEquitable';
    public final const CLAUSE_N3_ID = 4;
    public final const PROPERTY_DESCRIPTION = 'Filter by Sociale Commerce Equitable (test)(1 or 2 : is Sociale Commerce 
        Equitable = 1, is not Sociale Commerce Equitable = 2).';
}
