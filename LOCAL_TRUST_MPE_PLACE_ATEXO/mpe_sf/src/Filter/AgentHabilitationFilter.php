<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\HabilitationAgent;
use Doctrine\ORM\QueryBuilder;

class AgentHabilitationFilter extends SearchFilter
{
    public final const PROPERTY_NAME = 'habilitations';

    /**
     * {@inheritdoc}
     */
    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ) {
        if ($property != self::PROPERTY_NAME) {
            return;
        }

        $values = $this->normalizeValues((array)$value, $property);

        $exprBuilder = $queryBuilder->expr();
        $alias = $queryBuilder->getRootAliases()[0];
        $habilitationAlias = $queryNameGenerator->generateJoinAlias('HabilitationAgent');
        $queryBuilder->leftJoin($alias . '.habilitation', $habilitationAlias);

        $andExpressions = [];
        foreach ($values as $val) {
            if (property_exists(HabilitationAgent::class, $val)) {
                $andExpressions[] = $exprBuilder->eq("{$habilitationAlias}.{$val}", '1');
            }
        }

        $queryBuilder->andWhere($queryBuilder->expr()->andX(...$andExpressions));
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass): array
    {
        return [
            self::PROPERTY_NAME . '[]' => [
                'property'      => self::PROPERTY_NAME,
                'type'          => 'string',
                'required'      => false,
                'is_collection' => true,
                'openapi'       => [
                    'description'   => 'Filter by habilitations'
                ]
            ]
        ];
    }
}
