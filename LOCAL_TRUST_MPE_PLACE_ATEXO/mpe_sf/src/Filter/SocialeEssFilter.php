<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Filter;

class SocialeEssFilter extends AbstractClauseN3Filter
{
    public final const PROPERTY_NAME = 'clauseSocialeEss';
    public final const CLAUSE_N3_ID = 9;
    public final const PROPERTY_DESCRIPTION = 'Filter by Sociale Ess (1 or 2 : is Sociale Ess = 1, Sociale Ess = 2).';
}
