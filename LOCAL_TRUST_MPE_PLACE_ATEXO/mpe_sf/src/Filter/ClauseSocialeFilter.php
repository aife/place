<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;

class ClauseSocialeFilter extends AbstractClauseN1Filter
{
    public final const PROPERTY_NAME = 'clauseSociale';
    public final const CLAUSE_N1_ID = 1;
    public final const PROPERTY_DESCRIPTION = 'Filter by clause Sociale (1 or 2 : is Sociale = 1, is not Sociale = 2).';
}
