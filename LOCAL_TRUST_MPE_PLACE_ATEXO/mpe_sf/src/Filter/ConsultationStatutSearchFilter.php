<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Doctrine\Extension\ConsultationStatutExtension;
use App\Service\Consultation\ConsultationStatus;
use Doctrine\ORM\QueryBuilder;

class ConsultationStatutSearchFilter extends SearchFilter
{
    public final const PROPERTY_NAME = ConsultationStatutExtension::PROPERTY_NAME;

    /**
     * {@inheritdoc}
     */
    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ) {
        if ($property != self::PROPERTY_NAME) {
            return;
        }

        $values = $this->normalizeValues((array)$value, $property);

        $orExpressions = [];

        foreach ($values as $filterValue) {
            $exprBuilder = $queryBuilder->expr();
            $orExpressions[] = $exprBuilder->like(
                self::PROPERTY_NAME,
                $exprBuilder->concat("'%'", "'$filterValue'", "'%'")
            );
        }

        $queryBuilder->andHaving($queryBuilder->expr()->orX(...$orExpressions));
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass): array
    {
        return [
            self::PROPERTY_NAME => [
                'property' => self::PROPERTY_NAME,
                'type' => 'string',
                'required' => false,
                'is_collection' => true,
                'openapi' => [
                    'description' => 'Filter by consultation status: ' . json_encode(
                        array_flip(ConsultationStatus::STATUS), JSON_THROW_ON_ERROR
                    ),
                ],
            ]
        ];
    }
}
