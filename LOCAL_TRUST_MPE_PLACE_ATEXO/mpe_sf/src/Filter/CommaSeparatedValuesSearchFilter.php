<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;

class CommaSeparatedValuesSearchFilter extends SearchFilter
{
    /**
     * {@inheritdoc}
     */
    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ) {
        if (null === $value ||
            !$this->isPropertyEnabled($property, $resourceClass) ||
            !$this->isPropertyMapped($property, $resourceClass, true)
        ) {
            return;
        }

        $values = $this->normalizeValues((array)$value, $property);

        $alias = $queryBuilder->getRootAliases()[0];

        $orExpressions = [];

        foreach ($values as $filterValue) {
            $exprBuilder = $queryBuilder->expr();
            $orExpressions[] = $exprBuilder->like(
                $alias . '.' . $property,
                $exprBuilder->concat("'%'", "',$filterValue,'", "'%'")
            );
        }

        $queryBuilder->andWhere($queryBuilder->expr()->orX(...$orExpressions));
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass): array
    {
        $descriptions = [];

        foreach ($this->properties as $filterName => $properties) {
            $propertyNames = [];

            foreach ($properties as $property => $strategy) {
                if (!$this->isPropertyMapped($property, $resourceClass, true)) {
                    continue;
                }

                $propertyNames[] = $this->normalizePropertyName($property);
            }

            $filterParameterName = $filterName . '[]';
            $descriptions[$filterParameterName] = [
                'property' => $filterName,
                'type' => 'string',
                'required' => false,
                'is_collection' => true,
                'openapi' => [
                    'description' => 'Search involves the fields: ' . implode(', ', $propertyNames),
                ],
            ];
        }

        return $descriptions;
    }
}
