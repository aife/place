<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Lot;
use Doctrine\ORM\QueryBuilder;

class SocialeInsertionFilter extends SearchFilter
{
    public final const PROPERTY_NAME = 'socialeInsertionActiviterEconomique';
    public final const IS_SOCIALE_INSERTION = 1;
    public final const IS_NOT_SOCIALE_INSERTION = 2;

    protected function filterProperty(
        string $property,
               $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ) {
        $logicExpressions = [];
        if (
            $property !== self::PROPERTY_NAME 
            || !in_array((int) $value, [self::IS_SOCIALE_INSERTION, self::IS_NOT_SOCIALE_INSERTION])
        ) {
            return;
        }

        $exprBuilder = $queryBuilder->expr();
        $alias = $queryBuilder->getRootAliases()[0];
        $queryBuilder->leftJoin(Lot::class,
            'CLS',
            'WITH',
            $alias . '.id = CLS.consultation AND (CLS.clauseSpecificationTechnique like \'%:1:"1"%\' 
            OR CLS.clauseSocialeConditionExecution  like \'%:1:"1"%\' OR CLS.clauseSocialeInsertion like \'%:1:"1"%\')');
        if (1 === (int) $value) {
            $logicExpressions[] = $exprBuilder->like($alias.'.clauseSpecificationTechnique','\'%:1:"1"%\'');
            $logicExpressions[] = $exprBuilder->like($alias.'.clauseSocialeConditionExecution','\'%:1:"1"%\'');
            $logicExpressions[] = $exprBuilder->like($alias.'.clauseSocialeInsertion','\'%:1:"1"%\'');
            $logicExpressions[] = $exprBuilder->isNotNull('CLS.id');
            $queryBuilder->andWhere($queryBuilder->expr()->orX(...$logicExpressions));
        } else {
            $logicExpressions[] = $exprBuilder->notLike($alias.'.clauseSpecificationTechnique','\'%:1:"1"%\'');
            $logicExpressions[] = $exprBuilder->notLike($alias.'.clauseSocialeConditionExecution','\'%:1:"1"%\'');
            $logicExpressions[] = $exprBuilder->notLike($alias.'.clauseSocialeInsertion','\'%:1:"1"%\'');
            $logicExpressions[] = $exprBuilder->isNull('CLS.id');
            $queryBuilder->andWhere($queryBuilder->expr()->andX(...$logicExpressions));
        }

        $queryBuilder->addGroupBy($alias . '.id');
    }

    public function getDescription(string $resourceClass): array
    {
        return [
            self::PROPERTY_NAME => [
                'property' => self::PROPERTY_NAME,
                'type' => 'int',
                'required' => false,
                'is_collection' => false,
                'description' => 'Filter by Sociale Insertion Activiter Economique (1 or 2 : is Sociale Commerce Equitable = 1, is not
                    Sociale Commerce Equitable = 2).',
            ]
        ];
    }
}
