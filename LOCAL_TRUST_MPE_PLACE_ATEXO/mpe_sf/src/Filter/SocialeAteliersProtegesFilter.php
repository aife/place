<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Filter;

class SocialeAteliersProtegesFilter extends AbstractClauseN3Filter
{
    public final const PROPERTY_NAME = 'clauseSocialeAteliersProteges';
    public final const CLAUSE_N3_ID = 7;
    public final const PROPERTY_DESCRIPTION = 'Filter by Sociale Ateliers Proteges (1 or 2 : is Sociale Ateliers Proteges = 1,
     is not Sociale Ateliers Proteges = 2).';
}
