<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;

class AgentPerimeterVisionSearchFilter extends SearchFilter
{
    public const PROPERTY_NAME = 'applyPerimeterVisionConsultations';

    /**
     * {@inheritdoc}
     */
    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ) {
        if ($property != self::PROPERTY_NAME) {
            return;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass): array
    {
        return [
            self::PROPERTY_NAME => [
                'property'      => self::PROPERTY_NAME,
                'type'          => 'bool',
                'required'      => false,
                'is_collection' => false,
                'openapi'       => [
                    'description'   => 'Filter Apply PerimeterVision of Consultations'
                ]
            ]
        ];
    }
}
