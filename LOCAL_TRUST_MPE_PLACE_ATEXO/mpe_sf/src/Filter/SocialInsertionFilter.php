<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Filter;

class SocialInsertionFilter extends AbstractClauseN3Filter
{
    public final const PROPERTY_NAME = 'socialeInsertionActiviterEconomique';
    public final const CLAUSE_N3_ID = 1;
    public final const PROPERTY_DESCRIPTION = 'Filter by Sociale Insertion Activiter Economique (1 or 2 : is Sociale Commerce
     Equitable = 1, is not Sociale Commerce Equitable = 2).';
}
