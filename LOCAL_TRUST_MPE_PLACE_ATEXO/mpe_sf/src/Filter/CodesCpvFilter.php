<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;

class CodesCpvFilter extends SearchFilter
{
    public final const PROPERTY_NAME = 'codesCPV';
    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ) {
        if ($property !== self::PROPERTY_NAME) {
            return;
        }

        $exprBuilder = $queryBuilder->expr();
        $alias = $queryBuilder->getRootAliases()[0];
        $codesCPV = $this->normalizeValues((array)$value, $property);

        $orExpressions = [];
        foreach ($codesCPV as $cpv) {
            $lotAlias = $queryNameGenerator->generateJoinAlias('lots');
            $orExpressions[] = $exprBuilder->like($alias . '.codeCpv1', '\'%' . $cpv . '%\'');
            $orExpressions[] = $exprBuilder->like($alias . '.codeCpv2', '\'%' . $cpv . '%\'');
            $orExpressions[] = $exprBuilder->in(
                $alias . '.id',
                "SELECT IDENTITY($lotAlias.consultation)
                   FROM App\Entity\Lot $lotAlias 
                   WHERE ($lotAlias.codeCpv1 LIKE '%$cpv%' OR $lotAlias.codeCpv2 LIKE '%$cpv%')"
            );
        }

        $queryBuilder->andWhere($queryBuilder->expr()->orX(...$orExpressions));
    }

    public function getDescription(string $resourceClass): array
    {
        return [
            self::PROPERTY_NAME => [
                'property' => self::PROPERTY_NAME,
                'type' => 'string',
                'required' => false,
                'is_collection' => true,
                'description' => 'Filter by codes CPV.',
            ]
        ];
    }
}
