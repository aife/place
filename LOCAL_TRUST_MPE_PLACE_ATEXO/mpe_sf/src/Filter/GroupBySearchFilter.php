<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Filter;

use BadMethodCallException;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Doctrine\Extension\ConsultationStatutExtension;
use App\Service\Consultation\ConsultationStatus;
use Doctrine\ORM\QueryBuilder;

class GroupBySearchFilter extends SearchFilter
{
    public final const PROPERTY_NAME = 'groupBy';
    public final const GROUP_BY_ACCEPT = ['categorie', 'typeProcedure', 'statutCalcule'];
    private static bool $isSelectedSatutCalcule = false;

    /**
     * {@inheritdoc}
     */
    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ): void {
        if ($property == ConsultationStatutExtension::PROPERTY_NAME) {
            self::$isSelectedSatutCalcule = true;
        }

        if ($property != self::PROPERTY_NAME) {
            return;
        }

        if (in_array($value, self::GROUP_BY_ACCEPT)) {
            $rootAlias = $queryBuilder->getRootAliases()[0];

            $queryBuilder->addSelect('COUNT(' . $rootAlias . '.id) AS total');

            $functionName = 'addQuery' . ucfirst($value);

            if (!method_exists($this, $functionName)) {
                throw new BadMethodCallException(
                    sprintf('%s function doesn\'t exist in %s.', $functionName, $this::class)
                );
            }

            $this->$functionName($queryBuilder, $rootAlias, $value);
            if (self::$isSelectedSatutCalcule) {
                $queryBuilder->addGroupBy('statutCalcule');
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass): array
    {
        return [
            self::PROPERTY_NAME => [
                'property' => self::PROPERTY_NAME,
                'type' => 'string',
                'required' => false,
                'openapi' => [
                    'description' => 'Group by consultation [Status/Categorie/Procédure]: ',
                ],
            ]
        ];
    }

    private function addQueryCategorie(QueryBuilder $queryBuilder, string $rootAlias, string $value): void
    {
        $queryBuilder->addSelect($rootAlias . '.' . $value . ', c.libelle')
            ->innerJoin($rootAlias . '.categorieConsultation', 'c')
            ->addGroupBy($rootAlias . '.' .  $value)
        ;
    }

    private function addQueryTypeProcedure(QueryBuilder $queryBuilder, string $rootAlias, string $value): void
    {
        $queryBuilder->addSelect($rootAlias . '.idTypeProcedure, t.libelleTypeProcedure as libelle')
            ->innerJoin($rootAlias . '.' .  $value, 't')
            ->addGroupBy($rootAlias . '.' .  $value)
        ;
    }

    private function addQueryStatutCalcule(QueryBuilder $queryBuilder, string $rootAlias, string $value): void
    {
        $queryBuilder->addGroupBy($value);
    }
}
