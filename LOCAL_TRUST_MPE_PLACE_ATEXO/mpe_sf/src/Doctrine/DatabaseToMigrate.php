<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Doctrine;

use Doctrine\ORM\Decorator\EntityManagerDecorator;

class DatabaseToMigrate extends EntityManagerDecorator
{
}
