<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Doctrine\Extension;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Doctrine\Criteria\ConsultationCriteria;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Lot;
use App\Entity\WS\AgentTechniqueAssociation;
use App\Service\Agent\AgentTechniqueAssociationService;
use App\Service\ConfigurationOrganismeService;
use App\Service\Perimeter\PerimetreVisionService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

final class PerimetreVisionExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    public const SUB_RESOURCE_CLASS = [Lot::class];
    public const NOT_APPLY_SUB_RESOURCE_CLASS = [Consultation\CritereAttribution::class];
    public const CONSULTATION_RESOURCE_ALIAS = 'c_';

    public function __construct(
        private readonly Security $security,
        private readonly ParameterBagInterface $parameterBag,
        private readonly EntityManagerInterface $entityManager,
        private readonly ConfigurationOrganismeService $confOrganismeService,
        private PerimetreVisionService $perimetreVisionService,
        private readonly AgentTechniqueAssociationService $agentTechniqueAssociationService
    ) {
    }

    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ): void {
        $this->addWhere($queryBuilder, $resourceClass, true);
    }

    public function applyToItem(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        array $identifiers,
        string $operationName = null,
        array $context = []
    ): void {
        $this->addWhere($queryBuilder, $resourceClass, false);
    }

    private function addWhere(QueryBuilder $queryBuilder, string $resourceClass, bool $collection): void
    {
        $isFromSubResourceClass = $this->perimetreVisionService->isFromSubResourceClass($resourceClass);

        if (
            (
                Consultation::class !== $resourceClass && !$isFromSubResourceClass
            )
            || !$this->security->isGranted('ROLE_AGENT')
            || null === $user = $this->security->getUser()
        ) {
            return;
        }

        // Dans le cadre des recherches sur une sous ressource de Consultation, nous devons faire une jointure
        // avec la table Consultation permettant ainsi d'appliquer le périmétre de vision sur une sous ressources (Lot)
        if ($isFromSubResourceClass) {
            $consultationJoinedFieldName = $resourceClass === Agent::class ? 'consultations' : 'consultation';
            $subResourceAlias = $queryBuilder->getRootAliases()[0];
            $queryBuilder->leftJoin(
                "{$subResourceAlias}.{$consultationJoinedFieldName}",
                self::CONSULTATION_RESOURCE_ALIAS
            );
            $queryBuilder->addOrderBy($subResourceAlias . '.id', 'ASC');
        }

        $consultationCriteria = new ConsultationCriteria(
            $queryBuilder,
            $this->entityManager,
            $this->parameterBag,
            $this->perimetreVisionService,
            $isFromSubResourceClass,
        );

        /**
         * Si l'agent est technique, on vérifie seulement ses associations à des Organisme et à des Services.
         * Organisme et Service associés à l'Agent
         * Organisme et Service provenant de la table d'association d'agents techniques
         **/
        if ($user->isTechnique()) {
            $organismes = [];
            $services = [];
            $associations = $this->agentTechniqueAssociationService->getByAgent($user);

            if (empty($associations) && !$user->getOrganisme() && !$user->getService()) {
                // Permission de voir toutes les consultations, si l'utilisateur n'est associé à aucun Service/Organisme
                return;
            }

            /**
             * Permission de voir les consultations liées au service associé,
             * si seulement un organisme est associé, l'utilisateur voit toutes les consultations de l'organisme
            **/
            if (!empty($user->getService())) {
                $services[] = $user->getService();
            } elseif (!empty($user->getOrganisme())) {
                $organismes[] = $user->getOrganisme();
            }

            /**
             * Si l'agent technique a des associations de configurées,
             * On limite le scope au service configuré
             * Si Seulement un organisme est configuré, l'utilisateur voit les consultations de tout l'organisme
             **/
            if (!empty($associations)) {
                /** @var AgentTechniqueAssociation $association */
                foreach ($associations as $association) {
                    $organisme = $association->getOrganisme();
                    $service = $association->getService();

                    if (empty($service)) {
                        $organismes[] = $organisme;
                    } else {
                        $services[] = $service;
                    }
                }

                $this->applyPerimetreAgentTechniqueAssociationCriteria(
                    $queryBuilder,
                    $user,
                    $consultationCriteria,
                    $organismes,
                    $services
                );
                return;
            }
        }

        // Filtre de base
        $queryBuilder->addCriteria($consultationCriteria->consultationsFiltreBasique());

        if ($collection) {
            // Uniquement les consultations non archivées
            $queryBuilder->addCriteria($consultationCriteria->consultationsNonArchivees());
        }

        // Périmètre de vision de l'Agent
        $this->applyPerimetreAgentCriteria($queryBuilder, $user, $consultationCriteria);
    }

    public function applyPerimetreAgentCriteria(
        QueryBuilder $queryBuilder,
        UserInterface $user,
        ConsultationCriteria $consultationCriteria
    ) {
        $whereExpressions = [];

        $whereExpressions[] = $consultationCriteria->returnFalse();

        $whereExpressions[] = $consultationCriteria->agentInvitePonctuel($user);

        $whereExpressions[] = $consultationCriteria->agentInvitePonctuelSuiviSeul($user);

        $whereExpressions[] = $consultationCriteria->agentInvitePermanentTransverse($user);

        $whereExpressions[] = $consultationCriteria->agentInvitePermanentDeSonEntite($user);

        $whereExpressions[] = $consultationCriteria->agentMembreEntiteParent($user);

        $whereExpressions[] = $consultationCriteria->validationFinale($user, $this->confOrganismeService);

        $queryBuilder->andWhere($queryBuilder->expr()->orX(...$whereExpressions));
    }

    public function applyPerimetreAgentTechniqueAssociationCriteria(
        QueryBuilder $queryBuilder,
        UserInterface $user,
        ConsultationCriteria $consultationCriteria,
        array $organismes,
        array $services
    ) {
        $whereExpressions = [];

        $whereExpressions[] = $consultationCriteria->agentTechniqueServices($user, $services);
        $whereExpressions[] = $consultationCriteria->agentTechniqueOrganismes($user, $organismes);

        $queryBuilder->andWhere($queryBuilder->expr()->orX(...$whereExpressions));
    }
}
