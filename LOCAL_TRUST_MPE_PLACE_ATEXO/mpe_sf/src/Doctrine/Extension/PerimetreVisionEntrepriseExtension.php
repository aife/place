<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Doctrine\Extension;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\ContextAwareQueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Consultation;
use App\Service\Consultation\ConsultationStatus;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Security;

final class PerimetreVisionEntrepriseExtension implements
    ContextAwareQueryCollectionExtensionInterface,
    QueryItemExtensionInterface
{
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly Security $security
    ) {
    }

    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null,
        array $context = []
    ): void {
        $this->andWhere($queryBuilder, $queryNameGenerator, $resourceClass, $context);
    }

    public function applyToItem(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        array $identifiers,
        string $operationName = null,
        array $context = []
    ): void {
        $this->andWhere($queryBuilder, $queryNameGenerator, $resourceClass, $context);
    }

    private function andWhere(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        array $context
    ): void {
        if (
            Consultation::class !== $resourceClass
            || $this->security->isGranted('ROLE_AGENT')
        ) {
            return;
        }

        $rootAlias = $queryBuilder->getRootAliases()[0];
        $organismeAlias = $queryNameGenerator->generateJoinAlias('organisme');
        $queryBuilder->innerJoin($rootAlias . '.organisme', $organismeAlias);
        $andExpressions = [];
        $exprBuilder = $queryBuilder->expr();
        $andExpressions[] = $exprBuilder->eq(
            $rootAlias . '.idEtatConsultation',
            '\'' . ConsultationStatus::STATUS['ELABORATION'] . '\''
        );
        $andExpressions[] = $exprBuilder->eq($rootAlias . '.consultationAnnulee', '\'0\'');
        $andExpressions[] = $exprBuilder->eq($organismeAlias . '.active', '\'1\'');
        $andExpressions[] = $exprBuilder->gt($rootAlias . '.dateFinUnix', 'unix_timestamp()');
        $andExpressions[] = $exprBuilder->neq($rootAlias . '.dateMiseEnLigneCalcule', '\'\'');
        $andExpressions[] = $exprBuilder->neq($rootAlias . '.dateMiseEnLigneCalcule', '\'0000-00-00 00:00:00\'');
        $andExpressions[] = $exprBuilder->isNotNull($rootAlias . '.dateMiseEnLigneCalcule');
        $andExpressions[] = $exprBuilder->lte($rootAlias . '.dateMiseEnLigneCalcule', 'now()');

        if (!empty($context['filters']['reference']) && !empty($context['filters']['codeProcedureRestreinte'])) {
            $andExpressions[] = $exprBuilder->eq(
                $rootAlias . '.typeAcces',
                '\'' . $this->parameterBag->get('TYPE_PROCEDURE_RESTREINTE') . '\''
            );
            $andExpressions[] = $exprBuilder->eq($rootAlias . '.referenceUtilisateur', "'{$context['filters']['reference']}'");
            $andExpressions[] = $exprBuilder->eq($rootAlias . '.codeProcedure', "'{$context['filters']['codeProcedureRestreinte']}'");
        } else {
            $andExpressions[] = $exprBuilder->eq(
                $rootAlias . '.typeAcces',
                '\'' . $this->parameterBag->get('TYPE_PROCEDURE_PUBLICITE') . '\''
            );
        }

        //Perimètre Panier Entreprise
        if (!empty($context['filters']['panier']) && $this->security->isGranted('ROLE_ENTREPRISE')) {
            $user = $this->security->getUser();
            $andExpressions[] = $exprBuilder->in(
                $rootAlias . '.id',
                'Select p.consultationId from App\Entity\PanierEntreprise p WHERE p.idInscrit = ' . $user->getId()
            );
        }

        $queryBuilder->andWhere($queryBuilder->expr()->andX(...$andExpressions));
    }
}
