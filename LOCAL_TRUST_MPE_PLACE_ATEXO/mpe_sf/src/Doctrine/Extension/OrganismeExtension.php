<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Doctrine\Extension;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Organisme;
use App\Service\CurrentUser;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Security;

final class OrganismeExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    public function __construct(
        private readonly Security $security,
        private readonly CurrentUser $currentUser
    ) {
    }

    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ): void {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    public function applyToItem(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        array $identifiers,
        string $operationName = null,
        array $context = []
    ): void {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    private function addWhere(QueryBuilder $queryBuilder, string $resourceClass): void
    {
        if (Organisme::class !== $resourceClass) {
            return;
        }

        $user = $this->security->getUser();

        if (!$this->security->isGranted('ROLE_AGENT') || empty($user)) {
            // Condition pour que la requête ne retourne rien
            $queryBuilder->andWhere("1 = 2");
            return;
        }

        $userOrganismeId = $user->getOrganisme()?->getId();
        if ($user->isTechnique() && empty($userOrganismeId)) {
            // Permission de voir tous les organismes
            return;
        }

        if (!$this->currentUser->checkHabilitation('HyperAdmin')) {
            $rootAlias = $queryBuilder->getRootAliases()[0];
            $queryBuilder->andWhere("{$rootAlias}.id = :organismeId");
            $queryBuilder->setParameter('organismeId', $userOrganismeId);
        }
    }
}
