<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Doctrine\Extension;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\ContratTitulaire;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\RequestStack;

class ContratTitulaireApiLegacyExtension implements QueryCollectionExtensionInterface
{
    final public const MARCHE_DEFENSE = ['0', '2'];

    public function __construct(private RequestStack $request)
    {
    }

    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ) {
        if (
            $resourceClass !== ContratTitulaire::class
            || !str_contains($this->request->getCurrentRequest()->getUri(), '/place/contrat-titulaires')
        ) {
            return;
        }

        $rootAlias = $queryBuilder->getRootAliases()[0];
        $queryBuilder
            ->andWhere($rootAlias . '.marcheDefense IN (:marcheDefense)')
            ->setParameter('marcheDefense', self::MARCHE_DEFENSE)
        ;
    }
}
