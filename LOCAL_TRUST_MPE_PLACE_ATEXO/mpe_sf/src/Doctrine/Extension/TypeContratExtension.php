<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Doctrine\Extension;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\TypeContrat;
use App\Entity\TypeContratEtTypeProcedure;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Security;

final class TypeContratExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    public function __construct(
        private readonly Security $security,
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ): void {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    public function applyToItem(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        array $identifiers,
        string $operationName = null,
        array $context = []
    ): void {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    private function addWhere(QueryBuilder $queryBuilder, string $resourceClass): void
    {
        if (TypeContrat::class !== $resourceClass) {
            return;
        }

        $user = $this->security->getUser();

        if (!$this->security->isGranted('ROLE_AGENT') || !$user) {
            // On passe une condition erroné pour retourner une réponse vide
            $queryBuilder->andWhere("1 <> 1");
            return;
        }

        $userOrganismeId = $user->getOrganisme()?->getId();
        if ($user->isTechnique() && !$userOrganismeId) {
            // Permission de voir tous les typeContrat
            return;
        }

        $allowedTypeContratIdsByOrganisme = $this->entityManager
            ->getRepository(TypeContratEtTypeProcedure::class)
            ->getTypeContratIdsFilteredByOrganisme($user->getOrganisme())
        ;

        $rootAlias = $queryBuilder->getRootAliases()[0];
        $queryBuilder
            ->andWhere("{$rootAlias}.idTypeContrat in (:idTypeContrat)")
            ->setParameter('idTypeContrat', $allowedTypeContratIdsByOrganisme)
        ;
    }
}
