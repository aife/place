<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Doctrine\Extension;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\AffiliationService;
use App\Entity\Agent;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Security;

final class AgentExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    public function __construct(private readonly Security $security, private readonly EntityManagerInterface $em)
    {
    }

    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ) {
        $this->andWhere($queryBuilder, $queryNameGenerator, $resourceClass);
    }

    public function applyToItem(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        array $identifiers,
        string $operationName = null,
        array $context = []
    ) {
        $this->andWhere($queryBuilder, $queryNameGenerator, $resourceClass);
    }

    private function andWhere(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass
    ): void {
        $andWhere = [];
        if (
            Agent::class !== $resourceClass
            || !$this->security->isGranted('ROLE_AGENT')
            || null === $this->security->getUser()
            || null === $this->security->getUser()?->getOrganisme()
        )  {
            return;
        }
        $rootAlias = $queryBuilder->getRootAliases()[0];
        if (null !== $this->security->getUser()?->getService()) {
            $exprBuilder = $queryBuilder->expr();
            $service = $this->security->getUser()->getService();
            $childServices = [];
            $organisme = $this->security->getUser()->getOrganisme();
            $this->em->getRepository(AffiliationService::class)
                ->getAllChildsId($service->getId(), $organisme->getAcronyme(), $childServices);
            $andWhere[] = $exprBuilder->in($rootAlias . '.service', $childServices);
            $andWhere[] = $exprBuilder->eq($rootAlias . '.organisme', "'" . $organisme->getAcronyme() . "'");

            $queryBuilder->andWhere($queryBuilder->expr()->andX(...$andWhere));
        }
    }
}
