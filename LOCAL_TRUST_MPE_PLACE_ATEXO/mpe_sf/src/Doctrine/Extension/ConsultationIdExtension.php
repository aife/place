<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Doctrine\Extension;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Consultation;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\RequestStack;

final class ConsultationIdExtension implements QueryCollectionExtensionInterface
{
    public const IDS_ONLY = 'idsOnly';

    public function __construct(
        private readonly RequestStack $requestStack
    ) {
    }

    /**
     * Si le paramètre 'idsOnly' est présent dans la requête HTTP,
     * on ne retourne que les IDs des consultations (performance).
     *
     * @param string|null $operationName
     * @return void
     */
    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ) {
        if (Consultation::class !== $resourceClass) {
            return;
        }
        if (!$this->requestStack->getCurrentRequest()->query->has(self::IDS_ONLY)) {
            return;
        }

        $rootAlias = $queryBuilder->getRootAliases()[0];
        $queryBuilder->select("partial $rootAlias.{id}");
    }

    public static function shouldReturnOnlyIds(array $context): bool
    {
        if (!isset($context['request_uri'])) {
            return false;
        }

        parse_str(parse_url($context['request_uri'], PHP_URL_QUERY), $queryParams);

        return array_key_exists(ConsultationIdExtension::IDS_ONLY, $queryParams);
    }
}
