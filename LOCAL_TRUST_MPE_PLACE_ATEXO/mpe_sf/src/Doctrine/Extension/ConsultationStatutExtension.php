<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Doctrine\Extension;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Consultation;
use App\Service\Consultation\ConsultationStatus;
use App\Service\Perimeter\PerimetreVisionService;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

final class ConsultationStatutExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    public const PROPERTY_NAME = 'statutCalcule';

    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly PerimetreVisionService $perimetreVisionService
    ) {
    }

    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ): void {
        $this->addStatutToSelect($queryBuilder, $resourceClass);
    }

    public function applyToItem(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        array $identifiers,
        string $operationName = null,
        array $context = []
    ): void {
        $this->addStatutToSelect($queryBuilder, $resourceClass);
    }

    private function addStatutToSelect(QueryBuilder $queryBuilder, string $resourceClass): void
    {
        $isFromSubResource = $this->perimetreVisionService->isFromSubResourceClass($resourceClass);

        if (
            Consultation::class !== $resourceClass && !$isFromSubResource
        ) {
            return;
        }

        $rootAlias = $isFromSubResource
            ? PerimetreVisionExtension::CONSULTATION_RESOURCE_ALIAS
            : $queryBuilder->getRootAliases()[0]
        ;

        $queryBuilder->addSelect(static::getStatutCalculeSelectPart($rootAlias) . " AS " . self::PROPERTY_NAME);

        $queryBuilder->setParameter('statutElaboration', ConsultationStatus::STATUS['ELABORATION']);
        $queryBuilder->setParameter('statutPreparation', ConsultationStatus::STATUS['PREPARATION']);
        $queryBuilder->setParameter('statutConsultation', ConsultationStatus::STATUS['CONSULTATION']);
        $queryBuilder->setParameter('statutOuvertureAnalyse', ConsultationStatus::STATUS['OUVERTURE_ANALYSE']);
        $queryBuilder->setParameter('statutDecision', ConsultationStatus::STATUS['DECISION']);

        $queryBuilder->setParameter('emptyDateTime', ConsultationStatus::EMPTY_DATE_TIME);

        $queryBuilder->setParameter(
            'paramDecisionPartielleOui',
            $this->parameterBag->get('DECISION_PARTIELLE_OUI')
        );
        $queryBuilder->setParameter(
            'paramDepouillablePhaseConsultation',
            $this->parameterBag->get('CONSULTATION_DEPOUIABLE_PHASE_1')
        );
    }

    public static function getStatutCalculeSelectPart($rootAlias): string
    {
        $statutCalculeSubQuery = static::getStatutCalculeSubQuery($rootAlias);

        return
            "
            CASE WHEN
                $rootAlias.decisionPartielle = :paramDecisionPartielleOui
                AND ($statutCalculeSubQuery) = :statutOuvertureAnalyse
            THEN
                concat(:statutOuvertureAnalyse, ',', :statutDecision)
            ELSE
                CASE WHEN
                    $rootAlias.depouillablePhaseConsultation = :paramDepouillablePhaseConsultation
                    AND ($statutCalculeSubQuery) > :statutPreparation
                THEN
                    concat(
                        CASE WHEN $rootAlias.datefin > now()
                        THEN concat(:statutConsultation, ',') ELSE '' END,                        
                        :statutOuvertureAnalyse, ',', :statutDecision)
                ELSE
                    $statutCalculeSubQuery
                END
            END"
            ;
    }

    protected static function getStatutCalculeSubQuery($rootAlias): string
    {
        return "
            CASE WHEN 
                $rootAlias.idEtatConsultation > :statutOuvertureAnalyse
            THEN $rootAlias.idEtatConsultation 
            ELSE
                CASE WHEN
                    $rootAlias.dateMiseEnLigneCalcule is null
                    OR $rootAlias.dateMiseEnLigneCalcule = :emptyDateTime
                    OR $rootAlias.dateMiseEnLigneCalcule > now()
                THEN
                    CASE WHEN $rootAlias.etatEnAttenteValidation = '1'
                    THEN :statutPreparation
                    ELSE :statutElaboration
                    END
                ELSE
                    CASE WHEN
                        $rootAlias.datefin > now()
                    THEN
                        :statutConsultation
                    ELSE
                        CASE WHEN
                            $rootAlias.idEtatConsultation = '0'
                        THEN
                            :statutOuvertureAnalyse
                        ELSE
                            1000
                        END
                    END
                END
            END
        ";
    }
}
