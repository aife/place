<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Doctrine\Extension;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Service;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Security;

final class ServiceExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    public function __construct(private readonly Security $security)
    {
    }

    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ): void {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    public function applyToItem(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        array $identifiers,
        string $operationName = null,
        array $context = []
    ): void {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    private function addWhere(QueryBuilder $queryBuilder, string $resourceClass): void
    {
        if (Service::class !== $resourceClass) {
            return;
        }

        $user = $this->security->getUser();

        if (!$this->security->isGranted('ROLE_AGENT') || empty($user)) {
            // Condition pour que la requête ne retourne rien
            $queryBuilder->andWhere("1 = 2");
            return;
        }

        $userOrganismeAcronyme = $user->getOrganisme()?->getAcronyme();
        if ($user->isTechnique() && empty($userOrganismeAcronyme)) {
            // Permission de voir tous les services
            return;
        }

        $rootAlias = $queryBuilder->getRootAliases()[0];
        $userServiceId = $user->getService()?->getId();

        if (empty($userServiceId)) {
            // Permission de voir tous les services de l'organisme
            $queryBuilder->andWhere("{$rootAlias}.organisme = :organismeAcronyme");
            $queryBuilder->setParameter('organismeAcronyme', $userOrganismeAcronyme);
        } else {
            $queryBuilder->andWhere("{$rootAlias}.id = :serviceId");
            $queryBuilder->setParameter('serviceId', $userServiceId);
        }
    }
}
