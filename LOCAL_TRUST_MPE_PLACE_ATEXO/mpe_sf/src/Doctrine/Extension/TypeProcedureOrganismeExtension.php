<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Doctrine\Extension;

use App\Service\Agent\HabilitationTypeProcedureService;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Entity\Procedure\TypeProcedureOrganisme;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Security;

final class TypeProcedureOrganismeExtension implements
    QueryCollectionExtensionInterface,
    QueryItemExtensionInterface
{
    public function __construct(
        private Security $security,
        private ParameterBagInterface $parameterBag,
        private HabilitationTypeProcedureService $habilitationTypeProcedureService
    ) {
    }

    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ): void {
        $this->addWhere($queryBuilder, $resourceClass, $operationName);
    }

    public function applyToItem(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        array $identifiers,
        string $operationName = null,
        array $context = []
    ): void {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    private function addWhere(
        QueryBuilder $queryBuilder,
        string $resourceClass,
        string $operationName = null
    ): void {
        if (TypeProcedureOrganisme::class !== $resourceClass) {
            return;
        }

        $user = $this->security->getUser();

        if (!$this->security->isGranted('ROLE_AGENT') || empty($user)) {
            // Condition pour que la requête ne retourne rien
            $queryBuilder->andWhere("1 = 2");

            return;
        }

        $userOrganismeAcronyme = $user->getOrganisme()?->getAcronyme();

        if (empty($userOrganismeAcronyme)) {
            // Permission de voir tous les types de procedures
            return;
        } else {
            $rootAlias = $queryBuilder->getRootAliases()[0];
            $queryBuilder->andWhere("{$rootAlias}.organisme = :organismeAcronyme");
            $queryBuilder->setParameter('organismeAcronyme', $userOrganismeAcronyme);
        }
        if (!$user->isTechnique()) {
            $habilitation = $user->getHabilitation();

            if ($this->parameterBag->get('ACTIVE_HABILITATION_V2')) {
                $this->includeByNewHabilitation($user, $queryBuilder, $operationName);
            } else {
                $this->includeByHabilitation($habilitation, $queryBuilder);
            }
        }
    }

    private function includeByHabilitation(HabilitationAgent $habilitation, QueryBuilder $queryBuilder)
    {
        $rootAlias = $queryBuilder->getRootAliases()[0];

        $gestionMapaInf = $habilitation->getGererMapaInferieurMontant();
        $gestionMapaSup = $habilitation->getGererMapaSuperieurMontant();
        $administrerProcedureFormalisees = $habilitation->getAdministrerProceduresFormalisees();

        $query = [];

        if ($administrerProcedureFormalisees === '1') {
            $query[] = "({$rootAlias}.mapa = '0')";
        }

        if ($gestionMapaInf === '1') {
            $query[] = "
                (
                    {$rootAlias}.idMontantMapa = :montantMapaInf 
                        AND {$rootAlias}.mapa = '1'
                        AND {$rootAlias}.activerMapa = '1'
                )
            ";
            $queryBuilder->setParameter(
                'montantMapaInf',
                $this->parameterBag->get('MONTANT_MAPA_INF_90')
            );
        }

        if ($gestionMapaSup === '1') {
            $query[] = "
                (
                    {$rootAlias}.idMontantMapa = :montantMapaSup 
                        AND {$rootAlias}.mapa = '1'
                        AND {$rootAlias}.activerMapa = '1'
                )
            ";
            $queryBuilder->setParameter(
                'montantMapaSup',
                $this->parameterBag->get('MONTANT_MAPA_SUP_90')
            );
        }

        // Aucune habilitation attribuée au type de procedure
        if (empty($query)) {
            // Condition pour que la requête ne retourne rien
            $queryBuilder->andWhere("1 = 2");

            return;
        }

        $queryBuilder->andWhere(implode(' OR ', $query));
    }

    private function includeByNewHabilitation(Agent $agent, QueryBuilder $queryBuilder, string $operationName = null)
    {
        $rootAlias = $queryBuilder->getRootAliases()[0];

        switch ($operationName) {
            case 'get_for_suite':
                $idTypeProcedures = array_filter(
                    $this->habilitationTypeProcedureService
                        ->getCreerSuiteConsultationIdTypeProcedures($agent)
                );
                break;
            case 'get_for_consultation_creation':
                $idTypeProcedures = array_filter(
                    $this->habilitationTypeProcedureService
                        ->getCreerConsultationIdTypeProcedures($agent)
                );
                break;
            default:
                $idTypeProcedures = [];
                break;
        }

        $query = [];

        if (!empty($idTypeProcedures)) {
            $query[] = "
                (
                    {$rootAlias}.idTypeProcedure IN (" . implode(",", $idTypeProcedures) . ")
                )
            ";
        }

        // Aucune habilitation attribuée au type de procedure pour la création de consultation et de suite
        if (empty($query) && in_array($operationName, ['get_for_suite', 'get_for_consultation_creation'])) {
            // Condition pour que la requête ne retourne rien
            $queryBuilder->andWhere("1 = 2");

            return;
        }

        if (!empty($query)) {
            $queryBuilder->andWhere(implode(' OR ', $query));
        }
    }
}
