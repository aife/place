<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Doctrine\Extension;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\ConsLotContrat;
use App\Entity\ContratTitulaire;
use App\Repository\InvitationConsultationTransverseRepository;
use App\Service\WebservicesMpeConsultations;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Security;

final class ContratTitulaireExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    public function __construct(
        private readonly Security $security,
        private readonly WebservicesMpeConsultations $wsConsultations,
        private readonly InvitationConsultationTransverseRepository $invitationRepository,
        private readonly ParameterBagInterface $parameterBag
    ) {
    }

    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ): void {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    public function applyToItem(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        array $identifiers,
        string $operationName = null,
        array $context = []
    ): void {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    private function addWhere(QueryBuilder $queryBuilder, string $resourceClass): void
    {
        if (ContratTitulaire::class !== $resourceClass) {
            return;
        }

        $user = $this->security->getUser();

        if (!$this->security->isGranted('ROLE_AGENT') || !$user) {
            // On passe une condition erroné pour retourner une réponse vide
            $queryBuilder->andWhere("1 <> 1");
            return;
        }

        $userOrganismeAcronyme = $user->getOrganisme()?->getAcronyme();
        if ($user->isTechnique() && !$userOrganismeAcronyme) {
            // Permission de voir tous les contrats
            return;
        }

        $rootAlias = $queryBuilder->getRootAliases()[0];

        $wsResponse = $this->wsConsultations->getContent('searchConsultations', [
            'pagination' => false,
            ConsultationIdExtension::IDS_ONLY => 1
        ]);

        $consultationId = [];

        $contratIds = $this->invitationRepository->findContratsByOrganismInvited(
            $userOrganismeAcronyme,
            $this->parameterBag->get('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE')
        );

        foreach ($wsResponse->{'hydra:member'} as $item) {
            $consultationId[] = $item->{'0'}->{'id'};
        }

        $queryBuilder->leftJoin(
            ContratTitulaire::class,
            'tct2',
            Join::WITH,
            sprintf('%s.idContratTitulaire = tct2.idContratMulti', $rootAlias)
        );

        $queryBuilder->leftJoin(
            ConsLotContrat::class,
            'clc',
            Join::WITH,
            sprintf('%s.idContratTitulaire = clc.idContratTitulaire', $rootAlias)
        );

        $queryBuilder->leftJoin(
            ConsLotContrat::class,
            'clc2',
            Join::WITH,
            sprintf('%s.idContratTitulaire = clc2.idContratTitulaire', 'tct2')
        );

        $queryBuilder->andWhere(
            sprintf(
                'clc.consultationId IN (:consultationId) OR clc2.consultationId IN (:consultationId) OR %s.idContratTitulaire IN (:contratIds)',
                $rootAlias
            )
        );
        $queryBuilder->setParameter('consultationId', $consultationId);
        $queryBuilder->setParameter('contratIds', $contratIds);
    }
}
