<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Doctrine\Criteria;

use App\Enum\Habilitation\HabilitationSlug;
use App\Service\Perimeter\PerimetreVisionService;
use Doctrine\ORM\Query\Expr\Comparison;
use Doctrine\ORM\Query\Expr\Func;
use Doctrine\ORM\Query\Expr\Orx;
use Doctrine\ORM\Query\Expr\Andx;
use App\Doctrine\Extension\ConsultationStatutExtension;
use App\Doctrine\Extension\PerimetreVisionExtension;
use App\Entity\AffiliationService;
use App\Entity\Agent;
use App\Entity\Consultation\InterneConsultation;
use App\Entity\Consultation\InterneConsultationSuiviSeul;
use App\Entity\InvitePermanentTransverse;
use App\Entity\Service;
use App\Service\ConfigurationOrganismeService;
use App\Service\Consultation\ConsultationStatus;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\CompositeExpression;
use Doctrine\DBAL\Driver\Exception as DBALDriverException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\Expr\Composite;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class ConsultationCriteria
{
    private readonly string $rootAlias;

    public function __construct(
        private readonly QueryBuilder $queryBuilder,
        private readonly EntityManagerInterface $entityManager,
        private readonly ParameterBagInterface $parameterBag,
        private PerimetreVisionService $perimetreVisionService,
        private readonly bool $isFromSubResourceClass = false,
    ) {
        $this->rootAlias = $isFromSubResourceClass
            ? PerimetreVisionExtension::CONSULTATION_RESOURCE_ALIAS
            : $queryBuilder->getRootAliases()[0]
        ;
    }

    /**
     * Uniquement les consultations :
     *  - avis de type 3,
     *  - non doublon,
     *  - consultation achat public
     *
     */
    public function consultationsFiltreBasique(): Criteria
    {
        $criteria = Criteria::create();

        $expression = new CompositeExpression(
            CompositeExpression::TYPE_AND,
            [
                Criteria::expr()->eq(
                    $this->rootAlias . '.idTypeAvis',
                    $this->parameterBag->get('TYPE_AVIS_CONSULTATION')
                ),
                Criteria::expr()->eq($this->rootAlias . '.doublon', '0'),
                Criteria::expr()->eq($this->rootAlias . '.consultationAchatPublique', '0'),
            ]
        );

        $criteria->where($expression);

        return $criteria;
    }

    /**
     * Uniquement les consultations non archivées
     *
     */
    public function consultationsNonArchivees(): Criteria
    {
        $criteria = Criteria::create();
        $criteria->where(
            Criteria::expr()->lte(
                $this->rootAlias . '.idEtatConsultation',
                $this->parameterBag->get('STATUS_DECISION')
            )
        );

        return $criteria;
    }

    /**
     * Ce critère retournera une condition jamais remplie pour exclure par défaut toutes les consultations.
     * Ensuite, on ajoute les consultations via les autres critères.
     *
     */
    public function returnFalse(): Comparison
    {
        return (new Expr())->eq($this->rootAlias . '.id', -10);
    }

    /**
     * L'agent est invité ponctuel de la consultation
     *
     */
    public function agentInvitePonctuel(
        UserInterface $user
    ): ?Func {
        return static::getInvitePonctuelCriteria($user, InterneConsultation::class);
    }

    /**
     * L'agent est invité ponctuel de la consultation, en suivi seul
     *
     */
    public function agentInvitePonctuelSuiviSeul(
        UserInterface $user
    ): ?Func {
        return static::getInvitePonctuelCriteria($user, InterneConsultationSuiviSeul::class);
    }

    private function getInvitePonctuelCriteria(
        UserInterface $user,
        string $entityClass
    ): ?Func {
        if ($user instanceof Agent) {
            $interneConsultations = $this->entityManager->getRepository($entityClass)
                ->findBy(['agent' => $user]);

            $consultationsIds = array_map(
                fn($interneConsultation) => $interneConsultation->getConsultation()?->getId(),
                $interneConsultations
            );

            if (!empty($consultationsIds)) {
                return (new Expr())->in($this->rootAlias . '.id', $consultationsIds);
            }
        }
        return null;
    }

    /**
     * L'agent possède l’habilitation « Invité permanent transverse »
     * ET a l'entité d'origine de la consultation dans son périmètre d'intervention transverse
     *
     */
    public function agentInvitePermanentTransverse(
        UserInterface $user
    ): Func|Orx|null {
        if ($user instanceof Agent) {
            if ($this->parameterBag->get('ACTIVE_HABILITATION_V2')) {
                $isInvitePermanentTransverse = $this->perimetreVisionService
                    ->isPerimetreVisionHabilitationActiveBySlug(
                        HabilitationSlug::INVITE_PERMANENT_TRANSVERSE(),
                        $user
                    );
            } else {
                $isInvitePermanentTransverse = (bool)$user->getHabilitation()?->getInvitePermanentTransverse();
            }
        }
        if ($isInvitePermanentTransverse) {
            $servicesIds = $this->entityManager->getRepository(InvitePermanentTransverse::class)
                ->getServicesIdByAgentId($user->getId());

            if (!empty($servicesIds)) {
                $inServicesExpression = (new Expr())->in($this->rootAlias . '.service', $servicesIds);

                if (in_array(0, $servicesIds)) {
                    $organismeLevelExpression = (new Expr())->andX(
                        (new Expr())->eq(
                            $this->rootAlias . '.organisme',
                            sprintf("'%s'", $user->getOrganisme()->getAcronyme())
                        ),
                        (new Expr())->isNull($this->rootAlias . '.service')
                    );
                    return (new Expr())->orX($inServicesExpression, $organismeLevelExpression);
                }
                return $inServicesExpression;
            }
        }
        return null;
    }

    /**
     * L'agent possède l’habilitation « Invité permanent de mon entité »
     * ET est membre de l’entité d’origine de la consultation
     *
     * @return Expr\Func|null
     * @throws DBALDriverException
     * @throws Exception
     */
    public function agentInvitePermanentDeSonEntite(
        UserInterface $user
    ): Comparison|Andx|null {
        if ($user instanceof Agent) {
            if ($this->parameterBag->get('ACTIVE_HABILITATION_V2')) {
                $isInvitePermanent = $this->perimetreVisionService
                    ->isPerimetreVisionHabilitationActiveBySlug(
                        HabilitationSlug::INVITE_PERMANENT_MON_ENTITE(),
                        $user
                    );
            } else {
                $isInvitePermanent = (bool)$user->getHabilitation()?->getInvitePermanentMonEntite();
            }
        }
        if ($isInvitePermanent) {
            if ($user->getService() instanceof Service) {
                return (new Expr())->eq($this->rootAlias . '.service', $user->getService()->getId());
            } else {
                return (new Expr())->andX(
                    (new Expr())->eq(
                        $this->rootAlias . '.organisme',
                        sprintf("'%s'", $user->getOrganisme()->getAcronyme())
                    ),
                    (new Expr())->isNull($this->rootAlias . '.service')
                );
            }
        }
        return null;
    }

    /**
     * L'agent possède l’habilitation « Invité permanent sur les entités dépendantes de mon entité »
     * ET est membre d'une entité parent de l'entité d’origine de la consultation
     *
     * @throws DBALDriverException
     * @throws Exception
     */
    public function agentMembreEntiteParent(
        UserInterface $user
    ): ?Func {
        if ($user instanceof Agent) {
            if ($this->parameterBag->get('ACTIVE_HABILITATION_V2')) {
                $isMembreEntiteParent = $this->perimetreVisionService
                    ->isPerimetreVisionHabilitationActiveBySlug(
                        HabilitationSlug::INVITE_PERMANENT_ENTITE_DEPENDANTE(),
                        $user
                    );
            } else {
                $isMembreEntiteParent = (bool)$user->getHabilitation()?->getInvitePermanentEntiteDependante();
            }
            if ($isMembreEntiteParent) {
                $servicesIds = static::getSousServicesIds($user, $this->entityManager);

                if (!empty($servicesIds)) {
                    return (new Expr())->in($this->rootAlias . '.service', $servicesIds);
                }
            }
        }
        return null;
    }

    /**
     * La consultation est au statut EV (= Préparation)
     * ET a pour règle de validation « Type 3 : Validation finale requise par l'Entité ci-après »
     * ET l'agent possède l’habilitation « Validation finale (avec affectation de clé le cas échéant) »
     * ET (
     *      est membre de l'entité renseignée dans la règle de validation
     *      OU (
     *          est membre d'une entité parent de l'entité renseignée dans la règle de validation
     *          ET l'organisme de rattachement de l'agent est centralisé
     *      )
     * )
     *
     * @throws DBALDriverException
     * @throws Exception
     */
    public function validationFinale(
        UserInterface $user,
        ConfigurationOrganismeService $confOrganismeService
    ): ?Composite {
        if (
            !$user instanceof Agent
            || !(bool)$user->getHabilitation()?->getValidationFinale()
        ) {
            // Si l'agent n'a pas l'habilitation requise, on n'ajoute aucune condition. Il ne verra rien de plus.
            return null;
        }

        $criteriasAgentMembreEntiteRegleValidation = [];

        // L'agent est membre de l'entité renseignée dans la règle de validation
        $sousServicesIds = static::getSousServicesIds($user, $this->entityManager);

        if ($user->getService() instanceof Service || !empty($sousServicesIds)) {
            $criteriasAgentMembreEntiteRegleValidation[] = (new Expr())
                ->in(
                    $this->rootAlias . '.serviceValidation',
                    ($user->getService() instanceof Service)
                        ? [$user->getService()->getId()]
                        : $sousServicesIds
                );
        } else {
            // Si l'organisme n'a aucun service associé, l'agent peut voir la consultation si service_validation
            // de celle-ci est null et que l'organisme de la consultation est égal à celui de l'agent.
            $criteriasAgentMembreEntiteRegleValidation[] = (new Expr())->andX(
                (new Expr())->isNull($this->rootAlias . '.serviceValidation'),
                (new Expr())->eq($this->rootAlias . '.organisme', ':regleValidationOrganisme'),
            );
            $this->queryBuilder->setParameter('regleValidationOrganisme', $user->getAcronymeOrganisme());
        }

        $organisationCentralisee = false;
        if (!empty($user->getOrganisme())) {
            $organisationCentralisee = $confOrganismeService->moduleIsEnabled(
                $user->getOrganisme(),
                'OrganisationCentralisee'
            );
        }

        // L'organisme de rattachement de l'agent est centralisé
        if ($organisationCentralisee) {
            $servicesIds = static::getSousServicesIds($user, $this->entityManager);

            // L'agent est membre d'une entité parent de l'entité renseignée dans la règle de validation
            if (!empty($servicesIds)) {
                $criteriasAgentMembreEntiteRegleValidation[] = (new Expr())
                    ->in($this->rootAlias . '.serviceValidation', $servicesIds);
            }
        }

        $exprBuilder = $this->queryBuilder->expr();

        $filterValue = ConsultationStatus::STATUS['PREPARATION'];

        $expression = $exprBuilder->andX(
            // La consultation est au statut EV (= Préparation)
            // Inutile de tenir compte des multi-statuts car cas impossible pour le statut EV.
            (new Expr())->eq(ConsultationStatutExtension::getStatutCalculeSelectPart($this->rootAlias), $filterValue),
            $exprBuilder->orX(
                $exprBuilder->andX(
                    // Et a pour règle de validation « Type 3 : Validation finale requise par l'Entité ci-après »
                    (new Expr())->eq(
                        $this->rootAlias . '.idRegleValidation',
                        $this->parameterBag->get('REGLE_VALIDATION_TYPE_3')
                    ),
                    (new Expr())->isNull($this->rootAlias . '.serviceValidationIntermediaire'),
                ),
                $exprBuilder->andX(
                    // Et a pour règle de validation « Type 4 : Validation finale requise par l'Entité ci-après »
                    (new Expr())->eq(
                        $this->rootAlias . '.idRegleValidation',
                        $this->parameterBag->get('REGLE_VALIDATION_TYPE_4')
                    ),
                    (new Expr())->isNotNull($this->rootAlias . '.serviceValidationIntermediaire'),
                    (new Expr())->isNotNull($this->rootAlias . '.dateValidationIntermediaire'),
                    (new Expr())->neq($this->rootAlias . '.dateValidationIntermediaire', ':nullDate'),
                    (new Expr())->eq($this->rootAlias . '.etatApprobation', '1')
                )
            ),
            // Et l'agent fait partie d'un service de validation (organisation centralisée/décentralisée)
            (new Expr())->orX(...$criteriasAgentMembreEntiteRegleValidation),
            // Et pas de date de validation
            (new Expr())->orX(
                (new Expr())->isNull($this->rootAlias . '.datevalidation'),
                (new Expr())->eq($this->rootAlias . '.datevalidation', ':nullDate')
            )
        );

        $this->queryBuilder->setParameter('nullDate', '0000-00-00 00:00:00');

        return $expression;
    }

    /**
     * @throws DBALDriverException
     * @throws Exception
     */
    public function getSousServicesIds(UserInterface $user, EntityManagerInterface $entityManager): array
    {
        if (empty($user->getOrganisme())) {
            $allServicesObject = $entityManager->getRepository(Service::class)->findAll();
            return array_map(fn($e) => $e->getId(), $allServicesObject);
        }
        if ($user->getService() instanceof Service) {
            return $entityManager->getRepository(AffiliationService::class)->getServiceDescendantsIds(
                $user->getService()->getId()
            );
        } else {
            return $entityManager->getRepository(AffiliationService::class)
                ->getOrganismeServicesIds($user->getOrganisme()->getAcronyme());
        }
    }

    public function agentTechniqueServices(
        UserInterface $user,
        array $services
    ): ?Func {
        if ($user instanceof Agent) {
            if (!empty($services)) {
                $servicesIds = array_map(fn($service) => $service->getId(), $services);
            }

            if (!empty($servicesIds)) {
                return (new Expr())->in($this->rootAlias . '.service', $servicesIds);
            }
        }

        return null;
    }

    public function agentTechniqueOrganismes(
        UserInterface $user,
        array $organismes
    ): ?Func {
        if ($user instanceof Agent) {
            if (!empty($organismes)) {
                $organismesIds = array_map(fn($organisme) => $organisme->getAcronyme(), $organismes);
            }

            if (!empty($organismesIds)) {
                return (new Expr())->in($this->rootAlias . '.organisme', $organismesIds);
            }
        }

        return null;
    }
}
