<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Input;

use ApiPlatform\Core\DataTransformer\DataTransformerInitializerInterface;
use ApiPlatform\Core\Serializer\AbstractItemNormalizer;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\Input\InscritInput;
use App\Entity\Inscrit;
use App\Service\AtexoConfiguration;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class InscritInputDataTransformer implements DataTransformerInitializerInterface
{
    public function __construct(
        private ValidatorInterface $validator,
        private UserPasswordHasherInterface $encoder,
        private AtexoConfiguration $configuration
    ) {
    }

    /**
     * @param $input
     * @param string $to
     * @param array $context
     * @return Inscrit
     */
    public function transform($input, string $to, array $context = [])
    {
        $this->validator->validate($input);

        $inscrit = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE] ?? new $context['resource_class']();

        $dateNow = new \DateTime();
        $dateCreation = $input->dateCreation;
        $dateModification = $input->dateModification;
        $inscrit->setPrenom($input->prenom);
        $inscrit->setNom($input->prenom);
        $inscrit->setLogin($input->login);
        $inscrit->setAdresse($input->adresse);
        $inscrit->setCodepostal($input->codepostal);
        $inscrit->setVille($input->ville);
        $inscrit->setPays($input->pays);
        $inscrit->setTelephone($input->telephone);
        $inscrit->setBloque($input->bloque);
        $inscrit->setSiret($input->siret);
        $inscrit->setDateCreation($dateCreation->format('Y-m-d H:m:s') ?? $dateNow->format('Y-m-d H:m:s'));
        $inscrit->setDateModification($dateModification->format('Y-m-d H:m:s') ?? $dateNow->format('Y-m-d H:m:s'));
        $inscrit->setEtablissement($input->etablissement);
        $inscrit->setInscritAnnuaireDefense($input->inscritAnnuaireDefense);
        $inscrit->setIdExterne($input->idExterne);
        $inscrit->setEntreprise($input->entreprise);
        $inscrit->setMdp($this->encoder->hashPassword($inscrit, $input->mdp));
        $inscrit->setEmail($input->email);

        return $inscrit;
    }

    /**
     * @param $data
     * @param string $to
     * @param array $context
     * @return bool
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Inscrit) {
            // already transformed
            return false;
        }

        return Inscrit::class === $to && null !== ($context['input']['class'] ?? null);
    }

    public function initialize(string $inputClass, array $context = [])
    {
        $entity = $context['object_to_populate'] ?? null;

        $dto = new InscritInput();
        // not an edit, so just return an empty DTO
        if (!$entity) {
            return $dto;
        }
        if (!$entity instanceof Inscrit) {
            throw new \Exception(sprintf('Unexpected resource class "%s"', get_class($entity)));
        }
        $dateNow = new \DateTime();

        $dto->identifiantTechnique = $entity->getId();
        $dto->nom = $entity->getNom();
        $dto->login = $entity->getLogin();
        $dto->email = $entity->getEmail();
        $dto->adresse = $entity->getAdresse();
        $dto->codepostal = $entity->getCodepostal();
        $dto->ville = $entity->getVille();
        $dto->pays = $entity->getPays();

        $dto->telephone = $entity->getTelephone();
        $dto->bloque = !$entity->getBloque();
        $dto->siren = $entity->getEntreprise()->getSiren();
        $dto->codeEtablissement = $entity->getEtablissement()->getCodeEtablissement();
        $dto->inscritAnnuaireDefense = $entity->getInscritAnnuaireDefense();
        $dto->dateCreation = new \DateTime($entity->getDateCreation()) ?? $dateNow;
        $dto->dateModification = (new \DateTime($entity->getDateModification()) ?? $dateNow);
        $dto->etablissement = $entity->getEtablissement();
        $dto->entreprise = $entity->getEntreprise();

        if ($this->configuration->getConfigurationPlateforme()->isRecueilConsentementRgpd()) {
            $dto->dateModificationRgpd = $entity->getDateValidationRgpd();
            $dto->rgpd = $this->inscritService->getRgpdLabels($entity);
        }

        $dto->idExterne = $entity->getIdExterne();
        $dto->siret = $entity->getSiret();
        $dto->mdp = $entity->getPassword();

        return $dto;
    }
}
