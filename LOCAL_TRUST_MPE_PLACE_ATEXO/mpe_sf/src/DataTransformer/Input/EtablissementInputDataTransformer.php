<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Input;

use ApiPlatform\Core\DataTransformer\DataTransformerInitializerInterface;
use ApiPlatform\Core\Serializer\AbstractItemNormalizer;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\Input\EtablissementInput;
use App\Entity\Etablissement;
use App\Service\Agent\Habilitation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class EtablissementInputDataTransformer implements DataTransformerInitializerInterface
{
    public function __construct(
        private ValidatorInterface $validator
    ) {
    }

    /**
     * @param $input
     * @param string $to
     * @param array $context
     * @return mixed|object
     */
    public function transform($input, string $to, array $context = [])
    {
        $this->validator->validate($input);
        $establishment = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE] ?? new $context['resource_class']();

        $dateNow = new \DateTime();

        $dateCreation = $input->dateCreation;
        $dateModification = $input->dateModification;
        $establishment->setSiret($input->siret);
        $establishment->setEstSiege($input->estSiege);
        $establishment->setCodeEtablissement($input->codeEtablissement);
        $establishment->setIdExterne($input->idExterne);
        $establishment->setEntreprise($input->entreprise);
        $establishment->setAdresse($input->adresse);
        $establishment->setCodePostal($input->codePostal);
        $establishment->setVille($input->ville);
        $establishment->setPays($input->pays);
        $establishment->setDateCreation($dateCreation ?? $dateNow);
        $establishment->setDateModification($dateModification ?? $dateNow);

        return $establishment;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Etablissement) {
            return false;
        }

        return Etablissement::class === $to && !empty($context['input']['class'])
            && $context['input']['class'] === EtablissementInput::class;
    }

    /**
     * @param string $inputClass
     * @param array $context
     * @return object|void|null
     */
    public function initialize(string $inputClass, array $context = [])
    {
        $currentEstablishment = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE] ?? null;

        if (!$currentEstablishment) {
            return new EtablissementInput();
        }

        $dateNow = new \DateTime();

        $establishmentInput = new EtablissementInput();
        $establishmentInput->siret = $currentEstablishment->getSiret();
        $establishmentInput->idEntreprise = $currentEstablishment->getIdEntreprise();
        $establishmentInput->estSiege = $currentEstablishment->getEstSiege();
        $establishmentInput->codeEtablissement = $currentEstablishment->getCodeEtablissement();
        $establishmentInput->idExterne = $currentEstablishment->getIdExterne();
        $establishmentInput->entreprise = $currentEstablishment->getEntreprise();
        $establishmentInput->adresse = $currentEstablishment->getAdresse();
        $establishmentInput->codePostal = $currentEstablishment->getCodePostal();
        $establishmentInput->ville = $currentEstablishment->getVille();
        $establishmentInput->pays = $currentEstablishment->getPays();
        $establishmentInput->dateCreation = (($currentEstablishment->getDateCreation()) ?? $dateNow);
        $establishmentInput->dateModification = (($currentEstablishment->getDateModification()) ?? $dateNow);

        return $establishmentInput;
    }
}
