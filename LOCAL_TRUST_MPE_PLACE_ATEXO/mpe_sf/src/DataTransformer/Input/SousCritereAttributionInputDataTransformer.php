<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Input;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\Consultation\SousCritereAttribution;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Input\SousCritereAttributionInput;

final class SousCritereAttributionInputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private readonly ValidatorInterface $validator
    ) {
    }

    /**
     * @param SousCritereAttributionInput $object
     */
    public function transform($object, string $to, array $context = []): SousCritereAttribution
    {
        $this->validator->validate($object);

        $sousCritereAttribution = new SousCritereAttribution();
        $sousCritereAttribution->setCritereAttribution($object->critereAttribution);
        $sousCritereAttribution->setEnonce($object->enonce);
        $sousCritereAttribution->setPonderation($object->ponderation);

        return $sousCritereAttribution;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof SousCritereAttribution) {
            return false;
        }

        return SousCritereAttribution::class === $to && null !== ($context['input']['class'] ?? null);
    }
}
