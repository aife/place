<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Input;

use ApiPlatform\Core\DataTransformer\DataTransformerInitializerInterface;
use ApiPlatform\Core\Serializer\AbstractItemNormalizer;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\Input\OrganismeInput;
use App\Entity\Organisme;

class OrganismeInputDataTransformer implements DataTransformerInitializerInterface
{
    public function __construct(
        private ValidatorInterface $validator
    ) {
    }

    public function transform($object, string $to, array $context = [])
    {
        $this->validator->validate($object);
        $organisme = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE] ?? new $context['resource_class']();
        $organisme->setAcronyme($object->acronyme);
        $organisme->setDenominationOrg($object->denominationOrg);
        $organisme->setAdresse($object->denominationOrg);
        $organisme->setCp($object->codepostal);
        $organisme->setPays($object->pays);
        $organisme->setVille($object->ville);
        $organisme->setEmail($object->email);
        $organisme->setSigle($object->sigle);
        $organisme->setDescriptionOrg($object->description);
        $organisme->setIdExterne($object->idExterne);
        $organisme->setComplement($object->complement);

        return $organisme;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {

        if ($data instanceof Organisme) {
            return false;
        }

        return Organisme::class === $to && null !== ($context['input']['class'] ?? null);
    }

    public function initialize(string $inputClass, array $context = [])
    {
        $entity = $context['object_to_populate'] ?? null;

        $dto = new OrganismeInput();

        if (!$entity) {
            return $dto;
        }
        if (!$entity instanceof Organisme) {
            throw new \Exception(sprintf('Unexpected resource class "%s"', get_class($entity)));
        }

        $dto->acronyme = $entity->getAcronyme();
        $dto->adresse = $entity->getAdresse();
        $dto->codepostal = $entity->getCp();
        $dto->ville = $entity->getVille();
        $dto->adresse = $entity->getAdresse();
        $dto->pays = $entity->getPays();
        $dto->ville = $entity->getVille();
        $dto->email = $entity->getEmail();
        $dto->denominationOrg = $entity->getDenominationOrg();
        $dto->active = $entity->getActive();
        $dto->sigle = $entity->getSigle();
        $dto->description = $entity->getDescriptionOrg();
        $dto->idExterne = $entity->getIdExterne();
        $dto->complement = $entity->getComplement();

        return $dto;
    }
}
