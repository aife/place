<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Input;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\Consultation\CritereAttribution;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Input\CritereAttributionInput;

final class CritereAttributionInputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private readonly ValidatorInterface $validator
    ) {
    }

    /**
     * @param CritereAttributionInput $object
     */
    public function transform($object, string $to, array $context = []): CritereAttribution
    {
        $this->validator->validate($object);

        $critereAttribution = new CritereAttribution();
        $critereAttribution->setDonneeComplementaire($object->donneeComplementaire);
        $critereAttribution->setLot($object->lot);
        $critereAttribution->setEnonce($object->enonce);
        $critereAttribution->setPonderation($object->ponderation);
        $critereAttribution->setOrdre($object->ordre);

        return $critereAttribution;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof CritereAttribution) {
            return false;
        }

        return CritereAttribution::class === $to && null !== ($context['input']['class'] ?? null);
    }
}
