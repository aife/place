<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Input;

use App\Service\Consultation\ConsultationService;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use DateTime;
use ApiPlatform\Core\DataTransformer\DataTransformerInitializerInterface;
use ApiPlatform\Core\Serializer\AbstractItemNormalizer;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\Input\AgentInput;
use App\Entity\Agent;

class AgentInputDataTransformer implements DataTransformerInitializerInterface
{
    public function __construct(
        private readonly UserPasswordHasherInterface $encoder,
        private readonly ValidatorInterface $validator,
        private readonly ConsultationService $consultationService,
    ) {
    }

    /**
     * @param AgentInput $object
     */
    public function transform($object, string $to, array $context = [])
    {
        $this->validator->validate($object);
        $agent = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE] ?? new $context['resource_class']();
        if (null === $agent->getId()) {
            $agent->setDateCreation((new DateTime())->format('Y-m-d H:i:s'));
        }
        $agent->setIdentifiant($object->login);
        if (strtolower($context['collection_operation_name']) === "post") {
            $agent->setPassword($this->encoder->hashPassword($agent, $object->password));
        }
        $agent->setEmail($object->email);
        $agent->setNom($object->nom);
        $agent->setPrenom($object->prenom);
        $agent->setOrganisme($object->organisme);
        $agent->setIdExterne($object->idExterne);
        $agent->setActif($object->actif ? '1' : '0');
        $agent->setTelephone($object->telephone);
        $agent->setFax($object->fax);
        $agent->setLieuExecution($this->consultationService->getLieuExecution($object->lieuExecution));

        $agent->setService($object->service);
        $agent->setDateModification((new DateTime())->format('Y-m-d H:i:s'));

        return $agent;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Agent) {
            return false;
        }

        return Agent::class === $to && !empty($context['input']['class'])
            && $context['input']['class'] === AgentInput::class;
    }

    /**
     * @param string $inputClass
     * @param array $context
     * @return object|void|null
     */
    public function initialize(string $inputClass, array $context = [])
    {
        /** @var  Agent $currentAgent */
        $currentAgent = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE] ?? null;

        if (!$currentAgent) {
            return new AgentInput();
        }

        $agentInput = new AgentInput();
        $agentInput->login = $currentAgent->getLogin();
        $agentInput->email = $currentAgent->getEmail();
        $agentInput->password = $currentAgent->getPassword();
        $agentInput->nom = $currentAgent->getNom();
        $agentInput->prenom = $currentAgent->getPrenom();
        $agentInput->organisme = $currentAgent->getOrganisme();
        $agentInput->idExterne = $currentAgent->getIdExterne();
        $agentInput->actif = $currentAgent->getActif();
        $agentInput->telephone = $currentAgent->getTelephone();
        $agentInput->fax = $currentAgent->getNumFax();
        $agentInput->service = $currentAgent->getService();
        $agentInput->lieuExecution = $this->consultationService->getDepartmentsWithLieuExecution(
            $currentAgent->getLieuExecution()
        );

        return $agentInput;
    }
}
