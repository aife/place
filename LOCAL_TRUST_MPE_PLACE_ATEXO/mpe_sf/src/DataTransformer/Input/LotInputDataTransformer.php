<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Input;

use ApiPlatform\Core\DataTransformer\DataTransformerInitializerInterface;
use ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface;
use ApiPlatform\Core\Serializer\AbstractItemNormalizer;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\Input\LotInput;
use App\Entity\Agent;
use App\Entity\Lot;
use App\Service\ClausesService;
use App\Service\LotService;
use Symfony\Component\Security\Core\Security;

final class LotInputDataTransformer implements DataTransformerInitializerInterface
{
    public const ITEM_OPERATION_NAME = 'item_operation_name';

    public function __construct(
        private readonly Security $security,
        private readonly ValidatorInterface $validator,
        private readonly LotService $lotService,
        private readonly ClausesService $clausesService,
        private readonly ResourceMetadataFactoryInterface $resourceMetadataFactory
    ) {
    }

    /**
     * {@inheritdoc}
     * @param LotInput $object
     */
    public function transform($object, string $to, array $context = []): Lot
    {
        $resourceMetadata = $this->resourceMetadataFactory->create($context['resource_class']);
        $validationGroups = $resourceMetadata->getOperationAttribute($context, 'validation_groups', null, true);

        $this->validator->validate($object, ['groups' => $validationGroups]);

        $lot = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE] ?? null;

        if (!$lot) {
            /** @var Agent $user */
            $user = $this->security->getUser();

            $lot = new Lot();
            $lot->setConsultation($object->consultation);
            $lot->setDonneeComplementaire($object->donneeComplementaire);
            $lot->setOrganisme($object->organisme?->getAcronyme() ? $object->organisme : $user->getOrganisme());
        }

        $lot->setLot($object->numero);
        $lot->setDescription($object->intitule);
        $lot->setDescriptionDetail($object->descriptionSuccinte);
        $lot->setCategorie($object->naturePrestation);
        $lot->setCodeCpv1($object->codeCpvPrincipal);
        $lot->setCodeCpv2($this->lotService->getConcatenatedCodeCpv($object));
        $lot->setIdTrDescription($object->idTraductionDescription);
        $lot->setIdTrDescriptionDetail($object->idTraductionDescriptionDetail);
        if (null !== $object->marcheInsertion) {
            $lot->setMarcheInsertion($object->marcheInsertion);
        }

        if (null !== $object->clauseSpecificationTechnique) {
            $lot->setClauseEnvSpecsTechniques($object->clauseSpecificationTechnique ? '1' : '0');
        }

        if (is_array($object->clauses)) {
            $this->clausesService->updateClauses($lot, $object->clauses);
        }

        return $lot;
    }

    /**
     * {@inheritdoc}
     */
    public function initialize(string $inputClass, array $context = [])
    {
        /** @var Lot $existingLot */
        $existingLot = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE] ?? null;
        if (!$existingLot) {
            return new LotInput();
        }

        $lotInput = new LotInput();
        $lotInput->numero = $existingLot->getLot();
        $lotInput->consultation = $existingLot->getConsultation();
        $lotInput->intitule = $existingLot->getDescription();
        $lotInput->descriptionSuccinte = $existingLot->getDescriptionDetail();
        $lotInput->naturePrestation = $existingLot->getCategorie();
        $lotInput->codeCpvPrincipal = $existingLot->getCodeCpv1();
        $lotInput->donneeComplementaire = $existingLot->getDonneeComplementaire();
        $lotInput->idTraductionDescription = $existingLot->getIdTrDescription();
        $lotInput->idTraductionDescriptionDetail = $existingLot->getIdTrDescriptionDetail();

        $codeCpv2 = explode('#', $existingLot->getCodeCpv2());
        if (!empty($codeCpv2[1])) {
            $lotInput->codeCpvSecondaire1 = $codeCpv2[1];
        }
        if (!empty($codeCpv2[2])) {
            $lotInput->codeCpvSecondaire2 = $codeCpv2[2];
        }
        if (!empty($codeCpv2[3])) {
            $lotInput->codeCpvSecondaire3 = $codeCpv2[3];
        }

        $lotInput->clauses = $existingLot->getClausesN1();

        return $lotInput;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Lot) {
            return false;
        }

        return Lot::class === $to && null !== ($context['input']['class'] ?? null);
    }
}
