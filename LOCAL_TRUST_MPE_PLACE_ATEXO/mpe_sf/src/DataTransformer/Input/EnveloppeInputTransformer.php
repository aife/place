<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Input;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Input\EnveloppeInput;
use App\Dto\Input\FichierEnveloppeInput;
use App\Entity\Enveloppe;
use App\Entity\EnveloppeFichier;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use ApiPlatform\Core\Validator\ValidatorInterface;

final class EnveloppeInputTransformer implements DataTransformerInterface
{
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly ValidatorInterface $validator
    ) {
    }

    /**
     * @param EnveloppeInput $object
     * @inheritDoc
     */
    public function transform($object, string $to, array $context = [])
    {
        $this->validator->validate($object);

        $enveloppe = new Enveloppe();
        $enveloppe->setOffre($object->offre);
        $enveloppe->setTypeEnv($this->parameterBag->get('TYPE_ENV_' . $object->type));
        $enveloppe->setSousPli($object->numeroLot)
            ->setChampsOptionnels('')
            ->setChampsOptionnels('')
            ->setDonneesOuverture('')
            ->setHorodatageDonneesOuverture('')
            ->setNomAgentOuverture('');

        $enveloppe->setOrganisme($object->offre->getConsultation()->getAcronymeOrg());
        $fileOrderNumber = 0;

        foreach ($object->fichiersEtSignature as $file) {
            $this->validator->validate($file);
            $fichierSignatures = null;
            if (!empty($file->idSignature)) {
                $fileOrderNumber++;
                $fichierSignatures = $this->createFichierEnveloppe(
                    $file,
                    $fileOrderNumber,
                    EnveloppeFichier::TYPE_FICHIER_SIGNATURE
                );
                $enveloppe->addFichierEnveloppe($fichierSignatures);
            }

            $fileOrderNumber++;
            $enveloppeFichier = $this->createFichierEnveloppe(
                $file,
                $fileOrderNumber,
                $file->typeFichier
            );

            if ($fichierSignatures instanceof EnveloppeFichier) {
                $enveloppeFichier->setBlobSignature($file->idSignature->getMedia());
            }

            $enveloppe->addFichierEnveloppe($enveloppeFichier);
        }

        return $enveloppe;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Enveloppe) {
            return false;
        }

        return Enveloppe::class === $to && null !== ($context['input']['class'] ?? null);
    }

    private function createFichierEnveloppe(FichierEnveloppeInput $file, int $orderFile, string $fileType): EnveloppeFichier
    {
        $fileFullPath = $this->parameterBag->get('BASE_ROOT_DIR')
            . $file->idFichier->getMedia()->getChemin()
            . $file->idFichier->getMedia()->getId() . '-0' . $file->idFichier->getMedia()->getExtension();

        $enveloppeFichier = new EnveloppeFichier();
        $enveloppeFichier
            ->setOrganisme($file->idFichier->getMedia()->getOrganisme())
            ->setNumOrdreFichier($orderFile)
            ->setNomFichier($file->idFichier->getMedia()->getName())
            ->setTypeFichier($fileType)
            ->setTypePiece(EnveloppeFichier::TYPE_PIECE[$fileType]['id'])
            ->setTailleFichier(file_exists($fileFullPath) ? filesize($fileFullPath) : 0)
            ->setHash($file->idFichier->getMedia()->getHash())
            ->setBlob($file->idFichier->getMedia())
            ->setHash256(file_exists($fileFullPath) ? hash_file('sha256', $fileFullPath) : '');

        return $enveloppeFichier;
    }
}
