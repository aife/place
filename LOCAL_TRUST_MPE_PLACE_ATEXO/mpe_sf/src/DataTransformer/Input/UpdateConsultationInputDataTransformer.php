<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Input;

use ApiPlatform\Core\DataTransformer\DataTransformerInitializerInterface;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\Input\UpdateConsultationInput;
use App\Entity\Consultation;
use App\Repository\ConfigurationPlateformeRepository;
use App\Repository\Procedure\TypeProcedureOrganismeRepository;
use App\Service\Agent\ConsultationSimplifieeService;
use App\Service\ClausesService;
use App\Service\Consultation\ConsultationService;
use App\Service\DataToolBoxTrait;
use App\Service\DonneeComplementaireService;
use App\Service\LotService;
use DateTimeZone;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use App\Event\ModificationConsultationEvent;

final class UpdateConsultationInputDataTransformer implements DataTransformerInitializerInterface
{
    use DataToolBoxTrait;

    public function __construct(
        private readonly ValidatorInterface $validator,
        private readonly ParameterBagInterface $parameterBag,
        private readonly ConsultationService $consultationService,
        private readonly ClausesService $clausesService,
        private readonly ConfigurationPlateformeRepository $configurationPlateformeRepository,
        private readonly DonneeComplementaireService $donneeComplementaireService,
        private readonly LotService $lotService,
        private readonly TypeProcedureOrganismeRepository $typeProcedureOrganismeRepository
    ) {
    }

    /**
     * {@inheritdoc}
     * @param UpdateConsultationInput $object
     */
    public function transform($object, string $to, array $context = []): Consultation
    {
        $this->validator->validate($object);


        /** @var  Consultation $consultation */
        $consultation = $context[AbstractNormalizer::OBJECT_TO_POPULATE];

        $consultation->setPoursuivreAffichage($object->poursuivreAffichage);
        $consultation->setPoursuivreAffichageUnite($object->poursuivreAffichageUnite);

        if ($object->dateLimiteRemisePlis) {
            if ($object->dateLimiteRemisePlis instanceof \DateTimeImmutable) {
                $copyDate = \DateTime::createFromImmutable($object->dateLimiteRemisePlis);
                $copyDate->setTimezone(new DateTimeZone(date_default_timezone_get()));
                $object->dateLimiteRemisePlis = $copyDate;
            }

            $consultation->setDatefin($object->dateLimiteRemisePlis);
            $consultation->setDateFinUnix(
                strtotime(
                    $object->dateLimiteRemisePlis->format('Y-m-d H:i:s')
                    . ' + '
                    . $consultation->getPoursuivreAffichage()
                    . ' '
                    . $consultation->getPoursuivreAffichageUnite()
                )
            );
        }
        if (!is_null($object->dateLimiteRemisePlisLocale) && !empty($object->dateLimiteRemisePlisLocale)) {
            if ($object->dateLimiteRemisePlisLocale instanceof \DateTimeImmutable) {
                $copyDate = \DateTime::createFromImmutable($object->dateLimiteRemisePlisLocale);
                $copyDate->setTimezone(new DateTimeZone(date_default_timezone_get()));
                $object->dateLimiteRemisePlisLocale = $copyDate;
            }
            $dateLocale = $this->getDisplayedDateTime($object->dateLimiteRemisePlisLocale);
            if (!empty($dateLocale)) {
                $dateLimiteRemisePlisLocale = (new \DateTime($dateLocale))
                    ->format('Y-m-d H:i:s');
                $consultation->setDateFinLocale($dateLimiteRemisePlisLocale);
            }
            $consultation->setLieuResidence($object->lieuResidence);
        }

        $consultation->setAdresseRetraisDossiers($object->adresseRetraisDossiers);
        $consultation->setAdresseDepotOffres($object->adresseDepotOffres);
        $consultation->setLieuOuverturePlis($object->lieuOuverturePlis);
        $consultation->setPartialDceDownload($object->dcePartialDownload ? '1' : '0');

        //Tout ce qui est au dessus d'ici peut etre modifié quand la consultation est en ligne
        if ((int)$consultation->getCalculatedStatus() >= (int)$this->parameterBag->get('STATUS_CONSULTATION')) {
            return $consultation;
        }

        $consultation->setCategorieConsultation($object->naturePrestation);
        $consultation->setReferenceUtilisateur($object->reference);
        $consultation->setIntitule($object->intitule);
        $consultation->setObjet($object->objet);
        $consultation->setChampSuppInvisible($object->commentaireInterne);

        if ($object->alloti && $consultation->getAlloti() == '0') {
            $this->donneeComplementaireService->deleteCritereAttribution($consultation->getDonneeComplementaire());
        } elseif (!$object->alloti && $consultation->getAlloti() == '1') {
            $this->donneeComplementaireService->deleteCritereAttribution($consultation->getDonneeComplementaire());
            $this->lotService->removeLotsFromConsultation($consultation);
        }
        $consultation->setAlloti($object->alloti ? '1' : '0');

        $config = $this->configurationPlateformeRepository->getConfigurationPlateforme();
        if ($config->getPublicite() && $config->getAfficherValeurEstimee() && $object->valeurEstimee) {
            $consultation = $this->consultationService->setMontantMarcheForInitialization(
                $consultation,
                $object->valeurEstimee
            );
        }
        if (!empty($object->lieuxExecution[0])) {
            $consultation = $this->consultationService->setLieuxExecutionForInitialization(
                $consultation,
                $object->lieuxExecution
            );
        }

        $conf = $this->configurationPlateformeRepository->getConfigurationPlateforme();
        if ($conf->getCodeNutLtReferentiel()) {
            $nuts = trim(implode('#', $object->codesNuts), '#');
            $consultation->setCodesNuts("#{$nuts}");
        }

        $codeCpv2 = trim(implode('#', [
            $object->codeCpvSecondaire1,
            $object->codeCpvSecondaire2,
            $object->codeCpvSecondaire3
        ]), '#');
        $consultation->setCodeCpv1($object->codeCpvPrincipal);
        $consultation->setCodeCpv2("#{$codeCpv2}#");
        $consultation->setCodeExterne($object->codeExterne);

        if ($object->dateLimiteRemisePlis) {
            if ($object->dateLimiteRemisePlis instanceof \DateTimeImmutable) {
                $copyDate = \DateTime::createFromImmutable($object->dateLimiteRemisePlis);
                $copyDate->setTimezone(new DateTimeZone(date_default_timezone_get()));
                $object->dateLimiteRemisePlis = $copyDate;
            }
            $consultation->setDatefin($object->dateLimiteRemisePlis);
            $consultation->setDateFinUnix(
                strtotime(
                    $object->dateLimiteRemisePlis->format('Y-m-d H:i:s')
                    . ' + '
                    . $consultation->getPoursuivreAffichage()
                    . ' '
                    . $consultation->getPoursuivreAffichageUnite()
                )
            );
        }
        if ($object->dateMiseEnLigneCalcule) {
            $consultation->setDateMiseEnLigneCalcule($object->dateMiseEnLigneCalcule);
        }

        $consultation->setReferenceConsultationParent($object->consultationParent);
        $consultation->setOrganismeConsultationInit($object->consultationParent?->getOrganisme() ?? '');
        $consultation->setEntiteAdjudicatrice($object->entiteAdjudicatrice);

        $consultation->setDonneeComplementaireObligatoire(
            $object->donneeComplementaireObligatoire ? '1' : '0'
        );
        $consultation->setTypeFormulaireDume($object->dumeSimplifie ? '1' : '0');
        $consultation->setAutreTechniqueAchat($object->autreTechniqueAchat);
        $consultation->setNumeroAc($object->numeroSad);
        $consultation->setDonneePubliciteObligatoire(
            $object->donneePubliciteObligatoire ? '1' : '0'
        );

        if ($config->getCaseAttestationConsultation()) {
            $consultation->setAttestationConsultation($object->attestationConsultation ? '1' : '0');
        }

        $consultation->setTypeAcces(
            $object->consultationRestreinte
                ? Consultation::TYPE_ACCES_RESTREINT
                : Consultation::TYPE_ACCES_PUBLIC
        );

        $consultation->setCodeProcedure($object->codeProcedure);
        $consultation->setProcedureOuverte($object->procedureOuverte);
        $consultation->setGroupement($object->groupement);
        if (null !== $object->justificationNonAlloti) {
            $consultation = $this->consultationService->setJustificationNonAlloti(
                $consultation,
                $object->justificationNonAlloti
            );
        }

        if (is_array($object->clauses)) {
            $this->clausesService->updateClauses($consultation, $object->clauses);
        }

        $consultation->setReferentielAchat($object->referentielAchat);

        if (false === $object->etatEnAttenteValidation) {
            $consultation->setEtatEnAttenteValidation($object->etatEnAttenteValidation);
            $consultation->setNumeroPhase(ConsultationSimplifieeService::PHASE_BROUILLON);
        }

        if (null !== $object->besoinRecurrent) {
            $consultation->setBesoinRecurrent($object->besoinRecurrent);
        }

        if (null !== $object->technicalReference) {
            $consultation->setReference($object->technicalReference);
        }

        if (null !== $object->modeOuvertureReponse) {
            $consultation->setModeOuvertureReponse($object->modeOuvertureReponse);
        }

        if (null !== $object->reponseElectronique) {
            if (
                $object->reponseElectronique === ConsultationService::TYPE_REPONSE_AUTORISEE
                || $object->reponseElectronique === ConsultationService::TYPE_REPONSE_OBLIGATOIRE
            ) {
                $consultation->setAutoriserReponseElectronique('1');
            } else {
                $consultation->setAutoriserReponseElectronique('0');
            }

            if ($object->reponseElectronique === ConsultationService::TYPE_REPONSE_OBLIGATOIRE) {
                $consultation->setReponseObligatoire('1');
            } else {
                $consultation->setReponseObligatoire('0');
            }
        }

        if (null !== $object->enveloppeAnonyme) {
            $consultation->setEnvAnonymat($object->enveloppeAnonyme ? '1' : '0');
        }

        if (null !== $object->enveloppeCandidature) {
            $consultation->setEnvCandidature($object->enveloppeCandidature ? '1' : '0');
        }

        if (null !== $object->enveloppeOffre) {
            $consultation->setEnvOffre($object->enveloppeOffre ? '1' : '0');
        }

        if (null !== $object->signatureActeEngagement) {
            $consultation->setSignatureActeEngagement($object->signatureActeEngagement);
        }

        if (null !== $object->marchePublicSimplifie) {
            $consultation->setMarchePublicSimplifie($object->marchePublicSimplifie);
        }

        if (null !== $object->urlExterne) {
            $consultation->setUrlConsultationExterne($object->urlExterne);
        }

        if (null !== $object->chiffrement) {
            $consultation->setChiffrementOffre($object->chiffrement ? '1' : '0');
        }

        return $consultation;
    }

    /**
     * {@inheritdoc}
     */
    public function initialize(string $inputClass, array $context = [])
    {
        /** @var  Consultation $existingConsultation */
        $existingConsultation = $context[AbstractNormalizer::OBJECT_TO_POPULATE];

        $consultationInput = new UpdateConsultationInput();
        $consultationInput->naturePrestation = $existingConsultation->getCategorieConsultation();
        $consultationInput->reference = $existingConsultation->getReferenceUtilisateur();
        $consultationInput->intitule = $existingConsultation->getIntitule();
        $consultationInput->objet = $existingConsultation->getObjet();
        $consultationInput->commentaireInterne = $existingConsultation->getChampSuppInvisible() ?? '';
        $consultationInput->alloti = (bool)$existingConsultation->getAlloti();
        $consultationInput->valeurEstimee = $existingConsultation->getDonneeComplementaire()?->getMontantMarche();
        $consultationInput->attestationConsultation = $existingConsultation->getAttestationConsultation();
        $consultationInput->codesNuts = $this->consultationService->getDepartmentsWithCodeNuts($existingConsultation);

        $consultationInput->codeCpvPrincipal = $existingConsultation->getCodeCpv1();
        $codeCpv2 = explode('#', $existingConsultation->getCodeCpv2());
        if (!empty($codeCpv2[1])) {
            $consultationInput->codeCpvSecondaire1 = $codeCpv2[1];
        }
        if (!empty($codeCpv2[2])) {
            $consultationInput->codeCpvSecondaire2 = $codeCpv2[2];
        }
        if (!empty($codeCpv2[3])) {
            $consultationInput->codeCpvSecondaire3 = $codeCpv2[3];
        }

        $consultationInput->codeExterne = $existingConsultation->getCodeExterne();
        $consultationInput->dateLimiteRemisePlis = $existingConsultation->getDatefin();
        $consultationInput->dateMiseEnLigneCalcule = $existingConsultation->getDateMiseEnLigneCalcule();
        $consultationInput->consultationParent = $existingConsultation->getReferenceConsultationParent();
        $consultationInput->entiteAdjudicatrice = $existingConsultation->getEntiteAdjudicatrice();
        $consultationInput->organisme = $existingConsultation->getOrganisme();
        $typeProcedureOrganisme = $this->typeProcedureOrganismeRepository->findOneBy([
            'idTypeProcedure'   => $existingConsultation->getIdTypeProcedureOrg(),
            'organisme'         => $existingConsultation->getAcronymeOrg()
        ]);
        $consultationInput->typeProcedure = $typeProcedureOrganisme;

        $consultationInput->lieuxExecution = [0];
        $consultationInput->clauses = $existingConsultation->getClausesN1();

        $consultationInput->donneeComplementaireObligatoire = $existingConsultation
            ->getDonneeComplementaireObligatoire();
        $consultationInput->dumeSimplifie = $existingConsultation->getTypeFormulaireDume();
        $consultationInput->entiteAdjudicatrice = $existingConsultation->getEntiteAdjudicatrice();
        $consultationInput->entiteAdjudicatrice = $existingConsultation->getEntiteAdjudicatrice();
        $consultationInput->autreTechniqueAchat = $existingConsultation->getAutreTechniqueAchat();
        $consultationInput->numeroSad = $existingConsultation->getNumeroAc();
        $consultationInput->donneePubliciteObligatoire = $existingConsultation->getDonneePubliciteObligatoire();
        $consultationInput->consultationRestreinte =
            Consultation::TYPE_ACCES_RESTREINT === $existingConsultation->getTypeAcces();
        $consultationInput->codeProcedure = $existingConsultation->getCodeProcedure();
        $consultationInput->procedureOuverte = $existingConsultation->isProcedureOuverte();
        $consultationInput->groupement = $existingConsultation->isGroupement();
        $consultationInput->justificationNonAlloti = $existingConsultation->getDonneeComplementaire()
            ?->getJustificationNonAlloti();
        $consultationInput->referentielAchat = $existingConsultation->getReferentielAchat();
        $consultationInput->adresseRetraisDossiers = $existingConsultation->getAdresseRetraisDossiers();
        $consultationInput->adresseDepotOffres = $existingConsultation->getAdresseDepotOffres();
        $consultationInput->lieuOuverturePlis =  $existingConsultation->getLieuOuverturePlis();
        $consultationInput->poursuivreAffichage =  $existingConsultation->getPoursuivreAffichage();
        $consultationInput->poursuivreAffichageUnite =  $existingConsultation->getPoursuivreAffichageUnite();
        $consultationInput->dcePartialDownload = (bool)$existingConsultation->getPartialDceDownload();
        $consultationInput->technicalReference = $existingConsultation->getReference();
        $consultationInput->modeOuvertureReponse = $existingConsultation->getModeOuvertureReponse();
        $consultationInput->enveloppeAnonyme = $existingConsultation->getEnvAnonymat();
        $consultationInput->enveloppeCandidature = $existingConsultation->getEnvCandidature();
        $consultationInput->enveloppeOffre = $existingConsultation->getEnvOffre();
        $consultationInput->signatureActeEngagement = $existingConsultation->getSignatureActeEngagement();
        $consultationInput->marchePublicSimplifie = $existingConsultation->getMarchePublicSimplifie();
        $consultationInput->urlExterne = $existingConsultation->getUrlConsultationExterne();

        return $consultationInput;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Consultation) {
            return false;
        }

        return Consultation::class === $to && !empty($context['input']['class'])
            && $context['input']['class'] == UpdateConsultationInput::class;
    }
}
