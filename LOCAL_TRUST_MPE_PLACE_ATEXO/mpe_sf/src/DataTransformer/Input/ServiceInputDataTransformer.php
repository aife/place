<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Input;

use ApiPlatform\Core\DataTransformer\DataTransformerInitializerInterface;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Serializer\AbstractItemNormalizer;
use App\Dto\Input\ServiceInput;
use App\Entity\Organisme;
use App\Entity\Service;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;
use ApiPlatform\Core\Validator\ValidatorInterface;

class ServiceInputDataTransformer implements DataTransformerInitializerInterface
{
    public function __construct(
        private ValidatorInterface $validator
    ) {
    }

    public function transform($input, string $to, array $context = [])
    {
        $this->validator->validate($input);
        $service = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE] ?? new $context['resource_class']();

        $service->setSigle($input->sigle);
        $service->setSiren($input->siren);
        $service->setCp($input->codepostal);
        $service->setPays($input->adresse);
        $service->setVille($input->ville);
        $service->setMail($input->email);
        $service->setIdParent($input->idParent);
        $service->setOrganisme($input->organisme);
        $service->setLibelle($input->libelle);
        $service->setIdExterne($input->idExterne);
        $service->setFormeJuridique($input->formeJuridique);
        $service->setFormeJuridiqueCode($input->formeJuridiqueCode);
        $service->setComplement($input->complement);

        return $service;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Service) {
            return false;
        }
        return Service::class === $to && null !== ($context['input']['class'] ?? null);
    }

    public function initialize(string $inputClass, array $context = [])
    {
        $entity = $context['object_to_populate'] ?? null;
        $dto = new ServiceInput();
        if (!$entity) {
            return $dto;
        }
        if (!$entity instanceof Service) {
            throw new \Exception(sprintf('Unexpected resource class "%s"', get_class($entity)));
        }

        $dto->idExterne = $entity->getIdExterne();
        $dto->sigle = $entity->getSigle();
        $dto->siren = $entity->getSiren();
        $dto->email = $entity->getMail();
        $dto->libelle = $entity->getLibelle();
        $dto->adresse = $entity->getAdresse();
        $dto->codepostal = $entity->getCp();
        $dto->ville = $entity->getVille();
        $dto->idParent = $entity->getIdParent();
        $dto->formeJuridique = $entity->getFormeJuridique();
        $dto->formeJuridique = $entity->getFormeJuridiqueCode();
        $dto->complement = $entity->getComplement();
        $dto->organisme = $entity->getOrganisme();

        return $dto;
    }
}
