<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Input\Chorus;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\Input\Chorus\ChorusEchangeContratInput;
use App\Dto\Input\Chorus\ChorusEchangeInput;
use App\Dto\Input\Chorus\ChorusFicheModificativeInput;
use App\Entity\Agent;
use App\Entity\Chorus\ChorusEchange;
use App\Entity\Chorus\ChorusFicheModificative;
use App\Entity\ContratTitulaire;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\TypeContrat;
use App\Exception\ApiProblemInternalException;
use App\Service\AtexoEchangeChorus;
use App\Service\Chorus\FicheModificative;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ChorusEchangeInputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ValidatorInterface $validator,
        private readonly AtexoEchangeChorus $atexoEchangeChorusService,
        private ParameterBagInterface $parameterBag,
        private FicheModificative $ficheModificativeService
    ) {
    }

    /**
     * @var ChorusEchangeInput $object
     */
    public function transform($object, string $to, array $context = []): ChorusEchange
    {
        $this->validator->validate($object);

        /** @var ChorusEchangeContratInput $contrat */
        $contrat = $object->echangeChorusContrat;

        //case create new chorus echange
        $todayDate = (new \DateTime())->format('Y-m-d H:i:s');
        if (strtolower($context['collection_operation_name']) === "create") {
            if (!is_null($object->agentId)) {
                $agent = $this->entityManager
                    ->getRepository(Agent::class)
                    ->findOneBy(['id' => $object->agentId]);
            }
            $echangeChorus = new ChorusEchange();
            $echangeChorus->setDateCreation($todayDate)
                ->setStatutEchange('brouillon');

            $echangeChorus = $this->setDonneesContrat($object, $echangeChorus);
            $echangeChorus = $this->setDataFromLastEchange($object, $echangeChorus, $agent);

            $this->entityManager->beginTransaction();
            $this->entityManager->persist($echangeChorus);
            $this->entityManager->flush();
        } elseif ($context['item_operation_name'] === "update") {//create ficheModificative
            if (is_null($object->acte)) {
                throw new ApiProblemInternalException();
            }

            $echangeChorus = $this->entityManager
                ->getRepository(ChorusEchange::class)
                ->findOneBy(['id' => $object->id]);

            if (!$echangeChorus) {
                throw new ApiProblemInternalException();
            }
            $echangeChorus->setDateModification($todayDate);

            $chorusFicheModificative = $this->setChorusFicheModificative($object->acte, $echangeChorus);
            $this->ficheModificativeService->generatePdfFicheModificative(
                $chorusFicheModificative,
                $contrat->numEj,
                $contrat->referenceLibre,
                $contrat->montant,
                $contrat->idTitulaire
            );
        }

        $echangeChorus->setStatutEchange($this->parameterBag->get('STATUT_ECHANGE_CHORUS_FLUX_A_GENERER'));

        $this->entityManager->flush();

        return $echangeChorus;
    }

    public function setChorusFicheModificative(
        ChorusFicheModificativeInput $acte,
        ChorusEchange $echangeChorus
    ): ChorusFicheModificative {
        $chorusFicheModificative = new ChorusFicheModificative();

        $chorusFicheModificative->setDateFinMarcheModifie($acte->dateFinMarcheModifie)
            ->setTauxTva($acte->tauxTVA)
            ->setOrganisme($echangeChorus->getOrganisme())
            ->setMontantActe($acte->montantActe)
            ->setMontantMarche($acte->montantMarche)
            ->setIdEchange($echangeChorus->getId())
            ->setDateCreation($acte->dateCreation)
            ->setDateModification($acte->dateModification)
            ->setDatePrevueNotification($acte->dateNotificationPrevisionnelle);

        $chorusFicheModificative->setVisaPrefet($this->encodeVisaResponse($acte->visaPrefet));
        $chorusFicheModificative->setVisaAccf($this->encodeVisaResponse($acte->visaACCF));

        if (!is_null($acte->typeFournisseurEntreprise)) {
            $acteArray = (array) $acte;
            $chorusFicheModificative = $this->atexoEchangeChorusService
                ->setFournisseur($acteArray, $chorusFicheModificative);
        }

        $chorusFicheModificative->setRemarque($acte->commentaire)
            ->setTypeModification($acte->typeModification)
            ->setDateFinMarche($acte->datefinMarche);

        $this->entityManager->persist($chorusFicheModificative);
        $this->entityManager->flush();

        return $chorusFicheModificative;
    }

    public function encodeVisaResponse($visaType): int
    {
        $visa = 0;
        if (!is_null($visaType) && in_array($visaType, ['OUI', 'NON'])) {
            $visa = ($visaType === 'OUI') ? 1 : 2;
        }

        return $visa;
    }

    public function setDataFromLastEchange(
        ChorusEchangeInput $object,
        ChorusEchange $echangeChorus,
        Agent $agent = null
    ): ChorusEchange {
        if (!is_null($echangeChorus->getIdDecision())) {
            $lastEchange = $this->entityManager
                ->getRepository(ChorusEchange::class)
                ->findOneBy(
                    [
                        'organisme' => $object->echangeChorusContrat->organisme,
                        'idDecision' => $echangeChorus->getIdDecision(),
                    ],
                    ['id' => 'DESC']
                );

            if ($lastEchange) {
                $echangeChorus = $this->atexoEchangeChorusService
                    ->setDataFromLastEchange($echangeChorus, $lastEchange);

                $numOrdre = $lastEchange->getNumOrdre();
                $echangeChorus->setNumOrdre(1 + intval($numOrdre));
            }
        }
        $echangeChorus = $this->setTypeEnvoi($object, $echangeChorus);
        $echangeChorus->setIdsBlobEnv('');

        if (!is_null($agent)) {
            $echangeChorus->setNomCreateur($agent->getNom());
            $echangeChorus->setPrenomCreateur($agent->getPrenom());
            $echangeChorus->setIdCreateur($agent->getId());
        }

        return $echangeChorus;
    }

    public function setTypeEnvoi(ChorusEchangeInput $object, ChorusEchange $echangeChorus): ChorusEchange
    {
        if (
            ($object->modeEchangeChorus !== $this->parameterBag->get('TYPE_CONTRAT_MODE_ECHANGE_CHORUS_INTEGRER'))
            && ($object->echangeChorusContrat->statutEJ == $this->parameterBag->get('CHORUS_STATUT_EJ_COMMANDE')
                && $object->echangeChorusContrat->statut ==
                $this->parameterBag->get('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE'))
        ) {
            $echangeChorus->setTypeEnvoi($this->parameterBag->get('TYPE_ENVOI_CHORUS_ACTE_MODIF'));
        }

        return $echangeChorus;
    }

    public function setDonneesContrat(ChorusEchangeInput $object, ChorusEchange $echangeChorus): ChorusEchange
    {
        $organisme = $object->echangeChorusContrat->organisme;
        $echangeChorus->setUuidExterneExec($object->uuidExterneExec);
        $echangeChorus->setOrganisme($organisme);
        /** @var Entreprise $titulaire */
        $titulaire = $this->entityManager
            ->getRepository(Entreprise::class)
            ->find($object->echangeChorusContrat->idTitulaire);

        $etabTitulaire = $this->entityManager
            ->getRepository(Etablissement::class)
            ->find($organisme);

        if ($etabTitulaire) {
            $echangeChorus->setNumeroSiretTitulaire($etabTitulaire->getCodeEtablissement());
        }

        if (!is_null($titulaire)) {
            $echangeChorus->setCodePaysTitulaire($titulaire->getPaysenregistrement())
                ->setNumeroSirenTitulaire($titulaire->getSiren())
                ->setRaisonSocialeAttributaire($titulaire->getNom())
                ->setFormeJuridique($titulaire->getFormejuridique())
                ->setPme(
                    $titulaire->getCategorieEntreprise() === ContratTitulaire::CATEGORIE_ENTREPRISE_PME
                )
                ->setPaysTerritoire($titulaire->getPays())
                ->setNumeroNationalAttributaire($titulaire->getSirenetranger())
                ->setCodeApe($this->atexoEchangeChorusService->getCodeApe(trim($titulaire->getCodeape())));

            if ($etabTitulaire) {
                $echangeChorus->setSiretAttributaire(
                    $this->atexoEchangeChorusService
                        ->getSiretAttributaire($titulaire->getSiren(), $etabTitulaire->getCodeEtablissement())
                );
            }
        }

        $echangeChorus->setClauseSociale($object->echangeChorusContrat->clauseSociale ?? 0);
        $echangeChorus->setClauseEnvironnementale($object->echangeChorusContrat->clauseEnvironnementale ?? 0);
        $echangeChorus->setDateNotification(
            date_format($object->echangeChorusContrat->datePrevisionnelleNotification, 'Y-m-d')
        );
        $echangeChorus->setDateNotificationReelle(
            date_format($object->echangeChorusContrat->dateNotification, 'Y-m-d')
        );
        $echangeChorus->setDateFinMarche(
            date_format($object->echangeChorusContrat->datePrevisionnelleFinMarche, 'Y-m-d')
        );
        $echangeChorus->setDateFinMarcheReelle(
            date_format($object->echangeChorusContrat->dateFin, 'Y-m-d')
        );

        $echangeChorus->setCpv1($object->echangeChorusContrat->codeCpv1);
        $echangeChorus->setCpv2($object->echangeChorusContrat->codeCpv2);

        $echangeChorus->setObjetContrat($object->echangeChorusContrat->objet);
        $echangeChorus->setMontantHt($object->echangeChorusContrat->montant);

        $echangeChorus->setNomAgent($object->echangeChorusContrat->nomAgent);
        $echangeChorus->setPrenomAgent($object->echangeChorusContrat->prenomAgent);

        if (!is_null($object->echangeChorusContrat->typeContrat)) {
            $echangeChorus->setTypeContrat($this->getTypeContrat($object->echangeChorusContrat->typeContrat));
        }
        if (!is_null($object->echangeChorusContrat->typeProcedure)) {
            $echangeChorus->setIdTypeProcedure(
                $this->getTypeProcedureOrg(
                    $object->echangeChorusContrat->typeProcedure,
                    $organisme
                )
            );
        }

        $echangeChorus->setIntituleContrat($object->echangeChorusContrat->intituleContrat);
        $echangeChorus->setIdFormePrix($object->echangeChorusContrat->formePrix);
        $echangeChorus->setSiret($object->echangeChorusContrat->siret);

        return $echangeChorus;
    }

    public function getTypeProcedureOrg(string $abbreviation, string $organisme): ?string
    {
        $typeProcedureOrg = $this->entityManager
            ->getRepository(TypeProcedureOrganisme::class)
            ->findOneBy([
                'organisme' => $organisme,
                'abbreviation' => $abbreviation,
            ]);

        return !is_null($typeProcedureOrg) ? $typeProcedureOrg->getIdTypeProcedure() : null;
    }

    public function getTypeContrat(string $typeContrat): ?int
    {
        $typeContrat = $this->entityManager
            ->getRepository(TypeContrat::class)
            ->findOneBy(['libelleTypeContrat' => 'TYPE_CONTRAT_' . $typeContrat]);

        return !is_null($typeContrat) ? $typeContrat->getIdTypeContrat() : null;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof ChorusEchange) {
            return false;
        }

        return ChorusEchange::class === $to && !empty($context['input']['class'])
            && $context['input']['class'] === ChorusEchangeInput::class;
    }
}
