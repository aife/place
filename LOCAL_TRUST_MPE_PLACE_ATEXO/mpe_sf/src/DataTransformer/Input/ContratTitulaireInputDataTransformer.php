<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Input;

use DateTime;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\Input\ContratTitulaireInput;
use App\Entity\Agent;
use App\Entity\ContratTitulaire;
use App\Service\ClausesService;
use App\Service\Consultation\ConsultationService;
use App\Service\DataTransformer\ContratTransformer;
use Symfony\Component\Security\Core\Security;

final class ContratTitulaireInputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private readonly Security $security,
        private readonly ValidatorInterface $validator,
        private readonly ClausesService $clausesService,
        private readonly ConsultationService $consultationService,
        private readonly ContratTransformer $contratTransformer
    ) {
    }

    /**
     * {@inheritdoc}
     * @param ContratTitulaireInput $object
     */
    public function transform($object, string $to, array $context = []): ContratTitulaire
    {
        /** @var Agent $user */
        $user = $this->security->getUser();

        $this->validator->validate($object);

        $contratTitulaire = new ContratTitulaire();

        $contratTitulaire->setOrganisme(
            $object->organisme?->getAcronyme() ? $object->organisme : $user->getOrganisme()
        );

        $contratTitulaire->setTypeContrat($object->typeContrat);
        $contratTitulaire->setIdTypeContrat($object->typeContrat->getIdTypeContrat());
        $contratTitulaire->setIdTitulaireEtab($object->etablissement->getIdEtablissement());
        $contratTitulaire->setIdContactContrat($object->idContact);
        $contratTitulaire->setIdTitulaire($object->idTitulaire->getId());
        $contratTitulaire->setHorsPassation($object->horsPassation);
        $contratTitulaire->setNumero($object->numero);
        $contratTitulaire->setNumeroLong($object->numeroLong);
        $contratTitulaire->setReferenceLibre($object->referenceLibre);
        $contratTitulaire->setObjet($object->objet);
        $contratTitulaire->setServiceId($object->idService?->getId());
        $contratTitulaire->setIdCreateur($object->idCreateur?->getId() ?? $user->getId());
        $contratTitulaire->setFormePrix($object->formePrix ?? $user->getIdExterne());
        $contratTitulaire->setMarcheDefense($object->defenseOuSecurite);
        $contratTitulaire->setLieuExecution($this->consultationService->getLieuExecution($object->lieuExecutions));
        $contratTitulaire->setDateCreation($object->dateCreation);
        $contratTitulaire->setDateNotification($object->dateNotification);
        $contratTitulaire->setDatePrevueNotification($object->datePrevisionnelleNotification);
        $contratTitulaire->setDatePrevueFinContrat($object->datePrevisionnelleFinMarche);
        $contratTitulaire->setDatePrevueMaxFinContrat($object->datePrevisionnelleFinMaximaleMarche);
        $contratTitulaire->setDateDebutExecution($object->dateDebutExecution);
        $contratTitulaire->setDateAttribution($object->decisionAttribution);
        $contratTitulaire->setDateModification($object->dateModification);
        $contratTitulaire->setDateFin($object->dateFin);
        $contratTitulaire->setDateMaxFin($object->dateMaxFin);
        $contratTitulaire->setDatePublicationSN(null);
        $contratTitulaire->setDateModificationSN(null);
        $contratTitulaire->setDatePublicationInitialeDe(new DateTime('0000-00-00 00:00:00'));
        $contratTitulaire->setDureeInitialeContrat($object->dureeMaximaleMarche);
        $contratTitulaire->setIntitule($object->intitule);
        $contratTitulaire->setIdTrancheBudgetaire($object->trancheBudgetaire?->getId());
        $contratTitulaire->setCcagApplicable($object->ccagApplicable?->getId());
        $contratTitulaire->setStatut($this->contratTransformer->getStatutIdByLabel($object->statut));
        $contratTitulaire->setCategorie($this->contratTransformer->getCategorieByLabel($object->naturePrestation));
        $contratTitulaire->setIdChapeau($object->idChapeau);
        $contratTitulaire->setMontant($object->montant);
        if ($object->typeContrat->getAvecMontantMax()) {
            $contratTitulaire->setMontantMaxEstime($object->montantMaxEstime);
        }
        $contratTitulaire->setCodeCpv1($object->cpv[ContratTransformer::CODE_PRINCIPAL] ?? null);
        $contratTitulaire->setCodeCpv2($object->cpv[ContratTransformer::CODE_SECONDAIRE] ?? null);

        if (is_array($object->clauses)) {
            $this->clausesService->updateClauses($contratTitulaire, $object->clauses);
        }

        return $contratTitulaire;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof ContratTitulaire) {
            return false;
        }

        return
            ContratTitulaire::class === $to
            && null !== ($context['input']['class'] ?? null)
            && $context['input']['class'] == ContratTitulaireInput::class
        ;
    }
}
