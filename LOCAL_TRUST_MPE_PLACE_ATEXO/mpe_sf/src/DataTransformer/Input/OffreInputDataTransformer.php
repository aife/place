<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Input;

use DateTime;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Input\OffreInput;
use App\Entity\Entreprise;
use App\Entity\Offre;
use App\Service\Consultation\ConsultationDepotService;

final class OffreInputDataTransformer implements DataTransformerInterface
{
    public function __construct(private readonly ConsultationDepotService $consultationDepotService)
    {
    }

    /**
     * @param OffreInput $object
     */
    public function transform($object, string $to, array $context = []): Offre
    {
        $offre = $this->consultationDepotService->initDefaultFieldsOffre();

        $consultation = $object->consultation;
        $inscrit = $object->inscrit;
        /**
         * @var Entreprise $entreprise
         */
        $entreprise = $inscrit->getEntreprise();

        $offre->setConsultation($object->consultation);
        $offre->setOrganisme($consultation->getAcronymeOrg());
        $offre->setEntrepriseId($entreprise->getId());
        $offre->setInscrit($inscrit);
        $offre->setIdEtablissement($inscrit->getIdEtablissement());

        $dateNow = new DateTime();
        $offre->setDateDepot($object->dateDepot ?? $dateNow);
        $offre->setUntrusteddate($object->dateDepot ?? $dateNow);
        $offre->setHorodatage($object->horodatage ?? '');

        return $offre;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Offre) {
            return false;
        }

        return Offre::class === $to && null !== ($context['input']['class'] ?? null);
    }
}
