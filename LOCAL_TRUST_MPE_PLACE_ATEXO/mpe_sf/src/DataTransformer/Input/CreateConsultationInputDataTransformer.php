<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Input;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\Input\CreateConsultationInput;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Repository\ConfigurationPlateformeRepository;
use App\Service\AtexoConfiguration;
use App\Service\ClausesService;
use App\Service\Consultation\ConsultationService;
use App\Service\DataToolBoxTrait;
use App\Service\Procedure\ProcedureEquivalenceAgentService;
use DateTimeZone;
use Symfony\Component\Security\Core\Security;

final class CreateConsultationInputDataTransformer implements DataTransformerInterface
{
    use DataToolBoxTrait;

    public function __construct(
        private readonly ValidatorInterface $validator,
        private readonly Security $security,
        private readonly ConsultationService $consultationService,
        private readonly ProcedureEquivalenceAgentService $procedureEquivalenceAgentService,
        private readonly ClausesService $clausesService,
        private readonly ConfigurationPlateformeRepository $configurationPlateformeRepository,
        private readonly AtexoConfiguration $atexoConfiguration,
    ) {
    }

    /**
     * {@inheritdoc}
     * @param CreateConsultationInput $object
     */
    public function transform($object, string $to, array $context = []): Consultation
    {
        $this->validator->validate($object);

        $consultation = new Consultation();

        /** @var Agent $user */
        $user = $this->security->getUser();

        $userCreateur = $object->agentCreateur ?? $user;
        $consultation->setIdCreateur($userCreateur->getId());
        $consultation->setAgentCreateur($userCreateur);
        $consultation->setPrenomCreateur($userCreateur->getPrenom());
        $consultation->setNomCreateur($userCreateur->getNom());
        $consultation->setAgentTechniqueCreateur($userCreateur);

        $service = $object->directionService ?? $user->getService();
        $consultation->setService($service);
        $consultation->setServiceValidation($service?->getId());
        $consultation->setServiceAssocieId($service?->getId());

        $organisme = $object->organisme ?? $user->getOrganisme();
        $consultation->setOrganisme($organisme);
        $consultation->setOrgDenomination($organisme->getDenominationOrg());
        $consultation->setDonneeComplementaire($object->donneeComplementaire);

        $consultation->setCategorieConsultation($object->naturePrestation);
        $consultation->setIdTypeProcedureOrg($object->typeProcedure->getIdTypeProcedure());
        $consultation->setProcedureType($object->typeProcedure->getProcedurePortailType());
        $consultation->setDepouillablePhaseConsultation($object->typeProcedure->getDepouillablePhaseConsultation());
        $consultation->setConsultationTransverse($object->typeProcedure->getConsultationTransverse());
        $consultation->setTypeMarche($object->typeContrat);
        if (!empty($object->consultationParent) && $this->atexoConfiguration->isModuleEnabled('NumerotationRefCons', $organisme, 'get')) {
            $consultation->setReferenceUtilisateur($object->consultationParent->getReferenceUtilisateur());
        } else {
            $consultation = $this->consultationService->setReferenceUtilisateurForInitialization(
                $consultation,
                $user,
                $object->reference
            );
        }
        $consultation->setIntitule($object->intitule);
        $consultation->setObjet($object->objet);
        $consultation->setChampSuppInvisible($object->commentaireInterne);
        $consultation->setAlloti($object->alloti ? '1' : '0');
        $consultation->setDumeDemande($object->dume ? '1' : '0');
        $consultation->setTypeProcedureDume($object->typeProcedureDume?->getId() ?? 0);
        $config = $this->configurationPlateformeRepository->getConfigurationPlateforme();

        if (
            $object->valeurEstimee
            && ($config->getPublicite() && $config->getAfficherValeurEstimee())
        ) {
            $consultation = $this->consultationService->setMontantMarcheForInitialization(
                $consultation,
                $object->valeurEstimee
            );
        }

        $consultation = $this->consultationService->setLieuxExecutionForInitialization(
            $consultation,
            $object->lieuxExecution
        );

        $conf = $this->configurationPlateformeRepository->getConfigurationPlateforme();
        if ($conf->getCodeNutLtReferentiel()) {
            $nuts = trim(implode('#', $object->codesNuts), '#');
            $consultation->setCodesNuts("#{$nuts}");
        }

        if ($conf->getAffichageCodeCpv()) {
            $codeCpv2 = trim(implode('#', [
                $object->codeCpvSecondaire1,
                $object->codeCpvSecondaire2,
                $object->codeCpvSecondaire3
            ]), '#');
            $consultation->setCodeCpv1($object->codeCpvPrincipal);
            $consultation->setCodeCpv2("#{$codeCpv2}#");
        }

        $consultation = $this->consultationService->setDefaultValuesForInitialization($consultation);
        $consultation = $this->consultationService->setAvisTypeForInitialization($consultation);
        $consultation = $this->consultationService->setMessecForInitialization($consultation);
        $consultation = $this->consultationService->setPFVForInitialization($consultation);
        $consultation = $this->consultationService->setEmptyValuesForInitialization($consultation);
        $consultation = $this->consultationService->setDatesForInitialization($consultation);
        $this->consultationService->setInvitePonctuelForInitialization($consultation, $userCreateur);
        $this->consultationService->setFavorisForInitialization($consultation, $userCreateur);

        $consultation = $this->consultationService->setConsultationWithParametreProcedure(
            $consultation,
            $this->procedureEquivalenceAgentService
        );
        $consultation->setCodeExterne($object->codeExterne);

        if ($object->dateLimiteRemisePlis) {
            if ($object->dateLimiteRemisePlis instanceof \DateTimeImmutable) {
                $copyDate = \DateTime::createFromImmutable($object->dateLimiteRemisePlis);
                $copyDate->setTimezone(new DateTimeZone(date_default_timezone_get()));
                $object->dateLimiteRemisePlis = $copyDate;
            }
            $consultation->setDatefin($object->dateLimiteRemisePlis);
            $dateFinUnix = strtotime(
                $consultation->getDatefin()->format("Y-m-d H:i:s")
                . " + " .
                $consultation->getPoursuivreAffichage() . " " . $consultation->getPoursuivreAffichageUnite()
            );
            $consultation->setDateFinUnix($dateFinUnix);
        }

        $consultation = $this->setDLROLocal($object, $consultation, $organisme, $service);

        $consultation->setReferenceConsultationParent($object->consultationParent);
        $consultation->setOrganismeConsultationInit($object->consultationParent?->getOrganisme() ?? '');
        $consultation->setEntiteAdjudicatrice($object->entiteAdjudicatrice);
        $consultation->setAutreTechniqueAchat($object->autreTechniqueAchat);
        $consultation->setDonneePubliciteObligatoire($object->donneePubliciteObligatoire ? '1' : '0');
        $consultation->setDonneeComplementaireObligatoire($object->donneeComplementaireObligatoire ? '1' : '0');
        if ($config->getCaseAttestationConsultation()) {
            $consultation->setAttestationConsultation($object->attestationConsultation ? '1' : '0');
        }

        if (is_array($object->clauses)) {
            $this->clausesService->updateClauses($consultation, $object->clauses);
        }

        $consultation->setTypeAcces(
            $object->consultationRestreinte
                ? Consultation::TYPE_ACCES_RESTREINT
                : Consultation::TYPE_ACCES_PUBLIC
        );

        $consultation->setCodeProcedure($object->codeProcedure);
        $consultation->setProcedureOuverte($object->procedureOuverte);
        $consultation->setGroupement($object->groupement);
        if (null !== $object->justificationNonAlloti) {
            $consultation = $this->consultationService->setJustificationNonAlloti(
                $consultation,
                $object->justificationNonAlloti
            );
        }

        $consultation->setNumeroAc($object->numeroSad);
        $consultation->setIdContrat($object->idContratTitulaire);
        $consultation->setContratExecUuid($object->contratExecUuid);

        $consultation->setReferentielAchat($object->referentielAchat);

        $consultation->setAdresseRetraisDossiers($object->adresseRetraisDossiers);
        $consultation->setAdresseDepotOffres($object->adresseDepotOffres);
        $consultation->setLieuOuverturePlis($object->lieuOuverturePlis);

        $consultation->setTypeFormulaireDume($object->dumeSimplifie ? '1' : '0');
        if (null !== $object->besoinRecurrent) {
            $consultation->setBesoinRecurrent($object->besoinRecurrent);
        }

        if (null !== $object->technicalReference) {
            $consultation->setReference($object->technicalReference);
        }

        if (null !== $object->modeOuvertureReponse) {
            $consultation->setModeOuvertureReponse($object->modeOuvertureReponse);
        }

        if (null !== $object->reponseElectronique) {
            if (
                $object->reponseElectronique === ConsultationService::TYPE_REPONSE_AUTORISEE
                || $object->reponseElectronique === ConsultationService::TYPE_REPONSE_OBLIGATOIRE
            ) {
                $consultation->setAutoriserReponseElectronique('1');
            } else {
                $consultation->setAutoriserReponseElectronique('0');
            }

            if ($object->reponseElectronique === ConsultationService::TYPE_REPONSE_OBLIGATOIRE) {
                $consultation->setReponseObligatoire('1');
            } else {
                $consultation->setReponseObligatoire('0');
            }
        }

        if (null !== $object->enveloppeAnonyme) {
            $consultation->setEnvAnonymat($object->enveloppeAnonyme ? '1' : '0');
        }

        if (null !== $object->enveloppeCandidature) {
            $consultation->setEnvCandidature($object->enveloppeCandidature ? '1' : '0');
        }

        if (null !== $object->enveloppeOffre) {
            $consultation->setEnvOffre($object->enveloppeOffre ? '1' : '0');
        }

        if (null !== $object->signatureActeEngagement) {
            $consultation->setSignatureActeEngagement($object->signatureActeEngagement);
        }

        if (null !== $object->marchePublicSimplifie) {
            $consultation->setMarchePublicSimplifie($object->marchePublicSimplifie);
        }

        $consultation->setConsultationExterne($object->urlExterne ? '1' : '0');

        if (null !== $object->dateMiseEnLigneCalcule) {
            $consultation->setDateMiseEnLigneCalcule($object->dateMiseEnLigneCalcule);
        }
        if (null !== $object->urlExterne) {
            $consultation->setUrlConsultationExterne($object->urlExterne);
        }

        if (null !== $object->chiffrement) {
            $consultation->setChiffrementOffre($object->chiffrement ? '1' : '0');
        }

        if (null !== $object->chiffrement) {
            $consultation->setChiffrementOffre($object->chiffrement ? '1' : '0');
        }

        return $consultation;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Consultation) {
            return false;
        }

        return Consultation::class === $to && !empty($context['input']['class'])
            && $context['input']['class'] == CreateConsultationInput::class;
    }


    protected function setDLROLocal(
        CreateConsultationInput $object,
        Consultation $consultation,
        Organisme $organisme,
        ?Service $service = null
    ): Consultation {
        $fuseauHoraireActive = $service?->getActivationFuseauHoraire() ?? $organisme->getActivationFuseauHoraire();
        $fuseauHoraire = $service?->getDecalageHoraire() ?? $organisme->getDecalageHoraire();
        $lieuResidence = $service?->getLieuResidence() ?? $organisme->getLieuResidence();

        if ($fuseauHoraireActive && $object->dateLimiteRemisePlis instanceof \DateTime) {
            $dateLimiteRemisePlisLocale = (new \DateTime($object->dateLimiteRemisePlis->format('Y-m-d H:i:s')))
                ->modify($fuseauHoraire . ' hours')
                ->format('Y-m-d H:i:s');
            $object->dateLimiteRemisePlisLocale = $dateLimiteRemisePlisLocale;
            $consultation->setDateFinLocale($dateLimiteRemisePlisLocale);
            $consultation->setLieuResidence($lieuResidence);
        }

        return $consultation;
    }
}
