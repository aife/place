<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Input;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\Input\FormePrixInput;
use App\Entity\FormePrix;
use App\Entity\Tranche;
use App\Entity\ValeurReferentiel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class FormePrixInputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private EntityManagerInterface $em,
        private ParameterBagInterface $parameterBag,
        private ValidatorInterface $validator
    ) {
    }

    public function transform($object, string $to, array $context = [])
    {
        $this->validator->validate($object);
        $variationsPrix = null;
        $formePrix = new FormePrix();
        $formePrix->setFormePrix($object->typeFormePrix)
            ->setPuMin($object->puMin)
            ->setPuMax($object->puMax)
            ->setModalite($object->modalite);

        if ($object->variationPrixForfaitaire) {
            $variationsPrix = $this->em->getRepository(ValeurReferentiel::class)->getReferentielsById(
                $this->parameterBag->get('REFERENTIEL_VARIATION_PRIX')
            );
            $formePrix->variationPrixForfaitaire = array_search($object->variationPrixForfaitaire, $variationsPrix);
        }

        if ($object->variationPrixUnaitaire) {
            if (null === $variationsPrix) {
                $variationsPrix = $this->em->getRepository(ValeurReferentiel::class)->getReferentielsById(
                    $this->parameterBag->get('REFERENTIEL_VARIATION_PRIX')
                );
            }
            $formePrix->variationPrixUnaitaire = array_search($object->variationPrixUnaitaire, $variationsPrix);
        }

        if ($object->typePrix) {
            $typesPrix = $this->em->getRepository(ValeurReferentiel::class)->getReferentielsById(
                $this->parameterBag->get('REFERENTIEL_TYPE_PRIX')
            );
            $list = [];
            foreach ($object->typePrix as $type) {
                $list[] =  array_search($type, $typesPrix);
            }
            $formePrix->typePrix = array_filter($list);
        }

        if ($object->uniteMinMax) {
            $unites = $this->em->getRepository(ValeurReferentiel::class)->getReferentielsById(
                $this->parameterBag->get('REFERENTIEL_UNITE_PRIX')
            );
            if ($idMinMax = array_search($object->uniteMinMax, $unites)) {
                $formePrix->setIdMinMax($idMinMax);
            }
        }

        if ($object->donneesComplementaire) {
            $formePrix->donneesComplementaire = $object->donneesComplementaire->getIdDonneeComplementaire();
        }

        return $formePrix;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {

        if ($data instanceof FormePrix) {
            return false;
        }

        return FormePrix::class === $to && null !== ($context['input']['class'] ?? null);
    }
}
