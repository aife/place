<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Input;

use DateTime;
use ApiPlatform\Core\DataTransformer\DataTransformerInitializerInterface;
use ApiPlatform\Core\Serializer\AbstractItemNormalizer;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\Input\UpdateContratTitulaireInput;
use App\Entity\Agent;
use App\Entity\ContactContrat;
use App\Entity\ContratTitulaire;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\Service;
use App\Entity\TrancheArticle133;
use App\Entity\TypeContrat;
use App\Entity\ValeurReferentiel;
use App\Model\TypePrestation;
use App\Service\ClausesService;
use App\Service\Consultation\ConsultationService;
use App\Service\DataTransformer\ContratTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Security;

final class UpdateContratTitulaireInputDataTransformer implements DataTransformerInitializerInterface
{
    public function __construct(
        private readonly ValidatorInterface $validator,
        private readonly ConsultationService $consultationService,
        private readonly ClausesService $clausesService,
        private readonly Security $security,
        private readonly ContratTransformer $contratTransformer,
        private readonly EntityManagerInterface $entityManager,
        private readonly ParameterBagInterface $parameterBag,
    ) {
    }

    /**
     * {@inheritdoc}
     * @param UpdateContratTitulaireInput $object
     */
    public function transform($object, string $to, array $context = []): ContratTitulaire
    {
        /** @var Agent $user */
        $user = $this->security->getUser();

        $this->validator->validate($object);

        $updateContratTitulaire = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE];

        $updateContratTitulaire->setTypeContrat($object->typeContrat);
        $updateContratTitulaire->setIdTypeContrat($object->typeContrat->getIdTypeContrat());
        $updateContratTitulaire->setIdTitulaireEtab($object->etablissement?->getIdEtablissement());
        $updateContratTitulaire->setIdContactContrat($object->idContact);
        $updateContratTitulaire->setIdTitulaire($object->idTitulaire?->getId());
        $updateContratTitulaire->setHorsPassation($object->horsPassation);
        $updateContratTitulaire->setNumero($object->numero);
        $updateContratTitulaire->setNumeroLong($object->numeroLong);
        $updateContratTitulaire->setReferenceLibre($object->referenceLibre);
        $updateContratTitulaire->setObjet($object->objet);
        $updateContratTitulaire->setServiceId($object->idService?->getId());
        $updateContratTitulaire->setFormePrix($object->formePrix ?? $user->getIdExterne());
        $updateContratTitulaire->setMarcheDefense($object->defenseOuSecurite);
        $updateContratTitulaire->setLieuExecution(
            $this->consultationService->getLieuExecution($object->lieuExecutions)
        );
        $updateContratTitulaire->setDateCreation($object->dateCreation);
        $updateContratTitulaire->setDateNotification($object->dateNotification);
        $updateContratTitulaire->setDatePrevueNotification($object->datePrevisionnelleNotification);
        $updateContratTitulaire->setDatePrevueFinContrat($object->datePrevisionnelleFinMarche);
        $updateContratTitulaire->setDatePrevueMaxFinContrat($object->datePrevisionnelleFinMaximaleMarche);
        $updateContratTitulaire->setDateDebutExecution($object->dateDebutExecution);
        $updateContratTitulaire->setDateAttribution($object->decisionAttribution);
        $updateContratTitulaire->setDateModification($object->dateModification);
        $updateContratTitulaire->setDateFin($object->dateFin);
        $updateContratTitulaire->setDateMaxFin($object->dateMaxFin);
        $updateContratTitulaire->setDatePublicationSN(null);
        $updateContratTitulaire->setDateModificationSN(null);
        $updateContratTitulaire->setDatePublicationInitialeDe(new DateTime('0000-00-00 00:00:00'));
        $updateContratTitulaire->setDureeInitialeContrat($object->dureeMaximaleMarche);
        $updateContratTitulaire->setIntitule($object->intitule);
        $updateContratTitulaire->setIdTrancheBudgetaire($object->trancheBudgetaire?->getId());
        $updateContratTitulaire->setCcagApplicable($object->ccagApplicable?->getId());
        $updateContratTitulaire->setStatut($this->contratTransformer->getStatutIdByLabel($object->statut));
        $updateContratTitulaire->setCategorie(
            $this->contratTransformer->getCategorieByLabel($object->naturePrestation)
        );
        $updateContratTitulaire->setIdChapeau($object->idChapeau);
        $updateContratTitulaire->setMontant($object->montant);
        $updateContratTitulaire->setMontantMaxEstime($object->montantMaxEstime);
        $updateContratTitulaire->setCodeCpv1($object->cpv[ContratTransformer::CODE_PRINCIPAL] ?? null);
        $updateContratTitulaire->setCodeCpv2($object->cpv[ContratTransformer::CODE_SECONDAIRE] ?? null);

        if (is_array($object->clauses)) {
            $this->clausesService->updateClauses($updateContratTitulaire, $object->clauses);
        }

        return $updateContratTitulaire;
    }

    /**
     * {@inheritdoc}
     */
    public function initialize(string $inputClass, array $context = [])
    {
        /** @var ContratTitulaire $existingContratTitulaire */
        $existingContratTitulaire = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE];

        $contratTitulaireInput = new UpdateContratTitulaireInput();

        $service = $existingContratTitulaire->getIdService()
            ? $this->entityManager->getRepository(Service::class)->find($existingContratTitulaire->getIdService())
            : null
        ;
        $contratTitulaireInput->idService = $service;

        $contratTitulaireInput->typeContrat = $this->entityManager
            ->getRepository(TypeContrat::class)
            ->find($existingContratTitulaire->getIdTypeContrat())
        ;

        $contratTitulaireInput->trancheBudgetaire = $existingContratTitulaire->getIdTrancheBudgetaire()
            ? $this->entityManager
            ->getRepository(TrancheArticle133::class)
            ->find($existingContratTitulaire->getIdTrancheBudgetaire())
            :
            null
        ;
        $contratTitulaireInput->ccagApplicable = $existingContratTitulaire->getCcagApplicable()
            ? $this->entityManager
            ->getRepository(ValeurReferentiel::class)
            ->findOneBy(
                [
                    'id' => $existingContratTitulaire->getCcagApplicable(),
                    'idReferentiel' => $this->parameterBag->get('REFERENTIEL_CCAG_REFERENCE')
                ]
            )
            :
            null
        ;

        $contratTitulaireInput->idContact = $existingContratTitulaire->getIdContactContrat()
            ? $this->entityManager
            ->getRepository(ContactContrat::class)
            ->find($existingContratTitulaire->getIdContactContrat())
            :
            null
        ;

        $contratTitulaireInput->etablissement = $existingContratTitulaire->getIdTitulaireEtab()
            ? $this->entityManager
            ->getRepository(Etablissement::class)
            ->find($existingContratTitulaire->getIdTitulaireEtab())
            :
            null
        ;

        $contratTitulaireInput->idTitulaire = $existingContratTitulaire->getIdTitulaire()
            ? $this->entityManager
            ->getRepository(Entreprise::class)
            ->find($existingContratTitulaire->getIdTitulaire())
            :
            null
        ;

        $contratTitulaireInput->naturePrestation =
            TypePrestation::TYPE_PRESTATIONS[$existingContratTitulaire->getCategorie()]
            ?? null
        ;

        $contratTitulaireInput->horsPassation = $existingContratTitulaire->getHorsPassation();
        $contratTitulaireInput->numero = $existingContratTitulaire->getNumero();
        $contratTitulaireInput->numeroLong = $existingContratTitulaire->getNumeroLong();
        $contratTitulaireInput->referenceLibre = $existingContratTitulaire->getReferenceLibre();
        $contratTitulaireInput->objet = $existingContratTitulaire->getObjet();
        $contratTitulaireInput->formePrix = $existingContratTitulaire->getFormePrix();
        $contratTitulaireInput->defenseOuSecurite = $existingContratTitulaire->getMarcheDefense();
        $contratTitulaireInput->datePrevisionnelleNotification = $existingContratTitulaire->getDatePrevueNotification();
        $contratTitulaireInput->datePrevisionnelleFinMarche = $existingContratTitulaire->getDatePrevueFinContrat();
        $contratTitulaireInput->datePrevisionnelleFinMaximaleMarche =
            $existingContratTitulaire->getDatePrevueMaxFinContrat()
        ;
        $contratTitulaireInput->dureeMaximaleMarche = $existingContratTitulaire->getDureeInitialeContrat();
        $contratTitulaireInput->dateDebutExecution = $existingContratTitulaire->getDateDebutExecution();
        $contratTitulaireInput->lieuExecutions = $this->contratTransformer->getArrayLieuExecutionById(
            $existingContratTitulaire->getLieuExecution()
        );
        $contratTitulaireInput->decisionAttribution = $existingContratTitulaire->getDateAttribution();
        $contratTitulaireInput->intitule = $existingContratTitulaire->getIntitule();
        $contratTitulaireInput->dateModification = $existingContratTitulaire->getDateModification();
        $contratTitulaireInput->dateCreation = $existingContratTitulaire->getDateCreation();
        $contratTitulaireInput->dateNotification = $existingContratTitulaire->getDateNotification();
        $contratTitulaireInput->dateFin = $existingContratTitulaire->getDateFin();
        $contratTitulaireInput->dateMaxFin = $existingContratTitulaire->getDateMaxFin();
        $contratTitulaireInput->statut = $this->contratTransformer->getStatutLabelById(
            $existingContratTitulaire->getStatutContrat()
        );
        $contratTitulaireInput->idChapeau = $existingContratTitulaire->getIdChapeau();
        $contratTitulaireInput->montant = $existingContratTitulaire->getMontant();
        $contratTitulaireInput->montantMaxEstime = $existingContratTitulaire->getMontantMaxEstime();
        $contratTitulaireInput->cpv = $this->contratTransformer->getCpv($existingContratTitulaire);

        $contratTitulaireInput->clauses = $existingContratTitulaire->getClausesN1();

        return $contratTitulaireInput;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof ContratTitulaire) {
            return false;
        }

        return ContratTitulaire::class === $to && !empty($context['input']['class'])
            && $context['input']['class'] == UpdateContratTitulaireInput::class;
    }
}
