<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Input;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\Input\TrancheInput;
use App\Entity\Tranche;

class TrancheInputDataTransformer implements DataTransformerInterface
{
    public function __construct(private ValidatorInterface $validator)
    {
    }
    public function transform($object, string $to, array $context = [])
    {
        $this->validator->validate($object);
        $tranche = new Tranche();
        $tranche->setNatureTranche($object->natureTranche)
            ->setIntituleTranche($object->intituleTranche)
            ->setCodeTranche($object->codeTranche)
            ->setIdFormePrix($object->formePrix?->getIdFormePrix() ?? null)
            ->setIdDonneeComplementaire($object->donneesComplementaire ?? null);

        return $tranche;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Tranche) {
            return false;
        }

        return Tranche::class === $to && null !== ($context['input']['class'] ?? null);
    }
}
