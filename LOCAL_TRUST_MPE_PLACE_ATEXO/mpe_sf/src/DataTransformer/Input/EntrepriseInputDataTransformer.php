<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Input;

use ApiPlatform\Core\DataTransformer\DataTransformerInitializerInterface;
use ApiPlatform\Core\Serializer\AbstractItemNormalizer;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\Input\EntrepriseInput;
use App\Entity\Entreprise;
use DateTime;

class EntrepriseInputDataTransformer implements DataTransformerInitializerInterface
{
    public function __construct(
        private ValidatorInterface $validator
    ) {
    }

    public function transform($input, string $to, array $context = [])
    {
        $this->validator->validate($input);

        $entreprise = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE] ?? new $context['resource_class']();

        $dateNow = (new DateTime())->format('Y-m-d H:m:s');

        $entreprise->setNom($input->nom);
        $entreprise->setSiren($input->siren);
        $entreprise->setTelephone($input->telephone);
        $entreprise->setSiteInternet($input->siteInternet);
        $entreprise->setIdExterne($input->idExterne);
        $entreprise->setAdresse($input->adresse);
        $entreprise->setVille($input->ville);
        $entreprise->setCodepostal($input->codepostal);
        $entreprise->setPays($input->pays);
        $entreprise->setAcronymePays($input->acronymePays);
        $entreprise->setFormejuridique($input->formeJuridique);
        $entreprise->setCodeape($input->codeAPE);
        $entreprise->setLibelleApe($input->libelleApe);
        $entreprise->setCapitalSocial($input->capitalSocial);
        $entreprise->setRaisonSociale($input->nom);
        $entreprise->setDateCreation(
            $input->dateCreation?->format('Y-m-d H:m:s') ?? $dateNow
        );
        $entreprise->setDateModification(
            $input->dateModification?->format('Y-m-d H:m:s') ?? $dateNow
        );

        return $entreprise;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Entreprise) {
            return false;
        }

        return Entreprise::class === $to && null !== ($context['input']['class'] ?? null);
    }

    /**
     * @throws \Exception
     */
    public function initialize(string $inputClass, array $context = [])
    {
        $entity = $context['object_to_populate'] ?? null;
        $dto = new EntrepriseInput();

        if (!$entity) {
            return $dto;
        }
        if (!$entity instanceof Entreprise) {
            throw new \Exception(sprintf('Unexpected resource class "%s"', get_class($entity)));
        }

        $dto->nom = $entity->getNom();
        $dto->siren = $entity->getSiren();
        $dto->telephone = $entity->getTelephone();
        $dto->siteInternet = $entity->getSiteInternet();
        $dto->idExterne = $entity->getIdExterne();
        $dto->adresse = $entity->getAdresse();
        $dto->ville = $entity->getVille();
        $dto->codepostal = $entity->getCodepostal();
        $dto->pays = $entity->getPays();
        $dto->acronymePays = $entity->getAcronymePays();
        $dto->formeJuridique = $entity->getFormejuridique();
        $dto->codeAPE = $entity->getCodeape();
        $dto->libelleApe = $entity->getLibelleApe();
        $dto->capitalSocial = $entity->getCapitalSocial();
        $dto->raisonSociale = $entity->getNom();
        $dto->dateCreation = new DateTime($entity->getDateCreation());
        $dto->dateModification = new DateTime($entity->getDateModification());

        return $dto;
    }
}
