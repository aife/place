<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\TypeProcedureOrganismeOutput;
use App\Entity\Procedure\TypeProcedureOrganisme;

final class TypeProcedureOrganismeOutputDataTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($object, string $to, array $context = []): TypeProcedureOrganismeOutput
    {
        /** @var TypeProcedureOrganisme $object */

        $output = new TypeProcedureOrganismeOutput();
        $output->organisme = $object->getOrganisme();
        $output->libelle = htmlspecialchars_decode($object->getLibelleTypeProcedure());
        $output->abreviation = $object->getAbbreviation();
        $output->abreviationInterface = $object->getAbreviationInterface();
        $output->procedureSimplifie = $object->getProcedureSimplifie();
        $output->idExterne = $object->getIdExterne();

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return TypeProcedureOrganismeOutput::class === $to && $data instanceof TypeProcedureOrganisme;
    }
}
