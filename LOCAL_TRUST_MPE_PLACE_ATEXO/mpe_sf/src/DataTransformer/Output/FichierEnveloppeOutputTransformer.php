<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\FichierEnveloppeOutput;
use App\Entity\BloborganismeFile;
use App\Entity\EnveloppeFichier;
use App\Entity\MediaUuid;
use Doctrine\ORM\EntityManagerInterface;

class FichierEnveloppeOutputTransformer implements DataTransformerInterface
{
    public function __construct(private readonly IriConverterInterface $iriConverter, private EntityManagerInterface $entityManager)
    {
    }

    /**
     * @param EnveloppeFichier $object
     * @inheritDoc
     */
    public function transform($object, string $to, array $context = [])
    {
        $enveloppeOutput = new FichierEnveloppeOutput();
        $media = new MediaUuid();
        $media->setMedia($object->getBlob());
        $media->setCreatedAt(new \DateTime());
        $this->entityManager->persist($media);

        $enveloppeOutput->idFichier = $this->iriConverter->getIriFromItem($media);
        $enveloppeOutput->typeFichier = $object->getTypeFichier();
        if ($object->getBlobSignature() instanceof BloborganismeFile) {
            $signedMedia = new MediaUuid();
            $signedMedia->setMedia($object->getBlobSignature());
            $signedMedia->setCreatedAt(new \DateTime());
            $this->entityManager->persist($signedMedia);
            $enveloppeOutput->idSignature = $this->iriConverter->getIriFromItem($signedMedia);
        }

        $this->entityManager->flush();

        return $enveloppeOutput;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return FichierEnveloppeOutput::class === $to && $data instanceof EnveloppeFichier;
    }
}
