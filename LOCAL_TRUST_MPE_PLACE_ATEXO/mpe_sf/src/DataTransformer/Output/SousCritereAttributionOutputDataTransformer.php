<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\SousCritereAttributionOutput;
use App\Entity\Consultation\SousCritereAttribution;

final class SousCritereAttributionOutputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private readonly IriConverterInterface $iriConverter
    ) {
    }

    /**
     * @param SousCritereAttribution $object
     */
    public function transform($object, string $to, array $context = []): SousCritereAttributionOutput
    {
        $output = new SousCritereAttributionOutput();

        $output->critereAttribution = $this->iriConverter->getIriFromItem($object->getCritereAttribution());
        $output->enonce = $object->getEnonce();
        $output->ponderation = $object->getPonderation();

        return $output;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return SousCritereAttributionOutput::class === $to && $data instanceof SousCritereAttribution;
    }
}
