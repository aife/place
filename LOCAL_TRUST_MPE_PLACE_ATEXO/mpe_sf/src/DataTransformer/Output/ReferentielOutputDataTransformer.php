<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\ReferentielOutput;
use App\Entity\Referentiel;

final class ReferentielOutputDataTransformer implements DataTransformerInterface
{

    /**
     * {@inheritdoc}
     * @param Referentiel $object
     */
    public function transform($object, string $to, array $context = []): ReferentielOutput
    {
        $output = new ReferentielOutput();
        $output->libelleReferentiel = $object->getLibelleReferentiel();

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return ReferentielOutput::class === $to && $data instanceof Referentiel;
    }
}
