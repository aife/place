<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\OffreOutput;
use App\Entity\Offre;

final class OffreOutputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private readonly IriConverterInterface $iriConverter
    ) {
    }

    /**
     * @param Offre $object
     */
    public function transform($object, string $to, array $context = []): OffreOutput
    {
        $output = new OffreOutput();

        $output->consultation = $this->iriConverter->getIriFromItem($object->getConsultation());
        $output->inscrit = $this->iriConverter->getIriFromItem($object->getInscrit());
        $output->statut = $object->getStatutOffres();
        $output->dateDepot = $object->getUntrusteddate();
        $output->entrepriseId = $object->getEntrepriseId();
        $output->nomEntrepriseInscrit = $object->getNomEntrepriseInscrit();

        $output->horodatage = !empty($object->getHorodatage())
            ? (
                is_resource($object->getHorodatage())
                ? stream_get_contents($object->getHorodatage())
                : $object->getHorodatage()
            )
            : ''
        ;

        return $output;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return OffreOutput::class === $to && $data instanceof Offre;
    }
}
