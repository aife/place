<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\ValeurReferentielOutput;
use App\Entity\ValeurReferentiel;

final class ValeurReferentielOutputDataTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     * @param ValeurReferentiel $object
     */
    public function transform($object, string $to, array $context = []): ValeurReferentielOutput
    {
        $output = new ValeurReferentielOutput();
        $output->libelleValeurReferentiel = $object->getLibelleValeurReferentiel();
        $output->libelleValeurReferentielFr = $object->getLibelleValeurReferentielFr();
        $output->referentiel = $object->getIdReferentiel();

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return ValeurReferentielOutput::class === $to && $data instanceof ValeurReferentiel;
    }
}
