<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\AcheteurPublicOutput;
use App\Entity\Agent\AcheteurPublic;

final class AcheteurPublicOutputDataTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($object, string $to, array $context = []): AcheteurPublicOutput
    {
        /** @var AcheteurPublic $object */

        $output = new AcheteurPublicOutput();

        $output->login = $object->getBoampLogin();
        $output->password = $object->getBoampPassword();
        $output->email = $object->getBoampMail();
        $output->token = $object->getToken();
        $output->moniteurProvenance = $object->getMoniteurProvenance();

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return AcheteurPublicOutput::class === $to && $data instanceof AcheteurPublic;
    }
}
