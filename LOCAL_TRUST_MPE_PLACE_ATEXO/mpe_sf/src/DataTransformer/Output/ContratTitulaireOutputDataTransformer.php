<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\ContratTitulaireOutput;
use App\Entity\CategorieConsultation;
use App\Entity\ContratTitulaire;
use App\Entity\Etablissement;
use App\Entity\TypeContrat;
use App\Serializer\ContratTitulaireNormalizer;
use App\Service\Consultation\ConsultationService;
use App\Service\DataTransformer\ClausesTransformer;
use App\Service\DataTransformer\ContratTransformer;
use Doctrine\ORM\EntityManagerInterface;

final class ContratTitulaireOutputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private readonly ContratTransformer $contratTransformer,
        private readonly ContratTitulaireNormalizer $contratTitulaireNormalizer,
        private readonly IriConverterInterface $iriConverter,
        private readonly EntityManagerInterface $entityManager,
        private readonly ClausesTransformer $clausesTransformer,
        private readonly ConsultationService $consultationService
    ) {
    }

    /**
     * {@inheritdoc}
     */
    public function transform($object, string $to, array $context = []): ContratTitulaireOutput
    {
        /** @var ContratTitulaire $object */
        $output = new ContratTitulaireOutput();

        $output->organisme = $object->getOrganisme();
        $output->idTitulaire = $object->getIdTitulaire();
        $output->horsPassation = $object->getHorsPassation();
        $output->id = $object->getId();
        $output->numero = $object->getNumero();
        $output->numeroLong = $object->getNumeroLong();
        $output->referenceLibre = $object->getReferenceLibre();
        $output->objet = $object->getObjet();
        $output->idService = $object->getIdService();
        $output->idCreateur = $object->getIdCreateur();

        if (!empty($object->getFormePrix())) {
            $output->formePrix = $object->getFormePrix();
            $output->modaliteRevisionPrix = $object->getFormePrix();
        }

        $output->defenseOuSecurite = $object->getMarcheDefense();
        $output->datePrevisionnelleNotification = $object->getDatePrevueNotification();
        $output->datePrevisionnelleFinMarche = $object->getDatePrevueFinContrat();
        $output->datePrevisionnelleFinMaximaleMarche = $object->getDatePrevueMaxFinContrat();
        $output->dureeMaximaleMarche = $object->getDureeInitialeContrat();
        $output->dateDebutExecution = $object->getDateDebutExecution();
        $output->lieuExecution = $this->consultationService->getDepartmentsWithLieuExecution(
            $object->getLieuExecution()
        );
        $output->decisionAttribution = $object->getDateAttribution();
        $output->intitule = $object->getIntitule();
        $output->trancheBudgetaire = $this->contratTransformer->getTrancheBudgetaire($object->getIdTrancheBudgetaire());
        $output->ccagApplicable = $this->contratTransformer->getCcag($object->getCcagApplicable());
        $output->idAttributeur = $object->getHorsPassation() ? $object->getIdAgent() : null;
        $output->typeContrat = $this->iriConverter->getIriFromItem(
            $this->entityManager->getRepository(TypeContrat::class)->find($object->getIdTypeContrat())
        );
        $output->dateModification = $object->getDateModification();
        $output->dateCreation = $object->getDateCreation();
        $output->dateNotification = $object->getDateNotification();
        $output->dateFin = $object->getDateFin();
        $output->dateMaxFin = $object->getDateMaxFin();
        $output->statut = $this->contratTitulaireNormalizer->getStatut($object->getStatutContrat());
        $output->naturePrestation = $this->iriConverter->getIriFromItem(
            $this->entityManager->getRepository(CategorieConsultation::class)->find($object->getCategorie())
        );
        $output->idChapeau = $object->getIdChapeau();
        $output->chapeau = (bool)$object->getChapeau();
        $output->idEtablissementTitulaire = $object->getIdTitulaireEtab();
        $output->montant = $object->getMontant() ?? $object->getMontantMaxEstime() ?? null;
        $output->idContact = $object->getContact()?->getIdContactContrat();
        $output->cpv = $this->contratTransformer->getCpv($object);
        $output->contratTransverse = $this->contratTransformer->getContratTransverse($object);

        /** @var Etablissement $etablissement */
        $etablissement = $this->entityManager
            ->getRepository(Etablissement::class)
            ->findOneBy(['idEtablissement' => $object->getIdTitulaireEtab()])
        ;
        $output->etablissement = $etablissement ? $this->iriConverter->getIriFromItem($etablissement) : null;
        $output->entreprise = $etablissement && $etablissement->getEntreprise()
            ? $this->iriConverter->getIriFromItem($etablissement->getEntreprise())
            : null
        ;

        // Clauses Sociales et environnementale
        $output->clauses = $this->clausesTransformer->getNestedClauses($object);

        $output->numEj = $object->getNumEJ();
        $output->statutEj = $object->getStatutEJ();
        $output->lienAcSad = $object->getLienACSAD();

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return ContratTitulaireOutput::class === $to && $data instanceof ContratTitulaire;
    }
}
