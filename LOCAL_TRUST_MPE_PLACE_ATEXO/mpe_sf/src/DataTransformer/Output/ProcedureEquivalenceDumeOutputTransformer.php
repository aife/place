<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\ProcedureEquivalenceDumeOutput;
use App\Entity\Organisme;
use App\Entity\ProcedureEquivalenceDume;
use App\Entity\TypeProcedureDume;
use Doctrine\ORM\EntityManagerInterface;

class ProcedureEquivalenceDumeOutputTransformer implements DataTransformerInterface
{
    public function __construct(
        private IriConverterInterface $iriConverter,
        private EntityManagerInterface $entityManager
    ) {
    }

    public function transform($object, string $to, array $context = [])
    {
        /** @var $object ProcedureEquivalenceDume */

        $output = new ProcedureEquivalenceDumeOutput();
        $output->organisme = $this->iriConverter->getIriFromItem(
            $this->entityManager->getRepository(Organisme::class)->findOneByAcronyme($object->getOrganisme())
        );
        $typeProcedureDume = $this->entityManager->getRepository(TypeProcedureDume::class)->find($object->getIdTypeProcedureDume());
        $output->typePrecedureDume = $this->iriConverter->getIriFromItem(
            $typeProcedureDume
        );
        $output->labelTypePrecedureDume = $typeProcedureDume->getLibelle();

        $output->selected = (bool) $object->getSelectionner();
        $output->display = (bool) $object->getAfficher();
        $output->fixed = (bool) $object->getFiger();

        return $output;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return ProcedureEquivalenceDumeOutput::class === $to && $data instanceof ProcedureEquivalenceDume;
    }
}
