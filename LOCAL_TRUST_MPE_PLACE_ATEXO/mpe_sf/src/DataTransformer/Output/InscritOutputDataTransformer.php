<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\InscritOutput;
use App\Entity\Inscrit;
use App\Service\AtexoConfiguration;
use App\Service\Inscrit\InscritService;

final class InscritOutputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private readonly AtexoConfiguration $configuration,
        private readonly InscritService $inscritService,
        private readonly IriConverterInterface $iriConverter
    ) {
    }

    /**
     * {@inheritdoc}
     */
    public function transform($object, string $to, array $context = []): InscritOutput
    {
        /** @var Inscrit $object */

        $output = new InscritOutput();

        $output->identifiantTechnique = $object->getId();
        $output->prenom = $object->getPrenom();
        $output->nom = $object->getNom();
        $output->login = $object->getLogin();
        $output->email = $object->getEmail();
        $output->adresse = [
            "rue" => $object->getAdresse(),
            "codePostal" => $object->getCodepostal(),
            "ville" => $object->getVille(),
            "pays" => $object->getPays()
        ];
        $output->telephone = $object->getTelephone();
        $output->actif = !$object->getBloque();
        $output->siren = $object->getEntreprise()->getSiren();
        $output->codeEtablissement = $object->getEtablissement()->getCodeEtablissement();
        $output->inscritAnnuaireDefense = $object->getInscritAnnuaireDefense();

        $output->etablissement = $object->getEtablissement()
            ? $this->iriConverter->getIriFromItem($object->getEtablissement())
            : null
        ;

        $output->entreprise = $object->getEntreprise()
            ? $this->iriConverter->getIriFromItem($object->getEntreprise())
            : null
        ;

        if ($this->configuration->getConfigurationPlateforme()->isRecueilConsentementRgpd()) {
            $output->dateModificationRgpd = $object->getDateValidationRgpd();
            $output->rgpd = $this->inscritService->getRgpdLabels($object);
        }

        $output->idExterne = $object->getIdExterne();

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return InscritOutput::class === $to && $data instanceof Inscrit;
    }
}
