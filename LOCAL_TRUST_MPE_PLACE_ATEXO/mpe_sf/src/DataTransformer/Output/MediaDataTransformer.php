<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\MediaOutput;
use App\Entity\MediaUuid;

class MediaDataTransformer implements DataTransformerInterface
{
    public function transform($object, string $to, array $context = [])
    {
        $mediaObject = new MediaOutput();
        $mediaObject->id = $object->getUuid();
        $mediaObject->hash = $object->getMedia()->getHash();

        return $mediaObject;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return MediaOutput::class === $to && $data instanceof MediaUuid;
    }
}
