<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\EntrepriseOutput;
use App\Entity\Entreprise;

final class EntrepriseOutputDataTransformer implements DataTransformerInterface
{
    public function __construct(
    ) {
    }

    /**
     * {@inheritdoc}
     */
    public function transform($object, string $to, array $context = []): EntrepriseOutput
    {
        /** @var Entreprise $object */
        $output = new EntrepriseOutput();

        $output->id = $object->getId();
        $output->siren = $object->getSiren();
        $output->telephone = $object->getTelephone();
        $output->siteInternet = $object->getSiteInternet();
        $output->idExterne = $object->getIdExterne();
        $output->adresse = [
            "ville" => $object->getVille(),
            "rue" => $object->getAdresse(),
            "codePostal" => $object->getCodepostal(),
            "pays" => $object->getPays(),
            "acronymePays" => $object->getAcronymePays(),
        ];
        $output->formeJuridique = $object->getFormejuridique();
        $output->codeAPE = $object->getCodeape();
        $output->libelleAPE = $object->getLibelleApe();
        $output->dateModification = $object->getDateModification();
        $output->dateCreation = $object->getDateCreation();
        $output->capitalSocial = $object->getCapitalSocial();
        $output->raisonSociale = $object->getNom();

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return EntrepriseOutput::class === $to && $data instanceof Entreprise;
    }
}
