<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\ServiceOutput;
use App\Entity\AffiliationService;
use App\Entity\Service;
use Doctrine\ORM\EntityManagerInterface;

final class ServiceOutputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private readonly IriConverterInterface $iriConverter,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    /**
     * {@inheritdoc}
     * @param Service $object
     */
    public function transform($object, string $to, array $context = []): ServiceOutput
    {
        $output = new ServiceOutput();
        $siret = sprintf("%s%s", $object->getSiren(), $object->getComplement());

        $output->sigle = $object->getSigle();
        $output->siren = $object->getSiren();
        $output->siret = !empty($siret) ? $siret : null;
        $output->libelle = $object->getLibelle();
        $output->adresse = $object->getAdresse();
        $output->codePostal = $object->getCp();
        $output->ville = $object->getVille();
        $output->idExterne = $object->getIdExterne();
        $output->oldId = $object->getOldId();

        $affiliation = $this->entityManager
            ->getRepository(AffiliationService::class)
            ->findOneBy(['serviceId' => $object]);

        if (!empty($affiliation)) {
            $output->idParent = $this->iriConverter->getIriFromItem($affiliation->getServiceParentId());
        }

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return ServiceOutput::class === $to && $data instanceof Service;
    }
}
