<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\EtablissementOutput;
use App\Entity\Etablissement;

final class EtablissementOutputDataTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($object, string $to, array $context = []): EtablissementOutput
    {
        /** @var Etablissement $object */
        $output = new EtablissementOutput();

        $siret = sprintf("%s%s", $object->getEntreprise()?->getSiren(), $object->getCodeEtablissement());

        $output->id = $object->getIdEtablissement();
        $output->codeEtablissement = $object->getCodeEtablissement();
        $output->idExterne = $object->getIdExterne();
        $output->siege = $object->getEstSiege();
        $output->siret = !empty($siret) && $object->getEntreprise()?->getSiren() ? $siret : null;
        $output->dateCreation = $object->getDateCreation();
        $output->dateModification = $object->getDateModification();
        $output->adresse = [
            "rue" => $object->getAdresse(),
            "codePostal" => $object->getCodepostal(),
            "ville" => $object->getVille(),
            "pays" => $object->getPays()
        ];

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return EtablissementOutput::class === $to && $data instanceof Etablissement;
    }
}
