<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Doctrine\Extension\ConsultationIdExtension;
use App\Dto\Output\ConsultationOutput;
use App\Entity\Consultation;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\TypeProcedureDume;
use App\Repository\DecisionLotRepository;
use App\Service\AtexoConfiguration;
use App\Service\Consultation\ConsultationService;
use App\Service\DataToolBoxTrait;
use App\Service\DataTransformer\ClausesTransformer;
use App\Service\LotService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

final class ConsultationOutputDataTransformer implements DataTransformerInterface
{
    use DataToolBoxTrait;

    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly EntityManagerInterface $em,
        private readonly ClausesTransformer $clausesTransformer,
        private readonly ConsultationService $consultationService,
        private readonly AtexoConfiguration $configuration,
        private readonly IriConverterInterface $iriConverter,
        private ParameterBagInterface $parameterBag,
        private UrlGeneratorInterface $router,
        private DecisionLotRepository $decisionLotRepository,
        private readonly LotService $lotService
    ) {
    }

    /**
     * {@inheritdoc}
     *
     * @param Consultation $object
     */
    public function transform($object, string $to, array $context = []): ConsultationOutput
    {
        $output = new ConsultationOutput();

        $output->id = $object->getId();
        $output->statutCalcule = $object->getCalculatedStatus();

        // On ne renvoit que l'id dans le dto si le param ids_only est envoyé en paramètre
        if ($this->requestStack->getCurrentRequest()->query->has(ConsultationIdExtension::IDS_ONLY)) {
            return $output;
        }

        $typeProcedureOrganisme = $this->em->getRepository(TypeProcedureOrganisme::class)
            ->findOneBy([
                'idTypeProcedure' => $object->getIdTypeProcedureOrg(),
                'organisme' => $object->getOrganisme()
            ]);

        $output->codeExterne = $object->getCodeExterne();
        $output->organisme = $object->getOrganisme() ?
            $this->iriConverter->getIriFromItem($object->getOrganisme()) : '';
        $output->directionService = $object->getService() ?
            $this->iriConverter->getIriFromItem($object->getService()) : '';
        $output->naturePrestation = $this->iriConverter->getIriFromItem($object->getCategorieConsultation());
        $output->typeProcedure = $typeProcedureOrganisme ?
            $this->iriConverter->getIriFromItem($typeProcedureOrganisme) : '';
        $output->typeContrat = $object->getTypeMarche() ?
            $this->iriConverter->getIriFromItem($object->getTypeMarche()) : '';
        $output->donneeComplementaire = $object->getDonneeComplementaire() ?
            $this->iriConverter->getIriFromItem($object->getDonneeComplementaire()) : '';

        $output->reference = $object->getReferenceUtilisateur();
        $output->intitule = htmlspecialchars_decode(html_entity_decode($object->getIntitule()));
        $output->objet = htmlspecialchars_decode(html_entity_decode($object->getObjet()));
        $output->commentaireInterne = htmlspecialchars_decode(html_entity_decode($object->getChampSuppInvisible()));
        $output->idCreateur = $object->getIdCreateur();
        $output->dume = (bool)$object->getDumeDemande();
        if ($object->getTypeProcedureDume()) {
            $output->typeProcedureDume = $this->iriConverter->getIriFromItem(
                $this->em->getRepository(TypeProcedureDume::class)->findOneById($object->getTypeProcedureDume())
            );
        }
        $output->valeurEstimee = $object->getDonneeComplementaire()?->getMontantMarche();
        $output->lieuxExecution = $this->consultationService->getDepartmentsWithLieuExecution($object);
        $output->dumeSimplifie = (bool) $object->getTypeFormulaireDume();
        $output->dateLimiteRemisePlis = $this->getDisplayedDateTime($object->getDatefin());
        $output->dateDebut = $object->getDatedebut();
        $output->datefinAffichage = $object->getAffichageDateFin();
        $output->dateLimiteRemiseOffres = $this->getDisplayedDateTime($object->getDatefin());
        $output->dateMiseEnLigneCalcule = $object->getDateMiseEnLigneCalcule();
        $output->alloti = (bool)$object->getAlloti();
        $output->dateModification = $object->getDateModification();
        $output->modeOuvertureReponse = $object->getModeOuvertureReponse();
        $output->signatureActeEngagement = $object->getSignatureActeEngagement();
        $output->marchePublicSimplifie = $object->getMarchePublicSimplifie();
        $output->codeProcedure = $object->getCodeProcedure();
        $output->urlConsultationExterne = (string)$object->getUrlConsultationExterne();
        $output->consultationExterne = $object->getConsultationExterne();
        $output->typeDecisionDeclarationSansSuite = $object->getTypeDecisionDeclarationSansSuite();
        $output->typeDecisionDeclarationInfructueux = $object->getTypeDecisionDeclarationInfructueux();
        $output->typeDecisionARenseigner = $object->getTypeDecisionARenseigner();
        $output->typeDecisionAutre = $object->getTypeDecisionAutre();
        $output->typeDecisionAttributionMarche = $object->getTypeDecisionAttributionMarche();
        $output->typeDecisionAttributionAccordCadre = $object->getTypeDecisionAttributionAccordCadre();
        $output->typeDecisionAdmissionSad = $object->getTypeDecisionAdmissionSad();
        $output->lieuxExecution = $this->consultationService->getDepartmentsWithLieuExecution($object);
        $output->autreTechniqueAchat = $object->getAutreTechniqueAchat();
        $output->dateLimiteRemisePlisLocale = $object->getDateFinLocale();
        $output->lieuResidence = $object->getLieuResidence();

        // consultation parent
        if (!empty($object->getReferenceConsultationInit())) {
            $consultationParent = $this->em->getRepository(Consultation::class)->find(
                $object->getReferenceConsultationInit()
            );
            $output->consultationParent = $consultationParent
                ? $this->iriConverter->getIriFromItem($consultationParent)
                : null
            ;
        }

        $output->codeCpvPrincipal = $object->getCodeCpv1();
        // Explode codeCpv2 separated by #
        $separatedCodeCpv = $this->lotService->getExplodedCodeCpv($object->getCodeCpv2(), $this->lotService::SEPARATOR);
        if ($separatedCodeCpv) {
            $output->codeCpvSecondaire1 = $separatedCodeCpv[0] ?? null;
            $output->codeCpvSecondaire2 = $separatedCodeCpv[1] ?? null;
            $output->codeCpvSecondaire3 = $separatedCodeCpv[2] ?? null;
        }

        if (!(bool)$object->getAlloti()) {
            // Clauses Sociales et environnementales
            $output->clauses = $this->clausesTransformer->getNestedClauses($object);
        }

        // Organisme Decentralisé - on récupére la donnée dans conf_organisme dans le champ organisme_centralisee
        $confOrganisme = $this->configuration->getConfigurationOrganisme($object->getOrganisme());
        $output->organismeDecentralise = !$confOrganisme->getOrganisationCentralisee();

        $output->bourseCotraitance = (bool)$this->configuration->getConfigurationPlateforme()->getBourseCotraitance();
        $output->chiffrement = (bool)$object->getChiffrementOffre();
        $output->signatureElectronique = $this->consultationService->getSignatureElectronique($object);
        $output->reponseElectronique = $this->consultationService->getTypeResponse($object);
        $output->enveloppeCandidature = (bool)$object->getEnvCandidature();
        $output->enveloppeAnonymat = (bool)$object->getEnvAnonymat();
        $output->enveloppeOffre = (bool)$object->getEnvOffre();
        $output->enveloppeOffreTechnique = (bool)$object->getEnvOffreTechnique();
        $output->nombreLots = $object->isAlloti() ? $object->getLots()->count() : null;
        $output->entiteAdjudicatrice = $object->getEntiteAdjudicatrice();
        $output->urlConsultation = rtrim($this->parameterBag->get('PF_URL_REFERENCE'), '/')
            . $this->router->generate(
                'atexo_consultation_index',
                ['id' => $object->getId(), 'orgAcronyme' => $object->getAcronymeOrg()]
            );
        $output->poursuivreAffichage = $object->getPoursuivreAffichage();
        $output->poursuivreAffichageUnite = $object->getPoursuivreAffichageUnite();
        $output->donneeComplementaireObligatoire = (bool) $object->getDonneeComplementaireObligatoire();
        $output->donneePubliciteObligatoire = (bool) $object->getDonneePubliciteObligatoire();
        if ($this->configuration->getConfigurationPlateforme()->getCaseAttestationConsultation()) {
            $output->attestationConsultation = (bool) $object->getAttestationConsultation();
        }
        $output->consultationRestreinte = Consultation::TYPE_ACCES_RESTREINT === $object->getTypeAcces();
        $output->numeroSad = $object->getNumeroAc();
        $output->procedureOuverte = $object->isProcedureOuverte();
        $output->groupement = $object->isGroupement();
        $output->justificationNonAlloti = $object->getDonneeComplementaire()?->getJustificationNonAlloti();
        $output->referentielAchat = $object->getReferentielAchat()
                                    ? $this->iriConverter->getIriFromItem($object->getReferentielAchat())
                                    : null;
        $output->adresseRetraisDossiers = $object->getAdresseRetraisDossiers();
        $output->adresseDepotOffres = $object->getAdresseDepotOffres();
        $output->lieuOuverturePlis =  $object->getLieuOuverturePlis();

        $output->lieuxExecution = $this->consultationService->getDepartmentsWithLieuExecution($object);
        $output->departmentsWithNames = $this->consultationService->getDepartmentsWithLieuExecution($object, true);
        $output->departmentsWithNames = empty($output->departmentsWithNames) ? null : $output->departmentsWithNames;
        if (!$object->isAlloti()) {
            $decision = $this->decisionLotRepository->findOneBy(['consultation' => $object->getId(), 'lot' => 0]);
            $output->typeDecision = $decision?->getIdTypeDecision()?->getCodeTypeDecision();
            $output->dateDecision = $decision?->getDateDecision();
        }

        if ($this->configuration->getConfigurationPlateforme()->getConsultationAdresseRetraisDossiers()) {
            $output->modaliteRetraitDossier = htmlspecialchars_decode(html_entity_decode($object->getAdresseRetraisDossiers()));
        }
        if ($this->configuration->getConfigurationPlateforme()->getConsultationAdresseDepotOffres()) {
            $output->receptionOffresCandidatures = htmlspecialchars_decode(html_entity_decode($object->getAdresseDepotOffres()));
        }
        if ($this->configuration->getConfigurationPlateforme()->getConsultationLieuOuverturePlis()) {
            $output->lieuOuvertureOffresRemiseCandidatures = htmlspecialchars_decode(html_entity_decode($object->getLieuOuverturePlis()));
        }
        if ($this->configuration->getConfigurationPlateforme()->getCodeNutLtReferentiel()) {
            $output->codesNuts = $this->consultationService->getDepartmentsWithCodeNuts($object);
        }
        $output->dcePartialDownload = (bool)$object->getPartialDceDownload();

        $output->dateValidation = $object->getDateValidation();
        $output->etatValidation = (bool)$object->getEtatValidation();

        $output->besoinRecurrent = $object->getBesoinRecurrent();

        $output->uuid = $object->getUuid();
        $output->idContratTitulaire = $object->getIdContrat();

        $output->lieuxWithCodeInterface = $this->consultationService->getDepartmentsWithLieuExecution($object, withCodeInterface: true);
        $output->sourceExterne = $object->getSourceExterne();
        $output->idSourceExterne = $object->getIdSourceExterne();
        $output->idDossierVolumineux = $object->getDossierVolumineux()?->getId();
        $output->isEnvoiPubliciteValidation = $object->getIsEnvoiPubliciteValidation();
        $output->annexeFinanciere = $object->getAnnexeFinanciere();
        $output->cmsActif = $object->getCmsActif();
        $output->controleTailleDepot = $object->getControleTailleDepot();
        $output->numeroProjetAchat = $object->getNumeroProjetAchat();
        $output->marcheInsertion = $object->getMarcheInsertion();
        $output->clauseSpecificationTechnique = $object->getClauseSpecificationTechnique();
        $output->oldServiceId = $object->getOldServiceId();

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return ConsultationOutput::class === $to && $data instanceof Consultation ;
    }
}
