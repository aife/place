<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\OrganismeOutput;
use App\Entity\Organisme;

final class OrganismeOutputDataTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($object, string $to, array $context = []): OrganismeOutput
    {
        /** @var Organisme $object */
        $siret = sprintf("%s%s", $object->getSiren(), $object->getComplement());

        $output = new OrganismeOutput();
        $output->id = $object->getId();
        $output->sigle = $object->getSigle();
        $output->denomination = $object->getDenominationOrg();
        $output->siret = !empty($siret) ? $siret : null;
        $output->description = $object->getDescriptionOrg();
        $output->acronyme = $object->getAcronyme();
        $output->adresse = $object->getAdresse();
        $output->codePostal = $object->getCp();
        $output->ville = $object->getVille();
        $output->idExterne = $object->getIdExterne();
        $output->formeJuridique = $object->getCategorieInsee();

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return OrganismeOutput::class === $to && $data instanceof Organisme;
    }
}
