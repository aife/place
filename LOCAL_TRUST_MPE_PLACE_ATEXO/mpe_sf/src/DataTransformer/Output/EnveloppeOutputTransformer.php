<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\EnveloppeOutput;
use App\Dto\Output\FichierEnveloppeOutput;
use App\Entity\BloborganismeFile;
use App\Entity\Enveloppe;
use App\Entity\EnveloppeFichier;

class EnveloppeOutputTransformer implements DataTransformerInterface
{
    public function __construct(private readonly IriConverterInterface $iriConverter, private FichierEnveloppeOutputTransformer $transformer)
    {
    }

    /**
     * @param Enveloppe $object
     * @inheritDoc
     */
    public function transform($object, string $to, array $context = [])
    {
        $enveloppeOutput = new EnveloppeOutput();
        $enveloppeOutput->offre = $this->iriConverter->getIriFromItem($object->getOffre());
        $enveloppeOutput->type = Enveloppe::TYPE_ENVELOPPE_BY_ID[$object->getTypeEnv()];
        $enveloppeOutput->numeroLot = $object->getSousPli();
        foreach ($object->getFichierEnveloppes() as $ficherEnvelop) {
            $enveloppeOutput->fichiersEtSignature[] = $ficherEnvelop;
        }

        return $enveloppeOutput;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return EnveloppeOutput::class === $to && $data instanceof Enveloppe;
    }
}
