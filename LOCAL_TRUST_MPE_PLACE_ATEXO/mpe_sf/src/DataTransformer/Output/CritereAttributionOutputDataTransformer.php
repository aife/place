<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\CritereAttributionOutput;
use App\Entity\Consultation\CritereAttribution;

final class CritereAttributionOutputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private readonly IriConverterInterface $iriConverter
    ) {
    }

    /**
     * @param CritereAttribution $object
     */
    public function transform($object, string $to, array $context = []): CritereAttributionOutput
    {
        $output = new CritereAttributionOutput();

        $output->donneeComplementaire = $this->iriConverter->getIriFromItem($object->getDonneeComplementaire());
        $output->lot = $object->getLot() ? $this->iriConverter->getIriFromItem($object->getLot()) : null;
        $output->enonce = $object->getEnonce();
        $output->ordre = $object->getOrdre();
        $output->ponderation = $object->getPonderation();

        $sousCritereAttributions = [];
        foreach ($object->getSousCriteresCollection() as $sousCritere) {
            $sousCritereAttributions[] = $this->iriConverter->getIriFromItem($sousCritere);
        }
        $output->sousCritereAttributions = $sousCritereAttributions;

        return $output;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return CritereAttributionOutput::class === $to && $data instanceof CritereAttribution;
    }
}
