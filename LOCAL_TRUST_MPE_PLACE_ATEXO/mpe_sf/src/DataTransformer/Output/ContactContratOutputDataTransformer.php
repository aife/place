<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\ContactContratOutput;
use App\Entity\ContactContrat;

final class ContactContratOutputDataTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     * @param ContactContrat $object
     */
    public function transform($object, string $to, array $context = []): ContactContratOutput
    {
        $output = new ContactContratOutput();
        $output->nom = $object->getNom();
        $output->prenom = $object->getPrenom();
        $output->telephone = $object->getTelephone();
        $output->email = $object->getEmail();

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return ContactContratOutput::class === $to && $data instanceof ContactContrat;
    }
}
