<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\AgentOutput;
use App\Entity\Agent;
use App\Entity\HabilitationProfil;
use App\Repository\ConfigurationOrganismeRepository;
use App\Service\Consultation\ConsultationService;
use Doctrine\ORM\EntityManagerInterface;

class AgentOutputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private readonly IriConverterInterface $iriConverter,
        private readonly EntityManagerInterface $em,
        private ConsultationService $consultationService,
        private ConfigurationOrganismeRepository $configurationOrganismeRepository
    ) {
    }

    /**
     * @param Agent $object
     */
    public function transform($object, string $to, array $context = [])
    {
        $agentOutput = new AgentOutput();
        $agentOutput->id = $object->getId();
        $agentOutput->login = $object->getIdentifiant();
        $agentOutput->email = $object->getEmail();
        $agentOutput->nom = $object->getNom();
        $agentOutput->prenom = $object->getPrenom();
        $agentOutput->organisme = $object->getOrganisme()
            ? $this->iriConverter->getIriFromItem($object->getOrganisme())
            : null;
        $agentOutput->dateCreation = $object->getDateCreation();
        $agentOutput->dateModification = $object->getDateModification();
        $agentOutput->idExterne = $object->getIdExterne();
        $agentOutput->actif = (bool) $object->getActif();
        $agentOutput->telephone = $object->getTelephone();
        if ($object->getIdProfilSocleExterne() > 0) {
            $profil = $this->em->getRepository(HabilitationProfil::class)->findOneById(
                $object->getIdProfilSocleExterne()
            );
            $agentOutput->profil = $this->iriConverter->getIriFromItem($profil);
        }
        $agentOutput->service = $object->getService()
            ? $this->iriConverter->getIriFromItem($object->getService())
            : null
        ;

        $agentOutput->libelleDetail = $object->getDescriptionDetails();
        if ($object->getLieuExecution()) {
            $agentOutput->lieuExecution = $this->consultationService->getDepartmentsWithLieuExecution(
                $object->getLieuExecution()
            );
        }

        $modules = $this->configurationOrganismeRepository->createQueryBuilder('m')
                    ->where('m.organisme = :organism')
                    ->setParameter('organism', $object->getOrganisme()?->getAcronyme())
                    ->getQuery()->getArrayResult();
        foreach ($modules[0] as $key => $values) {
            if (in_array(needle: $values, haystack: [true, '1', 1], strict: true)) {
                $agentOutput->modulesOrganisme[] = $key;
            }
        }

        return $agentOutput;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return AgentOutput::class === $to && $data instanceof Agent;
    }
}
