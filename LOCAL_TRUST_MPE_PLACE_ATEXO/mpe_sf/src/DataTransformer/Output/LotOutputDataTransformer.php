<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\LotOutput;
use App\Entity\Consultation\ClausesN1;
use App\Entity\Lot;
use App\Repository\DecisionLotRepository;
use App\Service\DataTransformer\ClausesTransformer;
use App\Service\LotService;

final class LotOutputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private readonly IriConverterInterface $iriConverter,
        private readonly LotService $lotService,
        private readonly ClausesTransformer $clausesTransformer,
        private DecisionLotRepository $decisionLotRepository
    ) {
    }

    /**
     * {@inheritdoc}
     *
     * @var Lot $object
     */
    public function transform($object, string $to, array $context = []): LotOutput
    {
        $output = new LotOutput();

        $output->consultation = $this->iriConverter->getIriFromItem($object->getConsultation());

        if ($donneComplementaire = $object->getDonneeComplementaire()) {
            $output->donneeComplementaire = $this->iriConverter->getIriFromItem($donneComplementaire);
        }

        $output->numero = $object->getLot();
        $output->intitule = $object->getDescription();
        $output->descriptionSuccinte = $object->getDescriptionDetail();
        $output->naturePrestation = $this->iriConverter->getIriFromItem($object->getCategorie());
        $output->codeCpvPrincipal = $object->getCodeCpv1();
        $output->decision = $object->getDecision();
        $output->marcheInsertion = $object->getMarcheInsertion();
        $output->idTraductionDescription = $object->getIdTrDescription();
        $output->idTraductionDescriptionDetail = $object->getIdTrDescriptionDetail();

        // Explode codeCpv2 separated by #
        $separatedCodeCpv = $this->lotService->getExplodedCodeCpv($object->getCodeCpv2(), $this->lotService::SEPARATOR);
        if ($separatedCodeCpv) {
            $output->codeCpvSecondaire1 = $separatedCodeCpv[0] ?? null;
            $output->codeCpvSecondaire2 = $separatedCodeCpv[1] ?? null;
            $output->codeCpvSecondaire3 = $separatedCodeCpv[2] ?? null;
        }

        // Clauses Sociales et environnementale
        $output->clauses = $this->clausesTransformer->getNestedClauses($object);

        $decision = $this->decisionLotRepository->findOneBy([
            'consultation' => $object->getConsultation()->getId(),
            'lot' => $object->getLot()
        ]);
        $output->typeDecision = $decision?->getIdTypeDecision()?->getCodeTypeDecision();

        $output->dateDecision = $decision?->getDateDecision();

        $output->marcheInsertion = $object->getMarcheInsertion();
        $output->clauseSpecificationTechnique = $object->getClauseSpecificationTechnique()
            ? (bool) $object->getClauseSpecificationTechnique()
            : null;

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return LotOutput::class === $to && $data instanceof Lot;
    }
}
