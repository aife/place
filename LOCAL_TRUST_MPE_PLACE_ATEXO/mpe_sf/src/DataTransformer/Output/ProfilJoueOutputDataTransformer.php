<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\ProfilJoueOutput;
use App\Entity\GeolocalisationN2;
use App\Entity\TProfilJoue;
use Doctrine\ORM\EntityManagerInterface;

final class ProfilJoueOutputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private IriConverterInterface $iriConverter,
        private EntityManagerInterface $entityManager
    ) {
    }

    /**
     * {@inheritdoc}
     * @param TProfilJoue $object
     */
    public function transform($object, string $to, array $context = []): ProfilJoueOutput
    {
        $output = new ProfilJoueOutput();

        $output->agent = $this->iriConverter->getIriFromItem($object->getAgent());
        $output->organisme = $this->iriConverter->getIriFromItem($object->getOrganisme());

        $geolocalisationN2 = $this->entityManager->getRepository(GeolocalisationN2::class)->find($object->getPays());

        $output->nomOfficiel = $object->getNomOfficiel();
        $output->pays = $geolocalisationN2 ? $geolocalisationN2->getDenomination1() : '';
        $output->ville = $object->getVille();
        $output->adresse = $object->getAdresse();
        $output->codePostal = $object->getCodePostal();
        $output->pointContact = $object->getPointContact();
        $output->aAttentionDe = $object->getAAttentionDe();
        $output->telephone = $object->getTelephone();
        $output->fax = $object->getFax();
        $output->email = $object->getEmail();
        $output->adressePouvoirAdjudicateur = $object->getAdressePouvoirAdjudicateur();
        $output->adresseProfilAcheteur = $object->getAdresseProfilAcheteur();
        $output->autoriteNationale = $object->getAutoriteNationale();
        $output->officeNationale = $object->getOfficeNationale();
        $output->collectiviteTerritoriale = $object->getCollectiviteTerritoriale();
        $output->officeRegionale = $object->getOfficeRegionale();
        $output->organismePublic = $object->getOrganismePublic();
        $output->organisationEuropenne = $object->getOrganisationEuropenne();
        $output->autreTypePouvoirAdjudicateur = $object->getAutreTypePouvoirAdjudicateur();
        $output->autreLibelleTypePouvoirAdjudicateur = $object->getAutreLibelleTypePouvoirAdjudicateur();
        $output->servicesGeneraux = $object->getServicesGeneraux();
        $output->defense = $object->getDefense();
        $output->securitePublic = $object->getSecuritePublic();
        $output->environnement = $object->getEnvironnement();
        $output->developpementCollectif = $object->getDeveloppementCollectif();
        $output->affairesEconomiques = $object->getAffairesEconomiques();
        $output->sante = $object->getSante();
        $output->protectionSociale = $object->getProtectionSociale();
        $output->loisirs = $object->getLoisirs();
        $output->education = $object->getEduction();
        $output->autreActivitesPrincipales = $object->getAutreActivitesPrincipales();
        $output->autreLibelleActivitesPrincipales = $object->getAutreLibelleActivitesPrincipales();
        $output->pouvoirAdjudicateurAgit = $object->getPouvoirAdjudicateurAgit();
        $output->pouvoirAdjudicateurMarcheCouvert = $object->getPouvoirAdjudicateurMarcheCouvert();
        $output->entiteAdjudicatriceMarcheCouvert = $object->getEntiteAdjudicatriceMarcheCouvert();
        $output->numeroNationalIdentification = $object->getNumeroNationalIdentification();

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return ProfilJoueOutput::class === $to && $data instanceof TProfilJoue;
    }
}
