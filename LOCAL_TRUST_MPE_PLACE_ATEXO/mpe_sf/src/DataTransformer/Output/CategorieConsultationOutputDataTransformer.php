<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\CategorieConsultationOutput;
use App\Entity\CategorieConsultation;

final class CategorieConsultationOutputDataTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($object, string $to, array $context = []): CategorieConsultationOutput
    {
        /** @var CategorieConsultation $object */

        $output = new CategorieConsultationOutput();
        $output->libelle = $object->getLibelle();
        $output->idExterne = $object->getIdExterne();

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return CategorieConsultationOutput::class === $to && $data instanceof CategorieConsultation;
    }
}
