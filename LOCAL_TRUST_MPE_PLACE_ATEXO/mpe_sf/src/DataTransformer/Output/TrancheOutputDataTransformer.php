<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\TrancheOutput;
use App\Entity\Tranche;
use App\Repository\FormePrixRepository;

class TrancheOutputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private IriConverterInterface $iriConverter,
        private FormePrixRepository $formePrixRepository
    ) {
    }

    public function transform($object, string $to, array $context = [])
    {
        $output = new TrancheOutput();
        $output->codeTranche = $object->getCodeTranche();
        $output->natureTranche = $object->getNatureTranche();
        $output->intituleTranche = $object->getIntituleTranche();
        if ($object->getIdDonneeComplementaire()) {
            $output->donneesComplementaire = $this->iriConverter->getIriFromItem($object->getIdDonneeComplementaire());
        }
        if ($object->getIdFormePrix()) {
            $output->formePrix = $this->iriConverter->getIriFromItem(
                $this->formePrixRepository->find($object->getIdFormePrix())
            );
        }

        return $output;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return TrancheOutput::class === $to && $data instanceof Tranche;
    }
}
