<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\FormePrixOutput;
use App\Entity\Consultation\DonneeComplementaire;
use App\Entity\FormePrix;
use App\Entity\ValeurReferentiel;
use App\Repository\FormePrixHasRefTypePrixRepository;
use App\Repository\FormePrixPfHasRefVariationRepository;
use App\Repository\FormePrixPuHasRefVariationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class FormePrixOutputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private EntityManagerInterface $em,
        private ParameterBagInterface $parameterBag,
        private FormePrixHasRefTypePrixRepository $typePrixRepository,
        private FormePrixPfHasRefVariationRepository $variationPfRepository,
        private FormePrixPuHasRefVariationRepository $variationPuRepository,
        private IriConverterInterface $iriConverter
    ) {
    }
    public function transform($object, string $to, array $context = [])
    {
        $output = new FormePrixOutput();
        $variationsPrix = null;
        $output->typeFormePrix = $object->getFormePrix();
        if ($object->getModalite()) {
            $output->modalite = $object->getModalite();
        }
        if ($object->getIdMinMax()) {
            $unites = $this->em->getRepository(ValeurReferentiel::class)->getReferentielsById(
                $this->parameterBag->get('REFERENTIEL_UNITE_PRIX')
            );
            $output->uniteMinMax = $unites[$object->getIdMinMax()];
        }

        if ($object->getPuMin()) {
            $output->puMin = (float)$object->getPuMin();
        }
        if ($object->getPuMax()) {
            $output->puMax = (float)$object->getPuMax();
        }

        $variationPf = $this->variationPfRepository->findOneBy(['idFormePrix' => $object->getIdFormePrix()]);
        if ($variationPf) {
            $variationsPrix = $this->em->getRepository(ValeurReferentiel::class)->getReferentielsById(
                $this->parameterBag->get('REFERENTIEL_VARIATION_PRIX')
            );

            $output->variationPrixForfaitaire = $variationsPrix[$variationPf->getIdVariation()];
        }

        $variationPu = $this->variationPuRepository->findOneBy(['idFormePrix' => $object->getIdFormePrix()]);
        if ($variationPu) {
            if (null === $variationsPrix) {
                $variationsPrix = $this->em->getRepository(ValeurReferentiel::class)->getReferentielsById(
                    $this->parameterBag->get('REFERENTIEL_VARIATION_PRIX')
                );
            }

            $output->variationPrixUnaitaire = $variationsPrix[$variationPu->getIdVariation()];
        }

        $typesPrix = $this->typePrixRepository->findBy(['idFormePrix' => $object->getIdFormePrix()]);
        if (count($typesPrix)) {
            $typesPrixReferentiel = $this->em->getRepository(ValeurReferentiel::class)->getReferentielsById(
                $this->parameterBag->get('REFERENTIEL_TYPE_PRIX')
            );
            foreach ($typesPrix as $type) {
                $output->typePrix[] = $typesPrixReferentiel[$type->getIdTypePrix()];
            }
        }

        $donneesComplementaire = $this->em->getRepository(DonneeComplementaire::class)
            ->findOneBy(['idFormePrix' => $object->getIdFormePrix()]);
        if ($donneesComplementaire) {
            $output->donneesComplementaire = $this->iriConverter->getIriFromItem($donneesComplementaire);
        }

        return $output;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return FormePrixOutput::class === $to && $data instanceof FormePrix;
    }
}
