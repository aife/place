<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\TypeContratOutput;
use App\Entity\TypeContrat;
use Symfony\Contracts\Translation\TranslatorInterface;

final class TypeContratOutputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private readonly TranslatorInterface $translator,
    ) {
    }

    /**
     * {@inheritdoc}
     */
    public function transform($object, string $to, array $context = []): TypeContratOutput
    {
        /** @var TypeContrat $object */
        $output = new TypeContratOutput();

        $output->libelle = $this->translator->trans($object->getLibelleTypeContrat());
        $output->untranslatedLibelle = $object->getLibelleTypeContrat();
        $output->abreviation = $object->getAbreviationTypeContrat();
        $output->idExterne = $object->getIdExterne();
        $output->codeExterne = $object->getIdExterne();
        $output->typeTechniqueAchat = $object->getTechniqueAchat()?->getLibelle();
        $output->marcheSubsequent = $object->getMarcheSubsequent();
        $output->accordCadreSad = $object->getAccordCadreSad();

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return TypeContratOutput::class === $to && $data instanceof TypeContrat;
    }
}
