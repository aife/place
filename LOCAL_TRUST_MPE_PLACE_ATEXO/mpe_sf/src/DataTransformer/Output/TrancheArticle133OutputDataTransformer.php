<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\TrancheArticle133Output;
use App\Entity\TrancheArticle133;

final class TrancheArticle133OutputDataTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     * @param TrancheArticle133 $object
     */
    public function transform($object, string $to, array $context = []): TrancheArticle133Output
    {
        $output = new TrancheArticle133Output();
        $output->organisme = $object->getAcronymeOrg();
        $output->libelleTrancheBudgetaire = $object->getLibelleTrancheBudgetaire();

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return TrancheArticle133Output::class === $to && $data instanceof TrancheArticle133;
    }
}
