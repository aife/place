<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output\Referentiel;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\Referentiel\ClausesN2Output;
use App\Entity\Referentiel\Consultation\ClausesN2;
use Symfony\Contracts\Translation\TranslatorInterface;

final class ClausesN2OutputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private readonly TranslatorInterface $translator
    ) {
    }

    /**
     * {@inheritdoc}
     * @param ClausesN2 $object
     */
    public function transform($object, string $to, array $context = []): ClausesN2Output
    {
        $output = new ClausesN2Output();

        $output->label = $this->translator->trans($object->getLabel());
        $output->slug = $object->getSlug();
        $output->tooltip = $this->translator->trans($object->getTooltip());
        $output->actif = $object->isActif();
        $output->limitation = $object->getLimitation();

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return ClausesN2Output::class === $to && $data instanceof ClausesN2;
    }
}
