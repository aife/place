<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataTransformer\Output\Referentiel;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\Referentiel\ClausesN4Output;
use App\Entity\Referentiel\Consultation\ClausesN4;
use Symfony\Contracts\Translation\TranslatorInterface;

final class ClausesN4OutputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private readonly TranslatorInterface $translator,
        private readonly IriConverterInterface $iriConverter
    ) {
    }

    /**
     * {@inheritdoc}
     * @param ClausesN4 $object
     */
    public function transform($object, string $to, array $context = []): ClausesN4Output
    {
        $output = new ClausesN4Output();

        $output->label = $this->translator->trans($object->getLabel());
        $output->slug = $object->getSlug();
        $output->tooltip = $this->translator->trans($object->getTooltip());
        $output->actif = $object->getActif();
        $output->typeChamp = $object->getTypeChamp();
        $output->clauseN3 = $this->iriConverter->getIriFromItem($object->getClauseN3());

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return ClausesN4Output::class === $to && $data instanceof ClausesN4;
    }
}
