<?php

namespace App\Repository;

use App\Entity\FormePrix;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FormePrix>
 *
 * @method FormePrix|null find($id, $lockMode = null, $lockVersion = null)
 * @method FormePrix|null findOneBy(array $criteria, array $orderBy = null)
 * @method FormePrix[]    findAll()
 * @method FormePrix[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormePrixRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FormePrix::class);
    }
}
