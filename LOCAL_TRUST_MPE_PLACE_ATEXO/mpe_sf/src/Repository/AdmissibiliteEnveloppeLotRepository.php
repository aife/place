<?php

namespace App\Repository;

use App\Entity\AdmissibiliteEnveloppeLot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * AdmissibiliteEnveloppeLotRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AdmissibiliteEnveloppeLotRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdmissibiliteEnveloppeLot::class);
    }
}
