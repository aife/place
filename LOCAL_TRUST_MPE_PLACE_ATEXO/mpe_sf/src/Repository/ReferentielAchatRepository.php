<?php

namespace App\Repository;

use App\Entity\ReferentielAchat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ReferentielAchat>
 *
 * @method ReferentielAchat|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReferentielAchat|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReferentielAchat[]    findAll()
 * @method ReferentielAchat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReferentielAchatRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReferentielAchat::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(ReferentielAchat $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(ReferentielAchat $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
