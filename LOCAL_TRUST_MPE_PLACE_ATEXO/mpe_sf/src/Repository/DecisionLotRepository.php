<?php

namespace App\Repository;

use App\Entity\DecisionLot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DecisionLot>
 *
 * @method DecisionLot|null find($id, $lockMode = null, $lockVersion = null)
 * @method DecisionLot|null findOneBy(array $criteria, array $orderBy = null)
 * @method DecisionLot[]    findAll()
 * @method DecisionLot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DecisionLotRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DecisionLot::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(DecisionLot $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(DecisionLot $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
