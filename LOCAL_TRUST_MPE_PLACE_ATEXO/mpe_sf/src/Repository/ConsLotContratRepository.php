<?php

namespace App\Repository;

use App\Entity\ConsLotContrat;
use App\Entity\Consultation;
use App\Entity\ContratTitulaire;
use App\Entity\Entreprise;
use App\Entity\Lot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * ConsLotContratRepository.
 */
class ConsLotContratRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConsLotContrat::class);
    }

    /**
     * @param $organisme
     * @param $idConsultation
     *
     * @return array
     */
    public function getContratTitulaireIds($organisme, $idConsultation)
    {
        return $this->createQueryBuilder('clc')
            ->select('clc.idContratTitulaire as id')
            ->where('clc.organisme = :organisme')
            ->setParameter('organisme', $organisme)
            ->andWhere('clc.consultationId = :idConsultation')
            ->setParameter('idConsultation', $idConsultation)
            ->getQuery()
            ->getResult();
    }

    public function getListContrat($idConsultation)
    {
        return $this->createQueryBuilder('clc')
            ->select('ct.idContratTitulaire, ct.numeroContrat, ct.montantContrat, e.nom')
            ->leftJoin(ContratTitulaire::class, 'ct', 'WITH', 'ct.idContratTitulaire = clc.idContratTitulaire')
            ->leftJoin(Entreprise::class, 'e', 'WITH', 'e.id = ct.idTitulaire')
            ->where('clc.consultationId = :idConsultation')
            ->setParameter('idConsultation', $idConsultation)
            ->getQuery()
            ->getResult();
    }

    public function getListContratLotByIdConsultation(int $idConsultation)
    {
        return $this->createQueryBuilder('clc')
            ->select('clc.lot, clc.consultationId, clc.organisme, 
            CONCAT(co.referenceUtilisateur, \' - \' ,co.objet) as intitule, ct.nbTotalPropositionsLot,
            e.nom as entreprise, e.codepostal, e.villeadresse, e.id as entrepriseId, ct.statutContrat, 
            ct.dateNotification, ct.referenceLibre, l.description, ct.uuid')
            ->leftJoin(ContratTitulaire::class, 'ct', 'WITH', 'ct.idContratTitulaire = clc.idContratTitulaire')
            ->leftJoin(Consultation::class, 'co', 'WITH', 'co.id = clc.consultationId')
            ->leftJoin(Entreprise::class, 'e', 'WITH', 'e.id = ct.idTitulaire')
            ->leftJoin(Lot::class, 'l', 'WITH', 'clc.lot = l.id')
            ->where('clc.consultationId = :idConsultation')
            ->setParameter('idConsultation', $idConsultation)
            ->getQuery()
            ->getResult()
            ;
    }

    public function getConsultationIdByContratTitulaire($idContratTitulaire)
    {
        return $this->createQueryBuilder('clc')
            ->select('clc.consultationId as id')
            ->where('clc.idContratTitulaire = :idContratTitulaire')
            ->setParameter('idContratTitulaire', $idContratTitulaire)
            ->getQuery()
            ->getResult();
    }
}
