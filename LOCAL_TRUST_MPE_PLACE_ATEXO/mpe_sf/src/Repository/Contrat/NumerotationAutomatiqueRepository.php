<?php

namespace App\Repository\Contrat;

use App\Entity\Contrat\NumerotationAutomatique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class NumerotationAutomatiqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NumerotationAutomatique::class);
    }

    public function getAllNumerotationAutomatique(int $idService, string $organisme)
    {
        $criterias = ['organisme' => $organisme, 'serviceId' => $idService];

        return $this->findBy($criterias);
    }
}
