<?php

namespace App\Repository;

use App\Entity\BlobFichier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Repository BlobFichierRepository.
 *
 * @author Amal EL BEKKAOUI <amal@atexo.com>
 *
 * @since     ESR2016
 *
 * @copyright Atexo 2016
 */
class BlobFichierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BlobFichier::class);
    }

    /**
     * @return array
     */
    public function listCheck()
    {
        return $this->createQueryBuilder('b')
            ->select('b.id, b.hash')
            ->where('b.statutSynchro in (0,2)')
            ->getQuery()
            ->getResult();
    }
}
