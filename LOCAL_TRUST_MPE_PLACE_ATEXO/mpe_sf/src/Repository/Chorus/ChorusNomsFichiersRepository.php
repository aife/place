<?php

namespace App\Repository\Chorus;

use App\Entity\Chorus\ChorusNomsFichiers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ChorusNomsFichiersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ChorusNomsFichiers::class);
    }
}
