<?php

namespace App\Repository\Chorus;

use App\Entity\Chorus\ChorusGroupementAchat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ChorusGroupementAchatRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ChorusGroupementAchat::class);
    }
}
