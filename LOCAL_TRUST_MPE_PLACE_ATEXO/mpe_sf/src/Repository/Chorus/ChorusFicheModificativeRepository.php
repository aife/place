<?php

namespace App\Repository\Chorus;

use App\Entity\Chorus\ChorusFicheModificative;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ChorusFicheModificativeRepository
 * @package App\Repository\Chorus
 */
class ChorusFicheModificativeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ChorusFicheModificative::class);
    }
}