<?php

namespace App\Repository\Chorus;

use App\Entity\Chorus\ChorusFicheModificativePj;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ChorusFicheModificativePjRepository
 * @package App\Repository\Chorus
 */
class ChorusFicheModificativePjRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ChorusFicheModificativePj::class);
    }

}
