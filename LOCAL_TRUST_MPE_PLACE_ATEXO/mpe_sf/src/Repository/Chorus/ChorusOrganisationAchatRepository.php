<?php

namespace App\Repository\Chorus;

use App\Entity\Chorus\ChorusOrganisationAchat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ChorusOrganisationAchatRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ChorusOrganisationAchat::class);
    }
}
