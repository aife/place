<?php

namespace App\Repository\Chorus;

use DateTime;
use Generator;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use App\Entity\Chorus\ChorusEchange;
use App\Entity\ConfigurationOrganisme;
use App\Entity\ContratTitulaire;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ChorusEchangeRepository
 * @package App\Repository\Chorus
 */
class ChorusEchangeRepository extends ServiceEntityRepository
{
    public $headWs = 'echanges-chorus';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ChorusEchange::class);
    }

    public function findAllByParamWs(
        int $nombreElementsParPage,
        int $NumeroPage,
        DateTime $dateModification,
        string $module,
        array $orgServicesAllowed = [],
        array $tabParams = [],
        $searchId = false
    ): Generator {
        $queryBuilder = $this->createQueryBuilder('ce');
        if ($dateModification->getTimestamp() != 1) {
            $queryBuilder->where('ce.dateModification > :dateModification ');
            $queryBuilder->setParameter('dateModification', $dateModification->format('Y-m-d h:i:s'));
            $queryBuilder->orderBy('ce.dateModification', 'DESC');
        }


        if ($module === "EXEC") {
            $queryBuilder->join(ConfigurationOrganisme::class, 'c', 'WITH', 'ce.organisme = c.organisme ');
            $queryBuilder->andWhere(" c.moduleExec = '1' ");
        }
        $limit = $nombreElementsParPage;
        $offset = $this->getOffset($NumeroPage, $nombreElementsParPage);
        $queryBuilder->setFirstResult($offset);
        $queryBuilder->setMaxResults($limit);

        $chorusEchanges = $queryBuilder->getQuery()->getResult();

        foreach ($chorusEchanges as $chorusEchange) {
            yield $chorusEchange;
            $this->_em->detach($chorusEchange);
        }
    }

    public function getOffset(int $page, int $limit): float|int
    {
        $offset = 0;
        if ($page != 0 && $page != 1) {
            $offset = ($page - 1) * $limit;
        }
        return $offset;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateChorusEchange(ChorusEchange $chorusEchange): void
    {
        if ($chorusEchange instanceof ChorusEchange) {
            $this->_em->persist($chorusEchange);
            $this->_em->flush();
        }
    }

    public function getChorusEchangeByNumLongOeap(array $numLongOeap, int $limit, int $offset): array
    {
        $qb = $this->createQueryBuilder('ce')
            ->select('distinct
            e.nom as RaisonSocialeAttributaire,
            e.siren as sirenEntreprise,
            et.codeEtablissement as codeEtablissement,
            e.codeape as CodeAPE,
            e.formejuridique as FormeJuridique,
            e.categorieEntreprise as PME,
            e.paysadresse as PaysTerritoire,
            e.sirenetranger as NumeroNationalAttributaire,
            ct.numLongOEAP as numLongOEAP
            ')
            ->leftJoin(
                ContratTitulaire::class,
                'ct',
                'WITH',
                'ce.idDecision = ct.idContratTitulaire'
            )
            ->leftJoin(
                Entreprise::class,
                'e',
                'WITH',
                'ct.idTitulaire = e.id'
            )
            ->leftJoin(
                Etablissement::class,
                'et',
                'WITH',
                ' ct.idTitulaireEtab = et.idEtablissement'
            )
            ->andWhere('ct.numLongOEAP IN (:numLongOEAP)')
            ->setParameter('numLongOEAP', $numLongOeap)
            ->orderBy('e.nom', 'ASC')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
        ;

        return $qb->getQuery()->getArrayResult();
    }

    public function getPendingEchangeByContrat(string $uuidContrat, ?int $idEchangeExcluded = null): ?array
    {
        $query = $this->createQueryBuilder('e')
            ->where('e.uuidExterneExec = :uuidContrat')
            ->andWhere('e.retourChorus = :retourChorus')
            ->setParameter('uuidContrat', $uuidContrat, 'uuid')
            ->setParameter('retourChorus', ChorusEchange::CHORUS_RETURN_STATUS_IN_PROGRESS)
        ;
        if ($idEchangeExcluded) {
            $query->andWhere('e.id != :idEchangeExcluded')
                ->setParameter('idEchangeExcluded', $idEchangeExcluded);
        }

        return $query->getQuery()->getResult();
    }

    public function getFailedEchangeByContrat(string $uuidContrat, ?int $idEchangeExcluded = null): ?array
    {
        $query = $this->createQueryBuilder('e')
            ->where('e.uuidExterneExec = :uuidContrat')
            ->andWhere('e.statutEchange IN( :statutEchange)')
            ->setParameter('uuidContrat', $uuidContrat, 'uuid')
            ->setParameter('statutEchange', [
                ChorusEchange::ECHANGE_STATUS_DRAFT,
                ChorusEchange::ECHANGE_STATUS_ERROR
            ])
        ;
        if ($idEchangeExcluded) {
            $query->andWhere('e.id != :idEchangeExcluded')
                ->setParameter('idEchangeExcluded', $idEchangeExcluded);
        }

        return $query->getQuery()->getResult();
    }
}
