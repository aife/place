<?php

namespace App\Repository;

use App\Entity\FormePrixHasRefTypePrix;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FormePrixHasRefTypePrix>
 *
 * @method FormePrixHasRefTypePrix|null find($id, $lockMode = null, $lockVersion = null)
 * @method FormePrixHasRefTypePrix|null findOneBy(array $criteria, array $orderBy = null)
 * @method FormePrixHasRefTypePrix[]    findAll()
 * @method FormePrixHasRefTypePrix[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormePrixHasRefTypePrixRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FormePrixHasRefTypePrix::class);
    }
}
