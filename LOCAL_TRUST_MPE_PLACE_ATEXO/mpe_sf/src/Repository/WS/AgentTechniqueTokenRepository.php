<?php

namespace App\Repository\WS;

use Doctrine\ORM\EntityRepository;

class AgentTechniqueTokenRepository extends EntityRepository
{
    /**
     * @param $token
     *
     * @return bool|mixed
     */
    public function checkToken($token)
    {
        $qb = $this->createQueryBuilder('att');

        $qb->where('att.token = :token')
            ->setParameter('token', $token)
            ->andWhere('att.dateExpiration >= :dateExpiration')
            ->setParameter('dateExpiration', date('Y-m-d H:i:s'));

        $result = $qb->getQuery()->getOneOrNullResult();

        return empty($result) ? false : $result;
    }
}
