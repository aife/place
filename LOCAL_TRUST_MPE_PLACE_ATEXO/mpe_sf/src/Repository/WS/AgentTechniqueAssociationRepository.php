<?php

namespace App\Repository\WS;

use App\Entity\Agent;
use Doctrine\ORM\EntityRepository;

/**
 * Class AgentTechniqueAssociationRepository.
 */
class AgentTechniqueAssociationRepository extends EntityRepository
{
    public function getAssociationAgentForOrgAndServices(Agent $agent, $org, $services)
    {
        $qb = $this->createQueryBuilder('ats');
        $qb->where('ats.idAgent = :idAgent');
        $qb->setParameter('idAgent', $agent->getId());
        $qb->andWhere('ats.organisme = :acronyme');
        $qb->setParameter('acronyme', $org);
        if (is_array($services) && !empty($services)) {
            $qb->andWhere('ats.serviceId in (:arrayIdsServices)');
            $qb->setParameter('arrayIdsServices', $services);
        }

        return $qb->getQuery()->getResult();
    }
}
