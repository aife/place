<?php

namespace App\Repository;

use App\Entity\ModificationContrat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ModificationContratRepository.
 */
class ModificationContratRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ModificationContrat::class);
    }

    public function findIfContratExists($idContratTitulaire)
    {
        return $this->createQueryBuilder('m')
            ->select('m')
            ->where('m.idContratTitulaire = :idContratTitulaire')
            ->setParameter('idContratTitulaire', $idContratTitulaire)
            ->getQuery()
            ->getResult();
    }
}
