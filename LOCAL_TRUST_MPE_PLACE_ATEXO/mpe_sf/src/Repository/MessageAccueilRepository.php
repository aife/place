<?php

namespace App\Repository;

use App\Entity\MessageAccueil;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

class MessageAccueilRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MessageAccueil::class);
    }

    public function getMessageAccueilAgentNonAuthentifie(): ?MessageAccueil
    {
        return $this->findOneBy(
            [
                'destinataire' => 'agent',
                'authentifier' => '0',
            ],
            ['id' => 'DESC']
        );
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getTypeMessage(?string $destinataire, ?string $authentifier = null): MessageAccueil
    {
        $query = $this->createQueryBuilder('ma')
            ->where('ma.destinataire = :destinataire')
            ->setParameter('destinataire', $destinataire)
            ->setMaxResults(1)
        ;

        if (!is_null($authentifier)) {
            $query
                ->andWhere('ma.authentifier = :authentifier')
                ->setParameter('authentifier', $authentifier)
            ;
        }

        return $query->getQuery()->getSingleResult();
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getContenu(?string $destinataire, ?string $authentifier = null): string
    {
        $query = $this->createQueryBuilder('ma') ->select('ma.contenu');
        $query
            ->where('ma.destinataire= :destinataire')
            ->setParameter('destinataire', $destinataire)
            ->setMaxResults(1)
        ;

        if (!is_null($authentifier)) {
            $query->andWhere('ma.authentifier= :authentifier')->setParameter('authentifier', $authentifier);
        }

        return $query->getQuery()->getSingleScalarResult();
    }

    public function updateMessageAccueil(
        string $typeMessage,
        string $contenu,
        string $destinataire,
        ?string $authentifier
    ): int {
        $query = $this->createQueryBuilder('ma')->update()
            ->set('ma.typeMessage', ':typeMessage')
            ->set('ma.contenu', ':contenu')
            ->where('ma.destinataire= :destinataire')
            ->setParameter('typeMessage', $typeMessage)
            ->setParameter('contenu', $contenu)
            ->setParameter('destinataire', $destinataire);

        if (trim($authentifier) !== '') {
            $query->andWhere('ma.authentifier= :authentifier')->setParameter('authentifier', $authentifier);
        }

        return $query ->getQuery()->execute();
    }
}
