<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class TypeProcedureRepository extends EntityRepository
{
    public function setEm($em)
    {
        $this->_em = $em;
    }
}
