<?php

namespace App\Repository;

use App\Entity\TypeDecision;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TypeDecision>
 *
 * @method TypeDecision|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeDecision|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeDecision[]    findAll()
 * @method TypeDecision[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeDecisionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeDecision::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(TypeDecision $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(TypeDecision $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
