<?php

namespace App\Repository;

use App\Entity\FormePrixPfHasRefVariation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FormePrixPfHasRefVariation>
 *
 * @method FormePrixPfHasRefVariation|null find($id, $lockMode = null, $lockVersion = null)
 * @method FormePrixPfHasRefVariation|null findOneBy(array $criteria, array $orderBy = null)
 * @method FormePrixPfHasRefVariation[]    findAll()
 * @method FormePrixPfHasRefVariation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormePrixPfHasRefVariationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FormePrixPfHasRefVariation::class);
    }
}
