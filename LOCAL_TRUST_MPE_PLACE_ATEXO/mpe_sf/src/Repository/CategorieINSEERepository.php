<?php

namespace App\Repository;

use App\Entity\CategorieINSEE;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CategorieINSEE>
 *
 * @method CategorieINSEE|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategorieINSEE|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategorieINSEE[]    findAll()
 * @method CategorieINSEE[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategorieINSEERepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategorieINSEE::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(CategorieINSEE $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(CategorieINSEE $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function setEm($em)
    {
        $this->_em = $em;
    }
}
