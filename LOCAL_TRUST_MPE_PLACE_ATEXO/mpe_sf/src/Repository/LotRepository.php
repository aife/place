<?php

namespace App\Repository;

use App\Entity\Lot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Lot>
 *
 * @method Lot|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lot|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lot[]    findAll()
 * @method Lot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LotRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lot::class);
    }

    /**
     * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     */
    public function findLots(int $consultationId, string $organisme, array $lots): array
    {
        $qb = $this->createQueryBuilder('l')
            ->where('l.consultation = :consultationId')->setParameter('consultationId', $consultationId)
            ->andWhere('l.organisme = :organisme')->setParameter('organisme', $organisme)
            ->andWhere('l.lot in (:lots)')->setParameter('lots', $lots);

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    public function getLotsFormatJson(int $consultationId, string $organisme): array
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery(
            'SELECT l.lot, l.description
              FROM App:Lot l
              WHERE l.consultation = :consultation
              AND l.organisme = :organisme'
        )
            ->setParameter('consultation', $consultationId)
            ->setParameter('organisme', $organisme);

        $listLot = $query->getResult();

        return $listLot;
    }

    /**
     * @param string $lots
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    public function getIntituleAndNumLot(int $consultationId, string $organisme, $lots): array
    {
        $data = [];

        $numLots = explode(',', $lots);

        $em = $this->getEntityManager();

        for ($i = 0; $i < count($numLots); ++$i) {
            $query = $em->createQuery(
                'SELECT l.description
                FROM App:Lot l
                WHERE l.consultation = :consultation
                AND l.organisme = :organisme
                AND l.lot = :lot'
            )
                ->setParameter('consultation', $consultationId)
                ->setParameter('organisme', $organisme)
                ->setParameter('lot', $numLots[$i]);

            $info = $query->getSingleScalarResult();

            $data[] = [
                'num_lot' => $numLots[$i],
                'intitule_lot' => $info,
            ];
        }

        return $data;
    }

    /**
     * @return array
     */
    public function findLotsConsultation(int $consultationId, string $organisme)
    {
        $qb = $this->createQueryBuilder('l')
            ->where('l.consultation = :consultationId')->setParameter('consultationId', $consultationId)
            ->andWhere('l.organisme = :organisme')->setParameter('organisme', $organisme);

        return $qb->getQuery()->getResult();
    }

    public function persistLot(Lot $lot): void
    {
        $this->getEntityManager()->persist($lot);
        $this->getEntityManager()->flush();
    }
}
