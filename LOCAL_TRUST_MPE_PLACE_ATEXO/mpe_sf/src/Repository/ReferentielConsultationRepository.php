<?php

namespace App\Repository;

use App\Entity\ReferentielConsultation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ReferentielConsultation>
 *
 * @method ReferentielConsultation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReferentielConsultation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReferentielConsultation[]    findAll()
 * @method ReferentielConsultation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReferentielConsultationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReferentielConsultation::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(ReferentielConsultation $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function flush(): void
    {
        $this->_em->flush();
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(ReferentielConsultation $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
