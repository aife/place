<?php

namespace App\Repository;

use App\Entity\BloborganismeFile;
use App\Entity\Complement;
use App\Entity\Consultation;
use App\Entity\DCE;
use App\Entity\Enveloppe;
use App\Entity\EnveloppeFichier;
use App\Entity\Offre;
use App\Entity\RG;
use App\Entity\TDumeContexte;
use App\Entity\TDumeNumero;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Repository BloborganismeFileRepository.
 *
 * @author Amal EL BEKKAOUI <amal@atexo.com>
 *
 * @since     ESR2016
 *
 * @copyright Atexo 2016
 */
class BloborganismeFileRepository extends ServiceEntityRepository
{
    final public const DCE_BLOB_TYPE = 'AUTRE_PIECE';
    final public const DUME_BLOB_TYPE = 'DUME';
    final public const RC_BLOB_TYPE = 'RC';
    final public const COMPLEMENT_BLOB_TYPE = 'COMPLEMENT';


    public function __construct(ManagerRegistry $registry, private readonly ParameterBagInterface $parameterBag)
    {
        parent::__construct($registry, BloborganismeFile::class);
    }

    /**
     * recuperer blobfile par id.
     *
     * @return BloborganismeFile
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1
     *
     * @since   develop
     *
     * @copyright Atexo 2016
     *
     * @throws NonUniqueResultException
     */
    public function getOrganismeBlobFichier(int $idBlob): ?BloborganismeFile
    {
        return $this->createQueryBuilder('blob')
            ->select('blob')
            ->where('blob.id = :idBlob')
            ->setParameter('idBlob', $idBlob)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return array
     */
    public function listCheck()
    {
        return $this->createQueryBuilder('b')
            ->select('b.id, b.organisme, b.hash')
            ->where('b.statutSynchro in (0,2)')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $dateDebut
     * @param $dateFin
     * @param string $extension
     */
    public function getListBlob($dateDebut, $dateFin, $extension = '.attente_chiffrement'): array|int|string
    {
        return $this->createQueryBuilder('b')
            ->select('b.id, b.organisme, b.chemin, b.extension')
            ->leftJoin(EnveloppeFichier::class, 'fe', 'WITH', 'b.id = fe.idBlob and b.organisme = fe.organisme')
            ->leftJoin(Enveloppe::class, 'e', 'WITH', 'e.idEnveloppeElectro = fe.enveloppe')
            ->leftJoin(Offre::class, 'o', 'WITH', 'o.id = e.offre')
            ->leftJoin(Consultation::class, 'c', 'WITH', 'c.reference = o.consultation and o.organisme = c.organisme')
            ->where('c.chiffrementOffre = 0')
            ->andWhere('o.createdAt between :dateDebut and :dateFin')
            ->andWhere('b.extension = :extension')
            ->setParameter('dateDebut', $dateDebut)
            ->setParameter('dateFin', $dateFin)
            ->setParameter('extension', $extension)
            ->getQuery()->getResult();
    }

    /**
     * @param $dateDebut
     * @param $dateFin
     * @param string $extension
     * @param string $typeFichier
     */
    public function getListBlobJetonOffreChiffre(
        $dateDebut,
        $dateFin,
        string $extension = '.attente_chiffrement',
        string $typeFichier = 'SIG'
    ): array|int|string {
        return $this->createQueryBuilder('b')
            ->select('b.id, b.organisme, b.chemin, b.extension')
            ->leftJoin(EnveloppeFichier::class, 'fe', 'WITH', 'b.id = fe.idBlob and b.organisme = fe.organisme')
            ->leftJoin(Enveloppe::class, 'e', 'WITH', 'e.idEnveloppeElectro = fe.enveloppe')
            ->leftJoin(Offre::class, 'o', 'WITH', 'o.id = e.offre')
            ->leftJoin(Consultation::class, 'c', 'WITH', 'c.reference = o.consultation and o.organisme = c.organisme')
            ->andWhere('o.createdAt between :dateDebut and :dateFin')
            ->andWhere('b.extension = :extension')
            ->andWhere('fe.typeFichier = :type_fichier')
            ->setParameter('dateDebut', $dateDebut)
            ->setParameter('dateFin', $dateFin)
            ->setParameter('extension', $extension)
            ->setParameter('type_fichier', $typeFichier)
            ->getQuery()->getResult();
    }

    public function getRcByConsultation(int $consultationId): array
    {
        return $this->createQueryBuilder('b')
            ->select(
                'b as blob',
                'rg.untrusteddate as dateModification',
                'rg.nomFichier as nomFichier',
                '\'' . self::RC_BLOB_TYPE . '\' as type',
                'rg.id as technicalId'
            )
            ->innerJoin(RG::class, 'rg', Join::WITH, 'rg.rg = b.id')
            ->andWhere('rg.consultationId = :consultationId')
            ->setParameter('consultationId', $consultationId)
            ->orderBy('rg.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()->getResult();
    }

    public function getDceByConsultation(int $consultationId): array
    {
        return $this->createQueryBuilder('b')
            ->select(
                'b as blob',
                'dce.untrusteddate as dateModification',
                'dce.nomFichier as nomFichier',
                'dce.tailleDce as taille',
                '\'' . self::DCE_BLOB_TYPE . '\' as type',
                'dce.id as technicalId'
            )
            ->innerJoin(DCE::class, 'dce', Join::WITH, 'dce.dce = b.id')
            ->andWhere('dce.consultationId = :consultationId')
            ->setParameter('consultationId', $consultationId)
            ->orderBy('dce.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()->getResult();
    }

    public function getDumeByConsultation(int $consultationId): array
    {
        return $this->createQueryBuilder('b')
            ->select('b as blob', 'dc.dateModification', 'dc.dateCreation', '\'' . self::DUME_BLOB_TYPE . '\' as type', 'dc.id as technicalId')
            ->innerJoin(TDumeNumero::class, 'dn', Join::WITH, 'dn.blobId = b.id OR dn.blobIdXml = b.id')
            ->innerJoin(TDumeContexte::class, 'dc', Join::WITH, 'dn.idDumeContexte = dc.id')
            ->andWhere('dc.consultation = :consultationId')
            ->setParameter('consultationId', $consultationId)
            ->getQuery()->getResult();
    }

    public function getAutrePieceByConsultation(int $consultationId): array
    {
        return $this->createQueryBuilder('b')
            ->select(
                'b as blob',
                'comp.untrusteddate as dateModification',
                'comp.nomFichier as nomFichier',
                '\'' . self::COMPLEMENT_BLOB_TYPE . '\' as type'
            )
            ->innerJoin(Complement::class, 'comp', Join::WITH, 'comp.complement = b.id')
            ->andWhere('comp.consultationId = :consultationId')
            ->andWhere('comp.statut = :validStatus')
            ->setParameter('consultationId', $consultationId)
            ->setParameter('validStatus', $this->parameterBag->get('AJOUT_FILE'))
            ->getQuery()->getResult();
    }
}
