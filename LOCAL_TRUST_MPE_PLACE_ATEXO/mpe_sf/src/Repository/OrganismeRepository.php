<?php

namespace App\Repository;

use App\Model\OrganismeModel;
use Exception;
use Doctrine\DBAL\Exception as DBALException;
use App\Entity\ConfigurationOrganisme;
use App\Entity\Organisme;
use App\Entity\OrganismeServiceMetier;
use Doctrine\ORM\EntityRepository;

/**
 * Class OrganismeRepository.
 */
class OrganismeRepository extends EntityRepository
{
    public string $headWs = 'organismes';

    public function findAllActiveAcronyme()
    {
        $result = $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('o.acronyme')
            ->from($this->getEntityName(), 'o')
            ->where('o.active = 1')
            ->getQuery()
            ->getResult();

        if (empty($result)) {
            return [];
        }

        return array_column(
            $result,
            'acronyme'
        );
    }

    public function findAllByParamWs(
        $nombreElementsParPage,
        $NumeroPage,
        $dateModification,
        $module,
        $orgServicesAllowed = [],
        $tabParams = [],
        $searchId = false
    ) {
        $queryBuilder = $this->createQueryBuilder('o')
            ->select('o')
            ->distinct();

        if ('EXEC' === $module) {
            $queryBuilder->join(ConfigurationOrganisme::class, 'c', 'WITH', 'o.acronyme = c.organisme ');
            $queryBuilder->andWhere(" c.moduleExec = '1' ");
        }

        $limit = $nombreElementsParPage;
        $offset = $this->getOffset($NumeroPage, $nombreElementsParPage);
        $queryBuilder->setFirstResult($offset);
        $queryBuilder->setMaxResults($limit);
        $organismes = $queryBuilder->getQuery()->getResult();
        foreach ($organismes as $organisme) {
            yield $organisme;
            $this->_em->detach($organisme);
        }
    }

    public function getOffset($page, $limit)
    {
        $offset = 0;
        if (0 != $page && 1 != $page) {
            $offset = ($page - 1) * $limit;
        }

        return $offset;
    }

    public function findByAcronymeOrganisme(string $acronymeOrganisme)
    {
        return $this->_em->getRepository(Organisme::class)->findOneBy(['acronyme' => $acronymeOrganisme]);
    }

    public function syncTable($acronymeReference, $acronyme, $serviceIdToUse, $pathToInitScript)
    {
        $organisme = $this->findOneByAcronyme($acronymeReference);
        $organismeReferenceExist = ($organisme instanceof Organisme);
        $this->initOrg($acronyme, $pathToInitScript);
        if ($organismeReferenceExist) {
            $this->syncOrg($acronyme, $acronymeReference);
        }

        $this->syncService($acronyme, $acronymeReference, $serviceIdToUse, $organismeReferenceExist);

        return true;
    }

    public function syncService($acronyme, $acronymeReference, $serviceIdToUse, $organismeReferenceExist = false)
    {
        if ($organismeReferenceExist && $acronyme != $acronymeReference) {
            $organismeServiceMetier = $this->getEntityManager()
                ->getRepository(OrganismeServiceMetier::class)
                ->findByOrganisme($acronymeReference);
            foreach ($organismeServiceMetier as $row) {
                $organismeServiceMetierCible = new OrganismeServiceMetier();
                $organismeServiceMetierCible->setOrganisme($acronyme);
                $organismeServiceMetierCible->setIdServiceMetier($row->getIdServiceMetier());

                $this->_em->persist($organismeServiceMetierCible);
                $this->_em->flush();
            }
        } else {
            $organismeServiceMetierCible = new OrganismeServiceMetier();
            $organismeServiceMetierCible->setOrganisme($acronyme);
            $organismeServiceMetierCible->setIdServiceMetier($serviceIdToUse);
            $this->_em->persist($organismeServiceMetierCible);
            $this->_em->flush();
        }
    }

    public function syncOrg($acronyme, $acronymeReference)
    {
        $this->clean($acronyme);
        $this->syncOrgGenerique(
            $acronyme,
            $acronymeReference,
            'Type_Procedure_Organisme',
            'organisme'
        );
        $this->syncOrgGenerique(
            $acronyme,
            $acronymeReference,
            'Tranche_Article_133',
            'acronyme_org',
            'id'
        );
        $this->syncOrgGenerique(
            $acronyme,
            $acronymeReference,
            't_calendrier_etape_referentiel',
            'ORGANISME'
        );
        $this->syncOrgGenerique(
            $acronyme,
            $acronymeReference,
            't_calendrier_transition_referentiel',
            'ORGANISME'
        );
        $this->syncOrgGenerique(
            $acronyme,
            $acronymeReference,
            't_type_contrat_et_procedure',
            'organisme',
            'id_type_contrat_et_procedure'
        );
        $this->syncOrgGenerique(
            $acronyme,
            $acronymeReference,
            'ProcedureEquivalence',
            'organisme'
        );
    }

    public function syncOrgGenerique($acronyme, $acronymeReference, $table, $champsAcronyme, $champsToBeNullable = null)
    {
        $sqlSelect = "SELECT * FROM $table WHERE $champsAcronyme = :acronyme";

        $stmt = $this->_em->getConnection()->prepare($sqlSelect);

        $stmt->bindParam(':acronyme', $acronymeReference);
        $stmt->execute();
        while ($row = $stmt->fetch()) {
            $row[$champsAcronyme] = $acronyme;
            if ($champsToBeNullable) {
                $row[$champsToBeNullable] = null;
            }
            $champs = '';
            $valeurs = '';
            $i = 0;
            foreach ($row as $key => $value) {
                if ($i > 0) {
                    $champs .= ',';
                    $valeurs .= ',';
                }
                $champs .= '`' . $key . '`';
                if (null === $value) {
                    $valeurs .= 'null';
                } else {
                    $valeurs .= "'" . addslashes($value) . "'";
                }
                ++$i;
            }
            $sqlInsert = "insert into $table (" . $champs . ') VALUES (' . $valeurs . ')';
            $localStmt = $this->_em->getConnection()->prepare($sqlInsert);
            $localStmt->execute();
        }
    }

    public function initOrg($acronyme, $pathToScript)
    {
        preg_match_all(
            "/[\n]{1}((INSERT|UPDATE).*;)[ ]?[\n]{0,1}/Uxs",
            (string) file_get_contents($pathToScript),
            $sql_ORGA
        );

        foreach ($sql_ORGA[1] as $query) {
            try {
                $query = str_replace('##ACCRONYME_NEW_ORG##', $acronyme, $query);
                $localStmt = $this->_em->getConnection()->prepare($query);
                $localStmt->execute();
            } catch (Exception $e) {
                throw $e;
            }
        }
    }

    /**
     * clean des tables avant de les mettres à jour
     * car on fait un import init puis dans certains cas un sync avec l'organisme reference.
     *
     * @param $acronyme
     *
     * @throws DBALException
     */
    public function clean($acronyme)
    {
        $sqlDelete = 'SET FOREIGN_KEY_CHECKS=0;';
        $deleteStmt = $this->_em->getConnection()->prepare($sqlDelete);
        $deleteStmt->execute();

        $sqlDelete = "DELETE FROM Type_Procedure_Organisme WHERE organisme = '$acronyme'";
        $deleteStmt = $this->_em->getConnection()->prepare($sqlDelete);
        $deleteStmt->execute();

        $sqlDelete = "DELETE FROM ProcedureEquivalence WHERE organisme = '$acronyme'";
        $deleteStmt = $this->_em->getConnection()->prepare($sqlDelete);
        $deleteStmt->execute();

        $sqlDelete = "DELETE FROM t_calendrier_etape_referentiel WHERE ORGANISME = '$acronyme'";
        $deleteStmt = $this->_em->getConnection()->prepare($sqlDelete);
        $deleteStmt->execute();

        $sqlDelete = "DELETE FROM t_calendrier_transition_referentiel WHERE ORGANISME = '$acronyme'";
        $deleteStmt = $this->_em->getConnection()->prepare($sqlDelete);
        $deleteStmt->execute();

        $sqlDelete = "DELETE FROM Tranche_Article_133 WHERE acronyme_org = '$acronyme'";
        $deleteStmt = $this->_em->getConnection()->prepare($sqlDelete);
        $deleteStmt->execute();

        $sqlDelete = "DELETE FROM t_type_contrat_et_procedure WHERE organisme = '$acronyme'";
        $deleteStmt = $this->_em->getConnection()->prepare($sqlDelete);
        $deleteStmt->execute();

        $sqlDelete = 'SET FOREIGN_KEY_CHECKS=1;';
        $deleteStmt = $this->_em->getConnection()->prepare($sqlDelete);
        $deleteStmt->execute();
    }

    public function getNumberOfCreatedOrganisme()
    {
        return $this->createQueryBuilder('o')
            ->select('COUNT(o)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getOrganismsForSearchPage(array $ids = null)
    {
        $qb = $this->createQueryBuilder('o')
            ->select(
                sprintf(
                    'NEW %s(
                           o.id, 
                           o.denominationOrg, 
                           o.acronyme, 
                           o.sigle, 
                           sm.sigle
                )',
                    OrganismeModel::class
                )
            );

        if (!is_null($ids)) {
            $qb->where('o.id IN (:ids)');
        }

        $qb->join(OrganismeServiceMetier::class, 'osm', 'WITH', 'osm.organisme = o.acronyme')
            ->join(Organisme\ServiceMetier::class, 'sm', 'WITH', 'sm.id = osm.idServiceMetier');

        if (!is_null($ids)) {
            $qb->setParameter('ids', $ids);
        }

        return $qb->addOrderBy('o.denominationOrg')
            ->getQuery()
            ->getResult();
    }

    public function findAllByIdentifierNotIn($notIn, $limit)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('o')
            ->from($this->getEntityName(), 'o')
            ->setMaxResults($limit)
        ;
        $qb->where('o.active = 1');

        if (!empty($notIn)) {
            $qb->andWhere(
                $qb->expr()->notIn('o.acronyme', $notIn)
            );
        }

        return $qb
            ->getQuery()
            ->getResult();
    }


    public function countByIdentifierNotIn($notIn)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('COUNT(o.acronyme)')
            ->from($this->getEntityName(), 'o')
        ;
        $qb->where('o.active = 1');

        if (!empty($notIn)) {
            $qb->andWhere(
                $qb->expr()->notIn('o.acronyme', $notIn)
            );
        }

        return $qb
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function setEm($em)
    {
        $this->_em = $em;
    }
}
