<?php

namespace App\Repository;

use App\Entity\Agent;
use App\Entity\ComptesAgentsAssocies;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ComptesAgentsAssociesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ComptesAgentsAssocies::class);
    }

    public function findComptesAssocies(Agent $agent): array
    {
        return $this->createQueryBuilder('c')
            ->where('c.comptePrincipal = :idComptePrincipal')
            ->setParameter('idComptePrincipal', $agent->getId())
            ->orWhere('c.compteSecondaire = :idCompteSecondaire')
            ->setParameter('idCompteSecondaire', $agent->getId())
            ->getQuery()
            ->getResult()
            ;
    }

    public function getAllActiveComptesAssocies(Agent $agent): array
    {
        return $this->createQueryBuilder('c')
            ->where('c.comptePrincipal = :idComptePrincipal')
            ->setParameter('idComptePrincipal', $agent->getId())
            ->andWhere('c.statutActivationCompteSecondaire = :statutActivationCompteSecondaire')
            ->setParameter('statutActivationCompteSecondaire', '1')
            ->getQuery()
            ->getResult();
    }

    public function deleteAssociationByAgent(int $agentId): array|int|string
    {
        return $this->createQueryBuilder('caa')
            ->delete()
            ->where("caa.compteSecondaire = :idAgent")
            ->setParameter('idAgent', $agentId)
            ->getQuery()
            ->getResult()
            ;
    }
}
