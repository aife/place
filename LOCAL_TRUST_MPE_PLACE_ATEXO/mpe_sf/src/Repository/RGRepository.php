<?php

namespace App\Repository;

use App\Entity\RG;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * RGRepository.
 */
class RGRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RG::class);
    }

    /**
     * Cree le lien de dossier Reglement de la consultation.
     */
    public function getReglement($consultationId, $organisme)
    {
        $qb = $this->createQueryBuilder('rg')
            ->where('rg.consultationId = :consultationId')->setParameter('consultationId', $consultationId)
            ->andWhere('rg.organisme = :organisme')->setParameter('organisme', $organisme)
            ->andWhere('rg.rg != 0')
            ->addOrderBy('rg.id', 'desc')
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }
}
