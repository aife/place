<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Repository;

use App\Entity\PieceGenereConsultation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

class PieceGenereConsultationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PieceGenereConsultation::class);
    }

    /**
     * @return int|mixed|string|null
     *
     * @throws NonUniqueResultException
     */
    public function findPieceGenereeByIdConsultationAndFileName(string $fileName, int $idConsultation)
    {
        $qb = $this->createQueryBuilder('p');

        $qb->innerJoin('p.consultation', 'c')
            ->innerJoin('p.blobId', 'b')
            ->where('c.id = :id')
            ->andWhere('b.name = :name')
            ->setParameter('id', $idConsultation)
            ->setParameter('name', $fileName)
            ;

        return $qb->getQuery()->getOneOrNullResult();
    }
}
