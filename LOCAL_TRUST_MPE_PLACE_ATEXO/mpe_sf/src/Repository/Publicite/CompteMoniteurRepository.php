<?php

namespace App\Repository\Publicite;

use App\Entity\Publicite\CompteMoniteur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CompteMoniteurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompteMoniteur::class);
    }

    public function findAllByIdentifierNotIn($notIn, $limit)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('c')
            ->from($this->getEntityName(), 'c')
            ->setMaxResults($limit);

        if (!empty($notIn)) {
            $qb->where(
                $qb->expr()->notIn(
                    'CONCAT(c.id, \';\', c.organisme)',
                    ':notIn'
                )
            )
                ->setParameter('notIn', $notIn);
        }

        return $qb
            ->getQuery()
            ->getResult();
    }

    public function countByIdentifierNotIn(array $notIn): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('COUNT(c)')
            ->from($this->getEntityName(), 'c');

        if (!empty($notIn)) {
            $qb->where(
                $qb->expr()->notIn(
                    'CONCAT(c.id, \';\', c.organisme)',
                    ':notIn'
                )
            )
                ->setParameter('notIn', $notIn);
        }

        return $qb
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getMaxId(): int
    {
        $maxId = $this->createQueryBuilder('c')
            ->select('MAX(c.id)')
            ->getQuery()
            ->getSingleScalarResult();

        return $maxId ?? 0;
    }

    public function setEm($em)
    {
        $this->_em = $em;
    }
}
