<?php

namespace App\Repository\Publicite;

use App\Entity\Publicite\RenseignementsBoamp;
use App\Repository\ServiceRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class RenseignementsBoampRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RenseignementsBoamp::class);
    }
}
