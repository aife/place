<?php

namespace App\Repository\Publicite;

use App\Entity\Publicite\GroupeMoniteur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class GroupeMoniteurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GroupeMoniteur::class);
    }

    public function findAllByIdentifierNotIn($notIn, $limit)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('g')
            ->from($this->getEntityName(), 'g')
            ->setMaxResults($limit);

        if (!empty($notIn)) {
            $qb->where(
                $qb->expr()->notIn(
                    'CONCAT(g.id, \';\', g.organisme)',
                    ':notIn'
                )
            )
                ->setParameter('notIn', $notIn);
        }

        return $qb
            ->getQuery()
            ->getResult();
    }

    public function countByIdentifierNotIn(array $notIn): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('COUNT(g)')
            ->from($this->getEntityName(), 'g');

        if (!empty($notIn)) {
            $qb->where(
                $qb->expr()->notIn(
                    'CONCAT(g.id, \';\', g.organisme)',
                    ':notIn'
                )
            )
                ->setParameter('notIn', $notIn);
        }

        return $qb
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getMaxId(): int
    {
        $maxId = $this->createQueryBuilder('g')
            ->select('MAX(g.id)')
            ->getQuery()
            ->getSingleScalarResult();

        return $maxId ?? 0;
    }

    public function setEm($em)
    {
        $this->_em = $em;
    }
}
