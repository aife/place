<?php

namespace App\Repository\Configuration;

use App\Entity\Configuration\PlateformeVirtuelleOrganisme;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Class PlateformeVirtuelleOrganismeRepository.
 */
class PlateformeVirtuelleOrganismeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlateformeVirtuelleOrganisme::class);
    }

    /**
     * @return array
     */
    public function getListAcronymeOrgEligible(string $domaine)
    {
        $qb = $this->createQueryBuilder('pfo')
            ->join('pfo.organisme', 'o')
            ->join('pfo.plateforme', 'pf')
            ->select('o.acronyme')
            ->where('pf.domain like :domain')
            ->setParameter('domain', '%'.$domaine.'%');
        $rs = $qb->getQuery()->getArrayResult();
        $fct = fn($element) => $element['acronyme'];

        return array_map($fct, $rs);
    }

    public function getListAcronymeOrgEligibleById(int $id): array
    {
        $qb = $this->createQueryBuilder('pfo')
            ->join('pfo.organisme', 'o')
            ->join('pfo.plateforme', 'pf')
            ->select('o.acronyme')
            ->where('pf.id = :id')
            ->setParameter('id', $id);
        $rs = $qb->getQuery()->getArrayResult();
        $fct = fn($element) => $element['acronyme'];

        return array_map($fct, $rs);
    }
}
