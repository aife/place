<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Repository\Configuration;

use App\Entity\Configuration\PlateformeVirtuelle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class PlateformeVirtuelleRepository.
 *
 * @author Sébastien Lepers <sebastien.lepers@atexo.com>
 */
class PlateformeVirtuelleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlateformeVirtuelle::class);
    }
}
