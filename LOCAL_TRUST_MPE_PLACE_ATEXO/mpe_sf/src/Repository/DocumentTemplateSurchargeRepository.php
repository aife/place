<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use App\Entity\DocumentTemplateSurcharge;
use Doctrine\Persistence\ManagerRegistry;

class DocumentTemplateSurchargeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DocumentTemplateSurcharge::class);
    }

    /**
     * @param string|null $organismeAcronyme
     *
     * @throws NonUniqueResultException
     */
    public function getOverrideTemplate(
        string $documentName,
        int $priority,
        string $organismeAcronyme = null
    ): ?DocumentTemplateSurcharge {
        $qb = $this->createQueryBuilder('dts')
            ->leftJoin('dts.documentTemplate', 'dt')
            ->andWhere('dt.nom = :documentName')
            ->andWhere('dts.priority = :priority')
            ->setParameters(
                [
                    'documentName' => $documentName,
                    'priority' => $priority,
                ]
            )
            ->orderBy('dts.priority', 'DESC')
            ->setMaxResults(1)
        ;

        if ($organismeAcronyme) {
            $qb
                ->leftJoin('dts.organisme', 'o')
                ->andWhere('o.acronyme = :acronyme')
                ->setParameter('acronyme', $organismeAcronyme)
            ;
        } else {
            $qb->andWhere('dts.organisme IS NULL');
        }

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @return array|mixed
     */
    public function findSurchargeTemplate(string $organismeAcronyme): array
    {
        $qb = $this->createQueryBuilder('dts')
            ->where('dts.organisme = :organisme')
            ->orWhere('dts.priority = :priority')
            ->setParameters(
                [
                    'organisme' => $organismeAcronyme,
                    'priority' => DocumentTemplateSurcharge::PRIORITY_CLIENT
                ]
            )
            ->orderBy('dts.priority', 'DESC')
        ;

        return $qb->getQuery()->getResult();
    }
}
