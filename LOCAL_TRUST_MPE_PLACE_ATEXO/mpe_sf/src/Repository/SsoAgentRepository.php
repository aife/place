<?php

namespace App\Repository;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\DBAL\Exception as DBALException;
use Doctrine\ORM\EntityRepository;
use Exception;

/**
 * Class SsoAgentRepository.
 */
class SsoAgentRepository extends EntityRepository
{
    /**
     * @param $token
     *
     * @return bool|mixed
     *
     * @throws NonUniqueResultException
     */
    public function checkToken($token, $idServiceMetierConsommateur)
    {
        $qb = $this->createQueryBuilder('s');

        $qb->where('s.idSso = :idSso')
            ->setParameter('idSso', $token)
            ->andWhere('s.dateLastRequest >= :dateLastRequest')
            ->setParameter('dateLastRequest', date('Y-m-d H:i:s'))
            ->andWhere('s.serviceId = :serviceId')
            ->setParameter('serviceId', $idServiceMetierConsommateur);

        $result = $qb->getQuery()->getOneOrNullResult();

        return empty($result) ? false : $result;
    }

    /**
     * @param string $ttl
     * @return bool
     * @throws DBALException
     */
    public function purge(string $ttl)
    {
        try {
            $em = $this->getEntityManager();
            $em->beginTransaction();
            $sql = <<<QUERY
        DELETE FROM sso_agent
        where date_last_request < NOW() - INTERVAL $ttl
QUERY;
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $em->commit();

            return true;
        } catch (Exception $e) {
            $em->rollback();
            throw $e;
        }
    }

    /**
     * @param $idAgent
     * @param $organisme
     * @param $idServiceMetierConsommateur
     *
     * @return int|mixed|string|null
     *
     * @throws NonUniqueResultException
     */
    public function getSsoAgentValid($idAgent, $organisme, $idServiceMetierConsommateur)
    {
        return $this->createQueryBuilder('s')
            ->where('s.idAgent = :idAgent')
            ->andWhere('s.dateLastRequest >= :dateLastRequest')
            ->andWhere('s.serviceId = :serviceId')
            ->andWhere('s.organisme = :organisme')
            ->setParameters([
                'dateLastRequest' => date('Y-m-d H:i:s'),
                'idAgent' => $idAgent,
                'organisme' => $organisme,
                'serviceId' => $idServiceMetierConsommateur,
            ])
            ->getQuery()->getOneOrNullResult();
    }
}
