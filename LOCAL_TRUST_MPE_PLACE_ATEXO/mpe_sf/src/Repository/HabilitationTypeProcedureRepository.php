<?php

namespace App\Repository;

use App\Entity\Agent;
use App\Entity\HabilitationTypeProcedure;
use App\Enum\Habilitation\HabilitationSlug;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<HabilitationTypeProcedure>
 *
 * @method HabilitationTypeProcedure|null find($id, $lockMode = null, $lockVersion = null)
 * @method HabilitationTypeProcedure|null findOneBy(array $criteria, array $orderBy = null)
 * @method HabilitationTypeProcedure[]    findAll()
 * @method HabilitationTypeProcedure[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HabilitationTypeProcedureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HabilitationTypeProcedure::class);
    }

    public function findByAgentAndSlug(
        Agent $agent,
        string $slug,
        ?int $typeProcedure = null
    ) {
        $qb = $this->createQueryBuilder('htp')
            ->where('htp.agent = :agent')
            ->leftJoin('htp.habilitation', 'h')
            ->andWhere('h.slug = :slug')
            ->setParameter('agent', $agent)
            ->setParameter('slug', $slug);

        if (!empty($typeProcedure)) {
            $qb->andWhere('htp.typeProcedure = :typeProcedure')
                ->andWhere('htp.organisme = :organisme')
                ->setParameter('typeProcedure', $typeProcedure)
                ->setParameter('organisme', $agent->getOrganisme());
        }

        return $qb->getQuery()->getResult();
    }
}
