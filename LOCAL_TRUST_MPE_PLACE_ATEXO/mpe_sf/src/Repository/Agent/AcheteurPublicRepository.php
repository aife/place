<?php

namespace App\Repository\Agent;

use App\Entity\Agent\AcheteurPublic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AcheteurPublic>
 */
class AcheteurPublicRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AcheteurPublic::class);
    }

    public function getAllAcheteurPublics(int $serviceId, string $organisme)
    {
        $criterias = ['organisme' => $organisme, 'serviceId' => $serviceId];

        return $this->findBy($criterias);
    }
}
