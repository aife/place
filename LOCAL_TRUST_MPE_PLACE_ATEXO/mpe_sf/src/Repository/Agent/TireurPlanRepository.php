<?php

namespace App\Repository\Agent;

use App\Entity\Agent\TireurPlan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TireurPlan>
 */
class TireurPlanRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TireurPlan::class);
    }

    public function getAllTireurPlans(int $serviceId, string $organisme)
    {
        $criterias = ['organisme' => $organisme, 'serviceId' => $serviceId];

        return $this->findBy($criterias);
    }
}
