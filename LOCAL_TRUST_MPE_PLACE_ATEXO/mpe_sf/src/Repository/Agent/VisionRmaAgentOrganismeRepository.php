<?php

namespace App\Repository\Agent;

use App\Entity\Agent\VisionRmaAgentOrganisme;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<VisionRmaAgentOrganisme>
 */
class VisionRmaAgentOrganismeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VisionRmaAgentOrganisme::class);
    }
}
