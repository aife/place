<?php

namespace App\Repository\Agent;

use App\Entity\Agent\RPA;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RPA>
 */
class RPARepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RPA::class);
    }

    public function getAllRPAs(int $serviceId, string $organisme)
    {
        $criterias = ['acronymeOrg' => $organisme, 'serviceId' => $serviceId];

        return $this->findBy($criterias);
    }
}
