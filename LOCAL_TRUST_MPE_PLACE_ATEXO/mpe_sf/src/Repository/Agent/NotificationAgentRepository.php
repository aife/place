<?php

namespace App\Repository\Agent;

use App\Entity\Agent;
use App\Entity\Agent\NotificationAgent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<NotificationAgent>
 */
class NotificationAgentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NotificationAgent::class);
    }

    /**
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function countUnread(Agent $agent): int
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('count(n.idNotification)')
            ->from(NotificationAgent::class, 'n')
            ->where('n.idAgent = :idAgent')
            ->andWhere('n.notificationActif = :actif')
            ->andWhere('n.notificationLue = :unread')
            ->setParameters([
                'idAgent' => $agent->getId(),
                'actif' => NotificationAgent::NOTIFICATION_ACTIVE,
                'unread' => NotificationAgent::NOTIFICATION_UNREAD,
            ]);

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function listUnread(Agent $agent): array
    {
        $qb = $this->createQueryBuilder('n');
        $qb->where('n.idAgent = :idAgent')
            ->andWhere('n.notificationActif = :actif')
            ->andWhere('n.notificationLue = :unread')
            ->orderBy('n.notificationLue', 'asc')
            ->addOrderBy('n.dateCreation', 'desc')
            ->setParameters([
                'idAgent' => $agent->getId(),
                'actif' => NotificationAgent::NOTIFICATION_ACTIVE,
                'unread' => NotificationAgent::NOTIFICATION_UNREAD,
            ]);

        return $qb->getQuery()->getResult();
    }
}
