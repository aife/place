<?php

namespace App\Repository\Agent;

use App\Entity\Agent\AgentServiceMetier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AgentServiceMetier>
 */
class AgentServiceMetierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AgentServiceMetier::class);
    }

    public function setEm($em)
    {
        $this->_em = $em;
    }
}
