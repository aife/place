<?php

namespace App\Repository;

use App\Entity\InterfaceSuivi;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class InterfaceSuiviRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InterfaceSuivi::class);
    }

    public function findLastIdDestinationByConsultation(int $idConsultation): ?string
    {
        $result = $this->createQueryBuilder('i')
           ->select('i.idObjetDestination')
           ->where('i.consultation = :consultation')
            ->andWhere('i.statut != :status')
           ->setParameter('consultation', $idConsultation)
           ->setParameter('status', InterfaceSuivi::STATUS_ERREUR)
           ->orderBy('i.dateCreation', 'desc')
            ->setMaxResults(1)
           ->getQuery()
           ->getOneOrNullResult();

        if (is_array($result)) {
            return $result['idObjetDestination'];
        }

        return $result;
    }
}
