<?php

namespace App\Repository;

use App\Entity\Administrateur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * @extends ServiceEntityRepository<Administrateur>
 */
class AdministrateurRepository extends ServiceEntityRepository implements
    UserProviderInterface,
    PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Administrateur::class);
    }

    public function loadUserByIdentifier($username): ?Administrateur
    {
        return $this->findOneBy(
            [
                'login' => $username,
                'adminGeneral' => '0',
            ]
        );
    }

    public function loadSuperAdminByUsername($username): ?Administrateur
    {
        return $this->findOneBy(
            [
                'login' => $username,
                'adminGeneral' => '1',
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function refreshUser(UserInterface $user)
    {
        if (method_exists($user, 'getAdminGeneral') && $user->getAdminGeneral() === '1') {
            return $this->loadSuperAdminByUsername($user->getUsername());
        }

        return $this->loadUserByIdentifier($user->getUsername());
    }

    /**
     * @inheritDoc
     */
    public function supportsClass($class): bool
    {
        return Administrateur::class === $class;
    }

    /**
     * Retourne un administrateur correspondant à un nouvel organisme si l'organisme est disponible.
     *
     * @param $acronyme
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function getAvailableOrganisme($acronyme)
    {
        return $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.nom = :acronyme and a.organisme IS NULL')
            ->setParameter('acronyme', $acronyme)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Retour un organisme du pool au hasard.
     *
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function getRandomAvailableOrganisme()
    {
        return $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.organisme IS NULL')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @inheritDoc
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        // set the new encoded password on the User object
        $user->setMdp($newEncodedPassword);

        // execute the queries on the database
        $this->getEntityManager()->flush();
    }

    /**
     * @return Administrateur[]
     */
    public function getAllPasswordsNotArgon2Hashed(): array
    {
        return $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.mdp NOT LIKE \'%$argon2id$v=%\' ')
            ->getQuery()
            ->getResult();
    }

    public function persist(Administrateur $administrateur): void
    {
        $this->getEntityManager()->persist($administrateur);
    }

    public function flush(): void
    {
        $this->getEntityManager()->flush();
    }

    public function loadUserByUsername($username)
    {
        return $this->loadUserByIdentifier($username);
    }
}
