<?php

namespace App\Repository;

use App\Entity\HabilitationAgent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ManagerRegistry;

class HabilitationAgentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HabilitationAgent::class);
    }

    public function retrievePermanentGuests($acrOrg, $service, $params = [])
    {
        $query = $this->createQueryBuilder('ha')
            ->leftJoin('ha.agent', 'a', 'WITH', 'ha.agent = a.id')
            ->where('a.organisme = :organisme')
            ->setParameter('organisme', $acrOrg);

        if (array_key_exists('invitePermanentMonEntite', $params)) {
            if (null === $service) {
                $query->andWhere('a.serviceId is null')
                    ->andWhere('ha.invitePermanentMonEntite = :invitePermanentMonEntite')
                    ->setParameter('invitePermanentMonEntite', $params['invitePermanentMonEntite']);
            } else {
                $query->andWhere('a.serviceId = :service')
                    ->setParameter('service', $service)
                    ->andWhere('ha.invitePermanentMonEntite = :invitePermanentMonEntite')
                    ->setParameter('invitePermanentMonEntite', $params['invitePermanentMonEntite']);
            }
        }

        if (array_key_exists('invitePermanentEntiteDependante', $params)) {
            if (null === $service) {
                $query->andWhere('a.serviceId  is null')
                    ->andWhere('ha.invitePermanentEntiteDependante = :invitePermanentEntiteDependante')
                    ->setParameter('invitePermanentEntiteDependante', $params['invitePermanentEntiteDependante']);
            } else {
                $query->andWhere('a.serviceId = :service')
                    ->setParameter('service', $service)
                    ->andWhere('ha.invitePermanentEntiteDependante = :invitePermanentEntiteDependante')
                    ->setParameter('invitePermanentEntiteDependante', $params['invitePermanentEntiteDependante']);
            }
        }

        if (array_key_exists('invitePermanentTransverse', $params)) {
            $query->andWhere('ha.invitePermanentTransverse = :invitePermanentTransverse')
                ->setParameter('invitePermanentTransverse', $params['invitePermanentTransverse']);
        }

        return $query->getQuery()->getResult();
    }

    public function retrievePermanentEntiteDependante($acrOrg, $service, $params = [], $pole = [])
    {
        $query = $this->createQueryBuilder('ha')
            ->leftJoin('ha.agent', 'a', 'WITH', 'ha.agent = a.id')
            ->where('a.organisme = :organisme')
            ->setParameter('organisme', $acrOrg);

        $query->andWhere('(a.serviceId in (:pole) or a.serviceId is null)')
                ->setParameter('pole', $pole, Connection::PARAM_STR_ARRAY);

        if (array_key_exists('invitePermanentEntiteDependante', $params)) {
            $query
                ->andWhere('ha.invitePermanentEntiteDependante = :invitePermanentEntiteDependante')
                ->setParameter('invitePermanentEntiteDependante', $params['invitePermanentEntiteDependante']);
        }

        return $query->getQuery()->getResult();
    }
}
