<?php

namespace App\Repository;

use App\Entity\AccessToken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AccessToken>
 */
class AccessTokenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccessToken::class);
    }

    /**
     * @param string $ttl
     * @return bool
     *
     * @throws Exception
     */
    public function purge(string $ttl)
    {
        try {
            $em = $this->getEntityManager();
            $em->beginTransaction();
            $sql = 'DELETE FROM t_access_token where created_at < NOW() - INTERVAL :ttl DAY';
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->bindValue(':ttl', $ttl);
            $stmt->executeStatement();
            $em->commit();

            return true;
        } catch (Exception $e) {
            $em->rollback();
            throw $e;
        }
    }
}
