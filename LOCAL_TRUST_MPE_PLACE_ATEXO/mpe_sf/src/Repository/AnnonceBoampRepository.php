<?php

namespace App\Repository;

use App\Entity\AnnonceBoamp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AnnonceBoamp>
 */
class AnnonceBoampRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AnnonceBoamp::class);
    }

    /**
     * @param int    $consultationId
     * @param string $organisme
     * @param array  $lots
     *
     * @author Fernando Lozano
     */
    public function findAnnonceByRefCons($consultationId, $organisme): array
    {
        $qb = $this->createQueryBuilder('a')
            ->where('a.consultationId = :consultationId')->setParameter('consultationId', $consultationId)
            ->andWhere('a.organisme = :organisme')->setParameter('organisme', $organisme);

        return $qb->getQuery()->getResult();
    }

    public function retrieveAnnonceByRefCons($consultationId, $organisme)
    {
        $qb = $this->createQueryBuilder('a')
            ->where('a.consultationId = :consultationId')
            ->setParameter('consultationId', $consultationId)
            ->andWhere('a.organisme = :organisme')
            ->setParameter('organisme', $organisme)
        ;

        return $qb->getQuery()->getResult();
    }
}
