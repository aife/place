<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * Class SsoTiersRepository.
 */
class SsoTiersRepository extends EntityRepository
{
    /**
     * @param $token
     *
     * @return bool|mixed
     */
    public function checkToken($token, $idFonctionnalite = null)
    {
        $qb = $this->createQueryBuilder('t');

        $qb->where('t.idSsoTiers = :idSsoTiers')
            ->setParameter('idSsoTiers', $token)
            ->andWhere('t.dateLastRequest >= :dateLastRequest')
            ->setParameter('dateLastRequest', date('Y-m-d H:i:s'));

        if (null !== $idFonctionnalite) {
            $qb->andWhere('t.idFonctionnalite = :idFonctionnalite')
                ->setParameter('idFonctionnalite', $idFonctionnalite);
        }

        $result = $qb->getQuery()->getOneOrNullResult();

        return empty($result) ? false : $result;
    }
}
