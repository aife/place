<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use App\Entity\ValeurReferentiel;

/**
 * ValeurReferentielRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ValeurReferentielRepository extends EntityRepository
{
    /**
     * Permet de recuperer l'ID de la valeur referentiel.
     *
     * @param $idReferentiel
     * @param $libelle
     *
     * @return int|null
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getIdValeurReferentiel($idReferentiel, $libelle)
    {
        $referentiel = $this->findOneBy(['idReferentiel' => $idReferentiel, 'libelle2' => $libelle]);
        if ($referentiel instanceof ValeurReferentiel) {
            return $referentiel->getId();
        }

        return null;
    }

    public function getReferentielsById(int $id): array
    {
        $list = [];
        $referentiels = $this->findBy(['idReferentiel' => $id]);
        foreach ($referentiels as $referentiel) {
            $list[$referentiel->getId()] = $referentiel->getLibelleValeurReferentiel();
        }

        return $list;
    }
}
