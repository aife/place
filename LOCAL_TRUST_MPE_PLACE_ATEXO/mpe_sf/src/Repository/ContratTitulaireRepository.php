<?php

namespace App\Repository;

use Doctrine\ORM\Query\Expr\Join;
use Generator;
use App\Entity\Chorus\ChorusNomsFichiers;
use App\Entity\ConfigurationOrganisme;
use App\Entity\ConsLotContrat;
use App\Entity\Consultation;
use App\Entity\ContactContrat;
use App\Entity\ContratTitulaire;
use App\Entity\ContratTitulaireFavori;
use App\Entity\DonneesAnnuellesConcession;
use App\Entity\Entreprise;
use App\Entity\ModificationContrat;
use App\Entity\Offre;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Entity\TypeContrat;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ContratTitulaireRepository.
 */
class ContratTitulaireRepository extends ServiceEntityRepository
{
    public string $headWs = 'contrats';

    public const STATUT_DONNEES_CONTRAT_A_SAISIR = '1';
    public const STATUT_NUMEROTATION_AUTONOME = '2';
    public const STATUT_NOTIFICATION_CONTRAT = '3';
    public const STATUT_NOTIFICATION_CONTRAT_EFFECTUEE = '4';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContratTitulaire::class);
    }

    /**
     * @param $essentialDataType
     * @param string         $siren
     * @param string         $complement
     * @param \DateTime|null $dateDerniereModif
     *
     * @return Generator
     */
    public function getContracts(
        DateTime $dateNotifMin = null,
        DateTime $dateNotifMax = null,
        $siren = null,
        $complement = null,
        $dateDerniereModif = null,
        $statutPublicationSn = null,
        $exclusionOrganisme = null,
        $limit = null,
        $orgServicesAllowed = []
    ) {
        $em = $this->getEntityManager();

        $dql = "SELECT c.*
                FROM t_contrat_titulaire as c
                LEFT JOIN MarchePublie as Mr 
                on ( c.organisme = Mr.organisme 
                AND ( (c.service_id = Mr.service_id) OR (c.service_id is null AND Mr.service_id is NULL)))";

        $parameters = [];
        if (!empty($siren)) {
            $dql .= ' INNER JOIN Organisme o ON c.organisme = o.acronyme
                    AND o.siren = :siren';

            $parameters['siren'] = $siren;

            if (!empty($complement)) {
                $dql .= ' AND o.complement = :complement';

                $parameters['complement'] = $complement;
            }
        }

        $dql .= "  WHERE c.publication_contrat = 0 
        AND c.contrat_class_key != 2 
 AND( (Mr.isPubliee = '1'  AND Mr.numeroMarcheAnnee = YEAR(c.date_notification) ) OR Mr.id is null) ";

        if (!is_null($dateNotifMin)) {
            $parameters['dateNotifMax'] = $dateNotifMax->format('Y-m-d');
            $parameters['dateNotifMin'] = $dateNotifMin->format('Y-m-d');

            $dql .= ' AND c.date_notification BETWEEN :dateNotifMin AND :dateNotifMax ';
        } else {
            $parameters['dateNotifMax'] = $dateNotifMax->format('Y-m-d');
            $dql .= ' AND c.date_notification <= :dateNotifMax ';
        }

        if (!is_null($statutPublicationSn)) {
            $dql .= ' AND c.id_contrat_titulaire IN (
                        SELECT id_contrat_titulaire
                        FROM t_contrat_titulaire
                        WHERE statut_publication_sn = :statutPublicationSn
                        UNION
                        SELECT m.id_contrat_titulaire
                        FROM modification_contrat m
                        LEFT JOIN t_contrat_titulaire c ON 
                            c.id_contrat_titulaire = m.id_contrat_titulaire
                        WHERE m.statut_publication_sn = :statutPublicationSn
                        AND c.statut_publication_sn = 1
                        UNION
                        SELECT id_contrat
                        FROM donnees_annuelles_concession
                        WHERE suivi_publication_sn = :statutPublicationSn
                    )';
            $parameters['statutPublicationSn'] = $statutPublicationSn;
        }

        if (!empty($dateDerniereModif)) {
            $dql .= ' AND c.id_contrat_titulaire IN (
                        SELECT id_contrat_titulaire
                        FROM t_contrat_titulaire
                        WHERE date_modification >= :dateDerniereModif
                        UNION
                        SELECT id_contrat_titulaire
                        FROM modification_contrat
                        WHERE date_modification >= :dateDerniereModif
                    )';

            $parameters['dateDerniereModif'] = $dateDerniereModif->format('Y-m-d');
        }
        if (is_array($orgServicesAllowed) && !empty($orgServicesAllowed)) {
            $condition = [];
            foreach ($orgServicesAllowed as $key => $services) {
                $param = str_replace('-', '_', $key);
                $condition[$key] = " (  c.organisme = :$param";
                $parameters[$param] = $key;
                if (!in_array(0, $services)) {
                    $condition[$key] .= " AND c.service_id in ('" . implode("','", $services) . "')";
                }
                $condition[$key] .= ')';
            }
            if (!empty($condition)) {
                $dql .= 'AND ( ' . implode(' OR ', $condition) . ')';
            }
        }
        if (!empty($limit)) {
            $dql .= ' LIMIT ' . $limit;
        }

        $rsm = new ResultSetMappingBuilder($em);
        $rsm->addRootEntityFromClassMetadata(ContratTitulaire::class, 'c');
        $query = $em->createNativeQuery($dql, $rsm)->setParameters($parameters);

        $data = [];

        foreach ($query->getResult() as $key => $contrat) {
            $qb = $this->_em->createQueryBuilder();
            $qb
                ->select('m')
                ->from(ModificationContrat::class, 'm')
                ->where('m.idContratTitulaire = :id ')
                ->andWhere('m.dateSignature <= :date_signature')
                ->andWhere('m.statutPublicationSn = :statutPublicationSn')
                ->setParameter('id', $contrat->getIdContratTitulaire())
                ->setParameter('date_signature', $dateNotifMax->format('Y-m-d H:i:s'))
                ->setParameter('statutPublicationSn', $statutPublicationSn)
            ;

            $modifications = $qb->getQuery()->getResult();

            $data[] = ['contrat' => $contrat];

            if (!empty($modifications)) {
                $data[$key]['modifications'] = $modifications;
            }
        }

        foreach ($query->getResult() as $key => $contrat) {
            $qb = $this->createQueryBuilder('c');

            $qb
                ->select('m')
                ->from(DonneesAnnuellesConcession::class, 'm')
                ->where('m.idContrat = :id')
                ->andwhere('m.suiviPublicationSn = :statutPublicationSn')
                ->setParameter('id', $contrat->getIdContratTitulaire())
                ->setParameter('statutPublicationSn', $statutPublicationSn);

            $modifications = $qb->getQuery()->getResult();

            if (!empty($modifications)) {
                $data[$key]['ajoutDonneeAnnuelles'] = $modifications;
            }
        }

        foreach ($data as $contrat) {
            yield $contrat;
        }
    }

    /**
     * @param $idEntitePublique
     * @param $arrayIdsServices
     * @param $annee
     * @param $nomAttributaire
     * @param $idCategorie
     * @param $montantMin
     * @param $montantMax
     * @param $lieuxExecution
     * @param $codesCpv
     * @param $dateNotifMin
     * @param $dateNotifMax
     * @param $motsCles
     * @param $encoding
     * @param $concession
     *
     * @return array
     */
    public function getMarchesByCriteria(
        $idEntitePublique,
        $arrayIdsServices,
        $annee,
        $nomAttributaire,
        $idCategorie,
        $montantMin,
        $montantMax,
        $lieuxExecution,
        $codesCpv,
        $dateNotifMin,
        $dateNotifMax,
        $motsCles,
        $encoding,
        $concession = 0
    ) {
        $filteredValues = [];
        $em = $this->getEntityManager();

        $sql = 'SELECT c.*
                FROM t_contrat_titulaire as c
                LEFT JOIN Entreprise e ON c.id_titulaire = e.id
                LEFT JOIN t_contrat_titulaire tc ON c.id_contrat_multi=tc.id_contrat_titulaire
                WHERE c.organisme = :organisme
                AND c.date_notification LIKE :annee
                AND c.contrat_class_key = :one
                AND c.publication_contrat = :zero
                AND c.date_notification IS NOT NULL';



        $tabIdConcession = [];
        $typeContrats = $this->createQueryBuilder('c')
            ->select('ttc')
            ->from(TypeContrat::class, 'ttc')
            ->where('ttc.concession = true')
            ->getQuery()->getResult();

        foreach ($typeContrats as $typeContrat) {
            $tabIdConcession[] = $typeContrat->getIdTypeContrat();
        }

        $in = 'not in ';
        if (1 == $concession) {
            $in = 'in ';
        }

        $parameters = [
            'organisme' => $idEntitePublique,
            'annee' => $annee . '%',
            'one' => 1,
            'zero' => 0,
            'arrayIdTypeContrat' => 0,
        ];


        $sql .= ' AND c.id_type_contrat ' . $in . ' (:arrayIdTypeContrat)';
        $parameters['arrayIdTypeContrat'] = implode($tabIdConcession);

        if ('false' != $nomAttributaire) {
            $sql .= ' AND e.nom LIKE :nomAttributaire';

            $parameters['nomAttributaire'] = '%' . $nomAttributaire . '%';
        }

        if ('false' != $idCategorie) {
            $sql .= ' AND c.categorie = :idCategorie';

            $parameters['idCategorie'] = $idCategorie;
        }

        if ('false' != $dateNotifMin) {
            $sql .= ' AND c.date_notification >= :dateNotifMin';

            $parameters['dateNotifMin'] = $dateNotifMin->format('Y-m-d');
        }

        if ('false' != $dateNotifMax) {
            $sql .= ' AND c.date_notification <= :dateNotifMax';

            $parameters['dateNotifMax'] = $dateNotifMax->format('Y-m-d');
        }

        if ('false' != $montantMin) {
            $sql .= ' AND c.montant_contrat >= :montantMin';

            $parameters['montantMin'] = $montantMin;
        }

        if ('false' != $montantMax) {
            $sql .= ' AND c.montant_contrat <= :montantMax';

            $parameters['montantMax'] = $montantMax;
        }

        if ('false' != $lieuxExecution) {
            $sql .= ' AND c.nom_lieu_principal_execution IN (:lieuxExecution)';

            $parameters['lieuxExecution'] = explode(', ', (string) $lieuxExecution);
        }

        if ('false' != $codesCpv) {
            $sql .= ' AND c.code_cpv1 IN (:codesCpv)';

            $parameters['codesCpv'] = explode(',', (string) $codesCpv);
        }

        if ('false' != $motsCles) {
            $arrayMotsCles = [];

            if ($motsCles) {
                $motsCles = strtr($motsCles, ",;.&!?'\"()[]{}°+*/\\|:%", '                      ');
                $arrayMotsCles = explode(' ', $motsCles);
                $arrayMotsInterdits = ' and or et ou le la les un une du de des a à au aux son sa ses ne ni non sauf
                 ce ces cet je tu il elle on nous vous ils elles etc mon ma ton ta vos se y en sur ';
                $excludedTermsArray = explode(' ', $arrayMotsInterdits);
                $filteredValues = [];

                foreach ($arrayMotsCles as $id => $unMotCle) {
                    $unMotCle = trim(addslashes(($unMotCle)));
                    $arrayCaractere = ["'", '-', ' ', '"'];
                    $str = str_replace($arrayCaractere, '%', $unMotCle);
                    $unMotCle = strtolower(htmlspecialchars($str, ENT_NOQUOTES | ENT_QUOTES, $encoding));

                    if ($unMotCle && false === array_search($unMotCle, $excludedTermsArray) && strlen($unMotCle) >= 2) {
                        $filteredValues[] = '%' . $unMotCle . '%';
                    }
                }
            }

            $filteredFirstText = current($filteredValues);
            $keyWordsCondition = '';
            // Generate Openning Parenthesis
            $nbrKeyWords = count($filteredValues);
            $nbrParenthesis = (7 * $nbrKeyWords) + ($nbrKeyWords - 1);
            $openedParenthesis = '';
            for ($i = 1; $i <= $nbrParenthesis; ++$i) {
                $openedParenthesis .= '(';
            }
            // Construct sql Condition for keywords filter
            foreach ($filteredValues as $key => $mot) {
                $keyWordsCondition .= (1 === $nbrKeyWords || $filteredFirstText === $mot) ?
                    ' c.numero_contrat LIKE :mot' . $key :
                    ' OR c.numero_contrat LIKE :mot' . $key . ')';
                $keyWordsCondition .= ' OR 
                        c.objet_contrat LIKE :mot' . $key . ') OR
                        c.num_long_OEAP LIKE :mot' . $key . ') OR
                        c.reference_libre LIKE :mot' . $key . ') OR
                        e.nom LIKE :mot' . $key . ') OR
                        e.siren LIKE :mot' . $key . ') OR
                        e.sirenetranger LIKE :mot' . $key . ') OR
                        tc.numero_contrat LIKE :mot' . $key . ')';

                $parameters['mot' . $key] = $mot;
            }
            $sql = $sql . 'AND' . $openedParenthesis . $keyWordsCondition;
        }

        $sql .= ' ORDER BY c.id_type_contrat, c.id_tranche_budgetaire';

        $rsm = new ResultSetMappingBuilder($em);
        $rsm->addRootEntityFromClassMetadata(
            ContratTitulaire::class,
            'c',
            ['date_creation', 'date_creation_contrat']
        );
        $query = $em->createNativeQuery($sql, $rsm)->setParameters($parameters);
        unset($openedParenthesis);
        $data = [];

        foreach ($query->getResult() as $key => $contrat) {
            $qb = $this->createQueryBuilder('c');

            $qb
                ->select('m')
                ->from(ModificationContrat::class, 'm')
                ->where('m.idContratTitulaire = :id')
                ->setParameter('id', $contrat->getIdContratTitulaire());

            $modifications = $qb->getQuery()->getResult();

            $data[] = ['contrat' => $contrat];

            if (!empty($modifications)) {
                $data[$key]['modifications'] = $modifications;
            }

            if ('1' == $concession) {
                foreach ($query->getResult() as $key => $contrat) {
                    $qb = $this->createQueryBuilder('c');

                    $qb
                        ->select('m')
                        ->from(DonneesAnnuellesConcession::class, 'm')
                        ->where('m.idContrat = :id')
                        ->setParameter('id', $contrat->getIdContratTitulaire());

                    $modifications = $qb->getQuery()->getResult();

                    if (!empty($modifications)) {
                        $data[$key]['ajoutDonneeAnnuelles'] = $modifications;
                    }
                }
            }
        }

        foreach ($data as $contrat) {
            yield $contrat;
        }
    }

    /**
     * @param $idEntitePublique
     * @param $arrayIdsServices
     * @param $annee
     * @param $nomAttributaire
     * @param $idCategorie
     * @param $montantMin
     * @param $montantMax
     * @param $lieuxExecution
     * @param $codesCpv
     * @param $dateNotifMin
     * @param $dateNotifMax
     * @param $motsCles
     * @param $encoding
     * @param $concession
     *
     * @return array
     */
    public function getMarchesOrConcessionByCriteria(
        $idEntitePublique,
        $arrayIdsServices,
        $annee,
        $nomAttributaire,
        $idCategorie,
        $montantMin,
        $montantMax,
        $lieuxExecution,
        $codesCpv,
        $dateNotifMin,
        $dateNotifMax,
        $motsCles,
        $encoding,
        $concession = 0
    ) {
        $em = $this->getEntityManager();

        $sql = 'SELECT c.*
                FROM t_contrat_titulaire as c
                LEFT JOIN Entreprise e ON c.id_titulaire = e.id
                WHERE c.organisme = :organisme
                AND c.service_id IN (:arrayIdsServices)
                AND c.date_notification LIKE :annee
                AND c.contrat_class_key = :one
                AND c.publication_contrat = :zero
                AND c.date_notification IS NOT NULL';

        $tabIdConcession = [];
        $typeContrats = $this->createQueryBuilder('c')
            ->select('ttc')
            ->from(TypeContrat::class, 'ttc')
            ->where('ttc.concession = true')
            ->getQuery()->getResult();
        foreach ($typeContrats as $typeContrat) {
            $tabIdConcession[] = $typeContrat->getIdTypeContrat();
        }

        $in = 'not in ';
        if (1 == $concession) {
            $in = 'in ';
        }

        $parameters = [
            'organisme' => $idEntitePublique,
            'arrayIdsServices' => $arrayIdsServices,
            'annee' => $annee . '%',
            'one' => 1,
            'zero' => 0,
            'arrayIdTypeContrat' => 0,
        ];

        $sql .= ' AND c.id_type_contrat ' . $in . ' (:arrayIdTypeContrat)';
        $parameters['arrayIdTypeContrat'] = implode($tabIdConcession);

        if ('false' != $nomAttributaire) {
            $sql .= ' AND e.nom LIKE :nomAttributaire';

            $parameters['nomAttributaire'] = '%' . $nomAttributaire . '%';
        }

        if ('false' != $idCategorie) {
            $sql .= ' AND c.categorie = :idCategorie';

            $parameters['idCategorie'] = $idCategorie;
        }

        if ('false' != $dateNotifMin) {
            $sql .= ' AND c.date_notification >= :dateNotifMin';

            if ($dateNotifMin instanceof DateTime) {
                $dateNotifMin = $dateNotifMin->format('Y-m-d');
            }
            $parameters['dateNotifMin'] = $dateNotifMin;
        }

        if ('false' != $dateNotifMax) {
            $sql .= ' AND c.date_notification <= :dateNotifMax';

            if ($dateNotifMax instanceof DateTime) {
                $dateNotifMax = $dateNotifMax->format('Y-m-d');
            }
            $parameters['dateNotifMax'] = $dateNotifMax;
        }

        if ('false' != $montantMin) {
            $sql .= ' AND c.montant_contrat >= :montantMin';

            $parameters['montantMin'] = $montantMin;
        }

        if ('false' != $montantMax) {
            $sql .= ' AND c.montant_contrat <= :montantMax';

            $parameters['montantMax'] = $montantMax;
        }

        if ('false' != $lieuxExecution) {
            $sql .= ' AND c.nom_lieu_principal_execution IN (:lieuxExecution)';

            $parameters['lieuxExecution'] = explode(', ', (string) $lieuxExecution);
        }

        if ('false' != $codesCpv) {
            $sql .= ' AND c.code_cpv1 IN (:codesCpv)';

            $parameters['codesCpv'] = explode(',', (string) $codesCpv);
        }

        if ('false' != $motsCles) {
            $arrayMotsCles = [];

            if ($motsCles) {
                $motsCles = strtr($motsCles, ",;.&!?'\"()[]{}°+*/\\|:%", '                      ');
                $arrayMotsCles = explode(' ', $motsCles);
                $arrayMotsInterdits = explode(' ', 'and or et ou le la les un une du de des a à au aux 
                son sa ses ne ni non sauf ce ces cet je tu il elle on nous vous ils
                 elles etc mon ma ton ta vos se y en sur');
                $diff = array_diff($arrayMotsCles, $arrayMotsInterdits);

                foreach ($diff as $unMotCle) {
                    $unMotCle = trim(addslashes(($unMotCle)));
                    $arrayCaractere = ["'", '-', ' ', '"'];
                    $str = str_replace($arrayCaractere, '%', $unMotCle);
                    $unMotCle = htmlspecialchars($str, ENT_NOQUOTES | ENT_QUOTES, $encoding);
                    $arrayMotsCles[] = '%' . $unMotCle . '%';
                }
            }

            foreach ($arrayMotsCles as $key => $mot) {
                $sql .= ' AND (c.numero_contrat LIKE :mot' . $key . ' OR
                        c.objet_contrat LIKE :mot' . $key . ' OR
                        c.num_long_OEAP LIKE :mot' . $key . ' OR
                        c.reference_libre LIKE :mot' . $key . ' OR
                        e.nom LIKE :mot' . $key . ' OR
                        e.siren LIKE :mot' . $key . ' OR
                        e.sirenetranger LIKE :mot' . $key . ')';

                $parameters['mot' . $key] = "%$mot%";
            }
        }

        $sql .= ' ORDER BY c.id_type_contrat, c.id_tranche_budgetaire';

        $rsm = new ResultSetMappingBuilder($em);
        $rsm->addRootEntityFromClassMetadata(
            ContratTitulaire::class,
            'c',
            ['date_creation', 'date_creation_contrat']
        );
        $query = $em->createNativeQuery($sql, $rsm)->setParameters($parameters);

        $data = [];

        foreach ($query->getResult() as $key => $contrat) {
            $qb = $this->createQueryBuilder('c');
            $qb
                ->select('m')
                ->from(ModificationContrat::class, 'm')
                ->where('m.idContratTitulaire = :id')
                ->setParameter('id', $contrat->getIdContratTitulaire());

            $modifications = $qb->getQuery()->getResult();
            $data[] = ['contrat' => $contrat];
            if (!empty($modifications)) {
                $data[$key]['modifications'] = $modifications;
            }
            if ('1' == $concession) {
                foreach ($query->getResult() as $key => $contrat) {
                    $qb = $this->createQueryBuilder('c');

                    $qb
                        ->select('m')
                        ->from(DonneesAnnuellesConcession::class, 'm')
                        ->where('m.idContrat = :id')
                        ->setParameter('id', $contrat->getIdContratTitulaire());

                    $modifications = $qb->getQuery()->getResult();

                    if (!empty($modifications)) {
                        $data[$key]['ajoutDonneeAnnuelles'] = $modifications;
                    }
                }
            }
        }
        foreach ($data as $contrat) {
            yield $contrat;
        }
    }

    public function getListeChorusEchangeInvalid()
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select(['c.idContratTitulaire', 'c.numEJ', 'c.statutEJ', 'c.organisme', 'cnf.nomFichier',
            'cnf.typeFichier'])
            ->leftJoin(ChorusNomsFichiers::class, 'cnf', 'WITH', $qb->expr()->andX(
                $qb->expr()->eq('c.organisme', 'cnf.acronymeOrganisme'),
                $qb->expr()->eq('c.idContratTitulaire', 'cnf.idEchange'),
                $qb->expr()->eq('cnf.typeFichier', '\'FSO\'')
            ))
            ->andWhere('c.statutEJ is not null')
            ->andWhere('cnf.typeFichier is null');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $nombreElementsParPage
     * @param $NumeroPage
     * @param $dateModification
     * @param $module
     * @param $orgServicesAllowed
     * @param $searchId
     * @return Generator
     */
    public function findAllByParamWs(
        $nombreElementsParPage,
        $NumeroPage,
        $dateModification,
        $module,
        $orgServicesAllowed = [],
        $tabParams = [],
        $searchId = false
    ) {
        return $this->findWithParamWs(
            $nombreElementsParPage,
            $NumeroPage,
            $dateModification,
            $module,
            $orgServicesAllowed,
            $tabParams,
            $searchId
        );
    }

    /**
     * Récupérer un contrat via son ID
     *
     */
    public function findByIdAndParamWs(
        int $idEntityToSearch,
        string $module,
        array $orgServicesAllowed = []
    ): Generator {
        return $this->findWithParamWs(
            null,
            null,
            $idEntityToSearch,
            $module,
            $orgServicesAllowed,
            [],
            true
        );
    }

    /**
     * Récupérer une liste de contrat ou un contrat spécifique
     *
     */
    public function findWithParamWs(
        ?int $nombreElementsParPage,
        ?int $NumeroPage,
        mixed $criteria,
        string $module,
        array $orgServicesAllowed,
        array $tabParams = [],
        bool $searchId = false
    ): Generator {

        /**
         * Règles Métier :
         * pour les contrats rattachés à un service, ils sont exposés si :
         *  - le module est activé au niveau de l'organisme (configuration_organisme.module_exec = 1)
         * ET la synchro est activée au niveau du service de rattachement du contrat (service.synchronisation_exec = 1)
         * pour les contrats rattachés directement à la racine de l'organisme, ils sont exposés si :
         *  - le module est activé au niveau de l'organisme (configuration_organisme.module_exec = 1)
         */
        $contracts = $this->getContratsForService(
            $nombreElementsParPage,
            $NumeroPage,
            $criteria,
            $module,
            $orgServicesAllowed,
            $tabParams,
            $searchId
        );

        foreach ($contracts as $contract) {
            if ($contract instanceof ContratTitulaire) {
                if (!empty($contract->getReferenceConsultation())) {
                    $contract->setConsultation(
                        $this->getConsultationByIdContratTitulaire($contract->getIdContratTitulaire())
                    );
                }

                if (!empty($contract->getIdTypeContrat())) {
                    $contract->setTypeContrat($this->getTypeContrat($contract->getIdTypeContrat()));
                }

                $contract->setContratTitulaireFavori($this->getFavoris($contract->getIdContratTitulaire()));
                $contract->setFormePrix(strtoupper($this->getFormePrix($contract->getIdTypeContrat())));
                yield $contract;
                $this->_em->detach($contract);
            }
        }
    }

    /**
     * @param int|null $nombreElementsParPage
     * @param int|null $NumeroPage
     * @param mixed $criteria
     * @param string $module
     * @param array $orgServicesAllowed
     * @param array $tabParams
     * @param bool $searchId
     * @return float|int|mixed|string
     */
    private function getContratsForService(
        ?int $nombreElementsParPage,
        ?int $NumeroPage,
        mixed $criteria,
        string $module,
        array $orgServicesAllowed,
        array $tabParams = [],
        bool $searchId = false
    ) {
        $queryBuilder = $this->createQueryBuilder('c');
        $queryBuilder->select('c')
            ->distinct()
            ->leftJoin(
                ContactContrat::class,
                'r',
                'WITH',
                'c.idContactContrat = r.idContactContrat'
            )
            ->addSelect('r');

        if (is_int($criteria) && $searchId) {
            $queryBuilder->where('c.idContratTitulaire = :idContratTitulaire');
        } else {
            $queryBuilder->where('c.dateModification >= :datemodification');
        }

        if ('true' !== strtolower($tabParams['disableFiltering'])) {
            $queryBuilder->orderBy('c.dateModification', 'DESC');
        }

        $parameters = (is_int($criteria) && $searchId)
            ? ['idContratTitulaire' => $criteria]
            : ['datemodification' => $criteria]
        ;

        $queryBuilder->leftJoin(
            Service::class,
            's',
            'WITH',
            'c.serviceId = s.id and c.organisme = s.organisme'
        );
        $queryBuilder->join(
            ConfigurationOrganisme::class,
            'co',
            'WITH',
            'c.organisme = co.organisme '
        );

        if (key_exists('statut', $tabParams) && $tabParams['statut'] !== false) {
            $statut = self::STATUT_NOTIFICATION_CONTRAT_EFFECTUEE;
            switch ($tabParams['statut']) {
                case 'STATUT_DONNEES_CONTRAT_A_SAISIR':
                    $statut = self::STATUT_DONNEES_CONTRAT_A_SAISIR;
                    break;
                case 'STATUT_NUMEROTATION_AUTONOME':
                    $statut = self::STATUT_NUMEROTATION_AUTONOME;
                    break;
                case 'STATUT_NOTIFICATION_CONTRAT':
                    $statut = self::STATUT_NOTIFICATION_CONTRAT;
                    break;
            }
            $queryBuilder->andWhere(" c.statutContrat =  '" . $statut . "'");
            $queryBuilder->setParameter('statut_contrat', $statut);
        }

        if (is_array($orgServicesAllowed) && !empty($orgServicesAllowed)) {
            $condition = [];
            foreach ($orgServicesAllowed as $key => $services) {
                $param = str_replace('-', '_', $key);
                $condition[$key] = " (  c.organisme = :$param";
                $parameters[$param] = $key;
                if (!in_array(0, $services)) {
                    $condition[$key] .= " AND c.serviceId in ('" . implode("','", $services) . "')";
                }
                $condition[$key] .= ')';
            }
            if (!empty($condition)) {
                $queryBuilder->andWhere('( ' . implode(' OR ', $condition) . ')');
            }
        }

        $queryBuilder->setParameters($parameters);

        if (!$searchId) {
            $limit = $nombreElementsParPage;
            $offset = $this->getOffset($NumeroPage, $nombreElementsParPage);
            $queryBuilder->setFirstResult($offset);
            $queryBuilder->setMaxResults($limit);
        }

        return  $queryBuilder->getQuery()->getResult();
    }


    private function getContratsForNoService(
        ?int $nombreElementsParPage,
        ?int $NumeroPage,
        mixed $criteria,
        string $module,
        array $orgServicesAllowed,
        array $tabParams = [],
        bool $searchId = false
    ) {
        $queryBuilder = $this->createQueryBuilder('c');
        $queryBuilder->select('c')
            ->distinct()
            ->leftJoin(
                ContactContrat::class,
                'r',
                'WITH',
                'c.idContactContrat = r.idContactContrat'
            )
            ->addSelect('r');

        if (is_int($criteria) && $searchId) {
            $queryBuilder->where('c.idContratTitulaire = :idContratTitulaire');
        } else {
            $queryBuilder->where('c.dateModification >= :datemodification');
        }

        $queryBuilder->orderBy('c.dateModification', 'DESC');

        $parameters = (is_int($criteria) && $searchId)
            ? ['idContratTitulaire' => $criteria]
            : ['datemodification' => $criteria]
        ;

        $em = $this->getEntityManager();
        $organismes = $em->createQueryBuilder()
            ->select('o')
            ->from(Organisme::class, 'o');
        $sub = $em->createQueryBuilder();
        $sub->select("s");
        $sub->from(Service::class, "s");
        $sub->andWhere('o.acronyme = s.organisme');

        $organismes->andWhere($queryBuilder->expr()->not($queryBuilder->expr()->exists($sub->getDQL())));
        $listOrganismeSansService = [];
        foreach ($organismes->getQuery()->getResult() as $org) {
            $listOrganismeSansService[] = $org->getAcronyme();
        }

        $queryBuilder->andWhere(
            $queryBuilder->expr()->in('co.organisme', $listOrganismeSansService)
        );

        $queryBuilder->join(
            ConfigurationOrganisme::class,
            'co',
            'WITH',
            'c.organisme = co.organisme '
        );

        if ('EXEC' === $module) {
            $queryBuilder->andWhere(" co.moduleExec = '1' ");
        }

        if (key_exists('statut', $tabParams) && $tabParams['statut'] !== false) {
            $statut = self::STATUT_NOTIFICATION_CONTRAT_EFFECTUEE;
            switch ($tabParams['statut']) {
                case 'STATUT_DONNEES_CONTRAT_A_SAISIR':
                    $statut = self::STATUT_DONNEES_CONTRAT_A_SAISIR;
                    break;
                case 'STATUT_NUMEROTATION_AUTONOME':
                    $statut = self::STATUT_NUMEROTATION_AUTONOME;
                    break;
                case 'STATUT_NOTIFICATION_CONTRAT':
                    $statut = self::STATUT_NOTIFICATION_CONTRAT;
                    break;
            }
            $queryBuilder->andWhere(" c.statutContrat =  '" . $statut . "'");
            $queryBuilder->setParameter(':statut_contrat', $statut);
        }

        if (is_array($orgServicesAllowed) && !empty($orgServicesAllowed)) {
            $condition = [];
            foreach ($orgServicesAllowed as $key => $services) {
                $param = str_replace('-', '_', $key);
                $condition[$key] = " (  c.organisme = :$param";
                $parameters[$param] = $key;
                if (!in_array(0, $services)) {
                    $condition[$key] .= " AND c.serviceId in ('" . implode("','", $services) . "')";
                }
                $condition[$key] .= ')';
            }
            if (!empty($condition)) {
                $queryBuilder->andWhere('( ' . implode(' OR ', $condition) . ')');
            }
        }

        $queryBuilder->setParameters($parameters);

        if (!$searchId) {
            $limit = $nombreElementsParPage;
            $offset = $this->getOffset($NumeroPage, $nombreElementsParPage);
            $queryBuilder->setFirstResult($offset);
            $queryBuilder->setMaxResults($limit);
        }

        return  $queryBuilder->getQuery()->getResult();
    }

    public function getOffset($page, $limit)
    {
        $offset = 0;
        if (0 != $page && 1 != $page) {
            $offset = ($page - 1) * $limit;
        }

        return $offset;
    }

    /**
     * @param $idTypeContract
     *
     * @return string
     */
    public function getFormePrix($idTypeContract)
    {
        $formePrix = '';

        if (in_array($idTypeContract, [1, 4, 6, 7, 8, 9, 10, 11, 12, 13])) {
            $formePrix = 'Forfaitaire';
        } elseif (in_array($idTypeContract, [3, 5])) {
            $formePrix = 'Unitaire';
        }

        return $formePrix;
    }

    /**
     * @param string $organisme
     * @param array  $idServices
     * @param int    $statut
     *
     * @return int
     */
    public function getContratsTableauBord($organisme, $idServices, $statutsContrat)
    {
        $qb = $this->createQueryBuilder('c');
        $qb->select('count(c) as nb,c.statutContrat');
        $qb->LeftJoin(Entreprise::class, 'e', 'WITH', 'c.idTitulaire = e.id');
        $qb->where('c.organisme = :organisme');
        $qb->andWhere('c.serviceId in (:arrayIdsServices)');
        $qb->andWhere('c.statutContrat in (:arrayStatut)');
        $qb->groupBy('c.statutContrat');

        $qb->setParameters([
            'organisme' => $organisme,
            'arrayIdsServices' => $idServices,
            'arrayStatut' => $statutsContrat,
        ]);

        $resultQuery = $qb->getQuery()->getResult();

        $finalResult = [];
        foreach ($resultQuery as $row) {
            $finalResult[$row['statutContrat']] = $row['nb'];
        }

        return $finalResult;
    }

    public function getConsultation($organisme, $reference)
    {
        $result = $this
            ->getEntityManager()
            ->getRepository(Consultation::class)->findOneBy(
                ['organisme' => $organisme, 'referenceUtilisateur' => $reference]
            );

        return $result;
    }

    public function getConsultationByIdContratTitulaire($idContratTitulaire)
    {
        $idConsultation = $this
            ->getEntityManager()
            ->getRepository(ConsLotContrat::class)->getConsultationIdByContratTitulaire($idContratTitulaire);

        return $this->getEntityManager()
            ->getRepository(Consultation::class)->findOneBy(
                ['id' => $idConsultation[0]['id']]
            );
    }

    public function getTypeContrat($idTypeContrat)
    {
        $result = $this
            ->getEntityManager()
            ->getRepository(TypeContrat::class)->find($idTypeContrat);

        return $result;
    }

    public function getFavoris($idContratTitulaire)
    {
        $result = $this
            ->getEntityManager()
            ->getRepository(ContratTitulaireFavori::class)->findBy(['idContratTitulaire' => $idContratTitulaire]);

        return $result;
    }

    /**
     * @return array
     */
    public function getOffreEtEntreprise(array $ids)
    {
        return $this->createQueryBuilder('c')
            ->select('c.idOffre, o.nomEntrepriseInscrit as entreprise, o.numeroReponse ')
            ->join(Offre::class, 'o', 'WITH', 'o.id = c.idOffre')
            ->join('o.enveloppes', 'e')
            ->where('c.idContratTitulaire in (:ids)')
            ->setParameter('ids', $ids)
            ->andWhere('e.typeEnv = 1')
            ->getQuery()
            ->getResult();
    }

    public function persistContrat(ContratTitulaire $contratTitulaire): void
    {
        $this->getEntityManager()->persist($contratTitulaire);
        $this->getEntityManager()->flush();
    }

    public function getContratsWithConsLotNotChapeau($organisme)
    {
        try {
            $sql = <<<SQL
SELECT t_contrat_titulaire.id_contrat_titulaire as idContratTitulaire,
       t_contrat_titulaire.id_offre as idOffre,
       t_cons_lot_contrat.lot,
       t_cons_lot_contrat.consultation_id as consultationId,
       t_contrat_titulaire.numero_contrat as numeroContrat,
       t_contrat_titulaire.date_notification as dateNotification,
       t_contrat_titulaire.montant_contrat as montantContrat,
       t_contrat_titulaire.type_depot_reponse as typeDepotReponse
FROM t_contrat_titulaire,
     t_cons_lot_contrat
WHERE t_contrat_titulaire.id_contrat_titulaire = t_cons_lot_contrat.id_contrat_titulaire
  AND t_contrat_titulaire.contrat_class_key = :classKey
  AND t_contrat_titulaire.organisme = :org
SQL;
            $rsm = $this->_em->getConnection()->prepare($sql);
            $parameters["classKey"] = "1";
            $parameters["org"] = $organisme;
            return $rsm->executeQuery($parameters);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getSadAndAcContratByNumeroContrat(string $numero, $typeSAD = null): ?ContratTitulaire
    {
        $qb = $this->createQueryBuilder('c')
            ->select('c')
            ->innerJoin(TypeContrat::class, 'tc', Join::WITH, 'c.idTypeContrat=tc.idTypeContrat')
            ->andWhere('c.numeroContrat = :numero')
            ->setParameter('numero', $numero)
            ;
        if (null !== $typeSAD) {
            $qb->andWhere('tc.accordCadreSad = :type ')
                ->setParameter('type', $typeSAD);
        } else {
            $qb->andWhere('tc.accordCadreSad IN (\'1\', \'2\')');
        }

        return  $qb->setMaxResults(1)->getQuery()->getOneOrNullResult();


    }
}
