<?php

namespace App\Repository;

use App\Entity\ConfigurationOrganisme;
use App\Entity\ContactContrat;
use App\Entity\ContratTitulaire;
use App\Entity\Service;
use App\Utils\Utils;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Serializer\Serializer;

/**
 * Class ContactContratRepository.
 */
class ContactContratRepository extends ServiceEntityRepository
{
    public string $headWs = 'contacts';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContactContrat::class);
    }

    public function findAllByParamWs($nombreElementsParPage, $NumeroPage, $dateModification, $module, $orgServicesAllowed = [])
    {
        $queryBuilder = $this->createQueryBuilder('c')
            ->select('c')
            ->where('c.updatedAt >= :datemodification')
            ->orderBy('c.updatedAt', 'DESC');
        $parameters = ['datemodification' => $dateModification];

        $queryBuilder->join(ContratTitulaire::class, 'ct', 'WITH', 'ct.idContactContrat = c.idContactContrat');
        if ('EXEC' === $module) {
            $listOrgs = $this->_em->getRepository(ConfigurationOrganisme::class)->getOrganismeAcronymesWhereSynchroExecActive();
            $conditionExec = [];
            foreach ($listOrgs as $orgSynchro) {
                if (isset($orgSynchro['organisme'])) {
                    $conditionExec[$orgSynchro['organisme']] = " ct.organisme = '".$orgSynchro['organisme']."'";
                    $services = $this->_em->getRepository(Service::class)->getServiceIdsWhereSynchroExecActive($orgSynchro['organisme']);

                    if (is_array($services) && !empty($services)) {
                        $conditionExec[$orgSynchro['organisme']] .= " AND ct.serviceId in ('".implode("','", (new Utils())->flatArray($services))."')";
                    }
                }
            }
            if (!empty($conditionExec)) {
                $queryBuilder->andWhere(implode('OR', $conditionExec));
            }
        }

        $queryBuilder->setParameters($parameters);
        $limit = $nombreElementsParPage;
        $offset = $this->getOffset($NumeroPage, $nombreElementsParPage);
        $queryBuilder->setFirstResult($offset);
        $queryBuilder->setMaxResults($limit);

        $contacts = $queryBuilder->getQuery()->getResult();

        foreach ($contacts as $contact) {
            yield $contact;
            $this->_em->detach($contact);
        }
    }

    public function getOffset($page, $limit)
    {
        $offset = 0;
        if (0 != $page && 1 != $page) {
            $offset = ($page - 1) * $limit;
        }

        return $offset;
    }

    public function getNode(ContactContrat $contact, Serializer $serializer, $mode)
    {
        $contactNormalize = $serializer->normalize($contact, $mode, ['groups' => ['webservice']]);
        $content = $serializer->serialize($contactNormalize, $mode);
        $content = str_replace('<?xml version="1.0"?>', '', $content);
        $content = str_replace("\n", '', $content);
        $content = str_replace('response>', 'contact>', $content);

        return $content;
    }
}
