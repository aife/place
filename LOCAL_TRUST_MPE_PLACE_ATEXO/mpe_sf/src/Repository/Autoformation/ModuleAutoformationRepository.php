<?php

namespace App\Repository\Autoformation;

use App\Entity\Autoformation\ModuleAutoformation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ModuleAutoformation>
 */
class ModuleAutoformationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ModuleAutoformation::class);
    }
}
