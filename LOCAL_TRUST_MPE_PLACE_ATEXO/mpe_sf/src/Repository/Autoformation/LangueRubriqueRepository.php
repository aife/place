<?php

namespace App\Repository\Autoformation;

use App\Entity\Autoformation\LangueRubrique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<LangueRubrique>
 */
class LangueRubriqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LangueRubrique::class);
    }
}
