<?php

namespace App\Repository;

use App\Entity\ConfigurationOrganisme;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

class JalRepository extends EntityRepository
{
    public function getAllJALs(int $serviceId, string $organisme)
    {
        $criterias = ['organisme' => $organisme, 'serviceId' => $serviceId];

        return $this->findBy($criterias);
    }

    public function getJallForLux(string $organism, ?int $service): array
    {
        $query = $this->createQueryBuilder('j')
            ->select('j')
            ->innerJoin(
                ConfigurationOrganisme::class,
                'co',
                Join::WITH,
                'j.organisme = co.organisme'
            )
            ->andWhere('co.pubJalLux = :pubJalLux')
            ->andWhere('j.organisme = :organism')
            ->setParameter('pubJalLux', '1')
            ->setParameter('organism', $organism);
        if ($service) {
            $query->andWhere('j.serviceId = :service')
                ->setParameter('service', $service);
        }

        return $query->getQuery()->getResult();
    }
}
