<?php

namespace App\Repository\DossierVolumineux;

use App\Entity\DossierVolumineux\DossierVolumineux;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class DossierVolumineuxRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DossierVolumineux::class);
    }

    public function getDataForAgentMessecV2(int $idAgent): array
    {
        $qb = $this->getFieldsForMessecV2()
            ->innerJoin('dv.agent', 'a')
            ->andWhere('a.id = :idAgent')
            ->setParameter('idAgent', $idAgent)
        ;

        return $qb->getQuery()->getArrayResult();
    }

    public function getDataForInscritMessecV2(int $idInscrit): array
    {
        $qb = $this->getFieldsForMessecV2()
            ->innerJoin('dv.inscrit', 'i')
            ->andWhere('i.id = :idInscrit')
            ->setParameter('idInscrit', $idInscrit)
        ;

        return $qb->getQuery()->getArrayResult();
    }

    private function getFieldsForMessecV2(): QueryBuilder
    {
        return $this->createQueryBuilder('dv')
            ->select('dv.nom', 'DATE_FORMAT(dv.dateCreation, \'%Y-%m-%dT%TZ\') AS dateCreation', 'dv.taille', 'dv.uuidReference', 'dv.uuidTechnique')
            ->where('dv.actif = true')
            ->orderBy('dv.nom', 'ASC');
    }
}
