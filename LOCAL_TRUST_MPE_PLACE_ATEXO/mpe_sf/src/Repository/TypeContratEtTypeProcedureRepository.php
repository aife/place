<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Repository;

use App\Entity\Organisme;
use Doctrine\ORM\EntityRepository;

class TypeContratEtTypeProcedureRepository extends EntityRepository
{
    private const ID_TYPE_CONTRAT = 'idTypeContrat';

    public function getTypeContratIdsFilteredByOrganisme(?Organisme $organisme): array
    {
        $qb = $this->createQueryBuilder('tctp')
            ->select('tctp.idTypeContrat')
            ->distinct()
            ->where('tctp.organisme = :organisme')
            ->setParameter('organisme', $organisme);

        return array_column($qb->getQuery()->getArrayResult(), self::ID_TYPE_CONTRAT);
    }

    public function findAllByIdentifierNotIn($notIn, $limit)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('tctp')
            ->from($this->getEntityName(), 'tctp')
            ->setMaxResults($limit);

        if (!empty($notIn)) {
            $qb->andWhere(
                $qb->expr()->notIn('tctp.idTypeContratEtProcedure', $notIn)
            );
        }

        $res = $qb
            ->getQuery()
            ->getResult();

        return $res;
    }


    public function countByIdentifierNotIn($notIn)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('COUNT(tctp.idTypeContratEtProcedure)')
            ->from($this->getEntityName(), 'tctp');

        if (!empty($notIn)) {
            $qb->andWhere(
                $qb->expr()->notIn('tctp.idTypeContratEtProcedure', $notIn)
            );
        }

        return $qb
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function setEm($em)
    {
        $this->_em = $em;
    }
}
