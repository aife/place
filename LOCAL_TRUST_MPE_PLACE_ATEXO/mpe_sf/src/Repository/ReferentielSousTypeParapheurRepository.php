<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Repository;

use App\Entity\ReferentielSousTypeParapheur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ReferentielSousTypeParapheur>
 *
 * @method ReferentielSousTypeParapheur|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReferentielSousTypeParapheur|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReferentielSousTypeParapheur[]    findAll()
 * @method ReferentielSousTypeParapheur[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReferentielSousTypeParapheurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReferentielSousTypeParapheur::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(ReferentielSousTypeParapheur $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(ReferentielSousTypeParapheur $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
