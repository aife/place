<?php

namespace App\Repository;

use App\Entity\AffiliationService;
use App\Entity\Service;
use Doctrine\DBAL\Exception as DBALException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AffiliationService>
 */
class AffiliationServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AffiliationService::class);
    }

    public function getDistinctByIdPoleAndOrganisme($idPole)
    {
        $qb = $this->createQueryBuilder('a');

        $qb
            ->where('a.serviceParentId = :serviceParentId')
            ->setParameter('serviceParentId', $idPole)
            ->distinct(true)
            ->groupBy('a.serviceId');

        return $qb->getQuery()->getResult();
    }

    public function getParentByServiceIdAndOrganisme($serviceId, $org)
    {
        $qb = $this->createQueryBuilder('a');

        $qb
            ->where('a.serviceId = :serviceId')
            ->setParameter('serviceId', $serviceId)
            ->andWhere('a.organisme = :org')
            ->setParameter('org', $org);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $serviceId
     * @param $org
     *
     * @return array
     */
    public function getAllChildrenServices($serviceId, $org)
    {
        $arrayChilds = [];
        if ($serviceId) {
            $arrayChilds = self::getAllChildsId($serviceId, $org, $arrayChilds);
        } else {
            $services = self::retrieveEntities($org);
            $arrayChilds[] = '0';
            if ($services) {
                $arrayChilds[] = $serviceId;
            }
        }

        return $arrayChilds;
    }

    /**
     * Permet de récupérer la liste des entités d'achat d'un organisme donné en parmètre
     * le tri se fait sur le chemin complet asc.
     *
     * @param string $organisme
     *
     * @return array
     */
    public function retrieveEntities($organisme)
    {
        $qb = $this->createQueryBuilder('a');
        $qb->select('s.id')
            ->from(Service::class, 's')
            ->where('s.acronymeOrg = :organisme')
            ->addOrderBy('s.cheminComplet', 'ASC')
            ->setParameter('organisme', $organisme);

        return $qb->getQuery()->getResult();
    }

    public function getAllChildsId($idPurchaseEntity, $org, &$arrayChilds)
    {
        if (is_array($arrayChilds)) {
            $levelOneChilds = self:: getLevelOneChilds($idPurchaseEntity, $org);
            if ($idPurchaseEntity) {
                $arrayChilds[] = $idPurchaseEntity;
            }
            if (is_array($levelOneChilds) && count($levelOneChilds) > 0) {
                foreach ($levelOneChilds as $child) {
                    self:: getAllChildsId($child, $org, $arrayChilds);
                }
            }
        }

        return $arrayChilds;
    }

    public function getLevelOneChilds($idPurchaseEntity, $org)
    {
        $resServices = [];
        if ($idPurchaseEntity) {
            $sql = <<<SQL
SELECT 
    service_id
FROM
    AffiliationService
WHERE 
    organisme like :org
    AND
    service_parent_id = :serviceParentId
SQL;
            $em = $this->getEntityManager();
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->bindValue('org', $org);
            $stmt->bindValue('serviceParentId', $idPurchaseEntity);
            $resServices = $stmt->executeQuery()->fetchFirstColumn();
        }

        return $resServices;
    }

    /**
     * Retourne les IDs d'un service et de tous ses descendants (recursivement).
     *
     * @throws DBALException
     */
    public function getServiceDescendantsIds(int $parentId): array
    {
        $sql = <<<SQL
            with recursive service_hierarchie (id, parent_id) as (
                select service_id,
                       service_parent_id
                from AffiliationService
                where service_parent_id = :ancestorId
                union all
                select p.service_id,
                       p.service_parent_id
                from AffiliationService p
                         inner join service_hierarchie
                                    on p.service_parent_id = service_hierarchie.id
            )
            select id
            from service_hierarchie;
            SQL;

        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('ancestorId', $parentId);

        return $stmt->executeQuery()->fetchFirstColumn();
    }

    /**
     * Retourne les IDs des services d'un organisme.
     *
     * @throws DBALException
     */
    public function getOrganismeServicesIds(string $organismeAcronyme): array
    {
        $sql = 'select id from Service where organisme = :organisme;';

        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('organisme', $organismeAcronyme);

        return $stmt->executeQuery()->fetchFirstColumn();
    }

    /**
     * Le schema avec une clé composite ne permet pas à l'heure actuelle de faire correctement un persist.
     *
     * @param $serviceId
     * @param $acronymeOrganisme
     * @param $serviceParentId
     *
     * @throws DBALException
     */
    public function updateAffiliation($serviceId, $acronymeOrganisme, $serviceParentId)
    {
        $sql = 'UPDATE `AffiliationService` 
                SET `service_parent_id`= :serviceParentId 
                WHERE  `service_id`= :idService 
                AND `organisme`= :acronymeOrganisme ;';
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('serviceParentId', $serviceParentId);
        $stmt->bindValue('idService', $serviceId);
        $stmt->bindValue('acronymeOrganisme', $acronymeOrganisme);
        $stmt->execute();
    }

    /**
     * Le schema avec une clé composite ne permet pas à l'heure actuelle de faire correctement un remove.
     *
     * @param $idService
     * @param $acronymeOrganisme
     *
     * @throws DBALException
     */
    public function removeAffiliation($idService, $acronymeOrganisme)
    {
        $sql = 'DELETE FROM `AffiliationService` 
                WHERE  `service_id`= :idService 
                AND `organisme`= :acronymeOrganisme ;';
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('idService', $idService);
        $stmt->bindValue('acronymeOrganisme', $acronymeOrganisme);
        $stmt->executeStatement();
    }

    /**
     * @throws DBALException
     */
    public function getServiceParent($serviceId, $acronymeOrganisme): array
    {
        $sql = 'SELECT *
                FROM `AffiliationService` a0_ 
                WHERE a0_.service_id = :serviceId 
                AND a0_.organisme = :acronymeOrganisme';
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('serviceId', $serviceId);
        $stmt->bindValue('acronymeOrganisme', $acronymeOrganisme);

        return $stmt->executeQuery()->fetchAllAssociative();
    }
}
