<?php

namespace App\Repository;

use App\Entity\HabilitationProfil;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ManagerRegistry;

class HabilitationProfilRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HabilitationProfil::class);
    }

    public function retrieveAgentProfile(int $agentId)
    {
        $sql = "SELECT hp.*
        FROM HabilitationProfil hp
        INNER JOIN Agent_Service_Metier AS asm ON asm.id_profil_service = hp.id
        WHERE asm.id_agent = :agentId";

        $stmt = $this->getEntityManager()
            ->getConnection()->prepare($sql);
        $stmt->bindValue(':agentId', $agentId);

        return $stmt->executeQuery()->fetchAssociative();
    }
}
