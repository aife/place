<?php

namespace App\Repository;

use App\Entity\HabilitationProfilHabilitation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<HabilitationProfilHabilitation>
 *
 * @method HabilitationProfilHabilitation|null find($id, $lockMode = null, $lockVersion = null)
 * @method HabilitationProfilHabilitation|null findOneBy(array $criteria, array $orderBy = null)
 * @method HabilitationProfilHabilitation[] findAll()
 */
class HabilitationProfilHabilitationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HabilitationProfilHabilitation::class);
    }
}
