<?php

namespace App\Repository;

use App\Entity\DCE;
use App\Entity\HistoriquePurge;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class DCERepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DCE::class);
    }

    /**
     * @param int    $consultationId
     * @param string $organisme
     *
     * @return array
     *
     * @author Fernando Lozano
     */
    public function getDce($consultationId, $organisme)
    {
        $qb = $this->createQueryBuilder('dce')
            ->where('dce.consultationId = :consultationId')->setParameter('consultationId', $consultationId)
            ->andWhere('dce.organisme = :organisme')->setParameter('organisme', $organisme)
            ->andWhere('dce.dce != 0')
            ->addOrderBy('dce.id', 'desc')
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getListOfDceToDelete(
        array $consultationStatut,
        ?string $organisme,
        int $limit,
        array $stateList,
        DateTimeInterface $dateDlro,
        ?array $consultationIdsList = null,
    ): array {
        $qb = $this
            ->createQueryBuilder('dce')
            ->setMaxResults($limit)
            ->leftJoin('dce.consultation', 'c')
            ->leftJoin('dce.historiquePurge', 'hp')
            ->andWhere('hp.deleted = :isDeleted or hp.deleted is null')
            ->andWhere('hp.state IN (:state) or hp.state is null')
            ->andWhere('c.idEtatConsultation IN (:idEtatConsultation)')
            ->andWhere('c.datefin < :dlro')
            ->andWhere('c.dateMiseEnLigneCalcule <> :dateMiseEnLigneCalcule')
            ->setParameters([
                'isDeleted' => HistoriquePurge::FILE_NOT_DELETED,
                'state' => $stateList,
                'idEtatConsultation' => $consultationStatut,
                'dlro' => $dateDlro->format('Y-m-d'),
                'dateMiseEnLigneCalcule' => '0000-00-00 00:00:00',
            ])
        ;

        if ($organisme) {
            $qb->andWhere('c.organisme = :organisme')->setParameter('organisme', $organisme);
        }

        if ($consultationIdsList) {
            $qb->andWhere('c.id IN (:consultationsIds)')->setParameter('consultationsIds', $consultationIdsList);
        }

        return $qb->getQuery()->getResult();
    }
}
