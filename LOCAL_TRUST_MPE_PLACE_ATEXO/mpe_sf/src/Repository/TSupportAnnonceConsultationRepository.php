<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * TSupportAnnonceConsultationRepository.
 */
class TSupportAnnonceConsultationRepository extends EntityRepository
{
    /**
     * @param $refCons
     * @param $idSupport
     * @param $statut
     *
     * @return array
     */
    public function getSupportAnnonceByRefConsAndSupportPub($consultationId, $idSupport, $statut)
    {
        $qb = $this->createQueryBuilder('s');

        $qb->andWhere('s.idSupport = :idSupport')
            ->setParameter('idSupport', $idSupport)
            ->andWhere('s.statut = :statut')
            ->setParameter('statut', $statut)
            ->join('s.idAnnonceCons', 'a')
            ->andWhere('a.consultationId = :consultationId')
            ->setParameter('consultationId', $consultationId);

        return $qb->getQuery()->getResult();
    }
}
