<?php

namespace App\Repository\Referentiel\Consultation;

use App\Entity\Referentiel\Consultation\ClausesN2;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ClausesN2>
 *
 * @method ClausesN2|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClausesN2|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClausesN2[]    findAll()
 * @method ClausesN2[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClausesN2Repository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClausesN2::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(ClausesN2 $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(ClausesN2 $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * Retourne un tableau de toutes les clauses N2 indexé par slug
     *
     */
    public function getAllClausesN2IndexedBySlug(): array
    {
        return $this->createQueryBuilder('cn2', 'cn2.slug')
            ->getQuery()
            ->getResult();
    }
}
