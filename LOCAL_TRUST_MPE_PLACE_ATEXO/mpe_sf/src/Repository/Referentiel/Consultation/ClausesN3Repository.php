<?php

namespace App\Repository\Referentiel\Consultation;

use App\Entity\Referentiel\Consultation\ClausesN3;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ClausesN3>
 *
 * @method ClausesN3|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClausesN3|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClausesN3[]    findAll()
 * @method ClausesN3[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClausesN3Repository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClausesN3::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(ClausesN3 $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(ClausesN3 $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * Retourne un tableau de toutes les clauses N3 indexé par slug
     *
     */
    public function getAllClausesN3IndexedBySlug(): array
    {
        return $this->createQueryBuilder('cn3', 'cn3.slug')
            ->getQuery()
            ->getResult();
    }
}
