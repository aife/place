<?php

namespace App\Repository\Referentiel\Consultation;

use App\Entity\Referentiel\Consultation\ClausesN4;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ClausesN4>
 *
 * @method ClausesN4|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClausesN4|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClausesN4[]    findAll()
 * @method ClausesN4[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClausesN4Repository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClausesN4::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(ClausesN4 $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(ClausesN4 $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
