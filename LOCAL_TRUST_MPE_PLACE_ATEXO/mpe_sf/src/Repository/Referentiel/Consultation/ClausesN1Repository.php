<?php

namespace App\Repository\Referentiel\Consultation;

use App\Entity\Referentiel\Consultation\ClausesN1;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ClausesN1>
 *
 * @method ClausesN1|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClausesN1|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClausesN1[]    findAll()
 * @method ClausesN1[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClausesN1Repository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClausesN1::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(ClausesN1 $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(ClausesN1 $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * Retourne un tableau de toutes les clauses N1 indexé par slug
     *
     */
    public function getAllClausesN1IndexedBySlug(): array
    {
        return $this->createQueryBuilder('cn1', 'cn1.slug')
            ->getQuery()
            ->getResult();
    }
}
