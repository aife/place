<?php

namespace App\Repository;

use App\Entity\CommandMonitoring;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CommandMonitoringRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CommandMonitoring::class);
    }

    public function clean(?\DateTime $date = null)
    {
        if (null === $date) {
            $date = (new \DateTime('now'))->modify('-1 month');
        }

        $query = $this->createQueryBuilder('c')
            ->delete()
            ->where("c.endDate < :date")
            ->setParameter('date', $date->format('Y-m-d'))
            ->getQuery();

        return $query->getResult();
    }
}
