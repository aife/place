<?php

namespace App\Repository;

use App\Entity\Inscrit\InscritHistorique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<InscritHistorique>
 *
 * @method InscritHistorique|null find($id, $lockMode = null, $lockVersion = null)
 * @method InscritHistorique|null findOneBy(array $criteria, array $orderBy = null)
 * @method InscritHistorique[]    findAll()
 * @method InscritHistorique[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InscritHistoriqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InscritHistorique::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(InscritHistorique $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(InscritHistorique $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
