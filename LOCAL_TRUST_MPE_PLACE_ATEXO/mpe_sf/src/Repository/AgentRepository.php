<?php

namespace App\Repository;

use InvalidArgumentException;
use Generator;
use DateTime;
use App\Entity\Agent;
use App\Entity\ConfigurationOrganisme;
use App\Entity\Consultation;
use App\Entity\Service;
use App\Utils\Utils;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * @extends ServiceEntityRepository<Agent>
 */
class AgentRepository extends ServiceEntityRepository implements UserProviderInterface, PasswordUpgraderInterface
{
    final public const ALERTE_ELECTRONIQUE_ON = '1';
    final public const ALERTE_ELECTRONIQUE_OFF = '0';
    final public const ALERT_RECEPTION_MESSAGE = 'alerteReceptionMessage';
    final public const ALERT_REPONSE_ELECTRONIQUE = 'alerteReponseElectronique';
    final public const ALERTE_PUBLICATION_BOAMP = 'alertePublicationBoamp';
    final public const ALERTE_PUBLICATION_BOAMP_FAILED = 'AlerteEchecPublicationBoamp';


    public string $headWs = 'agents';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Agent::class);
    }

    /**
     * @param string $username
     *
     * @return object|null
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function loadUserByIdentifier($username)
    {
        return $this->findOneBy(
            [
                'login' => $username,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function getInvitedAgents(Consultation $consultation)
    {
        $organisme = $consultation->getOrganisme()->getAcronyme();

        $referenceConsultation = $consultation->getId();

        $organisme = sprintf('"%s"', $organisme);

        $sql = 'select a.id, a.email from Agent a inner join InterneConsultation ic' .
            " where a.id=ic.interne_id and ic.organisme = $organisme and ic.consultation_id = $referenceConsultation";
        $sql .= ' union ';
        $sql .= ' select a.id, a.email from Agent a inner join InterneConsultationSuiviSeul icss' .
            " where a.id=icss.interne_id and icss.organisme = $organisme " .
            "and icss.consultation_id = $referenceConsultation";

        $em = $this->getEntityManager();

        $stmt = $em->getConnection()->prepare($sql);

        return $stmt->executeQuery()->fetchAllAssociative();
    }

    /**
     * @throws Exception
     */
    public function getPermanentsAgent(Consultation $consultation, $centralizedManagement = false)
    {
        $poleAgent = $consultation->getServiceId();

        $acrOrg = $consultation->getOrganisme()->getAcronyme();

        $acrOrg = sprintf("'%s'", $acrOrg);

        $associatedPole = $consultation->getServiceAssocieId();

        if (empty($poleAgent) || '' === $acrOrg) {
            return [];
        }

        $sql = 'SELECT a.id, a.email,a.service_id FROM HabilitationAgent ha LEFT JOIN Agent a  on a.id= ha.id_agent' .
            " where a.organisme = $acrOrg and ha.suivre_consultation_pole = '1' ";

        if ($centralizedManagement) {
            $poleAgentParents = $this->getArParentsId($poleAgent, $acrOrg, true);
            $sql .= ' AND ( a.service_id IN (' . implode(',', $poleAgentParents) . ')';
        } else {
            $sql .= " AND (a.service_id = $poleAgent";
        }

        if (!empty($associatedPole)) {
            $sql .= " OR a.service_id = $poleAgent";
        }
        $sql .= ' )';

        $em = $this->getEntityManager();

        $stmt = $em->getConnection()->prepare($sql);

        return $stmt->executeQuery()->fetchAllAssociative();
    }

    /**
     * @return object|null
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function refreshUser(UserInterface $user)
    {
        return $this->loadUserByIdentifier($user->getUsername());
    }

    /**
     * @param string $class
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function supportsClass($class)
    {
        return Agent::class === $class;
    }

    /**
     * @param  $serviceId
     * @param  $organism
     * @param bool $includeCurrService
     *
     * @return array
     */
    public function getArParentsId($serviceId, $organism, $includeCurrService = false)
    {
        $listParent = [];
        $idParent = $this->getParent($serviceId, $organism);

        while ($idParent) {
            $listParent[] = $idParent;
            $idParent = $this->getParent($idParent, $organism);
        }
        $listParent[] = 0;

        if ($includeCurrService) {
            if (!in_array($serviceId, $listParent)) {
                $listParent = [
                    ...[
                        $serviceId,
                    ],
                    ...$listParent,
                ];
            }
        }

        return $listParent;
    }

    /**
     * TODO A move towards ServiceRepository.
     *
     * @param  $serviceId
     * @param  $organism
     *
     * @return mixed
     *
     * @throws Exception
     */
    public function getParent($serviceId, $organism)
    {
        $sql = 'SELECT service_parent_id FROM AffiliationService WHERE service_id = :serviceId '
            . 'AND organisme = :organism';

        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('serviceId', $serviceId);
        $stmt->bindValue('organism', $organism);
        $stmt->execute();
        $result = $stmt->fetch();

        return $result['service_parent_id'];
    }

    /**
     * @return mixed
     */
    public function getAgentsWhoWantBeAlertedElectronicResponse(array $ids)
    {
        return $this->getAgentsByAlerte($ids, AgentRepository::ALERT_REPONSE_ELECTRONIQUE);
    }

    /**
     * Liste des agents par type d'alerte
     *
     * @return mixed
     */
    public function getAgentsByAlerte(array $ids, string $nomAlerte): array
    {
        $authorized = [
            self::ALERT_REPONSE_ELECTRONIQUE,
            self::ALERT_RECEPTION_MESSAGE,
            self::ALERTE_PUBLICATION_BOAMP,
            self::ALERTE_PUBLICATION_BOAMP_FAILED,
        ];
        if (!in_array($nomAlerte, $authorized)) {
            throw new InvalidArgumentException(sprintf("L'argument '%s' n'est pas valid!", $nomAlerte));
        }

        return $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.id IN (:ids)')
            ->andWhere('a.' . $nomAlerte . ' = :status')
            ->andWhere('a.actif = :actif')
            ->andWhere('a.deletedAt is null')
            ->setParameter('ids', $ids)
            ->setParameter('actif', "1")
            ->setParameter('status', self::ALERTE_ELECTRONIQUE_ON)
            ->getQuery()->getResult();
    }

    /**
     * @param $salt
     * @param $token
     *
     * @return mixed
     *
     * @throws Exception
     */
    public function getIdAgentHash($salt, $token)
    {
        $em = $this->getEntityManager();

        $sql = 'SELECT `id` 
                FROM Agent 
                WHERE SHA2(CONCAT(`id`, :salt), 256) = :token';

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindParam(':salt', $salt);
        $stmt->bindParam(':token', $token);
        $stmt->execute();

        $result = $stmt->fetch();

        return $result['id'];
    }

    /**
     * @param $id
     *
     * @return array|null
     *
     * @throws Exception
     */
    public function getIdAgentHyperAdmin($id)
    {
        $em = $this->getEntityManager();

        $sql = 'SELECT id_agent, hyper_admin
                FROM HabilitationAgent
                WHERE `id_agent` = :id';

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $result = $stmt->fetch();

        if (empty($result)) {
            return null;
        }

        return ['hyper_admin' => $result['hyper_admin']];
    }

    /**
     * @param $nombreElementsParPage
     * @param $NumeroPage
     *
     * @return Generator
     */
    public function findAllByParamWs(
        $nombreElementsParPage,
        $NumeroPage,
        DateTime $dateModification,
        $module,
        $orgServicesAllowed = [],
        $tabParams = [],
        $searchId = false
    ) {
        $queryBuilder = $this->createQueryBuilder('a')
            ->select('a')
            ->distinct()
            ->where('a.dateModification > :dateModification ')
            ->andWhere("a.deletedAt is null")
            ->andWhere("a.technique = '0'")
            ->orderBy('a.dateModification', 'DESC');
        $parameters = ['dateModification' => $dateModification];

        $this->addFilterForExternalModule($module, $queryBuilder);
        $this->addFilterOnAllowedServices($queryBuilder, $orgServicesAllowed);

        $limit = $nombreElementsParPage;
        $offset = $this->getOffset($NumeroPage, $nombreElementsParPage);
        $queryBuilder->setFirstResult($offset);
        $queryBuilder->setMaxResults($limit);

        $queryBuilder->setParameters($parameters);
        $agents = $queryBuilder->getQuery()->getResult();

        foreach ($agents as $agent) {
            yield $agent;
            $this->_em->detach($agent);
        }
    }

    /**
     * @param  $page
     * @param  $limit
     *
     * @return int
     */
    public function getOffset($page, $limit)
    {
        $offset = 0;
        if (0 != $page && 1 != $page) {
            $offset = ($page - 1) * $limit;
        }

        return $offset;
    }

    /**
     * @param  $mode
     *
     * @return bool|float|int|mixed|string
     */
    public function getNode(Agent $agent, Serializer $serializer, $mode)
    {
        $agentNormalize = $serializer->normalize($agent, $mode, ['groups' => ['webservice']]);
        $content = $serializer->serialize($agentNormalize, $mode);
        if ('xml' == $mode) {
            $content = str_replace('<?xml version="1.0"?>', '', $content);
            $content = str_replace("\n", '', $content);
            $content = str_replace('response>', 'agent>', $content);
        }

        return $content;
    }

    /**
     * @param  $login
     * @param int $id
     *
     * @return bool
     */
    public function isOtherAgentWithLoginExist($login, $id = 0)
    {
        $queryBuilder = $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.login like :login ');
        $params = [];
        $params['login'] = $login;
        if ($id) {
            $queryBuilder->andWhere('a.id not like :id');
            $params['id'] = $id;
        }
        $queryBuilder->setParameters(
            $params
        );

        $agents = $queryBuilder->getQuery()->getResult();

        return !empty($agents);
    }

    public function getAllAgents(int $idService, string $organisme)
    {
        $criterias = ['acronymeOrganisme' => $organisme, 'serviceId' => $idService];

        return $this->findBy($criterias);
    }

    public function getAgentPermanent($params = [])
    {
        $queryBuilder = $this->createQueryBuilder('a');
        $queryBuilder->leftJoin('a.habilitation', 'h');

        foreach ($params as $param) {
            if (array_key_exists('invite_permanent_entite_dependante', $params)) {
                $queryBuilder->andWhere('h.invitePermanentEntiteDependante = :invitePermanentEntiteDependante');
                $queryBuilder->setParameter('invitePermanentEntiteDependante', $param);
            }

            if (array_key_exists('invite_permanent_mon_entite', $params)) {
                $queryBuilder->andWhere('h.invitePermanentMonEntite = :invitePermanentMonEntite');
                $queryBuilder->setParameter('invitePermanentMonEntite', $param);
            }

            if (array_key_exists('invite_permanent_transverse', $params)) {
                $queryBuilder->andWhere('h.invitePermanentTransverse = :invitePermanentTransverse');
                $queryBuilder->setParameter('invitePermanentTransverse', $param);
            }
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @inheritDoc
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        // set the new encoded password on the User object
        $user->setPassword($newEncodedPassword);

        // execute the queries on the database
        $this->getEntityManager()->flush();
    }

    /**
     * @return int|mixed|string
     */
    public function findListAgentFromAccountAssoc(array $agentsIds)
    {
        return $this->createQueryBuilder('a', 'a.id')
            ->where('a.id IN (:ids)')
            ->setParameter('ids', $agentsIds)
            ->getQuery()
            ->getResult();
    }

    public function findAgentTechniqueByLogin(string $login): ?Agent
    {
        return $this->findOneBy([
            'login' => $login,
            'technique' => 1,
        ]);
    }

    public function getAgentReferentByIds(array $idsList): array
    {
        return $this->createQueryBuilder('a')
            ->where('a.id IN (:listIdsCreateur)')
            ->setParameter('listIdsCreateur', $idsList)
            ->getQuery()
            ->getResult();
    }

    /**
     * Récupérer un contrat via son ID
     *
     * @param int $idEntityToSearch
     * @param string $module
     * @param array $orgServicesAllowed
     */
    public function findByIdAndParamWs(
        int $idEntityToSearch,
        string $module,
        array $orgServicesAllowed = []
    ) {
        $queryBuilder = $this->createQueryBuilder('a')
            ->select('a')
            ->distinct()
            ->where('a.id = :idEntityToSearch ')
            ->andWhere("a.deletedAt is null")
            ->andWhere("a.technique = '0'")
            ->orderBy('a.dateModification', 'DESC');
        $parameters = ['idEntityToSearch' => $idEntityToSearch];

        $this->addFilterForExternalModule($module, $queryBuilder);
        $this->addFilterOnAllowedServices($queryBuilder, $orgServicesAllowed);

        $queryBuilder->setParameters($parameters);
        $agents = $queryBuilder->getQuery()->getResult();

        foreach ($agents as $agent) {
            yield $agent;
            $this->_em->detach($agent);
        }
    }

    private function addFilterForExternalModule(string $module, QueryBuilder $queryBuilder)
    {
        // Si l'application appelante souhaite un mode EXEC
        if ('EXEC' === $module) {
            $listOrgs = $this->_em->getRepository(ConfigurationOrganisme::class)
                ->getOrganismeAcronymesWhereSynchroExecActive();
            $conditionExec = [];
            foreach ($listOrgs as $orgSynchro) {
                if (isset($orgSynchro['organisme'])) {
                    $conditionExec[$orgSynchro['organisme']] = " a.organisme = '" . $orgSynchro['organisme'] . "'";
                    $services = $this->_em->getRepository(Service::class)
                        ->getServiceIdsWhereSynchroExecActive($orgSynchro['organisme']);

                    if (is_array($services) && !empty($services)) {
                        $conditionExec[$orgSynchro['organisme']] .= " AND (a.serviceId is null OR a.serviceId in ('"
                            . implode("','", (new Utils())->flatArray($services)) . "'))";
                    } else {
                        $conditionExec[$orgSynchro['organisme']] .= " AND a.serviceId is null ";
                    }
                }
            }
            if (!empty($conditionExec)) {
                $queryBuilder->andWhere(implode('OR', $conditionExec));
            }
        }
    }

    private function addFilterOnAllowedServices(QueryBuilder $queryBuilder, array $orgServicesAllowed = [])
    {
        if (!empty($orgServicesAllowed)) {
            $condition = [];
            foreach ($orgServicesAllowed as $key => $services) {
                $condition[$key] = " (  a.organisme = '{$key}'";
                if (!in_array(0, $services)) {
                    $condition[$key] .= " AND a.service in ('" . implode("','", $services) . "')";
                }
                $condition[$key] .= ')';
            }
            $queryBuilder->andWhere('( ' . implode(' OR ', $condition) . ')');
        }
    }

    public function loadUserByUsername($username)
    {
        return $this->loadUserByIdentifier($username);
    }

    public function getAgentForIdExterne($idAgentExterne, $idServiceExterne)
    {
        $queryBuilder = $this->createQueryBuilder('a')
            ->select('a')
            ->leftJoin('a.service', 's', 'a.service_id = s.id')
            ->where('a.idExterne = :idAgentExterne')
            ->andwhere('s.idExterne = :idServiceExterne');

        $parameters = [
            'idAgentExterne' => $idAgentExterne,
            'idServiceExterne' => $idServiceExterne,
        ];
        $queryBuilder->setParameters($parameters);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function findAllByIdentifierNotIn($notIn, $limit)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('a')
            ->from($this->getEntityName(), 'a')
            ->setMaxResults($limit)
        ;

        if (!empty($notIn)) {
            $qb->where(
                $qb->expr()->notIn('a.id', $notIn)
            );
        }

        return $qb
            ->getQuery()
            ->getResult();
    }

    public function findAllLogins()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('a.login')
            ->from($this->getEntityName(), 'a')
        ;

        $result = $qb
            ->getQuery()
            ->getResult();

        return array_column($result, 'login');
    }

    public function countByIdentifierNotIn(array $notIn)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('COUNT(a.id)')
            ->from($this->getEntityName(), 'a')
        ;

        if (!empty($notIn)) {
            $qb->where(
                $qb->expr()->notIn('a.id', $notIn)
            );
        }

        return $qb
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function setEm($em)
    {
        $this->_em = $em;
    }
}
