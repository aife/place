<?php

namespace App\Repository;

use App\Entity\Responsableengagement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * ResponsableengagementRepository
 */
class ResponsableengagementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Responsableengagement::class);
    }

    /**
     * LEZ TODO.
     */
    public function deleteResponsableByIdEntreprise($idEntreprise)
    {
        $qb = $this->createQueryBuilder('r');
        $qb->delete($this->_entityName, 'r');
        $qb->where('r.entrepriseId = :idEntreprise');
        $qb->setParameter('idEntreprise', $idEntreprise);
        $q = $qb->getQuery();
        $q->execute();
    }
}
