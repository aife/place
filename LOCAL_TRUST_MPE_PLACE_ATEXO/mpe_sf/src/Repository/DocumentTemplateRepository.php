<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Repository;

use App\Entity\DocumentTemplate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class DocumentTemplateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DocumentTemplate::class);
    }

    public function findAllTemplate(): array
    {
        $qb = $this->createQueryBuilder('d', 'd.document')
            ->select('d.nom', 'd.nomAfficher', 'd.document')
            ->where('d.isCustom = :isCustom')
            ->setParameter('isCustom', false)
            ->orderBy('d.document', 'ASC')
        ;

        return $qb->getQuery()->getArrayResult();
    }
}
