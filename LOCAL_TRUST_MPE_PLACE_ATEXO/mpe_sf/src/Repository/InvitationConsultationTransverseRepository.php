<?php

namespace App\Repository;

use App\Entity\ContratTitulaire;
use App\Entity\InvitationConsultationTransverse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * InvitationConsultationTransverseRepository.
 */
class InvitationConsultationTransverseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InvitationConsultationTransverse::class);
    }

    /**
     * @param  $idContratTitulaire
     *
     * @return array
     */
    public function findIfContratExists($idContratTitulaire)
    {
        return $this->createQueryBuilder('i')
            ->select('o.id, o.acronyme, o.denominationOrg')
            ->leftJoin('App:Organisme', 'o', 'WITH', 'o.acronyme = i.organismeInvite')
            ->where('i.idContratTitulaire = :idContratTitulaire')
            ->setParameter('idContratTitulaire', $idContratTitulaire)
            ->getQuery()
            ->getResult();
    }

    public function findContratsByOrganismInvited(string $organism, string $contratStatus): array
    {
        $contratIds = $this->createQueryBuilder('i')
            ->select('i.idContratTitulaire, c.idContratMulti')
            ->innerJoin(
                ContratTitulaire::class,
                'c',
                Join::WITH,
                'i.idContratTitulaire = c.idContratTitulaire'
            )->andWhere('i.organismeInvite = :organism')
            ->andWhere('c.statutContrat = :status')
            ->setParameter('organism', $organism)
            ->setParameter('status', $contratStatus)
            ->getQuery()->getArrayResult();

        return array_filter(
            array_unique(
                array_merge(
                    array_column($contratIds, 'idContratTitulaire'),
                    array_column($contratIds, 'idContratMulti')
                )
            )
        );
    }
}
