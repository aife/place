<?php

namespace App\Repository;

use App\Entity\ConfigurationOrganisme;
use App\Entity\ContactContrat;
use App\Entity\ContratTitulaire;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\Service;
use App\Utils\Utils;
use Doctrine\ORM\EntityRepository;

/**
 * Repository DataSgmapRepository.
 *
 * @author     Oumar KONATE <oumar.konate@atexo.com>
 * @version    0.0
 * @since      0.0
 * @copyright  Atexo 2015
 */
class EtablissementRepository extends EntityRepository
{
    public string $headWs = 'etablissements';

    public function updateEtablissement($etablissement)
    {
        if ($etablissement instanceof Etablissement) {
            $this->_em->persist($etablissement);
            $this->_em->flush();
        }
    }

    public function getEtablissementByCodeAndIdEntreprise($code, $idEntreprise)
    {
        return $this->findOneBy(['codeEtablissement' => $code, 'idEntreprise' => $idEntreprise]);
    }

    public function getEtablissementSiegeByIdEntreprise($idEntreprise)
    {
        return $this->findOneBy(['estSiege' => '1', 'idEntreprise' => $idEntreprise]);
    }

    public function findAllByParamWs(
        $nombreElementsParPage,
        $NumeroPage,
        $dateModification,
        $module,
        $orgServicesAllowed = [],
        $tabParams = [],
        $searchId = false
    ) {
        $queryBuilder = $this->createQueryBuilder('e')
            ->distinct()
            ->where('e.dateModification >= :dateModification ')
            ->setParameter('dateModification', $dateModification)
            ->orderBy('e.dateModification', 'DESC');

        if ('EXEC' === $module) {
            $listOrgs = $this->_em->getRepository(ConfigurationOrganisme::class)
                ->getOrganismeAcronymesWhereSynchroExecActive();
            $conditionExec = [];
            foreach ($listOrgs as $orgSynchro) {
                if (isset($orgSynchro['organisme'])) {
                    $conditionExec[$orgSynchro['organisme']] = " ct.organisme = '" . $orgSynchro['organisme'] . "'";
                    $services = $this->_em->getRepository(Service::class)
                        ->getServiceIdsWhereSynchroExecActive($orgSynchro['organisme']);

                    if (is_array($services) && !empty($services)) {
                        $conditionExec[$orgSynchro['organisme']] .= " AND ct.serviceId in ('" .
                            implode("','", (new Utils())->flatArray($services)) . "')";
                        $conditionExec[$orgSynchro['organisme']] .= " or ct.serviceId is null ";
                    }
                }
            }
            if (!empty($conditionExec)) {
                $queryBuilder->join(Entreprise::class, 'en', 'WITH', 'en.id = e.entreprise');
                $queryBuilder->join(ContactContrat::class, 'cc', 'WITH', 'cc.idEntreprise = en.id');
                $queryBuilder->join(ContratTitulaire::class, 'ct', 'WITH', 'ct.idContactContrat = cc.idContactContrat');
                $queryBuilder->where('ct.dateModification >= :dateModification');
                $queryBuilder->setParameter('dateModification', $dateModification);
                $queryBuilder->andWhere(implode('OR', $conditionExec));
            }
        }

        $limit = $nombreElementsParPage;
        $offset = $this->getOffset($NumeroPage, $nombreElementsParPage);
        $queryBuilder->setFirstResult($offset);
        $queryBuilder->setMaxResults($limit);
        $etablissements = $queryBuilder->getQuery()->getResult();
        foreach ($etablissements as $etablissement) {
            yield $etablissement;
            $this->_em->detach($etablissement);
        }
    }

    public function getOffset($page, $limit)
    {
        $offset = 0;
        if (0 != $page && 1 != $page) {
            $offset = ($page - 1) * $limit;
        }

        return $offset;
    }

    /**
     * Cette méthode doit être supprimée une fois que tous les mails sont envoyés.
     *
     * @param int $offset
     * @param int $limit
     *
     * @return array
     */
    public function findAccountsEstablishmentsAndSiret($offset, $limit)
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->select('CONCAT(en.siren, e.codeEtablissement) AS siret, i.email, i.id')
            ->innerJoin('e.inscrits', 'i')
            ->innerJoin('e.entreprise', 'en')
            ->where('i.id >:id ')
            ->andWhere('i.profil =:admin')
            ->andWhere('i.bloque = \'0\'')
            ->setParameter('admin', 2)
            ->setParameter('id', $offset)
            ->setMaxResults($limit)
        ;

        return $queryBuilder->getQuery()->getArrayResult();
    }

}
