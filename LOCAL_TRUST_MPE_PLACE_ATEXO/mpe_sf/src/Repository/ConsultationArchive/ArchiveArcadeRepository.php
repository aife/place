<?php

namespace App\Repository\ConsultationArchive;

use App\Entity\ConsultationArchive\ArchiveArcade;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ArchiveArcadeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ArchiveArcade::class);
    }
}
