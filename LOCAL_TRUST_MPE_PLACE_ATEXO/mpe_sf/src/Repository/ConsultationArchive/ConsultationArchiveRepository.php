<?php

namespace App\Repository\ConsultationArchive;

use App\Entity\ConsultationArchive\ConsultationArchive;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ConsultationArchiveRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConsultationArchive::class);
    }

    public function findByStatusNonFragmente($limit = 1000)
    {
        $qb = $this->createQueryBuilder('c');
        $result = $qb
            ->where($qb->expr()->isNotNull('c.cheminFichier'))
            ->andWhere('c.statusFragmentation = false')
            ->andWhere('c.statusGlobalTransmission = \'NON_DEMARREE\'')
            ->setMaxResults($limit)
        ;

        return $result->getQuery()->getResult();
    }
}
