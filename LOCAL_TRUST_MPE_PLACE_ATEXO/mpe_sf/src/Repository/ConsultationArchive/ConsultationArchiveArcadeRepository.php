<?php

namespace App\Repository\ConsultationArchive;

use App\Entity\ConsultationArchive\ConsultationArchiveArcade;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ConsultationArchiveArcadeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConsultationArchiveArcade::class);
    }
}
