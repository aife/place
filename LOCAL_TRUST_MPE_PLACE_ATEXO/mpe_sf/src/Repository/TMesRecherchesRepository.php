<?php

namespace App\Repository;

use App\Entity\Inscrit;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

class TMesRecherchesRepository extends EntityRepository
{
    public final const TYPE_CREATEUR_AGENT = 'AGENT';
    public final const TYPE_CREATEUR_ENTREPRISE = 'ENTREPRISE';

    /**
     * @param int $idCreateur
     * @param string $typeCreateur
     * @param string $typeAvisConsultation
     * @param int $maxResults
     * @param bool $forMenu
     * @param bool $alertesOnly
     * @param bool $typeAvisCons
     * @return array|float|int|mixed[]|string
     */
    public function retreiveRecherches(
        int $idCreateur,
        string $typeCreateur,
        string $typeAvisConsultation,
        int $maxResults,
        bool $forMenu = false,
        bool $alertesOnly = false,
        bool $typeAvisCons = true
    ) {
        $qb = $this->createQueryBuilder('tmrr');

        $query = $qb->where('tmrr.idCreateur = :idCreateur')->setParameter('idCreateur', $idCreateur);
        $query = $query->andWhere('tmrr.typeCreateur = :typeCreateur')
            ->setParameter('typeCreateur', $typeCreateur);

        if ($alertesOnly) {
            $query = $query->andWhere('tmrr.alerte = :alerte')->setParameter('alerte', '1');
        }

        $comparateur = '!=';
        if ($typeAvisCons) {
            $comparateur = '=';
        }

        $query = $query->andWhere('tmrr.typeAvis ' . $comparateur . ' :typeAvis')
            ->setParameter('typeAvis', $typeAvisConsultation);

        if ($forMenu) {
            $query = $query->orderBy('tmrr.dateModification', 'desc');
            $query = $query->setMaxResults($maxResults);
        }

        return $query->getQuery()->getArrayResult();
    }

    public function findAllWithLimit(int $offset, int $limit): array
    {
        $qb = $this->createQueryBuilder('tmrr')
            ->andWhere('tmrr.xmlCriteria NOT LIKE \'%Application\\\\Service%\'')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
        ;

        return $qb->getQuery()->getResult();
    }

    public function findSearchCriteriaByTypeCreateur(string $type): array
    {
        return $this->createQueryBuilder('c')
            ->where('c.typeCreateur =:type')
            ->andWhere('c.alerte = \'1\'')
            ->setParameter('type', $type)
            ->groupBy('c.hashedCriteria')
            ->getQuery()->getResult();
    }

    public function findAlertsEmailsInscritsByHashedCriteria(string $hashedCriteria): array
    {
        return $this->createQueryBuilder('c')
            ->select('c.denomination, c.criteria, c.platformeVirtuelleId, i.email, c.typeAvis')
            ->innerJoin(Inscrit::class, 'i', Join::WITH, 'c.idCreateur = i.id')
            ->where('c.hashedCriteria =:hashedCriteria')
            ->andWhere('c.alerte = \'1\'')
            ->andWhere('i.bloque <> \'1\'')
            ->setParameter('hashedCriteria', $hashedCriteria)
            ->getQuery()->getScalarResult();
    }

    /**
     * @param int $idCreateur
     * @param string $typeCreateur
     * @param string $typeAvisConsultation
     * @param int $maxResults
     * @param bool $forMenu
     * @param bool $alertesOnly
     * @return float|int|mixed|string
     */
    public function displayRechercheSauvegardees(
        int $idCreateur,
        string $typeCreateur,
        string $typeAvisConsultation,
        int $maxResults,
        bool $forMenu = false,
        bool $alertesOnly = false
    ) {
        $qb = $this->createQueryBuilder('p');

        $query = $qb
            ->where('p.idCreateur = :idCreateur')
            ->setParameter('idCreateur', $idCreateur)
            ->andWhere('p.typeCreateur = :typeCreateur')
            ->setParameter('typeCreateur', $typeCreateur)
            ->andWhere('p.typeAvis =:typeAvis')
            ->setParameter('typeAvis', $typeAvisConsultation)
        ;

        if ($alertesOnly) {
            $query = $query->andWhere('p.alerte = :alerte')->setParameter('alerte', '1');
        }

        $query = $query->andWhere('p.typeAvis =:typeAvis')
            ->setParameter('typeAvis', $typeAvisConsultation);

        if ($forMenu) {
            $query = $query->orderBy('p.dateModification', 'desc');
            $query = $query->setMaxResults($maxResults);
        }

        return $query->getQuery()->getResult();
    }
}
