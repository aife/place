<?php

namespace App\Repository;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use DateTime;
use Exception;
use Doctrine\DBAL\Exception as DBALException;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\InterfaceSuivi;
use App\Entity\PanierEntreprise;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\Telechargement;
use App\Model\ConsultationsEnCours;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr\Join;

/**
 * @extends ServiceEntityRepository<Consultation>
 *
 * @method Consultation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Consultation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Consultation[]    findAll()
 * @method Consultation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConsultationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Consultation::class);
    }

    /**
     * @param $id
     *
     * @author Grégory CHEVRET <gregory.chevret@atexo.com>
     */
    public function findOneByReference($id): Consultation
    {
        $qb = $this->createQueryBuilder('c')
            ->where('c.id = :id')->setParameter('id', $id)
            ->join('c.organisme', 'o')->addSelect('o')
            ->join('c.service', 's', 'WITH', 'c.service_id = s.id AND c.organisme = s.organisme')->addSelect('s');

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Permet de récupérer une unique consultation à partir de critères
     * Aide à la gestion de la migration de reference vers id.
     *
     * @param $reference  id ou reference de la consultation
     * @param $typeProcedureRestreinte valeur du paramètre TYPE_PROCEDURE_RESTREINTE
     * @param $acronyme acronyme de l'organisme
     * @param  $code                    code d'accès à la procédure (dans le cas d'une procédure
     *                                  restreinte
     *
     * @return mixed|null
     */
    public function getMappedConsultation($reference, $typeProcedureRestreinte, $acronyme = null, $code = null)
    {
        $qb = $this->createQueryBuilder('c')
            ->where('c.id = :id or c.reference = :id')
            ->setParameter('id', $reference);

        if (!empty($acronyme)) {
            $qb->andWhere('c.organisme = :acronyme')->setParameter('acronyme', $acronyme);
        }

        if (!empty($code)) {
            $qb->andWhere('c.codeProcedure = :code')->setParameter('code', $code);
            $qb->andWhere('c.typeAcces = :typeAcces')->setParameter('typeAcces', $typeProcedureRestreinte);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $idEtatConsultation
     * @param int $offset
     * @param int $limit
     *
     * @return array
     */
    public function findByEtatConsultation($idEtatConsultation, $offset = 0, $limit = 10)
    {
        $qb = $this->createQueryBuilder('c')
            ->where('c.idEtatConsultation = :idEtatConsultation')
            ->setParameter('idEtatConsultation', $idEtatConsultation)
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @param $idEtatConsultation
     *
     * @return int
     */
    public function countConsultationsArchivees($idEtatConsultation)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('count(c)')
            ->from('App:Consultation', 'c')
            ->where('c.idEtatConsultation = :idEtatConsultation')
            ->setParameter('idEtatConsultation', $idEtatConsultation);

        $s = $qb->getQuery()->getScalarResult();

        return (int) $s[0][1];
    }

    /**
     * @param $id
     *
     * @return Consultation|null
     *
     * @author Grégory CHEVRET <gregory.chevret@atexo.com>
     */
    public function getConsultationForDepot($id, array $orgs = [])
    {
        $qb = $this->createQueryBuilder('c')
            ->join('c.organisme', 'o')->addSelect('o')
            ->leftJoin('c.service', 's', 'WITH', 'c.organisme = s.organisme')
            ->addSelect('s')
            ->where('c.id = :id')->setParameter('id', $id)
            ->andWhere('c.dateMiseEnLigneCalcule <= now()')
            ->andWhere('c.dateMiseEnLigneCalcule != :dateMiseEnLigneCalcule')
            ->setParameter('dateMiseEnLigneCalcule', "0000-00-00 00:00:00")
            ->andWhere('c.dateFinUnix >= unix_timestamp()')
            ->andWhere("c.consultationAnnulee = '0'")
            ->andWhere('c.idTypeAvis = 3');

        if (!empty($orgs)) {
            $qb->andWhere('o.acronyme in (:acronymes)')
                ->setParameter('acronymes', $orgs);
        }

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param $id
     * @return int|mixed|string|null
     * @throws NonUniqueResultException
     */
    public function getConsultationAvis(int $id, int $idTypeAvis, array $orgs = [])
    {
        $qb = $this->createQueryBuilder('c')
            ->join('c.organisme', 'o')->addSelect('o')
            ->leftJoin('c.service', 's', 'WITH', 'c.organisme = s.organisme')->addSelect('s')
            ->where('c.id = :id')->setParameter('id', $id)
            ->andWhere('c.dateMiseEnLigneCalcule <= now()')
            ->andWhere('c.dateFinUnix >= unix_timestamp()')
            ->andWhere("c.consultationAnnulee = '0'")
            ->andWhere('c.idTypeAvis = :idTypeAvis')
            ->setParameter('idTypeAvis', $idTypeAvis);
        ;

        if (!empty($orgs)) {
            $qb->andWhere('o.acronyme in (:acronymes)')
                ->setParameter('acronymes', $orgs);
        }

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @return int|mixed
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>*/
    public function findPublishedConsultations(DateTime $datemin, DateTime $datemax)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder()
            ->select('count(c)')
            ->from('App:Consultation', 'c');
        $qb->innerJoin('App:Organisme', 'o', 'WITH', 'o.acronyme = c.organisme');
        $qb->where('o.active = 1');
        $qb->andWhere('c.avisType = :avistype')->setParameter('avistype', '3');
        $qb->andWhere('c.consultationExterne = :consultationExterne')
            ->setParameter('consultationExterne', '0');
        $qb->andWhere('c.dateMiseEnLigneCalcule BETWEEN :datemin AND :datemax')
            ->setParameter('datemin', $datemin->format('Y-m-d'))
            ->setParameter('datemax', $datemax->format('Y-m-d'));
        $qb->andWhere('c.dateMiseEnLigneCalcule != :datezero')
            ->setParameter('datezero', '0000-00-00 00:00:00');
        $qb->andWhere('c.dateMiseEnLigneCalcule IS NOT NULL');
        $qb->andWhere('c.dateMiseEnLigneCalcule < c.datefin');

        $publish_consultation = $qb->getQuery()->getSingleScalarResult();

        if (empty($publish_consultation)) {
            $publish_consultation = 0;
        }

        return $publish_consultation;
    }

    /**
     * @return int
     *
     * @throws DBALException
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    public function findPublishedLots(DateTime $datemin, DateTime $datemax)
    {
        $em = $this->getEntityManager();

        $datemin_loc = $datemin->format('Y-m-d H:i:s');
        $datemax_loc = $datemax->format('Y-m-d H:i:s');

        $sql = "SELECT (Consultation+Lot) as total 
                FROM (
                    SELECT (
                        SELECT count(*) 
                        FROM consultation 
                        WHERE id_type_avis='3' 
                        AND consultation_externe = '0'
                        AND organisme IN (
                            SELECT acronyme FROM Organisme WHERE active=1
                        ) 
                        AND alloti='0' 
                        AND date_mise_en_ligne_calcule >= '$datemin_loc' 
                        AND date_mise_en_ligne_calcule < '$datemax_loc'
                    ) as Consultation, 
                    ( 
                        SELECT count(*) 
                        FROM consultation, CategorieLot 
                        WHERE id_type_avis='3' 
                        AND consultation_externe = '0'
                        AND consultation.organisme IN (
                            SELECT acronyme FROM Organisme WHERE active=1
                        ) 
                        AND alloti='1' 
                        AND consultation.id=consultation_id 
                        AND consultation.organisme=CategorieLot.organisme 
                        AND date_mise_en_ligne_calcule >= '$datemin_loc' 
                        AND date_mise_en_ligne_calcule < '$datemax_loc'
                    ) as Lot
                ) t";
        $q = $em->getConnection();
        $publish_lot = $q->executeQuery($sql)->fetchAllAssociative();

        return $publish_lot[0]['total'];
    }

    public function findOnlinePublicConsultations(DateTime $datemin, DateTime $datemax)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery(
            'SELECT count(c) as total
              FROM App:Consultation c 
              WHERE c.typeAcces = :typeacces
              AND c.avisType = :avistype
              AND c.consultationExterne = :consultationExterne
              AND c.organisme IN (
                  SELECT o.acronyme FROM App:Organisme o WHERE o.active = 1
              )
              AND c.dateMiseEnLigneCalcule < c.datefin 
              AND c.dateMiseEnLigneCalcule != \'0000-00-00 00:00:00\' 
              AND c.dateMiseEnLigneCalcule is not null 
              AND c.datefin >= :datemin
              AND c.dateMiseEnLigneCalcule < :datemax'
        )
            ->setParameter('typeacces', '1')
            ->setParameter('avistype', '3')
            ->setParameter('consultationExterne', '0')
            ->setParameter('datemin', $datemin->format('Y-m-d H:i:s'))
            ->setParameter('datemax', $datemax->format('Y-m-d H:i:s'));

        $public_consultations_online = $query->getSingleScalarResult();

        if (empty($public_consultations_online)) {
            $public_consultations_online = 0;
        }

        return $public_consultations_online;
    }

    /**
     * @param DateTime $datemin
     * @param DateTime $datemax
     *
     * @return int
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>*/
    public function findOnlineConsultationWithDossierVolumineux($id)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery(
            'SELECT count(c) as total FROM App:Consultation c 
              WHERE c.dateMiseEnLigneCalcule < c.datefin 
              AND c.dateMiseEnLigneCalcule != \'0000-00-00 00:00:00\' 
              AND c.dateMiseEnLigneCalcule is not null 
              AND c.datefin > NOW()
              AND c.dossierVolumineux = :id'
        )->setParameter('id', $id);

        $public_consultations_online = $query->getSingleScalarResult();

        if (empty($public_consultations_online)) {
            $public_consultations_online = 0;
        }

        return $public_consultations_online;
    }

    /**
     * @return int
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    public function findOnlineRestrictedConsultations(DateTime $datemin, DateTime $datemax)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery(
            'SELECT count(c) as total
              FROM App:Consultation c 
              WHERE c.typeAcces = :typeacces
              AND c.avisType = :avistype
              AND c.consultationExterne = :consultationExterne
              AND c.organisme IN (
                  SELECT o.acronyme FROM App:Organisme o WHERE o.active = 1
              )
              AND c.dateMiseEnLigneCalcule < c.datefin 
              AND c.dateMiseEnLigneCalcule != \'0000-00-00 00:00:00\' 
              AND c.dateMiseEnLigneCalcule is not null 
              AND c.datefin >= :datemin
              AND c.dateMiseEnLigneCalcule < :datemax'
        )
            ->setParameter('typeacces', '2')
            ->setParameter('avistype', '3')
            ->setParameter('consultationExterne', '0')
            ->setParameter('datemin', $datemin->format('Y-m-d H:i:s'))
            ->setParameter('datemax', $datemax->format('Y-m-d H:i:s'));

        $restraint_consultations_online = $query->getSingleScalarResult();

        if (empty($restraint_consultations_online)) {
            $restraint_consultations_online = 0;
        }

        return $restraint_consultations_online;
    }

    /**
     * @return int
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    public function findClosedConsultations(DateTime $datemin, DateTime $datemax)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder()
            ->select('count(c)')
            ->from('App:Consultation', 'c');
        $qb->innerJoin('App:Organisme', 'o', 'WITH', 'o.acronyme = c.organisme');
        $qb->where('o.active = 1');
        $qb->andWhere('c.avisType = :avistype')->setParameter('avistype', '3');
        $qb->andWhere('c.consultationExterne = :consultationExterne')
            ->setParameter('consultationExterne', '0');
        $qb->andWhere('c.datefin BETWEEN :datemin AND :datemax')
            ->setParameter('datemin', $datemin->format('Y-m-d'))
            ->setParameter('datemax', $datemax->format('Y-m-d'));
        $qb->andWhere('c.dateMiseEnLigneCalcule != :datezero')
            ->setParameter('datezero', '0000-00-00 00:00:00');
        $qb->andWhere('c.dateMiseEnLigneCalcule IS NOT NULL');
        $qb->andWhere('c.dateMiseEnLigneCalcule < c.datefin');

        $close_consultations = $qb->getQuery()->getSingleScalarResult();

        if (empty($close_consultations)) {
            $close_consultations = 0;
        }

        return $close_consultations;
    }

    /**
     * @param $id
     * @param $organisme
     *
     * @return Consultation
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     */
    public function findOneByReferenceAnOrganisme($id, $organisme)
    {
        return $this->findOneBy(['id' => $id, 'organisme' => $organisme]);
    }

    /**
     * Récupération des consultation en cours pour un AGENT (fonction à mettre à jour avec les habillitations).
     *
     * @return ConsultationsEnCours
     *
     * @throws Exception
     */
    public function getConsultationsEnCours(Agent $agent)
    {
        $queryBuilder = $this->createQueryBuilder('c')
            ->select('c')
            ->where('c.organisme = :organisme')
            ->andWhere('c.serviceId = :serviceId')
            ->setParameter('organisme', $agent->getOrganisme())
            ->setParameter('serviceId', $agent->getServiceId());

        $consultations = $queryBuilder->getQuery()->getResult();

        $dateNow = new DateTime();
        $consultationsEnCours = new ConsultationsEnCours();

        foreach ($consultations as $consultation) {
            /**
             * @var Consultation $consultation
             */
            $dateFin = $consultation->getDatefin();

            if ($dateFin->getTimestamp() < 0) {
                continue;
            }

            $dateMiseEnLigneCalcule = $consultation->getDateMiseEnLigneCalcule();
            $reference = $consultation->getReferenceUtilisateur();

            $idEtatConsultation = $consultation->getIdEtatConsultation();
            if (
                ($dateFin > $dateNow || $dateFin->getTimestamp() < 0)
                && ((!$dateMiseEnLigneCalcule
                        || $dateMiseEnLigneCalcule->getTimestamp() < 0)
                    || $dateMiseEnLigneCalcule > $dateNow)
            ) {
                if ($consultation->getEtatEnAttenteValidation()) {
                    ++$consultationsEnCours->attenteValidation;
                } else {
                    ++$consultationsEnCours->elaboration;
                }
            } elseif (
                $dateFin > $dateNow
                && $dateMiseEnLigneCalcule
                && $dateMiseEnLigneCalcule->getTimestamp() > 0
                && $dateMiseEnLigneCalcule <= $dateNow
            ) {
                ++$consultationsEnCours->consultation;
            } elseif ($dateFin <= $dateNow && !$idEtatConsultation) {
                ++$consultationsEnCours->ouvertureAnalyse;
            } else {
                ++$consultationsEnCours->decision;
            }
        }

        return $consultationsEnCours;
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public function findListReprise($params = [])
    {
        $queryBuilder = $this->createQueryBuilder('c')
            ->select('c.idCreateur, c.id');

        if (array_key_exists('idEtatConsultation', $params)) {
            $queryBuilder->where('c.idEtatConsultation < :idEtatConsultation')
            ->setParameter('idEtatConsultation', $params['idEtatConsultation']);
        }

        if (array_key_exists('dateDebut', $params)) {
            $queryBuilder->andWhere('c.datedebut != :dateDebut')
                ->setParameter('dateDebut', $params['dateDebut']);
        }

        if (array_key_exists('datefin', $params)) {
            $queryBuilder->andWhere('c.datefin > :dateFin')
                ->setParameter('dateFin', $params['datefin']);
        }

        if (array_key_exists('service', $params)) {
            $queryBuilder->andWhere('c.serviceId = :service')
                ->setParameter('service', $params['service']);
        }

        if (array_key_exists('acronymeOrg', $params)) {
            $queryBuilder->andWhere('c.acronymeOrg = :acronymeOrg')
                ->setParameter('acronymeOrg', $params['acronymeOrg']);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * A refaire en DQL lorsque les entités seront liées.
     *
     * @throws DBALException
     */
    public function findContactForConsultationContract(int $idContrat): array
    {
        $sql = <<<EOD
  SELECT
      DISTINCT 
    "Destinataires issus d'une phase précédente" AS type,
    contact.id_contact_contrat AS idContactDest,
    contact.id_entreprise AS idEntrepriseDest, 
    contact.email AS mailContactDestinataire,
    CONCAT( contact.nom, ' ', contact.prenom ) AS nomContactDest,
    entreprise.nom AS nomEntrepriseDest
  FROM consultation
  INNER JOIN t_contrat_titulaire as contrat on consultation.id_contrat = contrat.id_contrat_titulaire
  INNER JOIN t_contact_contrat as contact on contrat.id_contact_contrat = contact.id_contact_contrat
  INNER JOIN Entreprise as entreprise on contact.id_entreprise = entreprise.id
  WHERE contrat.id_contrat_titulaire = :id
EOD;
        $em = $this->getEntityManager();

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue(':id', $idContrat);

        return $stmt->executeQuery()->fetchAllAssociative();
    }

    /**
     * @throws DBALException
     */
    public function findContactForConsultationContractMultiAttributaire(int $idContrat): array
    {
        $sql = <<<EOD
  SELECT
      DISTINCT
    "Destinataires issus d'une phase précédente" AS type,
    contact.id_contact_contrat AS idContactDest,
    contact.id_entreprise AS idEntrepriseDest, 
    contact.email AS mailContactDestinataire,
    CONCAT( contact.nom, ' ', contact.prenom ) AS nomContactDest,
    entreprise.nom AS nomEntrepriseDest
  FROM consultation
  INNER JOIN t_contrat_titulaire as contrat on consultation.id_contrat = contrat.id_contrat_multi
  INNER JOIN t_contact_contrat as contact on contrat.id_contact_contrat = contact.id_contact_contrat
  INNER JOIN Entreprise as entreprise on contact.id_entreprise = entreprise.id
  WHERE contrat.id_contrat_multi = :id
EOD;
        $em = $this->getEntityManager();

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue(':id', $idContrat);

        return $stmt->executeQuery()->fetchAllAssociative();
    }

    /**
     * @param null $orgs
     *
     * @return int|mixed|string|null
     *
     * @throws NonUniqueResultException
     */
    public function getClosedConsultations(int $consultationId, int $inscritId, $orgs = null)
    {
        $qb = $this->createQueryBuilder('c')
            ->join('c.organisme', 'o')->addSelect('o')
            ->leftJoin('c.service', 's', 'WITH', 'c.organisme = s.organisme')
            ->leftJoin(PanierEntreprise::class, 'p', 'WITH', 'c.id = p.consultationId')
            ->addSelect('s')
            ->where('c.id = :id')->setParameter('id', $consultationId);
        $qb->andWhere('c.dateMiseEnLigneCalcule IS NOT NULL');
        $qb->andWhere('p.idInscrit = :inscrit')
            ->setParameter('inscrit', $inscritId);
        $qb->andWhere('c.dateMiseEnLigneCalcule != :datezero')
           ->setParameter('datezero', '0000-00-00 00:00:00');
        $qb->andWhere('c.datefin < now()');
        $qb->andWhere("c.consultationAnnulee = '0'");
        $qb->andWhere('c.idTypeAvis = 3');

        if (!empty($orgs)) {
            $qb->andWhere('o.acronyme in (:acronymes)')
                ->setParameter('acronymes', $orgs);
        }

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param $categorie
     * @param $typeAvis
     * @param $dateMin
     * @param $dateMax
     *
     * @return int|mixed|string
     */
    public function getNbProcedureParCategorie($categorie, $typeAvis, $dateMin, $dateMax)
    {
        return $this->createQueryBuilder('c')
            ->select(
                'count(c.id) as nombreConsultation, c.idTypeProcedureOrg as id_type_procedure',
                't.idTypeProcedurePortail as id_type_procedure_portail, 
                 t.ao, t.mn, t.pn, t.dc, t.autre, t.mapa, p.idMontantMapa as id_montant_mapa, 
                 p.sad, p.accordCadre as accordcadre'
            )
            ->join('c.organisme', 'o')
            ->leftJoin(
                TypeProcedureOrganisme::class,
                'p',
                'WITH',
                'c.organisme = p.organisme'
            )
            ->leftJoin(
                TypeProcedureOrganisme::class,
                't',
                'WITH',
                'c.idTypeProcedureOrg = t.idTypeProcedure AND c.organisme = t.organisme'
            )
            ->where('o.active = 1')
            ->andWhere('c.categorie = :categorie')
            ->setParameter('categorie', $categorie)
            ->andWhere('c.idTypeAvis= :typeAvis')
            ->setParameter('typeAvis', $typeAvis)
            ->andWhere('c.dateMiseEnLigneCalcule is not null')
            ->andWhere("c.dateMiseEnLigneCalcule NOT LIKE '0000-00-00%'")
            ->andWhere('c.dateMiseEnLigneCalcule >= :dateMin')
            ->andWhere('c.dateMiseEnLigneCalcule <= :dateMax')
            ->setParameter('dateMin', $dateMin . ' 00:00')
            ->setParameter('dateMax', $dateMax . ' 23:59')
            ->groupBy('id_type_procedure')
            ->getQuery()->getResult();
    }

    /**
     * @param $categorie
     * @param $dateMin
     * @param $dateMax
     *
     * @return int|mixed|string
     */
    public function getNbTelechargementParCategorie($categorie, $dateMin, $dateMax)
    {
        return $this->createQueryBuilder('c')
            ->select(
                'count(te.id) as nombreConsultation, c.idTypeProcedureOrg as id_type_procedure',
                't.ao, t.mn, t.pn, t.dc, t.autre, t.mapa, p.idMontantMapa as id_montant_mapa, 
		         p.sad, p.accordCadre as accordcadre'
            )
            ->join('c.organisme', 'o')
            ->leftJoin(
                Telechargement::class,
                'te',
                'WITH',
                'c.id = te.consultation_id AND c.organisme = te.organisme'
            )
            ->leftJoin(
                TypeProcedureOrganisme::class,
                'p',
                'WITH',
                'c.organisme = p.organisme'
            )
            ->leftJoin(
                TypeProcedureOrganisme::class,
                't',
                'WITH',
                'c.idTypeProcedureOrg = t.idTypeProcedure AND c.organisme = t.organisme'
            )
            ->where('o.active = 1')
            ->andWhere('c.categorie = :categorie')
            ->setParameter('categorie', $categorie)
            ->andWhere('c.dateMiseEnLigneCalcule is not null')
            ->andWhere('c.dateMiseEnLigneCalcule >= :dateMin')
            ->andWhere('c.dateMiseEnLigneCalcule <= :dateMax')
            ->setParameter('dateMin', $dateMin . ' 00:00')
            ->setParameter('dateMax', $dateMax . ' 23:59')
            ->groupBy('id_type_procedure')
            ->getQuery()->getResult();
    }

    /**
     * @param $categorie
     * @param $statutOffres
     * @param $dateMin
     * @param $dateMax
     *
     * @return int|mixed|string
     */
    public function getNbReponseElectroniqueParCategorie($categorie, $statutOffres, $dateMin, $dateMax)
    {
        return $this->createQueryBuilder('c')
            ->select('count(f.id) as nombreConsultation,c.idTypeProcedureOrg as id_type_procedure, t.ao, 
                t.mn, t.pn, t.dc, t.autre, t.mapa, p.idMontantMapa as id_montant_mapa, 
                p.sad, p.accordCadre as accordcadre')
            ->join('c.organisme', 'o')
            ->leftJoin('c.offres', 'f')
            ->leftJoin(
                TypeProcedureOrganisme::class,
                'p',
                'WITH',
                'c.organisme = p.organisme'
            )
            ->leftJoin(
                TypeProcedureOrganisme::class,
                't',
                'WITH',
                'c.idTypeProcedureOrg = t.idTypeProcedure AND c.organisme = t.organisme'
            )
            ->where('o.active = 1')
            ->andWhere('c.categorie = :categorie')
            ->setParameter('categorie', $categorie)
            ->andWhere('c.dateMiseEnLigneCalcule is not null')
            ->andWhere('c.dateMiseEnLigneCalcule >= :dateMin')
            ->andWhere('c.dateMiseEnLigneCalcule <= :dateMax')
            ->setParameter('dateMin', $dateMin . ' 00:00')
            ->setParameter('dateMax', $dateMax . ' 23:59')
            ->andWhere('f.statutOffres != :statutOffres')
            ->setParameter('statutOffres', $statutOffres)
            ->groupBy('id_type_procedure')
            ->getQuery()->getResult();
    }

    public function getUnpublishedConsultation()
    {
        return $this->createQueryBuilder('c')
            ->select('c.id, c.codeExterne, isu.uuid')
            ->leftJoin(InterfaceSuivi::class, 'isu', Join::WITH, 'isu.consultation = c.id')
            ->andWhere('c.typeAcces = 1')
            ->andWhere('c.dateFinUnix > unix_timestamp()')
            ->andWhere('c.dateMiseEnLigneCalcule <> \'0000-00-00 00:00:00\'')
            ->andWhere('c.dateMiseEnLigneCalcule IS NOT NULL')
            ->andWhere('c.dateMiseEnLigneCalcule < now()')
            ->andWhere('c.idTypeAvis = :typeAvisConsultation')
            ->andWhere('isu.uuid IS NULL')
            ->setParameter('typeAvisConsultation', Consultation::TYPE_AVIS_CONSULTATION)
            ->getQuery()->getResult();
    }

    public function persistConsultation(Consultation $consultation): void
    {
        $this->getEntityManager()->persist($consultation);
        $this->getEntityManager()->flush();
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getNbConsultationWithSimilarName(string $reference)
    {
        return $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->where('c.referenceUtilisateur LIKE :reference')
            ->setParameter('reference', '%' . $reference . '%')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    public function getDonneesPassation($organisme, $id = null, $lot = null)
    {
        try {
            $sql = <<<SQL
SELECT c.id,
       c.date_mise_en_ligne_calcule,
       c.reference_utilisateur,
       c.id_type_procedure_org,
       Type_Procedure_Organisme.libelle_Type_Procedure                                                                        as procedure_org,
       Type_Procedure_Organisme.mapa,
       CategorieConsultation.libelle                                                                                          as libelle_categorie,
       (SELECT Count(*) FROM CategorieLot WHERE CategorieLot.consultation_id = c.id AND CategorieLot.organisme = c.organisme) as nombre_lot,
       c.categorie,
       c.objet,
       c.lieu_execution,
       c.code_cpv_1,
       c.alloti,
       c.service_id,
       c.service_associe_id,
       c.datefin,
       c.clause_sociale                                                                                                       as clause_sociale,
       c.clause_environnementale                                                                                              as clause_environnementale,
       CategorieLot.lot                                                                                                       as numLot,
       CategorieLot.description,
       CategorieLot.code_cpv_1                                                                                                as cpvLot,
       CategorieLot.clause_sociale                                                                                            as clause_sociale_lot,
       CategorieLot.clause_environnementale                                                                                   as clause_environnementale_lot,
       Passation_consultation.id                                                                                              as idPassation,
       Passation_consultation.unite,
       Fcsp_unite.libelle as unite_fcsp,
       Passation_consultation.mandataire,
       Fcsp_Mandataire.libelle as libelle_mandataire,
       Passation_consultation.numero_deliberation_financiere,
       Passation_consultation.date_deliberation_financiere,
       Passation_consultation.numero_deliberation_autorisant_signature_marche,
       Passation_consultation.date_deliberation_autorisant_signature_marche,
       Passation_consultation.date_notification_previsionnelle,
       Passation_consultation.date_reception_projet_DCE_Service_Validateur,
       Passation_consultation.date_formulations_premieres_observations,
       Passation_consultation.date_retour_projet_DCE_finalise,
       Passation_consultation.date_validation_projet_DCE_par_service_validateur,
       Passation_consultation.date_validation_projet_DCE_vue_par,
       Passation_consultation.date_reception_projet_AAPC_par_Service_Validateur,
       Passation_consultation.date_formulations_premieres_observations_AAPC,
       Passation_consultation.date_retour_projet_AAPC_finalise,
       Passation_consultation.date_validation_projet_AAPC_par_Service_Validateur,
       Passation_consultation.date_validation_projet_AAPC_par_Service_Validateur_vu_par,
       Passation_consultation.date_envoi_publicite,
       Passation_consultation.date_envoi_invitations_remettre_offre,
       Passation_consultation.date_limite_remise_offres,
       Passation_consultation.delai_validite_offres,
       Passation_consultation.date_reunion_ouverture_candidatures,
       Passation_consultation.date_reunion_ouverture_offres,
       passation_marche_a_venir.id                                                                                            as idMarche,
       passation_marche_a_venir.lot,
       passation_marche_a_venir.id_nature_acte_juridique,
       nature_acte_juridique.libelle as libelle_nature_acte_juridique,
       passation_marche_a_venir.forme_groupement,
       passation_marche_a_venir.mode_execution_contrat,
       mode_execution_contrat.libelle as libelle_mode_execution_contrat,
       passation_marche_a_venir.min_bon_commande,
       passation_marche_a_venir.max_bon_commande,
       passation_marche_a_venir.duree_execution_marche_hors_reconduction,
       passation_marche_a_venir.nombre_reconduction,
       passation_marche_a_venir.duree_total_marche,
       passation_marche_a_venir.ccag_applicable,
       ccag_applicable.libelle as libelle_ccag_applicable,
       passation_marche_a_venir.date_reception_analyse_offre,
       passation_marche_a_venir.date_formulation_observation_projet_rapport,
       passation_marche_a_venir.date_retour_projet_rapport_finalise,
       passation_marche_a_venir.date_validation_projet_rapport,
       passation_marche_a_venir.projet_rapport_vu_par,
       passation_marche_a_venir.date_reunion_attribution,
       passation_marche_a_venir.decision,
       DecisionPassationConsultation.libelle as libelle_decision,
       passation_marche_a_venir.date_envoi_courrier_condidat_non_retenu,
       passation_marche_a_venir.date_signature_marche_pa,
       passation_marche_a_venir.date_reception_controle_legalite,
       passation_marche_a_venir.date_formulation_observation_dossier,
       passation_marche_a_venir.date_retour_dossier_finalise,
       passation_marche_a_venir.date_transmission_prefecture,
       passation_marche_a_venir.dossier_vu_par,
       passation_marche_a_venir.date_validation_rapport_information,
       passation_marche_a_venir.montant_estime,
       passation_marche_a_venir.id_passation_consultation,
       DecisionLot.date_decision
FROM consultation c
         LEFT JOIN CategorieLot on (CategorieLot.consultation_id = c.id AND CategorieLot.organisme = c.organisme)
         LEFT JOIN Type_Procedure_Organisme on (Type_Procedure_Organisme.id_type_procedure = c.id_type_procedure_org AND Type_Procedure_Organisme.organisme = c.organisme)
         LEFT JOIN DecisionLot ON (c.id = DecisionLot.consultation_id AND c.organisme = DecisionLot.organisme)
         LEFT JOIN CategorieConsultation on (CategorieConsultation.id = c.categorie)
SQL;

            if ($id) {
                //Si appellé depuis la fonction Marches Conclus créer (une ligne) csv même si passation n'existe pas
                $sql .= <<<SQL
        LEFT JOIN Passation_consultation ON (c.id = Passation_consultation.consultation_id AND c.organisme = Passation_consultation.organisme)
        LEFT JOIN passation_marche_a_venir
                   on (
        passation_marche_a_venir.id_passation_consultation = Passation_consultation.id 
        AND passation_marche_a_venir.organisme = Passation_consultation.organisme
        
SQL;
                if (null !== $lot) {
                    $sql .= ' AND passation_marche_a_venir.lot= :lot ';
                }
                $sql .= ')  ';
                $where = ' WHERE 1 ';
            } else {
                //Si appellé directement (ref null et lot null) ne mettre dans le csv que les passations existantes.
                $sql .= '		 , Passation_consultation '
                    . '		 LEFT JOIN passation_marche_a_venir on (passation_marche_a_venir.id_passation_consultation=Passation_consultation.id AND passation_marche_a_venir.organisme=Passation_consultation.organisme) ';

                $where = "	WHERE c.id=Passation_consultation.consultation_id AND c.organisme=Passation_consultation.organisme AND (passation_marche_a_venir.lot = CategorieLot.lot OR passation_marche_a_venir.lot='0')";
            }
            $sql .= <<<SQL
         LEFT JOIN Fcsp_unite on (Fcsp_unite.id =  Passation_consultation.unite and Passation_consultation.organisme = Fcsp_unite.organisme)
         LEFT JOIN Fcsp_Mandataire on (Fcsp_Mandataire.id =  Passation_consultation.mandataire and Passation_consultation.organisme = Fcsp_Mandataire.organisme)
         LEFT JOIN nature_acte_juridique on (nature_acte_juridique.id =  passation_marche_a_venir.id_nature_acte_juridique )
         LEFT JOIN mode_execution_contrat on (mode_execution_contrat.id =  passation_marche_a_venir.mode_execution_contrat )
         LEFT JOIN ccag_applicable on (ccag_applicable.id =  passation_marche_a_venir.ccag_applicable )
         LEFT JOIN DecisionPassationConsultation on (DecisionPassationConsultation.id =  passation_marche_a_venir.decision )
SQL;

            $sql .= $where;
            if ($id) {
                $sql .= " AND c.id= :id ";
                $parameters['id'] = $id;
                if ($lot) {
                    $sql .= " AND CategorieLot.lot= :lot";
                }
                $parameters['lot'] = $lot;
            }
            $sql .= ' AND DecisionLot.lot= :decisionlot ';
            $parameters['decisionlot'] = '0';
            if (null !== $lot) {
                $parameters['decisionlot'] = $lot;
            }
            $sql .= " AND c.organisme = :org";
            $parameters['org'] = $organisme;
            $rsm = $this->_em->getConnection()->prepare($sql);
            $res = $rsm->executeQuery($parameters);

            return $res->fetchAllAssociative();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getConditionStatesConsultation(string $alias, array $idsEtatConsultation, &$params = [], $condition = "OR")
    {
        $conditions = [];

        if (in_array(Consultation::STATUT_CONSULTATION, $idsEtatConsultation)) {
            $conditions []  = <<<SQL
({$alias}.idEtatConsultation = :EtatConsultationC 
AND {$alias}.dateMiseEnLigneCalcule != '' 
AND {$alias}.dateMiseEnLigneCalcule IS NOT NULL 
AND {$alias}.dateMiseEnLigneCalcule != '0000-00-00 00:00:00'
AND {$alias}.dateMiseEnLigneCalcule<=now())
SQL;
            $params[':EtatConsultationC'] = '0';
        }
        if (in_array(Consultation::STATUT_OA, $idsEtatConsultation)) {
            $conditions [] = <<<SQL
(( {$alias}.idEtatConsultation = :EtatConsultationOA  OR  {$alias}.depouillablePhaseConsultation=:phaseSAD )
    AND ({$alias}.datefin<=now() AND {$alias}.datefin != '0000-00-00 00:00:00') ) 
SQL;
            $params[':EtatConsultationOA'] = '0';
            $params[':phaseSAD'] = '1';
        }

        $sqlState =  <<<SQL
({$alias}.idEtatConsultation IN (:EtatConsultation) )
SQL;

        if (in_array(Consultation::STATUT_DECISION, $idsEtatConsultation)) {
            $params[':EtatConsultation'] [] = Consultation::STATUT_DECISION;
        }
        if (in_array(Consultation::STATUT_A_ARCHIVER, $idsEtatConsultation)) {
            $params[':EtatConsultation'] [] = Consultation::STATUT_A_ARCHIVER;
        }
        if (in_array(Consultation::STATUT_ARCHIVE_REALISEE, $idsEtatConsultation)) {
            $params[':EtatConsultation'] [] = Consultation::STATUT_ARCHIVE_REALISEE;
        }
        if (!empty($params[':EtatConsultation'])) {
            $conditions [] = $sqlState;
        }
        $condition = " " . $condition . " ";
        return "(" . implode($condition, $conditions) . ")";
    }
}
