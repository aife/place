<?php

namespace App\Repository;

use App\Entity\ConfigurationOrganisme;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ConfigurationOrganismeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConfigurationOrganisme::class);
    }

    /**
     * @param $acronyme
     *
     * @return mixed
     */
    public function getByAcronyme($acronyme)
    {
        return $this->findOneByOrganisme($acronyme);
    }

    /**
     * returns only the organizations where module Exec is active.
     *
     * @return int|mixed|string
     */
    public function getOrganismeAcronymesWhereSynchroExecActive()
    {
        $qb = $this->createQueryBuilder('co');
        $qb->select('co.organisme')
            ->where("co.moduleExec = '1'");

        return $qb->getQuery()->getResult();
    }

    /**
     * returns only the organizations where module suiviPassation is active.
     *
     * @return int|mixed|string
     */
    public function getOrganismeAcronymesWhereSuiviPassationActive($org = null)
    {
        $qb = $this->createQueryBuilder('co');
        $qb->select('co.organisme')
            ->where("co.suiviPassation = '1'");
        if (!empty($org)) {
            $qb->andWhere("co.organisme like :org");
            $qb->setParameter("org", $org);
        }
        return $qb->getQuery()->getResult();
    }

    public function setEm($em)
    {
        $this->_em = $em;
    }
}
