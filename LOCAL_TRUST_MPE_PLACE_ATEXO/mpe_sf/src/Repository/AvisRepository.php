<?php

namespace App\Repository;

use App\Entity\Avis;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Avis>
 */
class AvisRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Avis::class);
    }

    /**
     * @param int    $consultationId
     * @param string $organisme
     * @param array  $lots
     *
     * @return mixed
     *
     * @author Fernando Lozano
     */
    public function checkAvis($consultationId, $organisme): array
    {
        $qb = $this->createQueryBuilder('comp')
            ->where('comp.consultationId = :consultationId')->setParameter('consultationId', $consultationId)
            ->andWhere('comp.organisme = :organisme')->setParameter('organisme', $organisme);

        return $qb->getQuery()->getArrayResult();
    }

    public function retrieveListAvisEnvoye($consultationId, $organisme): array
    {
        $qb = $this->createQueryBuilder('av')
            ->where('av.consultationId = :consultationId')->setParameter('consultationId', $consultationId)
            ->andWhere('av.organisme = :organisme')->setParameter('organisme', $organisme)
            ->addOrderBy('av.datePub', 'DESC');

        if ($qb->getQuery()->getArrayResult()) {
            return $qb->getQuery()->getArrayResult();
        } else {
            return false;
        }
    }

    public function getAvisFilesAvailable($consultationId, $organisme)
    {
        $qb = $this->createQueryBuilder('av')
            ->where('av.consultationId = :consultationId')->setParameter('consultationId', $consultationId)
            ->andWhere('av.statut = 2')
            ->andWhere('av.organisme = :organisme')->setParameter('organisme', $organisme)

            ->addOrderBy('av.datePub', 'DESC');

        if ($qb->getQuery()->getArrayResult()) {
            return $qb->getQuery()->getArrayResult();
        } else {
            return false;
        }
    }
}
