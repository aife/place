<?php

namespace App\Repository;

use App\Entity\HistoriquePurge;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<HistoriquePurge>
 *
 * @method HistoriquePurge|null find($id, $lockMode = null, $lockVersion = null)
 * @method HistoriquePurge|null findOneBy(array $criteria, array $orderBy = null)
 * @method HistoriquePurge[]    findAll()
 * @method HistoriquePurge[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HistoriquePurgeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HistoriquePurge::class);
    }

    public function purgeBrouillonFichierEnveloppe($limit)
    {
        $sql = 'Delete *
                From fichierEnveloppe fe
                Left Join historique_purge hp ON hp.id_fichier_enveloppe = fe.id_fichier
                Where hp.state = :state
                And fe.nom_fichier like "%.brouillon"
                LIMIT :limit';

        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);

        $stmt->bindValue(':state', HistoriquePurge::STATE_DELETED);
        $stmt->bindValue(':limit', $limit);
    }

    public function purgeBrouillonDCE($limit)
    {
        $sql = 'Delete *
                From DCE dce
                Left Join historique_purge hp ON hp.id_fichier_enveloppe = dce.id_fichier
                Where hp.state = :state
                And dce.nom_fichier like "%.brouillon"
                LIMIT :limit';

        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);

        $stmt->bindValue(':state', HistoriquePurge::STATE_DELETED);
        $stmt->bindValue(':limit', $limit);
    }
}
