<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Repository\Procedure;

use App\Entity\ProcedureEquivalence;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ProcedureEquivalenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProcedureEquivalence::class);
    }

    public function findAllByIdentifierNotIn($notIn, $limit)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('p')
            ->from($this->getEntityName(), 'p')
            ->setMaxResults($limit)
        ;

        if (!empty($notIn)) {
            $qb->where(
                $qb->expr()->notIn(
                    'CONCAT(p.idTypeProcedure, \';\', p.organisme)',
                    ':notIn'
                )
            )
                ->setParameter('notIn', $notIn);
        }

        return $qb
            ->getQuery()
            ->getResult();
    }

    public function countByIdentifierNotIn(array $notIn): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('COUNT(p)')
            ->from($this->getEntityName(), 'p')
        ;

        if (!empty($notIn)) {
            $qb->where(
                $qb->expr()->notIn(
                    'CONCAT(p.idTypeProcedure, \';\', p.organisme)',
                    ':notIn'
                )
            )
                ->setParameter('notIn', $notIn);
        }

        return $qb
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getMaxId(): int
    {
        $maxId = $this->createQueryBuilder('p')
            ->select('MAX(p.idTypeProcedure)')
            ->getQuery()
            ->getSingleScalarResult();

        return $maxId ?? 0;
    }

    public function setEm($em)
    {
        $this->_em = $em;
    }
}
