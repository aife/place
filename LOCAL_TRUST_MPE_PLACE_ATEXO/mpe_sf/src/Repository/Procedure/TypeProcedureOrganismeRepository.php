<?php

namespace App\Repository\Procedure;

use App\Entity\Agent;
use App\Entity\ConfigurationOrganisme;
use App\Entity\HabilitationTypeProcedure;
use App\Entity\Organisme;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\TypeContrat;
use App\Entity\TypeContratEtTypeProcedure;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Generator;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Security;

class TypeProcedureOrganismeRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private readonly ParameterBagInterface $parameterBag,
        private readonly Security $security
    ) {
        parent::__construct($registry, TypeProcedureOrganisme::class);
    }

    /**
     * @param $atexoUtil
     */
    public function getTypeProcedureDistinctWithCodesOrganisme($atexoUtil, ?string $organisme = null): Generator
    {
        $qb = $this->createQueryBuilder('p')
            ->groupBy('p.idTypeProcedure');
        if (isset($organisme)) {
            $qb->innerJoin(ConfigurationOrganisme::class, 'co', 'WITH', 'p.organisme = co.organisme');
        }
        if (isset($organisme)) {
            $qb->andWhere('p.organisme = :organisme')
                ->setParameter('organisme', $organisme);
        }
        $procedures = $qb->getQuery()
            ->getResult();
        foreach ($procedures as $procedure) {
            if ($procedure instanceof TypeProcedureOrganisme) {
                $procedure->setLibelleTypeProcedure(
                    $atexoUtil->supprimerCodeHtml($procedure->getLibelleTypeProcedure())
                );
                $procedure->setOrganismes(
                    $this->getAllOrganismesTypeProcedure(
                        $procedure->getIdTypeProcedure(),
                        $organisme
                    )
                );
            }
            yield $procedure;
        }
    }

    /**
     * @param $idTypeProcedure
     */
    private function getAllOrganismesTypeProcedure(
        $idTypeProcedure,
        ?string $organisme = null
    ): array {
        $qb = $this->createQueryBuilder('p')
            ->select('p.organisme')
            ->where('p.idTypeProcedure = :id')
            ->setParameter('id', $idTypeProcedure);
        if (isset($organisme)) {
            $qb->innerJoin(ConfigurationOrganisme::class, 'co', 'WITH', 'p.organisme = co.organisme');
        }
        if (isset($organisme)) {
            $qb->andWhere('p.organisme = :organisme')
                ->setParameter('organisme', $organisme);
        }

        return $qb->getQuery()->getArrayResult();
    }

    public function getNewTypeProcedureOrganisme(string $organisme)
    {
        /** @var Agent $agent */
        $agent = $this->security->getUser();
        $qb = $this->createQueryBuilder('tpo')
            ->select('tpo')
            ->join(HabilitationTypeProcedure::class, 'htp', 'WITH', 'htp.typeProcedure = tpo.idTypeProcedure')
            ->where('tpo.organisme = :organisme')
            ->andWhere('htp.agent = :agent')
            ->setParameters([
                'organisme' => $organisme,
                'agent' => $agent
            ])
            ->distinct(true);

        return $qb->getQuery()->getResult();
    }

    /**
     * Retourne les types de procédures simplifiées pour un organisme & type de contrat.
     *
     * @return int|mixed|string
     */
    public function procedureSimplifieesWithOraganismeAndTypeContrat(
        Organisme $organisme,
        TypeContrat $typeContrat,
        Agent $agent = null
    ) {
        $qb = $this->createQueryBuilder('tp')
            ->innerJoin(
                TypeContratEtTypeProcedure::class,
                'tctp',
            );
        $parameters = [
            'simplifie' => true,
            'organisme' => $organisme,
            'typeContrat' => $typeContrat,
        ];
        if ($this->parameterBag->get('ACTIVE_HABILITATION_V2')) {
            $parameters['agent'] = $agent;
            $qb->innerJoin(HabilitationTypeProcedure::class, 'htp');
        }
        $qb->where('tp.procedureSimplifie = :simplifie')
            ->andwhere('tp.organisme = :organisme')
            ->andWhere('tctp.idTypeContrat = :typeContrat');
        if ($this->parameterBag->get('ACTIVE_HABILITATION_V2')) {
            $qb->andWhere('htp.typeProcedure = tp.idTypeProcedure')
                ->andWhere('htp.agent = :agent');
        }

        return $qb->setParameters($parameters)
            ->orderBy('tp.ordreAffichage', 'desc')
            ->getQuery()
            ->getResult();
    }

    public function findAllByIdentifierNotIn($notIn, $limit)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('t')
            ->from($this->getEntityName(), 't')
            ->setMaxResults($limit);

        if (!empty($notIn)) {
            $qb->where(
                $qb->expr()->notIn(
                    'CONCAT(t.idTypeProcedure, \';\', t.organisme)',
                    ':notIn'
                )
            )
                ->setParameter('notIn', $notIn);
        }

        return $qb
            ->getQuery()
            ->getResult();
    }

    public function countByIdentifierNotIn(array $notIn): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('COUNT(t)')
            ->from($this->getEntityName(), 't');

        if (!empty($notIn)) {
            $qb->where(
                $qb->expr()->notIn(
                    'CONCAT(t.idTypeProcedure, \';\', t.organisme)',
                    ':notIn'
                )
            )
                ->setParameter('notIn', $notIn);
        }

        return $qb
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getMaxId(): int
    {
        $maxId = $this->createQueryBuilder('t')
            ->select('MAX(t.idTypeProcedure)')
            ->getQuery()
            ->getSingleScalarResult();

        return $maxId ?? 0;
    }

    public function setEm($em)
    {
        $this->_em = $em;
    }
}
