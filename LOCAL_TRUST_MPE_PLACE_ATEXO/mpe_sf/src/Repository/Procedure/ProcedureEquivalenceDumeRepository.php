<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Repository\Procedure;

use App\Entity\ProcedureEquivalenceDume;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ProcedureEquivalenceDumeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProcedureEquivalenceDume::class);
    }

    public function getIdProcedureDumeByOrganism(int $typeProcedure, string $organism): array
    {
        return array_column($this->createQueryBuilder('p')
            ->select('p.idTypeProcedureDume')
            ->where('p.idTypeProcedure = :typeProcedure')
            ->andWhere('p.organisme = :organism')
            ->andWhere('p.afficher = :display')
            ->setParameter('typeProcedure', $typeProcedure)
            ->setParameter('organism', $organism)
            ->setParameter('display', '1')
            ->getQuery()->getResult(), 'idTypeProcedureDume');
    }

    public function getFrozenIdProcedureDumeByOrganism(int $typeProcedure, string $organism): array
    {
        return array_column($this->createQueryBuilder('p')
            ->select('p.idTypeProcedureDume')
            ->where('p.idTypeProcedure = :typeProcedure')
            ->andWhere('p.organisme = :organism')
            ->andWhere('p.afficher = :display')
            ->andWhere('p.selectionner = :selected')
            ->andWhere('p.figer = :frozen')
            ->setParameter('typeProcedure', $typeProcedure)
            ->setParameter('organism', $organism)
            ->setParameter('display', '1')
            ->setParameter('selected', '1')
            ->setParameter('frozen', '1')
            ->getQuery()->getResult(), 'idTypeProcedureDume');
    }
}
