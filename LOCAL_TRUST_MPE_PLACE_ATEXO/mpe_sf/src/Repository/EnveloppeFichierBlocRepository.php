<?php

namespace App\Repository;

use App\Entity\Consultation;
use App\Entity\HistoriquePurge;
use App\Entity\Offre;
use DateTimeInterface;
use Doctrine\ORM\EntityRepository;

class EnveloppeFichierBlocRepository extends EntityRepository
{
    final public const LIMIT = 100;

    public function getEnveloppeFichierBlocOffreDecryptee(
        array $consultationStatut,
        string $organisme,
        array $stateList,
        DateTimeInterface $dateDechiffrement,
        int $limit,
        int $step = null,
    ): array {
        $params = [
            'statutOffres' => Offre::STATUT_OFFRE_OUVERT,
            'isDeleted' => HistoriquePurge::FILE_NOT_DELETED,
            'state' => $stateList,
            'blobChiffre' => 0,
            'Dechiffrement' => $dateDechiffrement->format('Y-m-d'),
        ];
        $conditionsStatCons =
            $this->getEntityManager()
                ->getRepository(Consultation::class)
                ->getConditionStatesConsultation(
                    'c',
                    $consultationStatut,
                    $params
                );
        $qb = $this
            ->createQueryBuilder('bfe')
            ->leftJoin('bfe.fichierEnveloppe', 'fe')
            ->leftJoin('fe.enveloppe', 'e')
            ->leftJoin('e.offre', 'o')
            ->leftJoin('o.consultation', 'c')
            ->leftJoin(
                HistoriquePurge::class,
                'hp',
                'WITH',
                'bfe.idBlocFichier = hp.idBlocFichierEnveloppe AND bfe.organisme = hp.organisme'
            )
            ->leftJoin('bfe.blobOrganismeChiffre', 'blobOrganismeChiffre')
            ->andWhere('o.statutOffres = :statutOffres')
            ->andWhere($conditionsStatCons)
            ->andWhere('hp.deleted = :isDeleted or hp.deleted is null')
            ->andWhere('hp.state IN (:state) or hp.state is null')
            ->andWhere('blobOrganismeChiffre.id is not null')
            ->andWhere('bfe.idBlobChiffre > :blobChiffre')
            ->andWhere('o.dateHeureOuverture  < :Dechiffrement')
            ->setParameters($params);
        if ($organisme && $organisme !== HistoriquePurge::ORGANISME_ALL) {
            $qb->andWhere('c.organisme = :organisme')->setParameter('organisme', $organisme);
        }
        $qb->setFirstResult($limit); // here we take the parameter limit as the offset or the limit depends on how we call it
        if (!is_null($step)) {
            $qb->setMaxResults($step);
        }

        return $qb->getQuery()->getResult();
    }
}
