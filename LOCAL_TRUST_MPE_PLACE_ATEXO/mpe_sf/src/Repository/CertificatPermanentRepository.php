<?php

namespace App\Repository;

use Doctrine\ORM\NoResultException;
use Doctrine\ORM\NonUniqueResultException;
use App\Entity\CertificatPermanent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Mohamed Wazni.
 *
 * Class CertificatPermanent
 */
class CertificatPermanentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CertificatPermanent::class);
    }

    public function getAllCertificatPermanents(int $idService, string $organisme)
    {
        $criterias = ['organisme' => $organisme, 'serviceId' => $idService];

        return $this->findBy($criterias);
    }

    /**
     * @return CertificatPermanent
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getHighestId(): int
    {
        $highestId = $this->createQueryBuilder('c')
            ->select('MAX(c.id)')
            ->getQuery()
            ->getSingleScalarResult();

        return $highestId ?? 0;
    }

    public function findAllByIdentifierNotIn($notIn, $limit)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('c')
            ->from($this->getEntityName(), 'c')
            ->setMaxResults($limit);

        if (!empty($notIn)) {
            $qb->where(
                $qb->expr()->notIn(
                    'CONCAT(c.id, \';\', c.organisme)',
                    ':notIn'
                )
            )
                ->setParameter('notIn', $notIn);
        }

        return $qb
            ->getQuery()
            ->getResult();
    }

    public function countByIdentifierNotIn(array $notIn): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('COUNT(c)')
            ->from($this->getEntityName(), 'c');

        if (!empty($notIn)) {
            $qb->where(
                $qb->expr()->notIn(
                    'CONCAT(c.id, \';\', c.organisme)',
                    ':notIn'
                )
            )
                ->setParameter('notIn', $notIn);
        }

        return $qb
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getMaxId(): int
    {
        $connection = $this->getEntityManager()->getConnection();
        $stmt = $connection->prepare('SELECT MAX(c.id) FROM CertificatPermanent c;');

        return $stmt->executeQuery()->fetchOne() ?? 0;
    }

    public function setEm($em)
    {
        $this->_em = $em;
    }
}
