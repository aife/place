<?php

namespace App\Repository\Api;

use Doctrine\ORM\EntityRepository;
use Datetime;
use Doctrine\ORM\NonUniqueResultException;
use App\Entity\Api\SupervisionInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SupervisionInterface>
 */
class SupervisionInterfaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SupervisionInterface::class);
    }
    /**
     * @param Datetime $date
     *
     * @return array
     */
    public function getSupervisionInterfaceByDate($date)
    {
        $qb = $this->createQueryBuilder('s')
            ->where('s.createdDate <= :date')
            ->setParameter('date', $date->format('Y-m-d 00:00:00'))
            ->getQuery();

        return $qb->getResult();
    }

    /**
     * @param DateTime $date
     * @param string    $service
     *
     * @return array
     */
    public function getSupervisionInterfaceByDateByService($date, $service)
    {
        $qb = $this->createQueryBuilder('s')
            ->select('s.createdDate', 's.interface', 's.totalOk', 's.service', 's.poids', 's.poidsOk', 's.total')
            ->addSelect('s.totalOk', 's.webserviceBatch')
            ->where('s.createdDate LIKE :date')
            ->setParameter('date', '%' . $date->format('Y-m-d') . '%')
            ->andWhere('s.service = :service')
            ->setParameter('service', $service)
            ->getQuery();

        return $qb->getArrayResult();
    }

    /**
     * @param Datetime $date
     *
     * @return array
     */
    public function getSupervisionInterfaceByDateByNomInterface($date, $interface)
    {
        $qb = $this->createQueryBuilder('s')
            ->where('s.createdDate LIKE :date')
            ->setParameter('date', $date->format('Y-m-d') . '%')
            ->andWhere('s.interface = :interface')
            ->setParameter('interface', $interface)
            ->getQuery();

        return $qb->getArrayResult();
    }

    /**
     * @param $interface
     * @param $webserviceBatch
     *
     * @return mixed
     *
     * @throws NonUniqueResultException
     */
    public function getByDateAndInterface($interface, $service, $webserviceBatch, DateTime $date)
    {
        $qb = $this->createQueryBuilder('s')
            ->where('s.createdDate LIKE :date')
            ->setParameter('date', $date->format('Y-m-d'))
            ->andWhere('s.interface = :interface')
            ->setParameter('interface', $interface)
            ->andWhere('s.service = :service')
            ->setParameter('service', $service)
            ->andWhere('s.webserviceBatch = :webserviceBatch')
            ->setParameter('webserviceBatch', $webserviceBatch)
            ->getQuery();

        return $qb->getOneOrNullResult();
    }

    /**
     * @param SupervisionInterface $supervisionInterface
     * @param $poids
     * @param $success
     * @return int
     */
    public function update(SupervisionInterface $supervisionInterface, $poids, $success = false)
    {
        $query = 'update supervision_interface set ';
        $query .= 'total = total + 1';

        if (true === $success) {
            $query .= ' , total_ok = total_ok + 1';
            $query .= ' , poids_ok = poids_ok + ' . $poids;
        }
        $query .= ' , poids = poids + ' . $poids;
        $query .= ' where id = ? ';

        return $this->_em->getConnection()->executeUpdate($query, [$supervisionInterface->getId()]);
    }
}
