<?php

namespace App\Repository;

use App\Entity\MarchePublie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class MarchePublieRepository.
 */
class MarchePublieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MarchePublie::class);
    }

    /**
     * @param $idEntitePublique
     * @param $entityPurchaseArray
     * @param $annee
     *
     * @return array
     */
    public function getListeMarchesPublies(
        $idEntitePublique,
        $entityPurchaseArray,
        $annee,
        $isService = false
    ) {
        $qb = $this->createQueryBuilder('m');

        $qb
            ->where('m.organisme = :organisme')
            ->setParameter('organisme', $idEntitePublique)
            ->andWhere('m.numeroMarcheAnnee = :numeroMarcheAnnee')
            ->setParameter('numeroMarcheAnnee', $annee)
            ->andWhere('m.isPubliee = :true')
            ->setParameter('true', '1');
        if ($isService) {
            $qb->andWhere('m.serviceId IN (:listeServices)')
                ->setParameter('listeServices', $entityPurchaseArray);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $annee
     * @param $logger
     *
     * @return bool
     */
    public function insererMarchePublie($annee, $logger)
    {
        try {
            $sqlInsertForServices = "INSERT INTO MarchePublie (organisme,numeroMarcheAnnee,service_id,isPubliee)
SELECT S.organisme,:annee,S.id,'1' from Service  S
WHERE NOT EXISTS (SELECT *
FROM MarchePublie  M
where M.organisme=S.organisme
AND M.service_id = S.id
AND M.numeroMarcheAnnee=:annee )";

            $logger->info("La requete d'insertion pour les services => ".$sqlInsertForServices);
            $this->insererMarchePublieByQuery($sqlInsertForServices, $annee, $logger);

            $sqlInsertForOrganisme = "INSERT INTO MarchePublie (organisme,numeroMarcheAnnee,service_id,isPubliee)
SELECT O.acronyme,:annee,0,'1' from Organisme  O
WHERE NOT EXISTS (SELECT *
FROM MarchePublie  M
where M.organisme=O.acronyme
AND (M.service_id = 0 OR M.service_id IS NULL )
AND M.numeroMarcheAnnee=:annee )
AND O.active='1'
";
            $logger->info("La requete d'insertion pour les organismes => ".$sqlInsertForOrganisme);
            $this->insererMarchePublieByQuery($sqlInsertForOrganisme, $annee, $logger);

            return true;
        } catch (Exception $e) {
            $erreur = 'Erreur : '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString();
            $logger->error("Erreur lors de l'insertion des lignes dans marchePublie  [annnee = $annee], ".PHP_EOL.$erreur);
        }

        return false;
    }

    public function insererMarchePublieByQuery($sqlInsert, $annee, $logger)
    {
        $em = $this->getEntityManager();
        $stmtOrg = $em->getConnection()->prepare($sqlInsert);
        $stmtOrg->bindValue('annee', $annee);
        $logger->info("Début d'insertion");
        $stmtOrg->execute();
        $logger->info('Nombre des lignes insere = '.$stmtOrg->rowCount());
        $logger->info("Fin d'insertion ");
    }

    /**
     * @param $acronyme
     * @param $serviceId
     * @param $annee
     *
     * @return mixed
     *
     * @throws NonUniqueResultException
     */
    public function getMarchePublieByYear($acronyme, $serviceId, $annee)
    {
        return $this->createQueryBuilder('mp')
            ->select('mp')
            ->where('mp.organisme = :acronyme and mp.serviceId = :serviceId and mp.numeroMarcheAnnee = :annee')
            ->setParameter('acronyme', $acronyme)
            ->setParameter('serviceId', $serviceId)
            ->setParameter('annee', $annee)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
