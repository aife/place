<?php

namespace App\Repository;

use Doctrine\ORM\NoResultException;
use Doctrine\ORM\NonUniqueResultException;
use App\Entity\ConfigurationOrganisme;
use App\Entity\ContactContrat;
use App\Entity\ContratTitulaire;
use App\Entity\Entreprise;
use App\Entity\Service;
use App\Utils\Utils;
use Doctrine\ORM\EntityRepository;

/**
 * Repository DataSgmapRepository.
 *
 * @author     Oumar KONATE <oumar.konate@atexo.com>
 *
 * @version    0.0
 *
 * @since      0.0
 *
 * @copyright  Atexo 2015
 */
class EntrepriseRepository extends EntityRepository
{
    public string $headWs = 'entreprises';

    /**
     * LEZ TODO.
     */
    public function updateEntreprise($entreprise)
    {
        if ($entreprise instanceof Entreprise) {
            $this->_em->persist($entreprise);
            $this->_em->flush();
        }
    }

    /**
     * LEZ TODO.
     */
    public function getEntrepriseBySiren($siren)
    {
        return $this->findOneBySiren($siren);
    }

    public function findAllByParamWs(
        $nombreElementsParPage,
        $NumeroPage,
        $dateModification,
        $module,
        $orgServicesAllowed = [],
        $tabParams = [],
        $searchId = false
    ) {
        $queryBuilder = $this->createQueryBuilder('e')
            ->distinct()
            ->where('e.dateModification >= :datemodification')
            ->setParameter('datemodification', $dateModification)
            ->orderBy('e.dateModification', 'DESC');

        if ('EXEC' === $module) {
            $listOrgs = $this->_em->getRepository(ConfigurationOrganisme::class)
                ->getOrganismeAcronymesWhereSynchroExecActive();
            $conditionExec = [];
            foreach ($listOrgs as $orgSynchro) {
                if (isset($orgSynchro['organisme'])) {
                    $conditionExec[$orgSynchro['organisme']] = " ct.organisme = '" . $orgSynchro['organisme'] . "'";
                    $services = $this->_em->getRepository(Service::class)
                        ->getServiceIdsWhereSynchroExecActive($orgSynchro['organisme']);

                    if (is_array($services) && !empty($services)) {
                        $conditionExec[$orgSynchro['organisme']] .=
                        " AND ct.serviceId in ('" . implode("','", (new Utils())->flatArray($services)) . "')";
                        $conditionExec[$orgSynchro['organisme']] .= " or ct.serviceId is null ";
                    }
                }
            }
            if (!empty($conditionExec)) {
                $queryBuilder->join(
                    ContactContrat::class,
                    'cc',
                    'WITH',
                    'cc.idEntreprise = e.id'
                );
                $queryBuilder->join(
                    ContratTitulaire::class,
                    'ct',
                    'WITH',
                    'ct.idContactContrat = cc.idContactContrat'
                );
                $queryBuilder->where('ct.dateModification >= :datemodification');
                $queryBuilder->setParameter('datemodification', $dateModification);
                $queryBuilder->andWhere(implode('OR', $conditionExec));
            }
        }

        $limit = $nombreElementsParPage;
        $offset = $this->getOffset($NumeroPage, $nombreElementsParPage);
        $queryBuilder->setFirstResult($offset);
        $queryBuilder->setMaxResults($limit);

        $contracts = $queryBuilder->getQuery()->getResult();

        foreach ($contracts as $contract) {
            yield $contract;
            $this->_em->detach($contract);
        }
    }

    public function getOffset($page, $limit)
    {
        $offset = 0;
        if (0 != $page && 1 != $page) {
            $offset = ($page - 1) * $limit;
        }

        return $offset;
    }

    /**
     * @param array $data
     *
     * @return Entreprise|null
     */
    public function getEntrepriseBySirenEtrangerAndCountry($data)
    {
        $qb = $this->createQueryBuilder('e')
            ->where('e.sirenetranger = :siren')
            ->andWhere('e.paysadresse = :country')
            ->setParameter('siren', $data['siren'])
            ->setParameter('country', $data['country'])
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getNameAndIdEntrepriseByIds(array $ids): array
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.nom')
            ->where('e.id IN (:ids)')
            ->setParameter('ids', $ids);

        return $qb->getQuery()->getArrayResult();
    }

    public function getRaisonSocialAndSiretEntrepriseByIds(array $ids): array
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.nom AS raisonSocial', 'CONCAT(e.siren, et.codeEtablissement ) AS siret')
            ->innerJoin('e.etablissements', 'et')
            ->where('e.id IN (:ids)')
            ->setParameter('ids', $ids);

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getRaisonSocialAndSiretEntrepriseByIdAndEtablissement(int $entrepriseId, int $etabliId): array
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.nom AS raisonSocial', 'CONCAT(e.siren, et.codeEtablissement ) AS siret')
            ->innerJoin('e.etablissements', 'et')
            ->where('e.id = :entrepriseId')
            ->andWhere('et.idEtablissement = :etablissementId')
            ->setParameter('entrepriseId', $entrepriseId)
            ->setParameter('etablissementId', $etabliId);

        return $qb->getQuery()->getSingleResult();
    }
}
