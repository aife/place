<?php

namespace App\Repository;

use Doctrine\ORM\NonUniqueResultException;
use App\Entity\ConfigurationPlateforme;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ConfigurationPlateformeRepository.
 *
 * @author    Oumar KONATE <oumar.konate@atexo.com>
 *
 * @since     ESR2016
 *
 * @copyright Atexo 2016
 */
class ConfigurationPlateformeRepository extends ServiceEntityRepository
{
    protected $configurationPlateforme;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConfigurationPlateforme::class);
    }

    /**
     * Permet de recuperer la configuration plateforme.
     *
     * @throws NonUniqueResultException
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016*/
    public function getConfigurationPlateforme(): ConfigurationPlateforme
    {
        if (!$this->configurationPlateforme) {
            $this->configurationPlateforme = $this->createQueryBuilder('c')->orderBy('c.idAuto', 'ASC')
                ->setMaxResults(1)->getQuery()->getSingleResult();
        }

        return $this->configurationPlateforme;
    }
}
