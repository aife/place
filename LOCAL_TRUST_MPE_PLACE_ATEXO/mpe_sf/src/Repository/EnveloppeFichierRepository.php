<?php

namespace App\Repository;

use App\Entity\HistoriquePurge;
use App\Entity\Lot;
use DateTimeInterface;
use Exception;
use Generator;
use App\Entity\ConsLotContrat;
use App\Entity\Consultation;
use App\Entity\ContratTitulaire;
use App\Entity\Enveloppe;
use App\Entity\EnveloppeFichier;
use App\Entity\Offre;
use App\Entity\TCandidature;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

/**
 * Repository EnveloppeFichierRepository.
 *
 * @author    Oumar KONATE <oumar.konate@atexo.com>
 *
 * @since     ESR2016
 *
 * @copyright Atexo 2016
 */
class EnveloppeFichierRepository extends EntityRepository
{
    public final const LIMIT = 100;

    private $logger;

    /**
     * Permet de recuperer la liste des fichiers transmis pour une offre.
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getListeFichiersDepot(Offre $offre, array $listeTypesEnv): array
    {
        try {
            if ($offre instanceof Offre) {
                $arrayFiles = [];
                $enveloppes = $offre->getEnveloppes();
                if (!empty($enveloppes)) {
                    foreach ($enveloppes as $enveloppe) {
                        if ($enveloppe instanceof Enveloppe) {
                            $fichiersEnveloppe = $enveloppe->getFichierEnveloppes();
                            foreach ($fichiersEnveloppe as $fichier) {
                                if ($fichier instanceof EnveloppeFichier) {
                                    $fichier = $this->_em->getReference(
                                        EnveloppeFichier::class,
                                        $fichier->getIdFichier()
                                    );
                                    if ($fichier instanceof EnveloppeFichier) {
                                        $lot = $this->_em->getRepository(Lot::class)
                                            ->findOneBy([
                                                'consultation' => $offre->getConsultationId(),
                                                'organisme' => $offre->getOrganisme(),
                                                'lot' => $enveloppe->getSousPli(),
                                            ]);
                                        $nomFichierSignature = '';
                                        if ($fichier->getIdFichierSignature()) {
                                            $fichierSignature = $this
                                                ->getFichierById($fichier->getIdFichierSignature());
                                            if ($fichierSignature instanceof EnveloppeFichier) {
                                                $nomFichierSignature = $fichierSignature->getNomFichier();
                                                $fichier->setFichierSignature($fichierSignature);
                                            }
                                        }
                                        $jsonInfosSignature = $fichier->getSignatureInfos() ?? '{}';
                                        $jsonInfosSignatureObjet = json_decode(
                                            $jsonInfosSignature, true, 512, JSON_THROW_ON_ERROR);
                                        $jsonInfosSignatureObjet['name'] = $nomFichierSignature;
                                        $jsonInfosSignatureObjet['isSignaturePADES'] = ('pades' == strtolower($fichier
                                                ->getTypeSignatureFichier()));

                                        //Mise a jour des infos signature de l'offre
                                        $fichier->setSignatureInfos(
                                            json_encode($jsonInfosSignatureObjet, JSON_THROW_ON_ERROR)
                                        );

                                        $arrayFiles[$enveloppe
                                            ->getTypeEnv()][$enveloppe->getSousPli()]['lot'] = $lot;
                                        $arrayFiles[$enveloppe
                                            ->getTypeEnv()][$enveloppe->getSousPli()]['enveloppe_object'] = $enveloppe;
                                        $arrayFiles[$enveloppe
                                            ->getTypeEnv()][$enveloppe->getSousPli()]['enveloppe'][$enveloppe
                                            ->getTypeEnv()]['typeEnv'] = $enveloppe->getTypeEnv();
                                        $arrayFiles[$enveloppe
                                            ->getTypeEnv()][$enveloppe->getSousPli()]['enveloppe'][$enveloppe
                                            ->getTypeEnv()]['fichiers'][$fichier->getIdFichier()] = $fichier;
                                    }
                                }
                            }
                            if (!empty($enveloppe->getDossierVolumineux())) {
                                if (!isset($arrayFiles[$enveloppe->getTypeEnv()][$enveloppe->getSousPli()])) {
                                    $lot = $this->_em->getRepository(Lot::class)
                                        ->findOneBy([
                                            'consultation' => $offre->getConsultationId(),
                                            'organisme' => $offre->getOrganisme(),
                                            'lot' => $enveloppe->getSousPli(),
                                        ]);
                                    $arrayFiles[$enveloppe->getTypeEnv()][$enveloppe->getSousPli()]['lot'] = $lot;
                                    $arrayFiles
                                        [$enveloppe->getTypeEnv()]
                                        [$enveloppe->getSousPli()]
                                        ['enveloppe_object'] = $enveloppe
                                    ;
                                    $arrayFiles
                                        [$enveloppe->getTypeEnv()]
                                        [$enveloppe->getSousPli()]['enveloppe']
                                        [$enveloppe->getTypeEnv()]['typeEnv'] = $enveloppe->getTypeEnv()
                                    ;
                                }
                                $arrayFiles
                                    [$enveloppe->getTypeEnv()]
                                    [$enveloppe->getSousPli()]
                                    ['enveloppe'][$enveloppe->getTypeEnv()]
                                    ['dossierVolumineux'] = $enveloppe->getDossierVolumineux()
                                ;
                            }
                        }
                    }
                }
            }

            //Ordonner la liste des fichiers par types enveloppes, puis par lots
            $dataSources = null;
            foreach ($listeTypesEnv as $typeEnv => $val) {
                if (array_key_exists($typeEnv, $arrayFiles) && is_array($arrayFiles[$typeEnv])) {
                    ksort($arrayFiles[$typeEnv]);
                    if (is_array($dataSources)) {
                        $dataSources = [...$dataSources, ...$arrayFiles[$typeEnv]];
                    } else {
                        $dataSources = $arrayFiles[$typeEnv];
                    }
                }
            }

            return (is_array($dataSources)) ? $dataSources : [];
        } catch (Exception $e) {
            if (!empty($this->logger)) {
                $erreur = 'Erreur: ' . $e->getMessage() . PHP_EOL . 'Trace : ' . $e->getTraceAsString();
                $this->logger->info('Erreur lors de la recuperation de la liste des fichiers deposes '
                    . PHP_EOL . 'id_offre=' . $offre->getId() . PHP_EOL . $erreur);
            }

            return [];
        }
    }

    /**
     * nombre Fichier pour une enveloppe.
     *
     * @param int $idFichier
     *
     * @return bool
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1
     *
     * @since   develop
     *
     * @copyright Atexo 2016
     */
    public function getNombreEnveloppeFichier(int $idEnveloppe)
    {
        return $this->createQueryBuilder('fichier')
            ->select('COUNT(fichier)')
            ->where('fichier.idEnveloppe = :idEnveloppe')
            ->setParameter('idEnveloppe', $idEnveloppe)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Permet de recuperer le nom du fichier de la signature.
     *
     * @param EnveloppeFichier $fichier :fichier source
     *
     * @return string
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getNomFichierSignature($fichier)
    {
        try {
            if ($fichier instanceof EnveloppeFichier) {
                $fichierSignature = $this->find($fichier->getIdFichierSignature());
                if ($fichierSignature instanceof EnveloppeFichier) {
                    return $fichierSignature->getNomFichier();
                }
            }

            return '';
        } catch (Exception $e) {
            if ($this->logger) {
                $idFichier = $fichier->getIdFichier();
                $this->logger->info("Erreur lors de la recuperation du nom du fichier: [idFichier=$idFichier] "
                    . PHP_EOL . 'Erreur: ' . $e->getMessage() . 'Exception: ' . $e->getTraceAsString());
            }
        }
    }

    /**
     * @return mixed
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param mixed $logger
     *
     * @return EnveloppeFichierRepository
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;

        return $this;
    }

    /**
     * Permet de recuperer un fichier.
     *
     * @param string $idFichier id du fichier
     *
     * @return EnveloppeFichier
     *
     * @author    Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @since     01.00.00-RC2-develop
     *
     * @copyright Atexo 2016
     */
    public function getFichierById($idFichier)
    {
        try {
            $fichier = $this->find($idFichier);
            if ($fichier instanceof EnveloppeFichier) {
                return $fichier;
            }

            return false;
        } catch (Exception $e) {
            if ($this->logger) {
                $this->logger->info("Erreur lors de la recuperation du  fichier: [idFichier=$idFichier] "
                    . PHP_EOL . 'Erreur: ' . $e->getMessage() . 'Exception: ' . $e->getTraceAsString());
            }
        }
    }

    public function getNameFilesEnveloppe(int $idEnveloppe)
    {
        $qb = $this->createQueryBuilder('f')
            ->select('f.nomFichier')
            ->where('f.idEnveloppe = :idEnveloppe')
            ->setParameter('idEnveloppe', $idEnveloppe);

        return $qb->getQuery()->getResult();
    }

    /**
     * @return array
     */
    public function listeFichiersEnveloppeASupprimer(int $idEnveloppe, string $organisme)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->where('e.organisme = :organisme')->setParameter('organisme', $organisme);
        $qb->andWhere('e.idEnveloppe = :idEnveloppe')->setParameter('idEnveloppe', $idEnveloppe);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $idOffre
     * @param $type
     * @param array $statutEnveloppe
     *
     * @return array
     */
    public function getEnveloppeFichiersByTypeEnveloppeAndOffreId($idOffre, $type, $statutEnveloppe)
    {
        return $this->createQueryBuilder('f')
            ->select('f.typeFichier as type, 
            f.nomFichier as nom, 
            f.tailleFichier as poids, 
            f.idBlob AS id_blob,  e.sousPli as alloti')
            ->join('f.enveloppe', 'e')
            ->where('e.offre = :idOffre')
            ->andWhere('e.typeEnv = :type')
            ->andWhere('e.statutEnveloppe NOT IN (:statutEnveloppe)')
            ->setParameter('idOffre', $idOffre)
            ->setParameter('type', $type)
            ->setParameter('statutEnveloppe', $statutEnveloppe)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $idOffre
     * @param $type
     * @param array $statutEnveloppe
     *
     * @return array
     */
    public function getEnveloppeFichiersFermerByTypeEnveloppeAndOffreId($idOffre, $type, $statutEnveloppe)
    {
        return $this->createQueryBuilder('f')
            ->select('f.typeFichier as type, 
            f.nomFichier as nom, 
            f.tailleFichier as poids, 
            f.idBlob AS id_blob, e.sousPli as alloti')
            ->join('f.enveloppe', 'e')
            ->where('e.offre = :idOffre')
            ->andWhere('e.typeEnv = :type')
            ->andWhere('e.statutEnveloppe IN (:statutEnveloppe)')
            ->setParameter('idOffre', $idOffre)
            ->setParameter('type', $type)
            ->setParameter('statutEnveloppe', $statutEnveloppe)
            ->getQuery()
            ->getResult();
    }

    public function getEnveloppeAttributaire($idOffre, $type, EntityManagerInterface $em)
    {
        $dql = 'SELECT DISTINCT f.typeFichier AS type, 
f.nomFichier AS nom, 
f.tailleFichier AS poids, 
f.idBlob AS id_blob, 
e.sousPli AS alloti
FROM ' . EnveloppeFichier::class . ' as f
INNER JOIN ' . Enveloppe::class . ' e WITH f.idEnveloppe = e.idEnveloppeElectro
INNER JOIN ' . Offre::class . ' o WITH e.offre = o.id
INNER JOIN ' . ContratTitulaire::class . ' c WITH c.idOffre = o.id
WHERE e.offre = :idOffre 
AND e.typeEnv = :type 
AND e.statutEnveloppe NOT IN (1, 4, 6, 8, 9, 10, 11, 12, 99)';

        return $em->createQuery($dql)
            ->setParameter('idOffre', $idOffre)
            ->setParameter('type', $type)
            ->getResult();
    }

    public function getEnveloppeAllotiAttributaire($idOffre, $type, $listStatutEnv, EntityManagerInterface $em)
    {
        $dql = 'SELECT DISTINCT f.typeFichier AS type, 
f.nomFichier AS nom, 
f.tailleFichier AS poids, 
f.idBlob AS id_blob, 
e.sousPli AS alloti
FROM ' . EnveloppeFichier::class . ' as f
INNER JOIN ' . Enveloppe::class . ' e WITH f.idEnveloppe = e.idEnveloppeElectro
INNER JOIN ' . Offre::class . ' o WITH e.offre = o.id
INNER JOIN ' . ContratTitulaire::class . ' c WITH c.idOffre = o.id
INNER JOIN ' . ConsLotContrat::class . ' cl WITH cl.idContratTitulaire = c.idContratTitulaire
WHERE e.offre = :idOffre 
AND e.typeEnv = :type 
AND e.sousPli = cl.lot
AND e.statutEnveloppe NOT IN (:statutEnveloppe)';

        return $em->createQuery($dql)
            ->setParameter('idOffre', $idOffre)
            ->setParameter('type', $type)
            ->setParameter('statutEnveloppe', $listStatutEnv)
            ->getResult();
    }

    /**
     * @param int $idEnveloppe
     *
     * @return array
     */
    public function getIdBlobFiles(int $idOffre, string $organisme)
    {
        $qb = $this->createQueryBuilder('f')
            ->select('f.idBlob')
            ->leftJoin(
                Enveloppe::class,
                'e',
                Join::WITH,
                'f.idEnveloppe = e.idEnveloppeElectro and f.organisme = e.organisme'
            )
            ->Where('e.offreId = :idOffre')->setParameter('idOffre', $idOffre)
            ->andWhere('e.organisme = :org')->setParameter('org', $organisme)
        ;
        $res = $qb->getQuery()->getResult();
        $fn = fn($elmt) => $elmt['idBlob'];

        return array_map($fn, $res);
    }

    /**
     * @return Generator
     */
    public function progressiveFindAll()
    {
        $qb = $this->createQueryBuilder('f')
                    ->select('f.idFichier', 'f.organisme', 'f.signatureFichier', 'f.idEnveloppe')
                    ->where('f.idBlobSignature IS NULL')
                    ->andWhere('f.signatureFichier IS NOT NULL')
                    ->andWhere('f.signatureFichier != :empty')
                    ->setParameter('empty', '');

        $limit = self::LIMIT;

        $offset = 0;

        while (true) {
            $qb->setFirstResult($offset);
            $qb->setMaxResults($limit);

            $results = $qb->getQuery()->getResult();

            if (0 === (is_countable($results) ? count($results) : 0)) {
                break;
            }
            foreach ($results as $result) {
                yield $result;
            }
            unset($results);
        }
    }

    /**
     *
     * @return mixed|array
     */
    public function getEnveloppeAnnexeFinanciere(Consultation $consultation, array $statutOffre)
    {
        $qb = $this->createQueryBuilder('f')
            ->leftJoin(
                Enveloppe::class,
                'e',
                Join::WITH,
                'f.idEnveloppe = e.idEnveloppeElectro and f.organisme = e.organisme'
            )
            ->leftJoin('e.offre', 'o')
            ->andWhere('f.typeFichier = :typeFichier')->setParameter('typeFichier', EnveloppeFichier::TYPE_FICHIER_AFI)
            ->andWhere('f.typePiece = :typePiece')->setParameter('typePiece', EnveloppeFichier::TYPE_PIECE_AFI)
            ->andWhere('o.statutOffres IN (:statutOffre)')->setParameter('statutOffre', $statutOffre)
        ;

        if ($consultation->isAlloti()) {
            $qb
                ->leftJoin(
                    TCandidature::class,
                    'c',
                    Join::WITH,
                    'c.idOffre = o.id'
                )
                ->andWhere('o.consultationId = :consultationId')->setParameter('consultationId', $consultation)
            ;
        } else {
            $qb
                ->leftJoin('o.consultation', 'c')
                ->andWhere('c.id = :consultation')->setParameter('consultation', $consultation)
            ;
        }

        return $qb->getQuery()->getResult();
    }

    public function getFichierEnveloppeDechifree(
        array $consultationStatut,
        ?string $organisme,
        int $limit,
        array $stateList,
        DateTimeInterface $dateDlro,
        ?array $consultationIdsList = null,
    ): array {

        $params = [
            'statutOffres' => Offre::STATUT_OFFRE_OUVERT,
            'isDeleted' => HistoriquePurge::FILE_NOT_DELETED,
            'state' => $stateList,
            'dlro' => $dateDlro->format('Y-m-d'),
            'dateMiseEnLigneCalcule' => '0000-00-00 00:00:00',
        ];
        $conditionsStatCons =
            $this->getEntityManager()
            ->getRepository(Consultation::class)
            ->getConditionStatesConsultation(
                'c',
                $consultationStatut,
                $params
            );
        $qb = $this
            ->createQueryBuilder('fe')
            ->setMaxResults($limit)
            ->leftJoin('fe.enveloppe', 'e')
            ->leftJoin('e.offre', 'o')
            ->leftJoin('o.consultation', 'c')
            ->leftJoin(
                HistoriquePurge::class,
                'hp',
                'WITH',
                'fe.idFichier = hp.fichierEnveloppe'
            )
            ->andWhere('o.statutOffres = :statutOffres')
            ->andWhere('hp.deleted = :isDeleted or hp.deleted is null')
            ->andWhere('hp.state IN (:state) or hp.state is null')
            ->andWhere($conditionsStatCons)
            ->andWhere('c.datefin < :dlro')
            ->andWhere('c.dateMiseEnLigneCalcule <> :dateMiseEnLigneCalcule')
            ->andWhere('fe.idBlob <> 0')
            ->setParameters($params)
        ;

        if ($organisme && $organisme !== HistoriquePurge::ORGANISME_ALL) {
            $qb->andWhere('c.organisme = :organisme')->setParameter('organisme', $organisme);
        }

        if ($consultationIdsList) {
            $qb->andWhere('c.id IN (:consultationsIds)')->setParameter('consultationsIds', $consultationIdsList);
        }

        return $qb->getQuery()->getResult();
    }
}
