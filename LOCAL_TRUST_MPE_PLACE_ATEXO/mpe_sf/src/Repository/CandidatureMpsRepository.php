<?php
/**
 * Created by PhpStorm.
 * User: Aso
 * Date: 17/11/2016
 * Time: 14:49.
 */

namespace App\Repository;

use App\Entity\CandidatureMps;
use App\Entity\Consultation;
use App\Entity\Inscrit;
use App\Entity\Offre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Mohamed BLAL
 * Class CandidatureMpsRepository.
 */
class CandidatureMpsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CandidatureMps::class);
    }

    /**
     * @param $id
     * @param $consultation
     * @param $offer
     * @param $candidate
     *
     * @return mixed
     */
    public function getCandidatureMpsWithoutOffer($id, Consultation $consultation, Offre $offer, Inscrit $candidate)
    {
        $organism = sprintf("'%s'", $consultation->getAcronymeOrg());
        $qb = $this->createQueryBuilder('c');
        $qb->select('c')
            ->where("c.idOffre = {$offer->getId()}")
            ->andWhere("c.idCandidature = $id")
            ->andWhere("c.consultationId = {$consultation->getId()}")
            ->andWhere("c.organisme = $organism")
            ->andWhere("c.idInscrit = {$candidate->getId()}")
            ->andWhere("c.idEntreprise = {$candidate->getEntrepriseId()}");
        //return $qb->getQuery()->getSQL();
        return $qb->getQuery()->getResult();
    }

    /**
     * @param $id
     * @param $consultation
     * @param $candidate
     *
     * @return mixed
     */
    public function getCandidatureMpsBrouillon($id, Consultation $consultation, Inscrit $candidate)
    {
        $organism = sprintf("'%s'", $consultation->getAcronymeOrg());
        $qb = $this->createQueryBuilder('c');
        $qb->select('c')
            ->where('c.idOffre IS NULL')
            ->andWhere("c.idCandidature = $id")
            ->andWhere("c.consultationId = {$consultation->getId()}")
            ->andWhere("c.organisme = $organism")
            ->andWhere("c.idInscrit = {$candidate->getId()}")
            ->andWhere("c.idEntreprise = {$candidate->getEntrepriseId()}");
        //return $qb->getQuery()->getSQL();
        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param $organisme
     * @param $limit
     */
    public function listeCandidaturesMpsASupprimer($organisme, $date, $limit)
    {
        $qb = $this->createQueryBuilder('c');
        $qb->where('c.organisme = :organisme')->setParameter('organisme', $organisme);
        $qb->andWhere('c.untrustedDate < :date')->setParameter('date', $date);
        $qb->orderBy('c.idCandidature', 'ASC');
        $qb->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }
}
