<?php

namespace App\Repository;

use App\Entity\Consultation;
use App\Entity\Offre;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityRepository;

class EnveloppeRepository extends EntityRepository
{
    /**
     * @param $organisme
     * @param $limit
     */
    public function listeEnveloppesASupprimer(array $offres, string $organisme)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->where('e.organisme = :organisme')->setParameter('organisme', $organisme);
        $qb->andWhere('e.offreId IN (:offres)')->setParameter('offres', $offres, Connection::PARAM_STR_ARRAY);
        $qb->orderBy('e.fichier', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function getEnveloppesEnCoursDechiffrement(string $statutEnCoursDechiffrement, $delaiValidite)
    {
        $qb = $this->createQueryBuilder('e');
        $result = $qb->select('COUNT(DISTINCT(e.idEnveloppeElectro))')
            ->leftJoin(Offre::class, 'o', 'WITH', 'o.id = e.offreId')
            ->leftJoin(
                Consultation::class,
                'c',
                'WITH',
                'o.organisme = c.organisme  AND o.consultation = c.reference'
            )
            ->where('e.statutEnveloppe= :statutEnCoursDechiffrement AND c.modeOuvertureReponse=\'0\' '
                .'AND DATE_ADD(e.dateDebutDechiffrement, :delaiValidite, \'HOUR\') < now()')
            ->setParameter('statutEnCoursDechiffrement', $statutEnCoursDechiffrement)
            ->andWhere($qb->expr()->isNotNull('e.dateDebutDechiffrement'))
            ->setParameter('delaiValidite', $delaiValidite / 60)
            ->groupBy('o.id');

        $queryResult = $result->getQuery()->getOneOrNullResult();
        $nbrOffres = (!empty($queryResult)) ? current($queryResult) : 0;

        return $nbrOffres;
    }

    /**
     * @return array
     */
    public function getInfoEnveloppes(int $consultation, string $organisme)
    {
        return $this->createQueryBuilder('e')
            ->select('o.nomEntrepriseInscrit, e.sousPli, o.id, e.statutEnveloppe')
            ->leftJoin('e.offre', 'o')
            ->where('e.organisme = :organisme')
            ->andWhere('o.consultation = :consultation')
            ->setParameter('organisme', $organisme)
            ->setParameter('consultation', $consultation)
            ->getQuery()
            ->getResult();
    }
}
