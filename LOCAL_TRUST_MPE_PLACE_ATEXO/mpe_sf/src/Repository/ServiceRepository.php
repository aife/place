<?php

/**
 *  *
 *  * Copyright (C)  Atexo  - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential.
 */

namespace App\Repository;

use App\Entity\Agent;
use App\Entity\Organisme;
use Generator;
use Exception;
use Doctrine\ORM\NonUniqueResultException;
use App\Entity\AffiliationService;
use App\Entity\ConfigurationOrganisme;
use App\Entity\Service;
use App\Exception\ApiProblemDbException;
use App\Exception\OrganismeServiceNotFoundException;
use App\Exception\OrganismeServiceOldIdMultipleException;
use App\Utils\AtexoObjectConverter;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;

/**
 * Mohamed BLAL.
 *
 * Class ServiceRepository
 */
class ServiceRepository extends EntityRepository
{
    /**
     * @throws \Doctrine\DBAL\Exception
     */
    public function getAncestors(int $id, string $organism)
    {
        $sql = 'SELECT t.service_id AS id_service, @pv := t.service_parent_id AS id_pole FROM ( ';
        $sql .= "SELECT * FROM AffiliationService WHERE organisme =  $organism )t ";
        $sql .= "JOIN ( SELECT @pv := $id )tmp WHERE t.service_id = @pv";

        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $result = $stmt->executeQuery()->fetchAllAssociative();

        return array_map(fn($item) => $item['id_pole'], $result);
    }

    /**
     * Permet de retourner la liste des services qu'ils ont un siren.
     *
     * @return collection of object Service
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since ESR2018
     *
     * @copyright Atexo 2018
     */
    public function getServicesAvecSiren()
    {
        $qb = $this->createQueryBuilder('s')
            ->Where('s.siren IS NOT NULL')
            ->andWhere("s.siren NOT LIKE ''");

        return $qb->getQuery()->getResult();
    }

    /**
     * Permet de modifier un service.
     *
     * @param object Service
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since ESR2018
     *
     * @copyright Atexo 2018
     */
    public function updateService($service)
    {
        if ($service instanceof Service) {
            $this->_em->persist($service);
            $this->_em->flush();
        }
    }

    public function thisUpdate(Service $service)
    {
        $query = $this->createQueryBuilder('s')
            ->update(Service::class, 's')
            ->where('s.id = :id')
            ->setParameter('id', $service->getId())
            ->andWhere('s.acronymeOrg = :acronyme')
            ->setParameter('acronyme', $service->getAcronymeOrg());

        $properties = [
            'idExterne',
            'idExterneParent',
            'libelle',
            'sigle',
            'siren',
            'complement',
            'formeJuridique',
            'formeJuridiqueCode',
            'cheminComplet',
            'dateModification',
            'mail',
            'idEntite',
        ];

        foreach ($properties as $property) {
            $methodName = 'get' . ucfirst($property);

            if ('sigle' === $property) {
                $query->set('s.' . $property, ':' . $property);
                $query->setParameter($property, $service->$methodName());
            } elseif ($service->$methodName()) {
                $query->set('s.' . $property, ':' . $property);
                $query->setParameter($property, $service->$methodName());
            }
        }

        $query->getQuery()->execute();
    }

    public string $headWs = 'services';

    /**
     * @param $nombreElementsParPage
     * @param $NumeroPage
     * @param $dateModification
     * @param $module
     * @param array $orgServicesAllowed
     *
     * @return Generator
     *
     * @throws Exception
     */
    public function findAllByParamWs(
        $nombreElementsParPage,
        $NumeroPage,
        $dateModification,
        $module,
        $orgServicesAllowed = []
    ) {
        return $this->findWithParamWs(
            $nombreElementsParPage,
            $NumeroPage,
            $dateModification,
            $module,
            $orgServicesAllowed
        );
    }

    /**
     * Récupérer un service via son ID
     *
     */
    public function findByIdAndParamWs(int $idEntityToSearch, string $module, array $orgServicesAllowed = []): Generator
    {
        return $this->findWithParamWs(
            null,
            null,
            $idEntityToSearch,
            $module,
            $orgServicesAllowed,
            true
        );
    }

    /**
     * Récupérer une liste de services ou un service spécifique
     *
     */
    public function findWithParamWs(
        ?int $nombreElementsParPage,
        ?int $NumeroPage,
        mixed $criteria,
        string $module,
        array $orgServicesAllowed,
        bool $searchId = false
    ): Generator {
        $queryBuilder = $this->createQueryBuilder('s')
            ->select('s')
            ->distinct();

        if (is_int($criteria) && $searchId) {
            $queryBuilder->where('s.id = :idService');
        } else {
            $queryBuilder->where('s.dateModification >= :datemodification');
        }

        $queryBuilder->orderBy('s.dateModification', 'DESC');

        $parameters = (is_int($criteria) && $searchId)
            ? ['idService' => $criteria]
            : ['datemodification' => $criteria];

        if ('EXEC' === $module) {
            $queryBuilder->join(
                ConfigurationOrganisme::class,
                'c',
                'WITH',
                's.organisme = c.organisme '
            );
            $queryBuilder->andWhere("c.moduleExec = '1' ");
        }
        if (is_array($orgServicesAllowed) && !empty($orgServicesAllowed)) {
            $condition = [];
            foreach ($orgServicesAllowed as $key => $services) {
                $param = str_replace('-', '_', $key);
                $condition[$key] = " (  s.organisme = :$param";
                $parameters[$param] = $key;
                if (!in_array(0, $services)) {
                    $condition[$key] .= " AND s.id in ('" . implode("','", $services) . "')";
                }
                $condition[$key] .= ')';
            }
            if (!empty($condition)) {
                $queryBuilder->andWhere('( ' . implode(' OR ', $condition) . ')');
            }
        }
        $queryBuilder->setParameters($parameters);
        $limit = $nombreElementsParPage;
        $offset = $this->getOffset($NumeroPage, $nombreElementsParPage);
        $queryBuilder->setFirstResult($offset);
        $queryBuilder->setMaxResults($limit);

        //@todo Modifier getArrayResult => getResult et supprimer le converter lorsque l'on aura plu de double
        //@todo clé primaire.
        $services = $queryBuilder->getQuery()->getArrayResult();

        $objectConverter = new AtexoObjectConverter();
        foreach ($services as $serviceArray) {
            $service = $objectConverter->arrayToObject($serviceArray, Service::class);
            if ($service instanceof Service) {
                $parentService = $this->getServiceParent($service->getId(), $service->getAcronymeOrg());
                $service->setIdParent($parentService?->getId());
                $service->setOldIdParent($parentService?->getOldId());
                $service->setIdExterneParent($parentService ? $parentService->getIdExterne() : '0');

                yield $service;
                $this->_em->detach($service);
            }
        }
    }

    public function getOffset($page, $limit)
    {
        $offset = 0;
        if (0 != $page && 1 != $page) {
            $offset = ($page - 1) * $limit;
        }

        return $offset;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getServiceParent(int $id, string $organism): ?Service
    {
        $qb = $this->createQueryBuilder('s');
        $qb->select('s')
            ->leftJoin(
                'App:AffiliationService',
                'aa',
                'WITH',
                's.id = aa.serviceParentId and s.organisme = aa.organisme'
            )
            ->where('aa.organisme = :organisme')
            ->andWhere('aa.serviceId = :id')
            ->setParameter('organisme', $organism)
            ->setParameter('id', $id);
        $qb->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param $idPole
     * @param $organisme
     *
     * @return int|mixed|string
     *
     * @throws Exception
     */
    public function getAllServicesByIdPole(int $idPole, string $organisme)
    {
        try {
            $qb = $this->createQueryBuilder('s');
            $qb->innerJoin(
                AffiliationService::class,
                'a',
                'WITH',
                's.id = a.serviceId and s.organisme = a.organisme'
            )
                ->where('a.organisme = :organisme')
                ->andwhere('a.serviceParentId = :idPole')
                ->setParameter('organisme', $organisme)
                ->setParameter('idPole', $idPole)
                ->orderBy('s.sigle', 'ASC');

            return $qb->getQuery()->getResult();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * this function returns Only the IDs of the services where the synchronisation EXEC is active.
     *
     * @param $org
     *
     * @return int|mixed|string
     */
    public function getServiceIdsWhereSynchroExecActive($org)
    {
        $qb = $this->createQueryBuilder('s');
        $qb->select('s.id')
            ->Where('s.organisme = :acronyme')
            ->andWhere("s.synchronisationExec = '1'")
            ->setParameter('acronyme', $org);

        return $qb->getQuery()->getResult();
    }

    /**
     * Retourne l'unique Service correspondant à l'acronyme d'organisme
     * et soit à l'id, soit à l'old_id.
     *
     */
    public function findByIdOrOldIdAndOrganisme(int $id, string $acronymeOrganisme): ?Service
    {
        $criteria = (new Criteria())
            ->andWhere(
            /* @phpstan-ignore-next-line Car ne sera jamais null */
                (new Criteria())->where(Criteria::expr()->eq('id', $id))
                    ->orWhere(Criteria::expr()->eq('oldId', $id))->getWhereExpression()
            )
            ->andWhere(Criteria::expr()->eq('acronymeOrg', $acronymeOrganisme));

        $resultsCollection = $this->matching($criteria);

        if (1 < $resultsCollection->count()) {
            throw new OrganismeServiceOldIdMultipleException($acronymeOrganisme, $id);
        }

        $result = $resultsCollection->first();

        if (!$result instanceof Service) {
            throw new OrganismeServiceNotFoundException($acronymeOrganisme, $id);
        }

        return $result;
    }

    public function findAllByIdentifierNotIn($notIn, $limit)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('s')
            ->from($this->getEntityName(), 's')
            ->setMaxResults($limit)
        ;

        if (!empty($notIn)) {
            $qb->where(
                $qb->expr()->notIn('s.id', $notIn)
            );
        }

        return $qb
            ->getQuery()
            ->getResult();
    }

    public function countByIdentifierNotIn(array $notIn)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('COUNT(s.id)')
            ->from($this->getEntityName(), 's')
        ;

        if (!empty($notIn)) {
            $qb->where(
                $qb->expr()->notIn('s.id', $notIn)
            );
        }

        return $qb
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function setEm($em)
    {
        $this->_em = $em;
    }

    public function getServiceArborescence(Agent $agent)
    {
        $stmt = $this->getEntityManager()->getConnection()->prepare('
            SELECT 
                s.id,
                s.libelle,
                s.sigle,
                asv.service_parent_id AS parent_id,
                ipt.id AS invite_permanent_transverse_id
            FROM Service AS s 
            LEFT JOIN AffiliationService AS asv ON s.id = asv.service_id AND s.organisme = asv.organisme
            LEFT JOIN invite_permanent_transverse AS ipt ON 
                s.id = ipt.service_id AND s.organisme = ipt.acronyme AND ipt.agent_id = :agentId
            WHERE s.organisme = :organisme 
            ORDER BY s.sigle
        ');

        return $stmt->executeQuery(
            [
            'organisme' => $agent->getOrganisme()?->getAcronyme(),
            'agentId' => $agent->getId(),
            ]
        )->fetchAllAssociative();
    }
}
