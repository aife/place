<?php

namespace App\Repository\Messagerie;

use App\Entity\Messagerie\MailTemplate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Generator;
use DateTime;
use Doctrine\ORM\EntityRepository;

/**
 * Class MailTemplateRepository.
 */
class MailTemplateRepository extends ServiceEntityRepository
{
    public string $headWs = 'mail_template';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MailTemplate::class);
    }

    /**
     * @param $nombreElementsParPage
     * @param $NumeroPage
     * @param $module
     * @param array $orgServicesAllowed
     * @param array $tabParams
     *
     * @return Generator
     */
    public function findAllByParamWs(
        $nombreElementsParPage,
        $NumeroPage,
        DateTime $dateModification,
        $module,
        $orgServicesAllowed = [],
        $tabParams = []
    ) {
        $queryBuilder = $this->createQueryBuilder('a')
            ->select('a')
        ;

        $limit = $nombreElementsParPage;
        $offset = $this->getOffset($NumeroPage, $nombreElementsParPage);
        $queryBuilder->setFirstResult($offset);
        $queryBuilder->setMaxResults($limit);

        $listes = $queryBuilder->getQuery()->getResult();

        foreach ($listes as $liste) {
            yield $liste;
            $this->_em->detach($liste);
        }
    }

    /**
     * @param $page
     * @param $limit
     *
     * @return int
     */
    public function getOffset($page, $limit)
    {
        $offset = 0;
        if (0 != $page && 1 != $page) {
            $offset = ($page - 1) * $limit;
        }

        return $offset;
    }
}
