<?php

namespace App\Repository\Messagerie;

use App\Entity\Messagerie\MailType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class MailTypeRepository.
 */
class MailTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MailType::class);
    }
}
