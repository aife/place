<?php

namespace App\Repository\Messagerie;

use App\Entity\Messagerie\MailTypeGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class MailTypeGroupRepository.
 */
class MailTypeGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MailTypeGroup::class);
    }
}
