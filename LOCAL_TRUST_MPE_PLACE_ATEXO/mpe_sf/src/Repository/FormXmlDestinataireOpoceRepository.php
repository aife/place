<?php

namespace App\Repository;

use App\Entity\FormXmlDestinataireOpoce;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class FormXmlDestinataireOpoceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FormXmlDestinataireOpoce::class);
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function retrieveFormXmlDestPubByRefCons($data)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery(
            "SELECT f.lienPublication as lien_pub
              FROM App:AvisPub a, App:DestinatairePub d, App:FormXmlDestinataireOpoce f
              WHERE a.id = d.idAvis AND a.organisme = d.organisme
              AND d.id = f.idDestinataireOpoce AND d.organisme = f.organisme
              AND d.idSupport = :idSupport
              AND d.etat = :etat
              AND f.lienPublication IS NOT NULL AND f.lienPublication != ''
              AND a.statut != :statut
              AND a.consultationId = :consultationId AND a.organisme = :organisme"
        )
            ->setParameter('idSupport', $data['idSupport'])
            ->setParameter('etat', $data['etat'])
            ->setParameter('statut', $data['statut'])
            ->setParameter('consultationId', $data['consultationId'])
            ->setParameter('organisme', $data['organisme']);

        $result = $query->getResult();

        return $result;
    }
}
