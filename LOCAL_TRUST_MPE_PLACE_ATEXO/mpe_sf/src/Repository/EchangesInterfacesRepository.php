<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\DBAL\Exception as DBALException;
use Doctrine\ORM\NoResultException;
use Exception;

/**
 * Class EchangesInterfacesRepository.
 */
class EchangesInterfacesRepository extends EntityRepository
{
    /**
     * @param $info
     *
     * @return mixed
     *
     * @throws NonUniqueResultException
     */
    public function getTotalNumberAndWeightFlux($info, $nbFlux = false)
    {
        $query = $this->createQueryBuilder('e');

        if (true === $nbFlux) {
            $query->select('SUM(e.nbFlux) as total');
        } else {
            $query->select('COUNT(e) as total');
        }

        $query->addSelect('SUM(e.poids) as weight')
            ->where('e.service = :service')
            ->andWhere('e.nomBatch = :nomBatch')
            ->andWhere('e.debutExecution >= :dateMin')
            ->andWhere('e.debutExecution < :dateMax')
            ->setParameter('service', $info['service'])
            ->setParameter('nomBatch', $info['nomBatch'])
            ->setParameter('dateMin', $info['dateMin']->format('Y-m-d H:i:s'))
            ->setParameter('dateMax', $info['dateMax']->format('Y-m-d H:i:s'));

        if (isset($info['codeRetour'])) {
            $query->andWhere('e.codeRetour = :codeRetour')
                ->setParameter('codeRetour', $info['codeRetour']);
        }

        if (isset($info['typeFlux'])) {
            $query->andWhere('e.typeFlux = :typeFlux')
                ->setParameter('typeFlux', $info['typeFlux']);
        }

        if (isset($info['idEchangesInterfaces'])) {
            $query->andWhere('e.idEchangesInterfaces IS NOT NULL');
        }

        $totalFlux = $query->getQuery()->getOneOrNullResult();

        if (empty($totalFlux['total'])) {
            $totalFlux['total'] = 0;
        }

        if (empty($totalFlux['weight'])) {
            $totalFlux['weight'] = 0;
        }

        $totalFlux['nomBatch'] = $info['nomBatch'];
        $totalFlux['service'] = $info['service'];

        return $totalFlux;
    }

    /**
     * @param $info
     * @param bool $idEchangesInterfaces
     *
     * @return mixed
     *
     * @throws NonUniqueResultException
     */
    public function getTotalCallOrFlux($info, $idEchangesInterfaces = false)
    {
        $query = $this->createQueryBuilder('e');

        $query->select('COUNT(e) as total')
            ->where('e.service = :service')
            ->andWhere('e.nomBatch = :nomBatch')
            ->andWhere('e.debutExecution >= :dateMin')
            ->andWhere('e.debutExecution < :dateMax')
            ->setParameter('service', $info['service'])
            ->setParameter('nomBatch', $info['nomBatch'])
            ->setParameter('dateMin', $info['dateMin']->format('Y-m-d H:i:s'))
            ->setParameter('dateMax', $info['dateMax']->format('Y-m-d H:i:s'));

        if (isset($info['codeRetour'])) {
            $query->andWhere('e.codeRetour IN (:codeRetour)')
                ->setParameter('codeRetour', $info['codeRetour']);
        }

        if (isset($info['nomInterface'])) {
            $query->andWhere('e.nomInterface = :nomInterface')
                ->setParameter('nomInterface', $info['nomInterface']);
        }

        if ($idEchangesInterfaces) {
            $query->andWhere('e.idEchangesInterfaces IS NOT NULL');
        }

        $totalCall = $query->getQuery()->getOneOrNullResult();

        if (empty($totalCall['total'])) {
            $totalCall['total'] = 0;
        }

        $totalCall['nomBatch'] = $info['nomBatch'];
        $totalCall['service'] = $info['service'];

        if (isset($info['nomInterface'])) {
            $totalCall['nomInterface'] = $info['nomInterface'];
        }

        return $totalCall;
    }

    /**
     * @throws Exception
     */
    public function purge(string $ttl)
    {
        try {
            $em = $this->getEntityManager();
            $this->foreignKeyChecks(0);
            $em->beginTransaction();
            $sql = 'DELETE FROM echanges_interfaces where debut_execution < NOW() - INTERVAL :ttl DAY;';
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->bindParam(':ttl', $ttl);
            $stmt->execute();
            $em->commit();
        } catch (Exception $e) {
            $em->rollback();
            throw $e;
        }
        try {
            $this->foreignKeyChecks(1);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $val
     *
     * @throws DBALException
     */
    public function foreignKeyChecks($val)
    {
        $check = 1;
        if (0 === $val) {
            $check = 0;
        }
        $em = $this->getEntityManager();
        $sql = 'SET FOREIGN_KEY_CHECKS = '.$check;
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
    }

    /**
     * @return mixed
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function countAllRecords()
    {
        try {
            $qb = $this->createQueryBuilder('e');
            $qb->select('COUNT(e) as total');

            return $qb->getQuery()->getSingleScalarResult();
        } catch (Exception $e) {
            throw $e;
        }
    }
}
