<?php

namespace App\Repository;

use App\Entity\Consultation;
use App\Entity\PanierEntreprise;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Repository PanierEntreprise.
 *
 * @author     Oumar KONATE <oumar.konate@atexo.com>
 *
 * @version    0.0
 *
 * @since      0.0
 *
 * @copyright  Atexo 2015
 */
class PanierEntrepriseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PanierEntreprise::class);
    }

    public function getPanierEntreprise($organisme, $consultationId, $idEntreprise, $idInscrit)
    {
        $panierEntreprise = $this->findOneBy(['organisme' => $organisme,
            'consultationId' => $consultationId, 'idEntreprise' => $idEntreprise, 'idInscrit' => $idInscrit, ]);

        return $panierEntreprise;
    }

    public function addConsultationToPanierEntreprise($organisme, $consultationId, $idEntreprise, $idInscrit)
    {
        $panierEntreprise = new PanierEntreprise();
        $panierEntreprise->setOrganisme($organisme);
        $panierEntreprise->setConsultationId($consultationId);
        $panierEntreprise->setIdEntreprise($idEntreprise);
        $panierEntreprise->setIdInscrit($idInscrit);
        $panierEntreprise->setDateAjout(date('Y-m-d H:i:s'));
        $this->_em->persist($panierEntreprise);
        $this->_em->flush();
    }

    /**
     * @param $idInscrit
     *
     * @return array
     */
    public function getRefAndIdConsMessV2FromPanier($idInscrit)
    {
        $qb = $this->createQueryBuilder('Pe')
            ->leftJoin(
                Consultation::class,
                'Cons',
                Join::WITH,
                'Pe.consultationId = Cons.id'
            )
            ->select('Cons.id,Cons.referenceUtilisateur')
            ->where('Pe.idInscrit = :inscritId')
            ->andWhere('Cons.versionMessagerie = :versionMessagerie')
            ->setParameter('inscritId', $idInscrit)
            ->setParameter('versionMessagerie', 2);

        return $qb->getQuery()->getArrayResult();
    }
}
