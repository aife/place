<?php

namespace App\Repository\Consultation;

use App\Entity\Consultation\AutrePieceConsultation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class AutrePieceConsultationRepository.
 */
class AutrePieceConsultationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AutrePieceConsultation::class);
    }
}
