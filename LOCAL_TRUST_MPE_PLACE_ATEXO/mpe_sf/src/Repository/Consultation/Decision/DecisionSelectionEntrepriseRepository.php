<?php

namespace App\Repository\Consultation\Decision;

use App\Entity\Consultation\Decision\DecisionSelectionEntreprise;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class DecisionSelectionEntrepriseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DecisionSelectionEntreprise::class);
    }
}
