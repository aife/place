<?php

namespace App\Repository\EchangeDocumentaire;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * EchangeDocTypePieceActesRepository.
 */
class EchangeDocTypePieceActesRepository extends EntityRepository
{
    /**
     * @return int|mixed|string
     */
    public function findAllToArray()
    {
        return $this->createQueryBuilder('edtpa')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }
}
