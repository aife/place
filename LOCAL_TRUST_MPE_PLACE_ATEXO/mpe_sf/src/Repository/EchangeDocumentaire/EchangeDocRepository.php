<?php

namespace App\Repository\EchangeDocumentaire;

use Doctrine\ORM\QueryBuilder;
use App\Entity\Consultation;
use App\Entity\ContactContrat;
use App\Entity\EchangeDocumentaire\EchangeDocApplication;
use App\Entity\EchangeDocumentaire\EchangeDocApplicationClient;
use App\Entity\TypeContrat;
use App\Entity\TypeProcedure;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Serializer\Serializer;

/**
 * Class EchangeDocRepository.
 */
class EchangeDocRepository extends EntityRepository
{
    public string $headWs = 'echanges';

    public final const ID = 'id';
    public final const STATUT = 'statut';
    public final const ORGANISME = 'organisme';
    public final const CONSULTATION = 'consultation';
    public final const UPDATED_AT = 'updatedAt';
    public final const DATE_MODIFICATION_MIN = 'dateModificationMin';
    public final const DATE_MODIFICATION_MAX = 'dateModificationMax';
    public final const CODE = 'code';
    public final const CODE_APPLICATION = 'codeApplication';
    public final const CODE_APPLICATION_CLIENT = 'codeApplicationClient';

    protected ?QueryBuilder $queryBuilder = null;

    public function findAllByParamWs(
        $nombreElementsParPage,
        $NumeroPage,
        $dateModification,
        $module,
        $orgServicesAllowed = [],
        $tabParams = [],
        $searchId = false
    ) {
        $this->queryBuilder = $this->createQueryBuilder('ed');
        $this->queryBuilder->select('ed, tp.abbreviationPortailAnm');
        $this->queryBuilder->leftJoin(
            Consultation::class,
            'c',
            'WITH',
            'c.id = ed.consultation'
        );

        $this->queryBuilder->leftJoin(
            TypeContrat::class,
            'tc',
            'WITH',
            'tc.idTypeContrat = c.typeMarche '
        );

        $this->queryBuilder->leftJoin(
            TypeProcedure::class,
            'tp',
            'WITH',
            'tp.idTypeProcedure = c.idTypeProcedure '
        );

        $this->queryBuilder->leftJoin(
            EchangeDocApplicationClient::class,
            'edac',
            'WITH',
            'ed.echangeDocApplicationClient = edac.id '
        );

        $this->queryBuilder->leftJoin(
            EchangeDocApplication::class,
            'eda',
            'WITH',
            'edac.echangeDocApplication = eda.id '
        );

        $this->setFilter(
            $tabParams,
            self::ID,
            self::ID,
            self::ID,
            'ed'
        );

        $this->setFilter(
            $tabParams,
            self::STATUT,
            self::STATUT,
            self::STATUT,
            'ed'
        );

        $this->setFilter(
            $tabParams,
            self::ORGANISME,
            self::ORGANISME,
            self::ORGANISME,
            'c'
        );

        $this->setFilter(
            $tabParams,
            self::CONSULTATION,
            self::CONSULTATION,
            'idConsultation',
            'ed'
        );

        if (array_key_exists(self::DATE_MODIFICATION_MIN, $tabParams)) {
            $tabParams[self::DATE_MODIFICATION_MIN] = $tabParams[self::DATE_MODIFICATION_MIN].' 00:00:00';
        }

        if (array_key_exists(self::DATE_MODIFICATION_MAX, $tabParams)) {
            $tabParams[self::DATE_MODIFICATION_MAX] = $tabParams[self::DATE_MODIFICATION_MAX].' 23:59:59';
        }

        $this->setFilter(
            $tabParams,
            self::UPDATED_AT,
            self::DATE_MODIFICATION_MIN,
            self::DATE_MODIFICATION_MIN,
            'ed',
            '>'
        );

        $this->setFilter(
            $tabParams,
            self::UPDATED_AT,
            self::DATE_MODIFICATION_MAX,
            self::DATE_MODIFICATION_MAX,
            'ed',
            '<'
        );

        $this->setFilter(
            $tabParams,
            self::CODE,
            self::CODE_APPLICATION,
            self::CODE_APPLICATION,
            'eda'
        );

        $this->setFilter(
            $tabParams,
            self::CODE,
            self::CODE_APPLICATION_CLIENT,
            self::CODE_APPLICATION_CLIENT,
            'edac'
        );

        $limit = $nombreElementsParPage;
        $offset = $this->getOffset($NumeroPage, $nombreElementsParPage);
        $this->queryBuilder->setFirstResult($offset);
        $this->queryBuilder->setMaxResults($limit);

        $echangeDocumentaires = $this->queryBuilder->getQuery()->getResult();

        $typeProcedureRepo = $this->getEntityManager()->getRepository(TypeProcedure::class);
        foreach ($echangeDocumentaires as $echangeDocumentaire) {
            $abbreviationPortailAnm = $echangeDocumentaire['abbreviationPortailAnm'];
            $echangeDocumentaire = $echangeDocumentaire[0];
            $consultation = $echangeDocumentaire->getConsultation();
            $typeProcedure = $typeProcedureRepo->findOneBy([
                'abbreviationPortailAnm' => $abbreviationPortailAnm
            ]);
            $consultation->setTypeProcedure($typeProcedure);

            yield $echangeDocumentaire;
            $this->_em->detach($echangeDocumentaire);
        }
    }

    /**
     * @param $tabParams
     * @param $champ
     * @param $key
     * @param $alias
     * @param string $comparateur
     */
    protected function setFilter($tabParams, $champ, $variable, $key, $alias, $comparateur = '=')
    {
        if (array_key_exists($key, $tabParams)) {
            $this->queryBuilder->andWhere($alias.'.'.$champ.' '.$comparateur.' :'.$variable.'')
                ->setParameter($variable, $tabParams[$key]);
        }
    }

    public function getOffset($page, $limit)
    {
        $offset = 0;
        if (0 != $page && 1 != $page) {
            $offset = ($page - 1) * $limit;
        }

        return $offset;
    }

    public function getNode(ContactContrat $contact, Serializer $serializer, $mode)
    {
        $contactNormalize = $serializer->normalize($contact, $mode, ['groups' => ['webservice']]);
        $content = $serializer->serialize($contactNormalize, $mode);
        $content = str_replace('<?xml version="1.0"?>', '', $content);
        $content = str_replace("\n", '', $content);
        $content = str_replace('response>', 'contact>', $content);

        return $content;
    }

    /**
     * @param $consultationId
     * @param array $tabParams
     *
     * @return int|mixed|string
     */
    public function filterEchange($consultationId, $tabParams = [])
    {
        $this->queryBuilder = $this->createQueryBuilder('ed');
        $this->queryBuilder->where('ed.consultation = :consultation');
        $this->queryBuilder->setParameter('consultation', $consultationId);
        $this->queryBuilder->orderBy('ed.updatedAt', 'desc');

        if (array_key_exists('listeStatut', $tabParams)) {
            $this->filterStatut($tabParams['listeStatut']);
        }

        return $this->queryBuilder->getQuery()->getResult();
    }

    /**
     * @param $statut
     */
    public function filterStatut($statut)
    {
        if ($statut) {
            $this->queryBuilder->andwhere('ed.statut in (:statut)');
            $this->queryBuilder->setParameter('statut', $statut);
        }
    }
}
