<?php

namespace App\Repository\EchangeDocumentaire;

use App\Entity\EchangeDocumentaire\EchangeDocApplication;
use App\Entity\EchangeDocumentaire\EchangeDocApplicationClient;
use App\Entity\EchangeDocumentaire\EchangeDocApplicationClientOrganisme;
use App\Entity\Organisme;
use Doctrine\ORM\EntityRepository;

/**
 * Class EchangeDocApplicationClientRepository.
 */
class EchangeDocApplicationClientRepository extends EntityRepository
{
    /**
     * @return mixed
     */
    public function getAuthorizedApplicationClientAvailable(Organisme $organisme)
    {
        return $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.actif = 1')
            ->leftJoin(EchangeDocApplicationClientOrganisme::class, 'eo', 'WITH', '( eo.echangeDocApplicationClient = e.id )')
            ->andWhere('eo.organisme = :organisme')
            ->setParameter('organisme', $organisme->getId())
            ->getQuery()->getResult();
    }

    /**
     * @return mixed
     */
    public function getAuthorizedApplicationClientAll()
    {
        return $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.actif = 1')
            ->leftJoin(EchangeDocApplicationClientOrganisme::class, 'eo', 'WITH', '( eo.echangeDocApplicationClient = e.id )')
            ->andWhere('eo.organisme IS NULL')
            ->getQuery()->getResult();
    }

    public function getAvailabledApplicationClientForAutomatedEchanges(Organisme $organisme)
    {
        return $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.actif = 1')
            ->andWhere('e.envoiAutoArchivage = true')
            ->leftJoin(EchangeDocApplicationClientOrganisme::class, 'eo', 'WITH', '( eo.echangeDocApplicationClient = e.id )')
            ->andWhere('(eo.organisme = :organisme OR eo.organisme IS NULL)')
            ->setParameter('organisme', $organisme->getId())
            ->leftJoin(EchangeDocApplication::class, 'eda', 'WITH', '( e.echangeDocApplication = eda.id )')
            ->andWhere('eda.envoiDic = 1')
            ->getQuery()->getResult();
    }
}
