<?php

namespace App\Repository\EchangeDocumentaire;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * EchangeDocTypePieceStandardRepository.
 */
class EchangeDocTypePieceStandardRepository extends EntityRepository
{
    /**
     * @return int|mixed|string
     */
    public function findAllToArray()
    {
        return $this->createQueryBuilder('edtps')
            ->orderBy('edtps.libelle', 'asc')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }
}
