<?php

namespace App\Repository\EchangeDocumentaire;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\EntityRepository;

/**
 * Class EchangeDocBlobRepository.
 */
class EchangeDocBlobRepository extends EntityRepository
{
    /**
     * @param $id
     * @param $echangeId
     *
     * @return int|mixed|string
     *
     * @throws NonUniqueResultException
     */
    public function getDocumentIfExists($id, $echangeId)
    {
        return $this->createQueryBuilder('e')
            ->select('e.chemin, b.name, b.organisme, b.chemin as chemin_blob, b.id as blobOrganismeId')
            ->leftJoin('e.blobOrganisme', 'b')
            ->where('e.id = :id')
            ->andWhere('e.echangeDoc = :idEchange')
            ->setParameter('id', $id)
            ->setParameter('idEchange', $echangeId)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
