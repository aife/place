<?php

namespace App\Repository\EchangeDocumentaire;

use Doctrine\ORM\EntityRepository;

/**
 * Class EchangeDocHistoriqueRepository.
 */
class EchangeDocHistoriqueRepository extends EntityRepository
{
    /**
     * @param $echangeId
     *
     * @return int|mixed|string
     */
    public function getHistorique($echangeId)
    {
        return $this->createQueryBuilder('h')
            ->select('h.id, a.nom, h.statut, h.messageFonctionnel, h.messageTechnique')
            ->leftJoin('h.echangeDoc', 'e')
            ->leftJoin('h.agent', 'a')
            ->where('e.id = :echangeId')
            ->setParameter('echangeId', $echangeId)
            ->getQuery()
            ->getResult();
    }
}
