<?php

namespace App\Repository;

use App\Entity\FormePrixPuHasRefVariation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FormePrixPuHasRefVariation>
 *
 * @method FormePrixPuHasRefVariation|null find($id, $lockMode = null, $lockVersion = null)
 * @method FormePrixPuHasRefVariation|null findOneBy(array $criteria, array $orderBy = null)
 * @method FormePrixPuHasRefVariation[]    findAll()
 * @method FormePrixPuHasRefVariation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormePrixPuHasRefVariationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FormePrixPuHasRefVariation::class);
    }
}
