<?php

namespace App\Repository;

use App\Entity\DerniersAppelsValidesWsSgmapDocuments;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class DerniersAppelsValidesWsSgmapDocumentsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DerniersAppelsValidesWsSgmapDocuments::class);
    }
}
