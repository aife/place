<?php

namespace App\Repository;

use App\Entity\ReferentielHabilitation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ReferentielHabilitation>
 *
 * @method ReferentielHabilitation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReferentielHabilitation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReferentielHabilitation[]    findAll()
 * @method ReferentielHabilitation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReferentielHabilitationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReferentielHabilitation::class);
    }
}
