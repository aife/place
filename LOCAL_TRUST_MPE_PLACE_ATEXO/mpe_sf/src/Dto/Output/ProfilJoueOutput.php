<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

final class ProfilJoueOutput
{
    public ?string $agent = null;
    public ?string $organisme = null;
    public string $nomOfficiel;
    public string $pays;
    public string $ville;
    public string $adresse;
    public ?string $codePostal = null;
    public string $pointContact;
    public string $aAttentionDe;
    public ?string $telephone = null;
    public ?string $fax = null;
    public ?string $email = null;
    public ?string $adressePouvoirAdjudicateur = null;
    public ?string $adresseProfilAcheteur = null;
    public ?int $autoriteNationale = null;
    public ?int $officeNationale = null;
    public ?int $collectiviteTerritoriale = null;
    public ?int $officeRegionale = null;
    public ?int $organismePublic = null;
    public ?int $organisationEuropenne = null;
    public ?int $autreTypePouvoirAdjudicateur = null;
    public ?int $servicesGeneraux = null;
    public ?int $defense = null;
    public ?int $securitePublic = null;
    public ?int $environnement = null;
    public ?int $developpementCollectif = null;
    public ?int $affairesEconomiques = null;
    public ?int $sante = null;
    public ?int $protectionSociale = null;
    public ?int $loisirs = null;
    public ?int $education = null;
    public ?int $autreActivitesPrincipales = null;
    public ?int $pouvoirAdjudicateurAgit = null;
    public ?int $pouvoirAdjudicateurMarcheCouvert = null;
    public ?int $entiteAdjudicatriceMarcheCouvert;
    public ?string $autreLibelleTypePouvoirAdjudicateur = null;
    public ?string $autreLibelleActivitesPrincipales = null;
    public ?string $numeroNationalIdentification = null;
}
