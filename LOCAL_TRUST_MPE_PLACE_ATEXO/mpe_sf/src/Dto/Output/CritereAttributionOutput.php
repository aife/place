<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

use Doctrine\Common\Collections\Collection;

final class CritereAttributionOutput
{
    public ?string $donneeComplementaire;
    public ?string $lot;
    public ?string $enonce;
    public ?int $ordre;
    public ?string $ponderation;
    public array $sousCritereAttributions;
}
