<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

use DateTimeInterface;

final class ContratTitulaireOutput
{
    public ?string $organisme;

    public ?int $idTitulaire;

    public int $horsPassation;

    public int $id;

    public ?string $numero;

    public ?string $numeroLong;

    public ?string $referenceLibre;

    public ?string $objet;

    public ?int $idService;

    public ?int $idCreateur;

    public ?string $formePrix = null;

    public ?string $modaliteRevisionPrix = null;

    public string|int|null $defenseOuSecurite;

    public ?DateTimeInterface $datePrevisionnelleNotification;

    public ?DateTimeInterface $datePrevisionnelleFinMarche;

    public ?DateTimeInterface $datePrevisionnelleFinMaximaleMarche;

    public ?int $dureeMaximaleMarche;

    public ?DateTimeInterface $dateDebutExecution;

    public array $lieuExecution;

    public ?DateTimeInterface $decisionAttribution;

    public ?string $intitule;

    public ?string $trancheBudgetaire;

    public ?string $ccagApplicable;

    // Iri de référence TypeContrat
    public ?string $typeContrat = null;

    public ?DateTimeInterface $dateModification;

    public ?DateTimeInterface $dateCreation;

    public ?DateTimeInterface $dateNotification;

    public ?DateTimeInterface $dateFin;

    public ?DateTimeInterface $dateMaxFin;

    public ?string $statut;

    public ?string $naturePrestation;

    public ?int $idChapeau;

    public bool $chapeau;

    public ?int $idEtablissementTitulaire;

    public float|int|null $montant;

    public ?int $idContact;

    public array $cpv;

    public array $contratTransverse;

    // Iri de référence Entreprise
    public ?string $entreprise;

    // Iri de référence Etablissement
    public ?string $etablissement;

    public ?array $clauses = null;

    public ?string $numEj = null;
    public ?string $statutEj = null;
    public ?int $lienAcSad = null;
}
