<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

final class TypeContratOutput
{
    public string $codeExterne;

    public string $libelle;

    public string $untranslatedLibelle;

    public ?string $abreviation;

    public string $idExterne;

    public ?string $typeTechniqueAchat;

    public ?string $associationProcedures = null;

    public ?string $accordCadreSad = null;

    public ?string $marcheSubsequent = null;
}
