<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

final class TypeProcedureOrganismeOutput
{

    public string $organisme;    /**
     * @var string|null
     */
    public ?string $libelle;

    /**
     * @var string|null
     */
    public ?string $abreviation;

    public ?string $abreviationInterface;

    public ?bool $procedureSimplifie;

    /**
     * @var string|null
     */
    public ?string $idExterne;
}
