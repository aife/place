<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

final class OrganismeOutput
{
    /**
     * @var int
     */
    public int $id;

    /**
     * @var string
     */
    public string $sigle;

    /**
     * @var string|null
     */
    public ?string $denomination;

    public ?string $siret;

    /**
     * @var string|null
     */
    public ?string $description;

    /**
     * @var string
     */
    public string $acronyme;

    /**
     * @var string
     */
    public string $adresse;

    /**
     * @var string
     */
    public string $codePostal;

    /**
     * @var string
     */
    public string $ville;

    /**
     * @var string
     */
    public string $idExterne;

    /**
     * Référence le champs categorieInsee
     */
    public ?string $formeJuridique;
}
