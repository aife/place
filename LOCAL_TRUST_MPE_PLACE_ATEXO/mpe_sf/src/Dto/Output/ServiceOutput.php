<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

final class ServiceOutput
{
    public ?string $sigle;

    public ?string $siren;

    public ?string $siret;

    public ?string $libelle;

    public string $adresse;

    public string $codePostal;

    public string $ville;

    public ?string $idParent = null;

    public ?string $idExterne;

    public ?int $oldId;
}
