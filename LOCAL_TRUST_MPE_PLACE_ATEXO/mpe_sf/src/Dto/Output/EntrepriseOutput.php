<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

final class EntrepriseOutput
{
    public int $id;

    public ?string $siren;

    public ?string $telephone;

    public ?string $siteInternet;

    public string|int $idExterne;

    public array $adresse;

    public string $formeJuridique;

    public string $codeAPE;

    public ?string $libelleAPE;

    public ?string $dateModification;

    public ?string $dateCreation;

    public ?string $capitalSocial;

    public ?string $raisonSociale;
}
