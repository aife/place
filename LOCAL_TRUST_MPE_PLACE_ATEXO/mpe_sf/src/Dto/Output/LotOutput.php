<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

final class LotOutput
{
    public string $consultation;

    public string $donneeComplementaire;

    public string $numero;

    public string $intitule;

    public string $descriptionSuccinte;

    public string $naturePrestation;

    public ?string $codeCpvPrincipal;

    public ?string $codeCpvSecondaire1 = null;

    public ?string $codeCpvSecondaire2 = null;

    public ?string $codeCpvSecondaire3 = null;

    public ?string $decision = null;

    public ?string $typeDecision;

    public ?\DateTimeInterface $dateDecision;
    public ?int $idTraductionDescription = null;

    public ?int $idTraductionDescriptionDetail = null;

    public ?array $clauses = null;

    public ?bool $marcheInsertion = null;

    public ?bool $clauseSpecificationTechnique = null;
}
