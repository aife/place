<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\BloborganismeFile;

class FichierEnveloppeOutput
{
    #[ApiProperty(example: '/api/v2/media_objects/123')]
    public string $idFichier;

    #[ApiProperty(
        description: "Correspondance :\n\n| Code | Valeur |
            |--|--|
            | PRI | Pièce libre |
            | ACE | Acte d'engagement |
            | AFI | Annexe financière |
            | SIG | Jeton de signature |",
        example: 'PRI'
    )]
    public string $typeFichier;

    #[ApiProperty(example: '/api/v2/media_objects/125')]
    public ?string $idSignature;
}
