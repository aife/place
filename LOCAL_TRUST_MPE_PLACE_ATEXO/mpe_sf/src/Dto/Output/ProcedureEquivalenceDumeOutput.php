<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

class ProcedureEquivalenceDumeOutput
{
    public string $organisme;
    public string $typePrecedureDume;
    public string $labelTypePrecedureDume;
    public bool $selected;
    public bool $display;
    public bool $fixed;
}
