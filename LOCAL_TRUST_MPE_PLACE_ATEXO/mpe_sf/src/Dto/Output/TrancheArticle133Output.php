<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

final class TrancheArticle133Output
{
    public string $organisme;

    public string $libelleTrancheBudgetaire;
}
