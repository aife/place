<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\EnveloppeFichier;
use App\Entity\Offre;

class EnveloppeOutput
{
    #[ApiProperty(example: '/api/v2/offres/123')]
    public string $offre;

    #[ApiProperty(example: 'ANONYMAT')]
    public string $type;

    #[ApiProperty(example: 1)]
    public int $numeroLot;

    #[ApiProperty(example: 1)]
    /** @var EnveloppeFichier[] */
    public array $fichiersEtSignature;

}
