<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

final class ContactContratOutput
{
    public ?string $nom = null;

    public ?string $prenom = null;

    public ?string $email = null;

    public ?string $telephone = null;
}
