<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

class FormePrixOutput
{
    public string $typeFormePrix;
    public ?string $variationPrixForfaitaire;
    public ?string $modalite;
    public ?string $uniteMinMax;
    public ?float $puMin;
    public ?float $puMax;
    public ?string $variationPrixUnaitaire;
    public ?array $typePrix;
    public ?string $donneesComplementaire;
}
