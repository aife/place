<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

use DateTimeInterface;

final class ConsultationOutput
{
    public int $id;

    public string $organisme;

    public bool $organismeDecentralise;

    public ?string $directionService;

    public ?string $naturePrestation;

    public string $typeProcedure;

    public string $donneeComplementaire;

    public string $typeContrat;

    public string $reference;

    public string $intitule;

    public string $objet;

    public string $commentaireInterne;

    public string $dateLimiteRemisePlis;

    public ?DateTimeInterface $dateDebut;

    public ?string $codeExterne = null;

    // Iri de référence Consultation
    public ?string $consultationParent = null;

    public DateTimeInterface|string|null $datefinAffichage;

    public string $dateLimiteRemiseOffres;

    public ?DateTimeInterface $dateMiseEnLigneCalcule;

    public int $idCreateur;

    public ?string $statutCalcule = null;

    public bool $dume;

    public ?string $typeProcedureDume;

    public ?bool $dumeSimplifie;

    public array $lieuxExecution;

    public ?array $codesNuts = null;

    public ?string $codeCpvPrincipal;

    public ?string $codeCpvSecondaire1 = null;

    public ?string $codeCpvSecondaire2 = null;

    public ?string $codeCpvSecondaire3 = null;

    public bool $bourseCotraitance;

    public bool $chiffrement;

    public ?string $signatureElectronique = null;

    public ?string $reponseElectronique = null;

    public bool $enveloppeCandidature;

    public bool $enveloppeAnonymat;

    public bool $enveloppeOffre;

    public bool $enveloppeOffreTechnique;

    public bool $alloti;

    public ?int $nombreLots;

    public ?bool $entiteAdjudicatrice = null;

    public ?array $clauses = null;

    public string $urlConsultation;

    public string|int $poursuivreAffichage;

    public string $poursuivreAffichageUnite;

    public DateTimeInterface|string|null $dateModification;

    public ?string $modeOuvertureReponse;

    public ?string $signatureActeEngagement;

    public ?string $marchePublicSimplifie;

    public ?string $codeProcedure;

    public string|null $urlConsultationExterne;

    public ?bool $consultationExterne;

    public ?string $typeDecisionDeclarationSansSuite;

    public ?string $typeDecisionDeclarationInfructueux;

    public ?string $typeDecisionARenseigner;

    public ?string $typeDecisionAutre;

    public ?string $typeDecisionAttributionMarche;

    public ?string $typeDecisionAttributionAccordCadre;

    public ?string $typeDecisionAdmissionSad;

    public ?DateTimeInterface $dateDecision;
    public ?array $departmentsWithNames;

    public ?string $typeDecision;

    public ?string $modaliteRetraitDossier = null;
    public ?string $receptionOffresCandidatures = null;
    public ?string $lieuOuvertureOffresRemiseCandidatures = null;

    public DateTimeInterface|string|null $dateValidation = null;

    public ?bool $etatValidation = null;

    public ?bool $donneeComplementaireObligatoire;

    public ?bool $donneePubliciteObligatoire;

    public ?bool $attestationConsultation = null;

    public ?bool $consultationRestreinte;

    public ?float $valeurEstimee;

    public ?string $autreTechniqueAchat;

    public ?string $numeroSad;

    public ?bool $procedureOuverte;

    public ?bool $groupement;

    public ?string $justificationNonAlloti;

    public ?string $referentielAchat;

    public ?string $adresseRetraisDossiers;

    public ?string $adresseDepotOffres;

    public ?string $lieuOuverturePlis;

    public bool $besoinRecurrent;

    public ?int $idContratTitulaire = null;

    public string $uuid;

    public DateTimeInterface|string|null $dateLimiteRemisePlisLocale;

    public ?string $lieuResidence;

    public ?array $lieuxWithCodeInterface;

    public ?string $sourceExterne;

    public ?int $idSourceExterne;

    public ?int $idDossierVolumineux;

    public ?int $isEnvoiPubliciteValidation;

    public ?int $annexeFinanciere;

    public ?int $cmsActif;

    public ?int $controleTailleDepot;

    public ?string $numeroProjetAchat;

    public ?int $marcheInsertion;

    public ?string $clauseSpecificationTechnique;

    public ?int $oldServiceId;

    public ?bool $dcePartialDownload = null;
}
