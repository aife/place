<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

use DateTimeInterface;

final class EtablissementOutput
{
    public int $id;

    public ?string $codeEtablissement;

    public string|int $idExterne;

    public string $siege;

    public ?string $siret;

    public ?DateTimeInterface $dateCreation;

    public ?DateTimeInterface $dateModification;

    public array $adresse;
}
