<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

final class AcheteurPublicOutput
{
    public string $login;

    public string $password;

    public string $email;

    public ?string $token;

    public int $moniteurProvenance;
}
