<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

class TrancheOutput
{
    public int $codeTranche;
    public string $natureTranche;
    public string $intituleTranche;
    public ?string $donneesComplementaire;
    public ?string $formePrix;
}
