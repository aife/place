<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

class AgentOutput
{
    public int $id;
    public string $login;
    public string $email;
    public ?string $nom;
    public ?string $prenom;
    public ?string $organisme;
    public ?string $dateCreation;
    public ?string $dateModification;
    public ?string $idExterne;
    public bool $actif;
    public ?string $telephone;
    public ?string $fax;
    public ?string $profil;
    public ?string $service;
    public ?string $libelleDetail;
    public ?array $lieuExecution;
    public ?array $modulesOrganisme;
}
