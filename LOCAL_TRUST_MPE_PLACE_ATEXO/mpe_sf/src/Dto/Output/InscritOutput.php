<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

use DateTimeInterface;

final class InscritOutput
{
    public int $identifiantTechnique;

    public ?string $login;

    public string $nom;

    public string $prenom;

    public string $email;

    public array $adresse;

    public string $telephone;

    public bool $actif;

    public string $siren;

    public string $codeEtablissement;

    public string $inscritAnnuaireDefense;

    // Iri de référence Etablissement
    public ?string $etablissement;

    // Iri de référence Entreprise
    public ?string $entreprise;

    public ?DateTimeInterface $dateModificationRgpd;

    public ?array $rgpd;

    public ?string $idExterne;
}
