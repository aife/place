<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output\Referentiel;

final class ClausesN1Output
{
    public string $label;

    public ?string $slug;

    public ?string $tooltip;

    public bool $actif;
}
