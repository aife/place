<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output\Referentiel;

use App\Entity\Referentiel\Consultation\ClausesN3;

final class ClausesN4Output
{
    public string $label;

    public ?string $slug;

    public ?string $tooltip;

    public string $typeChamp;

    public bool $actif;

    public string $clauseN3;
}
