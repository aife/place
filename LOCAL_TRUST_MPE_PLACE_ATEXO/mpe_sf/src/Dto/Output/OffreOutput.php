<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Output;

use DateTimeInterface;
use ApiPlatform\Core\Annotation\ApiProperty;

final class OffreOutput
{
    #[ApiProperty(example: '/api/v2/consultations/123')]
    public string $consultation;

    #[ApiProperty(example: '/api/v2/inscrits/123')]
    public string $inscrit;

    #[ApiProperty(
        example: '99',
        description: "Correspondance :\n\n| Code | Valeur |
        |--|--|
        | 1 | FERMEE |
        | 2 | OUVERTE_EN_LIGNE |
        | 3 | OUVERTE_HORS_LIGNE |
        | 4 | REFUSEE |
        | 5 | OUVERTE |
        | 6 | SUPPRIMEE |
        | 7 | OUVERTE_A_DISTANCE |
        | 8 | EN_COURS_CHIFFREMENT |
        | 9 | EN_COURS_DECHIFFREMENT |
        | 10 | EN_ATTENTE_CHIFFREMENT |
        | 11 | EN_COURS_FERMETURE |
        | 12 | EN_ATTENTE_FERMETURE |
        | 99 | BROUILLON |"
    )]
    public int $statut;

    public ?DateTimeInterface $dateDepot;

    public ?string $horodatage;

    public ?int $entrepriseId = null;

    public ?string $nomEntrepriseInscrit = null;
}
