<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Input;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\Organisme;
use Doctrine\ORM\Mapping as ORM;

class ServiceInput
{
    #[ORM\Column(type: 'integer')]
    #[ApiProperty(example: 0)]
    public int $idExterne = 0;
    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: "8865")]
    public ?string $idParent = null;
    /**
     * @var string
     */
    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'Poissy')]
    public ?string $ville = null;

    /**
     * @var string
     */
    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: '78300')]
    public ?string $codepostal = null;

    /**
     * @var string
     */
    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'rue des petits champs')]
    public ?string $adresse = null;
    /**
     * @var string
     */
    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'NON')]
    public ?string $libelle = null;
    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: '00027')]
    public ?string $complement = null;

    /**
     * @var string
     */
    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: '791918766')]
    public ?string $siren = null;
    /**
     * @var string
     */
    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'gogop@gmail.com')]
    public ?string $email = null;

    /**
     * @var string
     */
    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: '2222222')]
    public ?string $sigle = null;
    /**
     * @var string
     */
    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'BA')]
    public ?string $formeJuridique = null;
    /**
     * @var string
     */
    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: '48')]
    public ?string $formeJuridiqueCode = null;
    /**
     * @var Organisme
     */
    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: '/api/v2/referentiels/organismes/pmi-min-1')]
    public ?Organisme $organisme = null;
}
