<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Input;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\Entreprise;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

class EtablissementInput
{
    /**
     * @var string
     */
    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: "1288887")]
    public ?string $siret = null;

    /**
     * @var string
     */
    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: "Atexo")]
    public ?string $estSiege = null;

    /**
     * @var string
     */
    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: "8887894544")]
    public ?string $codeEtablissement = null;

    /**
     * @var int|null
     *
     */
    #[ORM\Column(type: 'integer')]
    #[ApiProperty(example: 1)]
    public ?int $idExterne = null;

    /**
     * @var int
     */
    #[ORM\Column(type: 'integer')]
    #[ApiProperty(example: 22)]
    public int $idEntreprise = 0;

    /**
     * @var Entreprise|null
     */
    #[ApiProperty(example: '/api/v2/entreprises/3')]
    public ?Entreprise $entreprise = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: "Rue du Montparnasse, Paris 14")]
    public ?string $adresse = null ;

    /**
     * @var string
     */
    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: "75014")]
    public ?string $codePostal = null;

    /**
     * @var string
     */
    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: "Paris")]
    public ?string $ville = null;

    /**
     * @var string
     */
    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: "France")]
    public ?string $pays = null;

    /**
     * @Assert\DateTime()
     */
    public ?\DateTime $dateCreation = null;
    /**
     * @Assert\DateTime()
     */
    public ?\DateTime $dateModification = null;
}
