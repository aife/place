<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Input;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\Agent;
use App\Entity\ContactContrat;
use App\Entity\ContratTitulaire;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Entity\TrancheArticle133;
use App\Entity\TypeContrat;
use App\Entity\ValeurReferentiel;
use App\Validator\Clauses;
use App\Validator\IsTitulaireEntrepriseValid;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\IsValidOrganisme;
use DateTime;
use App\Model\TypePrestation;

final class ContratTitulaireInput
{
    /**
     * @IsValidOrganisme()
     */
    #[ApiProperty(example: '/api/v2/referentiels/organismes/pmi-min-1')]
    public Organisme $organisme;

    #[ApiProperty(example: '/api/v2/contrat-titulaires/2')]
    public TypeContrat $typeContrat;

    #[ApiProperty(example: '/api/v2/etablissements/4')]
    public Etablissement $etablissement;

    #[ApiProperty(example: '/api/v2/referentiels/contact-contrats/1')]
    public ?ContactContrat $idContact;

    #[ApiProperty(example: '/api/v2/referentiels/services/1')]
    public ?Service $idService = null;

    #[ApiProperty(example: '/api/v2/referentiels/tranche-article133s/8651')]
    public ?TrancheArticle133 $trancheBudgetaire;

    #[ApiProperty(example: '/api/v2/referentiels/valeur-referentiels/id=1;idReferentiel=13')]
    public ?ValeurReferentiel $ccagApplicable;

    #[ApiProperty(example: '/api/v2/agents/1154')]
    public ?Agent $idCreateur = null;

    /**
     * @IsTitulaireEntrepriseValid()
     */
    #[ApiProperty(example: '/api/v2/entreprises/3')]
    public Entreprise $idTitulaire;

    #[ApiProperty(example: 0)]
    #[Assert\NotBlank]
    #[Assert\Choice(choices: ContratTitulaire::CHOICE_HORS_PASSATION, message: "La valeur que vous avez sélectionnée n'est pas un choix valide : {{ choices }}")]
    public int $horsPassation = 0;

    #[ApiProperty(example: '2016RA32GE')]
    #[Assert\Length(max: 265, maxMessage: "L'intitule ne peut pas avoir une taille supérieur à 265 caractères.")]
    public ?string $numero;

    #[ApiProperty(example: '2016RA32GEE54698546456')]
    #[Assert\Length(max: 45, maxMessage: "L'intitule ne peut pas avoir une taille supérieur à 45 caractères.")]
    public ?string $numeroLong;

    #[ApiProperty(example: 'ref')]
    #[Assert\Length(max: 45, maxMessage: "L'intitule ne peut pas avoir une taille supérieur à 45 caractères.")]
    public ?string $referenceLibre;

    #[ApiProperty(example: 'MARCHE POST PURGE BK')]
    public ?string $objet;

    #[ApiProperty(example: '1')]
    public string $formePrix = '';

    #[ApiProperty(
        description: " Choices :\n\n
        - 0
        - 1
        ",
        example: '0'
    )]
    #[Assert\Choice(choices: ContratTitulaire::CHOICE_MARCHE_DEFENSE, message: "La valeur que vous avez sélectionnée n'est pas un choix valide : {{ choices }}")]
    public string|int|null $defenseOuSecurite = '0';

    #[Assert\Type('\DateTimeInterface')]
    public ?DateTime $dateModification;

    #[Assert\Type('\DateTimeInterface')]
    public ?DateTime $dateCreation;

    #[Assert\Type('\DateTime')]
    public ?DateTime $datePrevisionnelleNotification;

    #[Assert\Type('\DateTime')]
    public ?DateTime $datePrevisionnelleFinMarche;

    #[Assert\Type('\DateTime')]
    public ?DateTime $datePrevisionnelleFinMaximaleMarche;

    #[Assert\Type('\DateTime')]
    public ?DateTime $dateDebutExecution;

    #[Assert\Type('\DateTime')]
    public ?DateTime $decisionAttribution;

    #[Assert\Type('\DateTime')]
    public ?DateTime $dateNotification;

    #[Assert\Type('\DateTime')]
    public ?DateTime $dateFin;

    #[Assert\Type('\DateTime')]
    public ?DateTime $dateMaxFin;

    #[ApiProperty(example: 10)]
    #[Assert\PositiveOrZero]
    public ?int $dureeMaximaleMarche;

    /**
     * @Assert\All({
     *     @Assert\Type("int")
     * })*/
    #[ApiProperty(example: [75, 92])]
    #[Assert\NotBlank]
    public ?array $lieuExecutions = null;

    #[ApiProperty(example: 'Marché à bons de commande')]
    #[Assert\Length(max: 256, maxMessage: "L'intitule ne peut pas avoir une taille supérieur à 256 caractères.")]
    public ?string $intitule;

    #[ApiProperty(
        description: " Choices :\n\n
        - 0,
        - 'STATUT_DECISION_CONTRAT',
        - 'STATUT_DONNEES_CONTRAT_A_SAISIR',
        - 'STATUT_NUMEROTATION_AUTONOME',
        - 'STATUT_NOTIFICATION_CONTRAT',
        - 'STATUT_NOTIFICATION_CONTRAT_EFFECTUEE'
        ",
        example: 'STATUT_NOTIFICATION_CONTRAT'
    )]
    #[Assert\NotBlank]
    #[Assert\Choice(choices: ContratTitulaire::CHOICE_STATUT_CONTRAT, message: "La valeur que vous avez sélectionnée n'est pas un choix valide : {{ choices }}")]
    public ?string $statut;

    #[ApiProperty(
        description: " Choices :\n\n
        - TRAVAUX
        - FOURNITURES
        - SERVICES
        ",
        example: 'TRAVAUX'
    )]
    #[Assert\Choice(choices: TypePrestation::TYPE_PRESTATIONS, message: "La valeur que vous avez sélectionnée n'est pas un choix valide : {{ choices }}")]
    public string $naturePrestation;

    #[ApiProperty(example: 10)]
    #[Assert\Positive]
    public ?int $idChapeau;

    #[ApiProperty(example: 15.78)]
    #[Assert\NotBlank]
    #[Assert\PositiveOrZero]
    public float|int|null $montant;

    #[ApiProperty(example: 412)]
    #[Assert\PositiveOrZero]
    public ?int $montantMaxEstime = null;

    #[ApiProperty(
        example:
        [
            'codePrincipal' => '16320000',
            'codeSecondaire1' => '#37000000#03100000#03400000#'
        ]
    )]
    public array $cpv = [];

    /**
     * @Clauses()*/
    #[ApiProperty(example: [
        [
            "referentielClauseN1" => "/api/v2/referentiels/clauses-n1s/1",
            "clausesN2"           => [
                [
                    "referentielClauseN2" => "/api/v2/referentiels/clauses-n2s/2",
                    "clausesN3"           => [
                        [
                            "referentielClauseN3" => "/api/v2/referentiels/clauses-n3s/2"
                        ],
                        [
                            "referentielClauseN3" => "/api/v2/referentiels/clauses-n3s/4"
                        ]
                    ]
                ]
            ]
        ]
    ])]
    #[Assert\NotBlank]
    public Collection|array|null $clauses = null;
}
