<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Input;

use DateTimeInterface;
use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\Consultation;
use App\Entity\Inscrit;
use Symfony\Component\Validator\Constraints as Assert;

final class OffreInput
{
    #[ApiProperty(example: '/api/v2/consultations/123')]
    #[Assert\NotBlank]
    public Consultation $consultation;

    #[ApiProperty(example: '/api/v2/inscrits/123')]
    #[Assert\NotBlank]
    public Inscrit $inscrit;

    #[Assert\Type('\DateTimeInterface')]
    public ?DateTimeInterface $dateDepot = null;

    #[Assert\NotBlank]
    public ?string $horodatage = null;
}
