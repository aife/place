<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Input;

use ApiPlatform\Core\Annotation\ApiProperty;
use Doctrine\ORM\Mapping as ORM;

final class OrganismeInput
{
    #[ORM\Column(type: 'integer')]
    #[ApiProperty(example: 0)]
    public ?int $idExterne = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'pmi-min-8')]
    public string $acronyme = '';

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'rue des petits champs')]
    public ?string $adresse = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: '78300')]
    public ?string $codepostal = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'Poissy')]
    public ?string $ville = null;

    #[ApiProperty(example: 'France')]
    #[ORM\Column(type: 'string')]
    public ?string $pays = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'orgorg@gmail.com')]
    public ?string $email = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'AgentTechniqueTNR')]
    public ?string $denominationOrg = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: '1')]
    public ?string $active = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'CCI')]
    public ?string $sigle = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'CCI')]
    public ?string $description = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'complement')]
    public ?string $complement = null;
}
