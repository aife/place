<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Input;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\Consultation\DonneeComplementaire;
use App\Entity\FormePrix;
use Symfony\Component\Validator\Constraints as Assert;

class TrancheInput
{
    #[ApiProperty(example: 0)]
    #[Assert\NotBlank]
    public int $codeTranche;
    #[ApiProperty(example: 'TF, TO ou TC')]
    #[Assert\NotBlank]
    #[Assert\Choice(['TF', 'TO', 'TC'])]
    public string $natureTranche;

    #[ApiProperty(example: 'Intitulé de la tranche')]
    #[Assert\NotBlank]
    public string $intituleTranche;

    #[ApiProperty(example: '/api/v2/donnee-complementaires/1')]
    public ?DonneeComplementaire $donneesComplementaire = null;

    #[ApiProperty(example: '/api/v2/forme-prixes/1')]
    public ?FormePrix $formePrix = null;
}
