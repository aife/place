<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Input;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

class InscritInput
{
    #[ApiProperty(example: '/api/v2/etablissements/15199')]
    public ?Etablissement $etablissement = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'mp3')]
    public ?string $login = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: '123456')]
    public ?string $mdp = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: '15')]
    public string $nom = '15';

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: '15')]
    public string $prenom = '15';

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'rue des petits champs')]
    public ?string $adresse = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: '78300')]
    public ?string $codepostal = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'Poissy')]
    public ?string $ville = null;

    #[ApiProperty(example: 'France')]
    #[ORM\Column(type: 'string')]
    public ?string $pays = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'googo@googo.gmail.com')]
    public ?string $email = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: '0606060606')]
    public ?string $telephone = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: '88888')]
    public ?string $siret = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: '1')]
    public ?string $bloque = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: '0')]
    public ?string $inscritAnnuaireDefense = null;

    #[ORM\Column(type: DateTimeInterface::class)]
    public ?DateTimeInterface $dateCreation = null;

    #[ORM\Column(type: DateTimeInterface::class)]
    public ?DateTimeInterface $dateModification = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: "ARGON2")]
    public ?string $typeHash = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: "00")]
    public ?string $idExterne = '0';

    #[ApiProperty(example: "/api/v2/entreprises/4")]
    public ?Entreprise $entreprise = null;
}
