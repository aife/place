<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Input;

use ApiPlatform\Core\Annotation\ApiProperty;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

final class EntrepriseInput
{
    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'ATEXO')]
    public ?string $nom = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: '791918766')]
    public ?string $siren = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: '0623236654')]
    public ?string $telephone = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'www.entreprise-italie.it')]
    public ?string $siteInternet = null;

    #[ORM\Column(type: 'integer')]
    #[ApiProperty(example: 0)]
    public int $idExterne = 0;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'rue des petits champs')]
    public ?string $adresse = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'Poissy')]
    public ?string $ville = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: '78300')]
    public ?string $codepostal = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'France')]
    public ?string $pays = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'FR')]
    public ?string $acronymePays = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'BA')]
    public ?string $formeJuridique = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'BA')]
    public ?string $codeAPE = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'Restauration traditionnelle')]
    public ?string $libelleApe = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'N.C.')]
    public ?string $capitalSocial = null;

    #[ORM\Column(type: 'string')]
    #[ApiProperty(example: 'Atexo')]
    public ?string $raisonSociale = null;

    #[ORM\Column(type: 'DateTimeInterface')]
    public ?\DateTimeInterface $dateCreation = null;

    #[ORM\Column(type: 'DateTimeInterface')]
    public ?\DateTimeInterface $dateModification = null;
}
