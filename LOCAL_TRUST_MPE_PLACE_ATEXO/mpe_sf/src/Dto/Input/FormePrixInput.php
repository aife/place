<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Input;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\Consultation\DonneeComplementaire;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class FormePrixInput
{
    #[ApiProperty(example: 'PU, PM ou PF')]
    #[Assert\NotBlank]
    #[Assert\Choice(['PU', 'PM', 'PF'])]
    public string $typeFormePrix;

    #[ApiProperty(example: 'Prix révisables, Prix actualisables, Prix fermes, Prix fermes et actualisables ou Prix actualisables et révisables')]
    #[Assert\Choice([
        'Prix révisables',
        'Prix actualisables',
        'Prix fermes',
        'Prix fermes et actualisables',
        'Prix actualisables et révisables',
        null
    ])]
    public ?string $variationPrixForfaitaire = null;

    #[ApiProperty(example: 'bon_commande ou quantite_definie')]
    #[Assert\Choice(['bon_commande', 'quantite_definie', null])]
    public ?string $modalite = null;

    #[ApiProperty(example: 'EUR HT, EUR TTC, Dossier(s) ou Jour(s)')]
    #[Assert\Choice([
        'EUR HT',
        'EUR TTC',
        'Dossier(s)',
        'Jour(s)',
        null
    ])]
    public ?string $uniteMinMax = null;

    #[ApiProperty(example: 444.45)]
    public ?float $puMin = null;

    #[ApiProperty(example: 55.45)]
    public ?float $puMax = null;

    #[ApiProperty(example: 'Prix révisables, Prix actualisables, Prix fermes, Prix fermes et actualisables ou Prix actualisables et révisables')]
    #[Assert\Choice([
        'Prix révisables',
        'Prix actualisables',
        'Prix fermes',
        'Prix fermes et actualisables',
        'Prix actualisables et révisables',
        null
    ])]
    public ?string $variationPrixUnaitaire = null;

    #[ApiProperty(example: ['Prix sur catalogues', 'Bordereau de prix', 'Autre'])]
    #[Assert\All(
        new Assert\Choice([
            'Prix sur catalogues',
            'Bordereau de prix',
            'Autre',
            null
        ])
    )]
    public ?array $typePrix = null;

    #[ApiProperty(example: '/api/v2/donnee-complementaires/1')]
    public ?DonneeComplementaire $donneesComplementaire = null;

    #[Assert\Callback]
    public function validate(ExecutionContextInterface $context, mixed $payload): void
    {
        if ('PU' === $this->typeFormePrix) {
            if (null !== $this->variationPrixForfaitaire) {
                $context->buildViolation('n\'est pas autoriser pour les prix unitaires')
                    ->atPath('variationPrixForfaitaire')
                    ->addViolation();
            }

            if (null === $this->typePrix) {
                $context->buildViolation('est obligatoire pour les prix unitaires')
                    ->atPath('typePrix')
                    ->addViolation();
            }

            if (null === $this->variationPrixUnaitaire) {
                $context->buildViolation('est obligatoire pour les prix unitaires')
                    ->atPath('variationPrixUnaitaire')
                    ->addViolation();
            }

            if (null === $this->modalite) {
                $context->buildViolation('est obligatoire pour les prix unitaires')
                    ->atPath('modalite')
                    ->addViolation();
            }
        }

        if ('PF' === $this->typeFormePrix) {
            if (null === $this->variationPrixForfaitaire) {
                $context->buildViolation('est obligatoire pour les prix forfaitaires')
                    ->atPath('variationPrixForfaitaire')
                    ->addViolation();
            }

            if (null !== $this->typePrix) {
                $context->buildViolation('n\'est pas autoriser pour les prix forfaitaires')
                    ->atPath('typePrix')
                    ->addViolation();
            }

            if (null !== $this->variationPrixUnaitaire) {
                $context->buildViolation('n\'est pas autoriser pour les prix forfaitaires')
                    ->atPath('variationPrixUnaitaire')
                    ->addViolation();
            }
        }

        if ('PM' === $this->typeFormePrix) {
            if (null === $this->variationPrixForfaitaire) {
                $context->buildViolation('est obligatoire pour les prix mixtes')
                    ->atPath('variationPrixForfaitaire')
                    ->addViolation();
            }

            if (null === $this->typePrix) {
                $context->buildViolation('est obligatoire pour les prix mixtes')
                    ->atPath('typePrix')
                    ->addViolation();
            }

            if (null === $this->variationPrixUnaitaire) {
                $context->buildViolation('est obligatoire pour les prix mixtes')
                    ->atPath('variationPrixUnaitaire')
                    ->addViolation();
            }

            if (null === $this->modalite) {
                $context->buildViolation('est obligatoire pour les prix mixtes')
                    ->atPath('modalite')
                    ->addViolation();
            }
        }

        if (null === $this->modalite || 'quantite_definie' === $this->modalite) {
            if ($this->uniteMinMax) {
                $context->buildViolation('n\'est pas autoriser')
                    ->atPath('uniteMinMax')
                    ->addViolation();
            }

            if ($this->puMin) {
                $context->buildViolation('n\'est pas autoriser')
                    ->atPath('puMin')
                    ->addViolation();
            }

            if ($this->puMax) {
                $context->buildViolation('n\'est pas autoriser')
                    ->atPath('puMax')
                    ->addViolation();
            }
        }
    }
}
