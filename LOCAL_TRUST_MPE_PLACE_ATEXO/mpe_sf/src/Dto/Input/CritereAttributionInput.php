<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Input;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\Consultation\DonneeComplementaire;
use App\Entity\Lot;
use App\Validator\MaxPonderation;
use Symfony\Component\Validator\Constraints as Assert;

class CritereAttributionInput
{
    #[ApiProperty(example: '/api/v2/donnee-complementaires/42')]
    #[Assert\NotBlank]
    public ?DonneeComplementaire $donneeComplementaire = null;

    #[ApiProperty(example: '/api/v2/lots/2020')]
    public ?Lot $lot = null;

    #[ApiProperty(example: 'Critere attribution 1')]
    #[Assert\NotBlank]
    public ?string $enonce = null;

    #[ApiProperty(example: 5)]
    #[Assert\NotBlank]
    public ?int $ordre = 0;

    /**
     * @MaxPonderation()
     */
    #[ApiProperty(example: '10.0')]
    #[Assert\NotBlank]
    public ?string $ponderation = '0.00';
}
