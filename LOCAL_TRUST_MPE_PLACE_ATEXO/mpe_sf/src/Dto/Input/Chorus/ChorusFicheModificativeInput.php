<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Input\Chorus;

use ApiPlatform\Core\Annotation\ApiProperty;
use Symfony\Component\Validator\Constraints as Assert;

class ChorusFicheModificativeInput
{
    #[Assert\Type('\DateTimeInterface')]
    public ?\DateTime $dateFinMarcheModifie = null; //anciennement nouvelleFinContrat

    #[ApiProperty(example: '2.4')]
    public ?string $tauxTVA = null;

    #[ApiProperty(example: 10000.55)]
    public ?float $montantActe = null; // anciennement montantHTChiffre

    #[ApiProperty(example: 10000.55)]
    public ?float $montantMarche = null; // anciennement montantTTCChiffre

    #[ApiProperty(example: 'OUI')]
    public ?string $visaPrefet = null;

    #[ApiProperty(example: 'NON')]
    public ?string $visaACCF = null;

    #[Assert\Type('\DateTimeInterface')]
    public ?\DateTime $dateCreation = null;

    #[Assert\Type('\DateTimeInterface')]
    public ?\DateTime $dateModification = null;

    #[Assert\Type('\DateTimeInterface')]
    public ?\DateTime $dateNotificationPrevisionnelle = null; // pour le champ  $datePrevueNotification

    #[ApiProperty(example: 'TITULAIRE')]
    public ?string $typeFournisseurEntreprise = null;

    #[ApiProperty(
        example:
        [
            'siret' => '489 420 075 00017',
            'fournisseur' => ['nom' => 'Lyreco', 'siren' => "123456 987"],
        ]
    )]
    public array $titulaire = [];

    #[ApiProperty(
        example:
        [
            'siret' => '489 420 075 00017',
            'siren' => '489 420 075',
            'nom' => 'ABCD Entreprise',
        ]
    )]
    public array $sousTraitant = [];

    #[ApiProperty(
        example: [
            [
                'siret' => '489 420 075 00017',
                'siren' => '489 420 075',
                'nom' => 'ABCD Entreprise',
            ],
        ]
    )]
    public array $coTraitant = [];

    #[ApiProperty(example: 'remarque spéciale')]
    public ?string $commentaire = null;

    #[Assert\NotNull]
    public ?int $typeModification = null;

    #[Assert\Type('\DateTimeInterface')]
    public ?\DateTime $datefinMarche = null;
}
