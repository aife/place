<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Input\Chorus;

use ApiPlatform\Core\Annotation\ApiProperty;
use Symfony\Component\Validator\Constraints as Assert;

class ChorusEchangeContratInput
{
    #[ApiProperty(example: 'pmi-1')]
    #[Assert\NotNull()]
    public ?string $organisme;

    #[ApiProperty(example: '36')]
    public ?string $idTitulaire;

    #[ApiProperty(example: '489 420 075')]
    public ?string $siren;

    #[ApiProperty(example: '489 420 075 00017')]
    public ?string $siret;

    #[ApiProperty(example: 'XXXXX YYYYY')]
    public ?string $nom;

    #[ApiProperty(example: 'BA')]
    public ?string $formejuridique;

    #[ApiProperty(example: 'xxxx')]
    public ?string $categorieEntreprise;

    #[ApiProperty(example: 'France')]
    public ?string $pays;

    #[ApiProperty(example: '1234 5678')]
    public ?string $sirenetranger;

    #[ApiProperty(example: '62.01Z')]
    public ?string $codeape;

    #[ApiProperty(example: 152)]
    public ?int $idTitulaireEtab;

    #[ApiProperty(example: 95)]
    public ?int $idOffre;

    #[Assert\Type('\DateTimeInterface')]
    public ?\DateTime $datePrevisionnelleNotification;

    #[Assert\Type('\DateTimeInterface')]
    public ?\DateTime $datePrevisionnelleFinMarche; //@todo $chorusEchange->setDateFinMarche

    #[Assert\Type('\DateTimeInterface')]
    public ?\DateTime $dateNotification; //@todo $chorusEchange->setDateNotificationReelle

    #[Assert\Type('\DateTimeInterface')]
    public ?\DateTime $dateFin; //@todo $chorusEchange->setDateFinMarcheReelle

    #[ApiProperty(example: '45111210')]
    public ?string $codeCpv1;

    #[ApiProperty(example: '45111210')]
    public ?string $codeCpv2;

    #[ApiProperty(example: 'objet de contrat')]
    public ?string $objet;

    #[ApiProperty(example: '15230')]
    public ?string $montant;

    #[ApiProperty(example: 'Durant')]
    public ?string $nomAgent;

    #[ApiProperty(example: 'Pierre')]
    public ?string $prenomAgent;

    #[ApiProperty(example: 'AC_MONO_ATTRIBUTAIRE')]
    public ?string $typeContrat;

    #[ApiProperty(example: 'Accord-cadre à marchés subséquents mono-attributaire')]
    public ?string $intituleContrat;

    #[ApiProperty(example: 'AOO')]
    public ?string $typeProcedure;

    #[ApiProperty()]
    public ?string $formePrix;

    #[ApiProperty(example: 1)]
    public ?int $clauseSociale;

    #[ApiProperty(example: 0)]
    public ?int $clauseEnvironnementale;

    #[ApiProperty(example: 'Commandé')]
    public ?string $statutEJ;

    #[ApiProperty(example: '1_1300006151#')]
    public ?string $numEj;

    #[ApiProperty(example: 'ANotifier')]
    public ?string $statut;

    #[ApiProperty(example: 'BKA2405')]
    public ?string $referenceLibre;
}
