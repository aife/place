<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Input\Chorus;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\Agent;
use App\Validator\Chorus\IsEchangeChorusContratEmpty;
use App\Validator\Chorus\IsEchangeChorusExecUuidEmpty;
use App\Validator\Chorus\IsFicheModificativeValid;
use Symfony\Component\Validator\Constraints as Assert;

class ChorusEchangeInput
{
    #[ApiProperty(example: 4)]
    public ?int $id = null;

    #[IsEchangeChorusExecUuidEmpty]
    #[ApiProperty(example: '8C2AE443-8158-4600-B034-5DD651D92EB8')]
    public ?string $uuidExterneExec = null;

    #[ApiProperty(example: '1154')]
    public ?string $agentId = null;

    #[Assert\Type('\DateTimeInterface')]
    public ?\DateTime $dateModification;

    #[ApiProperty(example: '2')]
    #[Assert\NotNull()]
    public ?string $modeEchangeChorus = null;

    #[IsFicheModificativeValid]
    public ?ChorusFicheModificativeInput $acte = null;

    #[IsEchangeChorusContratEmpty]
    public ?ChorusEchangeContratInput $echangeChorusContrat = null;
}
