<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Input;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\BloborganismeFile;
use App\Entity\MediaUuid;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\EnveloppeFichier;

class FichierEnveloppeInput
{
    #[ApiProperty(example: '/api/v2/media-objects/123')]
    #[Assert\NotBlank]
    public MediaUuid $idFichier;

    #[ApiProperty(
        description: "Correspondance :\n\n| Code | Valeur |
            |--|--|
            | PRI | Pièce libre |
            | ACE | Acte d'engagement |
            | AFI | Annexe financière |
            | SIG | Jeton de signature |",
        example: 'PRI'
    )]
    #[Assert\Choice(choices: EnveloppeFichier::TYPE_FICHIER, message: "La valeur que vous avez sélectionnée n'est pas un choix valide : {{ choices }}")]
    #[Assert\NotBlank]
    public string $typeFichier;

    #[ApiProperty(example: '/api/v2/media-objects/125')]
    public ?MediaUuid $idSignature;
}
