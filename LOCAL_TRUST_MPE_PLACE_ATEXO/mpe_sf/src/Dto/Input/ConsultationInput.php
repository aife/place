<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Input;

use App\Entity\Agent;
use App\Entity\ReferentielAchat;
use App\Entity\TypeProcedureDume;
use App\Validator\ModulesRulesConstraint;
use App\Validator\NumeroContratConstraint;
use App\Validator\ProcedureAccesConstraint;
use App\Validator\ProcedureAllotiConstraint;
use App\Validator\ProcedureCpvConstraint;
use App\Validator\ProcedureDumeConstraint;
use App\Validator\ProcedureOpenConstraint;
use App\Validator\ProcedurePubliciteConstraint;
use App\Validator\ProcedureRedacConstraint;
use App\Validator\ProcedureTypeDumeConstraint;
use App\Validator\ProcedureTypeFormulaireConstraint;
use DateTimeInterface;
use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\CategorieConsultation;
use App\Entity\Consultation;
use App\Validator\Clauses;
use App\Validator\IsValeurEstimeeMandatory;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cette classe regroupe les champs en commun avec les classes CreateConsultationInput et UpdateConsultationInput
 */
class ConsultationInput
{
    #[ApiProperty(example: '/api/v2/referentiels/nature-prestations/1')]
    #[Assert\NotBlank]
    public ?CategorieConsultation $naturePrestation = null;

    #[ApiProperty(example: '/api/v2/consultations/123')]
    public ?Consultation $consultationParent = null;

    /**
     * @IsValeurEstimeeMandatory()
     */
    #[ApiProperty(example: 10000.55)]
    #[ModulesRulesConstraint(rule: 'ValeurEstimeeRule', mandatory: true)]
    public ?float $valeurEstimee = null;

    #[ApiProperty(example: 'intitule')]
    #[Assert\NotBlank]
    public ?string $intitule = null;

    #[ApiProperty(example: 'Mon objet')]
    #[Assert\NotBlank]
    public ?string $objet = null;

    #[ApiProperty(example: 'Mon commentaire')]
    public string $commentaireInterne = '';

    #[Assert\Type('\DateTimeInterface')]
    public ?DateTimeInterface $dateLimiteRemisePlis = null;

    #[Assert\Type(['\DateTimeInterface', 'string'])]
    public DateTimeInterface|string|null $dateLimiteRemisePlisLocale = null;

    #[Assert\Length(max: 255)]
    public ?string $lieuResidence = null;

    #[Assert\Type('\DateTimeInterface')]
    public ?DateTimeInterface $dateMiseEnLigneCalcule = null;

    #[ApiProperty(example: 'CODE_EXTERNE')]
    #[Assert\Length(max: 225, maxMessage: 'Le code externe ne peut pas avoir une taille supérieure à 225 caractères.')]
    public ?string $codeExterne = null;

    #[ApiProperty()]
    #[Assert\Length(max: 225)]
    public ?Agent $agentCreateur = null;

    #[ApiProperty(example: [75, 92])]
    #[ModulesRulesConstraint(rule: 'LieuxExecutionRule', mandatory: true)]
    public ?array $lieuxExecution = null;

    #[ApiProperty(example: ['AT311', 'UKI1'])]
    #[ModulesRulesConstraint(rule: 'CodeNutsRule', mandatory: true)]
    public ?array $codesNuts = null;

    #[ApiProperty(example: '33100000')]
    #[ProcedureCpvConstraint]
    public ?string $codeCpvPrincipal = null;

    #[ApiProperty(example: '03222314')]
    public ?string $codeCpvSecondaire1 = null;

    #[ApiProperty(example: '03331000')]
    public ?string $codeCpvSecondaire2 = null;

    #[ApiProperty(example: '03333000')]
    public ?string $codeCpvSecondaire3 = null;

    #[ApiProperty(example: false)]
    #[ProcedureAllotiConstraint]
    public ?bool $alloti = null;

    #[ApiProperty(example: false)]
    public ?bool $entiteAdjudicatrice = null;

    #[ApiProperty(example: false)]
    #[ProcedureRedacConstraint]
    public ?bool $donneeComplementaireObligatoire = null;

    #[ApiProperty(example: false)]
    #[ProcedurePubliciteConstraint]
    public ?bool $donneePubliciteObligatoire = null;

    #[ApiProperty(example: false)]
    #[ProcedureDumeConstraint]
    public ?bool $dume = null;

    #[ApiProperty(example: '/api/v2/referentiels/type-procedure-dumes/1')]
    #[ProcedureTypeDumeConstraint]
    public ?TypeProcedureDume $typeProcedureDume = null;

    #[ApiProperty(example: false)]
    #[ProcedureTypeFormulaireConstraint]
    public ?bool $dumeSimplifie = null;

    #[ApiProperty(example: false)]
    #[ProcedureAccesConstraint]
    public ?bool $consultationRestreinte = null;

    public ?string $codeProcedure = null;

    #[ApiProperty(example: 'Technique Achat label')]
    public ?string $autreTechniqueAchat = null;

    #[ApiProperty(example: '2022T00012')]
    #[NumeroContratConstraint]
    public ?string $numeroSad = null;

    #[ApiProperty(example: true)]
    #[ProcedureOpenConstraint]
    public ?bool $procedureOuverte = null;

    #[ApiProperty(example: true)]
    #[ModulesRulesConstraint(rule: 'GroupBuyers', mandatory: true)]
    public ?bool $groupement = null;

    #[ApiProperty(example: 'Technique Achat label')]
    #[ModulesRulesConstraint(rule: 'JustificationNonAllotiRule')]
    public ?string $justificationNonAlloti = null;

    #[ApiProperty(example: 'Contractant(s) : ATEXO (13005 - MARSEILLE 5)')]
    public ?string $champSuppInvisible = '';

    #[ApiProperty(example: '/api/v2/referentiels/referentiel-achats/1')]
    public ?ReferentielAchat $referentielAchat = null;

    #[ApiProperty(example: '1 Rue de Paris')]
    #[ModulesRulesConstraint(rule: 'AdresseRetraisDossiersRule', mandatory: true)]
    public ?string $adresseRetraisDossiers = null;

    #[ApiProperty(example: '1 Rue de Paris')]
    #[ModulesRulesConstraint(rule: 'AdresseDepotOffresRule', mandatory: true)]
    public ?string $adresseDepotOffres = null;

    #[ApiProperty(example: '1 Rue de Paris')]
    #[ModulesRulesConstraint(rule: 'LieuOuverturePlisRule')]
    public ?string $lieuOuverturePlis = null;

    #[ApiProperty(example: false)]
    #[ModulesRulesConstraint(rule: 'AttestationConsultationRule')]
    public ?bool $attestationConsultation = null;

    #[ApiProperty(example: true)]
    public ?bool $besoinRecurrent = null;

    #[ApiProperty(example: 123444)]
    public ?int $technicalReference = null;

    #[ApiProperty(example: true)]
    public ?string $modeOuvertureReponse = null;

    #[ApiProperty(example: true)]
    public ?string $reponseElectronique = null;

    #[ApiProperty(example: true)]
    public ?bool $enveloppeAnonyme = null;

    #[ApiProperty(example: true)]
    public ?bool $enveloppeOffre = null;

    #[ApiProperty(example: true)]
    public ?bool $enveloppeCandidature = null;

    #[ApiProperty(example: true)]
    public ?string $signatureActeEngagement = null;

    #[ApiProperty(example: true)]
    public ?string $marchePublicSimplifie = null;

    #[ApiProperty(example: 'https://mpe-develop.local-trust.com/entreprise/consultation/504030?orgAcronyme=pmi-min-1')]
    public ?string $urlExterne = null;

    #[ApiProperty(example: '1')]
    public ?bool $consultationExterne = null;

    #[ApiProperty(example: 122245)]
    public ?int $idCreateur = null;

    /**
     * @Clauses()
     */
    #[ApiProperty(example: [
        [
            "referentielClauseN1" => "/api/v2/referentiels/clauses-n1s/1",
            "clausesN2"           => [
                [
                    "referentielClauseN2" => "/api/v2/referentiels/clauses-n2s/2",
                    "clausesN3"           => [
                        [
                            "referentielClauseN3" => "/api/v2/referentiels/clauses-n3s/2"
                        ],
                        [
                            "referentielClauseN3" => "/api/v2/referentiels/clauses-n3s/4",
                            "clausesN4" => []
                        ]
                    ]
                ]
            ]
        ]
    ])]
    #[ModulesRulesConstraint(rule: 'ClausesRule', mandatory: true)]
    public Collection|array|null $clauses = null;

    #[ApiProperty(example: true)]
    public ?bool $chiffrement = null;
}
