<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Input;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\HabilitationProfil;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Validator\IsPasswordMandatory;
use Symfony\Component\Validator\Constraints as Assert;

class AgentInput
{
    #[ApiProperty(example: 'admin')]
    /**
     * @Assert\NotBlank()
     */
    public ?string $login = 'login';

    /**
     * @IsPasswordMandatory()
     */
    #[ApiProperty(example: 'password')]
    public ?string $password = null;

    #[ApiProperty(example: 'admin@atexo.com')]
    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    public string $email = 'admin@atexo.com';

    #[ApiProperty(example: 'lastname')]
    /**
     * @Assert\NotBlank()
     */
    public ?string $nom = null;

    #[ApiProperty(example: 'firstname')]
    /**
     * @Assert\NotBlank()
     */
    public ?string $prenom = null;

    #[ApiProperty(example: '/api/v2/referentiels/organismes/pmi-min-1')]
    /**
     * @Assert\NotBlank()
     */
    public ?Organisme $organisme = null;

    #[ApiProperty(example: '1')]
    public ?string $idExterne = '1';

    #[ApiProperty(example: true)]
    /**
     * @Assert\Type("bool")
     */
    public bool $actif = true;

    #[ApiProperty(example: '0555555555')]
    public ?string $telephone = null;

    #[ApiProperty(example: '0555555555')]
    public ?string $fax = null;

    #[ApiProperty(example: '/api/v2/referentiels/services/2916')]
    public ?Service $service = null;

    #[ApiProperty(example: [75, 92])]
    public ?array $lieuExecution = null;
}
