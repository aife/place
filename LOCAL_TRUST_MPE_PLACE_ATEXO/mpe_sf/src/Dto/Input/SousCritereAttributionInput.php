<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Input;

use App\Entity\Consultation\CritereAttribution;
use ApiPlatform\Core\Annotation\ApiProperty;
use App\Validator\MaxPonderation;
use Symfony\Component\Validator\Constraints as Assert;

class SousCritereAttributionInput
{
    #[ApiProperty(example: '/api/v2/critere-attributions/1')]
    #[Assert\NotBlank]
    public ?CritereAttribution $critereAttribution = null;

    #[ApiProperty(example: 'Sous critere attribution 1')]
    #[Assert\NotBlank]
    public ?string $enonce = null;

    /**
     * @MaxPonderation()
     */
    #[ApiProperty(example: '10.0')]
    #[Assert\NotBlank]
    public ?string $ponderation = '0.00';
}
