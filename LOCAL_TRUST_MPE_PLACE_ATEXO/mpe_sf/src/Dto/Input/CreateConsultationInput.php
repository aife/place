<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Input;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\Consultation\DonneeComplementaire;
use App\Entity\ContratTitulaire;
use App\Entity\Organisme;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\Service;
use App\Entity\TypeContrat;
use App\Validator\IsValidOrganisme;
use App\Validator\ModulesRulesConstraint;
use Symfony\Component\Validator\Constraints as Assert;

final class CreateConsultationInput extends ConsultationInput
{
    #[ApiProperty(example: '/api/v2/referentiels/contrats/1')]
    #[Assert\NotBlank]
    public ?TypeContrat $typeContrat = null;

    #[ApiProperty(example: '/api/v2/referentiels/services/1')]
    public ?Service $directionService = null;

    #[ApiProperty(example: 'reference')]
    #[ModulesRulesConstraint(rule: 'ReferenceGenereeRule', mandatory: true)]
    public ?string $reference = null;

    /**
     * @IsValidOrganisme()
     */
    #[ApiProperty(example: '/api/v2/referentiels/organismes/pmi-min-1')]
    public ?Organisme $organisme = null;

    #[ApiProperty(example: '/api/v2/referentiels/procedures/idTypeProcedure=1;organisme=pmi-min-1')]
    #[Assert\NotBlank]
    public ?TypeProcedureOrganisme $typeProcedure = null;

    #[ApiProperty(example: 66)]
    public ?int $idContratTitulaire = null;

    #[ApiProperty(example: '1A22B3-12345-AZERT-POIUY')]
    public ?string $contratExecUuid = null;

    /**
     * TODO: Validator
     */
    #[ApiProperty(example: '/api/v2/donnee-complementaires/789')]
    public ?DonneeComplementaire $donneeComplementaire = null;
}
