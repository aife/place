<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Input;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\Offre;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Enveloppe;

class EnveloppeInput
{
    #[ApiProperty(example: '/api/v2/offres/123')]
    #[Assert\NotBlank]
    public Offre $offre;

    #[ApiProperty(
        description: " Choices :\n\n
- CANDIDATURE
- OFFRE
- ANONYMAT
- OFFRE_TECHNIQUE
        ",
        example: 'ANONYMAT')]
    #[Assert\Choice(choices: Enveloppe::TYPE_ENVELOPPE_BY_ID)]
    #[Assert\NotBlank]
    public string $type;

    #[ApiProperty(example: 1)]
    #[Assert\NotBlank]
    public int $numeroLot;

    /** @var FichierEnveloppeInput[] */
    public array $fichiersEtSignature;
}
