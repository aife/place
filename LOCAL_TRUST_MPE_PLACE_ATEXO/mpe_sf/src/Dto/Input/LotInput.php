<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Input;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\CategorieConsultation;
use App\Entity\Consultation;
use App\Entity\Organisme;
use App\Validator\Clauses;
use App\Validator\IsCodeCPVMandatory;
use App\Validator\IsStatusValidForLot;
use App\Validator\ProcedureLotCpvConstraint;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\IsValidOrganisme;
use App\Validator\IsConsultationAlloti;
use App\Validator\IsValidUniqueLot;

final class LotInput
{
    /**
     * @IsStatusValidForLot
     * @IsConsultationAlloti
     */
    #[ApiProperty(example: '/api/v2/consultations/123')]
    #[Assert\NotBlank(groups: ['postValidation'])]
    public ?Consultation $consultation = null;

    /**
     * @IsValidOrganisme()
     */
    #[ApiProperty(example: '/api/v2/referentiels/organismes/456')]
    public ?Organisme $organisme = null;

    #[ApiProperty(example: '/api/v2/donnee-complementaires/789')]
    public ?Consultation\DonneeComplementaire $donneeComplementaire = null;

    /**
     * @IsValidUniqueLot()
     */
    #[ApiProperty(example: '1')]
    #[Assert\NotBlank]
    public string|int|null $numero = null;

    #[Assert\NotBlank]
    #[Assert\Length(max: 1000)]
    public ?string $intitule = null;

    #[Assert\NotBlank]
    #[Assert\Length(max: 1000)]
    public ?string $descriptionSuccinte = null;

    #[ApiProperty(example: '/api/v2/referentiels/nature-prestations/789')]
    #[Assert\NotBlank]
    public ?CategorieConsultation $naturePrestation = null;


    #[ApiProperty(example: '12345678')]
    #[ProcedureLotCpvConstraint]
    public ?string $codeCpvPrincipal = null;

    public ?string $codeCpvSecondaire1 = null;

    public ?string $codeCpvSecondaire2 = null;

    public ?string $codeCpvSecondaire3 = null;

    #[ApiProperty(example: 1)]
    public ?int $idTraductionDescription = null;
    #[ApiProperty(example: 12)]
    public ?int $idTraductionDescriptionDetail = null;

    #[ApiProperty(example: true)]
    public ?bool $marcheInsertion = null;

    #[ApiProperty(example: true)]
    public ?bool $clauseSpecificationTechnique = null;

    /**
     * @Clauses()*/
    #[ApiProperty(example: [
        [
            "referentielClauseN1" => "/api/v2/referentiels/clauses-n1s/1",
            "clausesN2"           => [
                [
                    "referentielClauseN2" => "/api/v2/referentiels/clauses-n2s/2",
                    "clausesN3"           => [
                        [
                            "referentielClauseN3" => "/api/v2/referentiels/clauses-n3s/2"
                        ],
                        [
                            "referentielClauseN3" => "/api/v2/referentiels/clauses-n3s/4",
                            "clausesN4" => [
                                "referentielClauseN4" => [
                                    "id" => "/api/v2/referentiels/clauses-n4s/4",
                                    "value" => 5
                                ],
                            ],
                        ],
                    ]
                ]
            ]
        ]
    ])]
    public Collection|array|null $clauses = null;
}
