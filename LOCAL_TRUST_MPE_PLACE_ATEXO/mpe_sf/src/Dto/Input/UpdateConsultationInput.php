<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Dto\Input;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\CategorieConsultation;
use Symfony\Component\Validator\Constraints as Assert;

final class UpdateConsultationInput extends ConsultationInput
{
    #[ApiProperty(example: 'reference')]
    #[Assert\NotBlank]
    public ?string $reference = null;

    public ?bool $etatEnAttenteValidation = null;

    #[ApiProperty(example: 4)]
    public ?int $poursuivreAffichage = null;

    #[ApiProperty(example: 'HOUR')]
    public ?string $poursuivreAffichageUnite = null;

    #[ApiProperty(example: true)]
    public ?bool $dcePartialDownload = null;
}
