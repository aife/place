<?php

namespace App\Exception;

use App\Model\ApiProblem;
use Symfony\Component\HttpFoundation\Response;

class ApiProblemForbiddenException extends ApiProblemException
{
    public function __construct()
    {
        $apiProblem = new ApiProblem(
            Response::HTTP_FORBIDDEN,
            ApiProblem::TYPE_FORBIDDEN
        );

        parent::__construct($apiProblem);
    }
}
