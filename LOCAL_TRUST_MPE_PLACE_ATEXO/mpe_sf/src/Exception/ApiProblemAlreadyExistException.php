<?php

namespace App\Exception;

use App\Model\ApiProblem;
use Symfony\Component\HttpFoundation\Response;

class ApiProblemAlreadyExistException extends ApiProblemException
{
    public function __construct($error = null)
    {
        $apiProblem = new ApiProblem(
            Response::HTTP_BAD_REQUEST,
            ApiProblem::TYPE_OBJECT_ALREADY_EXIST
        );
        if (!empty($error)) {
            $apiProblem->set('errors', $error);
        }
        parent::__construct($apiProblem);
    }
}
