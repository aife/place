<?php

namespace App\Exception;

use App\Model\ApiProblem;
use Symfony\Component\HttpFoundation\Response;

class ApiProblemInvalidSsoException extends ApiProblemException
{
    public function __construct()
    {
        $apiProblem = new ApiProblem(
            Response::HTTP_FORBIDDEN,
            ApiProblem::TYPE_INVALID_SSO
        );

        parent::__construct($apiProblem);
    }
}
