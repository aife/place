<?php

namespace App\Exception;

use App\Model\ApiProblem;
use Symfony\Component\HttpFoundation\Response;

class ApiProblemValidationSchemaException extends ApiProblemException
{
    public function __construct($format = '')
    {
        $apiProblem = new ApiProblem(
            Response::HTTP_BAD_REQUEST,
            ApiProblem::TYPE_VALIDATION_ERROR
        );
        $apiProblem->set('errors', ['Structure '.$format.' not valid']);

        parent::__construct($apiProblem);
    }
}
