<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Exception;

use RuntimeException;
class AgentTechniqueNotFoundException extends RuntimeException
{

}
