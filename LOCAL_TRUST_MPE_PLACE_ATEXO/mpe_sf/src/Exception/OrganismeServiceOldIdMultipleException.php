<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Exception;

use RuntimeException;
class OrganismeServiceOldIdMultipleException extends RuntimeException
{
    public function __construct(string $acronymeOrganisme, int $id)
    {
        $message = sprintf(
            'More than 1 Service for id (or old_id) = "%s" and acronymeOrg = "%s"',
            $id,
            $acronymeOrganisme
        );
        parent::__construct($message);
    }
}
