<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Exception;

class InscritNotFoundException extends DataNotFoundException
{
    public function __construct(int $id, int $entrepriseId)
    {
        $message = sprintf(
            'Inscrit not found with id (or old_id) equals "%s" and entreprise_id = "%s"',
            $id,
            $entrepriseId
        );
        parent::__construct(404, $message);
    }
}
