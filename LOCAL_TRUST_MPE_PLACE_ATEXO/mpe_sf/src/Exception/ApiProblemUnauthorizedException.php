<?php

namespace App\Exception;

use App\Model\ApiProblem;
use Symfony\Component\HttpFoundation\Response;

class ApiProblemUnauthorizedException extends ApiProblemException
{
    public function __construct($msg = '')
    {
        $apiProblem = new ApiProblem(
            Response::HTTP_FORBIDDEN,
            ApiProblem::TYPE_UNAUTHORIZED
        );
        if (!empty($msg)) {
            $apiProblem->setTitle($msg);
        }
        parent::__construct($apiProblem);
    }
}
