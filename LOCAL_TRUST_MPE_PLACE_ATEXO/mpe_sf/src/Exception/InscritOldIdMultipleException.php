<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Exception;

use RuntimeException;
class InscritOldIdMultipleException extends RuntimeException
{
    public function __construct(int $id, int $entrepriseId)
    {
        $message = sprintf(
            'More than 1 Inscrit for id (or old_id) equals "%s" and entreprise_id = "%s"',
            $id,
            $entrepriseId
        );
        parent::__construct($message);
    }
}
