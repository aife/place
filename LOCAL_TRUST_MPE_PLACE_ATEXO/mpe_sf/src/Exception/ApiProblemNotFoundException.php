<?php

namespace App\Exception;

use App\Model\ApiProblem;
use Symfony\Component\HttpFoundation\Response;

class ApiProblemNotFoundException extends ApiProblemException
{
    public function __construct()
    {
        $apiProblem = new ApiProblem(
            Response::HTTP_NOT_FOUND,
            ApiProblem::TYPE_NOT_FOUND
        );

        parent::__construct($apiProblem);
    }
}
