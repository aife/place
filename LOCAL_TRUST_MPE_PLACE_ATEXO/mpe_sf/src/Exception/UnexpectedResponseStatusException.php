<?php

namespace App\Exception;

use App\Model\ApiProblem;
use Symfony\Component\HttpFoundation\Response;

class UnexpectedResponseStatusException extends ApiProblemException
{
    public final const UNEXPECTED_STATUS = 'An unexpected HTTP status code was returned by the server';

    public function __construct(string $msg = '')
    {
        $apiProblem = new ApiProblem(
            Response::HTTP_FORBIDDEN,
            ApiProblem::TYPE_UNAUTHORIZED
        );

        if (!empty($msg)) {
            $apiProblem->setTitle($msg);
        }

        parent::__construct($apiProblem);
    }
}
