<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Exception;

use Symfony\Component\Filesystem\Exception\IOException;

class CantReadFileException extends IOException
{
}
