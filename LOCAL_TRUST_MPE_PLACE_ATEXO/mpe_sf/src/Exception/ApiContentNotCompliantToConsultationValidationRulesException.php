<?php

namespace App\Exception;

use App\Model\ApiProblem;
use Symfony\Component\HttpFoundation\Response;

class ApiContentNotCompliantToConsultationValidationRulesException extends ApiProblemException
{
    public function __construct($message = '')
    {
        $apiProblem = new ApiProblem(
            Response::HTTP_UNPROCESSABLE_ENTITY,
            ApiProblem::TYPE_INVALID_REQUEST_BODY_FORMAT
        );

        $apiProblem->set('errors', $message);

        parent::__construct($apiProblem);
    }
}
