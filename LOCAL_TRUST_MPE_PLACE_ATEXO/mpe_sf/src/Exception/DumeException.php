<?php

namespace App\Exception;

use Exception;
/**
 * Class DumeException.
 *
 * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
 */
class DumeException extends Exception
{
}
