<?php

namespace App\Exception;

use App\Model\ApiProblem;
use Symfony\Component\HttpFoundation\Response;

class ApiProblemInvalidArgumentException extends ApiProblemException
{
    public function __construct($message = '')
    {
        $apiProblem = new ApiProblem(
            Response::HTTP_BAD_REQUEST,
            ApiProblem::TYPE_INVALID_ARGUMENT
        );
        $apiProblem->set('errors', $message);

        parent::__construct($apiProblem);
    }
}
