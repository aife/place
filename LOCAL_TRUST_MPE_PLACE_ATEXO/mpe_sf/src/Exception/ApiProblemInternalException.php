<?php

namespace App\Exception;

use App\Model\ApiProblem;
use Symfony\Component\HttpFoundation\Response;

class ApiProblemInternalException extends ApiProblemException
{
    public function __construct($error = null)
    {
        $apiProblem = new ApiProblem(
            Response::HTTP_INTERNAL_SERVER_ERROR,
            ApiProblem::TYPE_ERROR_INTERNAL
        );
        if (!empty($error)) {
            $apiProblem->set('error', $error);
        }

        parent::__construct($apiProblem);
    }
}
