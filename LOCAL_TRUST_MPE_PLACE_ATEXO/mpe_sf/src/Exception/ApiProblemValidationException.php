<?php

namespace App\Exception;

use App\Model\ApiProblem;
use Symfony\Component\HttpFoundation\Response;

class ApiProblemValidationException extends ApiProblemException
{
    public function __construct($errors = '')
    {
        $apiProblem = new ApiProblem(
            Response::HTTP_BAD_REQUEST,
            ApiProblem::TYPE_VALIDATION_ERROR
        );
        $apiProblem->set('errors', $errors);

        parent::__construct($apiProblem);
    }
}
