<?php

namespace App\Exception;

use GuzzleHttp\Exception\ClientException;

class ClientWsException extends ClientException
{
}
