<?php

namespace App\Exception;

use Exception;
/**
 * Class UidNotFoundException de gestion des exceptions liees a l'unique ID.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @since ESR2017
 *
 * @copyright Atexo 2017
 */
class UidNotFoundException extends Exception
{
}
