<?php

namespace App\Exception;

use App\Model\ApiProblem;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApiProblemOrganismePoolException.
 */
class ApiProblemOrganismePoolException extends ApiProblemException
{
    public function __construct()
    {
        $apiProblem = new ApiProblem(
            Response::HTTP_FORBIDDEN,
            ApiProblem::TYPE_ORGANISME_POOL
        );

        parent::__construct($apiProblem);
    }
}
