<?php

namespace App\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

class ConsultationNotFoundException extends HttpException
{
}
