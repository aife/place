<?php

namespace App\Exception;

use App\Model\ApiProblem;
use Symfony\Component\HttpFoundation\Response;

class ApiProblemInvalidTokenException extends ApiProblemException
{
    public function __construct(string $msg = '')
    {
        $apiProblem = new ApiProblem(
            Response::HTTP_UNAUTHORIZED,
            ApiProblem::TYPE_UNAUTHORIZED
        );

        if (!empty($msg)) {
            $apiProblem->setTitle($msg);
        }

        parent::__construct($apiProblem);
    }
}
