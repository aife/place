<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use App\Entity\ProcedureEquivalence;
use App\Repository\Procedure\ProcedureEquivalenceDumeRepository;
use App\Repository\Procedure\ProcedureEquivalenceRepository;
use App\Service\AtexoConfiguration;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ProcedureTypeDumeConstraintValidator extends ConstraintValidator
{
    use ApiVersionTrait;

    public function __construct(
        private Security $security,
        private ProcedureEquivalenceRepository $equivalenceRepository,
        private ProcedureEquivalenceDumeRepository $equivalenceDumeRepository,
        private AtexoConfiguration $configuration
    ) {
    }

    public function validate($value, Constraint $constraint)
    {
        if ($this->isValidatorDisabledForApiVersion()) {
            return;
        }

        $inputObject = $this->context->getObject();
        if ($inputObject?->organisme) {
            $organism = $inputObject?->organisme;
        } else {
            $organism = $this->security?->getUser()?->getOrganisme();
        }

        if ($value !== null && !$this->configuration->isModuleEnabled('InterfaceDume', $organism)) {
            $this->context->buildViolation($constraint->messageUnauthorized)->addViolation();
        }

        $procedureEquivalence = $this->equivalenceRepository->findOneBy([
            'idTypeProcedure' => $inputObject?->typeProcedure?->getIdTypeProcedure(),
            'organisme' => $organism?->getAcronyme()
        ]);

        if (
            null !== $value
            && str_starts_with($procedureEquivalence->getTypeProcedureDume(), '-')
        ) {
            $this->context->buildViolation($constraint->messageUnauthorized)->addViolation();
        }

        if (
            null === $value
            && $this->configuration->isModuleEnabled('InterfaceDume', $organism)
            && !str_starts_with($procedureEquivalence->getTypeProcedureDume(), '-')
        ) {
            $this->context->buildViolation($constraint->messageRequired)->addViolation();
        }

        $idDumeList = $this->equivalenceDumeRepository->getIdProcedureDumeByOrganism(
            $inputObject?->typeProcedure?->getIdTypeProcedure(),
            $organism?->getAcronyme()
        );

        if (
            $value
            && !in_array($value->getId(), $idDumeList)
        ) {
            $this->context->buildViolation($constraint->messageInvalidVal)->addViolation();
        }

        $frozenIdDumeList = $this->equivalenceDumeRepository->getFrozenIdProcedureDumeByOrganism(
            $inputObject?->typeProcedure?->getIdTypeProcedure(),
            $organism?->getAcronyme()
        );

        if (!empty($frozenIdDumeList) && $frozenIdDumeList[0] !== $value->getId()) {
            $this->context->buildViolation($constraint->messageInvalidVal)->addViolation();
        }
    }
}
