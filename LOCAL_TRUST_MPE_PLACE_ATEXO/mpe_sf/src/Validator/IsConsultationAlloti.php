<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsConsultationAlloti extends Constraint
{
    public $message = "Impossible de créer un lot si la consultation n'est pas alloti";
}
