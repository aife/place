<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class ProcedureTypeDumeConstraint extends Constraint
{
    public $messageUnauthorized = 'Le champ typeProcedureDume n\'est pas autorisé.';
    public $messageRequired = 'Le champ typeProcedureDume est obligatoire.';
    public $messageInvalidVal = 'La valeur du champ typeProcedureDume est invalide';
}
