<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class ProcedureDumeConstraint extends Constraint
{
    public $messageUnauthorized = 'Le champ dume n\'est pas autorisé.';
    public $messageRequired = 'Le champ dume est obligatoire.';
    public $messageInvalidVal = 'La valeur du champ dume est invalide';
}
