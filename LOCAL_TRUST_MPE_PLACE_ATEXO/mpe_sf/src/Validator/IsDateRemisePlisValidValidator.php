<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use DateTime;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsDateRemisePlisValidValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     * @param string $value
     * @param IsDateRemisePlisValid $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $dateNow = new DateTime();

        // On vérifie si la date de remise des plis n'est pas antérieur à la date du jour
        if ($value && $value > $dateNow) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setCode(Response::HTTP_BAD_REQUEST)
            ->addViolation()
        ;
    }
}
