<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use App\Repository\ConfigurationPlateformeRepository;
use App\Repository\Procedure\ProcedureEquivalenceRepository;

use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ProcedureLotCpvConstraintValidator extends ConstraintValidator
{
    use ApiVersionTrait;

    public function __construct(
        private readonly Security $security,
        private readonly ProcedureEquivalenceRepository $equivalenceRepository,
        private readonly ConfigurationPlateformeRepository $configurationPlateformeRepository
    ) {
    }

    public function validate($value, Constraint $constraint)
    {
        if ($this->isValidatorDisabledForApiVersion()) {
            return;
        }
        $inputObject = $this->context->getObject();
        if ($inputObject?->consultation?->getOrganisme()) {
            $acronymOrganism = $inputObject?->consultation?->getOrganisme()->getAcronyme();
        } else {
            $acronymOrganism = $this->security?->getUser()?->getOrganisme()?->getAcronyme();
        }
        $procedureEquivalence = $this->equivalenceRepository->findOneBy([
            'idTypeProcedure' => $inputObject?->consultation?->getIdTypeProcedureOrg(),
            'organisme' => $acronymOrganism
        ]);
        $conf = $this->configurationPlateformeRepository->getConfigurationPlateforme();

        if (
            ('0' === $procedureEquivalence?->getAfficherCodeCpv() || '0' === $conf->getAffichageCodeCpv())
            && null !== $value
        ) {

            $this->context->buildViolation($constraint->messageUnauthorized)->addViolation();
        }

        if ('1' === $procedureEquivalence?->getCodeCpvObligatoire() && null === $value) {
            $this->context->buildViolation($constraint->messageRequired)->addViolation();
        }
    }
}
