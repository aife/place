<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsTitulaireEntrepriseValid extends Constraint
{
    public $message = 'Cet entreprise titulaire ne correspond pas au titulaire assigné à cet établissement "{{ id }}" ';
}
