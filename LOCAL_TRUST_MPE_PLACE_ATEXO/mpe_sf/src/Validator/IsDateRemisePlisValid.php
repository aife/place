<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsDateRemisePlisValid extends Constraint
{
    public string $message = "La date de remise des plis doit être postérieure à la date du jour";
}
