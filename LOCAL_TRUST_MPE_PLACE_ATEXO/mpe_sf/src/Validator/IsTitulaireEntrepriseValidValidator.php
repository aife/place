<?php

namespace App\Validator;

use App\Dto\Input\ContratTitulaireInput;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsTitulaireEntrepriseValidValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        /** @var ContratTitulaireInput $objectDto */
        $objectDto = $this->context->getObject();

        $entreprise = $objectDto->idTitulaire;
        $etablissement = $objectDto->etablissement;

        /* @var $constraint IsTitulaireEntrepriseValid */
        if ($entreprise->getId() !== $etablissement->getEntreprise()?->getId()) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ id }}', $etablissement->getIdEtablissement())
                ->setCode(Response::HTTP_BAD_REQUEST)
                ->addViolation()
            ;
        }
    }
}
