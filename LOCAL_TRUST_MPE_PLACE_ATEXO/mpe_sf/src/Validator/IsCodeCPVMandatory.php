<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsCodeCPVMandatory extends Constraint
{
    public string $invalidCodeCPV = "TEXT_VALIDATOR_INVALID_CODE_CPV";
}
