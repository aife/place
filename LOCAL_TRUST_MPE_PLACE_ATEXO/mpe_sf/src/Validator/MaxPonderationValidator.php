<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use App\Dto\Input\CritereAttributionInput;
use App\Dto\Input\SousCritereAttributionInput;
use App\Service\DonneeComplementaire\CritereAttributionService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class MaxPonderationValidator extends ConstraintValidator
{
    public function __construct(
        private readonly CritereAttributionService $critereAttributionService
    ) {
    }

    /**
     * {@inheritdoc}
     * @param string $value
     * @param MaxPonderation $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        /** @var CritereAttributionInput|SousCritereAttributionInput $critereAttributionObject */
        $critereAttributionObject = $this->context->getObject();

        $criteres = [];
        if ($critereAttributionObject instanceof CritereAttributionInput) {
            $criteres = $critereAttributionObject->donneeComplementaire
                ? $critereAttributionObject->donneeComplementaire->getCriteresAttribution()
                : []
            ;
        } elseif ($critereAttributionObject instanceof SousCritereAttributionInput) {
            $criteres = $critereAttributionObject->critereAttribution
                ? $critereAttributionObject->critereAttribution->getSousCriteres()
                : []
            ;
        }

        if (
            $criteres
            && !$this->critereAttributionService->isCriterePonderationValid($criteres, $value)
        ) {
            $this->context
                ->buildViolation($constraint->message)
                ->setCode(Response::HTTP_BAD_REQUEST)
                ->addViolation()
            ;
        }
    }
}
