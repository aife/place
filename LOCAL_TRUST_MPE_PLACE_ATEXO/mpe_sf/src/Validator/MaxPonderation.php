<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class MaxPonderation extends Constraint
{
    public $message = "Impossible de créer le critère d'attribution. 
    La somme des pondérations des critères d'attribution ne doit pas dépasser 100%.";
}
