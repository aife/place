<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use App\Entity\ProcedureEquivalence;
use App\Repository\Procedure\ProcedureEquivalenceRepository;
use App\Service\AtexoConfiguration;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ProcedureRedacConstraintValidator extends ConstraintValidator
{
    use ApiVersionTrait;

    public function __construct(
        private Security $security,
        private ProcedureEquivalenceRepository $equivalenceRepository,
        private AtexoConfiguration $configuration
    ) {
    }

    public function validate($value, Constraint $constraint)
    {
        if ($this->isValidatorDisabledForApiVersion()) {
            return;
        }

        $inputObject = $this->context->getObject();
        if ($inputObject?->organisme) {
            $organism = $inputObject?->organisme;
        } else {
            $organism = $this->security?->getUser()?->getOrganisme();
        }

        if ($value !== null && !$this->configuration->isModuleEnabled('InterfaceModuleRsem', $organism)) {
            $this->context->buildViolation($constraint->messageUnauthorized)->addViolation();
        }

        /**
         * @var $procedureEquivalence ProcedureEquivalence
         */
        $procedureEquivalence = $this->equivalenceRepository->findOneBy([
            'idTypeProcedure' => $inputObject?->typeProcedure?->getIdTypeProcedure(),
            'organisme' => $organism?->getAcronyme()
        ]);

        if (
            null !== $value
            && str_starts_with($procedureEquivalence->getDonneesComplementaireOui(), '-')
            && str_starts_with($procedureEquivalence->getDonneesComplementaireNon(), '-')
        ) {
            $this->context->buildViolation($constraint->messageUnauthorized)->addViolation();
        }

        if (
            null === $value
            && $this->configuration->isModuleEnabled('InterfaceModuleRsem', $organism)
            && (
                !str_starts_with($procedureEquivalence->getDonneesComplementaireOui(), '-')
                || !str_starts_with($procedureEquivalence->getDonneesComplementaireNon(), '-')
            )
        ) {
            $this->context->buildViolation($constraint->messageRequired)->addViolation();
        }

        if (
            !str_starts_with($procedureEquivalence->getDonneesComplementaireNon(), '-')
            && str_starts_with($procedureEquivalence->getDonneesComplementaireOui(), '-')
            && $value !== (bool) (int) $procedureEquivalence->getDonneesComplementaireOui()
        ) {
            $this->context->buildViolation($constraint->messageInvalidVal)->addViolation();
        }


        if (
            !str_starts_with($procedureEquivalence->getDonneesComplementaireOui(), '-')
            && str_starts_with($procedureEquivalence->getDonneesComplementaireNon(), '-')
            && $value !== (bool) (int) $procedureEquivalence->getDonneesComplementaireOui()
        ) {
            $this->context->buildViolation($constraint->messageInvalidVal)->addViolation();
        }

        if (
            (
                !str_starts_with($procedureEquivalence->getDonneesComplementaireOui(), '-')
                && !str_starts_with($procedureEquivalence->getDonneesComplementaireOui(), '+')
            )
            &&
            (
                !str_starts_with($procedureEquivalence->getDonneesComplementaireNon(), '-')
                && !str_starts_with($procedureEquivalence->getDonneesComplementaireNon(), '+')
            )
            && $value !== (bool) (int) $procedureEquivalence->getDonneesComplementaireOui()
        ) {
            $this->context->buildViolation($constraint->messageInvalidVal)->addViolation();
        }
    }
}
