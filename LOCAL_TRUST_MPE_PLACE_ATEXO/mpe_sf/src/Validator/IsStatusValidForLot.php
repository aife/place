<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsStatusValidForLot extends Constraint
{
    public string $message = "Le statut de la consultation est trop avancée pour pouvoir y ajouter un lot";
}
