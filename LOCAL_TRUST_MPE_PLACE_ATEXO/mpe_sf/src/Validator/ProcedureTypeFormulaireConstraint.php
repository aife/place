<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class ProcedureTypeFormulaireConstraint extends Constraint
{
    public $messageUnauthorized = 'Le champ dumeSimplifie n\'est pas autorisé.';
    public $messageRequired = 'Le champ dumeSimplifie est obligatoire.';
    public $messageInvalidVal = 'La valeur du champ dumeSimplifie est invalide';
}
