<?php

namespace App\Validator;

use App\Entity\Organisme;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsValidOrganismeValidator extends ConstraintValidator
{
    public function __construct(
        private readonly Security $security,
    ) {
    }

    /**
     * {@inheritdoc}
     *
     * @var Organisme $value
     */
    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint IsValidOrganisme */
        if (
            $value?->getAcronyme() || $this->security->getUser()?->getOrganisme()
        ) {
            return ;
        }

        $this->context->buildViolation($constraint->message)
            ->setCode(Response::HTTP_BAD_REQUEST)
            ->addViolation();
    }
}
