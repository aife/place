<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use App\Entity\ProcedureEquivalence;
use App\Repository\Procedure\ProcedureEquivalenceRepository;
use App\Service\AtexoConfiguration;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ProcedureTypeFormulaireConstraintValidator extends ConstraintValidator
{
    use ApiVersionTrait;

    public function __construct(
        private Security $security,
        private ProcedureEquivalenceRepository $equivalenceRepository,
        private AtexoConfiguration $configuration
    ) {
    }

    public function validate($value, Constraint $constraint)
    {
        if ($this->isValidatorDisabledForApiVersion()) {
            return;
        }

        $inputObject = $this->context->getObject();
        if ($inputObject?->organisme) {
            $organism = $inputObject?->organisme;
        } else {
            $organism = $this->security?->getUser()?->getOrganisme();
        }

        if ($value !== null && !$this->configuration->isModuleEnabled('InterfaceDume', $organism)) {
            $this->context->buildViolation($constraint->messageUnauthorized)->addViolation();
        }

        $procedureEquivalence = $this->equivalenceRepository->findOneBy([
            'idTypeProcedure' => $inputObject?->typeProcedure?->getIdTypeProcedure(),
            'organisme' => $organism?->getAcronyme()
        ]);

        if (
            null !== $value
            && str_starts_with($procedureEquivalence->getTypeFormulaireDumeStandard(), '-')
            && str_starts_with($procedureEquivalence->getTypeFormulaireDumeSimplifie(), '-')
        ) {
            $this->context->buildViolation($constraint->messageUnauthorized)->addViolation();
        }

        if (
            null === $value
            && $this->configuration->isModuleEnabled('InterfaceDume', $organism)
            && (
                !str_starts_with($procedureEquivalence->getTypeFormulaireDumeStandard(), '-')
                || !str_starts_with($procedureEquivalence->getTypeFormulaireDumeSimplifie(), '-'))
        ) {
            $this->context->buildViolation($constraint->messageRequired)->addViolation();
        }

        if (
            !str_starts_with($procedureEquivalence->getTypeFormulaireDumeStandard(), '-')
            && str_starts_with($procedureEquivalence->getTypeFormulaireDumeSimplifie(), '-')
            && $value !== (bool) (int) $procedureEquivalence->getTypeFormulaireDumeSimplifie()
        ) {
            $this->context->buildViolation($constraint->messageInvalidVal)->addViolation();
        }

        if (
            !str_starts_with($procedureEquivalence->getTypeFormulaireDumeSimplifie(), '-')
            && str_starts_with($procedureEquivalence->getTypeFormulaireDumeStandard(), '-')
            && $value !== (bool) (int) $procedureEquivalence->getTypeFormulaireDumeSimplifie()
        ) {
            $this->context->buildViolation($constraint->messageInvalidVal)->addViolation();
        }

        if (
            (
                !str_starts_with($procedureEquivalence->getTypeFormulaireDumeSimplifie(), '-')
                && !str_starts_with($procedureEquivalence->getTypeFormulaireDumeSimplifie(), '+')
            )
            &&
            (
                !str_starts_with($procedureEquivalence->getTypeFormulaireDumeStandard(), '-')
                && !str_starts_with($procedureEquivalence->getTypeFormulaireDumeStandard(), '+')
            )
            && $value !== (bool) (int) $procedureEquivalence->getTypeFormulaireDumeSimplifie()
        ) {
            $this->context->buildViolation($constraint->messageInvalidVal)->addViolation();
        }
    }
}
