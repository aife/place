<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use App\Repository\ConfigurationPlateformeRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsPasswordMandatoryValidator extends ConstraintValidator
{
    public function __construct(
        private readonly ConfigurationPlateformeRepository $configurationPlateformeRepository,
    ) {
    }

    /**
     * {@inheritdoc}
     * @param string $value
     * @param IsPasswordMandatory $constraint
     * @throws NonUniqueResultException
     */
    public function validate($value, Constraint $constraint)
    {
        $config = $this->configurationPlateformeRepository->getConfigurationPlateforme();

        if (
            !(
                '1' === $config->getAuthenticateAgentOpenidMicrosoft()
                || '1' === $config->getAuthenticateAgentOpenidKeycloak()
                || '1' === $config->getAuthenticateAgentSaml()
            )
            && empty($value)
        ) {
            $this->context->buildViolation($constraint->message)
                ->setCode(Response::HTTP_BAD_REQUEST)
                ->addViolation()
            ;
        }
    }
}
