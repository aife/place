<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class ProcedureAllotiConstraint extends Constraint
{
    public $messageUnauthorized = 'Le champ alloti n\'est pas autorisé.';
    public $messageRequired = 'Le champ alloti est obligatoire.';
    public $messageInvalidVal = 'La valeur du champ alloti est invalide';
}
