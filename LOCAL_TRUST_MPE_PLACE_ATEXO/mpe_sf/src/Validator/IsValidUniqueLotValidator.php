<?php

namespace App\Validator;

use App\Entity\Consultation;
use App\Entity\Lot;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsValidUniqueLotValidator extends ConstraintValidator
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly RequestStack $requestStack
    ) {
    }

    /**
     * {@inheritdoc}
     *
     * @var Consultation
     */
    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint IsValidUniqueLot */

        // Nous vérifions si le numéro de lot existe déjà pour cette consultation
        $objectDto = $this->context?->getObject();
        if (($consultation = $objectDto?->consultation) && $consultation instanceof Consultation) {
            /** @var Lot $lot */
            $lot = $this->entityManager->getRepository(Lot::class)->findOneBy(
                [
                    'consultation' => $consultation,
                    'lot' => $value
                ]
            );

            $actualIdLot = $this->requestStack?->getCurrentRequest()?->attributes?->get('id');

            if ($lot && $actualIdLot != $lot->getId()) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ numeroLot }}', $value)
                    ->setCode(Response::HTTP_BAD_REQUEST)
                    ->addViolation()
                ;
            }
        }
    }
}
