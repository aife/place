<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;



use Symfony\Component\Validator\Constraints\EqualToValidator;

class EqualToNoCaseSensitiveValidator extends EqualToValidator
{
    protected function compareValues($value1, $value2)
    {
        return strtolower($value1) == strtolower($value2);
    }
}
