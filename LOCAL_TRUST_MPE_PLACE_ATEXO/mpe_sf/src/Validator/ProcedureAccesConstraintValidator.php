<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use App\Entity\ProcedureEquivalence;
use App\Repository\Procedure\ProcedureEquivalenceRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ProcedureAccesConstraintValidator extends ConstraintValidator
{
    use ApiVersionTrait;

    public function __construct(
        private Security $security,
        private ProcedureEquivalenceRepository $equivalenceRepository
    ) {
    }

    public function validate($value, Constraint $constraint)
    {
        if ($this->isValidatorDisabledForApiVersion()) {
            return;
        }

        $inputObject = $this->context->getObject();
        if ($inputObject?->organisme) {
            $acronymOrganism = $inputObject?->organisme?->getAcronyme();
        } else {
            $acronymOrganism = $this->security?->getUser()?->getOrganisme()?->getAcronyme();
        }

        /**
         * @var $procedureEquivalence ProcedureEquivalence
         */
        $procedureEquivalence = $this->equivalenceRepository->findOneBy([
            'idTypeProcedure' => $inputObject?->typeProcedure?->getIdTypeProcedure(),
            'organisme' => $acronymOrganism
        ]);

        if (
            null !== $value
            && str_starts_with($procedureEquivalence->getProcedurePublicite(), '-')
            && str_starts_with($procedureEquivalence->getProcedureRestreinte(), '-')
        ) {
            $this->context->buildViolation($constraint->messageUnauthorized)->addViolation();
        }

        if (
            null === $value
            && (
                !str_starts_with($procedureEquivalence->getProcedurePublicite(), '-')
                || !str_starts_with($procedureEquivalence->getProcedureRestreinte(), '-'))
        ) {
            $this->context->buildViolation($constraint->messageRequired)->addViolation();
        }

        if (
            !str_starts_with($procedureEquivalence->getProcedureRestreinte(), '-')
            && str_starts_with($procedureEquivalence->getProcedurePublicite(), '-')
            && $value === (bool) (int) $procedureEquivalence->getProcedurePublicite()
        ) {
            $this->context->buildViolation($constraint->messageInvalidVal)->addViolation();
        }

        if (
            !str_starts_with($procedureEquivalence->getProcedurePublicite(), '-')
            && str_starts_with($procedureEquivalence->getProcedureRestreinte(), '-')
            && $value === (bool) (int) $procedureEquivalence->getProcedurePublicite()
        ) {
            $this->context->buildViolation($constraint->messageInvalidVal)->addViolation();
        }

        if (
            (
                !str_starts_with($procedureEquivalence->getProcedurePublicite(), '-')
                && !str_starts_with($procedureEquivalence->getProcedurePublicite(), '+')
            )
            &&
            (
                !str_starts_with($procedureEquivalence->getProcedureRestreinte(), '-')
                && !str_starts_with($procedureEquivalence->getProcedureRestreinte(), '+')
            )
            && $value === (bool) (int) $procedureEquivalence->getProcedurePublicite()
        ) {
            $this->context->buildViolation($constraint->messageInvalidVal)->addViolation();
        }
    }
}
