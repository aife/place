<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class ProcedureAccesConstraint extends Constraint
{
    public $messageUnauthorized = 'Le champ consultationRestreinte n\'est pas autorisé.';
    public $messageRequired = 'Le champ consultationRestreinte est obligatoire.';
    public $messageInvalidVal = 'La valeur du champ consultationRestreinte est invalide';
}
