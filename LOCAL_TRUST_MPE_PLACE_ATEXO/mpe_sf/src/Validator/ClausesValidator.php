<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\Referentiel\Consultation\ClausesN1;
use App\Entity\Referentiel\Consultation\ClausesN2;
use App\Entity\Referentiel\Consultation\ClausesN3;
use App\Entity\Referentiel\Consultation\ClausesN4;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\Translation\TranslatorInterface;

class ClausesValidator extends ConstraintValidator
{
    public function __construct(
        private readonly IriConverterInterface $iriConverter,
        private readonly TranslatorInterface $translator
    ) {
    }

    /**
     * {@inheritdoc}
     * @param array $value
     * @param Clauses $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!is_array($value)) {
            return;
        }

        $inputObject = $this->context->getObject();
        if (property_exists($inputObject, 'alloti') && !empty($inputObject->alloti)) {
            if (!empty($value)) {
                $this->context->buildViolation($this->translator->trans($constraint->notEmptyClauses))
                    ->setCode(Response::HTTP_BAD_REQUEST)
                    ->addViolation();
            }
            return;
        }

        foreach ($value as $rawClauseN1) {
            if (empty($rawClauseN1['referentielClauseN1'])) {
                $this->context->buildViolation($constraint->invalidClauseN1Referentiel)
                    ->setCode(Response::HTTP_BAD_REQUEST)
                    ->addViolation();
                return;
            }
            $refClauseN1 = $this->iriConverter->getItemFromIri($rawClauseN1['referentielClauseN1']);
            if (!$refClauseN1 instanceof ClausesN1) {
                $this->context->buildViolation($constraint->invalidClauseN1Referentiel)
                    ->setCode(Response::HTTP_BAD_REQUEST)
                    ->addViolation();
                return;
            }

            if (empty($rawClauseN1['clausesN2']) || !is_array($rawClauseN1['clausesN2'])) {
                $this->context->buildViolation($constraint->noClausesN2Message)
                    ->setCode(Response::HTTP_BAD_REQUEST)
                    ->addViolation();
                return;
            }
            foreach ($rawClauseN1['clausesN2'] as $rawClauseN2) {
                if (empty($rawClauseN2['referentielClauseN2'])) {
                    $this->context->buildViolation($constraint->invalidClauseN2Referentiel)
                        ->setCode(Response::HTTP_BAD_REQUEST)
                        ->addViolation();
                    return;
                }
                $refClauseN2 = $this->iriConverter->getItemFromIri($rawClauseN2['referentielClauseN2']);
                if (!$refClauseN2 instanceof ClausesN2) {
                    $this->context->buildViolation($constraint->invalidClauseN2Referentiel)
                        ->setCode(Response::HTTP_BAD_REQUEST)
                        ->addViolation();
                    return;
                } elseif ($refClauseN2->getClauseN1()->getId() != $refClauseN1->getId()) {
                    $this->context->buildViolation($constraint->noLinkClausesN1N2Message)
                        ->setCode(Response::HTTP_BAD_REQUEST)
                        ->addViolation();
                    return;
                }

                //check for N3
                if (
                    !$refClauseN2->getClausesN3()->isEmpty() &&
                    (empty($rawClauseN2['clausesN3']) || !is_array($rawClauseN2['clausesN3']))
                ) {
                    $this->context->buildViolation($constraint->noClausesN3Message)
                        ->setCode(Response::HTTP_BAD_REQUEST)
                        ->addViolation();
                    return;
                }
                foreach ($rawClauseN2['clausesN3'] as $rawClauseN3) {
                    if (empty($rawClauseN3['referentielClauseN3'])) {
                        $this->context->buildViolation($constraint->invalidClauseN3Referentiel)
                            ->setCode(Response::HTTP_BAD_REQUEST)
                            ->addViolation();
                        return;
                    }
                    $refClauseN3 = $this->iriConverter->getItemFromIri($rawClauseN3['referentielClauseN3']);
                    if (!$refClauseN3 instanceof ClausesN3) {
                        $this->context->buildViolation($constraint->invalidClauseN3Referentiel)
                            ->setCode(Response::HTTP_BAD_REQUEST)
                            ->addViolation();
                        return;
                    } elseif (!$refClauseN3->getClausesN2()->contains($refClauseN2)) {
                        $this->context->buildViolation($constraint->noLinkClausesN2N3Message)
                            ->setCode(Response::HTTP_BAD_REQUEST)
                            ->addViolation();
                        return;
                    }

                    //check for N4
                    if (!empty($rawClauseN3['clausesN4']) && !is_array($rawClauseN3['clausesN4'])) {
                        $this->context->buildViolation($constraint->noClausesN4Message)
                            ->setCode(Response::HTTP_BAD_REQUEST)
                            ->addViolation();
                        return;
                    }
                    foreach ($rawClauseN3['clausesN4'] as $rawClauseN4) {
                        if (empty($rawClauseN4['referentielClauseN4'])) {
                            $this->context->buildViolation($constraint->invalidClauseN4Referentiel)
                                ->setCode(Response::HTTP_BAD_REQUEST)
                                ->addViolation();

                            return;
                        }

                        $refClauseN4 = $this->iriConverter->getItemFromIri($rawClauseN4['referentielClauseN4']['id']);
                        if (!$refClauseN4 instanceof ClausesN4) {
                            $this->context->buildViolation($constraint->invalidClauseN4Referentiel)
                                ->setCode(Response::HTTP_BAD_REQUEST)
                                ->addViolation();

                            return;
                        } elseif ($refClauseN4->getClauseN3()->getId() != $refClauseN3->getId()) {
                            $this->context->buildViolation($constraint->noLinkClausesN3N4Message)
                                ->setCode(Response::HTTP_BAD_REQUEST)
                                ->addViolation();
                            return;
                        }
                    }
                }
            }
        }
    }
}
