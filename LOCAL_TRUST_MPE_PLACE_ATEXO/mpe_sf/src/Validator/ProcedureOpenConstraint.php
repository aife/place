<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class ProcedureOpenConstraint extends Constraint
{
    public $messageUnauthorized = 'Ce champ n\'est pas autorisé.';
    public $messageRequired = 'Ce champ est obligatoire.';
}
