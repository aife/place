<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator\Chorus;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class IsFicheModificativeValid extends Constraint
{
    public string $echangeChorusVisaNotValid = "TEXT_VALIDATOR_ECHANGE_CHORUS_FIELD_VISA_NOT_VALID";
}
