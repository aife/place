<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator\Chorus;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class IsEchangeChorusContratEmpty extends Constraint
{
    public string $echangeChorusContratEmpty = "TEXT_VALIDATOR_EMPTY_ECHANGE_CHORUS_CONTRAT";
}
