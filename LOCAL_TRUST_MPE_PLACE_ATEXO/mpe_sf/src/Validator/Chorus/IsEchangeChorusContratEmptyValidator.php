<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator\Chorus;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\Translation\TranslatorInterface;

class IsEchangeChorusContratEmptyValidator extends ConstraintValidator
{
    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    /**
     * @param IsEchangeChorusContratEmpty $value
     */
    public function validate($value, Constraint $constraint): void
    {
        if (is_null($value)) {
            $this->context->buildViolation(
                $this->translator->trans($constraint->echangeChorusContratEmpty)
            )
                ->setCode(Response::HTTP_BAD_REQUEST)
                ->addViolation();
        }
    }
}
