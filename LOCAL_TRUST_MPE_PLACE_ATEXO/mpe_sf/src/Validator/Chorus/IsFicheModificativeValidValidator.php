<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator\Chorus;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\Translation\TranslatorInterface;

class IsFicheModificativeValidValidator extends ConstraintValidator
{
    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    /**
     * @param IsFicheModificativeValid $value
     */
    public function validate($value, Constraint $constraint)
    {
        if (!in_array($value->visaPrefet, ['OUI', 'NON']) || !in_array($value->visaACCF, ['OUI', 'NON'])) {
            $this->context->buildViolation(
                $this->translator->trans($constraint->echangeChorusVisaNotValid)
            )
                ->setCode(Response::HTTP_BAD_REQUEST)
                ->addViolation();

            return;
        }
    }
}
