<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator\Chorus;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class IsEchangeChorusExecUuidEmpty extends Constraint
{
    public string $echangeChorusExecUuidEmpty = "TEXT_VALIDATOR_EMPTY_ECHANGE_CHORUS_EXEC_UUID";

    public string $echangeChorusInProgress = "TEXT_VALIDATOR_IN_PROGRESS_ECHANGE_CHORUS";
}
