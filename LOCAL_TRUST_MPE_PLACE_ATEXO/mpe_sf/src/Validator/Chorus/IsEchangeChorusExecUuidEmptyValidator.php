<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator\Chorus;

use App\Entity\Chorus\ChorusEchange;
use App\Repository\Chorus\ChorusEchangeRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\Translation\TranslatorInterface;

class IsEchangeChorusExecUuidEmptyValidator extends ConstraintValidator
{
    public function __construct(
        private readonly TranslatorInterface $translator,
        private readonly ChorusEchangeRepository $chorusEchangeRepository
    ) {
    }

    /**
     * @param IsEchangeChorusExecUuidEmpty $value
     */
    public function validate($value, Constraint $constraint)
    {
        if (is_null($value) || empty($value)) {
            $this->context->buildViolation(
                $this->translator->trans($constraint->echangeChorusExecUuidEmpty)
            )
                ->setCode(Response::HTTP_BAD_REQUEST)
                ->addViolation();

            return;
        }

        /** @var ChorusEchange $echangeChorus */
        $echangeChorus = $this->chorusEchangeRepository
            ->findOneBy(['uuidExterneExec' => $value]);

        if (!is_null($echangeChorus) && in_array($echangeChorus->getStatutEchange(), ['FAG', 'EP', '1'])) {
            $this->context->buildViolation(
                $this->translator->trans($constraint->echangeChorusInProgress)
            )
                ->setCode(Response::HTTP_BAD_REQUEST)
                ->addViolation();
        }
    }
}
