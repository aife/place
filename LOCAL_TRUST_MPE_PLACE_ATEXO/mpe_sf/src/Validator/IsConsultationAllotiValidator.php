<?php

namespace App\Validator;

use App\Entity\Consultation;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface;
use Symfony\Component\HttpFoundation\Response;

class IsConsultationAllotiValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     *
     * @var Consultation $value
     */
    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint IsConsultationAlloti */

        if (!$value || ($value && $value->isAlloti())) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setCode(Response::HTTP_BAD_REQUEST)
            ->addViolation();
    }
}
