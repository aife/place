<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use App\Service\Procedure\ModuleRuleInterface;
use Symfony\Component\DependencyInjection\Attribute\TaggedLocator;
use Symfony\Component\DependencyInjection\ServiceLocator;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ModulesRulesConstraintValidator extends ConstraintValidator
{
    use ApiVersionTrait;

    public function __construct(
        #[TaggedLocator(ModuleRuleInterface::class, defaultIndexMethod: 'getRuleName')]
        private ServiceLocator $rules,
        private Security $security
    ) {
    }

    public function validate($value, Constraint $constraint)
    {
        if ($this->isValidatorDisabledForApiVersion()) {
            return;
        }

        $ruleService = $this->rules->get($constraint->rule);
        $inputObject = $this->context->getObject();
        if ($inputObject->organisme) {
            $organism = $inputObject?->organisme;
        } else {
            $organism = $this->security?->getUser()?->getOrganisme();
        }

        if (null !== $value && !$ruleService->isActive($organism)) {
            $this->context->buildViolation($constraint->messageUnauthorized)->addViolation();
        }

        if ('ClausesRule' === $constraint->rule && true === $inputObject->alloti) {
            return;
        }

        if (null === $value && true === $constraint->mandatory && $ruleService->isActive($organism)) {
            $this->context->buildViolation($constraint->messageRequired)->addViolation();
        }
    }
}
