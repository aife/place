<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsValidOrganisme extends Constraint
{
    public $message = "L'organisme n'est pas renseigné ou n'est pas valide";
}
