<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsValeurEstimeeMandatory extends Constraint
{
    public string $invalidValeurEstimee = "TEXT_VALIDATOR_INVALID_VALEUR_ESTIMEE";

    public string $notEmptyValeurEstimee = "TEXT_VALIDATOR_NOT_EMPTY_VALEUR_ESTIMEE";
}
