<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use App\Repository\ConfigurationPlateformeRepository;
use App\Service\WebServices\ApiEndpointLibrary;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\Translation\TranslatorInterface;

class IsValeurEstimeeMandatoryValidator extends ConstraintValidator
{
    public function __construct(
        private readonly ConfigurationPlateformeRepository $configurationPlateformeRepository,
        private readonly TranslatorInterface $translator,
        private readonly RequestStack $requestStack,
    ) {
    }

    /**
     * {@inheritdoc}
     * @param string $value
     * @param IsValeurEstimeeMandatory $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if ((bool)$this->requestStack->getCurrentRequest()->query?->get(ApiEndpointLibrary::KEY_DUPLICATE)) {
            return;
        }

        $conf = $this->configurationPlateformeRepository->getConfigurationPlateforme();

        if ($conf->getPublicite() && $conf->getAfficherValeurEstimee() && empty($this->context->getObject()->typeProcedure?->getProcedureSimplifie())) {
            if (empty($value) || !is_numeric($value) || $value < 1) {
                $this->context->buildViolation($this->translator->trans($constraint->invalidValeurEstimee))
                    ->setCode(Response::HTTP_BAD_REQUEST)
                    ->addViolation();
            }
        }
    }
}
