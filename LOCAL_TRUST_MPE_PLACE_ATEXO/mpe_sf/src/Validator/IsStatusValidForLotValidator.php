<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use App\Entity\Consultation;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsStatusValidForLotValidator extends ConstraintValidator
{
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
    ) {
    }

    /**
     * {@inheritdoc}
     * @param Consultation $value
     */
    public function validate($value, Constraint $constraint)
    {
        if (
            !empty($value) &&
            (int)$value->getCalculatedStatus() >= (int)$this->parameterBag->get('STATUS_CONSULTATION')
        ) {
            $this->context->buildViolation($constraint->message)
                ->setCode(Response::HTTP_BAD_REQUEST)
                ->addViolation();
        }
    }
}
