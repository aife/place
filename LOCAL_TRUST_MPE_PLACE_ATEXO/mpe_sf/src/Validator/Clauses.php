<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Clauses extends Constraint
{
    public string $notEmptyClauses = "TEXT_VALIDATOR_NOT_EMPTY_CLAUSES";

    public string $invalidClauseN1Referentiel = "Un référentiel de niveau 1 est manquant ou non valide";
    public string $invalidClauseN2Referentiel = "Un référentiel de niveau 2 est manquant ou non valide";
    public string $invalidClauseN3Referentiel = "Un référentiel de niveau 3 est manquant ou non valide";
    public string $invalidClauseN4Referentiel = "Un référentiel de niveau 4 est manquant ou non valide";

    public string $noClausesN2Message = "Les clauses de niveau 2 sont manquantes";
    public string $noClausesN3Message = "Les clauses de niveau 3 sont manquantes";
    public string $noClausesN4Message = "Les clauses de niveau 4 sont manquantes";

    public string $noLinkClausesN1N2Message = "Une clause de niveau 2 n'est pas liée à sa clause de niveau 1";
    public string $noLinkClausesN2N3Message = "Une clause de niveau 3 n'est pas liée à sa clause de niveau 2";
    public string $noLinkClausesN3N4Message = "Une clause de niveau 4 n'est pas liée à sa clause de niveau 3";
}
