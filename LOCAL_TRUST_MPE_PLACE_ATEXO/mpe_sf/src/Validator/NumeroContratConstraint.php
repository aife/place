<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class NumeroContratConstraint extends Constraint
{
    public $message = 'Ce numéro de contrat n\'existe pas';
    public $messageInvalid = 'Ce numéro de contrat est invalide';
}
