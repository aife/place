<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class ProcedureCpvConstraint extends Constraint
{
    public $messageUnauthorized = 'Le champ cpv n\'est pas autorisé.';
    public $messageRequired = 'Le champ cpv est obligatoire.';
}
