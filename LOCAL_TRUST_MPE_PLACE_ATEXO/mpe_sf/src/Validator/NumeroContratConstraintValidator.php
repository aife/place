<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use App\Entity\ContratTitulaire;
use App\Entity\TypeContrat;
use App\Repository\ContratTitulaireRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class NumeroContratConstraintValidator extends ConstraintValidator
{
    public function __construct(private ContratTitulaireRepository $contratTitulaireRepository, private EntityManagerInterface $entityManager, private ParameterBagInterface $parameterBag)
    {
    }

    public function validate($value, Constraint $constraint)
    {
        if (empty($value) || !empty($this->parameterBag->get('ACTIVE_EXEC_V2'))) {
            return;
        }

        $consultation = $this->context->getObject();
        if ($consultation?->idContratTitulaire) {
            $contrat = $this->contratTitulaireRepository->find($consultation?->idContratTitulaire);
        }
        if (!$contrat instanceof ContratTitulaire) {
            $contrat = $this->contratTitulaireRepository->getSadAndAcContratByNumeroContrat(
                $value,
                $consultation?->typeContrat?->getMarcheSubsequent()
            );
        }

        if (!$contrat instanceof ContratTitulaire) {
            $this->context->buildViolation($constraint->message)->addViolation();
        } else {
            $typeContrat = $this->entityManager->getRepository(TypeContrat::class)->find($contrat->getIdTypeContrat());
            if ($consultation?->typeContrat?->getMarcheSubsequent() !== $typeContrat?->getAccordCadreSad()) {
                $this->context->buildViolation($constraint->messageInvalid)->addViolation();
            }
        }
    }
}
