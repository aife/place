<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class ModulesRulesConstraint extends Constraint
{
    public function __construct(
        public string $rule,
        public bool $mandatory = false,
        $options = null,
        array $groups = null,
        $payload = null
    ) {
        parent::__construct($options, $groups, $payload);
    }

    public $messageUnauthorized = 'Ce champ n\'est pas autorisé.';
    public $messageRequired = 'Ce champ est obligatoire.';
}
