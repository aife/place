<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsValidUniqueLot extends Constraint
{
    public $message = 'Impossible d\'ajouter/modifier lot: 
    Ce numéro de lot "{{ numeroLot }}" existe déjà pour cette consultation';
}
