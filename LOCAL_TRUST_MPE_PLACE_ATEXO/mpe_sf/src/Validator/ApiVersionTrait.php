<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\Service\Attribute\Required;

trait ApiVersionTrait
{
    private RequestStack $requestStack;

    private function isValidatorDisabledForApiVersion(): bool
    {
        $apiVersion = $this->requestStack->getCurrentRequest()->headers->get('x-api-version');

        return null === $apiVersion || $apiVersion < 1;
    }

    #[Required]
    public function setRequest(RequestStack $requestStack): void
    {
        $this->requestStack = $requestStack;
    }
}
