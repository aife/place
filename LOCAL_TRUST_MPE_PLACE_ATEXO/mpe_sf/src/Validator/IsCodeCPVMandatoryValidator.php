<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Validator;

use App\Repository\ConfigurationPlateformeRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\Translation\TranslatorInterface;

class IsCodeCPVMandatoryValidator extends ConstraintValidator
{
    public function __construct(
        private readonly ConfigurationPlateformeRepository $configurationPlateformeRepository,
        private readonly TranslatorInterface $translator
    ) {
    }

    /**
     * {@inheritdoc}
     * @param string $value
     * @param IsCodeCPVMandatory $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $inputObject = $this->context->getObject();
        if (property_exists($inputObject, 'alloti') && !empty($inputObject->alloti)) {
            return;
        }

        $conf = $this->configurationPlateformeRepository->getConfigurationPlateforme();

        if ($conf->getAffichageCodeCpv()) {
            if (empty($value) || !is_numeric($value)) {
                $this->context->buildViolation($this->translator->trans($constraint->invalidCodeCPV))
                    ->setCode(Response::HTTP_BAD_REQUEST)
                    ->addViolation();
            }
        }
    }
}
