<?php

namespace App\Twig;

use App\Entity\Consultation;
use App\Service\TBourseCotraitanceService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ConsultationBourseExtension extends AbstractExtension
{
    public function __construct(
        protected TBourseCotraitanceService $bourseCotraitanceService
    ) {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_bourse_picto', [$this, 'getPicto'])
        ];
    }

    public function getPicto(Consultation $consultation)
    {
        $cas = $this->bourseCotraitanceService->getCasGroupement($consultation);
        switch ($cas) {
            case 'cas1':
                return 'icone-cotraitance-1.png';
            case 'cas2':
                return 'icone-cotraitance-2.png';
            case 'cas3':
                return 'icone-cotraitance-3.png';
            case 'cas4':
                return 'icone-cotraitance-4.png';
        }

        return 'icone-cotraitance-pas-actif.png';
    }
}
