<?php

namespace App\Twig;

use App\Entity\Organisme;
use App\Service\AtexoConfiguration;
use App\Service\ConfigurationOrganismeService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ModuleRightExtension extends AbstractExtension
{
    public function __construct(
        private readonly AtexoConfiguration $configuration,
        private readonly ConfigurationOrganismeService $configurationOrganismeService
    ) {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('is_module_enabled', [$this, 'isModuleEnabled']),
            new TwigFunction('is_module_disabled_for_one_organisme', [$this, 'isModuleDisabledForOneOrganisme']),
        ];
    }

    public function isModuleEnabled(string $moduleName, ?Organisme $organisme, string $prefixe = 'get'): bool
    {
        return $this->configuration->isModuleEnabled($moduleName, $organisme, $prefixe);
    }

    public function isModuleDisabledForOneOrganisme(string $moduleName): bool
    {
        return $this->configurationOrganismeService->isModuleDisabledForOneOrganisme($moduleName);
    }
}
