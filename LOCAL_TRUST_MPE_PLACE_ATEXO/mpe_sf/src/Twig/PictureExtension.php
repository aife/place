<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Twig;

use App\Service\File\FileUploader;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class PictureExtension extends AbstractExtension
{
    public function __construct(private readonly FileUploader $fileUploader)
    {
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('get_file_in_nas', array($this, 'recoverFromNAS')),
            new TwigFunction('file_exists', array($this, 'fileExists')),
        ];
    }

    public function recoverFromNAS(string $sourceDirectory, string $fileName): ?string
    {
        $this->fileUploader->copyDirectoryToPublic($sourceDirectory, 'images');

        return 'images/' . $fileName;
    }

    public function fileExists(string $path): bool
    {
        return file_exists($path);
    }
}
