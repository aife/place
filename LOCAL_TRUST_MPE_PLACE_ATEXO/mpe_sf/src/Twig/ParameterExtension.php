<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Twig;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ParameterExtension extends AbstractExtension
{
    public function __construct(protected ParameterBagInterface $params)
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_parameter', [$this, 'getParameter'])
        ];
    }

    public function getParameter(string $parameter): ?string
    {
        return $this->params->get($parameter);
    }
}
