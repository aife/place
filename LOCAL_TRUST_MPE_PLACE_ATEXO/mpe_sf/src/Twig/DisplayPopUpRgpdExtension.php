<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Twig;

use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;
use App\Service\RgpdService;

class DisplayPopUpRgpdExtension extends AbstractExtension
{

    public function __construct(private RgpdService $rgpdService)
    {
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('displayRgpd', [$this, 'displayRgpd']),
        ];
    }

    public function displayRgpd(): bool
    {
        return $this->rgpdService->displayPopUpRgpd();
    }
}