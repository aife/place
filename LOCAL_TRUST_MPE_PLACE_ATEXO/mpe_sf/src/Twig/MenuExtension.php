<?php

namespace App\Twig;

use App\Service\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class MenuExtension extends AbstractExtension
{
    /**
     * MenuExtension constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(
        protected EntityManagerInterface $em,
        private readonly Configuration $configuration,
        private readonly RouterInterface $router,
        private readonly AppTranslationExtension $translator
    ) {
    }

    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('urlSocle', [$this, 'getUrlSocle']),
            new TwigFunction('urlSocleVisible', [$this, 'getUrlSocleVisible']),
            new TwigFunction('urlPortail', [$this, 'getUrlPortail']),
            new TwigFunction('logoBoamp', [$this, 'getLogoBoamp']),
            new TwigFunction('visibleLink', [$this, 'getVisibleLink']),
            new TwigFunction('visibleLinkListeAcRGS', [$this, 'getVisibleLinkListeAcRGS']),
            new TwigFunction('isActiveAuthentificationInscritByForm', [$this, 'isActiveAuthentificationInscritByForm']),
            new TwigFunction('isActiveSocleEntreprise', [$this, 'isActiveSocleEntreprise']),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getUrlSocle()
    {
        $isEnabled = $this->configuration->getConfigurationPlateforme();
        $url = null;
        if ('1' === $isEnabled->getSocleExterneEntreprise()) {
            $url = $this->configuration->getParameter('URL_INDEX_SSO_ENTREPRISE');
        } elseif ('1' === $isEnabled->getSocleExternePpp()) {
            $url = $this->configuration->getParameter('URL_PPP_INSCRIPTION');
        } elseif ('1' === $isEnabled->getAccueilEntreprisePersonnalise()) {
            $url = $this->getUrlPf() . '?page=Entreprise.EntrepriseHome&goto='
                . urlencode('?page=Entreprise.EntrepriseAccueilAuthentifie')
                . '&inscription';
        } else {
            $url = $this->getUrlPf() . '?page=Entreprise.EntrepriseHome';
        }

        return $url;
    }

    /**
     * @return mixed
     */
    public function getUrlPf()
    {
        $url = $this->configuration->getParameter('PF_URL_ENTREPRISE');
        if (
            1 === preg_match(
                '/' . $this->router->getContext()->getHost() . '/',
                (string) $this->configuration->getParameter('PF_URL_AGENT')
            )
        ) {
            $url = $this->configuration->getParameter('PF_URL_AGENT');
        }

        return $url;
    }

    public function getUrlPortail()
    {
        $isEnabled = $this->configuration->getConfigurationPlateforme();
        $urlPortal = null;
        if ($this->configuration->getParameter('URL_ACCUEIL_SPIP')) {
            $urlPortal = $this->configuration->getParameter('URL_ACCUEIL_SPIP');
        }

        if ('1' === $isEnabled->getSocleExternePpp()) {
            $urlPortal = $this->configuration->getParameter('URL_SOCLE_EXTERNE');
        }

        return $urlPortal;
    }

    /**
     * @return bool
     */
    public function getUrlSocleVisible()
    {
        $isEnabled = $this->configuration->getConfigurationPlateforme();
        $visible = false;
        if ('1' === $isEnabled->getSocleExterneEntreprise() || '1' === $isEnabled->getSocleExternePpp()) {
            $visible = true;
        }

        return $visible;
    }

    /**
     * @return bool
     */
    public function getLogoBoamp()
    {
        $visible = false;
        if ('1' === $this->configuration->getParameter('AFFICHER_BANDEAU_LOGO_BOAMP_AGENT')) {
            $visible = true;
        }

        return $visible;
    }

    /**
     * @param $string
     *
     * @return bool
     */
    public function getVisibleLink($string)
    {
        $visible = true;

        if ('' !== $this->translator->trans($string) && $this->translator->trans($string) === $string) {
            $visible = false;
        }

        return $visible;
    }

    /**
     * @return bool
     */
    public function getVisibleLinkListeAcRGS()
    {
        $visible = false;
        $isEnabled = $this->configuration->getConfigurationPlateforme();
        if ('1' === $isEnabled->getListeACRgs()) {
            $visible = true;
        }

        return $visible;
    }

    /**
     * @return string
     */
    public function isActiveAuthentificationInscritByForm()
    {
        return $this->configuration->getConfigurationPlateforme()->getAuthenticateInscritByLogin();
    }

    /**
     * @return string
     */
    public function isActiveSocleEntreprise()
    {
        return $this->configuration->getConfigurationPlateforme()->getSocleExterneEntreprise();
    }
}
