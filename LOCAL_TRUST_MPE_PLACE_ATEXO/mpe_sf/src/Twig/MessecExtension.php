<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Twig;

use App\Service\Messagerie\WebServicesMessagerie;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class MessecExtension extends AbstractExtension
{
    public function __construct(private readonly WebServicesMessagerie $webServicesMessagerie)
    {
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('messec_access_token', [$this, 'getAccessToken']),
        ];
    }

    public function getAccessToken(): mixed
    {
        return $this->webServicesMessagerie->getContent('getToken');
    }
}
