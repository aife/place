<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ThousandSeparatorExtension extends AbstractExtension
{
    private const DECIMAL = 0;
    private const DECIMAL_SEPARATOR = ',';
    private const THOUSANDS_SEPARATOR = ' ';

    public function getFilters()
    {
        return [
            new TwigFilter('thousand_separator', [$this, 'thousandSeparator']),
        ];
    }

    public function thousandSeparator(?int $number): string
    {
        return number_format(
            $number,
            self::DECIMAL,
            self::DECIMAL_SEPARATOR,
            self::THOUSANDS_SEPARATOR
        );
    }
}
