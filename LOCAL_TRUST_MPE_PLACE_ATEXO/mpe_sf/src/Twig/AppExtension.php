<?php

namespace App\Twig;

use DateTime;
use App\Service\AtexoUtil;
use App\Utils\Translation;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/***
 * @author updatedBy Mohamed BLAL
 *
 * Class AppExtension
 * @package App\Twig
 */
class AppExtension extends AbstractExtension
{
    private const LINE_BREAK_CHARS = ["\n", "\r"];

    public function __construct(
        private readonly Translation $translationUtil,
        private readonly AtexoUtil $atexoUtil,
        private readonly SessionInterface $session,
        private LoggerInterface $logger
    ) {
    }

    public function getFilters()
    {
        return [
            new TwigFilter('noLineBreak', [$this, 'noLineBreak']),
            new TwigFilter('iso2frnDateTime', [$this, 'iso2frnDateTimeFilter']),
            new TwigFilter('fileSize', [$this, 'getSizeName']),
            new TwigFilter('formatCurrency', [$this, 'getFormatCurrency']),
            new TwigFilter('doubleAtTrans', [$this, 'getDoubleAtTrans']),
            new TwigFilter('addslashes', 'addslashes'),
            new TwigFilter('htmlspecialchars_decode', 'htmlspecialchars_decode', ['is_safe' => ['html']]),
            new TwigFilter('base64_encode', 'base64_encode'),
            new TwigFilter('base64_decode', 'base64_decode'),
        ];
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('get_flashes', array($this, 'getFlashes')),
            new TwigFunction('file_asset_exists', [$this, 'fileAssetExists']),
        ];
    }

    /**
     * Replace line break by blank space.
     */
    public function noLineBreak(string $str): string
    {
        return str_replace(self::LINE_BREAK_CHARS, ' ', $str);
    }

    /**
     * @param $octets
     */
    public function getSizeName($octets): string
    {
        return $this->atexoUtil->getSizeName($octets);
    }

    public function iso2frnDateTimeFilter($_isoDate, $withTime = true, $withTimeSeconds = false)
    {
        if ($_isoDate instanceof DateTime) {
            if (empty($_isoDate) or date_timestamp_get($_isoDate) < 0) {
                return;
            }
            $_isoDate = $_isoDate->format('Y-m-d H:i:s');
        }
        if (!$_isoDate || '0000-00-00 00:00:00' == $_isoDate) {
            return;
        }
        if (10 == strlen((string) $_isoDate)) {
            return $this->iso2frnDate($_isoDate);
        }

        [$a, $m, $j_et_heure] = explode('-', (string) $_isoDate);
        [$j, $heure] = explode(' ', $j_et_heure);
        [$h, $min, $sec] = explode(':', $heure);
        $date = "$j/$m/$a";
        if ($withTime) {
            $date = "$j/$m/$a $h:$min";
        }
        if ($withTimeSeconds) {
            $sec = empty($sec) ? '00' : $sec;
            $date = "$j/$m/$a $h:$min:$sec";
        }

        return $date;
    }

    public function getName()
    {
        return 'app_extension';
    }

    /** convertit une date ISO en date française.
     * @param $_isoDate date au format yyyy-mm-dd
     *
     * @return date au format dd/mm/yyyy
     */
    protected function iso2frnDate($_isoDate)
    {
        if (!$_isoDate || '0000-00-00' == $_isoDate) {
            return;
        }
        if (strstr($_isoDate, '-')) {
            [$y, $m, $d] = explode('-', (string) $_isoDate);

            return "$d/$m/$y";
        } else {
            return $_isoDate;
        }
    }

    public function getFormatCurrency($value)
    {
        $value = str_replace(',', ' ', $value);
        $value = str_replace('.', ',', $value);

        return $value;
    }

    public function getFlashes(string $type): array
    {
        return $this->session->getFlashBag()->get($type);
    }

    /**
     * Traduit les chaines suffixé et préfixé par un double @.
     *
     * @param $messages
     */
    public function getDoubleAtTrans($messages): string
    {
        return $this->translationUtil->doubleAtMessagesTranslate($messages);
    }

    public function fileAssetExists(string $path): bool
    {
        $fileHeaders = $this->getHeaders($path);

        $fileExist = $fileHeaders && $fileHeaders[0] != 'HTTP/1.1 404 Not Found';

        if (!$fileExist) {
            $this->logger->error(
                sprintf("Le fichier %s est introuvable sur le serveur", $path)
            );
        }

        return $fileExist;
    }

    protected function getHeaders(string $path): array|false
    {
        return @get_headers($path);
    }
}
