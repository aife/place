<?php

namespace App\Twig;

use App\Entity\EnveloppeFichier;
use App\Entity\Lot;
use App\Service\AtexoConfiguration;
use Symfony\Component\Asset\Packages;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Class AtexoCustomTwigMethod de gestion des methodes twig personnalisees.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @since ESR2016
 *
 * @copyright Atexo 2016
 */
class AtexoCustomTwigMethod extends AbstractExtension
{
    private static array $units = [
        'y' => 'year',
        'm' => 'month',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    ];
    private array $infosSignature = [];
    private $container;
    private $assets;

    /**
     * AtexoCustomTwigMethod constructor.
     *
     * @param ContainerInterface $container
     * @param Packages $assets
     * @param SessionInterface $session
     * @param TranslatorInterface $translator
     * @param AtexoConfiguration $moduleStateChecker
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function __construct(
        ContainerInterface $container,
        Packages $assets,
        private SessionInterface $session,
        private TranslatorInterface $translator,
        private AtexoConfiguration $moduleStateChecker,
        private ParameterBagInterface $parameterBag
    ) {
        $this->container = $container;
        $this->assets = $assets;
    }

    /**
     * @return array
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getInfosSignature()
    {
        return $this->infosSignature;
    }

    /**
     * @param array $infosSignature
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function setInfosSignature($infosSignature)
    {
        $this->infosSignature = $infosSignature;
    }

    /**
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getAssets()
    {
        return $this->assets;
    }

    /**
     * @param mixed $assets
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function setAssets($assets)
    {
        $this->assets = $assets;
    }

    /**
     * Les methodes twig personnalisees.
     *
     * @return array
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('infos_signature', [$this, 'getInfosSignatureFichier']),
            new TwigFunction('picto_signed', [$this, 'getPictoSigned']),
            new TwigFunction('picto_statut_signature', [$this, 'getPictoStatutSignature']),
            new TwigFunction('libelle_statut_signature', [$this, 'getLibelleStatutSignature']),
            new TwigFunction('nom_enveloppe_offre', [$this, 'getNomEnveloppeOffre']),
            new TwigFunction('lien_acces_agent_consultation', [$this, 'getLienAccesAgentConsultation']),
            new TwigFunction('date_diff', [$this, 'getDateDiff']),
            new TwigFunction('getTime', [$this, 'getTime']),
            new TwigFunction('getJourneeDeLAnnee', [$this, 'getJourneeDeLAnnee']),
            new TwigFunction('parameter', [$this, 'getParameter']),
            new TwigFunction('unescape', [$this, 'unescape']),
        ];
    }

    /**
     * Les filtres twig personnalises.
     *
     * @return array
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getFilters()
    {
        return [
            new TwigFilter('json_decode', 'json_decode'),
            new TwigFilter('date_diff', [$this, 'getDateDiff']),
        ];
    }

    /**
     * Retourne le nom du service parmi les services twig.
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getName()
    {
        return 'atexo_consultation_depot_custom_twig_extension';
    }

    public function getThemeUrl()
    {
        return ($this->session->isStarted() && $this->session->has('contexte_authentification/theme_url'))
            ? $this->session->get('contexte_authentification/theme_url')
            : $this->container->getParameter('PF_URL_REFERENCE') . 'themes';
    }

    /**
     * Permet de recuperer les infos de la signature pour un fichier.
     *
     * @param $fichier
     * @return string
     * @throws \JsonException
     * @since ESR2016
     *
     * @copyright Atexo 2016
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function getInfosSignatureFichier($fichier): string
    {
        if (
            $fichier instanceof EnveloppeFichier &&
            !empty($fichier->getIdFichierSignature()) &&
            !empty($fichier->getIdBlobSignature())
        ) {
            $arrayInfosSignature = json_decode(
                utf8_encode($fichier->getSignatureInfos()),
                true,
                512,
                JSON_THROW_ON_ERROR
            );

            if (
                is_array($arrayInfosSignature) &&
                !empty($arrayInfosSignature) &&
                array_key_exists('signatairePartiel', $arrayInfosSignature) &&
                array_key_exists('emetteur', $arrayInfosSignature)
            ) {
                $message = 'CN : ' . $arrayInfosSignature['signatairePartiel']
                    . ' ( AC :  ' . $arrayInfosSignature['emetteur'];
            } else {
                $message = $this->translator->trans('VALIDITE_SIGNATURE_NON_VERIFIABLE');
            }
        } else {
            $message = $this->translator->trans('TEXT_AUCUNE_SIGNATURE');
        }

        return $message;
    }

    /**
     * Permet de recuperer le picto du statut de la signature.
     *
     * @param string           $infoSignature json infos de la signature
     * @param EnveloppeFichier $fichier       objet fichier
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getPictoStatutSignature($fichier, $infoSignature)
    {
        $statut = $this->getStatutSignatureFichier($fichier, $infoSignature);

        return match ($statut) {
            'OK' => $this->getThemeUrl() . '/images/picto-check-ok-small.gif',
            'NOK' => $this->getThemeUrl() . '/images/picto-check-not-ok.gif',
            'UNKNOWN' => $this->getThemeUrl() . '/images/picto-validation-incertaine.gif',
            'UNVERIFIED' => $this->getThemeUrl() . '/images/picto-vide.gif',
            default => $this->getThemeUrl() . '/images/picto-vide.gif',
        };
    }

    /**
     * @param $fichier
     * @param $infoSignature
     *
     * @return string
     */
    public function getStatutSignatureFichier($fichier, $infoSignature)
    {
        $statut = '';

        if ($fichier instanceof EnveloppeFichier && empty($fichier->getStatutSignature())) {
            if (!empty($fichier->getIdFichierSignature()) && !empty($fichier->getIdBlobSignature())) {
                $arrayInfosSignature = json_decode(utf8_encode($infoSignature), true, 512, JSON_THROW_ON_ERROR);
                $resNok = false;
                $resUnkown = false;
                $unverified = false;

                if ($this->moduleStateChecker->hasConfigPlateforme('surchargeReferentiels')) {
                    if (
                        array_key_exists('repertoiresChaineCertification', $arrayInfosSignature) &&
                        null != $arrayInfosSignature['repertoiresChaineCertification'] &&
                        array_key_exists('statut', $arrayInfosSignature['repertoiresChaineCertification'][0]) &&
                        $arrayInfosSignature['repertoiresChaineCertification'][0]['statut'] == $this->getParameter(
                            'statut_referentiel_certificat_ko'
                        )
                    ) {
                        $resNok = true;
                    } elseif (
                        !array_key_exists('repertoiresChaineCertification', $arrayInfosSignature) ||
                        null == $arrayInfosSignature['repertoiresChaineCertification'] ||
                        (
                            null != $arrayInfosSignature['repertoiresChaineCertification'] &&
                            array_key_exists('statut', $arrayInfosSignature['repertoiresChaineCertification'][0]) &&
                            $arrayInfosSignature['repertoiresChaineCertification'][0]['statut'] != $this->getParameter(
                                'statut_referentiel_certificat_ok'
                            )
                        )
                    ) {
                        $resUnkown = true;
                    }
                } else {
                    if (
                        array_key_exists('chaineDeCertificationValide', $arrayInfosSignature) &&
                        2 == $arrayInfosSignature['chaineDeCertificationValide']
                    ) {
                        $resNok = true;
                    } elseif (
                        !array_key_exists(
                            'chaineDeCertificationValide',
                            $arrayInfosSignature
                        ) || 0 != $arrayInfosSignature['chaineDeCertificationValide']
                    ) {
                        $resUnkown = true;
                    }
                }

                if (
                    array_key_exists('absenceRevocationCRL', $arrayInfosSignature) &&
                    2 == $arrayInfosSignature['absenceRevocationCRL']
                ) {
                    $resNok = true;
                } elseif (
                    !array_key_exists('absenceRevocationCRL', $arrayInfosSignature) ||
                    0 != $arrayInfosSignature['absenceRevocationCRL']
                ) {
                    $resUnkown = true;
                }

                if (!array_key_exists('signatureValide', $arrayInfosSignature)) {
                    $unverified = true;
                } elseif (
                    array_key_exists('signatureValide', $arrayInfosSignature) &&
                    false == $arrayInfosSignature['signatureValide']
                ) {
                    $resNok = true;
                } elseif (true != $arrayInfosSignature['signatureValide']) {
                    $resUnkown = true;
                }

                if (
                    !array_key_exists('periodiciteValide', $arrayInfosSignature) ||
                    !$arrayInfosSignature['periodiciteValide']
                ) {
                    $resNok = true;
                }

                if ($unverified) {
                    $statut = 'UNVERIFIED';
                } elseif ($resNok) {
                    $statut = 'NOK';
                } elseif ($resUnkown) {
                    $statut = 'UNKNOWN';
                } else {
                    $statut = 'OK';
                }

                $fichier->setStatutSignature($statut);
            }
        } else {
            $statut = ($fichier instanceof EnveloppeFichier) ? $fichier->getStatutSignature() : '';
        }

        return $statut;
    }

    /**
     * Permet de recuperer un parametre.
     *
     * @param $param
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getParameter($param)
    {
        return $this->container->getParameter($param);
    }

    /**
     * Permet de recuperer le picto de la signature en fonction du parametre.
     *
     * @param $signed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getPictoSigned($signed): string
    {
        if (null != $signed) {
            return $this->getThemeUrl() . '/images/picto-certificat-small.gif';
        }

        return $this->getThemeUrl() . '/images/picto-aucune-signature.gif';
    }

    /**
     * Permet de recuperer le nom de l'enveloppe.
     *
     * @param int    $refCons
     * @param string $organisme
     * @param Lot    $lot
     * @param string $typeEnv
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getNomEnveloppeOffre($lot, $typeEnv): string
    {
        if ($typeEnv == $this->container->getParameter('TYPE_ENV_CANDIDATURE')) {
            return $this->translator->trans('DEFINE_DOSSIER_CANDIDATURE');
        }

        $nomEnveloppe = '';

        if ($lot instanceof Lot) {
            $nomEnveloppe = $this->translator->trans('TEXT_LOT') . ' ' . $lot->getLot() . ' - ';
        }

        if ($typeEnv == $this->container->getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
            $nomEnveloppe .= $this->translator->trans('DOSSIER_OFFRE_TECHNIQUE');
        } elseif ($typeEnv == $this->container->getParameter('TYPE_ENV_OFFRE')) {
            $nomEnveloppe .= $this->translator->trans('DEFINE_DOSSIER_OFFRES');
        } elseif ($typeEnv == $this->container->getParameter('TYPE_ENV_ANONYMAT')) {
            $nomEnveloppe .= $this->translator->trans('DOSSIER_OFFRE_ANONYMAT');
        }

        if ($lot instanceof Lot) {
            $nomEnveloppe .= ' : ' . $lot->getDescription();
        }

        return html_entity_decode($nomEnveloppe);
    }

    /**
     * Permet de construire le lien d'acces agent a la consultation.
     *
     * @param $urlMpe
     * @param $ref
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getLienAccesAgentConsultation($urlMpe, $ref)
    {
        $pfUrl = $this->getContainer()->get('atexo.atexo_util')->getPfUrl();
        $pfUrl = rtrim($pfUrl, '/');
        $params = [
            'page' => 'agent.TableauDeBord',
            'AS' => '0',
            'ref' => $ref,
        ];

        return $pfUrl . '/?' . http_build_query($params);
    }

    /**
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param mixed $container
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * Permet de decoder une chaine json en objet.
     *
     * @param $chaine
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function json_decode($chaine)
    {
        return json_decode(utf8_encode($chaine), true, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @param $date
     * @param null $now
     * @param bool $returnAllDetails
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getDateDiff($date, $now = null, $returnAllDetails = false)
    {


        $locale = $this->session->get('_locale') ?? $this->parameterBag->get('locale');

        // Convert both dates to DateTime instances.
        $now = new \DateTime($now);

        // Get the difference between the two DateTime objects.
        $diff = $date->diff($now);

        $dateDiff = '';
        $nbrJours = 0;

        foreach (self::$units as $attribute => $unit) {
            $count = $diff->$attribute;

            if (0 === $count && 0 === $nbrJours) {
                continue;
            }

            if (false === $returnAllDetails) {
                if ('y' == $attribute) {
                    $nbrJours += $count * 365;
                    continue;
                }

                if ('m' == $attribute) {
                    $nbrJours += round($count * 30.4167);
                    continue;
                }

                if ('d' == $attribute) {
                    $count += $nbrJours;
                }

                if ('s' == $attribute) {
                    continue;
                }
            }

            $dateDiff .= ' ' . $this->getPluralizedInterval($count, $unit, $locale);
        }

        return $diff->invert ? ('' != $dateDiff ? $this->translator->trans(
            'DEFINE_DANS_DATE',
            ['%time%' => $dateDiff], null, $locale
        ) : $this->translator->trans('DANS_MOINS_UNE_MINUTE')) : $this->translator->trans(
            'DEFINE_IL_Y_A_DATE',
            ['%time%' => $dateDiff], null, $locale
        );
    }

    /**
     * @param $count
     * @param $unit
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    protected function getPluralizedInterval($count, $unit, $lang)
    {


        if ($this->translator) {
            $id = sprintf('diff_%s', $unit);

            return $this->translator->trans($id, ['%count%' => $count], null,  $lang);
        }

        if (1 !== $count) {
            $unit .= 's';
        }

        return $count . ' '. $unit;
    }

    public function getTime()
    {
        return time() * 1000;
    }

    public function getJourneeDeLAnnee()
    {
        return date('Z');
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function unescape($value)
    {
        return html_entity_decode($value);
    }

    /**
     * Permet de recuperer le picto du statut de la signature.
     *
     * @param string           $infoSignature json infos de la signature
     * @param EnveloppeFichier $fichier       objet fichier
     *
     * @return mixed
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getLibelleStatutSignature($fichier, $infoSignature)
    {
        $statut = $this->getStatutSignatureFichier($fichier, $infoSignature);

        return match ($statut) {
            'OK' => $this->translator->trans('TEXT_VERIFICATION_OK'),
            'NOK' => $this->translator->trans('TEXT_VERIFICATION_NOK'),
            'UNKNOWN' => $this->translator->trans('TEXT_VERIFICATION_IMCOMPLETE'),
            'UNVERIFIED' => $this->translator->trans('VALIDITE_NON_VERIFIABLE'),
            default => $this->translator->trans('TEXT_INDEFINI'),
        };
    }
}
