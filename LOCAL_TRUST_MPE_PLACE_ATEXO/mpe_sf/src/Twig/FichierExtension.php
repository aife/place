<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class FichierExtension.
 */
class FichierExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('sizeFormat', [$this, 'sizeFormatFilter']),
        ];
    }

    public function sizeFormatFilter(string $tailleBrute, string $unity = 'Mo')
    {
        switch (strtolower($unity)) {
            case 'mo':
                $tailleBrute /= 1024;
                $tailleBrute = $this->aroundFileSize($tailleBrute);
                break;
        }

        return $tailleBrute;
    }

    protected function aroundFileSize($kiloOctets)
    {
        if ($kiloOctets < 1024) {
            $kiloOctets = str_replace('.', ',', round($kiloOctets, 2)).' '.'Ko';
        } else {
            $kiloOctets = str_replace('.', ',', round($kiloOctets / 1024, 2)).' '.'Mo';
        }

        return $kiloOctets;
    }

    public function getName()
    {
        return 'fichier-bundle-extension';
    }
}
