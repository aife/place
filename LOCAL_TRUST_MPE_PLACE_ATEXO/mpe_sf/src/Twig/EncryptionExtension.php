<?php

namespace App\Twig;

use App\Utils\Encryption;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class EncryptionExtension extends AbstractExtension
{
    /**
     * EncryptionExtension constructor.
     */
    public function __construct(private readonly Encryption $encryption)
    {
    }

    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('cryptage', [$this, 'getCryptage']),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getCryptage($id)
    {
        return $this->encryption->cryptId($id);
    }
}
