<?php

namespace App\Twig;

use App\Entity\Langue;
use App\Service\Configuration;
use Doctrine\ORM\EntityManagerInterface;
use Lcobucci\JWT\Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class LanguesExtension.
 */
class LanguesExtension extends AbstractExtension
{
    private ?Request $request = null;

    /**
     * LanguesExtension constructor.
     *
     * @param EntityManagerInterface $em Entity Manager
     * @param Configuration $configuration Configuration
     */
    public function __construct(
        EntityManagerInterface $em,
        private readonly Configuration $configuration,
        private readonly SessionInterface $session,
        private readonly ParameterBagInterface $parameterBag,
        private readonly LoggerInterface $logger,
        RequestStack $requestStack
    ) {
        $this->request = $requestStack->getCurrentRequest();
        $this->em = $em;
    }

    /**
     * Initialisation des filtres.
     *
     * @return array|TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('langues', [$this, 'listLangues']),
            new TwigFunction('showLanguage', [$this, 'isActiveLangue']),
            new TwigFunction('locale', [$this, 'getLocale']),
        ];
    }

    /**
     * Récupération de la liste de langues.
     *
     * @return array|object[]
     */
    public function listLangues()
    {
        return $this->em->getRepository(Langue::class)->findBy(['active' => 1]);
    }

    /**
     * Les pages agent et entreprise sont-elles multi langues.
     *
     * @return string
     */
    public function isActiveLangue()
    {
        try {
            if (strstr($this->request->getRequestUri(), 'entreprise')
                || strstr($this->request->query->get('page', ''), 'Entreprise.')) {
                return $this->configuration->getConfigurationPlateforme()->getMultiLinguismeEntreprise();
            } else {
                return $this->configuration->getConfigurationPlateforme()->getMultiLinguismeAgent();
            }
        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
    }

    public function getLocale()
    {
        $locale = $this->session->get('_locale');
        if ($locale === null) {
            $locale = $this->parameterBag->get('locale');
        }
        return $locale;
    }
}
