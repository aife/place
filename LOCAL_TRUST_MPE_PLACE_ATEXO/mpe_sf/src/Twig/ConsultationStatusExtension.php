<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Twig;

use App\Exception\InvalidArgumentException;
use App\Service\Consultation\ConsultationStatus;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ConsultationStatusExtension extends AbstractExtension
{
    public final const ICONS = [
        'ELABORATION' => 'ft-edit-1',
        'PREPARATION' => 'ft-check-circle',
        'CONSULTATION' => 'ft-globe',
        'OUVERTURE_ANALYSE' => 'ft-bar-chart-2',
        'DECISION' => 'ft-award',
    ];

    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('getConsultationStatusesByCodesAsArray', [$this, 'getConsultationStatusesByCodesAsArray']),
            new TwigFunction('getConsultationStatusIconByCode', [$this, 'getConsultationStatusIconByCode']),
            new TwigFunction('getConsultationStatusIdsByCodes', [$this, 'getConsultationStatusIdsByCodes']),
            new TwigFunction(
                'getTranslatedConsultationStatusesByCodes',
                [$this, 'getTranslatedConsultationStatusesByCodes']
            ),
            new TwigFunction(
                'getTranslatedConsultationStatusesAbbreviationsByCodes',
                [$this, 'getTranslatedConsultationStatusesAbbreviationsByCodes']
            ),
        ];
    }

    public function getConsultationStatusesByCodesAsArray(string $statusCodes): array
    {
        return preg_split("/[\s,\/]+/", $statusCodes);
    }

    public function getConsultationStatusIconByCode(string $statusCodes): string
    {
        $statusCode = $this->getConsultationStatusesByCodesAsArray($statusCodes)[0];

        if (!array_key_exists($statusCode, self::ICONS)) {
            throw new InvalidArgumentException(sprintf('Code de statut invalide : %s', $statusCode));
        }

        return self::ICONS[$statusCode];
    }

    public function getConsultationStatusIdsByCodes(string $statusCodes): array
    {
        $statusCodes = $this->getConsultationStatusesByCodesAsArray($statusCodes);
        $statusIds = [];

        foreach ($statusCodes as $statusCode) {
            if (!array_key_exists($statusCode, ConsultationStatus::STATUS)) {
                throw new InvalidArgumentException(sprintf('ID de statut invalide : %s', $statusCode));
            }
            $statusIds[] = ConsultationStatus::STATUS[$statusCode];
        }

        return $statusIds;
    }

    public function getTranslatedConsultationStatusesByCodes(string $statusCodes): string
    {
        $statusCodes = $this->getConsultationStatusesByCodesAsArray($statusCodes);

        return $this->getTranslatedArrayValues($statusCodes, 'STATUS_LABEL_');
    }

    public function getTranslatedConsultationStatusesAbbreviationsByCodes(string $statusCodes): string
    {
        $statusCodes = $this->getConsultationStatusesByCodesAsArray($statusCodes);

        return $this->getTranslatedArrayValues($statusCodes, 'STATUS_ABBR_');
    }

    protected function getTranslatedArrayValues(array $strings, string $prefix): string
    {
        $strings = preg_filter('/^/', $prefix, $strings);
        $strings = array_map([$this->translator, 'trans'], $strings);

        return implode(' / ', $strings);
    }
}
