<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Twig;

use App\Service\Footer\Footer;
use App\Service\Header\Header;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class BlocPageExtension extends AbstractExtension
{
    /**
     * @var string
     */
    public final const ENTREPRISE = 'entreprise';

    /**
     * BlocPageExtension constructor.
     * @param Header $headerService
     * @param Footer $footerService
     */
    public function __construct(private readonly Header $headerService, private readonly Footer $footerService)
    {
    }

    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('get_header', [$this, 'getHeader']),
            new TwigFunction('get_footer', [$this, 'getFooter'])
        ];
    }

    /**
     * @return string
     */
    public function getHeader(string $productName)
    {
        return $this->headerService->getTemplate($productName);
    }

    /**
     * @return string
     */
    public function getFooter(string $productName, string $callFrom = self::ENTREPRISE)
    {

        return $this->footerService->getTemplate($productName, $callFrom);
    }
}
