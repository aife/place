<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Twig;

use App\Entity\Inscrit;
use App\Service\CurrentUser;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class UtahExtension extends AbstractExtension
{
    /**
     * @var string
     */
    private $utiliserUtah;
    private $utiliserUtahModeDeconnecter;
    /**
     * @var CurrentUser
     */
    private $currentUser;


    private $parameterBag;

    /**
     * UtahExtension constructor.
     * @param CurrentUser $currentUser
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(
        CurrentUser $currentUser,
        ParameterBagInterface $parameterBag
    ) {
        $this->utiliserUtah = (bool) $parameterBag->get('UTILISER_UTAH') || (bool) $parameterBag->get('UTILISER_FAQ');
        $this->utiliserUtahModeDeconnecter =  (bool) $parameterBag->get('UTILISER_UTAH_MODE_DECONNECTER');
        $this->currentUser = $currentUser;
        $this->parameterBag = $parameterBag;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('isAssistanceButtonDisplayed', [$this, 'isAssistanceButtonDisplayed']),
            new TwigFunction('getNumeroAssistanceTelephonique', [$this, 'getNumeroAssistanceTelephonique']),
            new TwigFunction('getUrlAssistanceTelephonique', [$this, 'getUrlAssistanceTelephonique']),
        ];
    }

    /**
     * @param Inscrit $currentUser
     *
     * @return bool
     */
    public function isAssistanceButtonDisplayed($currentUser)
    {
        $show = false;
        if ($this->utiliserUtah && $this->utiliserUtahModeDeconnecter) {
            if (false === $this->currentUser->hasRole('ROLE_admin')) {
                $show = true;
            }
        }
        return $show;
    }

    public function getNumeroAssistanceTelephonique($calledFrom = 'entreprise')
    {
        if ('agent' == $calledFrom) {
            return $this->parameterBag->get('NUMERO_ASSISTANCE_TELEPHONIQUE_AGENT');
        } elseif ('entreprise' == $calledFrom) {
            return $this->parameterBag->get('NUMERO_ASSISTANCE_TELEPHONIQUE_ENTREPRISE');
        }

        return $this->parameterBag->get('NUMERO_ASSISTANCE_TELEPHONIQUE_DEFAUT');
    }


    public function getUrlAssistanceTelephonique($calledFrom = 'entreprise')
    {
        if ('agent' == $calledFrom && !empty($this->currentUser)) {
            return $this->parameterBag->get('PF_URL') . '?page=Agent.AgentAide';
        } elseif ('entreprise' == $calledFrom) {
            return  $this->parameterBag->get('PF_URL') . 'entreprise/aide/assistance-telephonique';
        }

        return '';
    }
}
