<?php

namespace App\Twig;

use Application\Service\Atexo\Atexo_Config;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class InscritHistoriqueExtension extends \Twig\Extension\AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('getActionTrans', [$this, 'correspondanceActionInscritText']),
            new TwigFilter('correspondanceIdProfilLibelle', [$this, 'correspondanceIdProfilLibelle']),
        ];
    }

    public function correspondanceActionInscritText(int $number): string
    {
        return match ($number) {
            5 => 'ENTREPRISE_INSCRIPTION_COMME_ADMIN',
            6 => 'ENTREPRISE_INSCRIPTION_COMME_INSCRIT_SIMPLE',
            1 => 'TEXT_MODIFICATION_COMPTE',
            4 => 'TEXT_SUPPRESSION_COMPTE',
            3 => 'TEXT_DEBLOCAGE_COMPTE',
            2 => 'TEXT_VERROUILLAGE_COMPTE',
            7 => 'TEXT_RETRAIT_ATES',
            default => '-',
        };
    }

    public function correspondanceIdProfilLibelle(string $idProfil): string
    {
        //Pour le cas des inscrits supprimes
        // (car on on ne peut plus faire de correspondance entre l'inscrit et l'historique)
        if ('0' != $idProfil) {
            $libelleProfil = '';
            return match ($idProfil) {
                Atexo_Config::getParameter('PROFIL_INSCRIT_ATES') => 'TEXT_ADMINISTRATEUR',
                Atexo_Config::getParameter('PROFIL_INSCRIT_USER') => 'TEXT_UES',
                default => $libelleProfil,
            };
        }

        return '<center/>-<center/>';
    }
}
