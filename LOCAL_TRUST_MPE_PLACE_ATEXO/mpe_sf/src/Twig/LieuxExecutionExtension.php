<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Twig;

use App\Entity\GeolocalisationN2;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class LieuxExecutionExtension extends AbstractExtension
{
    public function __construct(private EntityManagerInterface $em)
    {
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('intituleLieuxExecution', [$this, 'listExecutionIntitule']),
        ];
    }

    public function listExecutionIntitule(string $listIdLieux): string
    {
        $ids = explode(',', $listIdLieux);
        $lieux = $this->em->getRepository(GeolocalisationN2::class)->getDenominationLieuxExecution($ids);
        $result = '';
        if ($lieux) {
            $result = implode(', ', $lieux);
        }

        return $result;
    }
}