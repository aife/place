<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Twig;

use App\Service\Configuration;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AccueilExtension extends AbstractExtension
{
    public function __construct(private readonly TranslatorInterface $translator, private readonly Configuration $configuration, private readonly ParameterBagInterface $parameterBag)
    {
    }

    public function getFunctions(): array
    {
        return array(
            new TwigFunction('isLibelleValide', [$this, 'isLibelleValide']),
            new TwigFunction('isEnabled', [$this, 'isEnabled']),
            new TwigFunction('getInfoLien', [$this, 'getInfoLien']),
        );
    }

    public function isLibelleValide($libelle)
    {
        return $this->translator->trans($libelle) != $libelle;
    }

    public function isEnabled($moduleName, $organisme = ''): bool
    {
        $conf = $this->configuration->getConfigurationPlateforme();
        $checkModule = 'get' . $moduleName;

        if (method_exists($conf, $checkModule) == false) {
            return false;
        }
        return $conf->$checkModule();
    }

    public function getInfoLien($linkParameter, $linkText, $nouvelOnglet = false, $title = false)
    {
        $link = $this->parameterBag->get($linkParameter);

        if ($link && $link != $linkParameter) {
            $target = "";
            if ($nouvelOnglet && $this->parameterBag->get($nouvelOnglet) == "1") {
                $target = ' target="_blank" ';
            }
            $paramTitle = "";
            if ($title) {
                $paramTitle = ' title="' . $this->translator->trans($title) . '" ';
            }

            $url = $this->parameterBag->get('PF_URL') . ltrim($link);

            return sprintf(
                '<a href="%s" %s %s>%s</a>',
                $url,
                $paramTitle,
                $target,
                $this->translator->trans($linkText)
            );
        } else {
            return $this->translator->trans($linkText);
        }
    }
}
