<?php

namespace App\Twig;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppTranslationExtension extends AbstractExtension
{
    public function __construct(
        private TranslatorInterface $translator,
        private SessionInterface $session,
        private ParameterBagInterface $parameterBag
    ) {
    }

    public function getFilters()
    {
        return [
            new TwigFilter('trans', [$this, 'trans']),
        ];
    }

    public function trans($message, array $arguments = [], $domain = null, $locale = null)
    {
        if ($locale === null) {
            $locale = $this->session->get('_locale') ?? $this->parameterBag->get('locale');
        }

        return $this->translator->trans($message, $arguments, $domain, $locale);
    }
}
