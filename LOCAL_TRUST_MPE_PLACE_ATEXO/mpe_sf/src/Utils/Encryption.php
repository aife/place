<?php

namespace App\Utils;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class Encryption
{
    /**
     * Utils constructor.
     */
    public function __construct(private readonly ParameterBagInterface $parameterBag)
    {
    }

    public function cryptId($id)
    {
        $output = false;

        $encrypt_method = 'AES-256-CBC';

        // hash
        $key = hash('sha256', $this->parameterBag->get('MPE_SECRET_PRIVATE'));

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a
        // warning
        $iv = substr(hash('sha256', $this->parameterBag->get('MPE_SECRET_PUBLIC')), 0, 16);

        $output = openssl_encrypt($id, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);

        return $output;
    }

    /**
     * @param $hash
     */
    public function decryptId($hash): bool|string
    {
        $output = false;

        $encrypt_method = 'AES-256-CBC';

        // hash
        $key = hash('sha256', $this->parameterBag->get('MPE_SECRET_PRIVATE'));

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a
        // warning
        $iv = substr(hash('sha256', $this->parameterBag->get('MPE_SECRET_PUBLIC')), 0, 16);
        $output = openssl_decrypt(base64_decode($hash), $encrypt_method, $key, 0, $iv);

        return $output;
    }
}
