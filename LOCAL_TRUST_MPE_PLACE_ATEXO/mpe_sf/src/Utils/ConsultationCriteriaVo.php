<?php

namespace App\Utils;

/**
 * Class ConsultationCriteriaVo.
 */
class ConsultationCriteriaVo
{
    private string $referenceConsultation = '';
    private string $referenceConsultationRestreinte = '';
    private $idReference;
    private ?string $idTypeAvis = '';
    private string $notIdTypeAvis = '';
    private string $idTypeProcedure = '';
    private string $idEtatConsultation = '';
    private string $acronymeOrganisme = '';
    private string $acronymeOrganismeProcRest = '';
    private ?string $idService = '';
    private string $idServiceRma = '';
    private string $typeAcces = '';
    private string $categorieConsultation = '';
    private string $codeAccesProcedureRestreinte = '';
    private string $idCodeCpv1 = '';
    private string $idCodeCpv2 = '';
    private string $idCodeCpv3 = '';
    private string $idCodeCpv4 = '';
    private string $dateFinStart = '';
    private string $dateFinEnd = '';
    private string $intituleConsultation = '';
    private string $objetConsultation = '';
    private string $commentaireInterne = '';
    private string $intituleLot = '';
    private string $description = '';
    private string $typeSearch = '';
    private string $keyWordRechercheRapide = '';
    private string $keyWordAdvancedSearch = '';
    private string $etatConsultation = '';
    private string $defaultSortByElement = 'datefin';
    private string $sortByElement = '';
    private string $sensOrderBy = 'DESC';
    private int $offset = 0;
    private int $limit = 0;
    private string $secondConditionOnEtatConsultation = '';
    private bool $searchModeExact = false;
    private string $_lieuxExecution = '';
    private $_connectedAgentId;
    private bool $_calledFromPortail = false;
    private string $_periodicite = '';
    private string $_orgActive = '1';
    private string $_publicationEurope = '';
    private bool $_etatPublicationToBePublished = false;
    private bool $_etatPublicationPublished = false;
    private bool $_etatPublicationToBeUpdated = false;
    private bool $_etatPublicationUpdated = false;
    private bool $_alertesFromYesterday = true;
    private bool $_calledFromElios = false;
    private bool $_sansCritereActive = false;
    private bool $_consultationAValiderSeulement = false;
    private bool $_consultationAApprouverSeulement = false;
    //A true pour banir les consultations visible temporairement pour validation ou attribution
    private bool $_noTemporaryCOnsultation = false;
    private $_enLigneDepuis;
    private $_enLigneJusquau;
    private $_orgDenomination;
    private string $_typeOrganisme = '';
    private string $_domaineActivite = '';
    private string $_qualification = '';
    private string $_agrements = '';
    private bool $_fromArchive = false;
    private $_codesNuts;
    private string $_idsService = '';
    //tableau d'objet referentielVo
    private array $referentielVo = [];
    private string $_idInscrit = '';
    private string $_idEntreprise = '';
    private bool $_forPanierEntreprise = false;
    private bool $_avecRetrait = false;
    private bool $_avecQuestion = false;
    private bool $_avecDepot = false;
    private bool $_avecEchange = false;
    private bool $_sansRetrait = false;
    private bool $_sansQuestion = false;
    private bool $_sansDepot = false;
    private bool $_sansEchange = false;
    private int $idAlerteConsultation = 0;
    private int $alerteConsultationCloturee = 0;
    //mettre a true pour recuperer les consultation publiees depuis hier
    private bool $publieHier = false;
    //mettre $withDce a true  pour chercher que les consultation ayant un DCE
    private bool $withDce = false;
    //mettre  $informationMarche a true pour retourner que les Avis de marches et les avis de pre-informations
    private bool $informationMarche = false;
    private bool $dateTimeFin = false;
    private bool $_avecConsClotureesSansPoursuivreAffichage = false;
    private bool $exclureConsultationExterne = false;
    private int $afficherDoublon = 0;
    private $denominationAdapte;
    private int $clauseSociale = 0;
    private int $atelierProtege = 0;
    private int $ess = 0;
    private int $siae = 0;
    private int $clauseEnv = 0;
    private int $Mps = 0;
    private bool $consultaionPublier = false;
    //mettre a 1 pour recuperer les consultations annulees
    private bool $consultationAnnulee = false;
    // Accesseurs
    private $idOperation;
    private bool|string $inclureDescendance = true;
    private ?string $rechercheAvanceeAutoCompletion = null;
    private bool|string $consultationAArchiver = false;
    private string $bourseCotraitance = '0';
    private bool $visionRma = false;
    private bool $organismeTest = false;
    private $socialeCommerceEquitable;
    private $socialeInsertionActiviterEconomique;

    /**
     * @return bool
     */
    public function getOrganismeTest()
    {
        return $this->organismeTest;
    }

    /**
     * @param bool $organismeTest
     */
    public function setOrganismeTest($organismeTest)
    {
        $this->organismeTest = $organismeTest;

        return $this;
    }

    public function getSocialeCommerceEquitable()
    {
        return $this->socialeCommerceEquitable;
    }

    public function setSocialeCommerceEquitable($value)
    {
        $this->socialeCommerceEquitable = $value;

        return $this;
    }

    public function getSocialeInsertionActiviterEconomique()
    {
        return $this->socialeInsertionActiviterEconomique;
    }

    public function setSocialeInsertionActiviterEconomique($value)
    {
        $this->socialeInsertionActiviterEconomique = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getBourseCotraitance()
    {
        return $this->bourseCotraitance;
    }

    /**
     * @param string $bourseCotraitance
     */
    public function setBourseCotraitance($bourseCotraitance)
    {
        $this->bourseCotraitance = $bourseCotraitance;

        return $this;
    }

    public function getConsultationAnnulee()
    {
        return $this->consultationAnnulee;
    }

    public function setConsultationAnnulee($value)
    {
        $this->consultationAnnulee = $value;

        return $this;
    }

    public function getConsultaionPublier()
    {
        return $this->consultaionPublier;
    }

    public function setConsultaionPublier($value)
    {
        $this->consultaionPublier = $value;

        return $this;
    }

    public function getWithDce()
    {
        return $this->withDce;
    }

    public function setWithDce($value)
    {
        $this->withDce = $value;

        return $this;
    }

    public function getPublieHier()
    {
        return $this->publieHier;
    }

    public function setPublieHier($publieHier)
    {
        $this->publieHier = $publieHier;

        return $this;
    }

    public function getInformationMarche()
    {
        return $this->informationMarche;
    }

    public function setInformationMarche($informationMarche)
    {
        $this->informationMarche = $informationMarche;

        return $this;
    }

    public function getIdReference()
    {
        return $this->idReference;
    }

    public function setIdReference($idReference)
    {
        $this->idReference = $idReference;

        return $this;
    }

    public function getNotIdTypeAvis()
    {
        return $this->notIdTypeAvis;
    }

    public function setNotIdTypeAvis($notIdTypeAvis)
    {
        $this->notIdTypeAvis = $notIdTypeAvis;

        return $this;
    }

    public function getIdEtatConsultation()
    {
        return $this->idEtatConsultation;
    }

    public function setIdEtatConsultation($idEtatConsultation)
    {
        $this->idEtatConsultation = $idEtatConsultation;

        return $this;
    }

    public function getAcronymeOrganismeProcRest()
    {
        return $this->acronymeOrganismeProcRest;
    }

    public function setAcronymeOrganismeProcRest($acronymeOrganismeProRes)
    {
        $this->acronymeOrganismeProcRest = $acronymeOrganismeProRes;

        return $this;
    }

    public function getIdService()
    {
        return $this->idService;
    }

    public function setIdService($idService)
    {
        $this->idService = $idService;

        return $this;
    }

    public function getIdServiceRma()
    {
        return $this->idServiceRma;
    }

    public function setIdServiceRma($idService)
    {
        $this->idServiceRma = $idService;

        return $this;
    }

    public function getIdCodeCpv2()
    {
        return $this->idCodeCpv2;
    }

    public function setIdCodeCpv2($idCodeCpv2)
    {
        $this->idCodeCpv2 = $idCodeCpv2;

        return $this;
    }

    public function getIdCodeCpv3()
    {
        return $this->idCodeCpv3;
    }

    public function setIdCodeCpv3($idCodeCpv3)
    {
        $this->idCodeCpv3 = $idCodeCpv3;

        return $this;
    }

    public function getIdCodeCpv4()
    {
        return $this->idCodeCpv4;
    }

    public function setIdCodeCpv4($idCodeCpv4)
    {
        $this->idCodeCpv4 = $idCodeCpv4;

        return $this;
    }

    public function getDateFinStart()
    {
        return $this->dateFinStart;
    }

    public function setDateFinStart($dateFinStart)
    {
        $this->dateFinStart = $dateFinStart;

        return $this;
    }

    public function getDateFinEnd()
    {
        return $this->dateFinEnd;
    }

    public function setDateFinEnd($dateFinEnd)
    {
        $this->dateFinEnd = $dateFinEnd;

        return $this;
    }

    public function getIntituleConsultation()
    {
        return $this->intituleConsultation;
    }

    public function setIntituleConsultation($intituleConsultation)
    {
        $this->intituleConsultation = $intituleConsultation;

        return $this;
    }

    public function getObjetConsultation()
    {
        return $this->objetConsultation;
    }

    public function setObjetConsultation($objetConsultation)
    {
        $this->objetConsultation = $objetConsultation;

        return $this;
    }

    public function getCommentaireInterne()
    {
        return $this->commentaireInterne;
    }

    public function setCommentaireInterne($commentaireInterne)
    {
        $this->commentaireInterne = $commentaireInterne;

        return $this;
    }

    public function getIntituleLot()
    {
        return $this->intituleLot;
    }

    public function setIntituleLot($intituleLot)
    {
        $this->intituleLot = $intituleLot;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getTypeSearch()
    {
        return $this->typeSearch;
    }

    public function setTypeSearch($typeSearch)
    {
        $this->typeSearch = $typeSearch;

        return $this;
    }

    public function getKeyWordRechercheRapide()
    {
        return $this->keyWordRechercheRapide;
    }

    public function setKeyWordRechercheRapide($keyWordRechercheRapide)
    {
        $this->keyWordRechercheRapide = $keyWordRechercheRapide;

        return $this;
    }

    public function getKeyWordAdvancedSearch()
    {
        return $this->keyWordAdvancedSearch;
    }

    public function setKeyWordAdvancedSearch($keyWordAdvancedSearch)
    {
        $this->keyWordAdvancedSearch = $keyWordAdvancedSearch;

        return $this;
    }

    public function getEtatConsultation()
    {
        return $this->etatConsultation;
    }

    public function setEtatConsultation($etatConsultation)
    {
        $this->etatConsultation = $etatConsultation;

        return $this;
    }

    public function getDefaultSortByElement()
    {
        return $this->defaultSortByElement;
    }

    public function setDefaultSortByElement($defaultSortByElement)
    {
        $this->defaultSortByElement = $defaultSortByElement;

        return $this;
    }

    public function getSortByElement()
    {
        return $this->sortByElement;
    }

    public function setSortByElement($sortByElement)
    {
        $this->sortByElement = $sortByElement;

        return $this;
    }

    public function getSensOrderBy()
    {
        return $this->sensOrderBy;
    }

    public function setSensOrderBy($sensOrderBy)
    {
        $this->sensOrderBy = $sensOrderBy;

        return $this;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;

        return $this;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    public function getSearchModeExact()
    {
        return $this->searchModeExact;
    }

    public function setSearchModeExact($boolean)
    {
        $this->searchModeExact = $boolean;

        return $this;
    }

    public function getSecondConditionOnEtatConsultation()
    {
        return $this->secondConditionOnEtatConsultation;
    }

    public function setSecondConditionOnEtatConsultation($secondConditionOnEtatConsultation)
    {
        $this->secondConditionOnEtatConsultation = $secondConditionOnEtatConsultation;

        return $this;
    }

    public function getConnectedAgentId()
    {
        return $this->_connectedAgentId;
    }

    public function setConnectedAgentId($connectedAgentId)
    {
        $this->_connectedAgentId = $connectedAgentId;

        return $this;
    }

    /**
     * @return int
     */
    public function getEss()
    {
        return $this->ess;
    }

    /**
     * @param int $ess
     */
    public function setEss($ess)
    {
        $this->ess = $ess;

        return $this;
    }

    /**
     * @return int
     */
    public function getSiae()
    {
        return $this->siae;
    }

    /**
     * @param int $siae
     */
    public function setSiae($siae)
    {
        $this->siae = $siae;

        return $this;
    }

    public function toString()
    {
        $res = '';
        if ('' !== $this->getAcronymeOrganisme()) {
            $res .= ' / Org='.$this->getAcronymeOrganisme();
        }
        if ('' !== $this->getCategorieConsultation()) {
            $res .= ' / idCategorie='.$this->getCategorieConsultation();
        }
        if ('' !== $this->getReferenceConsultation()) {
            $res .= ' / Reference utilisateur='.$this->getReferenceConsultation();
        }
        if ('' != $this->getcalledFromPortail()) {
            $res .= ' / call from ='.$this->getcalledFromPortail();
        }
        if ('' != $this->getIdCodeCpv1()) {
            $res .= ' /cpv ='.$this->getIdCodeCpv1();
        }
        if ('' != $this->getLieuxexecution()) {
            $res .= ' /Lieux execution ='.$this->getLieuxexecution();
        }
        if ('' != $this->getIdTypeProcedure()) {
            $res .= ' /Id Type Procedure ='.$this->getIdTypeProcedure();
        }
        if ('' != $this->getIdTypeAvis()) {
            $res .= ' /Id Type Avis ='.$this->getIdTypeAvis();
        }
        if ('' != $this->getTypeAcces()) {
            $res .= ' /Type Procedure ='.$this->getTypeAcces();
        }
        if ('' != $this->getCodeAccesProcedureRestreinte()) {
            $res .= ' /Code Acces Procedure Restreinte ='.$this->getCodeAccesProcedureRestreinte();
        }

        return $res;
    }

    public function getAcronymeOrganisme()
    {
        return $this->acronymeOrganisme;
    }

    public function setAcronymeOrganisme($acronymeOrganisme)
    {
        $this->acronymeOrganisme = $acronymeOrganisme;

        return $this;
    }

    public function getCategorieConsultation()
    {
        return $this->categorieConsultation;
    }

    public function setCategorieConsultation($categorieConsultation)
    {
        $this->categorieConsultation = $categorieConsultation;

        return $this;
    }

    public function getReferenceConsultation()
    {
        return $this->referenceConsultation;
    }

    public function setReferenceConsultation($referenceConsultation)
    {
        $this->referenceConsultation = $referenceConsultation;

        return $this;
    }

    public function getcalledFromPortail()
    {
        return $this->_calledFromPortail;
    }

    public function setcalledFromPortail($calledFromPortail)
    {
        $this->_calledFromPortail = $calledFromPortail;

        return $this;
    }

    public function getIdCodeCpv1()
    {
        return $this->idCodeCpv1;
    }

    public function setIdCodeCpv1($idCodeCpv1)
    {
        $this->idCodeCpv1 = $idCodeCpv1;

        return $this;
    }

    public function getLieuxexecution()
    {
        return $this->_lieuxExecution;
    }

    public function setLieuxexecution($lieuxExecution)
    {
        $this->_lieuxExecution = $lieuxExecution;

        return $this;
    }

    public function getIdTypeProcedure()
    {
        return $this->idTypeProcedure;
    }

    public function setIdTypeProcedure($idTypeProcedure)
    {
        $this->idTypeProcedure = $idTypeProcedure;

        return $this;
    }

    public function getIdTypeAvis()
    {
        return $this->idTypeAvis;
    }

    public function setIdTypeAvis($idTypeAvis)
    {
        $this->idTypeAvis = $idTypeAvis;

        return $this;
    }

    public function getTypeAcces()
    {
        return $this->typeAcces;
    }

    public function setTypeAcces($typeAcces)
    {
        $this->typeAcces = $typeAcces;

        return $this;
    }

    public function getCodeAccesProcedureRestreinte()
    {
        return $this->codeAccesProcedureRestreinte;
    }

    public function setCodeAccesProcedureRestreinte($codeAccesProcedureRestreinte)
    {
        $this->codeAccesProcedureRestreinte = $codeAccesProcedureRestreinte;

        return $this;
    }

    public function getPeriodicite()
    {
        return $this->_periodicite;
    }

    public function setPeriodicite($periodicite)
    {
        $this->_periodicite = $periodicite;

        return $this;
    }

    public function getOrgActive()
    {
        return $this->_orgActive;
    }

    public function setOrgActive($value)
    {
        $this->_orgActive = $value;

        return $this;
    }

    public function getPublicationEurope()
    {
        return $this->_publicationEurope;
    }

    public function setPublicationEurope($value)
    {
        $this->_publicationEurope = $value;

        return $this;
    }

    public function getEtatPublicationToBePublished()
    {
        return $this->_etatPublicationToBePublished;
    }

    public function setEtatPublicationToBePublished($bool)
    {
        $this->_etatPublicationToBePublished = $bool;

        return $this;
    }

    public function getEtatPublicationPublished()
    {
        return $this->_etatPublicationPublished;
    }

    public function setEtatPublicationPublished($bool)
    {
        $this->_etatPublicationPublished = $bool;

        return $this;
    }

    public function getEtatPublicationToBeUpdated()
    {
        return $this->_etatPublicationToBeUpdated;
    }

    public function setEtatPublicationToBeUpdated($bool)
    {
        $this->_etatPublicationToBeUpdated = $bool;

        return $this;
    }

    public function getEtatPublicationUpdated()
    {
        return $this->_etatPublicationUpdated;
    }

    public function setEtatPublicationUpdated($bool)
    {
        $this->_etatPublicationUpdated = $bool;

        return $this;
    }

    public function getAlertesFromYesterday()
    {
        return $this->_alertesFromYesterday;
    }

    public function setAlertesFromYesterday($bool)
    {
        $this->_alertesFromYesterday = $bool;

        return $this;
    }

    public function getCalledFromHelios()
    {
        return $this->_calledFromElios;
    }

    public function setCalledFromHelios($bool)
    {
        $this->_calledFromElios = $bool;

        return $this;
    }

    /**
     * @deprecated use getCalledFromHelios
     */
    public function getCalledFromElios()
    {
        return $this->_calledFromElios;
    }

    /**
     * @deprecated use getCalledFromHelios
     */
    public function setCalledFromElios($bool)
    {
        $this->_calledFromElios = $bool;

        return $this;
    }

    public function getSansCritereActive()
    {
        return $this->_sansCritereActive;
    }

    public function setSansCritereActive($bool)
    {
        $this->_sansCritereActive = $bool;

        return $this;
    }

    public function getConsultationAValiderSeulement()
    {
        return $this->_consultationAValiderSeulement;
    }

    public function setConsultationAValiderSeulement($value)
    {
        $this->_consultationAValiderSeulement = $value;

        return $this;
    }

    public function getConsultationAApprouverSeulement()
    {
        return $this->_consultationAApprouverSeulement;
    }

    public function setConsultationAApprouverSeulement($value)
    {
        $this->_consultationAApprouverSeulement = $value;

        return $this;
    }

    /**
     * A true pour banir les consultations visible temporairement pour validation simple, finale ou interm.
     */
    public function getNoTemporaryCOnsultation()
    {
        return $this->_noTemporaryCOnsultation;
    }

    public function setNoTemporaryCOnsultation($value)
    {
        $this->_noTemporaryCOnsultation = $value;

        return $this;
    }

    public function getEnLigneDepuis()
    {
        return $this->_enLigneDepuis;
    }

    public function setEnLigneDepuis($value)
    {
        $this->_enLigneDepuis = $value;

        return $this;
    }

    public function getEnLigneJusquau()
    {
        return $this->_enLigneJusquau;
    }

    public function setEnLigneJusquau($value)
    {
        $this->_enLigneJusquau = $value;

        return $this;
    }

    public function getOrgDenomination()
    {
        return $this->_orgDenomination;
    }

    public function setOrgDenomination($value)
    {
        $this->_orgDenomination = $value;

        return $this;
    }

    public function getTypeOrganisme()
    {
        return $this->_typeOrganisme;
    }

    public function setTypeOrganisme($value)
    {
        $this->_typeOrganisme = $value;

        return $this;
    }

    public function getDomaineActivite()
    {
        return $this->_domaineActivite;
    }

    public function setDomaineActivite($value)
    {
        $this->_domaineActivite = $value;

        return $this;
    }

    public function getQualification()
    {
        return $this->_qualification;
    }

    public function setQualification($value)
    {
        $this->_qualification = $value;

        return $this;
    }

    public function getAgrements()
    {
        return $this->_agrements;
    }

    public function setAgrements($value)
    {
        $this->_agrements = $value;

        return $this;
    }

    public function getFromArchive()
    {
        return $this->_fromArchive;
    }

    public function setFromArchive($value)
    {
        $this->_fromArchive = $value;

        return $this;
    }

    public function getCodesNuts()
    {
        return $this->_codesNuts;
    }

    public function setCodesNuts($value)
    {
        $this->_codesNuts = $value;

        return $this;
    }

    public function getIdsService()
    {
        return $this->_idsService;
    }

    public function setIdsService($value)
    {
        $this->_idsService = $value;

        return $this;
    }

    public function getReferentielVo()
    {
        return $this->referentielVo;
    }

    public function setReferentielVo($value)
    {
        $this->referentielVo = $value;

        return $this;
    }

    public function getIdInscrit()
    {
        return $this->_idInscrit;
    }

    public function setIdInscrit($value)
    {
        $this->_idInscrit = $value;

        return $this;
    }

    public function getIdEntreprise()
    {
        return $this->_idEntreprise;
    }

    public function setIdEntreprise($value)
    {
        $this->_idEntreprise = $value;

        return $this;
    }

    public function getForPanierEntreprise()
    {
        return $this->_forPanierEntreprise;
    }

    public function setForPanierEntreprise($value)
    {
        $this->_forPanierEntreprise = $value;

        return $this;
    }

    public function getAvecRetrait()
    {
        return $this->_avecRetrait;
    }

    public function setAvecRetrait($value)
    {
        $this->_avecRetrait = $value;

        return $this;
    }

    public function getAvecQuestion()
    {
        return $this->_avecQuestion;
    }

    public function setAvecQuestion($value)
    {
        $this->_avecQuestion = $value;

        return $this;
    }

    public function getAvecDepot()
    {
        return $this->_avecDepot;
    }

    public function setAvecDepot($value)
    {
        $this->_avecDepot = $value;

        return $this;
    }

    public function getAvecEchange()
    {
        return $this->_avecEchange;
    }

    public function setAvecEchange($value)
    {
        $this->_avecEchange = $value;

        return $this;
    }

    public function getDateTimeFin()
    {
        return $this->dateTimeFin;
    }

    public function setDateTimeFin($dateTimeFin)
    {
        $this->dateTimeFin = $dateTimeFin;

        return $this;
    }

    public function getAvecConsClotureesSansPoursuivreAffichage()
    {
        return $this->_avecConsClotureesSansPoursuivreAffichage;
    }

    public function setAvecConsClotureesSansPoursuivreAffichage($value)
    {
        $this->_avecConsClotureesSansPoursuivreAffichage = $value;

        return $this;
    }

    public function getExclureConsultationExterne()
    {
        return $this->exclureConsultationExterne;
    }

    public function setExclureConsultationExterne($value)
    {
        $this->exclureConsultationExterne = $value;

        return $this;
    }

    public function getIdAlerteConsultation()
    {
        return $this->idAlerteConsultation;
    }

    public function setIdAlerteConsultation($value)
    {
        $this->idAlerteConsultation = $value;

        return $this;
    }

    public function getAlerteConsultationCloturee()
    {
        return $this->alerteConsultationCloturee;
    }

    public function setAlerteConsultationCloturee($value)
    {
        $this->alerteConsultationCloturee = $value;

        return $this;
    }

    public function getAfficherDoublon()
    {
        return $this->afficherDoublon;
    }

    public function setAfficherDoublon($value)
    {
        $this->afficherDoublon = $value;

        return $this;
    }

    public function getSansRetrait()
    {
        return $this->_sansRetrait;
    }

    public function setSansRetrait($value)
    {
        $this->_sansRetrait = $value;

        return $this;
    }

    public function getSansQuestion()
    {
        return $this->_sansQuestion;
    }

    public function setSansQuestion($value)
    {
        $this->_sansQuestion = $value;

        return $this;
    }

    public function getSansDepot()
    {
        return $this->_sansDepot;
    }

    public function setSansDepot($value)
    {
        $this->_sansDepot = $value;

        return $this;
    }

    public function getSansEchange()
    {
        return $this->_sansEchange;
    }

    public function setSansEchange($value)
    {
        $this->_sansEchange = $value;

        return $this;
    }

    public function getDenominationAdapte()
    {
        return $this->denominationAdapte;
    }

    public function setDenominationAdapte($value)
    {
        $this->denominationAdapte = $value;

        return $this;
    }

    public function getClauseSociale()
    {
        return $this->clauseSociale;
    }

    public function setClauseSociale($clauseSociale)
    {
        $this->clauseSociale = $clauseSociale;

        return $this;
    }

    public function getAtelierProtege()
    {
        return $this->atelierProtege;
    }

    public function setAtelierProtege($atelierProtege)
    {
        $this->atelierProtege = $atelierProtege;

        return $this;
    }

    public function getClauseEnv()
    {
        return $this->clauseEnv;
    }

    public function setClauseEnv($clauseEnv)
    {
        $this->clauseEnv = $clauseEnv;

        return $this;
    }

    public function getIdOperation()
    {
        return $this->idOperation;
    }

    public function setIdOperation($value)
    {
        $this->idOperation = $value;

        return $this;
    }

    public function getMps()
    {
        return $this->Mps;
    }

    public function setMps($mps)
    {
        $this->Mps = $mps;

        return $this;
    }

    /**
     * Permet de recuperer la valeur de l'attribut $inclureDescendance.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function getInclureDescendance()
    {
        return $this->inclureDescendance;
    }

    /**
     * Permet d'affecter la valeur de l'attribut $inclureDescendance.
     *
     * @param string $value : valeur a affecter a l'attribut
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function setInclureDescendance($value)
    {
        $this->inclureDescendance = $value;

        return $this;
    }

    /**
     * Permet de recuperer la valeur de l'attribut $rechercheAvanceeAutoCompletion.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function getRechercheAvanceeAutoCompletion()
    {
        return $this->rechercheAvanceeAutoCompletion;
    }

    /**
     * Permet d'affecter la valeur de l'attribut $rechercheAvanceeAutoCompletion.
     *
     * @param string $value : valeur a affecter a l'attribut
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function setRechercheAvanceeAutoCompletion($value)
    {
        $this->rechercheAvanceeAutoCompletion = $value;

        return $this;
    }

    /**
     * Permet de recuperer la valeur de l'attribut $consultationAArchiver.
     *
     * @author oubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getConsultationAArchiver()
    {
        return $this->consultationAArchiver;
    }

    /**
     * Permet d'affecter la valeur de l'attribut $consultationAArchiver.
     *
     * @param string $value : valeur a affecter a l'attribut
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setConsultationAArchiver($value)
    {
        $this->consultationAArchiver = $value;

        return $this;
    }

    public function getReferenceConsultationRestreinte()
    {
        return $this->referenceConsultationRestreinte;
    }

    public function setReferenceConsultationRestreinte($referenceConsultationRestreinte)
    {
        $this->referenceConsultationRestreinte = $referenceConsultationRestreinte;

        return $this;
    }

    /**
     * @return bool
     */
    public function getVisionRma()
    {
        return $this->visionRma;
    }

    /**
     * @param bool $visionRma
     */
    public function setVisionRma($visionRma)
    {
        $this->visionRma = $visionRma;

        return $this;
    }
}
