<?php

namespace App\Utils\Filesystem;

use Exception;
use League\Flysystem\FileNotFoundException;
use App\Entity\BlobFichier;
use App\Entity\BloborganismeFile;
use App\Utils\Filesystem\Adapter\Filesystem;
use App\Utils\Filesystem\Adapter\Local;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class MountManager extends \League\Flysystem\MountManager
{
    private readonly array $fileSystems;

    public final const BLOB_FILE_NAME_SUFFIX = '-0';

    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly EntityManagerInterface $entityManager,
        $fichierArchiveDir = '',
        $commonTmp = '',
        $documentRootDirMultiple = '',
        $memoryAdapter = null
    ) {
        $this->fileSystems = $this->getFileSystems(
            $fichierArchiveDir,
            $documentRootDirMultiple,
            $commonTmp,
            $memoryAdapter
        );
        parent::__construct($this->fileSystems);
    }

    /**
     * @param $fichierArchiveDir
     * @param $documentRootDirMultiple
     * @param $commonTmp
     * @param $memoryAdapter
     *
     * @return array
     */
    public function getFileSystems($fichierArchiveDir, $documentRootDirMultiple, $commonTmp, $memoryAdapter)
    {
        $fs = [];

        $mount = $fichierArchiveDir;
        if (!empty($documentRootDirMultiple)) {
            $mount = $documentRootDirMultiple;
        }
        $exploded = explode(';', (string) $mount);

        $active = true;
        foreach ($exploded as $path) {
            $path = rtrim($path, '/');
            try {
                if (empty($memoryAdapter)) {
                    $adapter = new Local($path);
                } else {
                    $adapter = $memoryAdapter;
                }
                if (true === $active) {
                    $path = 'nas';
                }
                $fs[$path] = new Filesystem($adapter);
                $active = false;
            } catch (Exception) {
                $msg = "Le point de montage $path n'est pas accessible";
                $this->logger->error($msg);
            }
        }

        if (isset($commonTmp)) {
            if (empty($memoryAdapter)) {
                $adapter = new Local($commonTmp);
            } else {
                $adapter = $memoryAdapter;
            }
            $fs['common_tmp'] = new Filesystem($adapter);
        }

        $upload_tmp_dir = ini_get('upload_tmp_dir');

        if (empty($memoryAdapter)) {
            if (empty($upload_tmp_dir)) {
                $upload_tmp_dir = $commonTmp;
            }
            $adapter = new Local($upload_tmp_dir);
        } else {
            $adapter = $memoryAdapter;
        }
        $fs['upload_tmp_dir'] = new Filesystem($adapter);

        return $fs;
    }

    /**
     * @param $idBlob
     * @param $org
     * @param $em
     *
     * @return string
     */
    public function getFilePath($idBlob, $org, $em)
    {
        $blobPath = null;
        $organismeBlobFichier = $em
            ->getRepository(BloborganismeFile::class)
            ->getOrganismeBlobFichier($idBlob);
        if ($organismeBlobFichier instanceof BloborganismeFile) {
            $fileName = "$idBlob-0";
            if (!empty($organismeBlobFichier->getOldId())) {
                $fileName = $organismeBlobFichier->getOldId() . '-0';
            }
            $blobPath = $org . '/files/';
            if ($organismeBlobFichier instanceof BloborganismeFile) {
                $fileName .= $organismeBlobFichier->getExtension();
                if (null !== $organismeBlobFichier->getChemin()) {
                    $blobPath = $organismeBlobFichier->getChemin();
                }
            }
            $blobPath .= $fileName;
        } else {
            $this->logger->error(
                "La requête dans blobOrganismeFile n'a retourné aucune ligne pour l'id blob : "
                . $idBlob . ' /  organisme : ' . $org
            );
        }

        return $blobPath;
    }

    /**
     * @param $idBlob
     * @param $org
     * @param $em
     *
     *
     * @throws FileNotFoundException
     */
    public function getFileContent($idBlob, $org, $em): bool|string
    {
        $blobPath = $this->getFilePath($idBlob, $org, $em);

        foreach ($this->fileSystems as $fileSystem) {
            if ($fileSystem->has($blobPath)) {
                return $fileSystem->read($blobPath);
            }
        }

        return false;
    }

    /**
     * @param $idBlob
     * @param $org
     * @param $em
     *
     *
     * @throws FileNotFoundException
     */
    public function deleteFile($idBlob, $org, $em): bool|string
    {
        $blobPath = $this->getFilePath($idBlob, $org, $em);
        foreach ($this->fileSystems as $fileSystem) {
            if ($fileSystem->has($blobPath)) {
                return $fileSystem->delete($blobPath);
            }
        }

        return false;
    }

    /**
     * @param $organisme
     *
     * @return mixed
     */
    public function getRepertoire($organisme)
    {
        foreach ($this->fileSystems as $fileSystem) {
            return $fileSystem->getRepertoire($organisme);
        }

        return $organisme;
    }

    /**
     * @param $dir
     *
     * @return mixed
     */
    public function uniformPath($dir)
    {
        foreach ($this->fileSystems as $fileSystem) {
            return $fileSystem->uniformPath($dir);
        }

        return $dir;
    }

    /**
     * @param $blobPath
     */
    public function getSize($blobPath): false|int
    {
        $size = false;
        foreach ($this->fileSystems as $fileSystem) {
            if ($fileSystem->has($blobPath)) {
                $size = $fileSystem->getSize($blobPath);
                break;
            }
        }

        return $size;
    }

    /**
     * Method qui revoit le poids en Octet.
     *
     * @param $idBlob
     * @param $org
     * @param $em
     *
     * @return bool
     */
    public function getFileSize($idBlob, $org, $em)
    {
        $blobPath = $this->getFilePath($idBlob, $org, $em);
        foreach ($this->fileSystems as $fileSystem) {
            if ($fileSystem->has($blobPath)) {
                return $fileSystem->getSize($blobPath);
            }
        }

        return false;
    }

    /**
     * @param $idBlob
     * @param $org
     * @param $em
     */
    public function getAbsolutePath($idBlob, $org, $em): bool|string
    {
        $blobPath = $this->getFilePath($idBlob, $org, $em);
        foreach ($this->fileSystems as $fileSystem) {
            if ($fileSystem->has($blobPath)) {
                return $fileSystem->getAdapter()->getPathPrefix() . $blobPath;
            }
        }

        return false;
    }

    public function getAbsolutePathBlobFichier(int $idBlob): ?string
    {
        $blobPath = $this->getFilePathBlobFichier($idBlob);
        foreach ($this->fileSystems as $fileSystem) {
            if ($fileSystem->has($blobPath)) {
                return $fileSystem->getAdapter()->getPathPrefix() . $blobPath;
            }
        }

        return null;
    }

    public function getFilePathBlobFichier(int $idBlob): ?string
    {
        $blobPath = null;

        $blobFile = $this->entityManager->getRepository(BlobFichier::class)->find($idBlob);
        if ($blobFile instanceof BlobFichier) {
            $fileName = "$idBlob";
            if (!empty($blobFile->getOldId())) {
                $fileName = $blobFile->getOldId();
            }

            $fileName .= self::BLOB_FILE_NAME_SUFFIX;
            $fileName .= $blobFile->getExtension();

            $blobPath = $blobFile->getChemin() ?? '/common/files/';

            $blobPath .= $fileName;
        } else {
            $this->logger->error(
                sprintf(
                    "La requête dans blobFile n'a retourné aucune ligne pour l'id blob : %s",
                    $idBlob
                )
            );
        }

        return $blobPath;
    }

    /**
     * @param $idBlob
     * @param $org
     * @param $em
     *
     * @return bool|mixed
     */
    public function getExtensionFile($idBlob, $org, $em)
    {
        $blobPath = $this->getFilePath($idBlob, $org, $em);
        foreach ($this->fileSystems as $fileSystem) {
            if ($fileSystem->has($blobPath)) {
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $filename = $fileSystem->getAdapter()->getPathPrefix() . $blobPath;
                $mimetype = finfo_file($finfo, $filename);

                return $mimetype;
            }
        }

        return false;
    }
}
