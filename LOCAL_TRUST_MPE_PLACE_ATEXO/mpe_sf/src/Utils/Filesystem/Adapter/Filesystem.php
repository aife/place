<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Utils\Filesystem\Adapter;

use Exception;
use DateTime;
use League\Flysystem\Util;

class Filesystem extends \League\Flysystem\Filesystem
{
    /**
     * {@inheritdoc}
     */
    public function split($path, $chunk, $timeout = 0)
    {
        if (!$timeout) {
            return $this->getAdapter()->split($path, $chunk);
        } else {
            return $this->getAdapter()->split($path, $chunk, 6, $timeout);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function remove($path)
    {
        $path = Util::normalizePath($path);

        return (bool) $this->getAdapter()->remove($path);
    }

    /**
     * @param $path
     *
     * @return string
     */
    public function sha1File($path)
    {
        return $this->getAdapter()->sha1File($path);
    }

    /**
     * @param $folder
     * @param null $date
     *
     * @return string
     *
     * @throws Exception
     */
    public function getRepertoire($folder, $date = null)
    {
        if (null === $date) {
            $date = (new DateTime())->format('Y/m/d');
        }

        return $date.'/'.$folder.'/files/';
    }

    /**
     * @param $blobPath
     *
     * @return mixed
     */
    public function uniformPath($blobPath)
    {
        return str_replace('//', '/', $blobPath);
    }
}
