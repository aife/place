<?php

namespace App\Utils\Filesystem\Adapter;

use Symfony\Component\Process\Process;

class Local extends \League\Flysystem\Adapter\Local
{
    use AdapterTrait;

    /**
     * @param $cmd
     * @param null $filesystem
     *
     * @return Process
     */
    public function processing($cmd, $filesystem = null, int $timeout = 0)
    {
        $process = new Process($cmd);
        if (0 !== $timeout) {
            $process->setTimeout($timeout);
        }

        $process->run();

        return $process;
    }

    /**
     * @param string $path
     *
     * @return string
     */
    public function sha1File($path)
    {
        $location = $this->applyPathPrefix($path);

        return sha1_file($location);
    }

    /**
     * @param $blobPath
     *
     * @return mixed
     */
    public function uniformPath($blobPath)
    {
        return str_replace('//', '/', $blobPath);
    }
}
