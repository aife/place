<?php

namespace App\Utils\Filesystem\Adapter;

use App\Utils\Filesystem\Process\MemoryProcess;
use League\Flysystem\Util;

class MemoryAdapter extends \League\Flysystem\Memory\MemoryAdapter
{
    use AdapterTrait;

    public function processing($cmd, $filesystem = null)
    {
        $process = new MemoryProcess($cmd, $filesystem);
        $process->run();

        return $process;
    }

    /**
     * @param $pathname
     * @param string $path
     *
     * @return string
     */
    public function sha1File($path)
    {
        $path = Util::normalizePath($path);
        $content = $this->read($path);

        return sha1($content['contents']);
    }
}
