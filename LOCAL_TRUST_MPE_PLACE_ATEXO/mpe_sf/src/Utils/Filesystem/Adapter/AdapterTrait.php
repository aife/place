<?php

namespace App\Utils\Filesystem\Adapter;

use Exception;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

trait AdapterTrait
{
    /**
     * {@inheritdoc}
     */
    public function split($absoluteFilenamePath, $chunk, $pad_left = 6, $timeout = 0)
    {
        $cmd = [
            'split',
            '-b',
            $chunk,
            '-d',
            '-a' . $pad_left,
            $absoluteFilenamePath,
            $absoluteFilenamePath . '-',
        ];
        $process = $this->processing($cmd, $this, $timeout);

        if (!$process->isSuccessful()) {
            /* @var Process $process */
            throw new ProcessFailedException($process);
        }

        return $this->splitted($absoluteFilenamePath);
    }

    /**
     * @param $path
     * @param $filename
     *
     * @return array
     */
    public function splitted($absoluteFilenamePath)
    {
        $pathExploded = explode('/', (string) $absoluteFilenamePath);
        $filename1 = array_pop($pathExploded);
        $pathExploded = explode('.', (string) $absoluteFilenamePath);
        $filename = array_pop($pathExploded);

        $pathDirectory = array_pop($pathExploded);
        $arrayPathDirectory = (array)$pathDirectory;
        $pathDir = array_pop($arrayPathDirectory);

        $files = [];
        $arrayPath = explode('/', (string) $pathDirectory);
        array_pop($arrayPath);
        $pathDir = implode('/', $arrayPath);
        foreach ($this->listContents($pathDir, true) as $file) {
            $path = $file['path'];
            if (strstr($path, (string) ($filename1 . '-'))) {
                $files[] = $file;
            }
        }

        return $files;
    }

    /**
     * remove with wildcard.
     *
     * @param $pathname
     * @param string $path
     *
     * @throws Exception
     */
    public function remove($pathname, $path = '')
    {
        if (strlen((string) $pathname) < 5) {
            throw new Exception("Can't remove pathname : '$pathname'. It should be > 5 characters!");
        }
        $pattern = str_replace('*', '(.*)', $pathname);
        foreach ($this->listContents($path, true) as $file) {
            if ('file' === $file['type']) {
                if (preg_match('#^' . $pattern . '$#', (string) $file['path'], $matches)) {
                    $this->delete($file['path']);
                }
            }
        }
    }
}
