<?php

namespace App\Utils\Filesystem\Process;

use App\Utils\Filesystem\Filesystem;
use League\Flysystem\Config;

class MemoryProcess
{
    public function __construct(private $cmd = '', private $filesystem = '')
    {
    }

    public function run()
    {
        $absoluteFilenamePath = $this->cmd[5];
        $chunk = $this->cmd[2];
        $this->splitProcess($absoluteFilenamePath, $chunk);
    }

    public function isSuccessful()
    {
        return true;
    }

    public function splitProcess($absoluteFilenamePath, $chunk = 10, $pad_left = 6)
    {
        $absoluteFilenamePath = str_replace('./', '', $absoluteFilenamePath);
        $s = $this->filesystem->read($absoluteFilenamePath);
        if (isset($s['contents'])) {
            $s = $s['contents'];
        }
        $count = floor(strlen((string) $s) / $chunk);
        if (\strlen((string) $s) % $chunk > 0) {
            ++$count;
        }
        for ($i = 0; $i < $count; ++$i) {
            $splittedFilename = $absoluteFilenamePath.'-'.
                str_pad(
                    $i,
                    $pad_left,
                    '0',
                    STR_PAD_LEFT
                );
            $ind = $i * $chunk;
            $content = substr($s, $ind, $chunk);
            $config = new Config();
            $this->filesystem->write($splittedFilename, $content, $config);
        }
    }
}
