<?php

namespace App\Utils;

/**
 * Vo pour définir les critères de Atexo_Consultation::search().
 */
class AtexoConsultationCriteriaVo
{
    private string $referenceConsultation = '';
    private string $referenceConsultationRestreinte = '';
    private $idReference;
    private string $idTypeAvis = '';
    private string $notIdTypeAvis = '';
    private string $idTypeProcedure = '';
    private string $idEtatConsultation = '';
    private string $acronymeOrganisme = '';
    private string $acronymeOrganismeProcRest = '';
    private string $idService = '';
    private string $idServiceRma = '';
    private string $typeAcces = '';
    private string $categorieConsultation = '';
    private string $codeAccesProcedureRestreinte = '';
    private string $idCodeCpv1 = '';
    private string $idCodeCpv2 = '';
    private string $idCodeCpv3 = '';
    private string $idCodeCpv4 = '';
    private string $dateFinStart = '';
    private string $dateFinEnd = '';
    private string $intituleConsultation = '';
    private string $objetConsultation = '';
    private string $commentaireInterne = '';
    private string $intituleLot = '';
    private string $description = '';
    private string $typeSearch = '';
    private string $keyWordRechercheRapide = '';
    private string $keyWordAdvancedSearch = '';
    private string $etatConsultation = '';
    private string $defaultSortByElement = 'datefin';
    private string $sortByElement = '';
    private string $sensOrderBy = 'DESC';
    private int $offset = 0;
    private int $limit = 0;
    private string $secondConditionOnEtatConsultation = '';
    private bool $searchModeExact = false;
    private string $_lieuxExecution = '';
    private $_connectedAgentId;
    private bool $_calledFromPortail = false;
    private string $_periodicite = '';
    private string $_orgActive = '1';
    private string $_publicationEurope = '';
    private bool $_etatPublicationToBePublished = false;
    private bool $_etatPublicationPublished = false;
    private bool $_etatPublicationToBeUpdated = false;
    private bool $_etatPublicationUpdated = false;
    private bool $_alertesFromYesterday = true;
    private bool $_calledFromElios = false;
    private bool $_sansCritereActive = false;
    private bool $_consultationAValiderSeulement = false;
    private bool $_consultationAApprouverSeulement = false;
    //A true pour banir les consultations visible temporairement pour validation ou attribution
    private bool $_noTemporaryCOnsultation = false;
    private $_enLigneDepuis;
    private $_enLigneJusquau;
    private $_orgDenomination;
    private string $_typeOrganisme = '';
    private string $_domaineActivite = '';
    private string $_qualification = '';
    private string $_agrements = '';
    private bool $_fromArchive = false;
    private $_codesNuts;
    private string $_idsService = '';
    //tableau d'objet referentielVo
    private array $referentielVo = [];
    private string $_idInscrit = '';
    private string $_idEntreprise = '';
    private bool $_forPanierEntreprise = false;
    private bool $_avecRetrait = false;
    private bool $_avecQuestion = false;
    private bool $_avecDepot = false;
    private bool $_avecEchange = false;
    private bool $_sansRetrait = false;
    private bool $_sansQuestion = false;
    private bool $_sansDepot = false;
    private bool $_sansEchange = false;
    private int $idAlerteConsultation = 0;
    private int $alerteConsultationCloturee = 0;
    //mettre a true pour recuperer les consultation publiees depuis hier
    private bool $publieHier = false;
    //mettre $withDce a true  pour chercher que les consultation ayant un DCE
    private bool $withDce = false;
    //mettre  $informationMarche a true pour retourner que les Avis de marches et les avis de pre-informations
    private bool $informationMarche = false;
    private bool $dateTimeFin = false;
    private bool $_avecConsClotureesSansPoursuivreAffichage = false;
    private bool $exclureConsultationExterne = false;
    private int $afficherDoublon = 0;
    private $denominationAdapte;
    private int $clauseSociale = 0;
    private int $atelierProtege = 0;
    private int $ess = 0;
    private int $siae = 0;
    private int $clauseEnv = 0;
    private int $Mps = 0;
    private bool $consultaionPublier = false;
    //mettre a 1 pour recuperer les consultations annulees
    private bool $consultationAnnulee = false;
    // Accesseurs
    private $idOperation;
    private bool|string $inclureDescendance = true;
    private $rechercheAvanceeAutoCompletion;
    private bool $consultationAArchiver = false;
    private string $bourseCotraitance = '0';
    private bool $visionRma = false;
    private bool $organismeTest = false;
    private $socialeCommerceEquitable;
    private $socialeInsertionActiviterEconomique;

    public function __construct($criteriaVo = null)
    {
        if (null !== $criteriaVo) {
            $this->referenceConsultation = $criteriaVo->getReferenceConsultation();
            $this->referenceConsultationRestreinte = $criteriaVo->getReferenceConsultationRestreinte();
            $this->idReference = $criteriaVo->getIdReference();
            $this->idTypeAvis = $criteriaVo->getIdTypeAvis();
            $this->notIdTypeAvis = $criteriaVo->getNotIdTypeAvis();
            $this->idTypeProcedure = $criteriaVo->getIdTypeProcedure();
            $this->idEtatConsultation = $criteriaVo->getIdEtatConsultation();
            $this->acronymeOrganisme = $criteriaVo->getAcronymeOrganisme();
            $this->acronymeOrganismeProcRest = $criteriaVo->getAcronymeOrganismeProcRest();
            $this->idService = $criteriaVo->getIdService();
            $this->idServiceRma = $criteriaVo->getIdServiceRma();
            $this->typeAcces = $criteriaVo->getTypeAcces();
            $this->categorieConsultation = $criteriaVo->getCategorieConsultation();
            $this->codeAccesProcedureRestreinte = $criteriaVo->getCodeAccesProcedureRestreinte();
            $this->idCodeCpv1 = $criteriaVo->getIdCodeCpv1();
            $this->idCodeCpv2 = $criteriaVo->getIdCodeCpv2();
            $this->idCodeCpv3 = $criteriaVo->getIdCodeCpv3();
            $this->idCodeCpv4 = $criteriaVo->getIdCodeCpv4();
            $this->dateFinStart = $criteriaVo->getDateFinStart();
            $this->dateFinEnd = $criteriaVo->getDateFinEnd();
            $this->intituleConsultation = $criteriaVo->getIntituleConsultation();
            $this->objetConsultation = $criteriaVo->getObjetConsultation();
            $this->commentaireInterne = $criteriaVo->getCommentaireInterne();
            $this->intituleLot = $criteriaVo->getIntituleLot();
            $this->description = $criteriaVo->getDescription();
            $this->typeSearch = $criteriaVo->getTypeSearch();
            $this->keyWordRechercheRapide = $criteriaVo->getKeyWordRechercheRapide();
            $this->keyWordAdvancedSearch = $criteriaVo->getKeyWordAdvancedSearch();
            $this->etatConsultation = $criteriaVo->getEtatConsultation();
            $this->defaultSortByElement = $criteriaVo->getDefaultSortByElement();
            $this->sortByElement = $criteriaVo->getSortByElement();
            $this->sensOrderBy = $criteriaVo->getSensOrderBy();
            $this->offset = $criteriaVo->getOffset();
            $this->limit = $criteriaVo->getLimit();
            $this->secondConditionOnEtatConsultation = $criteriaVo->getSecondConditionOnEtatConsultation();
            $this->searchModeExact = $criteriaVo->getSearchModeExact();
            $this->_lieuxExecution = $criteriaVo->getLieuxexecution();
            $this->_connectedAgentId = $criteriaVo->getConnectedAgentId();
            $this->_calledFromPortail = $criteriaVo->getcalledFromPortail();
            $this->_periodicite = $criteriaVo->getPeriodicite();
            $this->_orgActive = $criteriaVo->getOrgActive();
            $this->_publicationEurope = $criteriaVo->getPublicationEurope();
            $this->_etatPublicationToBePublished = $criteriaVo->getEtatPublicationToBePublished();
            $this->_etatPublicationPublished = $criteriaVo->getEtatPublicationPublished();
            $this->_etatPublicationToBeUpdated = $criteriaVo->getEtatPublicationToBeUpdated();
            $this->_etatPublicationUpdated = $criteriaVo->getEtatPublicationUpdated();
            $this->_alertesFromYesterday = $criteriaVo->getAlertesFromYesterday();
            $this->_calledFromElios = $criteriaVo->getCalledFromHelios();
            $this->_sansCritereActive = $criteriaVo->getSansCritereActive();
            $this->_consultationAValiderSeulement = $criteriaVo->getConsultationAValiderSeulement();
            $this->_consultationAApprouverSeulement = $criteriaVo->getConsultationAApprouverSeulement();
            //A true pour banir les consultations visible temporairement pour validation ou attribution
            $this->_noTemporaryCOnsultation = $criteriaVo->getNoTemporaryCOnsultation();
            $this->_enLigneDepuis = $criteriaVo->getEnLigneDepuis();
            $this->_enLigneJusquau = $criteriaVo->getEnLigneJusquau();
            $this->_orgDenomination = $criteriaVo->getOrgDenomination();
            $this->_typeOrganisme = $criteriaVo->getTypeOrganisme();
            $this->_domaineActivite = $criteriaVo->getDomaineActivite();
            $this->_qualification = $criteriaVo->getQualification();
            $this->_agrements = $criteriaVo->getAgrements();
            $this->_fromArchive = $criteriaVo->getFromArchive();
            $this->_codesNuts = $criteriaVo->getCodesNuts();
            $this->_idsService = $criteriaVo->getIdsService();
            //tableau d'objet referentielVo
            $this->referentielVo = $criteriaVo->getReferentielVo();
            $this->_idInscrit = $criteriaVo->getIdInscrit();
            $this->_idEntreprise = $criteriaVo->getIdEntreprise();
            $this->_forPanierEntreprise = $criteriaVo->getForPanierEntreprise();
            $this->_avecRetrait = $criteriaVo->getAvecRetrait();
            $this->_avecQuestion = $criteriaVo->getAvecQuestion();
            $this->_avecDepot = $criteriaVo->getAvecDepot();
            $this->_avecEchange = $criteriaVo->getAvecEchange();
            $this->_sansRetrait = $criteriaVo->getSansRetrait();
            $this->_sansQuestion = $criteriaVo->getSansQuestion();
            $this->_sansDepot = $criteriaVo->getSansDepot();
            $this->_sansEchange = $criteriaVo->getSansEchange();
            $this->idAlerteConsultation = $criteriaVo->getIdAlerteConsultation();
            $this->alerteConsultationCloturee = $criteriaVo->getAlerteConsultationCloturee();
            //mettre a true pour recuperer les consultation publiees depuis hier
            $this->publieHier = $criteriaVo->getPublieHier();
            //mettre $this->withDce a true  pour chercher que les consultation ayant un DCE
            $this->withDce = $criteriaVo->getWithDce();
            //mettre  $this->informationMarche a true pour retourner que les Avis de marches et les avis de pre-informations
            $this->informationMarche = $criteriaVo->getInformationMarche();
            $this->dateTimeFin = $criteriaVo->getDateTimeFin();
            $this->_avecConsClotureesSansPoursuivreAffichage = $criteriaVo->getAvecConsClotureesSansPoursuivreAffichage();
            $this->exclureConsultationExterne = $criteriaVo->getExclureConsultationExterne();
            $this->afficherDoublon = $criteriaVo->getAfficherDoublon();
            $this->denominationAdapte = $criteriaVo->getDenominationAdapte();
            $this->clauseSociale = $criteriaVo->getClauseSociale();
            $this->atelierProtege = $criteriaVo->getAtelierProtege();
            $this->ess = $criteriaVo->getEss();
            $this->siae = $criteriaVo->getSiae();
            $this->clauseEnv = $criteriaVo->getClauseEnv();
            $this->Mps = $criteriaVo->getMps();
            $this->consultaionPublier = $criteriaVo->getConsultaionPublier();
            //mettre a 1 pour recuperer les consultations annulees
            $this->consultationAnnulee = $criteriaVo->getConsultationAnnulee();
            // Accesseurs
            $this->idOperation = $criteriaVo->getIdOperation();
            $this->inclureDescendance = true;
            $this->rechercheAvanceeAutoCompletion = $criteriaVo->getRechercheAvanceeAutoCompletion();
            $this->consultationAArchiver = $criteriaVo->getConsultationAArchiver();
            $this->bourseCotraitance = $criteriaVo->getBourseCotraitance();
            $this->visionRma = $criteriaVo->getVisionRma();
            $this->organismeTest = $criteriaVo->getOrganismeTest();
            $this->socialeCommerceEquitable = $criteriaVo->getSocialeCommerceEquitable();
            $this->socialeInsertionActiviterEconomique = $criteriaVo->getSocialeInsertionActiviterEconomique();
        }
    }

    /**
     * @return bool
     */
    public function getOrganismeTest()
    {
        return $this->organismeTest;
    }

    /**
     * @param bool $organismeTest
     */
    public function setOrganismeTest($organismeTest)
    {
        $this->organismeTest = $organismeTest;
    }

    public function getSocialeCommerceEquitable()
    {
        return $this->socialeCommerceEquitable;
    }

    public function setSocialeCommerceEquitable($value)
    {
        $this->socialeCommerceEquitable = $value;
    }

    public function getSocialeInsertionActiviterEconomique()
    {
        return $this->socialeInsertionActiviterEconomique;
    }

    public function setSocialeInsertionActiviterEconomique($value)
    {
        $this->socialeInsertionActiviterEconomique = $value;
    }

    /**
     * @return string
     */
    public function getBourseCotraitance()
    {
        return $this->bourseCotraitance;
    }

    /**
     * @param string $bourseCotraitance
     */
    public function setBourseCotraitance($bourseCotraitance)
    {
        $this->bourseCotraitance = $bourseCotraitance;
    }

    public function getConsultationAnnulee()
    {
        return $this->consultationAnnulee;
    }

    public function setConsultationAnnulee($value)
    {
        $this->consultationAnnulee = $value;
    }

    public function getConsultaionPublier()
    {
        return $this->consultaionPublier;
    }

    public function setConsultaionPublier($value)
    {
        $this->consultaionPublier = $value;
    }

    public function getWithDce()
    {
        return $this->withDce;
    }

    public function setWithDce($value)
    {
        $this->withDce = $value;
    }

    public function getPublieHier()
    {
        return $this->publieHier;
    }

    public function setPublieHier($publieHier)
    {
        $this->publieHier = $publieHier;
    }

    public function getInformationMarche()
    {
        return $this->informationMarche;
    }

    public function setInformationMarche($informationMarche)
    {
        $this->informationMarche = $informationMarche;
    }

    public function getIdReference()
    {
        return $this->idReference;
    }

    public function setIdReference($idReference)
    {
        $this->idReference = $idReference;
    }

    public function getNotIdTypeAvis()
    {
        return $this->notIdTypeAvis;
    }

    public function setNotIdTypeAvis($notIdTypeAvis)
    {
        $this->notIdTypeAvis = $notIdTypeAvis;
    }

    public function getIdEtatConsultation()
    {
        return $this->idEtatConsultation;
    }

    public function setIdEtatConsultation($idEtatConsultation)
    {
        $this->idEtatConsultation = $idEtatConsultation;
    }

    public function getAcronymeOrganismeProcRest()
    {
        return $this->acronymeOrganismeProcRest;
    }

    public function setAcronymeOrganismeProcRest($acronymeOrganismeProRes)
    {
        $this->acronymeOrganismeProcRest = $acronymeOrganismeProRes;
    }

    public function getIdService()
    {
        return $this->idService;
    }

    public function setIdService($idService)
    {
        $this->idService = $idService;
    }

    public function getIdServiceRma()
    {
        return $this->idServiceRma;
    }

    public function setIdServiceRma($idService)
    {
        $this->idServiceRma = $idService;
    }

    public function getIdCodeCpv2()
    {
        return $this->idCodeCpv2;
    }

    public function setIdCodeCpv2($idCodeCpv2)
    {
        $this->idCodeCpv2 = $idCodeCpv2;
    }

    public function getIdCodeCpv3()
    {
        return $this->idCodeCpv3;
    }

    public function setIdCodeCpv3($idCodeCpv3)
    {
        $this->idCodeCpv3 = $idCodeCpv3;
    }

    public function getIdCodeCpv4()
    {
        return $this->idCodeCpv4;
    }

    public function setIdCodeCpv4($idCodeCpv4)
    {
        $this->idCodeCpv4 = $idCodeCpv4;
    }

    public function getDateFinStart()
    {
        return $this->dateFinStart;
    }

    public function setDateFinStart($dateFinStart)
    {
        $this->dateFinStart = $dateFinStart;
    }

    public function getDateFinEnd()
    {
        return $this->dateFinEnd;
    }

    public function setDateFinEnd($dateFinEnd)
    {
        $this->dateFinEnd = $dateFinEnd;
    }

    public function getIntituleConsultation()
    {
        return $this->intituleConsultation;
    }

    public function setIntituleConsultation($intituleConsultation)
    {
        $this->intituleConsultation = $intituleConsultation;
    }

    public function getObjetConsultation()
    {
        return $this->objetConsultation;
    }

    public function setObjetConsultation($objetConsultation)
    {
        $this->objetConsultation = $objetConsultation;
    }

    public function getCommentaireInterne()
    {
        return $this->commentaireInterne;
    }

    public function setCommentaireInterne($commentaireInterne)
    {
        $this->commentaireInterne = $commentaireInterne;
    }

    public function getIntituleLot()
    {
        return $this->intituleLot;
    }

    public function setIntituleLot($intituleLot)
    {
        $this->intituleLot = $intituleLot;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getTypeSearch()
    {
        return $this->typeSearch;
    }

    public function setTypeSearch($typeSearch)
    {
        $this->typeSearch = $typeSearch;
    }

    public function getKeyWordRechercheRapide()
    {
        return $this->keyWordRechercheRapide;
    }

    public function setKeyWordRechercheRapide($keyWordRechercheRapide)
    {
        $this->keyWordRechercheRapide = $keyWordRechercheRapide;
    }

    public function getKeyWordAdvancedSearch()
    {
        return $this->keyWordAdvancedSearch;
    }

    public function setKeyWordAdvancedSearch($keyWordAdvancedSearch)
    {
        $this->keyWordAdvancedSearch = $keyWordAdvancedSearch;
    }

    public function getEtatConsultation()
    {
        return $this->etatConsultation;
    }

    public function setEtatConsultation($etatConsultation)
    {
        $this->etatConsultation = $etatConsultation;
    }

    public function getDefaultSortByElement()
    {
        return $this->defaultSortByElement;
    }

    public function setDefaultSortByElement($defaultSortByElement)
    {
        $this->defaultSortByElement = $defaultSortByElement;
    }

    public function getSortByElement()
    {
        return $this->sortByElement;
    }

    public function setSortByElement($sortByElement)
    {
        $this->sortByElement = $sortByElement;
    }

    public function getSensOrderBy()
    {
        return $this->sensOrderBy;
    }

    public function setSensOrderBy($sensOrderBy)
    {
        $this->sensOrderBy = $sensOrderBy;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    public function getSearchModeExact()
    {
        return $this->searchModeExact;
    }

    public function setSearchModeExact($boolean)
    {
        $this->searchModeExact = $boolean;
    }

    public function getSecondConditionOnEtatConsultation()
    {
        return $this->secondConditionOnEtatConsultation;
    }

    public function setSecondConditionOnEtatConsultation($secondConditionOnEtatConsultation)
    {
        $this->secondConditionOnEtatConsultation = $secondConditionOnEtatConsultation;
    }

    public function getConnectedAgentId()
    {
        return $this->_connectedAgentId;
    }

    public function setConnectedAgentId($connectedAgentId)
    {
        $this->_connectedAgentId = $connectedAgentId;
    }

    /**
     * @return int
     */
    public function getEss()
    {
        return $this->ess;
    }

    /**
     * @param int $ess
     */
    public function setEss($ess)
    {
        $this->ess = $ess;
    }

    /**
     * @return int
     */
    public function getSiae()
    {
        return $this->siae;
    }

    /**
     * @param int $siae
     */
    public function setSiae($siae)
    {
        $this->siae = $siae;
    }

    public function toString()
    {
        $res = '';
        if ('' !== $this->getAcronymeOrganisme()) {
            $res .= ' / Org='.$this->getAcronymeOrganisme();
        }
        if ('' !== $this->getCategorieConsultation()) {
            $res .= ' / idCategorie='.$this->getCategorieConsultation();
        }
        if ('' !== $this->getReferenceConsultation()) {
            $res .= ' / Reference utilisateur='.$this->getReferenceConsultation();
        }
        if ('' != $this->getcalledFromPortail()) {
            $res .= ' / call from ='.$this->getcalledFromPortail();
        }
        if ('' != $this->getIdCodeCpv1()) {
            $res .= ' /cpv ='.$this->getIdCodeCpv1();
        }
        if ('' != $this->getLieuxexecution()) {
            $res .= ' /Lieux execution ='.$this->getLieuxexecution();
        }
        if ('' != $this->getIdTypeProcedure()) {
            $res .= ' /Id Type Procedure ='.$this->getIdTypeProcedure();
        }
        if ('' != $this->getIdTypeAvis()) {
            $res .= ' /Id Type Avis ='.$this->getIdTypeAvis();
        }
        if ('' != $this->getTypeAcces()) {
            $res .= ' /Type Procedure ='.$this->getTypeAcces();
        }
        if ('' != $this->getCodeAccesProcedureRestreinte()) {
            $res .= ' /Code Acces Procedure Restreinte ='.$this->getCodeAccesProcedureRestreinte();
        }

        return $res;
    }

    public function getAcronymeOrganisme()
    {
        return $this->acronymeOrganisme;
    }

    public function setAcronymeOrganisme($acronymeOrganisme)
    {
        $this->acronymeOrganisme = $acronymeOrganisme;
    }

    public function getCategorieConsultation()
    {
        return $this->categorieConsultation;
    }

    public function setCategorieConsultation($categorieConsultation)
    {
        $this->categorieConsultation = $categorieConsultation;
    }

    public function getReferenceConsultation()
    {
        return $this->referenceConsultation;
    }

    public function setReferenceConsultation($referenceConsultation)
    {
        $this->referenceConsultation = $referenceConsultation;
    }

    public function getcalledFromPortail()
    {
        return $this->_calledFromPortail;
    }

    public function setcalledFromPortail($calledFromPortail)
    {
        $this->_calledFromPortail = $calledFromPortail;
    }

    public function getIdCodeCpv1()
    {
        return $this->idCodeCpv1;
    }

    public function setIdCodeCpv1($idCodeCpv1)
    {
        $this->idCodeCpv1 = $idCodeCpv1;
    }

    public function getLieuxexecution()
    {
        return $this->_lieuxExecution;
    }

    public function setLieuxexecution($lieuxExecution)
    {
        $this->_lieuxExecution = $lieuxExecution;
    }

    public function getIdTypeProcedure()
    {
        return $this->idTypeProcedure;
    }

    public function setIdTypeProcedure($idTypeProcedure)
    {
        $this->idTypeProcedure = $idTypeProcedure;
    }

    public function getIdTypeAvis()
    {
        return $this->idTypeAvis;
    }

    public function setIdTypeAvis($idTypeAvis)
    {
        $this->idTypeAvis = $idTypeAvis;
    }

    public function getTypeAcces()
    {
        return $this->typeAcces;
    }

    public function setTypeAcces($typeAcces)
    {
        $this->typeAcces = $typeAcces;
    }

    public function getCodeAccesProcedureRestreinte()
    {
        return $this->codeAccesProcedureRestreinte;
    }

    public function setCodeAccesProcedureRestreinte($codeAccesProcedureRestreinte)
    {
        $this->codeAccesProcedureRestreinte = $codeAccesProcedureRestreinte;
    }

    public function getPeriodicite()
    {
        return $this->_periodicite;
    }

    public function setPeriodicite($periodicite)
    {
        $this->_periodicite = $periodicite;
    }

    public function getOrgActive()
    {
        return $this->_orgActive;
    }

    public function setOrgActive($value)
    {
        $this->_orgActive = $value;
    }

    public function getPublicationEurope()
    {
        return $this->_publicationEurope;
    }

    public function setPublicationEurope($value)
    {
        $this->_publicationEurope = $value;
    }

    public function getEtatPublicationToBePublished()
    {
        return $this->_etatPublicationToBePublished;
    }

    public function setEtatPublicationToBePublished($bool)
    {
        $this->_etatPublicationToBePublished = $bool;
    }

    public function getEtatPublicationPublished()
    {
        return $this->_etatPublicationPublished;
    }

    public function setEtatPublicationPublished($bool)
    {
        $this->_etatPublicationPublished = $bool;
    }

    public function getEtatPublicationToBeUpdated()
    {
        return $this->_etatPublicationToBeUpdated;
    }

    public function setEtatPublicationToBeUpdated($bool)
    {
        $this->_etatPublicationToBeUpdated = $bool;
    }

    public function getEtatPublicationUpdated()
    {
        return $this->_etatPublicationUpdated;
    }

    public function setEtatPublicationUpdated($bool)
    {
        $this->_etatPublicationUpdated = $bool;
    }

    public function getAlertesFromYesterday()
    {
        return $this->_alertesFromYesterday;
    }

    public function setAlertesFromYesterday($bool)
    {
        $this->_alertesFromYesterday = $bool;
    }

    public function getCalledFromHelios()
    {
        return $this->_calledFromElios;
    }

    public function setCalledFromHelios($bool)
    {
        $this->_calledFromElios = $bool;
    }

    /**
     * @deprecated use getCalledFromHelios
     */
    public function getCalledFromElios()
    {
        return $this->_calledFromElios;
    }

    /**
     * @deprecated use getCalledFromHelios
     */
    public function setCalledFromElios($bool)
    {
        $this->_calledFromElios = $bool;
    }

    public function getSansCritereActive()
    {
        return $this->_sansCritereActive;
    }

    public function setSansCritereActive($bool)
    {
        $this->_sansCritereActive = $bool;
    }

    public function getConsultationAValiderSeulement()
    {
        return $this->_consultationAValiderSeulement;
    }

    public function setConsultationAValiderSeulement($value)
    {
        $this->_consultationAValiderSeulement = $value;
    }

    public function getConsultationAApprouverSeulement()
    {
        return $this->_consultationAApprouverSeulement;
    }

    public function setConsultationAApprouverSeulement($value)
    {
        $this->_consultationAApprouverSeulement = $value;
    }

    /**
     * A true pour banir les consultations visible temporairement pour validation simple, finale ou interm.
     */
    public function getNoTemporaryCOnsultation()
    {
        return $this->_noTemporaryCOnsultation;
    }

    public function setNoTemporaryCOnsultation($value)
    {
        $this->_noTemporaryCOnsultation = $value;
    }

    public function getEnLigneDepuis()
    {
        return $this->_enLigneDepuis;
    }

    public function setEnLigneDepuis($value)
    {
        $this->_enLigneDepuis = $value;
    }

    public function getEnLigneJusquau()
    {
        return $this->_enLigneJusquau;
    }

    public function setEnLigneJusquau($value)
    {
        $this->_enLigneJusquau = $value;
    }

    public function getOrgDenomination()
    {
        return $this->_orgDenomination;
    }

    public function setOrgDenomination($value)
    {
        $this->_orgDenomination = $value;
    }

    public function getTypeOrganisme()
    {
        return $this->_typeOrganisme;
    }

    public function setTypeOrganisme($value)
    {
        $this->_typeOrganisme = $value;
    }

    public function getDomaineActivite()
    {
        return $this->_domaineActivite;
    }

    public function setDomaineActivite($value)
    {
        $this->_domaineActivite = $value;
    }

    public function getQualification()
    {
        return $this->_qualification;
    }

    public function setQualification($value)
    {
        $this->_qualification = $value;
    }

    public function getAgrements()
    {
        return $this->_agrements;
    }

    public function setAgrements($value)
    {
        $this->_agrements = $value;
    }

    public function getFromArchive()
    {
        return $this->_fromArchive;
    }

    public function setFromArchive($value)
    {
        $this->_fromArchive = $value;
    }

    public function getCodesNuts()
    {
        return $this->_codesNuts;
    }

    public function setCodesNuts($value)
    {
        $this->_codesNuts = $value;
    }

    public function getIdsService()
    {
        return $this->_idsService;
    }

    public function setIdsService($value)
    {
        $this->_idsService = $value;
    }

    public function getReferentielVo()
    {
        return $this->referentielVo;
    }

    public function setReferentielVo($value)
    {
        $this->referentielVo = $value;
    }

    public function getIdInscrit()
    {
        return $this->_idInscrit;
    }

    public function setIdInscrit($value)
    {
        $this->_idInscrit = $value;
    }

    public function getIdEntreprise()
    {
        return $this->_idEntreprise;
    }

    public function setIdEntreprise($value)
    {
        $this->_idEntreprise = $value;
    }

    public function getForPanierEntreprise()
    {
        return $this->_forPanierEntreprise;
    }

    public function setForPanierEntreprise($value)
    {
        $this->_forPanierEntreprise = $value;
    }

    public function getAvecRetrait()
    {
        return $this->_avecRetrait;
    }

    public function setAvecRetrait($value)
    {
        $this->_avecRetrait = $value;
    }

    public function getAvecQuestion()
    {
        return $this->_avecQuestion;
    }

    public function setAvecQuestion($value)
    {
        $this->_avecQuestion = $value;
    }

    public function getAvecDepot()
    {
        return $this->_avecDepot;
    }

    public function setAvecDepot($value)
    {
        $this->_avecDepot = $value;
    }

    public function getAvecEchange()
    {
        return $this->_avecEchange;
    }

    public function setAvecEchange($value)
    {
        $this->_avecEchange = $value;
    }

    public function getDateTimeFin()
    {
        return $this->dateTimeFin;
    }

    public function setDateTimeFin($dateTimeFin)
    {
        $this->dateTimeFin = $dateTimeFin;
    }

    public function getAvecConsClotureesSansPoursuivreAffichage()
    {
        return $this->_avecConsClotureesSansPoursuivreAffichage;
    }

    public function setAvecConsClotureesSansPoursuivreAffichage($value)
    {
        $this->_avecConsClotureesSansPoursuivreAffichage = $value;
    }

    public function getExclureConsultationExterne()
    {
        return $this->exclureConsultationExterne;
    }

    public function setExclureConsultationExterne($value)
    {
        $this->exclureConsultationExterne = $value;
    }

    public function getIdAlerteConsultation()
    {
        return $this->idAlerteConsultation;
    }

    public function setIdAlerteConsultation($value)
    {
        $this->idAlerteConsultation = $value;
    }

    public function getAlerteConsultationCloturee()
    {
        return $this->alerteConsultationCloturee;
    }

    public function setAlerteConsultationCloturee($value)
    {
        $this->alerteConsultationCloturee = $value;
    }

    public function getAfficherDoublon()
    {
        return $this->afficherDoublon;
    }

    public function setAfficherDoublon($value)
    {
        $this->afficherDoublon = $value;
    }

    public function getSansRetrait()
    {
        return $this->_sansRetrait;
    }

    public function setSansRetrait($value)
    {
        $this->_sansRetrait = $value;
    }

    public function getSansQuestion()
    {
        return $this->_sansQuestion;
    }

    public function setSansQuestion($value)
    {
        $this->_sansQuestion = $value;
    }

    public function getSansDepot()
    {
        return $this->_sansDepot;
    }

    public function setSansDepot($value)
    {
        $this->_sansDepot = $value;
    }

    public function getSansEchange()
    {
        return $this->_sansEchange;
    }

    public function setSansEchange($value)
    {
        $this->_sansEchange = $value;
    }

    public function getDenominationAdapte()
    {
        return $this->denominationAdapte;
    }

    public function setDenominationAdapte($value)
    {
        $this->denominationAdapte = $value;
    }

    public function getClauseSociale()
    {
        return $this->clauseSociale;
    }

    public function setClauseSociale($clauseSociale)
    {
        $this->clauseSociale = $clauseSociale;
    }

    public function getAtelierProtege()
    {
        return $this->atelierProtege;
    }

    public function setAtelierProtege($atelierProtege)
    {
        $this->atelierProtege = $atelierProtege;
    }

    public function getClauseEnv()
    {
        return $this->clauseEnv;
    }

    public function setClauseEnv($clauseEnv)
    {
        $this->clauseEnv = $clauseEnv;
    }

    public function getIdOperation()
    {
        return $this->idOperation;
    }

    public function setIdOperation($value)
    {
        $this->idOperation = $value;
    }

    public function getMps()
    {
        return $this->Mps;
    }

    public function setMps($mps)
    {
        $this->Mps = $mps;
    }

    /**
     * Permet de recuperer la valeur de l'attribut $inclureDescendance.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function getInclureDescendance()
    {
        return $this->inclureDescendance;
    }

    /**
     * Permet d'affecter la valeur de l'attribut $inclureDescendance.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @param string $value : valeur a affecter a l'attribut
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function setInclureDescendance($value)
    {
        $this->inclureDescendance = $value;
    }

    /**
     * Permet de recuperer la valeur de l'attribut $rechercheAvanceeAutoCompletion.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function getRechercheAvanceeAutoCompletion()
    {
        return $this->rechercheAvanceeAutoCompletion;
    }

    /**
     * Permet d'affecter la valeur de l'attribut $rechercheAvanceeAutoCompletion.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @param string $value : valeur a affecter a l'attribut
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function setRechercheAvanceeAutoCompletion($value)
    {
        $this->rechercheAvanceeAutoCompletion = $value;
    }

    /**
     * Permet de recuperer la valeur de l'attribut $consultationAArchiver.
     *
     * @author oubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getConsultationAArchiver()
    {
        return $this->consultationAArchiver;
    }

    /**
     * Permet d'affecter la valeur de l'attribut $consultationAArchiver.
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @param string $value : valeur a affecter a l'attribut
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setConsultationAArchiver($value)
    {
        $this->consultationAArchiver = $value;
    }

    public function getReferenceConsultationRestreinte()
    {
        return $this->referenceConsultationRestreinte;
    }

    public function setReferenceConsultationRestreinte($referenceConsultationRestreinte)
    {
        $this->referenceConsultationRestreinte = $referenceConsultationRestreinte;
    }

    /**
     * @return bool
     */
    public function getVisionRma()
    {
        return $this->visionRma;
    }

    /**
     * @param bool $visionRma
     */
    public function setVisionRma($visionRma)
    {
        $this->visionRma = $visionRma;
    }
}
