<?php

namespace App\Utils;

use Symfony\Contracts\Translation\TranslatorInterface;

class Translation
{
    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    /**
     * Traduit les chaines suffixé et préfixé par un double @.
     *
     * @param $doubleAtMessages
     */
    public function doubleAtMessagesTranslate($doubleAtMessages, string $separator = '@@'): string
    {
        $return = '';
        if (null !== $doubleAtMessages) {
            $messages = explode($separator, (string) $doubleAtMessages);
            if (is_array($messages)) {
                foreach ($messages as $message) {
                    if ('' !== $message) {
                        $return .= $this->translator->trans($message);
                    }
                }
            }
        }

        return $return;
    }
}
