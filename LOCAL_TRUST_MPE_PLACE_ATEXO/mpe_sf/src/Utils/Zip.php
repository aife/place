<?php

namespace App\Utils;

use \ZipArchive;

class Zip
{
    public function __construct(private readonly Encryption $encryption)
    {
    }

    public function getArrayOrderedFilesForZip($dce_tmp_file, &$maxIndex, $prefix)
    {
        if (empty($dce_tmp_file)) {
            return [];
        }
        $dceInfo = $this->getInfoFilesZip($dce_tmp_file, $maxIndex);
        $dce_items = $dceInfo['chemins'];
        $dce_items_size = $dceInfo['tailles'];
        $ordered_res = [];
        asort($dce_items, SORT_NATURAL);
        foreach ($dce_items as $index_file => $one_file) {
            $base_path = substr($one_file, 0, strrpos($one_file, '/'));
            $arbo = explode('/', rtrim($base_path, '/'));
            if (strrpos($one_file, '/')) {// fichier dans un ré&pertoire
                $name = substr($one_file, strrpos($one_file, '/') + 1, strlen((string) $one_file));
                $base = substr($one_file, 0, strrpos($one_file, '/') + 1);
            } else {// fichier à la racine
                $name = $one_file;
                $base = $one_file;
            }
            $taille = $dce_items_size[$index_file];
            // on ajoute l'élément à la fin du tableau ordered_res
            $count = count($arbo);
            $element = &$ordered_res;
            if ('/' === substr($one_file, strlen((string) $one_file) - 1, 1)) {
                for ($i = 0; $i < $count - 1; ++$i) {
                    if ($arbo[$i] && !isset($element[$arbo[$i]]['nom'])) {
                        $element[$arbo[$i]] = [
                            'id' => $this->encryption->cryptId($prefix.'_'.++$maxIndex),
                            'base' => rtrim($arbo[$i], '/'),
                            'nom' => $arbo[$i],
                            'type' => 'dossier',
                            'noeuds' => [], ];
                    }
                    $element = &$element[$arbo[$i]]['noeuds'];
                }
                $element[$arbo[$i]] = [
                    'id' => $this->encryption->cryptId($prefix.'_'.$index_file),
                    'base' => rtrim($base, '/'),
                    'nom' => $arbo[$i],
                    'type' => 'dossier',
                    'noeuds' => [], ];
            } else {
                if ($count > 0 && !empty($arbo[0])) {
                    for ($i = 0; $i < $count; ++$i) {
                        if ($arbo[$i] && !isset($element[$arbo[$i]]['nom'])) {
                            $element[$arbo[$i]] = [
                                'id' => $this->encryption->cryptId($prefix.'_'.++$maxIndex),
                                'base' => rtrim($base, '/'),
                                'nom' => $arbo[$i],
                                'type' => 'dossier',
                                'noeuds' => [], ];
                        }
                        $element = &$element[$arbo[$i]]['noeuds'];
                    }
                }
                $file = $base.$name;
                if ($base === $name) {
                    $file = $base;
                }

                $extension = $this->getFileExtension($name);
                $element[$name] = [
                    'id' => $this->encryption->cryptId($prefix.'_'.$index_file),
                    'fichier' => $this->encryption->cryptId($file),
                    'base' => $base,
                    'nom' => $name,
                    'extension' => $extension,
                    'poids' => $taille,
                    'type' => 'fichier', ];
            }
        }

        return $ordered_res;
    }

    public function getInfoFilesZip($filePath, &$i)
    {
        $zip_items = [];
        $zip = (new ZipArchive);
        $result = $zip->open($filePath);

        if ($result === true) {
            for ($i = 0; $entry = $zip->statIndex($i); $i++) {
                $zip_items['chemins'][$i] = $entry['name'];
                $zip_items['tailles'][$i] = $entry['size'];
            }
            $zip->close();
        }

        return $zip_items;
    }

    /**
     * @return int
     */
    public function getCountFilesZip(string $filePath)
    {
        if ('' === $filePath) {
            return 0;
        }
        $zip = (new ZipArchive);
        $result = $zip->open($filePath);
        $index = 0;

        if ($result === true) {
            for ($i = 0; $entry = $zip->statIndex($i); $i++) {
                //si ce n'est pas un dossier
                if ($entry['size'] > 0) {
                    ++$index;
                }
            }
            $zip->close();
        }

        return $index;
    }

    protected function getFileExtension(string $fileName): false|string
    {
        return substr(strrchr($fileName, '.'), 1);
    }
}
