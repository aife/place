<?php

namespace App\Utils;

use DateTime;
use Exception;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

/**
 * Class Utils.
 */
class Utils
{
    /**
     * @param $date
     * @param string $format
     *
     * @return bool
     */
    public function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);

        return $d && $d->format($format) == $date;
    }

    /**
     * returns a flat array.
     *
     * @param $array
     */
    public function flatArray($array): array
    {
        $flatArray = [];
        $flatArray[] = 0;
        foreach ($array as $key => $value) {
            $flatArray[] = $value['id'];
        }

        return $flatArray;
    }

    /**
     * Cette fonctionne permet de transformer les données récupérées de la table GeolocalisationN2 en tableau de choix
     * pour le formulaire LieuxExecutionType.
     *
     * @param $list
     */
    public function transformeDataLieuxExecutionIntoArray($lieuxExecution): array
    {
        $lieuxExecutionTransformed = [];
        try {
            if (!empty($lieuxExecution)) {
                foreach ($lieuxExecution as $key => $lieu) {
                    $lieuxExecutionTransformed[$lieu['denomination1']] = $lieu['id'] . '-' . $lieu['denomination2'];
                }
            }
        } catch (Exception) {
            throw new Exception('Class Utils.php Ligne 56 : les données ne sont pas correctes');
        }

        return $lieuxExecutionTransformed;
    }


    /**
     * Permet de determiner le systeme d'exploitation du visiteur du site.
     */
    public function getOsVisiteur()
    {
        $listeOs = ['Windows', 'Mac', 'Linux', 'FreeBSD', 'SunOS', 'IRIX', 'BeOS', 'OS/2', 'AIX'];
        foreach ($listeOs as $os) {
            if (stripos($_SERVER['HTTP_USER_AGENT'], $os)) {
                return $os;
            }
        }

        return 'Autre';
    }

    /**
     * Permet de recuperer les informations sur le navigateur utilise.
     *
     * @return array
     */
    public function getInfosBrowserVisiteur()
    {
        $ub = null;
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version = 'Unknown';

        //First get the platform?
        if (preg_match('/linux/i', (string) $u_agent)) {
            $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', (string) $u_agent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', (string) $u_agent)) {
            $platform = 'windows';
        }

        // Next get the name of the useragent yes seperately and for good reason
        if (
            (preg_match('/MSIE/i', (string) $u_agent)
                && !preg_match('/Opera/i', (string) $u_agent))
            || (preg_match('/Trident.*rv[ :]*(\d+\.\d+)/', (string) $u_agent, $matches))
        ) {
            $bname = 'Internet Explorer';
            $ub = 'MSIE';
        } elseif (preg_match('/Firefox/i', (string) $u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = 'Firefox';
        } elseif (preg_match('/Chrome/i', (string) $u_agent)) {
            $bname = 'Google Chrome';
            $ub = 'Chrome';
        } elseif (preg_match('/Safari/i', (string) $u_agent)) {
            $bname = 'Apple Safari';
            $ub = 'Safari';
        } elseif (preg_match('/Opera/i', (string) $u_agent)) {
            $bname = 'Opera';
            $ub = 'Opera';
        } elseif (preg_match('/Netscape/i', (string) $u_agent)) {
            $bname = 'Netscape';
            $ub = 'Netscape';
        }

        // finally get the correct version number
        $known = ['Version', $ub, 'other'];
        $pattern = '#(?<browser>' . join('|', $known) .
            ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, (string) $u_agent, $matches)) {
            // we have no matching number just continue
        }

        // see how many we have
        $i = is_countable($matches['browser']) ? count($matches['browser']) : 0;
        if (1 != $i) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent, 'Version') < strripos($u_agent, (string) $ub)) {
                $version = $matches['version'][0];
            } else {
                $version = $matches['version'][1];
            }
        } else {
            $version = $matches['version'][0];
        }

        //Cas particulier : IE11 Windows 8 (voir http://www.javascriptkit.com/javatutors/navigator.shtml)
        if ((preg_match('/Trident.*rv[ :]*(\d+\.\d+)/', (string) $u_agent, $matches))) {
            $version = $matches[1];
        } elseif (stripos($u_agent, 'Trident/7.0;')) {
            // IE11 in compatibility mode
            $version = '11.0';
        } elseif (stripos($u_agent, 'Trident/6.0;')) {
            // IE10 in compatibility mode
            $version = '10.0';
        } elseif (stripos($u_agent, 'Trident/5.0;')) {
            // IE9 in compatibility mode
            $version = '9.0';
        } elseif (stripos($u_agent, 'Trident/4.0;')) {
            // IE8 in compatibility mode
            $version = '8.0';
        }
        // check if we have a number
        if (
            null == $version || '' ==
            $version
        ) {
            $version = '?';
        }

        return [
            'userAgent' => $u_agent,
            'name' => $bname,
            'version' => $version,
            'platform' => $platform,
            'pattern' => $pattern,
        ];
    }


    public function atexoHtmlEntities($str)
    {
        return htmlspecialchars($str, ENT_NOQUOTES | ENT_QUOTES, 'utf-8');
    }

    /**
     * Permet d'encoder un tableau en utf8 de facon recursive.
     *
     * @param $array
     *
     * @return mixed
     */
    public function utf8Converter($array)
    {
        array_walk_recursive($array, function (&$item, $key) {
            if (!mb_detect_encoding($item, 'utf-8', true)) {
                $item = utf8_encode($item);
            }
        });

        return $array;
    }

    public static function truncateHTMLContents(string $html, int $maxLength = 100): string
    {
        mb_internal_encoding('UTF-8');
        $printedLength = 0;
        $position = 0;
        $tags = [];
        $newContent = '';

        $html = preg_replace("/<img[^>]+\>/i", '', $html);

        while (
            $printedLength < $maxLength
            && preg_match('{</?([a-z]+)[^>]*>|&#?[a-zA-Z0-9]+;}', (string) $html, $match, PREG_OFFSET_CAPTURE, $position)
        ) {
            [$tag, $tagPosition] = $match[0];
            // Print text leading up to the tag.
            $str = mb_strcut($html, $position, $tagPosition - $position);
            if ($printedLength + mb_strlen($str) > $maxLength) {
                $newstr = mb_strcut($str, 0, $maxLength - $printedLength);
                $newstr = preg_replace('~\s+\S+$~', '', $newstr);
                $newContent .= $newstr;
                $printedLength = $maxLength;
                break;
            }
            $newContent .= $str;
            $printedLength += mb_strlen($str);
            if ('&' == $tag[0]) {
                // Handle the entity.
                $newContent .= $tag;
                ++$printedLength;
            } else {
                // Handle the tag.
                $tagName = $match[1][0];
                if ('/' == $tag[1]) {
                    // This is a closing tag.
                    $openingTag = array_pop($tags);
                    assert($openingTag == $tagName); // check that tags are properly nested.
                    $newContent .= $tag;
                } elseif ('/' == $tag[mb_strlen($tag) - 2]) {
                    // Self-closing tag.
                    $newContent .= $tag;
                } else {
                    // Opening tag.
                    $newContent .= $tag;
                    if ($tagName != 'br') {
                        $tags[] = $tagName;
                    }
                }
            }

            // Continue after the tag.
            $position = $tagPosition + mb_strlen($tag);
        }

        // Print any remaining text.
        if ($printedLength < $maxLength && $position < mb_strlen($html)) {
            $newstr = mb_strcut($html, $position, $maxLength - $printedLength);
            $newstr = preg_replace('~\s+\S+$~', '', $newstr);
            $newContent .= $newstr;
        }

        // Close any open tags.
        while (!empty($tags)) {
            $newContent .= sprintf('</%s>', array_pop($tags));
        }

        return $newContent;
    }

    public function convertToCamelCase(string $code): string
    {
        return ucfirst((new CamelCaseToSnakeCaseNameConverter())->denormalize($code));
    }

    public function getGetterMethodName($code, $className): string
    {
        if (method_exists($className, 'is' . $this->convertToCamelCase($code))) {
            return 'is' . $this->convertToCamelCase($code);
        }

        return 'get' . $this->convertToCamelCase($code);
    }
}
