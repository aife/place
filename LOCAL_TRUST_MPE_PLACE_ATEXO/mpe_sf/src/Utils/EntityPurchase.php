<?php

namespace App\Utils;

use PDO;
use App\Entity\AffiliationService;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Service\AtexoConfiguration;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class EntityPurchase.
 */
class EntityPurchase
{
    public function __construct(private readonly EntityManagerInterface $em, private readonly AtexoConfiguration $atexoConfiguration)
    {
    }

    /**
     * Retourne le service passé en paramétre et la liste de ses fils sous la forme : (service1, service2,...).
     *
     * @param $idService : l'ID du service
     * @param $org Organisme
     * @param bool $returnArray Organisme
     *
     * @return array|string (service1, service2,...)
     */
    public function retrieveAllChildrenServices($idService, $org, $returnArray = false): array|string
    {
        $arrayChilds = [];
        if ($idService) {
            $arrayChilds = $this->getSubServices($idService, $org, true);
        } else {
            $services = $this->getCachedEntities($org);
            if (!$idService) {
                $arrayChilds[] = '0';
            }
            if ($services) {
                //$service = "";
                foreach ($services as $id => $service) {
                    $arrayChilds[] = $id;
                }
            }
        }

        if ($returnArray) {
            return $arrayChilds;
        }

        $stringIdsOfServicesWithPermission = '';
        if ($arrayChilds) {
            $stringIdsOfServicesWithPermission = ' ( ';
            $indice = 0;
            foreach ($arrayChilds as $id) {
                $stringIdsOfServicesWithPermission .= (($indice > 0) ? ',' : '') . $id;
                ++$indice;
            }

            $stringIdsOfServicesWithPermission .= ' ) ';
        }

        return $stringIdsOfServicesWithPermission;
    }

    /**
     * @param $idServiceAgent
     * @param $org
     * @param bool $justIds
     * @param bool $withLibelle
     *
     * @return array|bool|mixed
     */
    public function getSubServices($idServiceAgent, $org, $justIds = false, $withLibelle = false)
    {
        $serviceAgent = $this->retrieveEntityById($idServiceAgent, $org);
        if ($serviceAgent) {
            $result = [];
            $this->getAllChilds($serviceAgent->getId(), $result, $org);
            if ($justIds) {
                $idsArray = [];
                foreach ($result as $service) {
                    $idsArray[] = $service->getId();
                }

                return $idsArray;
            }

            return self::getAllChildsWithPath($result, $org, $withLibelle);
        } else {
            return false;
        }
    }

    /**
     * retourne l'objet service é partir de l'id passé en paramétre.
     *
     * @param  int $idService id du service recherché
     * @param string $org Organismes
     *
     * @return $service
     */
    public function retrieveEntityById($idService, $org)
    {
        $entityPurchase = $this->getCachedEntities($org);
        if (isset($entityPurchase[$idService]) && $entityPurchase[$idService] instanceof Service) {
            return $entityPurchase[$idService];
        } else {
            return false;
        }
    }

    /**
     * méthode qui permet de récupérer la liste des entités d'achat d'un organisme donné en parmètre
     * le tri se fait sur le chemin complet asc.
     *
     * @param string $organisme
     *
     * @return array
     */
    public function retrieveEntities($organisme)
    {
        $allServices = [];
        $entityPurchase = $this->em->getRepository(Service::class)
            ->findBy(['organisme' => $organisme], ['cheminComplet' => 'asc']);

        foreach ($entityPurchase as $oneEntity) {
            $allServices[$oneEntity->getId()] = $oneEntity;
        }

        return $allServices;
    }

    /**
     * retourne la liste des entités d'achat mises en cache.
     *
     * @param string $organisme
     *
     * @return array
     */
    public function getCachedEntities($organisme)
    {
        /*
         * @todo code en commentaire à reprendre !
         */
        //si c'est un agent qui est connecté et  qu'il demande l
        //es services de son organisme, on va les chercher dans la session
        //ce mécanisme est mis en place pour prendre en compte le
        // fait que l'on peut avoir plusieurs frontaux web
        //dans ce cas, il faut qu'il y ait une consistance des données dans
        // le cas ou l'agent créé des entités et qu'il bascule sur un autre frontal qu'il les retrouve bien
        //en effet, le cache APC est en mémoire et n'est pas partagé entre les frontaux
        /*if (( Atexo_CurrentUser::isAgent() || Atexo_CurrentUser::isAgentSocle() )
        && Atexo_CurrentUser::isConnected() && Atexo_CurrentUser::getOrganismAcronym()==$organisme){
            //d'abord on récupère les services de la session
            $allServices = Atexo_CurrentUser::readFromSession("cachedServices");

            //si les services ne sont pas dans la session, on va les récupérer de la BD
            et le mettre en cache dans la session de l'utilisateur
            if(!$allServices || !is_array($allServices)) {
                $allServices = self::retrieveEntities($organisme);
                Atexo_CurrentUser::writeToSession("cachedServices", $allServices);
            }
            return $allServices;
        }*/

        return $this->retrieveEntities($organisme);
    }

    /**
     * retourne un tableau ordonné de tous les fils.
     *
     * @param $idPurchaseEntity Service Id
     * @param $arrayChilds output array
     * @param $org String Acronyme de l'organisme
     *
     * @return void
     */
    public function getAllChilds($idPurchaseEntity, &$arrayChilds, $org)
    {
        if (is_array($arrayChilds)) {
            $levelOneChilds = $this->getLevelOneChilds($idPurchaseEntity, $org);
            $parents = $this->getArParentsId($idPurchaseEntity, $org);
            $depth = count($parents);
            if (0 != $idPurchaseEntity) {
                $purchaseEntity = $this->em->getRepository(Service::class)
                    ->findOneBy(['id' => $idPurchaseEntity, 'organisme' => $org]);
                $purchaseEntity->setDepth($depth);
                $arrayChilds[] = $purchaseEntity;
            }
            if (is_array($levelOneChilds) && 0 != count($levelOneChilds)) {
                foreach ($levelOneChilds as $child) {
                    $this->getAllChilds($child->getId(), $arrayChilds, $org);
                }
            }
        }

        return $arrayChilds;
    }

    /**
     * retourne un tableau des entités filles de l'entité passée en paramétre
     * n'y en a pas.
     *
     * @param $idPurchaseEntity Service Id
     * @param $org Acronyme de l'organisme'
     *
     * @return (id, libelle)
     */
    public function getLevelOneChilds($idPurchaseEntity, $org)
    {
        $resServices = [];
        if (0 === $idPurchaseEntity) {
            // Récupération des fils niveau 1
            $resServices = $this->getLevelOneServices($org);
        } else {
            $resServices = $this->em->getRepository(Service::class)
                ->getAllServicesByIdPole($idPurchaseEntity, $org);
        }

        return $resServices;
    }

    /**
     * retourne le des services de niveau 1 lié directement a l'organisme racine (id=0).
     *
     * @param $org la connexion
     *
     * @return Service[]
     */
    public function getLevelOneServices($org)
    {
        $arrayServices = $this->em->getRepository(Service::class)->findBySigle($org);
        $result = [];
        foreach ($arrayServices as $service) {
            if (!$this->hasParent($service->getId(), $org)) {
                $result[] = $service;
            }
        }

        return $result;
    }

    /**
     * retourne l'id du service parent ou NULL s'il
     * n'y en a pas.
     *
     * @param $serviceId
     * @param $org
     *
     * @return (id, libelle)
     */
    public function getParent($serviceId, $org)
    {
        $idPole = false;
        $result = $this->em->getRepository(AffiliationService::class)
            ->getServiceParent($serviceId, $org);
        if ((is_countable($result) ? count($result) : 0) > 0) {
            if (array_key_exists('service_parent_id', $result[0])) {
                $idPole = $result[0]['service_parent_id'];
            }
        }

        return $idPole;
    }

    /**
     * retourne un tableau ordonné de tous les parents
     * du service $service.
     *
     * @param $serviceId
     * @param $org
     *
     * @return array(id, libelle)
     */
    public function getAllParents($serviceId, $org)
    {
        $listParent = [];
        $idParent = $this->getParent($serviceId, $org);
        $index = 0;

        while ($idParent) {
            $entityParent = $this->retrieveEntityById($idParent, $org);
            if ($entityParent) {
                $listParent[$index]['id'] = $idParent;
                $listParent[$index]['libelle'] = $entityParent->getSigle();
            }
            ++$index;
            $idParent = $this->getParent($idParent, $org);
        }

        $listParent[$index]['id'] = 0;
        $listParent[$index]['libelle'] = '';

        return $listParent;
    }

    /**
     * retourne un tableau ordonné des id des parents
     * du service $serviceId.
     *
     * @param int  $serviceId
     * @param bool $includeCurrService si true, le service $serviceId est inclus dans le résultat
     *
     * @return array(id)
     */
    public function getArParentsId($serviceId, $org, $includeCurrService = false)
    {
        $listParent = [];
        $idParent = $this->getParent($serviceId, $org);
        while ($idParent && !in_array($idParent, $listParent)) {
            $listParent[] = $idParent;
            $idParent = $this->getParent($idParent, $org);
        }
        $listParent[] = 0;

        if ($includeCurrService) {
            if (!in_array($serviceId, $listParent)) {
                $listParent = [...[
                    $serviceId,
                ], ...$listParent];
            }
        }

        return $listParent;
    }

    public function getPathEntityByIdAgent($id, $organisme, $langDefault, $langue = '')
    {
        $res = '-';
        if (0 != $id) {
            $entity = self::retrieveEntityById($id, $organisme);
            if ($entity) {
                $res = $entity;
            }
        } else {
            $organismeO = $this->em
                ->getRepository(Organisme::class)
                ->findOneBy(['acronyme' => $organisme]);
            if ($organismeO instanceof Organisme) {
                if ($langue) {
                    $langue = ucfirst($langue);
                }
                $getDenomOrg = 'getDenominationOrg' . $langue;
                if (0 == strcmp($langue, $langDefault)) {
                    $res = $organismeO->getSigle() . ' - ' . $organismeO->getDenominationOrg();
                } else {
                    $res = $organismeO->getSigle() . ' - ' . $organismeO->$getDenomOrg();
                }
            } else {
                $res = '-';
            }
        }

        return $res;
    }

    /** ***************************************************************
     * ATTENTION : Classe en cours de migration
     * Toute les classe ci-dessous ne sont pas encore migret en SF !
     ****************************************************************** */

    /**
     * Retourne la liste des entités d'achat.
     *
     * @param bool $organisme si false on récupére toutes les entités d'achats de l'organisme de l'agent connecté,
     *                        sinon elle retourne les entités d'achats de l'organisme passé en paramétre
     * @param bool $withPath  si true on affiche le chemin complet du service
     *
     * @return array
     */
    public static function getEntityPurchase($organisme, $withPath = null, $langue = null)
    {
        $entityPurchaseArray = [];
        $entityPurchase = self::getCachedEntities($organisme);
        if (is_array($entityPurchase)) {
            foreach ($entityPurchase as $entity) {
                if (true == $withPath) {
                    $entityPurchaseArray[$entity->getId()] = $entity->getPathServiceTraduit();
                } else {
                    $entityPurchaseArray[$entity->getId()] = $entity->getSigle();
                }
            }
        }
        $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
        $getDenominationOrg = 'getDenominationOrg' . Atexo_Languages::getLanguageAbbreviation($langue);
        if ($organismeO instanceof CommonOrganisme) {
            if (
                0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                || !$organismeO->$getDenominationOrg()
            ) {
                $entityPurchaseArray = Atexo_Util::arrayUnshift(
                    $entityPurchaseArray,
                    $organismeO->getSigle() . ' - ' . $organismeO->getDenominationOrg()
                );
            } else {
                $entityPurchaseArray = Atexo_Util::arrayUnshift(
                    $entityPurchaseArray,
                    $organismeO->getSigle() . ' - ' . $organismeO->$getDenominationOrg()
                );
            }
        }

        return $entityPurchaseArray;
    }

    /**
     * teste si entity1 est père direct de entity2.
     *
     * @param int $idEntity1 l'id de l'entité 1
     * @param int $idEntity2 l'id de l'entité 2
     *
     * @deprecated
     */
    public static function isDirectParent($idEntity1, $idEntity2)
    {
        $c3 = new Criteria();
        $c3->add(CommonAffiliationServicePeer::ID_POLE, $idEntity1, CRITERIA::EQUAL);
        $c3->addJoin(CommonAffiliationServicePeer::ID_POLE, CommonServicePeer::ID);
        $servicesAffectes = CommonAffiliationServicePeer::doSelect($c3);

        if (is_array($servicesAffectes)) {
            if (in_array($idEntity2, $servicesAffectes)) {
                return true;
            }
        } else {
            // Pas de service affecté au péle
            return false;
        }
    }

    /**
     * teste si entity1 est un enfant direct de entity2.
     *
     * @param int $idEntity1 l'id de l'entité 1
     * @param int $idEntity2 l'id de l'entité 2
     *
     * @deprecated
     */
    public static function isDirectChild($idEntity1, $idEntity2)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $c1 = new Criteria();
        $c1->add(CommonAffiliationServicePeer::ID_POLE, $idEntity1, CRITERIA::EQUAL);
        $c1->add(CommonAffiliationServicePeer::ORGANISME, $organisme, CRITERIA::EQUAL);
        $c1->addJoin(CommonAffiliationServicePeer::ID_SERVICE, CommonServicePeer::ID);
        $polesService = CommonServicePeer::doSelect($c1, $connexion);
        if (is_array($polesService)) {
            if (in_array($idEntity2, $polesService)) {
                return true;
            }
        } else {
            return false;
        }
    }

    public function hasParent($service, $org)
    {
        if ($this->getParent($service, $org)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * retourne le libelle de l'entite d'achat ou - pour l'entite vide.
     *
     * @param $service objet de la classe Service
     *
     * @return string|-
     */
    public static function retrieveLibelleService($service)
    {
        if ($service && $service instanceof CommonService) {
            return $service->getSigle();
        }

        return Atexo_Config::getParameter('VOID_ENTITY_PURCHASE_VALUE');
    }

    /**
     * retourne le nom service en fonction de l'id du service.
     *
     * @param int    $idService l'id du service
     * @param string $org       acronyme de l'organisme
     *
     * @return string le nom du service
     */
    public static function getServiceAgentById($idService, $org)
    {
        $entityPurchase = self::getCachedEntities($org);
        if ($entityPurchase[$idService] instanceof CommonService) {
            return $entityPurchase[$idService]->getSigle();
        } else {
            return false;
        }
    }

    public static function getLibelleServiceAgentById($idService, $org)
    {
        $entityPurchase = self::getCachedEntities($org);
        if ($entityPurchase[$idService] instanceof CommonService) {
            return $entityPurchase[$idService]->getLibelle();
        } else {
            return false;
        }
    }

    /**
     * retourne le nom service en fonction de l'id du service.
     *
     * @param int $idService l'id du service
     *
     * @return string le nom du service
     */
    public static function getServiceById($idService, $org)
    {
        if (0 != $idService) {
            return self::getServiceAgentById($idService, $org);
        } else {
            $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($org);

            return $organismeO->getSigle();
        }
    }

    /**
     * retourne un tableau de fils.
     *
     * @param array $listChilds
     * @param $org
     */
    public static function getAllChildsWithPath($listChilds, $org, $withLibelle = false)
    {
        $childArrayWithPath = [];
        array_shift($listChilds);
        if (is_array($listChilds)) {
            foreach ($listChilds as $child) {
                $childArrayWithPath['id'] = $child->getId();
                $childArrayWithPath['libelle'] = self :: getServiceById($child->getId(), $org);
                $parents = self :: getAllParents($child->getId(), $org);
                $childArrayWithPath['path'][$child->getId()] =
                    Atexo_Util :: implodeArrayWithSeparator(
                        $childArrayWithPath['libelle'],
                        $parents,
                        ' / '
                    )
                    . (($withLibelle) ? ' - ' . self::getLibelleServiceAgentById($child->getId(), $org) : '');
            }
        }

        return $childArrayWithPath['path'];
    }

    /**
     * retourne true si $idService2 est un descendant de $idService1.
     *
     * @param $idService1 id du parent
     * @param $idService2 id du descendant
     */
    public static function isParent($idService1, $idService2, $organisme): bool
    {
        $arParentsId = self::getArParentsId($idService2, $organisme);

        return in_array($idService1, $arParentsId);
    }

    /**
     * Méthode qui permet d'invalider le cache et de recharger la liste
     * des entités d'achat pour l'organisme passé en paramètre.
     *
     * @param $organisme
     */
    public function reloadCachedEntities($organisme)
    {
        //si c'est un agent qui est connecté, il faut supprimer les services qui sont stockés en session
        if (
            (Atexo_CurrentUser::isAgent() || Atexo_CurrentUser::isAgentSocle())
            && Atexo_CurrentUser::isConnected()
            && Atexo_CurrentUser::getOrganismAcronym() == $organisme
        ) {
            Atexo_CurrentUser::deleteFromSession('cachedServices');
        }
        self :: getCachedEntities($organisme);
    }

    /**
     * retourne le chemin de l'entité d'achat.
     */
    public function getEntityPath($reference, $organisme)
    {
        $entityId = null;
        $entityPath = null;
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $consultation = CommonConsultationPeer::retrieveByPK($reference, $organisme, $connexion);
        if ($consultation) {
            $entityId = $consultation->getServiceId();
        }
        $entity = CommonServicePeer::retrieveByPk($entityId, $organisme, $connexion);
        $parents = self::getAllParents($entityId, $organisme);
        if ($parents && $entity) {
            $entityPath = Atexo_Util :: implodeArrayWithSeparator($entity->getSigle(), $parents, ' / ');
        }

        return $entityPath;
    }

    /**
     * retourne le libellé de l'entité d'achat.
     */
    public function getEntityLibelle($idService, $organisme, $langue = null)
    {
        if ($idService) {
            $entityId = $idService;
            if (0 == $entityId) {
                return '';
            }

            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $entity = CommonServicePeer::retrieveByPk($entityId, $organisme, $connexion);
            if ($entity) {
                if (!$langue) {
                    return $entity->getLibelle();
                } else {
                    $getLibelleTraduit = 'getLibelle' . Atexo_Languages::getLanguageAbbreviation($langue);
                    if (
                        0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                        || !$entity->$getLibelleTraduit()
                    ) {
                        return $entity->getLibelle();
                    } else {
                        return $entity->$getLibelleTraduit();
                    }
                }
            }
        }

        return '';
    }

    /**
     *  retourne le chemin de l'entité publique.
     */
    public function getEntityPathById($idEntity, $organisme, $withLibelle = false, $global = false)
    {
        $entityPath = null;
        $orgSigle = null;
        $organismeObj = $this->em
            ->getRepository(Organisme::class)
            ->findOneBy(['acronyme' => $organisme]);
        if (empty($idEntity)) {
            if ($organismeObj) {
                return $organismeObj->getSigle().' - '.$organismeObj->getDenominationOrg();
            }
        } else {
            $entity = $this->em
                ->getRepository(Service::class)
                ->findOneBy([
                    'id'=>$idEntity,
                    'organisme' => $organisme
                ]);
            $parents = self:: getAllParents($entity->getId(), $organisme);
            if ($parents) {
                $countArray = count($parents);
                $index = 0;
                foreach ($parents as $oneElement) {
                    if ($oneElement['libelle'] && $index < $countArray) {
                        $NewSeparateor = ' / ';
                    } else {
                        $NewSeparateor = '';
                    }
                    $entityPath = $oneElement['libelle'] . $NewSeparateor . $entityPath;
                    ++$index;
                }
            }
            if ($entityPath) {
                if (true == $global) {
                    $orgSigle = $organismeObj->getSigle();
                }
                if ($withLibelle) {
                    return $orgSigle.' / '.$entityPath.' - '.$entity->getLibelle();
                } else {
                    return $orgSigle.' / '.$entityPath;
                }
            } else {
                return '-';
            }
        }
    }

    /**
     * retourne la liste des entités d'achat.
     *
     * @param string $organisme
     *
     * @return array
     */
    public static function retrieveEntityPurchaseByOrganisme($organisme, $withPath = null, $langue = null)
    {
        return self::getEntityPurchase($organisme, $withPath, $langue);
    }

    public static function getPathEntityById($id, $organisme, $langue = null)
    {
        if (0 != $id) {
            $entity = self::retrieveEntityById($id, $organisme);
            if ($entity) {
                return $entity->getPathServiceTraduit();
            }
        } else {
            $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
            if ($organismeO instanceof CommonOrganisme) {
                $getDenominationOrg = 'getDenominationOrg' . Atexo_Languages::getLanguageAbbreviation($langue);
                if (
                    0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                    || !$organismeO->$getDenominationOrg()
                ) {
                    return $organismeO->getSigle() . ' - ' . $organismeO->getDenominationOrg();
                } else {
                    return $organismeO->getSigle() . ' - ' . $organismeO->$getDenominationOrg();
                }
            } else {
                return '-';
            }
        }
    }

    public function getSigleLibelleEntityById(?int $id, string $organisme): string
    {
        if (null !== $id) {
            $entity = self::retrieveEntityById($id, $organisme);
            if ($entity) {
                return $entity->getSigle() . ' - ' . $entity->getLibelle();
            }
        } else {
            $organismeO = $this->em->getRepository(Organisme::class)->findByAcronymeOrganisme($organisme);
            if ($organismeO) {
                return $organismeO->getSigle() . ' - ' . $organismeO->getDenominationOrg();
            }
        }

        return '';
    }

    public static function getEntitePublicEntityAchatById($id, $organisme = null)
    {
        if ($organisme) {
            $organismeAcronyme = $organisme;
        } else {
            $organismeAcronyme = Atexo_CurrentUser::getOrganismAcronym();
        }
        $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($organismeAcronyme);

        if (0 != $id) {
            $entity = self::retrieveEntityById($id, $organismeAcronyme);
            if ($entity) {
                return $organismeO->getSigle() . ' - ' . $organismeO->getDenominationOrg()
                    . '<br> ' . $entity->getSigle() . ' - ' . $entity->getLibelle();
            }
        } else {
            return $organismeO->getSigle() . ' - ' . $organismeO->getDenominationOrg();
        }
    }

    /**
     * retourne tout les sous services d'un autre.
     *
     * @param int $idServiceAgent l'id de l'agent
     *
     * @return la liste des sous services avec leurs chemins
     */
    public static function getListSubServices($idServiceAgent, $org)
    {
        $serviceAgent = self::retrieveEntityById($idServiceAgent, $org);
        if ($serviceAgent) {
            $result = [];
            self::getAllChilds($serviceAgent->getId(), $result, $org);

            return self::getAllChildsWithPaths($result, $org);
        } else {
            return false;
        }
    }

    /**
     * retourne un tableau de fils.
     *
     * @param array $listChilds
     * @param $org
     */
    public static function getAllChildsWithPaths($listChilds, $org)
    {
        $childArrayWithPath = [];
        if (is_array($listChilds)) {
            foreach ($listChilds as $child) {
                $childArrayWithPath['id'] = $child->getId();
                $childArrayWithPath['libelle'] = self :: getServiceById($child->getId(), $org);
                $parents = self :: getAllParents($child->getId(), $org);
                $childArrayWithPath['path'][$child->getId()] = Atexo_Util :: implodeArrayWithSeparator(
                    $childArrayWithPath['libelle'],
                    $parents,
                    ' / '
                );
            }
        }

        return $childArrayWithPath['path'];
    }

    public function getAllServices($organisme)
    {
        return self::getCachedEntities($organisme);
    }

    /**
     * retourne la denimination de l'organisme sinon false.
     *
     * @param $idEntity l'id du service
     * @param $organisme l'acronyme de l'organisme
     *
     * @return Service : $entity
     */
    public function getEntityById($idEntity, $organisme)
    {
        if ($idEntity) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $c = new Criteria();
            $c->add(CommonServicePeer::ID, $idEntity, CRITERIA::EQUAL);
            $c->add(CommonServicePeer:: ORGANISME, $organisme, CRITERIA::EQUAL);
            $entity = CommonServicePeer::doSelectOne($c, $connexion);

            return $entity;
        } else {
            return Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
        }
    }

    public static function deleteEntity($idEntity, $organisme)
    {
        $entity = self::getEntityById($idEntity, $organisme);
        if ($entity && $entity instanceof CommonService) {
            $consultations = Atexo_Consultation::retrieveConsultationsByService($idEntity, $organisme);
            $agents = Atexo_Agent::retriveServiceAgents($organisme, $idEntity);
            $childs = self::getSubServices($idEntity, $organisme);
            $rpas = Atexo_EntityPurchase_RPA::getRpaByIdService($idEntity, $organisme);
            if (
                ($consultations && (is_countable($consultations) ? count($consultations) : 0))
                || ($agents && (is_countable($agents) ? count($agents) : 0))
                || ($childs && (is_countable($childs) ? count($childs) : 0))
                || ($rpas && (is_countable($rpas) ? count($rpas) : 0))
            ) {
                return false;
            } else {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $entity->delete($connexion);

                return true;
            }
        } else {
            return false;
        }
    }

    public function hasChild($idEntity, $organisme)
    {
        return (self::getLevelOneChilds($idEntity, $organisme)) ? true : false;
    }

    /**
     * Mettre les infos récupérer depuis le web service dans les champs de la table service du projet associé.
     *
     * @param string $serv, array $wsService
     */
    public function remplirService($serv, $wsService)
    {
        $serv->setIdExterne($wsService->id);
        $serv->setTypeService($wsService->type_service);
        $serv->setLibelle(utf8_decode($wsService->service));
        $serv->setSigle(utf8_decode($wsService->sigle));
        $serv->setPays('France');
    }

    /**
     * Récupération des service par id_intial.
     */
    public function retrieveEntityByIdInitial($idExterne, $org, $connexion)
    {
        $cr = new Criteria();
        $cr->add(CommonServicePeer::ID_EXTERNE, $idExterne);
        $cr->add(CommonServicePeer::ORGANISME, $org);
        $service = CommonServicePeer::doSelectOne($cr, $connexion);

        return $service;
    }

    /**
     * Retourne la liste des entités d'achat avec uniquement les identifiants.
     *
     * @param $organisme organisme sur lequel porte la recherche
     *
     * @return array
     */
    public static function getEntityPurchaseById($organisme)
    {
        $entityPurchaseArray = [];
        $entityPurchase = self::getCachedEntities($organisme);
        if (is_array($entityPurchase)) {
            foreach ($entityPurchase as $entity) {
                $entityPurchaseArray[] = $entity->getId();
            }

            return $entityPurchaseArray;
        } else {
            return '';
        }
    }

    public static function getAllServicesAndPoles($org, $dateModif = null, $objOrganisme = null)
    {
        $sql = "select * from Service left join AffiliationService 
            on Service.id=AffiliationService.service_id 
            AND Service.organisme=AffiliationService.organisme 
            WHERE Service.organisme='" . $org . "' ";
        if ($dateModif) {
            $sql .= " AND Service.date_modification >='" . (new Atexo_Db())->quote($dateModif) . "'";
        }
        $statement = Atexo_Db::getLinkCommon()->query($sql);
        $arrayServices = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $service = [];
            $service['id'] = $row['id'];
            $service['parentId'] = $row['service_parent_id'];
            $service['title'] = $row['libelle'];
            $service['acronym'] = $row['sigle'];
            $service['address'] = $row['adresse'] . ' ' . $row['adresse_suite'];
            $service['postalCode'] = $row['cp'];
            $service['city'] = $row['ville'];
            $service['phone'] = $row['telephone'];
            $service['fax'] = $row['fax'];
            $service['country'] = $row['pays'];
            $service['email'] = $row['mail'];
            $service['id_initial'] = $row['id_initial']; // pour le socle PPP
            $service['acronymOrganism'] = $org; // pour le socle PPP
            $service['siren'] = $row['siren']; // pour le socle PPP
            if ($objOrganisme) {
                $service['id_initial_organism'] = $objOrganisme->getIdInitial(); // pour le socle PPP
                $service['id_organism'] = $objOrganisme->getId(); // pour le socle MPE
                $service['denominationOrganism'] = $objOrganisme->getDenominationOrg(); // pour le socle MPE
                $service['sigleOrganism'] = $objOrganisme->getSigle(); // pour le socle MPE
            }
            $arrayServices[$org . '#' . $row['id']] = $service;
        }
        Atexo_Db::closeCommon();

        return $arrayServices;
    }

    /***
     * retourne le nombre des services de la plate forme
     */
    public function getCountAllServicesPortail()
    {
        $nbr = 0;
        $allOrganisme = Atexo_Organismes::retrieveOrganismes();
        foreach ($allOrganisme as $OneOrganisme) {
            $nbr = $nbr + self::getCountService($OneOrganisme->getAcronyme());
        }

        return $nbr;
    }

    /****
     * récupérer le path d'un service donné
     */
    public static function getPathServiceById($id, $organisme, $langue = null, $trimSigle = false)
    {
        $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
        $getLibelle = 'getLibelle' . Atexo_Languages::getLanguageAbbreviation($langue);
        if (0 != $id) {
            $entity = self::retrieveEntityById($id, $organisme);
            if ($entity) {
                $parents = self :: getAllParents($entity->getId(), $organisme);
                if ($parents) {
                    $sigleEntite = $entity->getSigle();
                    $sigleOrganisme = $organismeO->getSigle();

                    if ($trimSigle) {
                        $sigleEntite = str_replace(' - ', '-', $entity->getSigle());
                        $sigleOrganisme = str_replace(' - ', '-', $organismeO->getSigle());
                    }

                    $entityPath = Atexo_Util :: implodeArrayWithSeparator($sigleEntite, $parents, ' / ');
                    if ($langue) {
                        return $sigleOrganisme . ' / ' . $entityPath . ' - ' . $entity->$getLibelle();
                    } else {
                        return $sigleOrganisme . ' / ' . $entityPath . ' - ' . $entity->getLibelle();
                    }
                } else {
                    $sigleEntite = $entity->getSigle();
                    $sigleOrganisme = $organismeO->getSigle();

                    if ($trimSigle) {
                        $sigleEntite = str_replace(' - ', '-', $entity->getSigle());
                        $sigleOrganisme = str_replace(' - ', '-', $organismeO->getSigle());
                    }
                    if ($langue) {
                        return $sigleOrganisme . ' / ' . $sigleEntite . ' - ' . $entity->$getLibelle();
                    } else {
                        return $sigleOrganisme . ' / ' . $sigleEntite . ' - ' . $entity->getLibelle();
                    }
                }
            }
        } else {
            $sigleOrganisme = $organismeO->getSigle();

            if ($trimSigle) {
                $sigleOrganisme = str_replace(' - ', '-', $organismeO->getSigle());
            }
            if ($langue) {
                return $sigleOrganisme . ' - ' . $organismeO->getDenominationOrg();
            } else {
                $getDenominationOrg = 'getDenominationOrg' . Atexo_Languages::getLanguageAbbreviation($langue);

                return $sigleOrganisme . ' - ' . $organismeO->$getDenominationOrg();
            }
        }
    }

    /****
     * calcule le nombre des service pour chauque organisme donné
     * @retuen : le nombre de service(s)
     */
    public function getCountService($organisme = null)
    {
        $countService = null;
        $sql = "Select count(*) as nombreService FROM Service where Service.organisme='" . $organisme . "'";
        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        foreach ($statement as $row) {
            $countService = $row['nombreService'];
        }
        Atexo_Db::closeCommon();

        return $countService;
    }

    public function getAllServicesPlateForme($fromDate = null)
    {
        $arrayGlobal = [];
        $allOrganisme = (new Atexo_Organismes())->retrieveAllEntity();
        foreach ($allOrganisme as $OneOrganisme) {
            $services = null;
            $services = self::getAllServicesAndPoles($OneOrganisme->getAcronyme(), $fromDate, $OneOrganisme);
            if (is_array($services)) {
                $arrayGlobal = array_merge($arrayGlobal, $services);
            }
        }

        return $arrayGlobal;
    }

    public function isEntityAbleToValidate($idServiceValidateur, $idService, Organisme $organisme): bool
    {
        if ($idServiceValidateur == $idService) {
            return true;
        }

        if ($this->atexoConfiguration->isModuleEnabled('OrganisationCentralisee', $organisme, 'get')) {
            $listeEntitesParents = self::getAllParents($idServiceValidateur, $organisme->getAcronyme());
            if (is_array($listeEntitesParents)) {
                foreach ($listeEntitesParents as $unParent) {
                    if ($unParent['id'] == $idService) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function retrieveServiceByCodeAcheteur($siren, $organisme = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $cr = new Criteria();
        $cr->add(CommonServicePeer::SIREN, $siren, CRITERIA::EQUAL);
        if ($organisme) {
            $cr->add(CommonServicePeer::ORGANISME, $organisme, CRITERIA::EQUAL);
        }
        $service = CommonServicePeer::doSelect($cr, $connexion);

        return $service;
    }

    public function getPathByEntityAndOrganismeObject($entity, $OrganismeObjet)
    {
        $langue = Atexo_CurrentUser::readFromSession('lang');
        $getLibelle = 'getLibelle' . Atexo_Languages::getLanguageAbbreviation($langue);
        if ($entity instanceof Service) {
            $parents = self :: getAllParents($entity->getId(), $OrganismeObjet->getAcronyme());
            if ($parents) {
                $entityPath = Atexo_Util :: implodeArrayWithSeparator($entity->getSigle(), $parents, ' / ');
                if ($langue) {
                    return $OrganismeObjet->getSigle() . ' / ' . $entityPath . ' - ' . $entity->$getLibelle();
                } else {
                    return $OrganismeObjet->getSigle() . ' / ' . $entityPath . ' - ' . $entity->getLibelle();
                }
            } else {
                if ($langue) {
                    return $OrganismeObjet->getSigle() . ' / ' . $entity->getSigle() . ' - ' . $entity->$getLibelle();
                } else {
                    return $OrganismeObjet->getSigle() . ' / ' . $entity->getSigle() . ' - ' . $entity->getLibelle();
                }
            }
        } else {
            if ($langue) {
                return $OrganismeObjet->getSigle() . ' - ' . $OrganismeObjet->getDenominationOrg();
            } else {
                $getDenominationOrg = 'getDenominationOrg' . Atexo_Languages::getLanguageAbbreviation($langue);

                return $OrganismeObjet->getSigle() . ' - ' . $OrganismeObjet->$getDenominationOrg();
            }
        }
    }

    /**
     * Fonction de récupération du sigle d'une entité d'achat.
     *
     * @param $id de l'entité d'achat
     * @param $organisme de l'entité d'achat
     * return $sigle
     */
    public function getSigleByEntityId($id, $organisme)
    {
        $sigle = '';
        if ('0' == $id) {
            $sigle = $organisme;
        } else {
            if (self::retrieveEntityById($id, $organisme)) {
                $sigle = self::retrieveEntityById($id, $organisme)->getSigle();
            }
        }

        return $sigle;
    }

    /**
     * Permet de determiner l'état de l'activation du fuseau horaire d'un service.
     *
     * @param $id: l'identifiant du service
     * @param $organisme: l'organisme du service
     *
     * @return : true si le champ 'activation_fuseau_horaire' de
     * la table 'service' est é '1' pour ce service (passé en parametre), false sinon
     */
    public function getEtatActivationFuseauHoraire($idService, $organisme)
    {
        //Cas de l'organisme
        if (0 == $idService) {
            $objetOrganisme = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
            if ($objetOrganisme instanceof CommonOrganisme && '1' == $objetOrganisme->getActivationFuseauHoraire()) {
                return true;
            }
        } else {
            $service = self::getEntityById($idService, $organisme);
            if ($service instanceof CommonService && '1' == $service->getActivationFuseauHoraire()) {
                return true;
            }
        }

        return false;
    }

    /*
     * Permet de retourner le libelle de service by siret( siren+complement)
     */
    public function getLibelleServiceBySiret($siret, $org)
    {
        $libelle = '';
        $sql = "select * from Service 
            WHERE organisme='" . $org . "' 
            AND CONCAT(siren, '', complement)='" . $siret . "'  ";
        $statement = Atexo_Db::getLinkCommon()->query($sql);
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $libelle = $row['libelle'];
        }

        return $libelle;
    }

    /*
  * Permet de retourner array des ids des sous services avec leur service pere
 */
    public function getArrayIdServiceWithChild($idService, $organisme)
    {
        $arrayIds = [];
        $sericeChild = self::retrieveAllChildrenServices($idService, $organisme);
        if ($sericeChild) {
            $sericeChild = str_replace('(', '', $sericeChild);
            $sericeChild = str_replace(')', '', $sericeChild);
            $arrayIds = explode(',', (string) $sericeChild);

            return $arrayIds;
        } else {
            $arrayIds[] = $idService;
        }

        return $arrayIds;
    }

    /**
     * Permet de retourner le service by id et organisme.
     *
     * @param int    $idService
     * @param string $acronyme  l'acronyme de l'organisme
     *
     * @return CommonService
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function retrieveServiceBYId($idService, $acronyme)
    {
        $serviceQuery = new CommonServiceQuery();

        return $serviceQuery->getServiceById($idService, $acronyme);
    }

    public static function getAllEntityPurchase($organisme, $withPath = null, $listeIdsToIgnore = [], $langue = null)
    {
        $entityPurchaseArray = [];
        $entityPurchase = self::getCachedEntities($organisme);
        $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);

        if (is_array($entityPurchase)) {
            foreach ($entityPurchase as $entity) {
                if (!in_array($entity->getId(), $listeIdsToIgnore)) {
                    $idEntity = $entity->getId();
                    $entityPurchaseArray[$idEntity]['id'] = $entity->getId();
                    if (true == $withPath) {
                        $entityPurchaseArray[$idEntity]['libelle'] = $entity->getPathServiceTraduit();
                    } else {
                        $entityPurchaseArray[$idEntity]['libelle'] = $entity->getSigle();
                    }
                }
            }
        }

        return $entityPurchaseArray;
    }

    public function retrievefusionServiceNotMergedByIdService(
        $idServiceSource,
        $idServiceCible,
        $organisme,
        $connexion = null
    ) {
        $fusionServiceQuery = new CommonTFusionnerServicesQuery();

        return $fusionServiceQuery->getFusionServiceByIdServiceSource(
            $idServiceSource,
            $idServiceCible,
            $organisme,
            '0',
            $connexion
        );
    }

    public function retrievefusionServiceNotMerged($connexion = null)
    {
        $fusionServiceQuery = new CommonTFusionnerServicesQuery();

        return $fusionServiceQuery->getFusionServiceNotMerged($connexion);
    }

    public function retrieveServiceBySiren($siren, $siret, $organisme)
    {
        $connexion = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY')
        );
        $serviceQuery = new CommonServiceQuery();

        return $serviceQuery->getServiceBySiren($siren, $siret, $organisme, $connexion);
    }
}
