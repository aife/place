<?php

namespace App\Utils;

use Exception;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class AtexoObjectConverter
{
    /**
     * Converti un array en objet de la classe fournit.
     *
     * @param $array
     * @param $className
     *
     * @return object
     */
    public function arrayToObject($array, $className)
    {
        try {
            $encoders = [new XmlEncoder(), new JsonEncoder()];
            $normalizers = new ObjectNormalizer();
            $serializer = new Serializer([$normalizers], $encoders);
            $json = $serializer->serialize($array, 'json');

            return $serializer->deserialize($json, $className, 'json');
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
