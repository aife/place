<?php

namespace App\Utils\Publicite;

/**
 * Class Utils.
 */
class Utils
{
    /**
     *
     * Format un numéro de téléphone au
     */
    public function formatTelephone(?string $telephone): ?string
    {
        if (empty($telephone)) {
            return null;
        }

        $indicatifNational = '+33';

        if (preg_match('/(\+\d{1,3})\s/', $telephone, $matches, PREG_OFFSET_CAPTURE)) {
            $indicatifNational = $matches[1][0] ?? '';
        }

        if (!empty($indicatifNational)) {
            $telephone = str_replace($indicatifNational, '', $telephone);
            $telephone = ltrim($telephone, '0');
        }

        $telephone = str_replace([' ', '-', '.', '/'], '', $telephone);
        $telephone = $indicatifNational . ' ' . $telephone;

        // Si le format final ne correspond pas au format autorisé par la XSD
        // ou si longueur > 100
        if (
            !preg_match('/^(\+\d{1,3}\s\d+$)/', $telephone)
            || strlen($telephone) > 100
        ) {
            $telephone = null;
        }

        return $telephone;
    }
}
