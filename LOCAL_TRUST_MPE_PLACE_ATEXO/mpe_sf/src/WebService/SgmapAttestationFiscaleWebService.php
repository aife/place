<?php

namespace App\WebService;

use Symfony\Component\DependencyInjection\Container;

/**
 * Class SgmapAttestationFiscaleWebService.
 *
 * @see https://api.apientreprise.fr/docs/1.0/v1-entreprises.html
 *
 * @author David Delobel <david.delobel@atexo.com>
 */
class SgmapAttestationFiscaleWebService extends WebService
{
    private string $pathname = '/api/v1/attestations_fiscales';

    final public function __construct(Container $container)
    {
        $parameters = new Parameters();
        $parameters
            ->add('context')
            ->add('recipient')
            ->add('object')
            ->add('user_id', true, 'Undefined')
            ->add('email')
            ->add('password')
            ->add('siren_is')
            ->add('siren_tva');
        $response_status_codes = new HttpStatusCodes();
        $response_status_codes
            ->add(200, 'Succès', true)
            ->add(401, "Une erreur s'est produite lors de la récupération des exercices, ou des paramètres sont manquants")
            ->add(422, "Une erreur s'est produite lors de la résolution de l'attestation, ou des paramètres sont manquants");
        $this->setContainer($container);
        parent::__construct($container->getParameter('sgmap_host').$this->pathname, $parameters, $response_status_codes);
        $this->addParameter('token', true, $container->getParameter('sgmap_token'));
    }
}
