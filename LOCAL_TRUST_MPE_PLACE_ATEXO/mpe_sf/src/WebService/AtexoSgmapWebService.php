<?php

namespace App\WebService;

use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class AtexoSgmapWebService d'initialisation des web service ATEXO de la sycnhronisation des documents SGMAP.
 *
 * @author    Oumar KONATE <oumar.konate@atexo.com>
 *
 * @since     ESR2016
 *
 * @copyright Atexo 2016
 */
class AtexoSgmapWebService extends WebService
{
    /**
     * AtexoSgmapWebService constructor.
     *
     * @param string $url
     * @param int    $type
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function __construct($url, $type, private readonly LoggerInterface $logger, private readonly Container $container)
    {
        $response_status_codes = new HttpStatusCodes();
        $response_status_codes
            ->add(200, 'Succès', true)
            ->add(401, 'Non autorisé : le jeton est manquant ou invalide')
            ->add(400, 'Bad request')
            ->add(404, "Le document est non trouvé ou n'existe pas")
            ->add(422, 'Erreur lors de la résolution du WS, ou paramètres manquants')
            ->add(500, 'Erreur interne au serveur')
            ->add(503, 'Service temporairement indisponible ou en maintenance');

        $this->setContainer($this->container);
        parent::__construct($url, new Parameters(), $response_status_codes);

        $this->addParameter('type', true, $type);
        $this->setLogger($this->logger);
    }
}
