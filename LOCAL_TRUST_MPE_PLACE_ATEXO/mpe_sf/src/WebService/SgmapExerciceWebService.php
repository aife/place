<?php

namespace App\WebService;

use Symfony\Component\DependencyInjection\Container;

/**
 * Class SgmapExerciceWebService.
 *
 * @see https://api.apientreprise.fr/docs/1.0/v1-exercices.html
 *
 * @author David Delobel <david.delobel@atexo.com>
 */
class SgmapExerciceWebService extends WebService
{
    private string $pathname = '/api/v1/etablissements/exercices';

    final public function __construct(Container $container)
    {
        $parameters = new Parameters();
        $parameters
            ->add('context')
            ->add('recipient')
            ->add('object')
            ->add('user_id', true, 'Undefined')
            ->add('email')
            ->add('password');
        $response_status_codes = new HttpStatusCodes();
        $response_status_codes
            ->add(200, 'Succès', true)
            ->add(401, 'Le couple email/password est invalide, ou le token est manquant')
            ->add(422, "Une erreur s'est produite lors de la récupération des exercices, ou des paramètres sont manquants");
        $this->setContainer($container);
        parent::__construct($container->getParameter('sgmap_host').$this->pathname, $parameters, $response_status_codes);
        $this->addParameter('token', true, $container->getParameter('sgmap_token'));
    }
}
