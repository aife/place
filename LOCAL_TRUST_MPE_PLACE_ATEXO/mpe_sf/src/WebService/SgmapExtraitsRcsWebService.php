<?php

namespace App\WebService;

use Symfony\Component\DependencyInjection\Container;

/**
 * Class SgmapExtraitsRcsWebService.
 *
 * @see https://api.apientreprise.fr/docs/1.0/v1-infogreffe-extraits_rcs.html
 */
class SgmapExtraitsRcsWebService extends WebService
{
    private string $pathname = '/api/v1/infogreffe/extraits_rcs';

    final public function __construct(Container $container)
    {
        $parameters = new Parameters();
        $parameters
            ->add('context')
            ->add('recipient')
            ->add('object');
        $response_status_codes = new HttpStatusCodes();
        $response_status_codes
            ->add(200, 'Succès', true)
            ->add(400, 'Requête incorrecte')
            ->add(401, 'Non autorisé : le jeton est manquant ou invalide')
            ->add(404, 'SIREN non trouvé')
            ->add(500, 'Erreur interne au serveur')
            ->add(503, 'Service indisponible');
        $this->setContainer($container);
        parent::__construct($container->getParameter('sgmap_host').$this->pathname, $parameters, $response_status_codes);
        $this->addParameter('token', true, $container->getParameter('sgmap_token'));
    }
}
