<?php

namespace App\WebService;

use Symfony\Component\DependencyInjection\Container;

/**
 * Class SgmapCertificatOpqibiWebService.
 *
 * @see https://api.apientreprise.fr/docs/1.0/v1-opqibi-certificat.html
 */
class SgmapCertificatOpqibiWebService extends WebService
{
    private string $pathname = '/api/v1/opqibi/certificat';

    final public function __construct(Container $container)
    {
        $parameters = new Parameters();
        $parameters
            ->add('context')
            ->add('recipient')
            ->add('object');
        $response_status_codes = new HttpStatusCodes();
        $response_status_codes
            ->add(200, 'Succès', true)
            ->add(401, 'Non autorisé : le jeton est manquant ou invalide')
            ->add(400, "Le SIREN n'est pas valide")
            ->add(404, "L'entreprise n'est pas connue d'OPQIBI")
            ->add(500, 'Erreur interne au serveur')
            ->add(503, 'Service indisponible');
        $this->setContainer($container);
        parent::__construct($container->getParameter('sgmap_host').$this->pathname, $parameters, $response_status_codes);
        $this->addParameter('token', true, $container->getParameter('sgmap_token'));
    }
}
