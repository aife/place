<?php

namespace App\WebService;

use Symfony\Component\DependencyInjection\Container;

/**
 * Class SgmapEligibiliteCotisationRetraiteWebService.
 *
 * @see https://api.apientreprise.fr/docs/1.0/v1-probtp-eligibilite_cotisation_retraite.html
 *
 * @author David Delobel <david.delobel@atexo.com>
 */
class SgmapEligibiliteCotisationRetraiteWebService extends WebService
{
    private string $pathname = '/api/v1/probtp/eligibilite_cotisation_retraite';

    final public function __construct(Container $container)
    {
        $parameters = new Parameters();
        $parameters
            ->add('context')
            ->add('recipient')
            ->add('object');
        $response_status_codes = new HttpStatusCodes();
        $response_status_codes
            ->add(200, 'Succès', true)
            ->add(401, 'Non autorisé : le jeton est manquant ou invalide')
            ->add(422, 'SIRET incorrect')
            ->add(404, 'SIRET non trouvé sur la plateforme PROBTP')
            ->add(503, 'Plateforme PROBTP indisponible');
        $this->setContainer($container);
        parent::__construct($container->getParameter('sgmap_host').$this->pathname, $parameters, $response_status_codes);
        $this->addParameter('token', true, $container->getParameter('sgmap_token'));
    }
}
