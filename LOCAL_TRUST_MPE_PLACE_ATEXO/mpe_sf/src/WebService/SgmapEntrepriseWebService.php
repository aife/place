<?php

namespace App\WebService;

use Symfony\Component\DependencyInjection\Container;

/**
 * Class SgmapEntrepriseWebService.
 *
 * @see https://api.apientreprise.fr/docs/1.0/v1-entreprises.html
 *
 * @author David Delobel <david.delobel@atexo.com>
 */
class SgmapEntrepriseWebService extends WebService
{
    private string $pathname = '/api/v1/entreprises';

    final public function __construct(Container $container, $pathname = null)
    {
        $parameters = new Parameters();
        $parameters
            ->add('context')
            ->add('recipient')
            ->add('object');
        $response_status_codes = new HttpStatusCodes();
        $response_status_codes
            ->add(200, 'Succès', true)
            ->add(401, 'Non autorisé : le jeton est manquant ou invalide')
            ->add(404, "L'entreprise n'a pas été trouvée");
        if (!$pathname) {
            $pathname = $this->pathname;
        }
        var_export($container->getParameter('sgmap_host').$pathname);
        exit(1);
        $this->setContainer($container);
        $this->addParameter('token', true, $container->getParameter('sgmap_token'));
    }

    public function isSuccessful()
    {
        return parent::isSuccessful() and !$this->getJson()['gateway_error'];
    }

    public function getErrorReason()
    {
        $gateway_error_reason = "Un des fournisseurs de données n'a pas correctement fourni les informations.";
        if ($this->getJson()['gateway_error']) {
            return $gateway_error_reason;
        }

        return parent::getErrorReason();
    }
}
