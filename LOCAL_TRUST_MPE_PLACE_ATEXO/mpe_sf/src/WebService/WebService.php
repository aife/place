<?php

namespace App\WebService;

use App\Traits\ConfigProxyTrait;
use Psr\Log\LoggerInterface;
use Exception;
use App\Exception\ClientWsException;
use App\Service\ClientWs\ClientWs;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DependencyInjection\Container;

abstract class WebService
{
    use ConfigProxyTrait;

    private readonly ClientWs $client;

    /**
     * @var ResponseInterface
     */
    private $response = null;

    private ?LoggerInterface $logger = null;

    private ?Container $container = null;

    /**
     * WebService constructor.
     *
     * @param string          $base_uri              base d'URL de l'API
     * @param Parameters      $parameters            paramètres à envoyer lors de la requête
     * @param HttpStatusCodes $response_status_codes liste des status codes renvoyés par l'API
     *
     * @author David Delobel <david.delobel@atexo.com>
     */
    public function __construct($base_uri, private readonly ?Parameters $parameters, private readonly ?HttpStatusCodes $response_status_codes)
    {
        $config = ['base_url' => $base_uri];
        $this->client = new ClientWs($this->container, $config);
    }

    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function setContainer(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Ajouter un paramètre pour l'envoi de la requête.
     *
     * @param string $field       Nom du champ
     * @param bool   $is_required true si le champ est obligatoire pour l'appel à l'API
     * @param mixed  $default     valeur par défaut envoyée si la valeur current n'est pas définie
     * @param mixed  $current     valeur à envoyer, la valeur par défaut est utilisée si celle-ci n'est pas définie
     *
     * @throws Exception si le champ est déjà défini dans les paramètres
     * @author David Delobel <david.delobel@atexo.com>*/
    final public function addParameter($field, $is_required = false, $default = null, $current = null)
    {
        $this->parameters->add($field, $is_required, $default, $current);
    }

    /**
     * Envoyer la requête après sa préparation éventuelle.
     *
     * @param string $relative_url
     * @param string $method       POST, GET, PUT, DELETE...
     * @param null   $headers      Headers à envoyer lors de la requête
     * @param null   $body         Corps du message à envoyer lors de la requête
     * @param array  $options      Options supplémentaires pour Guzzle
     *
     * @return WebService $this
     *
     * @author David Delobel <david.delobel@atexo.com>
     */
    final public function request($relative_url, $method = 'GET', $headers = null, $body = null, $options = [])
    {
        $relative_url_with_query_string = $relative_url.'?'.$this->parameters->getQueryString();
        if (!empty($headers)) {
            $options['headers'] = $headers;
        }
        if (!empty($body)) {
            $options['body'] = $body;
        }
        try {
            $this->logger->info("Appel du ws avec les parametres: [url = $relative_url_with_query_string] , [methode = $method] , [header = ]".var_export($headers, true).' , [body = ]'.var_export($body, true).' , [options = ]'.var_export($options, true));
            $this->response = $this->client->request($method, $relative_url_with_query_string, $options);
            $this->logger->info('Response = '.var_export($this->response, true));
        } catch (ClientWsException $e) {
            $this->response = $e->getResponse();
            $this->logger->error('Erreur Appel du ws (BadResponseException). Response = '.var_export($this->response, true));
        } catch (Exception $e) {
            $this->logger->error('Erreur Appel du ws (Exception). Message = '.$e->getMessage());
        } finally {
            return $this;
        }
    }

    /**
     * Renvoie le corps de la réponse suite à la request.
     *
     * @param bool $as_string true si le corps de la requête doit être sous forme de string et non d'objet
     *
     * @return string
     *
     * @author David Delobel <david.delobel@atexo.com>
     */
    final public function getBody($as_string = false)
    {
        return $this->response->getBody($as_string);
    }

    /**
     * Renvoie le corps de la réponse en Json.
     *
     * @return array|bool|float|int|string Le json renvoyé
     *
     * @author David Delobel <david.delobel@atexo.com>
     */
    final public function getJson(): array|bool|float|int|string
    {
        return $this->response->json();
    }

    /**
     * Obtenir le status code HTTP de la réponse.
     *
     * @return int le status code de la réponse
     *
     * @author David Delobel <david.delobel@atexo.com>
     */
    final public function getStatusCode()
    {
        return $this->response->getStatusCode();
    }

    /**
     * Savoir si la réponse est succès.
     *
     * @return bool true si la réponse est un succès
     *
     * @throws Exception si le code HTTP de la réponse n'a pas été indiqué dans $response_status_codes
     * @author David Delobel <david.delobel@atexo.com>*/
    public function isSuccessful()
    {
        return $this->response_status_codes->isSuccess($this->getStatusCode());
    }

    /**
     * Obtenir le message lié à l'erreur dans le cas d'un non-succès.
     *
     * @return string la description de l'erreur
     *
     * @throws Exception si la réponse a été un succès
     * @author David Delobel <david.delobel@atexo.com>*/
    public function getErrorReason()
    {
        if ($this->isSuccessful()) {
            throw new Exception("Impossible d'obtenir l'erreur car la requête s'est effectuée avec succès.");
        }

        return $this->response_status_codes->getDescription($this->getStatusCode());
    }
}
