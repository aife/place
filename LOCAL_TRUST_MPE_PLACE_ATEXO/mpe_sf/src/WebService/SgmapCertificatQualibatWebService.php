<?php

namespace App\WebService;

use Symfony\Component\DependencyInjection\Container;

/**
 * Class SgmapCertificatQualibatWebService.
 *
 * @see https://api.apientreprise.fr/docs/1.0/v1-certificats_qualibat.html
 *
 * @author David Delobel <david.delobel@atexo.com>
 */
class SgmapCertificatQualibatWebService extends WebService
{
    private string $pathname = '/api/v1/certificats_qualibat';

    final public function __construct(Container $container)
    {
        $parameters = new Parameters();
        $parameters
            ->add('context')
            ->add('recipient')
            ->add('object');
        $response_status_codes = new HttpStatusCodes();
        $response_status_codes
            ->add(200, 'Succès', true)
            ->add(401, 'Non autorisé : le jeton est manquant ou invalide')
            ->add(404, 'Certificat non trouvé')
            ->add(503, 'Plateforme indisponible');
        $this->setContainer($container);
        parent::__construct($container->getParameter('sgmap_host').$this->pathname, $parameters, $response_status_codes);
        $this->addParameter('token', true, $container->getParameter('sgmap_token'));
    }
}
