<?php

namespace App\WebService;

use Exception;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class SgmapAttestationSocialeWebService.
 *
 * @see https://api.apientreprise.fr/docs/1.0/v1-attestations_sociales.html
 *
 * @author David Delobel <david.delobel@atexo.com>
 */
class SgmapAttestationSocialeWebService extends WebService
{
    private string $pathname = '/api/v1/attestations_sociales';

    /**
     * SgmapAttestationSocialeWebService constructor.
     *
     * @param string $type_attestation 'AMP_UR' : de marché public Urssaf, 'AVG_UR' : de vigilance Urssaf
     *
     * @throws Exception si le type d'attestation n'est pas bon
     * @author David Delobel <david.delobel@atexo.com>*/
    final public function __construct(Container $container, $type_attestation)
    {
        if (!in_array($type_attestation, ['AMP_UR', 'AVG_UR'])) {
            throw new Exception("Le type d'attestation sociale demandée n'a pas été correctement défini : ce paramètre est obligatoire");
        }
        $parameters = new Parameters();
        $parameters
            ->add('context')
            ->add('recipient')
            ->add('object')
            ->add('type_attestation', true, null, $type_attestation);
        $response_status_codes = new HttpStatusCodes();
        $response_status_codes
            ->add(200, 'Succès', true)
            ->add(401, 'Non autorisé : le jeton est manquant ou invalide')
            ->add(422, "Une erreur s'est produite lors de la résolution de l'attestation ou les paramètres ne sont pas valides")
            ->add(503, "La plateforme ACOSS n'est pas disponible actuellement");
        $this->setContainer($container);
        parent::__construct($container->getParameter('sgmap_host').$this->pathname, $parameters, $response_status_codes);
        $this->addParameter('token', true, $container->getParameter('sgmap_token'));
    }
}
