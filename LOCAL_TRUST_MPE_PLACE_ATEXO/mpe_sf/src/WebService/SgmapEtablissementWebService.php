<?php

namespace App\WebService;

use Exception;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class SgmapEtablissementWebService.
 *
 * @see https://api.apientreprise.fr/docs/1.0/v1-etablissements.html
 *
 * @author David Delobel <david.delobel@atexo.com>
 */
class SgmapEtablissementWebService extends WebService
{
    private string $pathname = '/api/v1/etablissements';

    final public function __construct(Container $container)
    {
        $parameters = new Parameters();
        $parameters
            ->add('context')
            ->add('recipient')
            ->add('object');
        $response_status_codes = new HttpStatusCodes();
        $response_status_codes
            ->add(200, 'Succès', true)
            ->add(401, 'Non autorisé : le jeton est manquant ou invalide')
            ->add(404, "L'établissement n'a pas été trouvé");
        $this->setContainer($container);
        parent::__construct($container->getParameter('sgmap_host').$this->pathname, $parameters, $response_status_codes);
        $this->addParameter('token', true, $container->getParameter('sgmap_token'));
    }

    /**
     * Un gateway_error dans la réponse indique que des services
     * en amont n'ont pas fourni toutes les données à SGMAP.
     *
     * @return bool true si le service a correctement renvoyé sa donnée
     *
     * @author David Delobel <david.delobel@atexo.com>
     */
    public function isSuccessful()
    {
        return parent::isSuccessful() and !$this->getJson()['gateway_error'];
    }

    /**
     * Obtenir l'erreur liée à un non-succès.
     *
     * @return string
     *
     * @throws Exception
     *
     * @author David Delobel <david.delobel@atexo.com>*/
    public function getErrorReason()
    {
        // À la date du 22/06/2016 --- la clé 'gateway_error' n'est plus utilisée sur cette api.
        //$gateway_error_reason = "Un des fournisseurs de données n'a pas correctement fourni les informations.";
        //if ($this->getJson()['gateway_error']) return $gateway_error_reason;
        return parent::getErrorReason();
    }
}
