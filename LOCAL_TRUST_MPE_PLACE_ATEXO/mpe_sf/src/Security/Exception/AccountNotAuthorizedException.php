<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\Exception;

use Symfony\Component\Security\Core\Exception\AuthenticationException;

class AccountNotAuthorizedException extends AuthenticationException
{

}
