<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Security;

use App\Entity\Agent;
use App\Entity\Inscrit;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class ExternalSsoAuthenticator extends AbstractGuardAuthenticator
{
    use FindUserTrait;

    public function __construct(private readonly EntityManagerInterface $em, private readonly GuardManager $guardManager, private readonly LoggerInterface $logger)
    {
    }

    public function start(Request $request, AuthenticationException $authException = null): RedirectResponse
    {
        return new RedirectResponse($this->guardManager->getRedirectUrl($request));
    }

    public function supports(Request $request): bool
    {
        return $request->attributes->get('_route') === 'mpe_auth_sso_check'
            && ($request->server->has('user-login') || $request->server->has('user-external-id'))
            && $request->server->has('user-type')
            && $this->guardManager->isGuardEnabled($request->server->get('user-type'), self::class);
    }

    public function getCredentials(Request $request): array
    {
        return [
            'user_type' => $request->server->get('user-type'),
            'user_login' => $request->server->get('user-login'),
            'user_external_id' => $request->server->get('user-external-id'),
            'service_id' => $request->server->get('service-id'),
        ];
    }

    public function getUser($credentials, UserProviderInterface $userProvider): ?UserInterface
    {
        $userType = $credentials['user_type'];
        $userLogin = $credentials['user_login'];
        $userExternalId = $credentials['user_external_id'];
        $serviceId = $credentials['service_id'];

        if ($userLogin) {
            return $this->findUserByLogin($userType, $userLogin);
        }

        return $this->findAgentByExternalIdAndService($userExternalId, $serviceId);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey): RedirectResponse
    {
        return new RedirectResponse($this->guardManager->getRedirectUrl($request));
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): Response
    {
        $this->logger->debug(
            sprintf(
                "L'utilisateur n'existe pas : '%s'. Redirection vers la page d'inscription.",
                $request->server->get('user-login')
            )
        );

        return new RedirectResponse('/?page=Entreprise.FormulaireCompteEntreprise&action=creation');
    }

    public function checkCredentials($credentials, UserInterface $user): bool
    {
        return true;
    }

    public function supportsRememberMe(): bool
    {
        return false;
    }

    /**
     * Uniquement coté Agent (CNRS)
     */
    private function findAgentByExternalIdAndService(string $userExternalId, string $serviceId): ?UserInterface
    {
        return $this->em->getRepository(Agent::class)->findOneBy([
            'idExterne' => $userExternalId,
            'serviceId' => $serviceId,
        ]);
    }
}
