<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\UserFactory;

use App\Entity\Agent;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Exception\OrganismeNotFoundException;
use App\Service\Agent\Habilitation;
use Doctrine\ORM\EntityManagerInterface;
use Hslavich\OneloginSamlBundle\Security\User\SamlUserFactoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class SamlUserFactory implements SamlUserFactoryInterface
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag,
        private readonly Habilitation $habilitationService
    ) {
    }

    public function createUser($username, array $attributes = []): UserInterface
    {
        $user = null;

        $this->logger->info(sprintf("Begin creation User %s via SamlUserFactory", $username));

        if ($username) {
            [$username, $attributes] = [$username->getUserIdentifier(), $username->getAttributes()];
            // Get user infos from keycloak mapped attributes
            $email = '' !== $this->getMappedAttributes($attributes, 'email')
                ? $this->getMappedAttributes($attributes, 'email')
                : $this->getMappedAttributes($attributes, 'emailaddress');
            $firstName = '' !== $this->getMappedAttributes($attributes, 'firstName')
                ? $this->getMappedAttributes($attributes, 'firstName')
                : $this->getMappedAttributes($attributes, 'givenname');
            $lastName = '' !== $this->getMappedAttributes($attributes, 'lastName')
                ? $this->getMappedAttributes($attributes, 'lastName')
                : $this->getMappedAttributes($attributes, 'name');

            $firstName =  '' === $firstName ? $this->getMappedAttributes($attributes, 'firstname') : $firstName;
            $lastName = '' ===  $lastName ? $this->getMappedAttributes($attributes, 'lastname') : $lastName;
    
            $organismeAcronyme = $this->parameterBag->get('OAUTH_KEYCLOAK_SAML2_REFERENCE_ORGANISME') ?: null;
            if (!$organismeAcronyme) {
                $errorMessage = sprintf(
                    "Error - No organisme acronym found in parameter %s",
                    '`OAUTH_KEYCLOAK_SAML2_REFERENCE_ORGANISME`'
                );
                $this->logger->error($errorMessage);

                throw new OrganismeNotFoundException($errorMessage);
            }

            $serviceId = $this->parameterBag->get('OAUTH_KEYCLOAK_SAML2_REFERENCE_SERVICE') ?: null;
            $profileId = $this->parameterBag->get('OAUTH_KEYCLOAK_SAML2_REFERENCE_HABILITATION_PROFIL') ?: null;

            /** @var ?Organisme $organisme */
            $organisme = $this->em->getRepository(Organisme::class)->findOneBy([
                'acronyme' => $organismeAcronyme
            ]);

            if (null === $organisme) {
                $errorMessage = sprintf("Can't create User - No organism found for acronyme: %s", $organismeAcronyme);
                $this->logger->error($errorMessage);

                throw new OrganismeNotFoundException($errorMessage);
            }

            $service = null;
            if ($serviceId) {
                $service = $this->em->getRepository(Service::class)->findOneBy([
                    'id' => $serviceId,
                    'organisme' => $organisme,
                ]);
            }

            $password = random_int(0, mt_getrandmax());

            $habilitations = $this->habilitationService->createHabilitationsAgent($username, $profileId);

            $date = (new \DateTime())->format('Y-m-d H:i:s');
            $user = new Agent();
            $user
                ->setLogin($username)
                ->setPrenom($firstName)
                ->setNom($lastName)
                ->setEmail($email)
                ->setPassword($password)
                ->setMdp($password)
                ->setOrganisme($organisme)
                ->setService($service)
                ->setDateCreation($date)
                ->setDateModification($date)
                ->setHabilitation($habilitations)
            ;

            $habilitations->setAgent($user);

            $this->em->persist($user);
            $this->em->persist($habilitations);
            $this->em->flush();

            $this->logger->info(
                sprintf(
                    "User %s created via SamlUserFactory with organism=%s, profileId=%s, serviceId=%s",
                    $username,
                    $organismeAcronyme,
                    $profileId,
                    $serviceId
                )
            );
        }

        $this->logger->info(sprintf("End creation User %s via SamlUserFactory", $username));

        return $user;
    }

    private function getMappedAttributes(array $attributes, string $attributeValue): string
    {
        return $attributes[$attributeValue] && $attributes[$attributeValue][0] ? $attributes[$attributeValue][0] : '';
    }
}
