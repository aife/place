<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\SuperAdmin;

use Doctrine\Persistence\ObjectRepository;
use App\Entity\Administrateur;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface, PasswordUpgraderInterface
{
    private readonly ObjectRepository $administrateurRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->administrateurRepository = $entityManager->getRepository(Administrateur::class);
    }

    /**
     * @inheritDoc
     */
    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        $user = $this->administrateurRepository->loadSuperAdminByUsername($identifier);
        if (null === $user) {
            $e = new UserNotFoundException(sprintf('User "%s" not found.', $identifier));
            $e->setUserIdentifier($identifier);

            throw $e;
        }

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function refreshUser(UserInterface $user)
    {
        return $this->administrateurRepository->refreshUser($user);
    }

    /**
     * @inheritDoc
     */
    public function supportsClass($class)
    {
        return $this->administrateurRepository->supportsClass($class);
    }

    /**
     * {@inheritdoc}
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        $repository = $this->administrateurRepository;
        if ($repository instanceof PasswordUpgraderInterface) {
            $repository->upgradePassword($user, $newEncodedPassword);
        }
    }

    public function loadUserByUsername($username)
    {
        return $this->administrateurRepository->loadSuperAdminByUsername($username);
    }
}
