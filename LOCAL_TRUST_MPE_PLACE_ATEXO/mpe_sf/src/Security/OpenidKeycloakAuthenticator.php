<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security;

use Exception;
use App\Entity\Agent;
use App\Entity\Inscrit;
use Doctrine\ORM\EntityManagerInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Client\OAuth2ClientInterface;
use KnpU\OAuth2ClientBundle\Security\Authenticator\SocialAuthenticator;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Namshi\JOSE\JWS;

class OpenidKeycloakAuthenticator extends SocialAuthenticator
{
    use FindUserTrait;

    private ?array $mapping = null;

    protected ?array $credentials = null;

    public function __construct(
        private ClientRegistry $clientRegistry,
        private EntityManagerInterface $em,
        private GuardManager $guardManager,
        private LoggerInterface $logger,
        private TranslatorInterface $translator
    ) {
        $this->mapping = $guardManager->getOpenidMapping(self::class);
    }

    public function getClientRegistry(): ClientRegistry
    {
        return $this->clientRegistry;
    }

    public function setClientRegistry(ClientRegistry $clientRegistry): OpenidKeycloakAuthenticator
    {
        $this->clientRegistry = $clientRegistry;
        return $this;
    }

    public function getEm(): EntityManagerInterface
    {
        return $this->em;
    }

    public function setEm(EntityManagerInterface $em): OpenidKeycloakAuthenticator
    {
        $this->em = $em;
        return $this;
    }

    public function getGuardManager(): GuardManager
    {
        return $this->guardManager;
    }

    public function setGuardManager(GuardManager $guardManager): OpenidKeycloakAuthenticator
    {
        $this->guardManager = $guardManager;
        return $this;
    }

    /**
     * @return array
     */
    public function getMapping(): ?array
    {
        return $this->mapping;
    }

    /**
     * @param array $mapping
     */
    public function setMapping(?array $mapping): OpenidKeycloakAuthenticator
    {
        $this->mapping = $mapping;
        return $this;
    }

    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    public function setLogger(LoggerInterface $logger): OpenidKeycloakAuthenticator
    {
        $this->logger = $logger;
        return $this;
    }

    public function getTranslator(): TranslatorInterface
    {
        return $this->translator;
    }

    public function setTranslator(TranslatorInterface $translator): OpenidKeycloakAuthenticator
    {
        $this->translator = $translator;
        return $this;
    }

    public function supports(Request $request): bool
    {
        return $request->attributes->get('_route') === 'mpe_auth_keycloak_check'
            && $request->query->has('code')
            && $request->query->has('state')
            && ($this->guardManager->isGuardEnabled(GuardManager::USER_TYPE_AGENT, self::class)
                || $this->guardManager->isGuardEnabled(GuardManager::USER_TYPE_ENTREPRISE, self::class));
    }

    public function getCredentials(Request $request): ?array
    {
        if (is_null($this->credentials)) {
            try {
                $accessToken = $this->fetchAccessToken($this->getKeycloakClient());
                $this->getLogger()->info(sprintf('Le JWT access token  : %s', $accessToken));
            } catch (Exception $e) {
                $this->getLogger()->error('Erreur de récupération du jeton JWT : ' . $e->getMessage());
                return null;
            }

            $userType = null;
            $login = null;
            $jwtPayload = JWS::load($accessToken)->getPayload();
            $this->getLogger()->info(sprintf('Le jwtPayload : %s', json_encode($jwtPayload, JSON_THROW_ON_ERROR)));
            if (!empty($jwtPayload[$this->mapping['usertype']])) {
                $userType = ($jwtPayload[$this->mapping['usertype']] === $this->mapping['usertype_agent'])
                    ? GuardManager::USER_TYPE_AGENT : GuardManager::USER_TYPE_ENTREPRISE;
            } else {
                $this->getLogger()->error(sprintf('Aucun attribut "%s" dans le jeton JWT', $this->mapping['usertype']));
                $this->logger->error(var_export($jwtPayload, true));
            }

            if (!empty($jwtPayload[$this->mapping['login']])) {
                $login = $jwtPayload[$this->mapping['login']];
            } else {
                $this->getLogger()->error(sprintf('Aucun attribut "%s" dans le jeton JWT', $this->mapping['login']));
                $this->logger->error(var_export($jwtPayload, true));
            }

            $this->credentials = [
                'user_type' => $userType,
                'login' => $login,
            ];
        }

        return $this->credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider): ?UserInterface
    {
        return $this->findUserByLogin($credentials['user_type'], $credentials['login']);
    }

    public function getKeycloakClient(): OAuth2ClientInterface
    {
        return $this->getClientRegistry()->getClient('keycloak');
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey): RedirectResponse
    {
        return new RedirectResponse($this->guardManager->getRedirectUrl($request));
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): Response
    {
        $this->logger->debug('Erreur d\'authentification : ' . self::class);
        $this->logger->debug($exception->getMessage());
        $this->logger->debug($exception->getTraceAsString());

        $request->getSession()->getFlashBag()->add('error', $this->translator->trans('TEXT_COMPTE_ERRONNE'));

        return new RedirectResponse($this->guardManager->getLogoutUrl($request));
    }

    public function start(Request $request, AuthenticationException $authException = null): RedirectResponse
    {
        return new RedirectResponse($this->guardManager->getRedirectUrl($request));
    }
}
