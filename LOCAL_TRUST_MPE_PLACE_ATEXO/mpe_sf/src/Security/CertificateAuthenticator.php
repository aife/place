<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Security;

use App\Entity\Administrateur;
use App\Entity\Agent;
use App\Entity\Inscrit;
use App\Repository\AgentRepository;
use App\Service\AtexoConfiguration;
use App\Service\MpeSecurity;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Guard\PasswordAuthenticatedInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CertificateAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * BasicAuthentificator constructor.
     */
    public function __construct(private readonly TranslatorInterface $translator, private readonly GuardManager $guardManager, private readonly AtexoConfiguration $atexoConfiguration, private readonly EntityManagerInterface $entityManager, private readonly LoggerInterface $logger)
    {
    }

    /**
     * @param AuthenticationException|null $authException
     */
    public function start(Request $request, AuthenticationException $authException = null): RedirectResponse
    {
        return new RedirectResponse($this->guardManager->getRedirectUrl($request));
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning `false` will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request): bool
    {
        return $this->atexoConfiguration->hasConfigPlateforme('authenticateAgentByCert')
                && $request->server->has('SSL_CLIENT_CERT')
                && $request->server->has('SSL_CLIENT_M_SERIAL')
                && '/agent/login-by-certificate' === $request->getPathInfo();
    }

    public function getCredentials(Request $request): array
    {
        return [
            'certificate' => $request->server->get('SSL_CLIENT_CERT'),
            'cert_serial' => $request->server->get('SSL_CLIENT_M_SERIAL'),
        ];
    }

    /**
     * @param mixed $credentials
     */
    public function getUser($credentials, UserProviderInterface $userProvider): ?UserInterface
    {
        // L'auth par certificat n'est possible que côté Agent
        if (!$userProvider->supportsClass(Agent::class)) {
            return null;
        }

        return  $this->entityManager->getRepository(Agent::class)->findOneBy([
            'certificat' => $credentials['certificate']
        ]);
    }

    /**
     * @param mixed $credentials
     */
    public function checkCredentials($credentials, UserInterface $user): bool
    {
        return $user instanceof Agent
            && $credentials['cert_serial'] === $user->getNumCertificat();
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): RedirectResponse
    {
        $this->logger->debug('Erreur d\'authentification : ' . self::class);

        $request->getSession()->getFlashBag()->add('error', $this->translator->trans('TEXT_CERTIFICAT_NOT_VALIDE'));

        return new RedirectResponse($this->guardManager->getRedirectUrl($request));
    }

    /**
     * @param string $providerKey
     * @return RedirectResponse|Response|null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return new RedirectResponse($this->guardManager->getRedirectUrl($request));
    }

    public function supportsRememberMe(): bool
    {
        return true;
    }
}
