<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Security;

use App\Repository\AgentRepository;
use App\Repository\ConfigurationPlateformeRepository;
use Firebase\JWT\JWK;
use Firebase\JWT\JWT;
use App\Entity\Agent;
use App\Service\MpeSecurity;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Guard\PasswordAuthenticatedInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class KeycloakFormAuthenticator extends AbstractGuardAuthenticator implements PasswordAuthenticatedInterface
{
    /**
     * @var string
     */
    final public const LOGINFORM = 'form';
    final public const KEYCLOAK_GRANT_TYPE = 'password';
    final public const KEYCLOAK_REALM = 'mpe-agent';
    final public const KEYCLOAK_CLIENT_ID = 'mpe-api';

    protected ?UserInterface $entity = null;

    /**
     * BasicAuthentificator constructor.
     */
    public function __construct(
        private CsrfTokenManagerInterface $csrfTokenManager,
        protected RouterInterface $router,
        private TranslatorInterface $translator,
        private MpeSecurity $mpeSecurity,
        private GuardManager $guardManager,
        private LoggerInterface $logger,
        private HttpClientInterface $httpClient,
        private TagAwareCacheInterface $cache,
        private AgentRepository $agentRepository,
        private ParameterBagInterface $parameterBag,
        private ConfigurationPlateformeRepository $configurationPlateformeRepository
    ) {
    }

    /**
     * @param AuthenticationException|null $authException
     */
    public function start(Request $request, AuthenticationException $authException = null): RedirectResponse
    {
        return new RedirectResponse($this->guardManager->getRedirectUrl($request));
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning `false` will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request): bool
    {
        $configPlatform = $this->configurationPlateformeRepository->getConfigurationPlateforme();

        return  $request->getPathInfo() === '/agent/login'
            && $request->request->has(self::LOGINFORM)
            && $configPlatform->isAuthenticateAgentByInternalKeycloak();
    }

    public function getCredentials(Request $request): array
    {
        $form = $request->request->get(self::LOGINFORM);

        $csrfToken = $request->request->get('_csrf_token');

        $token = new CsrfToken('authenticate', $csrfToken);

        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        try {
            $this->entity = $this->agentRepository->loadUserByIdentifier($form['_username']);
            $response = $this->httpClient->request(
                'POST',
                sprintf(
                    '%s/auth/realms/%s/protocol/openid-connect/token',
                    $this->parameterBag->get('IDENTITE_BASE_URL'),
                    self::KEYCLOAK_REALM
                ),
                ['body' => [
                    'client_id' => self::KEYCLOAK_CLIENT_ID,
                    'username' => $form['_username'],
                    'password' => $form['_password'],
                    'grant_type' => self::KEYCLOAK_GRANT_TYPE,
                ]]
            )->getContent();

            $jwtToken = json_decode($response)->{'access_token'};

            $decodedToken = JWT::decode($jwtToken, $this->getJwks());
        } catch (\Exception $e) {
            throw new AuthenticationException($e->getMessage());
        }

        $userType = Agent::class;

        return [
            'user_type' => $userType,
            'login' => $decodedToken->{'preferred_username'} ?? null,
        ];
    }

    /**
     * @param mixed $credentials
     */
    public function getUser($credentials, UserProviderInterface $userProvider): ?UserInterface
    {
        return $userProvider->loadUserByIdentifier($credentials['login']);
    }

    /**
     * @param mixed $credentials
     */
    public function checkCredentials($credentials, UserInterface|PasswordAuthenticatedUserInterface $user): bool
    {
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): RedirectResponse
    {
        $this->logger->debug('Erreur d\'authentification : ' . self::class);

        if (!empty($this->entity)) {
            $this->mpeSecurity->incrementTentativesMdp($this->entity);
        }

        $request->getSession()->getFlashBag()->add('error', $this->translator->trans('TEXT_COMPTE_ERRONNE'));

        return new RedirectResponse($this->guardManager->getRedirectUrl($request));
    }

    /**
     * @param string $providerKey
     * @return RedirectResponse|Response|null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return new RedirectResponse($this->guardManager->getRedirectUrl($request));
    }

    public function supportsRememberMe(): bool
    {
        return true;
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function getPassword($credentials): ?string
    {
        return $credentials['password'];
    }

    private function getJwks(): array
    {
        $client = $this->httpClient;
        $jwks = $this->cache->get('jwk_keys', function (ItemInterface $item) use ($client) {
            $jwks = json_decode(
                $client->request('GET', sprintf(
                    '%s/auth/realms/%s/protocol/openid-connect/certs',
                    $this->parameterBag->get('IDENTITE_BASE_URL'),
                    self::KEYCLOAK_REALM
                ))->getContent(),
                true
            );

            $item->expiresAfter(3600);
            $item->tag(['authentication']);

            return $jwks;
        });

        return JWK::parseKeySet($jwks);
    }
}
