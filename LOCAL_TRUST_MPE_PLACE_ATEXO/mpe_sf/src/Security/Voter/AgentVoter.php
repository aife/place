<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\Voter;

use App\Entity\Agent;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class AgentVoter extends Voter
{
    final const EDIT = 'AGENT_EDIT';

    public function __construct(private readonly Security $security)
    {
    }

    protected function supports($attribute, $subject)
    {
        return $attribute === self::EDIT && $subject instanceof Agent;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $this->security->getUser();
        if (null === $user->getService() && null === $user->getOrganisme()) {
            return true;
        }

        if (null === $user->getService() && null !== $user->getOrganisme()) {
            return $user->getOrganisme() === $subject->getOrganisme();
        }

        if (null !== $user->getService()) {
            return $user->getService() === $subject->getService();
        }

        return false;
    }
}
