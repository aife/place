<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\Voter;

use App\Entity\Consultation;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\TypeProcedure;
use App\Service\Authorization\AuthorizationAgent;
use App\Service\Consultation\ConsultationEnLigneService;
use App\Service\CurrentUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ConsultationEnLigneVoter extends Voter
{
    final public const EDIT = 'ONLINE_CONSULTATION_UPDATE';

    public function __construct(
        private readonly CurrentUser $currentUser,
        private readonly AuthorizationAgent $authorizationAgent,
        private readonly ConsultationEnLigneService $consultationEnLigneService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    protected function supports($attribute, $subject): bool
    {
        return $attribute === self::EDIT && $subject instanceof Consultation;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        if (!$this->currentUser->isAgent()) {
            return false;
        }

        /** @var Consultation $consultation */
        $consultation = $subject;

        /** @var TypeProcedure $typeProcedure */
        $typeProcedureOrg = $this->entityManager->getRepository(TypeProcedureOrganisme::class)
            ->findOneBy([
                'idTypeProcedure' => $consultation->getIdTypeProcedureOrg(),
                'organisme' => $consultation->getOrganisme(),
            ]);

        return $this->authorizationAgent->isAuthorized($consultation->getId())
            && $this->consultationEnLigneService->hasHabilitationModifierConsultationApresValidation($typeProcedureOrg);
    }
}
