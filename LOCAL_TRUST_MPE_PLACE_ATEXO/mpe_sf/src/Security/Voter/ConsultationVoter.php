<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\Voter;

use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\HabilitationAgent;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Repository\ConfigurationOrganismeRepository;
use App\Service\CurrentUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ConsultationVoter extends Voter
{
    use VoterTrait;

    final public const CREATE  = 'CONSULTATION_CREATE';

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ParameterBagInterface $parameterBag,
        private readonly ConfigurationOrganismeRepository $configurationOrganismeRepository,
        private readonly CurrentUser $currentUser,
        private readonly RequestStack $requestStack
    ) {
    }

    protected function supports($attribute, $subject): bool
    {
        $supportsSubject = empty($subject) || $subject instanceof Consultation;

        return $attribute === self::CREATE && $supportsSubject;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        /** @var Agent $user */
        $user = $token->getUser();

        return $this->hasCreateConsultationHabilitation($user, $subject);
    }
}
