<?php

namespace App\Security\Voter;

use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\HabilitationAgent;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Repository\ConfigurationOrganismeRepository;
use App\Service\CurrentUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ConsultationSuiteVoter extends Voter
{
    use VoterTrait;

    final public const CREATE  = 'CONSULTATION_SUITE_CREATE';

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ParameterBagInterface $parameterBag,
        private readonly ConfigurationOrganismeRepository $configurationOrganismeRepository,
        private readonly CurrentUser $currentUser,
        private readonly RequestStack $requestStack
    ) {
    }

    protected function supports($attribute, $subject): bool
    {
        $supportsAttribute = in_array($attribute, [self::CREATE]);
        $supportsSubject = empty($subject) || $subject instanceof Consultation;

        return $supportsAttribute && $supportsSubject;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return
            (
                (
                    $subject instanceof Consultation
                    && $subject->getReferenceConsultationParent() instanceof Consultation
                )
                || !empty($this->requestStack->getCurrentRequest()->get('consultationParentId'))
            )
            && (
                $this->currentUser->checkHabilitation('gererMapaInferieurMontantSuite')
                || $this->currentUser->checkHabilitation('gererMapaSuperieurMontantSuite')
                || $this->currentUser->checkHabilitation('administrerProceduresFormaliseesSuite')
                || $user->isTechnique() && $this->currentUser->checkHabilitation('creerSuiteConsultation')
            );
    }
}
