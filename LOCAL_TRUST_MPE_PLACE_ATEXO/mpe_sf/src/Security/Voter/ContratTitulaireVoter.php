<?php

namespace App\Security\Voter;

use App\Entity\ContratTitulaire;
use App\Entity\HabilitationAgent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class ContratTitulaireVoter extends Voter
{
    public final const CREATE = 'CONTRAT_CREATE';
    public final const EDIT = 'CONTRAT_UPDATE';

    public function __construct(private readonly Security $security, private readonly EntityManagerInterface $entityManager)
    {
    }

    protected function supports($attribute, $subject): bool
    {
        $supportsAttribute = in_array($attribute, [self::CREATE, self::EDIT]);
        $supportsSubject = empty($subject) || $subject instanceof ContratTitulaire;

        return $supportsAttribute && $supportsSubject;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $this->security->getUser();

        /** @var HabilitationAgent $habilitations */
        $habilitations = $this->entityManager->getRepository(HabilitationAgent::class)
            ->findOneBy(['agent' => $user]);

        if ($attribute == self::CREATE) {
            return (bool)$habilitations->getCreerContrat();
        } elseif ($attribute == self::EDIT) {
            return (bool)$habilitations->getModifierContrat();
        }

        return false;
    }
}
