<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\Voter;

use App\Entity\MediaUuid;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class MediaVoter extends Voter
{
    public const VIEW = 'MEDIA_VIEW';

    protected function supports(string $attribute, $subject): bool
    {
        return self::VIEW === $attribute && $subject instanceof MediaUuid;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        return time() < $subject->getCreatedAt()->getTimestamp() + MediaUuid::EXPIRED_DOWNLOAD_TIME;
    }
}
