<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\Voter;

use App\Entity\Consultation;
use App\Service\Authorization\AuthorizationAgent;
use App\Service\CurrentUser;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Exception;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ConsultationSimplifieeVoter extends Voter
{
    final public const CREATE = 'CS_CREATE';
    final public const EDIT = 'CS_EDIT';

    public function __construct(
        private readonly CurrentUser $currentUser,
        private readonly AuthorizationAgent $authorizationAgent
    ) {
    }

    /**
     * @param string $attribute
     * @param mixed  $subject
     */
    protected function supports($attribute, $subject): bool
    {
        if (
            !in_array(
                $attribute,
                [
                    self::CREATE,
                    self::EDIT,
                ]
            )
        ) {
            return false;
        }

        if (self::CREATE !== $attribute && !$subject instanceof Consultation) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param mixed  $subject
     *
     * @throws Atexo_Consultation_Exception
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        if (!$this->currentUser->isAgent()) {
            return false;
        }
        /** @var Consultation $consultation */
        $consultation = $subject;

        return match ($attribute) {
            self::CREATE => $this->canCreate(),
            self::EDIT => $this->canEdit($consultation),
            default => false,
        };
    }

    private function canCreate(): bool
    {
        return $this->currentUser->checkHabilitation('administrerProceduresSimplifiees');
    }

    /**
     * @throws Atexo_Consultation_Exception
     */
    private function canEdit(Consultation $consultation): bool
    {
        return $this->canCreate() && $this->authorizationAgent->isAuthorized($consultation->getId());
    }
}
