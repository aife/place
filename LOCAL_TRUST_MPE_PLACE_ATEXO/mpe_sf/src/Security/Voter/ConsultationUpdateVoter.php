<?php

namespace App\Security\Voter;

use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\HabilitationAgent;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Repository\ConfigurationOrganismeRepository;
use App\Service\Consultation\ConsultationService;
use App\Service\CurrentUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ConsultationUpdateVoter extends Voter
{
    use VoterTrait;

    final public const EDIT = 'CONSULTATION_UPDATE';

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ParameterBagInterface $parameterBag,
        private readonly ConfigurationOrganismeRepository $configurationOrganismeRepository,
        private readonly CurrentUser $currentUser,
        private readonly ConsultationService $consultationService
    ) {
    }

    protected function supports($attribute, $subject): bool
    {
        return $attribute === self::EDIT && $subject instanceof Consultation;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        /** @var ?Consultation $subject */

        /** @var Agent $user */
        $user = $token->getUser();

        $typeProcedure = $this->entityManager->getRepository(TypeProcedureOrganisme::class)
            ->findOneBy(
                [
                    'idTypeProcedure' => $subject->getIdTypeProcedureOrg(),
                    'organisme' => $subject->getOrganisme()->getAcronyme(),
                ]
            );

        return $this->checkConsultationStatus(
            $attribute,
            $this->consultationService->getStatus($subject, false),
            $user,
            $typeProcedure
        );
    }
}
