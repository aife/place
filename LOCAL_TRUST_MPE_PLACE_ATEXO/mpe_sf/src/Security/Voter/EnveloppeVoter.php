<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\Voter;

use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Enveloppe;
use App\Entity\EnveloppeFichier;
use App\Entity\Inscrit;
use App\Service\Authorization\AuthorizationAgent;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class EnveloppeVoter extends Voter
{
    public const VIEW = 'VIEW_ENVELOPPE';
    public const CREATE = 'CREATE_ENVELOPPE';
    public function __construct(private ParameterBagInterface $parameterBag, private AuthorizationAgent $authorizationAgent)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [self::VIEW, self::CREATE])
            && ($subject instanceof Enveloppe || $subject instanceof EnveloppeFichier);
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        if (self::VIEW === $attribute) {
            if ($token->getUser() instanceof Agent) {
                return $this->isOffreOpen($subject)
                && $this->authorizationAgent->isInTheScope($this->getSubjectConsultationId($subject));
            }

            if ($token->getUser() instanceof Inscrit) {
                return $token?->getUser()?->getEntreprise()?->getId() === $this->getSubjectEnterpriseId($subject);
            }
        }

        if (self::CREATE === $attribute) {
            return $this->isConsultationOnline($subject);
        }

        return false;
    }

    private function isConsultationOnline(Enveloppe|EnveloppeFichier $subject): bool
    {
        if ($subject instanceof Enveloppe) {
            $consultation = $subject?->getOffre()?->getConsultation();
        } else {
            $consultation = $subject?->getEnveloppe()?->getOffre()?->getConsultation();
        }

        if (! $consultation instanceof Consultation) {
            return false;
        }

        if (null === $consultation->getDateMiseEnLigneCalcule() || $consultation->getDateFinUnix() < time()) {
            return false;
        }

        return $consultation->getDateMiseEnLigneCalcule()->getTimestamp() <= time();
    }

    private function isOffreOpen(Enveloppe|EnveloppeFichier $subject): bool
    {
        $enveloppe = $subject;
        if ($subject instanceof EnveloppeFichier) {
            $enveloppe = $subject?->getEnveloppe();
        }

        if (! $enveloppe instanceof Enveloppe) {
            return false;
        }

        return in_array($enveloppe->getStatutEnveloppe(), [
            $this->parameterBag->get('STATUT_ENV_OUVERTE_EN_LIGNE'),
            $this->parameterBag->get('STATUT_ENV_OUVERTE_HORS_LIGNE'),
            $this->parameterBag->get('STATUT_ENV_OUVERTE'),
            $this->parameterBag->get('STATUT_ENV_OUVERTE_A_DISTANCE'),
        ]);
    }

    private function getSubjectConsultationId(Enveloppe|EnveloppeFichier $subject): ?int
    {
        if ($subject instanceof EnveloppeFichier) {
            return $subject?->getEnveloppe()?->getOffre()?->getConsultation()?->getId();
        }

        return $subject?->getOffre()?->getConsultation()?->getId();
    }

    private function getSubjectEnterpriseId(Enveloppe|EnveloppeFichier $subject): ?int
    {
        if ($subject instanceof EnveloppeFichier) {
            return $subject?->getEnveloppe()?->getOffre()?->getEntrepriseId();
        }

        return $subject?->getOffre()?->getEntrepriseId();
    }
}
