<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\Voter;

use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\HabilitationAgent;
use App\Entity\Lot;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Service\CurrentUser;
use Symfony\Component\Security\Core\User\UserInterface;

trait VoterTrait
{
    public function __construct(private readonly CurrentUser $currentUser)
    {
    }

    public function checkConsultationStatus(
        string $attribute,
        ?string $calculatedStatus,
        Agent $user,
        TypeProcedureOrganisme $typeProcedure
    ): bool {
        $statutConsultation = explode(',', $calculatedStatus);

        if (in_array($attribute, [ConsultationUpdateVoter::EDIT, LotVoter::EDIT])) {
            if (
                in_array($this->parameterBag->get('STATUS_ELABORATION'), $statutConsultation) ||
                in_array($this->parameterBag->get('STATUS_PREPARATION'), $statutConsultation)
            ) {
                return $this->currentUser->checkHabilitation('modifierConsultationAvantValidation');
            } elseif (in_array($this->parameterBag->get('STATUS_CONSULTATION'), $statutConsultation)) {
                return $this->hasHabilitationModifierApresValidation($typeProcedure);
            } elseif (
                in_array($this->parameterBag->get('STATUS_OUVERTURE_ANALYSE'), $statutConsultation) ||
                in_array($this->parameterBag->get('STATUS_DECISION'), $statutConsultation)
            ) {
                if (!empty($user->getOrganisme())) {
                    $configOrganisme = $this->configurationOrganismeRepository
                        ->find($user->getOrganisme()->getAcronyme());
                }

                if (
                    empty($configOrganisme) ||
                    $configOrganisme->getAutoriserModificationApresPhaseConsultation() == '1'
                ) {
                    return $this->hasHabilitationModifierApresValidation($typeProcedure);
                }
            }
        }

        return false;
    }

    public function hasHabilitationModifierApresValidation(
        TypeProcedureOrganisme $typeProcedure
    ): bool {
        $hasHabilitation = false;
        if ($typeProcedure) {
            if ($typeProcedure->getMapa() == $this->parameterBag->get('IS_MAPA')) {
                if (
                    (
                        ($typeProcedure->getIdMontantMapa() == $this->parameterBag->get('MONTANT_MAPA_INF_90'))
                        &&
                        $this->currentUser
                            ->checkHabilitation('modifierConsultationMapaInferieurMontantApresValidation')
                    )
                    ||
                    (
                        ($typeProcedure->getIdMontantMapa() == $this->parameterBag->get('MONTANT_MAPA_SUP_90'))
                        &&
                        $this->currentUser
                            ->checkHabilitation('modifierConsultationMapaSuperieurMontantApresValidation')
                    )
                ) {
                    $hasHabilitation = true;
                }
            } elseif (
                $this->currentUser
                    ->checkHabilitation('modifierConsultationProceduresFormaliseesApresValidation')
            ) {
                $hasHabilitation = true;
            }
        }

        return $hasHabilitation;
    }

    private function hasCreateConsultationHabilitation(UserInterface $user, Consultation|Lot|null $subject = null): bool
    {
        return
            empty($this->requestStack->getCurrentRequest()->get('consultationParentId'))
            && (
                $this->currentUser->checkHabilitation('gererMapaInferieurMontant')
                || $this->currentUser->checkHabilitation('gererMapaSuperieurMontant')
                || $this->currentUser->checkHabilitation('administrerProceduresFormalisees')
                || $user->isTechnique() && $this->currentUser->checkHabilitation('creerConsultation')
            );
    }
}
