<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\Voter;

use App\Entity\Agent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class SwitchAgentVoter extends Voter
{
    public function __construct(private readonly Security $security, private readonly EntityManagerInterface $em, private readonly RequestStack $requestStack)
    {
    }

    protected function supports($attribute, $subject)
    {
        return in_array($attribute, ['CAN_SWITCH_USER'])
            && $subject instanceof Agent;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        // if the user is anonymous or if the subject is not a Agent, do not grant access
        if (!$user instanceof Agent || !$subject instanceof Agent) {
            return false;
        }

        if ($this->security->isGranted('ROLE_ALLOWED_TO_SWITCH')) {
            // Check account requested is part of assoc account of the Agent connected
            $session = $this->requestStack->getCurrentRequest()->getSession();

            $accountsAssoc = $session->get('accounts_agent_assoc');
            if (!$accountsAssoc) {
                return false;
            }

            if (
                array_key_exists($subject->getId(), $accountsAssoc)
                && array_key_exists($user->getId(), $accountsAssoc)
            ) {
                return true;
            }
        }

        return false;
    }
}
