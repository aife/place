<?php

namespace App\Security\Voter;

use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Entity\Lot;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Repository\ConfigurationOrganismeRepository;
use App\Service\CurrentUser;
use App\Service\WebservicesMpeConsultations;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class LotVoter extends Voter
{
    use VoterTrait;

    final public const CREATE  = 'LOT_CREATE';
    final public const EDIT    = 'LOT_UPDATE';

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ParameterBagInterface $parameterBag,
        private readonly ConfigurationOrganismeRepository $configurationOrganismeRepository,
        private readonly WebservicesMpeConsultations $wsConsultations,
        private readonly CurrentUser $currentUser,
        private readonly RequestStack $requestStack
    ) {
    }

    protected function supports($attribute, $subject): bool
    {
        $supportsAttribute = in_array($attribute, [self::CREATE, self::EDIT]);
        $supportsSubject = empty($subject) || $subject instanceof Lot;

        return $supportsAttribute && $supportsSubject;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        /** @var ?Lot $subject */

        /** @var Agent $user */
        $user = $token->getUser();

        if ($attribute == self::CREATE) {
            return $this->hasCreateConsultationHabilitation($user);
        } else {
            $wsResponse = $this->wsConsultations->getContent(
                'searchConsultationsById',
                $subject->getConsultation()->getId(),
                false
            );

            $consultation = json_decode(json_encode($wsResponse), true);
            $typeProcedure =  $this->entityManager->getRepository(TypeProcedureOrganisme::class)
                ->findOneBy(
                    [
                        'idTypeProcedure' => $subject->getConsultation()->getIdTypeProcedureOrg(),
                        'organisme' => $subject->getConsultation()->getOrganisme()->getAcronyme()
                    ]
                );
            return $this->checkConsultationStatus(
                $attribute,
                $consultation['statutCalcule'],
                $user,
                $typeProcedure
            );
        }
    }
}
