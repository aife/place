<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security;

use App\Entity\Agent;
use App\Entity\Inscrit;
use Symfony\Component\Security\Core\User\UserInterface;

trait FindUserTrait
{
    private function findUserByLogin(string $userType, string $login): ?UserInterface
    {
        $user = null;
        if (GuardManager::USER_TYPE_AGENT === $userType) {
            $user = $this->em->getRepository(Agent::class)->findOneBy(['login' => $login]);
        } elseif (GuardManager::USER_TYPE_ENTREPRISE === $userType) {
            $user = $this->em->getRepository(Inscrit::class)->findOneBy(['login' => $login]);
        }

        return $user;
    }
}
