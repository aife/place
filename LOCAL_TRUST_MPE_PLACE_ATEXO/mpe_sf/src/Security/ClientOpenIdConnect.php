<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Security;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ClientOpenIdConnect
{
    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var string
     */
    private $refreshToken;

    public function __construct(private readonly HttpClientInterface $httpClient)
    {
    }

    /**
     * @throws HttpExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function logIn(string $url, array $headers, array $body)
    {
        try {
            $request = $this->httpClient->request('POST', $url, [
                'headers' => $headers,
                'body' => $body
            ]);

            $response = $request->toArray();

            $this->accessToken = $response['access_token'];
            $this->refreshToken = $response['refresh_token'];
        } catch (HttpExceptionInterface $e) {
            throw $e;
        }
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }
}
