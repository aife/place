<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\PasswordEncoder;

use Symfony\Component\PasswordHasher\PasswordHasherInterface;

class LegacySha256PasswordEncoder implements PasswordHasherInterface
{
    public function __construct(private readonly string $sha256Enabled = '1', private readonly string $salt = '')
    {
    }

    /**
     * @inheritDoc
     */
    public function encodePassword($raw, $salt = '')
    {
        return hash('sha256', (empty($salt) ? $this->salt : $salt) . $raw);
    }

    /**
     * @inheritDoc
     */
    public function isPasswordValid($encoded, $raw, $salt = '')
    {
        return $this->sha256Enabled && $encoded === $this->encodePassword($raw, $salt);
    }

    /**
     * @inheritDoc
     */
    public function needsRehash(string $encoded): bool
    {
        return true;
    }

    public function hash(string $plainPassword): string
    {
        // TODO: Implement hash() method.
        return $this->encodePassword($plainPassword);
    }

    public function verify(string $hashedPassword, string $plainPassword): bool
    {
        // TODO: Implement verify() method.
        return $this->isPasswordValid($hashedPassword, $plainPassword);
    }
}
