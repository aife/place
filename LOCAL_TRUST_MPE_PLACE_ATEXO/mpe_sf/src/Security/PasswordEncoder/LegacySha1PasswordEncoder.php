<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\PasswordEncoder;

use Symfony\Component\PasswordHasher\PasswordHasherInterface;

class LegacySha1PasswordEncoder implements PasswordHasherInterface
{
    /**
     * @inheritDoc
     */
    public function encodePassword($raw, $salt = '')
    {
        return hash('sha1', $raw);
    }

    /**
     * @inheritDoc
     */
    public function isPasswordValid($encoded, $raw, $salt = '')
    {
        return $encoded === $this->encodePassword($raw, $salt);
    }

    /**
     * @inheritDoc
     */
    public function needsRehash(string $encoded): bool
    {
        return true;
    }

    public function hash(string $plainPassword): string
    {
        // TODO: Implement hash() method.
        return $this->encodePassword($plainPassword);
    }

    public function verify(string $hashedPassword, string $plainPassword): bool
    {
        // TODO: Implement verify() method.
        return $this->isPasswordValid($hashedPassword, $plainPassword);
    }
}
