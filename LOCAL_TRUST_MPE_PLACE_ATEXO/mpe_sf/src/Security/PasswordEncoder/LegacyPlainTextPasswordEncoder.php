<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\PasswordEncoder;

use Symfony\Component\PasswordHasher\Hasher\PlaintextPasswordHasher;

class LegacyPlainTextPasswordEncoder extends PlaintextPasswordHasher
{
    /**
     * @param bool|null $ignorePasswordCase
     */
    public function __construct(?bool $ignorePasswordCase = false)
    {
        parent::__construct($ignorePasswordCase ?? false);
    }
}
