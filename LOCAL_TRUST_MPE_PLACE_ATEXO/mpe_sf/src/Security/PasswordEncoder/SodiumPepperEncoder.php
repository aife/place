<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\PasswordEncoder;

use Symfony\Component\PasswordHasher\PasswordHasherInterface;
use Symfony\Component\PasswordHasher\Hasher\SodiumPasswordHasher;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class SodiumPepperEncoder implements PasswordHasherInterface
{
    private SodiumPasswordHasher $passwordHasher;

    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly LegacySha1PasswordEncoder $legacySha1,
        private readonly LegacySha256PasswordEncoder $legacySha256
    )
    {
        $this->passwordHasher = new SodiumPasswordHasher(
            $this->parameterBag->get('ARGON_MEMORY_COST'),
            $this->parameterBag->get('ARGON_MEMORY_COST')
        );
    }

    public function encodePassword($raw, $salt = '')
    {
        if (null !== $salt && '' !== $salt) {
            $raw .= $salt;
        } else {
            $raw .= $this->parameterBag->get('SODIUM_PEPPER');
        }

        return $this->passwordHasher->hash($raw, $salt); // TODO : to verify
    }

    public function isPasswordValid($encoded, $raw, $salt = '')
    {
        $plainTextPassword = $raw;
        if (null !== $salt && '' !== $salt) {
            $plainTextPassword .= $salt;
        } else {
            $plainTextPassword .= $this->parameterBag->get('SODIUM_PEPPER');
        }

        // TODO : to verify
        if ( true === $this->passwordHasher->verify($encoded, $plainTextPassword , $salt)) {
            return true;
        }

        if (true === $this->needsRehash($encoded)) {
            foreach ([$this->legacySha1, $this->legacySha256] as $legacyEncoder) {
                if ($legacyEncoder->isPasswordValid($encoded, $raw , $salt)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function needsRehash(string $encoded): bool
    {
        return $this->passwordHasher->needsRehash($encoded);
    }

    public function hash(string $plainPassword): string
    {
        // TODO: Implement hash() method.
        return $this->encodePassword($plainPassword);
    }

    public function verify(string $hashedPassword, string $plainPassword): bool
    {
        // TODO: Implement verify() method.
        return $this->isPasswordValid($hashedPassword, $plainPassword);
    }
}
