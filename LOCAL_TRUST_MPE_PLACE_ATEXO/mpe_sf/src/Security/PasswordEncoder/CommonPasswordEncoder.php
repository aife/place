<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\PasswordEncoder;

use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;

class CommonPasswordEncoder
{
    public function __construct(private PasswordHasherFactoryInterface $encoderFactory)
    {
    }

    public function encodePassword(string $userClassName, string $plainPassword): string
    {
        $hasher = $this->encoderFactory->getPasswordHasher($userClassName);

        return $hasher->hash($plainPassword);
    }

    public function isPasswordValid(string $userClassName, string $encodedPassword, string $plainPassword): bool
    {
        $hasher = $this->encoderFactory->getPasswordHasher($userClassName);

        return $hasher->verify($encodedPassword, $plainPassword);
    }
}
