<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\Provider;

use App\Entity\Administrateur;
use App\Entity\Agent;
use App\Entity\Inscrit;
use App\Repository\AdministrateurRepository;
use App\Repository\AgentRepository;
use App\Repository\InscritRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Security\User\PayloadAwareUserProviderInterface;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method UserInterface loadUserByIdentifierAndPayload(string $identifier, array $payload)
 * @method UserInterface loadUserByIdentifier(string $identifier)
 */
class ApiProvider implements PayloadAwareUserProviderInterface
{
    public function __construct(
        private InscritRepository $inscritRepository,
        private AgentRepository $agentRepository,
        private AdministrateurRepository $adminRepository
    ) {
    }

    public function loadUserByUsernameAndPayload(string $username, array $payload)
    {
        $user = null;

        if (isset($payload['roles']) && in_array('ROLE_ENTREPRISE', $payload['roles'])) {
            $user = $this->inscritRepository->findOneBy(['login' => $username]);
        }

        if (isset($payload['roles']) && in_array('ROLE_AGENT', $payload['roles'])) {
            $user = $this->agentRepository->findOneBy(['login' => $username]);
        }

        if (
            isset($payload['roles'])
            && 1 === count(array_diff(['ROLE_SUPER_ADMIN', 'ROLE_ADMIN'], $payload['roles']))
        ) {
            $user = $this->adminRepository->findOneBy(['login' => $username]);
        }

        if (null === $user) {
            throw new UserNotFoundException();
        }

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        return $user;
    }

    public function supportsClass(string $class)
    {
        return in_array($class, [Inscrit::class, Agent::class, Administrateur::class]);
    }

    public function loadUserByUsername(string $username)
    {
        return $this->loadUserByUsernameAndPayload($username, []);
    }
}
