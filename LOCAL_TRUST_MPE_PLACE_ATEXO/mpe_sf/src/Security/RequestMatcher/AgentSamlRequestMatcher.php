<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\RequestMatcher;

use App\Entity\ConfigurationPlateforme;
use App\Security\GuardManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;

class AgentSamlRequestMatcher implements RequestMatcherInterface
{
    public function __construct(
        private readonly GuardManager $guardManager,
        private readonly EntityManagerInterface $em
    ) {
    }

    /**
     * @inheritDoc
     */
    public function matches(Request $request): bool
    {
        /** @var ConfigurationPlateforme $configurationPlatform */
        $configurationPlatform = $this->em
            ->getRepository(ConfigurationPlateforme::class)
            ->getConfigurationPlateforme();

        return ('1' === $configurationPlatform->getAuthenticateAgentSaml())
            && (
                $this->guardManager->isAgent($request)
                || $request->getPathInfo() === '/saml/acs'
            );
    }
}
