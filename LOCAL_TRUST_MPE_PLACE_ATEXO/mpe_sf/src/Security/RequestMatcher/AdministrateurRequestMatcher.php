<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\RequestMatcher;

use App\Security\GuardManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;

class AdministrateurRequestMatcher implements RequestMatcherInterface
{
    public function __construct(private readonly GuardManager $guardManager)
    {
    }

    /**
     * @inheritDoc
     */
    public function matches(Request $request): bool
    {
        return $this->guardManager->isAdministrateur($request);
    }
}
