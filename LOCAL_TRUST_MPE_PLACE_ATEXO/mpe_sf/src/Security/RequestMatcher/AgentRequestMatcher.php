<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\RequestMatcher;

use App\Security\GuardManager;
use App\Security\OpenidKeycloakAuthenticator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;

class AgentRequestMatcher implements RequestMatcherInterface
{
    public function __construct(
        private readonly GuardManager $guardManager,
        private readonly OpenidKeycloakAuthenticator $openidKeycloakAuthenticator
    ) {
    }

    /**
     * @inheritDoc
     */
    public function matches(Request $request): bool
    {
        $credentials = [];

        if ($request->getPathInfo() === '/login/keycloak/check') {
            $credentials = $this->openidKeycloakAuthenticator->getCredentials($request);
        }

        return $this->guardManager->isAgent($request)
            || (
                !empty($credentials)
                && array_key_exists('user_type', $credentials)
                && $credentials['user_type'] == GuardManager::USER_TYPE_AGENT
            )
            || (
                $request->getPathInfo() === '/login/microsoft/check'
                && $request->query->has('user_type')
                && $request->query->get('user_type') === GuardManager::USER_TYPE_AGENT
            )
            || (
                $request->getPathInfo() === '/login/sso/check'
                && $request->server->has('user-type')
                && $request->server->get('user-type') === GuardManager::USER_TYPE_AGENT
            )
            || (
                $request->getPathInfo() === '/login/sso/shibboleth/check'
                && $request->server->has('HTTP_USERTYPE')
                && strtolower($request->server->get('HTTP_USERTYPE')) === GuardManager::USER_TYPE_AGENT
            );
    }
}
