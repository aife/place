<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\RequestMatcher;

use App\Security\GuardManager;
use App\Security\OpenidKeycloakAuthenticator;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;

class InscritRequestMatcher implements RequestMatcherInterface
{
    public function __construct(private readonly GuardManager $guardManager, private readonly OpenidKeycloakAuthenticator $openidKeycloakAuthenticator)
    {
    }

    /**
     * @inheritDoc
     */
    public function matches(Request $request): bool
    {
        $credentials = [];

        if ($request->getPathInfo() === '/login/keycloak/check') {
            $credentials = $this->openidKeycloakAuthenticator->getCredentials($request);
        }

        return $this->guardManager->isEntreprise($request)
            || (
                array_key_exists('user_type', $credentials)
                && $credentials['user_type'] == GuardManager::USER_TYPE_ENTREPRISE
            )
            || (
                $request->getPathInfo() === '/login/microsoft/check'
                && $request->query->has('user_type')
                && $request->query->get('user_type') === GuardManager::USER_TYPE_ENTREPRISE
            )
            || (
                $request->getPathInfo() === '/login/sso/check'
                && $request->server->has('user-type')
                && $request->server->get('user-type') === GuardManager::USER_TYPE_ENTREPRISE
            );
    }
}
