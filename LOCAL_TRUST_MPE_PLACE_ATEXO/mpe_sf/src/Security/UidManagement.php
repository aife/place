<?php

namespace App\Security;

use Exception;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class UidManagement de gestion de l'unique ID.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @since ESR2017
 *
 * @copyright Atexo 2017
 */
class UidManagement
{
    /**
     * UidManagement constructor.
     *
     * @param $prefixe
     * @param $session
     */
    public function __construct(private readonly SessionInterface $session, private readonly LoggerInterface $logger, private $prefixe = '')
    {
    }

    /**
     * Permet de generer un unique ID.
     *
     * @param int  $consultationId
     * @param bool $withSessionId  : true si uid prefixe par session_id, false sinon
     * @param bool $customPrefix   : true si uid prefixe par custom prefix ($this->prefixe), false sinon
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2017
     *
     * @copyright Atexo 2017
     */
    public function generateUid($consultationId, $withSessionId = true, $customPrefix = false)
    {
        try {
            $prefix = ((true === $customPrefix) ? $this->prefixe : '').((true === $withSessionId) ? $this->session->getId() : '');
            $uuid1 = Uuid::uuid1();
            $uidResponse = $prefix.'-'.$uuid1->toString().'-'.uniqid('', true);

            $this->session->set('refConsultation'.$uidResponse, $consultationId);
            $this->session->set('uid'.$consultationId, $uidResponse);

            return $uidResponse;
        } catch (UnsatisfiedDependencyException|Exception $e) {
            $this->logger->error('Error generation uid: '.$e->getMessage());
        }
    }
}
