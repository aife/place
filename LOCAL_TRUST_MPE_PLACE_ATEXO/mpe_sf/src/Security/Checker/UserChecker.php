<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security\Checker;

use App\Entity\Agent;
use App\Entity\Inscrit;
use App\Security\Exception\AccountDisabledException;
use App\Security\Exception\AccountNotAuthorizedException;
use App\Service\MpeSecurity;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
    public function __construct(private readonly MpeSecurity $mpeSecurity)
    {
    }

    /**
     * @inheritDoc
     */
    public function checkPreAuth(UserInterface $user): void
    {
        if (!$this->mpeSecurity->isUserActive($user)) {
            throw new AccountDisabledException();
        }

        if ($this->mpeSecurity->isTechnicalAgent($user)) {
            throw new AccountNotAuthorizedException();
        }
    }

    /**
     * @inheritDoc
     */
    public function checkPostAuth(UserInterface $user): void
    {
        if ($user instanceof Agent || $user instanceof Inscrit) {
            $this->mpeSecurity->resetTentativesMdp($user);
            $this->mpeSecurity->activerSynchronisationSgmap($user);
            $this->mpeSecurity->updateConnectionTime($user);
        }
    }
}
