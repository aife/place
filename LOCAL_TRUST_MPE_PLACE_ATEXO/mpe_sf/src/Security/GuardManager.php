<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security;

use App\Entity\Agent;
use App\Entity\ConfigurationPlateforme;
use App\Entity\Inscrit;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;

class GuardManager
{
    public final const USER_TYPE_AGENT = 'agent';
    public final const USER_TYPE_ENTREPRISE = 'inscrit';
    public final const USER_TYPE_ADMIN = 'administrateur';
    public final const USER_TYPE_SUPERADMIN = 'superadmin';

    public function __construct(
        private readonly EntityManagerInterface $em,
        /** ParameterBagInterface */
        private readonly ParameterBagInterface $parameterBag,
        private readonly Security $security,
        private readonly RouterInterface $router
    ) {
    }

    public function getMainGuard(Request $request): ?string
    {
        /** @var ConfigurationPlateforme $configurationPlatform */
        $configurationPlatform = $this->em
            ->getRepository(ConfigurationPlateforme::class)
            ->getConfigurationPlateforme();

        $mainGuard = null;

        if ($this->isAgent($request)) {
            if ('1' === $configurationPlatform->getAuthenticateAgentOpenidKeycloak()) {
                $mainGuard = OpenidKeycloakAuthenticator::class;
            } elseif ('1' === $configurationPlatform->getAuthenticateAgentOpenidMicrosoft()) {
                $mainGuard = OpenidMicrosoftAuthenticator::class;
            } elseif (
                '1' === $configurationPlatform->getSocleExterneAgent()
                || '1' === $configurationPlatform->getSocleExterneEntreprise()
            ) {
                $mainGuard = ExternalSsoAuthenticator::class;
                if($this->parameterBag->get('AUTHENTIFICATION_AGENT_SHIBBOLETH')) {
                    $mainGuard = ShibbolethAuthenticator::class;
                }
            } elseif ('1' === $configurationPlatform->getAuthenticateAgentByCert()) {
                $mainGuard = CertificateAuthenticator::class;
            } elseif ('1' === $configurationPlatform->getAuthenticateAgentByLogin()) {
                $mainGuard = FormAuthenticator::class;
            }
        } elseif ($this->isEntreprise($request)) {
            if ('1' === $configurationPlatform->getAuthenticateInscritOpenidKeycloak()) {
                $mainGuard = OpenidKeycloakAuthenticator::class;
            } elseif ('1' === $configurationPlatform->getAuthenticateInscritOpenidMicrosoft()) {
                $mainGuard = OpenidMicrosoftAuthenticator::class;
            } elseif (
                '1' === $configurationPlatform->getSocleExterneAgent()
                || '1' === $configurationPlatform->getSocleExterneEntreprise()
            ) {
                $mainGuard = ExternalSsoAuthenticator::class;
            } elseif ('1' === $configurationPlatform->getAuthenticateInscritByLogin()) {
                $mainGuard = FormAuthenticator::class;
            }
        } elseif ($this->isAdministrateur($request) || $this->isSuperAdmin($request)) {
            $mainGuard = FormAuthenticator::class;
        }

        return $mainGuard;
    }

    public function isGuardEnabled(string $userType, string $guardClass): bool
    {
        /** @var ConfigurationPlateforme $configurationPlatform */
        $configurationPlatform = $this->em
            ->getRepository(ConfigurationPlateforme::class)
            ->getConfigurationPlateforme();

        $enable = false;

        if (self::USER_TYPE_AGENT == $userType) {
            if (OpenidKeycloakAuthenticator::class === $guardClass) {
                $enable = (bool)$configurationPlatform->getAuthenticateAgentOpenidKeycloak();
            } elseif (OpenidMicrosoftAuthenticator::class === $guardClass) {
                $enable = (bool)$configurationPlatform->getAuthenticateAgentOpenidMicrosoft();
            } elseif (ExternalSsoAuthenticator::class === $guardClass || ShibbolethAuthenticator::class === $guardClass ) {
                $enable = (bool)$configurationPlatform->getSocleExterneAgent();
            } elseif (FormAuthenticator::class === $guardClass) {
                $enable = (bool)$configurationPlatform->getAuthenticateAgentByLogin();
            }
        } elseif (self::USER_TYPE_ENTREPRISE == $userType) {
            if (OpenidKeycloakAuthenticator::class === $guardClass) {
                $enable = (bool)$configurationPlatform->getAuthenticateInscritOpenidKeycloak();
            } elseif (OpenidMicrosoftAuthenticator::class === $guardClass) {
                $enable = (bool)$configurationPlatform->getAuthenticateInscritOpenidMicrosoft();
            } elseif (ExternalSsoAuthenticator::class === $guardClass) {
                $enable = (bool)$configurationPlatform->getSocleExterneEntreprise();
            } elseif (FormAuthenticator::class === $guardClass) {
                $enable = (bool)$configurationPlatform->getAuthenticateInscritByLogin();
            }
        } elseif (in_array($userType, [self::USER_TYPE_ADMIN, self::USER_TYPE_SUPERADMIN])) {
            $enable = (FormAuthenticator::class === $guardClass);
        }

        return $enable;
    }

    public function getRedirectUrl(Request $request, bool $checkSocleInterne = false): string
    {
        /** @var ConfigurationPlateforme $configurationPlatform */
        $configurationPlatform = $this->em
            ->getRepository(ConfigurationPlateforme::class)
            ->getConfigurationPlateforme();

        //On check si la plateforme a un SSO
        if ('1' === $configurationPlatform->getSocleExterneAgent()
            || '1' === $configurationPlatform->getSocleExterneEntreprise()
            || '1' === $configurationPlatform->getSocleExternePpp()
            || '1' === $configurationPlatform->getSocleInterne()
        ) {
            $checkSocleInterne = true;
        }

        $path = '/';
        if ($this->isEntreprise($request)) {
            $referer = $request->server->get('HTTP_REFERER', '/');
            $domain = $request->server->get('HTTP_HOST');
            $host = parse_url($referer, PHP_URL_HOST);

            if ($host !== $domain && !$checkSocleInterne) {
                return $this->router->generate('mpe_logout_entreprise');
            }
            $baseUrl = $request->getSchemeAndHttpHost();
            $path = (!empty($baseUrl) && str_starts_with($referer, $baseUrl)) ? $referer : '/';
        } elseif ($this->isAgent($request)) {
            $path = '/agent/login';
        } elseif ($this->isAdministrateur($request)) {
            $path = '/admin/login';
        } elseif ($this->isSuperAdmin($request)) {
            $path = '/superadmin/login';
        }

        return $path;
    }

    public function getLogoutUrl(Request $request): string
    {
        $baseUrl = $request->getSchemeAndHttpHost() . '/';
        $currentUrl = $request->getSchemeAndHttpHost() . $request->getRequestUri();

        $redirectUri = $baseUrl;

        if (
            str_starts_with($currentUrl, $baseUrl . 'agent')
            || str_starts_with($currentUrl, $baseUrl . 'index.php/agent')
        ) {
            $redirectUri = $this->router
                ->generate('mpe_agent_auth', [], UrlGeneratorInterface::ABSOLUTE_URL);
        } elseif (
            str_starts_with($currentUrl, $baseUrl . 'entreprise')
            || str_starts_with($currentUrl, $baseUrl . 'index.php/entreprise')
        ) {
            $redirectUri = $this->router
                ->generate('accueil_entreprise', [], UrlGeneratorInterface::ABSOLUTE_URL);
        }

        if ($this->getMainGuard($request) === OpenidKeycloakAuthenticator::class) {
            if (!empty($this->parameterBag->get('URL_PPP_OPENSSO_LOGOUT'))) {
                $redirectUri = $this->parameterBag->get('URL_PPP_OPENSSO_LOGOUT');
            } else {
                $params = [
                    'redirect_uri' => $baseUrl,
                    'session' => 'logout'
                ];

                $redirectUri = $this->parameterBag->get('OAUTH_KEYCLOAK_AUTH_SERVER_URL')
                    . '/realms/' . $this->parameterBag->get('OAUTH_KEYCLOAK_REALM')
                    . '/protocol/openid-connect/logout'
                    . '?' . http_build_query($params);
            }
        } elseif ($this->getMainGuard($request) === OpenidMicrosoftAuthenticator::class) {
            $redirectUri = $this->parameterBag->get('OAUTH_MICROSOFT_URL_LOGOUT');
        } elseif ($this->getMainGuard($request) === ExternalSsoAuthenticator::class
            || ShibbolethAuthenticator::class === $this->getMainGuard($request)) {
            $redirectUri = $this->parameterBag->get('URL_INDEX_AGENT_DISCONECT');
            if (str_starts_with($currentUrl, $baseUrl.'entreprise')
                || str_starts_with($currentUrl, $baseUrl.'index.php/entreprise')) {
                $redirectUri = $this->parameterBag->get('URL_INDEX_ENTREPRISE_DISCONECT');
            }
        }

        return $redirectUri;
    }

    public function isEntreprise(Request $request): bool
    {
        $user = $this->security->getUser();

        return $user instanceof Inscrit
            || str_starts_with($request->getPathInfo(), '/entreprise')
            || (
                $request->getPathInfo() === '/'
                && $request->query->has('page')
                && mb_strpos($request->query->get('page'), 'Entreprise.') === 0
            )
            || (
                $request->getPathInfo() === '/'
                && $request->query->has('page')
                && mb_strpos($request->query->get('page'), 'Commun.') === 0
            );
    }

    public function isAgent(Request $request): bool
    {
        $user = $this->security->getUser();

        return $user instanceof Agent
            || str_starts_with($request->getPathInfo(), '/agent')
            || (
                $request->getPathInfo() === '/'
                && $request->query->has('page')
                && (
                    str_starts_with($request->query->get('page'), 'Agent.')
                    || str_starts_with($request->query->get('page'), 'GestionPub.')
                    || str_starts_with($request->query->get('page'), 'Commission.')
                )
            );
    }

    public function isAdministrateur(Request $request): bool
    {
        return str_starts_with($request->getPathInfo(), '/admin')
            || (
                $request->getPathInfo() === '/'
                && $request->query->has('page')
                && mb_strpos($request->query->get('page'), 'Administration.') === 0
            );
    }

    public function isSuperAdmin(Request $request): bool
    {
        return mb_strpos($request->getPathInfo(), '/superadmin/') === 0;
    }

    /**
     * @return mixed
     */
    public function decodeJwtToken(string $token)
    {
        $tokenParts = explode(".", $token);
        $tokenPayload = base64_decode($tokenParts[1]);

        return json_decode($tokenPayload, null);
    }

    public function getOpenidMapping(string $guardClassName): ?array
    {
        if ($guardClassName == OpenidKeycloakAuthenticator::class) {
            return $this->parameterBag->get('oauth_mapping')['keycloak'];
        } elseif ($guardClassName == OpenidMicrosoftAuthenticator::class) {
            return $this->parameterBag->get('oauth_mapping')['microsoft'];
        }

        return null;
    }
}
