<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Security;

use App\Repository\ConfigurationPlateformeRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Entity\Administrateur;
use App\Entity\Agent;
use App\Entity\Inscrit;
use App\Service\MpeSecurity;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Guard\PasswordAuthenticatedInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class FormAuthenticator extends AbstractGuardAuthenticator implements PasswordAuthenticatedInterface
{
    /**
     * @var string
     */
    public final const LOGINFORM = 'form';

    protected ?UserInterface $entity = null;

    /**
     * BasicAuthentificator constructor.
     */
    public function __construct(
        private CsrfTokenManagerInterface $csrfTokenManager,
        protected RouterInterface $router,
        private TranslatorInterface $translator,
        private MpeSecurity $mpeSecurity,
        private GuardManager $guardManager,
        private LoggerInterface $logger,
        private UserPasswordHasherInterface $passwordEncoder,
        private ConfigurationPlateformeRepository $configurationPlateformeRepository
    ) {
    }

    /**
     * @param AuthenticationException|null $authException
     */
    public function start(Request $request, AuthenticationException $authException = null): RedirectResponse
    {
        return new RedirectResponse($this->guardManager->getRedirectUrl($request));
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning `false` will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request): bool
    {
        $configPlatform = $this->configurationPlateformeRepository->getConfigurationPlateforme();
        $listPath = [
            '/entreprise/login',
            '/admin/login',
            '/superadmin/login',
        ];
        if (false === $configPlatform->isAuthenticateAgentByInternalKeycloak()) {
            $listPath[] = '/agent/login';
        }
        return
            in_array(
                $request->getPathInfo(),
                $listPath
            )
            && $request->request->has(self::LOGINFORM);
    }

    public function getCredentials(Request $request): array
    {
        $form = $request->request->get(self::LOGINFORM);

        $csrfToken = $request->request->get('_csrf_token');

        $userType = Inscrit::class;
        if ($this->guardManager->isAgent($request)) {
            $userType = Agent::class;
        } elseif ($this->guardManager->isAdministrateur($request) || $this->guardManager->isSuperAdmin($request)) {
            $userType = Administrateur::class;
        }

        return [
            'user_type' => $userType,
            'login' => $form['_username'] ?? null,
            'password' => $form['_password'] ?? null,
            'csrf_token' => $csrfToken,
        ];
    }

    /**
     * @param mixed $credentials
     */
    public function getUser($credentials, UserProviderInterface $userProvider): ?UserInterface
    {
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);

        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        return $userProvider->loadUserByIdentifier($credentials['login']);
    }

    /**
     * @param mixed $credentials
     */
    public function checkCredentials($credentials, UserInterface|PasswordAuthenticatedUserInterface $user): bool
    {
        $this->entity = $user;

        return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): RedirectResponse
    {
        $this->logger->debug('Erreur d\'authentification : ' . self::class);

        if (!empty($this->entity)) {
            $this->mpeSecurity->incrementTentativesMdp($this->entity);
        }

        $request->getSession()->getFlashBag()->add('error', $this->translator->trans('TEXT_COMPTE_ERRONNE'));

        return new RedirectResponse($this->guardManager->getRedirectUrl($request));
    }

    /**
     * @param string $providerKey
     * @return RedirectResponse|Response|null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return new RedirectResponse($this->guardManager->getRedirectUrl($request));
    }

    public function supportsRememberMe(): bool
    {
        return true;
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function getPassword($credentials): ?string
    {
        return $credentials['password'];
    }
}
