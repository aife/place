<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Security;

use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Entity\HabilitationProfil;
use App\Entity\Inscrit;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Service\MpeSecurity;
use Doctrine\ORM\Exception\ORMException;
use KnpU\OAuth2ClientBundle\Client\OAuth2ClientInterface;
use KnpU\OAuth2ClientBundle\Security\Authenticator\SocialAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Psr\Log\LoggerInterface;
use ReflectionClass;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class OpenidMicrosoftAuthenticator extends SocialAuthenticator
{
    use FindUserTrait;

    private ?array $mapping = null;

    public function __construct(
        private readonly ClientRegistry $clientRegistry,
        private readonly EntityManagerInterface $em,
        private readonly GuardManager $guardManager,
        /** MpeSecurity */
        private readonly MpeSecurity $mpeSecurity,
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag
    ) {
        $this->mapping = $guardManager->getOpenidMapping(self::class);
    }

    public function supports(Request $request): bool
    {
        return $request->attributes->get('_route') === 'mpe_auth_microsoft_check'
            && $request->query->has('code')
            && $request->query->has('state')
            && $request->query->has('user_type')
            && $this->guardManager->isGuardEnabled($request->query->get('user_type'), self::class);
    }

    public function getCredentials(Request $request): array
    {
        $userType = $request->query->get('user_type');

        return [
            'access_token' => $this->fetchAccessToken($this->getMicrosoftClient($userType)),
            'user_type' => $userType
        ];
    }

    /**
     * @throws ORMException
     */
    public function getUser($credentials, UserProviderInterface $userProvider): ?UserInterface
    {
        $userType = $credentials['user_type'];
        $jwtPayload = $this->guardManager->decodeJwtToken($credentials['access_token']);

        // Vérifie si l'utilisateur existe
        $user = $this->findUserByLogin($userType, $jwtPayload->{$this->mapping['login']});
        if ($user) {
            if (false === $this->mpeSecurity->isUserActive($user)) {
                return null;
            }

            return $user;
        }

        // Si l'utilisateur n'existe pas => on le crée
        return $this->createNewUser($userType, $jwtPayload);
    }

    private function getMicrosoftClient(string $userType): OAuth2ClientInterface
    {
        return $this->clientRegistry->getClient(sprintf("microsoft_%s", $userType));
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey): RedirectResponse
    {
        return new RedirectResponse($this->guardManager->getRedirectUrl($request));
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): Response
    {
        $this->logger->debug('Erreur d\'authentification : ' . self::class);

        $message = strtr($exception->getMessageKey(), $exception->getMessageData());

        return new Response($message, Response::HTTP_FORBIDDEN);
    }

    public function start(Request $request, AuthenticationException $authException = null): RedirectResponse
    {
        return new RedirectResponse($this->guardManager->getRedirectUrl($request));
    }

    /**
     * @throws ORMException
     */
    private function createNewUser(string $userType, object $token): ?UserInterface
    {
        $user = null;
        if (GuardManager::USER_TYPE_AGENT === $userType) {
            $serviceId = (int)$token->{$this->mapping['service_id']};
            $organimeAcronyme = $token->{$this->mapping['organism_alias']};
            /** @var ?Organisme $organisme */
            $organisme = $this->em->getRepository(Organisme::class)->findOneBy([
                'acronyme' => $organimeAcronyme
            ]);
            if (null === $organisme) {
                $organisme = $this->em->getRepository(Organisme::class)->findOneBy([
                    'acronyme' => $this->parameterBag->get('OAUTH_KEYCLOAK_SAML2_REFERENCE_ORGANISME')
                ]);
            }
            if (null === $organisme) {
                $this->logger->error('Cant create User - No organism found for acronyme: ' . $organimeAcronyme);

                return null;
            }

            $service = $this->em->getRepository(Service::class)->findOneBy([
                'id' => $serviceId,
                'organisme' => $organisme,
            ]);

            $password = random_int(0, mt_getrandmax());
            $habilitations = $this->createHabilitaionsAgent($token->{$this->mapping['profile_id']});
            $user = new Agent();
            $user->setLogin($token->{$this->mapping['login']})
                ->setPrenom($token->{$this->mapping['firstname']})
                ->setNom($token->{$this->mapping['lastname']})
                ->setEmail($token->{$this->mapping['email']})
                ->setPassword($password)
                ->setMdp($password)
                ->setOrganisme($organisme)
                ->setService($service)
                ->setHabilitation($habilitations);
            $habilitations->setAgent($user);
            $this->em->persist($user);
            $this->em->flush();
        }

        return $user;
    }

    private function createHabilitaionsAgent(?string $profileId): HabilitationAgent
    {
        $habilitations = new HabilitationAgent();

        if (!empty($profileId)) {
            $habilitationProfil = $this->em->getRepository(HabilitationProfil::class)->find((int)$profileId);
            if ($habilitationProfil) {
                $excludeMethods = ['getId', 'getLibelle', 'getRecensementProgrammationStrategieAchat'];
                $class = new ReflectionClass(HabilitationProfil::class);
                foreach ($class->getMethods() as $method) {
                    $methodName = $method->getName();
                    if (str_starts_with($methodName, 'get') && !in_array($methodName, $excludeMethods)) {
                        $setter = str_replace('get', 'set', $methodName);
                        if (method_exists($habilitations, $setter)) {
                            $habilitations->$setter($habilitationProfil->$methodName());
                        }
                    }
                }
            }
        }

        return $habilitations;
    }
}
