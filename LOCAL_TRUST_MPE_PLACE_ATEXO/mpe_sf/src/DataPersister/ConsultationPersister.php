<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Consultation;
use App\Event\ModificationConsultationEvent;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ConsultationPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(
        private readonly ContextAwareDataPersisterInterface $decorated,
        private readonly EventDispatcherInterface $dispatcher,
        private readonly ParameterBagInterface $parameterBag
    ) {
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof Consultation
            && $this->decorated->supports($data, $context)
            && $context['item_operation_name'] === 'patch';
    }

    public function persist($data, array $context = [])
    {
        $oldDate = $context['previous_data']?->getDatefin();
        $consultation = $this->decorated->persist($data, $context);

        if (
            (int) $consultation->getCalculatedStatus() === (int) $this->parameterBag->get('STATUS_CONSULTATION')
            && $consultation->getDatefin() != $oldDate
        ) {
            $event = new ModificationConsultationEvent($consultation->getId(), (int) $consultation->getCodeExterne());
            $this->dispatcher->dispatch($event, ModificationConsultationEvent::class);
        }

        return $consultation;
    }

    public function remove($data, array $context = [])
    {
        return $this->decorated->remove($data, $context);
    }
}
