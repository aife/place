<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataPersister;

use App\Entity\MediaUuid;
use Exception;
use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\BloborganismeFile;
use App\Service\AtexoFichierOrganisme;
use Doctrine\ORM\EntityManagerInterface;

final class BloborganismeFilePostDataPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly AtexoFichierOrganisme $atexoFichierOrganisme
    ) {
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof MediaUuid;
    }

    /**
     * @param MediaUuid $data
     * @return mixed|object|void
     * @throws Exception
     */
    public function persist($data, array $context = [])
    {
        $this->em->flush();

        $this->atexoFichierOrganisme->moveTmpFile($data->getMedia()->getChemin());

        $this->atexoFichierOrganisme->insertFile(
            fileName: $data->getMedia()->getName(),
            pathFile: $data->getMedia()->getChemin(),
            organisme: $data->getMedia()->getOrganisme(),
            blobFile: $data->getMedia()
        );

        return $data;
    }

    public function remove($data, array $context = [])
    {
        // TO DO: Implement remove() method.
    }
}
