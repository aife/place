<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\Consultation\DonneeComplementaire;
use App\Entity\FormePrix;
use App\Entity\FormePrixHasRefTypePrix;
use App\Entity\FormePrixPfHasRefVariation;
use App\Entity\FormePrixPuHasRefVariation;
use Doctrine\ORM\EntityManagerInterface;

class FormePrixPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(
        private ContextAwareDataPersisterInterface $decorated,
        private EntityManagerInterface $em
    ) {
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof FormePrix && $this->decorated->supports($data, $context);
    }

    public function persist($data, array $context = [])
    {
        $formePrix = $this->decorated->persist($data, $context);

        if (isset($data->variationPrixForfaitaire) && false !== $data->variationPrixForfaitaire) {
            $variationPf = new FormePrixPfHasRefVariation();
            $variationPf->setIdVariation($data->variationPrixForfaitaire)
                ->setIdFormePrix($formePrix->getIdFormePrix());
            $this->em->persist($variationPf);
        }

        if (isset($data->variationPrixUnaitaire) && false !== $data->variationPrixUnaitaire) {
            $variationPu = new FormePrixPuHasRefVariation();
            $variationPu->setIdVariation($data->variationPrixUnaitaire)
                ->setIdFormePrix($formePrix->getIdFormePrix());
            $this->em->persist($variationPu);
        }

        if (isset($data->typePrix) && count($data->typePrix)) {
            foreach ($data->typePrix as $idType) {
                $typePrix = new FormePrixHasRefTypePrix();
                $typePrix->setIdTypePrix($idType)
                    ->setIdFormePrix($formePrix->getIdFormePrix());
                $this->em->persist($typePrix);
            }
        }

        if (isset($data->donneesComplementaire) && null !== $data->donneesComplementaire) {
            $donneesComplementaire = $this->em->getRepository(DonneeComplementaire::class)->find($data->donneesComplementaire);
            $donneesComplementaire->setIdFormePrix($formePrix->getIdFormePrix());
        }

        $this->em->flush();
    }

    public function remove($data, array $context = [])
    {
        return $this->decorated->remove($data, $context);
    }
}
