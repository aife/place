<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Agent;
use App\Entity\Agent\AgentServiceMetier;
use App\Service\Agent\AgentServiceMetierService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AgentPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(
        private readonly ContextAwareDataPersisterInterface $decorated,
        private readonly EntityManagerInterface $entityManager,
        private readonly AgentServiceMetierService $serviceMetier,
        private readonly ParameterBagInterface $parameterBag
    ) {
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof Agent && $this->decorated->supports($data, $context);
    }

    public function persist($data, array $context = [])
    {
        $agent = $this->decorated->persist($data, $context);
        $idServiceMetier = $this->parameterBag->get('SERVICE_METIER_MPE');
        if ($data instanceof Agent) {
            $agentServiceMetier = $this->entityManager->getRepository(AgentServiceMetier::class)->findOneBy([
                'idAgent' => $agent->getId(),
                'idServiceMetier' => $idServiceMetier,
            ]);

            if (
                $agentServiceMetier instanceof AgentServiceMetier
                && $agentServiceMetier->getIdProfilService() != $agent->getIdProfilSocleExterne()
            ) {
                $this->serviceMetier->modifyIdProfilAgentServiceMetier(
                    $agentServiceMetier,
                    $agent->getIdProfilSocleExterne()
                );
            } elseif (!($agentServiceMetier instanceof AgentServiceMetier)) {
                $this->serviceMetier->createAgentServiceMetier(
                    $agent->getId(),
                    $idServiceMetier,
                    $agent->getIdProfilSocleExterne()
                );
            }
        }

        return $agent;
    }

    public function remove($data, array $context = [])
    {
        return $this->decorated->remove($data, $context);
    }
}
