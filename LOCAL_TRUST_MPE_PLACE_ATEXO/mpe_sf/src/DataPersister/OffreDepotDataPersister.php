<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\DataPersister;

use Exception;
use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Offre;
use App\Entity\TCandidature;
use App\Service\CandidatureService;
use App\Service\Offre\ValidateDepotOffreService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

final class OffreDepotDataPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ValidateDepotOffreService $validateDepotOffreService,
        private readonly ParameterBagInterface $parameterBag,
        private readonly LoggerInterface $logger
    ) {
    }

    public function supports($data, array $context = []): bool
    {
        return (
            $data instanceof Offre &&
            ($context['item_operation_name'] == 'validate_depot' || $context['collection_operation_name'] == 'post')
        );
    }

    public function persist($data, array $context = [])
    {
        if ($context['collection_operation_name'] == 'post') {
            $this->createCandidatureAfterCreateAnOffre($data);
        } elseif ($context['item_operation_name'] == 'validate_depot') {
            $this->em->beginTransaction();
            try {
                $this->validateDepotOffreService->validate($data);
                $this->em->flush();
                $this->em->commit();
            }
            catch (Exception $e) {
                $erreur = sprintf(
                    "Une erreur est survenu lors de la validation du dépot de votre offre : 
                    %d pour la consultation => %d . Erreur => %s %s",
                    $data->getId(),
                    $data->getConsultation()->getId(),
                    $e->getMessage(),
                    $e->getTraceAsString()
                );

                $this->logger->error($erreur);
                $this->em->rollback();
            }
        }
    }

    public function remove($data, array $context = [])
    {
    }

    private function createCandidatureAfterCreateAnOffre(Offre $offre): void
    {
        $candidature = new TCandidature();
        $candidature->setConsultation($offre->getConsultation());
        $candidature->setOrganisme($offre->getConsultation()->getOrganisme());
        $candidature->setIdInscrit($offre->getInscrit()->getId());
        $candidature->setIdEntreprise($offre->getInscrit()->getEntrepriseId());
        $candidature->setIdEtablissement($offre->getInscrit()->getIdEtablissement());
        $candidature->setStatus($this->parameterBag->get('STATUT_ENV_BROUILLON'));
        $candidature->setTypeCandidature(CandidatureService::PHASE_STD);
        $this->em->persist($offre);
        $this->em->persist($candidature);
        $this->em->flush();
    }
}