#!/bin/sh
set -x
PTF_MPE_CIBLE=$1
VERSION_MPE_CIBLE=$2
CHEMIN_LOCAL=$(pwd)"/"
CHEMIN_RESULTATS=/var/www/html/place/"$PTF_MPE_CIBLE"/"$VERSION_MPE_CIBLE"/
sudo -u www-data mkdir -p $CHEMIN_RESULTATS
tar zxvf atom_tnr.tar.gz > /dev/null
cd atom_tnr/atom/lib/
tar zxvf /home/atexo/atom_lib.tar.gz > /dev/null
cd ..
java -cp atom.jar:lib/* com.selenium.bota.console.Main
#cat Resultats/*junit.xml > Resultats/junitResult.xml
cd ../..
mv atom_tnr/atom/Resultats/*junit.xml .
cp atom_tnr/atom/atom.log .
mv atom_tnr/atom/Resultats/*MPE_Campagne/ .
rm -rf atom_tnr/ atom_tnr.tar.gz
sed -i "s/version_mpe/${VERSION_MPE_CIBLE}/g" atom.log
cd *MPE_Campagne
find . -name index.html -exec sed -i "s/ Application / ${PTF_MPE_CIBLE} /g"  {} \;
find . -name index.html -exec sed -i "s/version_mpe/${VERSION_MPE_CIBLE}/g"  {} \;
find . -type d -print0 | xargs -0 chmod -t,a-rwxs,u+rwx,g+rx,o+rx
find . -type f -print0 | xargs -0 chmod -t,a-rwxs,u+rw,g+r,o+r
sudo chown -R www-data:www-data .
sudo mv ../*MPE_Campagne $CHEMIN_RESULTATS
