#!/bin/sh

############################################################################################################
# Lancement sous Linux des test Atom
############################################################################################################


set -x
LOGIN_DISTANT="amv-atx@atom-serveur.local-trust.com.pcc37"           # compte et machine distante sur laquelle les scripts Atom vont être exécutés (ne pas confondre avec l'application cible ni la machine Windows où s'exécute Selenium)
BRANCHE_GIT="TNRA-place-corr"             # branche du dépot Git souhaité
PROJET_GIT="lt_mpe"                          # projet du dépot Git contenant les sources des TNR Atom (fichiers csv, xml, config, etc)
CHEMIN_LOCAL="~/"$PROJET_GIT"/"  # chemin du répertoire racine contenant le projet git sur la machine locale

############################################################################################################
# Commande utile pour la première installation :
# Exporter la clef ssh publique locale vers la machine distante (afin de créer les futures connexions ssh sans mot de passe)
# ssh-copy-id -i $HOME/.ssh/id_rsa.pub $LOGIN_DISTANT
############################################################################################################


############################################################################################################
# Commande utile pour installer une nouvelle version d'ATOM
# ssh $LOGIN_DISTANT "curl ftp://update.local-trust.com/upload/atom_lib_21c.tar.gz --user 'atom_mpe:1LGpjO0g' --ftp-ssl -k > tnra/atom_lib.tar.gz"
############################################################################################################


############################################################################################################
# Commande utile occasionnellement :
# Pour récupérer de Gitlab les sources des TNR Atom (fichiers csv, xml, config, etc) du dossier lt_mpe/tnr/atom_tnr/ 
# git clone git@gitlab.local-trust.com:produit/${PROJET_GIT}.git --single-branch -b $BRANCHE_GIT --depth 1 $CHEMIN_LOCAL
############################################################################################################


############################################################################################################
# Pour commiter des changements, on peut utiliser le ticket Jira MPE-5919 :
# git commit -m "MPE-9939 Scripts et librairie Atom"
############################################################################################################



# Pour lancer les tests Atom en integration
cd $CHEMIN_LOCAL/tnr/atom_tnr/
git pull
./exec_distant.sh atexo-atom-dev-01 2020-02.08.01-SNAPSHOT amv-atx
