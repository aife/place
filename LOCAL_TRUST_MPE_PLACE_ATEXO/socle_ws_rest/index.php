<?php

use App\Kernel;
use Application\Service\Atexo\Atexo_Util;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

if (!function_exists('apache_request_headers')) {
    function apache_request_headers()
    {
        $arh = array();
        $rx_http = '/\AHTTP_/';
        foreach ($_SERVER as $key => $val) {
            if (preg_match($rx_http, $key)) {
                $arh_key = preg_replace($rx_http, '', $key);
                $rx_matches = array();
                // do some nasty string manipulations to restore the original letter case
                // this should work in most cases
                $rx_matches = explode('_', $arh_key);
                if (count($rx_matches) > 0 and strlen($arh_key) > 2) {
                    foreach ($rx_matches as $ak_key => $ak_val) {
                        $rx_matches[$ak_key] = ucfirst($ak_val);
                    }
                    $arh_key = implode('-', $rx_matches);
                }
                $arh[$arh_key] = $val;
            }
        }
        return ($arh);
    }
}

/**
 * @var Composer\Autoload\ClassLoader
 */
$loader = require dirname(__DIR__) . '/mpe_sf/vendor/autoload.php';

$dotenv = new  Dotenv();
$dotenv->usePutenv(true);
$dotenv->load(__DIR__ . '/../mpe_sf/.env');

if ($trustedProxies = $_SERVER['TRUSTED_PROXIES'] ?? false) {
    Request::setTrustedProxies(
        explode(',', $trustedProxies),
        Request::HEADER_X_FORWARDED_ALL ^ Request::HEADER_X_FORWARDED_HOST
    );
}

$kernel = new Kernel($_SERVER['APP_ENV'], (bool)$_SERVER['APP_DEBUG']);
$request = Request::createFromGlobals();

$response = $kernel->handle($request);

if ($request->attributes->get('_route') != '_wdt' && $response->getStatusCode() === Response::HTTP_NOT_FOUND) {
    Atexo_Util::setSfKernel($kernel);
    $libraryPath = realpath(dirname(__FILE__) . '/library');
    $actionPath = realpath(dirname(__FILE__) . '/library/Atexo/Action');

    set_include_path(get_include_path() . PATH_SEPARATOR . $libraryPath . PATH_SEPARATOR . $actionPath);
    require $libraryPath . '/Atexo/Controller/Front.php';

    Atexo_Controller_Front::run();
} else {
    $response->send();
}

$kernel->terminate($request, $response);
