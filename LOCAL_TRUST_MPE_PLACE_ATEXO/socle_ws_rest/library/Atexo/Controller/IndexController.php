<?php
class IndexController extends Zend_Controller_Action 
{
    public function IndexAction()
    {
        $this->_forward('Index', 'noRoute');
    }

    public function noRouteAction()
    {
        $infos= (new Atexo_UtilSocle())->getInformationsFromUri($_SERVER['REQUEST_URI']);
       
        $request = array('method'=> "erreur", "erreurs"=>array("400", "Bad Request", "unsupported controller ~/".$infos['controller']."/".$infos['action']));
		
		$rest=new Atexo_RestSocle();
		$rest->setRequest($request);
		$rest->setClassName("UnkownAction");
		$rest->run();
    }
 
}
