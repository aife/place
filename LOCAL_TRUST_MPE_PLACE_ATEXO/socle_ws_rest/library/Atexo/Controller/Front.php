<?php

class Atexo_Controller_Front
{
	public static function run()
	{
		
	    // génération de application.xml s'il ne le trouve pas
	    
		$configGenFile = Atexo_Controller_Front::getRootPath() . Atexo_ConfigSocle::CONFIG_GEN_FILE;
        if (!file_exists($configGenFile)) {
            $build =Atexo_ConfigSocle::buildApplicationXmlFile();
        }
        
        //Début routage
	    $route=new Atexo_Route();
	    $route->run();
	    
	}
	
    public static function autoload($className)
    {
	        @include strtr($className, '_', DIRECTORY_SEPARATOR) . '.php';
    }
    
	public static function getRootPath()
    {
        static $mpePath = null;

        if ($mpePath === null) {
            $mpePath = realpath(dirname(__FILE__) . '/../../..');
        }
        return $mpePath;
    }
}

spl_autoload_register(array('Atexo_Controller_Front', 'autoload'));