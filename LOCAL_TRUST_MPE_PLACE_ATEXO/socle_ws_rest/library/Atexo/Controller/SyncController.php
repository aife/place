<?php


class SyncController extends Zend_Controller_Action 
{
	
	public function IndexAction()
    {
		$infos= (new Atexo_UtilSocle())->getInformationsFromUri($_SERVER['REQUEST_URI']);
		$request = array('method'=> "erreur", "erreurs"=>array("400", "Bad Request", "unsupported action ~/sync/".$infos['action'].""));
		$className="UnkownAction";
		
		$rest=new Atexo_RestSocle($_SERVER['REQUEST_METHOD'], $this->_getAllParams());
		$rest->setRequest($request);
		$rest->setClassName($className);
		$rest->run();
    }
    
	public function AgentAction()
	{
        self::launchRest();
	}
	
	public function CompanyAction()
	{
		self::launchRest();
	}

    public function EmployeeAction()
	{
		self::launchRest();
	}

	public function EstablishmentAction()
	{
		self::launchRest();
	}
	public function OrganismAction()
	{
		self::launchRest();
	}
	
	public function DepartmentAction()
	{
		self::launchRest();
	}
	
	public function AlerteAction()
	{
		self::launchRest();
	}
	
	private function launchRest()
	{
		$rest=new Atexo_RestSocle($_SERVER['REQUEST_METHOD'], $this->_getAllParams());
		$rest->run();
	}
}
