<?php
class Atexo_RestSocle extends Zend_Rest_Server
{
	protected $_request;
	protected $_className;
	
	public function setRequest($value)
	{
		$this->_request=$value;
	}
	
	public function setClassName($value)
	{
		$this->_className=$value;
	}
	
	function __construct($requestMehod, $params)
	{
		parent::__construct();
		$this->_className=ucfirst($params['action']).Atexo_ConfigSocle::getParameter("PLATEFORME");
		
		unset($params['controller']);
        unset($params['action']);
		
		$this->_request=(new Atexo_UtilSocle())->getRequest($requestMehod, $params);
	}
	
	public function run()
	{
        $this->init();
        $this->setClass($this->_className);
        $this->getHeaders();
        $this->handle($this->_request); 
	}
	
	public function init()
	{
        if(!isset($this->_request['method']) || !method_exists($this->_className, $this->_request['method'])) {
        	
        	if(!isset($this->_request['method'])) {
        		$message="Erreur while trying to run rest service. No method specified ";
        	} else if(!method_exists($this->_className, $this->_request['method'])) {
        		$message="Erreur while trying to run rest service. function '".$this->_request['method']."' not found in '".$this->_className."' class";
        	}
        	
        	$this->_className="UnkownAction";        	
        	$this->_request = array('method'=> "erreur", "erreurs"=>array("500", "Internal Server Error", $message));
        }
	}
}