<?php

class Atexo_Queries
{
	public function getOrganismeByInitialId($idInitialOrganisme)
	{
		$databaseName=Atexo_ConfigSocle::getParameter('COMMON_DB');

		
		$db=new Atexo_DbSocle();
		
		$sql="Select * from Organisme where id_externe = '". $idInitialOrganisme ."'";
		$ressource=$db->execute($databaseName, $sql);
        if ($ressource) {
            if ($organisme = mysqli_fetch_assoc($ressource)) {
                return $organisme;
            }
        }
		return false;
	}
	
	public function getOrganismeByAcronyme($acronyme)
	{
		$databaseName=Atexo_ConfigSocle::getParameter('COMMON_DB');
		
		$db=new Atexo_DbSocle();
        $sql="Select * from Organisme where acronyme='". $acronyme ."'";
        $ressource=$db->execute($databaseName, $sql);
        if ($ressource) {
                if ($organisme = mysqli_fetch_assoc($ressource)) {
                    return $organisme;
                }
		}

		return false;
	}
	
	public function getServiceByIdInitial($databaseName, $idService,$champsVerificationService='id_externe', $organisme=null)
	{
		$db=new Atexo_DbSocle();
		
		$sql="Select * from Service where ".$champsVerificationService."='". $idService ."'";
		if($organisme){
			$sql.= " and organisme='".$organisme."'";
		}
		
		$ressource=$db->execute($databaseName, $sql);
		if ($ressource) {
                if ($service = mysqli_fetch_assoc($ressource)) {
                    return $service;
                }
		}
		return false;
	}
	
	public function getAgentByLogin($login)
	{
		$db=new Atexo_DbSocle();
		$databaseName=Atexo_ConfigSocle::getParameter('COMMON_DB');
		
		$sql="Select * from Agent where login='". $login ."'";
		
		$ressource=$db->execute($databaseName, $sql);
		if ($ressource) {

                if ($agent = mysqli_fetch_assoc($ressource)) {
                    return $agent;
                }
		}
		return false;
	}
	
	public function getAgentByMail($mail)
	{
		$db=new Atexo_DbSocle();

		$databaseName=Atexo_ConfigSocle::getParameter('COMMON_DB');
		
		$sql="Select * from Agent where email='". $mail ."'";
		
		$ressource=$db->execute($databaseName, $sql);
		if ($ressource) {
                if ($agent = mysqli_fetch_assoc($ressource)) {
                    return $agent;
                }
		} 
		
		return false;
	}
	
	public function getAgentByIdInitial($idInitial,$champs='id_externe')
	{
		$db=new Atexo_DbSocle();
		$databaseName=Atexo_ConfigSocle::getParameter('COMMON_DB');
		
		$sql="Select * from Agent where ".$champs."='". $idInitial ."'";
		
		$ressource=$db->execute($databaseName, $sql);
		if ($ressource) {
                if ($agent = mysqli_fetch_assoc($ressource)) {
                    return $agent;
                }
		}
		return false;
	}
	
	public function getCompanyByIdInitial($idInitial)
	{
		  $db=new Atexo_DbSocle();

		  $databaseName=Atexo_ConfigSocle::getParameter('COMMON_DB');
		  $sql="Select * from Entreprise where id_externe = '". $idInitial ."'";
		  $ressource=$db->execute($databaseName, $sql);
		  if ($ressource) {
                  if ($entreprise = mysqli_fetch_assoc($ressource)) {
                      return $entreprise;
                  }
		  }
		  return false;
	}
	 
	 public function getCompanyBySiren($siren)
	 {
		  $db=new Atexo_DbSocle();
		  $databaseName=Atexo_ConfigSocle::getParameter('COMMON_DB');
		  $sql="Select * from Entreprise where siren='". $siren ."'";
		  $ressource=$db->execute($databaseName, $sql);
		  if ($ressource) {
                  if ($entreprise = mysqli_fetch_assoc($ressource)) {
                      return $entreprise;
                  }
		  }
		  return false;
	 }
	 
	 
	 public function getInscritByIdInitial($idInitial)
	 {
		  $db=new Atexo_DbSocle();
		  $databaseName=Atexo_ConfigSocle::getParameter('COMMON_DB');
		  $sql="Select * from Inscrit where id_externe = '". $idInitial ."'";
		  $ressource=$db->execute($databaseName, $sql);
		  if ($ressource) {
                  if ($inscrit = mysqli_fetch_assoc($ressource)) {
                      return $inscrit;
                  }
		  } 
		  return false;
	 }
	 
	 public function getInscritByLogin($login)
	 {
		  $db=new Atexo_DbSocle();
		  $databaseName=Atexo_ConfigSocle::getParameter('COMMON_DB');
		  $sql="Select * from Inscrit where login='". $login ."'";
		  $ressource = $db->execute($databaseName, $sql);
		  if ($ressource) {
                  if ($inscrit = mysqli_fetch_assoc($ressource)) {
                      return $inscrit;
                  }
		  }
		  return false;
	 }
	 public function getInscritByEmail($mail)
	 {
		  $db=new Atexo_DbSocle();
		  $databaseName=Atexo_ConfigSocle::getParameter('COMMON_DB');
		  $sql="Select * from Inscrit where EMAIL='". $mail ."'";
		  $ressource = $db->execute($databaseName, $sql);
		  if ($ressource) {
                  if ($inscrit = mysqli_fetch_assoc($ressource)) {
                      return $inscrit;
                  }
		  }
		  return false;
	 }
	 
	 public function getProfilById($idProfil)
	 {
	 	$databaseName=Atexo_ConfigSocle::getParameter('COMMON_DB');
	 	$db=new Atexo_DbSocle();
	 	 
	 	$sql="Select * from  HabilitationProfil where id='".$idProfil."'";
	    $ressource = $db->execute($databaseName, $sql);
	    if ($ressource) {
                if ($profil = mysqli_fetch_assoc($ressource)) {
                    return $profil;
                }
	    }
	    return false;
	 }
	 
	 public function getDenominationPaysByAcronyme($acronymePays)
	 {
	 	$databaseName=Atexo_ConfigSocle::getParameter('COMMON_DB');
	 	$db=new Atexo_DbSocle();
	 	 
	 	$sql="Select denomination1 from  GeolocalisationN2 where denomination2='".$acronymePays."'";
	    $ressource = $db->execute($databaseName, $sql);
	    if ($ressource) {
                if ($geoN2 = mysqli_fetch_assoc($ressource)) {
                    return $geoN2["denomination1"];
                }
	    }
	    return false;
	 }
	 
	 public function getSubServices($acronyme, $idServices, $org=null)
	 {
	 	$db=new Atexo_DbSocle();
	 	$subServices=array();
	 	
	 	$sql="Select * from AffiliationService where service_parent_id ='".$idServices."' ";
	 	if($org){
	 		$sql.= " and organisme='".$org."'";
	 	}
	 	$ressource = $db->execute($acronyme, $sql);
             while ($affiliation = mysqli_fetch_assoc($ressource)) {
                 $subServices[] = $affiliation['service_id'];
             }
	    
	    return $subServices;
	 }
	 
	 public function getParent($acronyme, $idServices, $organisme=null)
	 {
	 	$db=new Atexo_DbSocle();

	 	
	 	$sql="Select * from AffiliationService where service_id ='".$idServices."' ";
	 	if($organisme){
	 		$sql.= " and organisme='".$organisme."'";
	 	}
	 	$ressource = $db->execute($acronyme, $sql);


             if ($affiliation = mysqli_fetch_assoc($ressource)) {
                 return $affiliation['service_id'];
             }

	    
	    return "0";
	 }
	public function getProfilActesById($idProfil)
	 {
	 	$databaseName=Atexo_ConfigSocle::getParameter('COMMON_DB');
	 	$db=new Atexo_DbSocle();
	 	 
	 	$sql="Select * from  ACTE_PROFIL_HABILITATION where id='".$idProfil."'";
	    $ressource = $db->execute($databaseName, $sql);
	    if ($ressource) {
                if ($profil = mysqli_fetch_assoc($ressource)) {
                    return $profil;
                }
	    }
	    return false;
	 }
	 public function getAccessAgentApplication($idAgent, $idApplication) {
	    $databaseName=Atexo_ConfigSocle::getParameter('COMMON_DB');
	 	$db=new Atexo_DbSocle();


	 	$sql="Select * from  Agent_Service_Metier where id_agent='".$idAgent."' AND id_service_metier ='".$idApplication."'";
	    $ressource = $db->execute($databaseName, $sql);
	    if ($ressource) {
                if ($agentAccessApplication = mysqli_fetch_assoc($ressource)) {
                    return $agentAccessApplication;
                }
	    }
	    return false;
	 }
	public function addAgentAccessApplication ($idAgent, $profilExterne, $db=null)
	{
	    if (self::getAccessAgentApplication($idAgent, Atexo_ConfigSocle::getParameter("SOCLE_WS_REST_ID_APPLICATION")) == false) {
				$sql = "INSERT INTO Agent_Service_Metier (id_agent, id_service_metier, id_profil_service) VALUES ";
				$sql .= " ('" . addslashes($idAgent) . "', '" . addslashes(Atexo_ConfigSocle::getParameter("SOCLE_WS_REST_ID_APPLICATION")) . "', '" . addslashes($profilExterne) . "')";
			} else {
				$sql = "UPDATE Agent_Service_Metier SET id_profil_service = '" . addslashes($profilExterne) . "' ";
				$sql .= "WHERE id_agent = '" . addslashes($idAgent) . "' AND id_service_metier = '" . addslashes(Atexo_ConfigSocle::getParameter("SOCLE_WS_REST_ID_APPLICATION")) . "'";
			}
			if($db===null) {
				$db=new Atexo_DbSocle();
			}
		    if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql)) {
				throw new Exception("Bad query : mysql respond : ".$db->errorMessage());
			}
	}
	public function deleteAgentAccessApplication ($idAgent, $db=null)
	{
	    $sql = "DELETE FROM Agent_Service_Metier WHERE id_agent = '".addslashes($idAgent)."' AND id_service_metier = '".addslashes(Atexo_ConfigSocle::getParameter("SOCLE_WS_REST_ID_APPLICATION"))."'";
		if($db===null) {
			$db=new Atexo_DbSocle();
		}
	    if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql)) {
			throw new Exception("Bad query : mysql respond : ".$db->errorMessage());
		}
	}
	public function getAlertByIdInitial ($idIntialAlert, $db=null)
	{
        $db=new Atexo_DbSocle();
	 	$databaseName=Atexo_ConfigSocle::getParameter('COMMON_DB');
	 	//En bdd c'est encore id_initial et non id_externe !
	 	$sql="Select * from Alerte where id_initial ='".$idIntialAlert."' ";
	  	$ressource = $db->execute($databaseName,$sql);

            if ($alerte = mysqli_fetch_assoc($ressource)) {
                return $alerte;
            }

	    return false;
	}
	public function deleteAlerte($idInitialAlerte)
	{
		$sql = "DELETE FROM Alerte WHERE id_initial = '".addslashes($idInitialAlerte)."'";
		$db=new Atexo_DbSocle();
		if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql)) {
			throw new Exception("Bad query : mysql respond : ".$db->errorMessage());
		}
	}
	public function getLieuExecusionByIdRegion ($idRegion, $db=null)
	{
        $db = new Atexo_DbSocle();
        $databaseName = Atexo_ConfigSocle::getParameter('COMMON_DB');
        $sql = "Select id from GeolocalisationN2 where id_geolocalisationN1 ='" . $idRegion . "' ";
        $ressource = $db->execute($databaseName, $sql);
        $arraylisteLieu = array();
        $i = 0;
        while ($data = mysqli_fetch_array($ressource)) {
            $i++;
            $arraylisteLieu [$i] = $data ['id'];
        }
        if ($arraylisteLieu) {
            return $arraylisteLieu;
        } else {
            return false;
        }
	}
	/**
	 * permet de de retourner l'établissement dans MPE
	 *
	 * @return Array etablissement
	 * @author ARH <anas.rhorbal@atexo.com>
	 * @version 1.0
	 * @since 4.9.0
	 * @copyright Atexo 2015
	 */
	public function getEstablishmentByIdInitial($id_initial)
	{
		$db=new Atexo_DbSocle();
		$databaseName=Atexo_ConfigSocle::getParameter('COMMON_DB');
		$sql="Select * from t_etablissement where id_externe = '". $id_initial ."'";
		$ressource=$db->execute($databaseName, $sql);
		if ($ressource) {
                if ($entreprise = mysqli_fetch_assoc($ressource)) {
                    return $entreprise;
                }
		}
		return false;
	}
}