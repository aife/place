<?php


class Atexo_DbSocle
{
    public $db;
    private $dataBase;
    private $hostspec;
    private $username;
    private $password;


    public function __construct($hostspec = null, $username = null, $password = null)
    {
        if (!$hostspec) {
            $hostspec = Atexo_ConfigSocle::getParameter("DATABASE_HOST");
        }
        if (!$username) {
            $username = Atexo_ConfigSocle::getParameter("DATABASE_USERNAME");
        }
        if (!$password) {
            $password = Atexo_ConfigSocle::getParameter("DATABASE_PASSWORD");
        }

        $db = @mysqli_connect($hostspec, $username, $password);

        if (!$db) {
            echo "pas de connexion";
            exit;
        }
        $this->db = $db;
    }

    public function execute($dateBse, $query)
    {
        if (Atexo_ConfigSocle::getParameter('DB_PREFIX')) {
            if (!strstr($dateBse, Atexo_ConfigSocle::getParameter('DB_PREFIX') . "_")) {
                $dateBse = Atexo_ConfigSocle::getParameter('DB_PREFIX') . "_" . $dateBse;
            }
        }
        try {
            mysqli_select_db($this->db, $dateBse);
        } catch (Exception $e) {
            throw new Exception(mysqli_error());
        }

        $req = mysqli_query($this->db, $query);
        return $req;
    }

    public function errorMessage()
    {

        return mysqli_error($this->db);

    }

    public function fetchArray($req)
    {

        return mysqli_fetch_array($req, MYSQLI_ASSOC);

    }

    public function close()
    {
        mysqli_close($this->db);
    }

    public function begin()
    {
        mysqli_query($this->db, "BEGIN");
    }

    public function commit()
    {
        mysqli_query($this->db, "COMMIT");
    }

    function rollback()
    {
        mysqli_query($this->db, "ROLLBACK");
    }
}