<?php
class Atexo_Route extends Zend_Controller_RewriteRouter
{
	protected $_atexoRoutes;
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function run()
	{
		$rewrite_base = '/';
		$this->setRewriteBase($rewrite_base);
		
		$this->_atexoRoutes=array();
		$this->_atexoRoutes['compat'] = $this->getRoute('default');
		
		$this->createRoute("sync", "agent");
		$this->createRoute("sync", "establishment");
		$this->createRoute("sync", "company");
		$this->createRoute("sync", "organism");
		$this->createRoute("sync", "department");
		$this->createRoute("sync", "employee"); 
		$this->createRoute("sync", "alerte"); 
		
		$this->addRoutes($this->_atexoRoutes);
		
		
		$controller = Zend_Controller_Front::getInstance();
		$controller->setRouter($this);
		try {
			$controller->run(dirname(__FILE__).'/Controller/');
		} catch(Exception $e) {
			echo $e;	
		}
	}
	
	public function createRoute($controllerName, $action)
	{
		$this->_atexoRoutes[$action."withId"]=new Zend_Controller_Router_Route(':controller/:action/:id', array('controller'=>$controllerName, 'action'=>$action),  array('id'=>'\d+'));
		$this->_atexoRoutes[$action."withoutId"]=new Zend_Controller_Router_Route(':controller/:action/', array('controller'=>$controllerName, 'action'=>$action));
	}
}
