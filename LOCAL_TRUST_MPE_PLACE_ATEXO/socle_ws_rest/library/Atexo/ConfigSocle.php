<?php

use Application\Service\Atexo\Atexo_Util;

/**
 * Gestion de la configuration.
 *
 * Cette classe permet d'accéder aux directives de configuration
 * n'importe ou dans le code ainsi que de gérer la génération du
 * fichier de configuration application.xml en tenant compte de
 * la configuration spécifique de l'application en cours.
 *
 * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
 * @copyright Atexo 2008
 * @version 1.0
 * @since MPE-3.0
 * @package atexo
 * @subpackage config
 */
class Atexo_ConfigSocle
{

    // Chemin vers les fichiers de configuration (à partir de la racine du site)
    const CONFIG_MPE_FILE      = '/../mpe_sf/legacy/protected/config/application.xml.php';
    const CONFIG_SPE_FILE      = '/config/application.php';
    const CONFIG_GEN_FILE      = '/../mpe_sf/legacy/protected/application.xml';
    const CONFIG_GEN_FILE_YML      = '/../mpe_sf/config/parameters.yml';
    const CONFIG_CACHE_DIR     = '/../cache/';
    const CONFIG_CACHE_FILE    = '/config_application.php';

    // Modes de fonctionnement de Prado
    const APP_MODE_DEBUG       = 'Debug';
    const APP_MODE_NORMAL      = 'Normal';
    const APP_MODE_PERFORMANCE = 'Performance';

    // Valeurs de configuration variables
    private static $namespaces = array();
    private static $appMode    = self::APP_MODE_DEBUG;
    private static $parameters = array();

    /**
     * Contenu du fichier de configuration
     *
     * @var SimpleXMLElement
     */
    private static $application = null;

    /**
     * Vider tous les parametres
     */
    public static function resetParameters()
    {
        self::$parameters = null;
        self::$parameters = array();
    }

    /**
     * Initialise le fichier de configuration
     *
     * @todo accès direct au fichier XML, retrait du chargement des paramètres
     */
    private static function init()
    {
        static $initialized = false;
        
        if ($initialized === false) {
            $configGenFile = Atexo_Controller_Front::getRootPath() . self::CONFIG_GEN_FILE;
            if (file_exists($configGenFile)) {
                self::$application = simplexml_load_file($configGenFile);
                self::loadParameters();
            }
            $initialized = true;
        }
    }

    /**
     * Chargement des paramètres de configuration.
     *
     * Chargement depuis le fichier application.xml avec mise en cache,
     * ou extraction des paramètres depuis le fichier de cache.
     *
     * @return boolean
     */
    private static function loadParameters()
    {
        if (self::$application === null) {
            throw new Exception('You must initialize before loading parameters.');
        }
        $cacheDir = Atexo_Controller_Front::getRootPath() . self::CONFIG_CACHE_DIR;
        if (!is_dir($cacheDir)) {
            if (!mkdir($cacheDir)) {
                trigger_error('Unable to create cache directory.', E_USER_NOTICE);
                return false;
            }
            chmod($cacheDir, 0777);
        }
        $cacheFile = $cacheDir . self::CONFIG_CACHE_FILE;
        if (file_exists($cacheFile)) {
            self::$parameters = unserialize(file_get_contents($cacheFile));
        } else {
            foreach (self::$application->parameters->parameter as $parameter) {
                self::$parameters[(string) $parameter['id']] = (string) $parameter['value'];
            }
            file_put_contents($cacheFile, serialize(self::$parameters));
        }
        return true;
    }

    /**
     * Génération du fichier de configuration application.xml à partir
     * des fichiers /config/application.php et /mpe/protected/config/application.xml.php
     *
     * @return boolean
     */
    public static function buildApplicationXmlFile()
    {
        $speFile = Atexo_Controller_Front::getRootPath() . self::CONFIG_SPE_FILE;
        $mpeFile = Atexo_Controller_Front::getRootPath() . self::CONFIG_MPE_FILE;
        $genFile = Atexo_Controller_Front::getRootPath() . self::CONFIG_GEN_FILE;
        //echo "test #" .$speFile; exit;
        /*$speFileSyntaxOk = (boolean) token_get_all(file_get_contents($speFile));
        if (!$speFileSyntaxOk) {
            throw new Exception($speFileSyntaxOk . ' file : syntax error.');
        }*/
        $mpeFileSyntaxOk = (boolean) token_get_all(file_get_contents($mpeFile));
        
        if (!$mpeFileSyntaxOk) {
            echo($mpeFileSyntaxOk . ' file : syntax error.');
            exit;
            throw new Exception($mpeFileSyntaxOk . ' file : syntax error.');
        }
        if (is_file($genFile)) {
            unlink($genFile);
        }
        ob_start();
        //include $speFile;
        include $mpeFile;
        $content = ob_get_clean();
        if (simplexml_load_string($content) === false) {
            echo('Application XML file syntax not correct.');
            exit;
            throw new Exception('Application XML file syntax not correct.');
        }

        // Suppression du cache des paramètres
        /*$cacheFile = Atexo_Controller_Front::getRootPath() . self::CONFIG_CACHE_DIR . self::CONFIG_CACHE_FILE;
        if (file_exists($cacheFile)) {
            unlink($cacheFile);
        }*/
        // Ecriture du fichier de configuration
        
        return (boolean) file_put_contents($genFile, $content);
    }

   
    /**
     * Sélectionne le mode d'exécution de l'application (Pardo)
     *
     * Modes possibles en constantes de la présente classe.
     *
     * @param integer $mode
     */
    public static function setAppMode($mode)
    {
        $mode = (string) $mode;
        if (!($mode === self::APP_MODE_DEBUG || $mode === self::APP_MODE_NORMAL || $mode === self::APP_MODE_PERFORMANCE)) {
            throw new Exception('Bad application runtime mode');
        }
        self::$appMode = $mode;
    }

    /**
     * Retourne le mode d'exécution courant de l'application (debug, performance, ...)
     *
     * @return integer
     */
    public static function getAppMode()
    {
        return self::$appMode;
    }

    /**
     * Ajoute un paramètre au fichier de configuration si celui-ci n'existe pas déjà.
     *
     * Note : utilisable uniquement pour la génération du fichier de configuration !
     *
     * @param string $key
     * @param string $value
     */
    public static function setParameterIfNotExists($key, $value)
    {
        self::init();
        if (!isset(self::$parameters[$key])) {
            self::setParameter($key, $value);
        }
    }

    /**
     * Ajoute ou modifie un paramètre au fichier de configuration
     *
     * Note : utilisable uniquement pour la génération du fichier de configuration !
     *
     * @param string $key
     * @param string $value
     */
    public static function setParameter($key, $value)
    {
        self::init();
        if (!is_string($key)) {
            throw new Exception('Key type not allowed.');
        }
        if (!is_string($value) && !is_numeric($value)) {
            throw new Exception('Value type not allowed.');
        }
        self::$parameters[$key] = $value;
    }

    /**
     * Retourne un paramètre du fichier de configuration.
     *
     * Utilisable tout le temps.
     *
     * @param string $key
     * @return string
     */
    public static function getParameter($key)
    {
        self::init();
        if (!is_string($key)) {
            throw new Exception('Key type not allowed.');
        }
        if (self::$parameters['PF_TYPE'] == 'HM' && isset($_COOKIE['selectedorg'])) {
            if (isset(self::$parameters[$key. '_' . $_COOKIE['selectedorg']])) {
                return self::$parameters[$key. '_' . $_COOKIE['selectedorg']];
            }
        }

        return Atexo_Util::getSfContainer()->getParameter($key);
    }

    /**
     * Retrait d'un paramètre du fichier de configuration lors de sa génération.
     *
     * @param string $key
     */
    public static function removeParameter($key)
    {
        self::init();
        if (!is_string($key)) {
            throw new Exception('Key type not allowed.');
        }
        if (isset(self::$parameters[$key])) {
            unset(self::$parameters[$key]);
        }
    }

    /**
     * Retourne la liste des paramètres du fichier de configuration.
     *
     * Utilisable tout le temps.
     *
     * @return array
     */
    public static function getParameters()
    {
        self::init();
        return self::$parameters;
    }
}
