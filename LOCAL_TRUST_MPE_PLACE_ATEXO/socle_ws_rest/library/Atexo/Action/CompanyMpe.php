<?php

class CompanyMpe extends Company
{
	public function create()
	{
		try{
			$companyInfo = self::generateDataCreate();
			if (is_array($companyInfo)) {
				$company = (new Atexo_Queries())->getCompanyByIdInitial($companyInfo['externalId']);
				if($company == false) {
					self::save($companyInfo);
					header('HTTP/1.1 201 CREATED');
				} else {
					self::updateCompany($companyInfo);
					header('HTTP/1.1 200 OK');
				}
			} else {
			 	return $companyInfo;
			}
		} catch(Exception $e) {
				$messageErreur=$e->getMessage();
				return self::erreur($companyInfo['uidCompany'], array("500", "Internal Server Error", $messageErreur, "006"));
			}
		//exit;
	}
	
	public function update($id)
	{
		try{
			$companyInfo = self::generateDataUpdate();
			if (is_array($companyInfo)) {
				if ($companyInfo[0] == "-1") {
					$companyInfoCreate = self::generateDataCreate($companyInfo[1]);
					if (is_array($companyInfoCreate)) {
						self::save($companyInfoCreate);
						header('HTTP/1.1 201 CREATED');
					} else {
					 	return $companyInfoCreate;
					}
				}else {
					self::updateCompany($companyInfo);
					header('HTTP/1.1 200 OK');
				}
			} else {
			 	return $companyInfo;
			}
		} catch(Exception $e) {
				$messageErreur=$e->getMessage();
				return self::erreur($companyInfo['uidCompany'], array("500", "Internal Server Error", $messageErreur,"006"));
			}
		exit;
	}
	
	public function updateCompany($companyInfo)
	{
		$db=new Atexo_DbSocle();
        //$db->begin();
		try {
			$updateQuery = "UPDATE Entreprise SET " ;
			$updateQuery .= " `nom` = '".(addslashes($companyInfo['nom']))."', " ;
			$updateQuery .= " `siren` = '".(addslashes($companyInfo['siren']))."', " ;
			$updateQuery .= " `villeadresse` = '".(addslashes($companyInfo['city']))."', " ;
			$updateQuery .= " `paysadresse` = '".(addslashes((new Atexo_Queries())->getDenominationPaysByAcronyme($companyInfo['country'])))."', " ;
			$updateQuery .= " `adresse` = '".(addslashes($companyInfo['postalAddress']))."', " ;
			$updateQuery .= " `codepostal` = '".(addslashes($companyInfo['postalCode']))."', " ;
			$updateQuery .= " `description_activite` = '".(addslashes($companyInfo['description']))."', " ;
			$updateQuery .= " `email` = '".(addslashes($companyInfo['email']))."', " ;
			$updateQuery .= " `fax` = '".(addslashes($companyInfo['fax']))."', " ;
			$updateQuery .= " `telephone` = '".(addslashes($companyInfo['phone']))."', " ;
			$updateQuery .= " `site_internet` = '".(addslashes($companyInfo['website']))."', " ;
			$updateQuery .= " `codeape` = '".(addslashes($companyInfo['apeCode']))."', " ;
			$updateQuery .= " `formejuridique` = '".(addslashes($companyInfo['legalCategory']))."', " ;
			$updateQuery .= " `paysenregistrement` = '".(addslashes($companyInfo['registrationCountry']))."', " ;
			$updateQuery .= " `date_modification` = now(),";
			$updateQuery .= " `nicSiege` = '".(addslashes($companyInfo['nic']))."'," ;
			$updateQuery .= " `sirenEtranger` = '".(addslashes($companyInfo['sirenEtranger']))."'" ;
		    $updateQuery .= " WHERE id_externe ='".(addslashes($companyInfo['externalId']))."'";
		    
		    $company = (new Atexo_Queries())->getCompanyByIdInitial($companyInfo['externalId']);

		    if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $updateQuery))  {
				throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
		    }
		    
	        $responsiblesQuery="";
	        
	        
	        $sqlDeleteResponsibles = "DELETE FROM  responsableengagement WHERE entreprise_id = '".$company['id']."';";
			if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sqlDeleteResponsibles)) {
	    		throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
	    	}
	        
	        foreach($companyInfo['responsibles'] as $oneResponsable)  {
	        	$responsiblesQuery = "INSERT INTO responsableengagement (ENTREPRISE_ID, NOM, PRENOM, qualite, ID_INITIAL) VALUES (";
				$responsiblesQuery .= "'".(addslashes($company['id']))."',";
				$responsiblesQuery .= "'".(addslashes($oneResponsable['firstname']))."',";
				$responsiblesQuery .= "'".(addslashes($oneResponsable['lastname']))."',";
				$responsiblesQuery .= "'".(addslashes($oneResponsable['label']))."',";
				$responsiblesQuery .= "'".(addslashes($oneResponsable['externalId']))."'";
	    		$responsiblesQuery .= ");";
	    		
	    		if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $responsiblesQuery)) {
		    		throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
		    	}
	        }
	        //$db->commit();
	    	//echo $updateQuery;exit;
		
		} catch(Exception $e) {
			//$db->rollback();
			throw $e;
		}
	}
	
	public function save($companyInfo)
	{
		$db=new Atexo_DbSocle();
    	//$db->begin();
    	
    	try {
    	
		        $sql = "INSERT INTO Entreprise (SIREN,NOM,ADRESSE,CODEPOSTAL,VILLEADRESSE,PAYSADRESSE,EMAIL,FORMEJURIDIQUE," .
		        			"PAYSENREGISTREMENT,CODEAPE,TELEPHONE,FAX,SITE_INTERNET,DESCRIPTION_ACTIVITE,ID_EXTERNE,DATE_CREATION,DATE_MODIFICATION,NICSIEGE,SIRENETRANGER) VALUES (";
				$sql .="'".addslashes($companyInfo['siren'])."',";
				$sql .="'".(addslashes($companyInfo['nom']))."',";
				$sql .="'".(addslashes($companyInfo['postalAddress']))."',";
				$sql .="'".(addslashes($companyInfo['postalCode']))."',";
				$sql .="'".(addslashes($companyInfo['city']))."',";
				$sql .="'".(addslashes((new Atexo_Queries())->getDenominationPaysByAcronyme($companyInfo['country'])))."',";
				$sql .="'".(addslashes($companyInfo['email']))."',";
				$sql .="'".(addslashes($companyInfo['legalCategory']))."',";
				$sql .="'".(addslashes($companyInfo['registrationCountry']))."',";
				$sql .="'".(addslashes($companyInfo['apeCode']))."',";
				$sql .="'".(addslashes($companyInfo['phone']))."',";
				$sql .="'".(addslashes($companyInfo['fax']))."',";
				$sql .="'".(addslashes($companyInfo['website']))."',";
				$sql .="'".(addslashes($companyInfo['description']))."',";
				$sql .="'".(addslashes($companyInfo['externalId']))."',";
			    $sql .="now(),";
			    $sql .="now(),";
			    $sql .="'".(addslashes($companyInfo['nic']))."',";
				$sql .="'".(addslashes($companyInfo['sirenEtranger']))."');";
			    if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql)) {
			    	throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
			    }
			    
			   $newCompany = (new Atexo_Queries())->getCompanyByIdInitial($companyInfo['externalId']);
		       $responsiblesQuery="";
		       foreach($companyInfo['responsibles'] as $oneResponsable)  {
		        $responsiblesQuery = "INSERT INTO responsableengagement (ENTREPRISE_ID, NOM, PRENOM, qualite, ID_INITIAL) VALUES (";
				$responsiblesQuery .= "'".(addslashes($newCompany['id']))."',";
				$responsiblesQuery .= "'".(addslashes($oneResponsable['firstname']))."',";
				$responsiblesQuery .= "'".(addslashes($oneResponsable['lastname']))."',";
				$responsiblesQuery .= "'".(addslashes($oneResponsable['label']))."',";
				$responsiblesQuery .= "'".(addslashes($oneResponsable['externalId']))."'";
	    		$responsiblesQuery .= ");";
		    		
		    		if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $responsiblesQuery)) {
			    		throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
			    	}
		       }
		       
		      // $db->commit();
		       
    	} catch(Exception $e) {
    		//$db->rollback();
			throw $e;
    	}
	}
}