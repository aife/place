<?php

class OrganismMpe extends Organism
{
	public function create()
	{
		try{
			$infosOrganism = self::generateDataCreate();
			if (is_array($infosOrganism)) {
                self::save($infosOrganism);
                self::updateOrganisme($infosOrganism);
				header('HTTP/1.1 201 CREATED');
			} else {
			 	return $infosOrganism;
			}
		} catch(Exception $e) {
				$messageErreur=$e->getMessage();
				return self::erreur($infosOrganism['id'], array("500", "Internal Server Error", $messageErreur, "006"));
			}
		exit;
	}
	
	public function update($id)
	{
		try{
			$infosOrganism = self::generateDataUpdate();
			if (is_array($infosOrganism)) {
				if ($infosOrganism[0] == "-1") {
					$infosOrganismCreate = self::generateDataCreate($infosOrganism[1]);
					if (is_array($infosOrganismCreate)) {
						self::save($infosOrganismCreate);
						self::updateOrganisme($infosOrganismCreate);
						header('HTTP/1.1 201 CREATED');
					} else {
					 	return $infosOrganismCreate;
					}
				}else {
					self::updateOrganisme($infosOrganism);
					header('HTTP/1.1 200 OK');
				}
			} else {
			 	return $infosOrganism;
			}
		} catch(Exception $e) {
			$messageErreur=$e->getMessage();
			return self::erreur($infosOrganism['id'], array("500", "Internal Server Error", $messageErreur, "006"));
		}
		exit;
	}
	public function save($infosOrganism)
	{
        $fileName = Atexo_ConfigSocle::getParameter("COMMON_TMP"). "creationOrg_".date('YmdHis')."";
        if(!is_file($fileName)) {
            (new Atexo_UtilSocle())->write_file($fileName, "acronyme Organisme;ancien id service; nouveau id service\r\n");
        }
        (new Atexo_UtilSocle())->write_file($fileName, ";" . $infosOrganism['acronyme'] . ";" . $infosOrganism['type_article_org'] . ";" . $infosOrganism['denomination_org'] . ";" . $infosOrganism['categorie_insee']  . ";" . $infosOrganism['description_org']  . ";" . $infosOrganism['adresse'] . ";" . $infosOrganism['cp'] . ";" . $infosOrganism['ville'] . ";;;;;;;;" . $infosOrganism['email']. ";"."\r\n", "a");
        system ("cd " . Atexo_ConfigSocle::getParameter("COMMON_TMP") . ";" . Atexo_ConfigSocle::getParameter("PATH_CLI_MPE")." createOrgs fromCsv $fileName");
        $this->updateOrganisme($infosOrganism);
	}
	
	
	public function updateOrganisme($infosOrganism)
	{
		try{
			$updateQuery = "UPDATE Organisme SET " ;
	    	foreach($infosOrganism as $key => $value) {
				if (!(in_array($key, array("iconUrl","logoUrl","id","apeCode")))) {
	    			$updateQuery .= " ".$key." = '".(addslashes($value))."', " ;
				}
	    	}
	    	$updateQuery = substr($updateQuery,0,-2);
	    	$updateQuery .= " WHERE ACRONYME ='".(addslashes($infosOrganism['acronyme']))."' ;";
			
			$db=new Atexo_DbSocle();
		    if(!$db->execute(Atexo_ConfigSocle::getParameter('DB_PREFIX')."_".Atexo_ConfigSocle::getParameter('COMMON_DB'), $updateQuery))  {
				throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
		    }
			if($infosOrganism['iconUrl']) {
	        	self::wgetLogo($infosOrganism['iconUrl'],$infosOrganism['acronyme'], 'logo-organisme-petit.jpg');
			}
			if($infosOrganism['logoUrl']) {
				self::wgetLogo($infosOrganism['logoUrl'],$infosOrganism['acronyme'], 'logo-organisme-grand.jpg');
			}
		}catch(Exception $e) {
			throw $e;
		}
	}
	
	public function delete($id)
	{
		return "ok pour DELETE";
	}
    
    public function wgetLogo($iconUrl, $acronyme, $imgName = '') {
        if ($iconUrl) {
            $name = strstr($iconUrl, "logo");
            if ($imgName !== '') $name = $imgName;
            if (substr($name, 0, 1) != '/') $name = '/' . $name;
            $folderThemes = Atexo_ConfigSocle::getParameter('BASE_ROOT_DIR');
            $folderOrg = $folderThemes . $acronyme;
            $folderOrgImage = $folderOrg . Atexo_ConfigSocle::getParameter('PATH_ORGANISME_IMAGE');
            //if (!is_file($folderOrgImage . $name)) {
                if (!is_dir($folderThemes)) {
                    system("mkdir " . $folderThemes, $res);
                    system("chmod 755 " . $folderThemes, $res);
                }
                if (!is_dir($folderOrg)) {
                    system("mkdir " . $folderOrg, $res);
                    system("chmod 777 " . $folderOrg, $res);
                }
                if (!is_dir($folderOrgImage)) {
                    system("mkdir " . $folderOrgImage, $res);
                    system("chmod 777 " . $folderOrgImage, $res);
                }
                if ($imgName !== '') {
                    $commande = "wget -O $imgName --no-check-certificate   " . $iconUrl;
                } else {
                    $commande = "wget --no-check-certificate   " . $iconUrl;
                }
                $proxy = Atexo_ConfigSocle::getParameter("PROXY_WGET");
                if ($proxy != "") {
                    system("export http_proxy=\"" . $proxy . "\";export https_proxy=\"" . $proxy . "\";cd " . $folderOrgImage . ";" . $commande, $res);
                } else {
                    system("cd " . $folderOrgImage . ";" . $commande, $res);
                }
                $mpeThemesDir = substr(Atexo_ConfigSocle::getParameter('PATH_CLI_MPE'), 0, strpos(Atexo_ConfigSocle::getParameter('PATH_CLI_MPE'), 'protected'));
                $mpeThemesDir .= 'themes/' . $acronyme . Atexo_ConfigSocle::getParameter('PATH_ORGANISME_IMAGE');
                if (is_dir($mpeThemesDir)) {
                    system('cd ' . $mpeThemesDir . " && rm -f $mpeThemesDir/logo*", $res);
                }
            //}
        }
    }
}