<?php

class Establishment extends Atexo_Action_Actions
{
	protected function processXml($xml)
	{
		$establishment = array();
		$domDocument = new DOMDocument;
		$domDocument->loadXML($xml);
		$xpath = new DOMXPath($domDocument);
		$nicNodes = $xpath->query('nic');
		$establishment['code_etablissement']  = $nicNodes?->item(0)?->nodeValue;

		$tagEstablishment = $domDocument?->getElementsByTagName('establishment')?->item(0);
		$tagAdresse = $tagEstablishment?->getElementsByTagName('address')?->item(0);
		$tagCompany = $tagEstablishment?->getElementsByTagName('company')?->item(0);
		$statut = $tagEstablishment?->getElementsByTagName('adminStateValue')?->item(0)?->nodeValue;

		$establishment['externalId'] = $tagEstablishment?->getElementsByTagName('externalId')?->item(0)?->nodeValue;
		$establishment['uidEstablishment'] = $tagEstablishment?->getElementsByTagName('id')?->item(0)?->nodeValue;
    	$establishment['nom'] = $tagEstablishment?->getElementsByTagName('label')?->item(0)?->nodeValue;
		$establishment['libelle'] = $tagEstablishment?->getElementsByTagName('name')?->item(0)?->nodeValue;
    	$establishment['city'] = $tagAdresse?->getElementsByTagName('city')?->item(0)?->nodeValue;
    	$establishment['country'] = $tagAdresse?->getElementsByTagName('country')?->item(0)?->nodeValue;
		$establishment['postalAddress'] = $tagAdresse?->getElementsByTagName('postalAddress')?->item(0)?->nodeValue;
		$establishment['postalCode'] = $tagAdresse?->getElementsByTagName('postalCode')?->item(0)?->nodeValue;
		$establishment['description'] = $tagEstablishment?->getElementsByTagName('description')?->item(0)?->nodeValue;
		$establishment['email'] = $tagEstablishment?->getElementsByTagName('email')?->item(0)?->nodeValue;
		$establishment['fax'] = $tagEstablishment?->getElementsByTagName('fax')?->item(0)?->nodeValue;
		$establishment['phone'] = $tagEstablishment?->getElementsByTagName('phone')?->item(0)?->nodeValue;
		$establishment['apeCode']  = $tagEstablishment?->getElementsByTagName('apeCode')?->item(0)?->nodeValue;
		$establishment['code_insee'] = $tagEstablishment?->getElementsByTagName('insee')?->item(0)?->nodeValue;
		$establishment['est_siege'] = 'true' == $tagEstablishment?->getElementsByTagName('isHeadOffice')?->item(0)?->nodeValue?1:0;
		$establishment['statut_actif'] = (in_array($statut, array('STATUS_UNKNOWN', 'STATUS_ACTIVE')))?1:0;
		$establishment['entrepriseExternalId'] = $tagCompany?->getElementsByTagName('externalId')?->item(0)?->nodeValue;

		$company = (new Atexo_Queries())->getCompanyByIdInitial($establishment['entrepriseExternalId']);
		if($company) {
			$establishment['entrepriseId'] = $company['id'];
		} else {
			$companyClass = 'Company'.Atexo_ConfigSocle::getParameter("PLATEFORME");
			$entreprise = new $companyClass;
			$entepriseInfos = $entreprise->processXml($xml);
			$entreprise->save($entepriseInfos);
			$company = (new Atexo_Queries())->getCompanyByIdInitial($establishment['entrepriseExternalId']);
			$establishment['entrepriseId'] = $company['id'];
		}

        return $establishment;
	}
	
	
	public function generateDataCreate($xml=null)
	{
		if(!$xml) {
			$xml = @file_get_contents('php://input');
		}
		$verification=self::isValid($xml);
		if($verification!==true) {
			return $verification;
		}
		$establishmentInfo = self::processXml($xml);
		
		$establishment = (new Atexo_Queries())->getEstablishmentByIdInitial($establishmentInfo['externalId']);
    	if($establishment) {
    		$messageErreur="Establishment ".$establishmentInfo['externalId']." already exists";
			return self::erreur($establishmentInfo['uidEstablishment'], array("400", "Bad Request", $messageErreur, "005"));
    	}
    	return $establishmentInfo;
	}
    		
	public function generateDataUpdate()
	{
		$xml = @file_get_contents('php://input');
		
		$verification=self::isValid($xml);
		if($verification!==true) {
			return $verification;
		}
		
		$establishmentInfo = self::processXml($xml);
		$establishment = (new Atexo_Queries())->getEstablishmentByIdInitial($establishmentInfo['externalId']);
		
    	if(!$establishment) {
    		return array("-1", $xml);
    		/*$messageErreur="Establishment '".$establishmentInfo['externalId']."' not found";
			return self::erreur(null, array("404", "Bad Request", $messageErreur, "003"));*/
    	}
    	return $establishmentInfo;
	}
	
	public function delete($id)
	{
        $headers = array_change_key_case(apache_request_headers(), CASE_UPPER);
		if(!isset($headers['establishmentExternalId'])&&!isset($headers['ESTABLISHMENTEXTERNALID'])) {
			return self::erreur($id, array("500", "Internal Server Error", "establishmentExternalId not sent in header", "006"));
		}
		$idInitial = isset($headers['establishmentExternalId']) ? $headers['establishmentExternalId'] : $headers['ESTABLISHMENTEXTERNALID'];
		$establishment = (new Atexo_Queries())->getEstablishmentByIdInitial($idInitial);
		
    	if(!$establishment) {
    		$messageErreur="Establishment '".$id."' not found";
			return self::erreur($id, array("404", "Not Found", $messageErreur, "003"));
    	}
		$sqlQuery = "UPDATE t_etablissement SET `statut_actif` = '0', `date_suppression` = now() WHERE id_externe ='".$idInitial."' AND id_externe !='';";
		$db=new Atexo_DbSocle();
	    if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sqlQuery)) {
			return self::erreur($id, array("500", "Internal Server Error", "Bad query, mysql responded : ".$db->errorMessage(), "006"));
	    }
	    header('HTTP/1.1 200 OK');
		exit;
	}
}