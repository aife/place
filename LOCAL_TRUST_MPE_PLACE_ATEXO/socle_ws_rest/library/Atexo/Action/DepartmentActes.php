<?php


class DepartmentActes extends Department
{
	public function create()
	{
		try{
			$infosService = self::generateDataCreate(true,'id_service_initial');
			if (is_array($infosService)) {
				self::save($infosService);
				header('HTTP/1.1 201 CREATED');
			} else {
			 	return $infosService;
			}
		} catch(Exception $e) {
				$messageErreur=$e->getMessage();
				return self::erreur($infosService['uidService'], array("500", "Internal Server Error", $messageErreur, "006"));
			}
		exit;
	}
	
	public function update($id)
	{
		try{
			$infosService = self::generateDataUpdate(true,'id_service_initial');
			if (is_array($infosService)) {
			if ($infosService[0] == "-1") {
					$infosServiceCreate = self::generateDataCreate(true, "id_service_initial", $infosService[1]);
					if (is_array($infosServiceCreate)) {
						self::save($infosServiceCreate);
						header('HTTP/1.1 201 CREATED');
					} else {
					 	return $infosServiceCreate;
					}
				}else {
					self::updateService($infosService);
					header('HTTP/1.1 200 OK');
				}
			} else {
			 	return $infosService;
			}
		} catch(Exception $e) {
				$messageErreur=$e->getMessage();
				return self::erreur($infosService['uidService'], array("500", "Internal Server Error", $messageErreur, "006"));
			}
		exit;
	}
	
	public function save($infosService)
	{
		$db=new Atexo_DbSocle();
		$db->begin();
		
		try {
				
				$idServiceParent=$infosService['parent']['id'];
				if($idServiceParent){
					$typeService='2';
				}else{
					$typeService='1';
				}
			
				$valeur="";
				$champs="";
				$idService=null;
				if(Atexo_ConfigSocle::getParameter("DO_NOT_CHANGE_AGENT_ID")) {
						$champs =", id";
						$valeur =", '".addslashes($infosService['id'])."'";
						$idService =$infosService['id'];				
					}
				
			$sql="INSERT INTO Service(service, sigle, id_service_initial,organisme,type_service,date_modification".$champs.") "
					."VALUES('".addslashes($infosService['libelle'])."', '".addslashes($infosService['sigle'])."',"
					."'".addslashes($infosService['id'])."' ,'".addslashes($infosService['acronyme'])."','".addslashes($typeService)."',NOW()".$valeur.")";
				$dateBase = Atexo_ConfigSocle::getParameter("COMMON_DB");
				if(!$db->execute($dateBase, $sql)) {
					throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
				}
			
				if(!$idService){
					$sql="select last_insert_id() as id";
					$ressource=$db->execute($dateBase, $sql);
					$resultat=$db->fetchArray($ressource);
					$idService=$resultat['id'];
				}
				$infosService['idService']=$idService;
				self::insertAffiliationService($infosService,$db,true,'id_service_initial');
					
				$db->commit();
				
		} catch(Exception $e) {
			$db->rollback();
			throw $e;
		}
	}
	
	public function updateService($infosService)
	{
		try {
				$idServiceParent=$infosService['parent']['id'];
				if($idServiceParent){
					$typeService='2';
				}else{
					$typeService='1';
				}
					$sql="UPDATE  Service set service='".addslashes($infosService['libelle'])."'," 
					." sigle='".addslashes($infosService['sigle'])."', type_service='".addslashes($typeService)."'," 
					." date_modification=NOW() " 
					." WHERE id_service_initial='".$infosService['id']."'";
		
		$dateBase = Atexo_ConfigSocle::getParameter("COMMON_DB");
		$db=new Atexo_DbSocle();
		if(!$db->execute($dateBase, $sql)) {
			throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
		}
		self::updateAffiliationService($infosService,$db,true,'id_service_initial');
		$db->close();
		}catch(Exception $e) {
			$db->rollback();
			throw $e;
		}
	}
	
	public function delete($id)
	{
		$headers=apache_request_headers();
		
		if(!isset($headers['organismDepartmentExternalId'])) {
			return self::erreur($id, array("500", "Internal Server Error", "Department ExternalId not sent in header", "006"));
		}
		
		if(!isset($headers['organismExternalId'])) {
			return self::erreur($id, array("500", "Internal Server Error", "Organism ExternalId in header not found", "006"));
		}
		
		$idService=$headers['organismDepartmentExternalId'];
		$idOrganisme=$headers['organismExternalId']; 
		//$idOrganisme=$headers['externalIdOrganism']; 
		//$organisme=(new Atexo_Queries())->getOrganismeByInitialId($idOrganisme);
		//$acronymeOrganisme="";
		
		//if($organisme!==false) {
		//	$acronymeOrganisme=$organisme['acronyme'];
		//}
		//$dateBase = Atexo_Config::getParameter("COMMON_DB");
		/*$verification=self::verifierExistanceOranisme($acronymeOrganisme,$idOrganisme);
		if($verification!==true) {
			return $verification;
		}*/
		$dateBase = Atexo_ConfigSocle::getParameter("COMMON_DB");
		$verification=self::verifierExistanceService($dateBase, $idService, 'id_service_initial', $id);
		if(!is_array($verification)) {
			return self::erreur($id, array("404", "Not Found", "Departement not found", "003"));
		}
		if(self::verifierPossibiliteSuppression($verification['id'])){
			$sql="DELETE FROM  Service WHERE id_service_initial='".$idService."'";
			$db=new Atexo_DbSocle();
			if(!$db->execute($dateBase, $sql)) {
				return self::erreur($id, array("500", "Internal Server Error", "Internal Server Error, mysql responded : ".$db->errorMessage(), "006"));
			}
		}else{
			//message
		}
		header('HTTP/1.1 200 OK');
		exit;
	}
	
	/*public function serviceExists($acronymeOrganisme, $idServiceExterne)
	{
		$service=(new Atexo_Queries())->getServiceByIdInitial($acronymeOrganisme, $idServiceExterne);
		if($service) {
			$messageErreur="Departement already exists";
			return self::erreur(null, array("404", "Bad Request", $messageErreur, "005"));
		}
		
		return false;
	}*/
	/**
	 * $idService / id BD Actes
	 */
	public function verifierPossibiliteSuppression($idService){
		$dateBase = Atexo_ConfigSocle::getParameter("COMMON_DB");
		
		//Agent => service
		$sql="SELECT * FROM `Agent` where service_id = '".$idService."'";	
		$db=new Atexo_DbSocle();
		$ressource=$db->execute($dateBase, $sql);

            if($service=mysqli_fetch_assoc($ressource)) {
                return false;
            }

		
		//affiliation
		$sql="SELECT * FROM `AffiliationService` where service_parent_id = '".$idService."' ";	
		$db=new Atexo_DbSocle();
		$ressource=$db->execute($dateBase, $sql);

            if ($service = mysqli_fetch_assoc($ressource)) {
                return false;
            }

	 	
		//ACTE_ETAPE_CIRCUIT
		$sql="SELECT * FROM `ACTE_ETAPE_CIRCUIT` where ID_SERVICE_REFUS = '".$idService."' OR ID_SERVICE_ACCEPT = '".$idService."'";	
		$db=new Atexo_DbSocle();
		$ressource=$db->execute($dateBase, $sql);

            if ($service = mysqli_fetch_assoc($ressource)) {
                return false;
            }
	 	
	 	//ACTE_ETAPE_ACTE
		$sql="SELECT * FROM `ACTE_ETAPE_ACTE` where ID_SERVICE = '".$idService."' ";	
		$db=new Atexo_DbSocle();
		$ressource=$db->execute($dateBase, $sql);

            if ($service = mysqli_fetch_assoc($ressource)) {
                return false;
            }
	 	
 		return true; 
	 	
 	}

}