<?php


class AgentAgc extends Agent
{
	public function create()
	{
		try{
			$infosAgent = self::generateDataCreate(false, "id_initial", "id_initial");
			if (is_array($infosAgent)) {
				self::save($infosAgent);
				header('HTTP/1.1 201 CREATED');
			} else {
			 	return $infosAgent;
			}
		} catch(Exception $e) {
				$messageErreur=$e->getMessage();
				return self::erreur($infosAgent['uidAgent'], array("500", "Internal Server Error", $messageErreur, "006"));
			}
		exit;
	}
	
	public function update($id)
	{
		try{
			$infosAgent = self::generateDataUpdate(false, "id_initial", "id_initial");
			if (is_array($infosAgent)) {
				if ($infosAgent[0] == "-1") {
					$infosAgentCreate = self::generateDataCreate(false, "id_initial", "id_initial", $infosAgent[1]);
					if (is_array($infosAgentCreate)) {
						self::save($infosAgentCreate);
						header('HTTP/1.1 201 CREATED');
					} else {
					 	return $infosAgentCreate;
					}
				}else {
					
				    self::updateAgent($infosAgent);
					header('HTTP/1.1 200 OK');
				}
			} else {
			 	return $infosAgent;
			}
		} catch(Exception $e) {
				$messageErreur=$e->getMessage();
				return self::erreur($infosAgent['uidAgent'], array("500", "Internal Server Error", $messageErreur, "006"));
			}
		exit;
	}
	
	public function updateAgent($infosAgent)
	{
		try {
				
				$tentativesMdp=0;
				if(!$infosAgent['enabled']){
					$tentativesMdp=3;
				}	
				$sql="UPDATE `Agent` SET  `id` = '".addslashes($infosAgent['id'])."',`email` = '".addslashes($infosAgent['email'])."'," 
				."`nom` = '".addslashes($infosAgent['nom'])."'," 
				."`prenom` = '".addslashes($infosAgent['prenom'])."', `elu` = '".addslashes($infosAgent['elu'])."', "
				."`num_tel` = '".addslashes($infosAgent['tel'])."',`num_fax` = '".addslashes($infosAgent['fax'])."'," 
				."`adr_postale` = '".addslashes($infosAgent['adresse']['adressePostale'])."',`civilite` = '".addslashes($infosAgent['civilite'])."'," 
				."`type_comm` = '".addslashes($infosAgent['type_comm'])."',"
				."`date_modification` = now(), " 
				. "service_id='".addslashes($infosAgent['idService'])."', tentatives_mdp='".addslashes($tentativesMdp)."' "
				." WHERE  `Agent`.`id_initial` = '".addslashes($infosAgent['id'])."'";
			
			$db=new Atexo_DbSocle();
			if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql)) {
				throw new Exception("Internal Server Error : mysql respond : ".$db->errorMessage());
			}
		} catch(Exception $e) {
			throw $e;
		}
	}
	
	public function delete($id)
	{
		$headers=apache_request_headers();
		
		if(!isset($headers['agentProfileExternalId'])) {
			$message="Agent id not sent in header";
			return self::erreur($id, array("500", "Internal Server Error", $message,'006'));
		}
		
		$idInitial=$headers['agentProfileExternalId'];
		
		$agent=(new Atexo_Queries())->getAgentByIdInitial($idInitial);
		
		if(!$agent) {
			return self::erreur($id, array("404", "Not Found", "Agent ".$id." not found", "003"));
		}
		$prefixeSuprime=Atexo_ConfigSocle::getParameter('PREFIXE_SUPPRIME');
		$email=$prefixeSuprime.$agent['id']."_".$agent['email'];
		$sql="UPDATE `Agent` set deleted_at = now(), service_id = NULL, tentatives_mdp='3' , email='".addslashes($email)."', date_modification = now() 	 where `id_initial`='".addslashes($idInitial)."'";
		$db=new Atexo_DbSocle();
		if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql)) {
			return self::erreur($id, array("500", "Internal Server Error", "Bad query, mysql responded : ". $db->errorMessage(), "006"));
		}
		header('HTTP/1.1 200 OK');
		exit;
	}
	
	public function save($infosAgent)
	{
		$db=new Atexo_DbSocle();
		$db->begin();
		
		try {
				
				$tentativesMdp=0;
				if(!$infosAgent['enabled']){
					$tentativesMdp=3;
				}
				$sqlAgent="INSERT INTO Agent (id, email, nom, prenom, organisme, service_id, num_tel, num_fax, civilite, adr_postale, date_creation, id_initial, elu, nom_fonction, type_comm, tentatives_mdp,date_modification) "
					."VALUES('".addslashes($infosAgent['id'])."', '".addslashes($infosAgent['email'])."', '".addslashes($infosAgent['nom'])."', '".addslashes($infosAgent['prenom'])."', '".addslashes($infosAgent['acronyme'])."', '".addslashes($infosAgent['idService'])."', '".addslashes($infosAgent['tel'])."', '".addslashes($infosAgent['fax'])."', '".addslashes($infosAgent['civilite'])."', '".addslashes($infosAgent['adresse']['adressePostale'])."', now(), '".addslashes($infosAgent['id'])."', '".addslashes($infosAgent['elu'])."', '".addslashes($infosAgent['fonction'])."', '".addslashes($infosAgent['type_comm'])."','".addslashes($tentativesMdp)."',now()) ";
				
		
				if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sqlAgent)) {
					throw new Exception("Bad query : mysql respond : ".$db->errorMessage());
				}
		
				$sql="select last_insert_id() as id";
				$ressource=$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql);
				$resultat=$db->fetchArray($ressource);
				$idAgent=$resultat['id'];
				$db->commit();
		} catch(Exception $e) {
			$db->rollback();
			throw $e;
		}
		
		 
	}
	
}
