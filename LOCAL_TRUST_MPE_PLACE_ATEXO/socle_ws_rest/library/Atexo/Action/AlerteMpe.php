<?php

class AlerteMpe extends Alerte
{
	public function create()
	{ 
		try{
			$alertInfo = self::generateDataCreate();
			if (is_array($alertInfo)) {
				self::save($alertInfo);
				header('HTTP/1.1 201 CREATED');
			} else {
				return $alertInfo;
			}
		} catch(Exception $e) {
				$messageErreur=$e->getMessage();
				return self::erreur($alertInfo['id'], array("500", "Internal Server Error", $messageErreur, "006"));
			}
		exit;
	}
	
	public function update($id)
	{
		try{ 
			$alertInfo = self::generateDataUpdate($id);
			if (is_array($alertInfo)) { 
				if($alertInfo[2]) {
					if($id != $alertInfo[2]){
						return self::erreur($id, array("400", "Bad Request ", "externalId is different than the  parameter in URL", ""));
					}
				} else if($alertInfo["id"]){
					if($id != $alertInfo["id"]){
							return self::erreur($id, array("400", "Bad Request ", "externalId is different than the  parameter in URL", ""));
						}
				}
				if ($alertInfo[0] == "-1") {
					$alertInfoCreate = self::generateDataCreate($alertInfo[1]);
					if (is_array($alertInfoCreate)) {
						self::save($alertInfoCreate);
						header('HTTP/1.1 201 CREATED');
					} else {
					 	return $alertInfoCreate; 
					}
				}else {
					self::updateAlert($alertInfo,$id);
					header('HTTP/1.1 200 OK');
				}
			} else {
		 			return $alertInfo;
			}
		} catch(Exception $e) {
				$messageErreur=$e->getMessage();
				return self::erreur($id, array("500", "Internal Server Error", $messageErreur, "006"));
			}
		exit;
	}
	
	public function updateAlert($infosAlert)
	{
		$db=new Atexo_DbSocle();
        $db->begin();
		try {
			$sqlAlert = "UPDATE `Alerte` SET  `id_inscrit` = '".addslashes($infosAlert['id_inscrit'])."',`denomination` = '".addslashes($infosAlert['denomination'])."'," 
			."`periodicite` = '".addslashes($infosAlert['periodicite'])."'," 
			."`xmlCriteria` = '".addslashes($infosAlert['xmlCriteria'])."', `categorie` = '".addslashes($infosAlert['categorie'])."'"
			." WHERE  id_initial = '".addslashes($infosAlert['id'])."'"; 
	    		if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sqlAlert)) {
		    		throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
		    	}
	        $db->commit();
		} catch(Exception $e) {
			$db->rollback();
			throw $e;
		}
	}
	public function save($alertInfo)
	{
		$db=new Atexo_DbSocle();
    	$db->begin();
    	try {
    		$sqlAlert = "INSERT INTO Alerte(id_inscrit,denomination,periodicite,xmlCriteria,categorie,id_initial"
    		." ) VALUES('".addslashes($alertInfo['id_inscrit'])."', '".addslashes($alertInfo['denomination'])."', '";
			$sqlAlert .= addslashes($alertInfo['periodicite'])."', '".addslashes($alertInfo['xmlCriteria'])."', '"
			.addslashes($alertInfo['categorie'])."', '".addslashes($alertInfo['id'])."' ) ";
			if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sqlAlert)) {
    			throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
	    	}
		    $db->commit();    
    	} catch(Exception $e) {
    		$db->rollback();
			throw $e;
    	}
	}
}