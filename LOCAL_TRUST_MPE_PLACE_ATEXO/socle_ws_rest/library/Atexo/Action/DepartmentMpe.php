<?php


class DepartmentMpe extends Department
{
	public function create()
	{
		try{
			$infosService = self::generateDataCreate(true, "id_externe", null, true);
			if (is_array($infosService)) {
				$idLastInsert = self::save($infosService);
				header('HTTP/1.1 201 CREATED');
                return self::resultCreateUpdate($idLastInsert);
			} else {
			 	return $infosService;
			}
		} catch(Exception $e) {
				$messageErreur=$e->getMessage();
				return self::erreur($infosService['uidService'], array("500", "Internal Server Error", $messageErreur, "006"));
			}
		exit;
	}

	public function resultCreateUpdate($id){
	    $string  = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
        $string .= '<success xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
        $string .= '   <resourceId>'.$id.'</resourceId>';
        $string .= '</success>';
	    $xml = new SimpleXMLElement($string);
	    return $xml;
    }

	public function update($id)
	{
		try{
			$infosService = self::generateDataUpdate(true, "id_externe", true);
			if (is_array($infosService)) {
				if ($infosService[0] == "-1") {
					$infosServiceCreate = self::generateDataCreate(true, "id_externe", $infosService[1], true);
					if (is_array($infosServiceCreate)) {
                        $idLastInsert = self::save($infosServiceCreate, true);
						header('HTTP/1.1 201 CREATED');
                        return self::resultCreateUpdate($idLastInsert);

					} else {
					 	return $infosServiceCreate;
					}
				}else {
                    $idUpdated = self::updateService($infosService);
					header('HTTP/1.1 200 OK');
                    return self::resultCreateUpdate($idUpdated);
				}
			} else {
			 	return $infosService;
			}
		} catch(Exception $e) {
				$messageErreur=$e->getMessage();
				return self::erreur($infosService['uidService'], array("500", "Internal Server Error", $messageErreur, "006"));
			}
		exit;
	}
	
	public function save($infosService, $common=true)
	{
		$db=new Atexo_DbSocle();
		$db->begin();
		$acronymeOrganisme = $infosService['acronyme'];
		try {
			$organisme = (new Atexo_Queries())->getOrganismeByAcronyme($acronymeOrganisme);
				$sql="INSERT INTO Service(";
					if($common){
						$sql.="organisme, ";
					}
					$sql.="sigle, adresse, cp, ville, pays, libelle, mail, telephone, fax, id_externe,date_creation,date_modification,complement) VALUES('";
					if($common){
						$sql.=addslashes($acronymeOrganisme)."','";
					}
					$sql.=addslashes($infosService['sigle'])."',"
					."'".addslashes($infosService['adresse'])."', '".addslashes($infosService['cp'])."',"
					."'".addslashes($infosService['ville'])."', '".addslashes($infosService['pays'])."', "
					."'".addslashes($infosService['libelle'])."', '".addslashes($infosService['mail'])."',"
					."'".addslashes($infosService['telephone'])."', '".addslashes($infosService['fax'])."', '".addslashes($infosService['id'])."',now(),now(),'".addslashes($organisme['complement'])."')";
					
					if($common){
						$databaseName=Atexo_ConfigSocle::getParameter('COMMON_DB');
					}	else {
						$databaseName=$acronymeOrganisme;
					}
				if(!$db->execute($databaseName, $sql)) {
					throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
				}
				self::fillServicePathField($infosService,$db,true,"id_externe", $acronymeOrganisme);
				$sqlId="select last_insert_id() as id";
				$ressourceId=$db->execute($databaseName, $sqlId);
				$resultatId=$db->fetchArray($ressourceId);
				
				$infosService['idService']=$resultatId['id'];
				self::insertAffiliationService($infosService,$db,true,"id_externe", $acronymeOrganisme);
				
				$db->commit();
				return $resultatId['id'];
		} catch(Exception $e) {
			$db->rollback();
			throw $e;
		}
	}
	
	public function updateService($infosService, $common=true)
	{
		try {
			$organisme = (new Atexo_Queries())->getOrganismeByAcronyme($infosService['acronyme']);

		$sql="UPDATE  Service set sigle='".addslashes($infosService['sigle'])."', adresse='".addslashes($infosService['adresse'])."',"
					." cp='".addslashes($infosService['cp'])."', ville='".addslashes($infosService['ville'])."', " 
					."pays='".addslashes($infosService['pays'])."', libelle='".addslashes($infosService['libelle'])."', mail='".addslashes($infosService['mail'])."', " 
					."telephone='".addslashes($infosService['telephone'])."', fax='".addslashes($infosService['fax'])."', date_modification=now(), complement='".addslashes($organisme['complement'])."'"
					." WHERE id_externe='".$infosService['id']."'";
					if($common){
						$sql.= " AND organisme='".$infosService['acronyme']."'";
						$databaseName=Atexo_ConfigSocle::getParameter('COMMON_DB');
					}else {
						$databaseName=$infosService['acronyme'];
					}
		
		$db=new Atexo_DbSocle();
		if(!$db->execute($databaseName, $sql)) {
			throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
		}
		self::fillServicePathField($infosService,$db,true, "id_externe", $infosService['acronyme']);
		self::updateAffiliationService($infosService,$db, true, "id_externe", $infosService['acronyme']);
		$db->commit();
        return $organisme['id'];
		}catch(Exception $e) {
			$db->rollback();
			throw $e;
		}
	}
	
	public function delete($id, $common=true)
	{
        $headers = array_change_key_case(apache_request_headers(), CASE_UPPER);
		if($common){
			$databaseName=Atexo_ConfigSocle::getParameter('COMMON_DB');
		}
		if(!isset($headers['organismDepartmentExternalId']) && !isset($headers['ORGANISMDEPARTMENTEXTERNALID'])) {
			return self::erreur($id, array("500", "Internal Server Error", "Department ExternalId not sent in header", "006"));
		}
		
		if(!isset($headers['organismExternalId']) && !isset($headers['ORGANISMEXTERNALID'])) {
			return self::erreur($id, array("500", "Internal Server Error", "Organism ExternalId in header not found", "006"));
		}
		
		$idService = isset($headers['organismDepartmentExternalId']) ? $headers['organismDepartmentExternalId'] : $headers['ORGANISMDEPARTMENTEXTERNALID'];
		$idOrganisme = isset($headers['organismExternalId']) ? $headers['organismExternalId'] : $headers['ORGANISMEXTERNALID'];
		$organisme = (new Atexo_Queries())->getOrganismeByInitialId($idOrganisme);
		$acronymeOrganisme="";
		
		if($organisme!==false) {
			$acronymeOrganisme=$organisme['acronyme'];
		}
		
		$verification=self::verifierExistanceOrganisme($acronymeOrganisme,$idOrganisme);
		if($verification!==true) {
			return $verification;
		}
		
		$verification=self::verifierExistanceService($databaseName, $idService, "id_externe", $id, $organisme['acronyme']);
		if(!is_array($verification)) {
			return self::erreur($id, array("404", "Not Found", "Departement not found", "003"));
		}
		//return utf8_encode(print_r($verification, true));
		if (!self::verifierPossibiliteSuppression($databaseName, $verification["id"], true))
		{
		    return self::erreur($id, array("500", "Internal Server Error", "There are some elements attached to this resource ", "006"));
		}
		$sql="DELETE FROM  Service WHERE id_externe='".$idService."'";
		if($common){
			$sql.=" and organisme='".$acronymeOrganisme."'";
		}
			
		$db=new Atexo_DbSocle();
		if(!$db->execute($databaseName, $sql)) {
			return self::erreur($id, array("500", "Internal Server Error", "Internal Server Error, mysql responded : ".$db->errorMessage(), "006"));
		}
		header('HTTP/1.1 200 OK');
		exit;
	}
	
	/*
	 * $idService / id BD SDM
	 */
	public function verifierPossibiliteSuppression($organisme, $idService, $common=true){
 		
		$dateBase = Atexo_ConfigSocle::getParameter("COMMON_DB");
		
		//Agent => service
		$sql="SELECT * FROM `Agent` where service_id = '".$idService."' AND organisme = '" .$organisme. "'";	
		$db=new Atexo_DbSocle();
		$ressource=$db->execute($dateBase, $sql);
		if (!$ressource) {
		    return false;
		}

            if($service=mysqli_fetch_assoc($ressource)) {
                return false;
            }


		
		//affiliation
		$sql="SELECT * FROM `AffiliationService` where service_parent_id = '".$idService."' ";
		if($common){
			$sql.=" and organisme='".$organisme."'";
			$databaseName=Atexo_ConfigSocle::getParameter('COMMON_DB');
		} else {
			$databaseName=$organisme;
		}
		$db=new Atexo_DbSocle();
		$ressource=$db->execute($databaseName, $sql);
		if (!$ressource) {
		    return false;
		}

            if ($service = mysqli_fetch_assoc($ressource)) {
                return false;
            }

	 	
		//ACTE_ETAPE_CIRCUIT
		$sql="SELECT * FROM `consultation` where service_id = '".$idService."' OR service_associe_id = '".$idService."'";	
		if($common){
			$sql.=" and organisme='".$organisme."'";
		}
		$db=new Atexo_DbSocle();
		$ressource=$db->execute($databaseName, $sql);
		if (!$ressource) {
		    return false;
		}

            if ($service = mysqli_fetch_assoc($ressource)) {
                return false;
            }

	 	
 		return true; 
	 	
 	}
	
	/*public function serviceExists($acronymeOrganisme, $idServiceExterne)
	{
		$service=(new Atexo_Queries())->getServiceByIdInitial($acronymeOrganisme, $idServiceExterne);
		if($service) {
			$messageErreur="Departement already exists";
			return self::erreur(null, array("400", "Bad Request", $messageErreur), "005");
		}
		
		return false;
	}*/

}
