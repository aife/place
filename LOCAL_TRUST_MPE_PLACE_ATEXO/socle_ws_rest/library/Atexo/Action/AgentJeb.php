<?php


class AgentJeb extends Agent
{
	public function create()
	{
		try{
			$infosAgent = self::generateDataCreate(true, "id_initial", "id_initial");
			if (is_array($infosAgent)) {
				self::save($infosAgent);
				header('HTTP/1.1 201 CREATED');
			} else {
				return $infosAgent;
			}
		} catch(Exception $e) {
			$messageErreur=$e->getMessage();
			return self::erreur($infosAgent['uidAgent'], array("400", "Bad Request", $messageErreur));
		}
		exit;
	}

	public function update($id)
	{
		try{
			$infosAgent = self::generateDataUpdate(true, "id_initial", "id_initial");
			if (is_array($infosAgent)) {
				if ($infosAgent[0] == "-1") {
					$infosAgentCreate = self::generateDataCreate(true, "id_initial", "id_initial", $infosAgent[1]);
					if (is_array($infosAgentCreate)) {
						self::save($infosAgentCreate);
						header('HTTP/1.1 201 CREATED');
					} else {
					 	return $infosAgentCreate;
					}
				}else {
					self::updateAgent($infosAgent);
					header('HTTP/1.1 200 OK');
				}
			} else {
				return $infosAgent;
			}
		} catch(Exception $e) {
			$messageErreur=$e->getMessage();
			return self::erreur($infosAgent['uidAgent'], array("500", "Internal Server Error", $messageErreur, "006"));
		}
		exit;
	}

	public function updateAgent($infosAgent)
	{
		try {
		    
			$tentativesMdp=0;
            if(!$infosAgent['enabled']){
                $tentativesMdp=3;
            }
			$sql=" UPDATE `Agent` SET "
			."`email` = '".addslashes($infosAgent['email'])."',"
			."`nom` = '".addslashes($infosAgent['nom'])."',"
			."`prenom` = '".addslashes($infosAgent['prenom'])."',"
			."`tentatives_mdp` = '".addslashes($tentativesMdp)."',"
			."`organisme` = '".addslashes($infosAgent['acronyme'])."',"
			." service_id='".addslashes($infosAgent['idService'])."', "
			."`elu` = '".addslashes($infosAgent['elu'])."', "
			."`num_tel` = '".addslashes($infosAgent['tel'])."',"
			."`num_fax` = '".addslashes($infosAgent['fax'])."',"
			."`adr_postale` = '".addslashes($infosAgent['adresse']['adressePostale'])."',"
			."`civilite` = '".addslashes($infosAgent['civilite'])."',"
			."`date_modification` = now() "
			." WHERE  `Agent`.`id_initial` = '".addslashes($infosAgent['id'])."'";

			$db=new Atexo_DbSocle();
			if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql)) {
				throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
			}
		} catch(Exception $e) {
			throw $e;
		}
	}

	public function delete($id)
	{
		$headers=apache_request_headers();

		if(!isset($headers['agentProfileExternalId'])) {
			$message="Agent id not sent in header";
			return self::erreur($id, array("500", "Internal Server Error", $message, "006"));
		}

		$idInitial=$headers['agentProfileExternalId'];

		$agent=(new Atexo_Queries())->getAgentByIdInitial($idInitial);

		if(!$agent) {
			return self::erreur($id, array("404", "Not Found", "Agent ".$id." not found", "003"));
		}
        $prefixeSuprime=Atexo_ConfigSocle::getParameter('PREFIXE_SUPPRIME');
		$email=$prefixeSuprime.$agent['id']."_".$agent['email'];
		$sql="UPDATE `Agent` set deleted_at = now(), service_id = NULL, tentatives_mdp='3', email='".addslashes($email)."', date_modification = now()  where `id_initial`='".addslashes($idInitial)."'";
		$db=new Atexo_DbSocle();
		if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql)) {
			return self::erreur($id, array("500", "Internal Server Error", "Bad query, mysql responded : ". $db->errorMessage(), "006"));
		}
		header('HTTP/1.1 200 OK');
		exit;
	}

	public function save($infosAgent)
	{
		$db=new Atexo_DbSocle();
		$db->begin();

		try {

		    $tentativesMdp=0;
            if(!$infosAgent['enabled']){
                $tentativesMdp=3;
            }
			
			$sqlAgent="INSERT INTO Agent(email, nom, prenom, tentatives_mdp, organisme, service_id, elu, nom_fonction, num_tel, num_fax, adr_postale, civilite,  date_creation, date_modification, id_initial) "
			."VALUES('".addslashes($infosAgent['email'])."', '"
			.addslashes($infosAgent['nom'])."', '"
			.addslashes($infosAgent['prenom'])."', '"
			.addslashes($tentativesMdp)."', '"
			.addslashes($infosAgent['acronyme'])."', '"
			.addslashes($infosAgent['idService'])."', '"
			.addslashes($infosAgent['elu'])."', '"
			.addslashes($infosAgent['fonction'])."', '"
			.addslashes($infosAgent['tel'])."', '"
			.addslashes($infosAgent['fax'])."', '"
			.addslashes($infosAgent['adresse']['adressePostale'])."', '"
			.addslashes($infosAgent['civilite'])."', "
			." now(), "
			." now(), '"
			.addslashes($infosAgent['id'])."')";
			 
			if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sqlAgent)) {
				throw new Exception("Bad query : mysql respond : ".$db->errorMessage());
			}

			$sql="select last_insert_id() as id";
			$ressource=$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql);
			$resultat=$db->fetchArray($ressource);
			$idAgent=$resultat['id'];

			self::insertAgentServiceEbrgn($idAgent, $infosAgent['rolesAgent']['profil']);
			 
			$db->commit();
		} catch(Exception $e) {
			$db->rollback();
			throw $e;
		}

		 
	}
	
	public function insertAgentServiceEbrgn($idAgent, $idProfil)
	{
		$sqlHabilitation="INSERT INTO AgentServiceEBRGN (id_agent,id_service)VALUES (".$idAgent.",".Atexo_ConfigSocle::getParameter('SOCLE_WS_REST_ID_APPLICATION').") ";
        $db=new Atexo_DbSocle();
		if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sqlHabilitation)) {
			throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
		}
	}

}