<?php

class Organism extends Atexo_Action_Actions
{
	
	public function generateDataCreate($xml=null)
	{
		if (!$xml) {
			$xml = @file_get_contents('php://input');
		}
		$infosOrganism = self::processXml($xml);
		$acronymeOrganisme = $infosOrganism['acronyme'];
		$verification = self::verifierNonExistanceOrganisme($acronymeOrganisme, $infosOrganism['id']);
		if($verification!==true) {
			return $verification;
		}
		return $infosOrganism;
						
	}
	
	
	public function generateDataUpdate()
	{
	    $xml = @file_get_contents('php://input');
	    $verification=self::isValid($xml);
		if($verification!==true) {
			return $verification;
		}
		$infosOrganism = self::processXml($xml);
		$acronymeOrganisme = $infosOrganism['acronyme'];
		$verification = self::verifierExistanceOrganisme($acronymeOrganisme);
		if($verification!==true) {
			return array("-1", $xml);//return $verification;
		}
		return $infosOrganism;
	}
	
	public function delete($id)
	{
		return "ok pour DELETE";
	}
	
	public function processXml($xml)
	{
		$infosAgent=array();
		
		$domDocument = new DOMDocument;
		$domDocument->loadXML($xml);
		$organism = $domDocument?->getElementsByTagName('organism')?->item(0);
		$infosOrganism['id']=$organism?->getElementsByTagName('id')?->item(0)?->nodeValue;
		$infosOrganism['id_externe']=$organism?->getElementsByTagName('externalId')?->item(0)?->nodeValue;
		$infosOrganism['denomination_org']=($organism?->getElementsByTagName('label')?->item(0)?->nodeValue);
		$infosOrganism['acronyme']=$organism?->getElementsByTagName('acronym')?->item(0)?->nodeValue;
		$infosOrganism['sigle']=$organism?->getElementsByTagName('abbreviation')?->item(0)?->nodeValue;
		
		$adresse=$organism?->getElementsByTagName('address')?->item(0);
		$infosOrganism['ville']=$adresse?->getElementsByTagName('city')?->item(0)?->nodeValue;
		$infosOrganism['pays']=$adresse?->getElementsByTagName('country')?->item(0)?->nodeValue;
		$infosOrganism['adresse']=$adresse?->getElementsByTagName('postalAddress')?->item(0)?->nodeValue;
		$infosOrganism['cp']=$adresse?->getElementsByTagName('postalCode')?->item(0)?->nodeValue;
		
		$infosOrganism['description_org']=$organism?->getElementsByTagName('description')?->item(0)?->nodeValue;
		
		$infosOrganism['email']=$organism?->getElementsByTagName('email')?->item(0)?->nodeValue;
		$infosOrganism['complement'] = $organism?->getElementsByTagName('nic')?->item(0)?->nodeValue;
		
		$active=$organism?->getElementsByTagName('enabled')?->item(0)?->nodeValue;
		if($active == "true") {
		      $infosOrganism['active']=1;
		}else {
			  $infosOrganism['active']=0;
		}
		
		$infosOrganism['telecopie']=$organism?->getElementsByTagName('fax')?->item(0)?->nodeValue;
		
		$infosOrganism['iconUrl']= $organism?->getElementsByTagName('iconUrl')?->item(0)?->nodeValue;
		$infosOrganism['logoUrl']=$organism?->getElementsByTagName('logoUrl')?->item(0)?->nodeValue;
        $infosOrganism['siren'] = $organism?->getElementsByTagName('siren')?->item(0)?->nodeValue;
       
		$infosOrganism['tel']=$organism?->getElementsByTagName('phone')?->item(0)?->nodeValue;
		
		$infosOrganism['url']=$organism?->getElementsByTagName('website')?->item(0)?->nodeValue;
		$articleOrganism =$organism?->getElementsByTagName('article')?->item(0)?->nodeValue;
		$arrayArticle = array('' => "0", 'LE' => "1" , 'LE' => "2", 'L' => "3");
		$infosOrganism['type_article_org'] = $arrayArticle [$articleOrganism];
		$infosOrganism['categorie_insee']=$organism?->getElementsByTagName('legalCategory')?->item(0)?->nodeValue;
		//$infosOrganism['member']=$organism?->getElementsByTagName('member')?->item(0)?->nodeValue;
		//$infosOrganism['siren']=$organism?->getElementsByTagName('siren')?->item(0)?->nodeValue;
		$infosOrganism['apeCode']=$organism?->getElementsByTagName('apeCode')?->item(0)?->nodeValue;
		return $infosOrganism;
		
	}
	
	public function wgetLogo($iconUrl,$acronyme){
		if($iconUrl) {
        	$name = strstr($iconUrl,"logo");
        	$folderThemes =  Atexo_ConfigSocle::getParameter('BASE_ROOT_DIR');
	        $folderOrg = $folderThemes.$acronyme;
	        $folderOrgImage = $folderOrg  . Atexo_ConfigSocle::getParameter('PATH_ORGANISME_IMAGE');
	        if(!is_file($folderOrgImage.$name)) {
		        if (!is_dir($folderThemes)) {
		            system("mkdir ".$folderThemes, $res);
		            system("chmod 755 ".$folderThemes, $res);
		        }
		        if (!is_dir($folderOrg)) {
		            system("mkdir ".$folderOrg, $res);
		            system("chmod 777 ".$folderOrg, $res);
		        }
		        if (!is_dir($folderOrgImage)) {
		            system("mkdir ".$folderOrgImage, $res);
		            system("chmod 777 ".$folderOrgImage, $res);
		        } 
		        $commande =  "wget --no-check-certificate   " .$iconUrl ;
		        $proxy = Atexo_ConfigSocle::getParameter("PROXY_WGET");
                if ($proxy != "") {
                       system("export http_proxy=\"".$proxy."\";export https_proxy=\"".$proxy."\";cd " . $folderOrgImage . ";" .$commande,$res);
                } else {
                       system("cd " . $folderOrgImage . ";" .$commande,$res);
                }
	        }
        }
	}
}
