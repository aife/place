<?php

class OrganismAgc extends Organism
{
    public function create()
    {
        try{
            $infosOrganism = self::generateDataCreate();
            if (is_array($infosOrganism)) {
                self::save($infosOrganism);
                header('HTTP/1.1 201 CREATED');
            } else {
                return $infosOrganism;
            }
        } catch(Exception $e) {
                $messageErreur=$e->getMessage();
                return self::erreur($infosOrganism['id'], array("500", "Internal Server Error", $messageErreur, "006"));
            }
        exit;
    }
    
    public function update($id)
    {
        try{
            $infosOrganism = self::generateDataUpdate();
            if (is_array($infosOrganism)) {
            	if ($infosOrganism[0] == "-1") {
					$infosOrganismCreate = self::generateDataCreate($infosOrganism[1]);
					if (is_array($infosOrganismCreate)) {
						self::save($infosOrganismCreate);
						header('HTTP/1.1 201 CREATED');
					} else {
					 	return $infosOrganismCreate;
					}
				}else {
					self::updateOrganisme($infosOrganism);
					header('HTTP/1.1 200 OK');
				}
            } else {
                return $infosOrganism;
            }
        } catch(Exception $e) {
                $messageErreur=$e->getMessage();
                return self::erreur($infosOrganism['id'], array("500", "Internal Server Error", $messageErreur, "006"));
            }
        exit;
    }
    public function save($infosOrganism)
    {
        
        $db=new Atexo_DbSocle();
        $db->begin();
        
        try {
                    $sql="INSERT INTO Organisme(acronyme, type_article_org, denomination_org, categorie_insee, denomination_courte,   "
                    ."description_org, adresse, cp, ville, canton,active, email, url,date_creation,date_modification,id_initial) "
                    ."VALUES('".addslashes($infosOrganism['acronyme'])."', '"
                    .addslashes($infosOrganism['type_article_org'])."', '"
                    .addslashes($infosOrganism['denomination_org'])."', '"
                    .addslashes($infosOrganism['categorie_insee'])."', '"
                    .addslashes($infosOrganism['sigle'])."', '"
                    .addslashes($infosOrganism['description_org'])."', '"
                    .addslashes($infosOrganism['adresse'])."', '"
                    .addslashes($infosOrganism['cp'])."', '"
                    .addslashes($infosOrganism['ville'])."', '"
                    .addslashes($infosOrganism['canton'])."', '"
                    .addslashes($infosOrganism['active'])."', '"
                    .addslashes($infosOrganism['email'])."', '"
                    .addslashes($infosOrganism['url'])."', now(),now(),'"
                    .addslashes($infosOrganism['id_initial'])."'); ";
                
        
                if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql)) {
                    throw new Exception("Bad query : mysql respond : ".$db->errorMessage());
                }
                
                
                $db->commit();
        } catch(Exception $e) {
            $db->rollback();
            throw $e;
        }
        
    }
    
    public function updateOrganisme($infosOrganism)
    {
        try{
            $updateQuery = "UPDATE `Organisme` SET `denomination_org` = '".addslashes($infosOrganism['denomination_org'])."', ";
            $updateQuery .= " `categorie_insee` = '".addslashes($infosOrganism['categorie_insee'])."', ";
            $updateQuery .= "`description_org` = '".addslashes($infosOrganism['description_org'])."', ";
            $updateQuery .= "`adresse` = '".addslashes($infosOrganism['adresse'])."', ";
            $updateQuery .= "`cp` = '".addslashes($infosOrganism['cp'])."',";
            $updateQuery .= "`ville` = '".addslashes($infosOrganism['ville'])."', ";
            $updateQuery .= "`canton` = '".addslashes($infosOrganism['canton'])."',";
            $updateQuery .= "`email` = '".addslashes($infosOrganism['email'])."', ";
            $updateQuery .= "`url` = '".addslashes($infosOrganism['url'])."', ";
            $updateQuery .= "`active` = '".addslashes($infosOrganism['active'])."', ";
            $updateQuery .= "`denomination_courte` = '".addslashes($infosOrganism['sigle'])."', ";
            $updateQuery .= "`type_article_org` = '".addslashes($infosOrganism['type_article_org'])."', ";
            $updateQuery .= "`date_modification` = now() ";
            $updateQuery .= " WHERE acronyme ='".(addslashes($infosOrganism['acronyme']))."' ;";
            
            $db=new Atexo_DbSocle();
            if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $updateQuery))  {
              	throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
            }
            
        }catch(Exception $e) {
            throw $e;
        }
    }
    
    
    public function delete()
    {
        return "ok pour DELETE";
    }
    
    
    
    
}