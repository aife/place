<?php

class EmployeeJeb extends Employee
{
	public function create()
	{
		try{
			$inscritInfo = self::generateDataCreate();
			if (is_array($inscritInfo)) {
				self::save($inscritInfo);
				header('HTTP/1.1 201 CREATED');
			} else {
				return $inscritInfo;
			}
		} catch(Exception $e) {
			$messageErreur=$e->getMessage();
			return self::erreur($inscritInfo['uidInscrit'], array("500", "Internal Server Error", $messageErreur, "006"));
		}
		exit;
	}

	public function update($id)
	{
		try{
			$inscritInfo = self::generateDataUpdate();
			if (is_array($inscritInfo)) {
				if ($inscritInfo[0] == "-1") {
					$inscritInfoCreate = self::generateDataCreate($inscritInfo[1]);
					if (is_array($inscritInfoCreate)) {
						self::save($inscritInfoCreate);
						header('HTTP/1.1 201 CREATED');
					} else {
						return $inscritInfoCreate;
					}
				}else {
					self::updateEmployee($inscritInfo);
					header('HTTP/1.1 200 OK');
				}
			} else {
				return $inscritInfo;
			}
		} catch(Exception $e) {
			$messageErreur=$e->getMessage();
			return self::erreur($inscritInfo['uidInscrit'], array("500", "Internal Server Error", $messageErreur, "006"));
		}
		exit;
	}

	public function updateEmployee($inscritInfo)
	{
		try {
			$updateQuery = "UPDATE Inscrit SET " ;
			$updateQuery .="nom ='".(addslashes($inscritInfo['firstname']))."',";
			$updateQuery .="prenom ='".(addslashes($inscritInfo['lastname']))."',";
			$updateQuery .="login ='".(addslashes($inscritInfo['login']))."',";
			$updateQuery .="adresse ='".(addslashes($inscritInfo['postalAddress']))."',";
			$updateQuery .="codepostal='".(addslashes($inscritInfo['postalCode']))."',";
			$updateQuery .="ville='".(addslashes($inscritInfo['city']))."',";
			$updateQuery .="pays='".(addslashes($inscritInfo['country']))."',";
			$updateQuery .="email='".(addslashes($inscritInfo['email']))."',";
			$updateQuery .="telephone='".(addslashes($inscritInfo['phone']))."',";
			if($inscritInfo['enabled']) {
				$updateQuery .="bloque='0',";
			}
			else {
				$updateQuery .="bloque='1',";
			}
			$updateQuery .="num_fax='".(addslashes($inscritInfo['fax']))."',";
			$updateQuery .="siret='".(addslashes($inscritInfo['siret']))."',";
			$updateQuery .="date_modification=now()";
			$updateQuery .= " WHERE id_initial ='".$inscritInfo['externalId']."' ;";
			 
			$db=new Atexo_DbSocle();
			if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $updateQuery)) {
				throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
			}

		} catch(Exception $e) {
			throw $e;
		}
	}

	public function save($inscritInfo)
	{
		$db=new Atexo_DbSocle();
		$db->begin();

		try {
			$sql=" INSERT INTO Inscrit(entreprise_id, nom, prenom, login, adresse, codepostal, ville, pays, email,";
			$sql.=" telephone, bloque, num_fax, siret, date_creation, date_modification, id_initial) VALUES (" ;
			$sql.="'".(addslashes($inscritInfo['entrepriseId']))."',";
			$sql.="'".(addslashes($inscritInfo['firstname']))."',";
			$sql.="'".(addslashes($inscritInfo['lastname']))."',";
			$sql.="'".(addslashes($inscritInfo['login']))."',";
			$sql.="'".(addslashes($inscritInfo['postalAddress']))."',";
			$sql.="'".(addslashes($inscritInfo['postalCode']))."',";
			$sql.="'".(addslashes($inscritInfo['city']))."',";
			$sql.="'".(addslashes($inscritInfo['country']))."',";
			$sql.="'".(addslashes($inscritInfo['email']))."',";
			$sql.="'".(addslashes($inscritInfo['phone']))."',";
			if($inscritInfo['enabled']) {
				$sql.="'0',";
			}
			else {
				$sql.="'1',";
			}
			$sql.="'".(addslashes($inscritInfo['fax']))."',";
			$sql.="'".(addslashes($inscritInfo['siret']))."',";
			$sql.="now(),";
			$sql.="now(),";
			$sql.="'".(addslashes($inscritInfo['externalId']))."');";

			if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql)) {
				//return self::erreur(null, array("500", "Bad Request", "Mysql responded : ".$db->errorMessage(), "006"));
				throw new Exception("Bad query : mysql respond : ".$sql);
			}
			$db->commit();
		} catch(Exception $e) {
			$db->rollback();
			throw $e;
		}
	}
	
	public function delete($id)
	{
		$headers=apache_request_headers();

		if(!isset($headers['employeeProfileExternalId'])) {
			return self::erreur($id, array("500", "Internal Server Error", "employeeProfileExternalId not sent in header", "006"));
		}
		$idInitial = $headers['employeeProfileExternalId'];

		$inscrit = (new Atexo_Queries())->getInscritByIdInitial($idInitial);
		if(!$inscrit) {
			$messageErreur="employee '".$id."' not found";
			return self::erreur($id, array("404", "Not Found", $messageErreur, "003"));
		}
        $prefixeSuprime=Atexo_ConfigSocle::getParameter('PREFIXE_SUPPRIME');
		$email=$prefixeSuprime.$inscrit['id']."_".$inscrit['email'];
		$login=$prefixeSuprime.$inscrit['id']."_".$inscrit['login'];
		
		$sqlQuery = "UPDATE Inscrit SET bloque='1', login = '".addslashes($login)."' , email='".addslashes($email)."' , date_modification = NOW() WHERE id_initial ='".$idInitial."' AND id_initial !='';";
		$db=new Atexo_DbSocle();
		if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sqlQuery)) {
			return self::erreur($id, array("500", "Internal Server Error", "Mysql responded : ".$db->errorMessage(), "006"));
		}
		header('HTTP/1.1 200 OK');
		exit;
	}
}