<?php


class Department extends Atexo_Action_Actions
{
	public function generateDataCreate($serviceCommonDb = false,$champsVerificationService = null, $xml=null, $organisme=false)
	{
		if (!$xml) {
			$xml=@file_get_contents('php://input');
		}
		$verification=self::isValid($xml);
		if($verification!==true) {
			return $verification;
		}

		$infosService=self::processXml($xml);

		$acronymeOrganisme=$infosService['acronyme'];
		$verification=self::verifierExistanceOrganisme($acronymeOrganisme, $infosService['uidOrganisme']);
		if($verification!==true) {
			return $verification;
		}

		$idInitialService=$infosService['id'];
		if ($serviceCommonDb) {
			$dateBase = Atexo_ConfigSocle::getParameter("COMMON_DB");
		} else {
			$dateBase = $acronymeOrganisme;
		}
		if($organisme){
			$organisme = $acronymeOrganisme;
		}
		$verification=self::serviceExists($dateBase, $idInitialService,$champsVerificationService, $infosService['uidService'], $organisme);
		if($verification!==false) {
			return $verification;
		}
		$idInitialServiceParent = $infosService['parent']['id'];
		if ($infosService['parent']['id']!=="0"){
			if($infosService['parent']['id']=="-1") {
				$messageError = "parentDepartment not found (rootDepartment is false)";
				return self::erreur($infosService['parent']['uid'], array("400", "Bad Request", $messageError, "001"));
			}
			$verificationParent=(new Atexo_Queries())->getServiceByIdInitial($dateBase, $idInitialServiceParent,$champsVerificationService, $organisme);
			//$verificationParent=self::verifierExistanceService($dateBase, $idInitialServiceParent,$champsVerificationService, $infosService['parent']['uid']);
			if($verificationParent===false) {
				$messageError = "parent Department not found ";
				return self::erreur($infosService['parent']['uid'], array("404", "Not Found", $messageError, "004", "ORGANISM_DEPARTMENT"));
			}
		}
		return $infosService;
	}

	public function generateDataUpdate($serviceCommonDb = false,$champsVerificationService = null, $organisme=null)
	{
		$xml=@file_get_contents('php://input');

		$verification=self::isValid($xml);
		if($verification!==true) {
			return $verification;
		}

		$infosService=self::processXml($xml);

		$acronymeOrganisme=$infosService['acronyme'];
		$verification=self::verifierExistanceOrganisme($acronymeOrganisme, $infosService['uidOrganisme']);
		if($verification!==true) {
			return $verification;
		}

		$idInitialService=$infosService['id'];
		if ($serviceCommonDb) {
			$dateBase = Atexo_ConfigSocle::getParameter("COMMON_DB");
		} else {
			$dateBase = $acronymeOrganisme;
		}
		if($organisme){
			$organisme = $acronymeOrganisme;
		}
		$verification=self::verifierExistanceService($dateBase, $idInitialService,$champsVerificationService, $infosService['uidService'], $organisme);
		if(!is_array($verification)) {
			return array("-1", $xml);//return $verification;
		}
		$idInitialServiceParent = $infosService['parent']['id'];
		if ($infosService['parent']['id']!=="0"){
			if($infosService['parent']['id']=="-1") {
				$messageError = "parentDepartment not found (rootDepartment is false)";
				return self::erreur($infosService['parent']['id'], array("400", "Bad Request", $messageError, "001"));
			}
			$verificationParent=self::verifierExistanceService($dateBase, $idInitialServiceParent,$champsVerificationService, $infosService['parent']['uid'], $organisme);
			if(!is_array($verificationParent)) {
				return self::erreur($infosService['parent']['uid'], array("404", "Not Found", $messageError, "004", "ORGANISM_DEPARTMENT"));
			}
		}

		return $infosService;
	}

	public function delete($id)
	{

	}

	protected function processXml($xml)
	{
		$infosService=array();

		$domDocument = new DOMDocument;
		$domDocument->loadXML($xml);
		$organismDepartment = $domDocument?->getElementsByTagName('organismDepartment')?->item(0);
		$arrayRootDepartment = $organismDepartment?->getElementsByTagName('rootDepartment');
		foreach($arrayRootDepartment as $oneElem) {
			$rootDepartement = $oneElem?->nodeValue;
		}
		$infosService['id']=$organismDepartment?->getElementsByTagName('externalId')?->item(0)?->nodeValue;
		$infosService['uidService']=$organismDepartment?->getElementsByTagName('id')?->item(0)?->nodeValue;
		$infosService['service']=$organismDepartment?->getElementsByTagName('description')?->item(0)?->nodeValue;
		$infosService['sigle']=$organismDepartment?->getElementsByTagName('abbreviation')?->item(0)?->nodeValue;

		$adress=$organismDepartment?->getElementsByTagName('address')?->item(0);
		$infosService['adresse']=$adress?->getElementsByTagName('postalAddress')?->item(0)?->nodeValue;
		$infosService['cp']=$adress?->getElementsByTagName('postalCode')?->item(0)?->nodeValue;
		$infosService['ville']=$adress?->getElementsByTagName('city')?->item(0)?->nodeValue;
		$infosService['pays']=$adress?->getElementsByTagName('country')?->item(0)?->nodeValue;
		/*if($infosService['pays']) {
			$infosService['pays']=(new Atexo_Queries())?->getDenominationPaysByAcronyme($infosService['pays']);
			}*/

		$infosService['libelle']=$organismDepartment?->getElementsByTagName('label')?->item(0)?->nodeValue;
		$infosService['mail']=$organismDepartment?->getElementsByTagName('email')?->item(0)?->nodeValue;
		$infosService['telephone']=$organismDepartment?->getElementsByTagName('phone')?->item(0)?->nodeValue;
		$infosService['fax']=$organismDepartment?->getElementsByTagName('fax')?->item(0)?->nodeValue;

		$organism=$organismDepartment?->getElementsByTagName('organism')?->item(0);
		$infosService['uidOrganisme']=$organism?->getElementsByTagName('id')?->item(0)?->nodeValue;
		$infosService['acronyme']=$organism?->getElementsByTagName('acronym')?->item(0)?->nodeValue;
		if ($rootDepartement == "true") {
			$infosService['parent']['id']= "0";
			$infosService['parent']['uid']="0";
		}else {
			$parentDepartment=$organismDepartment?->getElementsByTagName('parentDepartment')?->item(0);
			if($parentDepartment) {
				$infosService['parent']['id']=$parentDepartment?->getElementsByTagName('externalId')?->item(0)?->nodeValue;
				$infosService['parent']['uid']=$parentDepartment?->getElementsByTagName('id')?->item(0)?->nodeValue;

			} else {
				$infosService['parent']['id']="-1";
				$infosService['parent']['uid']="-1";
			}
		}


		return $infosService;
	}

	public function serviceExists($acronymeOrganisme, $idServiceExterne, $champsVerificationService, $uidService=null, $organisme=null)
	{
		$service=(new Atexo_Queries())->getServiceByIdInitial($acronymeOrganisme, $idServiceExterne , $champsVerificationService, $organisme);
		if($service) {
			$messageErreur="Departement already exists";
			return self::erreur($uidService, array("400", "Bad Request", $messageErreur, "005"));
		}

		return false;
	}

	public function updateAffiliationService($infosService,$db, $serviceCommonDb = false,$champsVerificationService=null, $org=null)
	{

		if ($serviceCommonDb) {
			$dataBase = Atexo_ConfigSocle::getParameter("COMMON_DB");
		}else {
			$dataBase = $infosService["acronyme"];
		}
		if($org){
			$org = $infosService["acronyme"];
		}
		try {
			$service=(new Atexo_Queries())->getServiceByIdInitial($dataBase, $infosService['id'],$champsVerificationService, $org);
			$currentParent=(new Atexo_Queries())->getParent($dataBase, $service['id'], $org);
			if($infosService['parent']['id']!=="0") {
				if($infosService['parent']['id']=="-1") {
					throw new Exception("parentDepartment not found (rootDepartment is false)");
				}
				$servicesParent=(new Atexo_Queries())->getServiceByIdInitial($dataBase, $infosService['parent']['id'],$champsVerificationService, $org);
				if($servicesParent===false) {
					throw new Exception("Parent Department ".$infosService['parent']['id']." not found");
				}
				$newParent=$servicesParent['id'];
			} else {
				$newParent="0";
			}

			if($newParent!=$currentParent) {

				$subServices=(new Atexo_Queries())->getSubServices($dataBase, $service['id'], $org);

				if(in_array($newParent, $subServices)) {
					throw new Exception("Cannot link department to one of his children");
				}

				$sql="Delete from AffiliationService where service_id='".$service['id']."'";
				if($org){
					$sql.= " and organisme='".$org."'";
				}
				if(!$db->execute($dataBase, $sql)) {
					throw new Exception("Bad query1, mysql responded : ".$db->errorMessage());
				}

				if($newParent) {
					$sql="INSERT INTO AffiliationService (";
					if($org){
						$sql.= "organisme,";
					}
					$sql.= "service_parent_id,service_id) VALUES('";
					if($org){
						$sql.= addslashes($org)."', '";
					}
					$sql.= addslashes($newParent)."', '".addslashes($service['id'])."') ";
					if(!$db->execute($dataBase, $sql)) {
						throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
					}
				}
			}


		} catch(Exception $e) {
			throw $e;
		}
	}
	public function insertAffiliationService($infosService, $db,$serviceCommonDb = false, $champsVerificationService=null, $org=null){

		if ($serviceCommonDb) {
			$dataBase = Atexo_ConfigSocle::getParameter("COMMON_DB");
		}else {
			$dataBase = $infosService["acronyme"];
		}
		if($org){
			$org=$infosService["acronyme"];
		}

		if($infosService['parent']['id']!=="0") {
			if($infosService['parent']['id']=="-1") {
				throw new Exception("parentDepartment not found (rootDepartment is false)");
			}
			$serviceParent=(new Atexo_Queries())->getServiceByIdInitial($dataBase, $infosService['parent']['id'],$champsVerificationService, $org);
			if($serviceParent===false) {
				throw new Exception("Parent Department ".$infosService['parent']['id']." not found");
			}
			$idService=$infosService['idService'];

			$sql="INSERT INTO AffiliationService (";
			if($org) {
				$sql.="organisme,";
			}
			$sql.="service_parent_id,service_id) VALUES('";
			if($org) {
				$sql.=addslashes($infosService["acronyme"])."', '";
			}
			$sql.= addslashes($serviceParent['id'])."', '".addslashes($idService)."') ";
			if(!$db->execute($dataBase, $sql)) {
				throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
			}
		} else {
			$newParent="0";
		}
	}

	public function fillServicePathField($infosService, $db,$serviceCommonDb = false, $champsVerificationService=null, $org=null) {
		$pathService='NULL';
		if ($serviceCommonDb) {
			$dataBase = Atexo_ConfigSocle::getParameter("COMMON_DB");
		}else {
			$dataBase = $infosService["acronyme"];
		}
		if($org){
			$org = $infosService["acronyme"];
		}
		if($infosService['parent']['id']!=="0") {//rootDepartement=false
			if($infosService['parent']['id']=="-1") {
				throw new Exception("parentDepartment not found (rootDepartment is false)");
			}
			$serviceParent=(new Atexo_Queries())->getServiceByIdInitial($dataBase, $infosService['parent']['id'],$champsVerificationService, $org);
			//echo $serviceParent['path_service'];exit;
			if($serviceParent===false) {
				throw new Exception("Parent Department ".$infosService['parent']['id']." not found");
			} else {
				$arrayParentPathServ=explode(" - ",$serviceParent['path_service']);
				if(is_array($arrayParentPathServ)) {
					$pathService=$arrayParentPathServ[0]." / ".$infosService['sigle']." - ".$infosService['libelle'];
				}
			}
		} else {//rootDepartement=true
			$organisme = false;
			if($org){
				$organisme=(new Atexo_Queries())->getOrganismeByAcronyme($org);
			}
			
			if($organisme===false) {
				throw new Exception("Organisme ".$org." not found");
			} else {
				$sigleOrganisme=$organisme['sigle'];
				$pathService=$sigleOrganisme." / ".$infosService['sigle']." - ".$infosService['libelle'];
			}
		}
		//echo $pathService;exit;
		$sql="UPDATE Service SET ";
		if($org){
			$sql.= "chemin_complet='";
		} else{
			$sql.= "path_service='";
		}
		$sql.= addslashes($pathService)."' WHERE ".$champsVerificationService."='". $infosService['id'] ."'";
		if($org){
			$sql.= " AND organisme='".$org."'";
		} 
		if(!$db->execute($dataBase, $sql)) {
			throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
		}

	}

}
