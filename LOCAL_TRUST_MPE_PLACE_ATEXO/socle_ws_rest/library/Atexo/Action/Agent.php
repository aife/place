<?php


class Agent extends Atexo_Action_Actions
{
	public function generateDataCreate($serviceCommonDb = false,$champsVerificationAgent=false,$champsVerificationService=false, $xml =null)
	{
		if (!$xml) {
			$xml=@file_get_contents('php://input');	
		}
		$verification=self::isValid($xml);
		if($verification!==true) {
			return $verification;
		}
			
		$infosAgent=self::processXml($xml);
		
		$idOrganisme=null;
		$idService=null;
		
		$acronymeOrganisme=$infosAgent['acronyme'];
		$verification=self::verifierExistanceOrganisme($acronymeOrganisme, $infosAgent['uidOrganisme']);
		if($verification!==true) {
			return $verification;
		}
		$verification=self::verifierExistanceAgent($infosAgent['id'], $infosAgent['login'], $infosAgent['email'],$champsVerificationAgent, $infosAgent['uidAgent']);
		if($verification!==false) {
			return $verification;
		}
	    if(!isset($infosAgent['rolesAgent']['profil'])) {
			$messageErreur="Agent role not found";
			return self::erreur($infosAgent['uidAgent'], array("404", "Not Found", $messageErreur,"004", "AGENT_PROFILE"));
		}
		
		if($infosAgent['rootDepartment']) {
			$idService="0";
		} else {
			$idInitialService=$infosAgent['idService'];
			if (!Atexo_ConfigSocle::getParameter("VERIFIER_EXISTENCE_SERVICE_AGENT")) {
		    	$idService= $idInitialService;
			} else {
				if ($serviceCommonDb) {
					$dateBase = Atexo_ConfigSocle::getParameter("COMMON_DB");
				} else {
					$dateBase = $acronymeOrganisme;
				}
				$verification=self::verifierExistanceService($acronymeOrganisme, $idInitialService,$champsVerificationService, $infosAgent['uidService']);
				if(!is_array($verification)) {
					return $verification;
				}
				$service=$verification;
				$idService=$service['id'];
			}
		}
		$infosAgent['idService'] = $idService;
		$infosAgent['idInitialService'] = $idInitialService;
		return $infosAgent;
	}
	
	
	
	public function generateDataUpdate($serviceCommonDb = false,$champsVerificationAgent = false,$champsVerificationService = false)
	{
		$xml=@file_get_contents('php://input');	
		
		$verification=self::isValid($xml);
		if($verification!==true) {
			return $verification;
		}
		
		$infosAgent=self::processXml($xml);
		
		
		$agent=(new Atexo_Queries())->getAgentByIdInitial($infosAgent['id'],$champsVerificationAgent);
		
		if(!$agent) {
			return array("-1", $xml);//self::erreur(null, array("404", "Bad Request", "Agent ".$infosAgent['id']." not found", "003"));
		}
		
		if(($agent['login']!=$infosAgent['login']) && isset($agent['login'])) {
			$verification=self::verifierExistanceAgent(null, $infosAgent['login'], null, $champsVerificationAgent, $infosAgent['uidAgent']);
			if($verification!==false) {
				return $verification;
			}
		}
		
		if($agent['email']!=$infosAgent['email']) {
			$verification=self::verifierExistanceAgent(null, null , $infosAgent['email'], $champsVerificationAgent, $infosAgent['uidAgent']);
			if($verification!==false) {
				return $verification;
			}
		}
		
		$acronymeOrganisme=$infosAgent['acronyme'];		
		$verification=self::verifierExistanceOrganisme($acronymeOrganisme, $infosAgent['uidOrganisme']);
                if($verification!==true) {
                        return $verification;
                }




		$idInitialService=$infosAgent['idService'];
		
		if($infosAgent['rootDepartment']) {
			$idService="0";
		} else {
		    if (!Atexo_ConfigSocle::getParameter("VERIFIER_EXISTENCE_SERVICE_AGENT")) {
		    	$idService= $idInitialService;
			} else {
			    if ($serviceCommonDb) {
					$dateBase = Atexo_ConfigSocle::getParameter("COMMON_DB");
				} else {
					$dateBase = $acronymeOrganisme;
				}
				$verification=self::verifierExistanceService($dateBase, $idInitialService,$champsVerificationService, $infosAgent['uidService']);
				if(!is_array($verification)) {
					return $verification;
				}
				$service=$verification;
				$idService=$service['id'];
			}
		}
		$infosAgent['idService'] = $idService;
		$infosAgent['idInitialService'] = $idInitialService;
	
		return $infosAgent;
	}
	
	public function delete($id)
	{
        $headers = array_change_key_case(apache_request_headers(), CASE_UPPER);
		if(!isset($headers['agentProfileExternalId'])&&!isset($headers['AGENTPROFILEEXTERNALID'])) {
			$message="Agent id not sent in header";
			return self::erreur($id, array("500", "Internal Server Error", $message,"006"));
		}
		
		$idInitial = isset($headers['agentProfileExternalId']) ? $headers['agentProfileExternalId'] : $headers['AGENTPROFILEEXTERNALID'];
		$agent=(new Atexo_Queries())->getAgentByIdInitial($idInitial);
		
		if(!$agent) {
			return self::erreur($id, array("404", "Not Found", "Agent ".$id." not found", "003"));
		}
		
		$sql="UPDATE `Agent` set deleted_at = now(), service_id = NULL, tentatives_mdp='3'  where `id_externe`='".addslashes($idInitial)."'";
		$db=new Atexo_DbSocle();
		if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql)) {
			return self::erreur($id, array("500", "Internal Server Error", "Bad query, mysql responded : ". $db->errorMessage(), "006"));
		}
		header('HTTP/1.1 200 OK');
		exit;
	}
	
	protected function processXml($xml)
	{
		$infosAgent=array();
		
		$domDocument = new DOMDocument;
		$domDocument->loadXML($xml);
		$agentProfile = $domDocument?->getElementsByTagName('agentProfile')?->item(0);
		
		$infosAgent['id']=$agentProfile?->getElementsByTagName('externalId')?->item(0)?->nodeValue;
		$infosAgent['uidAgent']=$agentProfile?->getElementsByTagName('id')?->item(0)?->nodeValue;
		
		$adresse=$agentProfile?->getElementsByTagName('address')?->item(0);
		$infosAgent['adresse']['canton']=$adresse?->getElementsByTagName('canton')?->item(0)?->nodeValue;
		$infosAgent['adresse']['city']=$adresse?->getElementsByTagName('city')?->item(0)?->nodeValue;
		$infosAgent['adresse']['country']=$adresse?->getElementsByTagName('country')?->item(0)?->nodeValue;
		$infosAgent['adresse']['adressePostale']=$adresse?->getElementsByTagName('postalAddress')?->item(0)?->nodeValue;
		$infosAgent['adresse']['postalCode']=$adresse?->getElementsByTagName('postalCode')?->item(0)?->nodeValue;
		
		$infosAgent['email']=$agentProfile?->getElementsByTagName('email')?->item(0)?->nodeValue;
		
		$infosAgent['fax']=$agentProfile?->getElementsByTagName('fax')?->item(0)?->nodeValue;
		
		$infosAgent['tel']=$agentProfile?->getElementsByTagName('phone')?->item(0)?->nodeValue;
		$prefComMedia = $agentProfile?->getElementsByTagName('prefComMedia')?->item(0)?->nodeValue;
		if ($prefComMedia == "EMAIL") {
		    $infosAgent['type_comm'] = "2"; //email
		} else {
		    $infosAgent['type_comm'] = "1"; 
		}
		$user=$agentProfile?->getElementsByTagName('user')?->item(0);
		$infosAgent['nom']=$user?->getElementsByTagName('lastname')?->item(0)?->nodeValue;
		$infosAgent['prenom']=$user?->getElementsByTagName('firstname')?->item(0)?->nodeValue;
		$infosAgent['login']=$user?->getElementsByTagName('username')?->item(0)?->nodeValue;
		$infosAgent['civilite']=ucfirst(strtolower($user?->getElementsByTagName('civility')?->item(0)?->nodeValue));
		
		$infosAgent['elu']=($agentProfile?->getElementsByTagName('elected')?->item(0)?->nodeValue)=="false"? '0':'1';
		
		$infosAgent['fonction']=$agentProfile?->getElementsByTagName('function')?->item(0)?->nodeValue;
		$enabled = $agentProfile?->getElementsByTagName('enabled')?->item(0)?->nodeValue;
		if ($enabled == "true") {
		    $infosAgent['enabled'] = true;
		} else {
		    $infosAgent['enabled'] = false;
		}
		
		
		$roles=$agentProfile?->getElementsByTagName('roles')?->item(0);
		$role=$agentProfile?->getElementsByTagName('role');
		$indice=1;
		foreach($role as $element) {
			
			$externalIdProfil=$element?->getElementsByTagName('externalId')?->item(0)?->nodeValue;
			$roleName=$element?->getElementsByTagName('name')?->item(0)?->nodeValue;
			
			$application=$element?->getElementsByTagName('application')?->item(0);
			$applicationName=strtolower($application?->getElementsByTagName('name')?->item(0)?->nodeValue);
			$idApplication=strtolower($application?->getElementsByTagName('externalId')?->item(0)?->nodeValue);
			if($idApplication==Atexo_ConfigSocle::getParameter('SOCLE_WS_REST_ID_APPLICATION')) {
				$infosAgent['rolesAgent']=array("profil"=>$externalIdProfil,"roleName"=>$roleName, "idApplication"=>$idApplication);
			}
		}
		
		$organismDepartment=$agentProfile?->getElementsByTagName('organismDepartment')?->item(0);
		$infosAgent['idService']=$organismDepartment?->getElementsByTagName('externalId')?->item(0)?->nodeValue;
		$infosAgent['uidService']=$organismDepartment?->getElementsByTagName('id')?->item(0)?->nodeValue;
		$infosAgent['labelService']=$organismDepartment?->getElementsByTagName('label')?->item(0)?->nodeValue;
		
		
		$arrayRootDepartment = $organismDepartment?->getElementsByTagName('rootDepartment');
		foreach($arrayRootDepartment as $oneElem) {
			$rootDepartement = $oneElem?->nodeValue;
		}
		$infosAgent['rootDepartment']=$rootDepartement=="true"? true:false;
		
		$organism=$organismDepartment?->getElementsByTagName('organism')?->item(0);
		$infosAgent['idOrganisme']=$organism?->getElementsByTagName('externalId')?->item(0)?->nodeValue;
		$infosAgent['uidOrganisme']=$organism?->getElementsByTagName('id')?->item(0)?->nodeValue;
		$infosAgent['labelOrganisme']=$organism?->getElementsByTagName('label')?->item(0)?->nodeValue;
		$infosAgent['acronyme']=$organism?->getElementsByTagName('acronym')?->item(0)?->nodeValue;
		
		return $infosAgent;
	}
	
	public function verifierExistanceAgent($id=null, $login=null, $mail=null,$champsVerificationAgent=null, $uidAgent=null) 
	{
		if($id!==null) {
			$agent=(new Atexo_Queries())->getAgentByIdInitial($id,$champsVerificationAgent);
			if($agent) {
				$messageErreur="Agent already exists.";
				
				return self::erreur($uidAgent, array("400", "Bad Request", $messageErreur, "005"));
			}
		}
		
		
		if($login!==null) {
			$agent=(new Atexo_Queries())->getAgentByLogin($login);
			if($agent) {
				$messageErreur="Agent with same login '".$login."' already exists";
				
				return self::erreur($uidAgent, array("400", "Bad Request", $messageErreur, "005"));
			}
		}
		
		/*if($mail!==null) {		
			$agent=(new Atexo_Queries())->getAgentByMail($mail);
			if($agent) {
				$messageErreur="Agent with same email '".$mail."' already exists";
				return self::erreur($uidAgent, array("400", "Bad Request", $messageErreur, "005"));
			}
		}*/
		
		return false;
		
	}
}