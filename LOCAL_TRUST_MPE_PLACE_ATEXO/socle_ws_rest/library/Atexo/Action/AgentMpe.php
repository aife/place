<?php


class AgentMpe extends Agent
{
    /**
     * Permet de determiner le type d'action realiser :
     * creation(create) ou mise a jour(update)
     *
     * @var null
     */
    private $action = null;

    public function create()
    {
        return $this->createOrUpdate();
    }

    public function update($id)
    {
        return $this->createOrUpdate($id);
    }

    private function createOrUpdate($id = null)
    {
        try{
            $infosAgent = $this->generateData(false, "id_externe", "id_externe");
            if (is_array($infosAgent)) {
                $this->save($infosAgent);
                header('HTTP/1.1 '.("create" == $this->action?'201 CREATED':'200 OK'));
            } else {
                return $infosAgent;
            }
        } catch(Exception $e) {
            $messageErreur=$e->getMessage();
            return $this->erreur($infosAgent['uidAgent'], array("500", "Internal Server Error", $messageErreur, "006"));
        }
        exit;
    }

    public function delete($id)
    {
        $headers = array_change_key_case(apache_request_headers(), CASE_UPPER);

        if(!isset($headers['AGENTPROFILEEXTERNALID'])) {
            $message="Agent id not sent in header";
            return $this->erreur($id, array("500", "Internal Server Error", $message,'006'));
        }

        $idInitial=$headers['AGENTPROFILEEXTERNALID'];

        $agent=(new Atexo_Queries())->getAgentByIdInitial($idInitial);

        if(!$agent) {
            return $this->erreur($id, array("404", "Not Found", "Agent ".$id." not found", "003"));
        }
        $prefixeSuprime = Atexo_ConfigSocle::getParameter('PREFIXE_SUPPRIME');
        $email      = addslashes($prefixeSuprime.$agent['id']."_".$agent['email']);
        $login      = addslashes($prefixeSuprime.$agent['id']."_".$agent['login']);
        $id_externe = addslashes($idInitial);
        $sql = <<<EOF
UPDATE `Agent` SET
    deleted_at = now(),
    service_id = NULL,
    tentatives_mdp='3',
    email='$email',
    login='$login' ,
    date_modification = now(),
    actif='0'
WHERE
    `id_externe`='$id_externe'

EOF;

        $db=new Atexo_DbSocle();
        if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql)) {
            return $this->erreur($id, array("500", "Internal Server Error", "Bad query, mysql responded : ". $db->errorMessage(), "006"));
        }
        (new Atexo_Queries())->deleteAgentAccessApplication($agent["id"], $db);
        header('HTTP/1.1 200 OK');
        exit;
    }

    public function save($infosAgent)
    {
        $db=new Atexo_DbSocle();
        $db->begin();
        try {
            $idAgent = $this->saveOrUpdateAgent($infosAgent, $db);
            $atexoQueries = new Atexo_Queries();
            $profilApplication = $atexoQueries->getAccessAgentApplication($idAgent, Atexo_ConfigSocle::getParameter("SOCLE_WS_REST_ID_APPLICATION"));
            if ($infosAgent['enabled'] == true) {
                if ("Mpe" == Atexo_ConfigSocle::getParameter("PLATEFORME") && $profilApplication['id_profil_service'] != $infosAgent['rolesAgent']['profil']) {
                    $this->saveProfil($idAgent, $infosAgent['rolesAgent']['profil'], $db);
                }
                (new Atexo_Queries())->addAgentAccessApplication($idAgent, $infosAgent['rolesAgent']['profil'], $db);
            }else {
                (new Atexo_Queries())->deleteAgentAccessApplication($idAgent, $db);
            }
            $db->commit();
        } catch(Exception $e) {
            $db->rollback();
            throw $e;
        }
    }
    public function saveOrUpdateAgent($infosAgent, $db)
    {
        $agent = (new Atexo_Queries())->getAgentByIdInitial($infosAgent['id'], 'id_externe');//$this->verifierExistanceAgent($infosAgent['id'], $infosAgent['login'], null, "id_externe");

        $login          = addslashes($infosAgent['login']);
        $email          = addslashes($infosAgent['email']);
        $nom            = addslashes($infosAgent['nom']);
        $prenom         = addslashes($infosAgent['prenom']);
        $organisme      = addslashes($infosAgent['acronyme']);
        $service_id      = null !== $infosAgent['idService'] ? "'" . addslashes($infosAgent['idService']) . "'" : "NULL";
        $num_tel        = addslashes($infosAgent['tel']);
        $num_fax        = addslashes($infosAgent['fax']);
        $civilite       = addslashes($infosAgent['civilite']);
        $adr_postale    = addslashes($infosAgent['adresse']['adressePostale']);
        $id_externe     = addslashes($infosAgent['id']);
        $elu            = addslashes($infosAgent['elu']);
        $nom_fonction   = addslashes($infosAgent['fonction']);
        $id_profil_socle_externe = addslashes($infosAgent['rolesAgent']['profil']);
        $type_comm      = addslashes($infosAgent['type_comm']);

        $sqlAgent = "";

        if($agent == false) {
            $this->action = "create";
            $sqlAgent = <<<EOF
INSERT INTO Agent(
  login, email, nom, prenom, organisme,
  service_id, num_tel, num_fax, civilite,
  adr_postale, date_creation, date_modification, id_externe,
  elu, nom_fonction, id_profil_socle_externe, type_comm)
VALUES(
  '$login', '$email', '$nom', '$prenom','$organisme',
  $service_id, '$num_tel', '$num_fax','$civilite',
  '$adr_postale', now(), now(), '$id_externe',
  '$elu', '$nom_fonction', '$id_profil_socle_externe', '$type_comm')

EOF;

        } else {
            $this->action = "update";
            $sqlAgent = <<<EOL
UPDATE `Agent` SET
    `login` = '$login',
    `email` = '$email',
    `nom` = '$nom',
    `prenom` = '$prenom',
    `elu` = '$elu',
    `num_tel` = '$num_tel',
    `num_fax` = '$num_fax',
    `adr_postale` = '$adr_postale',
    `civilite` = '$civilite',
    `type_comm` = '$type_comm',
    `nom_fonction` = '$nom_fonction',
    `date_modification` = now(),
    `service_id` = $service_id
WHERE
    `Agent`.`id_externe` = '$id_externe'

EOL;
        }
        if($db===null) {
            $db=new Atexo_DbSocle();
        }
        if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sqlAgent)) {
            throw new Exception("Bad query : mysql respond : ".$db->errorMessage());
        }
        if($agent==false) {
            $sql="select last_insert_id() as id";
            $ressource=$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql);
            $resultat=$db->fetchArray($ressource);
            $idAgent=$resultat['id'];
        }else {
            $idAgent= $agent["id"];
        }
        return $idAgent;
    }

    public function saveProfil($idAgent, $profilExterne, $db=null)
    {
        if($db===null) {
            $db=new Atexo_DbSocle();
        }
        $profil = (new Atexo_Queries())->getProfilById($profilExterne);
        if(!$profil) {
            $messageErreur="Profile ".$profilExterne." not found";
            throw new Exception($messageErreur);
        }
        $this->insertOrUpdateHabilitation($idAgent, $profil, $db);
    }

    public function insertOrUpdateHabilitation($idAgent, $profil, $db=null)
    {
        $profil['id_agent']=$idAgent;
        $hasHabilitation = "SELECT * FROM `HabilitationAgent` WHERE `id_agent` = '$idAgent'";
        $return = $db->fetchArray($db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $hasHabilitation));
        if(is_array($return) && in_array($idAgent,$return)){
            $deleteHabilitation  = "DELETE FROM `HabilitationAgent` WHERE  `id_agent`='$idAgent'";
            $db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $deleteHabilitation);
        }
        unset($profil['libelle']);
        unset($profil['id']);

        $fields = '( `'.implode('`, `',array_keys($profil)).'` )';
        $values = '( \''.implode('\', \'',$profil).'\' )';
        $sqlHabilitation = <<<EOF
INSERT INTO `HabilitationAgent`
    $fields
VALUES
    $values

EOF;
        if($db===null) {
            $db=new Atexo_DbSocle();
        }
        if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sqlHabilitation)) {
            throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
        }
    }

    public function generateData($serviceCommonDb = false,$champsVerificationAgent=false,$champsVerificationService=false, $xml =null)
    {
        if (!$xml) {
            $xml=@file_get_contents('php://input');
        }
        $verification=$this->isValid($xml);
        if($verification!==true) {
            return $verification;
        }

        $infosAgent=$this->processXml($xml);

        $idOrganisme=null;
        $idService=null;

        $acronymeOrganisme=$infosAgent['acronyme'];
        $verification=$this->verifierExistanceOrganisme($acronymeOrganisme, $infosAgent['uidOrganisme']);
        if($verification!==true) {
            return $verification;
        }

        /* Ce control bloque la MAJ, je le laisse pour que l'on discute de ce morceau de code
        $verification=$this->verifierExistanceAgentAccessApplication($infosAgent['id'], $infosAgent['login'], $infosAgent['email'],$champsVerificationAgent, $infosAgent['uidAgent']);
        if($verification!==false) {
            return $verification;
        }
        */

        if(!isset($infosAgent['rolesAgent']['profil'])) {
            $messageErreur="Agent role not found";
            return $this->erreur($infosAgent['uidAgent'], array("404", "Not Found", $messageErreur,"004", "AGENT_PROFILE"));
        }

        if($infosAgent['rootDepartment']) {
            $idService=null;
        } else {
            $idInitialService=$infosAgent['idService'];
            if (!Atexo_ConfigSocle::getParameter("VERIFIER_EXISTENCE_SERVICE_AGENT")) {
                $idService= $idInitialService;
            } else {
                $dateBase = Atexo_ConfigSocle::getParameter("COMMON_DB");
                $verification=$this->verifierExistanceService($dateBase, $idInitialService,$champsVerificationService, $infosAgent['uidService'],$acronymeOrganisme);
                if(!is_array($verification)) {
                    return $verification;
                }
                $service=$verification;
                $idService=$service['id'];
            }
        }
        $infosAgent['idService'] = $idService;
        $infosAgent['idInitialService'] = $idInitialService;
        return $infosAgent;
    }

    public function verifierExistanceAgentAccessApplication($id=null, $login=null, $mail=null,$champsVerificationAgent=null, $uidAgent)
    {
        $agentExist = false;
        if($id!==null) {
            $agent=(new Atexo_Queries())->getAgentByIdInitial($id,$champsVerificationAgent);

            if($agent) {
                $agentExist = true;
            }
        }
        if($login!==null) {
            $agent=(new Atexo_Queries())->getAgentByLogin($login);
            if($agent) {
                $agentExist = true;
            }
        }

        return $agentExist;

    }
}
