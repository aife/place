<?php

class Employee extends Atexo_Action_Actions
{
	protected function processXml($xml)
	{
		$inscrit = array(); 
		$domDocument = new DOMDocument;
		$domDocument->loadXML($xml);
		$tagInscrit = $domDocument?->getElementsByTagName('employeeProfile')?->item(0);
		$tagCompany = $tagInscrit?->getElementsByTagName('company')?->item(0);
		if(!empty($tagInscrit?->getElementsByTagName('establishment'))){
			$tagEtablissement = $tagInscrit?->getElementsByTagName('establishment')?->item(0);
		}
		$inscrit['externalId'] = $tagInscrit?->getElementsByTagName('externalId')?->item(0)?->nodeValue;
		$inscrit['uidInscrit'] = $tagInscrit?->getElementsByTagName('id')?->item(0)?->nodeValue;
		$inscrit['siren'] = $tagCompany?->getElementsByTagName('siren')?->item(0)?->nodeValue;
		$inscrit['uidCompany'] = $tagCompany?->getElementsByTagName('id')?->item(0)?->nodeValue;
		$inscrit['entrepriseExternalId'] = $tagCompany?->getElementsByTagName('externalId')?->item(0)?->nodeValue;

		$inscrit['lastname'] = $tagInscrit?->getElementsByTagName('lastname')?->item(0)?->nodeValue;
		$inscrit['firstname'] = $tagInscrit?->getElementsByTagName('firstname')?->item(0)?->nodeValue;
		$inscrit['login'] = $tagInscrit?->getElementsByTagName('username')?->item(0)?->nodeValue;
		$inscrit['postalAddress'] = $tagInscrit?->getElementsByTagName('postalAddress')?->item(0)?->nodeValue;
		$inscrit['postalCode'] = $tagInscrit?->getElementsByTagName('postalCode')?->item(0)?->nodeValue;
		$inscrit['city'] = $tagInscrit?->getElementsByTagName('city')?->item(0)?->nodeValue;
		$inscrit['country'] = $tagInscrit?->getElementsByTagName('country')?->item(0)?->nodeValue;
        $inscrit['email'] = $tagInscrit?->getElementsByTagName('email')?->item(0)?->nodeValue;
        $inscrit['phone'] = $tagInscrit?->getElementsByTagName('phone')?->item(0)?->nodeValue;
        $inscrit['enabled'] = $tagInscrit?->getElementsByTagName('enabled')?->item(0)?->nodeValue;
        $inscrit['fax'] = $tagInscrit?->getElementsByTagName('fax')?->item(0)?->nodeValue;
        $inscrit['siret'] = $tagInscrit?->getElementsByTagName('nic')?->item(2)?->nodeValue;
        $inscrit['civility'] = $tagInscrit?->getElementsByTagName('civility')?->item(0)?->nodeValue;
        
        $roles=$tagInscrit?->getElementsByTagName('roles')?->item(0);
		$role=$tagInscrit?->getElementsByTagName('role');
		foreach($role as $element) {
			
			$externalIdProfil=$element?->getElementsByTagName('externalId')?->item(0)?->nodeValue;
			$roleName=$element?->getElementsByTagName('name')?->item(0)?->nodeValue;
			
			$application=$element?->getElementsByTagName('application')?->item(0);
			$applicationName=strtolower($application?->getElementsByTagName('name')?->item(0)?->nodeValue);
			$idApplication=strtolower($application?->getElementsByTagName('externalId')?->item(0)?->nodeValue);
			if($idApplication==Atexo_ConfigSocle::getParameter('SOCLE_WS_REST_ID_APPLICATION')) {
				$inscrit['rolesInscrits']=array("profil"=>$externalIdProfil,"roleName"=>$roleName, "idApplication"=>$idApplication);
			}
		}
		$company = (new Atexo_Queries())?->getCompanyByIdInitial($inscrit['entrepriseExternalId']);
		if($company) {
			$inscrit['entrepriseId'] = $company['id'];	
		} else {
			$companyClass = 'Company'.Atexo_ConfigSocle::getParameter("PLATEFORME");
            (new $companyClass())->create();
			$company = (new Atexo_Queries())->getCompanyByIdInitial($inscrit['entrepriseExternalId']);
			$inscrit['entrepriseId'] = $company['id'];
		}

		if(isset($tagEtablissement)) {
			$inscrit['etablissementExternalId'] = $tagEtablissement?->getElementsByTagName('externalId')?->item(0)?->nodeValue;
			//$inscrit['externalId_etab'] = $tagEtablissement?->getElementsByTagName('externalId')?->item(0)->nodeValue;
			$establishment = (new Atexo_Queries())->getEstablishmentByIdInitial($inscrit['etablissementExternalId']);
			if($establishment) {
				$inscrit['id_etablissement'] = $establishment['id_etablissement'];
			} else {
				$establishmentClass = 'Establishment'.Atexo_ConfigSocle::getParameter("PLATEFORME");
				$etablissement = new $establishmentClass;
				$etablissementInfos = $etablissement->processXml($xml);
				$etablissement->save($etablissementInfos);
				$establishment = (new Atexo_Queries())->getEstablishmentByIdInitial($inscrit['etablissementExternalId']);
				$inscrit['id_etablissement'] = $establishment['id_etablissement'];
			}
		}

        return $inscrit;
	}
	
	public function generateDataCreate($xml=null)
	{
    	if (!$xml) {
			$xml = @file_get_contents('php://input');			
    	}
		$verification=self::isValid($xml);
		if($verification!==true) {
			return $verification;
		}
		
		$inscritInfo = self::processXml($xml);
		
    	$inscrit = (new Atexo_Queries())->getInscritByIdInitial($inscritInfo['externalId']);
    	if($inscrit) {
    		$messageErreur="employee '".$inscritInfo['externalId']."' already exists";
			return self::erreur($inscritInfo['uidInscrit'], array("400", "Bad Request", $messageErreur, "005"));
    	}
    	
    	$inscrit = (new Atexo_Queries())->getInscritByLogin($inscritInfo['login']);
    	if($inscrit) {
    		$messageErreur="Employee with same login '".$inscritInfo['login']."' already exists";
			return self::erreur($inscritInfo['uidInscrit'], array("400", "Bad Request", $messageErreur, "005"));
    	}
    	
    	$inscrit = (new Atexo_Queries())->getInscritByEmail($inscritInfo['email']);
    	if($inscrit) {
    		$messageErreur="Employee with same email '".$inscritInfo['email']."' already exists";
			return self::erreur($inscritInfo['uidInscrit'], array("400", "Bad Request", $messageErreur, "005"));
    	}
		$company = (new Atexo_Queries())->getCompanyByIdInitial($inscritInfo['entrepriseExternalId']);
		if(!$company) {
		    $messageErreur="Company '".$inscritInfo['uidCompany']."' not found";
			return self::erreur($inscritInfo['uidCompany'], array("404", "Not Found", $messageErreur, "004", "COMPANY"));
		}
    	return $inscritInfo;
	}
	
	public function generateDataUpdate()
	{
		$xml = @file_get_contents('php://input');	
		
		$verification=self::isValid($xml);
		if($verification!==true) {
			return $verification;
		}
		
		$inscritInfo = self::processXml($xml);
		
        $inscrit = (new Atexo_Queries())->getInscritByIdInitial($inscritInfo['externalId']);
    	if(!$inscrit) {
    		return array("-1", $xml);
    		/*$messageErreur="employee '".$inscritInfo['externalId']."' not found";
			return self::erreur(null, array("404", "Bad Request", $messageErreur, "003"));*/
    	}
        
	     return $inscritInfo;
	}
	
	public function delete($id)
	{
        $headers = array_change_key_case(apache_request_headers(), CASE_UPPER);
		if(!isset($headers['employeeProfileExternalId'])&&!isset($headers['EMPLOYEEPROFILEEXTERNALID'])) {
			return self::erreur($id, array("500", "Internal Server Error", "employeeProfileExternalId not sent in header", "006"));
		}
		$idInitial = isset($headers['employeeProfileExternalId']) ? $headers['employeeProfileExternalId'] : $headers['EMPLOYEEPROFILEEXTERNALID'];
		$inscrit = (new Atexo_Queries())->getInscritByIdInitial($idInitial);

    	if(!$inscrit) {
    		$messageErreur="employee '".$id."' not found";
			return self::erreur($id, array("404", "Not Found", $messageErreur, "003"));
    	}

        /**
         * Transformation de la suppression physique en suppression logique.
         */
        $sqlQuery = "UPDATE `Inscrit` 
                SET `BLOQUE`= '1',
                `date_modification` = '".(new DateTime())->format('Y-m-d h:i:s')."',
                `login` = 'inscrit_".$inscrit['id']."',
                `email` = 'inscrit_".$inscrit['id']
            ."-".$inscrit['entreprise_id']
            ."-".$inscrit['id'] + (rand() * 1000)
            ."-@atexo-anonymise.com',
                `tentatives_mdp` = '3',
                
                `nom` = 'nom_inscrit_".$inscrit['id']."',
                `prenom` = 'prenom_inscrit_".$inscrit['id']."',
                `adresse` = 'adresse_inscrit_".$inscrit['id']."',
                `mdp` = 'mdp_".$inscrit['id']."'";

        $sqlQuery .= " WHERE id_externe ='".$idInitial."' AND id_externe !='';";

        $db = new Atexo_DbSocle();

	    if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sqlQuery)) {
			return self::erreur($id, array("500", "Internal Server Error", "Mysql responded : ".$db->errorMessage(), "006"));
	    }
	    header('HTTP/1.1 200 OK');
		exit;
	}
	
	
}