<?php

class EmployeeMpe extends Employee
{
	public function create()
	{
		try{
			$inscritInfo = self::generateDataCreate();
			
			if (is_array($inscritInfo)) {
				self::save($inscritInfo);
				header('HTTP/1.1 201 CREATED');
			} else {
			 	return $inscritInfo;
			}
		} catch(Exception $e) {
				$messageErreur=$e->getMessage();
				return self::erreur($inscritInfo['uidInscrit'], array("500", "Internal Server Error", $messageErreur, "006"));
			}
		exit;
	}
	
	public function update($id)
	{
		try{
			$inscritInfo = self::generateDataUpdate();
			if (is_array($inscritInfo)) {
				if ($inscritInfo[0] == "-1") {
					$inscritInfoCreate = self::generateDataCreate($inscritInfo[1]);
					if (is_array($inscritInfoCreate)) {
						self::save($inscritInfoCreate);
						header('HTTP/1.1 201 CREATED');
					} else {
					 	return $inscritInfoCreate;
					}
				}else {
					self::updateEmployee($inscritInfo);
					header('HTTP/1.1 200 OK');
				}
			} else {
			 	return $inscritInfo;
			}
		} catch(Exception $e) {
				$messageErreur=$e->getMessage();
				return self::erreur($inscritInfo['uidInscrit'], array("500", "Internal Server Error", $messageErreur, "006"));
			}
		exit;
	}
	
	public function updateEmployee($inscritInfo)
	{
		try {
			$updateQuery = "UPDATE Inscrit SET " ;
			$updateQuery .="NOM ='".(addslashes($inscritInfo['lastname']))."',";
			$updateQuery .="PRENOM ='".(addslashes($inscritInfo['firstname']))."',";
			$updateQuery .="LOGIN ='".(addslashes($inscritInfo['login']))."',";
			$updateQuery .="ADRESSE ='".(addslashes($inscritInfo['postalAddress']))."',";
			$updateQuery .="CODEPOSTAL='".(addslashes($inscritInfo['postalCode']))."',";
			$updateQuery .="VILLE='".(addslashes($inscritInfo['city']))."',";
			$updateQuery .="PAYS='".(addslashes($inscritInfo['country']))."',";
			$updateQuery .="EMAIL='".(addslashes($inscritInfo['email']))."',";
			$updateQuery .="TELEPHONE='".(addslashes($inscritInfo['phone']))."',";
			if($inscritInfo['enabled']) {
				$updateQuery .="BLOQUE='0',";
			}
			else {
				$updateQuery .="BLOQUE='1',";
			}
			$updateQuery .="FAX='".(addslashes($inscritInfo['fax']))."',";
			$updateQuery .="SIRET='".(addslashes($inscritInfo['siret']))."',";
			$updateQuery .="DATE_MODIFICATION=now(),";
			$updateQuery .="ENTREPRISE_ID='".(addslashes($inscritInfo['entrepriseId']))."',";
			$updateQuery .="ID_ETABLISSEMENT='".(addslashes($inscritInfo['id_etablissement']))."',";
			$updateQuery .="PROFIL='".(addslashes($inscritInfo['rolesInscrits']['profil']))."'";
			
			$updateQuery .= " WHERE id_externe ='".$inscritInfo['externalId']."' ;";
			 
			$db=new Atexo_DbSocle();
		     if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $updateQuery)) {
				throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
		     }
		
		} catch(Exception $e) {
			throw $e;
		}
	}
	
	public function save($inscritInfo)
	{
    	
    	try {
	    	$sql ="INSERT INTO Inscrit (id_externe,ENTREPRISE_ID,ID_ETABLISSEMENT,NOM,PRENOM,LOGIN,ADRESSE," .
	    			"CODEPOSTAL,VILLE,PAYS,EMAIL,TELEPHONE,BLOQUE,FAX,SIRET,DATE_CREATION,DATE_MODIFICATION, PROFIL) VALUES ( " ;
	    	$sql .="'".(addslashes($inscritInfo['externalId']))."',";
			$sql .="'".(addslashes($inscritInfo['entrepriseId']))."',";
			$sql .="'".(addslashes($inscritInfo['id_etablissement']))."',";
			$sql .="'".(addslashes($inscritInfo['lastname']))."',";
			$sql .="'".(addslashes($inscritInfo['firstname']))."',";
			$sql .="'".(addslashes($inscritInfo['login']))."',";
			$sql .="'".(addslashes($inscritInfo['postalAddress']))."',";
			$sql .="'".(addslashes($inscritInfo['postalCode']))."',";
			$sql .="'".(addslashes($inscritInfo['city']))."',";
			$sql .="'".(addslashes((new Atexo_Queries())->getDenominationPaysByAcronyme($inscritInfo['country'])))."',";
			$sql .="'".(addslashes($inscritInfo['email']))."',";
			$sql .="'".(addslashes($inscritInfo['phone']))."',";
	    	if($inscritInfo['enabled']) {
	    		$sql .="'0',";
	    	}
	    	else {
	    		$sql .="'1',";
	    	}
	    	$sql .="'".(addslashes($inscritInfo['fax']))."',";
	    	$sql .="'".(addslashes($inscritInfo['siret']))."',";
	    	$sql .="now(), now(),";
	    	$profile = $inscritInfo['rolesInscrits']['profil'];
	    	$sql .="'".(addslashes($profile))."');";
	    	//print_r($lastQuery);exit;
	    	//return utf8_encode(print_r($sql,true));
			$db=new Atexo_DbSocle();
		    if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql)) {
				throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
		    }
    	} catch(Exception $e) {
    		$db->rollback();
			throw $e;
    	}
	}
}