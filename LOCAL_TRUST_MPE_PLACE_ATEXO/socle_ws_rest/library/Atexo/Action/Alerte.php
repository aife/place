<?php
class Alerte extends Atexo_Action_Actions
{
	protected function processXml($xml)
	{					
		$alert = array(); 
		$domDocument = new DOMDocument;
		$domDocument->loadXML($xml);
		$tagAlert = $domDocument?->getElementsByTagName('Alert')?->item(0);
		$alert['id'] = $tagAlert?->getElementsByTagName('externalId')?->item(0)?->nodeValue;
		$subscriber_id = $tagAlert?->getElementsByTagName('subscriber_id')?->item(0)?->nodeValue;
		$inscrit = (new Atexo_Queries())->getInscritByIdInitial($subscriber_id);
		if($inscrit) {
			$alert['id_inscrit']= $inscrit['id'];
		} else {
			return self::erreur($alert['id'], array("404", "Not Found", "Inscrit ".$subscriber_id." not found", "004"));
		}
		//creation du xml du champs xmlCreteria 
		$tagcreteria = $tagAlert?->getElementsByTagName('Criteria')?->item(0);
		$creteres = array();
		$creteres['organisme'] = utf8_decode($tagcreteria?->getElementsByTagName('organisme')?->item(0)?->nodeValue);
		$creteres['entiteAchat'] = utf8_decode($tagcreteria?->getElementsByTagName('entiteAchat')?->item(0)?->nodeValue);
		$creteres['descendant'] = utf8_decode($tagcreteria?->getElementsByTagName('descendant')?->item(0)?->nodeValue);
		$creteres['typeAnnonce'] = utf8_decode($tagcreteria?->getElementsByTagName('typeAnnonce')?->item(0)?->nodeValue);
		$creteres['typeProcedure'] = utf8_decode($tagcreteria?->getElementsByTagName('typeProcedure')?->item(0)?->nodeValue);
		$creteres['categorie'] = utf8_decode($tagcreteria?->getElementsByTagName('categorie')?->item(0)?->nodeValue);
		$creteres['lieuExecution'] = utf8_decode($tagcreteria?->getElementsByTagName('lieuExecution')?->item(0)?->nodeValue);
		$creteres['cpv1'] = utf8_decode($tagcreteria?->getElementsByTagName('cpv1')?->item(0)?->nodeValue);
		$creteres['cpv2'] = utf8_decode($tagcreteria?->getElementsByTagName('cpv2')?->item(0)?->nodeValue);
		$creteres['cpv3'] = utf8_decode($tagcreteria?->getElementsByTagName('cpv3')?->item(0)?->nodeValue);
		$creteres['cpv4'] = utf8_decode($tagcreteria?->getElementsByTagName('cpv4')?->item(0)?->nodeValue);
		$creteres['libelle_cpv1'] = utf8_decode($tagcreteria?->getElementsByTagName('libelle_cpv1')?->item(0)?->nodeValue);
		$creteres['libelle_cpv2'] = utf8_decode($tagcreteria?->getElementsByTagName('libelle_cpv2')?->item(0)?->nodeValue);
		$creteres['libelle_cpv3'] = utf8_decode($tagcreteria?->getElementsByTagName('libelle_cpv3')?->item(0)?->nodeValue);
		$creteres['libelle_cpv4'] = utf8_decode($tagcreteria?->getElementsByTagName('libelle_cpv4')?->item(0)?->nodeValue);
		$creteres['motCle'] = utf8_decode($tagcreteria?->getElementsByTagName('motCle')?->item(0)?->nodeValue);
		$creteres['codelochalles'] = utf8_decode($tagcreteria?->getElementsByTagName('codelochalles')?->item(0)?->nodeValue);
		$xmlcriteria = self::XmlCreteria($creteres);

		$alert['denomination'] = utf8_decode($tagAlert?->getElementsByTagName('alert_name')?->item(0)?->nodeValue);
		$alert['periodicite']= utf8_decode($tagAlert?->getElementsByTagName('alert_period')?->item(0)?->nodeValue);
		$alert['xmlCriteria'] = utf8_decode($xmlcriteria);
		if(is_array($xmlcriteria)) {
			 return self::erreur($xmlcriteria[0], array("404", "Not Found", $xmlcriteria[1], "004", $xmlcriteria[2]));
		}
		$alert['categorie'] = self::getCategorie($creteres['categorie']);
		return $alert;
	}
	public function generateDataCreate($xml=null)
	{
		if (!$xml) {
			$xml = @file_get_contents('php://input');
    	}
		$verification = self::isValid($xml,"alert.xsd");
		if($verification!==true) {
			return $verification;
		}
		$alertInfo = self::processXml($xml);
		if(is_array($alertInfo)) {
			$verificationAlerte=self::verifierExistanceAlerte($alertInfo['id']); 
			if($verificationAlerte!==false) {
				return $verificationAlerte;
			}
		}
    	return $alertInfo;
	}
	public function generateDataUpdate($id)
	{
			$xml = @file_get_contents('php://input');
		
			$verification = self::isValid($xml,"alert.xsd");
			if($verification!==true) {
				return $verification;
			}
			$alertInfo = self::processXml($xml);
			$alert = (new Atexo_Queries())->getAlertByIdInitial($id);
	    	if(!$alert) {
	    		return ["-1", $xml, $alertInfo['id']];
			}
	    	return $alertInfo;
	}
	public function delete($id)
	{
		$alert=(new Atexo_Queries())->getAlertByIdInitial($id);
		if(!$alert) {
			return self::erreur($id, array("404", "Not Found", "Alerte ".$id." not found", "003"));
		}
		(new Atexo_Queries())->deleteAlerte($id);
		header('HTTP/1.1 200 OK');
		exit;
	}

	public function XmlCreteria($creteres)
	{
	$file = Atexo_Controller_Front::getRootPath() . Atexo_ConfigSocle::getParameter('XML_ALERTE_PATH');
		if (file_exists($file)) {
			$xml = simplexml_load_file($file);
			if($creteres['organisme'] != 'NULL') {
				if ($org = (new Atexo_Queries())->getOrganismeByAcronyme($creteres['organisme'])) {
					$xml->organisme = $creteres['organisme'];
					//service
					if($creteres['entiteAchat'] != 'NULL') {
						$databaseName = Atexo_ConfigSocle::getParameter('COMMON_DB');
						if($service = (new Atexo_Queries())->getServiceByIdInitial($databaseName,$creteres['entiteAchat'],'id_externe',$creteres['organisme'])) {
							$xml->entiteAchat = $service["id"];
						} else {
							$messageErreur = "Entite d'achat '".$creteres['entiteAchat']."' not found";
							return array($creteres['entiteAchat'],$messageErreur,"");
						}
					} else {
						$xml->entiteAchat = $creteres['entiteAchat'];
					}
				} else {
					$messageErreur="Organisme ".$creteres['organisme']."' not found";
					return array($creteres['organisme'],$messageErreur,"ORGANISM");
				}
			} else {
				$xml->organisme = $creteres['organisme'];
			}
			if($creteres['descendant'] == 'NULL') {
				$xml->descendant = 'non';
			} else {
				$xml->descendant = $creteres['descendant'];
			}
			$xml->typeAnnonce = $creteres['typeAnnonce'];
			$xml->typeProcedure = "NULL";
			$xml->categorie = self::getCategorie($creteres['categorie']);
			if($creteres['lieuExecution'] != 'NULL') {
				$xml->lieuExecution = $creteres['lieuExecution'];
			} else {
				$id_region = Atexo_ConfigSocle :: getParameter('LIEU_EXECUSION_REGION_DEFAULT');
				$lieuExecusion = (new Atexo_Queries())->getLieuExecusionByIdRegion($id_region);
				if($lieuExecusion) {
					foreach ( $lieuExecusion as $lieu) {
						$lieux .= ",".$lieu;
					}
					$lieux .= ",";
					$xml->lieuExecution = $lieux;
				} else {
					$xml->lieuExecution = 'NULL';
				}
			}
			//metre le contenu des code cpv dans cpv 2
			$xml->cpv2 = '';
			if($creteres['cpv1'] != 'NULL') {
				$xml->cpv2 .= "#".$creteres['cpv1'];
			} else {
				$xml->cpv1 = 'NULL';
			}
			if($creteres['cpv3'] != 'NULL') {
				$xml->cpv2 .= "#".$creteres['cpv3'];
			} else {
				$xml->cpv3 = 'NULL';
			}
			if($creteres['cpv4'] != 'NULL') {
				$xml->cpv2 .= "#".$creteres['cpv4'];
			} else {
				$xml->cpv4 = 'NULL';
			}
			if($creteres['cpv2'] != 'NULL') {
				$xml->cpv2 .= "#".$creteres['cpv2'];
			} elseif($xml->cpv2 == '') {
				$xml->cpv2 = 'NULL';
			}
			$xml->libelle_cpv1 = $creteres['libelle_cpv1'];
			$xml->libelle_cpv2 = $creteres['libelle_cpv2'];
			$xml->libelle_cpv3 = $creteres['libelle_cpv3'];
			$xml->libelle_cpv4 = $creteres['libelle_cpv4'];
			$xml->motCle = $creteres['motCle'];
			$xml->codelochalles = $creteres['codelochalles'];
			$xml->domainesActivite='NULL';
			$xml->qualifications='NULL';
			$xml->agrements='NULL';
				if ($xml->asXML()) {
					return $xml->asXML();
				}
		}
	}
	public function verifierExistanceAlerte($idAlerte) 
	{
		if($idAlerte!==null) {
			$alerte=(new Atexo_Queries())->getAlertByIdInitial($idAlerte);
			if($alerte) {
				$messageErreur="Alerte already exists.";
				return self::erreur($idAlerte, array("400", "Bad Request", $messageErreur, "005"));
			}
		}
		return false;
	}
	public function getCategorie($categorie){
		switch ($categorie){
			case 'Travaux':
				return $categ = 1;
				 break;
			case  'Fournitures':
				return $categ = 2;
				 break;
			case  'Services':
		 		return $categ = 3;
				 break;
			default:
                return  $categ = 'NULL';
		}
	}
}