<?php

class Company extends Atexo_Action_Actions
{
	protected function processXml($xml)
	{
		$company = array(); 
		$domDocument = new DOMDocument;
		$domDocument->loadXML($xml);
		$tagCompany = $domDocument->getElementsByTagName('company')?->item(0);
		$company['externalId'] = $tagCompany?->getElementsByTagName('externalId')?->item(0)?->nodeValue;
		$company['uidCompany'] = $tagCompany?->getElementsByTagName('id')?->item(0)?->nodeValue;
    	$company['nom'] = $tagCompany?->getElementsByTagName('label')?->item(0)?->nodeValue;
        $company['siren'] = ('false' == ($tagCompany?->getElementsByTagName('foreignIdentifier')?->item(0)?->nodeValue))?$tagCompany?->getElementsByTagName('siren')?->item(0)?->nodeValue:"";
    	$company['city'] = $tagCompany?->getElementsByTagName('city')?->item(0)?->nodeValue;
    	$company['country'] = $tagCompany?->getElementsByTagName('country')?->item(0)?->nodeValue;
		$company['postalAddress'] = $tagCompany?->getElementsByTagName('postalAddress')?->item(0)?->nodeValue;
		$company['postalCode'] = $tagCompany?->getElementsByTagName('postalCode')?->item(0)?->nodeValue;
		$company['description'] = $tagCompany?->getElementsByTagName('description')?->item(0)?->nodeValue;
		$company['email'] = $tagCompany?->getElementsByTagName('email')?->item(0)?->nodeValue;
		$company['enabled'] = $tagCompany?->getElementsByTagName('enabled')?->item(0)?->nodeValue;
		$company['fax'] = $tagCompany?->getElementsByTagName('fax')?->item(0)?->nodeValue;
		$company['iconUrl'] = $tagCompany?->getElementsByTagName('iconUrl')?->item(0)?->nodeValue;
		$company['logoUrl'] = $tagCompany?->getElementsByTagName('logoUrl')?->item(0)?->nodeValue;
		$company['phone'] = $tagCompany?->getElementsByTagName('phone')?->item(0)?->nodeValue;
		$company['website'] = $tagCompany?->getElementsByTagName('website')?->item(0)?->nodeValue;
		$company['apeCode']  = $tagCompany?->getElementsByTagName('apeCode')?->item(0)?->nodeValue;
		$company['nic']  = $tagCompany?->getElementsByTagName('nic')?->item(0)?->nodeValue;
		$company['legalCategory'] = $tagCompany?->getElementsByTagName('legalCategory')?->item(0)?->nodeValue;
		$company['registrationCountry'] = $tagCompany?->getElementsByTagName('registrationCountry')?->item(0)?->nodeValue;
		$rcs = $tagCompany?->getElementsByTagName('RCS')?->item(0)?->nodeValue;
		if($rcs=='true'){
		  $company['rcs'] = 1;
		}else {
		  $company['rcs'] = 0;
		}
	    $rm = $tagCompany?->getElementsByTagName('RM')?->item(0)?->nodeValue;
        if($rm=='true'){
          $company['rm'] = 1;
        }else {
          $company['rm'] = 0;
        }
		$company['code_insee'] = $tagCompany?->getElementsByTagName('insee')?->item(0)?->nodeValue;
		$company['libelle_naf'] = $tagCompany?->getElementsByTagName('apeNafLabel')?->item(0)?->nodeValue;
		$company['sirenEtranger'] = ('true' == $tagCompany?->getElementsByTagName('foreignIdentifier')?->item(0)?->nodeValue)?$tagCompany?->getElementsByTagName('nationalID')?->item(0)?->nodeValue:"";
		
		$responsibles = $tagCompany?->getElementsByTagName('responsibles')?->item(0);
		$listResp=$responsibles?->getElementsByTagName('responsible') ?? [];
		$index=0;
		$arrayData = array();
        foreach($listResp as $oneResponsable)  {
        	$arrayData[$index]['externalId']  = $oneResponsable?->getElementsByTagName('externalId')?->item(0)?->nodeValue;
        	$arrayData[$index]['civility']  = $oneResponsable?->getElementsByTagName('civility')?->item(0)?->nodeValue;
    		$arrayData[$index]['firstname']  = $oneResponsable?->getElementsByTagName('firstname')?->item(0)?->nodeValue;
    		$arrayData[$index]['lastname']  = $oneResponsable?->getElementsByTagName('lastname')?->item(0)?->nodeValue;
    		$arrayData[$index]['label']  = $oneResponsable?->getElementsByTagName('label')?->item(0)?->nodeValue;
        	$index++;
        }
        $company['responsibles'] = $arrayData;
        return $company;
	}
	
	
	public function generateDataCreate($xml=null)
	{
		if(!$xml) {
			$xml = @file_get_contents('php://input');
		}
		$verification=self::isValid($xml);
		if($verification!==true) {
			return $verification;
		}
		
		$companyInfo = self::processXml($xml);
		
		$company = (new Atexo_Queries())->getCompanyByIdInitial($companyInfo['externalId']);
    	if($company) {
    		$messageErreur="Company ".$companyInfo['externalId']." already exists";
			return self::erreur($companyInfo['uidCompany'], array("400", "Bad Request", $messageErreur, "005"));
    	}
    	return $companyInfo;
	}
    		
	public function generateDataUpdate()
	{
		$xml = @file_get_contents('php://input');
		
		$verification=self::isValid($xml);
		if($verification!==true) {
			return $verification;
		}
		
		$companyInfo = self::processXml($xml);
		$company = (new Atexo_Queries())->getCompanyByIdInitial($companyInfo['externalId']);
		
    	if(!$company) {
    		return array("-1", $xml);
    		/*$messageErreur="Company '".$companyInfo['externalId']."' not found";
			return self::erreur(null, array("404", "Bad Request", $messageErreur, "003"));*/
    	}
    	return $companyInfo;
	}
	
	public function delete($id)
	{
        $headers = array_change_key_case(apache_request_headers(), CASE_UPPER);
		
		if(!isset($headers['companyExternalId'])&&!isset($headers['COMPANYEXTERNALID'])) {
			return self::erreur($id, array("500", "Internal Server Error", "companyExternalId not sent in header", "006"));
		}
		$idInitial = isset($headers['companyExternalId']) ? $headers['companyExternalId'] : $headers['COMPANYEXTERNALID'];
		$company = (new Atexo_Queries())->getCompanyByIdInitial($idInitial);
		
    	if(!$company) {
    		$messageErreur="Company '".$id."' not found";
			return self::erreur($id, array("404", "Not Found", $messageErreur, "003"));
    	}
		$sqlQuery = "DELETE FROM Entreprise WHERE id_externe ='".$idInitial."' AND id_externe !='';";
		$db=new Atexo_DbSocle();
	    if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sqlQuery)) {
			return self::erreur($id, array("500", "Internal Server Error", "Bad query, mysql responded : ".$db->errorMessage(), "006"));
	    }
	    header('HTTP/1.1 200 OK');
		exit;
	}
}