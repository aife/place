<?php

class OrganismActes extends Organism
{
	public function create()
	{
		try{
			$infosOrganism = self::generateDataCreate();
			if (is_array($infosOrganism)) {
				self::save($infosOrganism);
				header('HTTP/1.1 201 CREATED');
			} else {
			 	return $infosOrganism;
			}
		} catch(Exception $e) {
				$messageErreur=$e->getMessage();
				return self::erreur($infosOrganism['id'], array("500", "Internal Server Error", $messageErreur, "006"));
			}
		exit;
	}
	
	public function update($id)
	{
		try{
			$infosOrganism = self::generateDataUpdate();
			if (is_array($infosOrganism)) {
				if ($infosOrganism[0] == "-1") {
					$infosOrganismCreate = self::generateDataCreate($infosOrganism[1]);
					if (is_array($infosOrganismCreate)) {
						self::save($infosOrganismCreate);
						header('HTTP/1.1 201 CREATED');
					} else {
					 	return $infosOrganismCreate;
					}
				}else {
					self::updateOrganisme($infosOrganism);
					header('HTTP/1.1 200 OK');
				}
			} else {
			 	return $infosOrganism;
			}
		} catch(Exception $e) {
				$messageErreur=$e->getMessage();
				return self::erreur($infosOrganism['id'], array("500", "Internal Server Error", $messageErreur, "006"));
			}
		exit;
	}
	public function save($infosOrganism)
	{
		
		$db=new Atexo_DbSocle();
		$db->begin();
		
		try {
		
				$valeur="";
				$champs="";
				$idOrg=null;
				if(Atexo_ConfigSocle::getParameter("DO_NOT_CHANGE_AGENT_ID")) {
						$champs =", id";
						$valeur =", '".addslashes($infosOrganism['id'])."'";
						$idOrg =$infosOrganism['id'];				
					}
				$sql="INSERT INTO Organisme(acronyme, sigle, type_article_org, denomination_org, categorie_insee, "
					."description_org, adresse, cp, ville, canton,active, email, url,date_creation ".$champs.") "
					."VALUES('".addslashes($infosOrganism['acronyme'])."', '".addslashes($infosOrganism['sigle'])."', '"
					.addslashes($infosOrganism['type_article_org'])."', '".addslashes($infosOrganism['denomination_org'])."', '"
					.addslashes($infosOrganism['categorie_insee'])."', '".addslashes($infosOrganism['description_org'])."', '"
					.addslashes($infosOrganism['adresse'])."', '".addslashes($infosOrganism['cp'])."', '".addslashes($infosOrganism['ville'])."', '"
					.addslashes($infosOrganism['canton'])."', '".addslashes($infosOrganism['active'])."', '".addslashes($infosOrganism['email'])."', '"
					.addslashes($infosOrganism['url'])."', now()".$valeur.") ";
				
		
				if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql)) {
					throw new Exception("Bad query : mysql respond : ".$db->errorMessage());
				}
				if(!$idOrg){
					$sql="select last_insert_id() as id";
					$ressource=$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql);
					$resultat=$db->fetchArray($ressource);
					$idOrg=$resultat['id'];
				}
				
				
				$db->commit();
		} catch(Exception $e) {
			$db->rollback();
			throw $e;
		}
		
	}
	
	public function updateOrganisme($infosOrganism)
	{
		try{
	    	$updateQuery = "UPDATE `Organisme` SET `denomination_org` = '".addslashes($infosOrganism['denomination_org'])."', ";
			$updateQuery .= " `categorie_insee` = '".addslashes($infosOrganism['categorie_insee'])."', ";
			$updateQuery .= "`description_org` = '".addslashes($infosOrganism['description_org'])."', ";
			$updateQuery .= "`adresse` = '".addslashes($infosOrganism['adresse'])."', ";
			$updateQuery .= "`cp` = '".addslashes($infosOrganism['cp'])."', `ville` = '".addslashes($infosOrganism['ville'])."', ";
			$updateQuery .= "`canton` = '".addslashes($infosOrganism['canton'])."', `email` = '".addslashes($infosOrganism['email'])."', ";
			$updateQuery .= "`url` = '".addslashes($infosOrganism['url'])."', ";
			$updateQuery .= "`active` = '".addslashes($infosOrganism['active'])."', ";
			$updateQuery .= "`sigle` = '".addslashes($infosOrganism['sigle'])."', ";
			$updateQuery .= "`type_article_org` = '".addslashes($infosOrganism['type_article_org'])."' ";
	    	$updateQuery .= " WHERE ACRONYME ='".(addslashes($infosOrganism['acronyme']))."' ;";
			
			$db=new Atexo_DbSocle();
		    if(!$db->execute(Atexo_ConfigSocle::getParameter('DB_PREFIX')."_".Atexo_ConfigSocle::getParameter('COMMON_DB'), $updateQuery))  {
				throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
		    }
			
		}catch(Exception $e) {
			throw $e;
		}
	}
	
	
	public function delete()
	{
		return "ok pour DELETE";
	}
	
	
	
	public function wgetLogo($iconUrl,$acronyme){
		if($iconUrl) {
        	$name = strstr($iconUrl,"logo");
        	$folderThemes =  Atexo_ConfigSocle::getParameter('BASE_ROOT_DIR');
	        $folderOrg = $folderThemes.$acronyme;
	        $folderOrgImage = $folderOrg  . Atexo_ConfigSocle::getParameter('PATH_ORGANISME_IMAGE');
	        if(!is_file($folderOrgImage.$name)) {
		        if (!is_dir($folderThemes)) {
		            system("mkdir ".$folderThemes, $res);
		            system("chmod 755 ".$folderThemes, $res);
		        }
		        if (!is_dir($folderOrg)) {
		            system("mkdir ".$folderOrg, $res);
		            system("chmod 777 ".$folderOrg, $res);
		        }
		        if (!is_dir($folderOrgImage)) {
		            system("mkdir ".$folderOrgImage, $res);
		            system("chmod 777 ".$folderOrgImage, $res);
		        } 
		        $commande =  "wget --no-check-certificate   " .$iconUrl ;
                $proxy = Atexo_ConfigSocle::getParameter("PROXY_WGET");
                if ($proxy != "") {
                       system("export http_proxy=\"".$proxy."\";export https_proxy=\"".$proxy."\";cd " . $folderOrgImage . ";" .$commande,$res);
                } else {
                       system("cd " . $folderOrgImage . ";" .$commande,$res);
                }
	        }
        }
	}
}