<?php


class Atexo_Action_Actions
{
	/*private $_idResssource = null;
	
	public function getIdRessource()
	{
		return $this->_idResssource;
	}
	public function setIdRessource($idResssource)
	{
		$this->_idResssource = $idResssource;
	}*/
	public function create(){}
	
	public function update($id){}
	
	public function delete($id){}
	
	public function erreur($id=null, $erreurs=array())
	{
		$domDocument = new DOMDocument('1.0', "UTF-8");
		$domDocument->xmlStandalone = true;
		
	 	$error=$domDocument->createElement('error', '');
	 	$domDocument->appendChild($error);
	 	
	 	$attribute = $domDocument->createAttribute("xmlns:xsi");
		$error->appendChild($attribute);
		$attributeValue = $domDocument->createTextNode("http://www.w3.org/2001/XMLSchema-instance");
		$attribute->appendChild($attributeValue);
		
		$attribute = $domDocument->createAttribute("xsi:noNamespaceSchemaLocation");
		$error->appendChild($attribute);
		$attributeValue = $domDocument->createTextNode("error.xsd");
		$attribute->appendChild($attributeValue);
		
		if (isset($erreurs[4])) {
			$classType = $domDocument->createElement('classType',utf8_encode($erreurs[4])); 
			$error->appendChild($classType);
		}
		$errorCode=$domDocument->createElement('errorCode', utf8_encode($erreurs[3]));
	 	$error->appendChild($errorCode);
	 	
	 	$errorLabel=$domDocument->createElement('errorLabel', utf8_encode($erreurs[1]));
	 	$error->appendChild($errorLabel);
	 	
	 	$errorMessage=$domDocument->createElement('errorMessage', utf8_encode($erreurs[2]));
	 	$error->appendChild($errorMessage);
	 	
	 	$errorMessage=$domDocument->createElement('methodType', utf8_encode(strtoupper($_SERVER['REQUEST_METHOD'])));
	 	$error->appendChild($errorMessage);
	 	
	 	
	 	$ressourceId=$domDocument->createElement('resourceId', $id);
	 	$error->appendChild($ressourceId);
		
	 	
	 	header('HTTP/1.1 '.$erreurs[0].' '.$erreurs[1]);
	 	
	 	if ($erreurs[0] != '405') {
	 		$domDocument->save(Atexo_ConfigSocle::getParameter('LOG_DIR')."/erreur_".date("Ymd_H-i-s").".xml");
	 	}
	 	
		return $domDocument;
	}
	
	public function unsupportedMethod()
	{
		return $this->erreur(null, $erreurs=array("405", "Method Not Allowed", "Method Not Allowed, use POST|PUT|DELETE"));
	}
	
	protected function processXml($xml){}
	
	public function isValid($xml,$schema=null)
	{
		if((new Atexo_UtilSocle())->isEmpty($xml)) {
			$message="Bad Request : input was empty, no xml found";
   	     	return self::erreur(null, array("400", "Bad Request", $message, "001"));
		} else {
            $domDocument = new DOMDocument;
            $domDocument->loadXML($xml);

            $domDocument->save(Atexo_ConfigSocle::getParameter('COMMON_TMP')."/fichier_xml_socle_".date("Ymd_H-i-s").".xml");
			if(!$schema) {
				$schema = "schema.xsd";
			}
			libxml_use_internal_errors(true);
			if(!$domDocument->schemaValidate(Atexo_Controller_Front::getRootPath()."/ressources/".$schema))  {
				$message="Bad Request : xml didn't match xsd".PHP_EOL;
				$errors = libxml_get_errors();
				foreach ($errors as $error) {
					$message .= $this->libxml_display_error($error).PHP_EOL;
				}
	   	     	return self::erreur(null, array("400", "Bad Request", $message, "001"));
			}
		}
		
		return true;
	}

	public function libxml_display_error($error)
	{
		switch ($error->level) {
			case LIBXML_ERR_WARNING:
				$return .= "Warning $error->code : ";
				break;
			case LIBXML_ERR_ERROR:
				$return .= "Error $error->code : ";
				break;
			case LIBXML_ERR_FATAL:
				$return .= "Fatal Error $error->code : ";
				break;
		}
		$return .= trim($error->message);
		if ($error->file) {
			$return .=    " in $error->file";
		}
		$return .= " on line $error->line".PHP_EOL;

		return $return;
	}


	public function verifierExistanceOrganisme($acronyme, $idOrganisme=null)
	{
		$acronymeOrganisme=$acronyme;
		$organisme=(new Atexo_Queries())->getOrganismeByAcronyme($acronymeOrganisme);
		if(!$organisme) {
			$messageErreur="Organism '".$acronyme."'/$idOrganisme not found";
			return self::erreur($idOrganisme, array("404", "Not Found", $messageErreur, "004", "ORGANISM"));
		} 
		
		return true;
	}
	public function verifierNonExistanceOrganisme($acronyme, $idOrganisme=null)
	{
		$acronymeOrganisme=$acronyme;
		$organisme=(new Atexo_Queries())->getOrganismeByAcronyme($acronymeOrganisme);
		if($organisme) {
			$messageErreur="Organism '".$acronyme."'/$idOrganisme existe";
			return self::erreur($idOrganisme, array("400", "Bad Request", $messageErreur, "005"));
		} 
		
		return true;
	}
	
	public function verifierExistanceService($acronymeOrganisme, $idServiceExterne,$champsVerificationService=null, $uidService=null, $organisme=null)
	{
		$service=(new Atexo_Queries())->getServiceByIdInitial($acronymeOrganisme, $idServiceExterne,$champsVerificationService, $organisme);
		if(!$service) {
			$messageErreur="Departement not found";
			return self::erreur($uidService, array("404", "Not Found", $messageErreur, "004", "ORGANISM_DEPARTMENT"));
		}
		
		return $service;
	}
}
