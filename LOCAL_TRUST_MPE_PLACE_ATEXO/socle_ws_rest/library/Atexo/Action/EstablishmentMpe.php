<?php

class EstablishmentMpe extends Establishment
{
	public function create()
	{
		try{
			$establishmentInfo = self::generateDataCreate();
			if (is_array($establishmentInfo)) {
				self::save($establishmentInfo);
				header('HTTP/1.1 201 CREATED');
			} else {
			 	return $establishmentInfo;
			}
		} catch(Exception $e) {
				$messageErreur=$e->getMessage();
				return self::erreur($establishmentInfo['uidEstablishment'], array("500", "Internal Server Error", $messageErreur, "006"));
			}
		exit;
	}
	
	public function update($id)
	{
		try{
			$establishmentInfo = self::generateDataUpdate();
			if (is_array($establishmentInfo)) {
				if ($establishmentInfo[0] == "-1") {
					$establishmentInfoCreate = self::generateDataCreate($establishmentInfo[1]);
					if (is_array($establishmentInfoCreate)) {
						self::save($establishmentInfoCreate);
						header('HTTP/1.1 201 CREATED');
					} else {
					 	return $establishmentInfoCreate;
					}
				}else {
					self::updateEstablishment($establishmentInfo);
					header('HTTP/1.1 200 OK');
				}
			} else {
			 	return $establishmentInfo;
			}
		} catch(Exception $e) {
				$messageErreur=$e->getMessage();
				return self::erreur($establishmentInfo['uidEstablishment'], array("500", "Internal Server Error", $messageErreur,"006"));
			}
		exit;
	}
	
	public function updateEstablishment($establishmentInfo)
	{
		$db=new Atexo_DbSocle();
        $db->begin();
		try {
			$updateQuery = "UPDATE t_etablissement SET " ;
			$updateQuery .= " `id_entreprise` = '".(addslashes($establishmentInfo['entrepriseId']))."', " ;
			$updateQuery .= " `code_etablissement` = '".(addslashes($establishmentInfo['code_etablissement']))."', " ;
			$updateQuery .= " `ville` = '".(addslashes($establishmentInfo['city']))."', " ;
			$updateQuery .= " `pays` = '".(addslashes((new Atexo_Queries())->getDenominationPaysByAcronyme($establishmentInfo['country'])))."', " ;
			$updateQuery .= " `adresse` = '".(addslashes($establishmentInfo['postalAddress']))."', " ;
			$updateQuery .= " `est_siege` = '".(addslashes($establishmentInfo['est_siege']))."', " ;
			$updateQuery .= " `saisie_manuelle` = '0', " ;
			$updateQuery .= " `statut_actif` = '".(addslashes($establishmentInfo['statut_actif']))."', " ;
			$updateQuery .= " `code_postal` = '".(addslashes($establishmentInfo['postalCode']))."', " ;
			$updateQuery .= " `date_modification` = now()";
		    $updateQuery .= " WHERE id_externe ='".(addslashes($establishmentInfo['externalId']))."' ;";

		    if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $updateQuery))  {
				throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
		    }

	        $db->commit();
	    	//echo $updateQuery;exit;
		
		} catch(Exception $e) {
			$db->rollback();
			throw $e;
		}
	}
	
	public function save($establishmentInfo)
	{
		$db=new Atexo_DbSocle();
    	$db->begin();
    	
    	try {
    	
		        $sql = "INSERT INTO t_etablissement (id_entreprise,code_etablissement,adresse,code_postal,ville,pays,id_externe,est_siege, saisie_manuelle, statut_actif,date_creation,date_modification) VALUES (";
				$sql .="'".addslashes($establishmentInfo['entrepriseId'])."',";
				$sql .="'".addslashes($establishmentInfo['code_etablissement'])."',";
				$sql .="'".(addslashes($establishmentInfo['postalAddress']))."',";
				$sql .="'".(addslashes($establishmentInfo['postalCode']))."',";
				$sql .="'".(addslashes($establishmentInfo['city']))."',";
				$sql .="'".(addslashes((new Atexo_Queries())->getDenominationPaysByAcronyme($establishmentInfo['country'])))."',";
				$sql .="'".(addslashes($establishmentInfo['externalId']))."',";
				$sql .="'".(addslashes($establishmentInfo['est_siege']))."',";
				$sql .="'0',";
				$sql .="'".(addslashes($establishmentInfo['statut_actif']))."',";
			    $sql .="now(),";
			    $sql .="now());";

			    if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql)) {
			    	throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
			    }
		       
		       $db->commit();
		       
    	} catch(Exception $e) {
    		$db->rollback();
			throw $e;
    	}
	}
}