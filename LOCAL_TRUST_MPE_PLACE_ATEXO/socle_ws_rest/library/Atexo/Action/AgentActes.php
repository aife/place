<?php


class AgentActes extends Agent
{
	
	public function create()
	{
		try{
			$infosAgent = self::generateDataCreate(true,'id_agent_initial','id_service_initial');
			if (is_array($infosAgent)) {
				self::save($infosAgent);
				header('HTTP/1.1 201 CREATED');
			} else {
			 	return $infosAgent;
			}
		} catch(Exception $e) {
				$messageErreur=$e->getMessage();
				return self::erreur($infosAgent['uidAgent'], array("500", "Internal Server Error", $messageErreur,"006"));
			}
		exit;
	}
	
	public function update($id)
	{
		try{
			$infosAgent = self::generateDataUpdate(true,'id_agent_initial','id_service_initial'); 
			if (is_array($infosAgent)) {	
				if ($infosAgent[0] == "-1") {
					$infosAgentCreate = self::generateDataCreate(true, "id_agent_initial", "id_service_initial", $infosAgent[1]);
					if (is_array($infosAgentCreate)) {
						self::save($infosAgentCreate);
						header('HTTP/1.1 201 CREATED');
					} else {
					 	return $infosAgentCreate;
					}
				}else {
					self::updateAgent($infosAgent);
					header('HTTP/1.1 200 OK');
				}
			} else {
			 	return $infosAgent;
			}
		} catch(Exception $e) {
				$messageErreur=$e->getMessage();
				return self::erreur($infosAgent['uidAgent'], array("500", "Internal Server Error", $messageErreur, "006"));
			}
		exit;
	}
	
	public function updateAgent($infosAgent)
	{
		try {
				$tentativesMdp=0;
				if(!$infosAgent['enabled']){
					$tentativesMdp=3;
				}	
				$sql="UPDATE `Agent` SET  `login` = '".addslashes($infosAgent['login'])."',`email` = '".addslashes($infosAgent['email'])."'," 
				."`nom` = '".addslashes($infosAgent['nom'])."', `prenom` = '".addslashes($infosAgent['prenom'])
				."', `elu` = '".addslashes($infosAgent['elu'])."', `num_tel` = '".addslashes($infosAgent['tel'])
				."',`num_fax` = '".addslashes($infosAgent['fax'])."', `adr_postale` = '".addslashes($infosAgent['adresse']['adressePostale'])
				."',`civilite` = '".addslashes($infosAgent['civilite'])."', `date_modification` = now(), "
				." `nom_fonction`='".addslashes($infosAgent['fonction'])."', `service_id` ='".addslashes($infosAgent['idService'])."', "
				."`id_service_initial` ='".addslashes($infosAgent['idInitialService'])."', tentatives_mdp='".addslashes($tentativesMdp)."' "
				." WHERE  `Agent`.`id_agent_initial` = '".addslashes($infosAgent['id'])."'"; 
			
			$db=new Atexo_DbSocle();
			if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql)) {
				throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
			}
		} catch(Exception $e) {
			throw $e;
		}
	}
	
	public function delete($id)
	{
		$headers=apache_request_headers();
		
		if(!isset($headers['agentProfileExternalId'])) {
			$message="Agent id not sent in header";
			return self::erreur($id, array("500", "Internal Server Error", $message, "006"));
		}
		
		$idInitial=$headers['agentProfileExternalId'];
		
		$agent=(new Atexo_Queries())->getAgentByIdInitial($idInitial,'id_agent_initial');
		
		if(!$agent) {
			return self::erreur($id, array("404", "Not Found", "Agent ".$id." not found", "003"));
		}
		$prefixeSuprime=Atexo_ConfigSocle::getParameter('PREFIXE_SUPPRIME');
		$email=$prefixeSuprime.$agent['id']."_".$agent['email'];
		$login=$prefixeSuprime.$agent['id']."_".$agent['login'];
		$sql="UPDATE `Agent` set deleted_at = now(), service_id = NULL, tentatives_mdp='3', email='".addslashes($email)."', login='".addslashes($login)."', date_modification = now() where `id_agent_initial`='".addslashes($idInitial)."'";
		$db=new Atexo_DbSocle();
		if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql)) {
			return self::erreur($id, array("500", "Internal Server Error", "Bad query, mysql responded : ". $db->errorMessage(), "006"));
		}
		header('HTTP/1.1 200 OK');
		exit;
	}
	
	public function insertHabilitation($idAgent, $profil, $db=null)
	{
		$profil['id_agent']=$idAgent;		
		$sqlHabilitation="INSERT INTO ACTE_AGENT_HABILITATION SET ";

		unset($profil['libelle']);
		unset($profil['id']);

		foreach($profil as $key=>$value) {
			if($key!='denomination_profil')
				$sqlHabilitation.="$key='".addslashes($value)."',";
		}

		$sqlHabilitation=trim($sqlHabilitation, ",");
		
		if($db===null) {
			$db=new Atexo_DbSocle();
		}
		if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sqlHabilitation)) {
			throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
		}
	}
	
	public function save($infosAgent)
	{
		$db=new Atexo_DbSocle();
		$db->begin();
		
		try {
		
				$valeur="";
				$champs="";
				$idAgent=null;
				if(Atexo_ConfigSocle::getParameter("DO_NOT_CHANGE_AGENT_ID")) {
						$champs =", id";
						$valeur =", '".addslashes($infosAgent['id'])."'";
						$idAgent =$infosAgent['id'];				
					}
				$tentativesMdp=0;
				if(!$infosAgent['enabled']){
					$tentativesMdp=3;
				}		 
				$sqlAgent="INSERT INTO Agent(login, email, nom, prenom, organisme, service_id, num_tel, num_fax, civilite, adr_postale, date_creation, date_modification, id_agent_initial, elu, nom_fonction, id_service_initial, tentatives_mdp".$champs.") "
					."VALUES('".addslashes($infosAgent['login'])."', '".addslashes($infosAgent['email'])."', '"
					.addslashes($infosAgent['nom'])."', '".addslashes($infosAgent['prenom'])."', '"
					.addslashes($infosAgent['acronyme'])."', '".addslashes($infosAgent['idService'])."', '"
					.addslashes($infosAgent['tel'])."', '".addslashes($infosAgent['fax'])."', '"
					.addslashes($infosAgent['civilite'])."', '".addslashes($infosAgent['adresse']['adressePostale'])
					."', now(), now(),'".addslashes($infosAgent['id'])."', '".addslashes($infosAgent['elu'])."', '"
					.addslashes($infosAgent['fonction'])."', '".addslashes($infosAgent['idInitialService'])."','".addslashes($tentativesMdp)."'".$valeur.") ";
				//return print_r(utf8_encode($sqlAgent));
		
				if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sqlAgent)) {
					throw new Exception("Bad query : mysql respond : ".$db->errorMessage());
				}
				if(!$idAgent){
					$sql="select last_insert_id() as id";
					$ressource=$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql);
					$resultat=$db->fetchArray($ressource);
					$idAgent=$resultat['id'];
				}
				
				$profil=(new Atexo_Queries())->getProfilActesById($infosAgent['rolesAgent']['profil']);
				if(!$profil) {
					$messageErreur="Profile ".$infosAgent['rolesAgent']['profil']." not found";
					throw new Exception($messageErreur);
				}
				$profil['id_profil_initial']=$infosAgent['rolesAgent']['profil'];
				self::insertHabilitation($idAgent, $profil, $db);
				
				$db->commit();
		} catch(Exception $e) {
			$db->rollback();
			throw $e;
		}
		
		 
	}
}