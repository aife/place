<?php

class CompanyJeb extends Company
{
	public function create()
	{
		try{
			$companyInfo = self::generateDataCreate();
			if (is_array($companyInfo)) {
				self::save($companyInfo);
				header('HTTP/1.1 201 CREATED');
			} else {
				return $companyInfo;
			}
		} catch(Exception $e) {
			$messageErreur=$e->getMessage();
			return self::erreur($companyInfo['uidCompany'], array("500", "Internal Server Error", $messageErreur, "006"));
		}
		exit;
	}

	public function update($id)
	{
		try{
			$companyInfo = self::generateDataUpdate();
			if (is_array($companyInfo)) {
				if ($companyInfo[0] == "-1") {
					$companyInfoCreate = self::generateDataCreate($companyInfo[1]);
					if (is_array($companyInfoCreate)) {
						self::save($companyInfoCreate);
						header('HTTP/1.1 201 CREATED');
					} else {
					 	return $companyInfoCreate;
					}
				}else {
					self::updateCompany($companyInfo);
					header('HTTP/1.1 200 OK');
				}
			} else {
				return $companyInfo;
			}
		} catch(Exception $e) {
			$messageErreur=$e->getMessage();
			return self::erreur($companyInfo['uidCompany'], array("500", "Internal Server Error", $messageErreur,"006"));
		}
		exit;
	}

	public function updateCompany($companyInfo)
	{
		$db=new Atexo_DbSocle();
		$db->begin();
		try {
			$updateQuery = "UPDATE Entreprise SET " ;
			$updateQuery .= " `siren` = '".(addslashes($companyInfo['siren']))."', " ;
			$updateQuery .= " `nom` = '".(addslashes($companyInfo['nom']))."', " ;
			$updateQuery .= " `adresse` = '".(addslashes($companyInfo['postalAddress']))."', " ;
            $updateQuery .= " `codepostal` = '".(addslashes($companyInfo['postalCode']))."', " ;
			$updateQuery .= " `villeadresse` = '".(addslashes($companyInfo['city']))."', " ;
			$updateQuery .= " `paysadresse` = '".(addslashes($companyInfo['country']))."', " ;
			$updateQuery .= " `email` = '".(addslashes($companyInfo['email']))."', " ;
			$updateQuery .= " `site_web` = '".(addslashes($companyInfo['website']))."', " ;
			$updateQuery .= " `descriptif_activite` = '".(addslashes($companyInfo['description']))."', " ;
			$updateQuery .= " `formejuridique` = '".(addslashes($companyInfo['legalCategory']))."', " ;
			$updateQuery .= " `paysenregistrement` = '".(addslashes($companyInfo['registrationCountry']))."', " ;
			$updateQuery .= " `sirenEtranger` = '".(addslashes($companyInfo['sirenEtranger']))."', " ;
			$updateQuery .= " `code_naf` = '".(addslashes($companyInfo['apeCode']))."', " ;
			$updateQuery .= " `code_siege` = '".(addslashes($companyInfo['nic']))."'," ;
			$updateQuery .= " `rm` = '".(addslashes($companyInfo['rm']))."', " ;
			$updateQuery .= " `rcs` = '".(addslashes($companyInfo['rcs']))."', " ;
			$updateQuery .= " `libelle_naf` = '".(addslashes($companyInfo['libelle_naf']))."', " ;
			$updateQuery .= " `code_insee` = '".(addslashes($companyInfo['code_insee']))."', " ;
			$updateQuery .= " `date_modification` = now()";
			
			$updateQuery .= " WHERE id_initial ='".(addslashes($companyInfo['externalId']))."' ;";
			//echo $updateQuery;exit;
			
			$company = (new Atexo_Queries())->getCompanyByIdInitial($companyInfo['externalId']);

		    if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $updateQuery))  {
				throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
		    }
		    
	        $responsiblesQuery="";
	        
	        
	        $sqlDeleteResponsibles = "DELETE FROM  responsableengagement WHERE entreprise_id = '".$company['id']."';";
			if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sqlDeleteResponsibles)) {
	    		throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
	    	}
	        
	        foreach($companyInfo['responsibles'] as $oneResponsable)  {
	        	$responsiblesQuery = "INSERT INTO responsableengagement (id, entreprise_id, nom, prenom, qualite) VALUES (";
	        	$responsiblesQuery .= "'".(addslashes($oneResponsable['externalId']))."',";
				$responsiblesQuery .= "'".(addslashes($company['id']))."',";
				$responsiblesQuery .= "'".(addslashes($oneResponsable['firstname']))."',";
				$responsiblesQuery .= "'".(addslashes($oneResponsable['lastname']))."',";
				$responsiblesQuery .= "'".(addslashes($oneResponsable['label']))."'";
	    		$responsiblesQuery .= ");";
	    		
	    		if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $responsiblesQuery)) {
		    		throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
		    	}
	        }
	        $db->commit();

		} catch(Exception $e) {
			$db->rollback();
			throw $e;
		}
	}

	public function save($companyInfo)
	{
		$db=new Atexo_DbSocle();
		$db->begin();

		try {

			$sql = "INSERT INTO Entreprise (siren, nom, adresse, codepostal, villeadresse, paysadresse, email,".
                       " site_web, descriptif_activite, formejuridique, paysenregistrement, sirenEtranger, code_naf,".
                       " code_siege, rm, rcs, libelle_naf, code_insee, date_creation, date_modification, id_initial)".
                       " VALUES (";
			$sql .="'".addslashes($companyInfo['siren'])."',";
			$sql .="'".(addslashes($companyInfo['nom']))."',";
			$sql .="'".(addslashes($companyInfo['postalAddress']))."',";
			$sql .="'".(addslashes($companyInfo['postalCode']))."',";
			$sql .="'".(addslashes($companyInfo['city']))."',";
			$sql .="'".(addslashes($companyInfo['country']))."',";
			$sql .="'".(addslashes($companyInfo['email']))."',";
			$sql .="'".(addslashes($companyInfo['website']))."',";
			$sql .="'".(addslashes($companyInfo['description']))."',";
			$sql .="'".(addslashes($companyInfo['legalCategory']))."',";
			$sql .="'".(addslashes($companyInfo['registrationCountry']))."',";
			$sql .="'".(addslashes($companyInfo['sirenEtranger']))."',";
			$sql .="'".(addslashes($companyInfo['apeCode']))."',";
			$sql .="'".(addslashes($companyInfo['nic']))."',";
			$sql .="'".(addslashes($companyInfo['rm']))."',";
			$sql .="'".(addslashes($companyInfo['rcs']))."',";
			$sql .="'".(addslashes($companyInfo['libelle_naf']))."',";
			$sql .="'".(addslashes($companyInfo['code_insee']))."',";
			$sql .=" now(), now(),";
			$sql .="'".(addslashes($companyInfo['externalId']))."');";

			if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $sql)) {
				throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
			}

			$newCompany = (new Atexo_Queries())->getCompanyByIdInitial($companyInfo['externalId']);
			$responsiblesQuery="";
			foreach($companyInfo['responsibles'] as $oneResponsable)  {
				$responsiblesQuery = "INSERT INTO responsableengagement (id, entreprise_id, nom, prenom, qualite) VALUES (";
				$responsiblesQuery .= "'".(addslashes($oneResponsable['externalId']))."',";
				$responsiblesQuery .= "'".(addslashes($newCompany['id']))."',";
				$responsiblesQuery .= "'".(addslashes($oneResponsable['firstname']))."',";
				$responsiblesQuery .= "'".(addslashes($oneResponsable['lastname']))."',";
				$responsiblesQuery .= "'".(addslashes($oneResponsable['label']))."'";
				$responsiblesQuery .= ");";

				if(!$db->execute(Atexo_ConfigSocle::getParameter('COMMON_DB'), $responsiblesQuery)) {
					throw new Exception("Bad query, mysql responded : ".$db->errorMessage());
				}

			}
			 
			$db->commit();
			 
		} catch(Exception $e) {
			$db->rollback();
			throw $e;
		}
	}
}