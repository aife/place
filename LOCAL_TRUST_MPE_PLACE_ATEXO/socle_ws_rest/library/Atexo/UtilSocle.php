<?php
class Atexo_UtilSocle
{
	public function getRequest($methode, $parameters)
	{
        $paramKeys = array_keys($parameters);
        
        //Determiner l'action selon la méthode : POST=>création, PUT=>update, DELETE=>delete
        $action=self::getAction($methode);
        
        //Detection des erreurs : action est modifié si erreur trouvé (modifié en "erreur", qui est le nom de la fonction à appeler en cas d'erreur)
       	$erreurs=self::errorsVerification($action, $parameters, $erreur);
        
        $request = array('method'=>$action);
       
        $filterParams = new Zend_Filter_Input($parameters);	
        foreach($paramKeys as $key)
        {
            switch($key) {
                case 'id':
                    $request[$key] = $filterParams->testAlnum($key);
                    break;
                default:
                    $request[$key] = $filterParams->testDigits($key);
            }
        }

        if($erreur) {        	
        	header('HTTP/1.1 400 - Bad Request ');
        	if(!isset($request['id'])) {
        		$request['id']="";
        	} 
        	$request["erreurs"]=$erreurs;        		
        }  
        
        return $request;
	}
	
	public function isEmpty($string)
	{
		return ($string=="");
	}
	
	public function getAction($methode)
    {
    	 switch(strtoupper($methode)) {
        	
        	case "POST" : return "create";
        				  break;
        				  
        	case "PUT" : return "update";
        				 break;
        				 
        	case "DELETE" : return "delete";        	
        					break;
        	default : return "unsupportedMethod";
        }
        
    }
    
	public function errorsVerification(& $action, $parameters, & $erreur)
    {
    	$code="";
    	$libelle=""; 
    	$message="";
    	 
 	 	 if(($action=="update" || $action=="delete") && !isset($parameters['id'])) {
   	     	$code="400";
    		$libelle="Bad Request";
   	     	$message="Bad Request : missing ressource {id}";
   	     	$erreur=true;
   	     	
   	     } else if ($action=="create" && isset($parameters['id'])) {
   	     	 $code="400";
    		 $libelle="Bad Request";
   	     	 $message="Bad Request : unexpected ressource {id} found when using POST method";
   	     	 $erreur=true;
   	     	
   	     }  
   	     
   	    //Si erreur trouvé changer action en "erreur" pour envoyer un xml d'erreur
		if($erreur==true) {
   	     	$action="erreur";
   	     }
   	     
        return array($code, $libelle, $message); //array($code, $libelle, $message);
    }
    
    public function getInformationsFromUri($uri) 
    {
    	$url=explode("/", trim($uri, "/"));
        $id=$url[count($url)-1];
        if(!is_numeric($id)) {
        	$action=$url[count($url)-1];;
        	$controller=$url[count($url)-2];
        } else {
        	$action=$url[count($url)-2];;
        	$controller=$url[count($url)-3];
        }
        
        return array("controller"=>$controller, "action"=>$action,"id"=>$id);
    }
    
    public function write_file($file_name, $data, $mode ='w')
	{
	   $fp = fopen ($file_name, $mode);
	   if (!fwrite($fp, $data)) return null;
	   fclose ($fp);
	
	   return 1;
	}
    
    public function createDir($directory_name)
	{
		if ($directory_name!= null) {
			if (!is_dir($directory_name)) {
				mkdir($directory_name,0777,true);
			}
		}
	}

    public static function atexoHtmlEntities ($str)
    {
        return htmlspecialchars($str, ENT_NOQUOTES|ENT_QUOTES,
            Atexo_ConfigSocle::getParameter('HTTP_ENCODING')
        );
    }
}