<?php
/**
 * Created by PhpStorm.
 * User: ARH
 * Date: 28/09/2016
 * Time: 12:09.
 */
require_once __DIR__.'/../../autoload.php';

class IsValidTest extends PHPUnit_Framework_TestCase
{
    /**
     * Test objet Chiffrement.
     */
    public function testChiffrement()
    {
        $chiffrement = new AtexoCrypto\Dto\Chiffrement();
        $this->assertEquals(\AtexoCrypto\Tool\ValidEnum::ERROR_CONSULTATION, $chiffrement->isValid());
    }

    /**
     * Test objet Dechiffrement.
     */
    public function testDechiffrement()
    {
        $dechiffrement = new AtexoCrypto\Dto\Dechiffrement();
        $this->assertEquals(\AtexoCrypto\Tool\ValidEnum::ERROR_OFFRE, $dechiffrement->isValid());
    }

    /**
     * Test objet Certificat.
     */
    public function testCertificat()
    {
        $certificat = new AtexoCrypto\Dto\Certificat();
        $this->assertEquals(\AtexoCrypto\Tool\ValidEnum::ERROR_CERTIFICAT, $certificat->isValid());
    }

    /**
     * Test objet Consultation.
     */
    public function testConsultation()
    {
        $consultation = new AtexoCrypto\Dto\Consultation();
        $this->assertEquals(\AtexoCrypto\Tool\ValidEnum::ERROR_CONSULTATION, $consultation->isValid());
    }

    /**
     * Test objet Enveloppe.
     */
    public function testEnveloppe()
    {
        $enveloppe = new AtexoCrypto\Dto\Enveloppe();
        $this->assertEquals(\AtexoCrypto\Tool\ValidEnum::ERROR_ENVELOPPE, $enveloppe->isValid());
    }

    /**
     * Test objet Fichier.
     */
    public function testFichier()
    {
        $fichier = new AtexoCrypto\Dto\Fichier();
        $this->assertEquals(\AtexoCrypto\Tool\ValidEnum::ERROR_FICHIER, $fichier->isValid());
    }

    /**
     * Test objet Offre.
     */
    public function testOffre()
    {
        $offre = new AtexoCrypto\Dto\Offre();
        $this->assertEquals(\AtexoCrypto\Tool\ValidEnum::ERROR_ENVELOPPE, $offre->isValid());
    }

    /**
     * Test objet Signature.
     */
    public function testVerifFichier()
    {
        $signature = new AtexoCrypto\Dto\VerifFichier();
        $this->assertEquals(\AtexoCrypto\Tool\ValidEnum::ERROR_SIGNATURE, $signature->isValid());
    }

    /**
     * Test objet Signature.
     */
    public function testVerifHash()
    {
        $signature = new AtexoCrypto\Dto\VerifHash();
        $this->assertEquals(\AtexoCrypto\Tool\ValidEnum::ERROR_SIGNATURE, $signature->isValid());
    }

    /**
     * Test objet Signature.
     */
    public function testVerifPdf()
    {
        $signature = new AtexoCrypto\Dto\VerifPdf();
        $this->assertEquals(\AtexoCrypto\Tool\ValidEnum::ERROR_SIGNATURE, $signature->isValid());
    }
}
