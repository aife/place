<?php
/**
 * Created by PhpStorm.
 * User: ARH
 * Date: 28/09/2016
 * Time: 12:09.
 */
require_once __DIR__.'/../../autoload.php';

class ObjetTest extends PHPUnit_Framework_TestCase
{
    /**
     * Test objet Client.
     */
    public function testClient()
    {
        $this->assertInstanceOf(AtexoCrypto\Client\Client::class, new AtexoCrypto\Client\Client(null, null, null));
    }

    /**
     * Test objet ChiffrementService.
     */
    public function testChiffrementService()
    {
        $c = new AtexoCrypto\Client\Client(null, null, null);
        $this->assertInstanceOf(\AtexoCrypto\Service\ChiffrementService::class, $c->chiffrementService());
    }

    /**
     * Test objet DechiffrementService.
     */
    public function testDechiffrementService()
    {
        $c = new AtexoCrypto\Client\Client(null, null, null);
        $this->assertInstanceOf(\AtexoCrypto\Service\DechiffrementService::class, $c->dechiffrementService());
    }

    /**
     * Test objet SignatureService.
     */
    public function testSignatureService()
    {
        $c = new AtexoCrypto\Client\Client(null, null, null);
        $this->assertInstanceOf(\AtexoCrypto\Service\SignatureService::class, $c->signatureService());
    }

    /**
     * Test objet ValidationService.
     */
    public function testValidationService()
    {
        $c = new AtexoCrypto\Client\Client(null, null, null);
        $this->assertInstanceOf(\AtexoCrypto\Service\ValidationService::class, $c->validationService());
    }

    /**
     * Test objet RestRequestException.
     */
    public function testRestRequestException()
    {
        $this->assertInstanceOf(AtexoCrypto\Exception\RestRequestException::class, new AtexoCrypto\Exception\RestRequestException(null));
    }

    /**
     * Test objet ServiceException.
     */
    public function testServiceException()
    {
        $this->assertInstanceOf(AtexoCrypto\Exception\ServiceException::class, new AtexoCrypto\Exception\ServiceException(null, null, null));
    }

    /**
     * Test objet Chiffrement.
     */
    public function testChiffrement()
    {
        $this->assertInstanceOf(AtexoCrypto\Dto\Chiffrement::class, new AtexoCrypto\Dto\Chiffrement());
    }

    /**
     * Test objet Dechiffrement.
     */
    public function testDechiffrement()
    {
        $this->assertInstanceOf(AtexoCrypto\Dto\Dechiffrement::class, new AtexoCrypto\Dto\Dechiffrement());
    }

    /**
     * Test objet Certificat.
     */
    public function testCertificat()
    {
        $this->assertInstanceOf(AtexoCrypto\Dto\Certificat::class, new AtexoCrypto\Dto\Certificat());
    }

    /**
     * Test objet Consultation.
     */
    public function testConsultation()
    {
        $this->assertInstanceOf(AtexoCrypto\Dto\Consultation::class, new AtexoCrypto\Dto\Consultation());
    }

    /**
     * Test objet Enveloppe.
     */
    public function testEnveloppe()
    {
        $this->assertInstanceOf(AtexoCrypto\Dto\Enveloppe::class, new AtexoCrypto\Dto\Enveloppe());
    }

    /**
     * Test objet Fichier.
     */
    public function testFichier()
    {
        $this->assertInstanceOf(AtexoCrypto\Dto\Fichier::class, new AtexoCrypto\Dto\Fichier());
    }

    /**
     * Test objet InfosSignature.
     */
    public function testInfosSignature()
    {
        $this->assertInstanceOf(AtexoCrypto\Dto\InfosSignature::class, new AtexoCrypto\Dto\InfosSignature());
    }

    /**
     * Test objet Offre.
     */
    public function testOffre()
    {
        $this->assertInstanceOf(AtexoCrypto\Dto\Offre::class, new AtexoCrypto\Dto\Offre());
    }

    /**
     * Test objet Signature.
     */
    public function testSignature()
    {
        $this->assertInstanceOf(AtexoCrypto\Dto\Signature::class, new AtexoCrypto\Dto\Signature());
    }

    /**
     * Test objet SignatureFichier.
     */
    public function testSignatureFichier()
    {
        $this->assertInstanceOf(AtexoCrypto\Dto\SignatureFichier::class, new AtexoCrypto\Dto\SignatureFichier());
    }

    /**
     * Test objet JsonSerializer.
     */
    public function testJsonSerializer()
    {
        $this->assertInstanceOf(AtexoCrypto\Tool\JsonSerializer::class, new AtexoCrypto\Tool\JsonSerializer());
    }

    /**
     * Test objet RestRequest.
     */
    public function testRestRequest()
    {
        $this->assertInstanceOf(AtexoCrypto\Tool\RestRequest::class, new AtexoCrypto\Tool\RestRequest());
    }

    /**
     * Test objet TestUtils.
     */
    public function testTestUtils()
    {
        $this->assertInstanceOf(AtexoCrypto\Tool\TestUtils::class, new AtexoCrypto\Tool\TestUtils());
    }

    /**
     * Test objet Util.
     */
    public function testUtil()
    {
        $this->assertInstanceOf(AtexoCrypto\Tool\Util::class, new AtexoCrypto\Tool\Util());
    }
}
