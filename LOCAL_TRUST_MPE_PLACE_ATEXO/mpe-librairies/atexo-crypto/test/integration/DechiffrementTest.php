<?php
/**
 * Created by PhpStorm.
 * User: ARH
 * Date: 28/09/2016
 * Time: 12:09.
 */

use AtexoCrypto\Client\Client;
use AtexoCrypto\Tool\TestUtils;

class DechiffrementTest extends PHPUnit_Framework_TestCase
{
    /**
     * Test return jnlp xml.
     *
     * @throws \AtexoCrypto\Exception\ServiceException
     */
    public function testJnplDemandeDeChiffrement()
    {
        require_once __DIR__.'/../../autoload.php';

        $c = new Client(
            $GLOBALS['URL_SERVEUR_CRYPTO'],
            null,
            null
        );
        $dechiffrement = TestUtils::createDechiffrement();

        $results = $c->dechiffrementService()->demandeDechiffrement($dechiffrement);

        $expected = new DOMDocument();
        $expected->loadXML($results);

        $actual = new DOMDocument();
        $actual->load(__DIR__.'/jnlp.xml');

        $this->assertEqualXMLStructure($expected->firstChild, $actual->firstChild, true);
    }

    /**
     * Test return ServiceException.
     *
     * @expectedException     \AtexoCrypto\Exception\ServiceException
     * @expectedExceptionCode 0
     */
    public function testErreurJsonDemandeDeChiffrement()
    {
        require_once __DIR__.'/../../autoload.php';

        $c = new Client(
            $GLOBALS['URL_SERVEUR_CRYPTO'],
            null,
            null
        );
        $dechiffrement = TestUtils::createDechiffrementInvalid();
        $c->dechiffrementService()->demandeDechiffrement($dechiffrement);
    }
}
