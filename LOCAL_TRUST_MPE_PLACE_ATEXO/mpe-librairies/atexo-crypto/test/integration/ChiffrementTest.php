<?php
/**
 * Created by PhpStorm.
 * User: ARH
 * Date: 28/09/2016
 * Time: 12:09.
 */

use AtexoCrypto\Client\Client;
use AtexoCrypto\Tool\TestUtils;

class ChiffrementTest extends PHPUnit_Framework_TestCase
{
    /**
     * Test Integration return jeton horodatage.
     *
     * @throws \AtexoCrypto\Exception\ServiceException
     */
    public function testHorodatageDemandeChiffrement()
    {
        require_once __DIR__.'/../../autoload.php';

        $c = new Client(
            $GLOBALS['URL_SERVEUR_CRYPTO'],
            null,
            null
        );
        $chiffrement = TestUtils::createChiffrement();

        $results = $c->chiffrementService()->demandeChiffrement($chiffrement);

        $this->assertEquals('13', strlen($results->getHorodatage()));
    }

    /**
     * Test return ServiceException.
     *
     * @expectedException     \AtexoCrypto\Exception\ServiceException
     * @expectedExceptionCode 0
     */
    public function testErreurJsonDemandeChiffrement()
    {
        require_once __DIR__.'/../../autoload.php';

        $c = new Client(
            $GLOBALS['URL_SERVEUR_CRYPTO'],
            null,
            null
        );
        $chiffrement = TestUtils::createChiffrementInvalid();
        $c->chiffrementService()->demandeChiffrement($chiffrement);
    }
}
