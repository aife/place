<?php
/**
 * Created by PhpStorm.
 * User: ARH
 * Date: 28/09/2016
 * Time: 12:09.
 */

use AtexoCrypto\Client\Client;
use AtexoCrypto\Tool\TestUtils;

class SignatureTest extends PHPUnit_Framework_TestCase
{
    /**
     * Test return content pdf signed.
     *
     * @throws \AtexoCrypto\Exception\ServiceException
     */
    public function testSignaturePades()
    {
        require_once __DIR__.'/../../autoload.php';

        $c = new Client(
            $GLOBALS['URL_SERVEUR_CRYPTO'],
            null,
            null
        );
        $signatureFichier = TestUtils::createSignaturePades();

        $results = $c->signatureService()->signerPades($signatureFichier);

        $this->assertNotEmpty($results);
    }

    /**
     * Test return content jnlp.
     *
     * @throws \AtexoCrypto\Exception\ServiceException
     */
    public function testJnlpSignature()
    {
        require_once __DIR__.'/../../autoload.php';

        $c = new Client(
            $GLOBALS['URL_SERVEUR_CRYPTO'],
            null,
            null
        );

        $xml = $c->signatureService()->jnlpSignature($GLOBALS['URL_SERVEUR_CRYPTO']);
        $parts = explode("\n", $xml, 2);
        if ($parts[1]) {
            $xml = $parts[1];
        }

        $expected = new DOMDocument();
        $expected->loadXML($parts[1]);

        $actual = new DOMDocument();
        $actual->load(__DIR__.'/jnlp_signature.xml');

        $this->assertEqualXMLStructure($expected->firstChild, $actual->firstChild, true);
    }
}
