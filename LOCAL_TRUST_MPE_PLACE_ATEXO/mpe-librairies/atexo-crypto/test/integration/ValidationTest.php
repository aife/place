<?php
/**
 * Created by PhpStorm.
 * User: ARH
 * Date: 28/09/2016
 * Time: 12:09.
 */

use AtexoCrypto\Client\Client;
use AtexoCrypto\Dto\InfosSignature;
use AtexoCrypto\Tool\TestUtils;

class ValidationTest extends PHPUnit_Framework_TestCase
{
    /**
     * Test return array of InfosSignature.
     *
     * @throws \AtexoCrypto\Exception\ServiceException
     */
    public function testVerificationSignature()
    {
        require_once __DIR__.'/../../autoload.php';

        $c = new Client(
            $GLOBALS['URL_SERVEUR_CRYPTO'],
            null,
            null
        );
        $signature = TestUtils::createValidation();

        $results = $c->validationService()->verifierSignature($signature);

        $this->assertInstanceOf(InfosSignature::class, $results);
    }

    /**
     * Test return json.
     *
     * @throws \AtexoCrypto\Exception\ServiceException
     */
    public function testVerificationHorodatage()
    {
        require_once __DIR__.'/../../autoload.php';

        $c = new Client(
            $GLOBALS['URL_SERVEUR_CRYPTO'],
            null,
            null
        );

        $results = $c->validationService()->verifierHorodatage('', '');

        $this->assertNotEmpty($results);
    }
}
