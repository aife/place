<?php
/**
 * Created by PhpStorm.
 * User: ARH
 * Date: 28/09/2016
 * Time: 12:09.
 */

use AtexoCrypto\Client\Client;
use AtexoCrypto\Dto\InfosHorodatage;

class HorodatageTest extends PHPUnit_Framework_TestCase
{
    /**
     * Test Integration return jeton horodatage.
     *
     * @throws \AtexoCrypto\Exception\ServiceException
     */
    public function testHorodatage()
    {
        require_once __DIR__.'/../../autoload.php';

        $c = new Client(
            $GLOBALS['URL_SERVEUR_CRYPTO'],
            null,
            null
        );
        $xmlHash256 = <<<XML
<horodatage>
   <idOffre>1</idOffre>
   <fichiers>
      <fichier>
        <hash>456520505de27b46f32048beb43e5acdd449c45bf52a3f9de86a0c269ef2ef54</hash>
         <nom>fichier1</nom>
     </fichier>
     <fichier>
        <hash>336eb29103fd1dcfa997748cb581da84fef1910185487982ab9b504ac96db5ab</hash>
         <nom>fichier2</nom>
     </fichier>
</fichiers>
</horodatage>
XML;

        $results = $c->horodatageService()->demandeHorodatage(hash('sha256', $xmlHash256));

        $this->assertInstanceOf(InfosHorodatage::class, $results);
        $this->assertEquals('13', strlen($results->getHorodatage()));
    }

    /**
     * Test return ServiceException.
     *
     * @expectedException     \AtexoCrypto\Exception\ServiceException
     * @expectedExceptionCode 0
     */
    public function testErreurJsonDemandeChiffrement()
    {
        require_once __DIR__.'/../../autoload.php';

        $c = new Client(
            $GLOBALS['URL_SERVEUR_CRYPTO'],
            null,
            null
        );
        $c->horodatageService()->demandeHorodatage('');
    }
}
