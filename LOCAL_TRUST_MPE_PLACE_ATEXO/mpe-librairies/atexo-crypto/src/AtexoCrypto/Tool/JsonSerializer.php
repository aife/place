<?php
/**
 * Created by PhpStorm.
 * User: ARH
 * Date: 28/09/2016
 * Time: 10:42.
 */

namespace AtexoCrypto\Tool;

class JsonSerializer
{
    /**
     * Local cache of a property getters per class - optimize reflection code if the same object appears several times.
     *
     * @var array
     */
    private $classPropertyGetters = [];

    /**
     * @param mixed $object
     *
     * @return string|false
     */
    public function serialize($object)
    {
        return json_encode($this->serializeInternal($object), JSON_UNESCAPED_SLASHES);
    }

    /**
     * @param $object
     *
     * @return array
     */
    private function serializeInternal($object)
    {
        if (is_array($object)) {
            $result = $this->serializeArray($object);
        } elseif (is_object($object)) {
            $result = $this->serializeObject($object);
        } else {
            $result = $object;
        }

        return $result;
    }

    /**
     * @param $object
     *
     * @return \ReflectionClass
     */
    private function getClassPropertyGetters($object)
    {
        $className = get_class($object);
        if (!isset($this->classPropertyGetters[$className])) {
            $reflector = new \ReflectionClass($className);
            $properties = $reflector->getProperties();
            $getters = [];
            foreach ($properties as $property) {
                $name = $property->getName();
                $getter = 'get'.ucfirst($name);
                try {
                    $reflector->getMethod($getter);
                    $getters[$name] = $getter;
                } catch (\Exception $e) {
                    // if no getter for a specific property - ignore it
                }
            }
            $this->classPropertyGetters[$className] = $getters;
        }

        return $this->classPropertyGetters[$className];
    }

    /**
     * @param $object
     *
     * @return array
     */
    private function serializeObject($object)
    {
        $properties = $this->getClassPropertyGetters($object);
        $data = [];
        foreach ($properties as $name => $property) {
            $data[$name] = $this->serializeInternal($object->$property());
        }

        return $data;
    }

    /**
     * @param $array
     *
     * @return array
     */
    private function serializeArray($array)
    {
        $result = [];
        foreach ($array as $key => $value) {
            $result[$key] = $this->serializeInternal($value);
        }

        return $result;
    }
}
