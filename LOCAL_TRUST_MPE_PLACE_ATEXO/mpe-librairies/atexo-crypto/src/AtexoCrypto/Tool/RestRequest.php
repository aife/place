<?php

namespace AtexoCrypto\Tool;

use AtexoCrypto\Exception\RestRequestException;

class RestRequest
{
    protected $url;
    protected $verb;
    protected $request_body;
    protected $request_length;
    protected $username;
    protected $password;
    protected $accept_type;
    protected $content_type;
    protected $response_body;
    protected $response_info;
    protected $file_to_upload = [];
    protected $headers;
    protected $curl_timeout;
    protected $proxy;
    protected $debug = false;

    /**
     * Constructeur.
     *
     * @param null   $url
     * @param string $verb
     * @param null   $request_body
     * @param bool   $debug
     */
    public function __construct($url = null, $verb = 'GET', $request_body = null, $debug = false)
    {
        $this->url = $url;
        $this->verb = $verb;
        $this->request_body = $request_body;
        $this->debug = $debug;
        $this->request_length = 0;
        $this->username = null;
        $this->password = null;
        $this->accept_type = null;
        $this->content_type = 'application/json';
        $this->response_body = null;
        $this->response_info = null;
        $this->file_to_upload = [];
        $this->curl_timeout = 30;
        $this->proxy = null;

        if (null !== $this->request_body) {
            $this->buildPostBody();
        }
    }

    public $errorCode;

    /** This function will convert an indexed array of headers into an associative array where the key matches
     * the key of the headers, and the value matches the value of the header.
     *
     * This is useful to access headers by name that may be returned in the response from makeRequest.
     *
     * @param $array array Indexed header array returned by makeRequest
     *
     * @return array
     */
    public static function splitHeaderArray($array)
    {
        $result = [];
        foreach (array_values($array) as $value) {
            $pair = explode(':', $value, 2);
            if (count($pair) > 1) {
                $result[$pair[0]] = ltrim($pair[1]);
            } else {
                $result[] = $value;
            }
        }

        return $result;
    }

    /**
     * flush parameter.
     */
    public function flush()
    {
        $this->request_body = null;
        $this->request_length = 0;
        $this->verb = 'GET';
        $this->response_body = null;
        $this->response_info = null;
        $this->content_type = 'application/json';
        $this->accept_type = 'application/json';
        $this->file_to_upload = null;
        $this->headers = null;
    }

    /**
     * Executer la requete.
     *
     * @throws \Exception
     */
    public function execute()
    {
        $ch = curl_init();
        $this->setAuth($ch);
        $this->setTimeout($ch);

        if ($this->debug) {
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            $debugFile = fopen('/tmp/curl-debug.log', 'w+');
            curl_setopt($ch, CURLOPT_STDERR, $debugFile);
        }

        try {
            switch (strtoupper($this->verb)) {
                case 'GET':
                    $this->executeGet($ch);
                    break;
                case 'POST':
                    $this->executePost($ch);
                    break;
                case 'PUT':
                    $this->executePut($ch);
                    break;
                case 'DELETE':
                    $this->executeDelete($ch);
                    break;
                case 'PUT_MP':
                    $this->verb = 'PUT';
                    $this->executePutMultipart($ch);
                    break;
                case 'POST_MP':
                    $this->verb = 'POST';
                    $this->executePostMultipart($ch);
                    break;
                case 'POST_BIN':
                    $this->verb = 'POST';
                    $this->executeBinarySend($ch);
                    break;
                case 'PUT_BIN':
                    $this->verb = 'PUT';
                    $this->executeBinarySend($ch);
                    break;
                default:
                    throw new \InvalidArgumentException('Current verb ('.$this->verb.') is an invalid REST verb.');
            }
        } catch (\InvalidArgumentException $e) {
            curl_close($ch);
            throw $e;
        } catch (\Exception $e) {
            curl_close($ch);
            throw $e;
        }
    }

    /**
     * buildPostBody.
     *
     * @param null $data
     */
    public function buildPostBody($data = null)
    {
        $data = (null !== $data) ? $data : $this->request_body;
        $this->request_body = $data;
    }

    /**
     * Executer la requete GET.
     *
     * @param $ch
     */
    protected function executeGet($ch)
    {
        $this->doExecute($ch);
    }

    /**
     * Executer la requete POST.
     *
     * @param $ch
     */
    protected function executePost($ch)
    {
        if (!is_string($this->request_body)) {
            $this->buildPostBody();
        }
        /*
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: .' . $this->content_type
                ));
        */
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->request_body);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

        $this->doExecute($ch);
    }

    /**
     * Executer la requete send binary.
     *
     * @param $ch
     */
    protected function executeBinarySend($ch)
    {
        $post = $this->request_body;

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->verb);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);

        $this->response_body = curl_exec($ch);
        $this->response_info = curl_getinfo($ch);

        curl_close($ch);
    }

    /**
     * Executer la requete multipart PUT
     * Set verb to PUT_MP to use this function.
     *
     * @param $ch
     */
    protected function executePutMultipart($ch)
    {
        $post = $this->request_body;

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $this->response_body = curl_exec($ch);
        $this->response_info = curl_getinfo($ch);

        curl_close($ch);
    }

    /**
     * Executer la requete multipart POST
     * Set verb to POST_MP to use this function.
     *
     * @param $ch
     */
    protected function executePostMultipart($ch)
    {
        $post = $this->request_body;

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $this->response_body = curl_exec($ch);
        $this->response_info = curl_getinfo($ch);

        curl_close($ch);
    }

    /**
     * Executer la requete PUT.
     *
     * @param $ch
     */
    protected function executePut($ch)
    {
        if (!is_string($this->request_body)) {
            $this->buildPostBody();
        }

        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->request_body);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');

        $this->doExecute($ch);
    }

    /**
     * Executer la requete DELETE.
     *
     * @param $ch
     */
    protected function executeDelete($ch)
    {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

        $this->doExecute($ch);
    }

    /**
     * Executer la requete.
     *
     * @param $curlHandle
     */
    protected function doExecute(&$curlHandle)
    {
        $this->setCurlOpts($curlHandle);
        $response = curl_exec($curlHandle);
        $this->response_info = curl_getinfo($curlHandle);

        //$response = preg_replace("/^(?:HTTP\/1.1 100 Continue.*?\\r\\n\\r\\n)+/ms", "", $response);

        $header_size = curl_getinfo($curlHandle, CURLINFO_HEADER_SIZE);
        $this->response_headers = substr($response, 0, $header_size);
        $this->response_body = substr($response, $header_size);
        curl_close($curlHandle);

        //  100-continue chunks are returned on multipart communications
        //$headerblock = strstr($response, "\r\n\r\n", true);

        // strstr returns the matched characters and following characters, but we want to discard of "\r\n\r\n", so
        // we delete the first 4 bytes of the returned string.
        //$this->response_body = substr(strstr($response, "\r\n\r\n"), 4);
        // headers are always separated by \n until the end of the header block which is separated by \r\n\r\n.
        //$this->response_headers = explode("\r\n", $headerblock);

        //curl_close($curlHandle);
    }

    /**
     * Prepare les curl options.
     *
     * @param $curlHandle
     */
    protected function setCurlOpts(&$curlHandle)
    {
        curl_setopt($curlHandle, CURLOPT_URL, $this->url);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_COOKIEFILE, '/dev/null');
        curl_setopt($curlHandle, CURLOPT_HEADER, true);

        if (!empty($this->content_type)) {
            $this->headers[] = 'Content-Type: '.$this->content_type;
        }
        if (!empty($this->accept_type)) {
            $this->headers[] = 'Accept: '.$this->accept_type;
        }
        if (!empty($this->headers)) {
            curl_setopt($curlHandle, CURLOPT_HTTPHEADER, $this->headers);
        }
        if (!empty($this->proxy)) {
            curl_setopt($curlHandle, CURLOPT_PROXY, $this->proxy);
        }
    }

    /**
     * Prepare Auth.
     *
     * @param $curlHandle
     */
    protected function setAuth(&$curlHandle)
    {
        if (null !== $this->username && null !== $this->password) {
            curl_setopt($curlHandle, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curlHandle, CURLOPT_USERPWD, $this->username.':'.$this->password);
        }
    }

    /**
     * setTimeout.
     *
     * @param $curlHandle
     */
    protected function setTimeout(&$curlHandle)
    {
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, $this->curl_timeout);
    }

    public function defineTimeout($seconds)
    {
        $this->curl_timeout = $seconds;
    }

    public function getFileToUpload()
    {
        return $this->file_to_upload;
    }

    public function setFileToUpload($filepath)
    {
        $this->file_to_upload = $filepath;
    }

    public function getAcceptType()
    {
        return $this->accept_type;
    }

    public function setAcceptType($accept_type)
    {
        $this->accept_type = $accept_type;
    }

    public function getContentType()
    {
        return $this->content_type;
    }

    public function setContentType($content_type)
    {
        $this->content_type = $content_type;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getResponseBody()
    {
        return $this->response_body;
    }

    public function getResponseInfo()
    {
        return $this->response_info;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getVerb()
    {
        return $this->verb;
    }

    public function setVerb($verb)
    {
        $this->verb = $verb;
    }

    public function getProxy()
    {
        return $this->proxy;
    }

    public function setProxy($proxy)
    {
        $this->proxy = $proxy;
    }

    public function setDebug($debug)
    {
        $this->debug = $debug;
    }

    /**
     * handleError.
     *
     * @param $statusCode
     * @param $expectedCodes
     * @param $responseBody
     *
     * @throws RestRequestException
     */
    public function handleError($statusCode, $expectedCodes, $responseBody)
    {
        if (!empty($responseBody)) {
            $errorData = json_decode($responseBody);
            $exception = new RestRequestException(
                (empty($errorData->message)) ? RestRequestException::UNEXPECTED_CODE_MSG : $errorData->message
            );
            $exception->expectedStatusCodes = $expectedCodes;
            $exception->statusCode = $statusCode;
            if (!empty($errorData->errorCode)) {
                $exception->errorCode = $errorData->errorCode;
            }
            if (!empty($errorData->parameters)) {
                $exception->parameters = $errorData->parameters;
            }

            throw $exception;
        } else {
            $exception = new RestRequestException(RestRequestException::UNEXPECTED_CODE_MSG);
            $exception->expectedStatusCodes = $expectedCodes;
            $exception->statusCode = $statusCode;

            throw $exception;
        }
    }

    /**
     * makeRequest.
     *
     * @param $url
     * @param array  $expectedCodes
     * @param null   $verb
     * @param null   $reqBody
     * @param bool   $returnData
     * @param string $contentType
     * @param string $acceptType
     * @param array  $headers
     *
     * @return array
     *
     * @throws RestRequestException
     * @throws \Exception
     */
    public function makeRequest(
        $url,
        $expectedCodes = [200],
        $verb = null,
        $reqBody = null,
        $returnData = false,
        $contentType = 'application/json',
        $acceptType = 'application/json',
        $headers = []
    ) {
        $this->flush();
        $this->setUrl($url);
        if (null !== $verb) {
            $this->setVerb($verb);
        }
        if (null !== $reqBody) {
            $this->buildPostBody($reqBody);
        }
        if (!empty($contentType)) {
            $this->setContentType($contentType);
        }
        if (!empty($acceptType)) {
            $this->setAcceptType($acceptType);
        }
        if (!empty($headers)) {
            $this->headers = $headers;
        }

        $this->execute();

        $info = $this->getResponseInfo();
        $statusCode = $info['http_code'];
        $body = $this->getResponseBody();

        $headers = $this->response_headers;

        // An exception is thrown here if the expected code does not match the status code in the response
        if (!in_array($statusCode, $expectedCodes)) {
            $this->handleError($statusCode, $expectedCodes, $body);
        }

        return compact('body', 'statusCode', 'headers');
    }

    /**
     * prepAndSend.
     *
     * @param $url
     * @param array  $expectedCodes
     * @param null   $verb
     * @param null   $reqBody
     * @param bool   $returnData
     * @param string $contentType
     * @param string $acceptType
     * @param array  $headers
     *
     * @return bool|null
     *
     * @throws RestRequestException
     * @throws \Exception
     */
    public function prepAndSend(
        $url,
        $expectedCodes = [200],
        $verb = null,
        $reqBody = null,
        $returnData = false,
        $contentType = 'application/json',
        $acceptType = 'application/json',
        $headers = []
    ) {
        $this->flush();
        $this->setUrl($url);
        if (null !== $verb) {
            $this->setVerb($verb);
        }
        if (null !== $reqBody) {
            $this->buildPostBody($reqBody);
        }
        if (!empty($contentType)) {
            $this->setContentType($contentType);
        }
        if (!empty($acceptType)) {
            $this->setAcceptType($acceptType);
        }
        if (!empty($headers)) {
            $this->headers = $headers;
        }

        $this->execute();
        $statusCode = $this->getResponseInfo();
        $responseBody = $this->getResponseBody();
        $statusCode = $statusCode['http_code'];

        if (!in_array($statusCode, $expectedCodes)) {
            $this->handleError($statusCode, $expectedCodes, $responseBody);
        }

        if (true == $returnData) {
            return $this->getResponseBody();
        }

        return true;
    }

    /**
     * This function creates a multipart/form-data request and sends it to the server.
     * this function should only be used when a file is to be sent with a request (PUT/POST).
     *
     * @param $url - URL to send request to
     * @param int|$expectedCode - HTTP Status Code you expect to receive on success
     * @param $verb - HTTP Verb to send with request
     * @param $reqBody - The body of the request if necessary
     * @param array $file - An array with the URI representing the image, and the filepath to the image
     *
     * @return array - Returns an array with the response info and the response body, since the server sends a 100 request, it is hard to validate the success of the request
     */
    public function multipartRequestSend($url, $expectedCodes = [200, 201], $verb = 'PUT_MP', $reqBody = null, $file = null)
    {
        $this->flush();
        $this->setUrl($url);
        $this->setVerb($verb);
        if (!empty($reqBody)) {
            $this->buildPostBody($reqBody);
        }
        if (!empty($file)) {
            $this->setFileToUpload($file);
        }
        $this->execute();
        $response = $this->getResponseInfo();
        $responseBody = $this->getResponseBody();
        $statusCode = $response['http_code'];

        if (!in_array($statusCode, $expectedCodes)) {
            $this->handleError($statusCode, $expectedCodes, $responseBody);
        }

        return $responseBody;
    }

    /**
     * sendBinary.
     *
     * @param $url
     * @param $expectedCodes
     * @param $body
     * @param $contentType
     * @param $contentDisposition
     * @param $contentDescription
     * @param string $verb
     *
     * @return null
     *
     * @throws RestRequestException
     * @throws \Exception
     */
    public function sendBinary($url, $expectedCodes, $body, $contentType, $contentDisposition, $contentDescription, $verb = 'POST')
    {
        $this->flush();
        $this->setUrl($url);
        $this->setVerb($verb.'_BIN');
        $this->buildPostBody($body);
        $this->setContentType($contentType);
        $this->headers = ['Content-Type: '.$contentType, 'Content-Disposition: '.$contentDisposition, 'Content-Description: '.$contentDescription, 'Accept: application/json'];

        $this->execute();

        $statusCode = $this->getResponseInfo();
        $responseBody = $this->getResponseBody();
        $statusCode = $statusCode['http_code'];

        if (!in_array($statusCode, $expectedCodes)) {
            $this->handleError($statusCode, $expectedCodes, $responseBody);
        }

        return $this->getResponseBody();
    }
}
