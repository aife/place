<?php
/**
 * Created by PhpStorm.
 * User: ARH
 * Date: 29/09/2016
 * Time: 08:55.
 *
 * Enumération pour distinguer les différents erreurs
 */

namespace AtexoCrypto\Tool;

abstract class ValidEnum
{
    const VALID = 'OK';
    const ERROR_CONFIG = 'ERROR_OBJET_CONFIG';
    const ERROR_CONSULTATION = 'ERROR_OBJET_CONSULTATION';
    const ERROR_OFFRE = 'ERROR_OBJET_OFFRE';
    const ERROR_ENVELOPPE = 'ERROR_OBJET_ENVELOPPE';
    const ERROR_CERTIFICAT = 'ERROR_OBJET_CERTIFICAT';
    const ERROR_FICHIER = 'ERROR_OBJET_FICHIER';
    const ERROR_SIGNATURE = 'ERROR_OBJET_SIGNATURE';
    const ERROR_SIGNATURE_FICHIER = 'ERROR_OBJET_SIGNATURE_FICHIER';
    const ERROR_FICHIER_ABSENT = 'ERROR_FICHIER_ABSENT';
    const ERROR_XML_HASH256 = 'ERROR_XML_HASH256';
}
