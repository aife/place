<?php

namespace AtexoCrypto\Service;

use App\Service\Crypto\WebServicesCrypto;
use AtexoCrypto\Client\Client;
use AtexoCrypto\Dto\InfosHorodatage;
use AtexoCrypto\Exception\RestRequestException;
use AtexoCrypto\Exception\ServiceException;
use AtexoCrypto\Tool\ValidEnum;

/**
 * Class ChiffrementService.
 */
class ChiffrementService
{
    protected $service;
    protected $restUrl;
    protected $logger = null;
    protected ?WebServicesCrypto $webServicesCrypto = null;

    public function __construct(Client &$client)
    {
        $this->service = $client->getService();
        $this->restUrl = $client->getURL();
        $this->logger = $client->getLogger();
        $this->webServicesCrypto = $client->getWebServicesCrypto();
    }

    /**
     * Initier une demande de chiffrement.
     *
     * @param Chiffrement
     *
     * @return jeton horodatage
     */
    public function demandeChiffrement(\AtexoCrypto\Dto\Chiffrement $chiffrement)
    {
        $url = $this->restUrl.'/cryptographie/chiffrement';
        if ($this->logger) {
            $this->logger->info('Api Crypto : Service Chiffrement : demandeChiffrement : url : '.$url);
        }
        $isValid = $chiffrement->isValid();
        if (ValidEnum::VALID == $isValid) {
            $json = $chiffrement->toJSON();
            if ($this->logger) {
                $this->logger->info('Api Crypto : Service Chiffrement : demandeChiffrement : json : '.$json);
            }
            try {
                $resp = $this->webServicesCrypto->prepAndSend($url, [201, 200], 'POST', $json, true, 'application/json', 'application/json');
            } catch (RestRequestException $e) {
                if ($this->logger) {
                    $this->logger->error('Api Crypto : Service Chiffrement : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
                }
                throw new ServiceException(ServiceException::ERROR_REST, $isValid, $e->getMessage());
            }

            $result = json_decode($resp);

            if (200 == $result->code) {
                if ($this->logger) {
                    $this->logger->info('Api Crypto : Service Chiffrement : resultat : '.$resp);
                }
                $horodatage = new InfosHorodatage($result);

                return $horodatage;
            }
            if ($this->logger) {
                $this->logger->error('Api Crypto : Service Chiffrement : Erreur recuperation jeton horodatage : '.$result->message);
            }
            throw new ServiceException(ServiceException::ERROR_HORODATAGE, $isValid, $result->message);
        } else {
            if ($this->logger) {
                $this->logger->error('Api Crypto : Service Chiffrement : Erreur Json : CODE : '.$isValid.' Objet : '.$chiffrement->toJSON());
            }
            throw new ServiceException(ServiceException::ERROR_JSON, $isValid, 'invalid Object');
        }
    }

    /**
     * @param $params
     *
     * @return string|null
     */
    public function verifierDisponibilite($params)
    {
        $json = json_encode($params);
        $url = $this->restUrl.'/cryptographie/urlvie';
        $resp = null;
        try {
            $resp = $this->webServicesCrypto->prepAndSend($url, [201, 401, 403, 404], 'POST', $json, true, 'application/json', 'application/json');
        } catch (\Exception $e) {
        }

        return $resp;
    }
}
