<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace AtexoCrypto\Service;

use Application\Service\Atexo\CurrentUserTrait;
use App\Service\Crypto\WebServicesCrypto;
use AtexoCrypto\Client\Client;
use AtexoCrypto\Dto\Dechiffrement;
use AtexoCrypto\Exception\RestRequestException;
use AtexoCrypto\Exception\ServiceException;
use AtexoCrypto\Tool\ValidEnum;
use AtexoCrypto\Tool\Util;

/**
 * Class DechiffrementService.
 */
class DechiffrementService
{
    use CurrentUserTrait;
    const ENDPOINT_DECHIFFREMENT = '/cryptographie/local-server/dechiffrement-en-ligne';
    protected $service;
    protected $restUrl;
    protected $logger = null;
    protected ?WebServicesCrypto $webServicesCrypto = null;

    /**
     * DechiffrementService constructor.
     *
     * @param Client $client Utilisation du client http Atexo
     */
    public function __construct(Client $client)
    {
        $this->service = $client->getService();
        $this->restUrl = $client->getURL();
        $this->logger = $client->getLogger();
        $this->webServicesCrypto = $client->getWebServicesCrypto();
    }

    /**
     * Demande dechiffrement
     * @param Dechiffrement $dechiffrement
     * @return false|string
     * @throws ServiceException
     */
    public function demandeDechiffrement(Dechiffrement $dechiffrement)
    {
        if (!$this->isMAMPEnabled()) {
            $url = $this->restUrl . '/cryptographie/generateDechiffrementJNLP';
        } else {
            $url = $this->restUrl . self::ENDPOINT_DECHIFFREMENT;
        }

        $isValid = $dechiffrement->isValid();
        if (ValidEnum::VALID == $isValid) {
            $json = $dechiffrement->toJSON();
            if ($this->logger) {
                $this->logger->info(sprintf(
                        'Api Crypto: Service Dechiffrement: demandeDechiffrement: json : %s',
                        $json
                ));
            }
            try {
                $result = $this->webServicesCrypto->prepAndSend(
                    $url,
                    [201, 200],
                    'POST',
                    $json,
                    true,
                    'application/json',
                    'application/json'
                );
            } catch (RestRequestException $e) {
                if ($this->logger) {
                    $this->logger->error(
                        sprintf(
                            'Api Crypto: Service Dechiffrement: Erreur Rest: HTTP CODE: %s MESSAGE: %s',
                            $e->statusCode,
                            $e->getMessage()
                        )
                    );
                }
                throw new ServiceException(ServiceException::ERROR_REST, $isValid, $e->getMessage());
            }


            if (mb_detect_encoding($result, 'utf-8')) {
                $result = trim($result);
            } else {
                $result = utf8_encode(trim($result));
            }
            if($this->isMAMPEnabled()){
                if (!empty($result)) {
                    if ($this->logger) {
                        $this->logger->info(sprintf('Api Crypto : Service Dechiffrement : jnlp : %s', $result));
                    }
                    return $result;
                }
            }else{
                if (!empty($result) && Util::isXml($result)) {
                    if ($this->logger) {
                        $this->logger->info(sprintf("Api Crypto : Service Dechiffrement : jnlp : %s", $result));
                    }
                    return $result;
                }
            }

            if ($this->logger) {
                $this->logger->error(
                    sprintf(
                        'Api Crypto: Service Dechiffrement: Erreur recuperation jnlp / application: %s',
                        $result
                    )
                );
            }
            throw new ServiceException(ServiceException::ERROR_JNPL, $isValid, $result);
        } else {
            if ($this->logger) {
                $this->logger->error(
                    sprintf(
                        'Api Crypto: Service Dechiffrement: Erreur Json: CODE : %s Objet : %s',
                        $isValid,
                        $dechiffrement->toJSON()
                    )
                );
            }
            throw new ServiceException(ServiceException::ERROR_JSON, $isValid, 'invalid Object');
        }
    }
}
