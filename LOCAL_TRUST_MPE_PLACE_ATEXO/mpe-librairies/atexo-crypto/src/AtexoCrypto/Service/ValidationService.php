<?php

namespace AtexoCrypto\Service;

use App\Service\Crypto\WebServicesCrypto;
use AtexoCrypto\Client\Client;
use AtexoCrypto\Dto\InfosSignature;
use AtexoCrypto\Exception\RestRequestException;
use AtexoCrypto\Exception\ServiceException;
use AtexoCrypto\Tool\ValidEnum;

/**
 * Class QueryService.
 */
class ValidationService
{
    protected $service;
    protected $restUrl;
    protected $logger = null;
    protected $platform = null;
    protected ?WebServicesCrypto $webServicesCrypto = null;

    public function __construct(Client &$client)
    {
        $this->service = $client->getService();
        $this->restUrl = $client->getURL();
        $this->logger = $client->getLogger();
        $this->platform = $client->getPlatform();
        $this->webServicesCrypto = $client->getWebServicesCrypto();
    }

    /**
     * Vérification de signature.
     *
     * @param $signature
     * @param bool $arrayReturned
     * @param null $signatureFichier
     *
     * @return array|InfosSignature
     *
     * @throws ServiceException
     */
    public function verifierSignature($signature, $arrayReturned = false, $signatureFichier = null)
    {
        if ('PADES' == $signature->getSignatureType()) {
            if ($arrayReturned) {
                $url = $this->restUrl.'/signature/verifierSignaturePDF?plateforme='.$this->platform;
            } else {
                $url = $this->restUrl.'/signature/verifierSignaturePDF';
            }

            if ($this->logger) {
                $this->logger->info('Api Crypto : Service Validation Signature : verifierSignaturePDF : url : '.$url);
            }
        } else {
            if ($arrayReturned) {
                $url = $this->restUrl.'/signature/v2/verifierSignature?plateforme='.$this->platform;
            } else {
                $url = $this->restUrl.'/signature/verifierSignature';
            }

            if ($this->logger) {
                $this->logger->info('Api Crypto : Service Validation Signature : verifierSignature : url : '.$url);
            }
        }

        $isValid = $signature->isValid();

        if (ValidEnum::VALID == $isValid) {
            $json = $signature->toJSON();

            try {
                if ('PADES' != $signature->getSignatureType()) {
                    if ($this->logger) {
                        $this->logger->info('Api Crypto : Service Validation Signature : verifierSignature : json : '.$json);
                    }

                    $contentType = 'application/json';

                    $result = $this->webServicesCrypto->prepAndSend(
                        $url,
                        [201, 200],
                        'POST',
                        $json,
                        true,
                        $contentType,
                        $contentType,
                        [],
                        false
                    );
                } else {
                    $result = $this->webServicesCrypto->multipartRequestSend(
                        $url,
                        [201, 200],
                        'POST_MP',
                        $signatureFichier->getContenu(),
                        false
                    );
                }
            } catch (RestRequestException $e) {
                if ($this->logger) {
                    $this->logger->error('Api Crypto : Service Validation Signature : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
                }

                throw new ServiceException(ServiceException::ERROR_REST, $isValid, $e->getMessage());
            }

            if (!empty($result)) {
                if ($this->logger) {
                    $this->logger->info('Api Crypto : Service Validation Signature : resultat : '.$result);
                }

                if (true == $arrayReturned) {
                    foreach (json_decode($result, true) as $arraySignatures) {
                        if (is_array($arraySignatures)) {
                            foreach ($arraySignatures as $arraySignature) {
                                $listeVerification[] = new InfosSignature($arraySignature);
                            }
                        }
                    }
                } elseif (false == $arrayReturned) {
                    $data = json_decode($result, true);

                    if (isset($data['signatures'][0])) {
                        $listeVerification = new InfosSignature($data['signatures'][0]);
                    } else {
                        if ($arrayReturned) {
                            $listeVerification = new InfosSignature();
                        } else {
                            $listeVerification = new InfosSignature($data);
                        }
                    }
                }

                return $listeVerification;
            }

            if ($this->logger) {
                $this->logger->error('Api Crypto : Service Validation Signature : Erreur recuperation infos verification : '.$result);
            }

            throw new ServiceException(ServiceException::ERROR_VERIFICATION_SIGNATURE, $isValid, $result);
        } else {
            if ($this->logger) {
                $this->logger->error('Api Crypto : Service Validation Signature : Erreur Json : CODE : '.$isValid.' Objet : '.$signature->toJSON());
            }

            throw new ServiceException(ServiceException::ERROR_JSON, $isValid, 'invalid Object');
        }
    }

    /**
     * Vérification de l'horodatage.
     *
     * @param string $jetonTSR
     * @param string $hashSha256
     *
     * @return json
     *
     * @throws ServiceException
     */
    public function verifierHorodatage($jetonTSR, $hashSha256)
    {
        $url = $this->restUrl.'/signature/validationJetonHorodatage';

        if ($this->logger) {
            $this->logger->info('Api Crypto : Service Validation Horodatage : verifierHorodatage : url : '.$url.' params jetonTSR : '.$jetonTSR.' string : '.$hashSha256);
        }

        if (!empty($jetonTSR) && !empty($hashSha256)) {
            try {
                $result = $this->webServicesCrypto->prepAndSend(
                    $url,
                    [201, 200],
                    'POST',
                    ['jetonTSR' => $jetonTSR, 'string' => $hashSha256],
                    true,
                    'multipart/form-data'
                );
            } catch (RestRequestException $e) {
                if ($this->logger) {
                    $this->logger->error('Api Crypto : Service Validation Horodatage : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
                }

                throw new ServiceException(ServiceException::ERROR_REST, ValidEnum::VALID, $e->getMessage());
            }

            if (!empty($result)) {
                if ($this->logger) {
                    $this->logger->info('Api Crypto : Service Validation Horodatage : resultat : '.$result);
                }

                return $result;
            }

            if ($this->logger) {
                $this->logger->error('Api Crypto : Service Validation Horodatage : Erreur recuperation infos verification : '.$result);
            }

            throw new ServiceException(ServiceException::ERROR_VERIFICATION_HORODATAGE, ValidEnum::VALID, $result);
        } else {
            if ($this->logger) {
                $this->logger->error('Api Crypto : Service Validation Horodatage : Erreur params : CODE : '.ValidEnum::ERROR_CONFIG.' Objet : jetonTSR='.$jetonTSR.' hashSha256='.$hashSha256);
            }

            throw new ServiceException(ServiceException::ERROR_JSON, ValidEnum::ERROR_CONFIG, 'invalid Object');
        }
    }
}
