<?php

namespace AtexoCrypto\Service;

use App\Service\Crypto\WebServicesCrypto;
use AtexoCrypto\Client\Client;
use AtexoCrypto\Exception\RestRequestException;
use AtexoCrypto\Exception\ServiceException;
use AtexoCrypto\Tool\ValidEnum;

/**
 * Class SignatureService.
 */
class SignatureService
{
    protected $service;
    protected $restUrl;
    protected $logger = null;
    protected ?WebServicesCrypto $webServicesCrypto = null;

    public function __construct(Client &$client)
    {
        $this->service = $client->getService();
        $this->restUrl = $client->getURL();
        $this->logger = $client->getLogger();
        $this->webServicesCrypto = $client->getWebServicesCrypto();
    }

    /**
     * signerPades.
     *
     * @param SignatureFichier $signatureFichier
     *
     * @return content pdf
     */
    public function signerPades(\AtexoCrypto\Dto\SignatureFichier $signatureFichier)
    {
        $url = $this->restUrl.'/signature/padesSignature';
        if ($this->logger) {
            $this->logger->info('Api Crypto : Service Signature Pades : signerPades : url : '.$url);
        }
        $isValid = $signatureFichier->isValid();
        if (ValidEnum::VALID == $isValid) {
            try {
                $result = $this->webServicesCrypto->multipartRequestSend(
                    $url,
                    [201, 200],
                    'POST_MP',
                    $signatureFichier->getContenu(),
                    false
                );
            } catch (RestRequestException $e) {
                if ($this->logger) {
                    $this->logger->error('Api Crypto : Service Signature Pades : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
                }
                throw new ServiceException(ServiceException::ERROR_REST, $isValid, $e->getMessage());
            }
            if (!empty($result)) {
                if ($this->logger) {
                    $this->logger->info('Api Crypto : Service Signature Pades : taille resultat : '.count($result));
                }

                return $result;
            }
            if ($this->logger) {
                $this->logger->error('Api Crypto : Service Signature Pades : Erreur recuperation pades : '.$result);
            }
            throw new ServiceException(ServiceException::ERROR_VERIFICATION_SIGNATURE, $isValid, $result);
        } else {
            if ($this->logger) {
                $this->logger->error('Api Crypto : Service Signature Pades : Erreur Array : CODE : '.$isValid.' Objet : '.$signatureFichier->toJSON());
            }
            throw new ServiceException(ServiceException::ERROR_JSON, $isValid, 'invalid Object');
        }
    }

    /**
     * signerPades.
     *
     * @param string $urlServeurCrypto
     *
     * @return jnlp
     */
    public function jnlpSignature($urlServeurCrypto)
    {
        $url = $this->restUrl.'/signature/generateSignatureJNLP?serveurCryptoURLPublique='.urlencode($urlServeurCrypto.'/signature/').'&useragent='.urlencode($_SERVER['HTTP_USER_AGENT']);
        if ($this->logger) {
            $this->logger->info('Api Crypto : Service Signature JNLP : jnlpSignature : url : '.$url);
        }
        if (!empty($urlServeurCrypto)) {
            try {
                $result = $this->webServicesCrypto->prepAndSend(
                    $url, [201, 200],
                    'GET',
                    null,
                    true,
                    'application/x-java-jnlp-file',
                    'application/x-java-jnlp-file',
                    [],
                    false
                );
            } catch (RestRequestException $e) {
                if ($this->logger) {
                    $this->logger->error('Api Crypto : Service Signature JNLP : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
                }
                throw new ServiceException(ServiceException::ERROR_REST, ValidEnum::VALID, $e->getMessage());
            }
            if (!empty($result)) {
                if ($this->logger) {
                    $this->logger->info('Api Crypto : Service Signature JNLP : resultat : '.$result);
                }

                return $result;
            }
            if ($this->logger) {
                $this->logger->error('Api Crypto : Service Signature JNLP : Erreur recuperation jnlp : '.$result);
            }
            throw new ServiceException(ServiceException::ERROR_JNPL, ValidEnum::VALID, $result);
        } else {
            if ($this->logger) {
                $this->logger->error('Api Crypto : Service Signature JNLP : Empty Url');
            }
            throw new ServiceException(ServiceException::ERROR_JSON, ValidEnum::ERROR_CONFIG, 'invalid Object');
        }
    }
}
