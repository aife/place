<?php

namespace AtexoCrypto\Service;

use App\Service\Crypto\WebServicesCrypto;
use AtexoCrypto\Client\Client;
use AtexoCrypto\Dto\InfosHorodatage;
use AtexoCrypto\Exception\RestRequestException;
use AtexoCrypto\Exception\ServiceException;
use AtexoCrypto\Tool\ValidEnum;

/**
 * Class HorodatageService.
 */
class HorodatageService
{
    protected $service;
    protected $restUrl;
    protected $logger = null;
    protected $plateforme = null;
    protected ?WebServicesCrypto $webServicesCrypto = null;

    /**
     * HorodatageService constructor.
     */
    public function __construct(Client &$client)
    {
        $this->service = $client->getService();
        $this->restUrl = $client->getURL();
        $this->logger = $client->getLogger();
        $this->plateforme = $client->getPlatform();
        $this->webServicesCrypto = $client->getWebServicesCrypto();
    }

    /**
     * demande d'Horodatage.
     *
     * @return InfosHorodatage
     *
     * @throws ServiceException
     */
    public function demandeHorodatage(string $xmlHash256)
    {
        if (!empty($xmlHash256)) {
            $url = $this->restUrl.'/cryptographie/horodatage?text='.$xmlHash256.'&plateforme='.$this->plateforme;
            $this->writeLogInfo('Api Crypto : Service Horodatage : demandeHorodatage : url : '.$url);
            $isValid = ValidEnum::VALID;
            $this->writeLogInfo('Api Crypto : Service Horodatage : demandeHorodatage : xmlHash256 : '.$xmlHash256);
            try {
                $resp = $this->webServicesCrypto->prepAndSend(
                    $url, [201, 200],
                    'GET',
                    null,
                    true
                );
            } catch (RestRequestException $e) {
                $this->writeLogError('Api Crypto : Service Horodatage : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
                throw new ServiceException(ServiceException::ERROR_REST, $isValid, $e->getMessage());
            }

            $result = json_decode($resp);
            if (200 == $result->code) {
                $this->writeLogInfo('Api Crypto : Service Horodatage : resultat : '.$resp);

                return new InfosHorodatage($result);
            }
            $this->writeLogError('Api Crypto : Service Horodatage : Erreur recuperation jeton horodatage : '.$result->message);
            throw new ServiceException(ServiceException::ERROR_HORODATAGE, $isValid, $result->message);
        } else {
            $isValid = ValidEnum::ERROR_XML_HASH256;
            $this->writeLogError('Api Crypto : Service Horodatage : Erreur xmlhash256 vide');
            throw new ServiceException(ServiceException::ERROR_XML, $isValid, 'invalid xmlhash256');
        }
    }

    public function writeLogInfo(string $msg)
    {
        if ($this->logger) {
            $this->logger->info($msg);
        }
    }

    public function writeLogError(string $msg)
    {
        if ($this->logger) {
            $this->logger->info($msg);
        }
    }
}
