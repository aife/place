<?php

namespace AtexoCrypto\Client;

use App\Service\Crypto\WebServicesCrypto;
use AtexoCrypto\Service as Service;
use AtexoCrypto\Tool\RestRequest;

/**
 * Class Client.
 */
class Client
{
    private $restReq;
    protected $serverUrl;
    protected $username;
    protected $password;
    protected $horodatageService;
    protected $chiffrementService;
    protected $dechiffrementService;
    protected $signatureService;
    protected $validationService;
    protected $logger = null;
    protected $proxy = null;
    protected $platform = null;
    protected $webServicesCrypto = null;

    public function __construct($serverUrl, $username, $password, $logger = null, $proxy = null,
        $platform = null,
        $webServicesCrypto = null
    ) {
        $this->serverUrl = $serverUrl;
        $this->username = $username;
        $this->password = $password;

        $this->restReq = new RestRequest();
        $this->restReq->setUsername($this->username);
        $this->restReq->setPassword($this->password);
        $this->restReq->setProxy($proxy);

        $this->logger = $logger;
        $this->proxy = $proxy;
        $this->platform = $platform;
        $this->webServicesCrypto = $webServicesCrypto;
    }

    /**
     * Initialisation du service de chiffrement.
     *
     * @return Service\ChiffrementService
     */
    public function chiffrementService()
    {
        if (!isset($this->chiffrementService)) {
            $this->chiffrementService = new Service\ChiffrementService($this);
        }

        return $this->chiffrementService;
    }

    /**
     * Initialisation du service de déchiffrement.
     *
     * @return Service\DechiffrementService
     */
    public function dechiffrementService()
    {
        if (!isset($this->dechiffrementService)) {
            $this->dechiffrementService = new Service\DechiffrementService($this);
        }

        return $this->dechiffrementService;
    }

    /**
     * Initialisation du service de signature.
     *
     * @return Service\SignatureService
     */
    public function signatureService()
    {
        if (!isset($this->signatureService)) {
            $this->signatureService = new Service\SignatureService($this);
        }

        return $this->signatureService;
    }

    /**
     * Initialisation du service de validation de signature.
     *
     * @return Service\ValidationService
     */
    public function validationService()
    {
        if (!isset($this->validationService)) {
            $this->validationService = new Service\ValidationService($this);
        }

        return $this->validationService;
    }

    /** setRequestTimeout.
     *
     * Set the amount of time cURL is permitted to wait for a response to a request before timing out.
     *
     * @param $seconds Time in seconds
     */
    public function setRequestTimeout($seconds)
    {
        $this->restReq->defineTimeout($seconds);
    }

    /**
     * @return string
     */
    public function getURL()
    {
        return $this->serverUrl;
    }

    /**
     * Provides the RestRequest object to be reused by the services that require it.
     *
     * @return RestRequest
     */
    public function getService()
    {
        return $this->restReq;
    }

    /**
     * @return mixed
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @return mixed
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    public function getWebServicesCrypto()
    {
        return $this->webServicesCrypto;
    }

    /**
     * Initialisation du service de Horodatage.
     *
     * @return Service\HorodatageService
     */
    public function horodatageService()
    {
        if (!isset($this->horodatageService)) {
            $this->horodatageService = new Service\HorodatageService($this);
        }

        return $this->horodatageService;
    }
}
