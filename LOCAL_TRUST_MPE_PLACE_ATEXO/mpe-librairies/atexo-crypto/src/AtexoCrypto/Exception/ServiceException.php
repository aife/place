<?php

namespace AtexoCrypto\Exception;

class ServiceException extends \Exception
{
    const ERROR_JSON = 'ERROR_JSON';
    const ERROR_XML = 'ERROR_XML';
    const ERROR_REST = 'ERROR_REST';
    const ERROR_HORODATAGE = 'ERROR_HORODATAGE';
    const ERROR_JNPL = 'ERROR_JNPL';
    const ERROR_VERIFICATION_SIGNATURE = 'ERROR_VERIFICATION_SIGNATURE';
    const ERROR_VERIFICATION_HORODATAGE = 'ERROR_VERIFICATION_HORODATAGE';

    /** Internal Error Message.
     *
     * @var string
     */
    public $message;

    /** Error Code.
     *
     * @var string
     */
    public $errorCode;

    /** Etat Objet.
     *
     * @var string
     */
    public $etatObjet;

    public function __construct($code, $etatObjet, $message)
    {
        $this->message = $message;
        $this->errorCode = $code;
        $this->etatObjet = $etatObjet;
    }
}
