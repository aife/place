<?php
/**
 * objet SignatureFichier.
 *
 * @author Anas RHORBAL <anas.rhorbal@atexo.com>
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2016
 */

namespace AtexoCrypto\Dto;

use AtexoCrypto\Tool\ValidEnum;

if (!function_exists('curl_file_create')) {
    function curl_file_create($filename, $mimetype = '', $postname = '')
    {
        return "@$filename;filename="
        .($postname ?: basename($filename))
        .($mimetype ? ";type=$mimetype" : '');
    }
}

class SignatureFichier
{
    protected $certificat;
    protected $password;
    protected $contenu;

    /**
     * @return mixed
     */
    public function getCertificat()
    {
        return $this->certificat;
    }

    /**
     * @param mixed $certificat
     */
    public function setCertificat($certificat)
    {
        $this->certificat = $certificat;
    }

    /**
     * @return mixed
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * @param mixed $contenu
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * encodé l'objet en array.
     *
     * @return string
     */
    public function toMultiPart($fichier = false)
    {
        if (null != $this->password) {
            $params['password'] = $this->password;
        }

        if (null != $this->certificat) {
            $cfileCertificat = curl_file_create($this->certificat, 'application/x-pkcs12', 'certificat');
            $params['certificat'] = $cfileCertificat;
        }

        $cfileContenu = curl_file_create($this->contenu, 'application/pdf', 'pdfFichier');

        if (true == $fichier) {
            $params['fichier'] = $cfileContenu;
        } else {
            $params['pdfFichier'] = $cfileContenu;
        }

        return $params;
    }

    /**
     * encodé l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        $serializer = new \AtexoCrypto\Tool\JsonSerializer();

        return $serializer->serialize($this);
    }

    public function isValid()
    {
        if (empty($this->certificat) || empty($this->contenu)) {
            return ValidEnum::ERROR_SIGNATURE_FICHIER;
        }

        return ValidEnum::VALID;
    }
}
