<?php
/**
 * objet Chiffrement.
 *
 * @author Amal EL BEKKAOUI <aamal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2016
 */

namespace AtexoCrypto\Dto;

use AtexoCrypto\Tool\ValidEnum;

/**
 * Class Chiffrement.
 */
class Chiffrement
{
    /**
     * @var Consultation
     */
    protected $consultation = '';
    /**
     * @var string
     */
    protected $mpeURLPrivee = '';
    /**
     * @var string
     */
    protected $mpeFilePath = '';
    /**
     * @var string
     */
    protected $raisonSocialEntreprise = '';
    /**
     * @var Offre
     */
    protected $offre = '';

    /**
     * @var string
     */
    protected $serveurCryptoURLPublic = '';
    /**
     * @var string
     */
    protected $mpeLogin = '';
    /**
     * @var string
     */
    protected $mpePassword = '';

    /**
     * @var string
     */
    protected $fonction = '';

    /**
     * @return Consultation
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    /**
     * @param Consultation $consultation
     */
    public function setConsultation($consultation)
    {
        $this->consultation = $consultation;
    }

    /**
     * @return string
     */
    public function getMpeFilePath()
    {
        return $this->mpeFilePath;
    }

    /**
     * @param $mpeFilePath
     */
    public function setMpeFilePath($mpeFilePath)
    {
        $this->mpeFilePath = $mpeFilePath;
    }

    /**
     * @return string
     */
    public function getRaisonSocialEntreprise()
    {
        return $this->raisonSocialEntreprise;
    }

    /**
     * @param $raisonSocialEntreprise
     */
    public function setRaisonSocialEntreprise($raisonSocialEntreprise)
    {
        $this->raisonSocialEntreprise = $raisonSocialEntreprise;
    }

    /**
     * @return Offre
     */
    public function getOffre()
    {
        return $this->offre;
    }

    public function setOffre(Offre $offre)
    {
        $this->offre = $offre;
    }

    /**
     * @return string
     */
    public function getMpeURLPrivee()
    {
        return $this->mpeURLPrivee;
    }

    /**
     * @param $mpeURLPrivee
     */
    public function setMpeURLPrivee($mpeURLPrivee)
    {
        $this->mpeURLPrivee = $mpeURLPrivee;
    }

    /**
     * @return string
     */
    public function getMpeLogin()
    {
        return $this->mpeLogin;
    }

    /**
     * @param string $mpeLogin
     */
    public function setMpeLogin($mpeLogin)
    {
        $this->mpeLogin = $mpeLogin;
    }

    /**
     * @return string
     */
    public function getMpePassword()
    {
        return $this->mpePassword;
    }

    /**
     * @param string $mpePassword
     */
    public function setMpePassword($mpePassword)
    {
        $this->mpePassword = $mpePassword;
    }

    /**
     * @return string
     */
    public function getServeurCryptoURLPublic()
    {
        return $this->serveurCryptoURLPublic;
    }

    /**
     * @param string $serveurCryptoURLpublique
     */
    public function setServeurCryptoURLPublic($serveurCryptoURLpublique)
    {
        $this->serveurCryptoURLPublic = $serveurCryptoURLpublique;
    }

    /**
     * @return string
     */
    public function getFonction()
    {
        return $this->fonction;
    }

    /**
     * @param string $fonction
     */
    public function setFonction($fonction)
    {
        $this->fonction = $fonction;
    }

    /**
     * encodé l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        $serializer = new \AtexoCrypto\Tool\JsonSerializer();

        return $serializer->serialize($this);
    }

    /**
     * valide l'objet consultation.
     *
     * @return bool
     */
    public function isValid()
    {
        if ($this->consultation instanceof Consultation) {
            $valid = $this->consultation->isValid();
            if (ValidEnum::VALID != $valid) {
                return $valid;
            }
        } else {
            return ValidEnum::ERROR_CONSULTATION;
        }
        if ($this->offre instanceof Offre) {
            $valid = $this->offre->isValid(true, $this->consultation->getChiffrement());
            if (ValidEnum::VALID != $valid) {
                return $valid;
            }
        } else {
            return ValidEnum::ERROR_OFFRE;
        }

        if (empty($this->mpeURLPrivee) || empty($this->mpeLogin) || empty($this->mpePassword)
            || empty($this->serveurCryptoURLPublic) || empty($this->mpeFilePath)) {
            return ValidEnum::ERROR_CONFIG;
        }

        return ValidEnum::VALID;
    }
}
