<?php

/**
 * objet fichier.
 *
 * @author Amal EL BEKKAOUI <aamal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2016
 */

namespace AtexoCrypto\Dto;

use AtexoCrypto\Tool\ValidEnum;

class Fichier
{
    protected $idFichier;
    protected $typeFichier;
    protected $name;
    protected $numeroOrdre;
    protected $idFileSystem;
    protected $idFileSystemFichierSignature;
    protected $idFichierSignature;
    protected $fichierSignatureBase64;
    protected $hashFichier;
    protected $tailleFichier;

    /**
     * @return mixed
     */
    public function getIdFichier()
    {
        return $this->idFichier;
    }

    /**
     * @return mixed
     */
    public function getHashFichier()
    {
        return $this->hashFichier;
    }

    /**
     * @param mixed $hashFichier
     */
    public function setHashFichier($hashFichier)
    {
        $this->hashFichier = $hashFichier;
    }

    /**
     * @param mixed $idFichier
     */
    public function setIdFichier($idFichier)
    {
        $this->idFichier = $idFichier;
    }

    /**
     * @return mixed
     */
    public function getTypeFichier()
    {
        return $this->typeFichier;
    }

    /**
     * @param mixed $typeFichier
     */
    public function setTypeFichier($typeFichier)
    {
        $this->typeFichier = $typeFichier;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getNumeroOrdre()
    {
        return $this->numeroOrdre;
    }

    /**
     * @param mixed $numeroOrdre
     */
    public function setNumeroOrdre($numeroOrdre)
    {
        $this->numeroOrdre = $numeroOrdre;
    }

    /**
     * @return mixed
     */
    public function getIdFileSystem()
    {
        return $this->idFileSystem;
    }

    /**
     * @param mixed $idFileSystem
     */
    public function setIdFileSystem($idFileSystem)
    {
        $this->idFileSystem = $idFileSystem;
    }

    /**
     * @return mixed
     */
    public function getIdFileSystemFichierSignature()
    {
        return $this->idFileSystemFichierSignature;
    }

    /**
     * @param mixed $idFileSystemFichierSignature
     */
    public function setIdFileSystemFichierSignature($idFileSystemFichierSignature)
    {
        $this->idFileSystemFichierSignature = $idFileSystemFichierSignature;
    }

    /**
     * @return mixed
     */
    public function getIdFichierSignature()
    {
        return $this->idFichierSignature;
    }

    /**
     * @param mixed $idFichierSignature
     */
    public function setIdFichierSignature($idFichierSignature)
    {
        $this->idFichierSignature = $idFichierSignature;
    }

    /**
     * @return mixed
     */
    public function getFichierSignatureBase64()
    {
        return $this->fichierSignatureBase64;
    }

    /**
     * @param mixed $fichierSignatureBase64
     */
    public function setFichierSignatureBase64($fichierSignatureBase64)
    {
        $this->fichierSignatureBase64 = $fichierSignatureBase64;
    }


    /**
     * encodé l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        $serializer = new \AtexoCrypto\Tool\JsonSerializer();

        return $serializer->serialize($this);
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        if (empty($this->idFichier) || empty($this->name) || empty($this->typeFichier) || empty($this->numeroOrdre)) {
            return ValidEnum::ERROR_FICHIER;
        }

        return ValidEnum::VALID;
    }

    /**
     * @return ?string
     */
    public function getTailleFichier(): ?string
    {
        return $this->tailleFichier;
    }

    /**
     * @param ?string $tailleFichier
     * @return Fichier
     */
    public function setTailleFichier(?string $tailleFichier): Fichier
    {
        $this->tailleFichier = $tailleFichier;
        return $this;
    }
}
