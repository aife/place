<?php
/**
 * objet Certificat.
 *
 * @author Amal EL BEKKAOUI <aamal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2016
 */

namespace AtexoCrypto\Dto;

use AtexoCrypto\Tool\ValidEnum;

class Certificat
{
    protected $certificatBase64;
    protected $typeEnveloppe = 0;

    /**
     * @return mixed
     */
    public function getCertificatBase64()
    {
        return $this->certificatBase64;
    }

    /**
     * @param mixed $certificatBase64
     */
    public function setCertificatBase64($certificatBase64)
    {
        $certificatBase64 = str_replace('-----BEGIN CERTIFICATE-----', '', $certificatBase64);
        $certificatBase64 = str_replace('-----END CERTIFICATE-----', '', $certificatBase64);
        $certificatBase64 = str_replace(["\r\n", "\n", "\r"], '', $certificatBase64);
        $this->certificatBase64 = trim($certificatBase64);
    }

    /**
     * @return mixed
     */
    public function getTypeEnveloppe()
    {
        return $this->typeEnveloppe;
    }

    /**
     * @param mixed $typeEnveloppe
     */
    public function setTypeEnveloppe($typeEnveloppe)
    {
        $this->typeEnveloppe = $typeEnveloppe;
    }

    /**
     * encodé l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        $serializer = new \AtexoCrypto\Tool\JsonSerializer();

        return $serializer->serialize($this);
    }

    public function isValid()
    {
        if (empty($this->certificatBase64) || empty($this->typeEnveloppe)) {
            return ValidEnum::ERROR_CERTIFICAT;
        }

        return ValidEnum::VALID;
    }
}
