<?php
/**
 * objet Signature.
 *
 * @author Anas RHORBAL <anas.rhorbal@atexo.com>
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2016
 */

namespace AtexoCrypto\Dto;

class InfosSignature
{
    protected $etat;
    protected $signatairePartiel;
    protected $signataireComplet;
    protected $emetteur;
    protected $dateValiditeDu;
    protected $dateValiditeAu;
    protected $periodiciteValide;
    protected $chaineDeCertificationValide;
    protected $absenceRevocationCRL;
    protected $dateSignatureValide;
    protected $signatureValide;
    protected $repertoiresChaineCertification;
    protected $repertoiresRevocation;
    protected $emetteurDetails;
    protected $signataireDetail;
    protected $signataireDetails;
    protected $alternativeNameSignataire;
    protected $cnSignataire;
    protected $ouSignataire;
    protected $oSignataire;
    protected $cSignataire;
    protected $cnEmetteur;
    protected $ouEmetteur;
    protected $oEmetteur;
    protected $cEmetteur;
    protected $alternativeNameEmetteur;
    protected $qualifieEIDAS;
    protected $formatSignature;
    protected $dateIndicative;
    protected $dateHorodatage;
    protected $jetonHorodatage;
    protected $dateRevocation;

    public function __construct($array = [])
    {
        foreach ($array as $key => $value) {
            $setters = 'set'.ucfirst($key);
            if (method_exists($this, $setters)) {
                $this->$setters($value);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getRepertoiresRevocation()
    {
        return $this->repertoiresRevocation;
    }

    /**
     * @param mixed $repertoiresRevocation
     */
    public function setRepertoiresRevocation($repertoiresRevocation)
    {
        $this->repertoiresRevocation = $repertoiresRevocation;
    }

    /**
     * @return mixed
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param mixed $etat
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
    }

    /**
     * @return mixed
     */
    public function getSignatairePartiel()
    {
        return $this->signatairePartiel;
    }

    /**
     * @param mixed $signatairePartiel
     */
    public function setSignatairePartiel($signatairePartiel)
    {
        $this->signatairePartiel = $signatairePartiel;
    }

    /**
     * @return mixed
     */
    public function getSignataireComplet()
    {
        return $this->signataireComplet;
    }

    /**
     * @param mixed $signataireComplet
     */
    public function setSignataireComplet($signataireComplet)
    {
        $this->signataireComplet = $signataireComplet;
    }

    /**
     * @return mixed
     */
    public function getEmetteur()
    {
        return $this->emetteur;
    }

    /**
     * @param mixed $emetteur
     */
    public function setEmetteur($emetteur)
    {
        $this->emetteur = $emetteur;
    }

    /**
     * @return mixed
     */
    public function getDateValiditeDu()
    {
        return $this->dateValiditeDu;
    }

    /**
     * @param mixed $dateValiditeDu
     */
    public function setDateValiditeDu($dateValiditeDu)
    {
        $this->dateValiditeDu = $dateValiditeDu;
    }

    /**
     * @return mixed
     */
    public function getDateValiditeAu()
    {
        return $this->dateValiditeAu;
    }

    /**
     * @param mixed $dateValiditeAu
     */
    public function setDateValiditeAu($dateValiditeAu)
    {
        $this->dateValiditeAu = $dateValiditeAu;
    }

    /**
     * @return mixed
     */
    public function getPeriodiciteValide()
    {
        return $this->periodiciteValide;
    }

    /**
     * @param mixed $periodiciteValide
     */
    public function setPeriodiciteValide($periodiciteValide)
    {
        $this->periodiciteValide = $periodiciteValide;
    }

    /**
     * @return mixed
     */
    public function getChaineDeCertificationValide()
    {
        return $this->chaineDeCertificationValide;
    }

    /**
     * @param mixed $chaineDeCertificationValide
     */
    public function setChaineDeCertificationValide($chaineDeCertificationValide)
    {
        $this->chaineDeCertificationValide = $chaineDeCertificationValide;
    }

    /**
     * @return mixed
     */
    public function getAbsenceRevocationCRL()
    {
        return $this->absenceRevocationCRL;
    }

    /**
     * @param mixed $absenceRevocationCRL
     */
    public function setAbsenceRevocationCRL($absenceRevocationCRL)
    {
        $this->absenceRevocationCRL = $absenceRevocationCRL;
    }

    /**
     * @return mixed
     */
    public function getDateSignatureValide()
    {
        return $this->dateSignatureValide;
    }

    /**
     * @param mixed $dateSignatureValide
     */
    public function setDateSignatureValide($dateSignatureValide)
    {
        $this->dateSignatureValide = $dateSignatureValide;
    }

    /**
     * @return mixed
     */
    public function getSignatureValide()
    {
        return $this->signatureValide;
    }

    /**
     * @param mixed $signatureValide
     */
    public function setSignatureValide($signatureValide)
    {
        $this->signatureValide = $signatureValide;
    }

    /**
     * @return mixed
     */
    public function getRepertoiresChaineCertification()
    {
        return $this->repertoiresChaineCertification;
    }

    /**
     * @param mixed $repertoiresChaineCertification
     */
    public function setRepertoiresChaineCertification($repertoiresChaineCertification)
    {
        $this->repertoiresChaineCertification = $repertoiresChaineCertification;
    }

    /**
     * @return mixed
     */
    public function getEmetteurDetails()
    {
        return $this->emetteurDetails;
    }

    /**
     * @param mixed $emetteurDetails
     */
    public function setEmetteurDetails($emetteurDetails)
    {
        $emetteurDetails = (array) $emetteurDetails;
        if (is_array($emetteurDetails)) {
            $suffix = 'Emetteur';
            foreach ($emetteurDetails as $key => $value) {
                $setters = 'set'.ucfirst($key).$suffix;
                if (method_exists($this, $setters)) {
                    $this->$setters($value);
                }
            }
        }
        $this->emetteurDetails = $emetteurDetails;
    }

    /**
     * @return mixed
     */
    public function getSignataireDetail()
    {
        return $this->signataireDetail;
    }

    /**
     * @param mixed $signataireDetail
     */
    public function setSignataireDetail($signataireDetail)
    {
        $signataireDetail = (array) $signataireDetail;
        if (is_array($signataireDetail)) {
            $suffix = 'Signataire';
            foreach ($signataireDetail as $key => $value) {
                $setters = 'set'.ucfirst($key).$suffix;
                if (method_exists($this, $setters)) {
                    $this->$setters($value);
                }
            }
        }
        $this->signataireDetail = $signataireDetail;
    }

    /**
     * @return mixed
     */
    public function getCnSignataire()
    {
        return $this->cnSignataire;
    }

    /**
     * @param mixed $cnSignataire
     */
    public function setCnSignataire($cnSignataire)
    {
        $this->cnSignataire = $cnSignataire;
    }

    /**
     * @return mixed
     */
    public function getOuSignataire()
    {
        return $this->ouSignataire;
    }

    /**
     * @param mixed $ouSignataire
     */
    public function setOuSignataire($ouSignataire)
    {
        $this->ouSignataire = $ouSignataire;
    }

    /**
     * @return mixed
     */
    public function getOSignataire()
    {
        return $this->oSignataire;
    }

    /**
     * @param mixed $oSignataire
     */
    public function setOSignataire($oSignataire)
    {
        $this->oSignataire = $oSignataire;
    }

    /**
     * @return mixed
     */
    public function getCSignataire()
    {
        return $this->cSignataire;
    }

    /**
     * @param mixed $cSignataire
     */
    public function setCSignataire($cSignataire)
    {
        $this->cSignataire = $cSignataire;
    }

    /**
     * @return mixed
     */
    public function getCnEmetteur()
    {
        return $this->cnEmetteur;
    }

    /**
     * @param mixed $cnEmetteur
     */
    public function setCnEmetteur($cnEmetteur)
    {
        $this->cnEmetteur = $cnEmetteur;
    }

    /**
     * @return mixed
     */
    public function getOuEmetteur()
    {
        return $this->ouEmetteur;
    }

    /**
     * @param mixed $ouEmetteur
     */
    public function setOuEmetteur($ouEmetteur)
    {
        $this->ouEmetteur = $ouEmetteur;
    }

    /**
     * @return mixed
     */
    public function getOEmetteur()
    {
        return $this->oEmetteur;
    }

    /**
     * @param mixed $oEmetteur
     */
    public function setOEmetteur($oEmetteur)
    {
        $this->oEmetteur = $oEmetteur;
    }

    /**
     * @return mixed
     */
    public function getCEmetteur()
    {
        return $this->cEmetteur;
    }

    /**
     * @param mixed $cEmetteur
     */
    public function setCEmetteur($cEmetteur)
    {
        $this->cEmetteur = $cEmetteur;
    }

    /**
     * @return mixed
     */
    public function getAlternativeNameSignataire()
    {
        return $this->alternativeNameSignataire;
    }

    /**
     * @param mixed $alternativeNameSignataire
     */
    public function setAlternativeNameSignataire($alternativeNameSignataire)
    {
        $this->alternativeNameSignataire = $alternativeNameSignataire;
    }

    /**
     * @return mixed
     */
    public function getAlternativeNameEmetteur()
    {
        return $this->alternativeNameEmetteur;
    }

    /**
     * @param mixed $alternativeNameEmetteur
     */
    public function setAlternativeNameEmetteur($alternativeNameEmetteur)
    {
        $this->alternativeNameEmetteur = $alternativeNameEmetteur;
    }

    /**
     * @return mixed
     */
    public function getQualifieEIDAS()
    {
        return $this->qualifieEIDAS;
    }

    /**
     * @param mixed $qualifieEIDAS
     */
    public function setQualifieEIDAS($qualifieEIDAS)
    {
        $this->qualifieEIDAS = $qualifieEIDAS;
    }

    /**
     * @return mixed
     */
    public function getFormatSignature()
    {
        return $this->formatSignature;
    }

    /**
     * @param mixed $formatSignature
     */
    public function setFormatSignature($formatSignature)
    {
        $this->formatSignature = $formatSignature;
    }

    /**
     * @return mixed
     */
    public function getDateIndicative()
    {
        return $this->dateIndicative;
    }

    /**
     * @param mixed $dateIndicative
     */
    public function setDateIndicative($dateIndicative)
    {
        $this->dateIndicative = $dateIndicative;
    }

    /**
     * @return mixed
     */
    public function getDateHorodatage()
    {
        return $this->dateHorodatage;
    }

    /**
     * @param mixed $dateHorodatage
     */
    public function setDateHorodatage($dateHorodatage)
    {
        $this->dateHorodatage = $dateHorodatage;
    }

    /**
     * @return mixed
     */
    public function getJetonHorodatage()
    {
        return $this->jetonHorodatage;
    }

    /**
     * @param mixed $jetonHorodatage
     */
    public function setJetonHorodatage($jetonHorodatage)
    {
        $this->jetonHorodatage = $jetonHorodatage;
    }

    /**
     * @return mixed
     */
    public function getSignataireDetails()
    {
        return $this->signataireDetails;
    }

    /**
     * @param mixed $signataireDetails
     */
    public function setSignataireDetails($signataireDetails)
    {
        $signataireDetails = (array) $signataireDetails;
        if (is_array($signataireDetails)) {
            $suffix = 'Signataire';
            foreach ($signataireDetails as $key => $value) {
                $setters = 'set'.ucfirst($key).$suffix;
                if (method_exists($this, $setters)) {
                    $this->$setters($value);
                }
            }
        }
        $this->signataireDetails = $signataireDetails;
    }

    /**
     * @param $dateRevocation
     * @return mixed
     */
    public function setDateRevocation($dateRevocation)
    {
        return $this->dateRevocation = $dateRevocation;
    }
    public function getDateRevocation()
    {
        return $this->dateRevocation;
    }
}
