<?php
/**
 * objet Dechiffrement.
 *
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2016
 */

namespace AtexoCrypto\Dto;

use AtexoCrypto\Tool\ValidEnum;

class Dechiffrement
{
    /**
     * @var string
     */
    protected $serveurCryptoURLPublic = '';
    /**
     * @var string
     */
    protected $mpeURLPrivee = '';
    /**
     * @var string
     */
    protected $reference = '';
    /**
     * @var string
     */
    protected $organisme = '';
    /**
     * @var string
     */
    protected $mpeLogin = '';
    /**
     * @var string
     */
    protected $mpePassword = '';
    /**
     * @var string
     */
    protected $mpeFilePath = '';
    /**
     * @var string
     */
    protected $dateLimiteRemisePlis = '';
    /**
     * @var string
     */
    protected $format = '';
    /**
     * @var bool
     */
    protected $enLigne = true;
    /**
     * @var int
     */
    protected $idAgent = '';
    /**
     * @var array
     */
    protected $offres = [];

    /**
     * @return string
     */
    public function getServeurCryptoURLPublic()
    {
        return $this->serveurCryptoURLPublic;
    }

    /**
     * @param string $serveurCryptoURLpublique
     */
    public function setServeurCryptoURLPublic($serveurCryptoURLpublique)
    {
        $this->serveurCryptoURLPublic = $serveurCryptoURLpublique;
    }

    /**
     * @return string
     */
    public function getMpeURLPrivee()
    {
        return $this->mpeURLPrivee;
    }

    /**
     * @param string $mpeURLPrivee
     */
    public function setMpeURLPrivee($mpeURLPrivee)
    {
        $this->mpeURLPrivee = $mpeURLPrivee;
    }

    /**
     * @return string
     */
    public function getMpeLogin()
    {
        return $this->mpeLogin;
    }

    /**
     * @param string $mpeLogin
     */
    public function setMpeLogin($mpeLogin)
    {
        $this->mpeLogin = $mpeLogin;
    }

    /**
     * @return string
     */
    public function getMpePassword()
    {
        return $this->mpePassword;
    }

    /**
     * @param string $mpePassword
     */
    public function setMpePassword($mpePassword)
    {
        $this->mpePassword = $mpePassword;
    }

    /**
     * @return string
     */
    public function getMpeFilePath()
    {
        return $this->mpeFilePath;
    }

    /**
     * @param string $mpeFilePath
     */
    public function setMpeFilePath($mpeFilePath)
    {
        $this->mpeFilePath = $mpeFilePath;
    }

    /**
     * @return int
     */
    public function getIdAgent()
    {
        return $this->idAgent;
    }

    /**
     * @param int $idAgent
     */
    public function setIdAgent($idAgent)
    {
        $this->idAgent = $idAgent;
    }

    /**
     * @return array
     */
    public function getOffres()
    {
        return $this->offres;
    }

    public function setOffres(array $offres)
    {
        $this->offres = $offres;
    }

    /**
     * @param array $offre
     */
    public function addOffre(Offre $offre)
    {
        $this->offres[] = $offre;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param string $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param string $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }

    /**
     * @return bool
     */
    public function getEnLigne()
    {
        return $this->enLigne;
    }

    /**
     * @param bool $enLigne
     */
    public function setEnLigne($enLigne)
    {
        $this->enLigne = $enLigne;
    }

    /**
     * @return string
     */
    public function getDateLimiteRemisePlis()
    {
        return $this->dateLimiteRemisePlis;
    }

    /**
     * @param string $dateLimiteRemisePlis
     */
    public function setDateLimiteRemisePlis($dateLimiteRemisePlis)
    {
        $this->dateLimiteRemisePlis = $dateLimiteRemisePlis;
    }

    /**
     * encodé l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        $serializer = new \AtexoCrypto\Tool\JsonSerializer();

        return $serializer->serialize($this);
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        if (is_array($this->offres) && count($this->offres)) {
            foreach ($this->offres as $offre) {
                if (!$offre instanceof Offre) {
                    return ValidEnum::ERROR_OFFRE;
                }
                $validOffre = $offre->isValid(false);
                if (ValidEnum::VALID != $validOffre) {
                    return $validOffre;
                }
            }
        } else {
            return ValidEnum::ERROR_OFFRE;
        }

        if (empty($this->mpeURLPrivee) || empty($this->mpeLogin) || empty($this->mpePassword) || empty($this->serveurCryptoURLPublic)
            || empty($this->mpeFilePath) || empty($this->format)) {
            return ValidEnum::ERROR_CONFIG;
        }

        return ValidEnum::VALID;
    }
}
