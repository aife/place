<?php
/**
 * objet Signature.
 *
 * @author Anas RHORBAL <anas.rhorbal@atexo.com>
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2016
 */

namespace AtexoCrypto\Dto;

use AtexoCrypto\Tool\ValidEnum;

class VerifPdf extends Signature
{
    protected $pdfPath;

    /**
     * @return string
     */
    public function getPdfPath()
    {
        return $this->pdfPath;
    }

    /**
     * @param string $pdfPath
     */
    public function setPdfPath($pdfPath)
    {
        $this->pdfPath = $pdfPath;
    }

    /**
     * encodé l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        $serializer = new \AtexoCrypto\Tool\JsonSerializer();

        return $serializer->serialize($this);
    }

    public function isValid()
    {
        if (empty($this->pdfPath)) {
            return ValidEnum::ERROR_SIGNATURE;
        }

        if (!file_exists($this->getPdfPath())) {
            return ValidEnum::ERROR_FICHIER_ABSENT;
        }

        if (empty($this->signatureType) || !in_array($this->signatureType, ['XADES', 'PADES', 'CADES', 'INCONNUE'])) {
            return ValidEnum::ERROR_SIGNATURE;
        }

        return ValidEnum::VALID;
    }
}
