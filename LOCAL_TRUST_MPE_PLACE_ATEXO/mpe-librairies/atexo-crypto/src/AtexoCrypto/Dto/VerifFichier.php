<?php
/**
 * objet Signature.
 *
 * @author Anas RHORBAL <anas.rhorbal@atexo.com>
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2016
 */

namespace AtexoCrypto\Dto;

use AtexoCrypto\Tool\ValidEnum;

class VerifFichier extends Signature
{
    protected $signatureBase64;
    protected $mpeFilePath;
    protected $idFileSystem = 0;

    /**
     * @return mixed
     */
    public function getSignatureBase64()
    {
        return $this->signatureBase64;
    }

    /**
     * @param mixed $signatureBase64
     */
    public function setSignatureBase64($signatureBase64)
    {
        $this->signatureBase64 = str_replace(["\r\n", "\n", "\r"], '', $signatureBase64);
    }

    /**
     * @return mixed
     */
    public function getMpeFilePath()
    {
        return $this->mpeFilePath;
    }

    /**
     * @param mixed $mpeFilePath
     */
    public function setMpeFilePath($mpeFilePath)
    {
        $this->mpeFilePath = $mpeFilePath;
    }

    /**
     * @return int
     */
    public function getIdFileSystem()
    {
        return $this->idFileSystem;
    }

    /**
     * @param int $idFileSystem
     */
    public function setIdFileSystem($idFileSystem)
    {
        $this->idFileSystem = $idFileSystem;
    }

    /**
     * encodé l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        $serializer = new \AtexoCrypto\Tool\JsonSerializer();

        return $serializer->serialize($this);
    }

    public function isValid()
    {
        if (empty($this->signatureBase64) || empty($this->mpeFilePath) || 0 == $this->idFileSystem) {
            return ValidEnum::ERROR_SIGNATURE;
        }

        if (empty($this->signatureType) || !in_array($this->signatureType, ['XADES', 'PADES', 'CADES', 'INCONNUE'])) {
            return ValidEnum::ERROR_SIGNATURE;
        }

        return ValidEnum::VALID;
    }
}
