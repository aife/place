<?php

/**
 * objet Enveloppe.
 *
 * @author Amal EL BEKKAOUI <aamal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2016
 */

namespace AtexoCrypto\Dto;

use AtexoCrypto\Tool\ValidEnum;

class Enveloppe
{
    protected $idEnveloppe;
    protected $type;
    protected $fichiers = [];

    /**
     * @return mixed
     */
    public function getIdEnveloppe()
    {
        return $this->idEnveloppe;
    }

    /**
     * @param mixed $idEnveloppe
     */
    public function setIdEnveloppe($idEnveloppe)
    {
        $this->idEnveloppe = $idEnveloppe;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $typeEnveloppe
     */
    public function setType($typeEnveloppe)
    {
        $this->type = $typeEnveloppe;
    }

    /**
     * @return mixed
     */
    public function getFichiers()
    {
        return $this->fichiers;
    }

    /**
     * @param mixed $fichiers
     */
    public function setFichiers(array $fichiers)
    {
        $this->fichiers = $fichiers;
    }

    /**
     * @param mixed $fichiers
     */
    public function addFichier(Fichier $fichier)
    {
        $this->fichiers[] = $fichier;
    }

    /**
     * encodé l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        $serializer = new \AtexoCrypto\Tool\JsonSerializer();

        return $serializer->serialize($this);
    }

    /**
     * @return bool
     */
    public function isValid($chiffrement = true)
    {
        if (empty($this->idEnveloppe)) {
            return ValidEnum::ERROR_ENVELOPPE;
        }

        if ($chiffrement) {
            if (is_array($this->fichiers) && count($this->fichiers)) {
                foreach ($this->fichiers as $fichier) {
                    if (!$fichier instanceof Fichier) {
                        return ValidEnum::ERROR_FICHIER;
                    }
                    $validFichier = $fichier->isValid();
                    if (ValidEnum::VALID != $validFichier) {
                        return $validFichier;
                    }
                }
            } else {
                return ValidEnum::ERROR_FICHIER;
            }
        }

        return ValidEnum::VALID;
    }
}
