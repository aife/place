<?php
/**
 * objet Signature.
 *
 * @author Anas RHORBAL <anas.rhorbal@atexo.com>
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2016
 */

namespace AtexoCrypto\Dto;

use AtexoCrypto\Tool\ValidEnum;

class VerifHash extends Signature
{
    protected $hashes = [];
    protected $signatureBase64;

    /**
     * @return array
     */
    public function getHashes()
    {
        return $this->hashes;
    }

    /**
     * @param array $hashes
     */
    public function setHashes($hashes)
    {
        $this->hashes = $hashes;
    }

    /**
     * @param array $hashes
     */
    public function addHash($hash)
    {
        $this->hashes[] = $hash;
    }

    /**
     * @return mixed
     */
    public function getSignatureBase64()
    {
        return $this->signatureBase64;
    }

    /**
     * @param mixed $signatureBase64
     */
    public function setSignatureBase64($signatureBase64)
    {
        $this->signatureBase64 = str_replace(["\r\n", "\n", "\r"], '', $signatureBase64);
    }

    /**
     * encodé l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        $serializer = new \AtexoCrypto\Tool\JsonSerializer();

        return $serializer->serialize($this);
    }

    public function isValid()
    {
        if (empty($this->signatureBase64)) {
            return ValidEnum::ERROR_SIGNATURE;
        }
        if (empty($this->hashes)) {
            return ValidEnum::ERROR_SIGNATURE;
        }

        if (empty($this->signatureType) || !in_array($this->signatureType, ['XADES', 'PADES', 'CADES', 'INCONNUE'])) {
            return ValidEnum::ERROR_SIGNATURE;
        }

        return ValidEnum::VALID;
    }
}
