<?php

/**
 * objet Enveloppe.
 *
 * @author Amal EL BEKKAOUI <aamal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2016
 */

namespace AtexoCrypto\Dto;

class EnveloppeAlloti extends Enveloppe
{
    protected $numeroLot;

    /**
     * @return mixed
     */
    public function getNumeroLot()
    {
        return $this->numeroLot;
    }

    /**
     * @param mixed $numeroLot
     */
    public function setNumeroLot($numeroLot)
    {
        $this->numeroLot = $numeroLot;
    }
}
