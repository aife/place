<?php
/**
 * objet Offre.
 *
 * @author Amal EL BEKKAOUI <aamal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2016
 */

namespace AtexoCrypto\Dto;

use AtexoCrypto\Tool\ValidEnum;

class Offre
{
    /**
     * @var string
     */
    protected $idOffre = '';
    /**
     * @var string
     */
    protected $reponseAnnonceBase64 = '';
    /**
     * @var array
     */
    protected $certificats = [];
    /**
     * @var array
     */
    protected $enveloppes = [];
    /**
     * @var string
     */
    protected $jetonHorodatage;

    /**
     * @var string
     */
    protected $hashFichiersOffre;

    /**
     * gete id Offre.
     *
     * @return int id Offre
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since develop
     *
     * @copyright Atexo 2016
     */
    public function getIdOffre()
    {
        return $this->idOffre;
    }

    /**
     * set id Offre.
     *
     * @param int $idOffre
     *
     * @return void
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since develop
     *
     * @copyright Atexo 2016
     */
    public function setIdOffre($idOffre)
    {
        $this->idOffre = $idOffre;
    }

    /**
     * @return array
     */
    public function getCertificats()
    {
        return $this->certificats;
    }

    /**
     * @param mixed $certificates
     */
    public function setCertificates(array $certificates)
    {
        $this->certificates = $certificates;
    }

    /**
     * @param mixed $certificates
     */
    public function addCertificate(Certificat $certificate)
    {
        $this->certificats[] = $certificate;
    }

    /**
     * @return mixed
     */
    public function getEnveloppes()
    {
        return $this->enveloppes;
    }

    /**
     * @param mixed $enveloppes
     */
    public function setEnveloppes(array $enveloppes)
    {
        $this->enveloppes = $enveloppes;
    }

    /**
     * @param mixed $enveloppes
     */
    public function addEnveloppe(Enveloppe $enveloppe)
    {
        $this->enveloppes[] = $enveloppe;
    }

    /**
     * @return string
     */
    public function getReponseAnnonceBase64()
    {
        return $this->reponseAnnonceBase64;
    }

    /**
     * @param string $reponseAnnonceBase64
     */
    public function setReponseAnnonceBase64($reponseAnnonceBase64)
    {
        $this->reponseAnnonceBase64 = str_replace(["\r\n", "\n", "\r"], '', $reponseAnnonceBase64);
    }

    /**
     * @return string
     */
    public function getJetonHorodatage()
    {
        return $this->jetonHorodatage;
    }

    /**
     * @param string $jetonHorodatage
     */
    public function setJetonHorodatage($jetonHorodatage)
    {
        $this->jetonHorodatage = $jetonHorodatage;
    }

    /**
     * @return string
     */
    public function getHashFichiersOffre()
    {
        return $this->hashFichiersOffre;
    }

    /**
     * @param string $hashFichiersOffre
     */
    public function setHashFichiersOffre($hashFichiersOffre)
    {
        $this->hashFichiersOffre = $hashFichiersOffre;
    }

    /**
     * @return mixed
     */
    private function getTypeAllEnveloppes()
    {
        $types = [];
        if (is_array($this->enveloppes) && count($this->enveloppes)) {
            foreach ($this->enveloppes as $enveloppe) {
                $types[$enveloppe->getType()] = $enveloppe->getType();
            }
        }
    }

    /**
     * encodé l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        $serializer = new \AtexoCrypto\Tool\JsonSerializer();

        return $serializer->serialize($this);
    }

    /**
     * @param bool|true $chiffrement
     *
     * @return bool
     */
    public function isValid($chiffrement = true, $withCertificat = true)
    {
        if (is_array($this->enveloppes) && count($this->enveloppes)) {
            foreach ($this->enveloppes as $enveloppe) {
                if (!$enveloppe instanceof Enveloppe) {
                    return ValidEnum::ERROR_ENVELOPPE;
                }
                $validEnveloppe = $enveloppe->isValid($chiffrement);
                if (ValidEnum::VALID != $validEnveloppe) {
                    return $validEnveloppe;
                }
            }
        } else {
            return ValidEnum::ERROR_ENVELOPPE;
        }

        if ($chiffrement && $withCertificat) {
            if (is_array($this->certificats) && count($this->certificats)) {
                $typeEnvCertificat = [];
                foreach ($this->certificats as $certificat) {
                    if (!$certificat instanceof Certificat) {
                        return ValidEnum::ERROR_CERTIFICAT;
                    }
                    $validCertificat = $certificat->isValid();
                    if (ValidEnum::VALID != $validCertificat) {
                        return $validCertificat;
                    }

                    $typeEnvCertificat[$certificat->getTypeEnveloppe()] = $certificat->getTypeEnveloppe();
                }
                if ((is_array($typeEnvCertificat) && is_array($this->getTypeAllEnveloppes()))
                    && (count($typeEnvCertificat) < count($this->getTypeAllEnveloppes()))
                ) {
                    return ValidEnum::ERROR_CERTIFICAT;
                }
            } else {
                return ValidEnum::ERROR_CERTIFICAT;
            }
        }

        if (empty($this->idOffre)) {
            return ValidEnum::ERROR_OFFRE;
        }

        return ValidEnum::VALID;
    }
}
