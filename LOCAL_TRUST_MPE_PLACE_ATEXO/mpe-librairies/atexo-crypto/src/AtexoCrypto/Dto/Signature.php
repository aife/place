<?php
/**
 * objet Signature.
 *
 * @author Anas RHORBAL <anas.rhorbal@atexo.com>
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2016
 */

namespace AtexoCrypto\Dto;

class Signature
{
    protected $signatureType;

    /**
     * @return mixed
     */
    public function getSignatureType()
    {
        return $this->signatureType;
    }

    /**
     * @param mixed $signatureType
     */
    public function setSignatureType($signatureType)
    {
        $this->signatureType = $signatureType;
    }
}
