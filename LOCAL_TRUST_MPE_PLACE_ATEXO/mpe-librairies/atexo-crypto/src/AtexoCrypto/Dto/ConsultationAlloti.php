<?php
/**
 * objet Consultation.
 *
 * @author Amal EL BEKKAOUI <aamal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2016
 */

namespace AtexoCrypto\Dto;

class ConsultationAlloti extends Consultation
{
    protected $nombreLots = 0;

    /**
     * @return int
     */
    public function getNombreLots()
    {
        return $this->nombreLots;
    }

    /**
     * @param int $nombreLots
     */
    public function setNombreLots($nombreLots)
    {
        $this->nombreLots = $nombreLots;
    }

    /**
     * encodé l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        $serializer = new \AtexoCrypto\Tool\JsonSerializer();

        return $serializer->serialize($this);
    }
}
