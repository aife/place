<?php
/**
 * objet Signature.
 *
 * @author Anas RHORBAL <anas.rhorbal@atexo.com>
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2016
 */

namespace AtexoCrypto\Dto;

class InfosHorodatage
{
    protected $code;
    protected $message;
    protected $horodatage;
    protected $jetonHorodatageBase64;
    protected $hashFichiersOffre;

    public function __construct($array = [])
    {
        foreach ($array as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getHorodatage()
    {
        return $this->horodatage;
    }

    /**
     * @param mixed $horodatage
     */
    public function setHorodatage($horodatage)
    {
        $this->horodatage = $horodatage;
    }

    /**
     * @return mixed
     */
    public function getJetonHorodatageBase64()
    {
        return $this->jetonHorodatageBase64;
    }

    /**
     * @param mixed $jetonHorodatageBase64
     */
    public function setJetonHorodatageBase64($jetonHorodatageBase64)
    {
        $this->jetonHorodatageBase64 = $jetonHorodatageBase64;
    }

    /**
     * @return mixed
     */
    public function getHashFichiersOffre()
    {
        return $this->hashFichiersOffre;
    }

    /**
     * @param mixed $hashFichiersOffre
     */
    public function setHashFichiersOffre($hashFichiersOffre)
    {
        $this->hashFichiersOffre = $hashFichiersOffre;
    }
}
