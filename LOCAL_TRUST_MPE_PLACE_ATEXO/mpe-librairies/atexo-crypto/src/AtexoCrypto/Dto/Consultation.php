<?php
/**
 * objet Consultation.
 *
 * @author Amal EL BEKKAOUI <aamal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2016
 */

namespace AtexoCrypto\Dto;

use AtexoCrypto\Tool\ValidEnum;

class Consultation
{
    /**
     * @var string
     */
    protected $reference = '';
    /**
     * @var int
     */
    protected $id = 0;
    /**
     * @var string
     */
    protected $organisme = '';
    /**
     * @var string
     */
    protected $description = '';
    /**
     * @var bool
     */
    protected $chiffrement = '';
    /**
     * @var bool
     */
    protected $alloti = '';
    /**
     * @var string
     */
    protected $intitule = '';
    /**
     * @var string
     */
    protected $objet = '';
    /**
     * @var int
     */
    protected $nombreLots = 0;

    /**
     * @var bool
     * @description : Consultation chiffrée en mode PKCS#7 (Cryptographic Message Syntax)
     */
    protected $cmsActif = false;

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param string $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function getChiffrement()
    {
        return $this->chiffrement;
    }

    /**
     * @param bool $chiffrement
     */
    public function setChiffrement($chiffrement)
    {
        $this->chiffrement = $chiffrement;
    }

    /**
     * @return bool
     */
    public function getAlloti()
    {
        return $this->alloti;
    }

    /**
     * @param bool $alloti
     */
    public function setAlloti($alloti)
    {
        $this->alloti = $alloti;
    }

    /**
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * @param string $intitule
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;
    }

    /**
     * @return string
     */
    public function getObjet()
    {
        return $this->objet;
    }

    /**
     * @param string $objet
     */
    public function setObjet($objet)
    {
        $this->objet = $objet;
    }

    /**
     * @return int
     */
    public function getNombreLots()
    {
        return $this->nombreLots;
    }

    /**
     * @param int $nombreLots
     */
    public function setNombreLots($nombreLots)
    {
        $this->nombreLots = $nombreLots;
    }

    /**
     * @param $cmsActif
     * @return Consultation
     */
    public function setCmsActif($cmsActif): Consultation
    {
        $this->cmsActif = $cmsActif;
        return $this;
    }

    /**
     * @description : Méthode qui nous retourne si la consultation est du type PKCS#7 (Chiffrement)
     * @return bool
     */
    public function getCmsActif(): bool
    {
        return $this->cmsActif;
    }

    /**
     * encodé l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        $serializer = new \AtexoCrypto\Tool\JsonSerializer();

        return $serializer->serialize($this);
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        $isValid = ValidEnum::VALID;
        if (empty($this->id) || empty($this->organisme) || is_null($this->alloti)) {
            $isValid = ValidEnum::ERROR_CONSULTATION;
        }

        return $isValid;
    }
}
