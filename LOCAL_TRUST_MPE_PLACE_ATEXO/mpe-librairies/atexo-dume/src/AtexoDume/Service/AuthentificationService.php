<?php
/**
 * Created by PhpStorm.
 * User: MWA <mohamed.wazni@atexo.com>
 * Date: 06/07/2018
 * Time: 10:45.
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2018
 */

namespace AtexoDume\Service;

use AtexoDume\Client\Client;
use AtexoDume\Dto\ReponseLoginDume;
use AtexoDume\Exception\RestRequestException;
use AtexoDume\Exception\ServiceException;
use AtexoDume\Tool\ValidEnum;

/**
 * Class AuthentificationService.
 */
class AuthentificationService
{
    protected $service;
    protected $restUrl;
    protected $logger = null;

    const TYPE_DUME_ACHETEUR = 'acheteur';
    const TYPE_DUME_OE = 'oe';

    /**
     * AuthentificationService constructor.
     */
    public function __construct(Client &$client)
    {
        $this->service = $client->getService();
        $this->restUrl = $client->getURL();
        $this->logger = $client->getLogger();
    }

    /**
     * Méthode qui permet de s'authentifier auprès du WS LT_DUME et d'obtenir un tokenIdentification
     * pour gérer la sécurisation des web services.
     * Cette méthode retourne le token d'identification à utiliser avec les requêtes si l'authentification s'est bien déroulée
     * Autrement, elle lève une exception de type ServiceException.
     *
     * @param string $login
     * @param string $password
     *
     * @return ReponseLoginDume tokenIdentification
     *
     * @throws ServiceException
     */
    public function login($login = null, $password = null, $dumeType = null, $idContexteDume = null)
    {
        $url = $this->restUrl.ValidEnum::URL_DUME_API.'/oauth/token?grant_type=client_credentials';

        if (null !== $dumeType && null !== $idContexteDume) {
            $url .= '&'.((self::TYPE_DUME_ACHETEUR === $dumeType) ? 'idContextDumeAcheteur' : 'idContextDumeOE').'='.$idContexteDume;
        }

        if ($this->logger) {
            $this->logger->info('Api Dume : Service Authentification : login: url : '.$url);
        }

        if (null !== $login && null !== $password) {
            try {
                $response = $this->service->prepAndSend(
                    $url,
                    [201, 200],
                    'POST',
                    null,
                    true,
                    'application/json',
                    'application/json',
                    ['authorization: Basic '.base64_encode("$login:$password")]
                );
            } catch (RestRequestException $e) {
                if ($this->logger) {
                    $this->logger->error('Api Dume : Service Authentification :  login Dume : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
                }
                throw new ServiceException(ServiceException::ERROR_REST, ValidEnum::VALID, $e->getMessage());
            }

            $result = json_decode($response, true);

            if (count($result) > 0) {
                if ($this->logger) {
                    $this->logger->info('Api Dume : Service Service Authentification :  login Dume : resultat : '.$response);
                }

                return new ReponseLoginDume($result);
            }
        } else {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Authentification : login Dume : Erreur, login ou mot de passe NULL');
            }
            throw new ServiceException(ServiceException::ERROR_AUTHENTIFICATION_DUME, ValidEnum::ERROR_LOGIN_PWD_DUME, 'Invalid Value');
        }
    }
}
