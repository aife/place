<?php

namespace AtexoDume\Service;

use AtexoDume\Client\Client;
use AtexoDume\Exception\RestRequestException;
use AtexoDume\Exception\ServiceException;
use AtexoDume\Tool\ValidEnum;

/**
 * Class DonneesEssentiellesService.
 */
class DonneesEssentiellesService extends AuthentificationService
{
    protected $reponseLoginDume;

    /**
     * DonneesEssentiellesService constructor.
     *
     * @throws ServiceException
     */
    public function __construct(Client &$client)
    {
        parent::__construct($client);
        $this->reponseLoginDume = self::login($client->getUsername(), $client->getPassword());
    }

    /**
     * Méthode qui permet de connaitre le statut de publication d'un contrat.
     * Cette méthode retourne xxx si l'opération s'est bien déroulée.
     * Autrement, elle lève une exception de type ServiceException.
     *
     * @param $idContrat
     * @param $nbModification
     *
     * @return mixed
     *
     * @throws ServiceException
     */
    public function getStatutPublication($idContrat, $nbModification, $uuid)
    {
        $url = $this->restUrl.ValidEnum::URL_DUME_API.'/donneesEssentielles/statutContrat/';
        $url .= $idContrat.'/'.$nbModification.'/'.$uuid;

        if ($this->logger) {
            $this->logger->info('Api Dume : Service Données Essentielles : getStatutPublication : url : '.$url);
        }

        $json = json_encode([
            'contratId' => $idContrat,
            'updateNumber' => $nbModification,
            'uuid' => $uuid,
        ]);

        if ($this->logger) {
            $this->logger->info('Api Dume : Service Données Essentielles : getStatutPublication : json : '.$json);
        }

        try {
            $response = $this->service->prepAndSend(
                $url,
                [201, 200],
                'GET',
                null,
                true,
                'application/json',
                'application/json',
                ['Authorization: Bearer '.$this->reponseLoginDume->getAccessToken()]
            );
        } catch (RestRequestException $e) {
            if (404 !== $e->statusCode) {
                if ($this->logger) {
                    $this->logger->error('Api Dume : Service Données Essentielles : getStatutPublication : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
                }
            }
            throw new ServiceException(ServiceException::ERROR_REST, $e->statusCode, $e->getMessage());
        }

        if (!empty($response)) {
            if ($this->logger) {
                $this->logger->info('Api Dume : Service Données Essentielles : getStatutPublication : resultat : '.$response);
            }

            return json_decode($response, true);
        }

        if ($this->logger) {
            $this->logger->error('Api Dume : Service Données Essentielles : getStatutPublication : Erreur : '.$response);
        }

        throw new ServiceException(ServiceException::ERROR_STATUT_PUBLICATION_DONNEES_ESSENTIELLES, 500, $response);
    }
}
