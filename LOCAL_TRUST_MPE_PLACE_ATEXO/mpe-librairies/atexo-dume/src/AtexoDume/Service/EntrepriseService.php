<?php

namespace AtexoDume\Service;

use AtexoDume\Client\Client;
use AtexoDume\Dto\ReponseListPublishedDume;
use AtexoDume\Dto\ReponseValidPubDume;
use AtexoDume\Exception\RestRequestException;
use AtexoDume\Exception\ServiceException;
use AtexoDume\Tool\ValidEnum;

/**
 * Class EntrepriseService.
 */
class EntrepriseService extends AuthentificationService
{
    const CREATE = 'create';
    const UPDATE = 'update';
    const VALIDATE = 'validate';
    const PUBLISH = 'publish';

    protected $reponseLoginDume;

    public function __construct(Client &$client)
    {
        parent::__construct($client);
        $this->reponseLoginDume = self::login($client->getUsername(), $client->getPassword());
    }

    /**
     * Méthode qui permet de créer un Dume Entreprise.
     * Cette méthode retourne l'identifiant du contexte dume opérateur économique si l'opération s'est bien déroulée.
     * Autrement, elle lève une exception de type ServiceException.
     *
     * @param \AtexoDume\Dto\Entreprise\Consultation $consultation
     *
     * @return int IdContextDumeEntreprise
     *
     * @throws ServiceException
     */
    public function createDume($consultation, $idContextDumeEntreprise = 0, $numSn = null)
    {
        return $this->createOrUpdateDume($consultation, self::CREATE, $idContextDumeEntreprise, $numSn);
    }

    /**
     * Méthode qui permet de mettre à jour un Dume Entreprise existant.
     * Cette méthode retourne l'identifiant du contexte dume Entreprise si l'opération s'est bien déroulée.
     * Autrement, elle lève une exception de type ServiceException.
     *
     * @param \AtexoDume\Dto\Entreprise\Consultation $consultation
     * @param int                                    $idContextDumeEntreprise
     *
     * @return int IdContextDumeEntreprise
     *
     * @throws ServiceException
     */
    public function updateDume($consultation, $idContextDumeEntreprise = 0)
    {
        return $this->createOrUpdateDume($consultation, self::UPDATE, $idContextDumeEntreprise);
    }

    private function createOrUpdateDume(
        $consultation,
        $methodName,
        $idContextDumeEntreprise = 0,
        $secondDepot = null
    ) {
        if (self::CREATE == $methodName) {
            $url = $this->restUrl.ValidEnum::URL_DUME_API.'/operateurEconomique/create';
            if (!is_null($secondDepot)) {
                $url = $url.'/'.$idContextDumeEntreprise.'/'.$secondDepot;
            }
        } elseif ((self::UPDATE == $methodName) && null != $idContextDumeEntreprise) {
            $url = $this->restUrl.ValidEnum::URL_DUME_API.'/operateurEconomique/updateMetadonnee/'.$idContextDumeEntreprise;
        } else {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Entreprise : '.$methodName.'Dume : Erreur idContextDumeEntreprise : valeur non autorisée : '.$idContextDumeEntreprise);
            }
            throw new ServiceException(ServiceException::ERROR_UPDATE_DUME_ENTREPRISE, ValidEnum::ERROR_ID_CTX_DUME_ENTREPRISE, 'Invalid Value');
        }

        if ($this->logger) {
            $this->logger->info('Api Dume : Service Entreprise : '.$methodName.'Dume: url : '.$url);
        }

        $isValid = $consultation->isValid();
        $json = $consultation->toJSON();

        if (ValidEnum::VALID == $isValid) {
            if ($this->logger) {
                $this->logger->info('Api Dume : Service Entreprise : '.$methodName.'Dume : json : '.$json);
            }

            try {
                $response = $this->service->prepAndSend($url, [201, 200], 'POST', $json, true, 'application/json', 'application/json', ['Authorization: Bearer '.$this->reponseLoginDume->getAccessToken()]);
            } catch (RestRequestException $e) {
                if ($this->logger) {
                    $this->logger->error('Api Dume : Service Entreprise : '.$methodName.'Dume : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
                }
                throw new ServiceException(ServiceException::ERROR_REST, $isValid, $e->getMessage());
            }

            if (!empty($response)) {
                if ($this->logger) {
                    $this->logger->info('Api Dume : Service Entreprise : '.$methodName.'Dume : resultat : '.$response);
                }

                return $response;
            }

            if ($this->logger) {
                $this->logger->error('Api Dume : Service Entreprise : '.$methodName.'Dume : Erreur : '.$response);
            }
            if (self::CREATE == $methodName) {
                throw new ServiceException(ServiceException::ERROR_CREATION_DUME_ENTREPRISE, $isValid, $response);
            }
            if (self::UPDATE == $methodName) {
                throw new ServiceException(ServiceException::ERROR_UPDATE_DUME_ENTREPRISE, $isValid, $response);
            }
        } else {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Entreprise : '.$methodName.'Dume : Erreur Json : CODE : '.$isValid.' Objet : '.$json);
            }
            throw new ServiceException(ServiceException::ERROR_JSON, $isValid, 'Invalid Object');
        }
    }

    /**
     * Méthode qui permet de demander la validation d'un Dume Opérateur Economique existant.
     * Cette méthode retourne un objet de type Reponse si le service LT-DUME a été sollicité.
     * Autrement, elle lève une exception de type ServiceException.
     *
     * @param int $idContexteDumeOperateurEconomique
     *
     * @return ReponseValidPubDume l'objet Reponse contenant le résultat du WS LT DUME distant
     *
     * @throws ServiceException
     */
    public function validateDume($idContexteDumeOperateurEconomique = 0)
    {
        return $this->validateOrPublishDume(self::VALIDATE, $idContexteDumeOperateurEconomique);
    }

    /**
     * Méthode qui permet de demander la publication d'un Dume Opérateur Economique existant.
     * Cette méthode retourne un objet de type Reponse si le service LT-DUME a été sollicité.
     * Autrement, elle lève une exception de type ServiceException.
     *
     * @param int $idContextDumeAcheteur
     *
     * @return ReponseValidPubDume l'objet Reponse contenant le résultat du WS LT DUME distant
     *
     * @throws ServiceException
     */
    public function publishDume($idContexteDumeOperateurEconomique = 0)
    {
        return $this->validateOrPublishDume(self::PUBLISH, $idContexteDumeOperateurEconomique);
    }

    /**
     * @param string $methodName
     * @param int    $idContexteDumeOperateurEconomique
     *
     * @return ReponseValidPubDume
     *
     * @throws ServiceException
     */
    private function validateOrPublishDume($methodName, $idContexteDumeOperateurEconomique = 0)
    {
        if ((self::VALIDATE == $methodName) && null != $idContexteDumeOperateurEconomique) {
            $url = $this->restUrl.ValidEnum::URL_DUME_API.'/operateurEconomique/validation/'.$idContexteDumeOperateurEconomique;
        } elseif ((self::PUBLISH == $methodName) && null != $idContexteDumeOperateurEconomique) {
            $url = $this->restUrl.ValidEnum::URL_DUME_API.'/operateurEconomique/publication/'.$idContexteDumeOperateurEconomique;
        } else {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Entreprise : '.$methodName.'Dume : Erreur idContexteDumeOperateurEconomique : valeur non autorisée : '.$idContexteDumeOperateurEconomique);
            }
            throw new ServiceException(ServiceException::ERROR_VALIDATION_DUME_ENTREPRISE, ValidEnum::ERROR_ID_CTX_DUME_ENTREPRISE, 'Invalid Value');
        }

        if ($this->logger) {
            $this->logger->info('Api Dume : Service Entreprise : '.$methodName.'Dume: url : '.$url);
        }

        try {
            $response = $this->service->prepAndSend($url, [201, 200], 'GET', null, true, 'application/json', 'application/json', ['Authorization: Bearer '.$this->reponseLoginDume->getAccessToken()]);
        } catch (RestRequestException $e) {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Entreprise : '.$methodName.'Dume : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
            }
            throw new ServiceException(ServiceException::ERROR_REST, ValidEnum::VALID, $e->getMessage());
        }

        $result = json_decode($response, true);

        if (count($result) > 0) {
            if ($this->logger) {
                $this->logger->info('Api Dume : Service Entreprise : '.$methodName.'Dume : resultat : '.$response);
            }

            return new ReponseValidPubDume($result);
        }

        if (self::VALIDATE == $methodName) {
            throw new ServiceException(ServiceException::ERROR_VALIDATION_DUME_ENTREPRISE, ValidEnum::VALID, $response);
        }
        if (self::PUBLISH == $methodName) {
            throw new ServiceException(ServiceException::ERROR_PUBLICATION_DUME_ENTREPRISE, ValidEnum::VALID, $response);
        }
    }

    /**
     * Méthode qui permet de récupérer la liste des dumes Entreprises publiés pour le contexte dume Entreprise passé en paramètre
     * Cette méthode retourne un objet de type Reponse si le service LT-DUME a été sollicité.
     * Autrement, elle lève une exception de type ServiceException.
     *
     * @param int $idContexteDumeOperateurEconomique
     *
     * @return ReponseListPublishedDume l'objet ReponseListPublishedDume contenant le résultat du WS LT DUME distant
     *
     * @throws ServiceException
     */
    public function getPublishedDume($idContexteDumeOperateurEconomique = 0)
    {
        if (null != $idContexteDumeOperateurEconomique) {
            $url = $this->restUrl.ValidEnum::URL_DUME_API.'/operateurEconomique/publication/list/'.$idContexteDumeOperateurEconomique;
        } else {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Entreprise : getPublishedDume : Erreur idContexteDumeOperateurEconomique : valeur non autorisée : '.$idContexteDumeOperateurEconomique);
            }
            throw new ServiceException(ServiceException::ERROR_LIST_PUBLISHED_DUME_ENTREPRISE, ValidEnum::ERROR_ID_CTX_DUME_ENTREPRISE, 'Invalid Value');
        }

        if ($this->logger) {
            $this->logger->info('Api Dume : Service Entreprise : getPublishedDume: url : '.$url);
        }

        try {
            $response = $this->service->prepAndSend($url, [201, 200], 'GET', null, true, 'application/json', 'application/json', ['Authorization: Bearer '.$this->reponseLoginDume->getAccessToken()]);
        } catch (RestRequestException $e) {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Entreprise : getPublishedDume : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
            }
            throw new ServiceException(ServiceException::ERROR_REST, ValidEnum::VALID, $e->getMessage());
        }

        $result = json_decode($response, true);

        if (count($result) > 0) {
            if ($this->logger) {
                $this->logger->info('Api Dume : Service Entreprise : getPublishedDume : resultat : '.$response);
            }

            return new ReponseListPublishedDume($result);
        } else {
            throw new ServiceException(ServiceException::ERROR_LIST_PUBLISHED_DUME_ENTREPRISE, ValidEnum::VALID, $response);
        }
    }

    /**
     * Méthode qui permet de récupérer le fichier PDF Dume Entreprise pour un numéro de Dume publié
     * Cette méthode retourne un objet de type Reponse si le service LT-DUME a été sollicité.
     * Autrement, elle lève une exception de type ServiceException.
     *
     * @param int $numeroDume
     *
     * @return string le contenu du fichier PDF contenant le résultat du WS LT DUME distant
     *
     * @throws ServiceException
     */
    public function getPdf($numeroDume = 0)
    {
        if (null != $numeroDume) {
            $url = $this->restUrl.ValidEnum::URL_DUME_API.'/operateurEconomique/pdf/'.$numeroDume;
        } else {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Entreprise : getPdf : Erreur idContexteDumeOperateurEconomique : valeur non autorisée : '.$numeroDume);
            }
            throw new ServiceException(ServiceException::ERROR_GET_PDF_DUME_ENTREPRISE, ValidEnum::ERROR_ID_CTX_DUME_ACHETEUR, 'Invalid Value');
        }

        if ($this->logger) {
            $this->logger->info('Api Dume : Service Entreprise : getPdf: url : '.$url);
        }

        try {
            $response = $this->service->prepAndSend($url, [201, 200], 'GET', null, true, 'application/json', 'application/pdf', ['Authorization: Bearer '.$this->reponseLoginDume->getAccessToken()]);
        } catch (RestRequestException $e) {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Entreprise : getPdf : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
            }
            throw new ServiceException(ServiceException::ERROR_REST, ValidEnum::VALID, $e->getMessage());
        }

        if (!empty($response)) {
            if ($this->logger) {
                $this->logger->info('Api Dume : Service Entreprise : getPdf: resultat reçu ');
            }

            return $response;
        } else {
            throw new ServiceException(ServiceException::ERROR_GET_PDF_DUME_ENTREPRISE, ValidEnum::VALID, 'Reponse Vide');
        }
    }

    /**
     * Méthode qui permet de vérifier si le SN est down et vérifier si le dume acheteur est publié.
     *
     * @param int $idContexteDumeOe
     *
     * @return ReponseValidPubDume
     *
     * @throws ServiceException
     */
    public function checkAccess($idContexteDumeOe = 0)
    {
        if (null != $idContexteDumeOe) {
            $url = $this->restUrl.ValidEnum::URL_DUME_API.'/operateurEconomique/checkAccess/'.$idContexteDumeOe;
        } else {
            if ($this->logger) {
                $this->logger->error("Api Dume : Service Entreprise : checkAccess : Erreur $idContexteDumeOe : valeur non autorisée : ".$idContexteDumeOe);
            }

            throw new ServiceException(ServiceException::ERROR_CHECK_ACCESS_DUME_ENTREPRISE, ValidEnum::ERROR_ID_CTX_DUME_ENTREPRISE, 'Invalid Value');
        }

        if ($this->logger) {
            $this->logger->info('Api Dume : Service Entreprise : checkAccess : url : '.$url);
        }

        try {
            $response = $this->service->prepAndSend(
                $url,
                [201, 200],
                'GET',
                null,
                true,
                'application/json',
                'application/json',
                ['Authorization: Bearer '.$this->reponseLoginDume->getAccessToken()]
            );
        } catch (\Exception $e) {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Entreprise : checkAccess : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
            }

            throw new ServiceException(ServiceException::ERROR_REST, ValidEnum::VALID, $e->getMessage());
        }

        $result = json_decode($response, true);

        if (!empty($result)) {
            if ($this->logger) {
                $this->logger->info('Api Dume : Service Entreprise : checkAccess: resultat reçu ');

                if (isset($result['statut'])) {
                    $this->logger->info('Api Dume : Service Entreprise : checkAccess: status : '.$result['statut']);

                    if ('SN_DOWN' == $result['statut']) {
                        throw new ServiceException(ServiceException::ERROR_CHECK_ACCESS_DUME_ENTREPRISE, ValidEnum::VALID, 'Reponse Vide');
                    }
                }
            }

            return new ReponseValidPubDume($result);
        } else {
            throw new ServiceException(ServiceException::ERROR_CHECK_ACCESS_DUME_ENTREPRISE, ValidEnum::VALID, 'Reponse Vide');
        }
    }

    /**
     * Retourne un access token à utiliser côté navigateur.
     *
     * @param Client $client
     * @param $idContexteDume
     *
     * @return string
     *
     * @throws ServiceException
     */
    public function getClientAccessToken($client, $idContexteDume)
    {
        $response = self::login(
            $client->getUsername(),
            $client->getPassword(),
            parent::TYPE_DUME_OE,
            $idContexteDume
        );

        return $response->getAccessToken();
    }

    /**
     * Méthode qui permet de récupérer le fichier XML Dume Entreprise pour un numéro de Dume publié
     * Cette méthode retourne un objet de type Reponse si le service LT-DUME a été sollicité.
     * Autrement, elle lève une exception de type ServiceException.
     *
     * @param int $numeroDume
     *
     * @return string le contenu du fichier XML contenant le résultat du WS LT DUME distant
     *
     * @throws ServiceException
     */
    public function getXml($numeroDume = 0)
    {
        if (null != $numeroDume) {
            $url = $this->restUrl.ValidEnum::URL_DUME_API.'/operateurEconomique/xml/'.$numeroDume;
        } else {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Entreprise : getXml : Erreur idContexteDumeOperateurEconomique : valeur non autorisée : '.$numeroDume);
            }

            throw new ServiceException(ServiceException::ERROR_GET_XML_DUME_ENTREPRISE, ValidEnum::ERROR_ID_CTX_DUME_ACHETEUR, 'Invalid Value');
        }

        if ($this->logger) {
            $this->logger->info('Api Dume : Service Entreprise : getXml: url : '.$url);
        }

        try {
            $response = $this->service->prepAndSend(
                $url,
                [201, 200],
                'GET',
                null,
                true,
                'application/json',
                'application/pdf',
                ['Authorization: Bearer '.$this->reponseLoginDume->getAccessToken()]
            );
        } catch (RestRequestException $e) {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Entreprise : getXml : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
            }

            throw new ServiceException(ServiceException::ERROR_REST, ValidEnum::VALID, $e->getMessage());
        }

        if (!empty($response)) {
            if ($this->logger) {
                $this->logger->info('Api Dume : Service Entreprise : getXml: resultat reçu ');
            }

            return $response;
        } else {
            throw new ServiceException(ServiceException::ERROR_GET_XML_DUME_ENTREPRISE, ValidEnum::VALID, 'Reponse Vide');
        }
    }

    /**
     * @param array $arrayIdContexteDumeOperateurEconomique
     *
     * @return mixed
     *
     * @throws ServiceException
     */
    public function publishGroupementDume($arrayIdContexteDumeOperateurEconomique = [])
    {
        $url = $this->restUrl.ValidEnum::URL_DUME_API.'/operateurEconomique/publicationGroupement';

        if ($this->logger) {
            $this->logger->info('Api Dume : Service Entreprise : publicationGroupement Dume: url : '.$url);
        }

        if (!empty($arrayIdContexteDumeOperateurEconomique)) {
            $json = json_encode($arrayIdContexteDumeOperateurEconomique);
            $this->logger->error("Api Dume : Service Entreprise : publicationGroupement Dume : json : $json");
        } else {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Entreprise : publicationGroupement Dume : Erreur Pas de données pour ce groupement');
            }

            throw new ServiceException(ServiceException::ERROR_REST, ValidEnum::VALID, 'Erreur : Pas de données pour ce groupement');
        }

        try {
            $response = $this->service->prepAndSend(
                $url,
                [201, 200],
                'POST',
                $json,
                true,
                'application/json',
                'application/json',
                ['Authorization: Bearer '.$this->reponseLoginDume->getAccessToken()]
            );
        } catch (RestRequestException $e) {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Entreprise : publicationGroupement Dume : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
            }
            throw new ServiceException(ServiceException::ERROR_REST, ValidEnum::VALID, $e->getMessage());
        }

        $result = json_decode($response, true);

        if (count($result) > 0) {
            if ($this->logger) {
                $this->logger->info('Api Dume : Service Entreprise : publicationGroupement Dume : resultat : '.$response);
            }

            return $result;
        }

        throw new ServiceException(ServiceException::ERROR_PUBLICATION_DUME_ENTREPRISE, ValidEnum::VALID, $response);
    }
}
