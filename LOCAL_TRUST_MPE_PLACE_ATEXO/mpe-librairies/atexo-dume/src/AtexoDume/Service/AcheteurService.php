<?php

namespace AtexoDume\Service;

use AtexoDume\Client\Client;
use AtexoDume\Dto\ReponseListPublishedDume;
use AtexoDume\Dto\ReponseValidPubDume;
use AtexoDume\Exception\RestRequestException;
use AtexoDume\Exception\ServiceException;
use AtexoDume\Tool\ValidEnum;

/**
 * Class AcheteurService.
 */
class AcheteurService extends AuthentificationService
{
    const CREATE = 'create';
    const UPDATE = 'update';
    const UPDATE_DLRO = 'updateDLRO';
    const VALIDATE = 'validate';
    const PUBLISH = 'publish';
    const CHECK_PURGE = 'checkPurge';

    protected $reponseLoginDume;

    public function __construct(Client &$client)
    {
        parent::__construct($client);
        $this->reponseLoginDume = self::login($client->getUsername(), $client->getPassword());
    }

    /**
     * Méthode qui permet de créer un Dume Acheteur.
     * Cette méthode retourne l'identifiant du contexte dume Acheteur si l'opération s'est bien déroulée.
     * Autrement, elle lève une exception de type ServiceException.
     *
     * @param \AtexoDume\Dto\Acheteur\Consultation $consultation
     *
     * @return int IdContextDumeAcheteur
     *
     * @throws ServiceException
     */
    public function createDume($consultation)
    {
        return $this->createOrUpdateDume($consultation, self::CREATE);
    }

    /**
     * Méthode qui permet de mettre à jour un Dume Acheteur existant.
     * Cette méthode retourne l'identifiant du contexte dume Acheteur si l'opération s'est bien déroulée.
     * Autrement, elle lève une exception de type ServiceException.
     *
     * @param \AtexoDume\Dto\Acheteur\Consultation $consultation
     * @param int                                  $idContextDumeAcheteur
     *
     * @return int IdContextDumeAcheteur
     *
     * @throws ServiceException
     */
    public function updateDume($consultation, $idContextDumeAcheteur = 0)
    {
        return $this->createOrUpdateDume($consultation, self::UPDATE, $idContextDumeAcheteur);
    }

    /**
     * Méthode qui permet de mettre à jour la DLRO d'un Dume Acheteur existant.
     * Cette méthode retourne l'identifiant du contexte dume Acheteur si l'opération s'est bien déroulée.
     * Autrement, elle lève une exception de type ServiceException.
     *
     * @param \AtexoDume\Dto\Acheteur\Consultation $consultation
     * @param int                                  $idContextDumeAcheteur
     *
     * @return int IdContextDumeAcheteur
     *
     * @throws ServiceException
     */
    public function updateDLRODume($consultation, $idContextDumeAcheteur = 0)
    {
        return $this->createOrUpdateDume($consultation, self::UPDATE_DLRO, $idContextDumeAcheteur);
    }

    /**
     * @param \AtexoDume\Dto\Acheteur\Consultation $consultation
     * @param string                               $methodName
     * @param int                                  $idContextDumeAcheteur
     *
     * @throws ServiceException
     */
    private function createOrUpdateDume($consultation, $methodName, $idContextDumeAcheteur = 0)
    {
        if (self::CREATE == $methodName) {
            $url = $this->restUrl.ValidEnum::URL_DUME_API.'/acheteur/create';
        } elseif ((self::UPDATE == $methodName) && null != $idContextDumeAcheteur) {
            $url = $this->restUrl.ValidEnum::URL_DUME_API.'/acheteur/updateMetadonnee/'.$idContextDumeAcheteur;
        } elseif ((self::UPDATE_DLRO == $methodName) && null != $idContextDumeAcheteur) {
            $url = $this->restUrl.ValidEnum::URL_DUME_API.'/acheteur/updateDLRO/'.$idContextDumeAcheteur;
        } else {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Acheteur : '.$methodName.'Dume : Erreur idContextDumeAcheteur : valeur non autorisée : '.$idContextDumeAcheteur);
            }

            throw new ServiceException(ServiceException::ERROR_UPDATE_DUME_ACHETEUR, ValidEnum::ERROR_ID_CTX_DUME_ACHETEUR, 'Invalid Value');
        }

        if ($this->logger) {
            $this->logger->info('Api Dume : Service Acheteur : '.$methodName.'Dume: url : '.$url);
        }

        $isValid = $consultation->isValid();
        $json = $consultation->toJSON();

        if (ValidEnum::VALID == $isValid) {
            if ($this->logger) {
                $this->logger->info('Api Dume : Service Acheteur : '.$methodName.'Dume : json : '.$json);
            }

            try {
                $response = $this->service->prepAndSend(
                    $url,
                    [201, 200],
                    'POST',
                    $json,
                    true,
                    'application/json',
                    'application/json',
                    ['Authorization: Bearer '.$this->reponseLoginDume->getAccessToken()]
                );
            } catch (RestRequestException $e) {
                if ($this->logger) {
                    $this->logger->error('Api Dume : Service Acheteur : '.$methodName.'Dume : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
                }

                throw new ServiceException(ServiceException::ERROR_REST, $isValid, $e->getMessage());
            }

            if (!empty($response)) {
                if ($this->logger) {
                    $this->logger->info('Api Dume : Service Acheteur : '.$methodName.'Dume : resultat : '.$response);
                }

                return $response;
            }

            if ($this->logger) {
                $this->logger->error('Api Dume : Service Acheteur : '.$methodName.'Dume : Erreur : '.$response);
            }

            if (self::CREATE == $methodName) {
                throw new ServiceException(ServiceException::ERROR_CREATION_DUME_ACHETEUR, $isValid, $response);
            } elseif (self::UPDATE == $methodName) {
                throw new ServiceException(ServiceException::ERROR_UPDATE_DUME_ACHETEUR, $isValid, $response);
            } elseif (self::UPDATE_DLRO == $methodName) {
                throw new ServiceException(ServiceException::ERROR_UPDATE_DUME_ACHETEUR, $isValid, $response);
            }
        } else {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Acheteur : '.$methodName.'Dume : Erreur Json : CODE : '.$isValid.' Objet : '.$json);
            }

            throw new ServiceException(ServiceException::ERROR_JSON, $isValid, 'Invalid Object');
        }
    }

    /**
     * Méthode qui permet de demander la validation d'un Dume Acheteur existant.
     * Cette méthode retourne un objet de type Reponse si le service LT-DUME a été sollicité.
     * Autrement, elle lève une exception de type ServiceException.
     *
     * @param int $idContextDumeAcheteur
     *
     * @return ReponseValidPubDume l'objet Reponse contenant le résultat du WS LT DUME distant
     *
     * @throws ServiceException
     */
    public function validateDume($idContextDumeAcheteur = 0)
    {
        return $this->validateOrPublishDume(self::VALIDATE, $idContextDumeAcheteur);
    }

    /**
     * Méthode qui permet de demander la publication d'un Dume Acheteur existant.
     * Cette méthode retourne un objet de type Reponse si le service LT-DUME a été sollicité.
     * Autrement, elle lève une exception de type ServiceException.
     *
     * @param int $idContextDumeAcheteur
     *
     * @return ReponseValidPubDume l'objet Reponse contenant le résultat du WS LT DUME distant
     *
     * @throws ServiceException
     */
    public function publishDume($idContextDumeAcheteur = 0)
    {
        return $this->validateOrPublishDume(self::PUBLISH, $idContextDumeAcheteur);
    }

    /**
     * @param string $methodName
     * @param int    $idContextDumeAcheteur
     *
     * @return ReponseValidPubDume
     *
     * @throws ServiceException
     */
    private function validateOrPublishDume($methodName, $idContextDumeAcheteur = 0)
    {
        if ((self::VALIDATE == $methodName) && null != $idContextDumeAcheteur) {
            $url = $this->restUrl.ValidEnum::URL_DUME_API.'/acheteur/validation/'.$idContextDumeAcheteur;
        } elseif ((self::PUBLISH == $methodName) && null != $idContextDumeAcheteur) {
            $url = $this->restUrl.ValidEnum::URL_DUME_API.'/acheteur/publication/'.$idContextDumeAcheteur;
        } else {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Acheteur : '.$methodName.'Dume : Erreur idContextDumeAcheteur : valeur non autorisée : '.$idContextDumeAcheteur);
            }

            throw new ServiceException(ServiceException::ERROR_VALIDATION_DUME_ACHETEUR, ValidEnum::ERROR_ID_CTX_DUME_ACHETEUR, 'Invalid Value');
        }

        if ($this->logger) {
            $this->logger->info('Api Dume : Service Acheteur : '.$methodName.'Dume: url : '.$url);
        }

        try {
            $response = $this->service->prepAndSend(
                $url,
                [201, 200],
                'GET',
                null,
                true,
                'application/json',
                'application/json',
                ['Authorization: Bearer '.$this->reponseLoginDume->getAccessToken()]
            );
        } catch (RestRequestException $e) {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Acheteur : '.$methodName.'Dume : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
            }

            throw new ServiceException(ServiceException::ERROR_REST, ValidEnum::VALID, $e->getMessage());
        }

        $result = json_decode($response, true);

        if (count($result) > 0) {
            if ($this->logger) {
                $this->logger->info('Api Dume : Service Acheteur : '.$methodName.'Dume : resultat : '.$response);
            }

            return new ReponseValidPubDume($result);
        }

        if (self::VALIDATE == $methodName) {
            throw new ServiceException(ServiceException::ERROR_VALIDATION_DUME_ACHETEUR, ValidEnum::VALID, $response);
        }

        if (self::PUBLISH == $methodName) {
            throw new ServiceException(ServiceException::ERROR_PUBLICATION_DUME_ACHETEUR, ValidEnum::VALID, $response);
        }
    }

    /**
     * Méthode qui permet de récupérer la liste des dumes Acheteur publiés pour le contexte dume Acheteur passé en paramètre
     * Cette méthode retourne un objet de type Reponse si le service LT-DUME a été sollicité.
     * Autrement, elle lève une exception de type ServiceException.
     *
     * @param int $idContextDumeAcheteur
     *
     * @return ReponseListPublishedDume l'objet ReponseListPublishedDume contenant le résultat du WS LT DUME distant
     *
     * @throws ServiceException
     */
    public function getPublishedDume($idContextDumeAcheteur = 0)
    {
        if (null != $idContextDumeAcheteur) {
            $url = $this->restUrl.ValidEnum::URL_DUME_API.'/acheteur/publication/list/'.$idContextDumeAcheteur;
        } else {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Acheteur : getPublishedDume : Erreur idContextDumeAcheteur : valeur non autorisée : '.$idContextDumeAcheteur);
            }

            throw new ServiceException(ServiceException::ERROR_LIST_PUBLISHED_DUME_ACHETEUR, ValidEnum::ERROR_ID_CTX_DUME_ACHETEUR, 'Invalid Value');
        }

        if ($this->logger) {
            $this->logger->info('Api Dume : Service Acheteur : getPublishedDume: url : '.$url);
        }

        try {
            $response = $this->service->prepAndSend(
                $url,
                [201, 200],
                'GET',
                null,
                true,
                'application/json',
                'application/json',
                ['Authorization: Bearer '.$this->reponseLoginDume->getAccessToken()]
            );
        } catch (RestRequestException $e) {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Acheteur : getPublishedDume : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
            }

            throw new ServiceException(ServiceException::ERROR_REST, ValidEnum::VALID, $e->getMessage());
        }

        $result = json_decode($response, true);

        if (count($result) > 0) {
            if ($this->logger) {
                $this->logger->info('Api Dume : Service Acheteur : getPublishedDume : resultat : '.$response);
            }

            return new ReponseListPublishedDume($result);
        } else {
            throw new ServiceException(ServiceException::ERROR_LIST_PUBLISHED_DUME_ACHETEUR, ValidEnum::VALID, $response);
        }
    }

    /**
     * Méthode qui permet de récupérer le fichier PDF Dume Acheteur pour un numéro de Dume publié
     * Cette méthode retourne un objet de type Reponse si le service LT-DUME a été sollicité.
     * Autrement, elle lève une exception de type ServiceException.
     *
     * @param int $numeroDume
     *
     * @return string le contenu du fichier PDF contenant le résultat du WS LT DUME distant
     *
     * @throws ServiceException
     */
    public function getPdf($numeroDume = 0)
    {
        if (null != $numeroDume) {
            $url = $this->restUrl.ValidEnum::URL_DUME_API.'/acheteur/pdf/'.$numeroDume;
        } else {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Acheteur : getPdf : Erreur idContextDumeAcheteur : valeur non autorisée : '.$numeroDume);
            }

            throw new ServiceException(ServiceException::ERROR_GET_PDF_DUME_ACHETEUR, ValidEnum::ERROR_ID_CTX_DUME_ACHETEUR, 'Invalid Value');
        }

        if ($this->logger) {
            $this->logger->info('Api Dume : Service Acheteur : getPdf: url : '.$url);
        }

        try {
            $response = $this->service->prepAndSend(
                $url,
                [201, 200],
                'GET',
                null,
                true,
                'application/json',
                'application/pdf',
                ['Authorization: Bearer '.$this->reponseLoginDume->getAccessToken()]
            );
        } catch (RestRequestException $e) {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Acheteur : getPdf : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
            }

            throw new ServiceException(ServiceException::ERROR_REST, ValidEnum::VALID, $e->getMessage());
        }

        if (!empty($response)) {
            if ($this->logger) {
                $this->logger->info('Api Dume : Service Acheteur : getPdf: resultat reçu ');
            }

            return $response;
        } else {
            throw new ServiceException(ServiceException::ERROR_GET_PDF_DUME_ACHETEUR, ValidEnum::VALID, 'Reponse Vide');
        }
    }

    /**
     * Méthode qui permet de récupérer le fichier XML Dume Acheteur pour un numéro de Dume publié
     * Cette méthode retourne un objet de type Reponse si le service LT-DUME a été sollicité.
     * Autrement, elle lève une exception de type ServiceException.
     *
     * @param int $numeroDume
     *
     * @return string le contenu du fichier XML contenant le résultat du WS LT DUME distant
     *
     * @throws ServiceException
     */
    public function getXml($numeroDume = 0)
    {
        if (null != $numeroDume) {
            $url = $this->restUrl.ValidEnum::URL_DUME_API.'/acheteur/xml/'.$numeroDume;
        } else {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Acheteur : getXml : Erreur idContextDumeAcheteur : valeur non autorisée : '.$numeroDume);
            }

            throw new ServiceException(ServiceException::ERROR_GET_XML_DUME_ACHETEUR, ValidEnum::ERROR_ID_CTX_DUME_ACHETEUR, 'Invalid Value');
        }

        if ($this->logger) {
            $this->logger->info('Api Dume : Service Acheteur : getXml: url : '.$url);
        }

        try {
            $response = $this->service->prepAndSend(
                $url,
                [201, 200],
                'GET',
                null,
                true,
                'application/json',
                'application/xml',
                ['Authorization: Bearer '.$this->reponseLoginDume->getAccessToken()]
            );
        } catch (RestRequestException $e) {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Acheteur : getXml : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
            }

            throw new ServiceException(ServiceException::ERROR_REST, ValidEnum::VALID, $e->getMessage());
        }

        if (!empty($response)) {
            if ($this->logger) {
                $this->logger->info('Api Dume : Service Acheteur : getXml: resultat reçu ');
            }

            return $response;
        } else {
            throw new ServiceException(ServiceException::ERROR_GET_XML_DUME_ACHETEUR, ValidEnum::VALID, 'Reponse Vide');
        }
    }

    /**
     * Retourne un access token à utiliser côté navigateur.
     *
     * @param Client $client
     * @param $idContexteDume
     *
     * @return string
     *
     * @throws ServiceException
     */
    public function getClientAccessToken($client, $idContexteDume)
    {
        $response = self::login(
            $client->getUsername(),
            $client->getPassword(),
            parent::TYPE_DUME_ACHETEUR,
            $idContexteDume
        );

        return $response->getAccessToken();
    }

    /**
     * Méthode qui permet de demander à lt_dume le changement de statut et la désactivation des DUME OE brouillon
     * Cette méthode retourne un array contenant la liste des DUME OE passé au statut supprimé par lt_dume
     * Autrement, elle lève une exception de type ServiceException.
     *
     * @param int $numeroDume
     *
     * @return array
     *
     * @throws ServiceException
     */
    public function replacePublishedDume($numeroDume = 0)
    {
        if (null != $numeroDume) {
            $url = $this->restUrl.ValidEnum::URL_DUME_API.'/acheteur/replacePublished/'.$numeroDume;
        } else {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Acheteur : replacePublishedDume : Erreur idContextDumeAcheteur : valeur non autorisée : '.$numeroDume);
            }

            throw new ServiceException(ServiceException::ERROR_REPLACE_PUBLISHED_DUME_ACHETEUR, ValidEnum::ERROR_ID_CTX_DUME_ACHETEUR, 'Invalid Value');
        }

        if ($this->logger) {
            $this->logger->info('Api Dume : Service Acheteur : replacePublishedDume: url : '.$url);
        }

        try {
            $response = $this->service->prepAndSend(
                $url,
                [201, 200],
                'GET',
                null,
                true,
                'application/json',
                'application/json',
                ['Authorization: Bearer '.$this->reponseLoginDume->getAccessToken()]
            );
        } catch (RestRequestException $e) {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Acheteur : replacePublishedDume : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
            }

            throw new ServiceException(ServiceException::ERROR_REST, ValidEnum::VALID, $e->getMessage());
        }

        if (!empty($response)) {
            if ($this->logger) {
                $this->logger->info('Api Dume : Service Acheteur : replacePublishedDume: resultat reçu ');
            }

            return $response;
        } else {
            throw new ServiceException(ServiceException::ERROR_REPLACE_PUBLISHED_DUME_ACHETEUR, ValidEnum::VALID, 'Reponse Vide');
        }
    }

    /**
     * Méthode qui permet de mettre à jour la DLRO d'un Dume Acheteur existant.
     * Cette méthode retourne l'identifiant du contexte dume Acheteur si l'opération s'est bien déroulée.
     * Autrement, elle lève une exception de type ServiceException.
     *
     * @param \AtexoDume\Dto\Acheteur\Consultation $consultation
     * @param int                                  $idContextDumeAcheteur
     *
     * @return mixed
     *
     * @throws ServiceException
     */
    public function checkPurgeDume($consultation, $idContextDumeAcheteur)
    {
        $url = $this->restUrl.ValidEnum::URL_DUME_API.'/acheteur/checkPurge/'.$idContextDumeAcheteur;

        if ($this->logger) {
            $this->logger->info('Api Dume : Service Acheteur : checkPurgeDume : url : '.$url);
        }

        $isValid = $consultation->isValid();
        $json = $consultation->toJSON();

        if (ValidEnum::VALID == $isValid) {
            if ($this->logger) {
                $this->logger->info('Api Dume : Service Acheteur : checkPurgeDume : json : '.$json);
            }

            try {
                $response = $this->service->prepAndSend(
                    $url,
                    [201, 200],
                    'POST',
                    $json,
                    true,
                    'application/json',
                    'application/json',
                    ['Authorization: Bearer '.$this->reponseLoginDume->getAccessToken()]
                );
            } catch (RestRequestException $e) {
                if ($this->logger) {
                    $this->logger->error('Api Dume : Service Acheteur : checkPurgeDume : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
                }

                throw new ServiceException(ServiceException::ERROR_REST, $isValid, $e->getMessage());
            }

            if (!empty($response)) {
                if ($this->logger) {
                    $this->logger->info('Api Dume : Service Acheteur : checkPurgeDume : resultat : '.$response);
                }

                return json_decode($response, true);
            }

            if ($this->logger) {
                $this->logger->error('Api Dume : Service Acheteur : checkPurgeDume : Erreur : '.$response);
            }

            throw new ServiceException(ServiceException::ERROR_CHECK_PURGE_DUME_ACHETEUR, $isValid, $response);
        } else {
            if ($this->logger) {
                $this->logger->error('Api Dume : Service Acheteur : checkPurgeDume : Erreur Json : CODE : '.$isValid.' Objet : '.$json);
            }

            throw new ServiceException(ServiceException::ERROR_JSON, $isValid, 'Invalid Object');
        }
    }

    public function getInfoDume($idContextDume)
    {
        if (empty($idContextDume)) {
            throw new ServiceException(ServiceException::ERROR_REST, 'Context id manquant');
        }

        $info = 'Api Dume : Service Acheteur : getInfoDume';
        $url = $this->restUrl.ValidEnum::URL_DUME_API.'/acheteur/get/'.$idContextDume;
        if ($this->logger) {
            $this->logger->info($info.' : url : '.$url);
        }

        try {
            $response = $this->service->prepAndSend(
                $url,
                [201, 200],
                'GET',
                null,
                true,
                'application/json',
                'application/json',
                ['Authorization: Bearer '.$this->reponseLoginDume->getAccessToken()]
            );
        } catch (RestRequestException $e) {
            if ($this->logger) {
                $this->logger->error($info.' : Erreur Rest : HTTP CODE : '.$e->statusCode.' MESSAGE : '.$e->getMessage());
            }
            throw new ServiceException(ServiceException::ERROR_REST, $e->getMessage());
        }

        if (!empty($response)) {
            if ($this->logger) {
                $this->logger->info($info.' : resultat : '.$response);
            }

            return json_decode($response, true);
        }

        if ($this->logger) {
            $this->logger->error($info.' : Erreur : '.$response);
        }
    }
}
