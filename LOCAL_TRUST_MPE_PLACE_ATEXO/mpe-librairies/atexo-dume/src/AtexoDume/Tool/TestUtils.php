<?php
/**
 * Created by PhpStorm.
 * User: MWA
 * Date: 18/01/2018
 * Time: 19:02.
 */

namespace AtexoDume\Tool;

use AtexoDume\Dto\Acheteur\Consultation;
use AtexoDume\Dto\Acheteur\Metadata;
use AtexoDume\Dto\Lot;
use AtexoDume\Dto\NumeroDume;
use AtexoDume\Dto\ReponseListPublishedDume;
use AtexoDume\Dto\ReponseLoginDume;
use AtexoDume\Dto\ReponseValidPubDume;

/**
 * Création d'objet pour utilisation au niveau des tests unitaires.
 */
class TestUtils
{
    /**
     * Créer un objet de type Lot.
     *
     * @return Lot
     */
    public static function createLot()
    {
        $lot = new Lot();
        $lot->setIntituleLot('Intitule du Lot');
        $lot->setNumeroLot('1');

        return $lot;
    }

    /**
     * Créer un objet de type Lot.
     *
     * @return array
     */
    public static function createListeLots()
    {
        $Lots = [];

        $lot = new Lot();
        $lot->setIntituleLot('Intitule du Lot 1');
        $lot->setNumeroLot('1');

        $Lots[] = $lot;

        $lot = new Lot();
        $lot->setIntituleLot('Intitule du Lot 2');
        $lot->setNumeroLot('2');

        $Lots[] = $lot;

        return $Lots;
    }

    /**
     * Créer un objet de type Metadata Acheteur.
     *
     * @return \AtexoDume\Dto\Acheteur\Metadata
     */
    public static function createMetadataAcheteur()
    {
        $metadata = new Metadata();
        $metadata->setDateLimiteRemisePlis('2018-03-23T17:30:00+01:00');
        $metadata->setIdConsultation('1');
        $metadata->setIdentifiantPlateforme('test');
        $metadata->setOrganisme('t5y');

        return $metadata;
    }

    /**
     * Créer un objet de type Metadata Entreprise.
     *
     * @return \AtexoDume\Dto\Entreprise\Metadata
     */
    public static function createMetadataEntreprise()
    {
        $metadata = new \AtexoDume\Dto\Entreprise\Metadata();
        $metadata->setIdentifiantPlateforme('test');
        $metadata->setIdConsultation('123456');
        $metadata->setIdContextDumeAcheteur('9');
        $metadata->setIdInscrit('20');
        $metadata->setSiret('11122233300033');

        return $metadata;
    }

    /**
     * Créer un objet de type Consultation Acheteur.
     *
     * @return Consultation
     */
    public static function createConsultationAcheteur()
    {
        $consultation = new Consultation();

        $consultation->setIntitule('Intitulé de la consultation de test');
        $consultation->setObjet("Travaux de rénovation de la façade de l'école St-Vincent");
        $consultation->setLots(TestUtils::createListeLots());
        $consultation->setMetadata(TestUtils::createMetadataAcheteur());
        $consultation->setNomOfficiel('Ministère de test');
        $consultation->setReferenceConsultation('CONS-TEST-UNIT');
        $consultation->setTypeProcedure('ouverte');

        return $consultation;
    }

    /**
     * Créer un objet de type Consultation Entreprise.
     *
     * @return \AtexoDume\Dto\Entreprise\Consultation
     */
    public static function createConsultationEntreprise()
    {
        $consultation = new \AtexoDume\Dto\Entreprise\Consultation();
        $consultation->setMetadata(TestUtils::createMetadataEntreprise());
        $consultation->setLots(TestUtils::createListeLots());

        return $consultation;
    }

    /**
     * Créer un objet de type ReponseValidPubDume.
     *
     * @return ReponseValidPubDume
     */
    public static function createReponseValidPubDume()
    {
        $reponse = new ReponseValidPubDume();
        $reponse->setMessage('Validation / Publication du DUME REALISEE');
        $reponse->setStatut(ReponseValidPubDume::OK);

        return $reponse;
    }

    /**
     * Créer un objet de type NumeroDume.
     *
     * @return NumeroDume
     */
    public static function createNumeroDume()
    {
        $numeroDume = new NumeroDume();
        $numeroDume->setNumeroDumeSN('1234560');
        $numeroDume->setNumeroLot('1');
        $numeroDume->setIsStandard('true');

        return $numeroDume;
    }

    /**
     * Créer une liste de type NumeroDume.
     *
     * @return array
     */
    public static function createListNumeroDume()
    {
        $NumerosDume = [];

        $numeroDume = new NumeroDume();
        $numeroDume->setNumeroDumeSN('1234560');
        $numeroDume->setNumeroLot('1');
        $numeroDume->setIsStandard('true');

        $NumerosDume[] = $numeroDume;

        $numeroDume = new NumeroDume();
        $numeroDume->setNumeroDumeSN('7891011');
        $numeroDume->setNumeroLot('2');

        $NumerosDume[] = $numeroDume;

        return $NumerosDume;
    }

    /**
     * Créer un objet de type ReponseListPublishedDume.
     *
     * @return ReponseListPublishedDume
     */
    public static function createReponseListPublishedDume()
    {
        $reponseListPublishedDume = new \AtexoDume\Dto\ReponseListPublishedDume();
        $reponseListPublishedDume->setStatut(ReponseListPublishedDume::OK);
        $reponseListPublishedDume->setNumerosDume(self::createListNumeroDume());

        return $reponseListPublishedDume;
    }

    /**
     * Créer un objet de type ReponseLoginDume.
     *
     * @return ReponseLoginDume
     */
    public static function createReponseLoginDume()
    {
        $reponseLoginDume = new \AtexoDume\Dto\ReponseLoginDume();
        $reponseLoginDume->setAccessToken('3483205e-857a-430e-bf8d-67108ff74a0d');
        $reponseLoginDume->setTokenType('bearer');
        $reponseLoginDume->setRefreshToken('91c20744-c9dc-4dce-a73b-f44c03082aa2');
        $reponseLoginDume->setExpiresIn('120');
        $reponseLoginDume->setScope('read write trust');

        return $reponseLoginDume;
    }
}
