<?php

namespace AtexoDume\Tool;

use AtexoDume\Exception\RestRequestException;

/**
 * Class RestRequest.
 */
class RestRequest
{
    protected $url;
    protected $verb;
    protected $request_body;
    protected $request_length;
    protected $accept_type;
    protected $content_type;
    protected $response_body;
    protected $response_headers;
    protected $response_info;
    protected $headers;
    protected $curl_timeout;
    protected $proxy;
    protected $protocol;

    public $errorCode;

    /**
     * Constructeur.
     *
     * @param null   $url
     * @param string $verb
     * @param null   $request_body
     */
    public function __construct($url = null, $verb = 'GET', $request_body = null)
    {
        $this->url = $url;
        $this->verb = $verb;
        $this->request_body = $request_body;
        $this->request_length = 0;
        $this->accept_type = null;
        $this->content_type = 'application/json';
        $this->response_body = null;
        $this->response_headers = null;
        $this->response_info = null;
        $this->curl_timeout = 30;
        $this->proxy = null;
        $this->protocol = 'http_code';

        if (null !== $this->request_body) {
            $this->buildPostBody();
        }
    }

    /** This function will convert an indexed array of headers into an associative array where the key matches
     * the key of the headers, and the value matches the value of the header.
     *
     * This is useful to access headers by name that may be returned in the response from makeRequest.
     *
     * @param $array array Indexed header array returned by makeRequest
     *
     * @return array
     */
    public static function splitHeaderArray($array)
    {
        $result = [];
        foreach (array_values($array) as $value) {
            $pair = explode(':', $value, 2);
            if (count($pair) > 1) {
                $result[$pair[0]] = ltrim($pair[1]);
            } else {
                $result[] = $value;
            }
        }

        return $result;
    }

    /**
     * flush parameter.
     */
    public function flush()
    {
        $this->request_body = null;
        $this->request_length = 0;
        $this->verb = 'GET';
        $this->response_body = null;
        $this->response_info = null;
        $this->content_type = 'application/json';
        $this->accept_type = 'application/json';
        $this->headers = null;
    }

    /**
     * Executer la requete.
     *
     * @throws \Exception
     */
    public function execute()
    {
        $ch = curl_init();
        $this->setAuth();
        $this->setTimeout($ch);
        try {
            switch (strtoupper($this->verb)) {
                case 'GET':
                    $this->executeGet($ch);
                    break;
                case 'POST':
                    $this->executePost($ch);
                    break;
                case 'PUT':
                    $this->executePut($ch);
                    break;
                case 'DELETE':
                    $this->executeDelete($ch);
                    break;
                case 'PUT_MP':
                    $this->verb = 'PUT';
                    $this->executePutMultipart($ch);
                    break;
                case 'POST_MP':
                    $this->verb = 'POST';
                    $this->executePostMultipart($ch);
                    break;
                case 'POST_BIN':
                    $this->verb = 'POST';
                    $this->executeBinarySend($ch);
                    break;
                case 'PUT_BIN':
                    $this->verb = 'PUT';
                    $this->executeBinarySend($ch);
                    break;
                default:
                    throw new \InvalidArgumentException('Current verb ('.$this->verb.') is an invalid REST verb.');
            }
        } catch (\InvalidArgumentException $e) {
            curl_close($ch);
            throw $e;
        } catch (\Exception $e) {
            curl_close($ch);
            throw $e;
        }
    }

    /**
     * buildPostBody.
     *
     * @param null $data
     */
    public function buildPostBody($data = null)
    {
        $data = (null !== $data) ? $data : $this->request_body;
        $this->request_body = $data;
    }

    /**
     * Executer la requete GET.
     *
     * @param $ch
     */
    protected function executeGet($ch)
    {
        $this->doExecute($ch);
    }

    /**
     * Executer la requete POST.
     *
     * @param $ch
     */
    protected function executePost($ch)
    {
        if (!is_string($this->request_body)) {
            $this->buildPostBody();
        }

        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->request_body);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

        $this->doExecute($ch);
    }

    /**
     * Executer la requete send binary.
     *
     * @param $ch
     */
    protected function executeBinarySend($ch)
    {
        $post = $this->request_body;

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->verb);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);

        $this->response_body = curl_exec($ch);
        $this->response_info = curl_getinfo($ch);

        curl_close($ch);
    }

    /**
     * Executer la requete multipart PUT
     * Set verb to PUT_MP to use this function.
     *
     * @param $ch
     */
    protected function executePutMultipart($ch)
    {
        $post = $this->request_body;

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $this->response_body = curl_exec($ch);
        $this->response_info = curl_getinfo($ch);

        curl_close($ch);
    }

    /**
     * Executer la requete multipart POST
     * Set verb to POST_MP to use this function.
     *
     * @param $ch
     */
    protected function executePostMultipart($ch)
    {
        $post = $this->request_body;

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $this->response_body = curl_exec($ch);
        $this->response_info = curl_getinfo($ch);

        curl_close($ch);
    }

    /**
     * Executer la requete PUT.
     *
     * @param $ch
     */
    protected function executePut($ch)
    {
        if (!is_string($this->request_body)) {
            $this->buildPostBody();
        }

        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->request_body);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');

        $this->doExecute($ch);
    }

    /**
     * Executer la requete DELETE.
     *
     * @param $ch
     */
    protected function executeDelete($ch)
    {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

        $this->doExecute($ch);
    }

    /**
     * Executer la requete.
     *
     * @param $curlHandle
     */
    protected function doExecute(&$curlHandle)
    {
        $this->setCurlOpts($curlHandle);
        $response = curl_exec($curlHandle);
        $this->response_info = curl_getinfo($curlHandle);

        $header_size = curl_getinfo($curlHandle, CURLINFO_HEADER_SIZE);
        $this->response_headers = substr($response, 0, $header_size);
        $this->response_body = substr($response, $header_size);
        curl_close($curlHandle);
    }

    /**
     * Prepare les curl options.
     *
     * @param $curlHandle
     */
    protected function setCurlOpts(&$curlHandle)
    {
        curl_setopt($curlHandle, CURLOPT_URL, $this->url);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_COOKIEFILE, '/dev/null');
        curl_setopt($curlHandle, CURLOPT_HEADER, true);

        if (!empty($this->content_type)) {
            $this->headers[] = 'Content-Type: '.$this->content_type;
        }
        if (!empty($this->accept_type)) {
            $this->headers[] = 'Accept: '.$this->accept_type;
        }
        if (!empty($this->headers)) {
            curl_setopt($curlHandle, CURLOPT_HTTPHEADER, $this->headers);
        }
        if (!empty($this->proxy)) {
            curl_setopt($curlHandle, CURLOPT_PROXY, $this->proxy);
        }
    }

    /**
     * Prepare Auth à l'authentification.
     *
     * @param $curlHandle
     */
    protected function setAuth()
    {
    }

    /**
     * setTimeout.
     *
     * @param $curlHandle
     */
    protected function setTimeout(&$curlHandle)
    {
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, $this->curl_timeout);
    }

    public function defineTimeout($seconds)
    {
        $this->curl_timeout = $seconds;
    }

    public function getAcceptType()
    {
        return $this->accept_type;
    }

    public function setAcceptType($accept_type)
    {
        $this->accept_type = $accept_type;
    }

    public function getContentType()
    {
        return $this->content_type;
    }

    public function setContentType($content_type)
    {
        $this->content_type = $content_type;
    }

    public function getResponseBody()
    {
        return $this->response_body;
    }

    public function getResponseInfo()
    {
        return $this->response_info;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getVerb()
    {
        return $this->verb;
    }

    public function setVerb($verb)
    {
        $this->verb = $verb;
    }

    public function getProxy()
    {
        return $this->proxy;
    }

    public function setProxy($proxy)
    {
        $this->proxy = $proxy;
    }

    /**
     * handleError.
     *
     * @param $statusCode
     * @param $expectedCodes
     * @param $responseBody
     *
     * @throws RestRequestException
     */
    public function handleError($statusCode, $expectedCodes, $responseBody)
    {
        if (!empty($responseBody)) {
            $errorData = json_decode($responseBody);
            $exception = new RestRequestException(
                (empty($errorData->message)) ? RestRequestException::UNEXPECTED_CODE_MSG : $errorData->message
            );
            $exception->expectedStatusCodes = $expectedCodes;
            $exception->statusCode = $statusCode;
            if (!empty($errorData->errorCode)) {
                $exception->errorCode = $errorData->errorCode;
            }
            if (!empty($errorData->parameters)) {
                $exception->parameters = $errorData->parameters;
            }

            throw $exception;
        } else {
            $exception = new RestRequestException(RestRequestException::UNEXPECTED_CODE_MSG);
            $exception->expectedStatusCodes = $expectedCodes;
            $exception->statusCode = $statusCode;

            throw $exception;
        }
    }

    /**
     * makeRequest.
     *
     * @param $url
     * @param array  $expectedCodes
     * @param null   $verb
     * @param null   $reqBody
     * @param string $contentType
     * @param string $acceptType
     * @param array  $headers
     *
     * @return array
     *
     * @throws RestRequestException
     * @throws \Exception
     */
    public function makeRequest(
        $url,
        $expectedCodes = [200],
        $verb = null,
        $reqBody = null,
        $contentType = 'application/json',
        $acceptType = 'application/json',
        $headers = []
    ) {
        $this->flush();
        $this->setUrl($url);
        if (null !== $verb) {
            $this->setVerb($verb);
        }
        if (null !== $reqBody) {
            $this->buildPostBody($reqBody);
        }
        if (!empty($contentType)) {
            $this->setContentType($contentType);
        }
        if (!empty($acceptType)) {
            $this->setAcceptType($acceptType);
        }
        if (!empty($headers)) {
            $this->headers = $headers;
        }

        $this->execute();

        $info = $this->getResponseInfo();
        $statusCode = $info[$this->protocol];
        $body = $this->getResponseBody();

        $headers = $this->response_headers;

        // An exception is thrown here if the expected code does not match the status code in the response
        if (!in_array($statusCode, $expectedCodes)) {
            $this->handleError($statusCode, $expectedCodes, $body);
        }

        return compact('body', 'statusCode', 'headers');
    }

    /**
     * prepAndSend.
     *
     * @param $url
     * @param array  $expectedCodes
     * @param null   $verb
     * @param null   $reqBody
     * @param bool   $returnData
     * @param string $contentType
     * @param string $acceptType
     * @param array  $headers
     *
     * @return bool|null
     *
     * @throws RestRequestException
     * @throws \Exception
     */
    public function prepAndSend(
        $url,
        $expectedCodes = [200],
        $verb = null,
        $reqBody = null,
        $returnData = false,
        $contentType = 'application/json',
        $acceptType = 'application/json',
        $headers = []
    ) {
        $this->flush();
        $this->setUrl($url);
        if (null !== $verb) {
            $this->setVerb($verb);
        }
        if (null !== $reqBody) {
            $this->buildPostBody($reqBody);
        }
        if (!empty($contentType)) {
            $this->setContentType($contentType);
        }
        if (!empty($acceptType)) {
            $this->setAcceptType($acceptType);
        }
        if (!empty($headers)) {
            $this->headers = $headers;
        }

        $this->execute();
        $statusCode = $this->getResponseInfo();
        $responseBody = $this->getResponseBody();
        $statusCode = $statusCode[$this->protocol];

        if (!in_array($statusCode, $expectedCodes)) {
            $this->handleError($statusCode, $expectedCodes, $responseBody);
        }

        if ($returnData) {
            return $this->getResponseBody();
        }

        return true;
    }

    /**
     * This function creates a multipart/form-data request and sends it to the server.
     * this function should only be used when a file is to be sent with a request (PUT/POST).
     *
     * @param $url - URL to send request to
     * @param array|$expectedCodes - HTTP Status Code you expect to receive on success
     * @param $verb - HTTP Verb to send with request
     * @param $reqBody - The body of the request if necessary
     *
     * @return array - Returns an array with the response info and the response body, since the server sends a 100 request, it is hard to validate the success of the request
     */
    public function multipartRequestSend($url, $expectedCodes = [200, 201], $verb = 'PUT_MP', $reqBody = null)
    {
        $this->flush();
        $this->setUrl($url);
        $this->setVerb($verb);
        if (!empty($reqBody)) {
            $this->buildPostBody($reqBody);
        }
        $this->execute();
        $response = $this->getResponseInfo();
        $responseBody = $this->getResponseBody();
        $statusCode = $response[$this->protocol];

        if (!in_array($statusCode, $expectedCodes)) {
            $this->handleError($statusCode, $expectedCodes, $responseBody);
        }

        return $responseBody;
    }

    /**
     * sendBinary.
     *
     * @param $url
     * @param $expectedCodes
     * @param $body
     * @param $contentType
     * @param $contentDisposition
     * @param $contentDescription
     * @param string $verb
     *
     * @return null
     *
     * @throws RestRequestException
     * @throws \Exception
     */
    public function sendBinary($url, $expectedCodes, $body, $contentType, $contentDisposition, $contentDescription, $verb = 'POST')
    {
        $this->flush();
        $this->setUrl($url);
        $this->setVerb($verb.'_BIN');
        $this->buildPostBody($body);
        $this->setContentType($contentType);
        $this->headers = ['Content-Type: '.$contentType, 'Content-Disposition: '.$contentDisposition, 'Content-Description: '.$contentDescription, 'Accept: application/json'];

        $this->execute();

        $statusCode = $this->getResponseInfo();
        $responseBody = $this->getResponseBody();
        $statusCode = $statusCode[$this->protocol];

        if (!in_array($statusCode, $expectedCodes)) {
            $this->handleError($statusCode, $expectedCodes, $responseBody);
        }

        return $this->getResponseBody();
    }
}
