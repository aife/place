<?php
/**
 * Created by PhpStorm.
 * User: MWA
 * Date: 18/01/2018
 * Time: 19:02.
 */

namespace AtexoDume\Tool;

class JsonSerializer
{
    /**
     * Local cache of a property getters per class - optimize reflection code if the same object appears several times.
     *
     * @var array
     */
    private $classPropertyGetters = [];

    /**
     * @param mixed $object
     *
     * @return string|false
     */
    public function serialize($object)
    {
        return json_encode(
            $this->serializeInternal($object),
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
        );
    }

    /**
     * @param $object
     *
     * @return array
     */
    private function serializeInternal($object)
    {
        if (is_array($object)) {
            $result = $this->serializeArray($object);
        } elseif (is_object($object)) {
            $result = $this->serializeObject($object);
        } else {
            $result = $object;
        }

        return $result;
    }

    /**
     * @param $object
     *
     * @return \ReflectionClass
     */
    private function getClassPropertyGetters($object)
    {
        $className = get_class($object);

        if (!isset($this->classPropertyGetters[$className])) {
            $reflector = new \ReflectionClass($className);
            $properties = $reflector->getProperties();
            $getters = [];

            foreach ($properties as $property) {
                $name = $property->getName();
                $getter = 'get'.ucfirst($name);

                try {
                    $reflector->getMethod($getter);
                    $getters[$name] = $getter;
                } catch (\Exception $e) {
                    $name = $property->getName();
                    $getter = 'is'.ucfirst($name);

                    try {
                        $reflector->getMethod($getter);
                        $getters[$name] = $getter;
                    } catch (\Exception $e) {
                        // if no getter for a specific property - ignore it
                    }
                }
            }

            $this->classPropertyGetters[$className] = $getters;
        }

        return $this->classPropertyGetters[$className];
    }

    /**
     * @param $object
     *
     * @return array
     */
    private function serializeObject($object)
    {
        $properties = $this->getClassPropertyGetters($object);
        $data = [];

        foreach ($properties as $name => $property) {
            $data[$name] = $this->serializeInternal($object->$property());
        }

        return $data;
    }

    /**
     * @param $array
     *
     * @return array
     */
    private function serializeArray($array)
    {
        $result = [];

        foreach ($array as $key => $value) {
            $result[$key] = $this->serializeInternal($value);
        }

        return $result;
    }
}
