<?php
/**
 * Created by PhpStorm.
 * User: MWA
 * Date: 18/01/2018
 * Time: 19:02.
 */

namespace AtexoDume\Tool;

/**
 * Class Util.
 */
class Util
{
    /**
     * This function will create an HTTP query that may include repeated values.
     *
     * @param $params
     *
     * @return string
     */
    public static function query_suffix($params)
    {
        foreach ($params as $k => $v) {
            if (is_bool($v)) {
                $params[$k] = ($v) ? 'true' : 'false';
            }
        }
        $url = http_build_query($params, null, '&');

        return preg_replace('/%5B(?:[0-9]|[1-9][0-9]+)%5D=/', '=', $url);
    }

    /**
     * verfie si la chaine est un xml.
     *
     * @param string $xml
     *
     * @return bool
     */
    public static function isXml($xml)
    {
        try {
            libxml_use_internal_errors(true);
            $dom = new \DOMDocument();
            $dom->loadXml($xml);
            $errors = libxml_get_errors();
            if (!empty($errors)) {
                print_r($errors);

                return false;
            }
        } catch (\Exception $e) {
            echo $e->getMessage();

            return false;
        }

        return true;
    }
}
