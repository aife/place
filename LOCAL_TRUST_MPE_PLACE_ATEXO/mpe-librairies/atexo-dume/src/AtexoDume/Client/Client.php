<?php

namespace AtexoDume\Client;

use AtexoDume\Service as service;
use AtexoDume\Tool\RestRequest;

/**
 * Class Client.
 */
class Client
{
    private $restReq;
    protected $serverUrl;

    protected $acheteurService;
    protected $entrepriseService;
    protected $authentificationService;
    protected $donneesEssentiellesService;

    protected $username;
    protected $password;
    protected $logger = null;
    protected $proxy = null;

    /**
     * Client constructor.
     *
     * @param $serverUrl
     * @param $username
     * @param $password
     * @param null $logger
     * @param null $proxy
     */
    public function __construct($serverUrl, $username, $password, $logger = null, $proxy = null)
    {
        $this->serverUrl = $serverUrl;
        $this->username = $username;
        $this->password = $password;

        $this->restReq = new RestRequest();
        $this->restReq->setProxy($proxy);

        $this->logger = $logger;
        $this->proxy = $proxy;
    }

    /**
     * Initialisation du service Acheteur.
     *
     * @return service\AcheteurService
     */
    public function acheteurService()
    {
        if (!isset($this->acheteurService)) {
            $this->acheteurService = new service\AcheteurService($this);
        }

        return $this->acheteurService;
    }

    /**
     * Initialisation du service Entreprise.
     *
     * @return service\EntrepriseService
     */
    public function entrepriseService()
    {
        if (!isset($this->entrepriseService)) {
            $this->entrepriseService = new service\EntrepriseService($this);
        }

        return $this->entrepriseService;
    }

    /**
     * Initialisation du service Authentification.
     *
     * @return service\AuthentificationService
     */
    public function authentificationService()
    {
        if (!isset($this->authentificationService)) {
            $this->authentificationService = new service\AuthentificationService($this);
        }

        return $this->authentificationService;
    }

    /**
     * Initialisation du service DonneesEssentielles.
     *
     * @return service\DonneesEssentiellesService
     *
     * @throws \AtexoDume\Exception\ServiceException
     */
    public function donneesEssentiellesService()
    {
        if (!isset($this->donneesEssentiellesService)) {
            $this->donneesEssentiellesService = new service\DonneesEssentiellesService($this);
        }

        return $this->donneesEssentiellesService;
    }

    /**
     * setRequestTimeout
     * Set the amount of time cURL is permitted to wait for a response to a request before timing out.
     *
     * @param $seconds int
     */
    public function setRequestTimeout($seconds)
    {
        $this->restReq->defineTimeout($seconds);
    }

    /**
     * @return string
     */
    public function getURL()
    {
        return $this->serverUrl;
    }

    /**
     * Provides the RestRequest object to be reused by the services that require it.
     *
     * @return RestRequest
     */
    public function getService()
    {
        return $this->restReq;
    }

    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }
}
