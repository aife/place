<?php

namespace AtexoDume\Exception;

class ServiceException extends \Exception
{
    const ERROR_JSON = 'ERROR_JSON';
    const ERROR_REST = 'ERROR_REST';

    const ERROR_CREATION_DUME_ACHETEUR = 'ERROR_CREATION_DUME_ACHETEUR';
    const ERROR_UPDATE_DUME_ACHETEUR = 'ERROR_UPDATE_DUME_ACHETEUR';
    const ERROR_VALIDATION_DUME_ACHETEUR = 'ERROR_VALIDATION_DUME_ACHETEUR';
    const ERROR_PUBLICATION_DUME_ACHETEUR = 'ERROR_PUBLICATION_DUME_ACHETEUR';
    const ERROR_LIST_PUBLISHED_DUME_ACHETEUR = 'ERROR_LIST_PUBLISHED_DUME_ACHETEUR';
    const ERROR_GET_PDF_DUME_ACHETEUR = 'ERROR_GET_PDF_DUME_ACHETEUR';
    const ERROR_GET_XML_DUME_ACHETEUR = 'ERROR_GET_XML_DUME_ACHETEUR';
    const ERROR_REPLACE_PUBLISHED_DUME_ACHETEUR = 'ERROR_REPLACE_PUBLISHED_DUME_ACHETEUR';
    const ERROR_CHECK_PURGE_DUME_ACHETEUR = 'ERROR_CHECK_PURGE_DUME_ACHETEUR';

    const ERROR_CREATION_DUME_ENTREPRISE = 'ERROR_CREATION_DUME_ENTREPRISE';
    const ERROR_UPDATE_DUME_ENTREPRISE = 'ERROR_UPDATE_DUME_ENTREPRISE';
    const ERROR_VALIDATION_DUME_ENTREPRISE = 'ERROR_VALIDATION_DUME_ENTREPRISE';
    const ERROR_PUBLICATION_DUME_ENTREPRISE = 'ERROR_PUBLICATION_DUME_ENTREPRISE';
    const ERROR_LIST_PUBLISHED_DUME_ENTREPRISE = 'ERROR_LIST_PUBLISHED_DUME_ENTREPRISE';
    const ERROR_GET_PDF_DUME_ENTREPRISE = 'ERROR_GET_PDF_DUME_ENTREPRISE';
    const ERROR_GET_XML_DUME_ENTREPRISE = 'ERROR_GET_XML_DUME_ENTREPRISE';
    const ERROR_CHECK_ACCESS_DUME_ENTREPRISE = 'ERROR_CHECK_ACCESS_DUME_ENTREPRISE';
    const ERROR_AUTHENTIFICATION_DUME = 'ERROR_AUTHENTIFICATION_DUME';

    const ERROR_STATUT_PUBLICATION_DONNEES_ESSENTIELLES = 'ERROR_STATUT_PUBLICATION_DONNEES_ESSENTIELLES';

    /**
     * Internal Error Message.
     *
     * @var string
     */
    protected $message;

    /**
     * Error Code.
     *
     * @var string
     */
    protected $code;

    /**
     * Etat Objet.
     *
     * @var string
     */
    protected $etatObjet;

    public function __construct($code, $etatObjet, $message)
    {
        $this->message = $message;
        $this->code = $code;
        $this->etatObjet = $etatObjet;
    }

    public function setMessage(string $message): ServiceException
    {
        $this->message = $message;

        return $this;
    }

    public function setCode(string $code): ServiceException
    {
        $this->code = $code;

        return $this;
    }

    public function getEtatObjet(): string
    {
        return $this->etatObjet;
    }

    public function setEtatObjet(string $etatObjet): ServiceException
    {
        $this->etatObjet = $etatObjet;

        return $this;
    }
}
