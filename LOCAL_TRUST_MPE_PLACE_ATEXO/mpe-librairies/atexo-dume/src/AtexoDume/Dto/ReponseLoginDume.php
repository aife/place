<?php
/**
 * Created by PhpStorm.
 * User: MWA <mohamed.wazni@atexo.com>
 * Date: 06/02/2018
 * Time: 10:45.
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2018
 */

namespace AtexoDume\Dto;

/**
 * Class ReponseLoginDume : permet de véhiculer la réponse retournée par LT-DUME à l'authentification.
 */
class ReponseLoginDume
{
    public function __construct($array = [])
    {
        foreach ($array as $key => $value) {
            switch ($key) {
                case 'access_token':
                    $this->accessToken = $value;
                    break;
                case 'token_type':
                    $this->tokenType = $value;
                    break;
                case 'refresh_token':
                    $this->refreshToken = $value;
                    break;
                case 'expires_in':
                    $this->expiresIn = $value;
                    break;
                case 'scope':
                    $this->scope = $value;
                    break;
            }
        }
    }

    /**
     * le token pour acceder au WS (durée de vie trés faible).
     *
     * @var string
     */
    protected $accessToken = '';

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return string
     */
    public function getTokenType()
    {
        return $this->tokenType;
    }

    /**
     * @param string $tokenType
     */
    public function setTokenType($tokenType)
    {
        $this->tokenType = $tokenType;
    }

    /**
     * token de refresh pour récuperer un autre access_token (durée plus grande).
     *
     * @var string
     */
    protected $refreshToken = '';

    /**
     * délai d'expiration d'acces_token en seconds.
     *
     * @var string
     */
    protected $expiresIn = '';

    /**
     * privilèges donnés au token.
     *
     * @var string
     */
    protected $scope = '';

    /**
     * @return string
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    /**
     * @param string $refreshToken
     */
    public function setRefreshToken($refreshToken)
    {
        $this->refreshToken = $refreshToken;
    }

    /**
     * @return string
     */
    public function getExpiresIn()
    {
        return $this->expiresIn;
    }

    /**
     * @param string $expiresIn
     */
    public function setExpiresIn($expiresIn)
    {
        $this->expiresIn = $expiresIn;
    }

    /**
     * @return string
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @param string $scope
     */
    public function setScope($scope)
    {
        $this->scope = $scope;
    }

    /**
     * type de token (par défaut dans notre cas bearer).
     *
     * @var string
     */
    protected $tokenType = '';

    /**
     * encode l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        return json_encode($this->toArray(), JSON_UNESCAPED_SLASHES);
    }

    /**
     * encode l'objet au format tableau.
     *
     * @return array
     */
    public function toArray()
    {
        return ['access_token' => $this->accessToken, 'token_type' => $this->tokenType, 'refresh_token' => $this->refreshToken, 'expires_in' => $this->expiresIn, 'scope' => $this->scope];
    }
}
