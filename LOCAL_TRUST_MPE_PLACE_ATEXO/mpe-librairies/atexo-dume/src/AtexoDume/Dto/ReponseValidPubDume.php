<?php

namespace AtexoDume\Dto;

use AtexoDume\Tool\JsonSerializer;

/**
 * Class ReponseValidPubDume : permet de véhiculer la réponse retournée par LT-DUME à la validation ou la publication d'un Dume.
 */
class ReponseValidPubDume
{
    const OK = 'OK';
    const ERROR = 'ERROR';
    const LOT_NON_AFFECTE = 'LOT_NON_AFFECTE';

    public function __construct($array = [])
    {
        foreach ($array as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * @var string
     */
    protected $statut = '';

    /**
     * @var bool
     */
    protected $isStandard;

    /**
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @param string $statut
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return bool
     */
    public function isStandard()
    {
        return $this->isStandard;
    }

    /**
     * @param bool $isStandard
     */
    public function setIsStandard($isStandard)
    {
        $this->isStandard = $isStandard;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @var string
     */
    protected $message = '';

    /**
     * encode l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        $serializer = new JsonSerializer();

        return $serializer->serialize($this);
    }
}
