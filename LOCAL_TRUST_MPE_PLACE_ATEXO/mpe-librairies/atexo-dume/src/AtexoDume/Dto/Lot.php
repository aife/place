<?php
/**
 * Created by PhpStorm.
 * User: MWA <mohamed.wazni@atexo.com>
 * Date: 22/01/2018
 * Time: 10:45.
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2018
 */

namespace AtexoDume\Dto;

use AtexoDume\Tool\JsonSerializer;
use AtexoDume\Tool\ValidEnum;

class Lot
{
    /**
     * @var string
     */
    protected $intituleLot = '';

    /**
     * retourne l'intitulé du lot.
     *
     * @return string
     */
    public function getIntituleLot()
    {
        return $this->intituleLot;
    }

    /**
     * définit l'intitulé du lot.
     *
     * @param string $intituleLot
     */
    public function setIntituleLot($intituleLot)
    {
        $this->intituleLot = $intituleLot;
    }

    /**
     * @var int
     */
    protected $numeroLot = '';

    /**
     * retourne le numéro de lot.
     *
     * @return int
     */
    public function getNumeroLot()
    {
        return $this->numeroLot;
    }

    /**
     * définit le numéro du lot.
     *
     * @param int $numeroLot
     */
    public function setNumeroLot($numeroLot)
    {
        $this->numeroLot = $numeroLot;
    }

    /**
     * encode l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        $serializer = new JsonSerializer();

        return $serializer->serialize($this);
    }

    /**
     * @return string
     */
    public function isValid()
    {
        if (empty($this->intituleLot) || empty($this->numeroLot)) {
            return ValidEnum::ERROR_LOT;
        }

        return ValidEnum::VALID;
    }
}
