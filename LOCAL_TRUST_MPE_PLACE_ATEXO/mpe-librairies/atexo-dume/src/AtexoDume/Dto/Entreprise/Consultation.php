<?php
/**
 * Created by PhpStorm.
 * User: MWA <mohamed.wazni@atexo.com>
 * Date: 22/01/2018
 * Time: 10:45.
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2018
 */

namespace AtexoDume\Dto\Entreprise;

use AtexoDume\Tool\JsonSerializer;
use AtexoDume\Tool\ValidEnum;

class Consultation
{
    /**
     * metadata entreprise de la consultation.
     *
     * @var \AtexoDume\Dto\Entreprise\Metadata|null
     */
    protected $metadata = null;

    /**
     * liste des lots.
     *
     * @var array
     */
    protected $lots = [];

    /**
     * contact OE pour le DUME.
     *
     * @var \AtexoDume\Dto\Entreprise\Contact|null
     */
    protected $contact = null;

    /**
     * @return \AtexoDume\Dto\Entreprise\Metadata|null
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param \AtexoDume\Dto\Entreprise\Metadata $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return array
     */
    public function getLots()
    {
        return $this->lots;
    }

    /**
     * @param array $lots
     */
    public function setLots($lots)
    {
        $this->lots = $lots;
    }

    /**
     * @return \AtexoDume\Dto\Entreprise\Contact|null
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param \AtexoDume\Dto\Entreprise\Contact $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * encode l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        $serializer = new JsonSerializer();

        return $serializer->serialize($this);
    }

    /**
     * @return string
     */
    public function isValid()
    {
        if (null == $this->getMetadata()) {
            return ValidEnum::ERROR_CONSULTATION_ENTREPRISE;
        }

        if (ValidEnum::ERROR_METADATA_ENTREPRISE == $this->getMetadata()->isValid() &&
            ValidEnum::ERROR_CONTACT_ENTREPRISE == $this->getContact()->isValid()
        ) {
            return ValidEnum::ERROR_CONSULTATION_ENTREPRISE;
        }

        $lotsArray = $this->getLots();

        if (count($lotsArray) > 0) {
            foreach ($lotsArray as $item) {
                if (ValidEnum::ERROR_LOT == $item->isValid()) {
                    return ValidEnum::ERROR_LOT;
                }
            }
        }

        return ValidEnum::VALID;
    }
}
