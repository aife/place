<?php
/**
 * Created by PhpStorm.
 * User: MWA <mohamed.wazni@atexo.com>
 * Date: 22/01/2018
 * Time: 10:45.
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2018
 */

namespace AtexoDume\Dto\Entreprise;

use AtexoDume\Tool\JsonSerializer;
use AtexoDume\Tool\ValidEnum;

class Metadata
{
    /**
     * identifiant unique du contexte dume Acheteur.
     *
     * @var int
     */
    protected $idContextDumeAcheteur;

    /**
     * identifiant de la plateforme.
     *
     * @var string
     */
    protected $identifiantPlateforme;

    /**
     * identifiant unique de la consultation.
     *
     * @var int
     */
    protected $idConsultation;

    /**
     * siret de l'inscrit qui répond à la consultation.
     *
     * @var int
     */
    protected $siret;

    /**
     * identifiant unique de l'inscrit qui répond à la consultation.
     *
     * @var int
     */
    protected $idInscrit;

    /**
     * identifiant si une entreprise est étrangère ou non.
     *
     * @var string
     */
    protected $typeOE;

    /**
     * identifiant si c'est la première creation d'un dumeOE.
     *
     * @var bool
     */
    protected $first = false;

    /**
     * @return int
     */
    public function getIdContextDumeAcheteur()
    {
        return $this->idContextDumeAcheteur;
    }

    /**
     * @param int $idContextDumeAcheteur
     */
    public function setIdContextDumeAcheteur($idContextDumeAcheteur)
    {
        $this->idContextDumeAcheteur = $idContextDumeAcheteur;
    }

    /**
     * @return string
     */
    public function getIdentifiantPlateforme()
    {
        return $this->identifiantPlateforme;
    }

    /**
     * @param string $identifiantPlateforme
     */
    public function setIdentifiantPlateforme($identifiantPlateforme)
    {
        $this->identifiantPlateforme = $identifiantPlateforme;
    }

    /**
     * @return int
     */
    public function getIdConsultation()
    {
        return $this->idConsultation;
    }

    /**
     * @param int $idConsultation
     */
    public function setIdConsultation($idConsultation)
    {
        $this->idConsultation = $idConsultation;
    }

    /**
     * @return int
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * @param int $siret
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;
    }

    /**
     * @return int
     */
    public function getIdInscrit()
    {
        return $this->idInscrit;
    }

    /**
     * @param int $idInscrit
     */
    public function setIdInscrit($idInscrit)
    {
        $this->idInscrit = $idInscrit;
    }

    /**
     * @return string
     */
    public function getTypeOE()
    {
        return $this->typeOE;
    }

    /**
     * @param string $typeOE
     */
    public function setTypeOE($typeOE)
    {
        $this->typeOE = $typeOE;
    }

    /**
     * @return bool
     */
    public function isFirst()
    {
        return $this->first;
    }

    /**
     * @param bool $first
     */
    public function setFirst($first)
    {
        $this->first = $first;
    }

    /**
     * encode l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        $serializer = new JsonSerializer();

        return $serializer->serialize($this);
    }

    /**
     * @return string
     */
    public function isValid()
    {
        if (empty($this->identifiantPlateforme) ||
            empty($this->idConsultation) ||
            empty($this->idContextDumeAcheteur) ||
            empty($this->idInscrit) ||
            empty($this->siret) ||
            empty($this->typeOE)
        ) {
            return ValidEnum::ERROR_METADATA_ENTREPRISE;
        }

        return ValidEnum::VALID;
    }
}
