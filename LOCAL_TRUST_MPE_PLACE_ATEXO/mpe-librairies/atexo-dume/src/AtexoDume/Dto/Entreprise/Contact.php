<?php

namespace AtexoDume\Dto\Entreprise;

use AtexoDume\Tool\JsonSerializer;
use AtexoDume\Tool\ValidEnum;

class Contact
{
    /**
     * personne à contacter (concat "prenom" + "nom").
     *
     * @var string
     */
    protected $nom;

    /**
     * @var string
     */
    protected $telephone;

    /**
     * @var string
     */
    protected $email;

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     *
     * @return $this
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     *
     * @return $this
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * encode l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        $serializer = new JsonSerializer();

        return $serializer->serialize($this);
    }

    /**
     * @return string
     */
    public function isValid()
    {
        if (empty($this->nom) || empty($this->telephone) || empty($this->email)) {
            return ValidEnum::ERROR_CONTACT_ENTREPRISE;
        }

        return ValidEnum::VALID;
    }
}
