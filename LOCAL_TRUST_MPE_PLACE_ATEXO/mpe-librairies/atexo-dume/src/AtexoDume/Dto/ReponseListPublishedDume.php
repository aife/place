<?php
/**
 * Created by PhpStorm.
 * User: MWA <mohamed.wazni@atexo.com>
 * Date: 22/01/2018
 * Time: 10:45.
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2018
 */

namespace AtexoDume\Dto;

use AtexoDume\Tool\JsonSerializer;

/**
 * Class ReponseListPublishedDume : permet de véhiculer la réponse retournée par LT-DUME
 * pour avoir la liste de tous les numéros de Dumes publiés pour un contexte Dume donné.
 */
class ReponseListPublishedDume
{
    const OK = 'OK';
    const ERROR = 'ERROR_INTERNAL';
    const LOT_NON_AFFECTE = 'ERROR_SERVICE_NATIONAL';

    /**
     * @var string
     */
    protected $statut = '';

    /**
     * liste des numéros Dume.
     *
     * @var array
     */
    protected $numerosDume = [];

    /**
     * @var bool
     */
    protected $isStandard = true;

    /**
     * ReponseListPublishedDume constructor.
     *
     * @param array $array
     */
    public function __construct($array = [])
    {
        foreach ($array as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @param string $statut
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;
    }

    /**
     * @return array
     */
    public function getNumerosDume()
    {
        return $this->numerosDume;
    }

    /**
     * @param array $numerosDume
     */
    public function setNumerosDume($numerosDume)
    {
        $this->numerosDume = $numerosDume;
    }

    /**
     * @return bool
     */
    public function getIsStandard()
    {
        return $this->isStandard;
    }

    /**
     * @param bool $isStandard
     */
    public function setIsStandard($isStandard)
    {
        $this->isStandard = $isStandard;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * encode l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        $serializer = new JsonSerializer();

        return $serializer->serialize($this);
    }
}
