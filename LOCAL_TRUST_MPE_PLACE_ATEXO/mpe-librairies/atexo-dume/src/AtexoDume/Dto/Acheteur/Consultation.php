<?php
/**
 * Created by PhpStorm.
 * User: MWA <mohamed.wazni@atexo.com>
 * Date: 22/01/2018
 * Time: 10:45.
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2018
 */

namespace AtexoDume\Dto\Acheteur;

use Application\Service\Atexo\Atexo_Config;
use AtexoDume\Tool\JsonSerializer;
use AtexoDume\Tool\ValidEnum;

class Consultation
{
    /**
     * intitulé de la consultation.
     *
     * @var string
     */
    protected $intitule = '';

    /**
     * objet de la consultation.
     *
     * @var string
     */
    protected $objet = '';

    /**
     * référence de la consultation.
     *
     * @var string
     */
    protected $referenceConsultation = '';

    /**
     * type procédure DUME de la consultation.
     *
     * @var string
     */
    protected $typeProcedure = '';

    /**
     * metadata acheteur de la consultation.
     *
     * @var Metadata
     */
    protected $metadata = null;

    /**
     * liste des lots.
     *
     * @var array
     */
    protected $lots = [];

    /**
     * nom de l'entité d'achat qui passe la consultation.
     *
     * @var string
     */
    protected $nomOfficiel = '';

    /**
     * pays l'entité d'achat qui passe la consultation
     * par défaut, vaut FR.
     *
     * @var string
     */
    protected $pays = '';

    public function __construct()
    {
        $this->pays = Atexo_Config::getParameter('DUME_COUNTRY');
    }

    /**
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * @param string $intitule
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;
    }

    /**
     * @return string
     */
    public function getObjet()
    {
        return $this->objet;
    }

    /**
     * @param string $objet
     */
    public function setObjet($objet)
    {
        $this->objet = $objet;
    }

    /**
     * @return string
     */
    public function getReferenceConsultation()
    {
        return $this->referenceConsultation;
    }

    /**
     * @param string $referenceConsultation
     */
    public function setReferenceConsultation($referenceConsultation)
    {
        $this->referenceConsultation = $referenceConsultation;
    }

    /**
     * @return string
     */
    public function getTypeProcedure()
    {
        return $this->typeProcedure;
    }

    /**
     * @param string $typeProcedure
     */
    public function setTypeProcedure($typeProcedure)
    {
        $this->typeProcedure = $typeProcedure;
    }

    /**
     * @return Metadata
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param Metadata $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return array
     */
    public function getLots()
    {
        return $this->lots;
    }

    /**
     * @param array $lots
     */
    public function setLots($lots)
    {
        $this->lots = $lots;
    }

    /**
     * @return string
     */
    public function getNomOfficiel()
    {
        return $this->nomOfficiel;
    }

    /**
     * @param string $nomOfficiel
     */
    public function setNomOfficiel($nomOfficiel)
    {
        $this->nomOfficiel = $nomOfficiel;
    }

    /**
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * @param string $pays
     */
    public function setPays($pays)
    {
        $this->pays = $pays;
    }

    /**
     * encode l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        $serializer = new JsonSerializer();

        return $serializer->serialize($this);
    }

    /**
     * @return string
     */
    public function isValid()
    {
        if (empty($this->intitule) || empty($this->nomOfficiel) || empty($this->objet) || empty($this->pays)
            || empty($this->referenceConsultation) || empty($this->typeProcedure)) {
            return ValidEnum::ERROR_CONSULTATION_ACHETEUR;
        }
        if (null == $this->getMetadata()) {
            return ValidEnum::ERROR_CONSULTATION_ACHETEUR;
        }
        if (ValidEnum::ERROR_METADATA_ACHETEUR == $this->getMetadata()->isValid()) {
            return ValidEnum::ERROR_CONSULTATION_ACHETEUR;
        }

        $lotsArray = $this->getLots();
        if (count($lotsArray) > 0) {
            foreach ($lotsArray as $item) {
                if (ValidEnum::ERROR_LOT == $item->isValid()) {
                    return ValidEnum::ERROR_LOT;
                }
            }
        }

        return ValidEnum::VALID;
    }
}
