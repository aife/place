<?php
/**
 * Created by PhpStorm.
 * User: MWA <mohamed.wazni@atexo.com>
 * Date: 22/01/2018
 * Time: 10:45.
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2018
 */

namespace AtexoDume\Dto\Acheteur;

use AtexoDume\Tool\JsonSerializer;
use AtexoDume\Tool\ValidEnum;

class Metadata
{
    /**
     * date limite de remise des plis de la consultation.
     *
     * @var string
     */
    protected $dateLimiteRemisePlis;
    /**
     * identifiant de la plateforme.
     *
     * @var string
     */
    protected $identifiantPlateforme;

    /**
     * trigramme de l'organisme de rattachement de la consultation.
     *
     * @var string
     */
    protected $organisme;

    /**
     * identifiant unique de la consultation.
     *
     * @var string
     */
    protected $idConsultation;

    /**
     * identifiant unique de la consultation.
     *
     * @var string
     */
    protected $siret;

    /**
     * code type procedure dume de la consultation.
     *
     * @var string
     */
    protected $idTypeProcedure;

    /**
     * code type contrat dume de la consultation.
     *
     * @var string
     */
    protected $idTypeMarche;

    /**
     * code nature d'achat dume de la consultation.
     *
     * @var string
     */
    protected $idNatureMarket;

    /**
     * type formulaire dume.
     *
     * @var bool
     */
    protected $simplifie;

    /**
     * @return string
     */
    public function getDateLimiteRemisePlis()
    {
        return $this->dateLimiteRemisePlis;
    }

    /**
     * @param string $dateLimiteRemisePlis
     * @format aaaa-mm-jjThh:mm
     */
    public function setDateLimiteRemisePlis($dateLimiteRemisePlis)
    {
        $this->dateLimiteRemisePlis = $dateLimiteRemisePlis;
    }

    /**
     * @return string
     */
    public function getIdentifiantPlateforme()
    {
        return $this->identifiantPlateforme;
    }

    /**
     * @param string $identifiantPlateforme
     */
    public function setIdentifiantPlateforme($identifiantPlateforme)
    {
        $this->identifiantPlateforme = $identifiantPlateforme;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param string $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return string
     */
    public function getIdConsultation()
    {
        return $this->idConsultation;
    }

    /**
     * @param string $idConsultation
     */
    public function setIdConsultation($idConsultation)
    {
        $this->idConsultation = $idConsultation;
    }

    /**
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * @param string $siret
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;
    }

    /**
     * @return string
     */
    public function getIdTypeProcedure()
    {
        return $this->idTypeProcedure;
    }

    /**
     * @param string $idTypeProcedure
     */
    public function setIdTypeProcedure($idTypeProcedure)
    {
        $this->idTypeProcedure = $idTypeProcedure;
    }

    /**
     * @return string
     */
    public function getIdTypeMarche()
    {
        return $this->idTypeMarche;
    }

    /**
     * @param string $idTypeMarche
     */
    public function setIdTypeMarche($idTypeMarche)
    {
        $this->idTypeMarche = $idTypeMarche;
    }

    /**
     * @return string
     */
    public function getIdNatureMarket()
    {
        return $this->idNatureMarket;
    }

    /**
     * @param string $idNatureMarket
     */
    public function setIdNatureMarket($idNatureMarket)
    {
        $this->idNatureMarket = $idNatureMarket;
    }

    /**
     * @return bool
     */
    public function isSimplifie()
    {
        return $this->simplifie;
    }

    /**
     * @param bool $simplifie
     */
    public function setSimplifie($simplifie)
    {
        $this->simplifie = $simplifie;
    }

    /**
     * encode l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        $serializer = new JsonSerializer();

        return $serializer->serialize($this);
    }

    /**
     * @return string
     */
    public function isValid()
    {
        if (empty($this->organisme) || empty($this->idConsultation) || empty($this->identifiantPlateforme)
            || empty($this->idTypeProcedure) || empty($this->idTypeMarche) || empty($this->idNatureMarket)) {
            return ValidEnum::ERROR_METADATA_ACHETEUR;
        }

        return ValidEnum::VALID;
    }
}
