<?php
/**
 * Created by PhpStorm.
 * User: MWA <mohamed.wazni@atexo.com>
 * Date: 22/01/2018
 * Time: 10:45.
 *
 * @version 1.0
 *
 * @since develop
 *
 * @copyright Atexo 2018
 */

namespace AtexoDume\Dto;

use AtexoDume\Tool\JsonSerializer;

/**
 * Class NumeroDume : permet de véhiculer la réponse retournée par LT-DUME quand
 * on demande la liste des Dumes publiés pour un contexte Dume donné.
 */
class NumeroDume
{
    /**
     * @var string
     */
    protected $numeroDumeSN = '';

    /**
     * @var string
     */
    protected $numeroLot = '';

    /**
     * NumeroDume constructor.
     *
     * @param array $array
     */
    public function __construct($array = [])
    {
        foreach ($array as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getNumeroDumeSN()
    {
        return $this->numeroDumeSN;
    }

    /**
     * @param string $numeroDumeSN
     */
    public function setNumeroDumeSN($numeroDumeSN)
    {
        $this->numeroDumeSN = $numeroDumeSN;
    }

    /**
     * @return string
     */
    public function getNumeroLot()
    {
        return $this->numeroLot;
    }

    /**
     * @param string $numeroLot
     */
    public function setNumeroLot($numeroLot)
    {
        $this->numeroLot = $numeroLot;
    }

    /**
     * encode l'objet en json.
     *
     * @return string
     */
    public function toJSON()
    {
        $serializer = new JsonSerializer();

        return $serializer->serialize($this);
    }
}
