<?php
/**
 * Created by PhpStorm.
 * User: MWA
 * Date: 22/01/2018
 * Time: 11:06.
 */
require_once __DIR__.'/../../vendor/autoload.php';

class ReponseValidPubDumeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test de la méthode constructeur().
     */
    public function testCreateObject()
    {
        $reponse = new AtexoDume\Dto\ReponseValidPubDume(['message' => 'Validation / Publication du DUME REALISEE', 'statut' => 'OK']);
        $this->assertJsonStringEqualsJsonString($reponse->toJSON(), '{"message":"Validation / Publication du DUME REALISEE","statut" : "OK"}');
    }

    /**
     * Test de la méthode toJSON().
     */
    public function testToJSON()
    {
        $lot = AtexoDume\Tool\TestUtils::createReponseValidPubDume();
        $this->assertJsonStringEqualsJsonString($lot->toJSON(), '{"message":"Validation / Publication du DUME REALISEE","statut" : "OK"}');
    }
}
