<?php
/**
 * Created by PhpStorm.
 * User: MWA
 * Date: 22/01/2018
 * Time: 11:06.
 */
require_once __DIR__.'/../../vendor/autoload.php';

class MetadataAcheteurTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test de la méthode toJSON().
     */
    public function testToJSON()
    {
        $metadata = AtexoDume\Tool\TestUtils::createMetadataAcheteur();
        $this->assertJsonStringEqualsJsonString($metadata->toJSON(), '{"dateLimiteRemisePlis":"2018-01-29T17:30","identifiantPlateforme":"test","organisme":"t5y","idConsultation":"1"}');
    }

    /**
     * Test de la méthode isValid() avec Metadata non Vide.
     */
    public function testValidYes()
    {
        $metadata = AtexoDume\Tool\TestUtils::createMetadataAcheteur();
        $this->assertEquals(\AtexoDume\Tool\ValidEnum::VALID, $metadata->isValid());
    }

    /**
     * Test de la méthode isValid() avec Metadata vide.
     */
    public function testValidError()
    {
        $metadata = new AtexoDume\Dto\Acheteur\Metadata();
        $this->assertEquals(\AtexoDume\Tool\ValidEnum::ERROR_METADATA_ACHETEUR, $metadata->isValid());
    }
}
