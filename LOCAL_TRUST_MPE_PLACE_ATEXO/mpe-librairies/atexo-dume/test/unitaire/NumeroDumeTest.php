<?php
/**
 * Created by PhpStorm.
 * User: MWA
 * Date: 22/01/2018
 * Time: 11:06.
 */
require_once __DIR__.'/../../vendor/autoload.php';

class NumeroDumeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test de la méthode constructeur().
     */
    public function testCreateObject()
    {
        $reponse = new AtexoDume\Dto\NumeroDume(['numeroDumeSN' => '1234560', 'numeroLot' => '1', 'isStandard' => 'true']);
        $this->assertJsonStringEqualsJsonString($reponse->toJSON(), '{"numeroDumeSN":"1234560","numeroLot" : "1","isStandard" : "true"}');
    }

    /**
     * Test de la méthode toJSON().
     */
    public function testToJSON()
    {
        $objet = AtexoDume\Tool\TestUtils::createNumeroDume();
        $this->assertJsonStringEqualsJsonString($objet->toJSON(), '{"numeroDumeSN":"1234560","numeroLot" : "1","isStandard" : "true"}');
    }
}
