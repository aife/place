<?php
/**
 * Created by PhpStorm.
 * User: MWA
 * Date: 22/01/2018
 * Time: 11:06.
 */
require_once __DIR__.'/../../vendor/autoload.php';

class ReponseLoginDumeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test de la méthode constructeur().
     */
    public function testCreateObject()
    {
        $reponse = new AtexoDume\Dto\ReponseLoginDume(['access_token' => '3483205e-857a-430e-bf8d-67108ff74a0d', 'token_type' => 'bearer', 'refresh_token' => '91c20744-c9dc-4dce-a73b-f44c03082aa2', 'expires_in' => '120', 'scope' => 'read write trust']);
        $this->assertJsonStringEqualsJsonString($reponse->toJSON(), '{"access_token": "3483205e-857a-430e-bf8d-67108ff74a0d", "token_type": "bearer", "refresh_token": "91c20744-c9dc-4dce-a73b-f44c03082aa2", "expires_in": 120, "scope": "read write trust"}');
    }

    /**
     * Test de la méthode toJSON().
     */
    public function testToJSON()
    {
        $reponse = AtexoDume\Tool\TestUtils::createReponseLoginDume();
        $this->assertJsonStringEqualsJsonString($reponse->toJSON(), '{"access_token": "3483205e-857a-430e-bf8d-67108ff74a0d", "token_type": "bearer", "refresh_token": "91c20744-c9dc-4dce-a73b-f44c03082aa2", "expires_in": 120, "scope": "read write trust"}');
    }
}
