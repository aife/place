<?php
/**
 * Created by PhpStorm.
 * User: MWA
 * Date: 22/01/2018
 * Time: 11:06.
 */
require_once __DIR__.'/../../vendor/autoload.php';

class ConsultationEntrepriseTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test de la méthode toJSON().
     */
    public function testToJSON()
    {
        $consultation = AtexoDume\Tool\TestUtils::createConsultationEntreprise();
        $this->assertJsonStringEqualsJsonString($consultation->toJSON(), '{"metadata" :{"idContextDumeAcheteur": "1234567890","siret" :"11122233300033","identifiantPlateforme":"test","idInscrit":"20","idConsultation":"123456"},"lots" :[{"intituleLot":"Intitule du Lot 1","numeroLot":"1"},{"intituleLot":"Intitule du Lot 2","numeroLot":"2"}]}');
    }

    /**
     * Test de la méthode isValid() avec Consultation non Vide.
     */
    public function testValidYes()
    {
        $consultation = AtexoDume\Tool\TestUtils::createConsultationEntreprise();
        $this->assertEquals(\AtexoDume\Tool\ValidEnum::VALID, $consultation->isValid());
    }

    /**
     * Test de la méthode isValid() avec Consultation vide.
     */
    public function testValidErrorConsultation()
    {
        $consultation = new AtexoDume\Dto\Entreprise\Consultation();
        $this->assertEquals(\AtexoDume\Tool\ValidEnum::ERROR_CONSULTATION_ENTREPRISE, $consultation->isValid());
    }

    /**
     * Test de la méthode isValid() avec Metadata vide.
     */
    public function testValidErrorMetadata()
    {
        $consultation = new AtexoDume\Dto\Entreprise\Consultation();
        $this->assertEquals(\AtexoDume\Tool\ValidEnum::ERROR_CONSULTATION_ENTREPRISE, $consultation->isValid());
    }
}
