<?php
/**
 * Created by PhpStorm.
 * User: MWA
 * Date: 22/01/2018
 * Time: 11:06.
 */
require_once __DIR__.'/../../vendor/autoload.php';

class MetadataEntrepriseTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test de la méthode toJSON().
     */
    public function testToJSON()
    {
        $metadata = AtexoDume\Tool\TestUtils::createMetadataEntreprise();
        $this->assertJsonStringEqualsJsonString($metadata->toJSON(), '{"idContextDumeAcheteur": "1234567890","siret" :"11122233300033","identifiantPlateforme":"test","idInscrit":"20","idConsultation":"123456"}');
    }

    /**
     * Test de la méthode isValid() avec Metadata non Vide.
     */
    public function testValidYes()
    {
        $metadata = AtexoDume\Tool\TestUtils::createMetadataEntreprise();
        $this->assertEquals(\AtexoDume\Tool\ValidEnum::VALID, $metadata->isValid());
    }

    /**
     * Test de la méthode isValid() avec Metadata vide.
     */
    public function testValidError()
    {
        $metadata = new AtexoDume\Dto\Entreprise\Metadata();
        $this->assertEquals(\AtexoDume\Tool\ValidEnum::ERROR_METADATA_ENTREPRISE, $metadata->isValid());
    }
}
