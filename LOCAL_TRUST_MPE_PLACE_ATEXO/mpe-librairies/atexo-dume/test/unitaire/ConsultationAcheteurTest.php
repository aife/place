<?php
/**
 * Created by PhpStorm.
 * User: MWA
 * Date: 22/01/2018
 * Time: 11:06.
 */
require_once __DIR__.'/../../vendor/autoload.php';

class ConsultationAcheteurTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test de la méthode toJSON().
     */
    public function testToJSON()
    {
        $consultation = AtexoDume\Tool\TestUtils::createConsultationAcheteur();
        $this->assertJsonStringEqualsJsonString($consultation->toJSON(), '{"intitule": "Intitulé de la consultation de test","nomOfficiel": "Ministère de test","objet": "Travaux de rénovation de la façade de l\'école St-Vincent","pays": "FR","reference": "CONS-TEST-UNIT", "typeProcedure":"ouverte","metadata" :{"dateLimiteRemisePlis":"2018-01-29T17:30","identifiantPlateforme":"test","organisme":"t5y","idConsultation":"1"},"lots" :[{"intituleLot":"Intitule du Lot 1","numeroLot":"1"},{"intituleLot":"Intitule du Lot 2","numeroLot":"2"}]}');
    }

    /**
     * Test de la méthode isValid() avec Consultation non Vide.
     */
    public function testValidYes()
    {
        $consultation = AtexoDume\Tool\TestUtils::createConsultationAcheteur();
        $this->assertEquals(\AtexoDume\Tool\ValidEnum::VALID, $consultation->isValid());
    }

    /**
     * Test de la méthode isValid() avec Consultation vide.
     */
    public function testValidErrorConsultation()
    {
        $consultation = new AtexoDume\Dto\Acheteur\Consultation();
        $this->assertEquals(\AtexoDume\Tool\ValidEnum::ERROR_CONSULTATION_ACHETEUR, $consultation->isValid());
    }
}
