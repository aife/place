<?php
/**
 * Created by PhpStorm.
 * User: MWA
 * Date: 22/01/2018
 * Time: 11:06.
 */
require_once __DIR__.'/../../vendor/autoload.php';

/**
 * Class ReponseListPublishedDumeTest.
 */
class ReponseListPublishedDumeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test de la méthode constructeur().
     */
    public function testCreateObject()
    {
        $reponse = new AtexoDume\Dto\ReponseListPublishedDume(
            [
                'statut' => 'OK',
                'numerosDume' => [
                    [
                        'numeroDumeSN' => '1234560',
                        'numeroLot' => '1',
                    ],
                    [
                        'numeroDumeSN' => '7891011',
                        'numeroLot' => '2',
                    ],
                ],
                'isStandard' => 'true',
            ]
        );

        $this->assertJsonStringEqualsJsonString(
            $reponse->toJSON(),
            '{"statut":"OK","numerosDume":[{"numeroDumeSN":"1234560","numeroLot":"1"},{"numeroDumeSN":"7891011","numeroLot":"2"}],"isStandard":"true"}'
        );
    }

    /**
     * Test de la méthode constructeur à partir d'une réponse au format JSON.
     */
    public function testCreateObjectFromJSON()
    {
        $responseJson = '{"statut":"OK","numerosDume":[{"numeroDumeSN":"1234560","numeroLot":"1"},{"numeroDumeSN":"7891011","numeroLot":"2"}],"isStandard":"true"}';
        $reponseListPublishedDume = new \AtexoDume\Dto\ReponseListPublishedDume(json_decode($responseJson, true));

        $this->assertJsonStringEqualsJsonString(
            $reponseListPublishedDume->toJSON(),
            '{"statut":"OK","numerosDume":[{"numeroDumeSN":"1234560","numeroLot":"1"},{"numeroDumeSN":"7891011","numeroLot":"2"}],"isStandard":"true"}'
        );
    }

    /**
     * Test de la méthode toJSON().
     */
    public function testToJSON()
    {
        $listNumeroDume = AtexoDume\Tool\TestUtils::createReponseListPublishedDume();

        $this->assertJsonStringEqualsJsonString(
            $listNumeroDume->toJSON(),
            '{"statut":"OK","numerosDume":[{"numeroDumeSN":"1234560","numeroLot":"1"},{"numeroDumeSN":"7891011","numeroLot":"2"}],"isStandard":false}'
        );
    }
}
