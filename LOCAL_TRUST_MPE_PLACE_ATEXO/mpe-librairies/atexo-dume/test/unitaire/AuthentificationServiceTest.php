<?php
/**
 * Created by PhpStorm.
 * User: MWA
 * Date: 22/01/2018
 * Time: 11:06.
 */
require_once __DIR__.'/../../vendor/autoload.php';
use Monolog\Logger;

class AuthentificationServiceTest extends \PHPUnit_Framework_TestCase
{
    protected $logger;

    public function __construct()
    {
        $this->logger = new Logger('AuthentificationService');
    }

    /**
     * Test de la méthode createDume().
     */
    public function testLoginValid()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );

        $result = $c->authentificationService()->login($_ENV['LOGIN'], $_ENV['PWD']);

        $this->assertNotEmpty($result->getAccessToken());
    }

    /**
     * Test de la méthode createDume().
     *
     * @expectedException \AtexoDume\Exception\ServiceException
     */
    public function testLoginError()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $this->logger,
            null
        );

        $c->authentificationService()->login();
    }

    /**
     * Test de la méthode createDume().
     *
     * @expectedException \AtexoDume\Exception\ServiceException
     */
    public function testLoginErrorDistante()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $this->logger,
            null
        );

        $c->authentificationService()->login('wronglogin', 'wrongpassword');
    }
}
