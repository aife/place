<?php
/**
 * Created by PhpStorm.
 * User: MWA
 * Date: 22/01/2018
 * Time: 11:06.
 */
require_once __DIR__.'/../../vendor/autoload.php';
use AtexoDume\Exception\ServiceException;
use Monolog\Logger;

class AcheteurServiceTest extends \PHPUnit_Framework_TestCase
{
    protected $logger;

    public function __construct()
    {
        $this->logger = new Logger('AcheteurService');
    }

    /**
     * Test de la méthode createDume().
     */
    public function testCreateDumeValid()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );
        $consultation = AtexoDume\Tool\TestUtils::createConsultationAcheteur();

        $results = $c->acheteurService()->createDume($consultation);
        print_r($results);
        $this->assertNotEmpty($results);
    }

    /**
     * Test de la méthode createDume() avec un cas d'erreur (consultation vide).
     */
    public function testCreateDumeError()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );
        $consultation = new \AtexoDume\Dto\Acheteur\Consultation();
        try {
            $results = $c->acheteurService()->createDume($consultation);
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_JSON, $ex->getCode());
        }
    }

    /**
     * Test de la méthode createDume() avec un cas d'erreur déclenchée par les WS LT-DUME.
     */
    public function testCreateDumeErrorDistante()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );

        $metadata = AtexoDume\Tool\TestUtils::createMetadataAcheteur();
        $metadata->setDateLimiteRemisePlis('22-01-2017');

        $consultation = AtexoDume\Tool\TestUtils::createConsultationAcheteur();

        $consultation->setMetadata($metadata);

        try {
            $results = $c->acheteurService()->createDume($consultation);
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_REST, $ex->getCode());
        }
    }

    /**
     * Test de la méthode updateDume().
     */
    public function testUpdateDume()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );
        $consultation = AtexoDume\Tool\TestUtils::createConsultationAcheteur();

        $results = $c->acheteurService()->createDume($consultation);

        print_r('IdContextDumeAcheteur cree='.$results);
        $this->assertNotEmpty($results);

        $consultation->setTypeProcedure('restreinte');
        $resultsUpdate = $c->acheteurService()->updateDume($consultation, $results);
        print_r('IdContextDumeAcheteur update='.$resultsUpdate);
        $this->assertEquals($results, $resultsUpdate);
    }

    /**
     * Test de la méthode updateDume() avec un cas d'erreur (idContextDumeAcheteur vide).
     */
    public function testUpdateDumeError()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );
        $consultation = new \AtexoDume\Dto\Acheteur\Consultation();
        try {
            $results = $c->acheteurService()->updateDume($consultation);
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_UPDATE_DUME_ACHETEUR, $ex->getCode());
        }
    }

    /**
     * Test de la méthode updateDume() avec un cas d'erreur déclenchée par les WS LT-DUME.
     */
    public function testUpdateDumeErrorDistante()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );
        $consultation = AtexoDume\Tool\TestUtils::createConsultationAcheteur();
        try {
            $results = $c->acheteurService()->updateDume($consultation, 100000000);
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_REST, $ex->getCode());
        }
    }

    /**
     * Test de la méthode validateDume().
     */
    public function testValidateDume()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );
        $consultation = AtexoDume\Tool\TestUtils::createConsultationAcheteur();

        $idContextDumeAcheteur = $c->acheteurService()->createDume($consultation);

        $reponse = $c->acheteurService()->validateDume($idContextDumeAcheteur);
        $this->assertEquals($reponse->getStatut(), \AtexoDume\Tool\ValidEnum::VALID);
    }

    /**
     * Test de la méthode validateDume() mais avec un blocage en local.
     */
    public function testValidateDumeError()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );

        try {
            $reponse = $c->acheteurService()->validateDume();
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_VALIDATION_DUME_ACHETEUR, $ex->getCode());
        }
    }

    /**
     * Test de la méthode validateDume() avec un cas d'erreur déclenchée par les WS LT-DUME.
     */
    public function testValidateDumeErrorDistante()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );

        try {
            $reponse = $c->acheteurService()->validateDume(100000000);
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_REST, $ex->getCode());
        }
    }

    /**
     * Test de la méthode publishDume().
     */
    public function testPublishDume()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );
        $consultation = AtexoDume\Tool\TestUtils::createConsultationAcheteur();

        $idContextDumeAcheteur = $c->acheteurService()->createDume($consultation);

        $reponse = $c->acheteurService()->validateDume($idContextDumeAcheteur);
        $this->assertNotEmpty($reponse);

        $reponse = $c->acheteurService()->publishDume($idContextDumeAcheteur);
        $this->assertNotEmpty($reponse);
    }

    /**
     * Test de la méthode publishDume() mais avec un blocage en local.
     */
    public function testPublishDumeError()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );

        try {
            $reponse = $c->acheteurService()->publishDume();
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_VALIDATION_DUME_ACHETEUR, $ex->getCode());
        }
    }

    /**
     * Test de la méthode validateDume() avec un cas d'erreur déclenchée par les WS LT-DUME.
     */
    public function testPublishDumeErrorDistante()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );

        try {
            $reponse = $c->acheteurService()->publishDume(100000000);
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_REST, $ex->getCode());
        }
    }

    /**
     * Test de la méthode getPublishedDume().
     */
    public function testGetPublishedDume()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );
        $consultation = AtexoDume\Tool\TestUtils::createConsultationAcheteur();

        $idContextDumeAcheteur = $c->acheteurService()->createDume($consultation);

        $reponse = $c->acheteurService()->validateDume($idContextDumeAcheteur);
        $reponse = $c->acheteurService()->publishDume($idContextDumeAcheteur);
        $reponse = $c->acheteurService()->getPublishedDume($idContextDumeAcheteur);

        $this->assertNotEmpty($reponse);
    }

    /**
     * Test de la méthode getPublishedDume() mais avec un blocage en local.
     */
    public function testGetPublishedDumeError()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );
        try {
            $reponse = $c->acheteurService()->getPublishedDume();
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_LIST_PUBLISHED_DUME_ACHETEUR, $ex->getCode());
        }
    }

    /**
     * Test de la méthode getPublishedDume() avec un cas d'erreur déclenchée par les WS LT-DUME.
     */
    public function testGetPublishedDumeErrorDistante()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );
        try {
            $reponse = $c->acheteurService()->getPublishedDume(100000000);
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_REST, $ex->getCode());
        }
    }

    /**
     * Test de la méthode getPdf().
     */
    public function testGetPdf()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );

        $reponse = $c->acheteurService()->getPdf(50);

        $this->assertNotEmpty($reponse);
    }

    /**
     * Test de la méthode getPdf() mais avec un blocage en local.
     */
    public function testGetPdfError()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $this->logger,
            null
        );
        try {
            $reponse = $c->acheteurService()->getPdf();
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_GET_PDF_DUME_ACHETEUR, $ex->getCode());
        }
    }

    /**
     * Test de la méthode getPdf() avec un cas d'erreur déclenchée par les WS LT-DUME.
     */
    public function testGetPdfErrorDistante()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $this->logger,
            null
        );
        try {
            $reponse = $c->acheteurService()->getPdf(100000000);
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_REST, $ex->getCode());
        }
    }
}
