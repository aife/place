<?php
/**
 * Created by PhpStorm.
 * User: MWA
 * Date: 22/01/2018
 * Time: 11:06.
 */
require_once __DIR__.'/../../vendor/autoload.php';
use AtexoDume\Exception\ServiceException;
use Monolog\Logger;

class EntrepriseServiceTest extends \PHPUnit_Framework_TestCase
{
    protected $logger;

    public function __construct()
    {
        $this->logger = new Logger('EntrepriseService');
    }

    /**
     * Test de la méthode createDume().
     */
    public function testCreateDumeValid()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );
        $consultation = AtexoDume\Tool\TestUtils::createConsultationEntreprise();

        $results = $c->entrepriseService()->createDume($consultation);
        print_r($results);
        $this->assertNotEmpty($results);
    }

    /**
     * Test de la méthode createDume() avec un cas d'erreur (consultation vide).
     */
    public function testCreateDumeError()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );
        $consultation = new \AtexoDume\Dto\Entreprise\Consultation();
        try {
            $results = $c->entrepriseService()->createDume($consultation);
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_JSON, $ex->getCode());
        }
    }

    /**
     * Test de la méthode createDume() avec un cas d'erreur déclenchée par les WS LT-DUME.
     */
    public function testCreateDumeErrorDistante()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );

        $metadata = AtexoDume\Tool\TestUtils::createMetadataEntreprise();
        $metadata->setIdContextDumeAcheteur(0);

        $consultation = AtexoDume\Tool\TestUtils::createConsultationEntreprise();

        $consultation->setMetadata($metadata);

        try {
            $results = $c->entrepriseService()->createDume($consultation);
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_REST, $ex->getCode());
        }
    }

    /**
     * Test de la méthode updateDume().
     */
    public function testUpdateDume()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );
        $consultation = AtexoDume\Tool\TestUtils::createConsultationEntreprise();

        $results = $c->entrepriseService()->createDume($consultation);

        print_r('IdContextDumeEntreprise cree='.$results);
        $this->assertNotEmpty($results);

        $Lots = $consultation->getLots();

        $lot = new \AtexoDume\Dto\Lot();
        $lot->setIntituleLot('Intitule du Lot 3');
        $lot->setNumeroLot('3');

        $Lots[] = $lot;
        $consultation->setLots($Lots);

        $resultsUpdate = $c->entrepriseService()->updateDume($consultation, $results);
        print_r('IdContextDumeEntreprise update='.$resultsUpdate);
        $this->assertEquals($results, $resultsUpdate);
    }

    /**
     * Test de la méthode updateDume() avec un cas d'erreur (idContextDumeAcheteur vide).
     */
    public function testUpdateDumeError()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );
        $consultation = new \AtexoDume\Dto\Entreprise\Consultation();
        try {
            $results = $c->entrepriseService()->updateDume($consultation);
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_UPDATE_DUME_ENTREPRISE, $ex->getCode());
        }
    }

    /**
     * Test de la méthode updateDume() avec un cas d'erreur déclenchée par les WS LT-DUME.
     */
    public function testUpdateDumeErrorDistante()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );
        $consultation = AtexoDume\Tool\TestUtils::createConsultationEntreprise();
        try {
            $results = $c->entrepriseService()->updateDume($consultation, 100000000);
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_REST, $ex->getCode());
        }
    }

    /**
     * Test de la méthode validateDume().
     */
    public function testValidateDume()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );
        $consultation = AtexoDume\Tool\TestUtils::createConsultationEntreprise();

        $idContextDumeEntreprise = $c->entrepriseService()->createDume($consultation);

        $reponse = $c->entrepriseService()->validateDume($idContextDumeEntreprise);
        $this->assertNotEmpty($reponse);
    }

    /**
     * Test de la méthode validateDume() mais avec un blocage en local.
     */
    public function testValidateDumeError()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );

        try {
            $reponse = $c->entrepriseService()->validateDume();
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_VALIDATION_DUME_ENTREPRISE, $ex->getCode());
        }
    }

    /**
     * Test de la méthode validateDume() avec un cas d'erreur déclenchée par les WS LT-DUME.
     */
    public function testValidateDumeErrorDistante()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );

        try {
            $reponse = $c->entrepriseService()->validateDume(100000000);
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_REST, $ex->getCode());
        }
    }

    /**
     * Test de la méthode publishDume().
     */
    public function testPublishDume()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );
        $consultation = AtexoDume\Tool\TestUtils::createConsultationEntreprise();

        $idContextDumeEntreprise = $c->entrepriseService()->createDume($consultation);

        $reponse = $c->entrepriseService()->validateDume($idContextDumeEntreprise);
        $this->assertNotEmpty($reponse);

        $reponse = $c->entrepriseService()->publishDume($idContextDumeEntreprise);
        $this->assertNotEmpty($reponse);
    }

    /**
     * Test de la méthode publishDume() mais avec un blocage en local.
     */
    public function testPublishDumeError()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );

        try {
            $reponse = $c->entrepriseService()->publishDume();
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_VALIDATION_DUME_ENTREPRISE, $ex->getCode());
        }
    }

    /**
     * Test de la méthode validateDume() avec un cas d'erreur déclenchée par les WS LT-DUME.
     */
    public function testPublishDumeErrorDistante()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );

        try {
            $reponse = $c->entrepriseService()->publishDume(100000000);
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_REST, $ex->getCode());
        }
    }

    /**
     * Test de la méthode getPublishedDume().
     */
    public function testGetPublishedDume()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );
        $consultation = AtexoDume\Tool\TestUtils::createConsultationEntreprise();

        $idContextDumeEntreprise = $c->entrepriseService()->createDume($consultation);

        $reponse = $c->entrepriseService()->validateDume($idContextDumeEntreprise);
        //$reponse = $c->entrepriseService()->publishDume($idContextDumeEntreprise);
        $reponse = $c->entrepriseService()->getPublishedDume($idContextDumeEntreprise);

        $this->assertNotEmpty($reponse);
    }

    /**
     * Test de la méthode getPublishedDume() mais avec un blocage en local.
     */
    public function testGetPublishedDumeError()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );
        try {
            $reponse = $c->entrepriseService()->getPublishedDume();
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_LIST_PUBLISHED_DUME_ENTREPRISE, $ex->getCode());
        }
    }

    /**
     * Test de la méthode getPublishedDume() avec un cas d'erreur déclenchée par les WS LT-DUME.
     */
    public function testGetPublishedDumeErrorDistante()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );
        try {
            $reponse = $c->entrepriseService()->getPublishedDume(100000000);
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_REST, $ex->getCode());
        }
    }

    /**
     * Test de la méthode getPdf().
     */
    public function testGetPdf()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $_ENV['LOGIN'],
            $_ENV['PWD'],
            $this->logger,
            null
        );

        $reponse = $c->entrepriseService()->getPdf(3);

        $this->assertNotEmpty($reponse);
    }

    /**
     * Test de la méthode getPdf() mais avec un blocage en local.
     */
    public function testGetPdfError()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $this->logger,
            null
        );
        try {
            $reponse = $c->entrepriseService()->getPdf();
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_GET_PDF_DUME_ENTREPRISE, $ex->getCode());
        }
    }

    /**
     * Test de la méthode getPdf() avec un cas d'erreur déclenchée par les WS LT-DUME.
     */
    public function testGetPdfErrorDistante()
    {
        $c = new \AtexoDume\Client\Client(
            $_ENV['URL_SERVEUR_DUME'],
            $this->logger,
            null
        );
        try {
            $reponse = $c->entrepriseService()->getPdf(100000000);
        } catch (ServiceException $ex) {
            $this->assertEquals(ServiceException::ERROR_REST, $ex->getCode());
        }
    }
}
