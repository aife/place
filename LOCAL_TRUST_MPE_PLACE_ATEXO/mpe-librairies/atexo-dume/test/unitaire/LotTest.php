<?php
/**
 * Created by PhpStorm.
 * User: MWA
 * Date: 22/01/2018
 * Time: 11:06.
 */
require_once __DIR__.'/../../vendor/autoload.php';

class LotTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test de la méthode toJSON().
     */
    public function testToJSON()
    {
        $lot = AtexoDume\Tool\TestUtils::createLot();
        $this->assertJsonStringEqualsJsonString($lot->toJSON(), '{"intituleLot":"Intitule du Lot","numeroLot" : "1"}');
    }

    /**
     * Test de la méthode isValid() avec Lot non Vide.
     */
    public function testValidYes()
    {
        $lot = AtexoDume\Tool\TestUtils::createLot();
        $this->assertEquals(\AtexoDume\Tool\ValidEnum::VALID, $lot->isValid());
    }

    /**
     * Test de la méthode isValid() avec Lot vide.
     */
    public function testValidError()
    {
        $lot = new AtexoDume\Dto\Lot();
        $this->assertEquals(\AtexoDume\Tool\ValidEnum::ERROR_LOT, $lot->isValid());
    }
}
