# Commandes utiles

## Composer
Se base sur les fichiers de conf `composer.json` / `composer.lock`  
Se positionner dans le dossier de Symfony `mpe_sf` :
- `composer install` Installation des dépendances (lib php)

## Doctrine
Se positionner dans le dossier de Symfony `mpe_sf` :
- `php bin/console doctrine:migration:migrate` Mise à jour de la BDD avec les migrations

## Docker
Se positionner dans le dossier de Docker `docker` :
- `make docker-run` Lancer Docker
- `make docker-connect` Se connecter à Docker
    - `mpe_cd_root` Pour aller dans le dossier racine
    - `mpe_restart` Utile apres un switch de branche pour tout clean

## Webpack Encore (js et css/scss)
Se base sur les fichiers de conf `package.json` / `package-lock.json`  
Se positionner dans le dossier de Symfony `mpe_sf` :
- `yarn install` Installation des dépendances (lib javascript)
- `yarn watch` Génération à la volée
- `yarn dev` Génération en mode DEV
- `yarn build` Génération en mode PROD

Si yarn / nodejs sont mal installé :
1. `curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -`
2. `sudo apt-get install -y nodejs`
3. `sudo npm install --global yarn`

