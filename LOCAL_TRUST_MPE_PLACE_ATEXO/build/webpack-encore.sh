#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cd "${DIR}/../mpe_sf" || exit 1

echo
echo "✨ yarn install... "
yarn install || { echo "❌ Erreur à l'installation des paquets Yarn"; exit 1; }

if [ $1 = "prod" ]
then
  echo "Building assets pour la prod... "
  echo; echo "👷️ Building assets pour la prod..."; echo
  yarn build || { echo "❌ Erreur lors du Yarn build pour la prod"; exit 1; }
else
  echo; echo "👷️ Lancement pour le dev... (Ctrl C pour arrêter)"; echo
  echo; echo "💡 Pour lancer le build pour la prod, relancez le script avec l'argument 'prod'"; echo
  yarn encore dev-server --https --hot --host mpe-docker.local-trust.com --disable-host-check --port 8085
fi
