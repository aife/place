# Créer une carte spécifique pour un client

## Configuration de MPE

Exécuter cette commande afin d’activer la page d’accueil personnalisée :
```sql
update configuration_plateforme set accueil_entreprise_personnalise = '1';
```

Exécuter cette autre commande afin que ce soit les données de la map “locale” qui
soient prises en compte au lieu de la map nationale :
```sql
insert into configuration_client (parameter, value, updated_at)
values ('ACTIVER_OPTION_CARTE_PAGE_ACCUEIL', '0', now());
```

## Charte du client

* Créer un répertoire `map` dans le répertoire de la charte client :
par ex. `charte_client/config/cral/map/` pour le client `cral`. 
* Copier le fichier `mpe_sf/legacy/protected/themes/map/map_local.js` dans
ce nouveau répertoire.
* Adapter le fichier copié :
  * Les données JSON des régions/départements inutiles peuvent être supprimées afin d'alléger le fichier
  * Dans le noeud `canvas_local_subitems`, adapter les données du canvas pour centrer la région ou les départements souhaités :
    ```json
    "canvas_local_subitems": {
        "type": "departement",
        "paper_width": 380,
        "paper_height": 400,
        "x": 530,
        "y": 130,
        "width": 90,
        "height": 120,
        "fit": "false",
        "stroke-width": "1.1"
    },
    ```
    💡 Pour trouver les bonnes valeurs, il est conseillé d'utiliser les outils de développement
  du navigateur et d'ajuster les valeurs de l'attribut `viewBox` du noeud de DOM :
    ```
    <svg height="400" version="1.1" width="380" 
    xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; 
    position: relative; left: -0.483337px; top: -0.599976px;" 
    viewBox="530 130 90 120" preserveAspectRatio="xMinYMin" role="img" 
    title="Carte lieux d'exécution" aria-label="Carte lieux d'exécution">...</svg>
    ```
  * Supprimer les lignes `"local": 1,` des régions/départements non concernés
  * Ajouter le noeud `"local": 1,` en tant qu'enfant du noeud région et de tous les noeuds
département (après le noeud `color`) :
    ```json
         "color": "#8596cf",
         "local": 1,
    ```
  * Pour chaque noeud département, ajouter les noeuds enfants suivants :
    * `local_color` : code héxadécimal de la couleur pour le département
    * `local_selcolor` : code héxadécimal de la couleur pour le département lorsqu'il est sélectionné
    * `local_path` : copier/coller la valeur du noeud existant `path` du département
    ```json
        {
            "name": "Seine St Denis",
            "code": "93",
            "nuts": "FR106",
            "path": "M404.7,[...]",
            "color": "#8596cf",
            "local": 1,
            "local_color": "#d080ae",
            "local_selcolor": "#a2025d",
            "local_path": "M404.7,[...]"
        },
    ```
