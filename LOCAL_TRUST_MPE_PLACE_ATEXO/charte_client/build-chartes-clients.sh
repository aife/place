#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
shopt -s globstar
GREY='\e[38;5;243m'
NC='\e[0m' # No Color

# Vérification de la version de Node installée
if [[ $(node -v | grep '^v16') == '' ]]
then
  echo "La version installée de Node n'est pas la bonne : $(node -v)"
  echo "Lancez la commande 'make localhost-init'."
  echo
  exit 1
fi

buildClient() {
  echo

  if [ "${MPE_CLIENT_DIR}" == '' ]
  then
    echo "La variable d'environnement MPE_CLIENT_DIR n'est pas renseignée."
    exit 1
  fi

  if [ ! -f "${MPE_CLIENT_DIR}/mpe-new-client.css" ] \
  || [ "$filename" -nt "${MPE_CLIENT_DIR}/mpe-new-client.css" ] \
  || [ "${DIR}/../mpe_sf/assets/css/mpe-new-client.scss" -nt "${MPE_CLIENT_DIR}/mpe-new-client.css" ]
  then
    echo "Building ${filename}..."
    yarn build-css || exit 1
  else
    echo -e "${GREY}${MPE_CLIENT_DIR}/mpe-new-client.css déjà à jour.${NC}"
  fi
}

cd "$DIR"
yarn install || exit 1

if [[ $# -eq 1 && "$1" != "" ]]
then
  echo "##############################################################"
  echo "########## Building only 1 client : $1"
  echo "##############################################################"

  if [ ! -d "config/${1}" ]
  then
    echo "Le répertoire charte_client/config/${1} n'existe pas."
    exit 1
  fi

  filesList="./config/${1}/**/variables.scss"

  ls $filesList 2> /dev/null || { echo; echo "❌ Aucun fichier variables.scss trouvé dans le thème du client"; exit 1; }
else
  filesList="./config/**/variables.scss"
fi

for filename in $filesList
do
  export MPE_CLIENT_DIR=$(dirname "$filename")
  buildClient
done

echo
${DIR}/../mpe_sf/legacy/clean-themes.sh
