<#assign background = '#eee'>
<#assign padding = '12px'>
<#assign margin = '24px'>
<#assign font = 'Verdana'>
<#assign td_default_style = 'style="padding:${padding}"'>
<#assign table_style = 'style="width:98vw"'>
<#assign tr_default_style = 'style="background-color:${background}"'>
<#assign td_title_style = 'style="padding:${padding}; font-weight:bold;"'>
<#assign span_style = 'style="display:block; margin: ${margin} 0 0 0"'>
<#assign main_style = 'style="font-family: ${font}, sans-serif; color: #333; padding: 15px 0;"'>

<div ${main_style}>
    <div>
        <img src="https://pmp.b2g.etat.lu/themes/images/bandeau-email.jpg">
        <br/>
        <strong>Communication à utiliser pour la facturation</strong>
        <div>
            << Avis no : ${typeAvis.reference}, ${typeAvis.titre} >>
        </div>
        <div> Veuillez faire parvenir la facture à l'adresse suivante :</div>
        <div> ${typeAvis.facturation.adresse!}</div>
        <div> ${typeAvis.facturation.mail!}</div>
        <div><strong> En cas de problème, veuillez contacter :</strong></div>
        <div>${typeAvis.creerPar.prenom!} ${typeAvis.creerPar.nom!}</div>
        <div> e-mail ${typeAvis.creerPar.email!}</div>
        <div> Téléphone ${typeAvis.creerPar.telephone!}</div>
        <div> Fax ${typeAvis.creerPar.fax!}</div>
        <div> Organe de presse ${libelleSupport}</div>
        <div> ${mailContenu!}</div>
        <br/>

        <div>Cordialement,</div>
        <div> Portail des Marchés Publics Merci de votre intérêt pour ce marché. Portail des
            Marchés Publics Internet: http://www.marches.public.lu
        </div>
    </div>


</div>
