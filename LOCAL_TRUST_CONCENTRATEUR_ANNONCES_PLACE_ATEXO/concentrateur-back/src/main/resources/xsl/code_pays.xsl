<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="2.0">

    <xsl:template name="codePays">
        <xsl:param name="canal"/>
        <xsl:param name="emplacement"/>
        <xsl:choose>

            <xsl:when test="$emplacement = 'Afghanistan'">AF</xsl:when>

            <xsl:when test="$emplacement = 'Afrique du Sud'">ZA</xsl:when>

            <xsl:when test="$emplacement = 'Albanie'">AL</xsl:when>

            <xsl:when test="$emplacement = 'Alg�rie'">DZ</xsl:when>

            <xsl:when test="$emplacement = 'Allemagne'">DE</xsl:when>

            <xsl:when test="$emplacement = 'Andorre'">AD</xsl:when>

            <xsl:when test="$emplacement = 'Angola'">AO</xsl:when>

            <xsl:when test="$emplacement = 'Antigua-et-Barbuda'">AG</xsl:when>

            <xsl:when test="$emplacement = 'Arabie saoudite'">SA</xsl:when>

            <xsl:when test="$emplacement = 'Argentine'">AR</xsl:when>

            <xsl:when test="$emplacement = 'Arm�nie'">AM</xsl:when>

            <xsl:when test="$emplacement = 'Australie'">AU</xsl:when>

            <xsl:when test="$emplacement = 'Autriche'">AT</xsl:when>

            <xsl:when test="$emplacement = 'Azerba�djan'">AZ</xsl:when>

            <xsl:when test="$emplacement = 'Bahamas'">BS</xsl:when>

            <xsl:when test="$emplacement = 'Bahre�n'">BH</xsl:when>

            <xsl:when test="$emplacement = 'Bangladesh'">BD</xsl:when>

            <xsl:when test="$emplacement = 'Barbade'">BB</xsl:when>

            <xsl:when test="$emplacement = 'Belau'">PW</xsl:when>

            <xsl:when test="$emplacement = 'Belgique'">BE</xsl:when>

            <xsl:when test="$emplacement = 'Belize'">BZ</xsl:when>

            <xsl:when test="$emplacement = 'B�nin'">BJ</xsl:when>

            <xsl:when test="$emplacement = 'Bhoutan'">BT</xsl:when>

            <xsl:when test="$emplacement = 'Bi�lorussie'">BY</xsl:when>

            <xsl:when test="$emplacement = 'Birmanie'">MM</xsl:when>

            <xsl:when test="$emplacement = 'Bolivie'">BO</xsl:when>

            <xsl:when test="$emplacement = 'Bosnie-Herz�govine'">BA</xsl:when>

            <xsl:when test="$emplacement = 'Botswana'">BW</xsl:when>

            <xsl:when test="$emplacement = 'Br�sil'">BR</xsl:when>

            <xsl:when test="$emplacement = 'Brunei'">BN</xsl:when>

            <xsl:when test="$emplacement = 'Bulgarie'">BG</xsl:when>

            <xsl:when test="$emplacement = 'Burkina'">BF</xsl:when>

            <xsl:when test="$emplacement = 'Burundi'">BI</xsl:when>

            <xsl:when test="$emplacement = 'Cambodge'">KH</xsl:when>

            <xsl:when test="$emplacement = 'Cameroun'">CM</xsl:when>

            <xsl:when test="$emplacement = 'Canada'">CA</xsl:when>

            <xsl:when test="$emplacement = 'Cap-Vert'">CV</xsl:when>

            <xsl:when test="$emplacement = 'Chili'">CL</xsl:when>

            <xsl:when test="$emplacement = 'Chine'">CN</xsl:when>

            <xsl:when test="$emplacement = 'Chypre'">CY</xsl:when>

            <xsl:when test="$emplacement = 'Colombie'">CO</xsl:when>

            <xsl:when test="$emplacement = 'Comores'">KM</xsl:when>

            <xsl:when test="$emplacement = 'Congo'">CG</xsl:when>

            <xsl:when test="$emplacement = 'Congo (R�p. d�m. du)'">CD</xsl:when>

            <xsl:when test="$emplacement = 'Cook'">CK</xsl:when>

            <xsl:when test="$emplacement = 'Cor�e du Nord'">KP</xsl:when>

            <xsl:when test="$emplacement = 'Cor�e du Sud'">KR</xsl:when>

            <xsl:when test="$emplacement = 'Costa Rica'">CR</xsl:when>

            <xsl:when test="$emplacement = 'C�te d''Ivoire'">CI</xsl:when>

            <xsl:when test="$emplacement = 'Croatie'">HR</xsl:when>

            <xsl:when test="$emplacement = 'Cuba'">CU</xsl:when>

            <xsl:when test="$emplacement = 'Danemark'">DK</xsl:when>

            <xsl:when test="$emplacement = 'Djibouti'">DJ</xsl:when>

            <xsl:when test="$emplacement = 'Dominique'">DM</xsl:when>

            <xsl:when test="$emplacement = '�gypte'">EG</xsl:when>

            <xsl:when test="$emplacement = '�mirats arabes unis'">AE</xsl:when>

            <xsl:when test="$emplacement = '�quateur'">EC</xsl:when>

            <xsl:when test="$emplacement = '�rythr�e'">ER</xsl:when>

            <xsl:when test="$emplacement = 'Espagne'">ES</xsl:when>

            <xsl:when test="$emplacement = 'Estonie'">EE</xsl:when>

            <xsl:when test="$emplacement = '�tats-Unis'">US</xsl:when>

            <xsl:when test="$emplacement = '�thiopie'">ET</xsl:when>

            <xsl:when test="$emplacement = 'Fidji'">FJ</xsl:when>

            <xsl:when test="$emplacement = 'Finlande'">FI</xsl:when>

            <xsl:when test="$emplacement = 'France'">FR</xsl:when>

            <xsl:when test="$emplacement = 'Gabon'">GA</xsl:when>

            <xsl:when test="$emplacement = 'Gambie'">GM</xsl:when>

            <xsl:when test="$emplacement = 'G�orgie'">GE</xsl:when>

            <xsl:when test="$emplacement = 'Ghana'">GH</xsl:when>

            <xsl:when test="$emplacement = 'Gr�ce'">GR</xsl:when>

            <xsl:when test="$emplacement = 'Grenade'">GD</xsl:when>

            <xsl:when test="$emplacement = 'Guatemala'">GT</xsl:when>

            <xsl:when test="$emplacement = 'Guin�e'">GN</xsl:when>

            <xsl:when test="$emplacement = 'Guin�e �quatoriale'">GQ</xsl:when>

            <xsl:when test="$emplacement = 'Guin�e-Bissao'">GW</xsl:when>

            <xsl:when test="$emplacement = 'Guyana'">GY</xsl:when>

            <xsl:when test="$emplacement = 'Ha�ti'">HT</xsl:when>

            <xsl:when test="$emplacement = 'Honduras'">HN</xsl:when>

            <xsl:when test="$emplacement = 'Hongrie'">HU</xsl:when>

            <xsl:when test="$emplacement = 'Inde'">IN</xsl:when>

            <xsl:when test="$emplacement = 'Indon�sie'">ID</xsl:when>

            <xsl:when test="$emplacement = 'Iran'">IR</xsl:when>

            <xsl:when test="$emplacement = 'Iraq'">IQ</xsl:when>

            <xsl:when test="$emplacement = 'Irlande'">IE</xsl:when>

            <xsl:when test="$emplacement = 'Islande'">IS</xsl:when>

            <xsl:when test="$emplacement = 'Isra�l'">IL</xsl:when>

            <xsl:when test="$emplacement = 'Italie'">IT</xsl:when>

            <xsl:when test="$emplacement = 'Jama�que'">JM</xsl:when>

            <xsl:when test="$emplacement = 'Japon'">JP</xsl:when>

            <xsl:when test="$emplacement = 'Jordanie'">JO</xsl:when>

            <xsl:when test="$emplacement = 'Kazakhstan'">KZ</xsl:when>

            <xsl:when test="$emplacement = 'Kenya'">KE</xsl:when>

            <xsl:when test="$emplacement = 'Kirghizistan'">KG</xsl:when>

            <xsl:when test="$emplacement = 'Kiribati'">KI</xsl:when>

            <xsl:when test="$emplacement = 'Kowe�t'">KW</xsl:when>

            <xsl:when test="$emplacement = 'Laos'">LA</xsl:when>

            <xsl:when test="$emplacement = 'Lesotho'">LS</xsl:when>

            <xsl:when test="$emplacement = 'Lettonie'">LV</xsl:when>

            <xsl:when test="$emplacement = 'Liban'">LB</xsl:when>

            <xsl:when test="$emplacement = 'Liberia'">LR</xsl:when>

            <xsl:when test="$emplacement = 'Libye'">LY</xsl:when>

            <xsl:when test="$emplacement = 'Liechtenstein'">LI</xsl:when>

            <xsl:when test="$emplacement = 'Lituanie'">LT</xsl:when>

            <xsl:when test="$emplacement = 'Luxembourg'">LU</xsl:when>

            <xsl:when test="$emplacement = 'Mac�doine'">MK</xsl:when>

            <xsl:when test="$emplacement = 'Madagascar'">MG</xsl:when>

            <xsl:when test="$emplacement = 'Malaisie'">MY</xsl:when>

            <xsl:when test="$emplacement = 'Malawi'">MW</xsl:when>

            <xsl:when test="$emplacement = 'Maldives'">MV</xsl:when>

            <xsl:when test="$emplacement = 'Mali'">ML</xsl:when>

            <xsl:when test="$emplacement = 'Malte'">MT</xsl:when>

            <xsl:when test="$emplacement = 'Maroc'">MA</xsl:when>

            <xsl:when test="$emplacement = 'Marshall'">MH</xsl:when>

            <xsl:when test="$emplacement = 'Maurice'">MU</xsl:when>

            <xsl:when test="$emplacement = 'Mauritanie'">MR</xsl:when>

            <xsl:when test="$emplacement = 'Mexique'">MX</xsl:when>

            <xsl:when test="$emplacement = 'Micron�sie'">FM</xsl:when>

            <xsl:when test="$emplacement = 'Moldavie'">MD</xsl:when>

            <xsl:when test="$emplacement = 'Monaco'">MC</xsl:when>

            <xsl:when test="$emplacement = 'Mongolie'">MN</xsl:when>

            <xsl:when test="$emplacement = 'Mozambique'">MZ</xsl:when>

            <xsl:when test="$emplacement = 'Namibie'">NA</xsl:when>

            <xsl:when test="$emplacement = 'Nauru'">NR</xsl:when>

            <xsl:when test="$emplacement = 'N�pal'">NP</xsl:when>

            <xsl:when test="$emplacement = 'Nicaragua'">NI</xsl:when>

            <xsl:when test="$emplacement = 'Niger'">NE</xsl:when>

            <xsl:when test="$emplacement = 'Nigeria'">NG</xsl:when>

            <xsl:when test="$emplacement = 'Niue'">NU</xsl:when>

            <xsl:when test="$emplacement = 'Norv�ge'">NO</xsl:when>

            <xsl:when test="$emplacement = 'Nouvelle-Z�lande'">NZ</xsl:when>

            <xsl:when test="$emplacement = 'Oman'">OM</xsl:when>

            <xsl:when test="$emplacement = 'Ouganda'">UG</xsl:when>

            <xsl:when test="$emplacement = 'Ouzb�kistan'">UZ</xsl:when>

            <xsl:when test="$emplacement = 'Pakistan'">PK</xsl:when>

            <xsl:when test="$emplacement = 'Panama'">PA</xsl:when>

            <xsl:when test="$emplacement = 'Papouasie - Nouvelle Guin�e'">PG</xsl:when>

            <xsl:when test="$emplacement = 'Paraguay'">PY</xsl:when>

            <xsl:when test="$emplacement = 'Pays-Bas'">NL</xsl:when>

            <xsl:when test="$emplacement = 'P�rou'">PE</xsl:when>

            <xsl:when test="$emplacement = 'Philippines'">PH</xsl:when>

            <xsl:when test="$emplacement = 'Pologne'">PL</xsl:when>

            <xsl:when test="$emplacement = 'Portugal'">PT</xsl:when>

            <xsl:when test="$emplacement = 'Qatar'">QA</xsl:when>

            <xsl:when test="$emplacement = 'R�publique centrafricaine'">CF</xsl:when>

            <xsl:when test="$emplacement = 'R�publique dominicaine'">DO</xsl:when>

            <xsl:when test="$emplacement = 'R�publique tch�que'">CZ</xsl:when>

            <xsl:when test="$emplacement = 'Roumanie'">RO</xsl:when>

            <xsl:when test="$emplacement = 'Royaume-Uni'">
                <xsl:if test="$canal = 'BOAMP'">GB</xsl:if>
                <xsl:if test="$canal = 'LESECHOS'">UK</xsl:if>
            </xsl:when>  <!-- GB pour BOAMP ou UK pour Les Echos -->

            <xsl:when test="$emplacement = 'Russie'">RU</xsl:when>

            <xsl:when test="$emplacement = 'Rwanda'">RW</xsl:when>

            <xsl:when test="$emplacement = 'Sainte-Lucie'">LC</xsl:when>

            <xsl:when test="$emplacement = 'Saint-Marin'">SM</xsl:when>

            <xsl:when test="$emplacement = 'Salomon'">SB</xsl:when>

            <xsl:when test="$emplacement = 'Salvador'">SV</xsl:when>

            <xsl:when test="$emplacement = 'Samoa occidentales'">WS</xsl:when>

            <xsl:when test="$emplacement = 'S�n�gal'">SN</xsl:when>

            <xsl:when test="$emplacement = 'Seychelles'">SC</xsl:when>

            <xsl:when test="$emplacement = 'Sierra Leone'">SL</xsl:when>

            <xsl:when test="$emplacement = 'Singapour'">SG</xsl:when>

            <xsl:when test="$emplacement = 'Slovaquie'">SK</xsl:when>

            <xsl:when test="$emplacement = 'Slov�nie'">SI</xsl:when>

            <xsl:when test="$emplacement = 'Somalie'">SO</xsl:when>

            <xsl:when test="$emplacement = 'Soudan'">SD</xsl:when>

            <xsl:when test="$emplacement = 'Su�de'">SE</xsl:when>

            <xsl:when test="$emplacement = 'Suisse'">CH</xsl:when>

            <xsl:when test="$emplacement = 'Suriname'">SR</xsl:when>

            <xsl:when test="$emplacement = 'Swaziland'">SZ</xsl:when>

            <xsl:when test="$emplacement = 'Syrie'">SY</xsl:when>

            <xsl:when test="$emplacement = 'Tadjikistan'">TJ</xsl:when>

            <xsl:when test="$emplacement = 'Tanzanie'">TZ</xsl:when>

            <xsl:when test="$emplacement = 'Tchad'">TD</xsl:when>

            <xsl:when test="$emplacement = 'Tha�lande'">TH</xsl:when>

            <xsl:when test="$emplacement = 'Togo'">TG</xsl:when>

            <xsl:when test="$emplacement = 'Tonga'">TO</xsl:when>

            <xsl:when test="$emplacement = 'Trinit�-et-Tobago'">TT</xsl:when>

            <xsl:when test="$emplacement = 'Tunisie'">TN</xsl:when>

            <xsl:when test="$emplacement = 'Turkm�nistan'">TM</xsl:when>

            <xsl:when test="$emplacement = 'Turquie'">TR</xsl:when>

            <xsl:when test="$emplacement = 'Tuvalu'">TV</xsl:when>

            <xsl:when test="$emplacement = 'Ukraine'">UA</xsl:when>

            <xsl:when test="$emplacement = 'Uruguay'">UY</xsl:when>

            <xsl:when test="$emplacement = 'Vanuatu'">VU</xsl:when>

            <xsl:when test="$emplacement = 'Venezuela'">VE</xsl:when>

            <xsl:when test="$emplacement = 'Vi�t Nam'">VN</xsl:when>

            <xsl:when test="$emplacement = 'Y�men'">YE</xsl:when>

            <xsl:when test="$emplacement = 'Zambie'">ZM</xsl:when>

            <xsl:when test="$emplacement = 'Zimbabwe'">ZW</xsl:when>

            <xsl:when test="$emplacement = 'Anguilla'">AI</xsl:when>
            <xsl:when test="$emplacement = 'Antarctique'">AQ</xsl:when>
            <xsl:when test="$emplacement = 'Samoa Am�ricaines'">AS</xsl:when>
            <xsl:when test="$emplacement = 'Aruba'">AW</xsl:when>
            <xsl:when test="$emplacement = 'Bermudes'">BM</xsl:when>
            <xsl:when test="$emplacement = 'Bouvet, �le'">BV</xsl:when>
            <xsl:when test="$emplacement = 'Cocos (Keeling), �les'">CC</xsl:when>
            <xsl:when test="$emplacement = 'Christmas, �le'">CX</xsl:when>
            <xsl:when test="$emplacement = 'Sahara Occidental'">EH</xsl:when>
            <xsl:when test="$emplacement = 'Falkland, �les (Malvinas)'">FK</xsl:when>
            <xsl:when test="$emplacement = 'F�ro�, �les'">FO</xsl:when>
            <xsl:when test="$emplacement = 'Guyane Fran�aise'">GF</xsl:when>
            <xsl:when test="$emplacement = 'Gibraltar'">GI</xsl:when>
            <xsl:when test="$emplacement = 'Groenland'">GL</xsl:when>
            <xsl:when test="$emplacement = 'Guadeloupe'">GP</xsl:when>
            <xsl:when test="$emplacement = 'G�orgie Du Sud Et Les �les Sandwich Du Sud'">GS</xsl:when>
            <xsl:when test="$emplacement = 'Guam'">GU</xsl:when>
            <xsl:when test="$emplacement = 'Hong-Kong'">HK</xsl:when>
            <xsl:when test="$emplacement = 'Heard, �le Et Mcdonald, �les'">HM</xsl:when>
            <xsl:when test="$emplacement = 'Oc�an Indien, Territoire Britannique De L'">IO</xsl:when>
            <xsl:when test="$emplacement = 'Saint-Christophe-et-Ni�v�s'">KN</xsl:when>
            <xsl:when test="$emplacement = 'Ca�manes, �les'">KY</xsl:when>
            <xsl:when test="$emplacement = 'Sri Lanka'">LK</xsl:when>
            <xsl:when test="$emplacement = 'Macao'">MO</xsl:when>
            <xsl:when test="$emplacement = 'Mariannes Du Nord, �les'">MP</xsl:when>
            <xsl:when test="$emplacement = 'Martinique'">MQ</xsl:when>
            <xsl:when test="$emplacement = 'Montserrat'">MS</xsl:when>
            <xsl:when test="$emplacement = 'Nouvelle-Cal�donie'">NC</xsl:when>
            <xsl:when test="$emplacement = 'Norfolk, �le'">NF</xsl:when>
            <xsl:when test="$emplacement = 'Polyn�sie Fran�aise'">PF</xsl:when>
            <xsl:when test="$emplacement = 'Saint-Pierre-Et-Miquelon'">PM</xsl:when>
            <xsl:when test="$emplacement = 'Pitcairn'">PN</xsl:when>
            <xsl:when test="$emplacement = 'Porto Rico'">PR</xsl:when>
            <xsl:when test="$emplacement = 'Palestinien Occup�, Territoire'">PS</xsl:when>
            <xsl:when test="$emplacement = 'R�union'">RE</xsl:when>
            <xsl:when test="$emplacement = 'Sainte-H�l�ne'">SH</xsl:when>
            <xsl:when test="$emplacement = 'Svalbard Et �le Jan Mayen'">SJ</xsl:when>
            <xsl:when test="$emplacement = 'Sao Tom�-et-Principe'">ST</xsl:when>
            <xsl:when test="$emplacement = 'Turks Et Ca�ques, �les'">TC</xsl:when>
            <xsl:when test="$emplacement = 'Terres Australes Fran�aises'">TF</xsl:when>
            <xsl:when test="$emplacement = 'Tokelau'">TK</xsl:when>
            <xsl:when test="$emplacement = 'Ta�wan, Province De Chine'">TW</xsl:when>
            <xsl:when test="$emplacement = '�les Mineures �loign�es Des �tats-Unis'">UM</xsl:when>
            <xsl:when test="$emplacement = 'Saint-Si�ge, ou leVatican'">VA</xsl:when>
            <xsl:when test="$emplacement = 'Saint-Vincent-Et-Les Grenadines'">VC</xsl:when>
            <xsl:when test="$emplacement = '�les Vierges Britanniques'">VG</xsl:when>
            <xsl:when test="$emplacement = '�les Vierges Des �tats-Unis'">VI</xsl:when>
            <xsl:when test="$emplacement = 'Wallis Et Futuna'">WF</xsl:when>
            <xsl:when test="$emplacement = 'Mayotte'">YT</xsl:when>

        </xsl:choose>

    </xsl:template>
</xsl:stylesheet>
