<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:nuts="http://publications.europa.eu/resource/schema/ted/2016/nuts"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version="2.0"
                xpath-default-namespace="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception" exclude-result-prefixes="fo">
    <xsl:param name="brouillon"/>
    <xsl:param name="support"/>
    <xsl:param name="offre"/>
    <xsl:param name="statut"/>
    <xsl:param name="dateDemandeEnvoi"/>
    <xsl:param name="dateDebutEnvoi"/>
    <xsl:param name="dateEnvoi"/>
    <xsl:param name="datePublication"/>
    <xsl:decimal-format name="france" decimal-separator="," grouping-separator=" "/>
    <xsl:template match="/TED_ESENDERS/FORM_SECTION/F05_2014">
        <fo:root font-family="Times">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="page" page-width="8.268055555555555in"
                                       page-height="11.693055555555556in" margin-top="14.2pt" margin-bottom="6.65pt"
                                       margin-right="70.9pt" margin-left="70.9pt">
                    <fo:region-body margin-top="34.15pt" margin-bottom="35.9pt">
                        <xsl:if test="$brouillon">
                            <xsl:call-template name="filigrane"/>
                        </xsl:if>
                    </fo:region-body>
                    <fo:region-before region-name="header" extent="11in"/>
                    <fo:region-after region-name="footer" extent="11in" display-align="after"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="page"
                              id="IDX3Y3ZEDMONUFHVQ4TOOORYSG0NHUW3WZBNSSP1GUGOAI55CZ23NI" format="1">
                <fo:static-content flow-name="header">
                    <fo:retrieve-marker retrieve-class-name="header"
                                        retrieve-position="first-including-carryover" retrieve-boundary="page"/>
                </fo:static-content>
                <fo:static-content flow-name="footer">
                    <fo:retrieve-marker retrieve-class-name="footer"
                                        retrieve-position="first-including-carryover" retrieve-boundary="page"/>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <fo:block widows="2" orphans="2" font-size="11pt" line-height="1.147"
                              white-space-collapse="false" text-align="justify" language="FR">
                        <fo:marker marker-class-name="header"/>
                        <fo:marker marker-class-name="footer">
                            <fo:table start-indent="0pt" table-layout="fixed" width="100%">
                                <fo:table-column column-number="1" column-width="85%"/>
                                <fo:table-column column-number="2" column-width="15%"/>
                                <fo:table-body start-indent="0pt" end-indent="0pt">
                                    <fo:table-row>
                                        <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt"
                                                       padding-right="3.5pt" border-top-style="solid"
                                                       border-top-color="black" border-top-width="0.5pt"
                                                       display-align="center">
                                            <fo:block-container>
                                                <fo:block space-before="6pt" space-before.conditionality="retain"
                                                          text-align="left" font-size="8pt" color="#000080">
                                                    <fo:inline color="#666699">
                                                        <xsl:text>AAPC - </xsl:text>
                                                        <xsl:value-of
                                                                select="CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/OFFICIALNAME"/>
                                                    </fo:inline>
                                                </fo:block>
                                            </fo:block-container>
                                        </fo:table-cell>
                                        <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt"
                                                       padding-right="3.5pt" border-top-style="solid"
                                                       border-top-color="black" border-top-width="0.5pt"
                                                       display-align="center">
                                            <fo:block-container>
                                                <fo:block space-before="6pt" space-before.conditionality="retain"
                                                          text-align="right" font-size="8pt" color="#000080">
                                                    <fo:inline color="#666699">
                                                        <xsl:text>Page </xsl:text>
                                                        <fo:page-number/>
                                                        <xsl:text> /&#160;</xsl:text>
                                                        <fo:page-number-citation
                                                                ref-id="IDDX2UHEXKBPJCN0NQ02JYZUENHFJA04ZGKENTVQTGW0XNXR3B33B"/>
                                                    </fo:inline>
                                                </fo:block>
                                            </fo:block-container>
                                        </fo:table-cell>
                                    </fo:table-row>
                                </fo:table-body>
                            </fo:table>
                            <fo:block space-before="6pt" space-before.conditionality="retain">
                                <fo:leader/>
                            </fo:block>
                        </fo:marker>
                        <fo:block space-before="6pt" space-before.conditionality="retain" text-align="center"
                                  font-size="16pt" font-weight="bold">
                            <fo:leader/>
                        </fo:block>
                        <fo:block space-before="6pt" space-before.conditionality="retain" text-align="center">
                            <fo:inline font-weight="bold" font-size="16pt">
                                <xsl:text>Avis d'appel public à la concurrence</xsl:text>
                            </fo:inline>
                        </fo:block>
                        <fo:block space-before="6pt" space-before.conditionality="retain" text-align="center">
                            <fo:inline font-weight="bold">
                                <xsl:value-of select="CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/OFFICIALNAME"/>
                            </fo:inline>
                        </fo:block>
                        <fo:block space-before="6pt" space-before.conditionality="retain" text-align="center">
                            <fo:inline font-weight="bold">
                                <xsl:text>Date limite de réponse : </xsl:text>
                                <xsl:if test="PROCEDURE/DATE_RECEIPT_TENDERS != ''">
                                    <xsl:value-of
                                            select="format-date(PROCEDURE/DATE_RECEIPT_TENDERS,'[D01]/[M01]/[Y0001]')"/>
                                </xsl:if>
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="PROCEDURE/TIME_RECEIPT_TENDERS"/>
                            </fo:inline>
                        </fo:block>
                        <fo:block space-before="6pt" space-before.conditionality="retain">
                            <fo:leader/>
                        </fo:block>
                        <fo:block space-before="6pt" space-before.conditionality="retain">
                            <fo:leader/>
                        </fo:block>
                        <fo:block space-before="6pt" space-before.conditionality="retain">
                            <fo:leader/>
                        </fo:block>


                        <xsl:apply-templates select="CONTRACTING_BODY"/>

                        <xsl:apply-templates select="OBJECT_CONTRACT"/>

                        <xsl:apply-templates select="LEFTI"/>

                        <xsl:apply-templates select="PROCEDURE"/>

                        <xsl:apply-templates select="COMPLEMENTARY_INFO"/>


                        <fo:block space-before="6pt" space-before.conditionality="retain">
                            <fo:inline font-weight="bold">
                                <xsl:text>Section VIII : Date d'envoi à la publication</xsl:text>
                            </fo:inline>
                        </fo:block>
                        <xsl:if test="not($brouillon)">
                            <fo:block space-before="6pt" space-before.conditionality="retain">
                                <fo:inline>
                                    <xsl:text>Envoi au support </xsl:text>
                                    <xsl:value-of select="$support"/>
                                    <xsl:text> pour l'offre </xsl:text>
                                    <xsl:value-of select="$offre"/>
                                    <xsl:text> : </xsl:text>
                                    <xsl:choose>
                                        <xsl:when test="$statut = 'EN_ATTENTE'">
                                            <xsl:text>En cours d'envoi le </xsl:text>
                                            <xsl:value-of
                                                    select="format-dateTime($dateDemande,'[D01]/[M01]/[Y0001] à [H01]:[m01]')"/>
                                        </xsl:when>
                                        <xsl:when test="$statut = 'EN_COURS_DE_PUBLICATION'">
                                            <xsl:text>Envoyé le </xsl:text>
                                            <xsl:value-of
                                                    select="format-dateTime($dateTraitementInterne,'[D01]/[M01]/[Y0001] à [H01]:[m01]')"/>
                                        </xsl:when>
                                        <xsl:when test="$statut = 'REJETER_CONCENTRATEUR'">
                                            <xsl:text>Rejeté par le concentrateur le </xsl:text>
                                            <xsl:value-of
                                                    select="format-dateTime($dateTraitementInterne,'[D01]/[M01]/[Y0001] à [H01]:[m01]')"/>
                                        </xsl:when>
                                        <xsl:when test="$statut = 'REJETER_SUPPORT'">
                                            <xsl:text>Rejeté par le support le </xsl:text>
                                            <xsl:value-of
                                                    select="format-dateTime($dateTraitement,'[D01]/[M01]/[Y0001] à [H01]:[m01]')"/>
                                        </xsl:when>
                                        <xsl:when test="$statut = 'PUBLIER'">
                                            <xsl:text>Publié le </xsl:text>
                                            <xsl:choose>
                                                <xsl:when test="$datePublication">
                                                    <xsl:value-of
                                                            select="format-dateTime($datePublication,'[D01]/[M01]/[Y0001] à [H01]:[m01]')"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of
                                                            select="format-dateTime($dateTraitement,'[D01]/[M01]/[Y0001] à [H01]:[m01]')"/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:when>
                                    </xsl:choose>
                                </fo:inline>
                                <fo:inline font-size="8pt">
                                    <xsl:text> (information mise à jour le </xsl:text>
                                    <xsl:value-of
                                            select="format-dateTime(current-dateTime(),'[D01]/[M01]/[Y0001] à [H01]:[m01]')"/>
                                    <xsl:text>)</xsl:text>
                                </fo:inline>
                            </fo:block>
                        </xsl:if>
                    </fo:block>
                    <fo:block id="IDDX2UHEXKBPJCN0NQ02JYZUENHFJA04ZGKENTVQTGW0XNXR3B33B"/>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

    <xsl:template match="CONTRACTING_BODY">
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline font-weight="bold">
                <xsl:text>Section I : Acheteur public</xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>I.1) Nom et adresse</xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:value-of select="ADDRESS_CONTRACTING_BODY/OFFICIALNAME"/>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:value-of select="ADDRESS_CONTRACTING_BODY/ADDRESS"/>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:value-of select="ADDRESS_CONTRACTING_BODY/POSTAL_CODE"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="ADDRESS_CONTRACTING_BODY/TOWN"/>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:table start-indent="0pt" table-layout="fixed" width="100%">
                <fo:table-column column-number="1" column-width="25%"/>
                <fo:table-column column-number="2" column-width="75%"/>
                <fo:table-body start-indent="0pt" end-indent="0pt">
                    <fo:table-row height="17pt">
                        <fo:table-cell>
                            <fo:block-container>
                                <fo:block>
                                    <xsl:text>Point de contact :</xsl:text>
                                </fo:block>
                            </fo:block-container>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block-container>
                                <fo:block>
                                    <xsl:value-of select="ADDRESS_CONTRACTING_BODY/CONTACT_POINT"/>
                                </fo:block>
                            </fo:block-container>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="17pt">
                        <fo:table-cell>
                            <fo:block/>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block-container>
                                <fo:block>
                                    <xsl:value-of select="ADDRESS_CONTRACTING_BODY/PHONE"/>
                                </fo:block>
                            </fo:block-container>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="17pt">
                        <fo:table-cell>
                            <fo:block/>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block-container>
                                <fo:block>
                                    <xsl:value-of select="ADDRESS_CONTRACTING_BODY/E_MAIL"/>
                                </fo:block>
                            </fo:block-container>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>Adresse Internet : </xsl:text>
                <xsl:value-of select="ADDRESS_CONTRACTING_BODY/URL_GENERAL"/>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>Adresse Profil acheteur : </xsl:text>
                <xsl:value-of select="ADDRESS_CONTRACTING_BODY/URL_BUYER"/>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:leader/>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>I.3) Communication</xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>Les documents du marché sont disponibles gratuitement à l'adresse :</xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:value-of select="URL_DOCUMENT"/>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>Les offres ou les demandes de participation doivent être envoyées par voie électronique à l'adresse :</xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:value-of select="URL_PARTICIPATION"/>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:leader/>
        </fo:block>
    </xsl:template>

    <xsl:template match="OBJECT_CONTRACT">
        <xsl:param name="no_lot_division" select="NO_LOT_DIVISION"/>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline font-weight="bold">
                <xsl:text>Section II : Objet</xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>II.1) Étendue du marché</xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>II.1.1) Intitulé : </xsl:text>
            </fo:inline>
            <fo:inline font-weight="bold">
                <xsl:value-of select="TITLE/P/text()"/>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>Numéro de référence : </xsl:text>
                <xsl:value-of select="REFERENCE_NUMBER"/>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>II.1.2) Code CPV principal : </xsl:text>
                <xsl:value-of select="CPV_MAIN/CPV_CODE/@CODE"/>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>II.1.3) Type de marché : </xsl:text>
                <xsl:if test="TYPE_CONTRACT/@CTYPE = 'SERVICES'">
                    <xsl:text>Services</xsl:text>
                </xsl:if>
                <xsl:if test="TYPE_CONTRACT/@CTYPE = 'SUPPLIES'">
                    <xsl:text>Fournitures</xsl:text>
                </xsl:if>
                <xsl:if test="TYPE_CONTRACT/@CTYPE = 'WORKS'">
                    <xsl:text>Travaux</xsl:text>
                </xsl:if>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>II.1.4) Description succincte : </xsl:text>
            </fo:inline>
        </fo:block>
        <xsl:for-each select="SHORT_DESCR/P">
            <fo:block space-before="6pt" space-before.conditionality="retain">
                <fo:inline font-weight="bold">
                    <xsl:value-of select="text()"/>
                </fo:inline>
            </fo:block>
        </xsl:for-each>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>II.1.6) Allotissement - Ce marché est divisé en lots : </xsl:text>
                <xsl:call-template name="yes_no">
                    <xsl:with-param name="no_value" select="$no_lot_division"/>
                </xsl:call-template>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:leader/>
        </fo:block>
        <xsl:apply-templates select="OBJECT_DESCR">
            <xsl:with-param name="no_lot_division" select="$no_lot_division"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="OBJECT_DESCR">
        <xsl:param name="no_lot_division"/>
        <xsl:param name="no_renewal" select="NO_RENEWAL"/>
        <xsl:param name="no_variants" select="NO_ACCEPTED_VARIANTS"/>
        <xsl:param name="no_eu_progr" select="NO_EU_PROGR_RELATED"/>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline font-weight="bold">
                <xsl:text>II.2) Description</xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>II.2.1) Intitulé : </xsl:text>
            </fo:inline>
            <fo:inline font-weight="bold">
                <xsl:if test="not($no_lot_division)">
                    <xsl:text>Lot n°</xsl:text>
                    <xsl:value-of select="LOT_NO"/>
                    <xsl:text> - </xsl:text>
                </xsl:if>
                <xsl:value-of select="TITLE/P/text()"/>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>II.2.2) Code CPV principal : </xsl:text>
                <xsl:value-of select="CPV_ADDITIONAL/CPV_CODE/@CODE"/>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>II.2.3) Lieu principal d'exécution / Code NUTS : </xsl:text>
                <xsl:value-of select="nuts:NUTS/@CODE" separator=", "/>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>II.2.4) Description des prestations : </xsl:text>
            </fo:inline>
        </fo:block>
        <xsl:for-each select="SHORT_DESCR/P">
            <fo:block space-before="6pt" space-before.conditionality="retain">
                <fo:inline>
                    <xsl:value-of select="text()"/>
                </fo:inline>
            </fo:block>
        </xsl:for-each>

        <xsl:apply-templates select="AC"/>

        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>II.2.6) Valeur estimée hors TVA : </xsl:text>
                <xsl:choose>
                    <xsl:when test="VAL_OBJECT != ''">
                        <xsl:value-of select="format-number(VAL_OBJECT, '# ###,00', 'france')"/>
                        <xsl:text> EUR HT</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>Non renseigné</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>II.2.7) Durée du marché, de l'accord-cadre ou du système d'acquisition dynamique</xsl:text>
            </fo:inline>
        </fo:block>
        <xsl:choose>
            <xsl:when test="OPTIONS">
                <xsl:for-each select="OPTIONS_DESCR/P">
                    <fo:block space-before="6pt" space-before.conditionality="retain">
                        <fo:inline>
                            <xsl:value-of select="text()"/>
                        </fo:inline>
                    </fo:block>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <fo:block space-before="6pt" space-before.conditionality="retain">
                    <fo:inline>
                        <xsl:text>Durée ferme : </xsl:text>
                        <xsl:value-of select="DURATION"/>
                        <xsl:if test="DURATION/@TYPE = 'MONTH'">
                            <xsl:text> mois</xsl:text>
                        </xsl:if>
                        <xsl:if test="DURATION/@TYPE = 'DAY'">
                            <xsl:text> jour(s)</xsl:text>
                        </xsl:if>
                    </fo:inline>
                </fo:block>
            </xsl:otherwise>
        </xsl:choose>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>Ce marché peut faire l'objet d'une reconduction : </xsl:text>
                <xsl:call-template name="yes_no">
                    <xsl:with-param name="no_value" select="$no_renewal"/>
                </xsl:call-template>
            </fo:inline>
        </fo:block>
        <xsl:if test="not($no_renewal)">
            <xsl:for-each select="RENEWAL_DESCR/P">
                <fo:block space-before="6pt" space-before.conditionality="retain">
                    <fo:inline>
                        <xsl:value-of select="text()"/>
                    </fo:inline>
                </fo:block>
            </xsl:for-each>
        </xsl:if>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>II.2.10) Variantes</xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>Des variantes seront prises en considération : </xsl:text>
                <xsl:call-template name="yes_no">
                    <xsl:with-param name="no_value" select="$no_variants"/>
                </xsl:call-template>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>II.2.13) Information sur les fonds de l'Union européenne</xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>Le contrat s'inscrit dans un projet/programme financé par des fonds de l'Union européenne : </xsl:text>
                <xsl:call-template name="yes_no">
                    <xsl:with-param name="no_value" select="$no_eu_progr"/>
                </xsl:call-template>
            </fo:inline>
        </fo:block>
        <xsl:if test="not($no_eu_progr)">
            <fo:block space-before="6pt" space-before.conditionality="retain">
                <fo:inline>
                    <xsl:text>Identification du projet : </xsl:text>
                </fo:inline>
            </fo:block>
            <xsl:for-each select="EU_PROGR_RELATED/P">
                <fo:block space-before="6pt" space-before.conditionality="retain">
                    <fo:inline>
                        <xsl:value-of select="text()"/>
                    </fo:inline>
                </fo:block>
            </xsl:for-each>
        </xsl:if>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:leader/>
        </fo:block>
    </xsl:template>

    <xsl:template match="AC">
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>II.2.5) Critères d'attribution : </xsl:text>
                <xsl:if test="AC_PROCUREMENT_DOC">
                    <xsl:text>Critères énoncés dans les documents de la consultation.</xsl:text>
                </xsl:if>
                <xsl:if test="AC_QUALITY">
                    <xsl:text>Critères énoncés ci après (par ordre de priorité décroissante)</xsl:text>
                    <xsl:apply-templates select="AC_QUALITY"/>
                </xsl:if>
                <xsl:if test="AC_COST">
                    <xsl:text>Critères énoncés ci-après avec leur pondération</xsl:text>
                    <xsl:apply-templates select="AC_COST"/>
                </xsl:if>
                <xsl:if test="not(AC_QUALITY) and AC_PRICE">
                    <xsl:if test="AC_PRICE/AC_WEIGHTING">
                        <xsl:text>Critère unique de coût</xsl:text>
                    </xsl:if>
                    <xsl:if test="not(AC_PRICE/AC_WEIGHTING)">
                        <xsl:text>Critère unique de prix</xsl:text>
                    </xsl:if>
                </xsl:if>
            </fo:inline>
        </fo:block>
    </xsl:template>

    <xsl:template match="AC_QUALITY | AC_COST">
        <fo:list-block>
            <fo:list-item space-before="6pt" space-before.conditionality="retain"
                          start-indent="36pt">
                <fo:list-item-label start-indent="8pt">
                    <fo:block>
                        <fo:inline>
                            <xsl:text>-</xsl:text>
                        </fo:inline>
                    </fo:block>
                </fo:list-item-label>
                <fo:list-item-body start-indent="18pt">
                    <fo:block>
                        <fo:inline>
                            <xsl:value-of select="AC_CRITERION"/>
                            <xsl:if test="self::AC_COST">
                                <xsl:text> : </xsl:text>
                                <xsl:value-of select="AC_WEIGHTING"/>
                            </xsl:if>
                        </fo:inline>
                    </fo:block>
                </fo:list-item-body>
            </fo:list-item>
        </fo:list-block>
    </xsl:template>

    <xsl:template match="LEFTI">
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline font-weight="bold">
                <xsl:text>Section III : Renseignements d'ordre juridique, économique, financier et technique</xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>III.1) Conditions de participation</xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>Les conditions de participation, habilitations et capacités </xsl:text>
                <xsl:text>requises sont  définies dans les documents de la consultation. Les </xsl:text>
                <xsl:text>candidats doivent être conformes aux exigences qui y sont stipulées.</xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>III. 3) Forme juridique du groupement attributaire</xsl:text>
            </fo:inline>
        </fo:block>
        <xsl:for-each select="LEGAL_FORM/P">
            <fo:block space-before="6pt" space-before.conditionality="retain">
                <fo:inline>
                    <xsl:value-of select="text()"/>
                </fo:inline>
            </fo:block>
        </xsl:for-each>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:leader/>
        </fo:block>
    </xsl:template>

    <xsl:template match="PROCEDURE">
        <xsl:param name="no_gpa" select="NO_CONTRACT_COVERED_GPA"/>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline font-weight="bold">
                <xsl:text>Section IV : Procédure</xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>IV.1) Description</xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>IV.1.1) Type de procédure : </xsl:text>
                <xsl:if test="PT_OPEN">
                    <xsl:text>Appel d'offres ouvert</xsl:text>
                </xsl:if>
                <xsl:if test="PT_RESTRICTED">
                    <xsl:text>Appel d'offres restreint</xsl:text>
                </xsl:if>
                <xsl:if test="PT_NEGOTIATED_WITH_PRIOR_CALL">
                    <xsl:text>Procédure négociée avec mise en concurrence préalable</xsl:text>
                </xsl:if>
                <xsl:if test="PT_COMPETITIVE_DIALOGUE">
                    <xsl:text>Dialogue compétitif</xsl:text>
                </xsl:if>
                <xsl:if test="PT_INNOVATION_PARTNERSHIP">
                    <xsl:text>Partenariat d'innovation</xsl:text>
                </xsl:if>
                <xsl:if test="PT_01_AOO">
                    <xsl:text>Appel d'offre ouvert</xsl:text>
                </xsl:if>
                <xsl:if test="PT_03_MAPA">
                    <xsl:text>Procédure adaptée</xsl:text>
                </xsl:if>
                <xsl:if test="PT_04_Concours">
                    <xsl:text>Concours</xsl:text>
                </xsl:if>
                <xsl:if test="PT_12_Autre">
                    <xsl:text>Autre</xsl:text>
                </xsl:if>
                <xsl:if test="PT_13_AccordCadreSelection">
                    <xsl:text>Accord cadre</xsl:text>
                </xsl:if>
                <xsl:if test="PT_15_SAD">
                    <xsl:text>Système d'acquisition dynamique</xsl:text>
                </xsl:if>
                <xsl:if test="PT_08_MarcheNegocie">
                    <xsl:text>Procédure négociée</xsl:text>
                </xsl:if>
                <xsl:if test="PT_18_PCN">
                    <xsl:text>Procédure avec négociation</xsl:text>
                </xsl:if>
                <xsl:if test="PT_06_DialogueCompetitif">
                    <xsl:text>Dialogue compétitif</xsl:text>
                </xsl:if>
                <xsl:if test="PT_02_AOR">
                    <xsl:text>Appel d'offre restreint</xsl:text>
                </xsl:if>
                <xsl:if test="PT_05_ConcoursRestreint">
                    <xsl:text>Concours restreint</xsl:text>
                </xsl:if>
                <xsl:if test="PT_14_MarcheSubsequent">
                    <xsl:text>Marché subséquent</xsl:text>
                </xsl:if>
                <xsl:if test="PT_16_MarcheSpecifique">
                    <xsl:text>Marché spécifique</xsl:text>
                </xsl:if>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>IV.1.8) Information concernant l'accord sur les marchés publics (AMP)</xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>Le marché est couvert par l'accord sur les marchés publics : </xsl:text>
                <xsl:call-template name="yes_no">
                    <xsl:with-param name="no_value" select="$no_gpa"/>
                </xsl:call-template>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>IV.2) Renseignements d'ordre administratif</xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>IV.2.2) Date limite de réception des offres ou des demandes de participation : </xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:if test="DATE_RECEIPT_TENDERS != ''">
                    <xsl:value-of select="format-date(DATE_RECEIPT_TENDERS,'[D01]/[M01]/[Y0001]')"/>
                </xsl:if>
                <xsl:text> </xsl:text>
                <xsl:value-of select="TIME_RECEIPT_TENDERS"/>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>IV.2.6) Délai minimal pendant lequel le soumissionnaire est tenu de maintenir son offre : </xsl:text>
                <xsl:value-of select="DURATION_TENDER_VALID"/>
                <xsl:text> mois, à compter de la date limite de réception des offres</xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:leader/>
        </fo:block>
    </xsl:template>

    <xsl:template match="COMPLEMENTARY_INFO">
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline font-weight="bold">
                <xsl:text>Section VI : Renseignements complémentaires</xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>VI.3) Informations complémentaires</xsl:text>
            </fo:inline>
        </fo:block>
        <xsl:for-each select="INFO_ADD/P">
            <fo:block space-before="6pt" space-before.conditionality="retain">
                <fo:inline>
                    <xsl:value-of select="text()"/>
                </fo:inline>
            </fo:block>
        </xsl:for-each>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>VI.4) Procédures de recours</xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>VI.4.1) Instance chargée des procédures de recours : </xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:value-of select="ADDRESS_REVIEW_BODY/OFFICIALNAME"/>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:value-of select="ADDRESS_REVIEW_BODY/ADDRESS"/>
                <xsl:text> - </xsl:text>
                <xsl:value-of select="ADDRESS_REVIEW_BODY/POSTAL_CODE"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="ADDRESS_REVIEW_BODY/TOWN"/>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:value-of select="ADDRESS_REVIEW_BODY/URL"/>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:inline>
                <xsl:text>VI.5) Date d'envoi du présent avis : voir Section VIII</xsl:text>
            </fo:inline>
        </fo:block>
        <fo:block space-before="6pt" space-before.conditionality="retain">
            <fo:leader/>
        </fo:block>
    </xsl:template>

    <xsl:template name="yes_no">
        <xsl:param name="no_value"/>
        <xsl:choose>
            <xsl:when test="$no_value">
                <xsl:text>non</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>oui</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="filigrane">
        <xsl:attribute name="background-image">classpath:xsl/fo/filigrane.png</xsl:attribute>
        <xsl:attribute name="background-position-horizontal">center</xsl:attribute>
        <xsl:attribute name="background-position-vertical">center</xsl:attribute>
        <xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
    </xsl:template>

    <xsl:template match="text()|@*"/>
</xsl:stylesheet>
