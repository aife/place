<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="LEFTI">
        <xsl:param name="sigleForm"/>
        <xsl:if test="dossier_datas/data[(@nom='situation_situationPropre')]/valeur != ''">
            <SUITABILITY>
                <P>
                    <xsl:value-of
                            select="normalize-space(dossier_datas/data[(@nom='situation_situationPropre')]/valeur)"/>
                </P>
            </SUITABILITY>
        </xsl:if>

        <xsl:choose>
            <xsl:when test="dossier_datas/data[(@nom='situation_capaEcoFiDoc')]/referentiel/code ='1'">
                <ECONOMIC_CRITERIA_DOC/>
            </xsl:when>
            <xsl:when test="dossier_datas/data[(@nom='situation_capaEcoFiDoc')]/referentiel/code ='2'">
                <xsl:if test="dossier_datas/data[(@nom='situation_CapaEcoFiInfo')]/valeur != ''">
                    <ECONOMIC_FINANCIAL_INFO>
                        <P>
                            <xsl:value-of
                                    select="normalize-space(dossier_datas/data[(@nom='situation_CapaEcoFiInfo')]/valeur)"/>
                        </P>
                    </ECONOMIC_FINANCIAL_INFO>
                </xsl:if>
                <xsl:if test="dossier_datas/data[(@nom='situation_CapaEcoFiMin')]/valeur != ''">
                    <ECONOMIC_FINANCIAL_MIN_LEVEL>
                        <P>
                            <xsl:value-of
                                    select="normalize-space(dossier_datas/data[(@nom='situation_CapaEcoFiMin')]/valeur)"/>
                        </P>
                    </ECONOMIC_FINANCIAL_MIN_LEVEL>
                </xsl:if>
            </xsl:when>
        </xsl:choose>

        <xsl:choose>
            <xsl:when test="dossier_datas/data[(@nom='situation_capaTechDoc')]/referentiel/code ='1'">
                <TECHNICAL_CRITERIA_DOC/>
            </xsl:when>
            <xsl:when test="dossier_datas/data[(@nom='situation_capaTechDoc')]/referentiel/code ='2'">
                <xsl:if test="dossier_datas/data[(@nom='situation_CapaTechInfo')]/valeur != ''">
                    <TECHNICAL_PROFESSIONAL_INFO>
                        <P>
                            <xsl:value-of
                                    select="normalize-space(dossier_datas/data[(@nom='situation_CapaTechInfo')]/valeur)"/>
                        </P>
                    </TECHNICAL_PROFESSIONAL_INFO>
                </xsl:if>
                <xsl:if test="dossier_datas/data[(@nom='situation_CapaTechMin')]/valeur != ''">
                    <TECHNICAL_PROFESSIONAL_MIN_LEVEL>
                        <P>
                            <xsl:value-of
                                    select="normalize-space(dossier_datas/data[(@nom='situation_CapaTechMin')]/valeur)"/>
                        </P>
                    </TECHNICAL_PROFESSIONAL_MIN_LEVEL>
                </xsl:if>
            </xsl:when>
        </xsl:choose>

        <xsl:if test="dossier_datas/data[(@nom='situation_critereParticipation')]/valeur != ''">
            <RULES_CRITERIA>
                <P>
                    <xsl:value-of
                            select="normalize-space(dossier_datas/data[(@nom='situation_critereParticipation')]/valeur)"/>
                </P>
            </RULES_CRITERIA>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='situation_marcheReserve')]/referentiel/code = '1'">
            <xsl:if test="dossier_datas/data[(@nom='situation_marcheReserveOUI')]/item/referentielValeur/code = '01_lemarcheestreserveadesateliers' ">
                <RESTRICTED_SHELTERED_WORKSHOP/>
            </xsl:if>
            <xsl:if test="dossier_datas/data[(@nom='situation_marcheReserveOUI')]/item/referentielValeur/code = '02_lemarcheseraexecuteuniquement'">
                <RESTRICTED_SHELTERED_PROGRAM/>
            </xsl:if>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='qualification_conditions01')]/valeur != ''">
            <xsl:for-each select="dossier_datas/data[contains(@nom, 'qualification_conditions')]">
                <xsl:if test="./valeur != ''">
                    <QUALIFICATION>
                        <CONDITIONS>
                            <P>
                                <xsl:value-of select="normalize-space(./valeur)"/>
                            </P>
                        </CONDITIONS>
                        <xsl:variable name="qualification_methodes"
                                      select="concat('qualification_methodes',substring(./@nom,25,2))"/>
                        <xsl:if test="../data[@nom = $qualification_methodes]/valeur != ''">
                            <METHODS>
                                <P>
                                    <xsl:value-of
                                            select="normalize-space(../data[@nom = $qualification_methodes]/valeur)"/>
                                </P>
                            </METHODS>
                        </xsl:if>
                    </QUALIFICATION>
                </xsl:if>
            </xsl:for-each>
        </xsl:if>


        <xsl:if test="dossier_datas/data[(@nom='situation_garantiesExigees')]/valeur != ''">
            <DEPOSIT_GUARANTEE_REQUIRED>
                <P>
                    <xsl:value-of select="dossier_datas/data[(@nom='situation_garantiesExigees')]/valeur"/>
                </P>
            </DEPOSIT_GUARANTEE_REQUIRED>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='situation_modalitesPaiement')]/valeur != ''">
            <MAIN_FINANCING_CONDITION>
                <P>
                    <xsl:value-of select="dossier_datas/data[(@nom='situation_modalitesPaiement')]/valeur"/>
                </P>
            </MAIN_FINANCING_CONDITION>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='situation_formeJuridique')]/valeur != ''">
            <LEGAL_FORM>
                <P>
                    <xsl:value-of select="dossier_datas/data[(@nom='situation_formeJuridique')]/valeur"/>
                </P>
            </LEGAL_FORM>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='situation_criteresSelection')]/valeur != ''">
            <CRITERIA_SELECTION>
                <P>
                    <xsl:value-of
                            select="normalize-space(dossier_datas/data[(@nom='situation_criteresSelection')]/valeur)"/>
                </P>
            </CRITERIA_SELECTION>
        </xsl:if>

        <xsl:choose>
            <xsl:when test="$sigleForm = 'JOUE12'">
                <xsl:choose>
                    <xsl:when test="dossier_datas/data[(@nom='situation_PrestationReserveeON')]/referentiel/code = '1'">
                        <PARTICULAR_PROFESSION>
                            <P>
                                <xsl:value-of
                                        select="normalize-space(dossier_datas/data[(@nom='situation_PrestationParticuliereRef')]/valeur)"/>
                            </P>
                        </PARTICULAR_PROFESSION>
                    </xsl:when>
                    <xsl:when test="dossier_datas/data[(@nom='situation_PrestationReserveeON')]/referentiel/code = '2'">
                        <NO_PARTICULAR_PROFESSION/>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="dossier_datas/data[(@nom='situation_PrestationReserveeON')]/referentiel/code = '1'">
                    <PARTICULAR_PROFESSION CTYPE="SERVICES"/>

                    <xsl:if test="dossier_datas/data[(@nom='situation_PrestationReservee')]/valeur != ''">
                        <REFERENCE_TO_LAW>
                            <P>
                                <xsl:value-of
                                        select="normalize-space(dossier_datas/data[(@nom='situation_PrestationReservee')]/valeur)"/>
                            </P>
                        </REFERENCE_TO_LAW>
                    </xsl:if>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>

        <xsl:if test="dossier_datas/data[(@nom='situation_executionMarche')]/valeur != ''">
            <PERFORMANCE_CONDITIONS>
                <P>
                    <xsl:value-of
                            select="normalize-space(dossier_datas/data[(@nom='situation_executionMarche')]/valeur)"/>
                </P>
            </PERFORMANCE_CONDITIONS>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='situation_executionPersonnel')]/referentiel/code = '1'">
            <PERFORMANCE_STAFF_QUALIFICATION/>
        </xsl:if>


    </xsl:template>
</xsl:stylesheet>