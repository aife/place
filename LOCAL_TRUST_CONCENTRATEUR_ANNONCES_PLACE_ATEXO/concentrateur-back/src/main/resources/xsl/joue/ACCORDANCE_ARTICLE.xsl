<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="ACCORDANCE_ARTICLE">

        <D_ACCORDANCE_ARTICLE>

            <!-- JOUE 3-->
            <xsl:for-each select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure')]/item">
                <xsl:if test="referentielValeur/code = '01'">
                    <xsl:choose>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_infructueuxOuverte_OU_infructueuxRestreinte')]/referentiel/code = '01_une procÚdure ouverte'">
                            <D_PROC_OPEN/>
                        </xsl:when>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_infructueuxOuverte_OU_infructueuxRestreinte')]/referentiel/code = '02_une procÚdure restreinte'">
                            <D_PROC_RESTRICTED/>
                        </xsl:when>
                    </xsl:choose>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure')]/item">
                <xsl:if test="referentielValeur/code = '02'">
                    <D_MANUF_FOR_RESEARCH CTYPE="SUPPLIES"/>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure')]/item">
                <xsl:if test="referentielValeur/code = '03'">
                    <xsl:choose>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_prestataireDetermine')]/referentiel/code = '01'">
                            <D_TECHNICAL/>
                        </xsl:when>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_prestataireDetermine')]/referentiel/code = '02'">
                            <D_ARTISTIC/>
                        </xsl:when>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_prestataireDetermine')]/referentiel/code = '03'">
                            <D_PROTECT_RIGHTS/>
                        </xsl:when>
                    </xsl:choose>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure')]/item">
                <xsl:if test="referentielValeur/code = '04'">
                    <D_EXTREME_URGENCY/>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure')]/item">
                <xsl:if test="referentielValeur/code = '05'">
                    <D_ADD_DELIVERIES_ORDERED/>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure')]/item">
                <xsl:if test="referentielValeur/code = '06'">
                    <xsl:if test="(/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code != '') and (/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code != '02_fournitures')">
                        <D_REPETITION_EXISTING>
                            <xsl:attribute name="CTYPE">
                                <xsl:choose>
                                    <xsl:when
                                            test="/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '01_travaux'">
                                        WORKS
                                    </xsl:when>
                                    <xsl:when
                                            test="/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '03_services'">
                                        SERVICES
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:attribute>
                        </D_REPETITION_EXISTING>
                    </xsl:if>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure')]/item">
                <xsl:if test="referentielValeur/code = '07'">
                    <D_CONTRACT_AWARDED_DESIGN_CONTEST CTYPE="SERVICES"/>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure')]/item">
                <xsl:if test="referentielValeur/code = '08'">
                    <D_COMMODITY_MARKET CTYPE="SUPPLIES"/>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure')]/item">
                <xsl:if test="referentielValeur/code = '09'">
                    <xsl:choose>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_conditionsAvantageuses')]/referentiel/code = 'cessationActivite'">
                            <xsl:if test="(/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code != '') and (/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code != '01_travaux')">
                                <D_FROM_WINDING_PROVIDER>
                                    <xsl:attribute name="CTYPE">
                                        <xsl:choose>
                                            <xsl:when
                                                    test="/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '02_fournitures'">
                                                SUPPLIES
                                            </xsl:when>
                                            <xsl:when
                                                    test="/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '03_services'">
                                                SERVICES
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:attribute>
                                </D_FROM_WINDING_PROVIDER>
                            </xsl:if>
                        </xsl:when>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_conditionsAvantageuses')]/referentiel/code = 'liquidateur'">
                            <xsl:if test="(/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code != '') and (/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code != '01_travaux')">
                                <D_FROM_LIQUIDATOR_CREDITOR>
                                    <xsl:attribute name="CTYPE">
                                        <xsl:choose>
                                            <xsl:when
                                                    test="/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '02_fournitures'">
                                                SUPPLIES
                                            </xsl:when>
                                            <xsl:when
                                                    test="/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '03_services'">
                                                SERVICES
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:attribute>
                                </D_FROM_LIQUIDATOR_CREDITOR>
                            </xsl:if>
                        </xsl:when>
                    </xsl:choose>
                </xsl:if>
            </xsl:for-each>

            <!-- Fin JOUE-3 -->


            <!-- JOUE-6 -->
            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureJOUE6')]/item">
                <xsl:if test="referentielValeur/code = '01'">
                    <D_NO_TENDERS_REQUESTS/>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureJOUE6')]/item">
                <xsl:if test="referentielValeur/code = '10'">
                    <D_PURE_RESEARCH/>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureJOUE6')]/item">
                <xsl:if test="referentielValeur/code = '02'">
                    <xsl:choose>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_prestataireDetermine')]/referentiel/code = '01'">
                            <D_TECHNICAL/>
                        </xsl:when>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_prestataireDetermine')]/referentiel/code = '02'">
                            <D_ARTISTIC/>
                        </xsl:when>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_prestataireDetermine')]/referentiel/code = '03'">
                            <D_PROTECT_RIGHTS/>
                        </xsl:when>
                    </xsl:choose>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureJOUE6')]/item">
                <xsl:if test="referentielValeur/code = '03'">
                    <D_EXTREME_URGENCY/>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureJOUE6')]/item">
                <xsl:if test="referentielValeur/code = '04'">
                    <D_ADD_DELIVERIES_ORDERED/>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureJOUE6')]/item">
                <xsl:if test="referentielValeur/code = '05'">
                    <xsl:if test="(/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code != '') and (/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code != '02_fournitures')">
                        <D_REPETITION_EXISTING>
                            <xsl:attribute name="CTYPE">
                                <xsl:choose>
                                    <xsl:when
                                            test="/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '01_travaux'">
                                        WORKS
                                    </xsl:when>
                                    <xsl:when
                                            test="/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '03_services'">
                                        SERVICES
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:attribute>
                        </D_REPETITION_EXISTING>
                    </xsl:if>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureJOUE6')]/item">
                <xsl:if test="referentielValeur/code = '06'">
                    <D_CONTRACT_AWARDED_DESIGN_CONTEST CTYPE="SERVICES"/>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureJOUE6')]/item">
                <xsl:if test="referentielValeur/code = '07'">
                    <D_COMMODITY_MARKET CTYPE="SUPPLIES"/>
                </xsl:if>
            </xsl:for-each>


            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureJOUE6')]/item">
                <xsl:if test="referentielValeur/code = '09'">
                    <xsl:choose>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_conditionsAvantageuses')]/referentiel/code = 'cessationActivite'">
                            <xsl:if test="(/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code != '') and (/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code != '01_travaux')">
                                <D_FROM_WINDING_PROVIDER>
                                    <xsl:attribute name="CTYPE">
                                        <xsl:choose>
                                            <xsl:when
                                                    test="/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '02_fournitures'">
                                                SUPPLIES
                                            </xsl:when>
                                            <xsl:when
                                                    test="/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '03_services'">
                                                SERVICES
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:attribute>
                                </D_FROM_WINDING_PROVIDER>
                            </xsl:if>
                        </xsl:when>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_conditionsAvantageuses')]/referentiel/code = 'liquidateur'">
                            <xsl:if test="(/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code != '') and (/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code != '01_travaux')">
                                <D_FROM_LIQUIDATOR_CREDITOR>
                                    <xsl:attribute name="CTYPE">
                                        <xsl:choose>
                                            <xsl:when
                                                    test="/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '02_fournitures'">
                                                SUPPLIES
                                            </xsl:when>
                                            <xsl:when
                                                    test="/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '03_services'">
                                                SERVICES
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:attribute>
                                </D_FROM_LIQUIDATOR_CREDITOR>
                            </xsl:if>
                        </xsl:when>
                    </xsl:choose>
                </xsl:if>
            </xsl:for-each>


            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureJOUE6')]/item">
                <xsl:if test="referentielValeur/code = '09'">
                    <D_BARGAIN_PURCHASE/>
                </xsl:if>
            </xsl:for-each>

            <!-- Fin JOUE-6-->


            <!-- JOUE-15 -->
            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureAnnexeD')]/item">
                <xsl:if test="referentielValeur/code = '01'">
                    <D_NO_TENDERS_REQUESTS/>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureAnnexeD')]/item">
                <xsl:if test="referentielValeur/code = '17'">
                    <D_NO_TENDERS_REQUESTS/>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureAnnexeD')]/item">
                <xsl:if test="referentielValeur/code = '02'">
                    <xsl:choose>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_offresEnReponse')]/referentiel/code = '01'">
                            <D_PROC_OPEN/>
                        </xsl:when>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_offresEnReponse')]/referentiel/code = '02'">
                            <D_PROC_RESTRICTED/>
                        </xsl:when>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_offresEnReponse')]/referentiel/code = '04'">
                            <D_PROC_NEGOTIATED_PRIOR_CALL_COMPETITION/>
                        </xsl:when>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_offresEnReponse')]/referentiel/code = '03'">
                            <D_PROC_COMPETITIVE_DIALOGUE/>
                        </xsl:when>
                    </xsl:choose>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureAnnexeD')]/item">
                <xsl:if test="referentielValeur/code = '04'">
                    <xsl:if test="(/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code != '') and (/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code != '01_travaux')">
                        <D_OTHER_SERVICE>
                            <xsl:attribute name="CTYPE">
                                <xsl:choose>
                                    <xsl:when
                                            test="/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '02_fournitures'">
                                        SUPPLIES
                                    </xsl:when>
                                    <xsl:when
                                            test="/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '03_services'">
                                        SERVICES
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:attribute>
                        </D_OTHER_SERVICE>
                    </xsl:if>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureAnnexeD')]/item">
                <xsl:if test="referentielValeur/code = '16'">
                    <D_MANUF_FOR_RESEARCH CTYPE="SUPPLIES"/>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureAnnexeD')]/item">
                <xsl:if test="referentielValeur/code = '03'">
                    <D_PURE_RESEARCH/>
                </xsl:if>
            </xsl:for-each>


            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureAnnexeD')]/item">
                <xsl:if test="referentielValeur/code = '05'">
                    <D_ALL_TENDERS/>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureAnnexeD')]/item">
                <xsl:if test="referentielValeur/code = '06'">
                    <xsl:choose>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_prestataireDetermineAnenxeD')]/referentiel/code = '01'">
                            <D_TECHNICAL/>
                        </xsl:when>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_prestataireDetermineAnenxeD')]/referentiel/code = '02'">
                            <D_ARTISTIC/>
                        </xsl:when>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_prestataireDetermineAnenxeD')]/referentiel/code = '04'">
                            <D_ARTISTIC/>
                        </xsl:when>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_prestataireDetermineAnenxeD')]/referentiel/code = '05'">
                            <D_EXCLUSIVE_RIGHT/>
                        </xsl:when>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_prestataireDetermineAnenxeD')]/referentiel/code = '03'">
                            <D_PROTECT_RIGHTS/>
                        </xsl:when>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_prestataireDetermineAnenxeD')]/referentiel/code = '06'">
                            <D_PROTECT_RIGHTS/>
                        </xsl:when>
                    </xsl:choose>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureAnnexeD')]/item">
                <xsl:if test="referentielValeur/code = '07'">
                    <D_PERIODS_INCOMPATIBLE/>
                </xsl:if>
            </xsl:for-each>


            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureAnnexeD')]/item">
                <xsl:if test="referentielValeur/code = '08'">
                    <D_EXTREME_URGENCY/>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureAnnexeD')]/item">
                <xsl:if test="referentielValeur/code = '09'">
                    <D_ADD_DELIVERIES_ORDERED/>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureAnnexeD')]/item">
                <xsl:if test="referentielValeur/code = '10'">
                    <xsl:if test="(/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code != '') and (/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code != '02_fournitures')">
                        <D_REPETITION_EXISTING>
                            <xsl:attribute name="CTYPE">
                                <xsl:choose>
                                    <xsl:when
                                            test="/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '01_travaux'">
                                        WORKS
                                    </xsl:when>
                                    <xsl:when
                                            test="/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '03_services'">
                                        SERVICES
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:attribute>
                        </D_REPETITION_EXISTING>
                    </xsl:if>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureAnnexeD')]/item">
                <xsl:if test="referentielValeur/code = '11'">
                    <D_CONTRACT_AWARDED_DESIGN_CONTEST CTYPE="SERVICES"/>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureAnnexeD')]/item">
                <xsl:if test="referentielValeur/code = '12'">
                    <D_COMMODITY_MARKET CTYPE="SUPPLIES"/>
                </xsl:if>
            </xsl:for-each>


            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureAnnexeD')]/item">
                <xsl:if test="referentielValeur/code = '13'">
                    <xsl:choose>
                        <xsl:when
                                test="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_conditionsAvantageuses')]/referentiel/code = 'cessationActivite'">
                            <xsl:if test="(/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code != '') and (/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code != '01_travaux')">
                                <D_FROM_WINDING_PROVIDER>
                                    <xsl:attribute name="CTYPE">
                                        <xsl:choose>
                                            <xsl:when
                                                    test="/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '02_fournitures'">
                                                SUPPLIES
                                            </xsl:when>
                                            <xsl:when
                                                    test="/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '03_services'">
                                                SERVICES
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:attribute>
                                </D_FROM_WINDING_PROVIDER>
                            </xsl:if>
                        </xsl:when>
                        <xsl:when
                                test="/dossier/dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_conditionsAvantageuses')]/referentiel/code = 'liquidateur'">
                            <xsl:if test="(/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code != '') and (/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code != '01_travaux')">
                                <D_FROM_LIQUIDATOR_CREDITOR>
                                    <xsl:attribute name="CTYPE">
                                        <xsl:choose>
                                            <xsl:when
                                                    test="/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '02_fournitures'">
                                                SUPPLIES
                                            </xsl:when>
                                            <xsl:when
                                                    test="/dossier/dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '03_services'">
                                                SERVICES
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:attribute>
                                </D_FROM_LIQUIDATOR_CREDITOR>
                            </xsl:if>
                        </xsl:when>
                    </xsl:choose>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureAnnexeD')]/item">
                <xsl:if test="referentielValeur/code = '14'">
                    <D_BARGAIN_PURCHASE/>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each
                    select="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedureAnnexeD')]/item">
                <xsl:if test="referentielValeur/code = '15'">
                    <D_MARITIME_SERVICES CTYPE="SERVICES"/>
                </xsl:if>
            </xsl:for-each>


            <!-- Fin JOUE-15 -->

        </D_ACCORDANCE_ARTICLE>
    </xsl:template>
</xsl:stylesheet>
