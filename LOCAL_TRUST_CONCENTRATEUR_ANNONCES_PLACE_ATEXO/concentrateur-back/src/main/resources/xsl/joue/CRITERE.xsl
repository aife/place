<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="CRITERE">
        <xsl:param name="sigleForm"/>
        <xsl:choose>
            <xsl:when test="data[(@nom='criteresOUcriteresDoc')]/referentiel/code = 'critereCDC'">
                <AC>
                    <xsl:choose>
                        <xsl:when test="$sigleForm = 'JOUE6' or  $sigleForm = 'JOUE15'">
                            <xsl:choose>
                                <xsl:when test="data[(@nom='criteresPublication')]/referentiel/code = '1'">
                                    <xsl:attribute name="PUBLICATION">YES</xsl:attribute>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:attribute name="PUBLICATION">NO</xsl:attribute>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:when>
                    </xsl:choose>
                    <AC_PROCUREMENT_DOC/>
                </AC>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="(data[(@nom='criteresQualite')]/valeur = 'true') or (data[(@nom='critereCoutPrix')]/referentiel/code = '01_cout') or (data[(@nom='critereCoutPrix')]/referentiel/code = '02_prix')">
                    <AC>
                        <xsl:choose>
                            <xsl:when test="$sigleForm = 'JOUE6' or  $sigleForm = 'JOUE15'">
                                <xsl:choose>
                                    <xsl:when test="data[(@nom='criteresPublication')]/referentiel/code = '1'">
                                        <xsl:attribute name="PUBLICATION">YES</xsl:attribute>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:attribute name="PUBLICATION">NO</xsl:attribute>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                        </xsl:choose>
                        <xsl:if test="data[(@nom='criteresQualite')]/valeur = 'true'">
                            <xsl:for-each select="data[contains(./@nom, 'criteresQcritere')]">
                                <xsl:if test="./valeur != '' and not(contains(./@nom, 'Poids'))">
                                    <xsl:variable name="critereQPoidsEnCours"
                                                  select="concat('criteresQcriterePoids',substring(./@nom,17,2))"/>
                                    <AC_QUALITY>
                                        <AC_CRITERION>
                                            <xsl:value-of select="./valeur"/>
                                        </AC_CRITERION>
                                        <AC_WEIGHTING>
                                            <xsl:value-of select="../data[@nom = $critereQPoidsEnCours]/valeur"/>
                                        </AC_WEIGHTING>
                                    </AC_QUALITY>
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="data[(@nom='critereCoutPrix')]/referentiel/code = '01_cout'">
                            <xsl:for-each select="data[contains(./@nom, 'criteresECritere')]">
                                <xsl:if test="./valeur != '' and not(contains(./@nom, 'Poids'))">
                                    <xsl:variable name="critereEPoidsEnCours"
                                                  select="concat('criteresECriterePoids',substring(./@nom,17,2))"/>
                                    <AC_COST>
                                        <AC_CRITERION>
                                            <xsl:value-of select="./valeur"/>
                                        </AC_CRITERION>
                                        <AC_WEIGHTING>
                                            <xsl:value-of select="../data[@nom = $critereEPoidsEnCours]/valeur"/>
                                        </AC_WEIGHTING>
                                    </AC_COST>
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="data[(@nom='critereCoutPrix')]/referentiel/code = '02_prix'">
                            <AC_PRICE>
                                <xsl:if test="data[(@nom='criterePrixpoids')]/valeur != ''">
                                    <AC_WEIGHTING>
                                        <xsl:value-of select="data[(@nom='criterePrixpoids')]/valeur"/>
                                    </AC_WEIGHTING>
                                </xsl:if>
                            </AC_PRICE>
                        </xsl:if>
                    </AC>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>





