<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="PROCEDURE">
        <xsl:param name="sigleForm"/>

        <xsl:call-template name="TYPE_PROCEDURE">
            <xsl:with-param name="sigleForm" select="$sigleForm"/>
        </xsl:call-template>

        <xsl:if test="dossier_datas/data[(@nom='avisImplique_accordCadreOuiNon')]/referentiel/code = '1'">
            <FRAMEWORK>
                <xsl:choose>
                    <xsl:when
                            test="dossier_datas/data[(@nom='avisImplique_accordCadre')]/referentiel/code = '02_operateurPlusieurs'">
                        <SEVERAL_OPERATORS/>
                        <xsl:if test="dossier_datas/data[(@nom='avisImplique_accordCadre_operateurPlusieurs_maxNombreParticipant')]/valeur != ''">
                            <NB_PARTICIPANTS>
                                <xsl:value-of
                                        select="dossier_datas/data[(@nom='avisImplique_accordCadre_operateurPlusieurs_maxNombreParticipant')]/valeur"/>
                            </NB_PARTICIPANTS>
                        </xsl:if>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='avisImplique_accordCadre')]/referentiel/code = '01_operateurUnSeul'">
                        <SINGLE_OPERATOR/>
                    </xsl:when>
                </xsl:choose>

                <xsl:if test="dossier_datas/data[(@nom='avisImplique_accordCadre_justifSuperieurQuatreAns')]/valeur != ''">
                    <JUSTIFICATION>
                        <P>
                            <xsl:value-of
                                    select="normalize-space(dossier_datas/data[(@nom='avisImplique_accordCadre_justifSuperieurQuatreAns')]/valeur)"/>
                        </P>
                    </JUSTIFICATION>
                </xsl:if>
            </FRAMEWORK>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='avisImplique_SADOuiNon')]/referentiel/code = '1'">
            <DPS/>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='avisImplique_SAD_autreAcheteurSAD')]/referentiel/code = '1'">
            <DPS_ADDITIONAL_PURCHASERS/>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='avisImplique_reductionProgessive')]/referentiel/code = '1'">
            <REDUCTION_RECOURSE/>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='criteres_enchereElectronique')]/referentiel/code = '1'">
            <EAUCTION_USED/>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='criteres_enchereElectronique_rensComplEnchere')]/valeur != ''">
            <INFO_ADD_EAUCTION>
                <P>
                    <xsl:value-of
                            select="normalize-space(dossier_datas/data[(@nom='criteres_enchereElectronique_rensComplEnchere')]/valeur)"/>
                </P>
            </INFO_ADD_EAUCTION>
        </xsl:if>


        <xsl:if test="dossier_datas/data[(@nom='nbCandidats_criteresLimitation_attribOffresInitiales')]/referentiel/code = '1'">
            <RIGHT_CONTRACT_INITIAL_TENDERS/>
        </xsl:if>


        <xsl:choose>
            <xsl:when test="dossier_datas/data[(@nom='description_AMP')]/referentiel/code = '1'">
                <CONTRACT_COVERED_GPA/>
            </xsl:when>
            <xsl:when test="dossier_datas/data[(@nom='description_AMP')]/referentiel/code = '2'">
                <NO_CONTRACT_COVERED_GPA/>
            </xsl:when>
        </xsl:choose>

        <xsl:if test="dossier_datas/data[(@nom='criteresEvaluation')]/valeur != ''">
            <CRITERIA_EVALUATION>
                <xsl:choose>
                    <xsl:when
                            test="dossier_datas/data[(@nom='criteresEvaluation_accordPublication')]/referentiel/code = '1'">
                        <xsl:attribute name="PUBLICATION">YES</xsl:attribute>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='criteresEvaluation_accordPublication')]/referentiel/code = '2'">
                        <xsl:attribute name="PUBLICATION">NO</xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise></xsl:otherwise>
                </xsl:choose>
                <P>
                    <xsl:value-of select="normalize-space(dossier_datas/data[(@nom='criteresEvaluation')]/valeur)"/>
                </P>
            </CRITERIA_EVALUATION>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='publicationAnterieure_numAnnonce1')]/valeur != ''">
            <NOTICE_NUMBER_OJ>
                <xsl:value-of
                        select="concat(dossier_datas/data[(@nom='publicationAnterieure_numAnnonce1')]/valeur,'/S ',format-number(dossier_datas/data[(@nom='publicationAnterieure_numAnnonce2')]/valeur,'000'),'-',dossier_datas/data[(@nom='publicationAnterieure_numAnnonce3')]/valeur)"/>
            </NOTICE_NUMBER_OJ>
        </xsl:if>


        <xsl:if test="dossier_datas/data[(@nom='renseignementsAdm_receptionInteret')]/valeur != ''">
            <DATE_RECEIPT_TENDERS>
                <xsl:call-template name="typeDate">
                    <xsl:with-param name="date"
                                    select="dossier_datas/data[(@nom='renseignementsAdm_receptionInteret')]/valeur"/>
                </xsl:call-template>
            </DATE_RECEIPT_TENDERS>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='renseignementsAdm_receptionInteret')]/valeur != ''">
            <TIME_RECEIPT_TENDERS>
                <xsl:value-of
                        select="substring(dossier_datas/data[(@nom='renseignementsAdm_receptionInteret')]/valeur,12,5)"/>
            </TIME_RECEIPT_TENDERS>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='renseignementsAdm_dateEnvoiInvitation')]/valeur != ''">
            <DATE_DISPATCH_INVITATIONS>
                <xsl:call-template name="typeDate">
                    <xsl:with-param name="date"
                                    select="dossier_datas/data[(@nom='renseignementsAdm_dateEnvoiInvitation')]/valeur"/>
                </xsl:call-template>
            </DATE_DISPATCH_INVITATIONS>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='renseignementsAdm_langue_langEC')]/referentiel/code != ''">
            <LANGUAGES>
                <LANGUAGE>
                    <xsl:attribute name="VALUE">
                        <xsl:value-of
                                select="dossier_datas/data[(@nom='renseignementsAdm_langue_langEC')]/referentiel/code"/>
                    </xsl:attribute>
                </LANGUAGE>
                <xsl:if test="dossier_datas/data[(@nom='renseignementsAdm_langue_langEC2')]/referentiel/code != ''">
                    <LANGUAGE>
                        <xsl:attribute name="VALUE">
                            <xsl:value-of
                                    select="dossier_datas/data[(@nom='renseignementsAdm_langue_langEC2')]/referentiel/code"/>
                        </xsl:attribute>
                    </LANGUAGE>
                </xsl:if>
                <xsl:if test="dossier_datas/data[(@nom='renseignementsAdm_langue_langEC3')]/referentiel/code != ''">
                    <LANGUAGE>
                        <xsl:attribute name="VALUE">
                            <xsl:value-of
                                    select="dossier_datas/data[(@nom='renseignementsAdm_langue_langEC3')]/referentiel/code"/>
                        </xsl:attribute>
                    </LANGUAGE>
                </xsl:if>
            </LANGUAGES>
        </xsl:if>


        <xsl:choose>
            <xsl:when test="dossier_datas/data[(@nom='renseignementsAdm_validite')]/referentiel/code = 'delaidate'">
                <DATE_TENDER_VALID>
                    <xsl:call-template name="typeDate">
                        <xsl:with-param name="date"
                                        select="dossier_datas/data[(@nom='renseignementsAdm_validiteOffreDate')]/valeur"/>
                    </xsl:call-template>
                </DATE_TENDER_VALID>
            </xsl:when>
            <xsl:when test="dossier_datas/data[(@nom='renseignementsAdm_validite')]/referentiel/code = 'delaiduree'">
                <DURATION_TENDER_VALID TYPE="MONTH">
                    <xsl:value-of select="dossier_datas/data[(@nom='renseignementsAdm_validiteOffreDuree')]/valeur"/>
                </DURATION_TENDER_VALID>
            </xsl:when>
        </xsl:choose>

        <xsl:if test="dossier_datas/data[(@nom='renseignementsAdm_dateOuverture')]/valeur != ''">
            <OPENING_CONDITION>
                <DATE_OPENING_TENDERS>
                    <xsl:call-template name="typeDate">
                        <xsl:with-param name="date"
                                        select="dossier_datas/data[(@nom='renseignementsAdm_dateOuverture')]/valeur"/>
                    </xsl:call-template>
                </DATE_OPENING_TENDERS>

                <TIME_OPENING_TENDERS>
                    <xsl:value-of
                            select="substring(dossier_datas/data[(@nom='renseignementsAdm_dateOuverture')]/valeur,12,5)"/>
                </TIME_OPENING_TENDERS>

                <xsl:if test="dossier_datas/data[(@nom='renseignementsAdm_lieuOuverture')]/valeur != ''">
                    <PLACE>
                        <P>
                            <xsl:value-of
                                    select="normalize-space(dossier_datas/data[(@nom='renseignementsAdm_lieuOuverture')]/valeur)"/>
                        </P>
                    </PLACE>
                </xsl:if>

                <xsl:if test="dossier_datas/data[(@nom='renseignementsAdm_infoOuverture')]/valeur != ''">
                    <INFO_ADD>
                        <P>
                            <xsl:value-of
                                    select="normalize-space(dossier_datas/data[(@nom='renseignementsAdm_infoOuverture')]/valeur)"/>
                        </P>
                    </INFO_ADD>
                </xsl:if>
            </OPENING_CONDITION>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='renseignementsAdm_lancementProcAttrib')]/referentiel/code = '1'">
            <DATE_AWARD_SCHEDULED>
                <xsl:call-template name="typeDate">
                    <xsl:with-param name="date"
                                    select="dossier_datas/data[(@nom='renseignementsAdm_lancementProcAttrib')]/valeur"/>
                </xsl:call-template>
            </DATE_AWARD_SCHEDULED>
        </xsl:if>


        <xsl:if test="dossier_datas/data[(@nom='renseignementsAdm_abandonSAD')]/referentiel/code = '1'">
            <TERMINATION_DPS/>
        </xsl:if>
        <xsl:if test="dossier_datas/data[(@nom='renseignementsAdm_abandonProcedure')]/referentiel/code = '1'">
            <TERMINATION_PIN/>
        </xsl:if>

        <xsl:choose>
            <xsl:when test="dossier_datas/data[(@nom='renseignementsAdm_primeON')]/referentiel/code = '1'">
                <PRIZE_AWARDED/>
                <NUMBER_VALUE_PRIZE>
                    <P>
                        <xsl:value-of
                                select="normalize-space(dossier_datas/data[(@nom='renseignementsAdm_primeOui_montantPrime')]/valeur)"/>
                    </P>
                </NUMBER_VALUE_PRIZE>
            </xsl:when>
            <xsl:when test="dossier_datas/data[(@nom='renseignementsAdm_primeON')]/referentiel/code = '2'">
                <NO_PRIZE_AWARDED/>
            </xsl:when>
        </xsl:choose>

        <xsl:if test="dossier_datas/data[(@nom='renseignementsAdm_detailsPaiement')]/valeur != ''">
            <DETAILS_PAYMENT>
                <P>
                    <xsl:value-of
                            select="normalize-space(dossier_datas/data[(@nom='renseignementsAdm_detailsPaiement')]/valeur)"/>
                </P>
            </DETAILS_PAYMENT>
        </xsl:if>

        <xsl:choose>
            <xsl:when test="dossier_datas/data[(@nom='renseignementsAdm_marcheAttrAuLaureat')]/referentiel/code = '1'">
                <FOLLOW_UP_CONTRACTS/>
            </xsl:when>
            <xsl:when test="dossier_datas/data[(@nom='renseignementsAdm_marcheAttrAuLaureat')]/referentiel/code = '2'">
                <NO_FOLLOW_UP_CONTRACTS/>
            </xsl:when>
        </xsl:choose>

        <xsl:choose>
            <xsl:when
                    test="dossier_datas/data[(@nom='renseignementsAdm_decisionJuryContraignante')]/referentiel/code = '1'">
                <DECISION_BINDING_CONTRACTING/>
            </xsl:when>
            <xsl:when
                    test="dossier_datas/data[(@nom='renseignementsAdm_decisionJuryContraignante')]/referentiel/code = '2'">
                <NO_DECISION_BINDING_CONTRACTING/>
            </xsl:when>
        </xsl:choose>

        <xsl:for-each select="dossier_datas/data[contains(@nom, 'renseignementsAdm_membresJury')]">
            <xsl:if test="./valeur != ''">
                <MEMBER_NAME>
                    <xsl:value-of select="./valeur"/>
                </MEMBER_NAME>
            </xsl:if>
        </xsl:for-each>

    </xsl:template>
</xsl:stylesheet>