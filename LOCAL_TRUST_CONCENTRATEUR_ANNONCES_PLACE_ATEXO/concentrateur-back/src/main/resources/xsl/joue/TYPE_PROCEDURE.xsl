<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="TYPE_PROCEDURE">
        <xsl:param name="sigleForm"/>
        <xsl:choose>
            <xsl:when test="dossier_datas/data[(@nom='typeProcedureJOUE_2_3')]/referentiel/code != ''">
                <xsl:choose>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedureJOUE_2_3')]/referentiel/code = '01_ProcOuverte'">
                        <PT_OPEN/>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedureJOUE_2_3')]/referentiel/code = '02_ProcOuverteAcceleree'">
                        <PT_OPEN/>
                        <ACCELERATED_PROC>
                            <P>

                                <xsl:value-of
                                        select="normalize-space(dossier_datas/data[(@nom='procedure_justification')]/valeur)"/>

                            </P>
                        </ACCELERATED_PROC>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedureJOUE_2_3')]/referentiel/code = '03_ProcRestreinte'">
                        <PT_RESTRICTED/>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedureJOUE_2_3')]/referentiel/code = '04_ProcRestreinteAcceleree'">
                        <PT_RESTRICTED/>
                        <ACCELERATED_PROC>
                            <P>
                                <xsl:value-of
                                        select="normalize-space(dossier_datas/data[(@nom='procedure_justification')]/valeur)"/>
                            </P>
                        </ACCELERATED_PROC>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedureJOUE_2_3')]/referentiel/code = '05_ProcConcNegociee'">
                        <PT_COMPETITIVE_NEGOTIATION/>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedureJOUE_2_3')]/referentiel/code = '06_ProcConcNegocieeAcceleree'">
                        <PT_COMPETITIVE_NEGOTIATION/>
                        <ACCELERATED_PROC>
                            <P>
                                <xsl:value-of
                                        select="normalize-space(dossier_datas/data[(@nom='procedure_justification')]/valeur)"/>
                            </P>
                        </ACCELERATED_PROC>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedureJOUE_2_3')]/referentiel/code = '07_dialogueCompetitif'">
                        <PT_COMPETITIVE_DIALOGUE/>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedureJOUE_2_3')]/referentiel/code = '08_partenariatInnovation'">
                        <PT_INNOVATION_PARTNERSHIP/>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedureJOUE_2_3')]/referentiel/code = '09_ProcNegocieeSansPublication'">
                        <PT_AWARD_CONTRACT_WITHOUT_CALL>
                            <xsl:choose>
                                <xsl:when
                                        test="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_OU_justificationAttribution')]/referentiel/code = '01_justificationProcedure'">
                                    <xsl:call-template name="ACCORDANCE_ARTICLE"/>
                                </xsl:when>
                                <xsl:when
                                        test="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_OU_justificationAttribution')]/referentiel/code = '02_justificationAttribution'">
                                    <D_OUTSIDE_SCOPE/>
                                </xsl:when>
                            </xsl:choose>
                            <D_JUSTIFICATION>
                                <P>
                                    <xsl:value-of
                                            select="normalize-space(dossier_datas/data[(@nom='procNegocieeSansPublication_explication')]/valeur)"/>
                                </P>
                            </D_JUSTIFICATION>
                        </PT_AWARD_CONTRACT_WITHOUT_CALL>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>

            <xsl:when test="dossier_datas/data[(@nom='typeProcedure5')]/referentiel/code != ''">
                <xsl:choose>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedure5')]/referentiel/code = '01_ProcOuverte'">
                        <PT_OPEN/>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedure5')]/referentiel/code = '02_ProcRestreinte'">
                        <PT_RESTRICTED/>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedure5')]/referentiel/code = '03_ProcConcNegociee'">
                        <PT_NEGOTIATED_WITH_PRIOR_CALL/>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedure5')]/referentiel/code = '04_dialogueCompetitif'">
                        <PT_COMPETITIVE_DIALOGUE/>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedure5')]/referentiel/code = '05_partenariatInnovation'">
                        <PT_INNOVATION_PARTNERSHIP/>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedure5')]/referentiel/code = '06_ProcNegocieeSansPublication'">
                        <PT_AWARD_CONTRACT_WITHOUT_CALL>
                            <xsl:choose>
                                <xsl:when
                                        test="dossier_datas/data[(@nom='typeProcedure5')]/referentiel/code = '01_ProceRestreinte'">
                                    <D_ACCORDANCE_ARTICLE/>
                                </xsl:when>
                                <xsl:when
                                        test="dossier_datas/data[(@nom='typeProcedure5')]/referentiel/code = 'ProcédureNegocieeAvecAppelConc'">
                                    <D_OUTSIDE_SCOPE/>
                                </xsl:when>
                            </xsl:choose>
                            <!--<D_JUSTIFICATION>
                                <P>

                                        <xsl:value-of
                                                select="normalize-space(dossier_datas/data[(@nom='procNegocieeSansPublication_explication')]/valeur)" />

                                </P>
                            </D_JUSTIFICATION>-->
                        </PT_AWARD_CONTRACT_WITHOUT_CALL>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>

            <xsl:when test="dossier_datas/data[(@nom='typeProcedureJOUE_5_6')]/referentiel/code != ''">
                <xsl:choose>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedureJOUE_5_6')]/referentiel/code = '01_ProcOuverte'">
                        <PT_OPEN/>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedureJOUE_5_6')]/referentiel/code = '03_ProcRestreinte'">
                        <PT_RESTRICTED/>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedureJOUE_5_6')]/referentiel/code = '10_ProcedureNegocieeAvecAppelConc'">
                        <PT_NEGOTIATED_WITH_PRIOR_CALL/>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedureJOUE_5_6')]/referentiel/code = '07_dialogueCompetitif'">
                        <PT_COMPETITIVE_DIALOGUE/>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedureJOUE_5_6')]/referentiel/code = '08_partenariatInnovation'">
                        <PT_INNOVATION_PARTNERSHIP/>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedureJOUE_5_6')]/referentiel/code = '09_ProcNegocieeSansPublication'">
                        <PT_AWARD_CONTRACT_WITHOUT_CALL>
                            <xsl:choose>
                                <xsl:when
                                        test="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_OU_justificationAttributionJOUE6')]/referentiel/code = '01_justificationProcedure'">
                                    <xsl:call-template name="ACCORDANCE_ARTICLE"/>
                                </xsl:when>
                                <xsl:when
                                        test="dossier_datas/data[(@nom='procNegocieeSansPublication_justificationProcedure_OU_justificationAttributionJOUE6')]/referentiel/code = '02_justificationAttribution'">
                                    <D_OUTSIDE_SCOPE/>
                                </xsl:when>
                            </xsl:choose>
                            <D_JUSTIFICATION>
                                <P>
                                    <xsl:value-of
                                            select="normalize-space(dossier_datas/data[(@nom='procNegocieeSansPublication_explication')]/valeur)"/>
                                </P>
                            </D_JUSTIFICATION>
                        </PT_AWARD_CONTRACT_WITHOUT_CALL>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>


            <xsl:when test="dossier_datas/data[(@nom='typeProcedure3')]/referentiel/code != ''">
                <xsl:choose>
                    <xsl:when
                            test="dossier_datas/data[(@nom='TypeProcedure3')]/referentiel/code = '01_ProceRestreinte'">
                        <PT_RESTRICTED/>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='TypeProcedure3')]/referentiel/code = 'ProcédureNegocieeAvecAppelConc'">
                        <PT_NEGOTIATED_WITH_PRIOR_CALL/>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>

            <!-- cas JOUE4 -->
            <xsl:when test="dossier_datas/data[(@nom='typeProcedureJOUE_4')]/referentiel/code != ''">
                <xsl:choose>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedureJOUE_4')]/referentiel/code = '03_ProcRestreinte'">
                        <PT_RESTRICTED/>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedureJOUE_4')]/referentiel/code = '10_ProcedureNegocieeAvecAppelConc'">
                        <PT_NEGOTIATED_WITH_PRIOR_CALL/>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>

            <!-- cas JOUE1 -->
            <xsl:when test="dossier_datas/data[(@nom='typeProcedureJOUE_1')]/referentiel/code != ''">
                <xsl:choose>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedureJOUE_1')]/referentiel/code = '03_ProcRestreinte'">
                        <PT_RESTRICTED/>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedureJOUE_1')]/referentiel/code = '05_ProcConcNegociee'">
                        <PT_COMPETITIVE_NEGOTIATION/>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>

            <!-- cas JOUE12 ET 13 -->
            <xsl:when test="dossier_datas/data[(@nom='typeProcedureJOUE_12_13')]/referentiel/code != ''">
                <xsl:choose>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedureJOUE_12_13')]/referentiel/code = '11_concoursOuvert'">
                        <PT_OPEN/>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='typeProcedureJOUE_12_13')]/referentiel/code = '12_concoursRestreint'">
                        <PT_RESTRICTED/>
                        <xsl:if test="$sigleForm = 'JOUE12'">
                            <xsl:choose>
                                <xsl:when
                                        test="dossier_datas/data[(@nom='procedure_concoursRestreint_nbParticipants_OU_nbMinParticipantsNbMaxParticipants')]/referentiel/code = '01_NombreCandidats'">
                                    <NB_PARTICIPANTS>
                                        <xsl:value-of
                                                select="dossier_datas/data[(@nom='procedure_concoursRestreint_nbParticipants')]/valeur"/>
                                    </NB_PARTICIPANTS>
                                </xsl:when>
                                <xsl:when
                                        test="dossier_datas/data[(@nom='procedure_concoursRestreint_nbParticipants_OU_nbMinParticipantsNbMaxParticipants')]/referentiel/code = '02_NombreFourchette'">
                                    <NB_MIN_PARTICIPANTS>
                                        <xsl:value-of
                                                select="dossier_datas/data[(@nom='procedure_concoursRestreint_nbMinParticipants')]/valeur"/>
                                    </NB_MIN_PARTICIPANTS>
                                    <xsl:if test="dossier_datas/data[(@nom='procedure_concoursRestreint_nbMaxParticipants')]/valeur != ''">
                                        <NB_MAX_PARTICIPANTS>
                                            <xsl:value-of
                                                    select="dossier_datas/data[(@nom='procedure_concoursRestreint_nbMaxParticipants')]/valeur"/>
                                        </NB_MAX_PARTICIPANTS>
                                    </xsl:if>
                                </xsl:when>
                            </xsl:choose>

                            <xsl:for-each select="dossier_datas/data[contains(@nom, 'procedure_nomParticipant')]">
                                <xsl:if test="./valeur != ''">
                                    <PARTICIPANT_NAME>
                                        <xsl:value-of select="./valeur"/>
                                    </PARTICIPANT_NAME>
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:if>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>


            <xsl:when test="dossier_datas/data[(@nom='avisConcerneJoue15')]/referentiel/code='01_DIRECTIVE-23'">
                <DIRECTIVE_2014_23_EU>
                    <xsl:choose>
                        <xsl:when
                                test="dossier_datas/data[(@nom='typeProcedureJOUE_15')]/referentiel/code = '16_DIRECTIVE-23_ProcNegocieeSansPublication'">
                            <PT_AWARD_CONTRACT_WITHOUT_PUBLICATION>
                                <xsl:call-template name="ACCORDANCE_ARTICLE"/>
                                <D_JUSTIFICATION>
                                    <P>

                                        <xsl:value-of
                                                select="normalize-space(dossier_datas/data[(@nom='procedure_explication')]/valeur)"/>

                                    </P>
                                </D_JUSTIFICATION>
                            </PT_AWARD_CONTRACT_WITHOUT_PUBLICATION>
                        </xsl:when>
                        <xsl:when
                                test="dossier_datas/data[(@nom='typeProcedureJOUE_15')]/referentiel/code = '09_ProcNegocieeSansPublication'">
                            <PT_AWARD_CONTRACT_WITHOUT_CALL>
                                <D_OUTSIDE_SCOPE/>
                                <D_JUSTIFICATION>
                                    <P>

                                        <xsl:value-of
                                                select="normalize-space(dossier_datas/data[(@nom='procedure_explication')]/valeur)"/>

                                    </P>
                                </D_JUSTIFICATION>
                            </PT_AWARD_CONTRACT_WITHOUT_CALL>
                        </xsl:when>
                    </xsl:choose>
                </DIRECTIVE_2014_23_EU>
            </xsl:when>
            <xsl:when test="dossier_datas/data[(@nom='avisConcerneJoue15')]/referentiel/code='01_DIRECTIVE-24'">
                <DIRECTIVE_2014_24_EU>
                    <xsl:choose>
                        <xsl:when
                                test="dossier_datas/data[(@nom='typeProcedureJOUE_15')]/referentiel/code = '13_DIRECTIVE-24_ProcNegocieeSansPublication'">
                            <PT_NEGOTIATED_WITHOUT_PUBLICATION>
                                <xsl:call-template name="ACCORDANCE_ARTICLE"/>
                                <D_JUSTIFICATION>
                                    <P>

                                        <xsl:value-of
                                                select="normalize-space(dossier_datas/data[(@nom='procedure_explication')]/valeur)"/>

                                    </P>
                                </D_JUSTIFICATION>
                            </PT_NEGOTIATED_WITHOUT_PUBLICATION>
                        </xsl:when>
                        <xsl:when
                                test="dossier_datas/data[(@nom='typeProcedureJOUE_15')]/referentiel/code = '09_ProcNegocieeSansPublication'">
                            <PT_AWARD_CONTRACT_WITHOUT_CALL>
                                <D_OUTSIDE_SCOPE/>
                                <D_JUSTIFICATION>
                                    <P>

                                        <xsl:value-of
                                                select="normalize-space(dossier_datas/data[(@nom='procedure_explication')]/valeur)"/>

                                    </P>
                                </D_JUSTIFICATION>
                            </PT_AWARD_CONTRACT_WITHOUT_CALL>
                        </xsl:when>
                    </xsl:choose>
                </DIRECTIVE_2014_24_EU>
            </xsl:when>
            <xsl:when test="dossier_datas/data[(@nom='avisConcerneJoue15')]/referentiel/code='01_DIRECTIVE-25'">
                <DIRECTIVE_2014_25_EU>
                    <xsl:choose>

                        <xsl:when
                                test="dossier_datas/data[(@nom='typeProcedureJOUE_15')]/referentiel/code = '14_DIRECTIVE-25_ProcNegocieeSansPublication'">
                            <PT_NEGOTIATED_WITHOUT_PUBLICATION>
                                <xsl:call-template name="ACCORDANCE_ARTICLE"/>
                                <D_JUSTIFICATION>
                                    <P>

                                        <xsl:value-of
                                                select="normalize-space(dossier_datas/data[(@nom='procedure_explication')]/valeur)"/>

                                    </P>
                                </D_JUSTIFICATION>
                            </PT_NEGOTIATED_WITHOUT_PUBLICATION>
                        </xsl:when>
                        <xsl:when
                                test="dossier_datas/data[(@nom='typeProcedureJOUE_15')]/referentiel/code = '09_ProcNegocieeSansPublication'">
                            <PT_AWARD_CONTRACT_WITHOUT_CALL>
                                <D_OUTSIDE_SCOPE/>
                                <D_JUSTIFICATION>
                                    <P>

                                        <xsl:value-of
                                                select="normalize-space(dossier_datas/data[(@nom='procedure_explication')]/valeur)"/>

                                    </P>
                                </D_JUSTIFICATION>
                            </PT_AWARD_CONTRACT_WITHOUT_CALL>
                        </xsl:when>
                    </xsl:choose>
                </DIRECTIVE_2014_25_EU>
            </xsl:when>
            <xsl:when test="dossier_datas/data[(@nom='avisConcerneJoue15')]/referentiel/code='01_DIRECTIVE-81'">
                <DIRECTIVE_2009_81_EC>
                    <xsl:choose>
                        <xsl:when
                                test="dossier_datas/data[(@nom='typeProcedureJOUE_15')]/referentiel/code = '15_DIRECTIVE-81_ProcNegocieeSansPublication'">
                            <PT_NEGOTIATED_WITHOUT_PUBLICATION>
                                <xsl:call-template name="ACCORDANCE_ARTICLE"/>
                                <D_JUSTIFICATION>
                                    <P>
                                        <xsl:value-of
                                                select="normalize-space(dossier_datas/data[(@nom='procedure_explication')]/valeur)"/>
                                    </P>
                                </D_JUSTIFICATION>
                            </PT_NEGOTIATED_WITHOUT_PUBLICATION>
                        </xsl:when>
                        <xsl:when
                                test="dossier_datas/data[(@nom='typeProcedureJOUE_15')]/referentiel/code = '09_ProcNegocieeSansPublication'">
                            <PT_AWARD_CONTRACT_WITHOUT_CALL>
                                <xsl:choose>
                                    <xsl:when
                                            test="dossier_datas/data[(@nom='attributionSansMiseEnConcurrence_justificationAttribution_applicationServicesListes')]/referentiel/code = '01'">
                                        <D_SERVICES_LISTED/>
                                    </xsl:when>
                                    <xsl:when
                                            test="dossier_datas/data[(@nom='attributionSansMiseEnConcurrence_justificationAttribution_applicationServicesListes')]/referentiel/code = '02'">
                                        <D_OUTSIDE_SCOPE/>
                                    </xsl:when>
                                </xsl:choose>
                                <D_JUSTIFICATION>
                                    <P>
                                        <xsl:value-of
                                                select="normalize-space(dossier_datas/data[(@nom='procedure_explication')]/valeur)"/>
                                    </P>
                                </D_JUSTIFICATION>
                            </PT_AWARD_CONTRACT_WITHOUT_CALL>
                        </xsl:when>
                    </xsl:choose>
                </DIRECTIVE_2009_81_EC>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>