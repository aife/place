<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">

    <xsl:template name="F03_2014">
        <F03_2014 CATEGORY="ORIGINAL" FORM="F03">
            <xsl:attribute name="LG">
                <xsl:value-of select="dossier_datas/data[(@nom='languages')]/referentiel/code"/>
            </xsl:attribute>

            <LEGAL_BASIS>
                <xsl:call-template name="LEGAL_BASIS"/>
            </LEGAL_BASIS>

            <CONTRACTING_BODY>
                <xsl:call-template name="CONTRACTING_BODY">
                    <xsl:with-param name="formatAdresse" select="'CONTRACTING_BODY'"/>
                </xsl:call-template>
            </CONTRACTING_BODY>

            <OBJECT_CONTRACT>
                <xsl:call-template name="OBJECT_CONTRACT">
                    <xsl:with-param name="sigleForm" select="'JOUE3'"/>
                </xsl:call-template>
            </OBJECT_CONTRACT>


            <PROCEDURE>
                <xsl:call-template name="PROCEDURE">
                    <xsl:with-param name="sigleForm" select="'JOUE3'"/>
                </xsl:call-template>
            </PROCEDURE>


            <xsl:for-each select="dossier_datas/data[(@nom='attribution_lotMarche')]/liste_objet/objet">
                <AWARD_CONTRACT ITEM="{position()}">
                    <xsl:call-template name="AWARD_CONTRACT">
                        <xsl:with-param name="sigleForm" select="'JOUE3'"/>
                    </xsl:call-template>
                </AWARD_CONTRACT>
            </xsl:for-each>


            <COMPLEMENTARY_INFO>
                <xsl:call-template name="COMPLEMENTARY_INFO"/>
            </COMPLEMENTARY_INFO>
        </F03_2014>

    </xsl:template>
</xsl:stylesheet>