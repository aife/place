<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:n2016="http://publications.europa.eu/resource/schema/ted/2016/nuts"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="TYPE_ADDRESSE_CONTENEUR">

        <xsl:param name="formatAdresse"/>

        <OFFICIALNAME>
            <xsl:choose>
                <xsl:when test="data[(@nom='acheteurPublic')]/valeur != ''">
                    <xsl:value-of select="data[(@nom='acheteurPublic')]/valeur"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="data[(@nom='nomOfficiel')]/valeur"/>
                </xsl:otherwise>
            </xsl:choose>
        </OFFICIALNAME>

        <xsl:if test="data[(@nom='codeIdentificationNational')]/valeur != ''">
            <NATIONALID>
                <xsl:value-of select="data[(@nom= 'codeIdentificationNational' )]/valeur"/>
            </NATIONALID>
        </xsl:if>


        <xsl:if test="data[(@nom= 'adr' )]/adresse/@nomVoie != ''">
            <xsl:variable name="nomVoie" select="data[(@nom= 'adr' )]/adresse/@numeroVoie"/>
            <xsl:variable name="numeroVoie" select="data[(@nom= 'adr' )]/adresse/@nomVoie"/>
            <ADDRESS>
                <xsl:value-of select="concat($nomVoie,' ',$numeroVoie)"/>
            </ADDRESS>
        </xsl:if>

        <xsl:if test="data[(@nom= 'coordAdr' )]/adresse/@nomVoie != ''">
            <xsl:variable name="nomVoie" select="data[(@nom= 'coordAdr' )]/adresse/@numeroVoie"/>
            <xsl:variable name="numeroVoie" select="data[(@nom= 'coordAdr' )]/adresse/@nomVoie"/>
            <ADDRESS>
                <xsl:value-of select="concat($nomVoie,' ',$numeroVoie)"/>
            </ADDRESS>
        </xsl:if>

        <xsl:if test="data[(@nom= 'adr' )]/adresse/@nomCommune != ''">
            <TOWN>
                <xsl:value-of select="data[(@nom= 'adr' )]/adresse/@nomCommune"/>
            </TOWN>
        </xsl:if>

        <xsl:if test="data[(@nom= 'coordAdr' )]/adresse/@nomCommune != ''">
            <TOWN>
                <xsl:value-of select="data[(@nom= 'coordAdr' )]/adresse/@nomCommune"/>
            </TOWN>
        </xsl:if>


        <xsl:if test="data[(@nom= 'adr' )]/adresse/@codePostal != ''">
            <POSTAL_CODE>
                <xsl:value-of select="data[(@nom= 'adr' )]/adresse/@codePostal"/>
            </POSTAL_CODE>
        </xsl:if>

        <xsl:if test="data[(@nom= 'coordAdr' )]/adresse/@codePostal != ''">
            <POSTAL_CODE>
                <xsl:value-of select="data[(@nom= 'coordAdr' )]/adresse/@codePostal"/>
            </POSTAL_CODE>
        </xsl:if>


        <xsl:if test="data[(@nom='adr')]/adresse/pays/code != ''">
            <COUNTRY>
                <xsl:attribute name="VALUE">
                    <xsl:call-template name="codePays">
                        <xsl:with-param name="canal" select="'LESECHOS'"/>
                        <xsl:with-param name="emplacement"
                                        select="data[(@nom='adr')]/adresse/pays/code"/>
                    </xsl:call-template>
                </xsl:attribute>
            </COUNTRY>
        </xsl:if>

        <xsl:if test="data[(@nom='coordAdr')]/adresse/pays/code != ''">
            <COUNTRY>
                <xsl:attribute name="VALUE">
                    <xsl:call-template name="codePays">
                        <xsl:with-param name="canal" select="'LESECHOS'"/>
                        <xsl:with-param name="emplacement"
                                        select="data[(@nom='coordAdr')]/adresse/pays/code"/>
                    </xsl:call-template>
                </xsl:attribute>
            </COUNTRY>
        </xsl:if>

        <xsl:if test="data[(@nom= 'pointDeContact' )]/valeur != '' and $formatAdresse != 'CONTRACTING_BODY'">
            <CONTACT_POINT>
                <xsl:value-of select="data[(@nom= 'pointDeContact' )]/valeur"/>
            </CONTACT_POINT>
        </xsl:if>

        <xsl:if test="data[(@nom= 'coordTel' )]/valeur != ''">
            <PHONE>
                <xsl:value-of select="data[(@nom= 'coordTel')]/valeur"/>
            </PHONE>
        </xsl:if>

        <xsl:if test="data[(@nom= 'coordMel' )]/valeur != ''">
            <E_MAIL>
                <xsl:value-of select="data[(@nom= 'coordMel')]/valeur"/>
            </E_MAIL>
        </xsl:if>

        <xsl:if test="data[(@nom= 'coordFax' )]/valeur != ''">
            <FAX>
                <xsl:value-of select="data[(@nom= 'coordFax' )]/valeur"/>
            </FAX>
        </xsl:if>


        <n2016:NUTS>
            <xsl:choose>
                <xsl:when test="data[(@nom='acheteurPublic')]/valeur != ''">
                    <xsl:attribute name="CODE">
                        <xsl:value-of select="data[(@nom= 'lieuCodeNUTS1')]/referentiel/code"/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="CODE">
                        <xsl:value-of select="data[(@nom= 'codeNUTS')]/referentiel/code"/>
                    </xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>
        </n2016:NUTS>

        <xsl:if test="data[(@nom= 'coordUrl')]/valeur != ''">
            <xsl:choose>
                <xsl:when test="$formatAdresse = 'CONTRACTING_BODY'">
                    <URL_GENERAL>
                        <xsl:value-of select="data[(@nom= 'coordUrl' )]/valeur"/>
                    </URL_GENERAL>
                </xsl:when>
                <xsl:otherwise>
                    <URL>
                        <xsl:value-of select="data[(@nom= 'coordUrl' )]/valeur"/>
                    </URL>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>


        <xsl:if test="data[(@nom= 'urlProfilAcheteur' )]/valeur != '' and $formatAdresse != 'CONTRACTING_BODY'">
            <URL_BUYER>
                <xsl:value-of select="data[(@nom= 'urlProfilAcheteur' )]/valeur"/>
            </URL_BUYER>
        </xsl:if>

    </xsl:template>
</xsl:stylesheet>