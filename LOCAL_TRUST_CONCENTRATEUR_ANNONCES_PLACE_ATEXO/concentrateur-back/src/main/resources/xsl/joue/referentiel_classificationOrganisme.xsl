<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="classificationOrganisme">
        <xsl:param name="value"/>
        <xsl:choose>
            <xsl:when test="$value = '001_ministereoutouteautreautorite'">MINISTRY</xsl:when>
            <xsl:when test="$value = '002_agenceofficenational'">NATIONAL_AGENCY</xsl:when>
            <xsl:when test="$value = '003_autoriteregionaleoulocale'">REGIONAL_AUTHORITY</xsl:when>
            <xsl:when test="$value = '004_agenceofficeregionaleoulocale'">REGIONAL_AGENCY</xsl:when>
            <xsl:when test="$value = '005_organismededroitspublics'">BODY_PUBLIC</xsl:when>
            <xsl:when test="$value = '006_institutionagenceeuropéenne'">EU_INSTITUTION</xsl:when>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
			