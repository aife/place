<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:fn="http://www.w3.org/2005/xpath-functions"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="OBJECT_CONTRACT">
        <xsl:param name="sigleForm"/>
        <TITLE>
            <P>
                <xsl:value-of
                        select="normalize-space(dossier_datas/data[(@nom='descriptionMarche_titreMarche')]/valeur)"/>
            </P>
        </TITLE>

        <xsl:if test="dossier_datas/data[(@nom='descriptionMarche_numeroReference')]/valeur != ''">
            <REFERENCE_NUMBER>
                <xsl:value-of select="dossier_datas/data[(@nom='descriptionMarche_numeroReference')]/valeur"/>
            </REFERENCE_NUMBER>
        </xsl:if>

        <CPV_MAIN>
            <xsl:call-template name="CPV">
                <xsl:with-param name="principale" select="'descriptionMarche_CPV_objetPrincipal_classPrincipale'"/>
                <xsl:with-param name="supplementaire"
                                select="'descriptionMarche_CPV_objetPrincipal_classSupplementaire'"/>
            </xsl:call-template>
        </CPV_MAIN>

        <xsl:if test="dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code != ''">
            <TYPE_CONTRACT>
                <xsl:attribute name="CTYPE">
                    <xsl:choose>
                        <xsl:when
                                test="dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '01_travaux'">
                            WORKS
                        </xsl:when>
                        <xsl:when
                                test="dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '02_fournitures'">
                            SUPPLIES
                        </xsl:when>
                        <xsl:when
                                test="dossier_datas/data[(@nom='descriptionMarche_natureMarche')]/referentiel/code = '03_services'">
                            SERVICES
                        </xsl:when>
                    </xsl:choose>
                </xsl:attribute>
            </TYPE_CONTRACT>
        </xsl:if>

        <xsl:if test="$sigleForm != 'JOUE12' and $sigleForm != 'JOUE13'">
            <xsl:if test="dossier_datas/data[(@nom='descriptionMarche_description')]/valeur != ''">
                <SHORT_DESCR>
                    <P>
                        <xsl:value-of
                                select="normalize-space(dossier_datas/data[(@nom='descriptionMarche_description')]/valeur)"/>
                    </P>
                </SHORT_DESCR>
            </xsl:if>
        </xsl:if>
        <xsl:choose>
            <xsl:when
                    test="dossier_datas/data[(@nom='descriptionMarche_valeurTotal_valeur_OU_fourchette')]/referentiel/code = '01_valeur'">
                <VAL_TOTAL>
                    <xsl:if test="$sigleForm = 'JOUE6' or ($sigleForm = 'JOUE15' and dossier_datas/data[(@nom='baseLegal')]/referentiel/code = '32014L0025')">
                        <xsl:choose>
                            <xsl:when
                                    test="dossier_datas/data[(@nom='descriptionMarche_valeur_publication')]/referentiel/code = '1'">
                                <xsl:attribute name="PUBLICATION">YES</xsl:attribute>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:attribute name="PUBLICATION">NO</xsl:attribute>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:if>
                    <xsl:attribute name="CURRENCY">
                        <xsl:value-of
                                select="dossier_datas/data[(@nom='descriptionMarche_estimationValeurTotal_devise')]/referentiel/code"/>
                    </xsl:attribute>
                    <xsl:value-of
                            select="fn:replace(fn:replace(dossier_datas/data[(@nom='descriptionMarche_estimationValeurTotal')]/valeur,',','.'), '&#160;', '')"/>
                </VAL_TOTAL>
            </xsl:when>
            <xsl:when
                    test="dossier_datas/data[(@nom='descriptionMarche_valeurTotal_valeur_OU_fourchette')]/referentiel/code = '02_fourchette'">
                <VAL_RANGE_TOTAL>
                    <xsl:if test="$sigleForm = 'JOUE6' or ($sigleForm = 'JOUE15' and dossier_datas/data[(@nom='baseLegal')]/referentiel/code = '32014L0025')">
                        <xsl:choose>
                            <xsl:when
                                    test="dossier_datas/data[(@nom='descriptionMarche_valeur_publication')]/referentiel/code = '1'">
                                <xsl:attribute name="PUBLICATION">YES</xsl:attribute>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:attribute name="PUBLICATION">NO</xsl:attribute>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:if>
                    <xsl:attribute name="CURRENCY">
                        <xsl:value-of
                                select="dossier_datas/data[(@nom='descriptionMarche_offreBasse_devise')]/referentiel/code"/>
                    </xsl:attribute>
                    <LOW>
                        <xsl:value-of
                                select="fn:replace(fn:replace(dossier_datas/data[(@nom='descriptionMarche_offreBasse')]/valeur,',','.'),'&#160;','')"/>
                    </LOW>
                    <HIGH>
                        <xsl:value-of
                                select="fn:replace(fn:replace(dossier_datas/data[(@nom='descriptionMarche_offreHaute')]/valeur,',','.'),'&#160;','')"/>
                    </HIGH>
                </VAL_RANGE_TOTAL>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="dossier_datas/data[(@nom='descriptionMarche_estimationValeurTotal')]/valeur != ''">
                    <VAL_ESTIMATED_TOTAL>
                        <xsl:attribute name="CURRENCY">
                            <xsl:value-of
                                    select="dossier_datas/data[(@nom='descriptionMarche_estimationValeurTotal_devise')]/referentiel/code"/>
                        </xsl:attribute>
                        <xsl:value-of
                                select="fn:replace(fn:replace(dossier_datas/data[(@nom='descriptionMarche_estimationValeurTotal')]/valeur,',','.'), '&#160;', '')"/>  <!-- this is my code -->
                    </VAL_ESTIMATED_TOTAL>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>

        <xsl:choose>
            <xsl:when
                    test="dossier_datas/data[(@nom='descriptionMarche_marcheUnique_OU_lots')]/referentiel/code = '02_lots'">
                <LOT_DIVISION>
                    <xsl:choose>
                        <xsl:when
                                test="dossier_datas/data[(@nom='descriptionMarche_lots_unLot_OU_plusieursLot_OU_tousLots')]/referentiel/code = '03_tousLots'">
                            <LOT_ALL/>
                        </xsl:when>
                        <xsl:when
                                test="dossier_datas/data[(@nom='descriptionMarche_lots_unLot_OU_plusieursLot_OU_tousLots')]/referentiel/code = '02_nbMaxLotOffre'">
                            <LOT_MAX_NUMBER>
                                <xsl:value-of
                                        select="dossier_datas/data[(@nom='divisibleLotOui_nbMaxLotOffre')]/valeur"/>
                            </LOT_MAX_NUMBER>
                        </xsl:when>
                        <xsl:when
                                test="dossier_datas/data[(@nom='descriptionMarche_lots_unLot_OU_plusieursLot_OU_tousLots')]/referentiel/code = '01_unLot'">
                            <LOT_ONE_ONLY/>
                        </xsl:when>
                    </xsl:choose>

                    <xsl:if test="dossier_datas/data[(@nom='divisibleLotOui_nbMaxLotAttrib')]/valeur != ''">
                        <LOT_MAX_ONE_TENDERER>
                            <xsl:value-of select="dossier_datas/data[(@nom='divisibleLotOui_nbMaxLotAttrib')]/valeur"/>
                        </LOT_MAX_ONE_TENDERER>
                    </xsl:if>
                    <xsl:if test="dossier_datas/data[(@nom='divisibleLotOui_lotsReserves')]/valeur != ''">
                        <LOT_COMBINING_CONTRACT_RIGHT>
                            <P>
                                <xsl:value-of
                                        select="normalize-space(dossier_datas/data[(@nom='divisibleLotOui_lotsReserves')]/valeur)"/>
                            </P>
                        </LOT_COMBINING_CONTRACT_RIGHT>
                    </xsl:if>
                </LOT_DIVISION>

                <xsl:for-each select="dossier_datas/data[(@nom='situation_detailsLots')]/liste_objet/objet">
                    <OBJECT_DESCR>
                        <xsl:call-template name="OBJECT_DESCR">
                            <xsl:with-param name="position" select="position()"/>
                            <xsl:with-param name="sigleForm" select="$sigleForm"/>
                        </xsl:call-template>
                    </OBJECT_DESCR>
                </xsl:for-each>
            </xsl:when>

            <xsl:when
                    test="dossier_datas/data[(@nom='descriptionMarche_marcheUnique_OU_lots')]/referentiel/code = '01_marcheUnique'">
                <xsl:if test="$sigleForm != 'JOUE7'">
                    <NO_LOT_DIVISION/>
                </xsl:if>
                <xsl:for-each select="dossier_datas/data[(@nom='situation_detailsLots')]/liste_objet/objet">
                    <OBJECT_DESCR>
                        <xsl:call-template name="OBJECT_DESCR">
                            <xsl:with-param name="position" select="position()"/>
                            <xsl:with-param name="sigleForm" select="$sigleForm"/>
                        </xsl:call-template>
                    </OBJECT_DESCR>
                </xsl:for-each>
            </xsl:when>
        </xsl:choose>

        <xsl:if test="$sigleForm = 'JOUE12' or $sigleForm = 'JOUE13'">
            <OBJECT_DESCR>
                <xsl:call-template name="OBJECT_DESCR_12_13">
                </xsl:call-template>
            </OBJECT_DESCR>
        </xsl:if>
        <!-- Ci-dessous un ajout pour JOUE1, j'avoue ne pas avoir dans quel cas on l'ajoute... -->
        <xsl:if test="dossier_datas/data[(@nom='')]">
            <DATE_PUBLICATION_NOTICE/>
        </xsl:if>
        <!-- Ci-dessus un ajout pour JOUE1 -->
    </xsl:template>
</xsl:stylesheet>