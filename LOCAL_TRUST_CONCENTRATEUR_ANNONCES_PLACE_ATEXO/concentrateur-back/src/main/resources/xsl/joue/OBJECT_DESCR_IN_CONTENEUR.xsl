<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:fn="http://www.w3.org/2005/xpath-functions"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="OBJECT_DESCR_IN_CONTENEUR">
        <xsl:param name="position"/>
        <xsl:param name="sigleForm"/>
        <xsl:attribute name="ITEM">
            <xsl:value-of select="$position"/>
        </xsl:attribute>
        <xsl:if test="data[(@nom='CONTENEUR_NUMEROS_DETAILS_LOT_JOUE2')]/valeur = '0'">
            <xsl:attribute name="ITEM">
                <xsl:value-of select="data[(@nom='CONTENEUR_NUMEROS_DETAILS_LOT_JOUE2')]/valeur"/>
            </xsl:attribute>
        </xsl:if>

        <xsl:if test="data[(@nom='intituleLot')]/valeur != ''">
            <TITLE>
                <P>

                    <xsl:value-of select="normalize-space(data[(@nom='intituleLot')]/valeur)"/>

                </P>
            </TITLE>
        </xsl:if>
        <xsl:if test="data[(@nom='numLot')]/valeur != ''">
            <LOT_NO>
                <xsl:value-of select="data[(@nom='numLot')]/valeur"/>
            </LOT_NO>
        </xsl:if>

        <CPV_ADDITIONAL>
            <xsl:choose>
                <xsl:when test="$sigleForm = 'JOUE1'">
                    <xsl:call-template name="CPV_CONTENEUR_IN_CONTENEUR">
                        <xsl:with-param name="principale" select="'CPVObjetPrincipalClassPrincipaleAuto'"/>
                        <xsl:with-param name="supplementaire" select="'CPVObjetPrincipalClassSupplementaire'"/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="CPV_CONTENEUR">
                        <xsl:with-param name="principale" select="'CPVObjetPrincipalClassPrincipaleAuto'"/>
                        <xsl:with-param name="supplementaire" select="'CPVObjetPrincipalClassSupplementaire'"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </CPV_ADDITIONAL>

        <xsl:choose>
            <xsl:when test="$sigleForm = 'JOUE1'">
                <xsl:call-template name="NUTS_CONTENEUR_IN_CONTENEUR">
                    <xsl:with-param name="value" select="'lieuCodeNUTS'"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="NUTS_CONTENEUR_IN_CONTENEUR">
                    <xsl:with-param name="value" select="'lieuCodeNUTS'"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>

        <xsl:if test=" ../../data[(@nom='lieuExecutionLivraison')]/valeur != ''">
            <MAIN_SITE>
                <P>

                    <xsl:value-of
                            select="normalize-space(../../data[(@nom='lieuExecutionLivraison')]/valeur)"/>

                </P>
            </MAIN_SITE>
        </xsl:if>

        <SHORT_DESCR>
            <P>

                <xsl:value-of select="normalize-space(../../data[(@nom='description')]/valeur)"/>

            </P>
        </SHORT_DESCR>

        <xsl:call-template name="CRITERE">
            <xsl:with-param name="sigleForm" select="$sigleForm"/>
        </xsl:call-template>


        <xsl:if test="../../data[(@nom='estimationValeurValeur')]/valeur != ''">
            <VAL_OBJECT>
                <xsl:attribute name="CURRENCY">
                    <xsl:value-of select="../../data[(@nom='estimationValeurDevise')]/referentiel/code"/>
                </xsl:attribute>
                <xsl:value-of
                        select="fn:replace(fn:replace(../../data[(@nom='estimationValeurValeur')]/valeur,',','.'), '&#160;', '')"/>
            </VAL_OBJECT>
        </xsl:if>

        <xsl:choose>
            <xsl:when test="../../data[(@nom='dureeNbOUdate')]/referentiel/code = 'delaiduree'">
                <DURATION>
                    <xsl:attribute name="TYPE">
                        <xsl:choose>
                            <xsl:when
                                    test="../../data[(@nom='dureeNbMoisOUNbJours')]/referentiel/code = 'unitedureejours'">
                                DAY
                            </xsl:when>
                            <xsl:when
                                    test="../../data[(@nom='dureeNbMoisOUNbJours')]/referentiel/code = 'unitedureemois'">
                                MONTH
                            </xsl:when>
                        </xsl:choose>
                    </xsl:attribute>
                    <xsl:value-of select="../../data[(@nom='dureeNb')]/valeur"/>
                </DURATION>
            </xsl:when>

            <xsl:when
                    test="../../data[(@nom='dureeNbOUdate')]/referentiel/code = 'delaidate'">
                <xsl:if test="../../data[(@nom='dureeDateAcompterDu')]/valeur != ''">
                    <DATE_START>
                        <xsl:call-template name="typeDate">
                            <xsl:with-param name="date"
                                            select="../../data[(@nom='dureeDateAcompterDu')]/valeur"/>
                        </xsl:call-template>
                    </DATE_START>
                </xsl:if>
                <xsl:if test="../../data[(@nom='dureeDateJusquAu')]/valeur != ''">
                    <DATE_END>
                        <xsl:call-template name="typeDate">
                            <xsl:with-param name="date"
                                            select="../../data[(@nom='dureeDateJusquAu')]/valeur"/>
                        </xsl:call-template>
                    </DATE_END>
                </xsl:if>
            </xsl:when>
        </xsl:choose>

        <xsl:choose>
            <xsl:when test="../../data[(@nom='dureeIndeterminee')]/referentiel/code = '1'">
                <INDEFINITE_DURATION/>
            </xsl:when>
            <xsl:when test="../../data[(@nom='dureeIndeterminee')]/referentiel/code = '2'">
                <DATE_START>
                    <xsl:call-template name="typeDate">
                        <xsl:with-param name="date" select="../../data[(@nom='dateQualifACompterDu')]/valeur"/>
                    </xsl:call-template>
                </DATE_START>
                <DATE_END>
                    <xsl:call-template name="typeDate">
                        <xsl:with-param name="date" select="../../data[(@nom='dateQualifJusquau')]/valeur"/>
                    </xsl:call-template>
                </DATE_END>
            </xsl:when>
        </xsl:choose>

        <xsl:choose>
            <xsl:when
                    test="../../data[(@nom='infosSupReconductionOuiOUReconductionNon')]/referentiel/code = '1'">
                <RENEWAL/>
                <RENEWAL_DESCR>
                    <P>

                        <xsl:value-of select="normalize-space(../../data[(@nom='infosSupReconductionDesc')]/valeur)"/>

                    </P>
                </RENEWAL_DESCR>
            </xsl:when>
            <xsl:when
                    test="../../data[(@nom='infosSupReconductionOuiOUReconductionNon')]/referentiel/code = '2'">
                <xsl:if test="$sigleForm = 'JOUE2' or $sigleForm = 'JOUE5'">
                    <NO_RENEWAL/>
                </xsl:if>
            </xsl:when>
        </xsl:choose>

        <xsl:if test="$sigleForm = 'JOUE5' and dossier/dossier_datas/data[(@nom='typeProcedureJOUE_5_6')]/referentiel/code != '01_ProcOuverte'">
            <xsl:choose>
                <xsl:when
                        test="../../data[(@nom='nbCandidatNombreOUFourchette')]/referentiel/code = '01_NombreCandidats'">
                    <NB_ENVISAGED_CANDIDATE>
                        <xsl:value-of select="../../data[(@nom='candidatsNbEnvisageCandidat')]/valeur"/>
                    </NB_ENVISAGED_CANDIDATE>
                </xsl:when>

                <xsl:when
                        test="../../data[(@nom='nbCandidatNombreOUFourchette')]/referentiel/code = '02_NombreFourchette'">
                    <xsl:if test="../../data[(@nom='candidatsNbMinCandidat')]/valeur != ''">
                        <NB_MIN_LIMIT_CANDIDATE>
                            <xsl:value-of select="../../data[(@nom='candidatsNbMinCandidat')]/valeur"/>
                        </NB_MIN_LIMIT_CANDIDATE>
                    </xsl:if>
                    <xsl:if test="../../data[(@nom='candidatsNbMaxCandidat')]/valeur != ''">
                        <NB_MAX_LIMIT_CANDIDATE>
                            <xsl:value-of select="../../data[(@nom='candidatsNbMaxCandidat')]/valeur"/>
                        </NB_MAX_LIMIT_CANDIDATE>
                    </xsl:if>
                </xsl:when>
            </xsl:choose>
        </xsl:if>

        <xsl:if test="$sigleForm = 'JOUE2' and dossier/dossier_datas/data[(@nom='(typeProcedureJOUE_2_3')]/referentiel/code != '01_ProcOuverte' and dossier/dossier_datas/../../data[(@nom='(typeProcedureJOUE_2_3')]/referentiel/code != '02_ProcOuverteAcceleree'">
            <xsl:choose>
                <xsl:when
                        test="../../data[(@nom='nbCandidatNombreOUFourchette')]/referentiel/code = '01_NombreCandidats'">
                    <NB_ENVISAGED_CANDIDATE>
                        <xsl:value-of select="../../data[(@nom='candidatsNbEnvisageCandidat')]/valeur"/>
                    </NB_ENVISAGED_CANDIDATE>
                </xsl:when>

                <xsl:when
                        test="../../data[(@nom='nbCandidatNombreOUFourchette')]/referentiel/code = '02_NombreFourchette'">
                    <xsl:if test="../../data[(@nom='candidatsNbMinCandidat')]/valeur != ''">
                        <NB_MIN_LIMIT_CANDIDATE>
                            <xsl:value-of select="../../data[(@nom='candidatsNbMinCandidat')]/valeur"/>
                        </NB_MIN_LIMIT_CANDIDATE>
                    </xsl:if>
                    <xsl:if test="../../data[(@nom='candidatsNbMaxCandidat')]/valeur != ''">
                        <NB_MAX_LIMIT_CANDIDATE>
                            <xsl:value-of select="../../data[(@nom='candidatsNbMaxCandidat')]/valeur"/>
                        </NB_MAX_LIMIT_CANDIDATE>
                    </xsl:if>
                </xsl:when>
            </xsl:choose>
        </xsl:if>

        <xsl:if test="../../data[(@nom='candidatsCriteresCandidat')]/valeur != ''">
            <CRITERIA_CANDIDATE>
                <P>

                    <xsl:value-of select="normalize-space(../../data[(@nom='candidatsCriteresCandidat')]/valeur)"/>

                </P>
            </CRITERIA_CANDIDATE>
        </xsl:if>

        <xsl:choose>
            <xsl:when test="../../data[(@nom='caracteristiquesVariante')]/referentiel/code = '1'">
                <ACCEPTED_VARIANTS/>
            </xsl:when>
            <xsl:when
                    test="../../data[(@nom='caracteristiquesVariante')]/referentiel/code = '2'">
                <xsl:if test="$sigleForm = 'JOUE2' or $sigleForm = 'JOUE5'">
                    <NO_ACCEPTED_VARIANTS/>
                </xsl:if>
            </xsl:when>
        </xsl:choose>

        <xsl:choose>
            <xsl:when
                    test="../../data[(@nom='infosSupOptionOuiOUoptionNON')]/referentiel/code = '1'">
                <OPTIONS/>
                <OPTIONS_DESCR>
                    <P>

                        <xsl:value-of select="normalize-space(../../data[(@nom='infosSupOptionDescription')]/valeur)"/>

                    </P>
                </OPTIONS_DESCR>
            </xsl:when>
            <xsl:when test="../../data[(@nom='infosSupOptionOuiOUoptionNON')]/referentiel/code = '2'">
                <xsl:if test="$sigleForm != 'JOUE1' and $sigleForm != 'JOUE4'">
                    <NO_OPTIONS/>
                </xsl:if>
            </xsl:when>
        </xsl:choose>

        <xsl:if test="../../data[(@nom='infosSupCatalogueElecOuiOUcatalogueElecNon')]/referentiel/code = '1'">
            <ECATALOGUE_REQUIRED/>
        </xsl:if>

        <xsl:choose>
            <xsl:when
                    test="../../data[(@nom='infosSupFondsCommunautairesRefOUfondsCommunautairesNon')]/referentiel/code = '1'">
                <EU_PROGR_RELATED>
                    <P>
                        <xsl:value-of
                                select="normalize-space(../../data[(@nom='infosSupFondsCommunautairesRef')]/valeur)"/>
                    </P>
                </EU_PROGR_RELATED>
            </xsl:when>
            <xsl:when
                    test="../../data[(@nom='infosSupFondsCommunautairesRefOUfondsCommunautairesNon')]/referentiel/code = '2'">
                <NO_EU_PROGR_RELATED/>
            </xsl:when>
        </xsl:choose>

        <xsl:if test="../../data[(@nom='infosComplementaires')]/valeur != ''">
            <INFO_ADD>
                <P>

                    <xsl:value-of select="normalize-space(../../data[(@nom='infosComplementaires')]/valeur)"/>

                </P>
            </INFO_ADD>
        </xsl:if>

    </xsl:template>
</xsl:stylesheet>