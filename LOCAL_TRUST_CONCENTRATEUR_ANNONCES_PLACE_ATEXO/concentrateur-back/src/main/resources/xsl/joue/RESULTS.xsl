<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:fn="http://www.w3.org/2005/xpath-functions"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="RESULTS">
        <xsl:choose>
            <xsl:when test="dossier_datas/data[(@nom='attribution_primeON')]/referentiel/code='2'">
                <AWARDED_PRIZE>

                    <DATE_DECISION_JURY>
                        <xsl:call-template name="typeDate">
                            <xsl:with-param name="date"
                                            select="dossier_datas/data[(@nom='attribution_dateAttribution')]/valeur"/>
                        </xsl:call-template>
                    </DATE_DECISION_JURY>

                    <PARTICIPANTS>
                        <xsl:choose>
                            <xsl:when
                                    test="dossier_datas/data[(@nom='attribution_nbParticipants_accordPublication')]/referentiel/code = '1'">
                                <xsl:attribute name="PUBLICATION">YES</xsl:attribute>
                            </xsl:when>
                            <xsl:when
                                    test="dossier_datas/data[(@nom='attribution_nbParticipants_accordPublication')]/referentiel/code = '2'">
                                <xsl:attribute name="PUBLICATION">NO</xsl:attribute>
                            </xsl:when>
                        </xsl:choose>
                        <NB_PARTICIPANTS>
                            <xsl:value-of select="dossier_datas/data[(@nom='attribution_nbParticipants')]/valeur"/>
                        </NB_PARTICIPANTS>

                        <xsl:if test="(dossier_datas/data[(@nom='attribution_nbParticipantsPME')]/valeur != '') and (dossier_datas/data[(@nom='attribution_nbParticipantsNonUE')]/valeur != '') ">
                            <NB_PARTICIPANTS_SME>
                                <xsl:value-of
                                        select="dossier_datas/data[(@nom='attribution_nbParticipantsPME')]/valeur"/>
                            </NB_PARTICIPANTS_SME>

                            <NB_PARTICIPANTS_OTHER_EU>
                                <xsl:value-of
                                        select="dossier_datas/data[(@nom='attribution_nbParticipantsNonUE')]/valeur"/>
                            </NB_PARTICIPANTS_OTHER_EU>
                        </xsl:if>
                    </PARTICIPANTS>

                    <WINNERS>
                        <xsl:choose>
                            <xsl:when
                                    test="dossier_datas/data[(@nom='attribution_titulaire_accordPublication')]/referentiel/code = '1'">
                                <xsl:attribute name="PUBLICATION">YES</xsl:attribute>
                            </xsl:when>
                            <xsl:when
                                    test="dossier_datas/data[(@nom='attribution_titulaire_accordPublication')]/referentiel/code = '2'">
                                <xsl:attribute name="PUBLICATION">NO</xsl:attribute>
                            </xsl:when>
                        </xsl:choose>
                        <xsl:for-each select="dossier_datas/data[(@nom='attribution_titulaire')]/liste_objet/objet">
                            <WINNER>
                                <ADDRESS_WINNER>
                                    <xsl:call-template name="TYPE_ADDRESSE_CONTENEUR"/>
                                </ADDRESS_WINNER>
                                <xsl:choose>
                                    <xsl:when test="data[(@nom='PME')]/referentiel/code='1'">
                                        <SME/>
                                    </xsl:when>
                                    <xsl:when test="data[(@nom='PME')]/referentiel/code='2'">
                                        <NO_SME/>
                                    </xsl:when>
                                </xsl:choose>
                            </WINNER>
                        </xsl:for-each>
                    </WINNERS>

                    <xsl:if test="dossier_datas/data[(@nom='attribution_montantPrime')]/valeur != ''">
                        <VAL_PRIZE>
                            <xsl:attribute name="CURRENCY">
                                <xsl:value-of
                                        select="dossier_datas/data[(@nom='attribution_devisePrime')]/referentiel/code"/>
                            </xsl:attribute>
                            <xsl:choose>
                                <xsl:when
                                        test="dossier_datas/data[(@nom='attribution_montantPrime_accordPublication')]/referentiel/code = '1'">
                                    <xsl:attribute name="PUBLICATION">YES</xsl:attribute>
                                </xsl:when>
                                <xsl:when
                                        test="dossier_datas/data[(@nom='attribution_montantPrime_accordPublication')]/referentiel/code = '2'">
                                    <xsl:attribute name="PUBLICATION">NO</xsl:attribute>
                                </xsl:when>
                            </xsl:choose>
                            <xsl:value-of
                                    select="fn:replace(fn:replace(dossier_datas/data[(@nom='attribution_montantPrime')]/valeur,',','.'), '&#160;', '')"/>
                        </VAL_PRIZE>
                    </xsl:if>
                </AWARDED_PRIZE>
            </xsl:when>

            <xsl:when test="dossier_datas/data[(@nom='attribution_primeON')]/referentiel/code='1'">
                <NO_AWARDED_PRIZE>
                    <xsl:choose>
                        <xsl:when
                                test="dossier_datas/data[(@nom='attribution_lotMarche_attributionNon')]/referentiel/code='procedureInterrompue'">
                            <PROCUREMENT_DISCONTINUED>
                                <ORIGINAL_OTHER_MEANS PUBLICATION="NO">
                                    <xsl:value-of
                                            select="dossier_datas/data[(@nom='attribution_procedureInterrompue_numAnnonce')]/valeur"/>
                                </ORIGINAL_OTHER_MEANS>
                                <DATE_DISPATCH_ORIGINAL PUBLICATION="NO">
                                    <xsl:call-template name="typeDate">
                                        <xsl:with-param name="date"
                                                        select="dossier_datas/data[(@nom='attribution_procedureInterrompue_dateEnvoiInitial')]/valeur"/>
                                    </xsl:call-template>
                                </DATE_DISPATCH_ORIGINAL>
                            </PROCUREMENT_DISCONTINUED>
                        </xsl:when>
                        <xsl:when
                                test="dossier_datas/data[(@nom='attribution_lotMarche_attributionNon')]/referentiel/code='procedureInfructueuse'">
                            <PROCUREMENT_UNSUCCESSFUL/>
                        </xsl:when>
                    </xsl:choose>
                </NO_AWARDED_PRIZE>
            </xsl:when>
        </xsl:choose>

    </xsl:template>

</xsl:stylesheet>
