<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="CONTRACTOR">
        <xsl:choose>
            <xsl:when test="data[(@nom='attribGroupement')]/referentiel/code = '1'">
                <AWARDED_TO_GROUP/>
                <xsl:for-each select="data[(@nom='titulaires')]/item">

                    <xsl:if test="current()[@nom='idAttributaire']">
                        <xsl:variable name="attributaireId" select="current()[(@nom='idAttributaire')]/@valeur"/>
                        <xsl:for-each
                                select="/dossier/dossier_datas/data[(@nom='attribution_titulaire')]/liste_objet/objet">
                            <xsl:if test="$attributaireId = data[(@nom='idAttributaire')]/valeur">
                                <CONTRACTOR>
                                    <ADDRESS_CONTRACTOR>
                                        <xsl:call-template name="TYPE_ADDRESSE_CONTENEUR"/>
                                    </ADDRESS_CONTRACTOR>
                                    <xsl:choose>
                                        <xsl:when test="data[(@nom='PME')]/referentiel/code = '1'">
                                            <SME/>
                                        </xsl:when>
                                        <xsl:when test="data[(@nom='PME')]/referentiel/code = '2'">
                                            <NO_SME/>
                                        </xsl:when>
                                    </xsl:choose>
                                </CONTRACTOR>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:if>
                </xsl:for-each>
            </xsl:when>

            <xsl:when test="data[(@nom='attribGroupement')]/referentiel/code = '2'">
                <NO_AWARDED_TO_GROUP/>
                <CONTRACTOR>
                    <xsl:for-each select="data[(@nom='titulaire')]/item">
                        <xsl:if test="current()[@nom='idAttributaire']">
                            <xsl:variable name="attributaireId" select="current()[(@nom='idAttributaire')]/@valeur"/>
                            <xsl:for-each
                                    select="/dossier/dossier_datas/data[(@nom='attribution_titulaire')]/liste_objet/objet">
                                <xsl:if test="$attributaireId = data[(@nom='idAttributaire')]/valeur">
                                    <ADDRESS_CONTRACTOR>
                                        <xsl:call-template name="TYPE_ADDRESSE_CONTENEUR"/>
                                    </ADDRESS_CONTRACTOR>

                                    <xsl:choose>
                                        <xsl:when test="data[(@nom='PME')]/referentiel/code = '1'">
                                            <SME/>
                                        </xsl:when>
                                        <xsl:when test="data[(@nom='PME')]/referentiel/code = '2'">
                                            <NO_SME/>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:if>
                    </xsl:for-each>
                </CONTRACTOR>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
