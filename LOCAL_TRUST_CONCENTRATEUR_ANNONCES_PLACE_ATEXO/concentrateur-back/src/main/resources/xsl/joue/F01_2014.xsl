<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">

    <xsl:template name="F01_2014">
        <F01_2014 CATEGORY="ORIGINAL" FORM="F01">
            <xsl:attribute name="LG">
                <xsl:value-of select="dossier_datas/data[(@nom='languages')]/referentiel/code"/>
            </xsl:attribute>

            <LEGAL_BASIS>
                <xsl:call-template name="LEGAL_BASIS"/>
            </LEGAL_BASIS>

            <NOTICE>
                <xsl:attribute name="TYPE">
                    <xsl:choose>
                        <xsl:when
                                test="dossier_datas/data[(@nom='avisConcernePreInformation')]/referentiel/code =  '01_STANDARD'">
                            PRI_ONLY
                        </xsl:when>
                        <xsl:when
                                test="dossier_datas/data[(@nom='avisConcernePreInformation')]/referentiel/code = '02_REDUCTION_DELAI'">
                            PRI_REDUCING_TIME_LIMITS
                        </xsl:when>
                        <xsl:when
                                test="dossier_datas/data[(@nom='avisConcernePreInformation')]/referentiel/code = '03_MISE_EN_CONCURRENCE'">
                            PRI_CALL_COMPETITION
                        </xsl:when>
                    </xsl:choose>
                </xsl:attribute>
            </NOTICE>

            <CONTRACTING_BODY>
                <xsl:call-template name="CONTRACTING_BODY">
                    <xsl:with-param name="formatAdresse" select="'CONTRACTING_BODY'"/>
                </xsl:call-template>
            </CONTRACTING_BODY>

            <xsl:for-each select="dossier_datas/data[(@nom='detaildesobjetsmarche')]/liste_objet/objet">
                <OBJECT_CONTRACT ITEM="{position()}">
                    <xsl:call-template name="OBJECT_CONTRACT_CONTENEUR">
                        <xsl:with-param name="sigleForm" select="'JOUE1'"/>
                    </xsl:call-template>
                </OBJECT_CONTRACT>
            </xsl:for-each>

            <LEFTI>
                <xsl:call-template name="LEFTI">
                    <xsl:with-param name="sigleForm" select="'JOUE1'"/>
                </xsl:call-template>
            </LEFTI>

            <PROCEDURE>
                <xsl:call-template name="PROCEDURE">
                    <xsl:with-param name="sigleForm" select="'JOUE1'"/>
                </xsl:call-template>
            </PROCEDURE>

            <COMPLEMENTARY_INFO>
                <xsl:call-template name="COMPLEMENTARY_INFO"/>
            </COMPLEMENTARY_INFO>
        </F01_2014>
    </xsl:template>
</xsl:stylesheet>