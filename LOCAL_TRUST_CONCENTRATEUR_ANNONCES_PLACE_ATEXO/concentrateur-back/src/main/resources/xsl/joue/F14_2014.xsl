<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">

    <xsl:template name="F14_2014">

        <F14_2014 CATEGORY="ORIGINAL" FORM="F14">
            <xsl:attribute name="LG">
                <xsl:value-of select="dossier_datas/data[(@nom='languages')]/referentiel/code"/>
            </xsl:attribute>

            <LEGAL_BASIS>
                <xsl:call-template name="LEGAL_BASIS"/>
            </LEGAL_BASIS>

            <CONTRACTING_BODY>
                <xsl:call-template name="CONTRACTING_BODY"/>
            </CONTRACTING_BODY>

            <OBJECT_CONTRACT>
                <xsl:call-template name="OBJECT_CONTRACT">
                    <xsl:with-param name="sigleForm" select="'JOUE14'"/>
                </xsl:call-template>
            </OBJECT_CONTRACT>


            <COMPLEMENTARY_INFO>
                <xsl:call-template name="COMPLEMENTARY_INFO"/>
            </COMPLEMENTARY_INFO>


            <CHANGES>
                <xsl:call-template name="CHANGES"/>
            </CHANGES>
        </F14_2014>

    </xsl:template>
</xsl:stylesheet>