<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:fn="http://www.w3.org/2005/xpath-functions"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="AWARD_CONTRACT">
        <xsl:param name="sigleForm"/>
        <xsl:if test="data[(@nom='numMarche')]/valeur != ''">
            <CONTRACT_NO>
                <xsl:value-of select="data[(@nom='numMarche')]/valeur"/>
            </CONTRACT_NO>
        </xsl:if>
        <xsl:if test="data[(@nom='numLot')]/valeur != ''">
            <LOT_NO>
                <xsl:value-of select="data[(@nom='numLot')]/valeur"/>
            </LOT_NO>
        </xsl:if>
        <xsl:if test="data[(@nom='intitule')]/valeur != ''">
            <TITLE>
                <P>
                    <xsl:value-of select="normalize-space(data[(@nom='intitule')]/valeur)"/>
                </P>
            </TITLE>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="data[(@nom='attribution')]/referentiel/code = '2'">
                <NO_AWARDED_CONTRACT>
                    <xsl:choose>
                        <xsl:when test="data[(@nom='attributionNon')]/referentiel/code 	= '02_autreRaisons'">
                            <PROCUREMENT_DISCONTINUED>
                                <ORIGINAL_TED_ESENDER PUBLICATION="NO"></ORIGINAL_TED_ESENDER>
                                <ESENDER_LOGIN PUBLICATION="NO">TED72</ESENDER_LOGIN>
                                <NO_DOC_EXT PUBLICATION="NO">
                                    <xsl:value-of select="data[(@nom='numAnnonce')]/valeur"/>
                                </NO_DOC_EXT>
                                <xsl:if test="data[(@nom='dateEnvoiInitiale')]/valeur != ''">
                                    <DATE_DISPATCH_ORIGINAL PUBLICATION="NO">
                                        <xsl:call-template name="typeDate">
                                            <xsl:with-param name="date"
                                                            select="data[(@nom='dateEnvoiInitiale')]/valeur"/>
                                        </xsl:call-template>
                                    </DATE_DISPATCH_ORIGINAL>
                                </xsl:if>
                            </PROCUREMENT_DISCONTINUED>
                        </xsl:when>
                        <xsl:when test="data[(@nom='attributionNon')]/referentiel/code = '01_aucuneOffreRecue'">
                            <PROCUREMENT_UNSUCCESSFUL/>
                        </xsl:when>
                    </xsl:choose>
                </NO_AWARDED_CONTRACT>
            </xsl:when>
            <xsl:when test="data[(@nom='attribution')]/referentiel/code = '1'">
                <AWARDED_CONTRACT>
                    <DATE_CONCLUSION_CONTRACT>
                        <xsl:if test="data[(@nom='dateAttribution')]/valeur != ''">
                            <xsl:call-template name="typeDate">
                                <xsl:with-param name="date" select="data[(@nom='dateAttribution')]/valeur"/>
                            </xsl:call-template>
                        </xsl:if>
                    </DATE_CONCLUSION_CONTRACT>

                    <xsl:choose>
                        <xsl:when test="$sigleForm = 'JOUE6'">
                            <TENDERS>
                                <xsl:choose>
                                    <xsl:when test="data[(@nom='offresRecuesPublication')]/referentiel/code = '1'">
                                        <xsl:attribute name="PUBLICATION">YES</xsl:attribute>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:attribute name="PUBLICATION">NO</xsl:attribute>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <xsl:if test="data[(@nom='nbOffresRecues')]/valeur != ''">
                                    <NB_TENDERS_RECEIVED>
                                        <xsl:value-of select="data[(@nom='nbOffresRecues')]/valeur"/>
                                    </NB_TENDERS_RECEIVED>
                                </xsl:if>

                                <xsl:if test="data[(@nom='nbOffresPME')]/valeur != ''">
                                    <NB_TENDERS_RECEIVED_SME>
                                        <xsl:value-of select="data[(@nom='nbOffresPME')]/valeur"/>
                                    </NB_TENDERS_RECEIVED_SME>
                                </xsl:if>

                                <xsl:if test="data[(@nom='nbOffresEU')]/valeur != ''">
                                    <NB_TENDERS_RECEIVED_OTHER_EU>
                                        <xsl:value-of select="data[(@nom='nbOffresEU')]/valeur"/>
                                    </NB_TENDERS_RECEIVED_OTHER_EU>
                                </xsl:if>

                                <xsl:if test="data[(@nom='nbOffresNonEU')]/valeur != ''">
                                    <NB_TENDERS_RECEIVED_NON_EU>
                                        <xsl:value-of select="data[(@nom='nbOffresNonEU')]/valeur"/>
                                    </NB_TENDERS_RECEIVED_NON_EU>
                                </xsl:if>

                                <xsl:if test="data[(@nom='nbOffresElec')]/valeur != ''">
                                    <NB_TENDERS_RECEIVED_EMEANS>
                                        <xsl:value-of select="data[(@nom='nbOffresElec')]/valeur"/>
                                    </NB_TENDERS_RECEIVED_EMEANS>
                                </xsl:if>
                            </TENDERS>
                        </xsl:when>
                        <xsl:otherwise>
                            <TENDERS>
                                <xsl:if test="data[(@nom='nbOffresRecues')]/valeur != ''">
                                    <NB_TENDERS_RECEIVED>
                                        <xsl:value-of select="data[(@nom='nbOffresRecues')]/valeur"/>
                                    </NB_TENDERS_RECEIVED>
                                </xsl:if>

                                <xsl:if test="data[(@nom='nbOffresPME')]/valeur != ''">
                                    <NB_TENDERS_RECEIVED_SME>
                                        <xsl:value-of select="data[(@nom='nbOffresPME')]/valeur"/>
                                    </NB_TENDERS_RECEIVED_SME>
                                </xsl:if>

                                <xsl:if test="data[(@nom='nbOffresEU')]/valeur != ''">
                                    <NB_TENDERS_RECEIVED_OTHER_EU>
                                        <xsl:value-of select="data[(@nom='nbOffresEU')]/valeur"/>
                                    </NB_TENDERS_RECEIVED_OTHER_EU>
                                </xsl:if>

                                <xsl:if test="data[(@nom='nbOffresNonEU')]/valeur != ''">
                                    <NB_TENDERS_RECEIVED_NON_EU>
                                        <xsl:value-of select="data[(@nom='nbOffresNonEU')]/valeur"/>
                                    </NB_TENDERS_RECEIVED_NON_EU>
                                </xsl:if>

                                <xsl:if test="data[(@nom='nbOffresElec')]/valeur != ''">
                                    <NB_TENDERS_RECEIVED_EMEANS>
                                        <xsl:value-of select="data[(@nom='nbOffresElec')]/valeur"/>
                                    </NB_TENDERS_RECEIVED_EMEANS>
                                </xsl:if>
                            </TENDERS>
                        </xsl:otherwise>
                    </xsl:choose>

                    <xsl:choose>
                        <xsl:when test="$sigleForm = 'JOUE6'">
                            <CONTRACTORS>
                                <xsl:choose>
                                    <xsl:when test="data[(@nom='titulairePublication')]/referentiel/code = '1'">
                                        <xsl:attribute name="PUBLICATION">YES</xsl:attribute>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:attribute name="PUBLICATION">NO</xsl:attribute>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <xsl:call-template name="CONTRACTOR"/>
                            </CONTRACTORS>
                        </xsl:when>
                        <xsl:otherwise>
                            <CONTRACTORS>
                                <xsl:call-template name="CONTRACTOR"/>
                            </CONTRACTORS>
                        </xsl:otherwise>
                    </xsl:choose>

                    <xsl:choose>
                        <xsl:when test="$sigleForm = 'JOUE6'">
                            <VALUES>
                                <xsl:choose>
                                    <xsl:when test="data[(@nom='valeurPublication')]/referentiel/code = '1'">
                                        <xsl:attribute name="PUBLICATION">YES</xsl:attribute>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:attribute name="PUBLICATION">NO</xsl:attribute>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <xsl:call-template name="VALUE"/>
                            </VALUES>
                        </xsl:when>
                        <xsl:when test="$sigleForm = 'JOUE3'">
                            <VALUES>
                                <xsl:call-template name="VALUE"/>
                            </VALUES>
                        </xsl:when>
                    </xsl:choose>


                    <xsl:if test="data[(@nom='sousTraitance')]/referentiel/code = '1'">
                        <LIKELY_SUBCONTRACTED/>
                        <xsl:if test="data[(@nom='sousTraitancePartValeur')]/valeur != ''">
                            <VAL_SUBCONTRACTING>
                                <xsl:attribute name="CURRENCY">
                                    <xsl:value-of select="data[(@nom='sousTraitanceDevise')]/referentiel/code"/>
                                </xsl:attribute>
                                <xsl:value-of
                                        select="fn:replace(fn:replace(data[(@nom='sousTraitancePartValeur')]/valeur,',','.'), '&#160;', '')"/>
                            </VAL_SUBCONTRACTING>
                        </xsl:if>
                        <xsl:if test="data[(@nom='sousTraitancePartPct')]/valeur != ''">
                            <PCT_SUBCONTRACTING>
                                <xsl:value-of select="data[(@nom='sousTraitancePartPct')]/valeur"/>
                            </PCT_SUBCONTRACTING>
                        </xsl:if>
                        <xsl:if test="data[(@nom='sousTraitanceDesc')]/valeur != ''">
                            <INFO_ADD_SUBCONTRACTING>
                                <P>
                                    <xsl:value-of select="normalize-space(data[(@nom='sousTraitanceDesc')]/valeur)"/>
                                </P>
                            </INFO_ADD_SUBCONTRACTING>
                        </xsl:if>
                    </xsl:if>


                    <!-- Debut JOUE 6-->
                    <xsl:if test="data[(@nom='prixAchatOpportuniteValeur')]/valeur != ''">
                        <VAL_BARGAIN_PURCHASE>
                            <xsl:attribute name="CURRENCY">
                                <xsl:value-of select="data[(@nom='prixAchatOpportuniteDevise')]/referentiel/code"/>
                            </xsl:attribute>
                            <xsl:value-of
                                    select="fn:replace(fn:replace(data[(@nom='prixAchatOpportuniteValeur')]/valeur,',','.'), '&#160;', '')"/>
                        </VAL_BARGAIN_PURCHASE>
                    </xsl:if>
                    <xsl:if test="data[(@nom='nbMarchesAttribues')]/valeur != ''">
                        <NB_CONTRACT_AWARDED>
                            <xsl:attribute name="PUBLICATION">NO</xsl:attribute>
                            <xsl:value-of select="data[(@nom='nbMarchesAttribues')]/valeur"/>
                        </NB_CONTRACT_AWARDED>
                    </xsl:if>

                    <xsl:if test="data[(@nom='originePaysOrigineUE')]/referentiel/code != ''">
                        <COUNTRY_ORIGIN PUBLICATION="NO">
                            <xsl:choose>
                                <xsl:when test="data[(@nom='originePaysOrigineUE')]/referentiel/code = '1'">
                                    <COMMUNITY_ORIGIN/>
                                </xsl:when>
                                <xsl:when test="data[(@nom='originePaysOrigineNonUE')]/referentiel/code = '1'">
                                    <NON_COMMUNITY_ORIGIN>
                                        <xsl:for-each select="data[(@nom='originePaysOrigineNonUEPays')]/item">
                                            <xsl:attribute name="VALUE">
                                                <xsl:value-of select="substring-after(referentielValeur/code,'-')"/>
                                            </xsl:attribute>
                                        </xsl:for-each>
                                    </NON_COMMUNITY_ORIGIN>
                                </xsl:when>
                            </xsl:choose>
                        </COUNTRY_ORIGIN>
                    </xsl:if>

                    <xsl:choose>
                        <xsl:when test="data[(@nom='attribueVariante')]/referentiel/code = '1'">
                            <AWARDED_TENDERER_VARIANT>
                                <xsl:attribute name="PUBLICATION">NO</xsl:attribute>
                            </AWARDED_TENDERER_VARIANT>
                        </xsl:when>
                        <xsl:when test="data[(@nom='attribueVariante')]/referentiel/code = '2'">
                            <NO_AWARDED_TENDERER_VARIANT>
                                <xsl:attribute name="PUBLICATION">NO</xsl:attribute>
                            </NO_AWARDED_TENDERER_VARIANT>
                        </xsl:when>
                    </xsl:choose>

                    <xsl:choose>
                        <xsl:when test="data[(@nom='exclusionOffresBasses')]/referentiel/code = '1'">
                            <TENDERS_EXCLUDED>
                                <xsl:attribute name="PUBLICATION">NO</xsl:attribute>
                            </TENDERS_EXCLUDED>
                        </xsl:when>
                        <xsl:when test="data[(@nom='exclusionOffresBasses')]/referentiel/code = '2'">
                            <NO_TENDERS_EXCLUDED>
                                <xsl:attribute name="PUBLICATION">NO</xsl:attribute>
                            </NO_TENDERS_EXCLUDED>
                        </xsl:when>
                    </xsl:choose>
                    <!-- Fin JOUE 6-->

                </AWARDED_CONTRACT>
            </xsl:when>
        </xsl:choose>

        <!-- Debut JOUE-15 -->
        <xsl:if test="$sigleForm = 'JOUE15'">
            <AWARDED_CONTRACT>
                <DATE_CONCLUSION_CONTRACT>
                    <xsl:if test="data[(@nom='dateAttribution')]/valeur != ''">
                        <xsl:call-template name="typeDate">
                            <xsl:with-param name="date"
                                            select="data[(@nom='dateAttribution')]/valeur"/>
                        </xsl:call-template>
                    </xsl:if>
                </DATE_CONCLUSION_CONTRACT>
                <CONTRACTORS>
                    <xsl:if test="../../../data[(@nom='baseLegal')]/referentiel/code = '32014L0025'">
                        <xsl:choose>
                            <xsl:when test="data[(@nom='titulairePublication')]/referentiel/code = '1'">
                                <xsl:attribute name="PUBLICATION">YES</xsl:attribute>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:attribute name="PUBLICATION">NO</xsl:attribute>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:if>
                    <xsl:call-template name="CONTRACTOR"/>
                </CONTRACTORS>
                <VALUES>
                    <xsl:choose>
                        <xsl:when test="data[(@nom='valeurPublication')]/referentiel/code != ''">
                            <xsl:choose>
                                <xsl:when test="data[(@nom='valeurPublication')]/referentiel/code = '1'">
                                    <xsl:attribute name="PUBLICATION">YES</xsl:attribute>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:attribute name="PUBLICATION">NO</xsl:attribute>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:call-template name="VALUE"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="VALUE"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </VALUES>
                <xsl:if test="data[(@nom='sousTraitance')]/referentiel/code = '1'">
                    <LIKELY_SUBCONTRACTED/>

                    <xsl:if test="data[(@nom='sousTraitancePartValeur')]/valeur != ''">
                        <VAL_SUBCONTRACTING>
                            <xsl:attribute name="CURRENCY">
                                <xsl:value-of select="data[(@nom='sousTraitanceDevise')]/referentiel/code"/>
                            </xsl:attribute>
                            <xsl:value-of
                                    select="fn:replace(fn:replace(data[(@nom='sousTraitancePartValeur')]/valeur,',','.'), '&#160;', '')"/>
                        </VAL_SUBCONTRACTING>
                    </xsl:if>
                    <xsl:if test="data[(@nom='sousTraitancePartPct')]/valeur != ''">
                        <PCT_SUBCONTRACTING>
                            <xsl:value-of select="data[(@nom='sousTraitancePartPct')]/valeur"/>
                        </PCT_SUBCONTRACTING>
                    </xsl:if>
                    <xsl:if test="data[(@nom='sousTraitanceDesc')]/valeur != ''">
                        <INFO_ADD_SUBCONTRACTING>
                            <P>
                                <xsl:value-of select="normalize-space(data[(@nom='sousTraitanceDesc')]/valeur)"/>
                            </P>
                        </INFO_ADD_SUBCONTRACTING>
                    </xsl:if>
                </xsl:if>

                <xsl:if test="dossier/dossier_datas/data[(@nom='avisConcerneJoue15')]/referentiel/code='01_DIRECTIVE-81'">
                    <DIRECTIVE_2009_81_EC>
                        <xsl:if test="data[(@nom='contratsSousTraitanceMisEnConcurrence')]/referentiel/code = '1'">
                            <AWARDED_SUBCONTRACTING/>
                        </xsl:if>
                        <xsl:if test="data[(@nom='pourcentValeurMarcheMisEnConcurrence')]/referentiel/code = '1'">
                            <PCT_RANGE_SHARE_SUBCONTRACTING>
                                <xsl:if test="data[(@nom='pourcentMin')]/valeur != ''">
                                    <MIN>
                                        <xsl:value-of select="data[(@nom='pourcentMin')]/valeur"/>
                                    </MIN>
                                    <MAX>
                                        <xsl:value-of select="data[(@nom='pourcentMax')]/valeur"/>
                                    </MAX>
                                </xsl:if>
                            </PCT_RANGE_SHARE_SUBCONTRACTING>
                        </xsl:if>
                    </DIRECTIVE_2009_81_EC>
                </xsl:if>

            </AWARDED_CONTRACT>
        </xsl:if>
        <!-- Fin JOUE-15 -->
    </xsl:template>
</xsl:stylesheet>