<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="CHANGES">
        <xsl:choose>
            <xsl:when test="dossier_datas/data[(@nom='raisonRectif')]/referentiel/code = 'modifInfoOriginales'">
                <MODIFICATION_ORIGINAL PUBLICATION="NO"/>
            </xsl:when>
            <xsl:when test="dossier_datas/data[(@nom='raisonRectif')]/referentiel/code = 'pubTEDNonConforme'">
                <PUBLICATION_TED PUBLICATION="NO"/>
            </xsl:when>
        </xsl:choose>

        <xsl:for-each select="dossier_datas/data[(@nom='infosRectif2')]/liste_objet/objet">
            <CHANGE>
                <WHERE>
                    <SECTION>
                        <xsl:value-of select="data[(@nom='rubriqueSection')]/referentiel/libelle"/>
                    </SECTION>
                    <xsl:if test="data[(@nom='rubriqueLot')]/valeur != ''">
                        <LOT_NO>
                            <xsl:value-of select="data[(@nom='rubriqueLot')]/valeur"/>
                        </LOT_NO>
                    </xsl:if>
                    <xsl:if test="data[(@nom='rubriqueApresLaMention')]/valeur != ''">
                        <LABEL>
                            <xsl:value-of select="data[(@nom='rubriqueApresLaMention')]/valeur"/>
                        </LABEL>
                    </xsl:if>
                </WHERE>

                <OLD_VALUE>
                    <xsl:choose>
                        <xsl:when test="data[(@nom='typeRectification')]/referentiel/code ='vide'">
                            <NOTHING/>
                        </xsl:when>
                        <xsl:when test="data[(@nom='typeRectification')]/referentiel/code ='codeCPV'">
                            <CPV_MAIN>
                                <xsl:call-template name="CPV_CONTENEUR">
                                    <xsl:with-param name="principale"
                                                    select="'auLieuDeCPVObjetPrincipalClassPrincipale'"/>
                                    <xsl:with-param name="supplementaire"
                                                    select="'auLieuDeCPVObjetPrincipalClassSupplementaire'"/>
                                </xsl:call-template>
                            </CPV_MAIN>
                        </xsl:when>
                        <xsl:when test="data[(@nom='typeRectification')]/referentiel/code ='codeCPVAdditionnel'">
                            <CPV_ADDITIONAL>
                                <xsl:call-template name="CPV_CONTENEUR">
                                    <xsl:with-param name="principale"
                                                    select="'auLieuDeCPVObjetPrincipalClassPrincipale'"/>
                                    <xsl:with-param name="supplementaire"
                                                    select="'auLieuDeCPVObjetPrincipalClassSupplementaire'"/>
                                </xsl:call-template>
                            </CPV_ADDITIONAL>
                        </xsl:when>
                        <xsl:when test="data[(@nom='typeRectification')]/referentiel/code ='texte'">
                            <TEXT>
                                <P>
                                    <xsl:value-of select="normalize-space(data[(@nom='auLieuDeTexteArea')]/valeur)"/>
                                </P>
                            </TEXT>
                        </xsl:when>
                        <xsl:when test="data[(@nom='typeRectification')]/referentiel/code ='date'">
                            <DATE>
                                <xsl:call-template name="typeDate">
                                    <xsl:with-param name="date" select="data[(@nom='auLieuDeDate')]/valeur"/>
                                </xsl:call-template>
                            </DATE>
                        </xsl:when>
                        <xsl:when test="data[(@nom='typeRectification')]/referentiel/code ='dateHeure'">
                            <DATE>
                                <xsl:call-template name="typeDate">
                                    <xsl:with-param name="date" select="data[(@nom='auLieuDeDateHeure')]/valeur"/>
                                </xsl:call-template>
                            </DATE>
                            <TIME>
                                <xsl:value-of select="substring(data[(@nom='auLieuDeDateHeure')]/valeur,12,5)"/>
                            </TIME>
                        </xsl:when>
                    </xsl:choose>
                </OLD_VALUE>

                <NEW_VALUE>
                    <xsl:choose>
                        <xsl:when test="data[(@nom='typeRectification2')]/referentiel/code ='vide'">
                            <NOTHING/>
                        </xsl:when>
                        <xsl:when test="data[(@nom='typeRectification2')]/referentiel/code ='codeCPV'">
                            <CPV_MAIN>
                                <xsl:call-template name="CPV_CONTENEUR">
                                    <xsl:with-param name="principale" select="'lireCPVObjetPrincipalClassPrincipale'"/>
                                    <xsl:with-param name="supplementaire"
                                                    select="'lireCPVObjetPrincipalClassSupplementaire'"/>
                                </xsl:call-template>
                            </CPV_MAIN>
                        </xsl:when>
                        <xsl:when test="data[(@nom='typeRectification2')]/referentiel/code ='codeCPVAdditionnel'">
                            <CPV_ADDITIONAL>
                                <xsl:call-template name="CPV_CONTENEUR">
                                    <xsl:with-param name="principale" select="'lireCPVObjetPrincipalClassPrincipale'"/>
                                    <xsl:with-param name="supplementaire"
                                                    select="'lireCPVObjetPrincipalClassSupplementaire'"/>
                                </xsl:call-template>
                            </CPV_ADDITIONAL>
                        </xsl:when>
                        <xsl:when test="data[(@nom='typeRectification2')]/referentiel/code ='texte'">
                            <TEXT>
                                <P>
                                    <xsl:value-of select="normalize-space(data[(@nom='lireTexteArea')]/valeur)"/>
                                </P>
                            </TEXT>
                        </xsl:when>
                        <xsl:when test="data[(@nom='typeRectification2')]/referentiel/code ='date'">
                            <DATE>
                                <xsl:call-template name="typeDate">
                                    <xsl:with-param name="date" select="data[(@nom='lireDate')]/valeur"/>
                                </xsl:call-template>
                            </DATE>
                        </xsl:when>
                        <xsl:when test="data[(@nom='typeRectification2')]/referentiel/code ='dateHeure'">
                            <DATE>
                                <xsl:call-template name="typeDate">
                                    <xsl:with-param name="date" select="data[(@nom='lireDateHeure')]/valeur"/>
                                </xsl:call-template>
                            </DATE>
                            <TIME>
                                <xsl:value-of select="substring(data[(@nom='lireDateHeure')]/valeur,12,5)"/>
                            </TIME>
                        </xsl:when>
                    </xsl:choose>
                </NEW_VALUE>
            </CHANGE>
        </xsl:for-each>
        <xsl:if test="dossier_datas/data[(@nom='infoSup')]/valeur != ''">
            <INFO_ADD>
                <P>
                    <xsl:value-of select="normalize-space(dossier_datas/data[(@nom='infoSup')]/valeur)"/>
                </P>
            </INFO_ADD>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>