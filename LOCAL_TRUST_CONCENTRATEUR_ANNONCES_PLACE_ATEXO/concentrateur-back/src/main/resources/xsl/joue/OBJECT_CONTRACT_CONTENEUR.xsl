<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:fn="http://www.w3.org/2005/xpath-functions"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="OBJECT_CONTRACT_CONTENEUR">
        <xsl:param name="sigleForm"/>
        <TITLE>
            <P>

                <xsl:value-of select="normalize-space(data[(@nom='titreMarche')]/valeur)"/>

            </P>
        </TITLE>

        <xsl:if test="data[(@nom='numeroReference')]/valeur != ''">
            <REFERENCE_NUMBER>
                <xsl:value-of select="data[(@nom='numeroReference')]/valeur"/>
            </REFERENCE_NUMBER>
        </xsl:if>

        <CPV_MAIN>
            <xsl:call-template name="CPV_CONTENEUR">
                <xsl:with-param name="principale" select="'CPVObjetPrincipalClassPrincipale'"/>
                <xsl:with-param name="supplementaire" select="'CPVObjetPrincipalClassSupplementaire'"/>
            </xsl:call-template>
        </CPV_MAIN>

        <TYPE_CONTRACT>
            <xsl:attribute name="CTYPE">
                <xsl:choose>
                    <xsl:when
                            test="data[(@nom='natureMarche')]/referentiel/code = '01_travaux'">WORKS
                    </xsl:when>
                    <xsl:when
                            test="data[(@nom='natureMarche')]/referentiel/code = '02_fournitures'">SUPPLIES
                    </xsl:when>
                    <xsl:when
                            test="data[(@nom='natureMarche')]/referentiel/code = '03_services'">SERVICES
                    </xsl:when>
                </xsl:choose>
            </xsl:attribute>
        </TYPE_CONTRACT>

        <SHORT_DESCR>
            <P>

                <xsl:value-of select="normalize-space(data[(@nom='description')]/valeur)"/>

            </P>
        </SHORT_DESCR>
        <xsl:if test="data[(@nom='estimationValeurTotal')]/valeur != ''">
            <VAL_ESTIMATED_TOTAL>
                <xsl:attribute name="CURRENCY">
                    <xsl:value-of select="data[(@nom='estimationValeurTotalDevise')]/referentiel/code"/>
                </xsl:attribute>
                <xsl:value-of
                        select="fn:replace(fn:replace(data[(@nom='estimationValeurTotal')]/valeur,',','.'), '&#160;', '')"/>
            </VAL_ESTIMATED_TOTAL>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="data[(@nom='MarcheUniqueOUlots')]/referentiel/code = '02_lots'">
                <LOT_DIVISION>
                    <xsl:choose>
                        <xsl:when
                                test="data[(@nom='divisibleLotOuiLotsUnLotOUPlusieursLotOUTousLots')]/referentiel/code = '03_tousLots'">
                            <LOT_ALL/>
                        </xsl:when>
                        <xsl:when
                                test="data[(@nom='divisibleLotOuiLotsUnLotOUPlusieursLotOUTousLots')]/referentiel/code = '02_nbMaxLotOffre'">
                            <LOT_MAX_NUMBER>
                                <xsl:value-of
                                        select="data[(@nom='divisibleLotOuiNbMaxLotOffre')]/valeur"/>
                            </LOT_MAX_NUMBER>
                        </xsl:when>
                        <xsl:when
                                test="data[(@nom='divisibleLotOuiLotsUnLotOUPlusieursLotOUTousLots')]/referentiel/code = '01_unLot'">
                            <LOT_ONE_ONLY/>
                        </xsl:when>
                    </xsl:choose>

                    <xsl:if test="data[(@nom='divisibleLotOuiNbMaxLotAttrib')]/valeur != ''">
                        <LOT_MAX_ONE_TENDERER>
                            <xsl:value-of
                                    select="data[(@nom='divisibleLotOuiNbMaxLotAttrib')]/valeur"/>
                        </LOT_MAX_ONE_TENDERER>
                    </xsl:if>
                    <xsl:if test="data[(@nom='divisibleLotOuilotsReserves')]/valeur != ''">
                        <LOT_COMBINING_CONTRACT_RIGHT>
                            <P>

                                <xsl:value-of
                                        select="normalize-space(data[(@nom='divisibleLotOuilotsReserves')]/valeur)"/>

                            </P>
                        </LOT_COMBINING_CONTRACT_RIGHT>
                    </xsl:if>
                </LOT_DIVISION>


                <xsl:variable name="objet">
                    <xsl:value-of select="data[(@nom='titreMarche')]/valeur"/>
                </xsl:variable>

                <xsl:variable name="i" select="0"/>
                <!--Trouver les lots associs a cet objet-->
                <xsl:for-each
                        select="/dossier/dossier_datas/data[(@nom='situation_detailsLots')]/liste_objet/objet[data[(@nom='objet')]/item/@valeur=$objet]">
                    <OBJECT_DESCR>
                        <xsl:call-template name="OBJECT_DESCR">
                            <xsl:with-param name="position" select="position()"/>
                            <xsl:with-param name="sigleForm" select="$sigleForm"/>
                        </xsl:call-template>
                    </OBJECT_DESCR>
                </xsl:for-each>
            </xsl:when>

            <xsl:when test="data[(@nom='MarcheUniqueOUlots')]/referentiel/code = '01_marcheUnique'">
                <NO_LOT_DIVISION/>
                <xsl:variable name="i" select="0"/>
                <xsl:variable name="objet">
                    <xsl:value-of select="data[(@nom='titreMarche')]/valeur"/>
                </xsl:variable>
                <xsl:for-each select="/dossier/dossier_datas/data[(@nom='situation_detailsLots')]/liste_objet/objet">
                    <!--Trouver les lots associs a cet objet-->
                    <xsl:if test="fn:compare($objet, data[(@nom='objet')]/item/@valeur) = 0">
                        <xsl:variable name="i" select="$i+1"/>
                        <OBJECT_DESCR>
                            <xsl:call-template name="OBJECT_DESCR">
                                <xsl:with-param name="position" select="$i"/>
                                <xsl:with-param name="sigleForm" select="$sigleForm"/>
                            </xsl:call-template>
                        </OBJECT_DESCR>
                    </xsl:if>
                </xsl:for-each>
            </xsl:when>
        </xsl:choose>
        <xsl:if test="data[(@nom='descriptionMarcheDateLancementPassation')]/valeur != ''">
            <DATE_PUBLICATION_NOTICE>
                <xsl:variable name="dateLancementPassation">
                    <xsl:value-of select="data[(@nom='descriptionMarcheDateLancementPassation')]/valeur"/>
                </xsl:variable>
                <xsl:call-template name="typeDate">
                    <xsl:with-param name="date" select="$dateLancementPassation"/>
                </xsl:call-template>
            </DATE_PUBLICATION_NOTICE>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>