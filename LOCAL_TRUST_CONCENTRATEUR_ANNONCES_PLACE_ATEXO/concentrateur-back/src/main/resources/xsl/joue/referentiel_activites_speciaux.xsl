<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="activitesSpeciaux">
        <xsl:param name="value"/>
        <xsl:choose>
            <xsl:when test="$value = '01_production'">PRODUCTION_TRANSPORT_DISTRIBUTION_GAS_HEAT</xsl:when>
            <xsl:when test="$value = '02_prospection'">EXPLORATION_EXTRACTION_GAS_OIL</xsl:when>
            <xsl:when test="$value = '03_eau'">WATER</xsl:when>
            <xsl:when test="$value = '04_servicescheminfer'">RAILWAY_SERVICES</xsl:when>
            <xsl:when test="$value = '05_activitesportuaires'">PORT_RELATED_ACTIVITIES</xsl:when>
            <xsl:when test="$value = '06_electricite'">ELECTRICITY</xsl:when>
            <xsl:when test="$value = '07_prospection'">EXPLORATION_EXTRACTION_COAL_OTHER_SOLID_FUEL</xsl:when>
            <xsl:when test="$value = '08_servicespostaux'">POSTAL_SERVICES</xsl:when>
            <xsl:when test="$value = '09_servicescheminferurbains'">URBAN_RAILWAY_TRAMWAY_TROLLEYBUS_BUS_SERVICES
            </xsl:when>
            <xsl:when test="$value = '10_activitesaeroportuaires'">AIRPORT_RELATED_ACTIVITIES</xsl:when>
            <xsl:when test="$value = '20'">OTHER</xsl:when>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
