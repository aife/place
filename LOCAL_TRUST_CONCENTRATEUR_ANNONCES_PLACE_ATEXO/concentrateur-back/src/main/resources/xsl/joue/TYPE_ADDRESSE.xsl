<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:n2016="http://publications.europa.eu/resource/schema/ted/2016/nuts"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="TYPE_ADDRESSE">
        <xsl:param name="value"/>

        <OFFICIALNAME>
            <xsl:choose>
                <xsl:when test="dossier_datas/data[(@nom=concat($value, 'acheteurPublic'))]/valeur != ''">
                    <xsl:value-of select="dossier_datas/data[(@nom=concat($value, 'acheteurPublic'))]/valeur"/>
                </xsl:when>
                <xsl:when test="dossier_datas/data[(@nom=concat($value, 'PersonneMorale'))]/valeur != ''">
                    <xsl:value-of select="dossier_datas/data[(@nom=concat($value, 'PersonneMorale'))]/valeur"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="dossier_datas/data[(@nom=concat($value, 'nomOfficiel'))]/valeur"/>
                </xsl:otherwise>
            </xsl:choose>
        </OFFICIALNAME>

        <xsl:if test="dossier_datas/data[(@nom=concat($value, 'codeIdentificationNational'))]/valeur != ''">
            <NATIONALID>
                <xsl:value-of select="dossier_datas/data[(@nom=concat($value, 'codeIdentificationNational'))]/valeur"/>
            </NATIONALID>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom=concat($value, 'adr'))]/adresse/@nomVoie != ''">
            <xsl:variable name="nomVoie" select="dossier_datas/data[(@nom=concat($value, 'adr'))]/adresse/@numeroVoie"/>
            <xsl:variable name="numeroVoie" select="dossier_datas/data[(@nom=concat($value, 'adr'))]/adresse/@nomVoie"/>
            <ADDRESS>
                <xsl:value-of select="concat($nomVoie,' ',$numeroVoie)"/>
            </ADDRESS>
        </xsl:if>

        <TOWN>
            <xsl:value-of select="dossier_datas/data[(@nom=concat($value, 'adr'))]/adresse/@nomCommune"/>
        </TOWN>

        <xsl:if test="dossier_datas/data[(@nom=concat($value, 'adr'))]/adresse/@codePostal != ''">
            <POSTAL_CODE>
                <xsl:value-of select="dossier_datas/data[(@nom=concat($value, 'adr'))]/adresse/@codePostal"/>
            </POSTAL_CODE>
        </xsl:if>

        <COUNTRY>
            <xsl:attribute name="VALUE">
                <xsl:call-template name="codePays">
                    <xsl:with-param name="canal" select="'LESECHOS'"/>
                    <xsl:with-param name="emplacement"
                                    select="dossier_datas/data[(@nom=concat($value,'adr'))]/adresse/pays/code"/>
                </xsl:call-template>
            </xsl:attribute>
        </COUNTRY>

        <xsl:if test="dossier_datas/data[(@nom=concat($value, 'pointDeContact'))]/valeur != ''">
            <CONTACT_POINT>
                <xsl:value-of select="dossier_datas/data[(@nom=concat($value, 'pointDeContact'))]/valeur"/>
            </CONTACT_POINT>
        </xsl:if>

        <xsl:choose>
            <xsl:when test="dossier_datas/data[(@nom=concat($value, 'tel'))]/valeur != ''">
                <PHONE>
                    <xsl:value-of select="dossier_datas/data[(@nom=concat($value, 'tel'))]/valeur"/>
                </PHONE>
            </xsl:when>
            <xsl:when test="dossier_datas/data[(@nom=concat($value, 'coord_tel'))]/valeur != ''">
                <PHONE>
                    <xsl:value-of select="dossier_datas/data[(@nom=concat($value, 'coord_tel'))]/valeur"/>
                </PHONE>
            </xsl:when>
        </xsl:choose>


        <xsl:choose>
            <xsl:when test="dossier_datas/data[(@nom=concat($value, 'mel'))]/valeur != ''">
                <E_MAIL>
                    <xsl:value-of select="dossier_datas/data[(@nom=concat($value, 'mel'))]/valeur"/>
                </E_MAIL>
            </xsl:when>
            <xsl:when test="dossier_datas/data[(@nom=concat($value, 'coord_mel'))]/valeur != ''">
                <E_MAIL>
                    <xsl:value-of select="dossier_datas/data[(@nom=concat($value, 'coord_mel'))]/valeur"/>
                </E_MAIL>
            </xsl:when>
        </xsl:choose>

        <xsl:choose>
            <xsl:when test="dossier_datas/data[(@nom=concat($value, 'fax'))]/valeur != ''">
                <FAX>
                    <xsl:value-of select="dossier_datas/data[(@nom=concat($value, 'fax'))]/valeur"/>
                </FAX>
            </xsl:when>
            <xsl:when test="dossier_datas/data[(@nom=concat($value, 'coord_fax'))]/valeur != ''">
                <FAX>
                    <xsl:value-of select="dossier_datas/data[(@nom=concat($value, 'coord_fax'))]/valeur"/>
                </FAX>
            </xsl:when>
        </xsl:choose>

        <xsl:if test="dossier_datas/data[(@nom=concat($value, 'codeNUTS'))]/referentiel/code != ''">
            <n2016:NUTS>
                <xsl:attribute name="CODE">
                    <xsl:value-of select="dossier_datas/data[(@nom=concat($value, 'codeNUTS'))]/referentiel/code"/>
                </xsl:attribute>
            </n2016:NUTS>
        </xsl:if>

        <xsl:choose>
            <xsl:when test="dossier_datas/data[(@nom=concat($value, 'url'))]/valeur != ''">
                <URL_GENERAL>
                    <xsl:value-of select="dossier_datas/data[(@nom=concat($value, 'url'))]/valeur"/>
                </URL_GENERAL>
            </xsl:when>
            <xsl:when test="dossier_datas/data[(@nom=concat($value, 'urlPouvoirAdjudicateur'))]/valeur != ''">
                <URL_GENERAL>
                    <xsl:value-of select="dossier_datas/data[(@nom=concat($value, 'urlPouvoirAdjudicateur'))]/valeur"/>
                </URL_GENERAL>
            </xsl:when>
        </xsl:choose>

        <xsl:if test="dossier_datas/data[(@nom=concat($value, 'urlProfilAcheteur'))]/valeur != ''">
            <URL_BUYER>
                <xsl:value-of select="dossier_datas/data[(@nom=concat($value, 'urlProfilAcheteur'))]/valeur"/>
            </URL_BUYER>
        </xsl:if>

    </xsl:template>
</xsl:stylesheet>