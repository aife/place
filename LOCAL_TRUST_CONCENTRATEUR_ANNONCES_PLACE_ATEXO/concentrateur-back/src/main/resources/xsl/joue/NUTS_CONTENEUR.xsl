<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:n2016="http://publications.europa.eu/resource/schema/ted/2016/nuts"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="NUTS_CONTENEUR">
        <xsl:param name="value"/>
        <xsl:if test="data[(@nom=concat($value,'1'))]/referentiel/code != ''">
            <n2016:NUTS>
                <xsl:attribute name="CODE">
                    <xsl:value-of select="data[(@nom=concat($value,'1'))]/referentiel/code"/>
                </xsl:attribute>
            </n2016:NUTS>
        </xsl:if>
        <xsl:if test="data[(@nom=concat($value,'2'))]/referentiel/code != ''">
            <n2016:NUTS>
                <xsl:attribute name="CODE">
                    <xsl:value-of select="data[(@nom=concat($value,'2'))]/referentiel/code"/>
                </xsl:attribute>
            </n2016:NUTS>
        </xsl:if>
        <xsl:if test="data[(@nom=concat($value,'3'))]/referentiel/code != ''">
            <n2016:NUTS>
                <xsl:attribute name="CODE">
                    <xsl:value-of select="data[(@nom=concat($value,'3'))]/referentiel/code"/>
                </xsl:attribute>
            </n2016:NUTS>
        </xsl:if>
        <xsl:if test="data[(@nom=concat($value,'4'))]/referentiel/code != ''">
            <n2016:NUTS>
                <xsl:attribute name="CODE">
                    <xsl:value-of select="data[(@nom=concat($value,'4'))]/referentiel/code"/>
                </xsl:attribute>
            </n2016:NUTS>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>
