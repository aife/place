<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:fn="http://www.w3.org/2005/xpath-functions"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">

    <xsl:template name="COMPLEMENTARY_INFO">
        <xsl:choose>
            <xsl:when
                    test="dossier_datas/data[(@nom='renseignementsCompl_recurrentOui_OU_recurrentNon')]/referentiel/code = '1'">
                <RECURRENT_PROCUREMENT/>
                <xsl:if test="dossier_datas/data[(@nom='renseignementsCompl_calendrierPublication')]/valeur != ''">
                    <ESTIMATED_TIMING>
                        <P>
                            <xsl:value-of
                                    select="normalize-space(dossier_datas/data[(@nom='renseignementsCompl_calendrierPublication')]/valeur)"/>
                        </P>
                    </ESTIMATED_TIMING>
                </xsl:if>
            </xsl:when>
            <xsl:when
                    test="dossier_datas/data[(@nom='renseignementsCompl_recurrentOui_OU_recurrentNon')]/referentiel/code = '2'">
                <NO_RECURRENT_PROCUREMENT/>
            </xsl:when>
        </xsl:choose>

        <xsl:if test="dossier_datas/data[(@nom='renseignementsCompl_webCommande')]/referentiel/code = '1'">
            <EORDERING/>
        </xsl:if>
        <xsl:if test="dossier_datas/data[(@nom='renseignementsCompl_webFacturation')]/referentiel/code = '1'">
            <EINVOICING/>
        </xsl:if>
        <xsl:if test="dossier_datas/data[(@nom='renseignementsCompl_webPaiement')]/referentiel/code = '1'">
            <EPAYMENT/>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='infoCompl')]/valeur != ''">
            <INFO_ADD>
                <P>
                    <xsl:value-of select="normalize-space(dossier_datas/data[(@nom='infoCompl')]/valeur)"/>
                </P>
            </INFO_ADD>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='procedureRecours_instanceChargeeRecours_PersonneMorale')]/valeur != ''">
            <ADDRESS_REVIEW_BODY>
                <xsl:call-template name="TYPE_ADDRESSE">
                    <xsl:with-param name="value" select="'procedureRecours_instanceChargeeRecours_'"/>
                </xsl:call-template>
            </ADDRESS_REVIEW_BODY>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='procedureRecours_instanceChargeemediation_PersonneMorale')]/valeur != ''">
            <ADDRESS_MEDIATION_BODY>
                <xsl:call-template name="TYPE_ADDRESSE">
                    <xsl:with-param name="value" select="'procedureRecours_instanceChargeemediation_'"/>
                </xsl:call-template>
            </ADDRESS_MEDIATION_BODY>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='procedureRecours_introductionRecoursPrecisions')]/valeur != ''">
            <REVIEW_PROCEDURE>
                <P>
                    <xsl:value-of
                            select="normalize-space(dossier_datas/data[(@nom='procedureRecours_introductionRecoursPrecisions')]/valeur)"/>
                </P>
            </REVIEW_PROCEDURE>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='procedureRecours_introductionRecoursRenseignements_PersonneMorale')]/valeur != ''">
            <ADDRESS_REVIEW_INFO>
                <xsl:call-template name="TYPE_ADDRESSE">
                    <xsl:with-param name="value" select="'procedureRecours_introductionRecoursRenseignements_'"/>
                </xsl:call-template>
            </ADDRESS_REVIEW_INFO>
        </xsl:if>

        <DATE_DISPATCH_NOTICE>
            <xsl:variable name="dateEnvoi">
                <xsl:value-of select="current-date()"/>
            </xsl:variable>
            <xsl:value-of select="fn:substring($dateEnvoi,1,10)"/>
        </DATE_DISPATCH_NOTICE>

        <xsl:if test="fn:contains(@numeroDossier,'JOUE-14')">
            <xsl:choose>
                <xsl:when test="dossier_datas/data[(@nom='TED_avisInitial_numero_NO_DOC_EXT')]/valeur !=''">
                    <ORIGINAL_TED_ESENDER PUBLICATION="NO"/>
                    <ESENDER_LOGIN PUBLICATION="NO">TED72</ESENDER_LOGIN>
                    <NO_DOC_EXT PUBLICATION="NO">
                        <xsl:value-of select="dossier_datas/data[(@nom='TED_avisInitial_numero_NO_DOC_EXT')]/valeur"/>
                    </NO_DOC_EXT>
                </xsl:when>
                <xsl:otherwise>
                    <ORIGINAL_OTHER_MEANS PUBLICATION="NO">OTHER_MEANS</ORIGINAL_OTHER_MEANS>
                </xsl:otherwise>
            </xsl:choose>


            <xsl:if test="dossier_datas/data[(@nom='publicationAnterieure_numAnnonce1')]/valeur != ''">
                <NOTICE_NUMBER_OJ>
                    <xsl:value-of
                            select="concat(dossier_datas/data[(@nom='publicationAnterieure_numAnnonce1')]/valeur,'/S ',dossier_datas/data[(@nom='publicationAnterieure_numAnnonce2')]/valeur,'-',dossier_datas/data[(@nom='publicationAnterieure_numAnnonce3')]/valeur)"/>
                </NOTICE_NUMBER_OJ>
            </xsl:if>

            <xsl:if test="dossier_datas/data[(@nom='publicationAnterieure_datePublication')]/valeur != ''">
                <DATE_DISPATCH_ORIGINAL PUBLICATION="NO">
                    <xsl:call-template name="typeDate">
                        <xsl:with-param name="date"
                                        select="dossier_datas/data[(@nom='publicationAnterieure_datePublication')]/valeur"/>
                    </xsl:call-template>
                </DATE_DISPATCH_ORIGINAL>
            </xsl:if>

        </xsl:if>

    </xsl:template>
</xsl:stylesheet>