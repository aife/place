<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="activites">
        <xsl:param name="value"/>
        <xsl:choose>
            <xsl:when test="$value = '01_servicesGenerauxAdmPub'">GENERAL_PUBLIC_SERVICES</xsl:when>
            <xsl:when test="$value = '02_ordreSecuritePub'">PUBLIC_ORDER_AND_SAFETY</xsl:when>
            <xsl:when test="$value = '03_affairesEcoFi'">ECONOMIC_AND_FINANCIAL_AFFAIRS</xsl:when>
            <xsl:when test="$value = '04_logementDevCol'">HOUSING_AND_COMMUNITY_AMENITIES</xsl:when>
            <xsl:when test="$value = '05_loisirsCultureReligion'">RECREATION_CULTURE_AND_RELIGION</xsl:when>
            <xsl:when test="$value = '06_defense'">DEFENCE</xsl:when>
            <xsl:when test="$value = '07_environnement'">ENVIRONMENT</xsl:when>
            <xsl:when test="$value = '08_sante'">HEALTH</xsl:when>
            <xsl:when test="$value = '09_protectionSociale'">SOCIAL_PROTECTION</xsl:when>
            <xsl:when test="$value = '10_education'">EDUCATION</xsl:when>
            <xsl:when test="$value = '20'">OTHER</xsl:when>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
			