<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="CONTRACTING_BODY">
        <xsl:param name="formatAdresse"/>

        <ADDRESS_CONTRACTING_BODY>
            <xsl:call-template name="TYPE_ADDRESSE">
                <xsl:with-param name="value" select="'organisme_'"/>
            </xsl:call-template>
        </ADDRESS_CONTRACTING_BODY>

        <xsl:if test="dossier_datas/data[(@nom='procedureConjointe_appelOffreConjoint')]/referentiel/code = '1'">

            <xsl:for-each
                    select="dossier_datas/data[(@nom='procedureConjointe_adressePouvoirAdjudicateur')]/liste_objet/objet">
                <ADDRESS_CONTRACTING_BODY_ADDITIONAL>
                    <xsl:call-template name="TYPE_ADDRESSE_CONTENEUR">
                        <xsl:with-param name="formatAdresse" select="$formatAdresse"/>
                    </xsl:call-template>
                </ADDRESS_CONTRACTING_BODY_ADDITIONAL>
            </xsl:for-each>

            <JOINT_PROCUREMENT_INVOLVED/>

            <xsl:if test="dossier_datas/data[(@nom='procedureConjointe_appelOffreConjointLoi')]/valeur != ''">
                <PROCUREMENT_LAW>
                    <P>
                        <xsl:value-of
                                select="normalize-space(dossier_datas/data[(@nom='procedureConjointe_appelOffreConjointLoi')]/valeur)"/>
                    </P>
                </PROCUREMENT_LAW>
            </xsl:if>

        </xsl:if>
        <xsl:if test="dossier_datas/data[(@nom='procedureConjointe_acheteurCentral')]/referentiel/code = '1'">
            <CENTRAL_PURCHASING/>
        </xsl:if>

        <xsl:choose>
            <xsl:when
                    test="dossier_datas/data[(@nom='communication_documentMarche_accesComplet_OU_restreint')]/referentiel/code = '01_acces_complet'">
                <DOCUMENT_FULL/>
            </xsl:when>
            <xsl:when
                    test="dossier_datas/data[(@nom='communication_documentMarche_accesComplet_OU_restreint')]/referentiel/code = '02_acces_restreint'">
                <DOCUMENT_RESTRICTED/>
            </xsl:when>
        </xsl:choose>

        <xsl:if test="dossier_datas/data[(@nom='communication_documentMarche_urlDocument')]/valeur != ''">
            <URL_DOCUMENT>
                <xsl:value-of select="dossier_datas/data[(@nom='communication_documentMarche_urlDocument')]/valeur"/>
            </URL_DOCUMENT>
        </xsl:if>

        <xsl:choose>
            <xsl:when
                    test="dossier_datas/data[(@nom='communication_infosCompl')]/referentiel/code = '01_points_contact_susmentionnes'">
                <ADDRESS_FURTHER_INFO_IDEM/>
            </xsl:when>
            <xsl:when
                    test="dossier_datas/data[(@nom='communication_infosCompl')]/referentiel/code = '02_autre_adresse'">
                <ADDRESS_FURTHER_INFO>
                    <xsl:call-template name="TYPE_ADDRESSE">
                        <xsl:with-param name="value" select="'communication_infosCompl_'"/>
                    </xsl:call-template>
                </ADDRESS_FURTHER_INFO>
            </xsl:when>
        </xsl:choose>

        <xsl:choose>
            <xsl:when
                    test="dossier_datas/data[(@nom='communication_participation_urlParticipation')]/referentiel/code = '1'">

                <URL_PARTICIPATION>
                    <xsl:value-of
                            select="dossier_datas/data[(@nom='communication_participation_urlParticipationAdresse')]/valeur"/>
                </URL_PARTICIPATION>

                <xsl:choose>
                    <xsl:when
                            test="dossier_datas/data[(@nom='communication_participation')]/referentiel/code = '01_points_contact_susmentionnes'">
                        <ADDRESS_PARTICIPATION_IDEM/>
                    </xsl:when>
                    <xsl:when
                            test="dossier_datas/data[(@nom='communication_participation')]/referentiel/code = '02_adresse_suivante'">
                        <ADDRESS_PARTICIPATION>
                            <xsl:call-template name="TYPE_ADDRESSE">
                                <xsl:with-param name="value" select="'participation_adresseParticipation_'"/>
                            </xsl:call-template>
                        </ADDRESS_PARTICIPATION>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>

            <xsl:when
                    test="dossier_datas/data[(@nom='communication_participation_urlParticipation')]/referentiel/code = '2'">
                <xsl:choose>
                    <xsl:when
                            test="dossier_datas/data[(@nom='communication_participation')]/referentiel/code = '01_points_contact_susmentionnes'">
                        <ADDRESS_PARTICIPATION_IDEM/>
                    </xsl:when>

                    <xsl:when
                            test="dossier_datas/data[(@nom='communication_participation')]/referentiel/code = '02_adresse_suivante'">
                        <ADDRESS_PARTICIPATION>
                            <xsl:call-template name="TYPE_ADDRESSE">
                                <xsl:with-param name="value" select="'participation_adresseParticipation_'"/>
                            </xsl:call-template>
                        </ADDRESS_PARTICIPATION>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>

        </xsl:choose>

        <xsl:if test="dossier_datas/data[(@nom='communication_urlOutilLogiciel')]/valeur != ''">
            <URL_TOOL>
                <xsl:value-of select="dossier_datas/data[(@nom='communication_urlOutilLogiciel')]/valeur"/>
            </URL_TOOL>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='organisme_classificationOrganismeJOUE')]/referentiel/code != ''">
            <xsl:choose>
                <xsl:when
                        test="dossier_datas/data[(@nom='organisme_classificationOrganismeJOUE')]/referentiel/code = '020_autre'">
                    <CA_TYPE_OTHER>
                        <xsl:value-of
                                select="dossier_datas/data[(@nom='organisme_classificationOrganismeJOUE_autre')]/valeur"/>
                    </CA_TYPE_OTHER>
                </xsl:when>
                <xsl:otherwise>
                    <CA_TYPE>
                        <xsl:attribute name="VALUE">
                            <xsl:call-template name="classificationOrganisme">
                                <xsl:with-param name="value"
                                                select="dossier_datas/data[(@nom='organisme_classificationOrganismeJOUE')]/referentiel/code"/>
                            </xsl:call-template>
                        </xsl:attribute>
                    </CA_TYPE>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='organisme_activitesOrganisme_activites')]/referentiel/code != ''">
            <xsl:choose>
                <xsl:when
                        test="dossier_datas/data[(@nom='organisme_activitesOrganisme_activites')]/referentiel/code = '20'">
                    <CA_ACTIVITY_OTHER>
                        <xsl:value-of
                                select="dossier_datas/data[(@nom='organisme_activitesOrganisme_activites_autre')]/valeur"/>
                    </CA_ACTIVITY_OTHER>
                </xsl:when>
                <xsl:otherwise>
                    <CA_ACTIVITY>
                        <xsl:attribute name="VALUE">
                            <xsl:call-template name="activites">
                                <xsl:with-param name="value"
                                                select="dossier_datas/data[(@nom='organisme_activitesOrganisme_activites')]/referentiel/code"/>
                            </xsl:call-template>
                        </xsl:attribute>
                    </CA_ACTIVITY>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>

        <xsl:if test="dossier_datas/data[(@nom='organisme_activitesOrganisme_activitesSecteursSpeciaux')]/referentiel/code != ''">
            <xsl:choose>
                <xsl:when
                        test="dossier_datas/data[(@nom='organisme_activitesOrganisme_activitesSecteursSpeciaux')]/referentiel/code = '20'">
                    <CE_ACTIVITY_OTHER>
                        <xsl:value-of
                                select="dossier_datas/data[(@nom='organisme_activitesOrganisme_activitesSecteursSpeciaux_autre')]/valeur"/>
                    </CE_ACTIVITY_OTHER>
                </xsl:when>
                <xsl:otherwise>
                    <CE_ACTIVITY>
                        <xsl:attribute name="VALUE">
                            <xsl:call-template name="activitesSpeciaux">
                                <xsl:with-param name="value"
                                                select="dossier_datas/data[(@nom='organisme_activitesOrganisme_activitesSecteursSpeciaux')]/referentiel/code"/>
                            </xsl:call-template>
                        </xsl:attribute>
                    </CE_ACTIVITY>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>