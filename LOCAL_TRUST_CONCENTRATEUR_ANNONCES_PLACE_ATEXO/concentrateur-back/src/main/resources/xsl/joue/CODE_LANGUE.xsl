<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="2.0">

    <xsl:template name="CODE_LANGUE">
        <xsl:param name="emplacement"/>
        <xsl:choose>
            <xsl:when test="$emplacement = 'espagnol'">ES</xsl:when>
            <xsl:when test="$emplacement = 'fran�ais'">FR</xsl:when>
            <xsl:when test="$emplacement = 'danois'">DA</xsl:when>
            <xsl:when test="$emplacement = 'allemand'">DE</xsl:when>
            <xsl:when test="$emplacement = 'grec'">EL</xsl:when>
            <xsl:when test="$emplacement = 'anglais'">EN</xsl:when>
            <xsl:when test="$emplacement = 'italien'">IT</xsl:when>
            <xsl:when test="$emplacement = 'n�erlandais'">NL</xsl:when>
            <xsl:when test="$emplacement = 'portugais'">PT</xsl:when>
            <xsl:when test="$emplacement = 'finnois'">FI</xsl:when>
            <xsl:when test="$emplacement = 'su�dois'">SV</xsl:when>
            <xsl:when test="$emplacement = 'tch�que'">CS</xsl:when>
            <xsl:when test="$emplacement = 'estonien'">ET</xsl:when>
            <xsl:when test="$emplacement = 'hongrois'">HU</xsl:when>
            <xsl:when test="$emplacement = 'letton'">LV</xsl:when>
            <xsl:when test="$emplacement = 'lituanien'">LT</xsl:when>
            <xsl:when test="$emplacement = 'maltais'">MT</xsl:when>
            <xsl:when test="$emplacement = 'polonais'">PL</xsl:when>
            <xsl:when test="$emplacement = 'slovaque'">SK</xsl:when>
            <xsl:when test="$emplacement = 'slov�ne'">SL</xsl:when>
            <xsl:when test="$emplacement = 'autre'">OTHER
            </xsl:when> <!-- This is my code, can't find the "other" equivalent in the XSD -->
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
			