<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="LEGAL_BASIS">
        <xsl:if test="dossier_datas/data[(@nom='baseLegal')]/referentiel/code != ''">
            <xsl:attribute name="VALUE">
                <xsl:value-of select="dossier_datas/data[(@nom='baseLegal')]/referentiel/code"/>
            </xsl:attribute>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>