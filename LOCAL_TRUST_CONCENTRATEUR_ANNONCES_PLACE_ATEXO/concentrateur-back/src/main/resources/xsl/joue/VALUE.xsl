<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:fn="http://www.w3.org/2005/xpath-functions"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="VALUE">
        <xsl:if test="data[(@nom='estimationInitialeValeur')] != ''">
            <VAL_ESTIMATED_TOTAL>
                <xsl:attribute name="CURRENCY">
                    <xsl:value-of select="data[(@nom='estimationInitialeDevise')]/referentiel/code"/>
                </xsl:attribute>
                <xsl:value-of
                        select="fn:replace(fn:replace(data[(@nom='estimationInitialeValeur')]/valeur,',','.'), '&#160;', '')"/>
            </VAL_ESTIMATED_TOTAL>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="data[(@nom='montantOUOffres')]/referentiel/code = '01_valeur'">
                <VAL_TOTAL>
                    <xsl:attribute name="CURRENCY">
                        <xsl:value-of select="data[(@nom='montantOffresDevise')]/referentiel/code"/>
                    </xsl:attribute>
                    <xsl:value-of select="fn:replace(fn:replace(data[(@nom='montant')]/valeur,',','.'), '&#160;', '')"/>
                </VAL_TOTAL>
            </xsl:when>
            <xsl:when test="data[(@nom='montantOUOffres')]/referentiel/code = '02_fourchette'">
                <VAL_RANGE_TOTAL>
                    <xsl:attribute name="CURRENCY">
                        <xsl:value-of select="data[(@nom='montantOffresDevise')]/referentiel/code"/>
                    </xsl:attribute>
                    <LOW>
                        <xsl:value-of
                                select="fn:replace(fn:replace(data[(@nom='offreBasse')]/valeur,',','.'), '&#160;', '')"/>
                    </LOW>
                    <HIGH>
                        <xsl:value-of
                                select="fn:replace(fn:replace(data[(@nom='offreElevee')]/valeur,',','.'), '&#160;', '')"/>
                    </HIGH>
                </VAL_RANGE_TOTAL>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>