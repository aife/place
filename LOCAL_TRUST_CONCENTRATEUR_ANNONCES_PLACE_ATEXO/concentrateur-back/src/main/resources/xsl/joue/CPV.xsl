<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="CPV">
        <xsl:param name="principale"/>
        <xsl:param name="supplementaire"/>
        <CPV_CODE>
            <xsl:attribute name="CODE">
                <xsl:value-of select="substring(dossier_datas/data[(@nom=$principale)]/referentiel/code,1,8)"/>
            </xsl:attribute>
        </CPV_CODE>
        <xsl:if test="dossier_datas/data[(@nom=concat($supplementaire,'1'))]/referentiel/code != ''">
            <CPV_SUPPLEMENTARY_CODE>
                <xsl:attribute name="CODE">
                    <xsl:value-of
                            select="substring(dossier_datas/data[(@nom=concat($supplementaire,'1'))]/referentiel/code,1,4)"/>
                </xsl:attribute>
            </CPV_SUPPLEMENTARY_CODE>
        </xsl:if>
        <xsl:if test="dossier_datas/data[(@nom=concat($supplementaire,'2'))]/referentiel/code != ''">
            <CPV_SUPPLEMENTARY_CODE>
                <xsl:attribute name="CODE">
                    <xsl:value-of
                            select="substring(dossier_datas/data[(@nom=concat($supplementaire,'2'))]/referentiel/code,1,4)"/>
                </xsl:attribute>
            </CPV_SUPPLEMENTARY_CODE>
        </xsl:if>
        <xsl:if test="dossier_datas/data[(@nom=concat($supplementaire,'3'))]/referentiel/code != ''">
            <CPV_SUPPLEMENTARY_CODE>
                <xsl:attribute name="CODE">
                    <xsl:value-of
                            select="substring(dossier_datas/data[(@nom=concat($supplementaire,'3'))]/referentiel/code,1,4)"/>
                </xsl:attribute>
            </CPV_SUPPLEMENTARY_CODE>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>
