<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                version="2.0"
                xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception"
                xsi:schemaLocation="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception TED_ESENDERS.xsd">
    <xsl:template name="OBJECT_DESCR_12_13">
        <xsl:if test="/dossier/dossier_datas/data[(@nom='description_CPV_objetComplementaire1_classPrincipale')]/referentiel/code != ''">
            <CPV_ADDITIONAL>
                <CPV_CODE>
                    <xsl:attribute name="CODE">
                        <xsl:value-of
                                select="substring(/dossier/dossier_datas/data[(@nom='description_CPV_objetComplementaire1_classPrincipale')]/referentiel/code,1,8)"/>
                    </xsl:attribute>
                </CPV_CODE>

                <xsl:if test="/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire1_classSupplementaire','1'))]/referentiel/code != ''">
                    <CPV_SUPPLEMENTARY_CODE>
                        <xsl:attribute name="CODE">
                            <xsl:value-of
                                    select="substring(/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire1_classSupplementaire','1'))]/referentiel/code,1,4)"/>
                        </xsl:attribute>
                    </CPV_SUPPLEMENTARY_CODE>
                </xsl:if>
                <xsl:if test="/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire1_classSupplementaire','2'))]/referentiel/code != ''">
                    <CPV_SUPPLEMENTARY_CODE>
                        <xsl:attribute name="CODE">
                            <xsl:value-of
                                    select="substring(/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire1_classSupplementaire','2'))]/referentiel/code,1,4)"/>
                        </xsl:attribute>
                    </CPV_SUPPLEMENTARY_CODE>
                </xsl:if>

                <xsl:if test="/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire1_classSupplementaire','3'))]/referentiel/code != ''">
                    <CPV_SUPPLEMENTARY_CODE>
                        <xsl:attribute name="CODE">
                            <xsl:value-of
                                    select="substring(/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire1_classSupplementaire','3'))]/referentiel/code,1,4)"/>
                        </xsl:attribute>
                    </CPV_SUPPLEMENTARY_CODE>
                </xsl:if>
            </CPV_ADDITIONAL>
        </xsl:if>
        <xsl:if test="/dossier/dossier_datas/data[(@nom='description_CPV_objetComplementaire2_classPrincipale')]/referentiel/code != ''">
            <CPV_ADDITIONAL>
                <CPV_CODE>
                    <xsl:attribute name="CODE">
                        <xsl:value-of
                                select="substring(/dossier/dossier_datas/data[(@nom='description_CPV_objetComplementaire2_classPrincipale')]/referentiel/code,1,8)"/>
                    </xsl:attribute>
                </CPV_CODE>

                <xsl:if test="/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire2_classSupplementaire','1'))]/referentiel/code != ''">
                    <CPV_SUPPLEMENTARY_CODE>
                        <xsl:attribute name="CODE">
                            <xsl:value-of
                                    select="substring(/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire2_classSupplementaire','1'))]/referentiel/code,1,4)"/>
                        </xsl:attribute>
                    </CPV_SUPPLEMENTARY_CODE>
                </xsl:if>
                <xsl:if test="/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire2_classSupplementaire','2'))]/referentiel/code != ''">
                    <CPV_SUPPLEMENTARY_CODE>
                        <xsl:attribute name="CODE">
                            <xsl:value-of
                                    select="substring(/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire2_classSupplementaire','2'))]/referentiel/code,1,4)"/>
                        </xsl:attribute>
                    </CPV_SUPPLEMENTARY_CODE>
                </xsl:if>

                <xsl:if test="/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire2_classSupplementaire','3'))]/referentiel/code != ''">
                    <CPV_SUPPLEMENTARY_CODE>
                        <xsl:attribute name="CODE">
                            <xsl:value-of
                                    select="substring(/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire2_classSupplementaire','3'))]/referentiel/code,1,4)"/>
                        </xsl:attribute>
                    </CPV_SUPPLEMENTARY_CODE>
                </xsl:if>
            </CPV_ADDITIONAL>
        </xsl:if>
        <xsl:if test="/dossier/dossier_datas/data[(@nom='description_CPV_objetComplementaire3_classPrincipale')]/referentiel/code != ''">
            <CPV_ADDITIONAL>
                <CPV_CODE>
                    <xsl:attribute name="CODE">
                        <xsl:value-of
                                select="substring(/dossier/dossier_datas/data[(@nom='description_CPV_objetComplementaire3_classPrincipale')]/referentiel/code,1,8)"/>
                    </xsl:attribute>
                </CPV_CODE>

                <xsl:if test="/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire3_classSupplementaire','1'))]/referentiel/code != ''">
                    <CPV_SUPPLEMENTARY_CODE>
                        <xsl:attribute name="CODE">
                            <xsl:value-of
                                    select="substring(/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire3_classSupplementaire','1'))]/referentiel/code,1,4)"/>
                        </xsl:attribute>
                    </CPV_SUPPLEMENTARY_CODE>
                </xsl:if>
                <xsl:if test="/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire3_classSupplementaire','2'))]/referentiel/code != ''">
                    <CPV_SUPPLEMENTARY_CODE>
                        <xsl:attribute name="CODE">
                            <xsl:value-of
                                    select="substring(/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire3_classSupplementaire','2'))]/referentiel/code,1,4)"/>
                        </xsl:attribute>
                    </CPV_SUPPLEMENTARY_CODE>
                </xsl:if>

                <xsl:if test="/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire3_classSupplementaire','3'))]/referentiel/code != ''">
                    <CPV_SUPPLEMENTARY_CODE>
                        <xsl:attribute name="CODE">
                            <xsl:value-of
                                    select="substring(/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire3_classSupplementaire','3'))]/referentiel/code,1,4)"/>
                        </xsl:attribute>
                    </CPV_SUPPLEMENTARY_CODE>
                </xsl:if>
            </CPV_ADDITIONAL>
        </xsl:if>
        <xsl:if test="/dossier/dossier_datas/data[(@nom='description_CPV_objetComplementaire4_classPrincipale')]/referentiel/code != ''">
            <CPV_ADDITIONAL>
                <CPV_CODE>
                    <xsl:attribute name="CODE">
                        <xsl:value-of
                                select="substring(/dossier/dossier_datas/data[(@nom='description_CPV_objetComplementaire4_classPrincipale')]/referentiel/code,1,8)"/>
                    </xsl:attribute>
                </CPV_CODE>

                <xsl:if test="/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire4_classSupplementaire','1'))]/referentiel/code != ''">
                    <CPV_SUPPLEMENTARY_CODE>
                        <xsl:attribute name="CODE">
                            <xsl:value-of
                                    select="substring(/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire4_classSupplementaire','1'))]/referentiel/code,1,4)"/>
                        </xsl:attribute>
                    </CPV_SUPPLEMENTARY_CODE>
                </xsl:if>
                <xsl:if test="/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire4_classSupplementaire','2'))]/referentiel/code != ''">
                    <CPV_SUPPLEMENTARY_CODE>
                        <xsl:attribute name="CODE">
                            <xsl:value-of
                                    select="substring(/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire4_classSupplementaire','2'))]/referentiel/code,1,4)"/>
                        </xsl:attribute>
                    </CPV_SUPPLEMENTARY_CODE>
                </xsl:if>

                <xsl:if test="/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire4_classSupplementaire','3'))]/referentiel/code != ''">
                    <CPV_SUPPLEMENTARY_CODE>
                        <xsl:attribute name="CODE">
                            <xsl:value-of
                                    select="substring(/dossier/dossier_datas/data[(@nom=concat('description_CPV_objetComplementaire4_classSupplementaire','3'))]/referentiel/code,1,4)"/>
                        </xsl:attribute>
                    </CPV_SUPPLEMENTARY_CODE>
                </xsl:if>
            </CPV_ADDITIONAL>
        </xsl:if>

        <SHORT_DESCR>
            <P>

                <xsl:value-of
                        select="normalize-space(/dossier/dossier_datas/data[(@nom='descriptionMarche_description')]/valeur)"/>

            </P>
        </SHORT_DESCR>

        <xsl:choose>
            <xsl:when
                    test="/dossier/dossier_datas/data[(@nom='infosSup_fondsCommunautairesNoN')]/referentiel/code = '1'">
                <EU_PROGR_RELATED>
                    <P>
                        <xsl:value-of
                                select="normalize-space(/dossier/dossier_datas/data[(@nom='infosSup_fondsCommunautairesRef')]/valeur)"/>
                    </P>
                </EU_PROGR_RELATED>
            </xsl:when>
            <xsl:when
                    test="/dossier/dossier_datas/data[(@nom='infosSup_fondsCommunautairesNoN')]/referentiel/code = '2'">
                <NO_EU_PROGR_RELATED/>
            </xsl:when>
        </xsl:choose>

    </xsl:template>

</xsl:stylesheet>