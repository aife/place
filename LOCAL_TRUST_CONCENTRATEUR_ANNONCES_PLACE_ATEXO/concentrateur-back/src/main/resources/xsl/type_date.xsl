<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="2.0">

    <xsl:template name="typeDate">
        <xsl:param name="date"/>
        <xsl:value-of
                select="concat(substring($date, 7, 4), '-', substring($date, 4, 2), '-', substring($date, 1, 2))"/>
    </xsl:template>
</xsl:stylesheet>
		