<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
                xpath-default-namespace="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception">
    <xsl:param name="libelleSupport"/>
    <xsl:param name="libelleOffre"/>
    <xsl:output omit-xml-declaration="yes"/>

    <xsl:template match="/TED_ESENDERS">
        <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
        <html>
            <head>
                <meta charset="utf-8"/>
                <title>Annonce</title>
            </head>
            <body>
                <p>
                    <strong>Plateforme :</strong>
                    <xsl:value-of select="FORM_SECTION/F05_2014/CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/URL_BUYER"/>
                </p>
                <br/>
                <p>
                    <strong>Acheteur :</strong>
                </p>
                <p>Organisme :
                    <xsl:value-of
                            select="FORM_SECTION/F05_2014/CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/OFFICIALNAME"/>
                </p>
                <p>Mail :
                    <xsl:value-of select="SENDER/CONTACT/E_MAIL"/>
                </p>
                <p>Tel :
                    <xsl:value-of select="SENDER/CONTACT/PHONE"/>
                </p>
                <br/>
                <p>
                    <strong>Offre :</strong>
                </p>
                <p>Journal :
                    <xsl:value-of select="$libelleSupport"/>
                </p>
                <p>Offre sélectionnée :
                    <xsl:value-of select="$libelleOffre"/>
                </p>
                <br/>
                <p>
                    <strong>Coordonnée Acheteur :</strong>
                </p>
                <p>Nom Officiel :
                    <xsl:value-of
                            select="FORM_SECTION/F05_2014/CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/OFFICIALNAME"/>
                </p>
                <p>Adresse :
                    <xsl:value-of
                            select="FORM_SECTION/F05_2014/CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/ADDRESS"/>
                </p>
                <p>Code Postal :
                    <xsl:value-of
                            select="FORM_SECTION/F05_2014/CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/POSTAL_CODE"/>
                </p>
                <p>Pays :
                    <xsl:value-of
                            select="FORM_SECTION/F05_2014/CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/COUNTRY/@VALUE"/>
                </p>

                <br/>
                <p>
                    <strong>Consultation :</strong>
                </p>
                <p>Objet :
                    <xsl:value-of select="FORM_SECTION/F05_2014/OBJECT_CONTRACT/SHORT_DESCR"/>
                </p>
                <p>Reférence Consultation Utilisateur :
                    <xsl:value-of select="FORM_SECTION/F05_2014/OBJECT_CONTRACT/REFERENCE_NUMBER"/>
                </p>
                <p>Date de remise plis :
                    <xsl:value-of select="FORM_SECTION/F05_2014/PROCEDURE/DATE_RECEIPT_TENDERS"/><xsl:text> </xsl:text><xsl:value-of
                            select="FORM_SECTION/F05_2014/PROCEDURE/TIME_RECEIPT_TENDERS"/>
                </p>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="text()|@*"/>
</xsl:stylesheet>