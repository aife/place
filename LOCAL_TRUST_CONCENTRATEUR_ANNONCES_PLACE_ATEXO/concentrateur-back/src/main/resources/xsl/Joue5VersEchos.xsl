<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:nuts="http://publications.europa.eu/resource/schema/ted/2016/nuts"
                version="2.0"
                xpath-default-namespace="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception">
    <xsl:param name="codeOffre"/>
    <xsl:include href="codeNuts2016VersDepartement.xsl"/>

    <xsl:template match="/TED_ESENDERS/FORM_SECTION/F05_2014">
        <depot version="1.1">
            <annonce>
                <marchesPublics>
                    <typeAnnonce>
                        <initial>
                            <type>Avis d'appel public à concurrence</type>
                            <dates>
                                <dateLimiteCandidatureOuOffre>
                                    <dateLimiteOffre>
                                        <xsl:value-of
                                                select="concat(PROCEDURE/DATE_RECEIPT_TENDERS, 'T', PROCEDURE/TIME_RECEIPT_TENDERS, ':00')"/>
                                    </dateLimiteOffre>
                                </dateLimiteCandidatureOuOffre>
                            </dates>
                        </initial>
                    </typeAnnonce>
                    <typeProcedure>
                        <xsl:choose>
                            <xsl:when test="PROCEDURE/PT_OPEN">
                                <xsl:text>Appel d'offre ouvert</xsl:text>
                            </xsl:when>
                            <xsl:when test="PROCEDURE/PT_RESTRICTED">
                                <xsl:text>Appel d'offre restreint</xsl:text>
                            </xsl:when>
                            <xsl:when test="PROCEDURE/PT_COMPETITIVE_NEGOTIATION">
                                <xsl:text>Procédure négociée</xsl:text>
                            </xsl:when>
                            <xsl:when test="PROCEDURE/PT_COMPETITIVE_DIALOGUE">
                                <xsl:text>Dialogue compétitif</xsl:text>
                            </xsl:when>
                            <xsl:when test="PROCEDURE/PT_INNOVATION_PARTNERSHIP">
                                <xsl:text>Autre</xsl:text>
                            </xsl:when>
                            <xsl:when test="PROCEDURE/PT_01_AOO">
                                <xsl:text>Appel d'offre ouvert</xsl:text>
                            </xsl:when>
                            <xsl:when test="PROCEDURE/PT_03_MAPA">
                                <xsl:text>Procédure adaptée</xsl:text>
                            </xsl:when>
                            <xsl:when test="PROCEDURE/PT_04_Concours">
                                <xsl:text>Concours ouvert</xsl:text>
                            </xsl:when>
                            <xsl:when test="PROCEDURE/PT_12_Autre">
                                <xsl:text>Autre</xsl:text>
                            </xsl:when>
                            <xsl:when test="PROCEDURE/PT_13_AccordCadreSelection">
                                <xsl:text>Autre</xsl:text>
                            </xsl:when>
                            <xsl:when test="PROCEDURE/PT_15_SAD">
                                <xsl:text>Autre</xsl:text>
                            </xsl:when>
                            <xsl:when test="PROCEDURE/PT_08_MarcheNegocie">
                                <xsl:text>Procédure négociée</xsl:text>
                            </xsl:when>
                            <xsl:when test="PROCEDURE/PT_18_PCN">
                                <xsl:text>Procédure négociée</xsl:text>
                            </xsl:when>
                            <xsl:when test="PROCEDURE/PT_06_DialogueCompetitif">
                                <xsl:text>Dialogue compétitif</xsl:text>
                            </xsl:when>
                            <xsl:when test="PROCEDURE/PT_02_AOR">
                                <xsl:text>Appel d'offre restreint</xsl:text>
                            </xsl:when>
                            <xsl:when test="PROCEDURE/PT_05_ConcoursRestreint">
                                <xsl:text>Concours restreint</xsl:text>
                            </xsl:when>
                            <xsl:when test="PROCEDURE/PT_14_MarcheSubsequent">
                                <xsl:text>Autre</xsl:text>
                            </xsl:when>
                            <xsl:when test="PROCEDURE/PT_16_MarcheSpecifique">
                                <xsl:text>Autre</xsl:text>
                            </xsl:when>
                        </xsl:choose>
                    </typeProcedure>
                    <objet>
                        <xsl:value-of select="OBJECT_CONTRACT/SHORT_DESCR/P" separator="&#x0A;"/>
                    </objet>
                    <categorie>
                        <xsl:choose>
                            <xsl:when test="OBJECT_CONTRACT/TYPE_CONTRACT/@CTYPE = 'SERVICES'">
                                <xsl:text>Service</xsl:text>
                            </xsl:when>
                            <xsl:when test="OBJECT_CONTRACT/TYPE_CONTRACT/@CTYPE = 'SUPPLIES'">
                                <xsl:text>Fourniture</xsl:text>
                            </xsl:when>
                            <xsl:when test="OBJECT_CONTRACT/TYPE_CONTRACT/@CTYPE = 'WORKS'">
                                <xsl:text>Travaux</xsl:text>
                            </xsl:when>
                        </xsl:choose>
                    </categorie>

                    <departement>
                        <xsl:call-template name="nutsToDepNumber">
                            <!-- On prend le premier département -->
                            <xsl:with-param name="code" select="OBJECT_CONTRACT/OBJECT_DESCR/nuts:NUTS[1]/@CODE"/>
                        </xsl:call-template>
                    </departement>

                    <dateLigne>
                        <xsl:value-of
                                select="format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')"/>
                    </dateLigne>
                </marchesPublics>
            </annonce>
            <!-- Donnée non présente dans le formulaire -->
            <supportsPublication>
                <supportPublication>
                    <xsl:attribute name="id">
                        <!-- sélectionne les deux premiers caractères du code offre, par exemple '17_LESECHOS' > '17' -->
                        <xsl:value-of select="substring($codeOffre,1,2)"/>
                    </xsl:attribute>
                </supportPublication>
            </supportsPublication>
            <organisme>
                <nomOrganisme>
                    <xsl:value-of select="CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/OFFICIALNAME"/>
                </nomOrganisme>
                <adresse>
                    <xsl:value-of select="CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/ADDRESS"/>
                </adresse>
                <xsl:if test="CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/POSTAL_CODE">
                    <codePostal>
                        <xsl:value-of select="CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/POSTAL_CODE"/>
                    </codePostal>
                </xsl:if>
                <ville>
                    <xsl:value-of select="CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/TOWN"/>
                </ville>
                <pays>
                    <xsl:value-of select="CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/COUNTRY/@VALUE"/>
                </pays>
            </organisme>
            <facturation>
                <nomOrganisme>
                    <xsl:value-of select="CONTRACTING_BODY/ADDRESS_FURTHER_INFO/OFFICIALNAME"/>
                </nomOrganisme>
                <adresse>
                    <xsl:value-of select="CONTRACTING_BODY/ADDRESS_FURTHER_INFO/ADDRESS"/>
                </adresse>
                <xsl:if test="CONTRACTING_BODY/ADDRESS_FURTHER_INFO/POSTAL_CODE">
                    <codePostal>
                        <xsl:value-of select="CONTRACTING_BODY/ADDRESS_FURTHER_INFO/POSTAL_CODE"/>
                    </codePostal>
                </xsl:if>
                <ville>
                    <xsl:value-of select="CONTRACTING_BODY/ADDRESS_FURTHER_INFO/TOWN"/>
                </ville>
                <pays>
                    <xsl:value-of select="CONTRACTING_BODY/ADDRESS_FURTHER_INFO/COUNTRY/@VALUE"/>
                </pays>
            </facturation>
            <expedition>
                <idemOrganisme/>
            </expedition>
        </depot>
    </xsl:template>

    <xsl:template match="text()|@*"/>
</xsl:stylesheet>