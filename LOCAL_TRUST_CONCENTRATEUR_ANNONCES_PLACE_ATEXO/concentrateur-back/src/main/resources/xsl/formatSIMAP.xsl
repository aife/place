<?xml version="1.0" encoding="ISO-8859-1"?>
<?altova_samplexml file:///C:/Users/AUB/Desktop/JOUE13.xml?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions"
                version="2.0">
    <!-- Types -->
    <xsl:include href="joue/NUTS_CONTENEUR.xsl"/>
    <xsl:include href="joue/NUTS_CONTENEUR_IN_CONTENEUR.xsl"/>
    <xsl:include href="joue/CPV.xsl"/>
    <xsl:include href="joue/CPV_CONTENEUR.xsl"/>
    <xsl:include href="joue/CPV_CONTENEUR_IN_CONTENEUR.xsl"/>
    <xsl:include href="joue/OBJECT_DESCR_IN_CONTENEUR.xsl"/>
    <xsl:include href="joue/TYPE_ADDRESSE.xsl"/>
    <xsl:include href="joue/TYPE_ADDRESSE_CONTENEUR.xsl"/>
    <xsl:include href="joue/TYPE_PROCEDURE.xsl"/>
    <xsl:include href="code_pays.xsl"/>
    <xsl:include href="joue/CODE_LANGUE.xsl"/>
    <xsl:include href="type_date.xsl"/>
    <!-- Referentiel -->
    <xsl:include href="joue/referentiel_activites.xsl"/>
    <xsl:include href="joue/referentiel_activites_speciaux.xsl"/>
    <xsl:include href="joue/referentiel_classificationOrganisme.xsl"/>
    <!-- Module -->
    <xsl:include href="joue/LEGAL_BASIS.xsl"/>
    <xsl:include href="joue/COMPLEMENTARY_INFO.xsl"/>
    <xsl:include href="joue/CONTRACTING_BODY.xsl"/>
    <xsl:include href="joue/OBJECT_CONTRACT.xsl"/>
    <xsl:include href="joue/OBJECT_CONTRACT_CONTENEUR.xsl"/>
    <xsl:include href="joue/OBJECT_DESCR.xsl"/>
    <xsl:include href="joue/OBJECT_DESCR_12_13.xsl"/>
    <xsl:include href="joue/PROCEDURE.xsl"/>
    <xsl:include href="joue/LEFTI.xsl"/>
    <xsl:include href="joue/CHANGES.xsl"/>
    <xsl:include href="joue/AWARD_CONTRACT.xsl"/>
    <xsl:include href="joue/CRITERE.xsl"/>
    <xsl:include href="joue/CONTRACTOR.xsl"/>
    <xsl:include href="joue/VALUE.xsl"/>
    <xsl:include href="joue/RESULTS.xsl"/>
    <xsl:include href="joue/DIRECTIVE.xsl"/>
    <xsl:include href="joue/ACCORDANCE_ARTICLE.xsl"/>
    <!-- Formulaire -->
    <xsl:include href="joue/F01_2014.xsl"/>
    <xsl:include href="joue/F02_2014.xsl"/>
    <xsl:include href="joue/F14_2014.xsl"/>
    <xsl:include href="joue/F13_2014.xsl"/>
    <xsl:include href="joue/F12_2014.xsl"/>
    <xsl:include href="joue/F03_2014.xsl"/>
    <xsl:include href="joue/F04_2014.xsl"/>
    <xsl:include href="joue/F05_2014.xsl"/>
    <xsl:include href="joue/F06_2014.xsl"/>
    <xsl:include href="joue/F07_2014.xsl"/>
    <xsl:include href="joue/F15_2014.xsl"/>
    <xsl:template match="/dossier">
        <TED_ESENDERS xmlns="http://publications.europa.eu/resource/schema/ted/R2.0.9/reception" VERSION="R2.0.9.S04">
            <SENDER>
                <IDENTIFICATION>
                    <xsl:variable name="date" select="current-date()"/>
                    <ESENDER_LOGIN>TED72</ESENDER_LOGIN>
                    <NO_DOC_EXT>
                        <xsl:value-of
                                select="concat(format-date($date,'[Y0001]'),'-',format-number(@idDossier, '000000'))"/>
                    </NO_DOC_EXT>
                </IDENTIFICATION>
                <CONTACT>
                    <ORGANISATION>Portail des march�s publics du Grand-Duch� de Luxembourg</ORGANISATION>
                    <COUNTRY VALUE="LU"/>
                    <PHONE>+352 24783355</PHONE>
                    <E_MAIL>TED72@marches.public.lu</E_MAIL>
                </CONTACT>
            </SENDER>
            <FORM_SECTION>
                <xsl:choose>
                    <xsl:when test="fn:contains(@numeroDossier,'JOUE-2')">
                        <xsl:call-template name="F02_2014"/>
                    </xsl:when>
                    <xsl:when test="fn:contains(@numeroDossier,'JOUE-12')">
                        <xsl:call-template name="F12_2014"/>
                    </xsl:when>
                    <xsl:when test="fn:contains(@numeroDossier,'JOUE-13')">
                        <xsl:call-template name="F13_2014"/>
                    </xsl:when>
                    <xsl:when test="fn:contains(@numeroDossier,'JOUE-14')">
                        <xsl:call-template name="F14_2014"/>
                    </xsl:when>
                    <xsl:when test="fn:contains(@numeroDossier,'JOUE-15')">
                        <xsl:call-template name="F15_2014"/>
                    </xsl:when>
                    <xsl:when test="fn:contains(@numeroDossier,'JOUE-3')">
                        <xsl:call-template name="F03_2014"/>
                    </xsl:when>
                    <xsl:when test="fn:contains(@numeroDossier,'JOUE-4')">
                        <xsl:call-template name="F04_2014"/>
                    </xsl:when>
                    <xsl:when test="fn:contains(@numeroDossier,'JOUE-5')">
                        <xsl:call-template name="F05_2014"/>
                    </xsl:when>
                    <xsl:when test="fn:contains(@numeroDossier,'JOUE-6')">
                        <xsl:call-template name="F06_2014"/>
                    </xsl:when>
                    <xsl:when test="fn:contains(@numeroDossier,'JOUE-7')">
                        <xsl:call-template name="F07_2014"/>
                    </xsl:when>
                    <xsl:when test="fn:contains(@numeroDossier,'JOUE-1')">
                        <xsl:call-template name="F01_2014"/>
                    </xsl:when>
                </xsl:choose>
            </FORM_SECTION>
        </TED_ESENDERS>
    </xsl:template>
</xsl:stylesheet>
