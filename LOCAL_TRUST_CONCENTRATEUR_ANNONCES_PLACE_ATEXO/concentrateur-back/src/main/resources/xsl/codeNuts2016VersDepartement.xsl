<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

    <xsl:template name="nutsToDepNumber">
        <xsl:param name="code"/>
        <xsl:choose>
            <xsl:when test="$code = 'FR101'">
                <xsl:text>75</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FR102'">
                <xsl:text>77</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FR103'">
                <xsl:text>78</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FR104'">
                <xsl:text>91</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FR105'">
                <xsl:text>92</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FR106'">
                <xsl:text>93</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FR107'">
                <xsl:text>94</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FR108'">
                <xsl:text>95</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRB01'">
                <xsl:text>18</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRB02'">
                <xsl:text>28</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRB03'">
                <xsl:text>36</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRB04'">
                <xsl:text>37</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRB05'">
                <xsl:text>41</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRB06'">
                <xsl:text>45</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRC11'">
                <xsl:text>21</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRC12'">
                <xsl:text>58</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRC13'">
                <xsl:text>71</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRC14'">
                <xsl:text>89</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRC21'">
                <xsl:text>25</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRC22'">
                <xsl:text>39</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRC23'">
                <xsl:text>70</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRC24'">
                <xsl:text>90</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRD11'">
                <xsl:text>14</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRD12'">
                <xsl:text>50</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRD13'">
                <xsl:text>61</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRD21'">
                <xsl:text>27</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRD22'">
                <xsl:text>76</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRE11'">
                <xsl:text>59</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRE12'">
                <xsl:text>62</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRE21'">
                <xsl:text>02</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRE22'">
                <xsl:text>60</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRE23'">
                <xsl:text>80</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRF11'">
                <xsl:text>67</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRF12'">
                <xsl:text>68</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRF21'">
                <xsl:text>08</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRF22'">
                <xsl:text>10</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRF23'">
                <xsl:text>51</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRF24'">
                <xsl:text>52</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRF31'">
                <xsl:text>54</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRF32'">
                <xsl:text>55</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRF33'">
                <xsl:text>57</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRF34'">
                <xsl:text>88</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRG01'">
                <xsl:text>44</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRG02'">
                <xsl:text>49</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRG03'">
                <xsl:text>53</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRG04'">
                <xsl:text>72</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRG05'">
                <xsl:text>85</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRH01'">
                <xsl:text>22</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRH02'">
                <xsl:text>29</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRH03'">
                <xsl:text>35</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRH04'">
                <xsl:text>56</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI11'">
                <xsl:text>24</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI12'">
                <xsl:text>33</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI13'">
                <xsl:text>40</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI14'">
                <xsl:text>47</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI15'">
                <xsl:text>64</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI21'">
                <xsl:text>19</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI22'">
                <xsl:text>23</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI23'">
                <xsl:text>87</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI31'">
                <xsl:text>16</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI32'">
                <xsl:text>17</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI33'">
                <xsl:text>79</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI34'">
                <xsl:text>86</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ11'">
                <xsl:text>11</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ12'">
                <xsl:text>30</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ13'">
                <xsl:text>34</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ14'">
                <xsl:text>48</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ15'">
                <xsl:text>66</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ21'">
                <xsl:text>09</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ22'">
                <xsl:text>12</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ23'">
                <xsl:text>31</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ24'">
                <xsl:text>32</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ25'">
                <xsl:text>46</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ26'">
                <xsl:text>65</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ27'">
                <xsl:text>81</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ28'">
                <xsl:text>82</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK11'">
                <xsl:text>03</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK12'">
                <xsl:text>15</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK13'">
                <xsl:text>43</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK14'">
                <xsl:text>63</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK21'">
                <xsl:text>01</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK22'">
                <xsl:text>07</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK23'">
                <xsl:text>26</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK24'">
                <xsl:text>38</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK25'">
                <xsl:text>42</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK26'">
                <xsl:text>69</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK27'">
                <xsl:text>73</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK28'">
                <xsl:text>74</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRL01'">
                <xsl:text>04</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRL02'">
                <xsl:text>05</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRL03'">
                <xsl:text>06</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRL04'">
                <xsl:text>13</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRL05'">
                <xsl:text>83</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRL06'">
                <xsl:text>84</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRM01'">
                <xsl:text>2A</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRM02'">
                <xsl:text>2B</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRY10'">
                <xsl:text>971</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRY20'">
                <xsl:text>972</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRY30'">
                <xsl:text>973</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRY40'">
                <xsl:text>974</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRY50'">
                <xsl:text>976</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="nutsToDepName">
        <xsl:param name="code"/>
        <xsl:choose>
            <xsl:when test="$code = 'FR101'">
                <xsl:text>Paris</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FR102'">
                <xsl:text>Seine-et-Marne</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FR103'">
                <xsl:text>Yvelines</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FR104'">
                <xsl:text>Essonne</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FR105'">
                <xsl:text>Hauts-de-Seine</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FR106'">
                <xsl:text>Seine-Saint-Denis</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FR107'">
                <xsl:text>Val-de-Marne</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FR108'">
                <xsl:text>Val-d’Oise</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRB01'">
                <xsl:text>Cher</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRB02'">
                <xsl:text>Eure-et-Loir</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRB03'">
                <xsl:text>Indre</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRB04'">
                <xsl:text>Indre-et-Loire</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRB05'">
                <xsl:text>Loir-et-Cher</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRB06'">
                <xsl:text>Loiret</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRC11'">
                <xsl:text>Côte-d’Or</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRC12'">
                <xsl:text>Nièvre</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRC13'">
                <xsl:text>Saône-et-Loire</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRC14'">
                <xsl:text>Yonne</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRC21'">
                <xsl:text>Doubs</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRC22'">
                <xsl:text>Jura</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRC23'">
                <xsl:text>Haute-Saône</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRC24'">
                <xsl:text>Territoire de Belfort</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRD11'">
                <xsl:text>Calvados</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRD12'">
                <xsl:text>Manche</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRD13'">
                <xsl:text>Orne</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRD21'">
                <xsl:text>Eure</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRD22'">
                <xsl:text>Seine-Maritime</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRE11'">
                <xsl:text>Nord</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRE12'">
                <xsl:text>Pas-de-Calais</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRE21'">
                <xsl:text>Aisne</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRE22'">
                <xsl:text>Oise</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRE23'">
                <xsl:text>Somme</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRF11'">
                <xsl:text>Bas-Rhin</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRF12'">
                <xsl:text>Haut-Rhin</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRF21'">
                <xsl:text>Ardennes</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRF22'">
                <xsl:text>Aube</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRF23'">
                <xsl:text>Marne</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRF24'">
                <xsl:text>Haute-Marne</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRF31'">
                <xsl:text>Meurthe-et-Moselle</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRF32'">
                <xsl:text>Meuse</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRF33'">
                <xsl:text>Moselle</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRF34'">
                <xsl:text>Vosges</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRG01'">
                <xsl:text>Loire-Atlantique</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRG02'">
                <xsl:text>Maine-et-Loire</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRG03'">
                <xsl:text>Mayenne</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRG04'">
                <xsl:text>Sarthe</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRG05'">
                <xsl:text>Vendée</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRH01'">
                <xsl:text>Côtes-d’Armor</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRH02'">
                <xsl:text>Finistère</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRH03'">
                <xsl:text>Ille-et-Vilaine</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRH04'">
                <xsl:text>Morbihan</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI11'">
                <xsl:text>Dordogne</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI12'">
                <xsl:text>Gironde</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI13'">
                <xsl:text>Landes</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI14'">
                <xsl:text>Lot-et-Garonne</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI15'">
                <xsl:text>Pyrénées-Atlantiques</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI21'">
                <xsl:text>Corrèze</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI22'">
                <xsl:text>Creuse</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI23'">
                <xsl:text>Haute-Vienne</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI31'">
                <xsl:text>Charente</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI32'">
                <xsl:text>Charente-Maritime</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI33'">
                <xsl:text>Deux-Sèvres</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRI34'">
                <xsl:text>Vienne</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ11'">
                <xsl:text>Aude</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ12'">
                <xsl:text>Gard</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ13'">
                <xsl:text>Hérault</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ14'">
                <xsl:text>Lozère</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ15'">
                <xsl:text>Pyrénées-Orientales</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ21'">
                <xsl:text>Ariège</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ22'">
                <xsl:text>Aveyron</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ23'">
                <xsl:text>Haute-Garonne</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ24'">
                <xsl:text>Gers</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ25'">
                <xsl:text>Lot</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ26'">
                <xsl:text>Hautes-Pyrénées</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ27'">
                <xsl:text>Tarn</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRJ28'">
                <xsl:text>Tarn-et-Garonne</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK11'">
                <xsl:text>Allier</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK12'">
                <xsl:text>Cantal</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK13'">
                <xsl:text>Haute-Loire</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK14'">
                <xsl:text>Puy-de-Dôme</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK21'">
                <xsl:text>Ain</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK22'">
                <xsl:text>Ardèche</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK23'">
                <xsl:text>Drôme</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK24'">
                <xsl:text>Isère</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK25'">
                <xsl:text>Loire</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK26'">
                <xsl:text>Rhône</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK27'">
                <xsl:text>Savoie</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRK28'">
                <xsl:text>Haute-Savoie</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRL01'">
                <xsl:text>Alpes-de-Haute-Provence</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRL02'">
                <xsl:text>Hautes-Alpes</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRL03'">
                <xsl:text>Alpes-Maritimes</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRL04'">
                <xsl:text>Bouches-du-Rhône</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRL05'">
                <xsl:text>Var</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRL06'">
                <xsl:text>Vaucluse</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRM01'">
                <xsl:text>Corse-du-Sud</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRM02'">
                <xsl:text>Haute-Corse</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRY10'">
                <xsl:text>Guadeloupe</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRY20'">
                <xsl:text>Martinique</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRY30'">
                <xsl:text>Guyane (française)</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRY40'">
                <xsl:text>La Réunion</xsl:text>
            </xsl:when>
            <xsl:when test="$code = 'FRY50'">
                <xsl:text>Mayotte</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>