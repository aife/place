<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="2.0">

    <xsl:template name="codeLangue">
        <xsl:param name="emplacement"/>
        <xsl:choose>
            <xsl:when test="$emplacement = 'espagnol'">es</xsl:when>
            <xsl:when test="$emplacement = 'fran�ais'">fr</xsl:when>
            <xsl:when test="$emplacement = 'danois'">da</xsl:when>
            <xsl:when test="$emplacement = 'allemand'">de</xsl:when>
            <xsl:when test="$emplacement = 'grec'">el</xsl:when>
            <xsl:when test="$emplacement = 'anglais'">en</xsl:when>
            <xsl:when test="$emplacement = 'italien'">it</xsl:when>
            <xsl:when test="$emplacement = 'n�erlandais'">nl</xsl:when>
            <xsl:when test="$emplacement = 'portugais'">pt</xsl:when>
            <xsl:when test="$emplacement = 'finnois'">fi</xsl:when>
            <xsl:when test="$emplacement = 'su�dois'">sv</xsl:when>
            <xsl:when test="$emplacement = 'tch�que'">cs</xsl:when>
            <xsl:when test="$emplacement = 'estonien'">et</xsl:when>
            <xsl:when test="$emplacement = 'hongrois'">hu</xsl:when>
            <xsl:when test="$emplacement = 'letton'">lv</xsl:when>
            <xsl:when test="$emplacement = 'lituanien'">lt</xsl:when>
            <xsl:when test="$emplacement = 'maltais'">mt</xsl:when>
            <xsl:when test="$emplacement = 'polonais'">pl</xsl:when>
            <xsl:when test="$emplacement = 'slovaque'">sk</xsl:when>
            <xsl:when test="$emplacement = 'slov�ne'">sl</xsl:when>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
			