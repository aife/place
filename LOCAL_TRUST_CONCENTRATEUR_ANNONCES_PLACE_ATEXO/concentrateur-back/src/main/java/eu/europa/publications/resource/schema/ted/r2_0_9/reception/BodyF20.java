//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Section I: CONTRACTING AUTHORITY
 *
 * <p>Classe Java pour body_f20 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="body_f20"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ADDRESS_CONTRACTING_BODY"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "body_f20", propOrder = {
        "addresscontractingbody"
})
@ToString
@EqualsAndHashCode
public class BodyF20 {

    @XmlElement(name = "ADDRESS_CONTRACTING_BODY", required = true)
    protected ContactContractingBody addresscontractingbody;

    /**
     * Obtient la valeur de la propriété addresscontractingbody.
     *
     * @return possible object is
     * {@link ContactContractingBody }
     */
    public ContactContractingBody getADDRESSCONTRACTINGBODY() {
        return addresscontractingbody;
    }

    /**
     * Définit la valeur de la propriété addresscontractingbody.
     *
     * @param value allowed object is
     *              {@link ContactContractingBody }
     */
    public void setADDRESSCONTRACTINGBODY(ContactContractingBody value) {
        this.addresscontractingbody = value;
    }

}
