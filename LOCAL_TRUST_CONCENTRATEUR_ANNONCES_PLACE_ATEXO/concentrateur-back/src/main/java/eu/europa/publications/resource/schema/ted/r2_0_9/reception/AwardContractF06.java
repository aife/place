//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


/**
 * Section V: AWARD OF CONTRACT
 *
 * <p>Classe Java pour award_contract_f06 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="award_contract_f06"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contract_number"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TITLE" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NO_AWARDED_CONTRACT"/&gt;
 *           &lt;element name="AWARDED_CONTRACT"&gt;
 *             &lt;complexType&gt;
 *               &lt;complexContent&gt;
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                   &lt;sequence&gt;
 *                     &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DATE_CONCLUSION_CONTRACT"/&gt;
 *                     &lt;element name="TENDERS"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_man"&gt;
 *                             &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}nb_tenders"/&gt;
 *                           &lt;/extension&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;element name="CONTRACTORS"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_man"&gt;
 *                             &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contractor"/&gt;
 *                           &lt;/extension&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;element name="VALUES"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_man"&gt;
 *                             &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_contract_value"/&gt;
 *                           &lt;/extension&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;sequence minOccurs="0"&gt;
 *                       &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LIKELY_SUBCONTRACTED"/&gt;
 *                       &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}subcontracting"/&gt;
 *                     &lt;/sequence&gt;
 *                     &lt;element name="VAL_BARGAIN_PURCHASE" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}val" minOccurs="0"/&gt;
 *                     &lt;element name="NB_CONTRACT_AWARDED"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;simpleContent&gt;
 *                           &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;nb"&gt;
 *                             &lt;attribute name="PUBLICATION" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="NO" /&gt;
 *                           &lt;/extension&gt;
 *                         &lt;/simpleContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;element name="COUNTRY_ORIGIN"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}non_published"&gt;
 *                             &lt;choice&gt;
 *                               &lt;sequence&gt;
 *                                 &lt;element name="COMMUNITY_ORIGIN" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty"/&gt;
 *                                 &lt;element name="NON_COMMUNITY_ORIGIN" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}country" maxOccurs="10" minOccurs="0"/&gt;
 *                               &lt;/sequence&gt;
 *                               &lt;element name="NON_COMMUNITY_ORIGIN" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}country" maxOccurs="10"/&gt;
 *                             &lt;/choice&gt;
 *                           &lt;/extension&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;choice&gt;
 *                       &lt;element name="AWARDED_TENDERER_VARIANT" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}non_published"/&gt;
 *                       &lt;element name="NO_AWARDED_TENDERER_VARIANT" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}non_published"/&gt;
 *                     &lt;/choice&gt;
 *                     &lt;choice&gt;
 *                       &lt;element name="TENDERS_EXCLUDED" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}non_published"/&gt;
 *                       &lt;element name="NO_TENDERS_EXCLUDED" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}non_published"/&gt;
 *                     &lt;/choice&gt;
 *                   &lt;/sequence&gt;
 *                 &lt;/restriction&gt;
 *               &lt;/complexContent&gt;
 *             &lt;/complexType&gt;
 *           &lt;/element&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ITEM" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}nb_contract" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "award_contract_f06", propOrder = {
        "contractno",
        "lotno",
        "title",
        "noawardedcontract",
        "awardedcontract"
})
@ToString
@EqualsAndHashCode
public class AwardContractF06 {

    @XmlElement(name = "CONTRACT_NO")
    protected String contractno;
    @XmlElement(name = "LOT_NO")
    protected String lotno;
    @XmlElement(name = "TITLE")
    protected TextFtSingleLine title;
    @XmlElement(name = "NO_AWARDED_CONTRACT")
    protected NoAward noawardedcontract;
    @XmlElement(name = "AWARDED_CONTRACT")
    protected AwardContractF06.AWARDEDCONTRACT awardedcontract;
    @XmlAttribute(name = "ITEM", required = true)
    protected int item;

    /**
     * Obtient la valeur de la propriété contractno.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCONTRACTNO() {
        return contractno;
    }

    /**
     * Définit la valeur de la propriété contractno.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCONTRACTNO(String value) {
        this.contractno = value;
    }

    /**
     * Obtient la valeur de la propriété lotno.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLOTNO() {
        return lotno;
    }

    /**
     * Définit la valeur de la propriété lotno.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLOTNO(String value) {
        this.lotno = value;
    }

    /**
     * Obtient la valeur de la propriété title.
     *
     * @return possible object is
     * {@link TextFtSingleLine }
     */
    public TextFtSingleLine getTITLE() {
        return title;
    }

    /**
     * Définit la valeur de la propriété title.
     *
     * @param value allowed object is
     *              {@link TextFtSingleLine }
     */
    public void setTITLE(TextFtSingleLine value) {
        this.title = value;
    }

    /**
     * Obtient la valeur de la propriété noawardedcontract.
     *
     * @return possible object is
     * {@link NoAward }
     */
    public NoAward getNOAWARDEDCONTRACT() {
        return noawardedcontract;
    }

    /**
     * Définit la valeur de la propriété noawardedcontract.
     *
     * @param value allowed object is
     *              {@link NoAward }
     */
    public void setNOAWARDEDCONTRACT(NoAward value) {
        this.noawardedcontract = value;
    }

    /**
     * Obtient la valeur de la propriété awardedcontract.
     *
     * @return possible object is
     * {@link AwardContractF06 .AWARDEDCONTRACT }
     */
    public AwardContractF06.AWARDEDCONTRACT getAWARDEDCONTRACT() {
        return awardedcontract;
    }

    /**
     * Définit la valeur de la propriété awardedcontract.
     *
     * @param value allowed object is
     *              {@link AwardContractF06 .AWARDEDCONTRACT }
     */
    public void setAWARDEDCONTRACT(AwardContractF06.AWARDEDCONTRACT value) {
        this.awardedcontract = value;
    }

    /**
     * Obtient la valeur de la propriété item.
     */
    public int getITEM() {
        return item;
    }

    /**
     * Définit la valeur de la propriété item.
     */
    public void setITEM(int value) {
        this.item = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DATE_CONCLUSION_CONTRACT"/&gt;
     *         &lt;element name="TENDERS"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_man"&gt;
     *                 &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}nb_tenders"/&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="CONTRACTORS"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_man"&gt;
     *                 &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contractor"/&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="VALUES"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_man"&gt;
     *                 &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_contract_value"/&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;sequence minOccurs="0"&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LIKELY_SUBCONTRACTED"/&gt;
     *           &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}subcontracting"/&gt;
     *         &lt;/sequence&gt;
     *         &lt;element name="VAL_BARGAIN_PURCHASE" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}val" minOccurs="0"/&gt;
     *         &lt;element name="NB_CONTRACT_AWARDED"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;nb"&gt;
     *                 &lt;attribute name="PUBLICATION" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="NO" /&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="COUNTRY_ORIGIN"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}non_published"&gt;
     *                 &lt;choice&gt;
     *                   &lt;sequence&gt;
     *                     &lt;element name="COMMUNITY_ORIGIN" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty"/&gt;
     *                     &lt;element name="NON_COMMUNITY_ORIGIN" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}country" maxOccurs="10" minOccurs="0"/&gt;
     *                   &lt;/sequence&gt;
     *                   &lt;element name="NON_COMMUNITY_ORIGIN" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}country" maxOccurs="10"/&gt;
     *                 &lt;/choice&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;choice&gt;
     *           &lt;element name="AWARDED_TENDERER_VARIANT" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}non_published"/&gt;
     *           &lt;element name="NO_AWARDED_TENDERER_VARIANT" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}non_published"/&gt;
     *         &lt;/choice&gt;
     *         &lt;choice&gt;
     *           &lt;element name="TENDERS_EXCLUDED" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}non_published"/&gt;
     *           &lt;element name="NO_TENDERS_EXCLUDED" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}non_published"/&gt;
     *         &lt;/choice&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "dateconclusioncontract",
            "tenders",
            "contractors",
            "values",
            "likelysubcontracted",
            "valsubcontracting",
            "pctsubcontracting",
            "infoaddsubcontracting",
            "valbargainpurchase",
            "nbcontractawarded",
            "countryorigin",
            "awardedtenderervariant",
            "noawardedtenderervariant",
            "tendersexcluded",
            "notendersexcluded"
    })
    @ToString
    @EqualsAndHashCode
    public static class AWARDEDCONTRACT {

        @XmlElement(name = "DATE_CONCLUSION_CONTRACT", required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dateconclusioncontract;
        @XmlElement(name = "TENDERS", required = true)
        protected AwardContractF06.AWARDEDCONTRACT.TENDERS tenders;
        @XmlElement(name = "CONTRACTORS", required = true)
        protected AwardContractF06.AWARDEDCONTRACT.CONTRACTORS contractors;
        @XmlElement(name = "VALUES", required = true)
        protected AwardContractF06.AWARDEDCONTRACT.VALUES values;
        @XmlElement(name = "LIKELY_SUBCONTRACTED")
        protected Empty likelysubcontracted;
        @XmlElement(name = "VAL_SUBCONTRACTING")
        protected Val valsubcontracting;
        @XmlElement(name = "PCT_SUBCONTRACTING")
        @XmlSchemaType(name = "integer")
        protected Integer pctsubcontracting;
        @XmlElement(name = "INFO_ADD_SUBCONTRACTING")
        protected TextFtMultiLines infoaddsubcontracting;
        @XmlElement(name = "VAL_BARGAIN_PURCHASE")
        protected Val valbargainpurchase;
        @XmlElement(name = "NB_CONTRACT_AWARDED", required = true)
        protected AwardContractF06.AWARDEDCONTRACT.NBCONTRACTAWARDED nbcontractawarded;
        @XmlElement(name = "COUNTRY_ORIGIN", required = true)
        protected AwardContractF06.AWARDEDCONTRACT.COUNTRYORIGIN countryorigin;
        @XmlElement(name = "AWARDED_TENDERER_VARIANT")
        protected NonPublished awardedtenderervariant;
        @XmlElement(name = "NO_AWARDED_TENDERER_VARIANT")
        protected NonPublished noawardedtenderervariant;
        @XmlElement(name = "TENDERS_EXCLUDED")
        protected NonPublished tendersexcluded;
        @XmlElement(name = "NO_TENDERS_EXCLUDED")
        protected NonPublished notendersexcluded;

        /**
         * Obtient la valeur de la propriété dateconclusioncontract.
         *
         * @return possible object is
         * {@link XMLGregorianCalendar }
         */
        public XMLGregorianCalendar getDATECONCLUSIONCONTRACT() {
            return dateconclusioncontract;
        }

        /**
         * Définit la valeur de la propriété dateconclusioncontract.
         *
         * @param value allowed object is
         *              {@link XMLGregorianCalendar }
         */
        public void setDATECONCLUSIONCONTRACT(XMLGregorianCalendar value) {
            this.dateconclusioncontract = value;
        }

        /**
         * Obtient la valeur de la propriété tenders.
         *
         * @return possible object is
         * {@link AwardContractF06 .AWARDEDCONTRACT.TENDERS }
         */
        public AwardContractF06.AWARDEDCONTRACT.TENDERS getTENDERS() {
            return tenders;
        }

        /**
         * Définit la valeur de la propriété tenders.
         *
         * @param value allowed object is
         *              {@link AwardContractF06 .AWARDEDCONTRACT.TENDERS }
         */
        public void setTENDERS(AwardContractF06.AWARDEDCONTRACT.TENDERS value) {
            this.tenders = value;
        }

        /**
         * Obtient la valeur de la propriété contractors.
         *
         * @return possible object is
         * {@link AwardContractF06 .AWARDEDCONTRACT.CONTRACTORS }
         */
        public AwardContractF06.AWARDEDCONTRACT.CONTRACTORS getCONTRACTORS() {
            return contractors;
        }

        /**
         * Définit la valeur de la propriété contractors.
         *
         * @param value allowed object is
         *              {@link AwardContractF06 .AWARDEDCONTRACT.CONTRACTORS }
         */
        public void setCONTRACTORS(AwardContractF06.AWARDEDCONTRACT.CONTRACTORS value) {
            this.contractors = value;
        }

        /**
         * Obtient la valeur de la propriété values.
         *
         * @return possible object is
         * {@link AwardContractF06 .AWARDEDCONTRACT.VALUES }
         */
        public AwardContractF06.AWARDEDCONTRACT.VALUES getVALUES() {
            return values;
        }

        /**
         * Définit la valeur de la propriété values.
         *
         * @param value allowed object is
         *              {@link AwardContractF06 .AWARDEDCONTRACT.VALUES }
         */
        public void setVALUES(AwardContractF06.AWARDEDCONTRACT.VALUES value) {
            this.values = value;
        }

        /**
         * Obtient la valeur de la propriété likelysubcontracted.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getLIKELYSUBCONTRACTED() {
            return likelysubcontracted;
        }

        /**
         * Définit la valeur de la propriété likelysubcontracted.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setLIKELYSUBCONTRACTED(Empty value) {
            this.likelysubcontracted = value;
        }

        /**
         * Obtient la valeur de la propriété valsubcontracting.
         *
         * @return possible object is
         * {@link Val }
         */
        public Val getVALSUBCONTRACTING() {
            return valsubcontracting;
        }

        /**
         * Définit la valeur de la propriété valsubcontracting.
         *
         * @param value allowed object is
         *              {@link Val }
         */
        public void setVALSUBCONTRACTING(Val value) {
            this.valsubcontracting = value;
        }

        /**
         * Obtient la valeur de la propriété pctsubcontracting.
         *
         * @return possible object is
         * {@link Integer }
         */
        public Integer getPCTSUBCONTRACTING() {
            return pctsubcontracting;
        }

        /**
         * Définit la valeur de la propriété pctsubcontracting.
         *
         * @param value allowed object is
         *              {@link Integer }
         */
        public void setPCTSUBCONTRACTING(Integer value) {
            this.pctsubcontracting = value;
        }

        /**
         * Obtient la valeur de la propriété infoaddsubcontracting.
         *
         * @return possible object is
         * {@link TextFtMultiLines }
         */
        public TextFtMultiLines getINFOADDSUBCONTRACTING() {
            return infoaddsubcontracting;
        }

        /**
         * Définit la valeur de la propriété infoaddsubcontracting.
         *
         * @param value allowed object is
         *              {@link TextFtMultiLines }
         */
        public void setINFOADDSUBCONTRACTING(TextFtMultiLines value) {
            this.infoaddsubcontracting = value;
        }

        /**
         * Obtient la valeur de la propriété valbargainpurchase.
         *
         * @return possible object is
         * {@link Val }
         */
        public Val getVALBARGAINPURCHASE() {
            return valbargainpurchase;
        }

        /**
         * Définit la valeur de la propriété valbargainpurchase.
         *
         * @param value allowed object is
         *              {@link Val }
         */
        public void setVALBARGAINPURCHASE(Val value) {
            this.valbargainpurchase = value;
        }

        /**
         * Obtient la valeur de la propriété nbcontractawarded.
         *
         * @return possible object is
         * {@link AwardContractF06 .AWARDEDCONTRACT.NBCONTRACTAWARDED }
         */
        public AwardContractF06.AWARDEDCONTRACT.NBCONTRACTAWARDED getNBCONTRACTAWARDED() {
            return nbcontractawarded;
        }

        /**
         * Définit la valeur de la propriété nbcontractawarded.
         *
         * @param value allowed object is
         *              {@link AwardContractF06 .AWARDEDCONTRACT.NBCONTRACTAWARDED }
         */
        public void setNBCONTRACTAWARDED(AwardContractF06.AWARDEDCONTRACT.NBCONTRACTAWARDED value) {
            this.nbcontractawarded = value;
        }

        /**
         * Obtient la valeur de la propriété countryorigin.
         *
         * @return possible object is
         * {@link AwardContractF06 .AWARDEDCONTRACT.COUNTRYORIGIN }
         */
        public AwardContractF06.AWARDEDCONTRACT.COUNTRYORIGIN getCOUNTRYORIGIN() {
            return countryorigin;
        }

        /**
         * Définit la valeur de la propriété countryorigin.
         *
         * @param value allowed object is
         *              {@link AwardContractF06 .AWARDEDCONTRACT.COUNTRYORIGIN }
         */
        public void setCOUNTRYORIGIN(AwardContractF06.AWARDEDCONTRACT.COUNTRYORIGIN value) {
            this.countryorigin = value;
        }

        /**
         * Obtient la valeur de la propriété awardedtenderervariant.
         *
         * @return possible object is
         * {@link NonPublished }
         */
        public NonPublished getAWARDEDTENDERERVARIANT() {
            return awardedtenderervariant;
        }

        /**
         * Définit la valeur de la propriété awardedtenderervariant.
         *
         * @param value allowed object is
         *              {@link NonPublished }
         */
        public void setAWARDEDTENDERERVARIANT(NonPublished value) {
            this.awardedtenderervariant = value;
        }

        /**
         * Obtient la valeur de la propriété noawardedtenderervariant.
         *
         * @return possible object is
         * {@link NonPublished }
         */
        public NonPublished getNOAWARDEDTENDERERVARIANT() {
            return noawardedtenderervariant;
        }

        /**
         * Définit la valeur de la propriété noawardedtenderervariant.
         *
         * @param value allowed object is
         *              {@link NonPublished }
         */
        public void setNOAWARDEDTENDERERVARIANT(NonPublished value) {
            this.noawardedtenderervariant = value;
        }

        /**
         * Obtient la valeur de la propriété tendersexcluded.
         *
         * @return possible object is
         * {@link NonPublished }
         */
        public NonPublished getTENDERSEXCLUDED() {
            return tendersexcluded;
        }

        /**
         * Définit la valeur de la propriété tendersexcluded.
         *
         * @param value allowed object is
         *              {@link NonPublished }
         */
        public void setTENDERSEXCLUDED(NonPublished value) {
            this.tendersexcluded = value;
        }

        /**
         * Obtient la valeur de la propriété notendersexcluded.
         *
         * @return possible object is
         * {@link NonPublished }
         */
        public NonPublished getNOTENDERSEXCLUDED() {
            return notendersexcluded;
        }

        /**
         * Définit la valeur de la propriété notendersexcluded.
         *
         * @param value allowed object is
         *              {@link NonPublished }
         */
        public void setNOTENDERSEXCLUDED(NonPublished value) {
            this.notendersexcluded = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_man"&gt;
         *       &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contractor"/&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "awardedtogroup",
                "contractorSmeMan",
                "noawardedtogroup",
                "contractor"
        })
        @ToString(callSuper = true)
        @EqualsAndHashCode(callSuper = true)
        public static class CONTRACTORS
                extends AgreeToPublicationMan {

            @XmlElement(name = "AWARDED_TO_GROUP")
            protected Empty awardedtogroup;
            @XmlElement(name = "CONTRACTOR")
            protected List<eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR> contractorSmeMan;
            @XmlElement(name = "NO_AWARDED_TO_GROUP")
            protected Empty noawardedtogroup;
            @XmlElement(name = "CONTRACTOR")
            protected eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR contractor;

            /**
             * Obtient la valeur de la propriété awardedtogroup.
             *
             * @return possible object is
             * {@link Empty }
             */
            public Empty getAWARDEDTOGROUP() {
                return awardedtogroup;
            }

            /**
             * Définit la valeur de la propriété awardedtogroup.
             *
             * @param value allowed object is
             *              {@link Empty }
             */
            public void setAWARDEDTOGROUP(Empty value) {
                this.awardedtogroup = value;
            }

            /**
             * Gets the value of the contractorSmeMan property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the contractorSmeMan property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getContractorSmeMan().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25 .AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR }
             */
            public List<eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR> getContractorSmeMan() {
                if (contractorSmeMan == null) {
                    contractorSmeMan = new ArrayList<eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR>();
                }
                return this.contractorSmeMan;
            }

            /**
             * Obtient la valeur de la propriété noawardedtogroup.
             *
             * @return possible object is
             * {@link Empty }
             */
            public Empty getNOAWARDEDTOGROUP() {
                return noawardedtogroup;
            }

            /**
             * Définit la valeur de la propriété noawardedtogroup.
             *
             * @param value allowed object is
             *              {@link Empty }
             */
            public void setNOAWARDEDTOGROUP(Empty value) {
                this.noawardedtogroup = value;
            }

            /**
             * Obtient la valeur de la propriété contractor.
             *
             * @return possible object is
             * {@link eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25 .AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR }
             */
            public eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR getCONTRACTOR() {
                return contractor;
            }

            /**
             * Définit la valeur de la propriété contractor.
             *
             * @param value allowed object is
             *              {@link eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25 .AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR }
             */
            public void setCONTRACTOR(eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR value) {
                this.contractor = value;
            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}non_published"&gt;
         *       &lt;choice&gt;
         *         &lt;sequence&gt;
         *           &lt;element name="COMMUNITY_ORIGIN" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty"/&gt;
         *           &lt;element name="NON_COMMUNITY_ORIGIN" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}country" maxOccurs="10" minOccurs="0"/&gt;
         *         &lt;/sequence&gt;
         *         &lt;element name="NON_COMMUNITY_ORIGIN" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}country" maxOccurs="10"/&gt;
         *       &lt;/choice&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "rest"
        })
        @ToString(callSuper = true)
        @EqualsAndHashCode(callSuper = true)
        public static class COUNTRYORIGIN
                extends NonPublished {

            @XmlElementRefs({
                    @XmlElementRef(name = "COMMUNITY_ORIGIN", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "NON_COMMUNITY_ORIGIN", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false)
            })
            protected List<JAXBElement<?>> rest;

            /**
             * Obtient le reste du modèle de contenu.
             *
             * <p>
             * Vous obtenez la propriété "catch-all" pour la raison suivante :
             * Le nom de champ "NONCOMMUNITYORIGIN" est utilisé par deux parties différentes d'un schéma. Reportez-vous à :
             * ligne 94 sur file:/home/iat-atx/Bureau/ATEXO/WORKSPACE/lt_concentrateur_annonces/concentrateur-back/src/main/resources/simplified-xsd/joue/F06_2014.xsd
             * ligne 92 sur file:/home/iat-atx/Bureau/ATEXO/WORKSPACE/lt_concentrateur_annonces/concentrateur-back/src/main/resources/simplified-xsd/joue/F06_2014.xsd
             * <p>
             * Pour vous débarrasser de cette propriété, appliquez une personnalisation de propriété à l'une
             * des deux déclarations suivantes afin de modifier leurs noms :
             * Gets the value of the rest property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rest property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRest().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link JAXBElement }{@code <}{@link Empty }{@code >}
             * {@link JAXBElement }{@code <}{@link Country }{@code >}
             */
            public List<JAXBElement<?>> getRest() {
                if (rest == null) {
                    rest = new ArrayList<JAXBElement<?>>();
                }
                return this.rest;
            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;nb"&gt;
         *       &lt;attribute name="PUBLICATION" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="NO" /&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "value"
        })
        @ToString
        @EqualsAndHashCode
        public static class NBCONTRACTAWARDED {

            @XmlValue
            protected BigInteger value;
            @XmlAttribute(name = "PUBLICATION", required = true)
            @XmlSchemaType(name = "anySimpleType")
            protected String publication;

            /**
             * Obtient la valeur de la propriété value.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getValue() {
                return value;
            }

            /**
             * Définit la valeur de la propriété value.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setValue(BigInteger value) {
                this.value = value;
            }

            /**
             * Obtient la valeur de la propriété publication.
             *
             * @return possible object is
             * {@link String }
             */
            public String getPUBLICATION() {
                if (publication == null) {
                    return "NO";
                } else {
                    return publication;
                }
            }

            /**
             * Définit la valeur de la propriété publication.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setPUBLICATION(String value) {
                this.publication = value;
            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_man"&gt;
         *       &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}nb_tenders"/&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "nbtendersreceived",
                "nbtendersreceivedsme",
                "nbtendersreceivedothereu",
                "nbtendersreceivednoneu",
                "nbtendersreceivedemeans"
        })
        @ToString(callSuper = true)
        @EqualsAndHashCode(callSuper = true)
        public static class TENDERS
                extends AgreeToPublicationMan {

            @XmlElement(name = "NB_TENDERS_RECEIVED", required = true)
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger nbtendersreceived;
            @XmlElement(name = "NB_TENDERS_RECEIVED_SME")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger nbtendersreceivedsme;
            @XmlElement(name = "NB_TENDERS_RECEIVED_OTHER_EU")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger nbtendersreceivedothereu;
            @XmlElement(name = "NB_TENDERS_RECEIVED_NON_EU")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger nbtendersreceivednoneu;
            @XmlElement(name = "NB_TENDERS_RECEIVED_EMEANS")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger nbtendersreceivedemeans;

            /**
             * Obtient la valeur de la propriété nbtendersreceived.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getNBTENDERSRECEIVED() {
                return nbtendersreceived;
            }

            /**
             * Définit la valeur de la propriété nbtendersreceived.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setNBTENDERSRECEIVED(BigInteger value) {
                this.nbtendersreceived = value;
            }

            /**
             * Obtient la valeur de la propriété nbtendersreceivedsme.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getNBTENDERSRECEIVEDSME() {
                return nbtendersreceivedsme;
            }

            /**
             * Définit la valeur de la propriété nbtendersreceivedsme.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setNBTENDERSRECEIVEDSME(BigInteger value) {
                this.nbtendersreceivedsme = value;
            }

            /**
             * Obtient la valeur de la propriété nbtendersreceivedothereu.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getNBTENDERSRECEIVEDOTHEREU() {
                return nbtendersreceivedothereu;
            }

            /**
             * Définit la valeur de la propriété nbtendersreceivedothereu.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setNBTENDERSRECEIVEDOTHEREU(BigInteger value) {
                this.nbtendersreceivedothereu = value;
            }

            /**
             * Obtient la valeur de la propriété nbtendersreceivednoneu.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getNBTENDERSRECEIVEDNONEU() {
                return nbtendersreceivednoneu;
            }

            /**
             * Définit la valeur de la propriété nbtendersreceivednoneu.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setNBTENDERSRECEIVEDNONEU(BigInteger value) {
                this.nbtendersreceivednoneu = value;
            }

            /**
             * Obtient la valeur de la propriété nbtendersreceivedemeans.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getNBTENDERSRECEIVEDEMEANS() {
                return nbtendersreceivedemeans;
            }

            /**
             * Définit la valeur de la propriété nbtendersreceivedemeans.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setNBTENDERSRECEIVEDEMEANS(BigInteger value) {
                this.nbtendersreceivedemeans = value;
            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_man"&gt;
         *       &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_contract_value"/&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "valestimatedtotal",
                "valtotal",
                "valrangetotal"
        })
        @ToString(callSuper = true)
        @EqualsAndHashCode(callSuper = true)
        public static class VALUES
                extends AgreeToPublicationMan {

            @XmlElement(name = "VAL_ESTIMATED_TOTAL")
            protected Val valestimatedtotal;
            @XmlElement(name = "VAL_TOTAL")
            protected Val valtotal;
            @XmlElement(name = "VAL_RANGE_TOTAL")
            protected ValRange valrangetotal;

            /**
             * Obtient la valeur de la propriété valestimatedtotal.
             *
             * @return possible object is
             * {@link Val }
             */
            public Val getVALESTIMATEDTOTAL() {
                return valestimatedtotal;
            }

            /**
             * Définit la valeur de la propriété valestimatedtotal.
             *
             * @param value allowed object is
             *              {@link Val }
             */
            public void setVALESTIMATEDTOTAL(Val value) {
                this.valestimatedtotal = value;
            }

            /**
             * Obtient la valeur de la propriété valtotal.
             *
             * @return possible object is
             * {@link Val }
             */
            public Val getVALTOTAL() {
                return valtotal;
            }

            /**
             * Définit la valeur de la propriété valtotal.
             *
             * @param value allowed object is
             *              {@link Val }
             */
            public void setVALTOTAL(Val value) {
                this.valtotal = value;
            }

            /**
             * Obtient la valeur de la propriété valrangetotal.
             *
             * @return possible object is
             * {@link ValRange }
             */
            public ValRange getVALRANGETOTAL() {
                return valrangetotal;
            }

            /**
             * Définit la valeur de la propriété valrangetotal.
             *
             * @param value allowed object is
             *              {@link ValRange }
             */
            public void setVALRANGETOTAL(ValRange value) {
                this.valrangetotal = value;
            }

        }

    }

}
