//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import eu.europa.publications.resource.authority.currency.TCurrencyTedschema;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;


/**
 * <p>Classe Java pour val complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="val"&gt;
 *   &lt;simpleContent&gt;
 *     &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;cost"&gt;
 *       &lt;attribute name="CURRENCY" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}currencies" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "val", propOrder = {
        "value"
})
@XmlSeeAlso({
        eu.europa.publications.resource.schema.ted.r2_0_9.reception.ObjectContractF06.VALTOTAL.class,
        eu.europa.publications.resource.schema.ted.r2_0_9.reception.ResultsF13.AWARDEDPRIZE.VALPRIZE.class,
        eu.europa.publications.resource.schema.ted.r2_0_9.reception.ObjectContractF15.VALTOTAL.class,
        eu.europa.publications.resource.schema.ted.r2_0_9.reception.ObjectContractF22.VALTOTAL.class
})
@ToString
@EqualsAndHashCode
public class Val {

    @XmlValue
    protected BigDecimal value;
    @XmlAttribute(name = "CURRENCY", required = true)
    protected TCurrencyTedschema currency;

    /**
     * Obtient la valeur de la propriété value.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getValue() {
        return value;
    }

    /**
     * Définit la valeur de la propriété value.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setValue(BigDecimal value) {
        this.value = value;
    }

    /**
     * Obtient la valeur de la propriété currency.
     *
     * @return possible object is
     * {@link TCurrencyTedschema }
     */
    public TCurrencyTedschema getCURRENCY() {
        return currency;
    }

    /**
     * Définit la valeur de la propriété currency.
     *
     * @param value allowed object is
     *              {@link TCurrencyTedschema }
     */
    public void setCURRENCY(TCurrencyTedschema value) {
        this.currency = value;
    }

    public String getFormatedValue() {
        if (this.value == null)
            return null;
        NumberFormat numberFormat = NumberFormat.getInstance(java.util.Locale.FRENCH);

        numberFormat.setRoundingMode(RoundingMode.HALF_DOWN);
        return numberFormat.format(this.value);
    }
}
