//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.math.BigInteger;


/**
 * <p>Classe Java pour framework_info complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="framework_info"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;sequence&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SEVERAL_OPERATORS"/&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NB_PARTICIPANTS" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SINGLE_OPERATOR"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}JUSTIFICATION" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "framework_info", propOrder = {
        "severaloperators",
        "nbparticipants",
        "singleoperator",
        "justification"
})
@ToString
@EqualsAndHashCode
public class FrameworkInfo {

    @XmlElement(name = "SEVERAL_OPERATORS")
    protected Empty severaloperators;
    @XmlElement(name = "NB_PARTICIPANTS")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger nbparticipants;
    @XmlElement(name = "SINGLE_OPERATOR")
    protected Empty singleoperator;
    @XmlElement(name = "JUSTIFICATION")
    protected TextFtMultiLines justification;

    /**
     * Obtient la valeur de la propriété severaloperators.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getSEVERALOPERATORS() {
        return severaloperators;
    }

    /**
     * Définit la valeur de la propriété severaloperators.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setSEVERALOPERATORS(Empty value) {
        this.severaloperators = value;
    }

    /**
     * Obtient la valeur de la propriété nbparticipants.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public BigInteger getNBPARTICIPANTS() {
        return nbparticipants;
    }

    /**
     * Définit la valeur de la propriété nbparticipants.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public void setNBPARTICIPANTS(BigInteger value) {
        this.nbparticipants = value;
    }

    /**
     * Obtient la valeur de la propriété singleoperator.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getSINGLEOPERATOR() {
        return singleoperator;
    }

    /**
     * Définit la valeur de la propriété singleoperator.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setSINGLEOPERATOR(Empty value) {
        this.singleoperator = value;
    }

    /**
     * for any duration exceeding a certain number of years
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getJUSTIFICATION() {
        return justification;
    }

    /**
     * Définit la valeur de la propriété justification.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setJUSTIFICATION(TextFtMultiLines value) {
        this.justification = value;
    }

}
