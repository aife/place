//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java pour object_f13 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="object_f13"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CPV_ADDITIONAL" maxOccurs="100" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SHORT_DESCR"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}eu_union_funds"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "object_f13", propOrder = {
        "cpvadditional",
        "shortdescr",
        "euprogrrelated",
        "noeuprogrrelated"
})
@ToString
@EqualsAndHashCode
public class ObjectF13 {

    @XmlElement(name = "CPV_ADDITIONAL")
    protected List<CpvSet> cpvadditional;
    @XmlElement(name = "SHORT_DESCR", required = true)
    protected TextFtMultiLines shortdescr;
    @XmlElement(name = "EU_PROGR_RELATED")
    protected TextFtMultiLines euprogrrelated;
    @XmlElement(name = "NO_EU_PROGR_RELATED")
    protected Empty noeuprogrrelated;

    /**
     * Gets the value of the cpvadditional property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cpvadditional property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCPVADDITIONAL().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CpvSet }
     */
    public List<CpvSet> getCPVADDITIONAL() {
        if (cpvadditional == null) {
            cpvadditional = new ArrayList<CpvSet>();
        }
        return this.cpvadditional;
    }

    /**
     * Obtient la valeur de la propriété shortdescr.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getSHORTDESCR() {
        return shortdescr;
    }

    /**
     * Définit la valeur de la propriété shortdescr.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setSHORTDESCR(TextFtMultiLines value) {
        this.shortdescr = value;
    }

    /**
     * Obtient la valeur de la propriété euprogrrelated.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getEUPROGRRELATED() {
        return euprogrrelated;
    }

    /**
     * Définit la valeur de la propriété euprogrrelated.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setEUPROGRRELATED(TextFtMultiLines value) {
        this.euprogrrelated = value;
    }

    /**
     * Obtient la valeur de la propriété noeuprogrrelated.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getNOEUPROGRRELATED() {
        return noeuprogrrelated;
    }

    /**
     * Définit la valeur de la propriété noeuprogrrelated.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setNOEUPROGRRELATED(Empty value) {
        this.noeuprogrrelated = value;
    }

}
