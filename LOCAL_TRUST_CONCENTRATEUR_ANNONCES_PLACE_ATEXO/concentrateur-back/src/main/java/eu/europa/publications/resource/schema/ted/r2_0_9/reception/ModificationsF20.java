//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import eu.europa.publications.resource.schema.ted._2021.nuts.Nuts;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * Section VII: MODIFICATIONS TO THE CONTRACT / CONCESSION
 *
 * <p>Classe Java pour modifications_f20 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="modifications_f20"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DESCRIPTION_PROCUREMENT"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CPV_MAIN"/&gt;
 *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CPV_ADDITIONAL" maxOccurs="100" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/2016/nuts}NUTS" maxOccurs="250"/&gt;
 *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}MAIN_SITE" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SHORT_DESCR"/&gt;
 *                   &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}time_frame"/&gt;
 *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}JUSTIFICATION" minOccurs="0"/&gt;
 *                   &lt;element name="VALUES"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}VAL_TOTAL"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="CONTRACTORS"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contractor"/&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="INFO_MODIFICATIONS"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SHORT_DESCR"/&gt;
 *                   &lt;choice&gt;
 *                     &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ADDITIONAL_NEED"/&gt;
 *                     &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}UNFORESEEN_CIRCUMSTANCE"/&gt;
 *                   &lt;/choice&gt;
 *                   &lt;element name="VALUES"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}VAL_TOTAL_BEFORE"/&gt;
 *                             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}VAL_TOTAL_AFTER"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "modifications_f20", propOrder = {
        "descriptionprocurement",
        "infomodifications"
})
@ToString
@EqualsAndHashCode
public class ModificationsF20 {

    @XmlElement(name = "DESCRIPTION_PROCUREMENT", required = true)
    protected ModificationsF20.DESCRIPTIONPROCUREMENT descriptionprocurement;
    @XmlElement(name = "INFO_MODIFICATIONS", required = true)
    protected ModificationsF20.INFOMODIFICATIONS infomodifications;

    /**
     * Obtient la valeur de la propriété descriptionprocurement.
     *
     * @return possible object is
     * {@link ModificationsF20 .DESCRIPTIONPROCUREMENT }
     */
    public ModificationsF20.DESCRIPTIONPROCUREMENT getDESCRIPTIONPROCUREMENT() {
        return descriptionprocurement;
    }

    /**
     * Définit la valeur de la propriété descriptionprocurement.
     *
     * @param value allowed object is
     *              {@link ModificationsF20 .DESCRIPTIONPROCUREMENT }
     */
    public void setDESCRIPTIONPROCUREMENT(ModificationsF20.DESCRIPTIONPROCUREMENT value) {
        this.descriptionprocurement = value;
    }

    /**
     * Obtient la valeur de la propriété infomodifications.
     *
     * @return possible object is
     * {@link ModificationsF20 .INFOMODIFICATIONS }
     */
    public ModificationsF20.INFOMODIFICATIONS getINFOMODIFICATIONS() {
        return infomodifications;
    }

    /**
     * Définit la valeur de la propriété infomodifications.
     *
     * @param value allowed object is
     *              {@link ModificationsF20 .INFOMODIFICATIONS }
     */
    public void setINFOMODIFICATIONS(ModificationsF20.INFOMODIFICATIONS value) {
        this.infomodifications = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CPV_MAIN"/&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CPV_ADDITIONAL" maxOccurs="100" minOccurs="0"/&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/2016/nuts}NUTS" maxOccurs="250"/&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}MAIN_SITE" minOccurs="0"/&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SHORT_DESCR"/&gt;
     *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}time_frame"/&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}JUSTIFICATION" minOccurs="0"/&gt;
     *         &lt;element name="VALUES"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}VAL_TOTAL"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="CONTRACTORS"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contractor"/&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "content"
    })
    @ToString
    @EqualsAndHashCode
    public static class DESCRIPTIONPROCUREMENT {

        @XmlElementRefs({
                @XmlElementRef(name = "CPV_MAIN", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "CPV_ADDITIONAL", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "NUTS", namespace = "http://publications.europa.eu/resource/schema/ted/2016/nuts", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "MAIN_SITE", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "SHORT_DESCR", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "DURATION", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "DATE_START", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "DATE_END", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "JUSTIFICATION", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "VALUES", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "CONTRACTORS", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false)
        })
        protected List<JAXBElement<?>> content;

        /**
         * Obtient le reste du modèle de contenu.
         *
         * <p>
         * Vous obtenez la propriété "catch-all" pour la raison suivante :
         * Le nom de champ "DATEEND" est utilisé par deux parties différentes d'un schéma. Reportez-vous à :
         * ligne 449 sur file:/home/iat-atx/Bureau/ATEXO/WORKSPACE/lt_concentrateur_annonces/concentrateur-back/src/main/resources/simplified-xsd/joue/common_2014.xsd
         * ligne 447 sur file:/home/iat-atx/Bureau/ATEXO/WORKSPACE/lt_concentrateur_annonces/concentrateur-back/src/main/resources/simplified-xsd/joue/common_2014.xsd
         * <p>
         * Pour vous débarrasser de cette propriété, appliquez une personnalisation de propriété à l'une
         * des deux déclarations suivantes afin de modifier leurs noms :
         * Gets the value of the content property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the content property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getContent().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link JAXBElement }{@code <}{@link CpvSet }{@code >}
         * {@link JAXBElement }{@code <}{@link CpvSet }{@code >}
         * {@link JAXBElement }{@code <}{@link Nuts }{@code >}
         * {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
         * {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
         * {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         * {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
         * {@link JAXBElement }{@code <}{@link ModificationsF20 .DESCRIPTIONPROCUREMENT.VALUES }{@code >}
         * {@link JAXBElement }{@code <}{@link ModificationsF20 .DESCRIPTIONPROCUREMENT.CONTRACTORS }{@code >}
         */
        public List<JAXBElement<?>> getContent() {
            if (content == null) {
                content = new ArrayList<JAXBElement<?>>();
            }
            return this.content;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contractor"/&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "awardedtogroup",
                "contractorSmeMan",
                "noawardedtogroup",
                "contractor"
        })
        @ToString
        @EqualsAndHashCode
        public static class CONTRACTORS {

            @XmlElement(name = "AWARDED_TO_GROUP")
            protected Empty awardedtogroup;
            @XmlElement(name = "CONTRACTOR")
            protected List<eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR> contractorSmeMan;
            @XmlElement(name = "NO_AWARDED_TO_GROUP")
            protected Empty noawardedtogroup;
            @XmlElement(name = "CONTRACTOR")
            protected eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR contractor;

            /**
             * Obtient la valeur de la propriété awardedtogroup.
             *
             * @return possible object is
             * {@link Empty }
             */
            public Empty getAWARDEDTOGROUP() {
                return awardedtogroup;
            }

            /**
             * Définit la valeur de la propriété awardedtogroup.
             *
             * @param value allowed object is
             *              {@link Empty }
             */
            public void setAWARDEDTOGROUP(Empty value) {
                this.awardedtogroup = value;
            }

            /**
             * Gets the value of the contractorSmeMan property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the contractorSmeMan property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getContractorSmeMan().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25 .AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR }
             */
            public List<eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR> getContractorSmeMan() {
                if (contractorSmeMan == null) {
                    contractorSmeMan = new ArrayList<eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR>();
                }
                return this.contractorSmeMan;
            }

            /**
             * Obtient la valeur de la propriété noawardedtogroup.
             *
             * @return possible object is
             * {@link Empty }
             */
            public Empty getNOAWARDEDTOGROUP() {
                return noawardedtogroup;
            }

            /**
             * Définit la valeur de la propriété noawardedtogroup.
             *
             * @param value allowed object is
             *              {@link Empty }
             */
            public void setNOAWARDEDTOGROUP(Empty value) {
                this.noawardedtogroup = value;
            }

            /**
             * Obtient la valeur de la propriété contractor.
             *
             * @return possible object is
             * {@link eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25 .AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR }
             */
            public eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR getCONTRACTOR() {
                return contractor;
            }

            /**
             * Définit la valeur de la propriété contractor.
             *
             * @param value allowed object is
             *              {@link eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25 .AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR }
             */
            public void setCONTRACTOR(eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR value) {
                this.contractor = value;
            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}VAL_TOTAL"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "valtotal"
        })
        @ToString
        @EqualsAndHashCode
        public static class VALUES {

            @XmlElement(name = "VAL_TOTAL", required = true)
            protected Val valtotal;

            /**
             * Obtient la valeur de la propriété valtotal.
             *
             * @return possible object is
             * {@link Val }
             */
            public Val getVALTOTAL() {
                return valtotal;
            }

            /**
             * Définit la valeur de la propriété valtotal.
             *
             * @param value allowed object is
             *              {@link Val }
             */
            public void setVALTOTAL(Val value) {
                this.valtotal = value;
            }

        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SHORT_DESCR"/&gt;
     *         &lt;choice&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ADDITIONAL_NEED"/&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}UNFORESEEN_CIRCUMSTANCE"/&gt;
     *         &lt;/choice&gt;
     *         &lt;element name="VALUES"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}VAL_TOTAL_BEFORE"/&gt;
     *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}VAL_TOTAL_AFTER"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "shortdescr",
            "additionalneed",
            "unforeseencircumstance",
            "values"
    })
    @ToString
    @EqualsAndHashCode
    public static class INFOMODIFICATIONS {

        @XmlElement(name = "SHORT_DESCR", required = true)
        protected TextFtMultiLines shortdescr;
        @XmlElement(name = "ADDITIONAL_NEED")
        protected TextFtMultiLines additionalneed;
        @XmlElement(name = "UNFORESEEN_CIRCUMSTANCE")
        protected TextFtMultiLines unforeseencircumstance;
        @XmlElement(name = "VALUES", required = true)
        protected ModificationsF20.INFOMODIFICATIONS.VALUES values;

        /**
         * Obtient la valeur de la propriété shortdescr.
         *
         * @return possible object is
         * {@link TextFtMultiLines }
         */
        public TextFtMultiLines getSHORTDESCR() {
            return shortdescr;
        }

        /**
         * Définit la valeur de la propriété shortdescr.
         *
         * @param value allowed object is
         *              {@link TextFtMultiLines }
         */
        public void setSHORTDESCR(TextFtMultiLines value) {
            this.shortdescr = value;
        }

        /**
         * Obtient la valeur de la propriété additionalneed.
         *
         * @return possible object is
         * {@link TextFtMultiLines }
         */
        public TextFtMultiLines getADDITIONALNEED() {
            return additionalneed;
        }

        /**
         * Définit la valeur de la propriété additionalneed.
         *
         * @param value allowed object is
         *              {@link TextFtMultiLines }
         */
        public void setADDITIONALNEED(TextFtMultiLines value) {
            this.additionalneed = value;
        }

        /**
         * Obtient la valeur de la propriété unforeseencircumstance.
         *
         * @return possible object is
         * {@link TextFtMultiLines }
         */
        public TextFtMultiLines getUNFORESEENCIRCUMSTANCE() {
            return unforeseencircumstance;
        }

        /**
         * Définit la valeur de la propriété unforeseencircumstance.
         *
         * @param value allowed object is
         *              {@link TextFtMultiLines }
         */
        public void setUNFORESEENCIRCUMSTANCE(TextFtMultiLines value) {
            this.unforeseencircumstance = value;
        }

        /**
         * Obtient la valeur de la propriété values.
         *
         * @return possible object is
         * {@link ModificationsF20 .INFOMODIFICATIONS.VALUES }
         */
        public ModificationsF20.INFOMODIFICATIONS.VALUES getVALUES() {
            return values;
        }

        /**
         * Définit la valeur de la propriété values.
         *
         * @param value allowed object is
         *              {@link ModificationsF20 .INFOMODIFICATIONS.VALUES }
         */
        public void setVALUES(ModificationsF20.INFOMODIFICATIONS.VALUES value) {
            this.values = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}VAL_TOTAL_BEFORE"/&gt;
         *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}VAL_TOTAL_AFTER"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "valtotalbefore",
                "valtotalafter"
        })
        @ToString
        @EqualsAndHashCode
        public static class VALUES {

            @XmlElement(name = "VAL_TOTAL_BEFORE", required = true)
            protected Val valtotalbefore;
            @XmlElement(name = "VAL_TOTAL_AFTER", required = true)
            protected Val valtotalafter;

            /**
             * Obtient la valeur de la propriété valtotalbefore.
             *
             * @return possible object is
             * {@link Val }
             */
            public Val getVALTOTALBEFORE() {
                return valtotalbefore;
            }

            /**
             * Définit la valeur de la propriété valtotalbefore.
             *
             * @param value allowed object is
             *              {@link Val }
             */
            public void setVALTOTALBEFORE(Val value) {
                this.valtotalbefore = value;
            }

            /**
             * Obtient la valeur de la propriété valtotalafter.
             *
             * @return possible object is
             * {@link Val }
             */
            public Val getVALTOTALAFTER() {
                return valtotalafter;
            }

            /**
             * Définit la valeur de la propriété valtotalafter.
             *
             * @param value allowed object is
             *              {@link Val }
             */
            public void setVALTOTALAFTER(Val value) {
                this.valtotalafter = value;
            }

        }

    }

}
