//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import eu.europa.publications.resource.schema.ted._2021.nuts.Nuts;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.CollectionUtils;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * <p>Classe Java pour object_f05 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="object_f05"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TITLE" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LOT_NO" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CPV_ADDITIONAL" maxOccurs="100" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/2016/nuts}NUTS" maxOccurs="250"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}MAIN_SITE" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SHORT_DESCR"/&gt;
 *         &lt;element name="AC"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_criteria_doc"/&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}VAL_OBJECT" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}time_frame"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}renewal"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}variants"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}limit_candidate" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}options"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ECATALOGUE_REQUIRED" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}eu_union_funds"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}INFO_ADD" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ITEM" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}nb_lot" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "object_f05", propOrder = {
        "title",
        "lotNo",
        "cpvAdditional",
        "nuts",
        "nuts2021",
        "mainSite",
        "shortDescr",
        "ac",
        "marcheReserve",
        "valObject",
        "duration",
        "dateStart",
        "dateEnd",
        "renewal",
        "renewalDescr",
        "noRenewal",
        "acceptedVariants",
        "noAcceptedVariants",
        "nbEnvisagedCandidate",
        "nbMinLimitCandidate",
        "nbMaxLimitCandidate",
        "criteriaCandidate",
        "options",
        "optionsDescr",
        "noOptions",
        "ecatalogueRequired",
        "euProgrRelated",
        "noEuProgrRelated",
        "infoAdd"
})
@ToString
@EqualsAndHashCode
@Getter
@Setter
public class ObjectF05 {
    @XmlAttribute(name = "ITEM", required = true)
    protected int item;
    @XmlElement(name = "TITLE")
    private TextFtSingleLine title;
    @XmlElement(name = "LOT_NO")
    private String lotNo;
    @XmlElement(name = "CPV_ADDITIONAL")
    private List<CpvSet> cpvAdditional;
    @XmlElement(name = "NUTS", namespace = "http://publications.europa.eu/resource/schema/ted/2016/nuts")
    private List<Nuts> nuts;
    @XmlElement(name = "NUTS", namespace = "http://publications.europa.eu/resource/schema/ted/2021/nuts")
    private List<Nuts> nuts2021;
    @XmlElement(name = "MAIN_SITE")
    private TextFtMultiLines mainSite;
    @XmlElement(name = "SHORT_DESCR")
    private TextFtMultiLines shortDescr;
    @XmlElement(name = "AC")
    private AC ac;
    @XmlElement(name = "MARCHE_RESERVE")
    private MarcheReserve marcheReserve;
    @XmlElement(name = "VAL_OBJECT")
    private Val valObject;
    @XmlElement(name = "DURATION")
    private DurationMD duration;
    @XmlElement(name = "DATE_START")
    private String dateStart;
    @XmlElement(name = "DATE_END")
    private String dateEnd;
    @XmlElement(name = "RENEWAL")
    private Empty renewal;
    @XmlElement(name = "RENEWAL_DESCR")
    private TextFtMultiLines renewalDescr;
    @XmlElement(name = "NO_RENEWAL")
    private Empty noRenewal;
    @XmlElement(name = "ACCEPTED_VARIANTS")
    private Val acceptedVariants;
    @XmlElement(name = "NO_ACCEPTED_VARIANTS")
    private Empty noAcceptedVariants;
    @XmlElement(name = "NB_ENVISAGED_CANDIDATE")
    private Long nbEnvisagedCandidate;
    @XmlElement(name = "NB_MIN_LIMIT_CANDIDATE")
    private Long nbMinLimitCandidate;
    @XmlElement(name = "NB_MAX_LIMIT_CANDIDATE")
    private Long nbMaxLimitCandidate;
    @XmlElement(name = "CRITERIA_CANDIDATE")
    private TextFtMultiLines criteriaCandidate;
    @XmlElement(name = "OPTIONS")
    private Empty options;
    @XmlElement(name = "OPTIONS_DESCR")
    private TextFtMultiLines optionsDescr;
    @XmlElement(name = "NO_OPTIONS")
    private Empty noOptions;
    @XmlElement(name = "ECATALOGUE_REQUIRED")
    private Empty ecatalogueRequired;
    @XmlElement(name = "EU_PROGR_RELATED")
    private TextFtMultiLines euProgrRelated;
    @XmlElement(name = "NO_EU_PROGR_RELATED")
    private Empty noEuProgrRelated;
    @XmlElement(name = "INFO_ADD")
    private TextFtMultiLines infoAdd;

    public String getCpvCodes() {
        if (CollectionUtils.isEmpty(this.cpvAdditional)) {
            return null;
        }
        return this.cpvAdditional.stream().map(CpvSet::getCPVCODE)
                .filter(Objects::nonNull)
                .map(CpvCodes::getCODE)
                .filter(Objects::nonNull)
                .collect(Collectors.joining(", "));
    }

    public List<P> getMarcheReserveLibelle() {
        List<P> list = new ArrayList<>();
        if (this.marcheReserve != null) {

            if (this.marcheReserve.eaEsat != null) {
                P p = new P();
                p.setValue("Marché réservé à une entreprise adaptée, un établissement de service d'aide par le travail ou une structure équivalente, employant au moins 50% de travailleurs handicapés");
                list.add(p);
            }
            if (this.marcheReserve.siae != null) {
                P p = new P();
                p.setValue("Marché réservé à une structure d'insertion par l'activité économique ou structure équivalente, employant au moins 50% de travailleurs défavorisés");
                list.add(p);
            }
            if (this.marcheReserve.eess != null) {
                P p = new P();
                p.setValue("Marché portant exclusivement sur des services sociaux et autre services spécifiques, réservé au entreprises de l'économie sociale et solidaire ou structure équivalente");
                list.add(p);
            }
        }
        return list;
    }

    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_criteria_doc"/&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "acquality",
            "accost",
            "acprice",
            "acprocurementdoc"
    })
    @ToString
    @EqualsAndHashCode
    public static class AC {

        @XmlElement(name = "AC_QUALITY")
        protected List<AcDefinition> acquality;
        @XmlElement(name = "AC_COST")
        protected List<AcDefinition> accost;
        @XmlElement(name = "AC_PRICE")
        protected ACPRICE acprice;
        @XmlElement(name = "AC_PROCUREMENT_DOC")
        protected Empty acprocurementdoc;

        /**
         * Gets the value of the acquality property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the acquality property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getACQUALITY().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AcDefinition }
         */
        public List<AcDefinition> getACQUALITY() {
            if (acquality == null) {
                acquality = new ArrayList<AcDefinition>();
            }
            return this.acquality;
        }

        /**
         * Gets the value of the accost property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the accost property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getACCOST().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AcDefinition }
         */
        public List<AcDefinition> getACCOST() {
            if (accost == null) {
                accost = new ArrayList<AcDefinition>();
            }
            return this.accost;
        }

        /**
         * Obtient la valeur de la propriété acprice.
         *
         * @return possible object is
         * {@link ACPRICE }
         */
        public ACPRICE getACPRICE() {
            return acprice;
        }

        /**
         * Définit la valeur de la propriété acprice.
         *
         * @param value allowed object is
         *              {@link ACPRICE }
         */
        public void setACPRICE(ACPRICE value) {
            this.acprice = value;
        }

        /**
         * Obtient la valeur de la propriété acprocurementdoc.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getACPROCUREMENTDOC() {
            return acprocurementdoc;
        }

        /**
         * Définit la valeur de la propriété acprocurementdoc.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setACPROCUREMENTDOC(Empty value) {
            this.acprocurementdoc = value;
        }

    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "eaEsat",
            "siae",
            "eess"
    })
    @ToString
    @EqualsAndHashCode
    public static class MarcheReserve {

        @XmlElement(name = "EA_ESAT")
        protected Empty eaEsat;
        @XmlElement(name = "SIAE")
        protected Empty siae;
        @XmlElement(name = "EESS")
        protected Empty eess;

        public Empty getEaEsat() {
            return eaEsat;
        }

        public void setEaEsat(Empty eaEsat) {
            this.eaEsat = eaEsat;
        }

        public Empty getSiae() {
            return siae;
        }

        public void setSiae(Empty siae) {
            this.siae = siae;
        }

        public Empty getEess() {
            return eess;
        }

        public void setEess(Empty eess) {
            this.eess = eess;
        }
    }
}
