//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.math.BigInteger;


/**
 * <p>Classe Java pour duration_m_y complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="duration_m_y"&gt;
 *   &lt;simpleContent&gt;
 *     &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;duration_value_2d"&gt;
 *       &lt;attribute name="TYPE" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}duration_unit_m_y" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "duration_m_y", propOrder = {
        "value"
})
@ToString
@EqualsAndHashCode
public class DurationMY {

    @XmlValue
    protected BigInteger value;
    @XmlAttribute(name = "TYPE", required = true)
    protected DurationUnitMY type;

    /**
     * Format 99
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public BigInteger getValue() {
        return value;
    }

    /**
     * Définit la valeur de la propriété value.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public void setValue(BigInteger value) {
        this.value = value;
    }

    /**
     * Obtient la valeur de la propriété type.
     *
     * @return possible object is
     * {@link DurationUnitMY }
     */
    public DurationUnitMY getTYPE() {
        return type;
    }

    /**
     * Définit la valeur de la propriété type.
     *
     * @param value allowed object is
     *              {@link DurationUnitMY }
     */
    public void setTYPE(DurationUnitMY value) {
        this.type = value;
    }

}
