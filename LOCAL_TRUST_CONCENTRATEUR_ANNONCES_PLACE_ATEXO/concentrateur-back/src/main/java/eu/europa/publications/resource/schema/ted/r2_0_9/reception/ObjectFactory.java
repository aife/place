//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import eu.europa.publications.resource.schema.ted._2021.nuts.Nuts;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import java.math.BigInteger;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the eu.europa.publications.resource.schema.ted.r2_0_9.reception package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _LANGUAGE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "LANGUAGE");
    private final static QName _COUNTRY_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "COUNTRY");
    private final static QName _CPVCODE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CPV_CODE");
    private final static QName _CPVSUPPLEMENTARYCODE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CPV_SUPPLEMENTARY_CODE");
    private final static QName _NOTICEUUID_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NOTICE_UUID");
    private final static QName _DATEEXPECTEDPUBLICATION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "DATE_EXPECTED_PUBLICATION");
    private final static QName _ESENDERLOGIN_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "ESENDER_LOGIN");
    private final static QName _CUSTOMERLOGIN_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CUSTOMER_LOGIN");
    private final static QName _NODOCEXT_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NO_DOC_EXT");
    private final static QName _FT_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "FT");
    private final static QName _P_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "P");
    private final static QName _VALESTIMATEDTOTAL_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "VAL_ESTIMATED_TOTAL");
    private final static QName _VALOBJECT_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "VAL_OBJECT");
    private final static QName _VALRANGEOBJECT_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "VAL_RANGE_OBJECT");
    private final static QName _VALTOTAL_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "VAL_TOTAL");
    private final static QName _VALRANGETOTAL_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "VAL_RANGE_TOTAL");
    private final static QName _DATESTART_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "DATE_START");
    private final static QName _DATEEND_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "DATE_END");
    private final static QName _OFFICIALNAME_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "OFFICIALNAME");
    private final static QName _CODE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CODE");
    private final static QName _LIBELLE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "LIBELLE");
    private final static QName _NATIONALID_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NATIONALID");
    private final static QName _SIRET_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "SIRET");
    private final static QName _ADDRESS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "ADDRESS");
    private final static QName _POSTALCODE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "POSTAL_CODE");
    private final static QName _TOWN_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "TOWN");
    private final static QName _CONTACTPOINT_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CONTACT_POINT");
    private final static QName _PHONE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PHONE");
    private final static QName _FAX_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "FAX");
    private final static QName _EMAIL_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "E_MAIL");
    private final static QName _URL_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "URL");
    private final static QName _URLBUYER_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "URL_BUYER");
    private final static QName _URLGENERAL_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "URL_GENERAL");
    private final static QName _LOTNO_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "LOT_NO");
    private final static QName _LOTALL_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "LOT_ALL");
    private final static QName _LOTONEONLY_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "LOT_ONE_ONLY");
    private final static QName _LOTMAXNUMBER_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "LOT_MAX_NUMBER");
    private final static QName _LOTMAXONETENDERER_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "LOT_MAX_ONE_TENDERER");
    private final static QName _LOTCOMBININGCONTRACTRIGHT_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "LOT_COMBINING_CONTRACT_RIGHT");
    private final static QName _TRANCHE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "TRANCHE");
    private final static QName _NOTRANCHE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NO_TRANCHE");
    private final static QName _LOTDIVISION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "LOT_DIVISION");
    private final static QName _NOLOTDIVISION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NO_LOT_DIVISION");
    private final static QName _DPROCCOMPETITIVEDIALOGUE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_PROC_COMPETITIVE_DIALOGUE");
    private final static QName _DPROCNEGOTIATEDPRIORCALLCOMPETITION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_PROC_NEGOTIATED_PRIOR_CALL_COMPETITION");
    private final static QName _DPROCOPEN_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_PROC_OPEN");
    private final static QName _DPROCRESTRICTED_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_PROC_RESTRICTED");
    private final static QName _DNOTENDERSREQUESTS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_NO_TENDERS_REQUESTS");
    private final static QName _DALLTENDERS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_ALL_TENDERS");
    private final static QName _DPERIODSINCOMPATIBLE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_PERIODS_INCOMPATIBLE");
    private final static QName _DPURERESEARCH_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_PURE_RESEARCH");
    private final static QName _DTECHNICAL_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_TECHNICAL");
    private final static QName _DARTISTIC_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_ARTISTIC");
    private final static QName _DEXCLUSIVERIGHT_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_EXCLUSIVE_RIGHT");
    private final static QName _DPROTECTRIGHTS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_PROTECT_RIGHTS");
    private final static QName _DOTHERSERVICES_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_OTHER_SERVICES");
    private final static QName _DMARITIMESERVICES_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_MARITIME_SERVICES");
    private final static QName _DEXTREMEURGENCY_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_EXTREME_URGENCY");
    private final static QName _DADDDELIVERIESORDERED_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_ADD_DELIVERIES_ORDERED");
    private final static QName _DREPETITIONEXISTING_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_REPETITION_EXISTING");
    private final static QName _DCONTRACTAWARDEDDESIGNCONTEST_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_CONTRACT_AWARDED_DESIGN_CONTEST");
    private final static QName _DCOMMODITYMARKET_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_COMMODITY_MARKET");
    private final static QName _DFROMWINDINGPROVIDER_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_FROM_WINDING_PROVIDER");
    private final static QName _DFROMLIQUIDATORCREDITOR_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_FROM_LIQUIDATOR_CREDITOR");
    private final static QName _DBARGAINPURCHASE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_BARGAIN_PURCHASE");
    private final static QName _DSERVICESLISTED_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_SERVICES_LISTED");
    private final static QName _DOUTSIDESCOPE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_OUTSIDE_SCOPE");
    private final static QName _DJUSTIFICATION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "D_JUSTIFICATION");
    private final static QName _LEGALBASISOTHER_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "LEGAL_BASIS_OTHER");
    private final static QName _ADDRESSCONTRACTINGBODY_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "ADDRESS_CONTRACTING_BODY");
    private final static QName _OFFRE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "OFFRE");
    private final static QName _ADDRESSCONTRACTINGBODYADDITIONAL_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "ADDRESS_CONTRACTING_BODY_ADDITIONAL");
    private final static QName _JOINTPROCUREMENTINVOLVED_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "JOINT_PROCUREMENT_INVOLVED");
    private final static QName _PROCUREMENTLAW_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PROCUREMENT_LAW");
    private final static QName _CENTRALPURCHASING_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CENTRAL_PURCHASING");
    private final static QName _DOCUMENTFULL_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "DOCUMENT_FULL");
    private final static QName _DOCUMENTRESTRICTED_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "DOCUMENT_RESTRICTED");
    private final static QName _URLDOCUMENT_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "URL_DOCUMENT");
    private final static QName _ADDRESSFURTHERINFOIDEM_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "ADDRESS_FURTHER_INFO_IDEM");
    private final static QName _ADDRESSFURTHERINFO_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "ADDRESS_FURTHER_INFO");
    private final static QName _URLPARTICIPATION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "URL_PARTICIPATION");
    private final static QName _ADDRESSPARTICIPATIONIDEM_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "ADDRESS_PARTICIPATION_IDEM");
    private final static QName _ADDRESSPARTICIPATION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "ADDRESS_PARTICIPATION");
    private final static QName _URLTOOL_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "URL_TOOL");
    private final static QName _CATYPE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CA_TYPE");
    private final static QName _CATYPEOTHER_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CA_TYPE_OTHER");
    private final static QName _CAACTIVITY_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CA_ACTIVITY");
    private final static QName _CAACTIVITYOTHER_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CA_ACTIVITY_OTHER");
    private final static QName _CEACTIVITY_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CE_ACTIVITY");
    private final static QName _CEACTIVITYOTHER_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CE_ACTIVITY_OTHER");
    private final static QName _TITLE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "TITLE");
    private final static QName _REFERENCENUMBER_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "REFERENCE_NUMBER");
    private final static QName _GROUPEMENT_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "GROUPEMENT");
    private final static QName _CPVADDITIONAL_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CPV_ADDITIONAL");
    private final static QName _CPVMAIN_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CPV_MAIN");
    private final static QName _LIEUEXECUTION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "LIEU_EXECUTION");
    private final static QName _TYPECONTRACT_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "TYPE_CONTRACT");
    private final static QName _SHORTDESCR_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "SHORT_DESCR");
    private final static QName _MAINSITE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "MAIN_SITE");
    private final static QName _ACCRITERION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "AC_CRITERION");
    private final static QName _ACWEIGHTING_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "AC_WEIGHTING");
    private final static QName _ACCOST_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "AC_COST");
    private final static QName _ACQUALITY_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "AC_QUALITY");
    private final static QName _ACPROCUREMENTDOC_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "AC_PROCUREMENT_DOC");
    private final static QName _RENEWAL_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "RENEWAL");
    private final static QName _RENEWALDESCR_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "RENEWAL_DESCR");
    private final static QName _NORENEWAL_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NO_RENEWAL");
    private final static QName _NBENVISAGEDCANDIDATE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NB_ENVISAGED_CANDIDATE");
    private final static QName _NBMINLIMITCANDIDATE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NB_MIN_LIMIT_CANDIDATE");
    private final static QName _NBMAXLIMITCANDIDATE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NB_MAX_LIMIT_CANDIDATE");
    private final static QName _CRITERIACANDIDATE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CRITERIA_CANDIDATE");
    private final static QName _ACCEPTEDVARIANTS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "ACCEPTED_VARIANTS");
    private final static QName _NOACCEPTEDVARIANTS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NO_ACCEPTED_VARIANTS");
    private final static QName _OPTIONS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "OPTIONS");
    private final static QName _OPTIONSDESCR_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "OPTIONS_DESCR");
    private final static QName _NOOPTIONS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NO_OPTIONS");
    private final static QName _ECATALOGUEREQUIRED_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "ECATALOGUE_REQUIRED");
    private final static QName _EUPROGRRELATED_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "EU_PROGR_RELATED");
    private final static QName _NOEUPROGRRELATED_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NO_EU_PROGR_RELATED");
    private final static QName _INFOADD_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "INFO_ADD");
    private final static QName _DATEPUBLICATIONNOTICE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "DATE_PUBLICATION_NOTICE");
    private final static QName _SUITABILITY_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "SUITABILITY");
    private final static QName _ECONOMICCRITERIADOC_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "ECONOMIC_CRITERIA_DOC");
    private final static QName _ECONOMICFINANCIALINFO_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "ECONOMIC_FINANCIAL_INFO");
    private final static QName _ECONOMICFINANCIALMINLEVEL_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "ECONOMIC_FINANCIAL_MIN_LEVEL");
    private final static QName _TECHNICALCRITERIADOC_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "TECHNICAL_CRITERIA_DOC");
    private final static QName _TECHNICALPROFESSIONALINFO_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "TECHNICAL_PROFESSIONAL_INFO");
    private final static QName _TECHNICALPROFESSIONALMINLEVEL_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "TECHNICAL_PROFESSIONAL_MIN_LEVEL");
    private final static QName _RULESCRITERIA_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "RULES_CRITERIA");
    private final static QName _RESTRICTEDSHELTEREDWORKSHOP_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "RESTRICTED_SHELTERED_WORKSHOP");
    private final static QName _RESTRICTEDSHELTEREDPROGRAM_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "RESTRICTED_SHELTERED_PROGRAM");
    private final static QName _RESERVEDORGANISATIONSSERVICEMISSION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "RESERVED_ORGANISATIONS_SERVICE_MISSION");
    private final static QName _DEPOSITGUARANTEEREQUIRED_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "DEPOSIT_GUARANTEE_REQUIRED");
    private final static QName _MAINFINANCINGCONDITION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "MAIN_FINANCING_CONDITION");
    private final static QName _LEGALFORM_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "LEGAL_FORM");
    private final static QName _CRITERIASELECTION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CRITERIA_SELECTION");
    private final static QName _PARTICULARPROFESSION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PARTICULAR_PROFESSION");
    private final static QName _REFERENCETOLAW_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "REFERENCE_TO_LAW");
    private final static QName _PERFORMANCECONDITIONS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PERFORMANCE_CONDITIONS");
    private final static QName _PERFORMANCESTAFFQUALIFICATION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PERFORMANCE_STAFF_QUALIFICATION");
    private final static QName _PTAWARDCONTRACTWITHPRIORPUBLICATION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_AWARD_CONTRACT_WITH_PRIOR_PUBLICATION");
    private final static QName _PTAWARDCONTRACTWITHOUTCALL_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_AWARD_CONTRACT_WITHOUT_CALL");
    private final static QName _PTAWARDCONTRACTWITHOUTPUBLICATION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_AWARD_CONTRACT_WITHOUT_PUBLICATION");
    private final static QName _PTCOMPETITIVEDIALOGUE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_COMPETITIVE_DIALOGUE");
    private final static QName _PTCOMPETITIVENEGOTIATION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_COMPETITIVE_NEGOTIATION");
    private final static QName _PTINNOVATIONPARTNERSHIP_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_INNOVATION_PARTNERSHIP");
    private final static QName _PTINVOLVINGNEGOTIATION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_INVOLVING_NEGOTIATION");
    private final static QName _PTNEGOTIATEDWITHPRIORCALL_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_NEGOTIATED_WITH_PRIOR_CALL");
    private final static QName _PTNEGOTIATEDWITHOUTPUBLICATION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_NEGOTIATED_WITHOUT_PUBLICATION");
    private final static QName _PTOPEN_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_OPEN");
    private final static QName _PTRESTRICTED_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_RESTRICTED");
    private final static QName _PT01AOO_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_01_AOO");
    private final static QName _PT03MAPA_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_03_MAPA");
    private final static QName _PT04Concours_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_04_Concours");
    private final static QName _PT12Autre_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_12_Autre");
    private final static QName _PT13AccordCadreSelection_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_13_AccordCadreSelection");
    private final static QName _PT15SAD_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_15_SAD");
    private final static QName _PT08MarcheNegocie_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_08_MarcheNegocie");
    private final static QName _PT18PCN_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_18_PCN");
    private final static QName _PT06DialogueCompetitif_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_06_DialogueCompetitif");
    private final static QName _PT02AOR_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_02_AOR");
    private final static QName _PT05ConcoursRestreint_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_05_ConcoursRestreint");
    private final static QName _PT14MarcheSubsequent_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_14_MarcheSubsequent");
    private final static QName _PT16MarcheSpecifique_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PT_16_MarcheSpecifique");
    private final static QName _ACCELERATEDPROC_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "ACCELERATED_PROC");
    private final static QName _NBPARTICIPANTS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NB_PARTICIPANTS");
    private final static QName _JUSTIFICATION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "JUSTIFICATION");
    private final static QName _SEVERALOPERATORS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "SEVERAL_OPERATORS");
    private final static QName _SINGLEOPERATOR_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "SINGLE_OPERATOR");
    private final static QName _PO_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PO");
    private final static QName _PR_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PR");
    private final static QName _DPS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "DPS");
    private final static QName _CRITERIAEVALUATION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CRITERIA_EVALUATION");
    private final static QName _REDUCTIONRECOURSE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "REDUCTION_RECOURSE");
    private final static QName _RIGHTCONTRACTINITIALTENDERS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "RIGHT_CONTRACT_INITIAL_TENDERS");
    private final static QName _EAUCTIONUSED_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "EAUCTION_USED");
    private final static QName _INFOADDEAUCTION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "INFO_ADD_EAUCTION");
    private final static QName _CONTRACTCOVEREDGPA_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CONTRACT_COVERED_GPA");
    private final static QName _NOCONTRACTCOVEREDGPA_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NO_CONTRACT_COVERED_GPA");
    private final static QName _URLNATIONALPROCEDURE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "URL_NATIONAL_PROCEDURE");
    private final static QName _MAINFEATURESAWARD_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "MAIN_FEATURES_AWARD");
    private final static QName _NOTICENUMBEROJ_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NOTICE_NUMBER_OJ");
    private final static QName _DATERECEIPTTENDERS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "DATE_RECEIPT_TENDERS");
    private final static QName _TIMERECEIPTTENDERS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "TIME_RECEIPT_TENDERS");
    private final static QName _ATTRIBSANSNEGO_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "ATTRIB_SANS_NEGO");
    private final static QName _NOATTRIBSANSNEGO_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NO_ATTRIB_SANS_NEGO");
    private final static QName _VISITEOBLIGATOIRE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "VISITE_OBLIGATOIRE");
    private final static QName _NOVISITEOBLIGATOIRE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NO_VISITE_OBLIGATOIRE");
    private final static QName _DATEDISPATCHINVITATIONS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "DATE_DISPATCH_INVITATIONS");
    private final static QName _NBCANDIDATSMAX_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NB_CANDIDATS_MAX");
    private final static QName _DATEAWARDSCHEDULED_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "DATE_AWARD_SCHEDULED");
    private final static QName _OPENINGCONDITION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "OPENING_CONDITION");
    private final static QName _TERMINATIONDPS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "TERMINATION_DPS");
    private final static QName _TERMINATIONPIN_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "TERMINATION_PIN");
    private final static QName _CONTRACTNO_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CONTRACT_NO");
    private final static QName _PROCUREMENTUNSUCCESSFUL_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PROCUREMENT_UNSUCCESSFUL");
    private final static QName _NOAWARDEDCONTRACT_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NO_AWARDED_CONTRACT");
    private final static QName _DATECONCLUSIONCONTRACT_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "DATE_CONCLUSION_CONTRACT");
    private final static QName _NBTENDERSRECEIVED_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NB_TENDERS_RECEIVED");
    private final static QName _NBTENDERSRECEIVEDSME_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NB_TENDERS_RECEIVED_SME");
    private final static QName _NBTENDERSRECEIVEDOTHEREU_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NB_TENDERS_RECEIVED_OTHER_EU");
    private final static QName _NBTENDERSRECEIVEDNONEU_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NB_TENDERS_RECEIVED_NON_EU");
    private final static QName _NBTENDERSRECEIVEDEMEANS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NB_TENDERS_RECEIVED_EMEANS");
    private final static QName _ADDRESSCONTRACTOR_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "ADDRESS_CONTRACTOR");
    private final static QName _SME_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "SME");
    private final static QName _NOSME_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NO_SME");
    private final static QName _AWARDEDTOGROUP_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "AWARDED_TO_GROUP");
    private final static QName _NOAWARDEDTOGROUP_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NO_AWARDED_TO_GROUP");
    private final static QName _LIKELYSUBCONTRACTED_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "LIKELY_SUBCONTRACTED");
    private final static QName _AWARDEDSUBCONTRACTING_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "AWARDED_SUBCONTRACTING");
    private final static QName _PCTRANGESHARESUBCONTRACTING_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PCT_RANGE_SHARE_SUBCONTRACTING");
    private final static QName _RECURRENTPROCUREMENT_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "RECURRENT_PROCUREMENT");
    private final static QName _ESTIMATEDTIMING_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "ESTIMATED_TIMING");
    private final static QName _NORECURRENTPROCUREMENT_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NO_RECURRENT_PROCUREMENT");
    private final static QName _EORDERING_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "EORDERING");
    private final static QName _EINVOICING_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "EINVOICING");
    private final static QName _EPAYMENT_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "EPAYMENT");
    private final static QName _ADDRESSREVIEWBODY_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "ADDRESS_REVIEW_BODY");
    private final static QName _ADDRESSMEDIATIONBODY_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "ADDRESS_MEDIATION_BODY");
    private final static QName _REVIEWPROCEDURE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "REVIEW_PROCEDURE");
    private final static QName _ADDRESSREVIEWINFO_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "ADDRESS_REVIEW_INFO");
    private final static QName _DATEDISPATCHNOTICE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "DATE_DISPATCH_NOTICE");
    private final static QName _ADDITIONALNEED_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "ADDITIONAL_NEED");
    private final static QName _UNFORESEENCIRCUMSTANCE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "UNFORESEEN_CIRCUMSTANCE");
    private final static QName _VALTOTALBEFORE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "VAL_TOTAL_BEFORE");
    private final static QName _VALTOTALAFTER_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "VAL_TOTAL_AFTER");
    private final static QName _TEDESENDERS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "TED_ESENDERS");
    private final static QName _F012014LEGALBASIS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "LEGAL_BASIS");
    private final static QName _F012014NOTICE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NOTICE");
    private final static QName _F012014CONTRACTINGBODY_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CONTRACTING_BODY");
    private final static QName _F012014OBJECTCONTRACT_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "OBJECT_CONTRACT");
    private final static QName _F012014LEFTI_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "LEFTI");
    private final static QName _F012014PROCEDURE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PROCEDURE");
    private final static QName _F012014COMPLEMENTARYINFO_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "COMPLEMENTARY_INFO");
    private final static QName _F032014AWARDCONTRACT_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "AWARD_CONTRACT");
    private final static QName _F132014RESULTS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "RESULTS");
    private final static QName _F142014CHANGES_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CHANGES");
    private final static QName _F202014MODIFICATIONSCONTRACT_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "MODIFICATIONS_CONTRACT");
    private final static QName _ObjectF25AC_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "AC");
    private final static QName _ObjectF25DURATION_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "DURATION");
    private final static QName _ObjectF22QS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "QS");
    private final static QName _ObjectContractF25CALCULATIONMETHOD_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CALCULATION_METHOD");
    private final static QName _ObjectContractF25OBJECTDESCR_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "OBJECT_DESCR");
    private final static QName _ModificationsF20DESCRIPTIONPROCUREMENTVALUES_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "VALUES");
    private final static QName _ModificationsF20DESCRIPTIONPROCUREMENTCONTRACTORS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CONTRACTORS");
    private final static QName _ChangesF14MODIFICATIONORIGINAL_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "MODIFICATION_ORIGINAL");
    private final static QName _ChangesF14PUBLICATIONTED_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PUBLICATION_TED");
    private final static QName _ChangesF14CHANGE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "CHANGE");
    private final static QName _ProcedureF12NBMINPARTICIPANTS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NB_MIN_PARTICIPANTS");
    private final static QName _ProcedureF12NBMAXPARTICIPANTS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NB_MAX_PARTICIPANTS");
    private final static QName _ProcedureF12PARTICIPANTNAME_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PARTICIPANT_NAME");
    private final static QName _ProcedureF12PRIZEAWARDED_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "PRIZE_AWARDED");
    private final static QName _ProcedureF12NUMBERVALUEPRIZE_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NUMBER_VALUE_PRIZE");
    private final static QName _ProcedureF12NOPRIZEAWARDED_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NO_PRIZE_AWARDED");
    private final static QName _ProcedureF12DETAILSPAYMENT_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "DETAILS_PAYMENT");
    private final static QName _ProcedureF12FOLLOWUPCONTRACTS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "FOLLOW_UP_CONTRACTS");
    private final static QName _ProcedureF12NOFOLLOWUPCONTRACTS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NO_FOLLOW_UP_CONTRACTS");
    private final static QName _ProcedureF12DECISIONBINDINGCONTRACTING_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "DECISION_BINDING_CONTRACTING");
    private final static QName _ProcedureF12NODECISIONBINDINGCONTRACTING_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NO_DECISION_BINDING_CONTRACTING");
    private final static QName _ProcedureF12MEMBERNAME_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "MEMBER_NAME");
    private final static QName _AwardContractF06AWARDEDCONTRACTCOUNTRYORIGINCOMMUNITYORIGIN_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "COMMUNITY_ORIGIN");
    private final static QName _AwardContractF06AWARDEDCONTRACTCOUNTRYORIGINNONCOMMUNITYORIGIN_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "NON_COMMUNITY_ORIGIN");
    private final static QName _ProcedureF03FRAMEWORK_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "FRAMEWORK");
    private final static QName _ProcedureF02DPSADDITIONALPURCHASERS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "DPS_ADDITIONAL_PURCHASERS");
    private final static QName _ProcedureF02DATETENDERVALID_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "DATE_TENDER_VALID");
    private final static QName _ProcedureF02DURATIONTENDERVALID_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", "DURATION_TENDER_VALID");
    private final static QName _NUTS_QNAME = new QName("http://publications.europa.eu/resource/schema/ted/2016/nuts", "NUTS");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eu.europa.publications.resource.schema.ted.r2_0_9.reception
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LANGUAGES }
     */
    public LANGUAGES createLANGUAGES() {
        return new LANGUAGES();
    }

    /**
     * Create an instance of {@link PROCUREMENTDISCONTINUED }
     */
    public PROCUREMENTDISCONTINUED createPROCUREMENTDISCONTINUED() {
        return new PROCUREMENTDISCONTINUED();
    }

    /**
     * Create an instance of {@link Sender }
     */
    public Sender createSender() {
        return new Sender();
    }

    /**
     * Create an instance of {@link Sender.CONTACT }
     */
    public Sender.CONTACT createSenderCONTACT() {
        return new Sender.CONTACT();
    }

    /**
     * Create an instance of {@link ObjectF25 }
     */
    public ObjectF25 createObjectF25() {
        return new ObjectF25();
    }

    /**
     * Create an instance of {@link ObjectF24 }
     */
    public ObjectF24 createObjectF24() {
        return new ObjectF24();
    }

    /**
     * Create an instance of {@link ObjectF22 }
     */
    public ObjectF22 createObjectF22() {
        return new ObjectF22();
    }

    /**
     * Create an instance of {@link AnnexD2F22 }
     */
    public AnnexD2F22 createAnnexD2F22() {
        return new AnnexD2F22();
    }

    /**
     * Create an instance of {@link AnnexD1F21 }
     */
    public AnnexD1F21 createAnnexD1F21() {
        return new AnnexD1F21();
    }

    /**
     * Create an instance of {@link ObjectF15 }
     */
    public ObjectF15 createObjectF15() {
        return new ObjectF15();
    }

    /**
     * Create an instance of {@link ObjectF15 .DIRECTIVE200981EC }
     */
    public ObjectF15.DIRECTIVE200981EC createObjectF15DIRECTIVE200981EC() {
        return new ObjectF15.DIRECTIVE200981EC();
    }

    /**
     * Create an instance of {@link ObjectF15 .DIRECTIVE201425EU }
     */
    public ObjectF15.DIRECTIVE201425EU createObjectF15DIRECTIVE201425EU() {
        return new ObjectF15.DIRECTIVE201425EU();
    }

    /**
     * Create an instance of {@link ObjectF15 .DIRECTIVE201424EU }
     */
    public ObjectF15.DIRECTIVE201424EU createObjectF15DIRECTIVE201424EU() {
        return new ObjectF15.DIRECTIVE201424EU();
    }

    /**
     * Create an instance of {@link ObjectF15 .DIRECTIVE201423EU }
     */
    public ObjectF15.DIRECTIVE201423EU createObjectF15DIRECTIVE201423EU() {
        return new ObjectF15.DIRECTIVE201423EU();
    }

    /**
     * Create an instance of {@link ObjectF07 }
     */
    public ObjectF07 createObjectF07() {
        return new ObjectF07();
    }

    /**
     * Create an instance of {@link ObjectF06 }
     */
    public ObjectF06 createObjectF06() {
        return new ObjectF06();
    }

    /**
     * Create an instance of {@link ObjectF05 }
     */
    public ObjectF05 createObjectF05() {
        return new ObjectF05();
    }

    /**
     * Create an instance of {@link ObjectF04 }
     */
    public ObjectF04 createObjectF04() {
        return new ObjectF04();
    }

    /**
     * Create an instance of {@link ObjectF03 }
     */
    public ObjectF03 createObjectF03() {
        return new ObjectF03();
    }

    /**
     * Create an instance of {@link ObjectF02 }
     */
    public ObjectF02 createObjectF02() {
        return new ObjectF02();
    }

    /**
     * Create an instance of {@link ObjectF01 }
     */
    public ObjectF01 createObjectF01() {
        return new ObjectF01();
    }

    /**
     * Create an instance of {@link AnnexD4 }
     */
    public AnnexD4 createAnnexD4() {
        return new AnnexD4();
    }

    /**
     * Create an instance of {@link AnnexD3 }
     */
    public AnnexD3 createAnnexD3() {
        return new AnnexD3();
    }

    /**
     * Create an instance of {@link AnnexD2 }
     */
    public AnnexD2 createAnnexD2() {
        return new AnnexD2();
    }

    /**
     * Create an instance of {@link AnnexD1 }
     */
    public AnnexD1 createAnnexD1() {
        return new AnnexD1();
    }

    /**
     * Create an instance of {@link AwardContractMove }
     */
    public AwardContractMove createAwardContractMove() {
        return new AwardContractMove();
    }

    /**
     * Create an instance of {@link AwardContractMove.AWARDEDCONTRACT }
     */
    public AwardContractMove.AWARDEDCONTRACT createAwardContractMoveAWARDEDCONTRACT() {
        return new AwardContractMove.AWARDEDCONTRACT();
    }

    /**
     * Create an instance of {@link AwardContractMove.AWARDEDCONTRACT.CONTRACTORS }
     */
    public AwardContractMove.AWARDEDCONTRACT.CONTRACTORS createAwardContractMoveAWARDEDCONTRACTCONTRACTORS() {
        return new AwardContractMove.AWARDEDCONTRACT.CONTRACTORS();
    }

    /**
     * Create an instance of {@link ObjectContractMove }
     */
    public ObjectContractMove createObjectContractMove() {
        return new ObjectContractMove();
    }

    /**
     * Create an instance of {@link ObjectContractMove.OBJECTDESCR }
     */
    public ObjectContractMove.OBJECTDESCR createObjectContractMoveOBJECTDESCR() {
        return new ObjectContractMove.OBJECTDESCR();
    }

    /**
     * Create an instance of {@link AwardContractF25 }
     */
    public AwardContractF25 createAwardContractF25() {
        return new AwardContractF25();
    }

    /**
     * Create an instance of {@link AwardContractF25 .AWARDEDCONTRACT }
     */
    public AwardContractF25.AWARDEDCONTRACT createAwardContractF25AWARDEDCONTRACT() {
        return new AwardContractF25.AWARDEDCONTRACT();
    }

    /**
     * Create an instance of {@link AwardContractF25 .AWARDEDCONTRACT.CONTRACTORS }
     */
    public AwardContractF25.AWARDEDCONTRACT.CONTRACTORS createAwardContractF25AWARDEDCONTRACTCONTRACTORS() {
        return new AwardContractF25.AWARDEDCONTRACT.CONTRACTORS();
    }

    /**
     * Create an instance of {@link AwardContractF23 }
     */
    public AwardContractF23 createAwardContractF23() {
        return new AwardContractF23();
    }

    /**
     * Create an instance of {@link AwardContractF23 .AWARDEDCONTRACT }
     */
    public AwardContractF23.AWARDEDCONTRACT createAwardContractF23AWARDEDCONTRACT() {
        return new AwardContractF23.AWARDEDCONTRACT();
    }

    /**
     * Create an instance of {@link AwardContractF23 .AWARDEDCONTRACT.CONTRACTORS }
     */
    public AwardContractF23.AWARDEDCONTRACT.CONTRACTORS createAwardContractF23AWARDEDCONTRACTCONTRACTORS() {
        return new AwardContractF23.AWARDEDCONTRACT.CONTRACTORS();
    }

    /**
     * Create an instance of {@link AwardContractF22 }
     */
    public AwardContractF22 createAwardContractF22() {
        return new AwardContractF22();
    }

    /**
     * Create an instance of {@link AwardContractF22 .AWARDEDCONTRACT }
     */
    public AwardContractF22.AWARDEDCONTRACT createAwardContractF22AWARDEDCONTRACT() {
        return new AwardContractF22.AWARDEDCONTRACT();
    }

    /**
     * Create an instance of {@link ProcedureF22 }
     */
    public ProcedureF22 createProcedureF22() {
        return new ProcedureF22();
    }

    /**
     * Create an instance of {@link ObjectContractF22 }
     */
    public ObjectContractF22 createObjectContractF22() {
        return new ObjectContractF22();
    }

    /**
     * Create an instance of {@link AwardContractF21 }
     */
    public AwardContractF21 createAwardContractF21() {
        return new AwardContractF21();
    }

    /**
     * Create an instance of {@link AwardContractF21 .AWARDEDCONTRACT }
     */
    public AwardContractF21.AWARDEDCONTRACT createAwardContractF21AWARDEDCONTRACT() {
        return new AwardContractF21.AWARDEDCONTRACT();
    }

    /**
     * Create an instance of {@link ProcedureF21 }
     */
    public ProcedureF21 createProcedureF21() {
        return new ProcedureF21();
    }

    /**
     * Create an instance of {@link ModificationsF20 }
     */
    public ModificationsF20 createModificationsF20() {
        return new ModificationsF20();
    }

    /**
     * Create an instance of {@link ModificationsF20 .INFOMODIFICATIONS }
     */
    public ModificationsF20.INFOMODIFICATIONS createModificationsF20INFOMODIFICATIONS() {
        return new ModificationsF20.INFOMODIFICATIONS();
    }

    /**
     * Create an instance of {@link ModificationsF20 .DESCRIPTIONPROCUREMENT }
     */
    public ModificationsF20.DESCRIPTIONPROCUREMENT createModificationsF20DESCRIPTIONPROCUREMENT() {
        return new ModificationsF20.DESCRIPTIONPROCUREMENT();
    }

    /**
     * Create an instance of {@link AwardContractF20 }
     */
    public AwardContractF20 createAwardContractF20() {
        return new AwardContractF20();
    }

    /**
     * Create an instance of {@link AwardContractF20 .AWARDEDCONTRACT }
     */
    public AwardContractF20.AWARDEDCONTRACT createAwardContractF20AWARDEDCONTRACT() {
        return new AwardContractF20.AWARDEDCONTRACT();
    }

    /**
     * Create an instance of {@link AwardContractF15 }
     */
    public AwardContractF15 createAwardContractF15() {
        return new AwardContractF15();
    }

    /**
     * Create an instance of {@link AwardContractF15 .AWARDEDCONTRACT }
     */
    public AwardContractF15.AWARDEDCONTRACT createAwardContractF15AWARDEDCONTRACT() {
        return new AwardContractF15.AWARDEDCONTRACT();
    }

    /**
     * Create an instance of {@link ProcedureF15 }
     */
    public ProcedureF15 createProcedureF15() {
        return new ProcedureF15();
    }

    /**
     * Create an instance of {@link ProcedureF15 .DIRECTIVE200981EC }
     */
    public ProcedureF15.DIRECTIVE200981EC createProcedureF15DIRECTIVE200981EC() {
        return new ProcedureF15.DIRECTIVE200981EC();
    }

    /**
     * Create an instance of {@link ProcedureF15 .DIRECTIVE201425EU }
     */
    public ProcedureF15.DIRECTIVE201425EU createProcedureF15DIRECTIVE201425EU() {
        return new ProcedureF15.DIRECTIVE201425EU();
    }

    /**
     * Create an instance of {@link ProcedureF15 .DIRECTIVE201424EU }
     */
    public ProcedureF15.DIRECTIVE201424EU createProcedureF15DIRECTIVE201424EU() {
        return new ProcedureF15.DIRECTIVE201424EU();
    }

    /**
     * Create an instance of {@link ProcedureF15 .DIRECTIVE201423EU }
     */
    public ProcedureF15.DIRECTIVE201423EU createProcedureF15DIRECTIVE201423EU() {
        return new ProcedureF15.DIRECTIVE201423EU();
    }

    /**
     * Create an instance of {@link ObjectContractF15 }
     */
    public ObjectContractF15 createObjectContractF15() {
        return new ObjectContractF15();
    }

    /**
     * Create an instance of {@link ChangesF14 }
     */
    public ChangesF14 createChangesF14() {
        return new ChangesF14();
    }

    /**
     * Create an instance of {@link ChangesF14 .CHANGE }
     */
    public ChangesF14.CHANGE createChangesF14CHANGE() {
        return new ChangesF14.CHANGE();
    }

    /**
     * Create an instance of {@link CiF14 }
     */
    public CiF14 createCiF14() {
        return new CiF14();
    }

    /**
     * Create an instance of {@link ResultsF13 }
     */
    public ResultsF13 createResultsF13() {
        return new ResultsF13();
    }

    /**
     * Create an instance of {@link ResultsF13 .AWARDEDPRIZE }
     */
    public ResultsF13.AWARDEDPRIZE createResultsF13AWARDEDPRIZE() {
        return new ResultsF13.AWARDEDPRIZE();
    }

    /**
     * Create an instance of {@link ResultsF13 .AWARDEDPRIZE.WINNERS }
     */
    public ResultsF13.AWARDEDPRIZE.WINNERS createResultsF13AWARDEDPRIZEWINNERS() {
        return new ResultsF13.AWARDEDPRIZE.WINNERS();
    }

    /**
     * Create an instance of {@link ResultsF13 .AWARDEDPRIZE.WINNERS.WINNER }
     */
    public ResultsF13.AWARDEDPRIZE.WINNERS.WINNER createResultsF13AWARDEDPRIZEWINNERSWINNER() {
        return new ResultsF13.AWARDEDPRIZE.WINNERS.WINNER();
    }

    /**
     * Create an instance of {@link ProcedureF13 }
     */
    public ProcedureF13 createProcedureF13() {
        return new ProcedureF13();
    }

    /**
     * Create an instance of {@link AwardContractF06 }
     */
    public AwardContractF06 createAwardContractF06() {
        return new AwardContractF06();
    }

    /**
     * Create an instance of {@link AwardContractF06 .AWARDEDCONTRACT }
     */
    public AwardContractF06.AWARDEDCONTRACT createAwardContractF06AWARDEDCONTRACT() {
        return new AwardContractF06.AWARDEDCONTRACT();
    }

    /**
     * Create an instance of {@link ObjectContractF06 }
     */
    public ObjectContractF06 createObjectContractF06() {
        return new ObjectContractF06();
    }

    /**
     * Create an instance of {@link ProcedureF05 }
     */
    public ProcedureF05 createProcedureF05() {
        return new ProcedureF05();
    }

    /**
     * Create an instance of {@link AwardContractF03 }
     */
    public AwardContractF03 createAwardContractF03() {
        return new AwardContractF03();
    }

    /**
     * Create an instance of {@link AwardContractF03 .AWARDEDCONTRACT }
     */
    public AwardContractF03.AWARDEDCONTRACT createAwardContractF03AWARDEDCONTRACT() {
        return new AwardContractF03.AWARDEDCONTRACT();
    }

    /**
     * Create an instance of {@link Language }
     */
    public Language createLanguage() {
        return new Language();
    }

    /**
     * Create an instance of {@link Country }
     */
    public Country createCountry() {
        return new Country();
    }

    /**
     * Create an instance of {@link CpvCodes }
     */
    public CpvCodes createCpvCodes() {
        return new CpvCodes();
    }

    /**
     * Create an instance of {@link CpvSupplementaryCodes }
     */
    public CpvSupplementaryCodes createCpvSupplementaryCodes() {
        return new CpvSupplementaryCodes();
    }

    /**
     * Create an instance of {@link Ft }
     */
    public Ft createFt() {
        return new Ft();
    }

    /**
     * Create an instance of {@link P }
     */
    public P createP() {
        return new P();
    }

    /**
     * Create an instance of {@link Val }
     */
    public Val createVal() {
        return new Val();
    }

    /**
     * Create an instance of {@link ValRange }
     */
    public ValRange createValRange() {
        return new ValRange();
    }

    /**
     * Create an instance of {@link Empty }
     */
    public Empty createEmpty() {
        return new Empty();
    }

    /**
     * Create an instance of {@link TextFtMultiLines }
     */
    public TextFtMultiLines createTextFtMultiLines() {
        return new TextFtMultiLines();
    }

    /**
     * Create an instance of {@link NoWorks }
     */
    public NoWorks createNoWorks() {
        return new NoWorks();
    }

    /**
     * Create an instance of {@link Services }
     */
    public Services createServices() {
        return new Services();
    }

    /**
     * Create an instance of {@link NoSupplies }
     */
    public NoSupplies createNoSupplies() {
        return new NoSupplies();
    }

    /**
     * Create an instance of {@link Supplies }
     */
    public Supplies createSupplies() {
        return new Supplies();
    }

    /**
     * Create an instance of {@link LEGALBASIS }
     */
    public LEGALBASIS createLEGALBASIS() {
        return new LEGALBASIS();
    }

    /**
     * Create an instance of {@link ContactContractingBody }
     */
    public ContactContractingBody createContactContractingBody() {
        return new ContactContractingBody();
    }

    /**
     * Create an instance of {@link Offre }
     */
    public Offre createOffre() {
        return new Offre();
    }

    /**
     * Create an instance of {@link CaType }
     */
    public CaType createCaType() {
        return new CaType();
    }

    /**
     * Create an instance of {@link CaActivity }
     */
    public CaActivity createCaActivity() {
        return new CaActivity();
    }

    /**
     * Create an instance of {@link CeActivity }
     */
    public CeActivity createCeActivity() {
        return new CeActivity();
    }

    /**
     * Create an instance of {@link TextFtSingleLine }
     */
    public TextFtSingleLine createTextFtSingleLine() {
        return new TextFtSingleLine();
    }

    /**
     * Create an instance of {@link Groupement }
     */
    public Groupement createGroupement() {
        return new Groupement();
    }

    /**
     * Create an instance of {@link CpvSet }
     */
    public CpvSet createCpvSet() {
        return new CpvSet();
    }

    /**
     * Create an instance of {@link TypeContract }
     */
    public TypeContract createTypeContract() {
        return new TypeContract();
    }

    /**
     * Create an instance of {@link AcDefinition }
     */
    public AcDefinition createAcDefinition() {
        return new AcDefinition();
    }

    /**
     * Create an instance of {@link ACPRICE }
     */
    public ACPRICE createACPRICE() {
        return new ACPRICE();
    }

    /**
     * Create an instance of {@link QUALIFICATION }
     */
    public QUALIFICATION createQUALIFICATION() {
        return new QUALIFICATION();
    }

    /**
     * Create an instance of {@link LANGUAGES.LANGUAGE }
     */
    public LANGUAGES.LANGUAGE createLANGUAGESLANGUAGE() {
        return new LANGUAGES.LANGUAGE();
    }

    /**
     * Create an instance of {@link CondForOpeningTenders }
     */
    public CondForOpeningTenders createCondForOpeningTenders() {
        return new CondForOpeningTenders();
    }

    /**
     * Create an instance of {@link NonPublished }
     */
    public NonPublished createNonPublished() {
        return new NonPublished();
    }

    /**
     * Create an instance of {@link PROCUREMENTDISCONTINUED.ESENDERLOGIN }
     */
    public PROCUREMENTDISCONTINUED.ESENDERLOGIN createPROCUREMENTDISCONTINUEDESENDERLOGIN() {
        return new PROCUREMENTDISCONTINUED.ESENDERLOGIN();
    }

    /**
     * Create an instance of {@link PROCUREMENTDISCONTINUED.CUSTOMERLOGIN }
     */
    public PROCUREMENTDISCONTINUED.CUSTOMERLOGIN createPROCUREMENTDISCONTINUEDCUSTOMERLOGIN() {
        return new PROCUREMENTDISCONTINUED.CUSTOMERLOGIN();
    }

    /**
     * Create an instance of {@link PROCUREMENTDISCONTINUED.NODOCEXT }
     */
    public PROCUREMENTDISCONTINUED.NODOCEXT createPROCUREMENTDISCONTINUEDNODOCEXT() {
        return new PROCUREMENTDISCONTINUED.NODOCEXT();
    }

    /**
     * Create an instance of {@link PROCUREMENTDISCONTINUED.ORIGINALOTHERMEANS }
     */
    public PROCUREMENTDISCONTINUED.ORIGINALOTHERMEANS createPROCUREMENTDISCONTINUEDORIGINALOTHERMEANS() {
        return new PROCUREMENTDISCONTINUED.ORIGINALOTHERMEANS();
    }

    /**
     * Create an instance of {@link DATEDISPATCHORIGINAL }
     */
    public DATEDISPATCHORIGINAL createDATEDISPATCHORIGINAL() {
        return new DATEDISPATCHORIGINAL();
    }

    /**
     * Create an instance of {@link NoAward }
     */
    public NoAward createNoAward() {
        return new NoAward();
    }

    /**
     * Create an instance of {@link ContactContractor }
     */
    public ContactContractor createContactContractor() {
        return new ContactContractor();
    }

    /**
     * Create an instance of {@link PrctRange }
     */
    public PrctRange createPrctRange() {
        return new PrctRange();
    }

    /**
     * Create an instance of {@link ContactReview }
     */
    public ContactReview createContactReview() {
        return new ContactReview();
    }

    /**
     * Create an instance of {@link F012014 }
     */
    public F012014 createF012014() {
        return new F012014();
    }

    /**
     * Create an instance of {@link LegalBasisF01 }
     */
    public LegalBasisF01 createLegalBasisF01() {
        return new LegalBasisF01();
    }

    /**
     * Create an instance of {@link NoticeF01 }
     */
    public NoticeF01 createNoticeF01() {
        return new NoticeF01();
    }

    /**
     * Create an instance of {@link BodyF01 }
     */
    public BodyF01 createBodyF01() {
        return new BodyF01();
    }

    /**
     * Create an instance of {@link ObjectContractF01 }
     */
    public ObjectContractF01 createObjectContractF01() {
        return new ObjectContractF01();
    }

    /**
     * Create an instance of {@link LeftiF01 }
     */
    public LeftiF01 createLeftiF01() {
        return new LeftiF01();
    }

    /**
     * Create an instance of {@link ProcedureF01 }
     */
    public ProcedureF01 createProcedureF01() {
        return new ProcedureF01();
    }

    /**
     * Create an instance of {@link CiF01 }
     */
    public CiF01 createCiF01() {
        return new CiF01();
    }

    /**
     * Create an instance of {@link F022014 }
     */
    public F022014 createF022014() {
        return new F022014();
    }

    /**
     * Create an instance of {@link LegalBasisF02 }
     */
    public LegalBasisF02 createLegalBasisF02() {
        return new LegalBasisF02();
    }

    /**
     * Create an instance of {@link BodyF02 }
     */
    public BodyF02 createBodyF02() {
        return new BodyF02();
    }

    /**
     * Create an instance of {@link ObjectContractF02 }
     */
    public ObjectContractF02 createObjectContractF02() {
        return new ObjectContractF02();
    }

    /**
     * Create an instance of {@link LeftiF02 }
     */
    public LeftiF02 createLeftiF02() {
        return new LeftiF02();
    }

    /**
     * Create an instance of {@link ProcedureF02 }
     */
    public ProcedureF02 createProcedureF02() {
        return new ProcedureF02();
    }

    /**
     * Create an instance of {@link CiF02 }
     */
    public CiF02 createCiF02() {
        return new CiF02();
    }

    /**
     * Create an instance of {@link F032014 }
     */
    public F032014 createF032014() {
        return new F032014();
    }

    /**
     * Create an instance of {@link LegalBasisF03 }
     */
    public LegalBasisF03 createLegalBasisF03() {
        return new LegalBasisF03();
    }

    /**
     * Create an instance of {@link BodyF03 }
     */
    public BodyF03 createBodyF03() {
        return new BodyF03();
    }

    /**
     * Create an instance of {@link ObjectContractF03 }
     */
    public ObjectContractF03 createObjectContractF03() {
        return new ObjectContractF03();
    }

    /**
     * Create an instance of {@link ProcedureF03 }
     */
    public ProcedureF03 createProcedureF03() {
        return new ProcedureF03();
    }

    /**
     * Create an instance of {@link CiF03 }
     */
    public CiF03 createCiF03() {
        return new CiF03();
    }

    /**
     * Create an instance of {@link F042014 }
     */
    public F042014 createF042014() {
        return new F042014();
    }

    /**
     * Create an instance of {@link LegalBasisF04 }
     */
    public LegalBasisF04 createLegalBasisF04() {
        return new LegalBasisF04();
    }

    /**
     * Create an instance of {@link NoticeF04 }
     */
    public NoticeF04 createNoticeF04() {
        return new NoticeF04();
    }

    /**
     * Create an instance of {@link BodyF04 }
     */
    public BodyF04 createBodyF04() {
        return new BodyF04();
    }

    /**
     * Create an instance of {@link ObjectContractF04 }
     */
    public ObjectContractF04 createObjectContractF04() {
        return new ObjectContractF04();
    }

    /**
     * Create an instance of {@link LeftiF04 }
     */
    public LeftiF04 createLeftiF04() {
        return new LeftiF04();
    }

    /**
     * Create an instance of {@link ProcedureF04 }
     */
    public ProcedureF04 createProcedureF04() {
        return new ProcedureF04();
    }

    /**
     * Create an instance of {@link CiF04 }
     */
    public CiF04 createCiF04() {
        return new CiF04();
    }

    /**
     * Create an instance of {@link F052014 }
     */
    public F052014 createF052014() {
        return new F052014();
    }

    /**
     * Create an instance of {@link LegalBasisF05 }
     */
    public LegalBasisF05 createLegalBasisF05() {
        return new LegalBasisF05();
    }

    /**
     * Create an instance of {@link BodyF05 }
     */
    public BodyF05 createBodyF05() {
        return new BodyF05();
    }

    /**
     * Create an instance of {@link ObjectContractF05 }
     */
    public ObjectContractF05 createObjectContractF05() {
        return new ObjectContractF05();
    }

    /**
     * Create an instance of {@link LeftiF05 }
     */
    public LeftiF05 createLeftiF05() {
        return new LeftiF05();
    }

    /**
     * Create an instance of {@link CiF05 }
     */
    public CiF05 createCiF05() {
        return new CiF05();
    }

    /**
     * Create an instance of {@link F062014 }
     */
    public F062014 createF062014() {
        return new F062014();
    }

    /**
     * Create an instance of {@link LegalBasisF06 }
     */
    public LegalBasisF06 createLegalBasisF06() {
        return new LegalBasisF06();
    }

    /**
     * Create an instance of {@link BodyF06 }
     */
    public BodyF06 createBodyF06() {
        return new BodyF06();
    }

    /**
     * Create an instance of {@link ProcedureF06 }
     */
    public ProcedureF06 createProcedureF06() {
        return new ProcedureF06();
    }

    /**
     * Create an instance of {@link CiF06 }
     */
    public CiF06 createCiF06() {
        return new CiF06();
    }

    /**
     * Create an instance of {@link F072014 }
     */
    public F072014 createF072014() {
        return new F072014();
    }

    /**
     * Create an instance of {@link LegalBasisF07 }
     */
    public LegalBasisF07 createLegalBasisF07() {
        return new LegalBasisF07();
    }

    /**
     * Create an instance of {@link NoticeF07 }
     */
    public NoticeF07 createNoticeF07() {
        return new NoticeF07();
    }

    /**
     * Create an instance of {@link BodyF07 }
     */
    public BodyF07 createBodyF07() {
        return new BodyF07();
    }

    /**
     * Create an instance of {@link ObjectContractF07 }
     */
    public ObjectContractF07 createObjectContractF07() {
        return new ObjectContractF07();
    }

    /**
     * Create an instance of {@link LeftiF07 }
     */
    public LeftiF07 createLeftiF07() {
        return new LeftiF07();
    }

    /**
     * Create an instance of {@link ProcedureF07 }
     */
    public ProcedureF07 createProcedureF07() {
        return new ProcedureF07();
    }

    /**
     * Create an instance of {@link CiF07 }
     */
    public CiF07 createCiF07() {
        return new CiF07();
    }

    /**
     * Create an instance of {@link F082014 }
     */
    public F082014 createF082014() {
        return new F082014();
    }

    /**
     * Create an instance of {@link LegalBasisF08 }
     */
    public LegalBasisF08 createLegalBasisF08() {
        return new LegalBasisF08();
    }

    /**
     * Create an instance of {@link BodyF08 }
     */
    public BodyF08 createBodyF08() {
        return new BodyF08();
    }

    /**
     * Create an instance of {@link ObjectContractF08 }
     */
    public ObjectContractF08 createObjectContractF08() {
        return new ObjectContractF08();
    }

    /**
     * Create an instance of {@link CiF08 }
     */
    public CiF08 createCiF08() {
        return new CiF08();
    }

    /**
     * Create an instance of {@link F122014 }
     */
    public F122014 createF122014() {
        return new F122014();
    }

    /**
     * Create an instance of {@link LegalBasisF12 }
     */
    public LegalBasisF12 createLegalBasisF12() {
        return new LegalBasisF12();
    }

    /**
     * Create an instance of {@link BodyF12 }
     */
    public BodyF12 createBodyF12() {
        return new BodyF12();
    }

    /**
     * Create an instance of {@link ObjectContractF12 }
     */
    public ObjectContractF12 createObjectContractF12() {
        return new ObjectContractF12();
    }

    /**
     * Create an instance of {@link LeftiF12 }
     */
    public LeftiF12 createLeftiF12() {
        return new LeftiF12();
    }

    /**
     * Create an instance of {@link ProcedureF12 }
     */
    public ProcedureF12 createProcedureF12() {
        return new ProcedureF12();
    }

    /**
     * Create an instance of {@link CiF12 }
     */
    public CiF12 createCiF12() {
        return new CiF12();
    }

    /**
     * Create an instance of {@link F132014 }
     */
    public F132014 createF132014() {
        return new F132014();
    }

    /**
     * Create an instance of {@link LegalBasisF13 }
     */
    public LegalBasisF13 createLegalBasisF13() {
        return new LegalBasisF13();
    }

    /**
     * Create an instance of {@link BodyF13 }
     */
    public BodyF13 createBodyF13() {
        return new BodyF13();
    }

    /**
     * Create an instance of {@link ObjectContractF13 }
     */
    public ObjectContractF13 createObjectContractF13() {
        return new ObjectContractF13();
    }

    /**
     * Create an instance of {@link CiF13 }
     */
    public CiF13 createCiF13() {
        return new CiF13();
    }

    /**
     * Create an instance of {@link WHERE }
     */
    public WHERE createWHERE() {
        return new WHERE();
    }

    /**
     * Create an instance of {@link F142014 }
     */
    public F142014 createF142014() {
        return new F142014();
    }

    /**
     * Create an instance of {@link LegalBasisF14 }
     */
    public LegalBasisF14 createLegalBasisF14() {
        return new LegalBasisF14();
    }

    /**
     * Create an instance of {@link BodyF14 }
     */
    public BodyF14 createBodyF14() {
        return new BodyF14();
    }

    /**
     * Create an instance of {@link ObjectContractF14 }
     */
    public ObjectContractF14 createObjectContractF14() {
        return new ObjectContractF14();
    }

    /**
     * Create an instance of {@link F152014 }
     */
    public F152014 createF152014() {
        return new F152014();
    }

    /**
     * Create an instance of {@link LegalBasisF15 }
     */
    public LegalBasisF15 createLegalBasisF15() {
        return new LegalBasisF15();
    }

    /**
     * Create an instance of {@link BodyF15 }
     */
    public BodyF15 createBodyF15() {
        return new BodyF15();
    }

    /**
     * Create an instance of {@link CiF15 }
     */
    public CiF15 createCiF15() {
        return new CiF15();
    }

    /**
     * Create an instance of {@link F202014 }
     */
    public F202014 createF202014() {
        return new F202014();
    }

    /**
     * Create an instance of {@link LegalBasisF20 }
     */
    public LegalBasisF20 createLegalBasisF20() {
        return new LegalBasisF20();
    }

    /**
     * Create an instance of {@link BodyF20 }
     */
    public BodyF20 createBodyF20() {
        return new BodyF20();
    }

    /**
     * Create an instance of {@link ObjectContractF20 }
     */
    public ObjectContractF20 createObjectContractF20() {
        return new ObjectContractF20();
    }

    /**
     * Create an instance of {@link ProcedureF20 }
     */
    public ProcedureF20 createProcedureF20() {
        return new ProcedureF20();
    }

    /**
     * Create an instance of {@link CiF20 }
     */
    public CiF20 createCiF20() {
        return new CiF20();
    }

    /**
     * Create an instance of {@link F212014 }
     */
    public F212014 createF212014() {
        return new F212014();
    }

    /**
     * Create an instance of {@link LegalBasisF21 }
     */
    public LegalBasisF21 createLegalBasisF21() {
        return new LegalBasisF21();
    }

    /**
     * Create an instance of {@link NoticeF21 }
     */
    public NoticeF21 createNoticeF21() {
        return new NoticeF21();
    }

    /**
     * Create an instance of {@link BodyF21 }
     */
    public BodyF21 createBodyF21() {
        return new BodyF21();
    }

    /**
     * Create an instance of {@link ObjectContractF21 }
     */
    public ObjectContractF21 createObjectContractF21() {
        return new ObjectContractF21();
    }

    /**
     * Create an instance of {@link LeftiF21 }
     */
    public LeftiF21 createLeftiF21() {
        return new LeftiF21();
    }

    /**
     * Create an instance of {@link CiF21 }
     */
    public CiF21 createCiF21() {
        return new CiF21();
    }

    /**
     * Create an instance of {@link F222014 }
     */
    public F222014 createF222014() {
        return new F222014();
    }

    /**
     * Create an instance of {@link LegalBasisF22 }
     */
    public LegalBasisF22 createLegalBasisF22() {
        return new LegalBasisF22();
    }

    /**
     * Create an instance of {@link NoticeF22 }
     */
    public NoticeF22 createNoticeF22() {
        return new NoticeF22();
    }

    /**
     * Create an instance of {@link BodyF22 }
     */
    public BodyF22 createBodyF22() {
        return new BodyF22();
    }

    /**
     * Create an instance of {@link LeftiF22 }
     */
    public LeftiF22 createLeftiF22() {
        return new LeftiF22();
    }

    /**
     * Create an instance of {@link CiF22 }
     */
    public CiF22 createCiF22() {
        return new CiF22();
    }

    /**
     * Create an instance of {@link F232014 }
     */
    public F232014 createF232014() {
        return new F232014();
    }

    /**
     * Create an instance of {@link LegalBasisF23 }
     */
    public LegalBasisF23 createLegalBasisF23() {
        return new LegalBasisF23();
    }

    /**
     * Create an instance of {@link NoticeF23 }
     */
    public NoticeF23 createNoticeF23() {
        return new NoticeF23();
    }

    /**
     * Create an instance of {@link BodyF23 }
     */
    public BodyF23 createBodyF23() {
        return new BodyF23();
    }

    /**
     * Create an instance of {@link ObjectContractF23 }
     */
    public ObjectContractF23 createObjectContractF23() {
        return new ObjectContractF23();
    }

    /**
     * Create an instance of {@link LeftiF23 }
     */
    public LeftiF23 createLeftiF23() {
        return new LeftiF23();
    }

    /**
     * Create an instance of {@link ProcedureF23 }
     */
    public ProcedureF23 createProcedureF23() {
        return new ProcedureF23();
    }

    /**
     * Create an instance of {@link CiF23 }
     */
    public CiF23 createCiF23() {
        return new CiF23();
    }

    /**
     * Create an instance of {@link F242014 }
     */
    public F242014 createF242014() {
        return new F242014();
    }

    /**
     * Create an instance of {@link LegalBasisF24 }
     */
    public LegalBasisF24 createLegalBasisF24() {
        return new LegalBasisF24();
    }

    /**
     * Create an instance of {@link BodyF24 }
     */
    public BodyF24 createBodyF24() {
        return new BodyF24();
    }

    /**
     * Create an instance of {@link ObjectContractF24 }
     */
    public ObjectContractF24 createObjectContractF24() {
        return new ObjectContractF24();
    }

    /**
     * Create an instance of {@link LeftiF24 }
     */
    public LeftiF24 createLeftiF24() {
        return new LeftiF24();
    }

    /**
     * Create an instance of {@link ProcedureF24 }
     */
    public ProcedureF24 createProcedureF24() {
        return new ProcedureF24();
    }

    /**
     * Create an instance of {@link CiF24 }
     */
    public CiF24 createCiF24() {
        return new CiF24();
    }

    /**
     * Create an instance of {@link F252014 }
     */
    public F252014 createF252014() {
        return new F252014();
    }

    /**
     * Create an instance of {@link LegalBasisF25 }
     */
    public LegalBasisF25 createLegalBasisF25() {
        return new LegalBasisF25();
    }

    /**
     * Create an instance of {@link BodyF25 }
     */
    public BodyF25 createBodyF25() {
        return new BodyF25();
    }

    /**
     * Create an instance of {@link ObjectContractF25 }
     */
    public ObjectContractF25 createObjectContractF25() {
        return new ObjectContractF25();
    }

    /**
     * Create an instance of {@link ProcedureF25 }
     */
    public ProcedureF25 createProcedureF25() {
        return new ProcedureF25();
    }

    /**
     * Create an instance of {@link CiF25 }
     */
    public CiF25 createCiF25() {
        return new CiF25();
    }

    /**
     * Create an instance of {@link MOVE }
     */
    public MOVE createMOVE() {
        return new MOVE();
    }

    /**
     * Create an instance of {@link LegalBasisMove }
     */
    public LegalBasisMove createLegalBasisMove() {
        return new LegalBasisMove();
    }

    /**
     * Create an instance of {@link BodyMove }
     */
    public BodyMove createBodyMove() {
        return new BodyMove();
    }

    /**
     * Create an instance of {@link LeftiMove }
     */
    public LeftiMove createLeftiMove() {
        return new LeftiMove();
    }

    /**
     * Create an instance of {@link ProcedureMove }
     */
    public ProcedureMove createProcedureMove() {
        return new ProcedureMove();
    }

    /**
     * Create an instance of {@link CiMove }
     */
    public CiMove createCiMove() {
        return new CiMove();
    }

    /**
     * Create an instance of {@link TedEsenders }
     */
    public TedEsenders createTedEsenders() {
        return new TedEsenders();
    }

    /**
     * Create an instance of {@link AgreeToPublicationMan }
     */
    public AgreeToPublicationMan createAgreeToPublicationMan() {
        return new AgreeToPublicationMan();
    }

    /**
     * Create an instance of {@link AgreeToPublicationOpt }
     */
    public AgreeToPublicationOpt createAgreeToPublicationOpt() {
        return new AgreeToPublicationOpt();
    }

    /**
     * Create an instance of {@link TextFtMultiLinesOrString }
     */
    public TextFtMultiLinesOrString createTextFtMultiLinesOrString() {
        return new TextFtMultiLinesOrString();
    }

    /**
     * Create an instance of {@link NbRange }
     */
    public NbRange createNbRange() {
        return new NbRange();
    }

    /**
     * Create an instance of {@link DurationMD }
     */
    public DurationMD createDurationMD() {
        return new DurationMD();
    }

    /**
     * Create an instance of {@link DurationMY }
     */
    public DurationMY createDurationMY() {
        return new DurationMY();
    }

    /**
     * Create an instance of {@link NoServices }
     */
    public NoServices createNoServices() {
        return new NoServices();
    }

    /**
     * Create an instance of {@link Works }
     */
    public Works createWorks() {
        return new Works();
    }

    /**
     * Create an instance of {@link Contact }
     */
    public Contact createContact() {
        return new Contact();
    }

    /**
     * Create an instance of {@link LotNumbers }
     */
    public LotNumbers createLotNumbers() {
        return new LotNumbers();
    }

    /**
     * Create an instance of {@link Lefti }
     */
    public Lefti createLefti() {
        return new Lefti();
    }

    /**
     * Create an instance of {@link FrameworkInfo }
     */
    public FrameworkInfo createFrameworkInfo() {
        return new FrameworkInfo();
    }

    /**
     * Create an instance of {@link ComplementInfo }
     */
    public ComplementInfo createComplementInfo() {
        return new ComplementInfo();
    }

    /**
     * Create an instance of {@link LotDivisionF01 }
     */
    public LotDivisionF01 createLotDivisionF01() {
        return new LotDivisionF01();
    }

    /**
     * Create an instance of {@link LotDivisionF02 }
     */
    public LotDivisionF02 createLotDivisionF02() {
        return new LotDivisionF02();
    }

    /**
     * Create an instance of {@link LotDivisionF04 }
     */
    public LotDivisionF04 createLotDivisionF04() {
        return new LotDivisionF04();
    }

    /**
     * Create an instance of {@link LotDivisionF05 }
     */
    public LotDivisionF05 createLotDivisionF05() {
        return new LotDivisionF05();
    }

    /**
     * Create an instance of {@link ContactBuyer }
     */
    public ContactBuyer createContactBuyer() {
        return new ContactBuyer();
    }

    /**
     * Create an instance of {@link ObjectF08 }
     */
    public ObjectF08 createObjectF08() {
        return new ObjectF08();
    }

    /**
     * Create an instance of {@link ObjectF12 }
     */
    public ObjectF12 createObjectF12() {
        return new ObjectF12();
    }

    /**
     * Create an instance of {@link ObjectF13 }
     */
    public ObjectF13 createObjectF13() {
        return new ObjectF13();
    }

    /**
     * Create an instance of {@link ContactAddContractingBodyF14 }
     */
    public ContactAddContractingBodyF14 createContactAddContractingBodyF14() {
        return new ContactAddContractingBodyF14();
    }

    /**
     * Create an instance of {@link ContactContractingBodyF14 }
     */
    public ContactContractingBodyF14 createContactContractingBodyF14() {
        return new ContactContractingBodyF14();
    }

    /**
     * Create an instance of {@link ObjectF20 }
     */
    public ObjectF20 createObjectF20() {
        return new ObjectF20();
    }

    /**
     * Create an instance of {@link ObjectF21 }
     */
    public ObjectF21 createObjectF21() {
        return new ObjectF21();
    }

    /**
     * Create an instance of {@link LotDivisionF21 }
     */
    public LotDivisionF21 createLotDivisionF21() {
        return new LotDivisionF21();
    }

    /**
     * Create an instance of {@link LotDivisionF22 }
     */
    public LotDivisionF22 createLotDivisionF22() {
        return new LotDivisionF22();
    }

    /**
     * Create an instance of {@link ObjectF23 }
     */
    public ObjectF23 createObjectF23() {
        return new ObjectF23();
    }

    /**
     * Create an instance of {@link LotDivisionF23 }
     */
    public LotDivisionF23 createLotDivisionF23() {
        return new LotDivisionF23();
    }

    /**
     * Create an instance of {@link LotDivisionF24 }
     */
    public LotDivisionF24 createLotDivisionF24() {
        return new LotDivisionF24();
    }

    /**
     * Create an instance of {@link ContactPartyMove }
     */
    public ContactPartyMove createContactPartyMove() {
        return new ContactPartyMove();
    }

    /**
     * Create an instance of {@link ContactContractorMove }
     */
    public ContactContractorMove createContactContractorMove() {
        return new ContactContractorMove();
    }

    /**
     * Create an instance of {@link CaTypeMove }
     */
    public CaTypeMove createCaTypeMove() {
        return new CaTypeMove();
    }

    /**
     * Create an instance of {@link ContactAddContractingBodyMove }
     */
    public ContactAddContractingBodyMove createContactAddContractingBodyMove() {
        return new ContactAddContractingBodyMove();
    }

    /**
     * Create an instance of {@link ContactContractingBodyMove }
     */
    public ContactContractingBodyMove createContactContractingBodyMove() {
        return new ContactContractingBodyMove();
    }

    /**
     * Create an instance of {@link FormSection }
     */
    public FormSection createFormSection() {
        return new FormSection();
    }

    /**
     * Create an instance of {@link Sender.IDENTIFICATION }
     */
    public Sender.IDENTIFICATION createSenderIDENTIFICATION() {
        return new Sender.IDENTIFICATION();
    }

    /**
     * Create an instance of {@link Sender.CONTACT.COUNTRY }
     */
    public Sender.CONTACT.COUNTRY createSenderCONTACTCOUNTRY() {
        return new Sender.CONTACT.COUNTRY();
    }

    /**
     * Create an instance of {@link ObjectF25 .AC }
     */
    public ObjectF25.AC createObjectF25AC() {
        return new ObjectF25.AC();
    }

    /**
     * Create an instance of {@link ObjectF24 .AC }
     */
    public ObjectF24.AC createObjectF24AC() {
        return new ObjectF24.AC();
    }

    /**
     * Create an instance of {@link ObjectF22 .QS }
     */
    public ObjectF22.QS createObjectF22QS() {
        return new ObjectF22.QS();
    }

    /**
     * Create an instance of {@link AnnexD2F22 .DACCORDANCEARTICLE }
     */
    public AnnexD2F22.DACCORDANCEARTICLE createAnnexD2F22DACCORDANCEARTICLE() {
        return new AnnexD2F22.DACCORDANCEARTICLE();
    }

    /**
     * Create an instance of {@link AnnexD1F21 .DACCORDANCEARTICLE }
     */
    public AnnexD1F21.DACCORDANCEARTICLE createAnnexD1F21DACCORDANCEARTICLE() {
        return new AnnexD1F21.DACCORDANCEARTICLE();
    }

    /**
     * Create an instance of {@link ObjectF15 .DIRECTIVE200981EC.AC }
     */
    public ObjectF15.DIRECTIVE200981EC.AC createObjectF15DIRECTIVE200981ECAC() {
        return new ObjectF15.DIRECTIVE200981EC.AC();
    }

    /**
     * Create an instance of {@link ObjectF15 .DIRECTIVE201425EU.AC }
     */
    public ObjectF15.DIRECTIVE201425EU.AC createObjectF15DIRECTIVE201425EUAC() {
        return new ObjectF15.DIRECTIVE201425EU.AC();
    }

    /**
     * Create an instance of {@link ObjectF15 .DIRECTIVE201424EU.AC }
     */
    public ObjectF15.DIRECTIVE201424EU.AC createObjectF15DIRECTIVE201424EUAC() {
        return new ObjectF15.DIRECTIVE201424EU.AC();
    }

    /**
     * Create an instance of {@link ObjectF15 .DIRECTIVE201423EU.AC }
     */
    public ObjectF15.DIRECTIVE201423EU.AC createObjectF15DIRECTIVE201423EUAC() {
        return new ObjectF15.DIRECTIVE201423EU.AC();
    }

    /**
     * Create an instance of {@link ObjectF07 .AC }
     */
    public ObjectF07.AC createObjectF07AC() {
        return new ObjectF07.AC();
    }

    /**
     * Create an instance of {@link ObjectF06 .AC }
     */
    public ObjectF06.AC createObjectF06AC() {
        return new ObjectF06.AC();
    }

    /**
     * Create an instance of {@link ObjectF05 .AC }
     */
    public ObjectF05.AC createObjectF05AC() {
        return new ObjectF05.AC();
    }

    /**
     * Create an instance of {@link ObjectF04 .AC }
     */
    public ObjectF04.AC createObjectF04AC() {
        return new ObjectF04.AC();
    }

    /**
     * Create an instance of {@link ObjectF03 .AC }
     */
    public ObjectF03.AC createObjectF03AC() {
        return new ObjectF03.AC();
    }

    /**
     * Create an instance of {@link ObjectF02 .AC }
     */
    public ObjectF02.AC createObjectF02AC() {
        return new ObjectF02.AC();
    }

    /**
     * Create an instance of {@link ObjectF01 .AC }
     */
    public ObjectF01.AC createObjectF01AC() {
        return new ObjectF01.AC();
    }

    /**
     * Create an instance of {@link AnnexD4 .DACCORDANCEARTICLE }
     */
    public AnnexD4.DACCORDANCEARTICLE createAnnexD4DACCORDANCEARTICLE() {
        return new AnnexD4.DACCORDANCEARTICLE();
    }

    /**
     * Create an instance of {@link AnnexD3 .DACCORDANCEARTICLE }
     */
    public AnnexD3.DACCORDANCEARTICLE createAnnexD3DACCORDANCEARTICLE() {
        return new AnnexD3.DACCORDANCEARTICLE();
    }

    /**
     * Create an instance of {@link AnnexD2 .DACCORDANCEARTICLE }
     */
    public AnnexD2.DACCORDANCEARTICLE createAnnexD2DACCORDANCEARTICLE() {
        return new AnnexD2.DACCORDANCEARTICLE();
    }

    /**
     * Create an instance of {@link AnnexD1 .DACCORDANCEARTICLE }
     */
    public AnnexD1.DACCORDANCEARTICLE createAnnexD1DACCORDANCEARTICLE() {
        return new AnnexD1.DACCORDANCEARTICLE();
    }

    /**
     * Create an instance of {@link AwardContractMove.AWARDEDCONTRACT.VALUES }
     */
    public AwardContractMove.AWARDEDCONTRACT.VALUES createAwardContractMoveAWARDEDCONTRACTVALUES() {
        return new AwardContractMove.AWARDEDCONTRACT.VALUES();
    }

    /**
     * Create an instance of {@link AwardContractMove.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR }
     */
    public AwardContractMove.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR createAwardContractMoveAWARDEDCONTRACTCONTRACTORSCONTRACTOR() {
        return new AwardContractMove.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR();
    }

    /**
     * Create an instance of {@link ObjectContractMove.CATEGORY }
     */
    public ObjectContractMove.CATEGORY createObjectContractMoveCATEGORY() {
        return new ObjectContractMove.CATEGORY();
    }

    /**
     * Create an instance of {@link ObjectContractMove.OBJECTDESCR.ESSENTIALASSETS }
     */
    public ObjectContractMove.OBJECTDESCR.ESSENTIALASSETS createObjectContractMoveOBJECTDESCRESSENTIALASSETS() {
        return new ObjectContractMove.OBJECTDESCR.ESSENTIALASSETS();
    }

    /**
     * Create an instance of {@link AwardContractF25 .AWARDEDCONTRACT.TENDERS }
     */
    public AwardContractF25.AWARDEDCONTRACT.TENDERS createAwardContractF25AWARDEDCONTRACTTENDERS() {
        return new AwardContractF25.AWARDEDCONTRACT.TENDERS();
    }

    /**
     * Create an instance of {@link AwardContractF25 .AWARDEDCONTRACT.VALUES }
     */
    public AwardContractF25.AWARDEDCONTRACT.VALUES createAwardContractF25AWARDEDCONTRACTVALUES() {
        return new AwardContractF25.AWARDEDCONTRACT.VALUES();
    }

    /**
     * Create an instance of {@link AwardContractF25 .AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR }
     */
    public AwardContractF25.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR createAwardContractF25AWARDEDCONTRACTCONTRACTORSCONTRACTOR() {
        return new AwardContractF25.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR();
    }

    /**
     * Create an instance of {@link AwardContractF23 .AWARDEDCONTRACT.TENDERS }
     */
    public AwardContractF23.AWARDEDCONTRACT.TENDERS createAwardContractF23AWARDEDCONTRACTTENDERS() {
        return new AwardContractF23.AWARDEDCONTRACT.TENDERS();
    }

    /**
     * Create an instance of {@link AwardContractF23 .AWARDEDCONTRACT.VALUES }
     */
    public AwardContractF23.AWARDEDCONTRACT.VALUES createAwardContractF23AWARDEDCONTRACTVALUES() {
        return new AwardContractF23.AWARDEDCONTRACT.VALUES();
    }

    /**
     * Create an instance of {@link AwardContractF23 .AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR }
     */
    public AwardContractF23.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR createAwardContractF23AWARDEDCONTRACTCONTRACTORSCONTRACTOR() {
        return new AwardContractF23.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR();
    }

    /**
     * Create an instance of {@link AwardContractF22 .AWARDEDCONTRACT.TENDERS }
     */
    public AwardContractF22.AWARDEDCONTRACT.TENDERS createAwardContractF22AWARDEDCONTRACTTENDERS() {
        return new AwardContractF22.AWARDEDCONTRACT.TENDERS();
    }

    /**
     * Create an instance of {@link AwardContractF22 .AWARDEDCONTRACT.CONTRACTORS }
     */
    public AwardContractF22.AWARDEDCONTRACT.CONTRACTORS createAwardContractF22AWARDEDCONTRACTCONTRACTORS() {
        return new AwardContractF22.AWARDEDCONTRACT.CONTRACTORS();
    }

    /**
     * Create an instance of {@link AwardContractF22 .AWARDEDCONTRACT.VALUES }
     */
    public AwardContractF22.AWARDEDCONTRACT.VALUES createAwardContractF22AWARDEDCONTRACTVALUES() {
        return new AwardContractF22.AWARDEDCONTRACT.VALUES();
    }

    /**
     * Create an instance of {@link ProcedureF22 .FRAMEWORK }
     */
    public ProcedureF22.FRAMEWORK createProcedureF22FRAMEWORK() {
        return new ProcedureF22.FRAMEWORK();
    }

    /**
     * Create an instance of {@link ObjectContractF22 .VALTOTAL }
     */
    public ObjectContractF22.VALTOTAL createObjectContractF22VALTOTAL() {
        return new ObjectContractF22.VALTOTAL();
    }

    /**
     * Create an instance of {@link ObjectContractF22 .VALRANGETOTAL }
     */
    public ObjectContractF22.VALRANGETOTAL createObjectContractF22VALRANGETOTAL() {
        return new ObjectContractF22.VALRANGETOTAL();
    }

    /**
     * Create an instance of {@link AwardContractF21 .AWARDEDCONTRACT.TENDERS }
     */
    public AwardContractF21.AWARDEDCONTRACT.TENDERS createAwardContractF21AWARDEDCONTRACTTENDERS() {
        return new AwardContractF21.AWARDEDCONTRACT.TENDERS();
    }

    /**
     * Create an instance of {@link AwardContractF21 .AWARDEDCONTRACT.CONTRACTORS }
     */
    public AwardContractF21.AWARDEDCONTRACT.CONTRACTORS createAwardContractF21AWARDEDCONTRACTCONTRACTORS() {
        return new AwardContractF21.AWARDEDCONTRACT.CONTRACTORS();
    }

    /**
     * Create an instance of {@link AwardContractF21 .AWARDEDCONTRACT.VALUES }
     */
    public AwardContractF21.AWARDEDCONTRACT.VALUES createAwardContractF21AWARDEDCONTRACTVALUES() {
        return new AwardContractF21.AWARDEDCONTRACT.VALUES();
    }

    /**
     * Create an instance of {@link ProcedureF21 .FRAMEWORK }
     */
    public ProcedureF21.FRAMEWORK createProcedureF21FRAMEWORK() {
        return new ProcedureF21.FRAMEWORK();
    }

    /**
     * Create an instance of {@link ModificationsF20 .INFOMODIFICATIONS.VALUES }
     */
    public ModificationsF20.INFOMODIFICATIONS.VALUES createModificationsF20INFOMODIFICATIONSVALUES() {
        return new ModificationsF20.INFOMODIFICATIONS.VALUES();
    }

    /**
     * Create an instance of {@link ModificationsF20 .DESCRIPTIONPROCUREMENT.VALUES }
     */
    public ModificationsF20.DESCRIPTIONPROCUREMENT.VALUES createModificationsF20DESCRIPTIONPROCUREMENTVALUES() {
        return new ModificationsF20.DESCRIPTIONPROCUREMENT.VALUES();
    }

    /**
     * Create an instance of {@link ModificationsF20 .DESCRIPTIONPROCUREMENT.CONTRACTORS }
     */
    public ModificationsF20.DESCRIPTIONPROCUREMENT.CONTRACTORS createModificationsF20DESCRIPTIONPROCUREMENTCONTRACTORS() {
        return new ModificationsF20.DESCRIPTIONPROCUREMENT.CONTRACTORS();
    }

    /**
     * Create an instance of {@link AwardContractF20 .AWARDEDCONTRACT.CONTRACTORS }
     */
    public AwardContractF20.AWARDEDCONTRACT.CONTRACTORS createAwardContractF20AWARDEDCONTRACTCONTRACTORS() {
        return new AwardContractF20.AWARDEDCONTRACT.CONTRACTORS();
    }

    /**
     * Create an instance of {@link AwardContractF20 .AWARDEDCONTRACT.VALUES }
     */
    public AwardContractF20.AWARDEDCONTRACT.VALUES createAwardContractF20AWARDEDCONTRACTVALUES() {
        return new AwardContractF20.AWARDEDCONTRACT.VALUES();
    }

    /**
     * Create an instance of {@link AwardContractF15 .AWARDEDCONTRACT.CONTRACTORS }
     */
    public AwardContractF15.AWARDEDCONTRACT.CONTRACTORS createAwardContractF15AWARDEDCONTRACTCONTRACTORS() {
        return new AwardContractF15.AWARDEDCONTRACT.CONTRACTORS();
    }

    /**
     * Create an instance of {@link AwardContractF15 .AWARDEDCONTRACT.VALUES }
     */
    public AwardContractF15.AWARDEDCONTRACT.VALUES createAwardContractF15AWARDEDCONTRACTVALUES() {
        return new AwardContractF15.AWARDEDCONTRACT.VALUES();
    }

    /**
     * Create an instance of {@link AwardContractF15 .AWARDEDCONTRACT.DIRECTIVE200981EC }
     */
    public AwardContractF15.AWARDEDCONTRACT.DIRECTIVE200981EC createAwardContractF15AWARDEDCONTRACTDIRECTIVE200981EC() {
        return new AwardContractF15.AWARDEDCONTRACT.DIRECTIVE200981EC();
    }

    /**
     * Create an instance of {@link ProcedureF15 .DIRECTIVE200981EC.PTNEGOTIATEDWITHOUTPUBLICATION }
     */
    public ProcedureF15.DIRECTIVE200981EC.PTNEGOTIATEDWITHOUTPUBLICATION createProcedureF15DIRECTIVE200981ECPTNEGOTIATEDWITHOUTPUBLICATION() {
        return new ProcedureF15.DIRECTIVE200981EC.PTNEGOTIATEDWITHOUTPUBLICATION();
    }

    /**
     * Create an instance of {@link ProcedureF15 .DIRECTIVE200981EC.PTAWARDCONTRACTWITHOUTCALL }
     */
    public ProcedureF15.DIRECTIVE200981EC.PTAWARDCONTRACTWITHOUTCALL createProcedureF15DIRECTIVE200981ECPTAWARDCONTRACTWITHOUTCALL() {
        return new ProcedureF15.DIRECTIVE200981EC.PTAWARDCONTRACTWITHOUTCALL();
    }

    /**
     * Create an instance of {@link ProcedureF15 .DIRECTIVE201425EU.PTNEGOTIATEDWITHOUTPUBLICATION }
     */
    public ProcedureF15.DIRECTIVE201425EU.PTNEGOTIATEDWITHOUTPUBLICATION createProcedureF15DIRECTIVE201425EUPTNEGOTIATEDWITHOUTPUBLICATION() {
        return new ProcedureF15.DIRECTIVE201425EU.PTNEGOTIATEDWITHOUTPUBLICATION();
    }

    /**
     * Create an instance of {@link ProcedureF15 .DIRECTIVE201425EU.PTAWARDCONTRACTWITHOUTCALL }
     */
    public ProcedureF15.DIRECTIVE201425EU.PTAWARDCONTRACTWITHOUTCALL createProcedureF15DIRECTIVE201425EUPTAWARDCONTRACTWITHOUTCALL() {
        return new ProcedureF15.DIRECTIVE201425EU.PTAWARDCONTRACTWITHOUTCALL();
    }

    /**
     * Create an instance of {@link ProcedureF15 .DIRECTIVE201424EU.PTNEGOTIATEDWITHOUTPUBLICATION }
     */
    public ProcedureF15.DIRECTIVE201424EU.PTNEGOTIATEDWITHOUTPUBLICATION createProcedureF15DIRECTIVE201424EUPTNEGOTIATEDWITHOUTPUBLICATION() {
        return new ProcedureF15.DIRECTIVE201424EU.PTNEGOTIATEDWITHOUTPUBLICATION();
    }

    /**
     * Create an instance of {@link ProcedureF15 .DIRECTIVE201424EU.PTAWARDCONTRACTWITHOUTCALL }
     */
    public ProcedureF15.DIRECTIVE201424EU.PTAWARDCONTRACTWITHOUTCALL createProcedureF15DIRECTIVE201424EUPTAWARDCONTRACTWITHOUTCALL() {
        return new ProcedureF15.DIRECTIVE201424EU.PTAWARDCONTRACTWITHOUTCALL();
    }

    /**
     * Create an instance of {@link ProcedureF15 .DIRECTIVE201423EU.PTAWARDCONTRACTWITHOUTPUBLICATION }
     */
    public ProcedureF15.DIRECTIVE201423EU.PTAWARDCONTRACTWITHOUTPUBLICATION createProcedureF15DIRECTIVE201423EUPTAWARDCONTRACTWITHOUTPUBLICATION() {
        return new ProcedureF15.DIRECTIVE201423EU.PTAWARDCONTRACTWITHOUTPUBLICATION();
    }

    /**
     * Create an instance of {@link ProcedureF15 .DIRECTIVE201423EU.PTAWARDCONTRACTWITHOUTCALL }
     */
    public ProcedureF15.DIRECTIVE201423EU.PTAWARDCONTRACTWITHOUTCALL createProcedureF15DIRECTIVE201423EUPTAWARDCONTRACTWITHOUTCALL() {
        return new ProcedureF15.DIRECTIVE201423EU.PTAWARDCONTRACTWITHOUTCALL();
    }

    /**
     * Create an instance of {@link ObjectContractF15 .VALTOTAL }
     */
    public ObjectContractF15.VALTOTAL createObjectContractF15VALTOTAL() {
        return new ObjectContractF15.VALTOTAL();
    }

    /**
     * Create an instance of {@link ObjectContractF15 .VALRANGETOTAL }
     */
    public ObjectContractF15.VALRANGETOTAL createObjectContractF15VALRANGETOTAL() {
        return new ObjectContractF15.VALRANGETOTAL();
    }

    /**
     * Create an instance of {@link ChangesF14 .CHANGE.OLDVALUE }
     */
    public ChangesF14.CHANGE.OLDVALUE createChangesF14CHANGEOLDVALUE() {
        return new ChangesF14.CHANGE.OLDVALUE();
    }

    /**
     * Create an instance of {@link ChangesF14 .CHANGE.NEWVALUE }
     */
    public ChangesF14.CHANGE.NEWVALUE createChangesF14CHANGENEWVALUE() {
        return new ChangesF14.CHANGE.NEWVALUE();
    }

    /**
     * Create an instance of {@link CiF14 .ESENDERLOGIN }
     */
    public CiF14.ESENDERLOGIN createCiF14ESENDERLOGIN() {
        return new CiF14.ESENDERLOGIN();
    }

    /**
     * Create an instance of {@link CiF14 .CUSTOMERLOGIN }
     */
    public CiF14.CUSTOMERLOGIN createCiF14CUSTOMERLOGIN() {
        return new CiF14.CUSTOMERLOGIN();
    }

    /**
     * Create an instance of {@link CiF14 .NODOCEXT }
     */
    public CiF14.NODOCEXT createCiF14NODOCEXT() {
        return new CiF14.NODOCEXT();
    }

    /**
     * Create an instance of {@link CiF14 .ORIGINALOTHERMEANS }
     */
    public CiF14.ORIGINALOTHERMEANS createCiF14ORIGINALOTHERMEANS() {
        return new CiF14.ORIGINALOTHERMEANS();
    }

    /**
     * Create an instance of {@link ResultsF13 .AWARDEDPRIZE.PARTICIPANTS }
     */
    public ResultsF13.AWARDEDPRIZE.PARTICIPANTS createResultsF13AWARDEDPRIZEPARTICIPANTS() {
        return new ResultsF13.AWARDEDPRIZE.PARTICIPANTS();
    }

    /**
     * Create an instance of {@link ResultsF13 .AWARDEDPRIZE.VALPRIZE }
     */
    public ResultsF13.AWARDEDPRIZE.VALPRIZE createResultsF13AWARDEDPRIZEVALPRIZE() {
        return new ResultsF13.AWARDEDPRIZE.VALPRIZE();
    }

    /**
     * Create an instance of {@link ResultsF13 .AWARDEDPRIZE.WINNERS.WINNER.ADDRESSWINNER }
     */
    public ResultsF13.AWARDEDPRIZE.WINNERS.WINNER.ADDRESSWINNER createResultsF13AWARDEDPRIZEWINNERSWINNERADDRESSWINNER() {
        return new ResultsF13.AWARDEDPRIZE.WINNERS.WINNER.ADDRESSWINNER();
    }

    /**
     * Create an instance of {@link ProcedureF13 .CRITERIAEVALUATION }
     */
    public ProcedureF13.CRITERIAEVALUATION createProcedureF13CRITERIAEVALUATION() {
        return new ProcedureF13.CRITERIAEVALUATION();
    }

    /**
     * Create an instance of {@link AwardContractF06 .AWARDEDCONTRACT.TENDERS }
     */
    public AwardContractF06.AWARDEDCONTRACT.TENDERS createAwardContractF06AWARDEDCONTRACTTENDERS() {
        return new AwardContractF06.AWARDEDCONTRACT.TENDERS();
    }

    /**
     * Create an instance of {@link AwardContractF06 .AWARDEDCONTRACT.CONTRACTORS }
     */
    public AwardContractF06.AWARDEDCONTRACT.CONTRACTORS createAwardContractF06AWARDEDCONTRACTCONTRACTORS() {
        return new AwardContractF06.AWARDEDCONTRACT.CONTRACTORS();
    }

    /**
     * Create an instance of {@link AwardContractF06 .AWARDEDCONTRACT.VALUES }
     */
    public AwardContractF06.AWARDEDCONTRACT.VALUES createAwardContractF06AWARDEDCONTRACTVALUES() {
        return new AwardContractF06.AWARDEDCONTRACT.VALUES();
    }

    /**
     * Create an instance of {@link AwardContractF06 .AWARDEDCONTRACT.NBCONTRACTAWARDED }
     */
    public AwardContractF06.AWARDEDCONTRACT.NBCONTRACTAWARDED createAwardContractF06AWARDEDCONTRACTNBCONTRACTAWARDED() {
        return new AwardContractF06.AWARDEDCONTRACT.NBCONTRACTAWARDED();
    }

    /**
     * Create an instance of {@link AwardContractF06 .AWARDEDCONTRACT.COUNTRYORIGIN }
     */
    public AwardContractF06.AWARDEDCONTRACT.COUNTRYORIGIN createAwardContractF06AWARDEDCONTRACTCOUNTRYORIGIN() {
        return new AwardContractF06.AWARDEDCONTRACT.COUNTRYORIGIN();
    }

    /**
     * Create an instance of {@link ObjectContractF06 .VALTOTAL }
     */
    public ObjectContractF06.VALTOTAL createObjectContractF06VALTOTAL() {
        return new ObjectContractF06.VALTOTAL();
    }

    /**
     * Create an instance of {@link ObjectContractF06 .VALRANGETOTAL }
     */
    public ObjectContractF06.VALRANGETOTAL createObjectContractF06VALRANGETOTAL() {
        return new ObjectContractF06.VALRANGETOTAL();
    }

    /**
     * Create an instance of {@link ProcedureF05 .DURATIONTENDERVALID }
     */
    public ProcedureF05.DURATIONTENDERVALID createProcedureF05DURATIONTENDERVALID() {
        return new ProcedureF05.DURATIONTENDERVALID();
    }

    /**
     * Create an instance of {@link AwardContractF03 .AWARDEDCONTRACT.TENDERS }
     */
    public AwardContractF03.AWARDEDCONTRACT.TENDERS createAwardContractF03AWARDEDCONTRACTTENDERS() {
        return new AwardContractF03.AWARDEDCONTRACT.TENDERS();
    }

    /**
     * Create an instance of {@link AwardContractF03 .AWARDEDCONTRACT.CONTRACTORS }
     */
    public AwardContractF03.AWARDEDCONTRACT.CONTRACTORS createAwardContractF03AWARDEDCONTRACTCONTRACTORS() {
        return new AwardContractF03.AWARDEDCONTRACT.CONTRACTORS();
    }

    /**
     * Create an instance of {@link AwardContractF03 .AWARDEDCONTRACT.VALUES }
     */
    public AwardContractF03.AWARDEDCONTRACT.VALUES createAwardContractF03AWARDEDCONTRACTVALUES() {
        return new AwardContractF03.AWARDEDCONTRACT.VALUES();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Language }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Language }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LANGUAGE")
    public JAXBElement<Language> createLANGUAGE(Language value) {
        return new JAXBElement<Language>(_LANGUAGE_QNAME, Language.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Country }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Country }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "COUNTRY")
    public JAXBElement<Country> createCOUNTRY(Country value) {
        return new JAXBElement<Country>(_COUNTRY_QNAME, Country.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CpvCodes }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CpvCodes }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CPV_CODE")
    public JAXBElement<CpvCodes> createCPVCODE(CpvCodes value) {
        return new JAXBElement<CpvCodes>(_CPVCODE_QNAME, CpvCodes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CpvSupplementaryCodes }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CpvSupplementaryCodes }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CPV_SUPPLEMENTARY_CODE")
    public JAXBElement<CpvSupplementaryCodes> createCPVSUPPLEMENTARYCODE(CpvSupplementaryCodes value) {
        return new JAXBElement<CpvSupplementaryCodes>(_CPVSUPPLEMENTARYCODE_QNAME, CpvSupplementaryCodes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NOTICE_UUID")
    public JAXBElement<String> createNOTICEUUID(String value) {
        return new JAXBElement<String>(_NOTICEUUID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DATE_EXPECTED_PUBLICATION")
    public JAXBElement<XMLGregorianCalendar> createDATEEXPECTEDPUBLICATION(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DATEEXPECTEDPUBLICATION_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ESENDER_LOGIN")
    public JAXBElement<String> createESENDERLOGIN(String value) {
        return new JAXBElement<String>(_ESENDERLOGIN_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CUSTOMER_LOGIN")
    public JAXBElement<String> createCUSTOMERLOGIN(String value) {
        return new JAXBElement<String>(_CUSTOMERLOGIN_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NO_DOC_EXT")
    public JAXBElement<String> createNODOCEXT(String value) {
        return new JAXBElement<String>(_NODOCEXT_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Ft }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Ft }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "FT")
    public JAXBElement<Ft> createFT(Ft value) {
        return new JAXBElement<Ft>(_FT_QNAME, Ft.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link P }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link P }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "P")
    public JAXBElement<P> createP(P value) {
        return new JAXBElement<P>(_P_QNAME, P.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Val }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Val }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "VAL_ESTIMATED_TOTAL")
    public JAXBElement<Val> createVALESTIMATEDTOTAL(Val value) {
        return new JAXBElement<Val>(_VALESTIMATEDTOTAL_QNAME, Val.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Val }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Val }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "VAL_OBJECT")
    public JAXBElement<Val> createVALOBJECT(Val value) {
        return new JAXBElement<Val>(_VALOBJECT_QNAME, Val.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValRange }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ValRange }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "VAL_RANGE_OBJECT")
    public JAXBElement<ValRange> createVALRANGEOBJECT(ValRange value) {
        return new JAXBElement<ValRange>(_VALRANGEOBJECT_QNAME, ValRange.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Val }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Val }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "VAL_TOTAL")
    public JAXBElement<Val> createVALTOTAL(Val value) {
        return new JAXBElement<Val>(_VALTOTAL_QNAME, Val.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValRange }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ValRange }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "VAL_RANGE_TOTAL")
    public JAXBElement<ValRange> createVALRANGETOTAL(ValRange value) {
        return new JAXBElement<ValRange>(_VALRANGETOTAL_QNAME, ValRange.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DATE_START")
    public JAXBElement<XMLGregorianCalendar> createDATESTART(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DATESTART_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DATE_END")
    public JAXBElement<XMLGregorianCalendar> createDATEEND(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DATEEND_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OFFICIALNAME")
    public JAXBElement<String> createOFFICIALNAME(String value) {
        return new JAXBElement<String>(_OFFICIALNAME_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CODE")
    public JAXBElement<String> createCODE(String value) {
        return new JAXBElement<String>(_CODE_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LIBELLE")
    public JAXBElement<String> createLIBELLE(String value) {
        return new JAXBElement<String>(_LIBELLE_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NATIONALID")
    public JAXBElement<String> createNATIONALID(String value) {
        return new JAXBElement<String>(_NATIONALID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "SIRET")
    public JAXBElement<String> createSIRET(String value) {
        return new JAXBElement<String>(_SIRET_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ADDRESS")
    public JAXBElement<String> createADDRESS(String value) {
        return new JAXBElement<String>(_ADDRESS_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "POSTAL_CODE")
    public JAXBElement<String> createPOSTALCODE(String value) {
        return new JAXBElement<String>(_POSTALCODE_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "TOWN")
    public JAXBElement<String> createTOWN(String value) {
        return new JAXBElement<String>(_TOWN_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTACT_POINT")
    public JAXBElement<String> createCONTACTPOINT(String value) {
        return new JAXBElement<String>(_CONTACTPOINT_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PHONE")
    public JAXBElement<String> createPHONE(String value) {
        return new JAXBElement<String>(_PHONE_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "FAX")
    public JAXBElement<String> createFAX(String value) {
        return new JAXBElement<String>(_FAX_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "E_MAIL")
    public JAXBElement<String> createEMAIL(String value) {
        return new JAXBElement<String>(_EMAIL_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "URL")
    public JAXBElement<String> createURL(String value) {
        return new JAXBElement<String>(_URL_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "URL_BUYER")
    public JAXBElement<String> createURLBUYER(String value) {
        return new JAXBElement<String>(_URLBUYER_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "URL_GENERAL")
    public JAXBElement<String> createURLGENERAL(String value) {
        return new JAXBElement<String>(_URLGENERAL_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LOT_NO")
    public JAXBElement<String> createLOTNO(String value) {
        return new JAXBElement<String>(_LOTNO_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LOT_ALL")
    public JAXBElement<Empty> createLOTALL(Empty value) {
        return new JAXBElement<Empty>(_LOTALL_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LOT_ONE_ONLY")
    public JAXBElement<Empty> createLOTONEONLY(Empty value) {
        return new JAXBElement<Empty>(_LOTONEONLY_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LOT_MAX_NUMBER")
    public JAXBElement<Integer> createLOTMAXNUMBER(Integer value) {
        return new JAXBElement<Integer>(_LOTMAXNUMBER_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LOT_MAX_ONE_TENDERER")
    public JAXBElement<Integer> createLOTMAXONETENDERER(Integer value) {
        return new JAXBElement<Integer>(_LOTMAXONETENDERER_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LOT_COMBINING_CONTRACT_RIGHT")
    public JAXBElement<TextFtMultiLines> createLOTCOMBININGCONTRACTRIGHT(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_LOTCOMBININGCONTRACTRIGHT_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "TRANCHE")
    public JAXBElement<Empty> createTRANCHE(Empty value) {
        return new JAXBElement<Empty>(_TRANCHE_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NO_TRANCHE")
    public JAXBElement<Empty> createNOTRANCHE(Empty value) {
        return new JAXBElement<Empty>(_NOTRANCHE_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LOT_DIVISION")
    public JAXBElement<Empty> createLOTDIVISION(Empty value) {
        return new JAXBElement<Empty>(_LOTDIVISION_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NO_LOT_DIVISION")
    public JAXBElement<Empty> createNOLOTDIVISION(Empty value) {
        return new JAXBElement<Empty>(_NOLOTDIVISION_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_PROC_COMPETITIVE_DIALOGUE")
    public JAXBElement<Empty> createDPROCCOMPETITIVEDIALOGUE(Empty value) {
        return new JAXBElement<Empty>(_DPROCCOMPETITIVEDIALOGUE_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_PROC_NEGOTIATED_PRIOR_CALL_COMPETITION")
    public JAXBElement<Empty> createDPROCNEGOTIATEDPRIORCALLCOMPETITION(Empty value) {
        return new JAXBElement<Empty>(_DPROCNEGOTIATEDPRIORCALLCOMPETITION_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_PROC_OPEN")
    public JAXBElement<Empty> createDPROCOPEN(Empty value) {
        return new JAXBElement<Empty>(_DPROCOPEN_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_PROC_RESTRICTED")
    public JAXBElement<Empty> createDPROCRESTRICTED(Empty value) {
        return new JAXBElement<Empty>(_DPROCRESTRICTED_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_NO_TENDERS_REQUESTS")
    public JAXBElement<Empty> createDNOTENDERSREQUESTS(Empty value) {
        return new JAXBElement<Empty>(_DNOTENDERSREQUESTS_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_ALL_TENDERS")
    public JAXBElement<Empty> createDALLTENDERS(Empty value) {
        return new JAXBElement<Empty>(_DALLTENDERS_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_PERIODS_INCOMPATIBLE")
    public JAXBElement<Empty> createDPERIODSINCOMPATIBLE(Empty value) {
        return new JAXBElement<Empty>(_DPERIODSINCOMPATIBLE_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_PURE_RESEARCH")
    public JAXBElement<Empty> createDPURERESEARCH(Empty value) {
        return new JAXBElement<Empty>(_DPURERESEARCH_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_TECHNICAL")
    public JAXBElement<Empty> createDTECHNICAL(Empty value) {
        return new JAXBElement<Empty>(_DTECHNICAL_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_ARTISTIC")
    public JAXBElement<Empty> createDARTISTIC(Empty value) {
        return new JAXBElement<Empty>(_DARTISTIC_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_EXCLUSIVE_RIGHT")
    public JAXBElement<Empty> createDEXCLUSIVERIGHT(Empty value) {
        return new JAXBElement<Empty>(_DEXCLUSIVERIGHT_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_PROTECT_RIGHTS")
    public JAXBElement<Empty> createDPROTECTRIGHTS(Empty value) {
        return new JAXBElement<Empty>(_DPROTECTRIGHTS_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoWorks }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link NoWorks }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_OTHER_SERVICES")
    public JAXBElement<NoWorks> createDOTHERSERVICES(NoWorks value) {
        return new JAXBElement<NoWorks>(_DOTHERSERVICES_QNAME, NoWorks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Services }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Services }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_MARITIME_SERVICES")
    public JAXBElement<Services> createDMARITIMESERVICES(Services value) {
        return new JAXBElement<Services>(_DMARITIMESERVICES_QNAME, Services.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_EXTREME_URGENCY")
    public JAXBElement<Empty> createDEXTREMEURGENCY(Empty value) {
        return new JAXBElement<Empty>(_DEXTREMEURGENCY_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_ADD_DELIVERIES_ORDERED")
    public JAXBElement<Empty> createDADDDELIVERIESORDERED(Empty value) {
        return new JAXBElement<Empty>(_DADDDELIVERIESORDERED_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoSupplies }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link NoSupplies }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_REPETITION_EXISTING")
    public JAXBElement<NoSupplies> createDREPETITIONEXISTING(NoSupplies value) {
        return new JAXBElement<NoSupplies>(_DREPETITIONEXISTING_QNAME, NoSupplies.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Services }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Services }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_CONTRACT_AWARDED_DESIGN_CONTEST")
    public JAXBElement<Services> createDCONTRACTAWARDEDDESIGNCONTEST(Services value) {
        return new JAXBElement<Services>(_DCONTRACTAWARDEDDESIGNCONTEST_QNAME, Services.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Supplies }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Supplies }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_COMMODITY_MARKET")
    public JAXBElement<Supplies> createDCOMMODITYMARKET(Supplies value) {
        return new JAXBElement<Supplies>(_DCOMMODITYMARKET_QNAME, Supplies.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoWorks }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link NoWorks }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_FROM_WINDING_PROVIDER")
    public JAXBElement<NoWorks> createDFROMWINDINGPROVIDER(NoWorks value) {
        return new JAXBElement<NoWorks>(_DFROMWINDINGPROVIDER_QNAME, NoWorks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoWorks }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link NoWorks }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_FROM_LIQUIDATOR_CREDITOR")
    public JAXBElement<NoWorks> createDFROMLIQUIDATORCREDITOR(NoWorks value) {
        return new JAXBElement<NoWorks>(_DFROMLIQUIDATORCREDITOR_QNAME, NoWorks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_BARGAIN_PURCHASE")
    public JAXBElement<Empty> createDBARGAINPURCHASE(Empty value) {
        return new JAXBElement<Empty>(_DBARGAINPURCHASE_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_SERVICES_LISTED")
    public JAXBElement<Empty> createDSERVICESLISTED(Empty value) {
        return new JAXBElement<Empty>(_DSERVICESLISTED_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_OUTSIDE_SCOPE")
    public JAXBElement<Empty> createDOUTSIDESCOPE(Empty value) {
        return new JAXBElement<Empty>(_DOUTSIDESCOPE_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "D_JUSTIFICATION")
    public JAXBElement<TextFtMultiLines> createDJUSTIFICATION(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_DJUSTIFICATION_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEGAL_BASIS_OTHER")
    public JAXBElement<TextFtMultiLines> createLEGALBASISOTHER(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_LEGALBASISOTHER_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactContractingBody }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ContactContractingBody }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ADDRESS_CONTRACTING_BODY")
    public JAXBElement<ContactContractingBody> createADDRESSCONTRACTINGBODY(ContactContractingBody value) {
        return new JAXBElement<ContactContractingBody>(_ADDRESSCONTRACTINGBODY_QNAME, ContactContractingBody.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Offre }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Offre }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OFFRE")
    public JAXBElement<Offre> createOFFRE(Offre value) {
        return new JAXBElement<Offre>(_OFFRE_QNAME, Offre.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactContractingBody }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ContactContractingBody }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ADDRESS_CONTRACTING_BODY_ADDITIONAL")
    public JAXBElement<ContactContractingBody> createADDRESSCONTRACTINGBODYADDITIONAL(ContactContractingBody value) {
        return new JAXBElement<ContactContractingBody>(_ADDRESSCONTRACTINGBODYADDITIONAL_QNAME, ContactContractingBody.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "JOINT_PROCUREMENT_INVOLVED")
    public JAXBElement<Empty> createJOINTPROCUREMENTINVOLVED(Empty value) {
        return new JAXBElement<Empty>(_JOINTPROCUREMENTINVOLVED_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PROCUREMENT_LAW")
    public JAXBElement<TextFtMultiLines> createPROCUREMENTLAW(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_PROCUREMENTLAW_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CENTRAL_PURCHASING")
    public JAXBElement<Empty> createCENTRALPURCHASING(Empty value) {
        return new JAXBElement<Empty>(_CENTRALPURCHASING_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DOCUMENT_FULL")
    public JAXBElement<Empty> createDOCUMENTFULL(Empty value) {
        return new JAXBElement<Empty>(_DOCUMENTFULL_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DOCUMENT_RESTRICTED")
    public JAXBElement<Empty> createDOCUMENTRESTRICTED(Empty value) {
        return new JAXBElement<Empty>(_DOCUMENTRESTRICTED_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "URL_DOCUMENT")
    public JAXBElement<String> createURLDOCUMENT(String value) {
        return new JAXBElement<String>(_URLDOCUMENT_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ADDRESS_FURTHER_INFO_IDEM")
    public JAXBElement<Empty> createADDRESSFURTHERINFOIDEM(Empty value) {
        return new JAXBElement<Empty>(_ADDRESSFURTHERINFOIDEM_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactContractingBody }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ContactContractingBody }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ADDRESS_FURTHER_INFO")
    public JAXBElement<ContactContractingBody> createADDRESSFURTHERINFO(ContactContractingBody value) {
        return new JAXBElement<ContactContractingBody>(_ADDRESSFURTHERINFO_QNAME, ContactContractingBody.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "URL_PARTICIPATION")
    public JAXBElement<String> createURLPARTICIPATION(String value) {
        return new JAXBElement<String>(_URLPARTICIPATION_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ADDRESS_PARTICIPATION_IDEM")
    public JAXBElement<Empty> createADDRESSPARTICIPATIONIDEM(Empty value) {
        return new JAXBElement<Empty>(_ADDRESSPARTICIPATIONIDEM_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactContractingBody }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ContactContractingBody }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ADDRESS_PARTICIPATION")
    public JAXBElement<ContactContractingBody> createADDRESSPARTICIPATION(ContactContractingBody value) {
        return new JAXBElement<ContactContractingBody>(_ADDRESSPARTICIPATION_QNAME, ContactContractingBody.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "URL_TOOL")
    public JAXBElement<String> createURLTOOL(String value) {
        return new JAXBElement<String>(_URLTOOL_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CaType }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CaType }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CA_TYPE")
    public JAXBElement<CaType> createCATYPE(CaType value) {
        return new JAXBElement<CaType>(_CATYPE_QNAME, CaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CA_TYPE_OTHER")
    public JAXBElement<String> createCATYPEOTHER(String value) {
        return new JAXBElement<String>(_CATYPEOTHER_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CaActivity }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CaActivity }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CA_ACTIVITY")
    public JAXBElement<CaActivity> createCAACTIVITY(CaActivity value) {
        return new JAXBElement<CaActivity>(_CAACTIVITY_QNAME, CaActivity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CA_ACTIVITY_OTHER")
    public JAXBElement<String> createCAACTIVITYOTHER(String value) {
        return new JAXBElement<String>(_CAACTIVITYOTHER_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CeActivity }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CeActivity }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CE_ACTIVITY")
    public JAXBElement<CeActivity> createCEACTIVITY(CeActivity value) {
        return new JAXBElement<CeActivity>(_CEACTIVITY_QNAME, CeActivity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CE_ACTIVITY_OTHER")
    public JAXBElement<String> createCEACTIVITYOTHER(String value) {
        return new JAXBElement<String>(_CEACTIVITYOTHER_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtSingleLine }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtSingleLine }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "TITLE")
    public JAXBElement<TextFtSingleLine> createTITLE(TextFtSingleLine value) {
        return new JAXBElement<TextFtSingleLine>(_TITLE_QNAME, TextFtSingleLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "REFERENCE_NUMBER")
    public JAXBElement<String> createREFERENCENUMBER(String value) {
        return new JAXBElement<String>(_REFERENCENUMBER_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Groupement }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Groupement }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "GROUPEMENT")
    public JAXBElement<Groupement> createGROUPEMENT(Groupement value) {
        return new JAXBElement<Groupement>(_GROUPEMENT_QNAME, Groupement.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CpvSet }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CpvSet }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CPV_ADDITIONAL")
    public JAXBElement<CpvSet> createCPVADDITIONAL(CpvSet value) {
        return new JAXBElement<CpvSet>(_CPVADDITIONAL_QNAME, CpvSet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CpvSet }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CpvSet }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CPV_MAIN")
    public JAXBElement<CpvSet> createCPVMAIN(CpvSet value) {
        return new JAXBElement<CpvSet>(_CPVMAIN_QNAME, CpvSet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LIEU_EXECUTION")
    public JAXBElement<String> createLIEUEXECUTION(String value) {
        return new JAXBElement<String>(_LIEUEXECUTION_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TypeContract }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TypeContract }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "TYPE_CONTRACT")
    public JAXBElement<TypeContract> createTYPECONTRACT(TypeContract value) {
        return new JAXBElement<TypeContract>(_TYPECONTRACT_QNAME, TypeContract.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "SHORT_DESCR")
    public JAXBElement<TextFtMultiLines> createSHORTDESCR(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_SHORTDESCR_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "MAIN_SITE")
    public JAXBElement<TextFtMultiLines> createMAINSITE(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_MAINSITE_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "AC_CRITERION")
    public JAXBElement<String> createACCRITERION(String value) {
        return new JAXBElement<String>(_ACCRITERION_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "AC_WEIGHTING")
    public JAXBElement<String> createACWEIGHTING(String value) {
        return new JAXBElement<String>(_ACWEIGHTING_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AcDefinition }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link AcDefinition }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "AC_COST")
    public JAXBElement<AcDefinition> createACCOST(AcDefinition value) {
        return new JAXBElement<AcDefinition>(_ACCOST_QNAME, AcDefinition.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AcDefinition }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link AcDefinition }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "AC_QUALITY")
    public JAXBElement<AcDefinition> createACQUALITY(AcDefinition value) {
        return new JAXBElement<AcDefinition>(_ACQUALITY_QNAME, AcDefinition.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "AC_PROCUREMENT_DOC")
    public JAXBElement<Empty> createACPROCUREMENTDOC(Empty value) {
        return new JAXBElement<Empty>(_ACPROCUREMENTDOC_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "RENEWAL")
    public JAXBElement<Empty> createRENEWAL(Empty value) {
        return new JAXBElement<Empty>(_RENEWAL_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "RENEWAL_DESCR")
    public JAXBElement<TextFtMultiLines> createRENEWALDESCR(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_RENEWALDESCR_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NO_RENEWAL")
    public JAXBElement<Empty> createNORENEWAL(Empty value) {
        return new JAXBElement<Empty>(_NORENEWAL_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NB_ENVISAGED_CANDIDATE")
    public JAXBElement<BigInteger> createNBENVISAGEDCANDIDATE(BigInteger value) {
        return new JAXBElement<BigInteger>(_NBENVISAGEDCANDIDATE_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NB_MIN_LIMIT_CANDIDATE")
    public JAXBElement<BigInteger> createNBMINLIMITCANDIDATE(BigInteger value) {
        return new JAXBElement<BigInteger>(_NBMINLIMITCANDIDATE_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NB_MAX_LIMIT_CANDIDATE")
    public JAXBElement<BigInteger> createNBMAXLIMITCANDIDATE(BigInteger value) {
        return new JAXBElement<BigInteger>(_NBMAXLIMITCANDIDATE_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CRITERIA_CANDIDATE")
    public JAXBElement<TextFtMultiLines> createCRITERIACANDIDATE(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_CRITERIACANDIDATE_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ACCEPTED_VARIANTS")
    public JAXBElement<Empty> createACCEPTEDVARIANTS(Empty value) {
        return new JAXBElement<Empty>(_ACCEPTEDVARIANTS_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NO_ACCEPTED_VARIANTS")
    public JAXBElement<Empty> createNOACCEPTEDVARIANTS(Empty value) {
        return new JAXBElement<Empty>(_NOACCEPTEDVARIANTS_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OPTIONS")
    public JAXBElement<Empty> createOPTIONS(Empty value) {
        return new JAXBElement<Empty>(_OPTIONS_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OPTIONS_DESCR")
    public JAXBElement<TextFtMultiLines> createOPTIONSDESCR(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_OPTIONSDESCR_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NO_OPTIONS")
    public JAXBElement<Empty> createNOOPTIONS(Empty value) {
        return new JAXBElement<Empty>(_NOOPTIONS_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ECATALOGUE_REQUIRED")
    public JAXBElement<Empty> createECATALOGUEREQUIRED(Empty value) {
        return new JAXBElement<Empty>(_ECATALOGUEREQUIRED_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "EU_PROGR_RELATED")
    public JAXBElement<TextFtMultiLines> createEUPROGRRELATED(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_EUPROGRRELATED_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NO_EU_PROGR_RELATED")
    public JAXBElement<Empty> createNOEUPROGRRELATED(Empty value) {
        return new JAXBElement<Empty>(_NOEUPROGRRELATED_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "INFO_ADD")
    public JAXBElement<TextFtMultiLines> createINFOADD(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_INFOADD_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DATE_PUBLICATION_NOTICE")
    public JAXBElement<XMLGregorianCalendar> createDATEPUBLICATIONNOTICE(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DATEPUBLICATIONNOTICE_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "SUITABILITY")
    public JAXBElement<TextFtMultiLines> createSUITABILITY(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_SUITABILITY_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ECONOMIC_CRITERIA_DOC")
    public JAXBElement<Empty> createECONOMICCRITERIADOC(Empty value) {
        return new JAXBElement<Empty>(_ECONOMICCRITERIADOC_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ECONOMIC_FINANCIAL_INFO")
    public JAXBElement<TextFtMultiLines> createECONOMICFINANCIALINFO(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_ECONOMICFINANCIALINFO_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ECONOMIC_FINANCIAL_MIN_LEVEL")
    public JAXBElement<TextFtMultiLines> createECONOMICFINANCIALMINLEVEL(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_ECONOMICFINANCIALMINLEVEL_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "TECHNICAL_CRITERIA_DOC")
    public JAXBElement<Empty> createTECHNICALCRITERIADOC(Empty value) {
        return new JAXBElement<Empty>(_TECHNICALCRITERIADOC_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "TECHNICAL_PROFESSIONAL_INFO")
    public JAXBElement<TextFtMultiLines> createTECHNICALPROFESSIONALINFO(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_TECHNICALPROFESSIONALINFO_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "TECHNICAL_PROFESSIONAL_MIN_LEVEL")
    public JAXBElement<TextFtMultiLines> createTECHNICALPROFESSIONALMINLEVEL(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_TECHNICALPROFESSIONALMINLEVEL_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "RULES_CRITERIA")
    public JAXBElement<TextFtMultiLines> createRULESCRITERIA(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_RULESCRITERIA_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "RESTRICTED_SHELTERED_WORKSHOP")
    public JAXBElement<Empty> createRESTRICTEDSHELTEREDWORKSHOP(Empty value) {
        return new JAXBElement<Empty>(_RESTRICTEDSHELTEREDWORKSHOP_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "RESTRICTED_SHELTERED_PROGRAM")
    public JAXBElement<Empty> createRESTRICTEDSHELTEREDPROGRAM(Empty value) {
        return new JAXBElement<Empty>(_RESTRICTEDSHELTEREDPROGRAM_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "RESERVED_ORGANISATIONS_SERVICE_MISSION")
    public JAXBElement<Empty> createRESERVEDORGANISATIONSSERVICEMISSION(Empty value) {
        return new JAXBElement<Empty>(_RESERVEDORGANISATIONSSERVICEMISSION_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DEPOSIT_GUARANTEE_REQUIRED")
    public JAXBElement<TextFtMultiLines> createDEPOSITGUARANTEEREQUIRED(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_DEPOSITGUARANTEEREQUIRED_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "MAIN_FINANCING_CONDITION")
    public JAXBElement<TextFtMultiLines> createMAINFINANCINGCONDITION(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_MAINFINANCINGCONDITION_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEGAL_FORM")
    public JAXBElement<TextFtMultiLines> createLEGALFORM(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_LEGALFORM_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CRITERIA_SELECTION")
    public JAXBElement<TextFtMultiLines> createCRITERIASELECTION(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_CRITERIASELECTION_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Services }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Services }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PARTICULAR_PROFESSION")
    public JAXBElement<Services> createPARTICULARPROFESSION(Services value) {
        return new JAXBElement<Services>(_PARTICULARPROFESSION_QNAME, Services.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "REFERENCE_TO_LAW")
    public JAXBElement<TextFtMultiLines> createREFERENCETOLAW(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_REFERENCETOLAW_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PERFORMANCE_CONDITIONS")
    public JAXBElement<TextFtMultiLines> createPERFORMANCECONDITIONS(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_PERFORMANCECONDITIONS_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PERFORMANCE_STAFF_QUALIFICATION")
    public JAXBElement<Empty> createPERFORMANCESTAFFQUALIFICATION(Empty value) {
        return new JAXBElement<Empty>(_PERFORMANCESTAFFQUALIFICATION_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_AWARD_CONTRACT_WITH_PRIOR_PUBLICATION")
    public JAXBElement<Empty> createPTAWARDCONTRACTWITHPRIORPUBLICATION(Empty value) {
        return new JAXBElement<Empty>(_PTAWARDCONTRACTWITHPRIORPUBLICATION_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_AWARD_CONTRACT_WITHOUT_CALL")
    public JAXBElement<Empty> createPTAWARDCONTRACTWITHOUTCALL(Empty value) {
        return new JAXBElement<Empty>(_PTAWARDCONTRACTWITHOUTCALL_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_AWARD_CONTRACT_WITHOUT_PUBLICATION")
    public JAXBElement<Empty> createPTAWARDCONTRACTWITHOUTPUBLICATION(Empty value) {
        return new JAXBElement<Empty>(_PTAWARDCONTRACTWITHOUTPUBLICATION_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_COMPETITIVE_DIALOGUE")
    public JAXBElement<Empty> createPTCOMPETITIVEDIALOGUE(Empty value) {
        return new JAXBElement<Empty>(_PTCOMPETITIVEDIALOGUE_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_COMPETITIVE_NEGOTIATION")
    public JAXBElement<Empty> createPTCOMPETITIVENEGOTIATION(Empty value) {
        return new JAXBElement<Empty>(_PTCOMPETITIVENEGOTIATION_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_INNOVATION_PARTNERSHIP")
    public JAXBElement<Empty> createPTINNOVATIONPARTNERSHIP(Empty value) {
        return new JAXBElement<Empty>(_PTINNOVATIONPARTNERSHIP_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_INVOLVING_NEGOTIATION")
    public JAXBElement<Empty> createPTINVOLVINGNEGOTIATION(Empty value) {
        return new JAXBElement<Empty>(_PTINVOLVINGNEGOTIATION_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_NEGOTIATED_WITH_PRIOR_CALL")
    public JAXBElement<Empty> createPTNEGOTIATEDWITHPRIORCALL(Empty value) {
        return new JAXBElement<Empty>(_PTNEGOTIATEDWITHPRIORCALL_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_NEGOTIATED_WITHOUT_PUBLICATION")
    public JAXBElement<Empty> createPTNEGOTIATEDWITHOUTPUBLICATION(Empty value) {
        return new JAXBElement<Empty>(_PTNEGOTIATEDWITHOUTPUBLICATION_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_OPEN")
    public JAXBElement<Empty> createPTOPEN(Empty value) {
        return new JAXBElement<Empty>(_PTOPEN_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_RESTRICTED")
    public JAXBElement<Empty> createPTRESTRICTED(Empty value) {
        return new JAXBElement<Empty>(_PTRESTRICTED_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_01_AOO")
    public JAXBElement<Empty> createPT01AOO(Empty value) {
        return new JAXBElement<Empty>(_PT01AOO_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_03_MAPA")
    public JAXBElement<Empty> createPT03MAPA(Empty value) {
        return new JAXBElement<Empty>(_PT03MAPA_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_04_Concours")
    public JAXBElement<Empty> createPT04Concours(Empty value) {
        return new JAXBElement<Empty>(_PT04Concours_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_12_Autre")
    public JAXBElement<Empty> createPT12Autre(Empty value) {
        return new JAXBElement<Empty>(_PT12Autre_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_13_AccordCadreSelection")
    public JAXBElement<Empty> createPT13AccordCadreSelection(Empty value) {
        return new JAXBElement<Empty>(_PT13AccordCadreSelection_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_15_SAD")
    public JAXBElement<Empty> createPT15SAD(Empty value) {
        return new JAXBElement<Empty>(_PT15SAD_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_08_MarcheNegocie")
    public JAXBElement<Empty> createPT08MarcheNegocie(Empty value) {
        return new JAXBElement<Empty>(_PT08MarcheNegocie_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_18_PCN")
    public JAXBElement<Empty> createPT18PCN(Empty value) {
        return new JAXBElement<Empty>(_PT18PCN_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_06_DialogueCompetitif")
    public JAXBElement<Empty> createPT06DialogueCompetitif(Empty value) {
        return new JAXBElement<Empty>(_PT06DialogueCompetitif_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_02_AOR")
    public JAXBElement<Empty> createPT02AOR(Empty value) {
        return new JAXBElement<Empty>(_PT02AOR_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_05_ConcoursRestreint")
    public JAXBElement<Empty> createPT05ConcoursRestreint(Empty value) {
        return new JAXBElement<Empty>(_PT05ConcoursRestreint_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_14_MarcheSubsequent")
    public JAXBElement<Empty> createPT14MarcheSubsequent(Empty value) {
        return new JAXBElement<Empty>(_PT14MarcheSubsequent_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_16_MarcheSpecifique")
    public JAXBElement<Empty> createPT16MarcheSpecifique(Empty value) {
        return new JAXBElement<Empty>(_PT16MarcheSpecifique_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ACCELERATED_PROC")
    public JAXBElement<TextFtMultiLines> createACCELERATEDPROC(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_ACCELERATEDPROC_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NB_PARTICIPANTS")
    public JAXBElement<BigInteger> createNBPARTICIPANTS(BigInteger value) {
        return new JAXBElement<BigInteger>(_NBPARTICIPANTS_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "JUSTIFICATION")
    public JAXBElement<TextFtMultiLines> createJUSTIFICATION(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_JUSTIFICATION_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "SEVERAL_OPERATORS")
    public JAXBElement<Empty> createSEVERALOPERATORS(Empty value) {
        return new JAXBElement<Empty>(_SEVERALOPERATORS_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "SINGLE_OPERATOR")
    public JAXBElement<Empty> createSINGLEOPERATOR(Empty value) {
        return new JAXBElement<Empty>(_SINGLEOPERATOR_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PO")
    public JAXBElement<Empty> createPO(Empty value) {
        return new JAXBElement<Empty>(_PO_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PR")
    public JAXBElement<Empty> createPR(Empty value) {
        return new JAXBElement<Empty>(_PR_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DPS")
    public JAXBElement<Empty> createDPS(Empty value) {
        return new JAXBElement<Empty>(_DPS_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CRITERIA_EVALUATION")
    public JAXBElement<TextFtMultiLines> createCRITERIAEVALUATION(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_CRITERIAEVALUATION_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "REDUCTION_RECOURSE")
    public JAXBElement<Empty> createREDUCTIONRECOURSE(Empty value) {
        return new JAXBElement<Empty>(_REDUCTIONRECOURSE_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "RIGHT_CONTRACT_INITIAL_TENDERS")
    public JAXBElement<Empty> createRIGHTCONTRACTINITIALTENDERS(Empty value) {
        return new JAXBElement<Empty>(_RIGHTCONTRACTINITIALTENDERS_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "EAUCTION_USED")
    public JAXBElement<Empty> createEAUCTIONUSED(Empty value) {
        return new JAXBElement<Empty>(_EAUCTIONUSED_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "INFO_ADD_EAUCTION")
    public JAXBElement<TextFtMultiLines> createINFOADDEAUCTION(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_INFOADDEAUCTION_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTRACT_COVERED_GPA")
    public JAXBElement<Empty> createCONTRACTCOVEREDGPA(Empty value) {
        return new JAXBElement<Empty>(_CONTRACTCOVEREDGPA_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NO_CONTRACT_COVERED_GPA")
    public JAXBElement<Empty> createNOCONTRACTCOVEREDGPA(Empty value) {
        return new JAXBElement<Empty>(_NOCONTRACTCOVEREDGPA_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "URL_NATIONAL_PROCEDURE")
    public JAXBElement<String> createURLNATIONALPROCEDURE(String value) {
        return new JAXBElement<String>(_URLNATIONALPROCEDURE_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "MAIN_FEATURES_AWARD")
    public JAXBElement<TextFtMultiLines> createMAINFEATURESAWARD(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_MAINFEATURESAWARD_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NOTICE_NUMBER_OJ")
    public JAXBElement<String> createNOTICENUMBEROJ(String value) {
        return new JAXBElement<String>(_NOTICENUMBEROJ_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DATE_RECEIPT_TENDERS")
    public JAXBElement<XMLGregorianCalendar> createDATERECEIPTTENDERS(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DATERECEIPTTENDERS_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "TIME_RECEIPT_TENDERS")
    public JAXBElement<String> createTIMERECEIPTTENDERS(String value) {
        return new JAXBElement<String>(_TIMERECEIPTTENDERS_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ATTRIB_SANS_NEGO")
    public JAXBElement<Empty> createATTRIBSANSNEGO(Empty value) {
        return new JAXBElement<Empty>(_ATTRIBSANSNEGO_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NO_ATTRIB_SANS_NEGO")
    public JAXBElement<Empty> createNOATTRIBSANSNEGO(Empty value) {
        return new JAXBElement<Empty>(_NOATTRIBSANSNEGO_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NO_VISITE_OBLIGATOIRE")
    public JAXBElement<Empty> createNOVISITEOBLIGATOIRE(Empty value) {
        return new JAXBElement<Empty>(_NOVISITEOBLIGATOIRE_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DATE_DISPATCH_INVITATIONS")
    public JAXBElement<XMLGregorianCalendar> createDATEDISPATCHINVITATIONS(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DATEDISPATCHINVITATIONS_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NB_CANDIDATS_MAX")
    public JAXBElement<BigInteger> createNBCANDIDATSMAX(BigInteger value) {
        return new JAXBElement<BigInteger>(_NBCANDIDATSMAX_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DATE_AWARD_SCHEDULED")
    public JAXBElement<XMLGregorianCalendar> createDATEAWARDSCHEDULED(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DATEAWARDSCHEDULED_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CondForOpeningTenders }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CondForOpeningTenders }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OPENING_CONDITION")
    public JAXBElement<CondForOpeningTenders> createOPENINGCONDITION(CondForOpeningTenders value) {
        return new JAXBElement<CondForOpeningTenders>(_OPENINGCONDITION_QNAME, CondForOpeningTenders.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "TERMINATION_DPS")
    public JAXBElement<Empty> createTERMINATIONDPS(Empty value) {
        return new JAXBElement<Empty>(_TERMINATIONDPS_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "TERMINATION_PIN")
    public JAXBElement<Empty> createTERMINATIONPIN(Empty value) {
        return new JAXBElement<Empty>(_TERMINATIONPIN_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTRACT_NO")
    public JAXBElement<String> createCONTRACTNO(String value) {
        return new JAXBElement<String>(_CONTRACTNO_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PROCUREMENT_UNSUCCESSFUL")
    public JAXBElement<Empty> createPROCUREMENTUNSUCCESSFUL(Empty value) {
        return new JAXBElement<Empty>(_PROCUREMENTUNSUCCESSFUL_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoAward }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link NoAward }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NO_AWARDED_CONTRACT")
    public JAXBElement<NoAward> createNOAWARDEDCONTRACT(NoAward value) {
        return new JAXBElement<NoAward>(_NOAWARDEDCONTRACT_QNAME, NoAward.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DATE_CONCLUSION_CONTRACT")
    public JAXBElement<XMLGregorianCalendar> createDATECONCLUSIONCONTRACT(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DATECONCLUSIONCONTRACT_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NB_TENDERS_RECEIVED")
    public JAXBElement<BigInteger> createNBTENDERSRECEIVED(BigInteger value) {
        return new JAXBElement<BigInteger>(_NBTENDERSRECEIVED_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NB_TENDERS_RECEIVED_SME")
    public JAXBElement<BigInteger> createNBTENDERSRECEIVEDSME(BigInteger value) {
        return new JAXBElement<BigInteger>(_NBTENDERSRECEIVEDSME_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NB_TENDERS_RECEIVED_OTHER_EU")
    public JAXBElement<BigInteger> createNBTENDERSRECEIVEDOTHEREU(BigInteger value) {
        return new JAXBElement<BigInteger>(_NBTENDERSRECEIVEDOTHEREU_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NB_TENDERS_RECEIVED_NON_EU")
    public JAXBElement<BigInteger> createNBTENDERSRECEIVEDNONEU(BigInteger value) {
        return new JAXBElement<BigInteger>(_NBTENDERSRECEIVEDNONEU_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NB_TENDERS_RECEIVED_EMEANS")
    public JAXBElement<BigInteger> createNBTENDERSRECEIVEDEMEANS(BigInteger value) {
        return new JAXBElement<BigInteger>(_NBTENDERSRECEIVEDEMEANS_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactContractor }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ContactContractor }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ADDRESS_CONTRACTOR")
    public JAXBElement<ContactContractor> createADDRESSCONTRACTOR(ContactContractor value) {
        return new JAXBElement<ContactContractor>(_ADDRESSCONTRACTOR_QNAME, ContactContractor.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "SME")
    public JAXBElement<Empty> createSME(Empty value) {
        return new JAXBElement<Empty>(_SME_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NO_SME")
    public JAXBElement<Empty> createNOSME(Empty value) {
        return new JAXBElement<Empty>(_NOSME_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "AWARDED_TO_GROUP")
    public JAXBElement<Empty> createAWARDEDTOGROUP(Empty value) {
        return new JAXBElement<Empty>(_AWARDEDTOGROUP_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NO_AWARDED_TO_GROUP")
    public JAXBElement<Empty> createNOAWARDEDTOGROUP(Empty value) {
        return new JAXBElement<Empty>(_NOAWARDEDTOGROUP_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LIKELY_SUBCONTRACTED")
    public JAXBElement<Empty> createLIKELYSUBCONTRACTED(Empty value) {
        return new JAXBElement<Empty>(_LIKELYSUBCONTRACTED_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "AWARDED_SUBCONTRACTING")
    public JAXBElement<Empty> createAWARDEDSUBCONTRACTING(Empty value) {
        return new JAXBElement<Empty>(_AWARDEDSUBCONTRACTING_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrctRange }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link PrctRange }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PCT_RANGE_SHARE_SUBCONTRACTING")
    public JAXBElement<PrctRange> createPCTRANGESHARESUBCONTRACTING(PrctRange value) {
        return new JAXBElement<PrctRange>(_PCTRANGESHARESUBCONTRACTING_QNAME, PrctRange.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "RECURRENT_PROCUREMENT")
    public JAXBElement<Empty> createRECURRENTPROCUREMENT(Empty value) {
        return new JAXBElement<Empty>(_RECURRENTPROCUREMENT_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ESTIMATED_TIMING")
    public JAXBElement<TextFtMultiLines> createESTIMATEDTIMING(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_ESTIMATEDTIMING_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NO_RECURRENT_PROCUREMENT")
    public JAXBElement<Empty> createNORECURRENTPROCUREMENT(Empty value) {
        return new JAXBElement<Empty>(_NORECURRENTPROCUREMENT_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "EORDERING")
    public JAXBElement<Empty> createEORDERING(Empty value) {
        return new JAXBElement<Empty>(_EORDERING_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "EINVOICING")
    public JAXBElement<Empty> createEINVOICING(Empty value) {
        return new JAXBElement<Empty>(_EINVOICING_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "EPAYMENT")
    public JAXBElement<Empty> createEPAYMENT(Empty value) {
        return new JAXBElement<Empty>(_EPAYMENT_QNAME, Empty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactReview }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ContactReview }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ADDRESS_REVIEW_BODY")
    public JAXBElement<ContactReview> createADDRESSREVIEWBODY(ContactReview value) {
        return new JAXBElement<ContactReview>(_ADDRESSREVIEWBODY_QNAME, ContactReview.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactReview }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ContactReview }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ADDRESS_MEDIATION_BODY")
    public JAXBElement<ContactReview> createADDRESSMEDIATIONBODY(ContactReview value) {
        return new JAXBElement<ContactReview>(_ADDRESSMEDIATIONBODY_QNAME, ContactReview.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "REVIEW_PROCEDURE")
    public JAXBElement<TextFtMultiLines> createREVIEWPROCEDURE(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_REVIEWPROCEDURE_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactReview }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ContactReview }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ADDRESS_REVIEW_INFO")
    public JAXBElement<ContactReview> createADDRESSREVIEWINFO(ContactReview value) {
        return new JAXBElement<ContactReview>(_ADDRESSREVIEWINFO_QNAME, ContactReview.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DATE_DISPATCH_NOTICE")
    public JAXBElement<XMLGregorianCalendar> createDATEDISPATCHNOTICE(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DATEDISPATCHNOTICE_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ADDITIONAL_NEED")
    public JAXBElement<TextFtMultiLines> createADDITIONALNEED(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_ADDITIONALNEED_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "UNFORESEEN_CIRCUMSTANCE")
    public JAXBElement<TextFtMultiLines> createUNFORESEENCIRCUMSTANCE(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_UNFORESEENCIRCUMSTANCE_QNAME, TextFtMultiLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Val }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Val }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "VAL_TOTAL_BEFORE")
    public JAXBElement<Val> createVALTOTALBEFORE(Val value) {
        return new JAXBElement<Val>(_VALTOTALBEFORE_QNAME, Val.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Val }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Val }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "VAL_TOTAL_AFTER")
    public JAXBElement<Val> createVALTOTALAFTER(Val value) {
        return new JAXBElement<Val>(_VALTOTALAFTER_QNAME, Val.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TedEsenders }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TedEsenders }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "TED_ESENDERS")
    public JAXBElement<TedEsenders> createTEDESENDERS(TedEsenders value) {
        return new JAXBElement<TedEsenders>(_TEDESENDERS_QNAME, TedEsenders.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LegalBasisF01 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LegalBasisF01 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEGAL_BASIS", scope = F012014.class)
    public JAXBElement<LegalBasisF01> createF012014LEGALBASIS(LegalBasisF01 value) {
        return new JAXBElement<LegalBasisF01>(_F012014LEGALBASIS_QNAME, LegalBasisF01.class, F012014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoticeF01 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link NoticeF01 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NOTICE", scope = F012014.class)
    public JAXBElement<NoticeF01> createF012014NOTICE(NoticeF01 value) {
        return new JAXBElement<NoticeF01>(_F012014NOTICE_QNAME, NoticeF01.class, F012014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BodyF01 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BodyF01 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTRACTING_BODY", scope = F012014.class)
    public JAXBElement<BodyF01> createF012014CONTRACTINGBODY(BodyF01 value) {
        return new JAXBElement<BodyF01>(_F012014CONTRACTINGBODY_QNAME, BodyF01.class, F012014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF01 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF01 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_CONTRACT", scope = F012014.class)
    public JAXBElement<ObjectContractF01> createF012014OBJECTCONTRACT(ObjectContractF01 value) {
        return new JAXBElement<ObjectContractF01>(_F012014OBJECTCONTRACT_QNAME, ObjectContractF01.class, F012014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LeftiF01 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LeftiF01 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEFTI", scope = F012014.class)
    public JAXBElement<LeftiF01> createF012014LEFTI(LeftiF01 value) {
        return new JAXBElement<LeftiF01>(_F012014LEFTI_QNAME, LeftiF01.class, F012014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcedureF01 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ProcedureF01 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PROCEDURE", scope = F012014.class)
    public JAXBElement<ProcedureF01> createF012014PROCEDURE(ProcedureF01 value) {
        return new JAXBElement<ProcedureF01>(_F012014PROCEDURE_QNAME, ProcedureF01.class, F012014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CiF01 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CiF01 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "COMPLEMENTARY_INFO", scope = F012014.class)
    public JAXBElement<CiF01> createF012014COMPLEMENTARYINFO(CiF01 value) {
        return new JAXBElement<CiF01>(_F012014COMPLEMENTARYINFO_QNAME, CiF01.class, F012014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LegalBasisF02 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LegalBasisF02 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEGAL_BASIS", scope = F022014.class)
    public JAXBElement<LegalBasisF02> createF022014LEGALBASIS(LegalBasisF02 value) {
        return new JAXBElement<LegalBasisF02>(_F012014LEGALBASIS_QNAME, LegalBasisF02.class, F022014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BodyF02 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BodyF02 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTRACTING_BODY", scope = F022014.class)
    public JAXBElement<BodyF02> createF022014CONTRACTINGBODY(BodyF02 value) {
        return new JAXBElement<BodyF02>(_F012014CONTRACTINGBODY_QNAME, BodyF02.class, F022014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF02 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF02 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_CONTRACT", scope = F022014.class)
    public JAXBElement<ObjectContractF02> createF022014OBJECTCONTRACT(ObjectContractF02 value) {
        return new JAXBElement<ObjectContractF02>(_F012014OBJECTCONTRACT_QNAME, ObjectContractF02.class, F022014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LeftiF02 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LeftiF02 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEFTI", scope = F022014.class)
    public JAXBElement<LeftiF02> createF022014LEFTI(LeftiF02 value) {
        return new JAXBElement<LeftiF02>(_F012014LEFTI_QNAME, LeftiF02.class, F022014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcedureF02 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ProcedureF02 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PROCEDURE", scope = F022014.class)
    public JAXBElement<ProcedureF02> createF022014PROCEDURE(ProcedureF02 value) {
        return new JAXBElement<ProcedureF02>(_F012014PROCEDURE_QNAME, ProcedureF02.class, F022014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CiF02 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CiF02 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "COMPLEMENTARY_INFO", scope = F022014.class)
    public JAXBElement<CiF02> createF022014COMPLEMENTARYINFO(CiF02 value) {
        return new JAXBElement<CiF02>(_F012014COMPLEMENTARYINFO_QNAME, CiF02.class, F022014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LegalBasisF03 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LegalBasisF03 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEGAL_BASIS", scope = F032014.class)
    public JAXBElement<LegalBasisF03> createF032014LEGALBASIS(LegalBasisF03 value) {
        return new JAXBElement<LegalBasisF03>(_F012014LEGALBASIS_QNAME, LegalBasisF03.class, F032014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BodyF03 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BodyF03 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTRACTING_BODY", scope = F032014.class)
    public JAXBElement<BodyF03> createF032014CONTRACTINGBODY(BodyF03 value) {
        return new JAXBElement<BodyF03>(_F012014CONTRACTINGBODY_QNAME, BodyF03.class, F032014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF03 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF03 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_CONTRACT", scope = F032014.class)
    public JAXBElement<ObjectContractF03> createF032014OBJECTCONTRACT(ObjectContractF03 value) {
        return new JAXBElement<ObjectContractF03>(_F012014OBJECTCONTRACT_QNAME, ObjectContractF03.class, F032014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcedureF03 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ProcedureF03 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PROCEDURE", scope = F032014.class)
    public JAXBElement<ProcedureF03> createF032014PROCEDURE(ProcedureF03 value) {
        return new JAXBElement<ProcedureF03>(_F012014PROCEDURE_QNAME, ProcedureF03.class, F032014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AwardContractF03 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link AwardContractF03 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "AWARD_CONTRACT", scope = F032014.class)
    public JAXBElement<AwardContractF03> createF032014AWARDCONTRACT(AwardContractF03 value) {
        return new JAXBElement<AwardContractF03>(_F032014AWARDCONTRACT_QNAME, AwardContractF03.class, F032014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CiF03 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CiF03 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "COMPLEMENTARY_INFO", scope = F032014.class)
    public JAXBElement<CiF03> createF032014COMPLEMENTARYINFO(CiF03 value) {
        return new JAXBElement<CiF03>(_F012014COMPLEMENTARYINFO_QNAME, CiF03.class, F032014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LegalBasisF04 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LegalBasisF04 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEGAL_BASIS", scope = F042014.class)
    public JAXBElement<LegalBasisF04> createF042014LEGALBASIS(LegalBasisF04 value) {
        return new JAXBElement<LegalBasisF04>(_F012014LEGALBASIS_QNAME, LegalBasisF04.class, F042014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoticeF04 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link NoticeF04 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NOTICE", scope = F042014.class)
    public JAXBElement<NoticeF04> createF042014NOTICE(NoticeF04 value) {
        return new JAXBElement<NoticeF04>(_F012014NOTICE_QNAME, NoticeF04.class, F042014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BodyF04 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BodyF04 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTRACTING_BODY", scope = F042014.class)
    public JAXBElement<BodyF04> createF042014CONTRACTINGBODY(BodyF04 value) {
        return new JAXBElement<BodyF04>(_F012014CONTRACTINGBODY_QNAME, BodyF04.class, F042014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF04 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF04 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_CONTRACT", scope = F042014.class)
    public JAXBElement<ObjectContractF04> createF042014OBJECTCONTRACT(ObjectContractF04 value) {
        return new JAXBElement<ObjectContractF04>(_F012014OBJECTCONTRACT_QNAME, ObjectContractF04.class, F042014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LeftiF04 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LeftiF04 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEFTI", scope = F042014.class)
    public JAXBElement<LeftiF04> createF042014LEFTI(LeftiF04 value) {
        return new JAXBElement<LeftiF04>(_F012014LEFTI_QNAME, LeftiF04.class, F042014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcedureF04 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ProcedureF04 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PROCEDURE", scope = F042014.class)
    public JAXBElement<ProcedureF04> createF042014PROCEDURE(ProcedureF04 value) {
        return new JAXBElement<ProcedureF04>(_F012014PROCEDURE_QNAME, ProcedureF04.class, F042014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CiF04 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CiF04 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "COMPLEMENTARY_INFO", scope = F042014.class)
    public JAXBElement<CiF04> createF042014COMPLEMENTARYINFO(CiF04 value) {
        return new JAXBElement<CiF04>(_F012014COMPLEMENTARYINFO_QNAME, CiF04.class, F042014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LegalBasisF05 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LegalBasisF05 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEGAL_BASIS", scope = F052014.class)
    public JAXBElement<LegalBasisF05> createF052014LEGALBASIS(LegalBasisF05 value) {
        return new JAXBElement<LegalBasisF05>(_F012014LEGALBASIS_QNAME, LegalBasisF05.class, F052014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BodyF05 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BodyF05 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTRACTING_BODY", scope = F052014.class)
    public JAXBElement<BodyF05> createF052014CONTRACTINGBODY(BodyF05 value) {
        return new JAXBElement<BodyF05>(_F012014CONTRACTINGBODY_QNAME, BodyF05.class, F052014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF05 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF05 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_CONTRACT", scope = F052014.class)
    public JAXBElement<ObjectContractF05> createF052014OBJECTCONTRACT(ObjectContractF05 value) {
        return new JAXBElement<ObjectContractF05>(_F012014OBJECTCONTRACT_QNAME, ObjectContractF05.class, F052014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LeftiF05 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LeftiF05 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEFTI", scope = F052014.class)
    public JAXBElement<LeftiF05> createF052014LEFTI(LeftiF05 value) {
        return new JAXBElement<LeftiF05>(_F012014LEFTI_QNAME, LeftiF05.class, F052014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcedureF05 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ProcedureF05 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PROCEDURE", scope = F052014.class)
    public JAXBElement<ProcedureF05> createF052014PROCEDURE(ProcedureF05 value) {
        return new JAXBElement<ProcedureF05>(_F012014PROCEDURE_QNAME, ProcedureF05.class, F052014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CiF05 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CiF05 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "COMPLEMENTARY_INFO", scope = F052014.class)
    public JAXBElement<CiF05> createF052014COMPLEMENTARYINFO(CiF05 value) {
        return new JAXBElement<CiF05>(_F012014COMPLEMENTARYINFO_QNAME, CiF05.class, F052014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LegalBasisF06 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LegalBasisF06 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEGAL_BASIS", scope = F062014.class)
    public JAXBElement<LegalBasisF06> createF062014LEGALBASIS(LegalBasisF06 value) {
        return new JAXBElement<LegalBasisF06>(_F012014LEGALBASIS_QNAME, LegalBasisF06.class, F062014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BodyF06 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BodyF06 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTRACTING_BODY", scope = F062014.class)
    public JAXBElement<BodyF06> createF062014CONTRACTINGBODY(BodyF06 value) {
        return new JAXBElement<BodyF06>(_F012014CONTRACTINGBODY_QNAME, BodyF06.class, F062014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF06 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF06 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_CONTRACT", scope = F062014.class)
    public JAXBElement<ObjectContractF06> createF062014OBJECTCONTRACT(ObjectContractF06 value) {
        return new JAXBElement<ObjectContractF06>(_F012014OBJECTCONTRACT_QNAME, ObjectContractF06.class, F062014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcedureF06 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ProcedureF06 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PROCEDURE", scope = F062014.class)
    public JAXBElement<ProcedureF06> createF062014PROCEDURE(ProcedureF06 value) {
        return new JAXBElement<ProcedureF06>(_F012014PROCEDURE_QNAME, ProcedureF06.class, F062014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AwardContractF06 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link AwardContractF06 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "AWARD_CONTRACT", scope = F062014.class)
    public JAXBElement<AwardContractF06> createF062014AWARDCONTRACT(AwardContractF06 value) {
        return new JAXBElement<AwardContractF06>(_F032014AWARDCONTRACT_QNAME, AwardContractF06.class, F062014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CiF06 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CiF06 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "COMPLEMENTARY_INFO", scope = F062014.class)
    public JAXBElement<CiF06> createF062014COMPLEMENTARYINFO(CiF06 value) {
        return new JAXBElement<CiF06>(_F012014COMPLEMENTARYINFO_QNAME, CiF06.class, F062014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LegalBasisF07 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LegalBasisF07 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEGAL_BASIS", scope = F072014.class)
    public JAXBElement<LegalBasisF07> createF072014LEGALBASIS(LegalBasisF07 value) {
        return new JAXBElement<LegalBasisF07>(_F012014LEGALBASIS_QNAME, LegalBasisF07.class, F072014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoticeF07 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link NoticeF07 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NOTICE", scope = F072014.class)
    public JAXBElement<NoticeF07> createF072014NOTICE(NoticeF07 value) {
        return new JAXBElement<NoticeF07>(_F012014NOTICE_QNAME, NoticeF07.class, F072014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BodyF07 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BodyF07 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTRACTING_BODY", scope = F072014.class)
    public JAXBElement<BodyF07> createF072014CONTRACTINGBODY(BodyF07 value) {
        return new JAXBElement<BodyF07>(_F012014CONTRACTINGBODY_QNAME, BodyF07.class, F072014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF07 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF07 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_CONTRACT", scope = F072014.class)
    public JAXBElement<ObjectContractF07> createF072014OBJECTCONTRACT(ObjectContractF07 value) {
        return new JAXBElement<ObjectContractF07>(_F012014OBJECTCONTRACT_QNAME, ObjectContractF07.class, F072014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LeftiF07 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LeftiF07 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEFTI", scope = F072014.class)
    public JAXBElement<LeftiF07> createF072014LEFTI(LeftiF07 value) {
        return new JAXBElement<LeftiF07>(_F012014LEFTI_QNAME, LeftiF07.class, F072014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcedureF07 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ProcedureF07 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PROCEDURE", scope = F072014.class)
    public JAXBElement<ProcedureF07> createF072014PROCEDURE(ProcedureF07 value) {
        return new JAXBElement<ProcedureF07>(_F012014PROCEDURE_QNAME, ProcedureF07.class, F072014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CiF07 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CiF07 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "COMPLEMENTARY_INFO", scope = F072014.class)
    public JAXBElement<CiF07> createF072014COMPLEMENTARYINFO(CiF07 value) {
        return new JAXBElement<CiF07>(_F012014COMPLEMENTARYINFO_QNAME, CiF07.class, F072014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LegalBasisF08 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LegalBasisF08 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEGAL_BASIS", scope = F082014.class)
    public JAXBElement<LegalBasisF08> createF082014LEGALBASIS(LegalBasisF08 value) {
        return new JAXBElement<LegalBasisF08>(_F012014LEGALBASIS_QNAME, LegalBasisF08.class, F082014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BodyF08 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BodyF08 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTRACTING_BODY", scope = F082014.class)
    public JAXBElement<BodyF08> createF082014CONTRACTINGBODY(BodyF08 value) {
        return new JAXBElement<BodyF08>(_F012014CONTRACTINGBODY_QNAME, BodyF08.class, F082014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF08 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF08 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_CONTRACT", scope = F082014.class)
    public JAXBElement<ObjectContractF08> createF082014OBJECTCONTRACT(ObjectContractF08 value) {
        return new JAXBElement<ObjectContractF08>(_F012014OBJECTCONTRACT_QNAME, ObjectContractF08.class, F082014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CiF08 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CiF08 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "COMPLEMENTARY_INFO", scope = F082014.class)
    public JAXBElement<CiF08> createF082014COMPLEMENTARYINFO(CiF08 value) {
        return new JAXBElement<CiF08>(_F012014COMPLEMENTARYINFO_QNAME, CiF08.class, F082014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LegalBasisF12 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LegalBasisF12 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEGAL_BASIS", scope = F122014.class)
    public JAXBElement<LegalBasisF12> createF122014LEGALBASIS(LegalBasisF12 value) {
        return new JAXBElement<LegalBasisF12>(_F012014LEGALBASIS_QNAME, LegalBasisF12.class, F122014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BodyF12 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BodyF12 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTRACTING_BODY", scope = F122014.class)
    public JAXBElement<BodyF12> createF122014CONTRACTINGBODY(BodyF12 value) {
        return new JAXBElement<BodyF12>(_F012014CONTRACTINGBODY_QNAME, BodyF12.class, F122014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF12 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF12 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_CONTRACT", scope = F122014.class)
    public JAXBElement<ObjectContractF12> createF122014OBJECTCONTRACT(ObjectContractF12 value) {
        return new JAXBElement<ObjectContractF12>(_F012014OBJECTCONTRACT_QNAME, ObjectContractF12.class, F122014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LeftiF12 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LeftiF12 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEFTI", scope = F122014.class)
    public JAXBElement<LeftiF12> createF122014LEFTI(LeftiF12 value) {
        return new JAXBElement<LeftiF12>(_F012014LEFTI_QNAME, LeftiF12.class, F122014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcedureF12 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ProcedureF12 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PROCEDURE", scope = F122014.class)
    public JAXBElement<ProcedureF12> createF122014PROCEDURE(ProcedureF12 value) {
        return new JAXBElement<ProcedureF12>(_F012014PROCEDURE_QNAME, ProcedureF12.class, F122014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CiF12 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CiF12 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "COMPLEMENTARY_INFO", scope = F122014.class)
    public JAXBElement<CiF12> createF122014COMPLEMENTARYINFO(CiF12 value) {
        return new JAXBElement<CiF12>(_F012014COMPLEMENTARYINFO_QNAME, CiF12.class, F122014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LegalBasisF13 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LegalBasisF13 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEGAL_BASIS", scope = F132014.class)
    public JAXBElement<LegalBasisF13> createF132014LEGALBASIS(LegalBasisF13 value) {
        return new JAXBElement<LegalBasisF13>(_F012014LEGALBASIS_QNAME, LegalBasisF13.class, F132014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BodyF13 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BodyF13 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTRACTING_BODY", scope = F132014.class)
    public JAXBElement<BodyF13> createF132014CONTRACTINGBODY(BodyF13 value) {
        return new JAXBElement<BodyF13>(_F012014CONTRACTINGBODY_QNAME, BodyF13.class, F132014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF13 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF13 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_CONTRACT", scope = F132014.class)
    public JAXBElement<ObjectContractF13> createF132014OBJECTCONTRACT(ObjectContractF13 value) {
        return new JAXBElement<ObjectContractF13>(_F012014OBJECTCONTRACT_QNAME, ObjectContractF13.class, F132014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcedureF13 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ProcedureF13 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PROCEDURE", scope = F132014.class)
    public JAXBElement<ProcedureF13> createF132014PROCEDURE(ProcedureF13 value) {
        return new JAXBElement<ProcedureF13>(_F012014PROCEDURE_QNAME, ProcedureF13.class, F132014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResultsF13 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ResultsF13 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "RESULTS", scope = F132014.class)
    public JAXBElement<ResultsF13> createF132014RESULTS(ResultsF13 value) {
        return new JAXBElement<ResultsF13>(_F132014RESULTS_QNAME, ResultsF13.class, F132014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CiF13 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CiF13 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "COMPLEMENTARY_INFO", scope = F132014.class)
    public JAXBElement<CiF13> createF132014COMPLEMENTARYINFO(CiF13 value) {
        return new JAXBElement<CiF13>(_F012014COMPLEMENTARYINFO_QNAME, CiF13.class, F132014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LegalBasisF14 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LegalBasisF14 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEGAL_BASIS", scope = F142014.class)
    public JAXBElement<LegalBasisF14> createF142014LEGALBASIS(LegalBasisF14 value) {
        return new JAXBElement<LegalBasisF14>(_F012014LEGALBASIS_QNAME, LegalBasisF14.class, F142014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BodyF14 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BodyF14 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTRACTING_BODY", scope = F142014.class)
    public JAXBElement<BodyF14> createF142014CONTRACTINGBODY(BodyF14 value) {
        return new JAXBElement<BodyF14>(_F012014CONTRACTINGBODY_QNAME, BodyF14.class, F142014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF14 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF14 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_CONTRACT", scope = F142014.class)
    public JAXBElement<ObjectContractF14> createF142014OBJECTCONTRACT(ObjectContractF14 value) {
        return new JAXBElement<ObjectContractF14>(_F012014OBJECTCONTRACT_QNAME, ObjectContractF14.class, F142014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CiF14 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CiF14 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "COMPLEMENTARY_INFO", scope = F142014.class)
    public JAXBElement<CiF14> createF142014COMPLEMENTARYINFO(CiF14 value) {
        return new JAXBElement<CiF14>(_F012014COMPLEMENTARYINFO_QNAME, CiF14.class, F142014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangesF14 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ChangesF14 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CHANGES", scope = F142014.class)
    public JAXBElement<ChangesF14> createF142014CHANGES(ChangesF14 value) {
        return new JAXBElement<ChangesF14>(_F142014CHANGES_QNAME, ChangesF14.class, F142014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LegalBasisF20 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LegalBasisF20 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEGAL_BASIS", scope = F202014.class)
    public JAXBElement<LegalBasisF20> createF202014LEGALBASIS(LegalBasisF20 value) {
        return new JAXBElement<LegalBasisF20>(_F012014LEGALBASIS_QNAME, LegalBasisF20.class, F202014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BodyF20 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BodyF20 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTRACTING_BODY", scope = F202014.class)
    public JAXBElement<BodyF20> createF202014CONTRACTINGBODY(BodyF20 value) {
        return new JAXBElement<BodyF20>(_F012014CONTRACTINGBODY_QNAME, BodyF20.class, F202014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF20 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF20 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_CONTRACT", scope = F202014.class)
    public JAXBElement<ObjectContractF20> createF202014OBJECTCONTRACT(ObjectContractF20 value) {
        return new JAXBElement<ObjectContractF20>(_F012014OBJECTCONTRACT_QNAME, ObjectContractF20.class, F202014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcedureF20 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ProcedureF20 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PROCEDURE", scope = F202014.class)
    public JAXBElement<ProcedureF20> createF202014PROCEDURE(ProcedureF20 value) {
        return new JAXBElement<ProcedureF20>(_F012014PROCEDURE_QNAME, ProcedureF20.class, F202014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AwardContractF20 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link AwardContractF20 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "AWARD_CONTRACT", scope = F202014.class)
    public JAXBElement<AwardContractF20> createF202014AWARDCONTRACT(AwardContractF20 value) {
        return new JAXBElement<AwardContractF20>(_F032014AWARDCONTRACT_QNAME, AwardContractF20.class, F202014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CiF20 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CiF20 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "COMPLEMENTARY_INFO", scope = F202014.class)
    public JAXBElement<CiF20> createF202014COMPLEMENTARYINFO(CiF20 value) {
        return new JAXBElement<CiF20>(_F012014COMPLEMENTARYINFO_QNAME, CiF20.class, F202014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModificationsF20 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ModificationsF20 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "MODIFICATIONS_CONTRACT", scope = F202014.class)
    public JAXBElement<ModificationsF20> createF202014MODIFICATIONSCONTRACT(ModificationsF20 value) {
        return new JAXBElement<ModificationsF20>(_F202014MODIFICATIONSCONTRACT_QNAME, ModificationsF20.class, F202014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LegalBasisF21 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LegalBasisF21 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEGAL_BASIS", scope = F212014.class)
    public JAXBElement<LegalBasisF21> createF212014LEGALBASIS(LegalBasisF21 value) {
        return new JAXBElement<LegalBasisF21>(_F012014LEGALBASIS_QNAME, LegalBasisF21.class, F212014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoticeF21 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link NoticeF21 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NOTICE", scope = F212014.class)
    public JAXBElement<NoticeF21> createF212014NOTICE(NoticeF21 value) {
        return new JAXBElement<NoticeF21>(_F012014NOTICE_QNAME, NoticeF21.class, F212014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BodyF21 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BodyF21 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTRACTING_BODY", scope = F212014.class)
    public JAXBElement<BodyF21> createF212014CONTRACTINGBODY(BodyF21 value) {
        return new JAXBElement<BodyF21>(_F012014CONTRACTINGBODY_QNAME, BodyF21.class, F212014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF21 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF21 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_CONTRACT", scope = F212014.class)
    public JAXBElement<ObjectContractF21> createF212014OBJECTCONTRACT(ObjectContractF21 value) {
        return new JAXBElement<ObjectContractF21>(_F012014OBJECTCONTRACT_QNAME, ObjectContractF21.class, F212014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LeftiF21 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LeftiF21 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEFTI", scope = F212014.class)
    public JAXBElement<LeftiF21> createF212014LEFTI(LeftiF21 value) {
        return new JAXBElement<LeftiF21>(_F012014LEFTI_QNAME, LeftiF21.class, F212014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcedureF21 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ProcedureF21 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PROCEDURE", scope = F212014.class)
    public JAXBElement<ProcedureF21> createF212014PROCEDURE(ProcedureF21 value) {
        return new JAXBElement<ProcedureF21>(_F012014PROCEDURE_QNAME, ProcedureF21.class, F212014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AwardContractF21 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link AwardContractF21 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "AWARD_CONTRACT", scope = F212014.class)
    public JAXBElement<AwardContractF21> createF212014AWARDCONTRACT(AwardContractF21 value) {
        return new JAXBElement<AwardContractF21>(_F032014AWARDCONTRACT_QNAME, AwardContractF21.class, F212014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CiF21 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CiF21 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "COMPLEMENTARY_INFO", scope = F212014.class)
    public JAXBElement<CiF21> createF212014COMPLEMENTARYINFO(CiF21 value) {
        return new JAXBElement<CiF21>(_F012014COMPLEMENTARYINFO_QNAME, CiF21.class, F212014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LegalBasisF22 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LegalBasisF22 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEGAL_BASIS", scope = F222014.class)
    public JAXBElement<LegalBasisF22> createF222014LEGALBASIS(LegalBasisF22 value) {
        return new JAXBElement<LegalBasisF22>(_F012014LEGALBASIS_QNAME, LegalBasisF22.class, F222014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoticeF22 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link NoticeF22 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NOTICE", scope = F222014.class)
    public JAXBElement<NoticeF22> createF222014NOTICE(NoticeF22 value) {
        return new JAXBElement<NoticeF22>(_F012014NOTICE_QNAME, NoticeF22.class, F222014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BodyF22 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BodyF22 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTRACTING_BODY", scope = F222014.class)
    public JAXBElement<BodyF22> createF222014CONTRACTINGBODY(BodyF22 value) {
        return new JAXBElement<BodyF22>(_F012014CONTRACTINGBODY_QNAME, BodyF22.class, F222014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF22 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF22 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_CONTRACT", scope = F222014.class)
    public JAXBElement<ObjectContractF22> createF222014OBJECTCONTRACT(ObjectContractF22 value) {
        return new JAXBElement<ObjectContractF22>(_F012014OBJECTCONTRACT_QNAME, ObjectContractF22.class, F222014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LeftiF22 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LeftiF22 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEFTI", scope = F222014.class)
    public JAXBElement<LeftiF22> createF222014LEFTI(LeftiF22 value) {
        return new JAXBElement<LeftiF22>(_F012014LEFTI_QNAME, LeftiF22.class, F222014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcedureF22 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ProcedureF22 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PROCEDURE", scope = F222014.class)
    public JAXBElement<ProcedureF22> createF222014PROCEDURE(ProcedureF22 value) {
        return new JAXBElement<ProcedureF22>(_F012014PROCEDURE_QNAME, ProcedureF22.class, F222014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AwardContractF22 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link AwardContractF22 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "AWARD_CONTRACT", scope = F222014.class)
    public JAXBElement<AwardContractF22> createF222014AWARDCONTRACT(AwardContractF22 value) {
        return new JAXBElement<AwardContractF22>(_F032014AWARDCONTRACT_QNAME, AwardContractF22.class, F222014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CiF22 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CiF22 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "COMPLEMENTARY_INFO", scope = F222014.class)
    public JAXBElement<CiF22> createF222014COMPLEMENTARYINFO(CiF22 value) {
        return new JAXBElement<CiF22>(_F012014COMPLEMENTARYINFO_QNAME, CiF22.class, F222014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LegalBasisF23 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LegalBasisF23 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEGAL_BASIS", scope = F232014.class)
    public JAXBElement<LegalBasisF23> createF232014LEGALBASIS(LegalBasisF23 value) {
        return new JAXBElement<LegalBasisF23>(_F012014LEGALBASIS_QNAME, LegalBasisF23.class, F232014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoticeF23 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link NoticeF23 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NOTICE", scope = F232014.class)
    public JAXBElement<NoticeF23> createF232014NOTICE(NoticeF23 value) {
        return new JAXBElement<NoticeF23>(_F012014NOTICE_QNAME, NoticeF23.class, F232014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BodyF23 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BodyF23 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTRACTING_BODY", scope = F232014.class)
    public JAXBElement<BodyF23> createF232014CONTRACTINGBODY(BodyF23 value) {
        return new JAXBElement<BodyF23>(_F012014CONTRACTINGBODY_QNAME, BodyF23.class, F232014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF23 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF23 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_CONTRACT", scope = F232014.class)
    public JAXBElement<ObjectContractF23> createF232014OBJECTCONTRACT(ObjectContractF23 value) {
        return new JAXBElement<ObjectContractF23>(_F012014OBJECTCONTRACT_QNAME, ObjectContractF23.class, F232014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LeftiF23 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LeftiF23 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEFTI", scope = F232014.class)
    public JAXBElement<LeftiF23> createF232014LEFTI(LeftiF23 value) {
        return new JAXBElement<LeftiF23>(_F012014LEFTI_QNAME, LeftiF23.class, F232014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcedureF23 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ProcedureF23 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PROCEDURE", scope = F232014.class)
    public JAXBElement<ProcedureF23> createF232014PROCEDURE(ProcedureF23 value) {
        return new JAXBElement<ProcedureF23>(_F012014PROCEDURE_QNAME, ProcedureF23.class, F232014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AwardContractF23 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link AwardContractF23 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "AWARD_CONTRACT", scope = F232014.class)
    public JAXBElement<AwardContractF23> createF232014AWARDCONTRACT(AwardContractF23 value) {
        return new JAXBElement<AwardContractF23>(_F032014AWARDCONTRACT_QNAME, AwardContractF23.class, F232014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CiF23 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CiF23 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "COMPLEMENTARY_INFO", scope = F232014.class)
    public JAXBElement<CiF23> createF232014COMPLEMENTARYINFO(CiF23 value) {
        return new JAXBElement<CiF23>(_F012014COMPLEMENTARYINFO_QNAME, CiF23.class, F232014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LegalBasisF24 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LegalBasisF24 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEGAL_BASIS", scope = F242014.class)
    public JAXBElement<LegalBasisF24> createF242014LEGALBASIS(LegalBasisF24 value) {
        return new JAXBElement<LegalBasisF24>(_F012014LEGALBASIS_QNAME, LegalBasisF24.class, F242014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BodyF24 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BodyF24 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTRACTING_BODY", scope = F242014.class)
    public JAXBElement<BodyF24> createF242014CONTRACTINGBODY(BodyF24 value) {
        return new JAXBElement<BodyF24>(_F012014CONTRACTINGBODY_QNAME, BodyF24.class, F242014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF24 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF24 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_CONTRACT", scope = F242014.class)
    public JAXBElement<ObjectContractF24> createF242014OBJECTCONTRACT(ObjectContractF24 value) {
        return new JAXBElement<ObjectContractF24>(_F012014OBJECTCONTRACT_QNAME, ObjectContractF24.class, F242014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LeftiF24 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LeftiF24 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEFTI", scope = F242014.class)
    public JAXBElement<LeftiF24> createF242014LEFTI(LeftiF24 value) {
        return new JAXBElement<LeftiF24>(_F012014LEFTI_QNAME, LeftiF24.class, F242014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcedureF24 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ProcedureF24 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PROCEDURE", scope = F242014.class)
    public JAXBElement<ProcedureF24> createF242014PROCEDURE(ProcedureF24 value) {
        return new JAXBElement<ProcedureF24>(_F012014PROCEDURE_QNAME, ProcedureF24.class, F242014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CiF24 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CiF24 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "COMPLEMENTARY_INFO", scope = F242014.class)
    public JAXBElement<CiF24> createF242014COMPLEMENTARYINFO(CiF24 value) {
        return new JAXBElement<CiF24>(_F012014COMPLEMENTARYINFO_QNAME, CiF24.class, F242014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LegalBasisF25 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LegalBasisF25 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEGAL_BASIS", scope = F252014.class)
    public JAXBElement<LegalBasisF25> createF252014LEGALBASIS(LegalBasisF25 value) {
        return new JAXBElement<LegalBasisF25>(_F012014LEGALBASIS_QNAME, LegalBasisF25.class, F252014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BodyF25 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BodyF25 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTRACTING_BODY", scope = F252014.class)
    public JAXBElement<BodyF25> createF252014CONTRACTINGBODY(BodyF25 value) {
        return new JAXBElement<BodyF25>(_F012014CONTRACTINGBODY_QNAME, BodyF25.class, F252014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF25 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF25 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_CONTRACT", scope = F252014.class)
    public JAXBElement<ObjectContractF25> createF252014OBJECTCONTRACT(ObjectContractF25 value) {
        return new JAXBElement<ObjectContractF25>(_F012014OBJECTCONTRACT_QNAME, ObjectContractF25.class, F252014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcedureF25 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ProcedureF25 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PROCEDURE", scope = F252014.class)
    public JAXBElement<ProcedureF25> createF252014PROCEDURE(ProcedureF25 value) {
        return new JAXBElement<ProcedureF25>(_F012014PROCEDURE_QNAME, ProcedureF25.class, F252014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AwardContractF25 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link AwardContractF25 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "AWARD_CONTRACT", scope = F252014.class)
    public JAXBElement<AwardContractF25> createF252014AWARDCONTRACT(AwardContractF25 value) {
        return new JAXBElement<AwardContractF25>(_F032014AWARDCONTRACT_QNAME, AwardContractF25.class, F252014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CiF25 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CiF25 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "COMPLEMENTARY_INFO", scope = F252014.class)
    public JAXBElement<CiF25> createF252014COMPLEMENTARYINFO(CiF25 value) {
        return new JAXBElement<CiF25>(_F012014COMPLEMENTARYINFO_QNAME, CiF25.class, F252014.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LegalBasisMove }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LegalBasisMove }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEGAL_BASIS", scope = MOVE.class)
    public JAXBElement<LegalBasisMove> createMOVELEGALBASIS(LegalBasisMove value) {
        return new JAXBElement<LegalBasisMove>(_F012014LEGALBASIS_QNAME, LegalBasisMove.class, MOVE.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BodyMove }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BodyMove }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTRACTING_BODY", scope = MOVE.class)
    public JAXBElement<BodyMove> createMOVECONTRACTINGBODY(BodyMove value) {
        return new JAXBElement<BodyMove>(_F012014CONTRACTINGBODY_QNAME, BodyMove.class, MOVE.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractMove }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractMove }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_CONTRACT", scope = MOVE.class)
    public JAXBElement<ObjectContractMove> createMOVEOBJECTCONTRACT(ObjectContractMove value) {
        return new JAXBElement<ObjectContractMove>(_F012014OBJECTCONTRACT_QNAME, ObjectContractMove.class, MOVE.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LeftiMove }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LeftiMove }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LEFTI", scope = MOVE.class)
    public JAXBElement<LeftiMove> createMOVELEFTI(LeftiMove value) {
        return new JAXBElement<LeftiMove>(_F012014LEFTI_QNAME, LeftiMove.class, MOVE.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcedureMove }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ProcedureMove }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PROCEDURE", scope = MOVE.class)
    public JAXBElement<ProcedureMove> createMOVEPROCEDURE(ProcedureMove value) {
        return new JAXBElement<ProcedureMove>(_F012014PROCEDURE_QNAME, ProcedureMove.class, MOVE.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AwardContractMove }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link AwardContractMove }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "AWARD_CONTRACT", scope = MOVE.class)
    public JAXBElement<AwardContractMove> createMOVEAWARDCONTRACT(AwardContractMove value) {
        return new JAXBElement<AwardContractMove>(_F032014AWARDCONTRACT_QNAME, AwardContractMove.class, MOVE.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CiMove }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CiMove }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "COMPLEMENTARY_INFO", scope = MOVE.class)
    public JAXBElement<CiMove> createMOVECOMPLEMENTARYINFO(CiMove value) {
        return new JAXBElement<CiMove>(_F012014COMPLEMENTARYINFO_QNAME, CiMove.class, MOVE.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectF25 .AC }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectF25 .AC }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "AC", scope = ObjectF25.class)
    public JAXBElement<ObjectF25.AC> createObjectF25AC(ObjectF25.AC value) {
        return new JAXBElement<ObjectF25.AC>(_ObjectF25AC_QNAME, ObjectF25.AC.class, ObjectF25.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DURATION", scope = ObjectF25.class)
    public JAXBElement<DurationMD> createObjectF25DURATION(DurationMD value) {
        return new JAXBElement<DurationMD>(_ObjectF25DURATION_QNAME, DurationMD.class, ObjectF25.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectF24 .AC }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectF24 .AC }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "AC", scope = ObjectF24.class)
    public JAXBElement<ObjectF24.AC> createObjectF24AC(ObjectF24.AC value) {
        return new JAXBElement<ObjectF24.AC>(_ObjectF25AC_QNAME, ObjectF24.AC.class, ObjectF24.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DURATION", scope = ObjectF24.class)
    public JAXBElement<DurationMD> createObjectF24DURATION(DurationMD value) {
        return new JAXBElement<DurationMD>(_ObjectF25DURATION_QNAME, DurationMD.class, ObjectF24.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DURATION", scope = ObjectF23.class)
    public JAXBElement<DurationMD> createObjectF23DURATION(DurationMD value) {
        return new JAXBElement<DurationMD>(_ObjectF25DURATION_QNAME, DurationMD.class, ObjectF23.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DURATION", scope = ObjectF22.class)
    public JAXBElement<DurationMD> createObjectF22DURATION(DurationMD value) {
        return new JAXBElement<DurationMD>(_ObjectF25DURATION_QNAME, DurationMD.class, ObjectF22.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectF22 .QS }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectF22 .QS }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "QS", scope = ObjectF22.class)
    public JAXBElement<ObjectF22.QS> createObjectF22QS(ObjectF22.QS value) {
        return new JAXBElement<ObjectF22.QS>(_ObjectF22QS_QNAME, ObjectF22.QS.class, ObjectF22.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DURATION", scope = ObjectF21.class)
    public JAXBElement<DurationMD> createObjectF21DURATION(DurationMD value) {
        return new JAXBElement<DurationMD>(_ObjectF25DURATION_QNAME, DurationMD.class, ObjectF21.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DURATION", scope = ObjectF20.class)
    public JAXBElement<DurationMD> createObjectF20DURATION(DurationMD value) {
        return new JAXBElement<DurationMD>(_ObjectF25DURATION_QNAME, DurationMD.class, ObjectF20.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectF05 .AC }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectF05 .AC }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "AC", scope = ObjectF05.class)
    public JAXBElement<ObjectF05.AC> createObjectF05AC(ObjectF05.AC value) {
        return new JAXBElement<ObjectF05.AC>(_ObjectF25AC_QNAME, ObjectF05.AC.class, ObjectF05.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DURATION", scope = ObjectF05.class)
    public JAXBElement<DurationMD> createObjectF05DURATION(DurationMD value) {
        return new JAXBElement<DurationMD>(_ObjectF25DURATION_QNAME, DurationMD.class, ObjectF05.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectF04 .AC }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectF04 .AC }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "AC", scope = ObjectF04.class)
    public JAXBElement<ObjectF04.AC> createObjectF04AC(ObjectF04.AC value) {
        return new JAXBElement<ObjectF04.AC>(_ObjectF25AC_QNAME, ObjectF04.AC.class, ObjectF04.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DURATION", scope = ObjectF04.class)
    public JAXBElement<DurationMD> createObjectF04DURATION(DurationMD value) {
        return new JAXBElement<DurationMD>(_ObjectF25DURATION_QNAME, DurationMD.class, ObjectF04.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectF02 .AC }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectF02 .AC }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "AC", scope = ObjectF02.class)
    public JAXBElement<ObjectF02.AC> createObjectF02AC(ObjectF02.AC value) {
        return new JAXBElement<ObjectF02.AC>(_ObjectF25AC_QNAME, ObjectF02.AC.class, ObjectF02.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DURATION", scope = ObjectF02.class)
    public JAXBElement<DurationMD> createObjectF02DURATION(DurationMD value) {
        return new JAXBElement<DurationMD>(_ObjectF25DURATION_QNAME, DurationMD.class, ObjectF02.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectF01 .AC }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectF01 .AC }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "AC", scope = ObjectF01.class)
    public JAXBElement<ObjectF01.AC> createObjectF01AC(ObjectF01.AC value) {
        return new JAXBElement<ObjectF01.AC>(_ObjectF25AC_QNAME, ObjectF01.AC.class, ObjectF01.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DURATION", scope = ObjectF01.class)
    public JAXBElement<DurationMD> createObjectF01DURATION(DurationMD value) {
        return new JAXBElement<DurationMD>(_ObjectF25DURATION_QNAME, DurationMD.class, ObjectF01.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoSupplies }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link NoSupplies }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "TYPE_CONTRACT", scope = ObjectContractF25.class)
    public JAXBElement<NoSupplies> createObjectContractF25TYPECONTRACT(NoSupplies value) {
        return new JAXBElement<NoSupplies>(_TYPECONTRACT_QNAME, NoSupplies.class, ObjectContractF25.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CALCULATION_METHOD", scope = ObjectContractF25.class)
    public JAXBElement<TextFtMultiLines> createObjectContractF25CALCULATIONMETHOD(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_ObjectContractF25CALCULATIONMETHOD_QNAME, TextFtMultiLines.class, ObjectContractF25.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectF25 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectF25 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_DESCR", scope = ObjectContractF25.class)
    public JAXBElement<ObjectF25> createObjectContractF25OBJECTDESCR(ObjectF25 value) {
        return new JAXBElement<ObjectF25>(_ObjectContractF25OBJECTDESCR_QNAME, ObjectF25.class, ObjectContractF25.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoSupplies }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link NoSupplies }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "TYPE_CONTRACT", scope = ObjectContractF24.class)
    public JAXBElement<NoSupplies> createObjectContractF24TYPECONTRACT(NoSupplies value) {
        return new JAXBElement<NoSupplies>(_TYPECONTRACT_QNAME, NoSupplies.class, ObjectContractF24.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LotDivisionF24 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LotDivisionF24 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LOT_DIVISION", scope = ObjectContractF24.class)
    public JAXBElement<LotDivisionF24> createObjectContractF24LOTDIVISION(LotDivisionF24 value) {
        return new JAXBElement<LotDivisionF24>(_LOTDIVISION_QNAME, LotDivisionF24.class, ObjectContractF24.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectF24 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectF24 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_DESCR", scope = ObjectContractF24.class)
    public JAXBElement<ObjectF24> createObjectContractF24OBJECTDESCR(ObjectF24 value) {
        return new JAXBElement<ObjectF24>(_ObjectContractF25OBJECTDESCR_QNAME, ObjectF24.class, ObjectContractF24.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Services }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Services }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "TYPE_CONTRACT", scope = ObjectContractF23.class)
    public JAXBElement<Services> createObjectContractF23TYPECONTRACT(Services value) {
        return new JAXBElement<Services>(_TYPECONTRACT_QNAME, Services.class, ObjectContractF23.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LotDivisionF23 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LotDivisionF23 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LOT_DIVISION", scope = ObjectContractF23.class)
    public JAXBElement<LotDivisionF23> createObjectContractF23LOTDIVISION(LotDivisionF23 value) {
        return new JAXBElement<LotDivisionF23>(_LOTDIVISION_QNAME, LotDivisionF23.class, ObjectContractF23.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectF23 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectF23 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_DESCR", scope = ObjectContractF23.class)
    public JAXBElement<ObjectF23> createObjectContractF23OBJECTDESCR(ObjectF23 value) {
        return new JAXBElement<ObjectF23>(_ObjectContractF25OBJECTDESCR_QNAME, ObjectF23.class, ObjectContractF23.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Services }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Services }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "TYPE_CONTRACT", scope = ObjectContractF22.class)
    public JAXBElement<Services> createObjectContractF22TYPECONTRACT(Services value) {
        return new JAXBElement<Services>(_TYPECONTRACT_QNAME, Services.class, ObjectContractF22.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF22 .VALTOTAL }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF22 .VALTOTAL }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "VAL_TOTAL", scope = ObjectContractF22.class)
    public JAXBElement<ObjectContractF22.VALTOTAL> createObjectContractF22VALTOTAL(ObjectContractF22.VALTOTAL value) {
        return new JAXBElement<ObjectContractF22.VALTOTAL>(_VALTOTAL_QNAME, ObjectContractF22.VALTOTAL.class, ObjectContractF22.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF22 .VALRANGETOTAL }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF22 .VALRANGETOTAL }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "VAL_RANGE_TOTAL", scope = ObjectContractF22.class)
    public JAXBElement<ObjectContractF22.VALRANGETOTAL> createObjectContractF22VALRANGETOTAL(ObjectContractF22.VALRANGETOTAL value) {
        return new JAXBElement<ObjectContractF22.VALRANGETOTAL>(_VALRANGETOTAL_QNAME, ObjectContractF22.VALRANGETOTAL.class, ObjectContractF22.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LotDivisionF22 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LotDivisionF22 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LOT_DIVISION", scope = ObjectContractF22.class)
    public JAXBElement<LotDivisionF22> createObjectContractF22LOTDIVISION(LotDivisionF22 value) {
        return new JAXBElement<LotDivisionF22>(_LOTDIVISION_QNAME, LotDivisionF22.class, ObjectContractF22.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectF22 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectF22 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_DESCR", scope = ObjectContractF22.class)
    public JAXBElement<ObjectF22> createObjectContractF22OBJECTDESCR(ObjectF22 value) {
        return new JAXBElement<ObjectF22>(_ObjectContractF25OBJECTDESCR_QNAME, ObjectF22.class, ObjectContractF22.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Services }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Services }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "TYPE_CONTRACT", scope = ObjectContractF21.class)
    public JAXBElement<Services> createObjectContractF21TYPECONTRACT(Services value) {
        return new JAXBElement<Services>(_TYPECONTRACT_QNAME, Services.class, ObjectContractF21.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LotDivisionF21 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LotDivisionF21 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LOT_DIVISION", scope = ObjectContractF21.class)
    public JAXBElement<LotDivisionF21> createObjectContractF21LOTDIVISION(LotDivisionF21 value) {
        return new JAXBElement<LotDivisionF21>(_LOTDIVISION_QNAME, LotDivisionF21.class, ObjectContractF21.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectF21 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectF21 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_DESCR", scope = ObjectContractF21.class)
    public JAXBElement<ObjectF21> createObjectContractF21OBJECTDESCR(ObjectF21 value) {
        return new JAXBElement<ObjectF21>(_ObjectContractF25OBJECTDESCR_QNAME, ObjectF21.class, ObjectContractF21.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DURATION", scope = ModificationsF20.DESCRIPTIONPROCUREMENT.class)
    public JAXBElement<DurationMD> createModificationsF20DESCRIPTIONPROCUREMENTDURATION(DurationMD value) {
        return new JAXBElement<DurationMD>(_ObjectF25DURATION_QNAME, DurationMD.class, ModificationsF20.DESCRIPTIONPROCUREMENT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModificationsF20 .DESCRIPTIONPROCUREMENT.VALUES }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ModificationsF20 .DESCRIPTIONPROCUREMENT.VALUES }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "VALUES", scope = ModificationsF20.DESCRIPTIONPROCUREMENT.class)
    public JAXBElement<ModificationsF20.DESCRIPTIONPROCUREMENT.VALUES> createModificationsF20DESCRIPTIONPROCUREMENTVALUES(ModificationsF20.DESCRIPTIONPROCUREMENT.VALUES value) {
        return new JAXBElement<ModificationsF20.DESCRIPTIONPROCUREMENT.VALUES>(_ModificationsF20DESCRIPTIONPROCUREMENTVALUES_QNAME, ModificationsF20.DESCRIPTIONPROCUREMENT.VALUES.class, ModificationsF20.DESCRIPTIONPROCUREMENT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModificationsF20 .DESCRIPTIONPROCUREMENT.CONTRACTORS }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ModificationsF20 .DESCRIPTIONPROCUREMENT.CONTRACTORS }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CONTRACTORS", scope = ModificationsF20.DESCRIPTIONPROCUREMENT.class)
    public JAXBElement<ModificationsF20.DESCRIPTIONPROCUREMENT.CONTRACTORS> createModificationsF20DESCRIPTIONPROCUREMENTCONTRACTORS(ModificationsF20.DESCRIPTIONPROCUREMENT.CONTRACTORS value) {
        return new JAXBElement<ModificationsF20.DESCRIPTIONPROCUREMENT.CONTRACTORS>(_ModificationsF20DESCRIPTIONPROCUREMENTCONTRACTORS_QNAME, ModificationsF20.DESCRIPTIONPROCUREMENT.CONTRACTORS.class, ModificationsF20.DESCRIPTIONPROCUREMENT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF15 .VALTOTAL }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF15 .VALTOTAL }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "VAL_TOTAL", scope = ObjectContractF15.class)
    public JAXBElement<ObjectContractF15.VALTOTAL> createObjectContractF15VALTOTAL(ObjectContractF15.VALTOTAL value) {
        return new JAXBElement<ObjectContractF15.VALTOTAL>(_VALTOTAL_QNAME, ObjectContractF15.VALTOTAL.class, ObjectContractF15.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF15 .VALRANGETOTAL }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF15 .VALRANGETOTAL }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "VAL_RANGE_TOTAL", scope = ObjectContractF15.class)
    public JAXBElement<ObjectContractF15.VALRANGETOTAL> createObjectContractF15VALRANGETOTAL(ObjectContractF15.VALRANGETOTAL value) {
        return new JAXBElement<ObjectContractF15.VALRANGETOTAL>(_VALRANGETOTAL_QNAME, ObjectContractF15.VALRANGETOTAL.class, ObjectContractF15.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectF15 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectF15 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_DESCR", scope = ObjectContractF15.class)
    public JAXBElement<ObjectF15> createObjectContractF15OBJECTDESCR(ObjectF15 value) {
        return new JAXBElement<ObjectF15>(_ObjectContractF25OBJECTDESCR_QNAME, ObjectF15.class, ObjectContractF15.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NonPublished }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link NonPublished }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "MODIFICATION_ORIGINAL", scope = ChangesF14.class)
    public JAXBElement<NonPublished> createChangesF14MODIFICATIONORIGINAL(NonPublished value) {
        return new JAXBElement<NonPublished>(_ChangesF14MODIFICATIONORIGINAL_QNAME, NonPublished.class, ChangesF14.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NonPublished }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link NonPublished }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PUBLICATION_TED", scope = ChangesF14.class)
    public JAXBElement<NonPublished> createChangesF14PUBLICATIONTED(NonPublished value) {
        return new JAXBElement<NonPublished>(_ChangesF14PUBLICATIONTED_QNAME, NonPublished.class, ChangesF14.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangesF14 .CHANGE }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ChangesF14 .CHANGE }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "CHANGE", scope = ChangesF14.class)
    public JAXBElement<ChangesF14.CHANGE> createChangesF14CHANGE(ChangesF14.CHANGE value) {
        return new JAXBElement<ChangesF14.CHANGE>(_ChangesF14CHANGE_QNAME, ChangesF14.CHANGE.class, ChangesF14.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NB_MIN_PARTICIPANTS", scope = ProcedureF12.class)
    public JAXBElement<BigInteger> createProcedureF12NBMINPARTICIPANTS(BigInteger value) {
        return new JAXBElement<BigInteger>(_ProcedureF12NBMINPARTICIPANTS_QNAME, BigInteger.class, ProcedureF12.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NB_MAX_PARTICIPANTS", scope = ProcedureF12.class)
    public JAXBElement<BigInteger> createProcedureF12NBMAXPARTICIPANTS(BigInteger value) {
        return new JAXBElement<BigInteger>(_ProcedureF12NBMAXPARTICIPANTS_QNAME, BigInteger.class, ProcedureF12.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PARTICIPANT_NAME", scope = ProcedureF12.class)
    public JAXBElement<String> createProcedureF12PARTICIPANTNAME(String value) {
        return new JAXBElement<String>(_ProcedureF12PARTICIPANTNAME_QNAME, String.class, ProcedureF12.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PRIZE_AWARDED", scope = ProcedureF12.class)
    public JAXBElement<Empty> createProcedureF12PRIZEAWARDED(Empty value) {
        return new JAXBElement<Empty>(_ProcedureF12PRIZEAWARDED_QNAME, Empty.class, ProcedureF12.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NUMBER_VALUE_PRIZE", scope = ProcedureF12.class)
    public JAXBElement<TextFtMultiLines> createProcedureF12NUMBERVALUEPRIZE(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_ProcedureF12NUMBERVALUEPRIZE_QNAME, TextFtMultiLines.class, ProcedureF12.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NO_PRIZE_AWARDED", scope = ProcedureF12.class)
    public JAXBElement<Empty> createProcedureF12NOPRIZEAWARDED(Empty value) {
        return new JAXBElement<Empty>(_ProcedureF12NOPRIZEAWARDED_QNAME, Empty.class, ProcedureF12.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DETAILS_PAYMENT", scope = ProcedureF12.class)
    public JAXBElement<TextFtMultiLines> createProcedureF12DETAILSPAYMENT(TextFtMultiLines value) {
        return new JAXBElement<TextFtMultiLines>(_ProcedureF12DETAILSPAYMENT_QNAME, TextFtMultiLines.class, ProcedureF12.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "FOLLOW_UP_CONTRACTS", scope = ProcedureF12.class)
    public JAXBElement<Empty> createProcedureF12FOLLOWUPCONTRACTS(Empty value) {
        return new JAXBElement<Empty>(_ProcedureF12FOLLOWUPCONTRACTS_QNAME, Empty.class, ProcedureF12.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NO_FOLLOW_UP_CONTRACTS", scope = ProcedureF12.class)
    public JAXBElement<Empty> createProcedureF12NOFOLLOWUPCONTRACTS(Empty value) {
        return new JAXBElement<Empty>(_ProcedureF12NOFOLLOWUPCONTRACTS_QNAME, Empty.class, ProcedureF12.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DECISION_BINDING_CONTRACTING", scope = ProcedureF12.class)
    public JAXBElement<Empty> createProcedureF12DECISIONBINDINGCONTRACTING(Empty value) {
        return new JAXBElement<Empty>(_ProcedureF12DECISIONBINDINGCONTRACTING_QNAME, Empty.class, ProcedureF12.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NO_DECISION_BINDING_CONTRACTING", scope = ProcedureF12.class)
    public JAXBElement<Empty> createProcedureF12NODECISIONBINDINGCONTRACTING(Empty value) {
        return new JAXBElement<Empty>(_ProcedureF12NODECISIONBINDINGCONTRACTING_QNAME, Empty.class, ProcedureF12.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "MEMBER_NAME", scope = ProcedureF12.class)
    public JAXBElement<String> createProcedureF12MEMBERNAME(String value) {
        return new JAXBElement<String>(_ProcedureF12MEMBERNAME_QNAME, String.class, ProcedureF12.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactContractingBody }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ContactContractingBody }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "ADDRESS_CONTRACTING_BODY", scope = BodyF12.class)
    public JAXBElement<ContactContractingBody> createBodyF12ADDRESSCONTRACTINGBODY(ContactContractingBody value) {
        return new JAXBElement<ContactContractingBody>(_ADDRESSCONTRACTINGBODY_QNAME, ContactContractingBody.class, BodyF12.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "COMMUNITY_ORIGIN", scope = AwardContractF06.AWARDEDCONTRACT.COUNTRYORIGIN.class)
    public JAXBElement<Empty> createAwardContractF06AWARDEDCONTRACTCOUNTRYORIGINCOMMUNITYORIGIN(Empty value) {
        return new JAXBElement<Empty>(_AwardContractF06AWARDEDCONTRACTCOUNTRYORIGINCOMMUNITYORIGIN_QNAME, Empty.class, AwardContractF06.AWARDEDCONTRACT.COUNTRYORIGIN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Country }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Country }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "NON_COMMUNITY_ORIGIN", scope = AwardContractF06.AWARDEDCONTRACT.COUNTRYORIGIN.class)
    public JAXBElement<Country> createAwardContractF06AWARDEDCONTRACTCOUNTRYORIGINNONCOMMUNITYORIGIN(Country value) {
        return new JAXBElement<Country>(_AwardContractF06AWARDEDCONTRACTCOUNTRYORIGINNONCOMMUNITYORIGIN_QNAME, Country.class, AwardContractF06.AWARDEDCONTRACT.COUNTRYORIGIN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF06 .VALTOTAL }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF06 .VALTOTAL }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "VAL_TOTAL", scope = ObjectContractF06.class)
    public JAXBElement<ObjectContractF06.VALTOTAL> createObjectContractF06VALTOTAL(ObjectContractF06.VALTOTAL value) {
        return new JAXBElement<ObjectContractF06.VALTOTAL>(_VALTOTAL_QNAME, ObjectContractF06.VALTOTAL.class, ObjectContractF06.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectContractF06 .VALRANGETOTAL }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectContractF06 .VALRANGETOTAL }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "VAL_RANGE_TOTAL", scope = ObjectContractF06.class)
    public JAXBElement<ObjectContractF06.VALRANGETOTAL> createObjectContractF06VALRANGETOTAL(ObjectContractF06.VALRANGETOTAL value) {
        return new JAXBElement<ObjectContractF06.VALRANGETOTAL>(_VALRANGETOTAL_QNAME, ObjectContractF06.VALRANGETOTAL.class, ObjectContractF06.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LOT_DIVISION", scope = ObjectContractF06.class)
    public JAXBElement<Empty> createObjectContractF06LOTDIVISION(Empty value) {
        return new JAXBElement<Empty>(_LOTDIVISION_QNAME, Empty.class, ObjectContractF06.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectF06 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectF06 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_DESCR", scope = ObjectContractF06.class)
    public JAXBElement<ObjectF06> createObjectContractF06OBJECTDESCR(ObjectF06 value) {
        return new JAXBElement<ObjectF06>(_ObjectContractF25OBJECTDESCR_QNAME, ObjectF06.class, ObjectContractF06.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LotDivisionF05 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LotDivisionF05 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LOT_DIVISION", scope = ObjectContractF05.class)
    public JAXBElement<LotDivisionF05> createObjectContractF05LOTDIVISION(LotDivisionF05 value) {
        return new JAXBElement<LotDivisionF05>(_LOTDIVISION_QNAME, LotDivisionF05.class, ObjectContractF05.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectF05 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectF05 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_DESCR", scope = ObjectContractF05.class)
    public JAXBElement<ObjectF05> createObjectContractF05OBJECTDESCR(ObjectF05 value) {
        return new JAXBElement<ObjectF05>(_ObjectContractF25OBJECTDESCR_QNAME, ObjectF05.class, ObjectContractF05.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LotDivisionF04 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LotDivisionF04 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LOT_DIVISION", scope = ObjectContractF04.class)
    public JAXBElement<LotDivisionF04> createObjectContractF04LOTDIVISION(LotDivisionF04 value) {
        return new JAXBElement<LotDivisionF04>(_LOTDIVISION_QNAME, LotDivisionF04.class, ObjectContractF04.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectF04 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectF04 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_DESCR", scope = ObjectContractF04.class)
    public JAXBElement<ObjectF04> createObjectContractF04OBJECTDESCR(ObjectF04 value) {
        return new JAXBElement<ObjectF04>(_ObjectContractF25OBJECTDESCR_QNAME, ObjectF04.class, ObjectContractF04.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnnexD1 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link AnnexD1 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "PT_AWARD_CONTRACT_WITHOUT_CALL", scope = ProcedureF03.class)
    public JAXBElement<AnnexD1> createProcedureF03PTAWARDCONTRACTWITHOUTCALL(AnnexD1 value) {
        return new JAXBElement<AnnexD1>(_PTAWARDCONTRACTWITHOUTCALL_QNAME, AnnexD1.class, ProcedureF03.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "FRAMEWORK", scope = ProcedureF03.class)
    public JAXBElement<Empty> createProcedureF03FRAMEWORK(Empty value) {
        return new JAXBElement<Empty>(_ProcedureF03FRAMEWORK_QNAME, Empty.class, ProcedureF03.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LOT_DIVISION", scope = ObjectContractF03.class)
    public JAXBElement<Empty> createObjectContractF03LOTDIVISION(Empty value) {
        return new JAXBElement<Empty>(_LOTDIVISION_QNAME, Empty.class, ObjectContractF03.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectF03 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectF03 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_DESCR", scope = ObjectContractF03.class)
    public JAXBElement<ObjectF03> createObjectContractF03OBJECTDESCR(ObjectF03 value) {
        return new JAXBElement<ObjectF03>(_ObjectContractF25OBJECTDESCR_QNAME, ObjectF03.class, ObjectContractF03.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FrameworkInfo }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link FrameworkInfo }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "FRAMEWORK", scope = ProcedureF02.class)
    public JAXBElement<FrameworkInfo> createProcedureF02FRAMEWORK(FrameworkInfo value) {
        return new JAXBElement<FrameworkInfo>(_ProcedureF03FRAMEWORK_QNAME, FrameworkInfo.class, ProcedureF02.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DPS_ADDITIONAL_PURCHASERS", scope = ProcedureF02.class)
    public JAXBElement<Empty> createProcedureF02DPSADDITIONALPURCHASERS(Empty value) {
        return new JAXBElement<Empty>(_ProcedureF02DPSADDITIONALPURCHASERS_QNAME, Empty.class, ProcedureF02.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DATE_TENDER_VALID", scope = ProcedureF02.class)
    public JAXBElement<XMLGregorianCalendar> createProcedureF02DATETENDERVALID(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ProcedureF02DATETENDERVALID_QNAME, XMLGregorianCalendar.class, ProcedureF02.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcedureF05 .DURATIONTENDERVALID }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ProcedureF05 .DURATIONTENDERVALID }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "DURATION_TENDER_VALID", scope = ProcedureF02.class)
    public JAXBElement<ProcedureF05.DURATIONTENDERVALID> createProcedureF02DURATIONTENDERVALID(ProcedureF05.DURATIONTENDERVALID value) {
        return new JAXBElement<ProcedureF05.DURATIONTENDERVALID>(_ProcedureF02DURATIONTENDERVALID_QNAME, ProcedureF05.DURATIONTENDERVALID.class, ProcedureF02.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LotDivisionF02 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LotDivisionF02 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LOT_DIVISION", scope = ObjectContractF02.class)
    public JAXBElement<LotDivisionF02> createObjectContractF02LOTDIVISION(LotDivisionF02 value) {
        return new JAXBElement<LotDivisionF02>(_LOTDIVISION_QNAME, LotDivisionF02.class, ObjectContractF02.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectF02 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectF02 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_DESCR", scope = ObjectContractF02.class)
    public JAXBElement<ObjectF02> createObjectContractF02OBJECTDESCR(ObjectF02 value) {
        return new JAXBElement<ObjectF02>(_ObjectContractF25OBJECTDESCR_QNAME, ObjectF02.class, ObjectContractF02.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LotDivisionF01 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LotDivisionF01 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "LOT_DIVISION", scope = ObjectContractF01.class)
    public JAXBElement<LotDivisionF01> createObjectContractF01LOTDIVISION(LotDivisionF01 value) {
        return new JAXBElement<LotDivisionF01>(_LOTDIVISION_QNAME, LotDivisionF01.class, ObjectContractF01.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectF01 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ObjectF01 }{@code >}
     */
    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", name = "OBJECT_DESCR", scope = ObjectContractF01.class)
    public JAXBElement<ObjectF01> createObjectContractF01OBJECTDESCR(ObjectF01 value) {
        return new JAXBElement<ObjectF01>(_ObjectContractF25OBJECTDESCR_QNAME, ObjectF01.class, ObjectContractF01.class, value);
    }

    @XmlElementDecl(namespace = "http://publications.europa.eu/resource/schema/ted/2016/nuts", name = "NUTS")
    public JAXBElement<Nuts> createNUTS(Nuts value) {
        return new JAXBElement<Nuts>(_NUTS_QNAME, Nuts.class, null, value);
    }

    public Nuts createNuts() {
        return new Nuts();
    }

}
