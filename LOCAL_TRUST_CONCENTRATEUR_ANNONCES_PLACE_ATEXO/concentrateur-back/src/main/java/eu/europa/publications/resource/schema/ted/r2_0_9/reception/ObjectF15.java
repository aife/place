//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import eu.europa.publications.resource.schema.ted._2021.nuts.Nuts;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java pour object_f15 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="object_f15"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TITLE" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LOT_NO" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CPV_ADDITIONAL" maxOccurs="100" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/2016/nuts}NUTS" maxOccurs="250"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}MAIN_SITE" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SHORT_DESCR"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="DIRECTIVE_2014_23_EU"&gt;
 *             &lt;complexType&gt;
 *               &lt;complexContent&gt;
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                   &lt;sequence&gt;
 *                     &lt;element name="AC"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;sequence&gt;
 *                               &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}AC_CRITERION" maxOccurs="20"/&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                   &lt;/sequence&gt;
 *                 &lt;/restriction&gt;
 *               &lt;/complexContent&gt;
 *             &lt;/complexType&gt;
 *           &lt;/element&gt;
 *           &lt;element name="DIRECTIVE_2014_24_EU"&gt;
 *             &lt;complexType&gt;
 *               &lt;complexContent&gt;
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                   &lt;sequence&gt;
 *                     &lt;element name="AC"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_criteria"/&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                   &lt;/sequence&gt;
 *                 &lt;/restriction&gt;
 *               &lt;/complexContent&gt;
 *             &lt;/complexType&gt;
 *           &lt;/element&gt;
 *           &lt;element name="DIRECTIVE_2014_25_EU"&gt;
 *             &lt;complexType&gt;
 *               &lt;complexContent&gt;
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                   &lt;sequence&gt;
 *                     &lt;element name="AC"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_man"&gt;
 *                             &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_criteria"/&gt;
 *                           &lt;/extension&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                   &lt;/sequence&gt;
 *                 &lt;/restriction&gt;
 *               &lt;/complexContent&gt;
 *             &lt;/complexType&gt;
 *           &lt;/element&gt;
 *           &lt;element name="DIRECTIVE_2009_81_EC"&gt;
 *             &lt;complexType&gt;
 *               &lt;complexContent&gt;
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                   &lt;sequence&gt;
 *                     &lt;element name="AC"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;choice&gt;
 *                               &lt;element name="AC_PRICE" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty"/&gt;
 *                               &lt;element name="AC_CRITERIA" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ac_definition" maxOccurs="20"/&gt;
 *                             &lt;/choice&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                   &lt;/sequence&gt;
 *                 &lt;/restriction&gt;
 *               &lt;/complexContent&gt;
 *             &lt;/complexType&gt;
 *           &lt;/element&gt;
 *         &lt;/choice&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}options"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}eu_union_funds"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}INFO_ADD" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ITEM" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}nb_lot" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "object_f15", propOrder = {
        "title",
        "lotno",
        "cpvadditional",
        "nuts",
        "mainsite",
        "shortdescr",
        "directive201423EU",
        "directive201424EU",
        "directive201425EU",
        "directive200981EC",
        "options",
        "optionsdescr",
        "nooptions",
        "euprogrrelated",
        "noeuprogrrelated",
        "infoadd"
})
@ToString
@EqualsAndHashCode
public class ObjectF15 {

    @XmlElement(name = "TITLE")
    protected TextFtSingleLine title;
    @XmlElement(name = "LOT_NO")
    protected String lotno;
    @XmlElement(name = "CPV_ADDITIONAL")
    protected List<CpvSet> cpvadditional;
    @XmlElement(name = "NUTS", namespace = "http://publications.europa.eu/resource/schema/ted/2016/nuts", required = true)
    protected List<Nuts> nuts;
    @XmlElement(name = "MAIN_SITE")
    protected TextFtMultiLines mainsite;
    @XmlElement(name = "SHORT_DESCR", required = true)
    protected TextFtMultiLines shortdescr;
    @XmlElement(name = "DIRECTIVE_2014_23_EU")
    protected ObjectF15.DIRECTIVE201423EU directive201423EU;
    @XmlElement(name = "DIRECTIVE_2014_24_EU")
    protected ObjectF15.DIRECTIVE201424EU directive201424EU;
    @XmlElement(name = "DIRECTIVE_2014_25_EU")
    protected ObjectF15.DIRECTIVE201425EU directive201425EU;
    @XmlElement(name = "DIRECTIVE_2009_81_EC")
    protected ObjectF15.DIRECTIVE200981EC directive200981EC;
    @XmlElement(name = "OPTIONS")
    protected Empty options;
    @XmlElement(name = "OPTIONS_DESCR")
    protected TextFtMultiLines optionsdescr;
    @XmlElement(name = "NO_OPTIONS")
    protected Empty nooptions;
    @XmlElement(name = "EU_PROGR_RELATED")
    protected TextFtMultiLines euprogrrelated;
    @XmlElement(name = "NO_EU_PROGR_RELATED")
    protected Empty noeuprogrrelated;
    @XmlElement(name = "INFO_ADD")
    protected TextFtMultiLines infoadd;
    @XmlAttribute(name = "ITEM", required = true)
    protected int item;

    /**
     * Obtient la valeur de la propriété title.
     *
     * @return possible object is
     * {@link TextFtSingleLine }
     */
    public TextFtSingleLine getTITLE() {
        return title;
    }

    /**
     * Définit la valeur de la propriété title.
     *
     * @param value allowed object is
     *              {@link TextFtSingleLine }
     */
    public void setTITLE(TextFtSingleLine value) {
        this.title = value;
    }

    /**
     * Obtient la valeur de la propriété lotno.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLOTNO() {
        return lotno;
    }

    /**
     * Définit la valeur de la propriété lotno.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLOTNO(String value) {
        this.lotno = value;
    }

    /**
     * Gets the value of the cpvadditional property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cpvadditional property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCPVADDITIONAL().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CpvSet }
     */
    public List<CpvSet> getCPVADDITIONAL() {
        if (cpvadditional == null) {
            cpvadditional = new ArrayList<CpvSet>();
        }
        return this.cpvadditional;
    }

    /**
     * Gets the value of the nuts property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nuts property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNUTS().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Nuts }
     */
    public List<Nuts> getNUTS() {
        if (nuts == null) {
            nuts = new ArrayList<Nuts>();
        }
        return this.nuts;
    }

    /**
     * Obtient la valeur de la propriété mainsite.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getMAINSITE() {
        return mainsite;
    }

    /**
     * Définit la valeur de la propriété mainsite.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setMAINSITE(TextFtMultiLines value) {
        this.mainsite = value;
    }

    /**
     * Obtient la valeur de la propriété shortdescr.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getSHORTDESCR() {
        return shortdescr;
    }

    /**
     * Définit la valeur de la propriété shortdescr.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setSHORTDESCR(TextFtMultiLines value) {
        this.shortdescr = value;
    }

    /**
     * Obtient la valeur de la propriété directive201423EU.
     *
     * @return possible object is
     * {@link ObjectF15 .DIRECTIVE201423EU }
     */
    public ObjectF15.DIRECTIVE201423EU getDIRECTIVE201423EU() {
        return directive201423EU;
    }

    /**
     * Définit la valeur de la propriété directive201423EU.
     *
     * @param value allowed object is
     *              {@link ObjectF15 .DIRECTIVE201423EU }
     */
    public void setDIRECTIVE201423EU(ObjectF15.DIRECTIVE201423EU value) {
        this.directive201423EU = value;
    }

    /**
     * Obtient la valeur de la propriété directive201424EU.
     *
     * @return possible object is
     * {@link ObjectF15 .DIRECTIVE201424EU }
     */
    public ObjectF15.DIRECTIVE201424EU getDIRECTIVE201424EU() {
        return directive201424EU;
    }

    /**
     * Définit la valeur de la propriété directive201424EU.
     *
     * @param value allowed object is
     *              {@link ObjectF15 .DIRECTIVE201424EU }
     */
    public void setDIRECTIVE201424EU(ObjectF15.DIRECTIVE201424EU value) {
        this.directive201424EU = value;
    }

    /**
     * Obtient la valeur de la propriété directive201425EU.
     *
     * @return possible object is
     * {@link ObjectF15 .DIRECTIVE201425EU }
     */
    public ObjectF15.DIRECTIVE201425EU getDIRECTIVE201425EU() {
        return directive201425EU;
    }

    /**
     * Définit la valeur de la propriété directive201425EU.
     *
     * @param value allowed object is
     *              {@link ObjectF15 .DIRECTIVE201425EU }
     */
    public void setDIRECTIVE201425EU(ObjectF15.DIRECTIVE201425EU value) {
        this.directive201425EU = value;
    }

    /**
     * Obtient la valeur de la propriété directive200981EC.
     *
     * @return possible object is
     * {@link ObjectF15 .DIRECTIVE200981EC }
     */
    public ObjectF15.DIRECTIVE200981EC getDIRECTIVE200981EC() {
        return directive200981EC;
    }

    /**
     * Définit la valeur de la propriété directive200981EC.
     *
     * @param value allowed object is
     *              {@link ObjectF15 .DIRECTIVE200981EC }
     */
    public void setDIRECTIVE200981EC(ObjectF15.DIRECTIVE200981EC value) {
        this.directive200981EC = value;
    }

    /**
     * Obtient la valeur de la propriété options.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getOPTIONS() {
        return options;
    }

    /**
     * Définit la valeur de la propriété options.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setOPTIONS(Empty value) {
        this.options = value;
    }

    /**
     * Obtient la valeur de la propriété optionsdescr.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getOPTIONSDESCR() {
        return optionsdescr;
    }

    /**
     * Définit la valeur de la propriété optionsdescr.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setOPTIONSDESCR(TextFtMultiLines value) {
        this.optionsdescr = value;
    }

    /**
     * Obtient la valeur de la propriété nooptions.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getNOOPTIONS() {
        return nooptions;
    }

    /**
     * Définit la valeur de la propriété nooptions.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setNOOPTIONS(Empty value) {
        this.nooptions = value;
    }

    /**
     * Obtient la valeur de la propriété euprogrrelated.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getEUPROGRRELATED() {
        return euprogrrelated;
    }

    /**
     * Définit la valeur de la propriété euprogrrelated.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setEUPROGRRELATED(TextFtMultiLines value) {
        this.euprogrrelated = value;
    }

    /**
     * Obtient la valeur de la propriété noeuprogrrelated.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getNOEUPROGRRELATED() {
        return noeuprogrrelated;
    }

    /**
     * Définit la valeur de la propriété noeuprogrrelated.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setNOEUPROGRRELATED(Empty value) {
        this.noeuprogrrelated = value;
    }

    /**
     * Obtient la valeur de la propriété infoadd.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getINFOADD() {
        return infoadd;
    }

    /**
     * Définit la valeur de la propriété infoadd.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setINFOADD(TextFtMultiLines value) {
        this.infoadd = value;
    }

    /**
     * Obtient la valeur de la propriété item.
     */
    public int getITEM() {
        return item;
    }

    /**
     * Définit la valeur de la propriété item.
     */
    public void setITEM(int value) {
        this.item = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="AC"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;choice&gt;
     *                   &lt;element name="AC_PRICE" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty"/&gt;
     *                   &lt;element name="AC_CRITERIA" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ac_definition" maxOccurs="20"/&gt;
     *                 &lt;/choice&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "ac"
    })
    @ToString
    @EqualsAndHashCode
    public static class DIRECTIVE200981EC {

        @XmlElement(name = "AC", required = true)
        protected ObjectF15.DIRECTIVE200981EC.AC ac;

        /**
         * Obtient la valeur de la propriété ac.
         *
         * @return possible object is
         * {@link ObjectF15 .DIRECTIVE200981EC.AC }
         */
        public ObjectF15.DIRECTIVE200981EC.AC getAC() {
            return ac;
        }

        /**
         * Définit la valeur de la propriété ac.
         *
         * @param value allowed object is
         *              {@link ObjectF15 .DIRECTIVE200981EC.AC }
         */
        public void setAC(ObjectF15.DIRECTIVE200981EC.AC value) {
            this.ac = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;choice&gt;
         *         &lt;element name="AC_PRICE" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty"/&gt;
         *         &lt;element name="AC_CRITERIA" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ac_definition" maxOccurs="20"/&gt;
         *       &lt;/choice&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "acprice",
                "accriteria"
        })
        @ToString
        @EqualsAndHashCode
        public static class AC {

            @XmlElement(name = "AC_PRICE")
            protected Empty acprice;
            @XmlElement(name = "AC_CRITERIA")
            protected List<AcDefinition> accriteria;

            /**
             * Obtient la valeur de la propriété acprice.
             *
             * @return possible object is
             * {@link Empty }
             */
            public Empty getACPRICE() {
                return acprice;
            }

            /**
             * Définit la valeur de la propriété acprice.
             *
             * @param value allowed object is
             *              {@link Empty }
             */
            public void setACPRICE(Empty value) {
                this.acprice = value;
            }

            /**
             * Gets the value of the accriteria property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the accriteria property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getACCRITERIA().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link AcDefinition }
             */
            public List<AcDefinition> getACCRITERIA() {
                if (accriteria == null) {
                    accriteria = new ArrayList<AcDefinition>();
                }
                return this.accriteria;
            }

        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="AC"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}AC_CRITERION" maxOccurs="20"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "ac"
    })
    @ToString
    @EqualsAndHashCode
    public static class DIRECTIVE201423EU {

        @XmlElement(name = "AC", required = true)
        protected ObjectF15.DIRECTIVE201423EU.AC ac;

        /**
         * Obtient la valeur de la propriété ac.
         *
         * @return possible object is
         * {@link ObjectF15 .DIRECTIVE201423EU.AC }
         */
        public ObjectF15.DIRECTIVE201423EU.AC getAC() {
            return ac;
        }

        /**
         * Définit la valeur de la propriété ac.
         *
         * @param value allowed object is
         *              {@link ObjectF15 .DIRECTIVE201423EU.AC }
         */
        public void setAC(ObjectF15.DIRECTIVE201423EU.AC value) {
            this.ac = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}AC_CRITERION" maxOccurs="20"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "accriterion"
        })
        @ToString
        @EqualsAndHashCode
        public static class AC {

            @XmlElement(name = "AC_CRITERION", required = true)
            protected List<String> accriterion;

            /**
             * Gets the value of the accriterion property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the accriterion property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getACCRITERION().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             */
            public List<String> getACCRITERION() {
                if (accriterion == null) {
                    accriterion = new ArrayList<String>();
                }
                return this.accriterion;
            }

        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="AC"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_criteria"/&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "ac"
    })
    @ToString
    @EqualsAndHashCode
    public static class DIRECTIVE201424EU {

        @XmlElement(name = "AC", required = true)
        protected ObjectF15.DIRECTIVE201424EU.AC ac;

        /**
         * Obtient la valeur de la propriété ac.
         *
         * @return possible object is
         * {@link ObjectF15 .DIRECTIVE201424EU.AC }
         */
        public ObjectF15.DIRECTIVE201424EU.AC getAC() {
            return ac;
        }

        /**
         * Définit la valeur de la propriété ac.
         *
         * @param value allowed object is
         *              {@link ObjectF15 .DIRECTIVE201424EU.AC }
         */
        public void setAC(ObjectF15.DIRECTIVE201424EU.AC value) {
            this.ac = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_criteria"/&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "acquality",
                "accost",
                "acprice"
        })
        @ToString
        @EqualsAndHashCode
        public static class AC {

            @XmlElement(name = "AC_QUALITY")
            protected List<AcDefinition> acquality;
            @XmlElement(name = "AC_COST")
            protected List<AcDefinition> accost;
            @XmlElement(name = "AC_PRICE")
            protected ACPRICE acprice;

            /**
             * Gets the value of the acquality property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the acquality property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getACQUALITY().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link AcDefinition }
             */
            public List<AcDefinition> getACQUALITY() {
                if (acquality == null) {
                    acquality = new ArrayList<AcDefinition>();
                }
                return this.acquality;
            }

            /**
             * Gets the value of the accost property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the accost property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getACCOST().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link AcDefinition }
             */
            public List<AcDefinition> getACCOST() {
                if (accost == null) {
                    accost = new ArrayList<AcDefinition>();
                }
                return this.accost;
            }

            /**
             * Obtient la valeur de la propriété acprice.
             *
             * @return possible object is
             * {@link ACPRICE }
             */
            public ACPRICE getACPRICE() {
                return acprice;
            }

            /**
             * Définit la valeur de la propriété acprice.
             *
             * @param value allowed object is
             *              {@link ACPRICE }
             */
            public void setACPRICE(ACPRICE value) {
                this.acprice = value;
            }

        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="AC"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_man"&gt;
     *                 &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_criteria"/&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "ac"
    })
    @ToString
    @EqualsAndHashCode
    public static class DIRECTIVE201425EU {

        @XmlElement(name = "AC", required = true)
        protected ObjectF15.DIRECTIVE201425EU.AC ac;

        /**
         * Obtient la valeur de la propriété ac.
         *
         * @return possible object is
         * {@link ObjectF15 .DIRECTIVE201425EU.AC }
         */
        public ObjectF15.DIRECTIVE201425EU.AC getAC() {
            return ac;
        }

        /**
         * Définit la valeur de la propriété ac.
         *
         * @param value allowed object is
         *              {@link ObjectF15 .DIRECTIVE201425EU.AC }
         */
        public void setAC(ObjectF15.DIRECTIVE201425EU.AC value) {
            this.ac = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_man"&gt;
         *       &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_criteria"/&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "acquality",
                "accost",
                "acprice"
        })
        @ToString(callSuper = true)
        @EqualsAndHashCode(callSuper = true)
        public static class AC
                extends AgreeToPublicationMan {

            @XmlElement(name = "AC_QUALITY")
            protected List<AcDefinition> acquality;
            @XmlElement(name = "AC_COST")
            protected List<AcDefinition> accost;
            @XmlElement(name = "AC_PRICE")
            protected ACPRICE acprice;

            /**
             * Gets the value of the acquality property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the acquality property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getACQUALITY().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link AcDefinition }
             */
            public List<AcDefinition> getACQUALITY() {
                if (acquality == null) {
                    acquality = new ArrayList<AcDefinition>();
                }
                return this.acquality;
            }

            /**
             * Gets the value of the accost property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the accost property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getACCOST().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link AcDefinition }
             */
            public List<AcDefinition> getACCOST() {
                if (accost == null) {
                    accost = new ArrayList<AcDefinition>();
                }
                return this.accost;
            }

            /**
             * Obtient la valeur de la propriété acprice.
             *
             * @return possible object is
             * {@link ACPRICE }
             */
            public ACPRICE getACPRICE() {
                return acprice;
            }

            /**
             * Définit la valeur de la propriété acprice.
             *
             * @param value allowed object is
             *              {@link ACPRICE }
             */
            public void setACPRICE(ACPRICE value) {
                this.acprice = value;
            }

        }

    }

}
