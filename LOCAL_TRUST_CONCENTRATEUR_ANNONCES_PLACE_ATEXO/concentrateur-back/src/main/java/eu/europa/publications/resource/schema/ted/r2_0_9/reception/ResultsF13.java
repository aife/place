//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


/**
 * Section V: RESULTS OF CONTEST
 *
 * <p>Classe Java pour results_f13 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="results_f13"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="NO_AWARDED_PRIZE" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}no_award"/&gt;
 *         &lt;element name="AWARDED_PRIZE"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="DATE_DECISION_JURY" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}date_full"/&gt;
 *                   &lt;element name="PARTICIPANTS"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_opt"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NB_PARTICIPANTS"/&gt;
 *                             &lt;element name="NB_PARTICIPANTS_SME" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}_3car" minOccurs="0"/&gt;
 *                             &lt;element name="NB_PARTICIPANTS_OTHER_EU" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}_3car" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="WINNERS"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_opt"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="WINNER" maxOccurs="100"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="ADDRESS_WINNER"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contact_contractor"&gt;
 *                                             &lt;/extension&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;choice&gt;
 *                                         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SME"/&gt;
 *                                         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NO_SME"/&gt;
 *                                       &lt;/choice&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="VAL_PRIZE" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;simpleContent&gt;
 *                         &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;val"&gt;
 *                           &lt;attribute name="PUBLICATION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}publication" /&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/simpleContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "results_f13", propOrder = {
        "noawardedprize",
        "awardedprize"
})
@ToString
@EqualsAndHashCode
public class ResultsF13 {

    @XmlElement(name = "NO_AWARDED_PRIZE")
    protected NoAward noawardedprize;
    @XmlElement(name = "AWARDED_PRIZE")
    protected ResultsF13.AWARDEDPRIZE awardedprize;

    /**
     * Obtient la valeur de la propriété noawardedprize.
     *
     * @return possible object is
     * {@link NoAward }
     */
    public NoAward getNOAWARDEDPRIZE() {
        return noawardedprize;
    }

    /**
     * Définit la valeur de la propriété noawardedprize.
     *
     * @param value allowed object is
     *              {@link NoAward }
     */
    public void setNOAWARDEDPRIZE(NoAward value) {
        this.noawardedprize = value;
    }

    /**
     * Obtient la valeur de la propriété awardedprize.
     *
     * @return possible object is
     * {@link ResultsF13 .AWARDEDPRIZE }
     */
    public ResultsF13.AWARDEDPRIZE getAWARDEDPRIZE() {
        return awardedprize;
    }

    /**
     * Définit la valeur de la propriété awardedprize.
     *
     * @param value allowed object is
     *              {@link ResultsF13 .AWARDEDPRIZE }
     */
    public void setAWARDEDPRIZE(ResultsF13.AWARDEDPRIZE value) {
        this.awardedprize = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="DATE_DECISION_JURY" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}date_full"/&gt;
     *         &lt;element name="PARTICIPANTS"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_opt"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NB_PARTICIPANTS"/&gt;
     *                   &lt;element name="NB_PARTICIPANTS_SME" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}_3car" minOccurs="0"/&gt;
     *                   &lt;element name="NB_PARTICIPANTS_OTHER_EU" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}_3car" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="WINNERS"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_opt"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="WINNER" maxOccurs="100"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="ADDRESS_WINNER"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contact_contractor"&gt;
     *                                   &lt;/extension&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;choice&gt;
     *                               &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SME"/&gt;
     *                               &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NO_SME"/&gt;
     *                             &lt;/choice&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="VAL_PRIZE" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;val"&gt;
     *                 &lt;attribute name="PUBLICATION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}publication" /&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "datedecisionjury",
            "participants",
            "winners",
            "valprize"
    })
    @ToString
    @EqualsAndHashCode
    public static class AWARDEDPRIZE {

        @XmlElement(name = "DATE_DECISION_JURY", required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar datedecisionjury;
        @XmlElement(name = "PARTICIPANTS", required = true)
        protected ResultsF13.AWARDEDPRIZE.PARTICIPANTS participants;
        @XmlElement(name = "WINNERS", required = true)
        protected ResultsF13.AWARDEDPRIZE.WINNERS winners;
        @XmlElement(name = "VAL_PRIZE")
        protected ResultsF13.AWARDEDPRIZE.VALPRIZE valprize;

        /**
         * Obtient la valeur de la propriété datedecisionjury.
         *
         * @return possible object is
         * {@link XMLGregorianCalendar }
         */
        public XMLGregorianCalendar getDATEDECISIONJURY() {
            return datedecisionjury;
        }

        /**
         * Définit la valeur de la propriété datedecisionjury.
         *
         * @param value allowed object is
         *              {@link XMLGregorianCalendar }
         */
        public void setDATEDECISIONJURY(XMLGregorianCalendar value) {
            this.datedecisionjury = value;
        }

        /**
         * Obtient la valeur de la propriété participants.
         *
         * @return possible object is
         * {@link ResultsF13 .AWARDEDPRIZE.PARTICIPANTS }
         */
        public ResultsF13.AWARDEDPRIZE.PARTICIPANTS getPARTICIPANTS() {
            return participants;
        }

        /**
         * Définit la valeur de la propriété participants.
         *
         * @param value allowed object is
         *              {@link ResultsF13 .AWARDEDPRIZE.PARTICIPANTS }
         */
        public void setPARTICIPANTS(ResultsF13.AWARDEDPRIZE.PARTICIPANTS value) {
            this.participants = value;
        }

        /**
         * Obtient la valeur de la propriété winners.
         *
         * @return possible object is
         * {@link ResultsF13 .AWARDEDPRIZE.WINNERS }
         */
        public ResultsF13.AWARDEDPRIZE.WINNERS getWINNERS() {
            return winners;
        }

        /**
         * Définit la valeur de la propriété winners.
         *
         * @param value allowed object is
         *              {@link ResultsF13 .AWARDEDPRIZE.WINNERS }
         */
        public void setWINNERS(ResultsF13.AWARDEDPRIZE.WINNERS value) {
            this.winners = value;
        }

        /**
         * Obtient la valeur de la propriété valprize.
         *
         * @return possible object is
         * {@link ResultsF13 .AWARDEDPRIZE.VALPRIZE }
         */
        public ResultsF13.AWARDEDPRIZE.VALPRIZE getVALPRIZE() {
            return valprize;
        }

        /**
         * Définit la valeur de la propriété valprize.
         *
         * @param value allowed object is
         *              {@link ResultsF13 .AWARDEDPRIZE.VALPRIZE }
         */
        public void setVALPRIZE(ResultsF13.AWARDEDPRIZE.VALPRIZE value) {
            this.valprize = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_opt"&gt;
         *       &lt;sequence&gt;
         *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NB_PARTICIPANTS"/&gt;
         *         &lt;element name="NB_PARTICIPANTS_SME" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}_3car" minOccurs="0"/&gt;
         *         &lt;element name="NB_PARTICIPANTS_OTHER_EU" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}_3car" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "nbparticipants",
                "nbparticipantssme",
                "nbparticipantsothereu"
        })
        @ToString(callSuper = true)
        @EqualsAndHashCode(callSuper = true)
        public static class PARTICIPANTS
                extends AgreeToPublicationOpt {

            @XmlElement(name = "NB_PARTICIPANTS", required = true)
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger nbparticipants;
            @XmlElement(name = "NB_PARTICIPANTS_SME")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger nbparticipantssme;
            @XmlElement(name = "NB_PARTICIPANTS_OTHER_EU")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger nbparticipantsothereu;

            /**
             * Obtient la valeur de la propriété nbparticipants.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getNBPARTICIPANTS() {
                return nbparticipants;
            }

            /**
             * Définit la valeur de la propriété nbparticipants.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setNBPARTICIPANTS(BigInteger value) {
                this.nbparticipants = value;
            }

            /**
             * Obtient la valeur de la propriété nbparticipantssme.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getNBPARTICIPANTSSME() {
                return nbparticipantssme;
            }

            /**
             * Définit la valeur de la propriété nbparticipantssme.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setNBPARTICIPANTSSME(BigInteger value) {
                this.nbparticipantssme = value;
            }

            /**
             * Obtient la valeur de la propriété nbparticipantsothereu.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getNBPARTICIPANTSOTHEREU() {
                return nbparticipantsothereu;
            }

            /**
             * Définit la valeur de la propriété nbparticipantsothereu.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setNBPARTICIPANTSOTHEREU(BigInteger value) {
                this.nbparticipantsothereu = value;
            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;val"&gt;
         *       &lt;attribute name="PUBLICATION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}publication" /&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        @ToString(callSuper = true)
        @EqualsAndHashCode(callSuper = true)
        public static class VALPRIZE
                extends Val {

            @XmlAttribute(name = "PUBLICATION")
            protected Publication publication;

            /**
             * Obtient la valeur de la propriété publication.
             *
             * @return possible object is
             * {@link Publication }
             */
            public Publication getPUBLICATION() {
                return publication;
            }

            /**
             * Définit la valeur de la propriété publication.
             *
             * @param value allowed object is
             *              {@link Publication }
             */
            public void setPUBLICATION(Publication value) {
                this.publication = value;
            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_opt"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="WINNER" maxOccurs="100"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="ADDRESS_WINNER"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contact_contractor"&gt;
         *                         &lt;/extension&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;choice&gt;
         *                     &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SME"/&gt;
         *                     &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NO_SME"/&gt;
         *                   &lt;/choice&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "winner"
        })
        @ToString(callSuper = true)
        @EqualsAndHashCode(callSuper = true)
        public static class WINNERS
                extends AgreeToPublicationOpt {

            @XmlElement(name = "WINNER", required = true)
            protected List<ResultsF13.AWARDEDPRIZE.WINNERS.WINNER> winner;

            /**
             * Gets the value of the winner property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the winner property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getWINNER().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ResultsF13 .AWARDEDPRIZE.WINNERS.WINNER }
             */
            public List<ResultsF13.AWARDEDPRIZE.WINNERS.WINNER> getWINNER() {
                if (winner == null) {
                    winner = new ArrayList<ResultsF13.AWARDEDPRIZE.WINNERS.WINNER>();
                }
                return this.winner;
            }


            /**
             * <p>Classe Java pour anonymous complex type.
             *
             * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="ADDRESS_WINNER"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contact_contractor"&gt;
             *               &lt;/extension&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;choice&gt;
             *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SME"/&gt;
             *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NO_SME"/&gt;
             *         &lt;/choice&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "addresswinner",
                    "sme",
                    "nosme"
            })
            @ToString
            @EqualsAndHashCode
            public static class WINNER {

                @XmlElement(name = "ADDRESS_WINNER", required = true)
                protected ResultsF13.AWARDEDPRIZE.WINNERS.WINNER.ADDRESSWINNER addresswinner;
                @XmlElement(name = "SME")
                protected Empty sme;
                @XmlElement(name = "NO_SME")
                protected Empty nosme;

                /**
                 * Obtient la valeur de la propriété addresswinner.
                 *
                 * @return possible object is
                 * {@link ResultsF13 .AWARDEDPRIZE.WINNERS.WINNER.ADDRESSWINNER }
                 */
                public ResultsF13.AWARDEDPRIZE.WINNERS.WINNER.ADDRESSWINNER getADDRESSWINNER() {
                    return addresswinner;
                }

                /**
                 * Définit la valeur de la propriété addresswinner.
                 *
                 * @param value allowed object is
                 *              {@link ResultsF13 .AWARDEDPRIZE.WINNERS.WINNER.ADDRESSWINNER }
                 */
                public void setADDRESSWINNER(ResultsF13.AWARDEDPRIZE.WINNERS.WINNER.ADDRESSWINNER value) {
                    this.addresswinner = value;
                }

                /**
                 * Obtient la valeur de la propriété sme.
                 *
                 * @return possible object is
                 * {@link Empty }
                 */
                public Empty getSME() {
                    return sme;
                }

                /**
                 * Définit la valeur de la propriété sme.
                 *
                 * @param value allowed object is
                 *              {@link Empty }
                 */
                public void setSME(Empty value) {
                    this.sme = value;
                }

                /**
                 * Obtient la valeur de la propriété nosme.
                 *
                 * @return possible object is
                 * {@link Empty }
                 */
                public Empty getNOSME() {
                    return nosme;
                }

                /**
                 * Définit la valeur de la propriété nosme.
                 *
                 * @param value allowed object is
                 *              {@link Empty }
                 */
                public void setNOSME(Empty value) {
                    this.nosme = value;
                }


                /**
                 * <p>Classe Java pour anonymous complex type.
                 *
                 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
                 *
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contact_contractor"&gt;
                 *     &lt;/extension&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                @ToString(callSuper = true)
                @EqualsAndHashCode(callSuper = true)
                public static class ADDRESSWINNER
                        extends ContactContractor {


                }

            }

        }

    }

}
