//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * SECTION III: LEGAL, ECONOMIC, FINANCIAL AND TECHNICAL INFORMATION
 *
 * <p>Classe Java pour lefti complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="lefti"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SUITABILITY" minOccurs="0"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ECONOMIC_CRITERIA_DOC"/&gt;
 *           &lt;sequence&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ECONOMIC_FINANCIAL_INFO" minOccurs="0"/&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ECONOMIC_FINANCIAL_MIN_LEVEL" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *         &lt;/choice&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TECHNICAL_CRITERIA_DOC"/&gt;
 *           &lt;sequence&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TECHNICAL_PROFESSIONAL_INFO" minOccurs="0"/&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TECHNICAL_PROFESSIONAL_MIN_LEVEL" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *         &lt;/choice&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}RULES_CRITERIA" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}RESTRICTED_SHELTERED_WORKSHOP" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}RESTRICTED_SHELTERED_PROGRAM" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}RESERVED_ORGANISATIONS_SERVICE_MISSION" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DEPOSIT_GUARANTEE_REQUIRED" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}MAIN_FINANCING_CONDITION" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LEGAL_FORM" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}QUALIFICATION" maxOccurs="20" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CRITERIA_SELECTION" minOccurs="0"/&gt;
 *         &lt;sequence minOccurs="0"&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PARTICULAR_PROFESSION"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}REFERENCE_TO_LAW" minOccurs="0"/&gt;
 *         &lt;/sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PERFORMANCE_CONDITIONS" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PERFORMANCE_STAFF_QUALIFICATION" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "lefti", propOrder = {
        "suitability",
        "economiccriteriadoc",
        "economicfinancialinfo",
        "economicfinancialminlevel",
        "technicalcriteriadoc",
        "technicalprofessionalinfo",
        "technicalprofessionalminlevel",
        "rulescriteria",
        "restrictedshelteredworkshop",
        "restrictedshelteredprogram",
        "reservedorganisationsservicemission",
        "depositguaranteerequired",
        "mainfinancingcondition",
        "legalform",
        "qualification",
        "criteriaselection",
        "particularprofession",
        "referencetolaw",
        "performanceconditions",
        "performancestaffqualification"
})
@XmlSeeAlso({
        LeftiF01.class,
        LeftiF02.class,
        LeftiF04.class,
        LeftiF05.class,
        LeftiF07.class,
        LeftiF21.class,
        LeftiF22.class,
        LeftiF23.class,
        LeftiF24.class
})
@ToString
@EqualsAndHashCode
public class Lefti {

    @XmlElement(name = "SUITABILITY")
    protected TextFtMultiLines suitability;
    @XmlElement(name = "ECONOMIC_CRITERIA_DOC")
    protected Empty economiccriteriadoc;
    @XmlElement(name = "ECONOMIC_FINANCIAL_INFO")
    protected TextFtMultiLines economicfinancialinfo;
    @XmlElement(name = "ECONOMIC_FINANCIAL_MIN_LEVEL")
    protected TextFtMultiLines economicfinancialminlevel;
    @XmlElement(name = "TECHNICAL_CRITERIA_DOC")
    protected Empty technicalcriteriadoc;
    @XmlElement(name = "TECHNICAL_PROFESSIONAL_INFO")
    protected TextFtMultiLines technicalprofessionalinfo;
    @XmlElement(name = "TECHNICAL_PROFESSIONAL_MIN_LEVEL")
    protected TextFtMultiLines technicalprofessionalminlevel;
    @XmlElement(name = "RULES_CRITERIA")
    protected TextFtMultiLines rulescriteria;
    @XmlElement(name = "RESTRICTED_SHELTERED_WORKSHOP")
    protected Empty restrictedshelteredworkshop;
    @XmlElement(name = "RESTRICTED_SHELTERED_PROGRAM")
    protected Empty restrictedshelteredprogram;
    @XmlElement(name = "RESERVED_ORGANISATIONS_SERVICE_MISSION")
    protected Empty reservedorganisationsservicemission;
    @XmlElement(name = "DEPOSIT_GUARANTEE_REQUIRED")
    protected TextFtMultiLines depositguaranteerequired;
    @XmlElement(name = "MAIN_FINANCING_CONDITION")
    protected TextFtMultiLines mainfinancingcondition;
    @XmlElement(name = "LEGAL_FORM")
    protected TextFtMultiLines legalform;
    @XmlElement(name = "QUALIFICATION")
    protected List<QUALIFICATION> qualification;
    @XmlElement(name = "CRITERIA_SELECTION")
    protected TextFtMultiLines criteriaselection;
    @XmlElement(name = "PARTICULAR_PROFESSION")
    protected Services particularprofession;
    @XmlElement(name = "REFERENCE_TO_LAW")
    protected TextFtMultiLines referencetolaw;
    @XmlElement(name = "PERFORMANCE_CONDITIONS")
    protected TextFtMultiLines performanceconditions;
    @XmlElement(name = "PERFORMANCE_STAFF_QUALIFICATION")
    protected Empty performancestaffqualification;

    /**
     * Obtient la valeur de la propriété suitability.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getSUITABILITY() {
        return suitability;
    }

    /**
     * Définit la valeur de la propriété suitability.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setSUITABILITY(TextFtMultiLines value) {
        this.suitability = value;
    }

    /**
     * Obtient la valeur de la propriété economiccriteriadoc.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getECONOMICCRITERIADOC() {
        return economiccriteriadoc;
    }

    /**
     * Définit la valeur de la propriété economiccriteriadoc.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setECONOMICCRITERIADOC(Empty value) {
        this.economiccriteriadoc = value;
    }

    /**
     * Obtient la valeur de la propriété economicfinancialinfo.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getECONOMICFINANCIALINFO() {
        return economicfinancialinfo;
    }

    /**
     * Définit la valeur de la propriété economicfinancialinfo.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setECONOMICFINANCIALINFO(TextFtMultiLines value) {
        this.economicfinancialinfo = value;
    }

    /**
     * Obtient la valeur de la propriété economicfinancialminlevel.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getECONOMICFINANCIALMINLEVEL() {
        return economicfinancialminlevel;
    }

    /**
     * Définit la valeur de la propriété economicfinancialminlevel.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setECONOMICFINANCIALMINLEVEL(TextFtMultiLines value) {
        this.economicfinancialminlevel = value;
    }

    /**
     * Obtient la valeur de la propriété technicalcriteriadoc.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getTECHNICALCRITERIADOC() {
        return technicalcriteriadoc;
    }

    /**
     * Définit la valeur de la propriété technicalcriteriadoc.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setTECHNICALCRITERIADOC(Empty value) {
        this.technicalcriteriadoc = value;
    }

    /**
     * Obtient la valeur de la propriété technicalprofessionalinfo.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getTECHNICALPROFESSIONALINFO() {
        return technicalprofessionalinfo;
    }

    /**
     * Définit la valeur de la propriété technicalprofessionalinfo.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setTECHNICALPROFESSIONALINFO(TextFtMultiLines value) {
        this.technicalprofessionalinfo = value;
    }

    /**
     * Obtient la valeur de la propriété technicalprofessionalminlevel.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getTECHNICALPROFESSIONALMINLEVEL() {
        return technicalprofessionalminlevel;
    }

    /**
     * Définit la valeur de la propriété technicalprofessionalminlevel.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setTECHNICALPROFESSIONALMINLEVEL(TextFtMultiLines value) {
        this.technicalprofessionalminlevel = value;
    }

    /**
     * Obtient la valeur de la propriété rulescriteria.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getRULESCRITERIA() {
        return rulescriteria;
    }

    /**
     * Définit la valeur de la propriété rulescriteria.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setRULESCRITERIA(TextFtMultiLines value) {
        this.rulescriteria = value;
    }

    /**
     * Obtient la valeur de la propriété restrictedshelteredworkshop.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getRESTRICTEDSHELTEREDWORKSHOP() {
        return restrictedshelteredworkshop;
    }

    /**
     * Définit la valeur de la propriété restrictedshelteredworkshop.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setRESTRICTEDSHELTEREDWORKSHOP(Empty value) {
        this.restrictedshelteredworkshop = value;
    }

    /**
     * Obtient la valeur de la propriété restrictedshelteredprogram.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getRESTRICTEDSHELTEREDPROGRAM() {
        return restrictedshelteredprogram;
    }

    /**
     * Définit la valeur de la propriété restrictedshelteredprogram.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setRESTRICTEDSHELTEREDPROGRAM(Empty value) {
        this.restrictedshelteredprogram = value;
    }

    /**
     * Obtient la valeur de la propriété reservedorganisationsservicemission.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getRESERVEDORGANISATIONSSERVICEMISSION() {
        return reservedorganisationsservicemission;
    }

    /**
     * Définit la valeur de la propriété reservedorganisationsservicemission.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setRESERVEDORGANISATIONSSERVICEMISSION(Empty value) {
        this.reservedorganisationsservicemission = value;
    }

    /**
     * Obtient la valeur de la propriété depositguaranteerequired.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getDEPOSITGUARANTEEREQUIRED() {
        return depositguaranteerequired;
    }

    /**
     * Définit la valeur de la propriété depositguaranteerequired.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setDEPOSITGUARANTEEREQUIRED(TextFtMultiLines value) {
        this.depositguaranteerequired = value;
    }

    /**
     * Obtient la valeur de la propriété mainfinancingcondition.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getMAINFINANCINGCONDITION() {
        return mainfinancingcondition;
    }

    /**
     * Définit la valeur de la propriété mainfinancingcondition.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setMAINFINANCINGCONDITION(TextFtMultiLines value) {
        this.mainfinancingcondition = value;
    }

    /**
     * Obtient la valeur de la propriété legalform.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getLEGALFORM() {
        return legalform;
    }

    /**
     * Définit la valeur de la propriété legalform.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setLEGALFORM(TextFtMultiLines value) {
        this.legalform = value;
    }

    /**
     * Gets the value of the qualification property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the qualification property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQUALIFICATION().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QUALIFICATION }
     */
    public List<QUALIFICATION> getQUALIFICATION() {
        if (qualification == null) {
            qualification = new ArrayList<QUALIFICATION>();
        }
        return this.qualification;
    }

    /**
     * Obtient la valeur de la propriété criteriaselection.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getCRITERIASELECTION() {
        return criteriaselection;
    }

    /**
     * Définit la valeur de la propriété criteriaselection.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setCRITERIASELECTION(TextFtMultiLines value) {
        this.criteriaselection = value;
    }

    /**
     * Obtient la valeur de la propriété particularprofession.
     *
     * @return possible object is
     * {@link Services }
     */
    public Services getPARTICULARPROFESSION() {
        return particularprofession;
    }

    /**
     * Définit la valeur de la propriété particularprofession.
     *
     * @param value allowed object is
     *              {@link Services }
     */
    public void setPARTICULARPROFESSION(Services value) {
        this.particularprofession = value;
    }

    /**
     * Obtient la valeur de la propriété referencetolaw.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getREFERENCETOLAW() {
        return referencetolaw;
    }

    /**
     * Définit la valeur de la propriété referencetolaw.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setREFERENCETOLAW(TextFtMultiLines value) {
        this.referencetolaw = value;
    }

    /**
     * Obtient la valeur de la propriété performanceconditions.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getPERFORMANCECONDITIONS() {
        return performanceconditions;
    }

    /**
     * Définit la valeur de la propriété performanceconditions.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setPERFORMANCECONDITIONS(TextFtMultiLines value) {
        this.performanceconditions = value;
    }

    /**
     * Obtient la valeur de la propriété performancestaffqualification.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPERFORMANCESTAFFQUALIFICATION() {
        return performancestaffqualification;
    }

    /**
     * Définit la valeur de la propriété performancestaffqualification.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPERFORMANCESTAFFQUALIFICATION(Empty value) {
        this.performancestaffqualification = value;
    }

}
