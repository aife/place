//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import eu.europa.publications.resource.schema.ted._2021.nuts.Nuts;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java pour object_f03 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="object_f03"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TITLE" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LOT_NO" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CPV_ADDITIONAL" maxOccurs="100" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/2016/nuts}NUTS" maxOccurs="250"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}MAIN_SITE" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SHORT_DESCR"/&gt;
 *         &lt;element name="AC"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_criteria"/&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}options"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}eu_union_funds"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}INFO_ADD" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ITEM" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}nb_lot" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "object_f03", propOrder = {
        "title",
        "lotno",
        "cpvadditional",
        "nuts",
        "mainsite",
        "shortdescr",
        "ac",
        "options",
        "optionsdescr",
        "nooptions",
        "euprogrrelated",
        "noeuprogrrelated",
        "infoadd"
})
@ToString
@EqualsAndHashCode
public class ObjectF03 {

    @XmlElement(name = "TITLE")
    protected TextFtSingleLine title;
    @XmlElement(name = "LOT_NO")
    protected String lotno;
    @XmlElement(name = "CPV_ADDITIONAL")
    protected List<CpvSet> cpvadditional;
    @XmlElement(name = "NUTS", namespace = "http://publications.europa.eu/resource/schema/ted/2016/nuts", required = true)
    protected List<Nuts> nuts;
    @XmlElement(name = "MAIN_SITE")
    protected TextFtMultiLines mainsite;
    @XmlElement(name = "SHORT_DESCR", required = true)
    protected TextFtMultiLines shortdescr;
    @XmlElement(name = "AC", required = true)
    protected ObjectF03.AC ac;
    @XmlElement(name = "OPTIONS")
    protected Empty options;
    @XmlElement(name = "OPTIONS_DESCR")
    protected TextFtMultiLines optionsdescr;
    @XmlElement(name = "NO_OPTIONS")
    protected Empty nooptions;
    @XmlElement(name = "EU_PROGR_RELATED")
    protected TextFtMultiLines euprogrrelated;
    @XmlElement(name = "NO_EU_PROGR_RELATED")
    protected Empty noeuprogrrelated;
    @XmlElement(name = "INFO_ADD")
    protected TextFtMultiLines infoadd;
    @XmlAttribute(name = "ITEM", required = true)
    protected int item;

    /**
     * Obtient la valeur de la propriété title.
     *
     * @return possible object is
     * {@link TextFtSingleLine }
     */
    public TextFtSingleLine getTITLE() {
        return title;
    }

    /**
     * Définit la valeur de la propriété title.
     *
     * @param value allowed object is
     *              {@link TextFtSingleLine }
     */
    public void setTITLE(TextFtSingleLine value) {
        this.title = value;
    }

    /**
     * Obtient la valeur de la propriété lotno.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLOTNO() {
        return lotno;
    }

    /**
     * Définit la valeur de la propriété lotno.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLOTNO(String value) {
        this.lotno = value;
    }

    /**
     * Gets the value of the cpvadditional property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cpvadditional property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCPVADDITIONAL().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CpvSet }
     */
    public List<CpvSet> getCPVADDITIONAL() {
        if (cpvadditional == null) {
            cpvadditional = new ArrayList<CpvSet>();
        }
        return this.cpvadditional;
    }

    /**
     * Gets the value of the nuts property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nuts property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNUTS().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Nuts }
     */
    public List<Nuts> getNUTS() {
        if (nuts == null) {
            nuts = new ArrayList<Nuts>();
        }
        return this.nuts;
    }

    /**
     * Obtient la valeur de la propriété mainsite.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getMAINSITE() {
        return mainsite;
    }

    /**
     * Définit la valeur de la propriété mainsite.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setMAINSITE(TextFtMultiLines value) {
        this.mainsite = value;
    }

    /**
     * Obtient la valeur de la propriété shortdescr.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getSHORTDESCR() {
        return shortdescr;
    }

    /**
     * Définit la valeur de la propriété shortdescr.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setSHORTDESCR(TextFtMultiLines value) {
        this.shortdescr = value;
    }

    /**
     * Obtient la valeur de la propriété ac.
     *
     * @return possible object is
     * {@link ObjectF03 .AC }
     */
    public ObjectF03.AC getAC() {
        return ac;
    }

    /**
     * Définit la valeur de la propriété ac.
     *
     * @param value allowed object is
     *              {@link ObjectF03 .AC }
     */
    public void setAC(ObjectF03.AC value) {
        this.ac = value;
    }

    /**
     * Obtient la valeur de la propriété options.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getOPTIONS() {
        return options;
    }

    /**
     * Définit la valeur de la propriété options.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setOPTIONS(Empty value) {
        this.options = value;
    }

    /**
     * Obtient la valeur de la propriété optionsdescr.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getOPTIONSDESCR() {
        return optionsdescr;
    }

    /**
     * Définit la valeur de la propriété optionsdescr.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setOPTIONSDESCR(TextFtMultiLines value) {
        this.optionsdescr = value;
    }

    /**
     * Obtient la valeur de la propriété nooptions.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getNOOPTIONS() {
        return nooptions;
    }

    /**
     * Définit la valeur de la propriété nooptions.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setNOOPTIONS(Empty value) {
        this.nooptions = value;
    }

    /**
     * Obtient la valeur de la propriété euprogrrelated.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getEUPROGRRELATED() {
        return euprogrrelated;
    }

    /**
     * Définit la valeur de la propriété euprogrrelated.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setEUPROGRRELATED(TextFtMultiLines value) {
        this.euprogrrelated = value;
    }

    /**
     * Obtient la valeur de la propriété noeuprogrrelated.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getNOEUPROGRRELATED() {
        return noeuprogrrelated;
    }

    /**
     * Définit la valeur de la propriété noeuprogrrelated.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setNOEUPROGRRELATED(Empty value) {
        this.noeuprogrrelated = value;
    }

    /**
     * Obtient la valeur de la propriété infoadd.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getINFOADD() {
        return infoadd;
    }

    /**
     * Définit la valeur de la propriété infoadd.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setINFOADD(TextFtMultiLines value) {
        this.infoadd = value;
    }

    /**
     * Obtient la valeur de la propriété item.
     */
    public int getITEM() {
        return item;
    }

    /**
     * Définit la valeur de la propriété item.
     */
    public void setITEM(int value) {
        this.item = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_criteria"/&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "acquality",
            "accost",
            "acprice"
    })
    @ToString
    @EqualsAndHashCode
    public static class AC {

        @XmlElement(name = "AC_QUALITY")
        protected List<AcDefinition> acquality;
        @XmlElement(name = "AC_COST")
        protected List<AcDefinition> accost;
        @XmlElement(name = "AC_PRICE")
        protected ACPRICE acprice;

        /**
         * Gets the value of the acquality property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the acquality property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getACQUALITY().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AcDefinition }
         */
        public List<AcDefinition> getACQUALITY() {
            if (acquality == null) {
                acquality = new ArrayList<AcDefinition>();
            }
            return this.acquality;
        }

        /**
         * Gets the value of the accost property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the accost property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getACCOST().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AcDefinition }
         */
        public List<AcDefinition> getACCOST() {
            if (accost == null) {
                accost = new ArrayList<AcDefinition>();
            }
            return this.accost;
        }

        /**
         * Obtient la valeur de la propriété acprice.
         *
         * @return possible object is
         * {@link ACPRICE }
         */
        public ACPRICE getACPRICE() {
            return acprice;
        }

        /**
         * Définit la valeur de la propriété acprice.
         *
         * @param value allowed object is
         *              {@link ACPRICE }
         */
        public void setACPRICE(ACPRICE value) {
            this.acprice = value;
        }

    }

}
