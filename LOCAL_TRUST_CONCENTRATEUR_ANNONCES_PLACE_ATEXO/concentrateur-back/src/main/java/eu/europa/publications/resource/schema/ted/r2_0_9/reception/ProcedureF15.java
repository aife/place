//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Section IV: PROCEDURE
 *
 * <p>Classe Java pour procedure_f15 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="procedure_f15"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="DIRECTIVE_2014_23_EU"&gt;
 *             &lt;complexType&gt;
 *               &lt;complexContent&gt;
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                   &lt;choice&gt;
 *                     &lt;element name="PT_AWARD_CONTRACT_WITHOUT_PUBLICATION"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;sequence&gt;
 *                               &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d4_part1"/&gt;
 *                               &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;element name="PT_AWARD_CONTRACT_WITHOUT_CALL"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;sequence&gt;
 *                               &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_part2"/&gt;
 *                               &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                   &lt;/choice&gt;
 *                 &lt;/restriction&gt;
 *               &lt;/complexContent&gt;
 *             &lt;/complexType&gt;
 *           &lt;/element&gt;
 *           &lt;element name="DIRECTIVE_2014_24_EU"&gt;
 *             &lt;complexType&gt;
 *               &lt;complexContent&gt;
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                   &lt;choice&gt;
 *                     &lt;element name="PT_NEGOTIATED_WITHOUT_PUBLICATION"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;sequence&gt;
 *                               &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d1_part1"/&gt;
 *                               &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;element name="PT_AWARD_CONTRACT_WITHOUT_CALL"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;sequence&gt;
 *                               &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_part2"/&gt;
 *                               &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                   &lt;/choice&gt;
 *                 &lt;/restriction&gt;
 *               &lt;/complexContent&gt;
 *             &lt;/complexType&gt;
 *           &lt;/element&gt;
 *           &lt;element name="DIRECTIVE_2014_25_EU"&gt;
 *             &lt;complexType&gt;
 *               &lt;complexContent&gt;
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                   &lt;choice&gt;
 *                     &lt;element name="PT_NEGOTIATED_WITHOUT_PUBLICATION"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;sequence&gt;
 *                               &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d2_part1"/&gt;
 *                               &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;element name="PT_AWARD_CONTRACT_WITHOUT_CALL"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;sequence&gt;
 *                               &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_part2"/&gt;
 *                               &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                   &lt;/choice&gt;
 *                 &lt;/restriction&gt;
 *               &lt;/complexContent&gt;
 *             &lt;/complexType&gt;
 *           &lt;/element&gt;
 *           &lt;element name="DIRECTIVE_2009_81_EC"&gt;
 *             &lt;complexType&gt;
 *               &lt;complexContent&gt;
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                   &lt;choice&gt;
 *                     &lt;element name="PT_NEGOTIATED_WITHOUT_PUBLICATION"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;sequence&gt;
 *                               &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d3_part1"/&gt;
 *                               &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;element name="PT_AWARD_CONTRACT_WITHOUT_CALL"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;sequence&gt;
 *                               &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d3_part2"/&gt;
 *                               &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                   &lt;/choice&gt;
 *                 &lt;/restriction&gt;
 *               &lt;/complexContent&gt;
 *             &lt;/complexType&gt;
 *           &lt;/element&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="FRAMEWORK" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}gpa"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NOTICE_NUMBER_OJ" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "procedure_f15", propOrder = {
        "directive201423EU",
        "directive201424EU",
        "directive201425EU",
        "directive200981EC",
        "framework",
        "contractcoveredgpa",
        "nocontractcoveredgpa",
        "noticenumberoj"
})
@ToString
@EqualsAndHashCode
public class ProcedureF15 {

    @XmlElement(name = "DIRECTIVE_2014_23_EU")
    protected ProcedureF15.DIRECTIVE201423EU directive201423EU;
    @XmlElement(name = "DIRECTIVE_2014_24_EU")
    protected ProcedureF15.DIRECTIVE201424EU directive201424EU;
    @XmlElement(name = "DIRECTIVE_2014_25_EU")
    protected ProcedureF15.DIRECTIVE201425EU directive201425EU;
    @XmlElement(name = "DIRECTIVE_2009_81_EC")
    protected ProcedureF15.DIRECTIVE200981EC directive200981EC;
    @XmlElement(name = "FRAMEWORK")
    protected Empty framework;
    @XmlElement(name = "CONTRACT_COVERED_GPA")
    protected Empty contractcoveredgpa;
    @XmlElement(name = "NO_CONTRACT_COVERED_GPA")
    protected Empty nocontractcoveredgpa;
    @XmlElement(name = "NOTICE_NUMBER_OJ")
    protected String noticenumberoj;

    /**
     * Obtient la valeur de la propriété directive201423EU.
     *
     * @return possible object is
     * {@link ProcedureF15 .DIRECTIVE201423EU }
     */
    public ProcedureF15.DIRECTIVE201423EU getDIRECTIVE201423EU() {
        return directive201423EU;
    }

    /**
     * Définit la valeur de la propriété directive201423EU.
     *
     * @param value allowed object is
     *              {@link ProcedureF15 .DIRECTIVE201423EU }
     */
    public void setDIRECTIVE201423EU(ProcedureF15.DIRECTIVE201423EU value) {
        this.directive201423EU = value;
    }

    /**
     * Obtient la valeur de la propriété directive201424EU.
     *
     * @return possible object is
     * {@link ProcedureF15 .DIRECTIVE201424EU }
     */
    public ProcedureF15.DIRECTIVE201424EU getDIRECTIVE201424EU() {
        return directive201424EU;
    }

    /**
     * Définit la valeur de la propriété directive201424EU.
     *
     * @param value allowed object is
     *              {@link ProcedureF15 .DIRECTIVE201424EU }
     */
    public void setDIRECTIVE201424EU(ProcedureF15.DIRECTIVE201424EU value) {
        this.directive201424EU = value;
    }

    /**
     * Obtient la valeur de la propriété directive201425EU.
     *
     * @return possible object is
     * {@link ProcedureF15 .DIRECTIVE201425EU }
     */
    public ProcedureF15.DIRECTIVE201425EU getDIRECTIVE201425EU() {
        return directive201425EU;
    }

    /**
     * Définit la valeur de la propriété directive201425EU.
     *
     * @param value allowed object is
     *              {@link ProcedureF15 .DIRECTIVE201425EU }
     */
    public void setDIRECTIVE201425EU(ProcedureF15.DIRECTIVE201425EU value) {
        this.directive201425EU = value;
    }

    /**
     * Obtient la valeur de la propriété directive200981EC.
     *
     * @return possible object is
     * {@link ProcedureF15 .DIRECTIVE200981EC }
     */
    public ProcedureF15.DIRECTIVE200981EC getDIRECTIVE200981EC() {
        return directive200981EC;
    }

    /**
     * Définit la valeur de la propriété directive200981EC.
     *
     * @param value allowed object is
     *              {@link ProcedureF15 .DIRECTIVE200981EC }
     */
    public void setDIRECTIVE200981EC(ProcedureF15.DIRECTIVE200981EC value) {
        this.directive200981EC = value;
    }

    /**
     * Obtient la valeur de la propriété framework.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getFRAMEWORK() {
        return framework;
    }

    /**
     * Définit la valeur de la propriété framework.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setFRAMEWORK(Empty value) {
        this.framework = value;
    }

    /**
     * Obtient la valeur de la propriété contractcoveredgpa.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getCONTRACTCOVEREDGPA() {
        return contractcoveredgpa;
    }

    /**
     * Définit la valeur de la propriété contractcoveredgpa.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setCONTRACTCOVEREDGPA(Empty value) {
        this.contractcoveredgpa = value;
    }

    /**
     * Obtient la valeur de la propriété nocontractcoveredgpa.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getNOCONTRACTCOVEREDGPA() {
        return nocontractcoveredgpa;
    }

    /**
     * Définit la valeur de la propriété nocontractcoveredgpa.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setNOCONTRACTCOVEREDGPA(Empty value) {
        this.nocontractcoveredgpa = value;
    }

    /**
     * Obtient la valeur de la propriété noticenumberoj.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNOTICENUMBEROJ() {
        return noticenumberoj;
    }

    /**
     * Définit la valeur de la propriété noticenumberoj.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNOTICENUMBEROJ(String value) {
        this.noticenumberoj = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;choice&gt;
     *         &lt;element name="PT_NEGOTIATED_WITHOUT_PUBLICATION"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d3_part1"/&gt;
     *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="PT_AWARD_CONTRACT_WITHOUT_CALL"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d3_part2"/&gt;
     *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/choice&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "ptnegotiatedwithoutpublication",
            "ptawardcontractwithoutcall"
    })
    @ToString
    @EqualsAndHashCode
    public static class DIRECTIVE200981EC {

        @XmlElement(name = "PT_NEGOTIATED_WITHOUT_PUBLICATION")
        protected ProcedureF15.DIRECTIVE200981EC.PTNEGOTIATEDWITHOUTPUBLICATION ptnegotiatedwithoutpublication;
        @XmlElement(name = "PT_AWARD_CONTRACT_WITHOUT_CALL")
        protected ProcedureF15.DIRECTIVE200981EC.PTAWARDCONTRACTWITHOUTCALL ptawardcontractwithoutcall;

        /**
         * Obtient la valeur de la propriété ptnegotiatedwithoutpublication.
         *
         * @return possible object is
         * {@link ProcedureF15 .DIRECTIVE200981EC.PTNEGOTIATEDWITHOUTPUBLICATION }
         */
        public ProcedureF15.DIRECTIVE200981EC.PTNEGOTIATEDWITHOUTPUBLICATION getPTNEGOTIATEDWITHOUTPUBLICATION() {
            return ptnegotiatedwithoutpublication;
        }

        /**
         * Définit la valeur de la propriété ptnegotiatedwithoutpublication.
         *
         * @param value allowed object is
         *              {@link ProcedureF15 .DIRECTIVE200981EC.PTNEGOTIATEDWITHOUTPUBLICATION }
         */
        public void setPTNEGOTIATEDWITHOUTPUBLICATION(ProcedureF15.DIRECTIVE200981EC.PTNEGOTIATEDWITHOUTPUBLICATION value) {
            this.ptnegotiatedwithoutpublication = value;
        }

        /**
         * Obtient la valeur de la propriété ptawardcontractwithoutcall.
         *
         * @return possible object is
         * {@link ProcedureF15 .DIRECTIVE200981EC.PTAWARDCONTRACTWITHOUTCALL }
         */
        public ProcedureF15.DIRECTIVE200981EC.PTAWARDCONTRACTWITHOUTCALL getPTAWARDCONTRACTWITHOUTCALL() {
            return ptawardcontractwithoutcall;
        }

        /**
         * Définit la valeur de la propriété ptawardcontractwithoutcall.
         *
         * @param value allowed object is
         *              {@link ProcedureF15 .DIRECTIVE200981EC.PTAWARDCONTRACTWITHOUTCALL }
         */
        public void setPTAWARDCONTRACTWITHOUTCALL(ProcedureF15.DIRECTIVE200981EC.PTAWARDCONTRACTWITHOUTCALL value) {
            this.ptawardcontractwithoutcall = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d3_part2"/&gt;
         *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "dserviceslisted",
                "doutsidescope",
                "djustification"
        })
        @ToString
        @EqualsAndHashCode
        public static class PTAWARDCONTRACTWITHOUTCALL {

            @XmlElement(name = "D_SERVICES_LISTED")
            protected Empty dserviceslisted;
            @XmlElement(name = "D_OUTSIDE_SCOPE")
            protected Empty doutsidescope;
            @XmlElement(name = "D_JUSTIFICATION", required = true)
            protected TextFtMultiLines djustification;

            /**
             * Obtient la valeur de la propriété dserviceslisted.
             *
             * @return possible object is
             * {@link Empty }
             */
            public Empty getDSERVICESLISTED() {
                return dserviceslisted;
            }

            /**
             * Définit la valeur de la propriété dserviceslisted.
             *
             * @param value allowed object is
             *              {@link Empty }
             */
            public void setDSERVICESLISTED(Empty value) {
                this.dserviceslisted = value;
            }

            /**
             * Obtient la valeur de la propriété doutsidescope.
             *
             * @return possible object is
             * {@link Empty }
             */
            public Empty getDOUTSIDESCOPE() {
                return doutsidescope;
            }

            /**
             * Définit la valeur de la propriété doutsidescope.
             *
             * @param value allowed object is
             *              {@link Empty }
             */
            public void setDOUTSIDESCOPE(Empty value) {
                this.doutsidescope = value;
            }

            /**
             * Obtient la valeur de la propriété djustification.
             *
             * @return possible object is
             * {@link TextFtMultiLines }
             */
            public TextFtMultiLines getDJUSTIFICATION() {
                return djustification;
            }

            /**
             * Définit la valeur de la propriété djustification.
             *
             * @param value allowed object is
             *              {@link TextFtMultiLines }
             */
            public void setDJUSTIFICATION(TextFtMultiLines value) {
                this.djustification = value;
            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d3_part1"/&gt;
         *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "daccordancearticle",
                "djustification"
        })
        @ToString
        @EqualsAndHashCode
        public static class PTNEGOTIATEDWITHOUTPUBLICATION {

            @XmlElement(name = "D_ACCORDANCE_ARTICLE", required = true)
            protected eu.europa.publications.resource.schema.ted.r2_0_9.reception.AnnexD3.DACCORDANCEARTICLE daccordancearticle;
            @XmlElement(name = "D_JUSTIFICATION", required = true)
            protected TextFtMultiLines djustification;

            /**
             * Obtient la valeur de la propriété daccordancearticle.
             *
             * @return possible object is
             * {@link eu.europa.publications.resource.schema.ted.r2_0_9.reception.AnnexD3 .DACCORDANCEARTICLE }
             */
            public eu.europa.publications.resource.schema.ted.r2_0_9.reception.AnnexD3.DACCORDANCEARTICLE getDACCORDANCEARTICLE() {
                return daccordancearticle;
            }

            /**
             * Définit la valeur de la propriété daccordancearticle.
             *
             * @param value allowed object is
             *              {@link eu.europa.publications.resource.schema.ted.r2_0_9.reception.AnnexD3 .DACCORDANCEARTICLE }
             */
            public void setDACCORDANCEARTICLE(eu.europa.publications.resource.schema.ted.r2_0_9.reception.AnnexD3.DACCORDANCEARTICLE value) {
                this.daccordancearticle = value;
            }

            /**
             * Obtient la valeur de la propriété djustification.
             *
             * @return possible object is
             * {@link TextFtMultiLines }
             */
            public TextFtMultiLines getDJUSTIFICATION() {
                return djustification;
            }

            /**
             * Définit la valeur de la propriété djustification.
             *
             * @param value allowed object is
             *              {@link TextFtMultiLines }
             */
            public void setDJUSTIFICATION(TextFtMultiLines value) {
                this.djustification = value;
            }

        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;choice&gt;
     *         &lt;element name="PT_AWARD_CONTRACT_WITHOUT_PUBLICATION"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d4_part1"/&gt;
     *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="PT_AWARD_CONTRACT_WITHOUT_CALL"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_part2"/&gt;
     *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/choice&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "ptawardcontractwithoutpublication",
            "ptawardcontractwithoutcall"
    })
    @ToString
    @EqualsAndHashCode
    public static class DIRECTIVE201423EU {

        @XmlElement(name = "PT_AWARD_CONTRACT_WITHOUT_PUBLICATION")
        protected ProcedureF15.DIRECTIVE201423EU.PTAWARDCONTRACTWITHOUTPUBLICATION ptawardcontractwithoutpublication;
        @XmlElement(name = "PT_AWARD_CONTRACT_WITHOUT_CALL")
        protected ProcedureF15.DIRECTIVE201423EU.PTAWARDCONTRACTWITHOUTCALL ptawardcontractwithoutcall;

        /**
         * Obtient la valeur de la propriété ptawardcontractwithoutpublication.
         *
         * @return possible object is
         * {@link ProcedureF15 .DIRECTIVE201423EU.PTAWARDCONTRACTWITHOUTPUBLICATION }
         */
        public ProcedureF15.DIRECTIVE201423EU.PTAWARDCONTRACTWITHOUTPUBLICATION getPTAWARDCONTRACTWITHOUTPUBLICATION() {
            return ptawardcontractwithoutpublication;
        }

        /**
         * Définit la valeur de la propriété ptawardcontractwithoutpublication.
         *
         * @param value allowed object is
         *              {@link ProcedureF15 .DIRECTIVE201423EU.PTAWARDCONTRACTWITHOUTPUBLICATION }
         */
        public void setPTAWARDCONTRACTWITHOUTPUBLICATION(ProcedureF15.DIRECTIVE201423EU.PTAWARDCONTRACTWITHOUTPUBLICATION value) {
            this.ptawardcontractwithoutpublication = value;
        }

        /**
         * Obtient la valeur de la propriété ptawardcontractwithoutcall.
         *
         * @return possible object is
         * {@link ProcedureF15 .DIRECTIVE201423EU.PTAWARDCONTRACTWITHOUTCALL }
         */
        public ProcedureF15.DIRECTIVE201423EU.PTAWARDCONTRACTWITHOUTCALL getPTAWARDCONTRACTWITHOUTCALL() {
            return ptawardcontractwithoutcall;
        }

        /**
         * Définit la valeur de la propriété ptawardcontractwithoutcall.
         *
         * @param value allowed object is
         *              {@link ProcedureF15 .DIRECTIVE201423EU.PTAWARDCONTRACTWITHOUTCALL }
         */
        public void setPTAWARDCONTRACTWITHOUTCALL(ProcedureF15.DIRECTIVE201423EU.PTAWARDCONTRACTWITHOUTCALL value) {
            this.ptawardcontractwithoutcall = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_part2"/&gt;
         *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "doutsidescope",
                "djustification"
        })
        @ToString
        @EqualsAndHashCode
        public static class PTAWARDCONTRACTWITHOUTCALL {

            @XmlElement(name = "D_OUTSIDE_SCOPE", required = true)
            protected Empty doutsidescope;
            @XmlElement(name = "D_JUSTIFICATION", required = true)
            protected TextFtMultiLines djustification;

            /**
             * Other justification for the award of the award of the contract without prior
             * publication
             *
             * @return possible object is
             * {@link Empty }
             */
            public Empty getDOUTSIDESCOPE() {
                return doutsidescope;
            }

            /**
             * Définit la valeur de la propriété doutsidescope.
             *
             * @param value allowed object is
             *              {@link Empty }
             */
            public void setDOUTSIDESCOPE(Empty value) {
                this.doutsidescope = value;
            }

            /**
             * Obtient la valeur de la propriété djustification.
             *
             * @return possible object is
             * {@link TextFtMultiLines }
             */
            public TextFtMultiLines getDJUSTIFICATION() {
                return djustification;
            }

            /**
             * Définit la valeur de la propriété djustification.
             *
             * @param value allowed object is
             *              {@link TextFtMultiLines }
             */
            public void setDJUSTIFICATION(TextFtMultiLines value) {
                this.djustification = value;
            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d4_part1"/&gt;
         *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "daccordancearticle",
                "djustification"
        })
        @ToString
        @EqualsAndHashCode
        public static class PTAWARDCONTRACTWITHOUTPUBLICATION {

            @XmlElement(name = "D_ACCORDANCE_ARTICLE", required = true)
            protected eu.europa.publications.resource.schema.ted.r2_0_9.reception.AnnexD4.DACCORDANCEARTICLE daccordancearticle;
            @XmlElement(name = "D_JUSTIFICATION", required = true)
            protected TextFtMultiLines djustification;

            /**
             * Obtient la valeur de la propriété daccordancearticle.
             *
             * @return possible object is
             * {@link eu.europa.publications.resource.schema.ted.r2_0_9.reception.AnnexD4 .DACCORDANCEARTICLE }
             */
            public eu.europa.publications.resource.schema.ted.r2_0_9.reception.AnnexD4.DACCORDANCEARTICLE getDACCORDANCEARTICLE() {
                return daccordancearticle;
            }

            /**
             * Définit la valeur de la propriété daccordancearticle.
             *
             * @param value allowed object is
             *              {@link eu.europa.publications.resource.schema.ted.r2_0_9.reception.AnnexD4 .DACCORDANCEARTICLE }
             */
            public void setDACCORDANCEARTICLE(eu.europa.publications.resource.schema.ted.r2_0_9.reception.AnnexD4.DACCORDANCEARTICLE value) {
                this.daccordancearticle = value;
            }

            /**
             * Obtient la valeur de la propriété djustification.
             *
             * @return possible object is
             * {@link TextFtMultiLines }
             */
            public TextFtMultiLines getDJUSTIFICATION() {
                return djustification;
            }

            /**
             * Définit la valeur de la propriété djustification.
             *
             * @param value allowed object is
             *              {@link TextFtMultiLines }
             */
            public void setDJUSTIFICATION(TextFtMultiLines value) {
                this.djustification = value;
            }

        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;choice&gt;
     *         &lt;element name="PT_NEGOTIATED_WITHOUT_PUBLICATION"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d1_part1"/&gt;
     *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="PT_AWARD_CONTRACT_WITHOUT_CALL"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_part2"/&gt;
     *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/choice&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "ptnegotiatedwithoutpublication",
            "ptawardcontractwithoutcall"
    })
    @ToString
    @EqualsAndHashCode
    public static class DIRECTIVE201424EU {

        @XmlElement(name = "PT_NEGOTIATED_WITHOUT_PUBLICATION")
        protected ProcedureF15.DIRECTIVE201424EU.PTNEGOTIATEDWITHOUTPUBLICATION ptnegotiatedwithoutpublication;
        @XmlElement(name = "PT_AWARD_CONTRACT_WITHOUT_CALL")
        protected ProcedureF15.DIRECTIVE201424EU.PTAWARDCONTRACTWITHOUTCALL ptawardcontractwithoutcall;

        /**
         * Obtient la valeur de la propriété ptnegotiatedwithoutpublication.
         *
         * @return possible object is
         * {@link ProcedureF15 .DIRECTIVE201424EU.PTNEGOTIATEDWITHOUTPUBLICATION }
         */
        public ProcedureF15.DIRECTIVE201424EU.PTNEGOTIATEDWITHOUTPUBLICATION getPTNEGOTIATEDWITHOUTPUBLICATION() {
            return ptnegotiatedwithoutpublication;
        }

        /**
         * Définit la valeur de la propriété ptnegotiatedwithoutpublication.
         *
         * @param value allowed object is
         *              {@link ProcedureF15 .DIRECTIVE201424EU.PTNEGOTIATEDWITHOUTPUBLICATION }
         */
        public void setPTNEGOTIATEDWITHOUTPUBLICATION(ProcedureF15.DIRECTIVE201424EU.PTNEGOTIATEDWITHOUTPUBLICATION value) {
            this.ptnegotiatedwithoutpublication = value;
        }

        /**
         * Obtient la valeur de la propriété ptawardcontractwithoutcall.
         *
         * @return possible object is
         * {@link ProcedureF15 .DIRECTIVE201424EU.PTAWARDCONTRACTWITHOUTCALL }
         */
        public ProcedureF15.DIRECTIVE201424EU.PTAWARDCONTRACTWITHOUTCALL getPTAWARDCONTRACTWITHOUTCALL() {
            return ptawardcontractwithoutcall;
        }

        /**
         * Définit la valeur de la propriété ptawardcontractwithoutcall.
         *
         * @param value allowed object is
         *              {@link ProcedureF15 .DIRECTIVE201424EU.PTAWARDCONTRACTWITHOUTCALL }
         */
        public void setPTAWARDCONTRACTWITHOUTCALL(ProcedureF15.DIRECTIVE201424EU.PTAWARDCONTRACTWITHOUTCALL value) {
            this.ptawardcontractwithoutcall = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_part2"/&gt;
         *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "doutsidescope",
                "djustification"
        })
        @ToString
        @EqualsAndHashCode
        public static class PTAWARDCONTRACTWITHOUTCALL {

            @XmlElement(name = "D_OUTSIDE_SCOPE", required = true)
            protected Empty doutsidescope;
            @XmlElement(name = "D_JUSTIFICATION", required = true)
            protected TextFtMultiLines djustification;

            /**
             * Other justification for the award of the award of the contract without prior
             * publication
             *
             * @return possible object is
             * {@link Empty }
             */
            public Empty getDOUTSIDESCOPE() {
                return doutsidescope;
            }

            /**
             * Définit la valeur de la propriété doutsidescope.
             *
             * @param value allowed object is
             *              {@link Empty }
             */
            public void setDOUTSIDESCOPE(Empty value) {
                this.doutsidescope = value;
            }

            /**
             * Obtient la valeur de la propriété djustification.
             *
             * @return possible object is
             * {@link TextFtMultiLines }
             */
            public TextFtMultiLines getDJUSTIFICATION() {
                return djustification;
            }

            /**
             * Définit la valeur de la propriété djustification.
             *
             * @param value allowed object is
             *              {@link TextFtMultiLines }
             */
            public void setDJUSTIFICATION(TextFtMultiLines value) {
                this.djustification = value;
            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d1_part1"/&gt;
         *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "daccordancearticle",
                "djustification"
        })
        @ToString
        @EqualsAndHashCode
        public static class PTNEGOTIATEDWITHOUTPUBLICATION {

            @XmlElement(name = "D_ACCORDANCE_ARTICLE", required = true)
            protected eu.europa.publications.resource.schema.ted.r2_0_9.reception.AnnexD1.DACCORDANCEARTICLE daccordancearticle;
            @XmlElement(name = "D_JUSTIFICATION", required = true)
            protected TextFtMultiLines djustification;

            /**
             * Obtient la valeur de la propriété daccordancearticle.
             *
             * @return possible object is
             * {@link eu.europa.publications.resource.schema.ted.r2_0_9.reception.AnnexD1 .DACCORDANCEARTICLE }
             */
            public eu.europa.publications.resource.schema.ted.r2_0_9.reception.AnnexD1.DACCORDANCEARTICLE getDACCORDANCEARTICLE() {
                return daccordancearticle;
            }

            /**
             * Définit la valeur de la propriété daccordancearticle.
             *
             * @param value allowed object is
             *              {@link eu.europa.publications.resource.schema.ted.r2_0_9.reception.AnnexD1 .DACCORDANCEARTICLE }
             */
            public void setDACCORDANCEARTICLE(eu.europa.publications.resource.schema.ted.r2_0_9.reception.AnnexD1.DACCORDANCEARTICLE value) {
                this.daccordancearticle = value;
            }

            /**
             * Obtient la valeur de la propriété djustification.
             *
             * @return possible object is
             * {@link TextFtMultiLines }
             */
            public TextFtMultiLines getDJUSTIFICATION() {
                return djustification;
            }

            /**
             * Définit la valeur de la propriété djustification.
             *
             * @param value allowed object is
             *              {@link TextFtMultiLines }
             */
            public void setDJUSTIFICATION(TextFtMultiLines value) {
                this.djustification = value;
            }

        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;choice&gt;
     *         &lt;element name="PT_NEGOTIATED_WITHOUT_PUBLICATION"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d2_part1"/&gt;
     *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="PT_AWARD_CONTRACT_WITHOUT_CALL"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_part2"/&gt;
     *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/choice&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "ptnegotiatedwithoutpublication",
            "ptawardcontractwithoutcall"
    })
    @ToString
    @EqualsAndHashCode
    public static class DIRECTIVE201425EU {

        @XmlElement(name = "PT_NEGOTIATED_WITHOUT_PUBLICATION")
        protected ProcedureF15.DIRECTIVE201425EU.PTNEGOTIATEDWITHOUTPUBLICATION ptnegotiatedwithoutpublication;
        @XmlElement(name = "PT_AWARD_CONTRACT_WITHOUT_CALL")
        protected ProcedureF15.DIRECTIVE201425EU.PTAWARDCONTRACTWITHOUTCALL ptawardcontractwithoutcall;

        /**
         * Obtient la valeur de la propriété ptnegotiatedwithoutpublication.
         *
         * @return possible object is
         * {@link ProcedureF15 .DIRECTIVE201425EU.PTNEGOTIATEDWITHOUTPUBLICATION }
         */
        public ProcedureF15.DIRECTIVE201425EU.PTNEGOTIATEDWITHOUTPUBLICATION getPTNEGOTIATEDWITHOUTPUBLICATION() {
            return ptnegotiatedwithoutpublication;
        }

        /**
         * Définit la valeur de la propriété ptnegotiatedwithoutpublication.
         *
         * @param value allowed object is
         *              {@link ProcedureF15 .DIRECTIVE201425EU.PTNEGOTIATEDWITHOUTPUBLICATION }
         */
        public void setPTNEGOTIATEDWITHOUTPUBLICATION(ProcedureF15.DIRECTIVE201425EU.PTNEGOTIATEDWITHOUTPUBLICATION value) {
            this.ptnegotiatedwithoutpublication = value;
        }

        /**
         * Obtient la valeur de la propriété ptawardcontractwithoutcall.
         *
         * @return possible object is
         * {@link ProcedureF15 .DIRECTIVE201425EU.PTAWARDCONTRACTWITHOUTCALL }
         */
        public ProcedureF15.DIRECTIVE201425EU.PTAWARDCONTRACTWITHOUTCALL getPTAWARDCONTRACTWITHOUTCALL() {
            return ptawardcontractwithoutcall;
        }

        /**
         * Définit la valeur de la propriété ptawardcontractwithoutcall.
         *
         * @param value allowed object is
         *              {@link ProcedureF15 .DIRECTIVE201425EU.PTAWARDCONTRACTWITHOUTCALL }
         */
        public void setPTAWARDCONTRACTWITHOUTCALL(ProcedureF15.DIRECTIVE201425EU.PTAWARDCONTRACTWITHOUTCALL value) {
            this.ptawardcontractwithoutcall = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_part2"/&gt;
         *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "doutsidescope",
                "djustification"
        })
        @ToString
        @EqualsAndHashCode
        public static class PTAWARDCONTRACTWITHOUTCALL {

            @XmlElement(name = "D_OUTSIDE_SCOPE", required = true)
            protected Empty doutsidescope;
            @XmlElement(name = "D_JUSTIFICATION", required = true)
            protected TextFtMultiLines djustification;

            /**
             * Other justification for the award of the award of the contract without prior
             * publication
             *
             * @return possible object is
             * {@link Empty }
             */
            public Empty getDOUTSIDESCOPE() {
                return doutsidescope;
            }

            /**
             * Définit la valeur de la propriété doutsidescope.
             *
             * @param value allowed object is
             *              {@link Empty }
             */
            public void setDOUTSIDESCOPE(Empty value) {
                this.doutsidescope = value;
            }

            /**
             * Obtient la valeur de la propriété djustification.
             *
             * @return possible object is
             * {@link TextFtMultiLines }
             */
            public TextFtMultiLines getDJUSTIFICATION() {
                return djustification;
            }

            /**
             * Définit la valeur de la propriété djustification.
             *
             * @param value allowed object is
             *              {@link TextFtMultiLines }
             */
            public void setDJUSTIFICATION(TextFtMultiLines value) {
                this.djustification = value;
            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d2_part1"/&gt;
         *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "daccordancearticle",
                "djustification"
        })
        @ToString
        @EqualsAndHashCode
        public static class PTNEGOTIATEDWITHOUTPUBLICATION {

            @XmlElement(name = "D_ACCORDANCE_ARTICLE", required = true)
            protected eu.europa.publications.resource.schema.ted.r2_0_9.reception.AnnexD2.DACCORDANCEARTICLE daccordancearticle;
            @XmlElement(name = "D_JUSTIFICATION", required = true)
            protected TextFtMultiLines djustification;

            /**
             * Obtient la valeur de la propriété daccordancearticle.
             *
             * @return possible object is
             * {@link eu.europa.publications.resource.schema.ted.r2_0_9.reception.AnnexD2 .DACCORDANCEARTICLE }
             */
            public eu.europa.publications.resource.schema.ted.r2_0_9.reception.AnnexD2.DACCORDANCEARTICLE getDACCORDANCEARTICLE() {
                return daccordancearticle;
            }

            /**
             * Définit la valeur de la propriété daccordancearticle.
             *
             * @param value allowed object is
             *              {@link eu.europa.publications.resource.schema.ted.r2_0_9.reception.AnnexD2 .DACCORDANCEARTICLE }
             */
            public void setDACCORDANCEARTICLE(eu.europa.publications.resource.schema.ted.r2_0_9.reception.AnnexD2.DACCORDANCEARTICLE value) {
                this.daccordancearticle = value;
            }

            /**
             * Obtient la valeur de la propriété djustification.
             *
             * @return possible object is
             * {@link TextFtMultiLines }
             */
            public TextFtMultiLines getDJUSTIFICATION() {
                return djustification;
            }

            /**
             * Définit la valeur de la propriété djustification.
             *
             * @param value allowed object is
             *              {@link TextFtMultiLines }
             */
            public void setDJUSTIFICATION(TextFtMultiLines value) {
                this.djustification = value;
            }

        }

    }

}
