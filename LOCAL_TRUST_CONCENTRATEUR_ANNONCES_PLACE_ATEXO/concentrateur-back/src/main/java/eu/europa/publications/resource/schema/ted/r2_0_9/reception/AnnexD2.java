//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour annex_d2 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="annex_d2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d2_part1"/&gt;
 *           &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_part2"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "annex_d2", propOrder = {
        "daccordancearticle",
        "doutsidescope",
        "djustification"
})
@ToString
@EqualsAndHashCode
public class AnnexD2 {

    @XmlElement(name = "D_ACCORDANCE_ARTICLE")
    protected AnnexD2.DACCORDANCEARTICLE daccordancearticle;
    @XmlElement(name = "D_OUTSIDE_SCOPE")
    protected Empty doutsidescope;
    @XmlElement(name = "D_JUSTIFICATION", required = true)
    protected TextFtMultiLines djustification;

    /**
     * Obtient la valeur de la propriété daccordancearticle.
     *
     * @return possible object is
     * {@link AnnexD2 .DACCORDANCEARTICLE }
     */
    public AnnexD2.DACCORDANCEARTICLE getDACCORDANCEARTICLE() {
        return daccordancearticle;
    }

    /**
     * Définit la valeur de la propriété daccordancearticle.
     *
     * @param value allowed object is
     *              {@link AnnexD2 .DACCORDANCEARTICLE }
     */
    public void setDACCORDANCEARTICLE(AnnexD2.DACCORDANCEARTICLE value) {
        this.daccordancearticle = value;
    }

    /**
     * Other justification for the award of the award of the contract without prior
     * publication
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getDOUTSIDESCOPE() {
        return doutsidescope;
    }

    /**
     * Définit la valeur de la propriété doutsidescope.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setDOUTSIDESCOPE(Empty value) {
        this.doutsidescope = value;
    }

    /**
     * Obtient la valeur de la propriété djustification.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getDJUSTIFICATION() {
        return djustification;
    }

    /**
     * Définit la valeur de la propriété djustification.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setDJUSTIFICATION(TextFtMultiLines value) {
        this.djustification = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_NO_TENDERS_REQUESTS" minOccurs="0"/&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_PURE_RESEARCH" minOccurs="0"/&gt;
     *         &lt;choice minOccurs="0"&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_TECHNICAL"/&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_ARTISTIC"/&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_PROTECT_RIGHTS"/&gt;
     *         &lt;/choice&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_EXTREME_URGENCY" minOccurs="0"/&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_ADD_DELIVERIES_ORDERED" minOccurs="0"/&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_REPETITION_EXISTING" minOccurs="0"/&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_CONTRACT_AWARDED_DESIGN_CONTEST" minOccurs="0"/&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_COMMODITY_MARKET" minOccurs="0"/&gt;
     *         &lt;choice minOccurs="0"&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_FROM_WINDING_PROVIDER"/&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_FROM_LIQUIDATOR_CREDITOR"/&gt;
     *         &lt;/choice&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_BARGAIN_PURCHASE" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "dnotendersrequests",
            "dpureresearch",
            "dtechnical",
            "dartistic",
            "dprotectrights",
            "dextremeurgency",
            "dadddeliveriesordered",
            "drepetitionexisting",
            "dcontractawardeddesigncontest",
            "dcommoditymarket",
            "dfromwindingprovider",
            "dfromliquidatorcreditor",
            "dbargainpurchase"
    })
    @ToString
    @EqualsAndHashCode
    public static class DACCORDANCEARTICLE {

        @XmlElement(name = "D_NO_TENDERS_REQUESTS")
        protected Empty dnotendersrequests;
        @XmlElement(name = "D_PURE_RESEARCH")
        protected Empty dpureresearch;
        @XmlElement(name = "D_TECHNICAL")
        protected Empty dtechnical;
        @XmlElement(name = "D_ARTISTIC")
        protected Empty dartistic;
        @XmlElement(name = "D_PROTECT_RIGHTS")
        protected Empty dprotectrights;
        @XmlElement(name = "D_EXTREME_URGENCY")
        protected Empty dextremeurgency;
        @XmlElement(name = "D_ADD_DELIVERIES_ORDERED")
        protected Empty dadddeliveriesordered;
        @XmlElement(name = "D_REPETITION_EXISTING")
        protected NoSupplies drepetitionexisting;
        @XmlElement(name = "D_CONTRACT_AWARDED_DESIGN_CONTEST")
        protected Services dcontractawardeddesigncontest;
        @XmlElement(name = "D_COMMODITY_MARKET")
        protected Supplies dcommoditymarket;
        @XmlElement(name = "D_FROM_WINDING_PROVIDER")
        protected NoWorks dfromwindingprovider;
        @XmlElement(name = "D_FROM_LIQUIDATOR_CREDITOR")
        protected NoWorks dfromliquidatorcreditor;
        @XmlElement(name = "D_BARGAIN_PURCHASE")
        protected Empty dbargainpurchase;

        /**
         * Obtient la valeur de la propriété dnotendersrequests.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDNOTENDERSREQUESTS() {
            return dnotendersrequests;
        }

        /**
         * Définit la valeur de la propriété dnotendersrequests.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDNOTENDERSREQUESTS(Empty value) {
            this.dnotendersrequests = value;
        }

        /**
         * Obtient la valeur de la propriété dpureresearch.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDPURERESEARCH() {
            return dpureresearch;
        }

        /**
         * Définit la valeur de la propriété dpureresearch.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDPURERESEARCH(Empty value) {
            this.dpureresearch = value;
        }

        /**
         * Obtient la valeur de la propriété dtechnical.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDTECHNICAL() {
            return dtechnical;
        }

        /**
         * Définit la valeur de la propriété dtechnical.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDTECHNICAL(Empty value) {
            this.dtechnical = value;
        }

        /**
         * Obtient la valeur de la propriété dartistic.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDARTISTIC() {
            return dartistic;
        }

        /**
         * Définit la valeur de la propriété dartistic.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDARTISTIC(Empty value) {
            this.dartistic = value;
        }

        /**
         * Obtient la valeur de la propriété dprotectrights.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDPROTECTRIGHTS() {
            return dprotectrights;
        }

        /**
         * Définit la valeur de la propriété dprotectrights.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDPROTECTRIGHTS(Empty value) {
            this.dprotectrights = value;
        }

        /**
         * Obtient la valeur de la propriété dextremeurgency.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDEXTREMEURGENCY() {
            return dextremeurgency;
        }

        /**
         * Définit la valeur de la propriété dextremeurgency.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDEXTREMEURGENCY(Empty value) {
            this.dextremeurgency = value;
        }

        /**
         * Obtient la valeur de la propriété dadddeliveriesordered.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDADDDELIVERIESORDERED() {
            return dadddeliveriesordered;
        }

        /**
         * Définit la valeur de la propriété dadddeliveriesordered.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDADDDELIVERIESORDERED(Empty value) {
            this.dadddeliveriesordered = value;
        }

        /**
         * Obtient la valeur de la propriété drepetitionexisting.
         *
         * @return possible object is
         * {@link NoSupplies }
         */
        public NoSupplies getDREPETITIONEXISTING() {
            return drepetitionexisting;
        }

        /**
         * Définit la valeur de la propriété drepetitionexisting.
         *
         * @param value allowed object is
         *              {@link NoSupplies }
         */
        public void setDREPETITIONEXISTING(NoSupplies value) {
            this.drepetitionexisting = value;
        }

        /**
         * Obtient la valeur de la propriété dcontractawardeddesigncontest.
         *
         * @return possible object is
         * {@link Services }
         */
        public Services getDCONTRACTAWARDEDDESIGNCONTEST() {
            return dcontractawardeddesigncontest;
        }

        /**
         * Définit la valeur de la propriété dcontractawardeddesigncontest.
         *
         * @param value allowed object is
         *              {@link Services }
         */
        public void setDCONTRACTAWARDEDDESIGNCONTEST(Services value) {
            this.dcontractawardeddesigncontest = value;
        }

        /**
         * Obtient la valeur de la propriété dcommoditymarket.
         *
         * @return possible object is
         * {@link Supplies }
         */
        public Supplies getDCOMMODITYMARKET() {
            return dcommoditymarket;
        }

        /**
         * Définit la valeur de la propriété dcommoditymarket.
         *
         * @param value allowed object is
         *              {@link Supplies }
         */
        public void setDCOMMODITYMARKET(Supplies value) {
            this.dcommoditymarket = value;
        }

        /**
         * Obtient la valeur de la propriété dfromwindingprovider.
         *
         * @return possible object is
         * {@link NoWorks }
         */
        public NoWorks getDFROMWINDINGPROVIDER() {
            return dfromwindingprovider;
        }

        /**
         * Définit la valeur de la propriété dfromwindingprovider.
         *
         * @param value allowed object is
         *              {@link NoWorks }
         */
        public void setDFROMWINDINGPROVIDER(NoWorks value) {
            this.dfromwindingprovider = value;
        }

        /**
         * Obtient la valeur de la propriété dfromliquidatorcreditor.
         *
         * @return possible object is
         * {@link NoWorks }
         */
        public NoWorks getDFROMLIQUIDATORCREDITOR() {
            return dfromliquidatorcreditor;
        }

        /**
         * Définit la valeur de la propriété dfromliquidatorcreditor.
         *
         * @param value allowed object is
         *              {@link NoWorks }
         */
        public void setDFROMLIQUIDATORCREDITOR(NoWorks value) {
            this.dfromliquidatorcreditor = value;
        }

        /**
         * Obtient la valeur de la propriété dbargainpurchase.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDBARGAINPURCHASE() {
            return dbargainpurchase;
        }

        /**
         * Définit la valeur de la propriété dbargainpurchase.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDBARGAINPURCHASE(Empty value) {
            this.dbargainpurchase = value;
        }

    }

}
