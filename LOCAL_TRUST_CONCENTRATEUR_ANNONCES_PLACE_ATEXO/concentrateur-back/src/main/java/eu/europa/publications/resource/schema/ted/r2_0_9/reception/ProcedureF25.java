//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Section IV: PROCEDURE
 *
 * <p>Classe Java pour procedure_f25 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="procedure_f25"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_AWARD_CONTRACT_WITH_PRIOR_PUBLICATION"/&gt;
 *           &lt;element name="PT_AWARD_CONTRACT_WITHOUT_PUBLICATION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d4"/&gt;
 *         &lt;/choice&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="CONTRACT_COVERED_GPA" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}works"/&gt;
 *           &lt;element name="NO_CONTRACT_COVERED_GPA" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}works"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}MAIN_FEATURES_AWARD" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NOTICE_NUMBER_OJ" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "procedure_f25", propOrder = {
        "ptawardcontractwithpriorpublication",
        "ptawardcontractwithoutpublication",
        "contractcoveredgpa",
        "nocontractcoveredgpa",
        "mainfeaturesaward",
        "noticenumberoj"
})
@ToString
@EqualsAndHashCode
public class ProcedureF25 {

    @XmlElement(name = "PT_AWARD_CONTRACT_WITH_PRIOR_PUBLICATION")
    protected Empty ptawardcontractwithpriorpublication;
    @XmlElement(name = "PT_AWARD_CONTRACT_WITHOUT_PUBLICATION")
    protected AnnexD4 ptawardcontractwithoutpublication;
    @XmlElement(name = "CONTRACT_COVERED_GPA")
    protected Works contractcoveredgpa;
    @XmlElement(name = "NO_CONTRACT_COVERED_GPA")
    protected Works nocontractcoveredgpa;
    @XmlElement(name = "MAIN_FEATURES_AWARD")
    protected TextFtMultiLines mainfeaturesaward;
    @XmlElement(name = "NOTICE_NUMBER_OJ")
    protected String noticenumberoj;

    /**
     * Obtient la valeur de la propriété ptawardcontractwithpriorpublication.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTAWARDCONTRACTWITHPRIORPUBLICATION() {
        return ptawardcontractwithpriorpublication;
    }

    /**
     * Définit la valeur de la propriété ptawardcontractwithpriorpublication.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTAWARDCONTRACTWITHPRIORPUBLICATION(Empty value) {
        this.ptawardcontractwithpriorpublication = value;
    }

    /**
     * Obtient la valeur de la propriété ptawardcontractwithoutpublication.
     *
     * @return possible object is
     * {@link AnnexD4 }
     */
    public AnnexD4 getPTAWARDCONTRACTWITHOUTPUBLICATION() {
        return ptawardcontractwithoutpublication;
    }

    /**
     * Définit la valeur de la propriété ptawardcontractwithoutpublication.
     *
     * @param value allowed object is
     *              {@link AnnexD4 }
     */
    public void setPTAWARDCONTRACTWITHOUTPUBLICATION(AnnexD4 value) {
        this.ptawardcontractwithoutpublication = value;
    }

    /**
     * Obtient la valeur de la propriété contractcoveredgpa.
     *
     * @return possible object is
     * {@link Works }
     */
    public Works getCONTRACTCOVEREDGPA() {
        return contractcoveredgpa;
    }

    /**
     * Définit la valeur de la propriété contractcoveredgpa.
     *
     * @param value allowed object is
     *              {@link Works }
     */
    public void setCONTRACTCOVEREDGPA(Works value) {
        this.contractcoveredgpa = value;
    }

    /**
     * Obtient la valeur de la propriété nocontractcoveredgpa.
     *
     * @return possible object is
     * {@link Works }
     */
    public Works getNOCONTRACTCOVEREDGPA() {
        return nocontractcoveredgpa;
    }

    /**
     * Définit la valeur de la propriété nocontractcoveredgpa.
     *
     * @param value allowed object is
     *              {@link Works }
     */
    public void setNOCONTRACTCOVEREDGPA(Works value) {
        this.nocontractcoveredgpa = value;
    }

    /**
     * Obtient la valeur de la propriété mainfeaturesaward.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getMAINFEATURESAWARD() {
        return mainfeaturesaward;
    }

    /**
     * Définit la valeur de la propriété mainfeaturesaward.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setMAINFEATURESAWARD(TextFtMultiLines value) {
        this.mainfeaturesaward = value;
    }

    /**
     * Obtient la valeur de la propriété noticenumberoj.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNOTICENUMBEROJ() {
        return noticenumberoj;
    }

    /**
     * Définit la valeur de la propriété noticenumberoj.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNOTICENUMBEROJ(String value) {
        this.noticenumberoj = value;
    }

}
