//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java pour anonymous complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LANGUAGE" maxOccurs="50"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="VALUE" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}t_language_list" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "language"
})
@XmlRootElement(name = "LANGUAGES")
@ToString
@EqualsAndHashCode
public class LANGUAGES {

    @XmlElement(name = "LANGUAGE", required = true)
    protected List<LANGUAGES.LANGUAGE> language;

    /**
     * Gets the value of the language property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the language property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLANGUAGE().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LANGUAGES.LANGUAGE }
     */
    public List<LANGUAGES.LANGUAGE> getLANGUAGE() {
        if (language == null) {
            language = new ArrayList<LANGUAGES.LANGUAGE>();
        }
        return this.language;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="VALUE" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}t_language_list" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @ToString
    @EqualsAndHashCode
    public static class LANGUAGE {

        @XmlAttribute(name = "VALUE", required = true)
        protected TLanguageList value;

        /**
         * Obtient la valeur de la propriété value.
         *
         * @return possible object is
         * {@link TLanguageList }
         */
        public TLanguageList getVALUE() {
            return value;
        }

        /**
         * Définit la valeur de la propriété value.
         *
         * @param value allowed object is
         *              {@link TLanguageList }
         */
        public void setVALUE(TLanguageList value) {
            this.value = value;
        }

    }

}
