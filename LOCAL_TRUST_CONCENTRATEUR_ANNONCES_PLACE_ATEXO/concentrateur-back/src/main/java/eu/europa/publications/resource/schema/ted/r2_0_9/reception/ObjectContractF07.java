//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Section II: OBJECT OF THE CONTRACT
 *
 * <p>Classe Java pour object_contract_f07 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="object_contract_f07"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TITLE"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}REFERENCE_NUMBER" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CPV_MAIN"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TYPE_CONTRACT"/&gt;
 *         &lt;element name="OBJECT_DESCR" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}object_f07"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "object_contract_f07", propOrder = {
        "title",
        "referencenumber",
        "cpvmain",
        "typecontract",
        "objectdescr"
})
@ToString
@EqualsAndHashCode
public class ObjectContractF07 {

    @XmlElement(name = "TITLE", required = true)
    protected TextFtSingleLine title;
    @XmlElement(name = "REFERENCE_NUMBER")
    protected String referencenumber;
    @XmlElement(name = "CPV_MAIN", required = true)
    protected CpvSet cpvmain;
    @XmlElement(name = "TYPE_CONTRACT", required = true)
    protected TypeContract typecontract;
    @XmlElement(name = "OBJECT_DESCR", required = true)
    protected ObjectF07 objectdescr;

    /**
     * Obtient la valeur de la propriété title.
     *
     * @return possible object is
     * {@link TextFtSingleLine }
     */
    public TextFtSingleLine getTITLE() {
        return title;
    }

    /**
     * Définit la valeur de la propriété title.
     *
     * @param value allowed object is
     *              {@link TextFtSingleLine }
     */
    public void setTITLE(TextFtSingleLine value) {
        this.title = value;
    }

    /**
     * Obtient la valeur de la propriété referencenumber.
     *
     * @return possible object is
     * {@link String }
     */
    public String getREFERENCENUMBER() {
        return referencenumber;
    }

    /**
     * Définit la valeur de la propriété referencenumber.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setREFERENCENUMBER(String value) {
        this.referencenumber = value;
    }

    /**
     * Obtient la valeur de la propriété cpvmain.
     *
     * @return possible object is
     * {@link CpvSet }
     */
    public CpvSet getCPVMAIN() {
        return cpvmain;
    }

    /**
     * Définit la valeur de la propriété cpvmain.
     *
     * @param value allowed object is
     *              {@link CpvSet }
     */
    public void setCPVMAIN(CpvSet value) {
        this.cpvmain = value;
    }

    /**
     * Obtient la valeur de la propriété typecontract.
     *
     * @return possible object is
     * {@link TypeContract }
     */
    public TypeContract getTYPECONTRACT() {
        return typecontract;
    }

    /**
     * Définit la valeur de la propriété typecontract.
     *
     * @param value allowed object is
     *              {@link TypeContract }
     */
    public void setTYPECONTRACT(TypeContract value) {
        this.typecontract = value;
    }

    /**
     * Obtient la valeur de la propriété objectdescr.
     *
     * @return possible object is
     * {@link ObjectF07 }
     */
    public ObjectF07 getOBJECTDESCR() {
        return objectdescr;
    }

    /**
     * Définit la valeur de la propriété objectdescr.
     *
     * @param value allowed object is
     *              {@link ObjectF07 }
     */
    public void setOBJECTDESCR(ObjectF07 value) {
        this.objectdescr = value;
    }

}
