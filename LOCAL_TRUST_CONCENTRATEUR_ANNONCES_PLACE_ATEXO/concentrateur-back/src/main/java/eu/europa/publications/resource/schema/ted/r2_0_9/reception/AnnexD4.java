//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour annex_d4 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="annex_d4"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d4_part1"/&gt;
 *           &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_part2"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "annex_d4", propOrder = {
        "daccordancearticle",
        "doutsidescope",
        "djustification"
})
@ToString
@EqualsAndHashCode
public class AnnexD4 {

    @XmlElement(name = "D_ACCORDANCE_ARTICLE")
    protected AnnexD4.DACCORDANCEARTICLE daccordancearticle;
    @XmlElement(name = "D_OUTSIDE_SCOPE")
    protected Empty doutsidescope;
    @XmlElement(name = "D_JUSTIFICATION", required = true)
    protected TextFtMultiLines djustification;

    /**
     * Obtient la valeur de la propriété daccordancearticle.
     *
     * @return possible object is
     * {@link AnnexD4 .DACCORDANCEARTICLE }
     */
    public AnnexD4.DACCORDANCEARTICLE getDACCORDANCEARTICLE() {
        return daccordancearticle;
    }

    /**
     * Définit la valeur de la propriété daccordancearticle.
     *
     * @param value allowed object is
     *              {@link AnnexD4 .DACCORDANCEARTICLE }
     */
    public void setDACCORDANCEARTICLE(AnnexD4.DACCORDANCEARTICLE value) {
        this.daccordancearticle = value;
    }

    /**
     * Other justification for the award of the award of the contract without prior
     * publication
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getDOUTSIDESCOPE() {
        return doutsidescope;
    }

    /**
     * Définit la valeur de la propriété doutsidescope.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setDOUTSIDESCOPE(Empty value) {
        this.doutsidescope = value;
    }

    /**
     * Obtient la valeur de la propriété djustification.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getDJUSTIFICATION() {
        return djustification;
    }

    /**
     * Définit la valeur de la propriété djustification.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setDJUSTIFICATION(TextFtMultiLines value) {
        this.djustification = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_NO_TENDERS_REQUESTS" minOccurs="0"/&gt;
     *         &lt;choice minOccurs="0"&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_ARTISTIC"/&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_TECHNICAL"/&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_EXCLUSIVE_RIGHT"/&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_PROTECT_RIGHTS"/&gt;
     *         &lt;/choice&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "dnotendersrequests",
            "dartistic",
            "dtechnical",
            "dexclusiveright",
            "dprotectrights"
    })
    @ToString
    @EqualsAndHashCode
    public static class DACCORDANCEARTICLE {

        @XmlElement(name = "D_NO_TENDERS_REQUESTS")
        protected Empty dnotendersrequests;
        @XmlElement(name = "D_ARTISTIC")
        protected Empty dartistic;
        @XmlElement(name = "D_TECHNICAL")
        protected Empty dtechnical;
        @XmlElement(name = "D_EXCLUSIVE_RIGHT")
        protected Empty dexclusiveright;
        @XmlElement(name = "D_PROTECT_RIGHTS")
        protected Empty dprotectrights;

        /**
         * Obtient la valeur de la propriété dnotendersrequests.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDNOTENDERSREQUESTS() {
            return dnotendersrequests;
        }

        /**
         * Définit la valeur de la propriété dnotendersrequests.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDNOTENDERSREQUESTS(Empty value) {
            this.dnotendersrequests = value;
        }

        /**
         * Obtient la valeur de la propriété dartistic.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDARTISTIC() {
            return dartistic;
        }

        /**
         * Définit la valeur de la propriété dartistic.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDARTISTIC(Empty value) {
            this.dartistic = value;
        }

        /**
         * Obtient la valeur de la propriété dtechnical.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDTECHNICAL() {
            return dtechnical;
        }

        /**
         * Définit la valeur de la propriété dtechnical.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDTECHNICAL(Empty value) {
            this.dtechnical = value;
        }

        /**
         * Obtient la valeur de la propriété dexclusiveright.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDEXCLUSIVERIGHT() {
            return dexclusiveright;
        }

        /**
         * Définit la valeur de la propriété dexclusiveright.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDEXCLUSIVERIGHT(Empty value) {
            this.dexclusiveright = value;
        }

        /**
         * Obtient la valeur de la propriété dprotectrights.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDPROTECTRIGHTS() {
            return dprotectrights;
        }

        /**
         * Définit la valeur de la propriété dprotectrights.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDPROTECTRIGHTS(Empty value) {
            this.dprotectrights = value;
        }

    }

}
