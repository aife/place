//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour annex_d3 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="annex_d3"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d3_part1"/&gt;
 *           &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d3_part2"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_JUSTIFICATION"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "annex_d3", propOrder = {
        "daccordancearticle",
        "dserviceslisted",
        "doutsidescope",
        "djustification"
})
@ToString
@EqualsAndHashCode
public class AnnexD3 {

    @XmlElement(name = "D_ACCORDANCE_ARTICLE")
    protected AnnexD3.DACCORDANCEARTICLE daccordancearticle;
    @XmlElement(name = "D_SERVICES_LISTED")
    protected Empty dserviceslisted;
    @XmlElement(name = "D_OUTSIDE_SCOPE")
    protected Empty doutsidescope;
    @XmlElement(name = "D_JUSTIFICATION", required = true)
    protected TextFtMultiLines djustification;

    /**
     * Obtient la valeur de la propriété daccordancearticle.
     *
     * @return possible object is
     * {@link AnnexD3 .DACCORDANCEARTICLE }
     */
    public AnnexD3.DACCORDANCEARTICLE getDACCORDANCEARTICLE() {
        return daccordancearticle;
    }

    /**
     * Définit la valeur de la propriété daccordancearticle.
     *
     * @param value allowed object is
     *              {@link AnnexD3 .DACCORDANCEARTICLE }
     */
    public void setDACCORDANCEARTICLE(AnnexD3.DACCORDANCEARTICLE value) {
        this.daccordancearticle = value;
    }

    /**
     * Obtient la valeur de la propriété dserviceslisted.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getDSERVICESLISTED() {
        return dserviceslisted;
    }

    /**
     * Définit la valeur de la propriété dserviceslisted.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setDSERVICESLISTED(Empty value) {
        this.dserviceslisted = value;
    }

    /**
     * Obtient la valeur de la propriété doutsidescope.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getDOUTSIDESCOPE() {
        return doutsidescope;
    }

    /**
     * Définit la valeur de la propriété doutsidescope.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setDOUTSIDESCOPE(Empty value) {
        this.doutsidescope = value;
    }

    /**
     * Obtient la valeur de la propriété djustification.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getDJUSTIFICATION() {
        return djustification;
    }

    /**
     * Définit la valeur de la propriété djustification.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setDJUSTIFICATION(TextFtMultiLines value) {
        this.djustification = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;choice minOccurs="0"&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_PROC_RESTRICTED"/&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_PROC_NEGOTIATED_PRIOR_CALL_COMPETITION"/&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_PROC_COMPETITIVE_DIALOGUE"/&gt;
     *         &lt;/choice&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_OTHER_SERVICES" minOccurs="0"/&gt;
     *         &lt;element name="D_MANUF_FOR_RESEARCH" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}no_works" minOccurs="0"/&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_ALL_TENDERS" minOccurs="0"/&gt;
     *         &lt;choice minOccurs="0"&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_TECHNICAL"/&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_PROTECT_RIGHTS"/&gt;
     *         &lt;/choice&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_PERIODS_INCOMPATIBLE" minOccurs="0"/&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_EXTREME_URGENCY" minOccurs="0"/&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_ADD_DELIVERIES_ORDERED" minOccurs="0"/&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_REPETITION_EXISTING" minOccurs="0"/&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_COMMODITY_MARKET" minOccurs="0"/&gt;
     *         &lt;choice minOccurs="0"&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_FROM_WINDING_PROVIDER"/&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_FROM_LIQUIDATOR_CREDITOR"/&gt;
     *         &lt;/choice&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}D_MARITIME_SERVICES" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "dprocrestricted",
            "dprocnegotiatedpriorcallcompetition",
            "dproccompetitivedialogue",
            "dotherservices",
            "dmanufforresearch",
            "dalltenders",
            "dtechnical",
            "dprotectrights",
            "dperiodsincompatible",
            "dextremeurgency",
            "dadddeliveriesordered",
            "drepetitionexisting",
            "dcommoditymarket",
            "dfromwindingprovider",
            "dfromliquidatorcreditor",
            "dmaritimeservices"
    })
    @ToString
    @EqualsAndHashCode
    public static class DACCORDANCEARTICLE {

        @XmlElement(name = "D_PROC_RESTRICTED")
        protected Empty dprocrestricted;
        @XmlElement(name = "D_PROC_NEGOTIATED_PRIOR_CALL_COMPETITION")
        protected Empty dprocnegotiatedpriorcallcompetition;
        @XmlElement(name = "D_PROC_COMPETITIVE_DIALOGUE")
        protected Empty dproccompetitivedialogue;
        @XmlElement(name = "D_OTHER_SERVICES")
        protected NoWorks dotherservices;
        @XmlElement(name = "D_MANUF_FOR_RESEARCH")
        protected NoWorks dmanufforresearch;
        @XmlElement(name = "D_ALL_TENDERS")
        protected Empty dalltenders;
        @XmlElement(name = "D_TECHNICAL")
        protected Empty dtechnical;
        @XmlElement(name = "D_PROTECT_RIGHTS")
        protected Empty dprotectrights;
        @XmlElement(name = "D_PERIODS_INCOMPATIBLE")
        protected Empty dperiodsincompatible;
        @XmlElement(name = "D_EXTREME_URGENCY")
        protected Empty dextremeurgency;
        @XmlElement(name = "D_ADD_DELIVERIES_ORDERED")
        protected Empty dadddeliveriesordered;
        @XmlElement(name = "D_REPETITION_EXISTING")
        protected NoSupplies drepetitionexisting;
        @XmlElement(name = "D_COMMODITY_MARKET")
        protected Supplies dcommoditymarket;
        @XmlElement(name = "D_FROM_WINDING_PROVIDER")
        protected NoWorks dfromwindingprovider;
        @XmlElement(name = "D_FROM_LIQUIDATOR_CREDITOR")
        protected NoWorks dfromliquidatorcreditor;
        @XmlElement(name = "D_MARITIME_SERVICES")
        protected Services dmaritimeservices;

        /**
         * Obtient la valeur de la propriété dprocrestricted.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDPROCRESTRICTED() {
            return dprocrestricted;
        }

        /**
         * Définit la valeur de la propriété dprocrestricted.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDPROCRESTRICTED(Empty value) {
            this.dprocrestricted = value;
        }

        /**
         * Obtient la valeur de la propriété dprocnegotiatedpriorcallcompetition.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDPROCNEGOTIATEDPRIORCALLCOMPETITION() {
            return dprocnegotiatedpriorcallcompetition;
        }

        /**
         * Définit la valeur de la propriété dprocnegotiatedpriorcallcompetition.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDPROCNEGOTIATEDPRIORCALLCOMPETITION(Empty value) {
            this.dprocnegotiatedpriorcallcompetition = value;
        }

        /**
         * Obtient la valeur de la propriété dproccompetitivedialogue.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDPROCCOMPETITIVEDIALOGUE() {
            return dproccompetitivedialogue;
        }

        /**
         * Définit la valeur de la propriété dproccompetitivedialogue.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDPROCCOMPETITIVEDIALOGUE(Empty value) {
            this.dproccompetitivedialogue = value;
        }

        /**
         * Obtient la valeur de la propriété dotherservices.
         *
         * @return possible object is
         * {@link NoWorks }
         */
        public NoWorks getDOTHERSERVICES() {
            return dotherservices;
        }

        /**
         * Définit la valeur de la propriété dotherservices.
         *
         * @param value allowed object is
         *              {@link NoWorks }
         */
        public void setDOTHERSERVICES(NoWorks value) {
            this.dotherservices = value;
        }

        /**
         * Obtient la valeur de la propriété dmanufforresearch.
         *
         * @return possible object is
         * {@link NoWorks }
         */
        public NoWorks getDMANUFFORRESEARCH() {
            return dmanufforresearch;
        }

        /**
         * Définit la valeur de la propriété dmanufforresearch.
         *
         * @param value allowed object is
         *              {@link NoWorks }
         */
        public void setDMANUFFORRESEARCH(NoWorks value) {
            this.dmanufforresearch = value;
        }

        /**
         * Obtient la valeur de la propriété dalltenders.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDALLTENDERS() {
            return dalltenders;
        }

        /**
         * Définit la valeur de la propriété dalltenders.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDALLTENDERS(Empty value) {
            this.dalltenders = value;
        }

        /**
         * Obtient la valeur de la propriété dtechnical.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDTECHNICAL() {
            return dtechnical;
        }

        /**
         * Définit la valeur de la propriété dtechnical.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDTECHNICAL(Empty value) {
            this.dtechnical = value;
        }

        /**
         * Obtient la valeur de la propriété dprotectrights.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDPROTECTRIGHTS() {
            return dprotectrights;
        }

        /**
         * Définit la valeur de la propriété dprotectrights.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDPROTECTRIGHTS(Empty value) {
            this.dprotectrights = value;
        }

        /**
         * Obtient la valeur de la propriété dperiodsincompatible.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDPERIODSINCOMPATIBLE() {
            return dperiodsincompatible;
        }

        /**
         * Définit la valeur de la propriété dperiodsincompatible.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDPERIODSINCOMPATIBLE(Empty value) {
            this.dperiodsincompatible = value;
        }

        /**
         * Obtient la valeur de la propriété dextremeurgency.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDEXTREMEURGENCY() {
            return dextremeurgency;
        }

        /**
         * Définit la valeur de la propriété dextremeurgency.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDEXTREMEURGENCY(Empty value) {
            this.dextremeurgency = value;
        }

        /**
         * Obtient la valeur de la propriété dadddeliveriesordered.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getDADDDELIVERIESORDERED() {
            return dadddeliveriesordered;
        }

        /**
         * Définit la valeur de la propriété dadddeliveriesordered.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setDADDDELIVERIESORDERED(Empty value) {
            this.dadddeliveriesordered = value;
        }

        /**
         * Obtient la valeur de la propriété drepetitionexisting.
         *
         * @return possible object is
         * {@link NoSupplies }
         */
        public NoSupplies getDREPETITIONEXISTING() {
            return drepetitionexisting;
        }

        /**
         * Définit la valeur de la propriété drepetitionexisting.
         *
         * @param value allowed object is
         *              {@link NoSupplies }
         */
        public void setDREPETITIONEXISTING(NoSupplies value) {
            this.drepetitionexisting = value;
        }

        /**
         * Obtient la valeur de la propriété dcommoditymarket.
         *
         * @return possible object is
         * {@link Supplies }
         */
        public Supplies getDCOMMODITYMARKET() {
            return dcommoditymarket;
        }

        /**
         * Définit la valeur de la propriété dcommoditymarket.
         *
         * @param value allowed object is
         *              {@link Supplies }
         */
        public void setDCOMMODITYMARKET(Supplies value) {
            this.dcommoditymarket = value;
        }

        /**
         * Obtient la valeur de la propriété dfromwindingprovider.
         *
         * @return possible object is
         * {@link NoWorks }
         */
        public NoWorks getDFROMWINDINGPROVIDER() {
            return dfromwindingprovider;
        }

        /**
         * Définit la valeur de la propriété dfromwindingprovider.
         *
         * @param value allowed object is
         *              {@link NoWorks }
         */
        public void setDFROMWINDINGPROVIDER(NoWorks value) {
            this.dfromwindingprovider = value;
        }

        /**
         * Obtient la valeur de la propriété dfromliquidatorcreditor.
         *
         * @return possible object is
         * {@link NoWorks }
         */
        public NoWorks getDFROMLIQUIDATORCREDITOR() {
            return dfromliquidatorcreditor;
        }

        /**
         * Définit la valeur de la propriété dfromliquidatorcreditor.
         *
         * @param value allowed object is
         *              {@link NoWorks }
         */
        public void setDFROMLIQUIDATORCREDITOR(NoWorks value) {
            this.dfromliquidatorcreditor = value;
        }

        /**
         * Obtient la valeur de la propriété dmaritimeservices.
         *
         * @return possible object is
         * {@link Services }
         */
        public Services getDMARITIMESERVICES() {
            return dmaritimeservices;
        }

        /**
         * Définit la valeur de la propriété dmaritimeservices.
         *
         * @param value allowed object is
         *              {@link Services }
         */
        public void setDMARITIMESERVICES(Services value) {
            this.dmaritimeservices = value;
        }

    }

}
