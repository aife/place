//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import eu.europa.publications.resource.schema.ted._2021.nuts.Nuts;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java pour object_f04 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="object_f04"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TITLE" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LOT_NO" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CPV_ADDITIONAL" maxOccurs="100" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/2016/nuts}NUTS" maxOccurs="250"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}MAIN_SITE" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SHORT_DESCR"/&gt;
 *         &lt;element name="AC" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_criteria_doc"/&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}VAL_OBJECT" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}time_frame" minOccurs="0"/&gt;
 *         &lt;sequence minOccurs="0"&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}RENEWAL"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}RENEWAL_DESCR" minOccurs="0"/&gt;
 *         &lt;/sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ACCEPTED_VARIANTS" minOccurs="0"/&gt;
 *         &lt;sequence minOccurs="0"&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}OPTIONS"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}OPTIONS_DESCR" minOccurs="0"/&gt;
 *         &lt;/sequence&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}eu_union_funds" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}INFO_ADD" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ITEM" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}nb_lot" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "object_f04", propOrder = {
        "content"
})
@ToString
@EqualsAndHashCode
public class ObjectF04 {

    @XmlElementRefs({
            @XmlElementRef(name = "TITLE", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "LOT_NO", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "CPV_ADDITIONAL", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "NUTS", namespace = "http://publications.europa.eu/resource/schema/ted/2016/nuts", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "MAIN_SITE", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "SHORT_DESCR", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "AC", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "VAL_OBJECT", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "DURATION", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "DATE_START", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "DATE_END", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "RENEWAL", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "RENEWAL_DESCR", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "ACCEPTED_VARIANTS", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "OPTIONS", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "OPTIONS_DESCR", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "EU_PROGR_RELATED", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "NO_EU_PROGR_RELATED", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "INFO_ADD", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false)
    })
    protected List<JAXBElement<?>> content;
    @XmlAttribute(name = "ITEM", required = true)
    protected int item;

    /**
     * Obtient le reste du modèle de contenu.
     *
     * <p>
     * Vous obtenez la propriété "catch-all" pour la raison suivante :
     * Le nom de champ "DATEEND" est utilisé par deux parties différentes d'un schéma. Reportez-vous à :
     * ligne 449 sur file:/home/iat-atx/Bureau/ATEXO/WORKSPACE/lt_concentrateur_annonces/concentrateur-back/src/main/resources/simplified-xsd/joue/common_2014.xsd
     * ligne 447 sur file:/home/iat-atx/Bureau/ATEXO/WORKSPACE/lt_concentrateur_annonces/concentrateur-back/src/main/resources/simplified-xsd/joue/common_2014.xsd
     * <p>
     * Pour vous débarrasser de cette propriété, appliquez une personnalisation de propriété à l'une
     * des deux déclarations suivantes afin de modifier leurs noms :
     * Gets the value of the content property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link TextFtSingleLine }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link CpvSet }{@code >}
     * {@link JAXBElement }{@code <}{@link Nuts }{@code >}
     * {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     * {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     * {@link JAXBElement }{@code <}{@link ObjectF04 .AC }{@code >}
     * {@link JAXBElement }{@code <}{@link Val }{@code >}
     * {@link JAXBElement }{@code <}{@link DurationMD }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     * {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    public List<JAXBElement<?>> getContent() {
        if (content == null) {
            content = new ArrayList<JAXBElement<?>>();
        }
        return this.content;
    }

    /**
     * Obtient la valeur de la propriété item.
     */
    public int getITEM() {
        return item;
    }

    /**
     * Définit la valeur de la propriété item.
     */
    public void setITEM(int value) {
        this.item = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_criteria_doc"/&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "acquality",
            "accost",
            "acprice",
            "acprocurementdoc"
    })
    @ToString
    @EqualsAndHashCode
    public static class AC {

        @XmlElement(name = "AC_QUALITY")
        protected List<AcDefinition> acquality;
        @XmlElement(name = "AC_COST")
        protected List<AcDefinition> accost;
        @XmlElement(name = "AC_PRICE")
        protected ACPRICE acprice;
        @XmlElement(name = "AC_PROCUREMENT_DOC")
        protected Empty acprocurementdoc;

        /**
         * Gets the value of the acquality property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the acquality property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getACQUALITY().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AcDefinition }
         */
        public List<AcDefinition> getACQUALITY() {
            if (acquality == null) {
                acquality = new ArrayList<AcDefinition>();
            }
            return this.acquality;
        }

        /**
         * Gets the value of the accost property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the accost property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getACCOST().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AcDefinition }
         */
        public List<AcDefinition> getACCOST() {
            if (accost == null) {
                accost = new ArrayList<AcDefinition>();
            }
            return this.accost;
        }

        /**
         * Obtient la valeur de la propriété acprice.
         *
         * @return possible object is
         * {@link ACPRICE }
         */
        public ACPRICE getACPRICE() {
            return acprice;
        }

        /**
         * Définit la valeur de la propriété acprice.
         *
         * @param value allowed object is
         *              {@link ACPRICE }
         */
        public void setACPRICE(ACPRICE value) {
            this.acprice = value;
        }

        /**
         * Obtient la valeur de la propriété acprocurementdoc.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getACPROCUREMENTDOC() {
            return acprocurementdoc;
        }

        /**
         * Définit la valeur de la propriété acprocurementdoc.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setACPROCUREMENTDOC(Empty value) {
            this.acprocurementdoc = value;
        }

    }

}
