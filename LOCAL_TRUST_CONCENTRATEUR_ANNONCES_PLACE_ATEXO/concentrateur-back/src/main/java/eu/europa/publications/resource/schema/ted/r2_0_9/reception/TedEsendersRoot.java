package eu.europa.publications.resource.schema.ted.r2_0_9.reception;


import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "TED_ESENDERS")
@NoArgsConstructor
public class TedEsendersRoot extends TedEsenders {
}
