//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;


/**
 * Section IV: PROCEDURE
 *
 * <p>Classe Java pour procedure_f13 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="procedure_f13"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_OPEN"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_RESTRICTED"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="CRITERIA_EVALUATION"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines"&gt;
 *                 &lt;attribute name="PUBLICATION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}publication" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NOTICE_NUMBER_OJ" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "procedure_f13", propOrder = {
        "ptopen",
        "ptrestricted",
        "criteriaevaluation",
        "noticenumberoj"
})
@ToString
@EqualsAndHashCode
public class ProcedureF13 {

    @XmlElement(name = "PT_OPEN")
    protected Empty ptopen;
    @XmlElement(name = "PT_RESTRICTED")
    protected Empty ptrestricted;
    @XmlElement(name = "CRITERIA_EVALUATION", required = true)
    protected ProcedureF13.CRITERIAEVALUATION criteriaevaluation;
    @XmlElement(name = "NOTICE_NUMBER_OJ")
    protected String noticenumberoj;

    /**
     * Obtient la valeur de la propriété ptopen.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTOPEN() {
        return ptopen;
    }

    /**
     * Définit la valeur de la propriété ptopen.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTOPEN(Empty value) {
        this.ptopen = value;
    }

    /**
     * Obtient la valeur de la propriété ptrestricted.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTRESTRICTED() {
        return ptrestricted;
    }

    /**
     * Définit la valeur de la propriété ptrestricted.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTRESTRICTED(Empty value) {
        this.ptrestricted = value;
    }

    /**
     * Obtient la valeur de la propriété criteriaevaluation.
     *
     * @return possible object is
     * {@link ProcedureF13 .CRITERIAEVALUATION }
     */
    public ProcedureF13.CRITERIAEVALUATION getCRITERIAEVALUATION() {
        return criteriaevaluation;
    }

    /**
     * Définit la valeur de la propriété criteriaevaluation.
     *
     * @param value allowed object is
     *              {@link ProcedureF13 .CRITERIAEVALUATION }
     */
    public void setCRITERIAEVALUATION(ProcedureF13.CRITERIAEVALUATION value) {
        this.criteriaevaluation = value;
    }

    /**
     * Obtient la valeur de la propriété noticenumberoj.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNOTICENUMBEROJ() {
        return noticenumberoj;
    }

    /**
     * Définit la valeur de la propriété noticenumberoj.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNOTICENUMBEROJ(String value) {
        this.noticenumberoj = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines"&gt;
     *       &lt;attribute name="PUBLICATION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}publication" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @ToString(callSuper = true)
    @EqualsAndHashCode(callSuper = true)
    public static class CRITERIAEVALUATION
            extends TextFtMultiLines {

        @XmlAttribute(name = "PUBLICATION")
        protected Publication publication;

        /**
         * Obtient la valeur de la propriété publication.
         *
         * @return possible object is
         * {@link Publication }
         */
        public Publication getPUBLICATION() {
            return publication;
        }

        /**
         * Définit la valeur de la propriété publication.
         *
         * @param value allowed object is
         *              {@link Publication }
         */
        public void setPUBLICATION(Publication value) {
            this.publication = value;
        }

    }

}
