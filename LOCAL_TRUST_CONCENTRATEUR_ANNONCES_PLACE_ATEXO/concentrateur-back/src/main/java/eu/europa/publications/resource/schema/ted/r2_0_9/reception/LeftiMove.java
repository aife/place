//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * Section III: LEFTI
 *
 * <p>Classe Java pour lefti_move complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="lefti_move"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="COST_PARAMETERS" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="EXCLUSIVE_RIGHTS_GRANTED" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines"/&gt;
 *           &lt;element name="NO_EXCLUSIVE_RIGHTS_GRANTED" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="PCT_ALLOCATED_OPERATOR" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal"&gt;
 *               &lt;maxInclusive value="100"/&gt;
 *               &lt;minInclusive value="0"/&gt;
 *               &lt;fractionDigits value="1"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="SOCIAL_STANDARDS" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines" minOccurs="0"/&gt;
 *         &lt;element name="PUBLIC_SERVICE_OBLIGATIONS" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines"/&gt;
 *         &lt;element name="OTHER_PARTICULAR_CONDITIONS" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines" minOccurs="0"/&gt;
 *         &lt;element name="INFORMATION_TICKETS" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines" minOccurs="0"/&gt;
 *         &lt;element name="PUNCTUALITY_RELIABILITY" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines" minOccurs="0"/&gt;
 *         &lt;element name="CANCELLATIONS_SERVICES" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines" minOccurs="0"/&gt;
 *         &lt;element name="CLEANLINESS_ROLLING_STOCK" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines" minOccurs="0"/&gt;
 *         &lt;element name="CUST_SATISFACTION_SURVEY" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines" minOccurs="0"/&gt;
 *         &lt;element name="COMPLAINT_HANDLING" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines" minOccurs="0"/&gt;
 *         &lt;element name="ASSIST_PERSONS_REDUCTED_MOB" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines" minOccurs="0"/&gt;
 *         &lt;element name="OTHER_QUALITY_TARGET" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines" minOccurs="0"/&gt;
 *         &lt;element name="REWARDS_PENALITIES" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "lefti_move", propOrder = {
        "costparameters",
        "exclusiverightsgranted",
        "noexclusiverightsgranted",
        "pctallocatedoperator",
        "socialstandards",
        "publicserviceobligations",
        "otherparticularconditions",
        "informationtickets",
        "punctualityreliability",
        "cancellationsservices",
        "cleanlinessrollingstock",
        "custsatisfactionsurvey",
        "complainthandling",
        "assistpersonsreductedmob",
        "otherqualitytarget",
        "rewardspenalities"
})
@ToString
@EqualsAndHashCode
public class LeftiMove {

    @XmlElement(name = "COST_PARAMETERS", required = true)
    protected TextFtMultiLines costparameters;
    @XmlElement(name = "EXCLUSIVE_RIGHTS_GRANTED")
    protected TextFtMultiLines exclusiverightsgranted;
    @XmlElement(name = "NO_EXCLUSIVE_RIGHTS_GRANTED")
    protected Empty noexclusiverightsgranted;
    @XmlElement(name = "PCT_ALLOCATED_OPERATOR")
    protected BigDecimal pctallocatedoperator;
    @XmlElement(name = "SOCIAL_STANDARDS")
    protected TextFtMultiLines socialstandards;
    @XmlElement(name = "PUBLIC_SERVICE_OBLIGATIONS", required = true)
    protected TextFtMultiLines publicserviceobligations;
    @XmlElement(name = "OTHER_PARTICULAR_CONDITIONS")
    protected TextFtMultiLines otherparticularconditions;
    @XmlElement(name = "INFORMATION_TICKETS")
    protected TextFtMultiLines informationtickets;
    @XmlElement(name = "PUNCTUALITY_RELIABILITY")
    protected TextFtMultiLines punctualityreliability;
    @XmlElement(name = "CANCELLATIONS_SERVICES")
    protected TextFtMultiLines cancellationsservices;
    @XmlElement(name = "CLEANLINESS_ROLLING_STOCK")
    protected TextFtMultiLines cleanlinessrollingstock;
    @XmlElement(name = "CUST_SATISFACTION_SURVEY")
    protected TextFtMultiLines custsatisfactionsurvey;
    @XmlElement(name = "COMPLAINT_HANDLING")
    protected TextFtMultiLines complainthandling;
    @XmlElement(name = "ASSIST_PERSONS_REDUCTED_MOB")
    protected TextFtMultiLines assistpersonsreductedmob;
    @XmlElement(name = "OTHER_QUALITY_TARGET")
    protected TextFtMultiLines otherqualitytarget;
    @XmlElement(name = "REWARDS_PENALITIES")
    protected TextFtMultiLines rewardspenalities;

    /**
     * Obtient la valeur de la propriété costparameters.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getCOSTPARAMETERS() {
        return costparameters;
    }

    /**
     * Définit la valeur de la propriété costparameters.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setCOSTPARAMETERS(TextFtMultiLines value) {
        this.costparameters = value;
    }

    /**
     * Obtient la valeur de la propriété exclusiverightsgranted.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getEXCLUSIVERIGHTSGRANTED() {
        return exclusiverightsgranted;
    }

    /**
     * Définit la valeur de la propriété exclusiverightsgranted.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setEXCLUSIVERIGHTSGRANTED(TextFtMultiLines value) {
        this.exclusiverightsgranted = value;
    }

    /**
     * Obtient la valeur de la propriété noexclusiverightsgranted.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getNOEXCLUSIVERIGHTSGRANTED() {
        return noexclusiverightsgranted;
    }

    /**
     * Définit la valeur de la propriété noexclusiverightsgranted.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setNOEXCLUSIVERIGHTSGRANTED(Empty value) {
        this.noexclusiverightsgranted = value;
    }

    /**
     * Obtient la valeur de la propriété pctallocatedoperator.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getPCTALLOCATEDOPERATOR() {
        return pctallocatedoperator;
    }

    /**
     * Définit la valeur de la propriété pctallocatedoperator.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setPCTALLOCATEDOPERATOR(BigDecimal value) {
        this.pctallocatedoperator = value;
    }

    /**
     * Obtient la valeur de la propriété socialstandards.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getSOCIALSTANDARDS() {
        return socialstandards;
    }

    /**
     * Définit la valeur de la propriété socialstandards.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setSOCIALSTANDARDS(TextFtMultiLines value) {
        this.socialstandards = value;
    }

    /**
     * Obtient la valeur de la propriété publicserviceobligations.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getPUBLICSERVICEOBLIGATIONS() {
        return publicserviceobligations;
    }

    /**
     * Définit la valeur de la propriété publicserviceobligations.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setPUBLICSERVICEOBLIGATIONS(TextFtMultiLines value) {
        this.publicserviceobligations = value;
    }

    /**
     * Obtient la valeur de la propriété otherparticularconditions.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getOTHERPARTICULARCONDITIONS() {
        return otherparticularconditions;
    }

    /**
     * Définit la valeur de la propriété otherparticularconditions.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setOTHERPARTICULARCONDITIONS(TextFtMultiLines value) {
        this.otherparticularconditions = value;
    }

    /**
     * Obtient la valeur de la propriété informationtickets.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getINFORMATIONTICKETS() {
        return informationtickets;
    }

    /**
     * Définit la valeur de la propriété informationtickets.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setINFORMATIONTICKETS(TextFtMultiLines value) {
        this.informationtickets = value;
    }

    /**
     * Obtient la valeur de la propriété punctualityreliability.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getPUNCTUALITYRELIABILITY() {
        return punctualityreliability;
    }

    /**
     * Définit la valeur de la propriété punctualityreliability.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setPUNCTUALITYRELIABILITY(TextFtMultiLines value) {
        this.punctualityreliability = value;
    }

    /**
     * Obtient la valeur de la propriété cancellationsservices.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getCANCELLATIONSSERVICES() {
        return cancellationsservices;
    }

    /**
     * Définit la valeur de la propriété cancellationsservices.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setCANCELLATIONSSERVICES(TextFtMultiLines value) {
        this.cancellationsservices = value;
    }

    /**
     * Obtient la valeur de la propriété cleanlinessrollingstock.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getCLEANLINESSROLLINGSTOCK() {
        return cleanlinessrollingstock;
    }

    /**
     * Définit la valeur de la propriété cleanlinessrollingstock.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setCLEANLINESSROLLINGSTOCK(TextFtMultiLines value) {
        this.cleanlinessrollingstock = value;
    }

    /**
     * Obtient la valeur de la propriété custsatisfactionsurvey.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getCUSTSATISFACTIONSURVEY() {
        return custsatisfactionsurvey;
    }

    /**
     * Définit la valeur de la propriété custsatisfactionsurvey.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setCUSTSATISFACTIONSURVEY(TextFtMultiLines value) {
        this.custsatisfactionsurvey = value;
    }

    /**
     * Obtient la valeur de la propriété complainthandling.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getCOMPLAINTHANDLING() {
        return complainthandling;
    }

    /**
     * Définit la valeur de la propriété complainthandling.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setCOMPLAINTHANDLING(TextFtMultiLines value) {
        this.complainthandling = value;
    }

    /**
     * Obtient la valeur de la propriété assistpersonsreductedmob.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getASSISTPERSONSREDUCTEDMOB() {
        return assistpersonsreductedmob;
    }

    /**
     * Définit la valeur de la propriété assistpersonsreductedmob.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setASSISTPERSONSREDUCTEDMOB(TextFtMultiLines value) {
        this.assistpersonsreductedmob = value;
    }

    /**
     * Obtient la valeur de la propriété otherqualitytarget.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getOTHERQUALITYTARGET() {
        return otherqualitytarget;
    }

    /**
     * Définit la valeur de la propriété otherqualitytarget.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setOTHERQUALITYTARGET(TextFtMultiLines value) {
        this.otherqualitytarget = value;
    }

    /**
     * Obtient la valeur de la propriété rewardspenalities.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getREWARDSPENALITIES() {
        return rewardspenalities;
    }

    /**
     * Définit la valeur de la propriété rewardspenalities.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setREWARDSPENALITIES(TextFtMultiLines value) {
        this.rewardspenalities = value;
    }

}
