//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * Section VII: CHANGES
 *
 * <p>Classe Java pour changes_f14 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="changes_f14"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="MODIFICATION_ORIGINAL" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}non_published"/&gt;
 *           &lt;element name="PUBLICATION_TED" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}non_published"/&gt;
 *         &lt;/choice&gt;
 *         &lt;choice&gt;
 *           &lt;sequence&gt;
 *             &lt;element name="CHANGE" maxOccurs="120"&gt;
 *               &lt;complexType&gt;
 *                 &lt;complexContent&gt;
 *                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                     &lt;sequence&gt;
 *                       &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}WHERE"/&gt;
 *                       &lt;element name="OLD_VALUE"&gt;
 *                         &lt;complexType&gt;
 *                           &lt;complexContent&gt;
 *                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                               &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}data"/&gt;
 *                             &lt;/restriction&gt;
 *                           &lt;/complexContent&gt;
 *                         &lt;/complexType&gt;
 *                       &lt;/element&gt;
 *                       &lt;element name="NEW_VALUE"&gt;
 *                         &lt;complexType&gt;
 *                           &lt;complexContent&gt;
 *                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                               &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}data"/&gt;
 *                             &lt;/restriction&gt;
 *                           &lt;/complexContent&gt;
 *                         &lt;/complexType&gt;
 *                       &lt;/element&gt;
 *                     &lt;/sequence&gt;
 *                   &lt;/restriction&gt;
 *                 &lt;/complexContent&gt;
 *               &lt;/complexType&gt;
 *             &lt;/element&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}INFO_ADD" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}INFO_ADD"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "changes_f14", propOrder = {
        "content"
})
@ToString
@EqualsAndHashCode
public class ChangesF14 {

    @XmlElementRefs({
            @XmlElementRef(name = "MODIFICATION_ORIGINAL", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "PUBLICATION_TED", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "CHANGE", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "INFO_ADD", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false)
    })
    protected List<JAXBElement<?>> content;

    /**
     * Obtient le reste du modèle de contenu.
     *
     * <p>
     * Vous obtenez la propriété "catch-all" pour la raison suivante :
     * Le nom de champ "INFOADD" est utilisé par deux parties différentes d'un schéma. Reportez-vous à :
     * ligne 65 sur file:/home/iat-atx/Bureau/ATEXO/WORKSPACE/lt_concentrateur_annonces/concentrateur-back/src/main/resources/simplified-xsd/joue/F14_2014.xsd
     * ligne 63 sur file:/home/iat-atx/Bureau/ATEXO/WORKSPACE/lt_concentrateur_annonces/concentrateur-back/src/main/resources/simplified-xsd/joue/F14_2014.xsd
     * <p>
     * Pour vous débarrasser de cette propriété, appliquez une personnalisation de propriété à l'une
     * des deux déclarations suivantes afin de modifier leurs noms :
     * Gets the value of the content property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link NonPublished }{@code >}
     * {@link JAXBElement }{@code <}{@link NonPublished }{@code >}
     * {@link JAXBElement }{@code <}{@link ChangesF14 .CHANGE }{@code >}
     * {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     */
    public List<JAXBElement<?>> getContent() {
        if (content == null) {
            content = new ArrayList<JAXBElement<?>>();
        }
        return this.content;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}WHERE"/&gt;
     *         &lt;element name="OLD_VALUE"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}data"/&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="NEW_VALUE"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}data"/&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "where",
            "oldvalue",
            "newvalue"
    })
    @ToString
    @EqualsAndHashCode
    public static class CHANGE {

        @XmlElement(name = "WHERE", required = true)
        protected WHERE where;
        @XmlElement(name = "OLD_VALUE", required = true)
        protected ChangesF14.CHANGE.OLDVALUE oldvalue;
        @XmlElement(name = "NEW_VALUE", required = true)
        protected ChangesF14.CHANGE.NEWVALUE newvalue;

        /**
         * Obtient la valeur de la propriété where.
         *
         * @return possible object is
         * {@link WHERE }
         */
        public WHERE getWHERE() {
            return where;
        }

        /**
         * Définit la valeur de la propriété where.
         *
         * @param value allowed object is
         *              {@link WHERE }
         */
        public void setWHERE(WHERE value) {
            this.where = value;
        }

        /**
         * Obtient la valeur de la propriété oldvalue.
         *
         * @return possible object is
         * {@link ChangesF14 .CHANGE.OLDVALUE }
         */
        public ChangesF14.CHANGE.OLDVALUE getOLDVALUE() {
            return oldvalue;
        }

        /**
         * Définit la valeur de la propriété oldvalue.
         *
         * @param value allowed object is
         *              {@link ChangesF14 .CHANGE.OLDVALUE }
         */
        public void setOLDVALUE(ChangesF14.CHANGE.OLDVALUE value) {
            this.oldvalue = value;
        }

        /**
         * Obtient la valeur de la propriété newvalue.
         *
         * @return possible object is
         * {@link ChangesF14 .CHANGE.NEWVALUE }
         */
        public ChangesF14.CHANGE.NEWVALUE getNEWVALUE() {
            return newvalue;
        }

        /**
         * Définit la valeur de la propriété newvalue.
         *
         * @param value allowed object is
         *              {@link ChangesF14 .CHANGE.NEWVALUE }
         */
        public void setNEWVALUE(ChangesF14.CHANGE.NEWVALUE value) {
            this.newvalue = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}data"/&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "nothing",
                "cpvmain",
                "cpvadditional",
                "text",
                "date",
                "time"
        })
        @ToString
        @EqualsAndHashCode
        public static class NEWVALUE {

            @XmlElement(name = "NOTHING")
            protected Empty nothing;
            @XmlElement(name = "CPV_MAIN")
            protected CpvSet cpvmain;
            @XmlElement(name = "CPV_ADDITIONAL")
            protected List<CpvSet> cpvadditional;
            @XmlElement(name = "TEXT")
            protected TextFtMultiLines text;
            @XmlElement(name = "DATE")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar date;
            @XmlElement(name = "TIME")
            protected String time;

            /**
             * Obtient la valeur de la propriété nothing.
             *
             * @return possible object is
             * {@link Empty }
             */
            public Empty getNOTHING() {
                return nothing;
            }

            /**
             * Définit la valeur de la propriété nothing.
             *
             * @param value allowed object is
             *              {@link Empty }
             */
            public void setNOTHING(Empty value) {
                this.nothing = value;
            }

            /**
             * Obtient la valeur de la propriété cpvmain.
             *
             * @return possible object is
             * {@link CpvSet }
             */
            public CpvSet getCPVMAIN() {
                return cpvmain;
            }

            /**
             * Définit la valeur de la propriété cpvmain.
             *
             * @param value allowed object is
             *              {@link CpvSet }
             */
            public void setCPVMAIN(CpvSet value) {
                this.cpvmain = value;
            }

            /**
             * Gets the value of the cpvadditional property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the cpvadditional property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCPVADDITIONAL().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link CpvSet }
             */
            public List<CpvSet> getCPVADDITIONAL() {
                if (cpvadditional == null) {
                    cpvadditional = new ArrayList<CpvSet>();
                }
                return this.cpvadditional;
            }

            /**
             * Obtient la valeur de la propriété text.
             *
             * @return possible object is
             * {@link TextFtMultiLines }
             */
            public TextFtMultiLines getTEXT() {
                return text;
            }

            /**
             * Définit la valeur de la propriété text.
             *
             * @param value allowed object is
             *              {@link TextFtMultiLines }
             */
            public void setTEXT(TextFtMultiLines value) {
                this.text = value;
            }

            /**
             * Obtient la valeur de la propriété date.
             *
             * @return possible object is
             * {@link XMLGregorianCalendar }
             */
            public XMLGregorianCalendar getDATE() {
                return date;
            }

            /**
             * Définit la valeur de la propriété date.
             *
             * @param value allowed object is
             *              {@link XMLGregorianCalendar }
             */
            public void setDATE(XMLGregorianCalendar value) {
                this.date = value;
            }

            /**
             * Obtient la valeur de la propriété time.
             *
             * @return possible object is
             * {@link String }
             */
            public String getTIME() {
                return time;
            }

            /**
             * Définit la valeur de la propriété time.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setTIME(String value) {
                this.time = value;
            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}data"/&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "nothing",
                "cpvmain",
                "cpvadditional",
                "text",
                "date",
                "time"
        })
        @ToString
        @EqualsAndHashCode
        public static class OLDVALUE {

            @XmlElement(name = "NOTHING")
            protected Empty nothing;
            @XmlElement(name = "CPV_MAIN")
            protected CpvSet cpvmain;
            @XmlElement(name = "CPV_ADDITIONAL")
            protected List<CpvSet> cpvadditional;
            @XmlElement(name = "TEXT")
            protected TextFtMultiLines text;
            @XmlElement(name = "DATE")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar date;
            @XmlElement(name = "TIME")
            protected String time;

            /**
             * Obtient la valeur de la propriété nothing.
             *
             * @return possible object is
             * {@link Empty }
             */
            public Empty getNOTHING() {
                return nothing;
            }

            /**
             * Définit la valeur de la propriété nothing.
             *
             * @param value allowed object is
             *              {@link Empty }
             */
            public void setNOTHING(Empty value) {
                this.nothing = value;
            }

            /**
             * Obtient la valeur de la propriété cpvmain.
             *
             * @return possible object is
             * {@link CpvSet }
             */
            public CpvSet getCPVMAIN() {
                return cpvmain;
            }

            /**
             * Définit la valeur de la propriété cpvmain.
             *
             * @param value allowed object is
             *              {@link CpvSet }
             */
            public void setCPVMAIN(CpvSet value) {
                this.cpvmain = value;
            }

            /**
             * Gets the value of the cpvadditional property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the cpvadditional property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCPVADDITIONAL().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link CpvSet }
             */
            public List<CpvSet> getCPVADDITIONAL() {
                if (cpvadditional == null) {
                    cpvadditional = new ArrayList<CpvSet>();
                }
                return this.cpvadditional;
            }

            /**
             * Obtient la valeur de la propriété text.
             *
             * @return possible object is
             * {@link TextFtMultiLines }
             */
            public TextFtMultiLines getTEXT() {
                return text;
            }

            /**
             * Définit la valeur de la propriété text.
             *
             * @param value allowed object is
             *              {@link TextFtMultiLines }
             */
            public void setTEXT(TextFtMultiLines value) {
                this.text = value;
            }

            /**
             * Obtient la valeur de la propriété date.
             *
             * @return possible object is
             * {@link XMLGregorianCalendar }
             */
            public XMLGregorianCalendar getDATE() {
                return date;
            }

            /**
             * Définit la valeur de la propriété date.
             *
             * @param value allowed object is
             *              {@link XMLGregorianCalendar }
             */
            public void setDATE(XMLGregorianCalendar value) {
                this.date = value;
            }

            /**
             * Obtient la valeur de la propriété time.
             *
             * @return possible object is
             * {@link String }
             */
            public String getTIME() {
                return time;
            }

            /**
             * Définit la valeur de la propriété time.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setTIME(String value) {
                this.time = value;
            }

        }

    }

}
