//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Section II: OBJECT OF THE CONTRACT
 *
 * <p>Classe Java pour object_contract_f06 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="object_contract_f06"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TITLE"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}REFERENCE_NUMBER" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CPV_MAIN"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TYPE_CONTRACT"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SHORT_DESCR"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="VAL_TOTAL"&gt;
 *             &lt;complexType&gt;
 *               &lt;simpleContent&gt;
 *                 &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;val"&gt;
 *                   &lt;attribute name="PUBLICATION" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}publication" /&gt;
 *                 &lt;/extension&gt;
 *               &lt;/simpleContent&gt;
 *             &lt;/complexType&gt;
 *           &lt;/element&gt;
 *           &lt;element name="VAL_RANGE_TOTAL"&gt;
 *             &lt;complexType&gt;
 *               &lt;complexContent&gt;
 *                 &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}val_range"&gt;
 *                   &lt;attribute name="PUBLICATION" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}publication" /&gt;
 *                 &lt;/extension&gt;
 *               &lt;/complexContent&gt;
 *             &lt;/complexType&gt;
 *           &lt;/element&gt;
 *         &lt;/choice&gt;
 *         &lt;choice&gt;
 *           &lt;sequence&gt;
 *             &lt;element name="LOT_DIVISION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty"/&gt;
 *             &lt;element name="OBJECT_DESCR" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}object_f06" maxOccurs="4000"/&gt;
 *           &lt;/sequence&gt;
 *           &lt;sequence&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NO_LOT_DIVISION"/&gt;
 *             &lt;element name="OBJECT_DESCR" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}object_f06"/&gt;
 *           &lt;/sequence&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "object_contract_f06", propOrder = {
        "content"
})
@ToString
@EqualsAndHashCode
public class ObjectContractF06 {

    @XmlElementRefs({
            @XmlElementRef(name = "TITLE", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "REFERENCE_NUMBER", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "CPV_MAIN", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "TYPE_CONTRACT", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "SHORT_DESCR", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "VAL_TOTAL", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "VAL_RANGE_TOTAL", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "LOT_DIVISION", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "OBJECT_DESCR", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "NO_LOT_DIVISION", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false)
    })
    protected List<JAXBElement<?>> content;

    /**
     * Obtient le reste du modèle de contenu.
     *
     * <p>
     * Vous obtenez la propriété "catch-all" pour la raison suivante :
     * Le nom de champ "OBJECTDESCR" est utilisé par deux parties différentes d'un schéma. Reportez-vous à :
     * ligne 197 sur file:/home/iat-atx/Bureau/ATEXO/WORKSPACE/lt_concentrateur_annonces/concentrateur-back/src/main/resources/simplified-xsd/joue/F06_2014.xsd
     * ligne 193 sur file:/home/iat-atx/Bureau/ATEXO/WORKSPACE/lt_concentrateur_annonces/concentrateur-back/src/main/resources/simplified-xsd/joue/F06_2014.xsd
     * <p>
     * Pour vous débarrasser de cette propriété, appliquez une personnalisation de propriété à l'une
     * des deux déclarations suivantes afin de modifier leurs noms :
     * Gets the value of the content property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link TextFtSingleLine }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link CpvSet }{@code >}
     * {@link JAXBElement }{@code <}{@link TypeContract }{@code >}
     * {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     * {@link JAXBElement }{@code <}{@link ObjectContractF06 .VALTOTAL }{@code >}
     * {@link JAXBElement }{@code <}{@link ObjectContractF06 .VALRANGETOTAL }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link ObjectF06 }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    public List<JAXBElement<?>> getContent() {
        if (content == null) {
            content = new ArrayList<JAXBElement<?>>();
        }
        return this.content;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}val_range"&gt;
     *       &lt;attribute name="PUBLICATION" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}publication" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @ToString(callSuper = true)
    @EqualsAndHashCode(callSuper = true)
    public static class VALRANGETOTAL
            extends ValRange {

        @XmlAttribute(name = "PUBLICATION", required = true)
        protected Publication publication;

        /**
         * Obtient la valeur de la propriété publication.
         *
         * @return possible object is
         * {@link Publication }
         */
        public Publication getPUBLICATION() {
            return publication;
        }

        /**
         * Définit la valeur de la propriété publication.
         *
         * @param value allowed object is
         *              {@link Publication }
         */
        public void setPUBLICATION(Publication value) {
            this.publication = value;
        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;val"&gt;
     *       &lt;attribute name="PUBLICATION" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}publication" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @ToString(callSuper = true)
    @EqualsAndHashCode(callSuper = true)
    public static class VALTOTAL
            extends Val {

        @XmlAttribute(name = "PUBLICATION", required = true)
        protected Publication publication;

        /**
         * Obtient la valeur de la propriété publication.
         *
         * @return possible object is
         * {@link Publication }
         */
        public Publication getPUBLICATION() {
            return publication;
        }

        /**
         * Définit la valeur de la propriété publication.
         *
         * @param value allowed object is
         *              {@link Publication }
         */
        public void setPUBLICATION(Publication value) {
            this.publication = value;
        }

    }

}
