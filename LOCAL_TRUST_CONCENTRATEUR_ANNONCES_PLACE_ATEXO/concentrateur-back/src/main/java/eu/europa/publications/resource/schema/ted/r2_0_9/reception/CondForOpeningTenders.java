//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour cond_for_opening_tenders complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="cond_for_opening_tenders"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DATE_OPENING_TENDERS" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}date_full"/&gt;
 *         &lt;element name="TIME_OPENING_TENDERS" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}time"/&gt;
 *         &lt;element name="PLACE" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}INFO_ADD" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cond_for_opening_tenders", propOrder = {
        "dateopeningtenders",
        "timeopeningtenders",
        "place",
        "infoadd"
})
@ToString
@EqualsAndHashCode
public class CondForOpeningTenders {

    @XmlElement(name = "DATE_OPENING_TENDERS", required = true)
    protected String dateopeningtenders;
    @XmlElement(name = "TIME_OPENING_TENDERS", required = true)
    protected String timeopeningtenders;
    @XmlElement(name = "PLACE")
    protected TextFtMultiLines place;
    @XmlElement(name = "INFO_ADD")
    protected TextFtMultiLines infoadd;

    /**
     * Obtient la valeur de la propriété dateopeningtenders.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public String getDATEOPENINGTENDERS() {
        return dateopeningtenders;
    }

    /**
     * Définit la valeur de la propriété dateopeningtenders.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDATEOPENINGTENDERS(String value) {
        this.dateopeningtenders = value;
    }

    /**
     * Obtient la valeur de la propriété timeopeningtenders.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTIMEOPENINGTENDERS() {
        return timeopeningtenders;
    }

    /**
     * Définit la valeur de la propriété timeopeningtenders.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTIMEOPENINGTENDERS(String value) {
        this.timeopeningtenders = value;
    }

    /**
     * Obtient la valeur de la propriété place.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getPLACE() {
        return place;
    }

    /**
     * Définit la valeur de la propriété place.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setPLACE(TextFtMultiLines value) {
        this.place = value;
    }

    /**
     * Obtient la valeur de la propriété infoadd.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getINFOADD() {
        return infoadd;
    }

    /**
     * Définit la valeur de la propriété infoadd.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setINFOADD(TextFtMultiLines value) {
        this.infoadd = value;
    }

}
