//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Section IV: PROCEDURE
 *
 * <p>Classe Java pour procedure_move complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="procedure_move"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="PT_COMPETITIVE_TENDERING" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty"/&gt;
 *         &lt;element name="PT_DA_INTERNAL_OPERATOR" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty"/&gt;
 *         &lt;element name="PT_DA_SMALL_CONTRACT" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty"/&gt;
 *         &lt;element name="PT_DA_MEDIUM_ENTERPRISE" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty"/&gt;
 *         &lt;element name="PT_DA_RAILWAY_TRANSPORT" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "procedure_move", propOrder = {
        "ptcompetitivetendering",
        "ptdainternaloperator",
        "ptdasmallcontract",
        "ptdamediumenterprise",
        "ptdarailwaytransport"
})
@ToString
@EqualsAndHashCode
public class ProcedureMove {

    @XmlElement(name = "PT_COMPETITIVE_TENDERING")
    protected Empty ptcompetitivetendering;
    @XmlElement(name = "PT_DA_INTERNAL_OPERATOR")
    protected Empty ptdainternaloperator;
    @XmlElement(name = "PT_DA_SMALL_CONTRACT")
    protected Empty ptdasmallcontract;
    @XmlElement(name = "PT_DA_MEDIUM_ENTERPRISE")
    protected Empty ptdamediumenterprise;
    @XmlElement(name = "PT_DA_RAILWAY_TRANSPORT")
    protected Empty ptdarailwaytransport;

    /**
     * Obtient la valeur de la propriété ptcompetitivetendering.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTCOMPETITIVETENDERING() {
        return ptcompetitivetendering;
    }

    /**
     * Définit la valeur de la propriété ptcompetitivetendering.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTCOMPETITIVETENDERING(Empty value) {
        this.ptcompetitivetendering = value;
    }

    /**
     * Obtient la valeur de la propriété ptdainternaloperator.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTDAINTERNALOPERATOR() {
        return ptdainternaloperator;
    }

    /**
     * Définit la valeur de la propriété ptdainternaloperator.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTDAINTERNALOPERATOR(Empty value) {
        this.ptdainternaloperator = value;
    }

    /**
     * Obtient la valeur de la propriété ptdasmallcontract.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTDASMALLCONTRACT() {
        return ptdasmallcontract;
    }

    /**
     * Définit la valeur de la propriété ptdasmallcontract.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTDASMALLCONTRACT(Empty value) {
        this.ptdasmallcontract = value;
    }

    /**
     * Obtient la valeur de la propriété ptdamediumenterprise.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTDAMEDIUMENTERPRISE() {
        return ptdamediumenterprise;
    }

    /**
     * Définit la valeur de la propriété ptdamediumenterprise.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTDAMEDIUMENTERPRISE(Empty value) {
        this.ptdamediumenterprise = value;
    }

    /**
     * Obtient la valeur de la propriété ptdarailwaytransport.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTDARAILWAYTRANSPORT() {
        return ptdarailwaytransport;
    }

    /**
     * Définit la valeur de la propriété ptdarailwaytransport.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTDARAILWAYTRANSPORT(Empty value) {
        this.ptdarailwaytransport = value;
    }

}
