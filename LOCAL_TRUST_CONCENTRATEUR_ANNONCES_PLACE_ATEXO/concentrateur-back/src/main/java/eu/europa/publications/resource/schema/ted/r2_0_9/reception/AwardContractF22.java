//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


/**
 * Section V: AWARD OF CONTRACT
 *
 * <p>Classe Java pour award_contract_f22 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="award_contract_f22"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contract_number"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TITLE" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NO_AWARDED_CONTRACT"/&gt;
 *           &lt;element name="AWARDED_CONTRACT"&gt;
 *             &lt;complexType&gt;
 *               &lt;complexContent&gt;
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                   &lt;sequence&gt;
 *                     &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DATE_CONCLUSION_CONTRACT" minOccurs="0"/&gt;
 *                     &lt;element name="TENDERS"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_man"&gt;
 *                             &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}nb_tenders"/&gt;
 *                           &lt;/extension&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;element name="CONTRACTORS"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_man"&gt;
 *                             &lt;choice&gt;
 *                               &lt;sequence&gt;
 *                                 &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}AWARDED_TO_GROUP"/&gt;
 *                                 &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contractor_sme_opt" maxOccurs="100" minOccurs="2"/&gt;
 *                               &lt;/sequence&gt;
 *                               &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contractor_sme_opt"/&gt;
 *                             &lt;/choice&gt;
 *                           &lt;/extension&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;element name="VALUES"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_man"&gt;
 *                             &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_contract_value"/&gt;
 *                           &lt;/extension&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;sequence minOccurs="0"&gt;
 *                       &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LIKELY_SUBCONTRACTED"/&gt;
 *                       &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}subcontracting"/&gt;
 *                     &lt;/sequence&gt;
 *                   &lt;/sequence&gt;
 *                 &lt;/restriction&gt;
 *               &lt;/complexContent&gt;
 *             &lt;/complexType&gt;
 *           &lt;/element&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ITEM" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}nb_contract" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "award_contract_f22", propOrder = {
        "contractno",
        "lotno",
        "title",
        "noawardedcontract",
        "awardedcontract"
})
@ToString
@EqualsAndHashCode
public class AwardContractF22 {

    @XmlElement(name = "CONTRACT_NO")
    protected String contractno;
    @XmlElement(name = "LOT_NO")
    protected String lotno;
    @XmlElement(name = "TITLE")
    protected TextFtSingleLine title;
    @XmlElement(name = "NO_AWARDED_CONTRACT")
    protected NoAward noawardedcontract;
    @XmlElement(name = "AWARDED_CONTRACT")
    protected AwardContractF22.AWARDEDCONTRACT awardedcontract;
    @XmlAttribute(name = "ITEM", required = true)
    protected int item;

    /**
     * Obtient la valeur de la propriété contractno.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCONTRACTNO() {
        return contractno;
    }

    /**
     * Définit la valeur de la propriété contractno.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCONTRACTNO(String value) {
        this.contractno = value;
    }

    /**
     * Obtient la valeur de la propriété lotno.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLOTNO() {
        return lotno;
    }

    /**
     * Définit la valeur de la propriété lotno.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLOTNO(String value) {
        this.lotno = value;
    }

    /**
     * Obtient la valeur de la propriété title.
     *
     * @return possible object is
     * {@link TextFtSingleLine }
     */
    public TextFtSingleLine getTITLE() {
        return title;
    }

    /**
     * Définit la valeur de la propriété title.
     *
     * @param value allowed object is
     *              {@link TextFtSingleLine }
     */
    public void setTITLE(TextFtSingleLine value) {
        this.title = value;
    }

    /**
     * Obtient la valeur de la propriété noawardedcontract.
     *
     * @return possible object is
     * {@link NoAward }
     */
    public NoAward getNOAWARDEDCONTRACT() {
        return noawardedcontract;
    }

    /**
     * Définit la valeur de la propriété noawardedcontract.
     *
     * @param value allowed object is
     *              {@link NoAward }
     */
    public void setNOAWARDEDCONTRACT(NoAward value) {
        this.noawardedcontract = value;
    }

    /**
     * Obtient la valeur de la propriété awardedcontract.
     *
     * @return possible object is
     * {@link AwardContractF22 .AWARDEDCONTRACT }
     */
    public AwardContractF22.AWARDEDCONTRACT getAWARDEDCONTRACT() {
        return awardedcontract;
    }

    /**
     * Définit la valeur de la propriété awardedcontract.
     *
     * @param value allowed object is
     *              {@link AwardContractF22 .AWARDEDCONTRACT }
     */
    public void setAWARDEDCONTRACT(AwardContractF22.AWARDEDCONTRACT value) {
        this.awardedcontract = value;
    }

    /**
     * Obtient la valeur de la propriété item.
     */
    public int getITEM() {
        return item;
    }

    /**
     * Définit la valeur de la propriété item.
     */
    public void setITEM(int value) {
        this.item = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DATE_CONCLUSION_CONTRACT" minOccurs="0"/&gt;
     *         &lt;element name="TENDERS"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_man"&gt;
     *                 &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}nb_tenders"/&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="CONTRACTORS"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_man"&gt;
     *                 &lt;choice&gt;
     *                   &lt;sequence&gt;
     *                     &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}AWARDED_TO_GROUP"/&gt;
     *                     &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contractor_sme_opt" maxOccurs="100" minOccurs="2"/&gt;
     *                   &lt;/sequence&gt;
     *                   &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contractor_sme_opt"/&gt;
     *                 &lt;/choice&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="VALUES"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_man"&gt;
     *                 &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_contract_value"/&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;sequence minOccurs="0"&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LIKELY_SUBCONTRACTED"/&gt;
     *           &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}subcontracting"/&gt;
     *         &lt;/sequence&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "dateconclusioncontract",
            "tenders",
            "contractors",
            "values",
            "likelysubcontracted",
            "valsubcontracting",
            "pctsubcontracting",
            "infoaddsubcontracting"
    })
    @ToString
    @EqualsAndHashCode
    public static class AWARDEDCONTRACT {

        @XmlElement(name = "DATE_CONCLUSION_CONTRACT")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dateconclusioncontract;
        @XmlElement(name = "TENDERS", required = true)
        protected AwardContractF22.AWARDEDCONTRACT.TENDERS tenders;
        @XmlElement(name = "CONTRACTORS", required = true)
        protected AwardContractF22.AWARDEDCONTRACT.CONTRACTORS contractors;
        @XmlElement(name = "VALUES", required = true)
        protected AwardContractF22.AWARDEDCONTRACT.VALUES values;
        @XmlElement(name = "LIKELY_SUBCONTRACTED")
        protected Empty likelysubcontracted;
        @XmlElement(name = "VAL_SUBCONTRACTING")
        protected Val valsubcontracting;
        @XmlElement(name = "PCT_SUBCONTRACTING")
        @XmlSchemaType(name = "integer")
        protected Integer pctsubcontracting;
        @XmlElement(name = "INFO_ADD_SUBCONTRACTING")
        protected TextFtMultiLines infoaddsubcontracting;

        /**
         * Obtient la valeur de la propriété dateconclusioncontract.
         *
         * @return possible object is
         * {@link XMLGregorianCalendar }
         */
        public XMLGregorianCalendar getDATECONCLUSIONCONTRACT() {
            return dateconclusioncontract;
        }

        /**
         * Définit la valeur de la propriété dateconclusioncontract.
         *
         * @param value allowed object is
         *              {@link XMLGregorianCalendar }
         */
        public void setDATECONCLUSIONCONTRACT(XMLGregorianCalendar value) {
            this.dateconclusioncontract = value;
        }

        /**
         * Obtient la valeur de la propriété tenders.
         *
         * @return possible object is
         * {@link AwardContractF22 .AWARDEDCONTRACT.TENDERS }
         */
        public AwardContractF22.AWARDEDCONTRACT.TENDERS getTENDERS() {
            return tenders;
        }

        /**
         * Définit la valeur de la propriété tenders.
         *
         * @param value allowed object is
         *              {@link AwardContractF22 .AWARDEDCONTRACT.TENDERS }
         */
        public void setTENDERS(AwardContractF22.AWARDEDCONTRACT.TENDERS value) {
            this.tenders = value;
        }

        /**
         * Obtient la valeur de la propriété contractors.
         *
         * @return possible object is
         * {@link AwardContractF22 .AWARDEDCONTRACT.CONTRACTORS }
         */
        public AwardContractF22.AWARDEDCONTRACT.CONTRACTORS getCONTRACTORS() {
            return contractors;
        }

        /**
         * Définit la valeur de la propriété contractors.
         *
         * @param value allowed object is
         *              {@link AwardContractF22 .AWARDEDCONTRACT.CONTRACTORS }
         */
        public void setCONTRACTORS(AwardContractF22.AWARDEDCONTRACT.CONTRACTORS value) {
            this.contractors = value;
        }

        /**
         * Obtient la valeur de la propriété values.
         *
         * @return possible object is
         * {@link AwardContractF22 .AWARDEDCONTRACT.VALUES }
         */
        public AwardContractF22.AWARDEDCONTRACT.VALUES getVALUES() {
            return values;
        }

        /**
         * Définit la valeur de la propriété values.
         *
         * @param value allowed object is
         *              {@link AwardContractF22 .AWARDEDCONTRACT.VALUES }
         */
        public void setVALUES(AwardContractF22.AWARDEDCONTRACT.VALUES value) {
            this.values = value;
        }

        /**
         * Obtient la valeur de la propriété likelysubcontracted.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getLIKELYSUBCONTRACTED() {
            return likelysubcontracted;
        }

        /**
         * Définit la valeur de la propriété likelysubcontracted.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setLIKELYSUBCONTRACTED(Empty value) {
            this.likelysubcontracted = value;
        }

        /**
         * Obtient la valeur de la propriété valsubcontracting.
         *
         * @return possible object is
         * {@link Val }
         */
        public Val getVALSUBCONTRACTING() {
            return valsubcontracting;
        }

        /**
         * Définit la valeur de la propriété valsubcontracting.
         *
         * @param value allowed object is
         *              {@link Val }
         */
        public void setVALSUBCONTRACTING(Val value) {
            this.valsubcontracting = value;
        }

        /**
         * Obtient la valeur de la propriété pctsubcontracting.
         *
         * @return possible object is
         * {@link Integer }
         */
        public Integer getPCTSUBCONTRACTING() {
            return pctsubcontracting;
        }

        /**
         * Définit la valeur de la propriété pctsubcontracting.
         *
         * @param value allowed object is
         *              {@link Integer }
         */
        public void setPCTSUBCONTRACTING(Integer value) {
            this.pctsubcontracting = value;
        }

        /**
         * Obtient la valeur de la propriété infoaddsubcontracting.
         *
         * @return possible object is
         * {@link TextFtMultiLines }
         */
        public TextFtMultiLines getINFOADDSUBCONTRACTING() {
            return infoaddsubcontracting;
        }

        /**
         * Définit la valeur de la propriété infoaddsubcontracting.
         *
         * @param value allowed object is
         *              {@link TextFtMultiLines }
         */
        public void setINFOADDSUBCONTRACTING(TextFtMultiLines value) {
            this.infoaddsubcontracting = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_man"&gt;
         *       &lt;choice&gt;
         *         &lt;sequence&gt;
         *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}AWARDED_TO_GROUP"/&gt;
         *           &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contractor_sme_opt" maxOccurs="100" minOccurs="2"/&gt;
         *         &lt;/sequence&gt;
         *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contractor_sme_opt"/&gt;
         *       &lt;/choice&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "awardedtogroup",
                "contractorSmeOpt",
                "contractor"
        })
        @ToString(callSuper = true)
        @EqualsAndHashCode(callSuper = true)
        public static class CONTRACTORS
                extends AgreeToPublicationMan {

            @XmlElement(name = "AWARDED_TO_GROUP")
            protected Empty awardedtogroup;
            @XmlElement(name = "CONTRACTOR")
            protected List<eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF23.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR> contractorSmeOpt;
            @XmlElement(name = "CONTRACTOR")
            protected eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF23.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR contractor;

            /**
             * Obtient la valeur de la propriété awardedtogroup.
             *
             * @return possible object is
             * {@link Empty }
             */
            public Empty getAWARDEDTOGROUP() {
                return awardedtogroup;
            }

            /**
             * Définit la valeur de la propriété awardedtogroup.
             *
             * @param value allowed object is
             *              {@link Empty }
             */
            public void setAWARDEDTOGROUP(Empty value) {
                this.awardedtogroup = value;
            }

            /**
             * Gets the value of the contractorSmeOpt property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the contractorSmeOpt property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getContractorSmeOpt().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF23 .AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR }
             */
            public List<eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF23.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR> getContractorSmeOpt() {
                if (contractorSmeOpt == null) {
                    contractorSmeOpt = new ArrayList<eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF23.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR>();
                }
                return this.contractorSmeOpt;
            }

            /**
             * Obtient la valeur de la propriété contractor.
             *
             * @return possible object is
             * {@link eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF23 .AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR }
             */
            public eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF23.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR getCONTRACTOR() {
                return contractor;
            }

            /**
             * Définit la valeur de la propriété contractor.
             *
             * @param value allowed object is
             *              {@link eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF23 .AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR }
             */
            public void setCONTRACTOR(eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF23.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR value) {
                this.contractor = value;
            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_man"&gt;
         *       &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}nb_tenders"/&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "nbtendersreceived",
                "nbtendersreceivedsme",
                "nbtendersreceivedothereu",
                "nbtendersreceivednoneu",
                "nbtendersreceivedemeans"
        })
        @ToString(callSuper = true)
        @EqualsAndHashCode(callSuper = true)
        public static class TENDERS
                extends AgreeToPublicationMan {

            @XmlElement(name = "NB_TENDERS_RECEIVED", required = true)
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger nbtendersreceived;
            @XmlElement(name = "NB_TENDERS_RECEIVED_SME")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger nbtendersreceivedsme;
            @XmlElement(name = "NB_TENDERS_RECEIVED_OTHER_EU")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger nbtendersreceivedothereu;
            @XmlElement(name = "NB_TENDERS_RECEIVED_NON_EU")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger nbtendersreceivednoneu;
            @XmlElement(name = "NB_TENDERS_RECEIVED_EMEANS")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger nbtendersreceivedemeans;

            /**
             * Obtient la valeur de la propriété nbtendersreceived.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getNBTENDERSRECEIVED() {
                return nbtendersreceived;
            }

            /**
             * Définit la valeur de la propriété nbtendersreceived.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setNBTENDERSRECEIVED(BigInteger value) {
                this.nbtendersreceived = value;
            }

            /**
             * Obtient la valeur de la propriété nbtendersreceivedsme.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getNBTENDERSRECEIVEDSME() {
                return nbtendersreceivedsme;
            }

            /**
             * Définit la valeur de la propriété nbtendersreceivedsme.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setNBTENDERSRECEIVEDSME(BigInteger value) {
                this.nbtendersreceivedsme = value;
            }

            /**
             * Obtient la valeur de la propriété nbtendersreceivedothereu.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getNBTENDERSRECEIVEDOTHEREU() {
                return nbtendersreceivedothereu;
            }

            /**
             * Définit la valeur de la propriété nbtendersreceivedothereu.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setNBTENDERSRECEIVEDOTHEREU(BigInteger value) {
                this.nbtendersreceivedothereu = value;
            }

            /**
             * Obtient la valeur de la propriété nbtendersreceivednoneu.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getNBTENDERSRECEIVEDNONEU() {
                return nbtendersreceivednoneu;
            }

            /**
             * Définit la valeur de la propriété nbtendersreceivednoneu.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setNBTENDERSRECEIVEDNONEU(BigInteger value) {
                this.nbtendersreceivednoneu = value;
            }

            /**
             * Obtient la valeur de la propriété nbtendersreceivedemeans.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getNBTENDERSRECEIVEDEMEANS() {
                return nbtendersreceivedemeans;
            }

            /**
             * Définit la valeur de la propriété nbtendersreceivedemeans.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setNBTENDERSRECEIVEDEMEANS(BigInteger value) {
                this.nbtendersreceivedemeans = value;
            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_man"&gt;
         *       &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_contract_value"/&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "valestimatedtotal",
                "valtotal",
                "valrangetotal"
        })
        @ToString(callSuper = true)
        @EqualsAndHashCode(callSuper = true)
        public static class VALUES
                extends AgreeToPublicationMan {

            @XmlElement(name = "VAL_ESTIMATED_TOTAL")
            protected Val valestimatedtotal;
            @XmlElement(name = "VAL_TOTAL")
            protected Val valtotal;
            @XmlElement(name = "VAL_RANGE_TOTAL")
            protected ValRange valrangetotal;

            /**
             * Obtient la valeur de la propriété valestimatedtotal.
             *
             * @return possible object is
             * {@link Val }
             */
            public Val getVALESTIMATEDTOTAL() {
                return valestimatedtotal;
            }

            /**
             * Définit la valeur de la propriété valestimatedtotal.
             *
             * @param value allowed object is
             *              {@link Val }
             */
            public void setVALESTIMATEDTOTAL(Val value) {
                this.valestimatedtotal = value;
            }

            /**
             * Obtient la valeur de la propriété valtotal.
             *
             * @return possible object is
             * {@link Val }
             */
            public Val getVALTOTAL() {
                return valtotal;
            }

            /**
             * Définit la valeur de la propriété valtotal.
             *
             * @param value allowed object is
             *              {@link Val }
             */
            public void setVALTOTAL(Val value) {
                this.valtotal = value;
            }

            /**
             * Obtient la valeur de la propriété valrangetotal.
             *
             * @return possible object is
             * {@link ValRange }
             */
            public ValRange getVALRANGETOTAL() {
                return valrangetotal;
            }

            /**
             * Définit la valeur de la propriété valrangetotal.
             *
             * @param value allowed object is
             *              {@link ValRange }
             */
            public void setVALRANGETOTAL(ValRange value) {
                this.valrangetotal = value;
            }

        }

    }

}
