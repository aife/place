//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Section I: CONTRACTING AUTHORITY/ENTITY
 *
 * <p>Classe Java pour body_f08 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="body_f08"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ADDRESS_CONTRACTING_BODY" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contact_buyer"/&gt;
 *         &lt;sequence minOccurs="0"&gt;
 *           &lt;element name="ADDRESS_CONTRACTING_BODY_ADDITIONAL" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contact_buyer" maxOccurs="unbounded"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}JOINT_PROCUREMENT_INVOLVED"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PROCUREMENT_LAW" minOccurs="0"/&gt;
 *         &lt;/sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CENTRAL_PURCHASING" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}document_url_man" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}information" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}URL_TOOL" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}authority"/&gt;
 *           &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}entity"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "body_f08", propOrder = {
        "addresscontractingbody",
        "addresscontractingbodyadditional",
        "jointprocurementinvolved",
        "procurementlaw",
        "centralpurchasing",
        "documentfull",
        "documentrestricted",
        "urldocument",
        "addressfurtherinfoidem",
        "addressfurtherinfo",
        "urltool",
        "catype",
        "catypeother",
        "caactivity",
        "caactivityother",
        "ceactivity",
        "ceactivityother"
})
@ToString
@EqualsAndHashCode
public class BodyF08 {

    @XmlElement(name = "ADDRESS_CONTRACTING_BODY", required = true)
    protected ContactBuyer addresscontractingbody;
    @XmlElement(name = "ADDRESS_CONTRACTING_BODY_ADDITIONAL")
    protected List<ContactBuyer> addresscontractingbodyadditional;
    @XmlElement(name = "JOINT_PROCUREMENT_INVOLVED")
    protected Empty jointprocurementinvolved;
    @XmlElement(name = "PROCUREMENT_LAW")
    protected TextFtMultiLines procurementlaw;
    @XmlElement(name = "CENTRAL_PURCHASING")
    protected Empty centralpurchasing;
    @XmlElement(name = "DOCUMENT_FULL")
    protected Empty documentfull;
    @XmlElement(name = "DOCUMENT_RESTRICTED")
    protected Empty documentrestricted;
    @XmlElement(name = "URL_DOCUMENT")
    protected String urldocument;
    @XmlElement(name = "ADDRESS_FURTHER_INFO_IDEM")
    protected Empty addressfurtherinfoidem;
    @XmlElement(name = "ADDRESS_FURTHER_INFO")
    protected ContactContractingBody addressfurtherinfo;
    @XmlElement(name = "URL_TOOL")
    protected String urltool;
    @XmlElement(name = "CA_TYPE")
    protected CaType catype;
    @XmlElement(name = "CA_TYPE_OTHER")
    protected String catypeother;
    @XmlElement(name = "CA_ACTIVITY")
    protected CaActivity caactivity;
    @XmlElement(name = "CA_ACTIVITY_OTHER")
    protected String caactivityother;
    @XmlElement(name = "CE_ACTIVITY")
    protected CeActivity ceactivity;
    @XmlElement(name = "CE_ACTIVITY_OTHER")
    protected String ceactivityother;

    /**
     * Obtient la valeur de la propriété addresscontractingbody.
     *
     * @return possible object is
     * {@link ContactBuyer }
     */
    public ContactBuyer getADDRESSCONTRACTINGBODY() {
        return addresscontractingbody;
    }

    /**
     * Définit la valeur de la propriété addresscontractingbody.
     *
     * @param value allowed object is
     *              {@link ContactBuyer }
     */
    public void setADDRESSCONTRACTINGBODY(ContactBuyer value) {
        this.addresscontractingbody = value;
    }

    /**
     * Gets the value of the addresscontractingbodyadditional property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addresscontractingbodyadditional property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getADDRESSCONTRACTINGBODYADDITIONAL().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactBuyer }
     */
    public List<ContactBuyer> getADDRESSCONTRACTINGBODYADDITIONAL() {
        if (addresscontractingbodyadditional == null) {
            addresscontractingbodyadditional = new ArrayList<ContactBuyer>();
        }
        return this.addresscontractingbodyadditional;
    }

    /**
     * Obtient la valeur de la propriété jointprocurementinvolved.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getJOINTPROCUREMENTINVOLVED() {
        return jointprocurementinvolved;
    }

    /**
     * Définit la valeur de la propriété jointprocurementinvolved.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setJOINTPROCUREMENTINVOLVED(Empty value) {
        this.jointprocurementinvolved = value;
    }

    /**
     * Obtient la valeur de la propriété procurementlaw.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getPROCUREMENTLAW() {
        return procurementlaw;
    }

    /**
     * Définit la valeur de la propriété procurementlaw.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setPROCUREMENTLAW(TextFtMultiLines value) {
        this.procurementlaw = value;
    }

    /**
     * Obtient la valeur de la propriété centralpurchasing.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getCENTRALPURCHASING() {
        return centralpurchasing;
    }

    /**
     * Définit la valeur de la propriété centralpurchasing.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setCENTRALPURCHASING(Empty value) {
        this.centralpurchasing = value;
    }

    /**
     * Obtient la valeur de la propriété documentfull.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getDOCUMENTFULL() {
        return documentfull;
    }

    /**
     * Définit la valeur de la propriété documentfull.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setDOCUMENTFULL(Empty value) {
        this.documentfull = value;
    }

    /**
     * Obtient la valeur de la propriété documentrestricted.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getDOCUMENTRESTRICTED() {
        return documentrestricted;
    }

    /**
     * Définit la valeur de la propriété documentrestricted.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setDOCUMENTRESTRICTED(Empty value) {
        this.documentrestricted = value;
    }

    /**
     * Obtient la valeur de la propriété urldocument.
     *
     * @return possible object is
     * {@link String }
     */
    public String getURLDOCUMENT() {
        return urldocument;
    }

    /**
     * Définit la valeur de la propriété urldocument.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setURLDOCUMENT(String value) {
        this.urldocument = value;
    }

    /**
     * Obtient la valeur de la propriété addressfurtherinfoidem.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getADDRESSFURTHERINFOIDEM() {
        return addressfurtherinfoidem;
    }

    /**
     * Définit la valeur de la propriété addressfurtherinfoidem.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setADDRESSFURTHERINFOIDEM(Empty value) {
        this.addressfurtherinfoidem = value;
    }

    /**
     * Obtient la valeur de la propriété addressfurtherinfo.
     *
     * @return possible object is
     * {@link ContactContractingBody }
     */
    public ContactContractingBody getADDRESSFURTHERINFO() {
        return addressfurtherinfo;
    }

    /**
     * Définit la valeur de la propriété addressfurtherinfo.
     *
     * @param value allowed object is
     *              {@link ContactContractingBody }
     */
    public void setADDRESSFURTHERINFO(ContactContractingBody value) {
        this.addressfurtherinfo = value;
    }

    /**
     * Obtient la valeur de la propriété urltool.
     *
     * @return possible object is
     * {@link String }
     */
    public String getURLTOOL() {
        return urltool;
    }

    /**
     * Définit la valeur de la propriété urltool.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setURLTOOL(String value) {
        this.urltool = value;
    }

    /**
     * Obtient la valeur de la propriété catype.
     *
     * @return possible object is
     * {@link CaType }
     */
    public CaType getCATYPE() {
        return catype;
    }

    /**
     * Définit la valeur de la propriété catype.
     *
     * @param value allowed object is
     *              {@link CaType }
     */
    public void setCATYPE(CaType value) {
        this.catype = value;
    }

    /**
     * Obtient la valeur de la propriété catypeother.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCATYPEOTHER() {
        return catypeother;
    }

    /**
     * Définit la valeur de la propriété catypeother.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCATYPEOTHER(String value) {
        this.catypeother = value;
    }

    /**
     * Obtient la valeur de la propriété caactivity.
     *
     * @return possible object is
     * {@link CaActivity }
     */
    public CaActivity getCAACTIVITY() {
        return caactivity;
    }

    /**
     * Définit la valeur de la propriété caactivity.
     *
     * @param value allowed object is
     *              {@link CaActivity }
     */
    public void setCAACTIVITY(CaActivity value) {
        this.caactivity = value;
    }

    /**
     * Obtient la valeur de la propriété caactivityother.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCAACTIVITYOTHER() {
        return caactivityother;
    }

    /**
     * Définit la valeur de la propriété caactivityother.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCAACTIVITYOTHER(String value) {
        this.caactivityother = value;
    }

    /**
     * Obtient la valeur de la propriété ceactivity.
     *
     * @return possible object is
     * {@link CeActivity }
     */
    public CeActivity getCEACTIVITY() {
        return ceactivity;
    }

    /**
     * Définit la valeur de la propriété ceactivity.
     *
     * @param value allowed object is
     *              {@link CeActivity }
     */
    public void setCEACTIVITY(CeActivity value) {
        this.ceactivity = value;
    }

    /**
     * Obtient la valeur de la propriété ceactivityother.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCEACTIVITYOTHER() {
        return ceactivityother;
    }

    /**
     * Définit la valeur de la propriété ceactivityother.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCEACTIVITYOTHER(String value) {
        this.ceactivityother = value;
    }

}
