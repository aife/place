//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Section IV: PROCEDURE
 *
 * <p>Classe Java pour procedure_f23 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="procedure_f23"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;sequence&gt;
 *           &lt;choice&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_AWARD_CONTRACT_WITH_PRIOR_PUBLICATION"/&gt;
 *             &lt;element name="PT_AWARD_CONTRACT_WITHOUT_PUBLICATION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d4"/&gt;
 *           &lt;/choice&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}MAIN_FEATURES_AWARD" minOccurs="0"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NOTICE_NUMBER_OJ" minOccurs="0"/&gt;
 *         &lt;/sequence&gt;
 *         &lt;sequence&gt;
 *           &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}receipt_tenders"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LANGUAGES"/&gt;
 *         &lt;/sequence&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "procedure_f23", propOrder = {
        "ptawardcontractwithpriorpublication",
        "ptawardcontractwithoutpublication",
        "mainfeaturesaward",
        "noticenumberoj",
        "datereceipttenders",
        "timereceipttenders",
        "languages"
})
@ToString
@EqualsAndHashCode
public class ProcedureF23 {

    @XmlElement(name = "PT_AWARD_CONTRACT_WITH_PRIOR_PUBLICATION")
    protected Empty ptawardcontractwithpriorpublication;
    @XmlElement(name = "PT_AWARD_CONTRACT_WITHOUT_PUBLICATION")
    protected AnnexD4 ptawardcontractwithoutpublication;
    @XmlElement(name = "MAIN_FEATURES_AWARD")
    protected TextFtMultiLines mainfeaturesaward;
    @XmlElement(name = "NOTICE_NUMBER_OJ")
    protected String noticenumberoj;
    @XmlElement(name = "DATE_RECEIPT_TENDERS")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datereceipttenders;
    @XmlElement(name = "TIME_RECEIPT_TENDERS")
    protected String timereceipttenders;
    @XmlElement(name = "LANGUAGES")
    protected LANGUAGES languages;

    /**
     * Obtient la valeur de la propriété ptawardcontractwithpriorpublication.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTAWARDCONTRACTWITHPRIORPUBLICATION() {
        return ptawardcontractwithpriorpublication;
    }

    /**
     * Définit la valeur de la propriété ptawardcontractwithpriorpublication.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTAWARDCONTRACTWITHPRIORPUBLICATION(Empty value) {
        this.ptawardcontractwithpriorpublication = value;
    }

    /**
     * Obtient la valeur de la propriété ptawardcontractwithoutpublication.
     *
     * @return possible object is
     * {@link AnnexD4 }
     */
    public AnnexD4 getPTAWARDCONTRACTWITHOUTPUBLICATION() {
        return ptawardcontractwithoutpublication;
    }

    /**
     * Définit la valeur de la propriété ptawardcontractwithoutpublication.
     *
     * @param value allowed object is
     *              {@link AnnexD4 }
     */
    public void setPTAWARDCONTRACTWITHOUTPUBLICATION(AnnexD4 value) {
        this.ptawardcontractwithoutpublication = value;
    }

    /**
     * Obtient la valeur de la propriété mainfeaturesaward.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getMAINFEATURESAWARD() {
        return mainfeaturesaward;
    }

    /**
     * Définit la valeur de la propriété mainfeaturesaward.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setMAINFEATURESAWARD(TextFtMultiLines value) {
        this.mainfeaturesaward = value;
    }

    /**
     * Obtient la valeur de la propriété noticenumberoj.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNOTICENUMBEROJ() {
        return noticenumberoj;
    }

    /**
     * Définit la valeur de la propriété noticenumberoj.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNOTICENUMBEROJ(String value) {
        this.noticenumberoj = value;
    }

    /**
     * Obtient la valeur de la propriété datereceipttenders.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDATERECEIPTTENDERS() {
        return datereceipttenders;
    }

    /**
     * Définit la valeur de la propriété datereceipttenders.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDATERECEIPTTENDERS(XMLGregorianCalendar value) {
        this.datereceipttenders = value;
    }

    /**
     * Obtient la valeur de la propriété timereceipttenders.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTIMERECEIPTTENDERS() {
        return timereceipttenders;
    }

    /**
     * Définit la valeur de la propriété timereceipttenders.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTIMERECEIPTTENDERS(String value) {
        this.timereceipttenders = value;
    }

    /**
     * Obtient la valeur de la propriété languages.
     *
     * @return possible object is
     * {@link LANGUAGES }
     */
    public LANGUAGES getLANGUAGES() {
        return languages;
    }

    /**
     * Définit la valeur de la propriété languages.
     *
     * @param value allowed object is
     *              {@link LANGUAGES }
     */
    public void setLANGUAGES(LANGUAGES value) {
        this.languages = value;
    }

}
