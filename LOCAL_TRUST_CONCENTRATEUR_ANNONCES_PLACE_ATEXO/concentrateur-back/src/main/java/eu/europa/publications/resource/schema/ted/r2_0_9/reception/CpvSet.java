//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java pour cpv_set complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="cpv_set"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CPV_CODE"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CPV_SUPPLEMENTARY_CODE" maxOccurs="20" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cpv_set", propOrder = {
        "cpvcode",
        "cpvsupplementarycode"
})
@ToString
@EqualsAndHashCode
public class CpvSet {

    @XmlElement(name = "CPV_CODE", required = true)
    protected CpvCodes cpvcode;
    @XmlElement(name = "CPV_SUPPLEMENTARY_CODE")
    protected List<CpvSupplementaryCodes> cpvsupplementarycode;

    /**
     * Obtient la valeur de la propriété cpvcode.
     *
     * @return possible object is
     * {@link CpvCodes }
     */
    public CpvCodes getCPVCODE() {
        return cpvcode;
    }

    /**
     * Définit la valeur de la propriété cpvcode.
     *
     * @param value allowed object is
     *              {@link CpvCodes }
     */
    public void setCPVCODE(CpvCodes value) {
        this.cpvcode = value;
    }

    /**
     * Gets the value of the cpvsupplementarycode property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cpvsupplementarycode property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCPVSUPPLEMENTARYCODE().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CpvSupplementaryCodes }
     */
    public List<CpvSupplementaryCodes> getCPVSUPPLEMENTARYCODE() {
        if (cpvsupplementarycode == null) {
            cpvsupplementarycode = new ArrayList<CpvSupplementaryCodes>();
        }
        return this.cpvsupplementarycode;
    }

}
