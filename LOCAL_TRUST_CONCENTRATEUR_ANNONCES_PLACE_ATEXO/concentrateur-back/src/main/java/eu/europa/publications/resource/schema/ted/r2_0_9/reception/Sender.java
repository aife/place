//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour sender complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="sender"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IDENTIFICATION"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ESENDER_LOGIN"/&gt;
 *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CUSTOMER_LOGIN" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NO_DOC_EXT"/&gt;
 *                   &lt;element name="SOFTWARE_VERSION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}string_50" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CONTACT"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ORGANISATION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}string_300"/&gt;
 *                   &lt;element name="COUNTRY"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;attribute name="VALUE" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}t_country_list_eu_union" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PHONE" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}E_MAIL"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DATE_EXPECTED_PUBLICATION" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sender", propOrder = {
        "identification",
        "contact",
        "dateexpectedpublication"
})
@ToString
@EqualsAndHashCode
public class Sender {

    @XmlElement(name = "IDENTIFICATION", required = true)
    protected Sender.IDENTIFICATION identification;
    @XmlElement(name = "CONTACT", required = true)
    protected Sender.CONTACT contact;
    @XmlElement(name = "DATE_EXPECTED_PUBLICATION")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateexpectedpublication;

    /**
     * Obtient la valeur de la propriété identification.
     *
     * @return possible object is
     * {@link Sender.IDENTIFICATION }
     */
    public Sender.IDENTIFICATION getIDENTIFICATION() {
        return identification;
    }

    /**
     * Définit la valeur de la propriété identification.
     *
     * @param value allowed object is
     *              {@link Sender.IDENTIFICATION }
     */
    public void setIDENTIFICATION(Sender.IDENTIFICATION value) {
        this.identification = value;
    }

    /**
     * Obtient la valeur de la propriété contact.
     *
     * @return possible object is
     * {@link Sender.CONTACT }
     */
    public Sender.CONTACT getCONTACT() {
        return contact;
    }

    /**
     * Définit la valeur de la propriété contact.
     *
     * @param value allowed object is
     *              {@link Sender.CONTACT }
     */
    public void setCONTACT(Sender.CONTACT value) {
        this.contact = value;
    }

    /**
     * used only for DEVCO forms
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDATEEXPECTEDPUBLICATION() {
        return dateexpectedpublication;
    }

    /**
     * Définit la valeur de la propriété dateexpectedpublication.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDATEEXPECTEDPUBLICATION(XMLGregorianCalendar value) {
        this.dateexpectedpublication = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ORGANISATION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}string_300"/&gt;
     *         &lt;element name="COUNTRY"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;attribute name="VALUE" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}t_country_list_eu_union" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PHONE" minOccurs="0"/&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}E_MAIL"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "organisation",
            "country",
            "phone",
            "email"
    })
    @ToString
    @EqualsAndHashCode
    public static class CONTACT {

        @XmlElement(name = "ORGANISATION", required = true)
        protected String organisation;
        @XmlElement(name = "COUNTRY", required = true)
        protected Sender.CONTACT.COUNTRY country;
        @XmlElement(name = "PHONE")
        protected String phone;
        @XmlElement(name = "E_MAIL", required = true)
        protected String email;

        /**
         * Obtient la valeur de la propriété organisation.
         *
         * @return possible object is
         * {@link String }
         */
        public String getORGANISATION() {
            return organisation;
        }

        /**
         * Définit la valeur de la propriété organisation.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setORGANISATION(String value) {
            this.organisation = value;
        }

        /**
         * Obtient la valeur de la propriété country.
         *
         * @return possible object is
         * {@link Sender.CONTACT.COUNTRY }
         */
        public Sender.CONTACT.COUNTRY getCOUNTRY() {
            return country;
        }

        /**
         * Définit la valeur de la propriété country.
         *
         * @param value allowed object is
         *              {@link Sender.CONTACT.COUNTRY }
         */
        public void setCOUNTRY(Sender.CONTACT.COUNTRY value) {
            this.country = value;
        }

        /**
         * Obtient la valeur de la propriété phone.
         *
         * @return possible object is
         * {@link String }
         */
        public String getPHONE() {
            return phone;
        }

        /**
         * Définit la valeur de la propriété phone.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setPHONE(String value) {
            this.phone = value;
        }

        /**
         * Obtient la valeur de la propriété email.
         *
         * @return possible object is
         * {@link String }
         */
        public String getEMAIL() {
            return email;
        }

        /**
         * Définit la valeur de la propriété email.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setEMAIL(String value) {
            this.email = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;attribute name="VALUE" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}t_country_list_eu_union" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        @ToString
        @EqualsAndHashCode
        public static class COUNTRY {

            @XmlAttribute(name = "VALUE", required = true)
            protected String value;

            /**
             * Obtient la valeur de la propriété value.
             *
             * @return possible object is
             * {@link String }
             */
            public String getVALUE() {
                return value;
            }

            /**
             * Définit la valeur de la propriété value.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setVALUE(String value) {
                this.value = value;
            }

        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ESENDER_LOGIN"/&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CUSTOMER_LOGIN" minOccurs="0"/&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NO_DOC_EXT"/&gt;
     *         &lt;element name="SOFTWARE_VERSION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}string_50" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "esenderlogin",
            "customerlogin",
            "nodocext",
            "softwareversion"
    })
    @ToString
    @EqualsAndHashCode
    public static class IDENTIFICATION {

        @XmlElement(name = "ESENDER_LOGIN", required = true)
        protected String esenderlogin;
        @XmlElement(name = "CUSTOMER_LOGIN")
        protected String customerlogin;
        @XmlElement(name = "NO_DOC_EXT", required = true)
        protected String nodocext;
        @XmlElement(name = "SOFTWARE_VERSION")
        protected String softwareversion;

        /**
         * Obtient la valeur de la propriété esenderlogin.
         *
         * @return possible object is
         * {@link String }
         */
        public String getESENDERLOGIN() {
            return esenderlogin;
        }

        /**
         * Définit la valeur de la propriété esenderlogin.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setESENDERLOGIN(String value) {
            this.esenderlogin = value;
        }

        /**
         * Obtient la valeur de la propriété customerlogin.
         *
         * @return possible object is
         * {@link String }
         */
        public String getCUSTOMERLOGIN() {
            return customerlogin;
        }

        /**
         * Définit la valeur de la propriété customerlogin.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setCUSTOMERLOGIN(String value) {
            this.customerlogin = value;
        }

        /**
         * Obtient la valeur de la propriété nodocext.
         *
         * @return possible object is
         * {@link String }
         */
        public String getNODOCEXT() {
            return nodocext;
        }

        /**
         * Définit la valeur de la propriété nodocext.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setNODOCEXT(String value) {
            this.nodocext = value;
        }

        /**
         * Obtient la valeur de la propriété softwareversion.
         *
         * @return possible object is
         * {@link String }
         */
        public String getSOFTWAREVERSION() {
            return softwareversion;
        }

        /**
         * Définit la valeur de la propriété softwareversion.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setSOFTWAREVERSION(String value) {
            this.softwareversion = value;
        }

    }

}
