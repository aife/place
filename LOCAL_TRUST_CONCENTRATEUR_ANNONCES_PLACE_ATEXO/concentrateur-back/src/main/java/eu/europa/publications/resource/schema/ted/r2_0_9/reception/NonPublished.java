//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java pour non_published complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="non_published"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="PUBLICATION" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="NO" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "non_published")
@XmlSeeAlso({
        eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF06.AWARDEDCONTRACT.COUNTRYORIGIN.class
})
@ToString
@EqualsAndHashCode
public class NonPublished {

    @XmlAttribute(name = "PUBLICATION", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String publication;

    /**
     * Obtient la valeur de la propriété publication.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPUBLICATION() {
        if (publication == null) {
            return "NO";
        } else {
            return publication;
        }
    }

    /**
     * Définit la valeur de la propriété publication.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPUBLICATION(String value) {
        this.publication = value;
    }

}
