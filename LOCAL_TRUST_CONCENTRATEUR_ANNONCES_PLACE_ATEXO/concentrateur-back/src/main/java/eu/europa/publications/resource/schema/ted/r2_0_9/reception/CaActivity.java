//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour ca_activity complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="ca_activity"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="VALUE" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;enumeration value="DEFENCE"/&gt;
 *             &lt;enumeration value="ECONOMIC_AND_FINANCIAL_AFFAIRS"/&gt;
 *             &lt;enumeration value="EDUCATION"/&gt;
 *             &lt;enumeration value="ENVIRONMENT"/&gt;
 *             &lt;enumeration value="GENERAL_PUBLIC_SERVICES"/&gt;
 *             &lt;enumeration value="HEALTH"/&gt;
 *             &lt;enumeration value="HOUSING_AND_COMMUNITY_AMENITIES"/&gt;
 *             &lt;enumeration value="PUBLIC_ORDER_AND_SAFETY"/&gt;
 *             &lt;enumeration value="RECREATION_CULTURE_AND_RELIGION"/&gt;
 *             &lt;enumeration value="SOCIAL_PROTECTION"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ca_activity")
@ToString
@EqualsAndHashCode
public class CaActivity {

    @XmlAttribute(name = "VALUE", required = true)
    protected String value;

    /**
     * Obtient la valeur de la propriété value.
     *
     * @return possible object is
     * {@link String }
     */
    public String getVALUE() {
        return value;
    }

    /**
     * Définit la valeur de la propriété value.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setVALUE(String value) {
        this.value = value;
    }

}
