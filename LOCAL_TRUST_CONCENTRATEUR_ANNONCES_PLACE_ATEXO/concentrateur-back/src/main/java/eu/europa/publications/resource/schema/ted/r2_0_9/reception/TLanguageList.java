//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour t_language_list.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * <pre>
 * &lt;simpleType name="t_language_list"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="AR"/&gt;
 *     &lt;enumeration value="BG"/&gt;
 *     &lt;enumeration value="BR"/&gt;
 *     &lt;enumeration value="CA"/&gt;
 *     &lt;enumeration value="CO"/&gt;
 *     &lt;enumeration value="CS"/&gt;
 *     &lt;enumeration value="CY"/&gt;
 *     &lt;enumeration value="DA"/&gt;
 *     &lt;enumeration value="DE"/&gt;
 *     &lt;enumeration value="EL"/&gt;
 *     &lt;enumeration value="EN"/&gt;
 *     &lt;enumeration value="ES"/&gt;
 *     &lt;enumeration value="ET"/&gt;
 *     &lt;enumeration value="EU"/&gt;
 *     &lt;enumeration value="FI"/&gt;
 *     &lt;enumeration value="FO"/&gt;
 *     &lt;enumeration value="FR"/&gt;
 *     &lt;enumeration value="FY"/&gt;
 *     &lt;enumeration value="GA"/&gt;
 *     &lt;enumeration value="GD"/&gt;
 *     &lt;enumeration value="GL"/&gt;
 *     &lt;enumeration value="HE"/&gt;
 *     &lt;enumeration value="HR"/&gt;
 *     &lt;enumeration value="HU"/&gt;
 *     &lt;enumeration value="HY"/&gt;
 *     &lt;enumeration value="IS"/&gt;
 *     &lt;enumeration value="IT"/&gt;
 *     &lt;enumeration value="JA"/&gt;
 *     &lt;enumeration value="KL"/&gt;
 *     &lt;enumeration value="KO"/&gt;
 *     &lt;enumeration value="KU"/&gt;
 *     &lt;enumeration value="LB"/&gt;
 *     &lt;enumeration value="LT"/&gt;
 *     &lt;enumeration value="LV"/&gt;
 *     &lt;enumeration value="ME"/&gt;
 *     &lt;enumeration value="MK"/&gt;
 *     &lt;enumeration value="MS"/&gt;
 *     &lt;enumeration value="MT"/&gt;
 *     &lt;enumeration value="NL"/&gt;
 *     &lt;enumeration value="NO"/&gt;
 *     &lt;enumeration value="OC"/&gt;
 *     &lt;enumeration value="PL"/&gt;
 *     &lt;enumeration value="PT"/&gt;
 *     &lt;enumeration value="RM"/&gt;
 *     &lt;enumeration value="RO"/&gt;
 *     &lt;enumeration value="SE"/&gt;
 *     &lt;enumeration value="SK"/&gt;
 *     &lt;enumeration value="SL"/&gt;
 *     &lt;enumeration value="SQ"/&gt;
 *     &lt;enumeration value="SR"/&gt;
 *     &lt;enumeration value="SV"/&gt;
 *     &lt;enumeration value="TA"/&gt;
 *     &lt;enumeration value="TR"/&gt;
 *     &lt;enumeration value="WA"/&gt;
 *     &lt;enumeration value="ZH"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "t_language_list")
@XmlEnum
public enum TLanguageList {

    AR,
    BG,
    BR,
    CA,
    CO,
    CS,
    CY,
    DA,
    DE,
    EL,
    EN,
    ES,
    ET,
    EU,
    FI,
    FO,
    FR,
    FY,
    GA,
    GD,
    GL,
    HE,
    HR,
    HU,
    HY,
    IS,
    IT,
    JA,
    KL,
    KO,
    KU,
    LB,
    LT,
    LV,
    ME,
    MK,
    MS,
    MT,
    NL,
    NO,
    OC,
    PL,
    PT,
    RM,
    RO,
    SE,
    SK,
    SL,
    SQ,
    SR,
    SV,
    TA,
    TR,
    WA,
    ZH;

    public static TLanguageList fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
