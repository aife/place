//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


/**
 * Section V: AWARD OF CONTRACT
 *
 * <p>Classe Java pour award_contract_f23 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="award_contract_f23"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LOT_NO" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TITLE" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NO_AWARDED_CONTRACT"/&gt;
 *           &lt;element name="AWARDED_CONTRACT"&gt;
 *             &lt;complexType&gt;
 *               &lt;complexContent&gt;
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                   &lt;sequence&gt;
 *                     &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DATE_CONCLUSION_CONTRACT" minOccurs="0"/&gt;
 *                     &lt;element name="TENDERS"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}nb_tenders"/&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;element name="CONTRACTORS"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;choice&gt;
 *                               &lt;sequence&gt;
 *                                 &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}AWARDED_TO_GROUP"/&gt;
 *                                 &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contractor_sme_opt" maxOccurs="100" minOccurs="2"/&gt;
 *                               &lt;/sequence&gt;
 *                               &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contractor_sme_opt"/&gt;
 *                             &lt;/choice&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;element name="VALUES"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;sequence&gt;
 *                               &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}VAL_ESTIMATED_TOTAL" minOccurs="0"/&gt;
 *                               &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}VAL_TOTAL"/&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}val_concession"/&gt;
 *                   &lt;/sequence&gt;
 *                 &lt;/restriction&gt;
 *               &lt;/complexContent&gt;
 *             &lt;/complexType&gt;
 *           &lt;/element&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ITEM" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}nb_contract" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "award_contract_f23", propOrder = {
        "lotno",
        "title",
        "noawardedcontract",
        "awardedcontract"
})
@ToString
@EqualsAndHashCode
public class AwardContractF23 {

    @XmlElement(name = "LOT_NO")
    protected String lotno;
    @XmlElement(name = "TITLE")
    protected TextFtSingleLine title;
    @XmlElement(name = "NO_AWARDED_CONTRACT")
    protected NoAward noawardedcontract;
    @XmlElement(name = "AWARDED_CONTRACT")
    protected AwardContractF23.AWARDEDCONTRACT awardedcontract;
    @XmlAttribute(name = "ITEM", required = true)
    protected int item;

    /**
     * Obtient la valeur de la propriété lotno.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLOTNO() {
        return lotno;
    }

    /**
     * Définit la valeur de la propriété lotno.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLOTNO(String value) {
        this.lotno = value;
    }

    /**
     * Obtient la valeur de la propriété title.
     *
     * @return possible object is
     * {@link TextFtSingleLine }
     */
    public TextFtSingleLine getTITLE() {
        return title;
    }

    /**
     * Définit la valeur de la propriété title.
     *
     * @param value allowed object is
     *              {@link TextFtSingleLine }
     */
    public void setTITLE(TextFtSingleLine value) {
        this.title = value;
    }

    /**
     * Obtient la valeur de la propriété noawardedcontract.
     *
     * @return possible object is
     * {@link NoAward }
     */
    public NoAward getNOAWARDEDCONTRACT() {
        return noawardedcontract;
    }

    /**
     * Définit la valeur de la propriété noawardedcontract.
     *
     * @param value allowed object is
     *              {@link NoAward }
     */
    public void setNOAWARDEDCONTRACT(NoAward value) {
        this.noawardedcontract = value;
    }

    /**
     * Obtient la valeur de la propriété awardedcontract.
     *
     * @return possible object is
     * {@link AwardContractF23 .AWARDEDCONTRACT }
     */
    public AwardContractF23.AWARDEDCONTRACT getAWARDEDCONTRACT() {
        return awardedcontract;
    }

    /**
     * Définit la valeur de la propriété awardedcontract.
     *
     * @param value allowed object is
     *              {@link AwardContractF23 .AWARDEDCONTRACT }
     */
    public void setAWARDEDCONTRACT(AwardContractF23.AWARDEDCONTRACT value) {
        this.awardedcontract = value;
    }

    /**
     * Obtient la valeur de la propriété item.
     */
    public int getITEM() {
        return item;
    }

    /**
     * Définit la valeur de la propriété item.
     */
    public void setITEM(int value) {
        this.item = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DATE_CONCLUSION_CONTRACT" minOccurs="0"/&gt;
     *         &lt;element name="TENDERS"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}nb_tenders"/&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="CONTRACTORS"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;choice&gt;
     *                   &lt;sequence&gt;
     *                     &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}AWARDED_TO_GROUP"/&gt;
     *                     &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contractor_sme_opt" maxOccurs="100" minOccurs="2"/&gt;
     *                   &lt;/sequence&gt;
     *                   &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contractor_sme_opt"/&gt;
     *                 &lt;/choice&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="VALUES"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}VAL_ESTIMATED_TOTAL" minOccurs="0"/&gt;
     *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}VAL_TOTAL"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}val_concession"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "dateconclusioncontract",
            "tenders",
            "contractors",
            "values",
            "valrevenue",
            "valpricepayment",
            "infoaddvalue"
    })
    @ToString
    @EqualsAndHashCode
    public static class AWARDEDCONTRACT {

        @XmlElement(name = "DATE_CONCLUSION_CONTRACT")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dateconclusioncontract;
        @XmlElement(name = "TENDERS", required = true)
        protected AwardContractF23.AWARDEDCONTRACT.TENDERS tenders;
        @XmlElement(name = "CONTRACTORS", required = true)
        protected AwardContractF23.AWARDEDCONTRACT.CONTRACTORS contractors;
        @XmlElement(name = "VALUES", required = true)
        protected AwardContractF23.AWARDEDCONTRACT.VALUES values;
        @XmlElement(name = "VAL_REVENUE")
        protected Val valrevenue;
        @XmlElement(name = "VAL_PRICE_PAYMENT")
        protected Val valpricepayment;
        @XmlElement(name = "INFO_ADD_VALUE")
        protected TextFtMultiLines infoaddvalue;

        /**
         * Obtient la valeur de la propriété dateconclusioncontract.
         *
         * @return possible object is
         * {@link XMLGregorianCalendar }
         */
        public XMLGregorianCalendar getDATECONCLUSIONCONTRACT() {
            return dateconclusioncontract;
        }

        /**
         * Définit la valeur de la propriété dateconclusioncontract.
         *
         * @param value allowed object is
         *              {@link XMLGregorianCalendar }
         */
        public void setDATECONCLUSIONCONTRACT(XMLGregorianCalendar value) {
            this.dateconclusioncontract = value;
        }

        /**
         * Obtient la valeur de la propriété tenders.
         *
         * @return possible object is
         * {@link AwardContractF23 .AWARDEDCONTRACT.TENDERS }
         */
        public AwardContractF23.AWARDEDCONTRACT.TENDERS getTENDERS() {
            return tenders;
        }

        /**
         * Définit la valeur de la propriété tenders.
         *
         * @param value allowed object is
         *              {@link AwardContractF23 .AWARDEDCONTRACT.TENDERS }
         */
        public void setTENDERS(AwardContractF23.AWARDEDCONTRACT.TENDERS value) {
            this.tenders = value;
        }

        /**
         * Obtient la valeur de la propriété contractors.
         *
         * @return possible object is
         * {@link AwardContractF23 .AWARDEDCONTRACT.CONTRACTORS }
         */
        public AwardContractF23.AWARDEDCONTRACT.CONTRACTORS getCONTRACTORS() {
            return contractors;
        }

        /**
         * Définit la valeur de la propriété contractors.
         *
         * @param value allowed object is
         *              {@link AwardContractF23 .AWARDEDCONTRACT.CONTRACTORS }
         */
        public void setCONTRACTORS(AwardContractF23.AWARDEDCONTRACT.CONTRACTORS value) {
            this.contractors = value;
        }

        /**
         * Obtient la valeur de la propriété values.
         *
         * @return possible object is
         * {@link AwardContractF23 .AWARDEDCONTRACT.VALUES }
         */
        public AwardContractF23.AWARDEDCONTRACT.VALUES getVALUES() {
            return values;
        }

        /**
         * Définit la valeur de la propriété values.
         *
         * @param value allowed object is
         *              {@link AwardContractF23 .AWARDEDCONTRACT.VALUES }
         */
        public void setVALUES(AwardContractF23.AWARDEDCONTRACT.VALUES value) {
            this.values = value;
        }

        /**
         * Obtient la valeur de la propriété valrevenue.
         *
         * @return possible object is
         * {@link Val }
         */
        public Val getVALREVENUE() {
            return valrevenue;
        }

        /**
         * Définit la valeur de la propriété valrevenue.
         *
         * @param value allowed object is
         *              {@link Val }
         */
        public void setVALREVENUE(Val value) {
            this.valrevenue = value;
        }

        /**
         * Obtient la valeur de la propriété valpricepayment.
         *
         * @return possible object is
         * {@link Val }
         */
        public Val getVALPRICEPAYMENT() {
            return valpricepayment;
        }

        /**
         * Définit la valeur de la propriété valpricepayment.
         *
         * @param value allowed object is
         *              {@link Val }
         */
        public void setVALPRICEPAYMENT(Val value) {
            this.valpricepayment = value;
        }

        /**
         * Obtient la valeur de la propriété infoaddvalue.
         *
         * @return possible object is
         * {@link TextFtMultiLines }
         */
        public TextFtMultiLines getINFOADDVALUE() {
            return infoaddvalue;
        }

        /**
         * Définit la valeur de la propriété infoaddvalue.
         *
         * @param value allowed object is
         *              {@link TextFtMultiLines }
         */
        public void setINFOADDVALUE(TextFtMultiLines value) {
            this.infoaddvalue = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;choice&gt;
         *         &lt;sequence&gt;
         *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}AWARDED_TO_GROUP"/&gt;
         *           &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contractor_sme_opt" maxOccurs="100" minOccurs="2"/&gt;
         *         &lt;/sequence&gt;
         *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contractor_sme_opt"/&gt;
         *       &lt;/choice&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "awardedtogroup",
                "contractorSmeOpt",
                "contractor"
        })
        @ToString
        @EqualsAndHashCode
        public static class CONTRACTORS {

            @XmlElement(name = "AWARDED_TO_GROUP")
            protected Empty awardedtogroup;
            @XmlElement(name = "CONTRACTOR")
            protected List<AwardContractF23.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR> contractorSmeOpt;
            @XmlElement(name = "CONTRACTOR")
            protected AwardContractF23.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR contractor;

            /**
             * Obtient la valeur de la propriété awardedtogroup.
             *
             * @return possible object is
             * {@link Empty }
             */
            public Empty getAWARDEDTOGROUP() {
                return awardedtogroup;
            }

            /**
             * Définit la valeur de la propriété awardedtogroup.
             *
             * @param value allowed object is
             *              {@link Empty }
             */
            public void setAWARDEDTOGROUP(Empty value) {
                this.awardedtogroup = value;
            }

            /**
             * Gets the value of the contractorSmeOpt property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the contractorSmeOpt property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getContractorSmeOpt().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link AwardContractF23 .AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR }
             */
            public List<AwardContractF23.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR> getContractorSmeOpt() {
                if (contractorSmeOpt == null) {
                    contractorSmeOpt = new ArrayList<AwardContractF23.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR>();
                }
                return this.contractorSmeOpt;
            }

            /**
             * Obtient la valeur de la propriété contractor.
             *
             * @return possible object is
             * {@link AwardContractF23 .AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR }
             */
            public AwardContractF23.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR getCONTRACTOR() {
                return contractor;
            }

            /**
             * Définit la valeur de la propriété contractor.
             *
             * @param value allowed object is
             *              {@link AwardContractF23 .AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR }
             */
            public void setCONTRACTOR(AwardContractF23.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR value) {
                this.contractor = value;
            }


            /**
             * <p>Classe Java pour anonymous complex type.
             *
             * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ADDRESS_CONTRACTOR"/&gt;
             *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SME" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "addresscontractor",
                    "sme"
            })
            @ToString
            @EqualsAndHashCode
            public static class CONTRACTOR {

                @XmlElement(name = "ADDRESS_CONTRACTOR", required = true)
                protected ContactContractor addresscontractor;
                @XmlElement(name = "SME")
                protected Empty sme;

                /**
                 * Obtient la valeur de la propriété addresscontractor.
                 *
                 * @return possible object is
                 * {@link ContactContractor }
                 */
                public ContactContractor getADDRESSCONTRACTOR() {
                    return addresscontractor;
                }

                /**
                 * Définit la valeur de la propriété addresscontractor.
                 *
                 * @param value allowed object is
                 *              {@link ContactContractor }
                 */
                public void setADDRESSCONTRACTOR(ContactContractor value) {
                    this.addresscontractor = value;
                }

                /**
                 * Obtient la valeur de la propriété sme.
                 *
                 * @return possible object is
                 * {@link Empty }
                 */
                public Empty getSME() {
                    return sme;
                }

                /**
                 * Définit la valeur de la propriété sme.
                 *
                 * @param value allowed object is
                 *              {@link Empty }
                 */
                public void setSME(Empty value) {
                    this.sme = value;
                }

            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}nb_tenders"/&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "nbtendersreceived",
                "nbtendersreceivedsme",
                "nbtendersreceivedothereu",
                "nbtendersreceivednoneu",
                "nbtendersreceivedemeans"
        })
        @ToString
        @EqualsAndHashCode
        public static class TENDERS {

            @XmlElement(name = "NB_TENDERS_RECEIVED", required = true)
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger nbtendersreceived;
            @XmlElement(name = "NB_TENDERS_RECEIVED_SME")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger nbtendersreceivedsme;
            @XmlElement(name = "NB_TENDERS_RECEIVED_OTHER_EU")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger nbtendersreceivedothereu;
            @XmlElement(name = "NB_TENDERS_RECEIVED_NON_EU")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger nbtendersreceivednoneu;
            @XmlElement(name = "NB_TENDERS_RECEIVED_EMEANS")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger nbtendersreceivedemeans;

            /**
             * Obtient la valeur de la propriété nbtendersreceived.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getNBTENDERSRECEIVED() {
                return nbtendersreceived;
            }

            /**
             * Définit la valeur de la propriété nbtendersreceived.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setNBTENDERSRECEIVED(BigInteger value) {
                this.nbtendersreceived = value;
            }

            /**
             * Obtient la valeur de la propriété nbtendersreceivedsme.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getNBTENDERSRECEIVEDSME() {
                return nbtendersreceivedsme;
            }

            /**
             * Définit la valeur de la propriété nbtendersreceivedsme.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setNBTENDERSRECEIVEDSME(BigInteger value) {
                this.nbtendersreceivedsme = value;
            }

            /**
             * Obtient la valeur de la propriété nbtendersreceivedothereu.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getNBTENDERSRECEIVEDOTHEREU() {
                return nbtendersreceivedothereu;
            }

            /**
             * Définit la valeur de la propriété nbtendersreceivedothereu.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setNBTENDERSRECEIVEDOTHEREU(BigInteger value) {
                this.nbtendersreceivedothereu = value;
            }

            /**
             * Obtient la valeur de la propriété nbtendersreceivednoneu.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getNBTENDERSRECEIVEDNONEU() {
                return nbtendersreceivednoneu;
            }

            /**
             * Définit la valeur de la propriété nbtendersreceivednoneu.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setNBTENDERSRECEIVEDNONEU(BigInteger value) {
                this.nbtendersreceivednoneu = value;
            }

            /**
             * Obtient la valeur de la propriété nbtendersreceivedemeans.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getNBTENDERSRECEIVEDEMEANS() {
                return nbtendersreceivedemeans;
            }

            /**
             * Définit la valeur de la propriété nbtendersreceivedemeans.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setNBTENDERSRECEIVEDEMEANS(BigInteger value) {
                this.nbtendersreceivedemeans = value;
            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}VAL_ESTIMATED_TOTAL" minOccurs="0"/&gt;
         *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}VAL_TOTAL"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "valestimatedtotal",
                "valtotal"
        })
        @ToString
        @EqualsAndHashCode
        public static class VALUES {

            @XmlElement(name = "VAL_ESTIMATED_TOTAL")
            protected Val valestimatedtotal;
            @XmlElement(name = "VAL_TOTAL", required = true)
            protected Val valtotal;

            /**
             * Obtient la valeur de la propriété valestimatedtotal.
             *
             * @return possible object is
             * {@link Val }
             */
            public Val getVALESTIMATEDTOTAL() {
                return valestimatedtotal;
            }

            /**
             * Définit la valeur de la propriété valestimatedtotal.
             *
             * @param value allowed object is
             *              {@link Val }
             */
            public void setVALESTIMATEDTOTAL(Val value) {
                this.valestimatedtotal = value;
            }

            /**
             * Obtient la valeur de la propriété valtotal.
             *
             * @return possible object is
             * {@link Val }
             */
            public Val getVALTOTAL() {
                return valtotal;
            }

            /**
             * Définit la valeur de la propriété valtotal.
             *
             * @param value allowed object is
             *              {@link Val }
             */
            public void setVALTOTAL(Val value) {
                this.valtotal = value;
            }

        }

    }

}
