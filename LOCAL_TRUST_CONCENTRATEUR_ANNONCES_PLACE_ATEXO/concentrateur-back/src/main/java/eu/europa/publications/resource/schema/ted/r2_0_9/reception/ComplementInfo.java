//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Section VI: COMPLEMENTARY INFORMATION
 *
 * <p>Classe Java pour complement_info complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="complement_info"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;sequence&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}RECURRENT_PROCUREMENT"/&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ESTIMATED_TIMING" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NO_RECURRENT_PROCUREMENT"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}EORDERING" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}EINVOICING" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}EPAYMENT" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}INFO_ADD" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ADDRESS_REVIEW_BODY" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ADDRESS_MEDIATION_BODY" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}REVIEW_PROCEDURE" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ADDRESS_REVIEW_INFO" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DATE_DISPATCH_NOTICE"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "complement_info", propOrder = {
        "recurrentprocurement",
        "estimatedtiming",
        "norecurrentprocurement",
        "eordering",
        "einvoicing",
        "epayment",
        "infoadd",
        "addressreviewbody",
        "addressmediationbody",
        "reviewprocedure",
        "addressreviewinfo",
        "datedispatchnotice"
})
@XmlSeeAlso({
        CiF01.class,
        CiF02.class,
        CiF03.class,
        CiF04.class,
        CiF05.class,
        CiF06.class,
        CiF07.class,
        CiF08.class,
        CiF12.class,
        CiF13.class,
        CiF15.class,
        CiF20.class,
        CiF21.class,
        CiF22.class,
        CiF23.class,
        CiF24.class,
        CiF25.class,
        CiMove.class
})
@ToString
@EqualsAndHashCode
public class ComplementInfo {

    @XmlElement(name = "RECURRENT_PROCUREMENT")
    protected Empty recurrentprocurement;
    @XmlElement(name = "ESTIMATED_TIMING")
    protected TextFtMultiLines estimatedtiming;
    @XmlElement(name = "NO_RECURRENT_PROCUREMENT")
    protected Empty norecurrentprocurement;
    @XmlElement(name = "EORDERING")
    protected Empty eordering;
    @XmlElement(name = "EINVOICING")
    protected Empty einvoicing;
    @XmlElement(name = "EPAYMENT")
    protected Empty epayment;
    @XmlElement(name = "INFO_ADD")
    protected TextFtMultiLines infoadd;
    @XmlElement(name = "ADDRESS_REVIEW_BODY")
    protected ContactReview addressreviewbody;
    @XmlElement(name = "ADDRESS_MEDIATION_BODY")
    protected ContactReview addressmediationbody;
    @XmlElement(name = "REVIEW_PROCEDURE")
    protected TextFtMultiLines reviewprocedure;
    @XmlElement(name = "ADDRESS_REVIEW_INFO")
    protected ContactReview addressreviewinfo;
    @XmlElement(name = "DATE_DISPATCH_NOTICE", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datedispatchnotice;

    /**
     * Obtient la valeur de la propriété recurrentprocurement.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getRECURRENTPROCUREMENT() {
        return recurrentprocurement;
    }

    /**
     * Définit la valeur de la propriété recurrentprocurement.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setRECURRENTPROCUREMENT(Empty value) {
        this.recurrentprocurement = value;
    }

    /**
     * Obtient la valeur de la propriété estimatedtiming.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getESTIMATEDTIMING() {
        return estimatedtiming;
    }

    /**
     * Définit la valeur de la propriété estimatedtiming.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setESTIMATEDTIMING(TextFtMultiLines value) {
        this.estimatedtiming = value;
    }

    /**
     * Obtient la valeur de la propriété norecurrentprocurement.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getNORECURRENTPROCUREMENT() {
        return norecurrentprocurement;
    }

    /**
     * Définit la valeur de la propriété norecurrentprocurement.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setNORECURRENTPROCUREMENT(Empty value) {
        this.norecurrentprocurement = value;
    }

    /**
     * Obtient la valeur de la propriété eordering.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getEORDERING() {
        return eordering;
    }

    /**
     * Définit la valeur de la propriété eordering.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setEORDERING(Empty value) {
        this.eordering = value;
    }

    /**
     * Obtient la valeur de la propriété einvoicing.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getEINVOICING() {
        return einvoicing;
    }

    /**
     * Définit la valeur de la propriété einvoicing.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setEINVOICING(Empty value) {
        this.einvoicing = value;
    }

    /**
     * Obtient la valeur de la propriété epayment.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getEPAYMENT() {
        return epayment;
    }

    /**
     * Définit la valeur de la propriété epayment.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setEPAYMENT(Empty value) {
        this.epayment = value;
    }

    /**
     * Obtient la valeur de la propriété infoadd.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getINFOADD() {
        return infoadd;
    }

    /**
     * Définit la valeur de la propriété infoadd.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setINFOADD(TextFtMultiLines value) {
        this.infoadd = value;
    }

    /**
     * Obtient la valeur de la propriété addressreviewbody.
     *
     * @return possible object is
     * {@link ContactReview }
     */
    public ContactReview getADDRESSREVIEWBODY() {
        return addressreviewbody;
    }

    /**
     * Définit la valeur de la propriété addressreviewbody.
     *
     * @param value allowed object is
     *              {@link ContactReview }
     */
    public void setADDRESSREVIEWBODY(ContactReview value) {
        this.addressreviewbody = value;
    }

    /**
     * Obtient la valeur de la propriété addressmediationbody.
     *
     * @return possible object is
     * {@link ContactReview }
     */
    public ContactReview getADDRESSMEDIATIONBODY() {
        return addressmediationbody;
    }

    /**
     * Définit la valeur de la propriété addressmediationbody.
     *
     * @param value allowed object is
     *              {@link ContactReview }
     */
    public void setADDRESSMEDIATIONBODY(ContactReview value) {
        this.addressmediationbody = value;
    }

    /**
     * Obtient la valeur de la propriété reviewprocedure.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getREVIEWPROCEDURE() {
        return reviewprocedure;
    }

    /**
     * Définit la valeur de la propriété reviewprocedure.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setREVIEWPROCEDURE(TextFtMultiLines value) {
        this.reviewprocedure = value;
    }

    /**
     * Obtient la valeur de la propriété addressreviewinfo.
     *
     * @return possible object is
     * {@link ContactReview }
     */
    public ContactReview getADDRESSREVIEWINFO() {
        return addressreviewinfo;
    }

    /**
     * Définit la valeur de la propriété addressreviewinfo.
     *
     * @param value allowed object is
     *              {@link ContactReview }
     */
    public void setADDRESSREVIEWINFO(ContactReview value) {
        this.addressreviewinfo = value;
    }

    /**
     * Obtient la valeur de la propriété datedispatchnotice.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDATEDISPATCHNOTICE() {
        return datedispatchnotice;
    }

    /**
     * Définit la valeur de la propriété datedispatchnotice.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDATEDISPATCHNOTICE(XMLGregorianCalendar value) {
        this.datedispatchnotice = value;
    }

}
