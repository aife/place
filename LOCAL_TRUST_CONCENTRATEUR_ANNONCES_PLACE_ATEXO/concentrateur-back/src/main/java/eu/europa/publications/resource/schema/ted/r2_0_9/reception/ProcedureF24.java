//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Section IV: PROCEDURE
 *
 * <p>Classe Java pour procedure_f24 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="procedure_f24"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="CONTRACT_COVERED_GPA" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}works"/&gt;
 *           &lt;element name="NO_CONTRACT_COVERED_GPA" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}works"/&gt;
 *         &lt;/choice&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}receipt_tenders"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LANGUAGES"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "procedure_f24", propOrder = {
        "contractcoveredgpa",
        "nocontractcoveredgpa",
        "datereceipttenders",
        "timereceipttenders",
        "languages"
})
@ToString
@EqualsAndHashCode
public class ProcedureF24 {

    @XmlElement(name = "CONTRACT_COVERED_GPA")
    protected Works contractcoveredgpa;
    @XmlElement(name = "NO_CONTRACT_COVERED_GPA")
    protected Works nocontractcoveredgpa;
    @XmlElement(name = "DATE_RECEIPT_TENDERS", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datereceipttenders;
    @XmlElement(name = "TIME_RECEIPT_TENDERS")
    protected String timereceipttenders;
    @XmlElement(name = "LANGUAGES", required = true)
    protected LANGUAGES languages;

    /**
     * Obtient la valeur de la propriété contractcoveredgpa.
     *
     * @return possible object is
     * {@link Works }
     */
    public Works getCONTRACTCOVEREDGPA() {
        return contractcoveredgpa;
    }

    /**
     * Définit la valeur de la propriété contractcoveredgpa.
     *
     * @param value allowed object is
     *              {@link Works }
     */
    public void setCONTRACTCOVEREDGPA(Works value) {
        this.contractcoveredgpa = value;
    }

    /**
     * Obtient la valeur de la propriété nocontractcoveredgpa.
     *
     * @return possible object is
     * {@link Works }
     */
    public Works getNOCONTRACTCOVEREDGPA() {
        return nocontractcoveredgpa;
    }

    /**
     * Définit la valeur de la propriété nocontractcoveredgpa.
     *
     * @param value allowed object is
     *              {@link Works }
     */
    public void setNOCONTRACTCOVEREDGPA(Works value) {
        this.nocontractcoveredgpa = value;
    }

    /**
     * Obtient la valeur de la propriété datereceipttenders.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDATERECEIPTTENDERS() {
        return datereceipttenders;
    }

    /**
     * Définit la valeur de la propriété datereceipttenders.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDATERECEIPTTENDERS(XMLGregorianCalendar value) {
        this.datereceipttenders = value;
    }

    /**
     * Obtient la valeur de la propriété timereceipttenders.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTIMERECEIPTTENDERS() {
        return timereceipttenders;
    }

    /**
     * Définit la valeur de la propriété timereceipttenders.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTIMERECEIPTTENDERS(String value) {
        this.timereceipttenders = value;
    }

    /**
     * Obtient la valeur de la propriété languages.
     *
     * @return possible object is
     * {@link LANGUAGES }
     */
    public LANGUAGES getLANGUAGES() {
        return languages;
    }

    /**
     * Définit la valeur de la propriété languages.
     *
     * @param value allowed object is
     *              {@link LANGUAGES }
     */
    public void setLANGUAGES(LANGUAGES value) {
        this.languages = value;
    }

}
