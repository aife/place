//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.CollectionUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * Section II: OBJECT OF THE CONTRACT
 *
 * <p>Classe Java pour object_contract_f05 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="object_contract_f05"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TITLE"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}REFERENCE_NUMBER" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}GROUPEMENT"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CPV_MAIN"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LIEU_EXECUTION"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TYPE_CONTRACT"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SHORT_DESCR"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}VAL_ESTIMATED_TOTAL" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;sequence&gt;
 *             &lt;element name="LOT_DIVISION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}lot_division_f05"/&gt;
 *             &lt;choice&gt;
 *               &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TRANCHE"/&gt;
 *               &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NO_TRANCHE"/&gt;
 *             &lt;/choice&gt;
 *             &lt;element name="OBJECT_DESCR" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}object_f05" maxOccurs="4000"/&gt;
 *           &lt;/sequence&gt;
 *           &lt;sequence&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NO_LOT_DIVISION"/&gt;
 *             &lt;element name="OBJECT_DESCR" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}object_f05"/&gt;
 *           &lt;/sequence&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "object_contract_f05", propOrder = {
        "title",
        "referenceNumber",
        "groupement",
        "cpvMain",
        "lieuExecution",
        "typeContract",
        "shortDescr",
        "valEstimatedTotal",
        "lotDivision",
        "tranche",
        "noTranche",
        "objectDescr",
        "duration",
        "noLotDivision"
})
@ToString
@EqualsAndHashCode
@Getter
@Setter
public class ObjectContractF05 {
    @XmlElement(name = "TITLE")
    protected TextFtSingleLine title;
    @XmlElement(name = "REFERENCE_NUMBER")
    protected String referenceNumber;
    @XmlElement(name = "GROUPEMENT")
    protected Empty groupement;
    @XmlElement(name = "CPV_MAIN")
    protected CpvSet cpvMain;
    @XmlElement(name = "LIEU_EXECUTION")
    protected String lieuExecution;
    @XmlElement(name = "TYPE_CONTRACT")
    protected TypeContract typeContract;
    @XmlElement(name = "SHORT_DESCR")
    protected TextFtMultiLines shortDescr;
    @XmlElement(name = "VAL_ESTIMATED_TOTAL")
    protected Val valEstimatedTotal;
    @XmlElement(name = "LOT_DIVISION")
    protected Empty lotDivision;
    @XmlElement(name = "TRANCHE")
    protected Empty tranche;
    @XmlElement(name = "NO_TRANCHE")
    protected Empty noTranche;
    @XmlElement(name = "OBJECT_DESCR")
    protected List<ObjectF05> objectDescr;
    @XmlElement(name = "NO_LOT_DIVISION")
    protected Empty noLotDivision;
    @XmlElement(name = "DURATION")
    private DurationMD duration;

    public Val getValObject() {
        if (this.lotDivision != null || CollectionUtils.isEmpty(this.objectDescr)) {
            return null;
        }
        return this.objectDescr.get(0).getValObject();

    }

    public String getIsAcceptedVariants() {
        return "Non";
    }

    public List<AcDefinition> getCosts() {
        if (CollectionUtils.isEmpty(this.objectDescr))
            return null;
        return this.objectDescr.stream().map(ObjectF05::getAc)
                .filter(ac -> ac != null && !CollectionUtils.isEmpty(ac.getACCOST()))
                .map(ObjectF05.AC::getACCOST)
                .filter(Objects::nonNull)
                .flatMap(List::stream)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public List<P> getMarcheReserveLibelle() {
        if (CollectionUtils.isEmpty(this.objectDescr))
            return null;
        return this.objectDescr.stream().map(ObjectF05::getMarcheReserveLibelle)
                .filter(Objects::nonNull)
                .flatMap(List::stream)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public String getIsGroupement() {
        return this.groupement == null ? "Non" : "Oui";
    }

    public String getIsTranche() {
        return this.tranche == null ? "Non" : "Oui";
    }

    public String getIsLotDivision() {
        return this.lotDivision == null ? "Non" : "Oui";
    }

    public String getTypeContratLibelle() {
        if (this.typeContract == null) {
            return "";
        }
        switch (typeContract.ctype) {
            case "WORKS":
                return "travaux";
            case "SERVICES":
                return "Services";
            default:
                return "Fournitures";
        }
    }


    private boolean estMarcheReserve() {
        if (CollectionUtils.isEmpty(this.objectDescr))
            return false;
        return !CollectionUtils.isEmpty(this.objectDescr.stream().map(ObjectF05::getMarcheReserveLibelle)
                .filter(strings -> !CollectionUtils.isEmpty(strings))
                .collect(Collectors.toList()));
    }

    public String getIsMarcheReserve() {
        return this.estMarcheReserve() ? "Oui" : "Non";
    }

    public String getIsMarcheReserveAlloti() {
        return this.estMarcheReserve() && "Oui".equals(this.getIsLotDivision()) ? "Oui" : "Non";
    }

    public String getIsMarcheReserveNonAlloti() {
        return this.estMarcheReserve() && "Non".equals(this.getIsLotDivision()) ? "Oui" : "Non";
    }

}
