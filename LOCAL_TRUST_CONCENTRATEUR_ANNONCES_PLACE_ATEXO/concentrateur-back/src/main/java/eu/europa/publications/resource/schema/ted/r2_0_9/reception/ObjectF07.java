//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import eu.europa.publications.resource.schema.ted._2021.nuts.Nuts;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java pour object_f07 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="object_f07"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CPV_ADDITIONAL" maxOccurs="100" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/2016/nuts}NUTS" maxOccurs="250"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}MAIN_SITE" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SHORT_DESCR"/&gt;
 *         &lt;element name="AC" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_criteria_doc"/&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}time_frame_indefinite"/&gt;
 *         &lt;sequence minOccurs="0"&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}RENEWAL"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}RENEWAL_DESCR" minOccurs="0"/&gt;
 *         &lt;/sequence&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}eu_union_funds" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "object_f07", propOrder = {
        "cpvadditional",
        "nuts",
        "mainsite",
        "shortdescr",
        "ac",
        "indefiniteduration",
        "datestart",
        "dateend",
        "renewal",
        "renewaldescr",
        "euprogrrelated",
        "noeuprogrrelated"
})
@ToString
@EqualsAndHashCode
public class ObjectF07 {

    @XmlElement(name = "CPV_ADDITIONAL")
    protected List<CpvSet> cpvadditional;
    @XmlElement(name = "NUTS", namespace = "http://publications.europa.eu/resource/schema/ted/2016/nuts", required = true)
    protected List<Nuts> nuts;
    @XmlElement(name = "MAIN_SITE")
    protected TextFtMultiLines mainsite;
    @XmlElement(name = "SHORT_DESCR", required = true)
    protected TextFtMultiLines shortdescr;
    @XmlElement(name = "AC")
    protected ObjectF07.AC ac;
    @XmlElement(name = "INDEFINITE_DURATION")
    protected Empty indefiniteduration;
    @XmlElement(name = "DATE_START")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datestart;
    @XmlElement(name = "DATE_END")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateend;
    @XmlElement(name = "RENEWAL")
    protected Empty renewal;
    @XmlElement(name = "RENEWAL_DESCR")
    protected TextFtMultiLines renewaldescr;
    @XmlElement(name = "EU_PROGR_RELATED")
    protected TextFtMultiLines euprogrrelated;
    @XmlElement(name = "NO_EU_PROGR_RELATED")
    protected Empty noeuprogrrelated;

    /**
     * Gets the value of the cpvadditional property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cpvadditional property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCPVADDITIONAL().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CpvSet }
     */
    public List<CpvSet> getCPVADDITIONAL() {
        if (cpvadditional == null) {
            cpvadditional = new ArrayList<CpvSet>();
        }
        return this.cpvadditional;
    }

    /**
     * Gets the value of the nuts property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nuts property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNUTS().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Nuts }
     */
    public List<Nuts> getNUTS() {
        if (nuts == null) {
            nuts = new ArrayList<Nuts>();
        }
        return this.nuts;
    }

    /**
     * Obtient la valeur de la propriété mainsite.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getMAINSITE() {
        return mainsite;
    }

    /**
     * Définit la valeur de la propriété mainsite.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setMAINSITE(TextFtMultiLines value) {
        this.mainsite = value;
    }

    /**
     * Obtient la valeur de la propriété shortdescr.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getSHORTDESCR() {
        return shortdescr;
    }

    /**
     * Définit la valeur de la propriété shortdescr.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setSHORTDESCR(TextFtMultiLines value) {
        this.shortdescr = value;
    }

    /**
     * Obtient la valeur de la propriété ac.
     *
     * @return possible object is
     * {@link ObjectF07 .AC }
     */
    public ObjectF07.AC getAC() {
        return ac;
    }

    /**
     * Définit la valeur de la propriété ac.
     *
     * @param value allowed object is
     *              {@link ObjectF07 .AC }
     */
    public void setAC(ObjectF07.AC value) {
        this.ac = value;
    }

    /**
     * Obtient la valeur de la propriété indefiniteduration.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getINDEFINITEDURATION() {
        return indefiniteduration;
    }

    /**
     * Définit la valeur de la propriété indefiniteduration.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setINDEFINITEDURATION(Empty value) {
        this.indefiniteduration = value;
    }

    /**
     * Obtient la valeur de la propriété datestart.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDATESTART() {
        return datestart;
    }

    /**
     * Définit la valeur de la propriété datestart.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDATESTART(XMLGregorianCalendar value) {
        this.datestart = value;
    }

    /**
     * Obtient la valeur de la propriété dateend.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDATEEND() {
        return dateend;
    }

    /**
     * Définit la valeur de la propriété dateend.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDATEEND(XMLGregorianCalendar value) {
        this.dateend = value;
    }

    /**
     * Obtient la valeur de la propriété renewal.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getRENEWAL() {
        return renewal;
    }

    /**
     * Définit la valeur de la propriété renewal.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setRENEWAL(Empty value) {
        this.renewal = value;
    }

    /**
     * Obtient la valeur de la propriété renewaldescr.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getRENEWALDESCR() {
        return renewaldescr;
    }

    /**
     * Définit la valeur de la propriété renewaldescr.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setRENEWALDESCR(TextFtMultiLines value) {
        this.renewaldescr = value;
    }

    /**
     * Obtient la valeur de la propriété euprogrrelated.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getEUPROGRRELATED() {
        return euprogrrelated;
    }

    /**
     * Définit la valeur de la propriété euprogrrelated.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setEUPROGRRELATED(TextFtMultiLines value) {
        this.euprogrrelated = value;
    }

    /**
     * Obtient la valeur de la propriété noeuprogrrelated.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getNOEUPROGRRELATED() {
        return noeuprogrrelated;
    }

    /**
     * Définit la valeur de la propriété noeuprogrrelated.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setNOEUPROGRRELATED(Empty value) {
        this.noeuprogrrelated = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_criteria_doc"/&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "acquality",
            "accost",
            "acprice",
            "acprocurementdoc"
    })
    @ToString
    @EqualsAndHashCode
    public static class AC {

        @XmlElement(name = "AC_QUALITY")
        protected List<AcDefinition> acquality;
        @XmlElement(name = "AC_COST")
        protected List<AcDefinition> accost;
        @XmlElement(name = "AC_PRICE")
        protected ACPRICE acprice;
        @XmlElement(name = "AC_PROCUREMENT_DOC")
        protected Empty acprocurementdoc;

        /**
         * Gets the value of the acquality property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the acquality property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getACQUALITY().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AcDefinition }
         */
        public List<AcDefinition> getACQUALITY() {
            if (acquality == null) {
                acquality = new ArrayList<AcDefinition>();
            }
            return this.acquality;
        }

        /**
         * Gets the value of the accost property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the accost property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getACCOST().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AcDefinition }
         */
        public List<AcDefinition> getACCOST() {
            if (accost == null) {
                accost = new ArrayList<AcDefinition>();
            }
            return this.accost;
        }

        /**
         * Obtient la valeur de la propriété acprice.
         *
         * @return possible object is
         * {@link ACPRICE }
         */
        public ACPRICE getACPRICE() {
            return acprice;
        }

        /**
         * Définit la valeur de la propriété acprice.
         *
         * @param value allowed object is
         *              {@link ACPRICE }
         */
        public void setACPRICE(ACPRICE value) {
            this.acprice = value;
        }

        /**
         * Obtient la valeur de la propriété acprocurementdoc.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getACPROCUREMENTDOC() {
            return acprocurementdoc;
        }

        /**
         * Définit la valeur de la propriété acprocurementdoc.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setACPROCUREMENTDOC(Empty value) {
            this.acprocurementdoc = value;
        }

    }

}
