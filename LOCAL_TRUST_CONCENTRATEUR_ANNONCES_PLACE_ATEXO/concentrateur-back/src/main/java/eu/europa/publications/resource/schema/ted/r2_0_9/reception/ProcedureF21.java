//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Section IV: PROCEDURE
 *
 * <p>Classe Java pour procedure_f21 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="procedure_f21"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_OPEN"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_RESTRICTED"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_INVOLVING_NEGOTIATION"/&gt;
 *           &lt;element name="PT_AWARD_CONTRACT_WITHOUT_CALL" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d1_f21"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="FRAMEWORK" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}JUSTIFICATION" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}URL_NATIONAL_PROCEDURE" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}MAIN_FEATURES_AWARD" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NOTICE_NUMBER_OJ" minOccurs="0"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;sequence&gt;
 *             &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}receipt_tenders"/&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LANGUAGES"/&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DATE_AWARD_SCHEDULED" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TERMINATION_PIN"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "procedure_f21", propOrder = {
        "ptopen",
        "ptrestricted",
        "ptinvolvingnegotiation",
        "ptawardcontractwithoutcall",
        "framework",
        "urlnationalprocedure",
        "mainfeaturesaward",
        "noticenumberoj",
        "datereceipttenders",
        "timereceipttenders",
        "languages",
        "dateawardscheduled",
        "terminationpin"
})
@ToString
@EqualsAndHashCode
public class ProcedureF21 {

    @XmlElement(name = "PT_OPEN")
    protected Empty ptopen;
    @XmlElement(name = "PT_RESTRICTED")
    protected Empty ptrestricted;
    @XmlElement(name = "PT_INVOLVING_NEGOTIATION")
    protected Empty ptinvolvingnegotiation;
    @XmlElement(name = "PT_AWARD_CONTRACT_WITHOUT_CALL")
    protected AnnexD1F21 ptawardcontractwithoutcall;
    @XmlElement(name = "FRAMEWORK")
    protected ProcedureF21.FRAMEWORK framework;
    @XmlElement(name = "URL_NATIONAL_PROCEDURE")
    protected String urlnationalprocedure;
    @XmlElement(name = "MAIN_FEATURES_AWARD")
    protected TextFtMultiLines mainfeaturesaward;
    @XmlElement(name = "NOTICE_NUMBER_OJ")
    protected String noticenumberoj;
    @XmlElement(name = "DATE_RECEIPT_TENDERS")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datereceipttenders;
    @XmlElement(name = "TIME_RECEIPT_TENDERS")
    protected String timereceipttenders;
    @XmlElement(name = "LANGUAGES")
    protected LANGUAGES languages;
    @XmlElement(name = "DATE_AWARD_SCHEDULED")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateawardscheduled;
    @XmlElement(name = "TERMINATION_PIN")
    protected Empty terminationpin;

    /**
     * Obtient la valeur de la propriété ptopen.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTOPEN() {
        return ptopen;
    }

    /**
     * Définit la valeur de la propriété ptopen.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTOPEN(Empty value) {
        this.ptopen = value;
    }

    /**
     * Obtient la valeur de la propriété ptrestricted.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTRESTRICTED() {
        return ptrestricted;
    }

    /**
     * Définit la valeur de la propriété ptrestricted.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTRESTRICTED(Empty value) {
        this.ptrestricted = value;
    }

    /**
     * Obtient la valeur de la propriété ptinvolvingnegotiation.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTINVOLVINGNEGOTIATION() {
        return ptinvolvingnegotiation;
    }

    /**
     * Définit la valeur de la propriété ptinvolvingnegotiation.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTINVOLVINGNEGOTIATION(Empty value) {
        this.ptinvolvingnegotiation = value;
    }

    /**
     * Obtient la valeur de la propriété ptawardcontractwithoutcall.
     *
     * @return possible object is
     * {@link AnnexD1F21 }
     */
    public AnnexD1F21 getPTAWARDCONTRACTWITHOUTCALL() {
        return ptawardcontractwithoutcall;
    }

    /**
     * Définit la valeur de la propriété ptawardcontractwithoutcall.
     *
     * @param value allowed object is
     *              {@link AnnexD1F21 }
     */
    public void setPTAWARDCONTRACTWITHOUTCALL(AnnexD1F21 value) {
        this.ptawardcontractwithoutcall = value;
    }

    /**
     * Obtient la valeur de la propriété framework.
     *
     * @return possible object is
     * {@link ProcedureF21 .FRAMEWORK }
     */
    public ProcedureF21.FRAMEWORK getFRAMEWORK() {
        return framework;
    }

    /**
     * Définit la valeur de la propriété framework.
     *
     * @param value allowed object is
     *              {@link ProcedureF21 .FRAMEWORK }
     */
    public void setFRAMEWORK(ProcedureF21.FRAMEWORK value) {
        this.framework = value;
    }

    /**
     * Obtient la valeur de la propriété urlnationalprocedure.
     *
     * @return possible object is
     * {@link String }
     */
    public String getURLNATIONALPROCEDURE() {
        return urlnationalprocedure;
    }

    /**
     * Définit la valeur de la propriété urlnationalprocedure.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setURLNATIONALPROCEDURE(String value) {
        this.urlnationalprocedure = value;
    }

    /**
     * Obtient la valeur de la propriété mainfeaturesaward.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getMAINFEATURESAWARD() {
        return mainfeaturesaward;
    }

    /**
     * Définit la valeur de la propriété mainfeaturesaward.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setMAINFEATURESAWARD(TextFtMultiLines value) {
        this.mainfeaturesaward = value;
    }

    /**
     * Obtient la valeur de la propriété noticenumberoj.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNOTICENUMBEROJ() {
        return noticenumberoj;
    }

    /**
     * Définit la valeur de la propriété noticenumberoj.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNOTICENUMBEROJ(String value) {
        this.noticenumberoj = value;
    }

    /**
     * Obtient la valeur de la propriété datereceipttenders.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDATERECEIPTTENDERS() {
        return datereceipttenders;
    }

    /**
     * Définit la valeur de la propriété datereceipttenders.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDATERECEIPTTENDERS(XMLGregorianCalendar value) {
        this.datereceipttenders = value;
    }

    /**
     * Obtient la valeur de la propriété timereceipttenders.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTIMERECEIPTTENDERS() {
        return timereceipttenders;
    }

    /**
     * Définit la valeur de la propriété timereceipttenders.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTIMERECEIPTTENDERS(String value) {
        this.timereceipttenders = value;
    }

    /**
     * Obtient la valeur de la propriété languages.
     *
     * @return possible object is
     * {@link LANGUAGES }
     */
    public LANGUAGES getLANGUAGES() {
        return languages;
    }

    /**
     * Définit la valeur de la propriété languages.
     *
     * @param value allowed object is
     *              {@link LANGUAGES }
     */
    public void setLANGUAGES(LANGUAGES value) {
        this.languages = value;
    }

    /**
     * Obtient la valeur de la propriété dateawardscheduled.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDATEAWARDSCHEDULED() {
        return dateawardscheduled;
    }

    /**
     * Définit la valeur de la propriété dateawardscheduled.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDATEAWARDSCHEDULED(XMLGregorianCalendar value) {
        this.dateawardscheduled = value;
    }

    /**
     * Obtient la valeur de la propriété terminationpin.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getTERMINATIONPIN() {
        return terminationpin;
    }

    /**
     * Définit la valeur de la propriété terminationpin.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setTERMINATIONPIN(Empty value) {
        this.terminationpin = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}JUSTIFICATION" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "justification"
    })
    @ToString
    @EqualsAndHashCode
    public static class FRAMEWORK {

        @XmlElement(name = "JUSTIFICATION")
        protected TextFtMultiLines justification;

        /**
         * for any duration exceeding 4 years
         *
         * @return possible object is
         * {@link TextFtMultiLines }
         */
        public TextFtMultiLines getJUSTIFICATION() {
            return justification;
        }

        /**
         * Définit la valeur de la propriété justification.
         *
         * @param value allowed object is
         *              {@link TextFtMultiLines }
         */
        public void setJUSTIFICATION(TextFtMultiLines value) {
            this.justification = value;
        }

    }

}
