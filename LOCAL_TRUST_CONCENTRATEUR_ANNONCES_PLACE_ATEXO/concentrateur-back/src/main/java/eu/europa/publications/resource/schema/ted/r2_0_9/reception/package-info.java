//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//

@javax.xml.bind.annotation.XmlSchema(namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception",
        elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED,
        xmlns = {
                @javax.xml.bind.annotation.XmlNs(namespaceURI = "http://publications.europa.eu/resource/schema/ted/2016/nuts", prefix = "nuts"),
                @javax.xml.bind.annotation.XmlNs(namespaceURI = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", prefix = "")}
)
package eu.europa.publications.resource.schema.ted.r2_0_9.reception;
