//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java pour form_section complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="form_section"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NOTICE_UUID" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}F01_2014" maxOccurs="unbounded"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}F02_2014" maxOccurs="unbounded"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}F03_2014" maxOccurs="unbounded"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}F04_2014" maxOccurs="unbounded"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}F05_2014" maxOccurs="unbounded"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}F06_2014" maxOccurs="unbounded"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}F07_2014" maxOccurs="unbounded"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}F08_2014" maxOccurs="unbounded"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}F12_2014" maxOccurs="unbounded"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}F13_2014" maxOccurs="unbounded"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}F14_2014" maxOccurs="unbounded"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}F15_2014" maxOccurs="unbounded"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}F20_2014" maxOccurs="unbounded"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}F21_2014" maxOccurs="unbounded"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}F22_2014" maxOccurs="unbounded"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}F23_2014" maxOccurs="unbounded"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}F24_2014" maxOccurs="unbounded"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}F25_2014" maxOccurs="unbounded"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}MOVE" maxOccurs="unbounded"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "form_section", propOrder = {
        "noticeuuid",
        "f012014",
        "f022014",
        "f032014",
        "f042014",
        "f052014",
        "f062014",
        "f072014",
        "f082014",
        "f122014",
        "f132014",
        "f142014",
        "f152014",
        "f202014",
        "f212014",
        "f222014",
        "f232014",
        "f242014",
        "f252014",
        "move"
})
@ToString
@EqualsAndHashCode
public class FormSection {

    @XmlElement(name = "NOTICE_UUID")
    protected String noticeuuid;
    @XmlElement(name = "F01_2014")
    protected List<F012014> f012014;
    @XmlElement(name = "F02_2014")
    protected List<F022014> f022014;
    @XmlElement(name = "F03_2014")
    protected List<F032014> f032014;
    @XmlElement(name = "F04_2014")
    protected List<F042014> f042014;
    @XmlElement(name = "F05_2014")
    protected F052014 f052014;
    @XmlElement(name = "F06_2014")
    protected List<F062014> f062014;
    @XmlElement(name = "F07_2014")
    protected List<F072014> f072014;
    @XmlElement(name = "F08_2014")
    protected List<F082014> f082014;
    @XmlElement(name = "F12_2014")
    protected List<F122014> f122014;
    @XmlElement(name = "F13_2014")
    protected List<F132014> f132014;
    @XmlElement(name = "F14_2014")
    protected List<F142014> f142014;
    @XmlElement(name = "F15_2014")
    protected List<F152014> f152014;
    @XmlElement(name = "F20_2014")
    protected List<F202014> f202014;
    @XmlElement(name = "F21_2014")
    protected List<F212014> f212014;
    @XmlElement(name = "F22_2014")
    protected List<F222014> f222014;
    @XmlElement(name = "F23_2014")
    protected List<F232014> f232014;
    @XmlElement(name = "F24_2014")
    protected List<F242014> f242014;
    @XmlElement(name = "F25_2014")
    protected List<F252014> f252014;
    @XmlElement(name = "MOVE")
    protected List<MOVE> move;

    /**
     * Obtient la valeur de la propriété noticeuuid.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNOTICEUUID() {
        return noticeuuid;
    }

    /**
     * Définit la valeur de la propriété noticeuuid.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNOTICEUUID(String value) {
        this.noticeuuid = value;
    }

    /**
     * Gets the value of the f012014 property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the f012014 property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getF012014().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link F012014 }
     */
    public List<F012014> getF012014() {
        if (f012014 == null) {
            f012014 = new ArrayList<F012014>();
        }
        return this.f012014;
    }

    /**
     * Gets the value of the f022014 property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the f022014 property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getF022014().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link F022014 }
     */
    public List<F022014> getF022014() {
        if (f022014 == null) {
            f022014 = new ArrayList<F022014>();
        }
        return this.f022014;
    }

    /**
     * Gets the value of the f032014 property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the f032014 property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getF032014().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link F032014 }
     */
    public List<F032014> getF032014() {
        if (f032014 == null) {
            f032014 = new ArrayList<F032014>();
        }
        return this.f032014;
    }

    /**
     * Gets the value of the f042014 property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the f042014 property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getF042014().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link F042014 }
     */
    public List<F042014> getF042014() {
        if (f042014 == null) {
            f042014 = new ArrayList<F042014>();
        }
        return this.f042014;
    }

    /**
     * Gets the value of the f052014 property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the f052014 property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getF052014().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link F052014 }
     */
    public F052014 getF052014() {
        return this.f052014;
    }

    /**
     * Gets the value of the f062014 property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the f062014 property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getF062014().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link F062014 }
     */
    public List<F062014> getF062014() {
        if (f062014 == null) {
            f062014 = new ArrayList<F062014>();
        }
        return this.f062014;
    }

    /**
     * Gets the value of the f072014 property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the f072014 property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getF072014().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link F072014 }
     */
    public List<F072014> getF072014() {
        if (f072014 == null) {
            f072014 = new ArrayList<F072014>();
        }
        return this.f072014;
    }

    /**
     * Gets the value of the f082014 property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the f082014 property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getF082014().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link F082014 }
     */
    public List<F082014> getF082014() {
        if (f082014 == null) {
            f082014 = new ArrayList<F082014>();
        }
        return this.f082014;
    }

    /**
     * Gets the value of the f122014 property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the f122014 property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getF122014().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link F122014 }
     */
    public List<F122014> getF122014() {
        if (f122014 == null) {
            f122014 = new ArrayList<F122014>();
        }
        return this.f122014;
    }

    /**
     * Gets the value of the f132014 property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the f132014 property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getF132014().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link F132014 }
     */
    public List<F132014> getF132014() {
        if (f132014 == null) {
            f132014 = new ArrayList<F132014>();
        }
        return this.f132014;
    }

    /**
     * Gets the value of the f142014 property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the f142014 property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getF142014().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link F142014 }
     */
    public List<F142014> getF142014() {
        if (f142014 == null) {
            f142014 = new ArrayList<F142014>();
        }
        return this.f142014;
    }

    /**
     * Gets the value of the f152014 property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the f152014 property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getF152014().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link F152014 }
     */
    public List<F152014> getF152014() {
        if (f152014 == null) {
            f152014 = new ArrayList<F152014>();
        }
        return this.f152014;
    }

    /**
     * Gets the value of the f202014 property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the f202014 property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getF202014().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link F202014 }
     */
    public List<F202014> getF202014() {
        if (f202014 == null) {
            f202014 = new ArrayList<F202014>();
        }
        return this.f202014;
    }

    /**
     * Gets the value of the f212014 property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the f212014 property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getF212014().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link F212014 }
     */
    public List<F212014> getF212014() {
        if (f212014 == null) {
            f212014 = new ArrayList<F212014>();
        }
        return this.f212014;
    }

    /**
     * Gets the value of the f222014 property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the f222014 property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getF222014().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link F222014 }
     */
    public List<F222014> getF222014() {
        if (f222014 == null) {
            f222014 = new ArrayList<F222014>();
        }
        return this.f222014;
    }

    /**
     * Gets the value of the f232014 property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the f232014 property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getF232014().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link F232014 }
     */
    public List<F232014> getF232014() {
        if (f232014 == null) {
            f232014 = new ArrayList<F232014>();
        }
        return this.f232014;
    }

    /**
     * Gets the value of the f242014 property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the f242014 property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getF242014().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link F242014 }
     */
    public List<F242014> getF242014() {
        if (f242014 == null) {
            f242014 = new ArrayList<F242014>();
        }
        return this.f242014;
    }

    /**
     * Gets the value of the f252014 property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the f252014 property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getF252014().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link F252014 }
     */
    public List<F252014> getF252014() {
        if (f252014 == null) {
            f252014 = new ArrayList<F252014>();
        }
        return this.f252014;
    }

    /**
     * Gets the value of the move property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the move property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMOVE().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MOVE }
     */
    public List<MOVE> getMOVE() {
        if (move == null) {
            move = new ArrayList<MOVE>();
        }
        return this.move;
    }

}
