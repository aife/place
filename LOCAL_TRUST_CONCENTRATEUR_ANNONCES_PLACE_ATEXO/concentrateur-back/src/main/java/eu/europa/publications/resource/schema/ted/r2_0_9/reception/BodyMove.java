//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Section I: CONTRACTING AUTHORITY
 *
 * <p>Classe Java pour body_move complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="body_move"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ADDRESS_CONTRACTING_BODY" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contact_contracting_body_move"/&gt;
 *         &lt;sequence minOccurs="0"&gt;
 *           &lt;element name="ADDRESS_CONTRACTING_BODY_ADDITIONAL" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contact_add_contracting_body_move" maxOccurs="unbounded"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CENTRAL_PURCHASING"/&gt;
 *         &lt;/sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ADDRESS_FURTHER_INFO_IDEM"/&gt;
 *           &lt;element name="ADDRESS_FURTHER_INFO" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contact_contracting_body_move"/&gt;
 *         &lt;/choice&gt;
 *         &lt;choice&gt;
 *           &lt;element name="CA_TYPE" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ca_type_move"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CA_TYPE_OTHER"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "body_move", propOrder = {
        "addresscontractingbody",
        "addresscontractingbodyadditional",
        "centralpurchasing",
        "addressfurtherinfoidem",
        "addressfurtherinfo",
        "catype",
        "catypeother"
})
@ToString
@EqualsAndHashCode
public class BodyMove {

    @XmlElement(name = "ADDRESS_CONTRACTING_BODY", required = true)
    protected ContactContractingBodyMove addresscontractingbody;
    @XmlElement(name = "ADDRESS_CONTRACTING_BODY_ADDITIONAL")
    protected List<ContactAddContractingBodyMove> addresscontractingbodyadditional;
    @XmlElement(name = "CENTRAL_PURCHASING")
    protected Empty centralpurchasing;
    @XmlElement(name = "ADDRESS_FURTHER_INFO_IDEM")
    protected Empty addressfurtherinfoidem;
    @XmlElement(name = "ADDRESS_FURTHER_INFO")
    protected ContactContractingBodyMove addressfurtherinfo;
    @XmlElement(name = "CA_TYPE")
    protected CaTypeMove catype;
    @XmlElement(name = "CA_TYPE_OTHER")
    protected String catypeother;

    /**
     * Obtient la valeur de la propriété addresscontractingbody.
     *
     * @return possible object is
     * {@link ContactContractingBodyMove }
     */
    public ContactContractingBodyMove getADDRESSCONTRACTINGBODY() {
        return addresscontractingbody;
    }

    /**
     * Définit la valeur de la propriété addresscontractingbody.
     *
     * @param value allowed object is
     *              {@link ContactContractingBodyMove }
     */
    public void setADDRESSCONTRACTINGBODY(ContactContractingBodyMove value) {
        this.addresscontractingbody = value;
    }

    /**
     * Gets the value of the addresscontractingbodyadditional property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addresscontractingbodyadditional property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getADDRESSCONTRACTINGBODYADDITIONAL().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactAddContractingBodyMove }
     */
    public List<ContactAddContractingBodyMove> getADDRESSCONTRACTINGBODYADDITIONAL() {
        if (addresscontractingbodyadditional == null) {
            addresscontractingbodyadditional = new ArrayList<ContactAddContractingBodyMove>();
        }
        return this.addresscontractingbodyadditional;
    }

    /**
     * Obtient la valeur de la propriété centralpurchasing.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getCENTRALPURCHASING() {
        return centralpurchasing;
    }

    /**
     * Définit la valeur de la propriété centralpurchasing.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setCENTRALPURCHASING(Empty value) {
        this.centralpurchasing = value;
    }

    /**
     * Obtient la valeur de la propriété addressfurtherinfoidem.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getADDRESSFURTHERINFOIDEM() {
        return addressfurtherinfoidem;
    }

    /**
     * Définit la valeur de la propriété addressfurtherinfoidem.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setADDRESSFURTHERINFOIDEM(Empty value) {
        this.addressfurtherinfoidem = value;
    }

    /**
     * Obtient la valeur de la propriété addressfurtherinfo.
     *
     * @return possible object is
     * {@link ContactContractingBodyMove }
     */
    public ContactContractingBodyMove getADDRESSFURTHERINFO() {
        return addressfurtherinfo;
    }

    /**
     * Définit la valeur de la propriété addressfurtherinfo.
     *
     * @param value allowed object is
     *              {@link ContactContractingBodyMove }
     */
    public void setADDRESSFURTHERINFO(ContactContractingBodyMove value) {
        this.addressfurtherinfo = value;
    }

    /**
     * Obtient la valeur de la propriété catype.
     *
     * @return possible object is
     * {@link CaTypeMove }
     */
    public CaTypeMove getCATYPE() {
        return catype;
    }

    /**
     * Définit la valeur de la propriété catype.
     *
     * @param value allowed object is
     *              {@link CaTypeMove }
     */
    public void setCATYPE(CaTypeMove value) {
        this.catype = value;
    }

    /**
     * Obtient la valeur de la propriété catypeother.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCATYPEOTHER() {
        return catypeother;
    }

    /**
     * Définit la valeur de la propriété catypeother.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCATYPEOTHER(String value) {
        this.catypeother = value;
    }

}
