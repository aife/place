//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Section I: CONTRACTING AUTHORITY
 *
 * <p>Classe Java pour body_f25 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="body_f25"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ADDRESS_CONTRACTING_BODY"/&gt;
 *         &lt;choice&gt;
 *           &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}authority"/&gt;
 *           &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}entity"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "body_f25", propOrder = {
        "addresscontractingbody",
        "catype",
        "catypeother",
        "caactivity",
        "caactivityother",
        "ceactivity",
        "ceactivityother"
})
@ToString
@EqualsAndHashCode
public class BodyF25 {

    @XmlElement(name = "ADDRESS_CONTRACTING_BODY", required = true)
    protected ContactContractingBody addresscontractingbody;
    @XmlElement(name = "CA_TYPE")
    protected CaType catype;
    @XmlElement(name = "CA_TYPE_OTHER")
    protected String catypeother;
    @XmlElement(name = "CA_ACTIVITY")
    protected CaActivity caactivity;
    @XmlElement(name = "CA_ACTIVITY_OTHER")
    protected String caactivityother;
    @XmlElement(name = "CE_ACTIVITY")
    protected CeActivity ceactivity;
    @XmlElement(name = "CE_ACTIVITY_OTHER")
    protected String ceactivityother;

    /**
     * Obtient la valeur de la propriété addresscontractingbody.
     *
     * @return possible object is
     * {@link ContactContractingBody }
     */
    public ContactContractingBody getADDRESSCONTRACTINGBODY() {
        return addresscontractingbody;
    }

    /**
     * Définit la valeur de la propriété addresscontractingbody.
     *
     * @param value allowed object is
     *              {@link ContactContractingBody }
     */
    public void setADDRESSCONTRACTINGBODY(ContactContractingBody value) {
        this.addresscontractingbody = value;
    }

    /**
     * Obtient la valeur de la propriété catype.
     *
     * @return possible object is
     * {@link CaType }
     */
    public CaType getCATYPE() {
        return catype;
    }

    /**
     * Définit la valeur de la propriété catype.
     *
     * @param value allowed object is
     *              {@link CaType }
     */
    public void setCATYPE(CaType value) {
        this.catype = value;
    }

    /**
     * Obtient la valeur de la propriété catypeother.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCATYPEOTHER() {
        return catypeother;
    }

    /**
     * Définit la valeur de la propriété catypeother.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCATYPEOTHER(String value) {
        this.catypeother = value;
    }

    /**
     * Obtient la valeur de la propriété caactivity.
     *
     * @return possible object is
     * {@link CaActivity }
     */
    public CaActivity getCAACTIVITY() {
        return caactivity;
    }

    /**
     * Définit la valeur de la propriété caactivity.
     *
     * @param value allowed object is
     *              {@link CaActivity }
     */
    public void setCAACTIVITY(CaActivity value) {
        this.caactivity = value;
    }

    /**
     * Obtient la valeur de la propriété caactivityother.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCAACTIVITYOTHER() {
        return caactivityother;
    }

    /**
     * Définit la valeur de la propriété caactivityother.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCAACTIVITYOTHER(String value) {
        this.caactivityother = value;
    }

    /**
     * Obtient la valeur de la propriété ceactivity.
     *
     * @return possible object is
     * {@link CeActivity }
     */
    public CeActivity getCEACTIVITY() {
        return ceactivity;
    }

    /**
     * Définit la valeur de la propriété ceactivity.
     *
     * @param value allowed object is
     *              {@link CeActivity }
     */
    public void setCEACTIVITY(CeActivity value) {
        this.ceactivity = value;
    }

    /**
     * Obtient la valeur de la propriété ceactivityother.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCEACTIVITYOTHER() {
        return ceactivityother;
    }

    /**
     * Définit la valeur de la propriété ceactivityother.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCEACTIVITYOTHER(String value) {
        this.ceactivityother = value;
    }

}
