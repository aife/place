//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java pour anonymous complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LEGAL_BASIS" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}legal_basis_f15"/&gt;
 *         &lt;element name="LEGAL_BASIS_OTHER" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}legal_basis_f15" minOccurs="0"/&gt;
 *         &lt;element name="CONTRACTING_BODY" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}body_f15"/&gt;
 *         &lt;element name="OBJECT_CONTRACT" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}object_contract_f15"/&gt;
 *         &lt;element name="PROCEDURE" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}procedure_f15"/&gt;
 *         &lt;element name="AWARD_CONTRACT" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_contract_f15" maxOccurs="4000"/&gt;
 *         &lt;element name="COMPLEMENTARY_INFO" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ci_f15"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="LG" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}t_ce_language_list" /&gt;
 *       &lt;attribute name="CATEGORY" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}original_translation" /&gt;
 *       &lt;attribute name="FORM" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="F15" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "legalbasis",
        "legalbasisother",
        "contractingbody",
        "objectcontract",
        "procedure",
        "awardcontract",
        "complementaryinfo"
})
@XmlRootElement(name = "F15_2014")
@ToString
@EqualsAndHashCode
public class F152014 {

    @XmlElement(name = "LEGAL_BASIS", required = true)
    protected LegalBasisF15 legalbasis;
    @XmlElement(name = "LEGAL_BASIS_OTHER")
    protected LegalBasisF15 legalbasisother;
    @XmlElement(name = "CONTRACTING_BODY", required = true)
    protected BodyF15 contractingbody;
    @XmlElement(name = "OBJECT_CONTRACT", required = true)
    protected ObjectContractF15 objectcontract;
    @XmlElement(name = "PROCEDURE", required = true)
    protected ProcedureF15 procedure;
    @XmlElement(name = "AWARD_CONTRACT", required = true)
    protected List<AwardContractF15> awardcontract;
    @XmlElement(name = "COMPLEMENTARY_INFO", required = true)
    protected CiF15 complementaryinfo;
    @XmlAttribute(name = "LG", required = true)
    protected TCeLanguageList lg;
    @XmlAttribute(name = "CATEGORY", required = true)
    protected OriginalTranslation category;
    @XmlAttribute(name = "FORM", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String form;

    /**
     * Obtient la valeur de la propriété legalbasis.
     *
     * @return possible object is
     * {@link LegalBasisF15 }
     */
    public LegalBasisF15 getLEGALBASIS() {
        return legalbasis;
    }

    /**
     * Définit la valeur de la propriété legalbasis.
     *
     * @param value allowed object is
     *              {@link LegalBasisF15 }
     */
    public void setLEGALBASIS(LegalBasisF15 value) {
        this.legalbasis = value;
    }

    /**
     * Obtient la valeur de la propriété legalbasisother.
     *
     * @return possible object is
     * {@link LegalBasisF15 }
     */
    public LegalBasisF15 getLEGALBASISOTHER() {
        return legalbasisother;
    }

    /**
     * Définit la valeur de la propriété legalbasisother.
     *
     * @param value allowed object is
     *              {@link LegalBasisF15 }
     */
    public void setLEGALBASISOTHER(LegalBasisF15 value) {
        this.legalbasisother = value;
    }

    /**
     * Obtient la valeur de la propriété contractingbody.
     *
     * @return possible object is
     * {@link BodyF15 }
     */
    public BodyF15 getCONTRACTINGBODY() {
        return contractingbody;
    }

    /**
     * Définit la valeur de la propriété contractingbody.
     *
     * @param value allowed object is
     *              {@link BodyF15 }
     */
    public void setCONTRACTINGBODY(BodyF15 value) {
        this.contractingbody = value;
    }

    /**
     * Obtient la valeur de la propriété objectcontract.
     *
     * @return possible object is
     * {@link ObjectContractF15 }
     */
    public ObjectContractF15 getOBJECTCONTRACT() {
        return objectcontract;
    }

    /**
     * Définit la valeur de la propriété objectcontract.
     *
     * @param value allowed object is
     *              {@link ObjectContractF15 }
     */
    public void setOBJECTCONTRACT(ObjectContractF15 value) {
        this.objectcontract = value;
    }

    /**
     * Obtient la valeur de la propriété procedure.
     *
     * @return possible object is
     * {@link ProcedureF15 }
     */
    public ProcedureF15 getPROCEDURE() {
        return procedure;
    }

    /**
     * Définit la valeur de la propriété procedure.
     *
     * @param value allowed object is
     *              {@link ProcedureF15 }
     */
    public void setPROCEDURE(ProcedureF15 value) {
        this.procedure = value;
    }

    /**
     * Gets the value of the awardcontract property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the awardcontract property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAWARDCONTRACT().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AwardContractF15 }
     */
    public List<AwardContractF15> getAWARDCONTRACT() {
        if (awardcontract == null) {
            awardcontract = new ArrayList<AwardContractF15>();
        }
        return this.awardcontract;
    }

    /**
     * Obtient la valeur de la propriété complementaryinfo.
     *
     * @return possible object is
     * {@link CiF15 }
     */
    public CiF15 getCOMPLEMENTARYINFO() {
        return complementaryinfo;
    }

    /**
     * Définit la valeur de la propriété complementaryinfo.
     *
     * @param value allowed object is
     *              {@link CiF15 }
     */
    public void setCOMPLEMENTARYINFO(CiF15 value) {
        this.complementaryinfo = value;
    }

    /**
     * Obtient la valeur de la propriété lg.
     *
     * @return possible object is
     * {@link TCeLanguageList }
     */
    public TCeLanguageList getLG() {
        return lg;
    }

    /**
     * Définit la valeur de la propriété lg.
     *
     * @param value allowed object is
     *              {@link TCeLanguageList }
     */
    public void setLG(TCeLanguageList value) {
        this.lg = value;
    }

    /**
     * Obtient la valeur de la propriété category.
     *
     * @return possible object is
     * {@link OriginalTranslation }
     */
    public OriginalTranslation getCATEGORY() {
        return category;
    }

    /**
     * Définit la valeur de la propriété category.
     *
     * @param value allowed object is
     *              {@link OriginalTranslation }
     */
    public void setCATEGORY(OriginalTranslation value) {
        this.category = value;
    }

    /**
     * Obtient la valeur de la propriété form.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFORM() {
        if (form == null) {
            return "F15";
        } else {
            return form;
        }
    }

    /**
     * Définit la valeur de la propriété form.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFORM(String value) {
        this.form = value;
    }

}
