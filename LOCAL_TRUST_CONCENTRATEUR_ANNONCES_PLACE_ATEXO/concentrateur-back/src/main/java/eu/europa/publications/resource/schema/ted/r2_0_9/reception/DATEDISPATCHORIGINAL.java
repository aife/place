//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour anonymous complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;simpleContent&gt;
 *     &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;date_full"&gt;
 *       &lt;attribute name="PUBLICATION" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="NO" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "value"
})
@XmlRootElement(name = "DATE_DISPATCH_ORIGINAL")
@ToString
@EqualsAndHashCode
public class DATEDISPATCHORIGINAL {

    @XmlValue
    protected XMLGregorianCalendar value;
    @XmlAttribute(name = "PUBLICATION", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String publication;

    /**
     * Format (19|20)yy-mm-dd
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getValue() {
        return value;
    }

    /**
     * Définit la valeur de la propriété value.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setValue(XMLGregorianCalendar value) {
        this.value = value;
    }

    /**
     * Obtient la valeur de la propriété publication.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPUBLICATION() {
        if (publication == null) {
            return "NO";
        } else {
            return publication;
        }
    }

    /**
     * Définit la valeur de la propriété publication.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPUBLICATION(String value) {
        this.publication = value;
    }

}
