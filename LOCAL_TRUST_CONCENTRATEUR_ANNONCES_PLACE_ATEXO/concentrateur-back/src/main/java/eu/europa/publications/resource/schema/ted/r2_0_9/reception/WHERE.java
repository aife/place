//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java pour anonymous complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SECTION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}string_20"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LOT_NO" minOccurs="0"/&gt;
 *         &lt;element name="LABEL" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}string_400" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "section",
        "lotno",
        "label"
})
@XmlRootElement(name = "WHERE")
@ToString
@EqualsAndHashCode
public class WHERE {

    @XmlElement(name = "SECTION", required = true)
    protected String section;
    @XmlElement(name = "LOT_NO")
    protected String lotno;
    @XmlElement(name = "LABEL")
    protected String label;

    /**
     * Obtient la valeur de la propriété section.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSECTION() {
        return section;
    }

    /**
     * Définit la valeur de la propriété section.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSECTION(String value) {
        this.section = value;
    }

    /**
     * Obtient la valeur de la propriété lotno.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLOTNO() {
        return lotno;
    }

    /**
     * Définit la valeur de la propriété lotno.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLOTNO(String value) {
        this.lotno = value;
    }

    /**
     * Obtient la valeur de la propriété label.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLABEL() {
        return label;
    }

    /**
     * Définit la valeur de la propriété label.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLABEL(String value) {
        this.label = value;
    }

}
