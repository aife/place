//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


/**
 * Section V: AWARD OF CONTRACT
 *
 * <p>Classe Java pour award_contract_move complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="award_contract_move"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AWARDED_CONTRACT"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DATE_CONCLUSION_CONTRACT"/&gt;
 *                   &lt;element name="CONTRACTORS"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="CONTRACTOR" maxOccurs="100"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="ADDRESS_CONTRACTOR" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contact_contractor_move"/&gt;
 *                                       &lt;element name="OWNERSHIP" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}string_200" minOccurs="0"/&gt;
 *                                       &lt;element name="ADDRESS_PARTY" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contact_party_move" maxOccurs="100" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="VALUES"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}VAL_TOTAL"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="NB_KILOMETRES" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "award_contract_move", propOrder = {
        "awardedcontract"
})
@ToString
@EqualsAndHashCode
public class AwardContractMove {

    @XmlElement(name = "AWARDED_CONTRACT", required = true)
    protected AwardContractMove.AWARDEDCONTRACT awardedcontract;

    /**
     * Obtient la valeur de la propriété awardedcontract.
     *
     * @return possible object is
     * {@link AwardContractMove.AWARDEDCONTRACT }
     */
    public AwardContractMove.AWARDEDCONTRACT getAWARDEDCONTRACT() {
        return awardedcontract;
    }

    /**
     * Définit la valeur de la propriété awardedcontract.
     *
     * @param value allowed object is
     *              {@link AwardContractMove.AWARDEDCONTRACT }
     */
    public void setAWARDEDCONTRACT(AwardContractMove.AWARDEDCONTRACT value) {
        this.awardedcontract = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DATE_CONCLUSION_CONTRACT"/&gt;
     *         &lt;element name="CONTRACTORS"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="CONTRACTOR" maxOccurs="100"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="ADDRESS_CONTRACTOR" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contact_contractor_move"/&gt;
     *                             &lt;element name="OWNERSHIP" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}string_200" minOccurs="0"/&gt;
     *                             &lt;element name="ADDRESS_PARTY" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contact_party_move" maxOccurs="100" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="VALUES"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}VAL_TOTAL"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="NB_KILOMETRES" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "dateconclusioncontract",
            "contractors",
            "values",
            "nbkilometres"
    })
    @ToString
    @EqualsAndHashCode
    public static class AWARDEDCONTRACT {

        @XmlElement(name = "DATE_CONCLUSION_CONTRACT", required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dateconclusioncontract;
        @XmlElement(name = "CONTRACTORS", required = true)
        protected AwardContractMove.AWARDEDCONTRACT.CONTRACTORS contractors;
        @XmlElement(name = "VALUES", required = true)
        protected AwardContractMove.AWARDEDCONTRACT.VALUES values;
        @XmlElement(name = "NB_KILOMETRES")
        protected BigInteger nbkilometres;

        /**
         * Obtient la valeur de la propriété dateconclusioncontract.
         *
         * @return possible object is
         * {@link XMLGregorianCalendar }
         */
        public XMLGregorianCalendar getDATECONCLUSIONCONTRACT() {
            return dateconclusioncontract;
        }

        /**
         * Définit la valeur de la propriété dateconclusioncontract.
         *
         * @param value allowed object is
         *              {@link XMLGregorianCalendar }
         */
        public void setDATECONCLUSIONCONTRACT(XMLGregorianCalendar value) {
            this.dateconclusioncontract = value;
        }

        /**
         * Obtient la valeur de la propriété contractors.
         *
         * @return possible object is
         * {@link AwardContractMove.AWARDEDCONTRACT.CONTRACTORS }
         */
        public AwardContractMove.AWARDEDCONTRACT.CONTRACTORS getCONTRACTORS() {
            return contractors;
        }

        /**
         * Définit la valeur de la propriété contractors.
         *
         * @param value allowed object is
         *              {@link AwardContractMove.AWARDEDCONTRACT.CONTRACTORS }
         */
        public void setCONTRACTORS(AwardContractMove.AWARDEDCONTRACT.CONTRACTORS value) {
            this.contractors = value;
        }

        /**
         * Obtient la valeur de la propriété values.
         *
         * @return possible object is
         * {@link AwardContractMove.AWARDEDCONTRACT.VALUES }
         */
        public AwardContractMove.AWARDEDCONTRACT.VALUES getVALUES() {
            return values;
        }

        /**
         * Définit la valeur de la propriété values.
         *
         * @param value allowed object is
         *              {@link AwardContractMove.AWARDEDCONTRACT.VALUES }
         */
        public void setVALUES(AwardContractMove.AWARDEDCONTRACT.VALUES value) {
            this.values = value;
        }

        /**
         * Obtient la valeur de la propriété nbkilometres.
         *
         * @return possible object is
         * {@link BigInteger }
         */
        public BigInteger getNBKILOMETRES() {
            return nbkilometres;
        }

        /**
         * Définit la valeur de la propriété nbkilometres.
         *
         * @param value allowed object is
         *              {@link BigInteger }
         */
        public void setNBKILOMETRES(BigInteger value) {
            this.nbkilometres = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="CONTRACTOR" maxOccurs="100"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="ADDRESS_CONTRACTOR" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contact_contractor_move"/&gt;
         *                   &lt;element name="OWNERSHIP" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}string_200" minOccurs="0"/&gt;
         *                   &lt;element name="ADDRESS_PARTY" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contact_party_move" maxOccurs="100" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "contractor"
        })
        @ToString
        @EqualsAndHashCode
        public static class CONTRACTORS {

            @XmlElement(name = "CONTRACTOR", required = true)
            protected List<AwardContractMove.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR> contractor;

            /**
             * Gets the value of the contractor property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the contractor property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCONTRACTOR().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link AwardContractMove.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR }
             */
            public List<AwardContractMove.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR> getCONTRACTOR() {
                if (contractor == null) {
                    contractor = new ArrayList<AwardContractMove.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR>();
                }
                return this.contractor;
            }


            /**
             * <p>Classe Java pour anonymous complex type.
             *
             * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="ADDRESS_CONTRACTOR" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contact_contractor_move"/&gt;
             *         &lt;element name="OWNERSHIP" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}string_200" minOccurs="0"/&gt;
             *         &lt;element name="ADDRESS_PARTY" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contact_party_move" maxOccurs="100" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "addresscontractor",
                    "ownership",
                    "addressparty"
            })
            @ToString
            @EqualsAndHashCode
            public static class CONTRACTOR {

                @XmlElement(name = "ADDRESS_CONTRACTOR", required = true)
                protected ContactContractorMove addresscontractor;
                @XmlElement(name = "OWNERSHIP")
                protected String ownership;
                @XmlElement(name = "ADDRESS_PARTY")
                protected List<ContactPartyMove> addressparty;

                /**
                 * Obtient la valeur de la propriété addresscontractor.
                 *
                 * @return possible object is
                 * {@link ContactContractorMove }
                 */
                public ContactContractorMove getADDRESSCONTRACTOR() {
                    return addresscontractor;
                }

                /**
                 * Définit la valeur de la propriété addresscontractor.
                 *
                 * @param value allowed object is
                 *              {@link ContactContractorMove }
                 */
                public void setADDRESSCONTRACTOR(ContactContractorMove value) {
                    this.addresscontractor = value;
                }

                /**
                 * Obtient la valeur de la propriété ownership.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getOWNERSHIP() {
                    return ownership;
                }

                /**
                 * Définit la valeur de la propriété ownership.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setOWNERSHIP(String value) {
                    this.ownership = value;
                }

                /**
                 * Gets the value of the addressparty property.
                 *
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the addressparty property.
                 *
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getADDRESSPARTY().add(newItem);
                 * </pre>
                 *
                 *
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link ContactPartyMove }
                 */
                public List<ContactPartyMove> getADDRESSPARTY() {
                    if (addressparty == null) {
                        addressparty = new ArrayList<ContactPartyMove>();
                    }
                    return this.addressparty;
                }

            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}VAL_TOTAL"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "valtotal"
        })
        @ToString
        @EqualsAndHashCode
        public static class VALUES {

            @XmlElement(name = "VAL_TOTAL", required = true)
            protected Val valtotal;

            /**
             * Obtient la valeur de la propriété valtotal.
             *
             * @return possible object is
             * {@link Val }
             */
            public Val getVALTOTAL() {
                return valtotal;
            }

            /**
             * Définit la valeur de la propriété valtotal.
             *
             * @param value allowed object is
             *              {@link Val }
             */
            public void setVALTOTAL(Val value) {
                this.valtotal = value;
            }

        }

    }

}
