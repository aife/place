//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour ac_definition complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="ac_definition"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}AC_CRITERION"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}AC_WEIGHTING"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ac_definition", propOrder = {
        "accriterion",
        "acweighting"
})
@ToString
@EqualsAndHashCode
public class AcDefinition {

    @XmlElement(name = "AC_CRITERION", required = true)
    protected String accriterion;
    @XmlElement(name = "AC_WEIGHTING", required = true)
    protected String acweighting;

    /**
     * Obtient la valeur de la propriété accriterion.
     *
     * @return possible object is
     * {@link String }
     */
    public String getACCRITERION() {
        return accriterion;
    }

    /**
     * Définit la valeur de la propriété accriterion.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setACCRITERION(String value) {
        this.accriterion = value;
    }

    /**
     * Obtient la valeur de la propriété acweighting.
     *
     * @return possible object is
     * {@link String }
     */
    public String getACWEIGHTING() {
        return acweighting;
    }

    /**
     * Définit la valeur de la propriété acweighting.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setACWEIGHTING(String value) {
        this.acweighting = value;
    }

}
