//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import eu.europa.publications.resource.schema.ted._2021.nuts.Nuts;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java pour contact complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="contact"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}OFFICIALNAME"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NATIONALID" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SIRET"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ADDRESS" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TOWN"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}POSTAL_CODE" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}COUNTRY"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CONTACT_POINT" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PHONE" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}E_MAIL"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}FAX" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/2016/nuts}NUTS"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}URL_GENERAL"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}URL_BUYER" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "contact", propOrder = {
        "officialname",
        "nationalid",
        "siret",
        "address",
        "town",
        "postalcode",
        "country",
        "contactpoint",
        "phone",
        "email",
        "fax",
        "nuts",
        "urlgeneral",
        "urlbuyer"
})
@XmlSeeAlso({
        ContactReview.class,
        ContactContractor.class,
        ContactContractingBody.class,
        ContactAddContractingBodyF14.class,
        ContactContractingBodyF14.class,
        ContactPartyMove.class,
        ContactContractorMove.class,
        ContactAddContractingBodyMove.class,
        ContactContractingBodyMove.class
})
@ToString
@EqualsAndHashCode
@Getter
@Setter
public class Contact {

    @XmlElement(name = "OFFICIALNAME", required = true)
    protected String officialname;
    @XmlElement(name = "NATIONALID")
    protected String nationalid;
    @XmlElement(name = "SIRET", required = true)
    protected String siret;
    @XmlElement(name = "ADDRESS")
    protected String address;
    @XmlElement(name = "TOWN", required = true)
    protected String town;
    @XmlElement(name = "POSTAL_CODE")
    protected String postalcode;
    @XmlElement(name = "COUNTRY", required = true)
    protected Country country;
    @XmlElement(name = "CONTACT_POINT")
    protected String contactpoint;
    @XmlElement(name = "PHONE")
    protected String phone;
    @XmlElement(name = "E_MAIL", required = true)
    protected String email;
    @XmlElement(name = "FAX")
    protected String fax;
    @XmlElement(name = "NUTS", namespace = "http://publications.europa.eu/resource/schema/ted/2016/nuts", required = true)
    protected Nuts nuts;
    @XmlElement(name = "URL_GENERAL", required = true)
    protected String urlgeneral;
    @XmlElement(name = "URL_BUYER")
    protected String urlbuyer;

    /**
     * Obtient la valeur de la propriété officialname.
     *
     * @return possible object is
     * {@link String }
     */
    public String getOFFICIALNAME() {
        return officialname;
    }

    /**
     * Définit la valeur de la propriété officialname.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setOFFICIALNAME(String value) {
        this.officialname = value;
    }

    /**
     * Obtient la valeur de la propriété nationalid.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNATIONALID() {
        return nationalid;
    }

    /**
     * Définit la valeur de la propriété nationalid.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNATIONALID(String value) {
        this.nationalid = value;
    }

    /**
     * Obtient la valeur de la propriété siret.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSIRET() {
        return siret;
    }

    /**
     * Définit la valeur de la propriété siret.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSIRET(String value) {
        this.siret = value;
    }

    /**
     * Obtient la valeur de la propriété address.
     *
     * @return possible object is
     * {@link String }
     */
    public String getADDRESS() {
        return address;
    }

    /**
     * Définit la valeur de la propriété address.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setADDRESS(String value) {
        this.address = value;
    }

    /**
     * Obtient la valeur de la propriété town.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTOWN() {
        return town;
    }

    /**
     * Définit la valeur de la propriété town.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTOWN(String value) {
        this.town = value;
    }

    /**
     * Obtient la valeur de la propriété postalcode.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPOSTALCODE() {
        return postalcode;
    }

    /**
     * Définit la valeur de la propriété postalcode.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPOSTALCODE(String value) {
        this.postalcode = value;
    }

    /**
     * Obtient la valeur de la propriété country.
     *
     * @return possible object is
     * {@link Country }
     */
    public Country getCOUNTRY() {
        return country;
    }

    /**
     * Définit la valeur de la propriété country.
     *
     * @param value allowed object is
     *              {@link Country }
     */
    public void setCOUNTRY(Country value) {
        this.country = value;
    }

    /**
     * Obtient la valeur de la propriété contactpoint.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCONTACTPOINT() {
        return contactpoint;
    }

    /**
     * Définit la valeur de la propriété contactpoint.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCONTACTPOINT(String value) {
        this.contactpoint = value;
    }

    /**
     * Obtient la valeur de la propriété phone.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPHONE() {
        return phone;
    }

    /**
     * Définit la valeur de la propriété phone.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPHONE(String value) {
        this.phone = value;
    }

    /**
     * Obtient la valeur de la propriété email.
     *
     * @return possible object is
     * {@link String }
     */
    public String getEMAIL() {
        return email;
    }

    /**
     * Définit la valeur de la propriété email.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setEMAIL(String value) {
        this.email = value;
    }

    /**
     * Obtient la valeur de la propriété fax.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFAX() {
        return fax;
    }

    /**
     * Définit la valeur de la propriété fax.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFAX(String value) {
        this.fax = value;
    }

    /**
     * Obtient la valeur de la propriété nuts.
     *
     * @return possible object is
     * {@link Nuts }
     */
    public Nuts getNUTS() {
        return nuts;
    }

    /**
     * Définit la valeur de la propriété nuts.
     *
     * @param value allowed object is
     *              {@link Nuts }
     */
    public void setNUTS(Nuts value) {
        this.nuts = value;
    }

    /**
     * Obtient la valeur de la propriété urlgeneral.
     *
     * @return possible object is
     * {@link String }
     */
    public String getURLGENERAL() {
        return urlgeneral;
    }

    /**
     * Définit la valeur de la propriété urlgeneral.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setURLGENERAL(String value) {
        this.urlgeneral = value;
    }

    /**
     * Obtient la valeur de la propriété urlbuyer.
     *
     * @return possible object is
     * {@link String }
     */
    public String getURLBUYER() {
        return urlbuyer;
    }

    /**
     * Définit la valeur de la propriété urlbuyer.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setURLBUYER(String value) {
        this.urlbuyer = value;
    }

}
