//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour cpv_codes complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="cpv_codes"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="CODE" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}t_cpv_code_list" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cpv_codes")
@ToString
@EqualsAndHashCode
public class CpvCodes {

    @XmlAttribute(name = "CODE", required = true)
    protected String code;

    /**
     * Obtient la valeur de la propriété code.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCODE() {
        return code;
    }

    /**
     * Définit la valeur de la propriété code.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCODE(String value) {
        this.code = value;
    }

}
