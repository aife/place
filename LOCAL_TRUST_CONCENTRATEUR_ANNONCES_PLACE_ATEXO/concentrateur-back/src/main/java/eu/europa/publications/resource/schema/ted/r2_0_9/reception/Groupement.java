//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour groupement complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="groupement"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="GROUPEMENT" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty"/&gt;
 *         &lt;element name="NO_GROUPEMENT" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "groupement", propOrder = {
        "groupement",
        "nogroupement"
})
@ToString
@EqualsAndHashCode
public class Groupement {

    @XmlElement(name = "GROUPEMENT")
    protected Empty groupement;
    @XmlElement(name = "NO_GROUPEMENT")
    protected Empty nogroupement;

    /**
     * Obtient la valeur de la propriété groupement.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getGROUPEMENT() {
        return groupement;
    }

    /**
     * Définit la valeur de la propriété groupement.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setGROUPEMENT(Empty value) {
        this.groupement = value;
    }

    /**
     * Obtient la valeur de la propriété nogroupement.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getNOGROUPEMENT() {
        return nogroupement;
    }

    /**
     * Définit la valeur de la propriété nogroupement.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setNOGROUPEMENT(Empty value) {
        this.nogroupement = value;
    }

}
