//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java pour ted_esenders complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="ted_esenders"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SENDER" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}sender"/&gt;
 *         &lt;element name="FORM_SECTION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}form_section"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="VERSION" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="R2.0.9.S04" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ted_esenders", propOrder = {
        "sender",
        "formsection"
})
@ToString
@EqualsAndHashCode
@XmlRootElement(name = "TED_ESENDERS")
public class TedEsenders {

    @XmlElement(name = "SENDER", required = true)
    protected Sender sender;
    @XmlElement(name = "FORM_SECTION", required = true)
    protected FormSection formsection;
    @XmlAttribute(name = "VERSION", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String version;

    /**
     * Obtient la valeur de la propriété sender.
     *
     * @return possible object is
     * {@link Sender }
     */
    public Sender getSENDER() {
        return sender;
    }

    /**
     * Définit la valeur de la propriété sender.
     *
     * @param value allowed object is
     *              {@link Sender }
     */
    public void setSENDER(Sender value) {
        this.sender = value;
    }

    /**
     * Obtient la valeur de la propriété formsection.
     *
     * @return possible object is
     * {@link FormSection }
     */
    public FormSection getFORMSECTION() {
        return formsection;
    }

    /**
     * Définit la valeur de la propriété formsection.
     *
     * @param value allowed object is
     *              {@link FormSection }
     */
    public void setFORMSECTION(FormSection value) {
        this.formsection = value;
    }

    /**
     * Obtient la valeur de la propriété version.
     *
     * @return possible object is
     * {@link String }
     */
    public String getVERSION() {
        if (version == null) {
            return "R2.0.9.S04";
        } else {
            return version;
        }
    }

    /**
     * Définit la valeur de la propriété version.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setVERSION(String value) {
        this.version = value;
    }

}
