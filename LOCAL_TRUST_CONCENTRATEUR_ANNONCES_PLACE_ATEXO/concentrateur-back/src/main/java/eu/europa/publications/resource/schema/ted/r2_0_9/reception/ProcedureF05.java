//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import fr.atexo.annonces.commun.tncp.TncpUsageNegociationEnum;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Locale;


/**
 * Section IV: PROCEDURE
 *
 * <p>Classe Java pour procedure_f05 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="procedure_f05"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}type_proc"/&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_OPEN"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_RESTRICTED"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_NEGOTIATED_WITH_PRIOR_CALL"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_COMPETITIVE_DIALOGUE"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_INNOVATION_PARTNERSHIP"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_01_AOO"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_03_MAPA"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_04_Concours"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_12_Autre"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_13_AccordCadreSelection"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_15_SAD"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_08_MarcheNegocie"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_18_PCN"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_06_DialogueCompetitif"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_02_AOR"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_05_ConcoursRestreint"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_14_MarcheSubsequent"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_16_MarcheSpecifique"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="FRAMEWORK" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}framework_info" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}dps_purchasers" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}REDUCTION_RECOURSE" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}eauction" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}gpa"/&gt;
 *         &lt;element name="TECHNIQUE_ACHAT" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_single_line"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NOTICE_NUMBER_OJ" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}receipt_tenders"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DATE_DISPATCH_INVITATIONS" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NB_CANDIDATS_MAX" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LANGUAGES"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}attribution_nego"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}visite_obligatoire"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}time_frame_tender_valid" minOccurs="0"/&gt;
 *         &lt;element name="CATALOGUE_ELECTRONIQUE" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_single_line"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}OPENING_CONDITION" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "procedure_f05", propOrder = {
        "po",
        "pr",
        "ptopen",
        "ptrestricted",
        "ptnegotiatedwithpriorcall",
        "ptcompetitivedialogue",
        "ptinnovationpartnership",
        "pt01AOO",
        "pt03MAPA",
        "pt04Concours",
        "pt12Autre",
        "pt13AccordCadreSelection",
        "pt15SAD",
        "pt08MarcheNegocie",
        "pt18PCN",
        "pt06DialogueCompetitif",
        "pt02AOR",
        "pt05ConcoursRestreint",
        "pt14MarcheSubsequent",
        "pt16MarcheSpecifique",
        "framework",
        "dps",
        "dpsadditionalpurchasers",
        "reductionrecourse",
        "eauctionused",
        "infoaddeauction",
        "contractcoveredgpa",
        "nocontractcoveredgpa",
        "techniqueachat",
        "noticenumberoj",
        "datereceipttenders",
        "timereceipttenders",
        "datedispatchinvitations",
        "nbcandidatsmax",
        "languages",
        "conditions",
        "attribsansnego",
        "noattribsansnego",
        "visiteobligatoire",
        "novisiteobligatoire",
        "datetendervalid",
        "durationtendervalid",
        "catalogueelectronique",
        "openingcondition"
})
@ToString
@EqualsAndHashCode
public class ProcedureF05 {

    @XmlElement(name = "PO")
    protected Empty po;
    @XmlElement(name = "PR")
    protected Empty pr;
    @XmlElement(name = "PT_OPEN")
    protected Empty ptopen;
    @XmlElement(name = "PT_RESTRICTED")
    protected Empty ptrestricted;
    @XmlElement(name = "PT_NEGOTIATED_WITH_PRIOR_CALL")
    protected Empty ptnegotiatedwithpriorcall;
    @XmlElement(name = "PT_COMPETITIVE_DIALOGUE")
    protected Empty ptcompetitivedialogue;
    @XmlElement(name = "PT_INNOVATION_PARTNERSHIP")
    protected Empty ptinnovationpartnership;
    @XmlElement(name = "PT_01_AOO")
    protected Empty pt01AOO;
    @XmlElement(name = "PT_03_MAPA")
    protected Empty pt03MAPA;
    @XmlElement(name = "PT_04_Concours")
    protected Empty pt04Concours;
    @XmlElement(name = "PT_12_Autre")
    protected Empty pt12Autre;
    @XmlElement(name = "PT_13_AccordCadreSelection")
    protected Empty pt13AccordCadreSelection;
    @XmlElement(name = "PT_15_SAD")
    protected Empty pt15SAD;
    @XmlElement(name = "PT_08_MarcheNegocie")
    protected Empty pt08MarcheNegocie;
    @XmlElement(name = "PT_18_PCN")
    protected Empty pt18PCN;
    @XmlElement(name = "PT_06_DialogueCompetitif")
    protected Empty pt06DialogueCompetitif;
    @XmlElement(name = "PT_02_AOR")
    protected Empty pt02AOR;
    @XmlElement(name = "PT_05_ConcoursRestreint")
    protected Empty pt05ConcoursRestreint;
    @XmlElement(name = "PT_14_MarcheSubsequent")
    protected Empty pt14MarcheSubsequent;
    @XmlElement(name = "PT_16_MarcheSpecifique")
    protected Empty pt16MarcheSpecifique;
    @XmlElement(name = "FRAMEWORK")
    protected FrameworkInfo framework;
    @XmlElement(name = "DPS")
    protected Empty dps;
    @XmlElement(name = "DPS_ADDITIONAL_PURCHASERS")
    protected Empty dpsadditionalpurchasers;
    @XmlElement(name = "REDUCTION_RECOURSE")
    protected Empty reductionrecourse;
    @XmlElement(name = "EAUCTION_USED")
    protected Empty eauctionused;
    @XmlElement(name = "INFO_ADD_EAUCTION")
    protected TextFtMultiLines infoaddeauction;
    @XmlElement(name = "CONTRACT_COVERED_GPA")
    protected Empty contractcoveredgpa;
    @XmlElement(name = "NO_CONTRACT_COVERED_GPA")
    protected Empty nocontractcoveredgpa;
    @XmlElement(name = "TECHNIQUE_ACHAT", required = true)
    protected String techniqueachat;
    @XmlElement(name = "NOTICE_NUMBER_OJ")
    protected String noticenumberoj;
    @XmlElement(name = "DATE_RECEIPT_TENDERS", required = true)
    protected String datereceipttenders;
    @XmlElement(name = "TIME_RECEIPT_TENDERS")
    protected String timereceipttenders;
    @XmlElement(name = "DATE_DISPATCH_INVITATIONS")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datedispatchinvitations;
    @XmlElement(name = "NB_CANDIDATS_MAX")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger nbcandidatsmax;
    @XmlElement(name = "LANGUAGES", required = true)
    protected LANGUAGES languages;
    @XmlElement(name = "CONDITIONS")
    protected Conditions conditions;
    @XmlElement(name = "ATTRIB_SANS_NEGO")
    protected Empty attribsansnego;
    @XmlElement(name = "NO_ATTRIB_SANS_NEGO")
    protected Empty noattribsansnego;
    @XmlElement(name = "VISITE_OBLIGATOIRE")
    protected String visiteobligatoire;
    @XmlElement(name = "NO_VISITE_OBLIGATOIRE")
    protected Empty novisiteobligatoire;
    @XmlElement(name = "DATE_TENDER_VALID")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datetendervalid;
    @XmlElement(name = "DURATION_TENDER_VALID")
    protected ProcedureF05.DURATIONTENDERVALID durationtendervalid;
    @XmlElement(name = "CATALOGUE_ELECTRONIQUE", required = true)
    protected String catalogueelectronique;
    @XmlElement(name = "OPENING_CONDITION")
    protected CondForOpeningTenders openingcondition;

    /**
     * Obtient la valeur de la propriété po.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPO() {
        return po;
    }

    /**
     * Définit la valeur de la propriété po.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPO(Empty value) {
        this.po = value;
    }

    /**
     * Obtient la valeur de la propriété pr.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPR() {
        return pr;
    }

    /**
     * Définit la valeur de la propriété pr.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPR(Empty value) {
        this.pr = value;
    }

    /**
     * Obtient la valeur de la propriété ptopen.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTOPEN() {
        return ptopen;
    }

    /**
     * Définit la valeur de la propriété ptopen.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTOPEN(Empty value) {
        this.ptopen = value;
    }

    /**
     * Obtient la valeur de la propriété ptrestricted.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTRESTRICTED() {
        return ptrestricted;
    }

    /**
     * Définit la valeur de la propriété ptrestricted.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTRESTRICTED(Empty value) {
        this.ptrestricted = value;
    }

    /**
     * Obtient la valeur de la propriété ptnegotiatedwithpriorcall.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTNEGOTIATEDWITHPRIORCALL() {
        return ptnegotiatedwithpriorcall;
    }

    /**
     * Définit la valeur de la propriété ptnegotiatedwithpriorcall.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTNEGOTIATEDWITHPRIORCALL(Empty value) {
        this.ptnegotiatedwithpriorcall = value;
    }

    /**
     * Obtient la valeur de la propriété ptcompetitivedialogue.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTCOMPETITIVEDIALOGUE() {
        return ptcompetitivedialogue;
    }

    /**
     * Définit la valeur de la propriété ptcompetitivedialogue.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTCOMPETITIVEDIALOGUE(Empty value) {
        this.ptcompetitivedialogue = value;
    }

    /**
     * Obtient la valeur de la propriété ptinnovationpartnership.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTINNOVATIONPARTNERSHIP() {
        return ptinnovationpartnership;
    }

    /**
     * Définit la valeur de la propriété ptinnovationpartnership.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTINNOVATIONPARTNERSHIP(Empty value) {
        this.ptinnovationpartnership = value;
    }

    /**
     * Obtient la valeur de la propriété pt01AOO.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPT01AOO() {
        return pt01AOO;
    }

    /**
     * Définit la valeur de la propriété pt01AOO.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPT01AOO(Empty value) {
        this.pt01AOO = value;
    }

    /**
     * Obtient la valeur de la propriété pt03MAPA.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPT03MAPA() {
        return pt03MAPA;
    }

    /**
     * Définit la valeur de la propriété pt03MAPA.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPT03MAPA(Empty value) {
        this.pt03MAPA = value;
    }

    /**
     * Obtient la valeur de la propriété pt04Concours.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPT04Concours() {
        return pt04Concours;
    }

    /**
     * Définit la valeur de la propriété pt04Concours.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPT04Concours(Empty value) {
        this.pt04Concours = value;
    }

    /**
     * Obtient la valeur de la propriété pt12Autre.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPT12Autre() {
        return pt12Autre;
    }

    /**
     * Définit la valeur de la propriété pt12Autre.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPT12Autre(Empty value) {
        this.pt12Autre = value;
    }

    /**
     * Obtient la valeur de la propriété pt13AccordCadreSelection.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPT13AccordCadreSelection() {
        return pt13AccordCadreSelection;
    }

    /**
     * Définit la valeur de la propriété pt13AccordCadreSelection.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPT13AccordCadreSelection(Empty value) {
        this.pt13AccordCadreSelection = value;
    }

    /**
     * Obtient la valeur de la propriété pt15SAD.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPT15SAD() {
        return pt15SAD;
    }

    /**
     * Définit la valeur de la propriété pt15SAD.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPT15SAD(Empty value) {
        this.pt15SAD = value;
    }

    /**
     * Obtient la valeur de la propriété pt08MarcheNegocie.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPT08MarcheNegocie() {
        return pt08MarcheNegocie;
    }

    /**
     * Définit la valeur de la propriété pt08MarcheNegocie.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPT08MarcheNegocie(Empty value) {
        this.pt08MarcheNegocie = value;
    }

    /**
     * Obtient la valeur de la propriété pt18PCN.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPT18PCN() {
        return pt18PCN;
    }

    /**
     * Définit la valeur de la propriété pt18PCN.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPT18PCN(Empty value) {
        this.pt18PCN = value;
    }

    /**
     * Obtient la valeur de la propriété pt06DialogueCompetitif.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPT06DialogueCompetitif() {
        return pt06DialogueCompetitif;
    }

    /**
     * Définit la valeur de la propriété pt06DialogueCompetitif.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPT06DialogueCompetitif(Empty value) {
        this.pt06DialogueCompetitif = value;
    }

    /**
     * Obtient la valeur de la propriété pt02AOR.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPT02AOR() {
        return pt02AOR;
    }

    /**
     * Définit la valeur de la propriété pt02AOR.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPT02AOR(Empty value) {
        this.pt02AOR = value;
    }

    /**
     * Obtient la valeur de la propriété pt05ConcoursRestreint.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPT05ConcoursRestreint() {
        return pt05ConcoursRestreint;
    }

    /**
     * Définit la valeur de la propriété pt05ConcoursRestreint.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPT05ConcoursRestreint(Empty value) {
        this.pt05ConcoursRestreint = value;
    }

    /**
     * Obtient la valeur de la propriété pt14MarcheSubsequent.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPT14MarcheSubsequent() {
        return pt14MarcheSubsequent;
    }

    /**
     * Définit la valeur de la propriété pt14MarcheSubsequent.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPT14MarcheSubsequent(Empty value) {
        this.pt14MarcheSubsequent = value;
    }

    /**
     * Obtient la valeur de la propriété pt16MarcheSpecifique.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPT16MarcheSpecifique() {
        return pt16MarcheSpecifique;
    }

    /**
     * Définit la valeur de la propriété pt16MarcheSpecifique.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPT16MarcheSpecifique(Empty value) {
        this.pt16MarcheSpecifique = value;
    }

    /**
     * Obtient la valeur de la propriété framework.
     *
     * @return possible object is
     * {@link FrameworkInfo }
     */
    public FrameworkInfo getFRAMEWORK() {
        return framework;
    }

    /**
     * Définit la valeur de la propriété framework.
     *
     * @param value allowed object is
     *              {@link FrameworkInfo }
     */
    public void setFRAMEWORK(FrameworkInfo value) {
        this.framework = value;
    }

    /**
     * Obtient la valeur de la propriété dps.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getDPS() {
        return dps;
    }

    /**
     * Définit la valeur de la propriété dps.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setDPS(Empty value) {
        this.dps = value;
    }

    /**
     * Obtient la valeur de la propriété dpsadditionalpurchasers.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getDPSADDITIONALPURCHASERS() {
        return dpsadditionalpurchasers;
    }

    /**
     * Définit la valeur de la propriété dpsadditionalpurchasers.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setDPSADDITIONALPURCHASERS(Empty value) {
        this.dpsadditionalpurchasers = value;
    }

    /**
     * Obtient la valeur de la propriété reductionrecourse.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getREDUCTIONRECOURSE() {
        return reductionrecourse;
    }

    /**
     * Définit la valeur de la propriété reductionrecourse.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setREDUCTIONRECOURSE(Empty value) {
        this.reductionrecourse = value;
    }

    /**
     * Obtient la valeur de la propriété eauctionused.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getEAUCTIONUSED() {
        return eauctionused;
    }

    /**
     * Définit la valeur de la propriété eauctionused.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setEAUCTIONUSED(Empty value) {
        this.eauctionused = value;
    }

    /**
     * Obtient la valeur de la propriété infoaddeauction.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getINFOADDEAUCTION() {
        return infoaddeauction;
    }

    /**
     * Définit la valeur de la propriété infoaddeauction.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setINFOADDEAUCTION(TextFtMultiLines value) {
        this.infoaddeauction = value;
    }

    /**
     * Obtient la valeur de la propriété contractcoveredgpa.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getCONTRACTCOVEREDGPA() {
        return contractcoveredgpa;
    }

    /**
     * Définit la valeur de la propriété contractcoveredgpa.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setCONTRACTCOVEREDGPA(Empty value) {
        this.contractcoveredgpa = value;
    }

    /**
     * Obtient la valeur de la propriété nocontractcoveredgpa.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getNOCONTRACTCOVEREDGPA() {
        return nocontractcoveredgpa;
    }

    /**
     * Définit la valeur de la propriété nocontractcoveredgpa.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setNOCONTRACTCOVEREDGPA(Empty value) {
        this.nocontractcoveredgpa = value;
    }

    /**
     * Obtient la valeur de la propriété techniqueachat.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTECHNIQUEACHAT() {
        return techniqueachat;
    }

    /**
     * Définit la valeur de la propriété techniqueachat.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTECHNIQUEACHAT(String value) {
        this.techniqueachat = value;
    }

    /**
     * Obtient la valeur de la propriété noticenumberoj.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNOTICENUMBEROJ() {
        return noticenumberoj;
    }

    /**
     * Définit la valeur de la propriété noticenumberoj.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNOTICENUMBEROJ(String value) {
        this.noticenumberoj = value;
    }

    /**
     * Obtient la valeur de la propriété datereceipttenders.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public String getDATERECEIPTTENDERS() {
        return datereceipttenders;
    }

    /**
     * Définit la valeur de la propriété datereceipttenders.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDATERECEIPTTENDERS(String value) {
        this.datereceipttenders = value;
    }

    /**
     * Obtient la valeur de la propriété timereceipttenders.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTIMERECEIPTTENDERS() {
        return timereceipttenders;
    }

    /**
     * Définit la valeur de la propriété timereceipttenders.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTIMERECEIPTTENDERS(String value) {
        this.timereceipttenders = value;
    }

    /**
     * Obtient la valeur de la propriété datedispatchinvitations.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDATEDISPATCHINVITATIONS() {
        return datedispatchinvitations;
    }

    /**
     * Définit la valeur de la propriété datedispatchinvitations.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDATEDISPATCHINVITATIONS(XMLGregorianCalendar value) {
        this.datedispatchinvitations = value;
    }

    /**
     * Obtient la valeur de la propriété nbcandidatsmax.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public BigInteger getNBCANDIDATSMAX() {
        return nbcandidatsmax;
    }

    /**
     * Définit la valeur de la propriété nbcandidatsmax.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public void setNBCANDIDATSMAX(BigInteger value) {
        this.nbcandidatsmax = value;
    }

    /**
     * Obtient la valeur de la propriété languages.
     *
     * @return possible object is
     * {@link LANGUAGES }
     */
    public LANGUAGES getLANGUAGES() {
        return languages;
    }

    /**
     * Définit la valeur de la propriété languages.
     *
     * @param value allowed object is
     *              {@link LANGUAGES }
     */
    public void setLANGUAGES(LANGUAGES value) {
        this.languages = value;
    }

    public Conditions getConditions() {
        return conditions;
    }

    /**
     * Définit la valeur de la propriété languages.
     *
     * @param value allowed object is
     *              {@link LANGUAGES }
     */
    public void setConditions(Conditions value) {
        this.conditions = value;
    }

    /**
     * Obtient la valeur de la propriété attribsansnego.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getATTRIBSANSNEGO() {
        return attribsansnego;
    }

    /**
     * Définit la valeur de la propriété attribsansnego.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setATTRIBSANSNEGO(Empty value) {
        this.attribsansnego = value;
    }

    /**
     * Obtient la valeur de la propriété noattribsansnego.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getNOATTRIBSANSNEGO() {
        return noattribsansnego;
    }

    /**
     * Définit la valeur de la propriété noattribsansnego.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setNOATTRIBSANSNEGO(Empty value) {
        this.noattribsansnego = value;
    }

    /**
     * Obtient la valeur de la propriété visiteobligatoire.
     *
     * @return possible object is
     * {@link TextFtSingleLine }
     */
    public String getVISITEOBLIGATOIRE() {
        return visiteobligatoire;
    }

    /**
     * Définit la valeur de la propriété visiteobligatoire.
     *
     * @param value allowed object is
     *              {@link TextFtSingleLine }
     */
    public void setVISITEOBLIGATOIRE(String value) {
        this.visiteobligatoire = value;
    }

    /**
     * Obtient la valeur de la propriété novisiteobligatoire.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getNOVISITEOBLIGATOIRE() {
        return novisiteobligatoire;
    }

    /**
     * Définit la valeur de la propriété novisiteobligatoire.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setNOVISITEOBLIGATOIRE(Empty value) {
        this.novisiteobligatoire = value;
    }

    /**
     * Obtient la valeur de la propriété datetendervalid.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDATETENDERVALID() {
        return datetendervalid;
    }

    /**
     * Définit la valeur de la propriété datetendervalid.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDATETENDERVALID(XMLGregorianCalendar value) {
        this.datetendervalid = value;
    }

    /**
     * Obtient la valeur de la propriété durationtendervalid.
     *
     * @return possible object is
     * {@link ProcedureF05 .DURATIONTENDERVALID }
     */
    public ProcedureF05.DURATIONTENDERVALID getDURATIONTENDERVALID() {
        return durationtendervalid;
    }

    /**
     * Définit la valeur de la propriété durationtendervalid.
     *
     * @param value allowed object is
     *              {@link ProcedureF05 .DURATIONTENDERVALID }
     */
    public void setDURATIONTENDERVALID(ProcedureF05.DURATIONTENDERVALID value) {
        this.durationtendervalid = value;
    }

    /**
     * Obtient la valeur de la propriété catalogueelectronique.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCATALOGUEELECTRONIQUE() {
        return catalogueelectronique;
    }

    /**
     * Définit la valeur de la propriété catalogueelectronique.
     *
     * @param value allowed object is
     *              {@link TextFtSingleLine }
     */
    public void setCATALOGUEELECTRONIQUE(String value) {
        this.catalogueelectronique = value;
    }

    /**
     * Obtient la valeur de la propriété openingcondition.
     *
     * @return possible object is
     * {@link CondForOpeningTenders }
     */
    public CondForOpeningTenders getOPENINGCONDITION() {
        return openingcondition;
    }

    /**
     * Définit la valeur de la propriété openingcondition.
     *
     * @param value allowed object is
     *              {@link CondForOpeningTenders }
     */
    public void setOPENINGCONDITION(CondForOpeningTenders value) {
        this.openingcondition = value;
    }

    public String getTypeProcedure() {
        if (this.po != null) {
            return "Procédure adaptée ouverte";
        }
        if (this.pr != null) {
            return "Procédure adaptée restreinte";
        }
        return null;
    }

    public String getTypeProcedureLibelle() {
        if (this.pt01AOO != null) {
            return "Appel d'offre ouvert";
        }
        if (this.ptopen != null) {
            return "Appel d'offre ouvert";
        }
        if (this.ptrestricted != null) {
            return "Appel d'offre restreint";
        }
        if (this.ptcompetitivedialogue != null) {
            return "Dialogue compétitif";
        }
        if (this.ptinnovationpartnership != null) {
            return "Autre";
        }
        if (this.pt03MAPA != null) {
            return "Procédure adaptée";
        }
        if (this.pt04Concours != null) {
            return "Concours ouvert";
        }
        if (this.pt12Autre != null) {
            return "Autre";
        }
        if (this.pt13AccordCadreSelection != null) {
            return "Autre";
        }
        if (this.pt15SAD != null) {
            return "Autre";
        }
        if (this.pt08MarcheNegocie != null) {
            return "Procédure négociée";
        }
        if (this.pt18PCN != null) {
            return "Procédure négociée";
        }
        if (this.pt06DialogueCompetitif != null) {
            return "Dialogue compétitif";
        }
        if (this.pt02AOR != null) {
            return "Appel d'offre restreint";
        }
        if (this.pt05ConcoursRestreint != null) {
            return "Concours restreint";
        }
        if (this.pt14MarcheSubsequent != null) {
            return "Autre";
        }
        if (this.pt16MarcheSpecifique != null) {
            return "Autre";
        }
        return "Autre";
    }


    public String getTypeProcedureCode() {
        if (this.pt01AOO != null) {
            return "AOO";
        }
        if (this.ptopen != null) {
            return "open";
        }
        if (this.ptrestricted != null) {
            return "restricted";
        }
        if (this.ptcompetitivedialogue != null) {
            return "competitivedialogue";
        }
        if (this.ptinnovationpartnership != null) {
            return "innovationpartnership";
        }
        if (this.pt03MAPA != null) {
            return "MAPA";
        }
        if (this.pt04Concours != null) {
            return "Concours";
        }
        if (this.pt12Autre != null) {
            return "Autre";
        }
        if (this.pt13AccordCadreSelection != null) {
            return "AccordCadreSelection";
        }
        if (this.pt15SAD != null) {
            return "SAD";
        }
        if (this.pt08MarcheNegocie != null) {
            return "MarcheNegocie";
        }
        if (this.pt18PCN != null) {
            return "PCN";
        }
        if (this.pt06DialogueCompetitif != null) {
            return "DialogueCompetitif";
        }
        if (this.pt02AOR != null) {
            return "AOR";
        }
        if (this.pt05ConcoursRestreint != null) {
            return "ConcoursRestreint";
        }
        if (this.pt14MarcheSubsequent != null) {
            return "MarcheSubsequent";
        }
        if (this.pt16MarcheSpecifique != null) {
            return "MarcheSpecifique";
        }
        return "Autre";
    }

    public String getIsAttribSansnEgo() {
        return this.attribsansnego != null ? "Oui" : "Non";
    }
    public TncpUsageNegociationEnum getUsageNegociation() {
        return this.attribsansnego != null ? TncpUsageNegociationEnum.USAGE_POSSIBLE :
                (this.noattribsansnego != null ? TncpUsageNegociationEnum.USAGE_CERTAIN : TncpUsageNegociationEnum.NON_UTILISE);
    }

    public String getIsVisiteobligatoire() {
        return this.novisiteobligatoire == null ? "Oui" : "Non";
    }

    public String getIsReduit() {
        return this.nbcandidatsmax == null ? "Non" : "Oui";
    }

    public String getDateLimiteReponse() {
        if (this.datereceipttenders == null || this.timereceipttenders == null) {
            return null;
        }

        String dateString = this.datereceipttenders + " " + this.timereceipttenders;
        try {
            Date date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(dateString);
            String dateFormat = "dd MMMMM yyyy 'à' HH:mm";
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, new Locale("FR", "fr"));
            return sdf.format(date1.getTime());
        } catch (ParseException e) {
            return null;
        }
    }

    public ZonedDateTime getDLRO() {
        if (this.datereceipttenders == null || this.timereceipttenders == null) {
            return null;
        }

        String dateString = this.datereceipttenders + " " + this.timereceipttenders;
        try {
            Date date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(dateString);
            return ZonedDateTime.ofInstant(date1.toInstant(), ZoneId.systemDefault());
        } catch (ParseException e) {
            return null;
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "activiteProfessionel",
            "economiqueFinanciere",
            "techniquesProfessionels"
    })
    @ToString
    @EqualsAndHashCode
    public static class Conditions {

        @XmlElement(name = "ACTIVITE_PROFESSIONEL")
        protected String activiteProfessionel;
        @XmlElement(name = "ECONOMIQUE_FINANCIERE")
        protected String economiqueFinanciere;
        @XmlElement(name = "TECHNIQUES_PROFESSIONELS")
        protected String techniquesProfessionels;

        public String getActiviteProfessionel() {
            return activiteProfessionel;
        }

        public void setActiviteProfessionel(String activiteProfessionel) {
            this.activiteProfessionel = activiteProfessionel;
        }

        public String getEconomiqueFinanciere() {
            return economiqueFinanciere;
        }

        public void setEconomiqueFinanciere(String economiqueFinanciere) {
            this.economiqueFinanciere = economiqueFinanciere;
        }

        public String getTechniquesProfessionels() {
            return techniquesProfessionels;
        }

        public void setTechniquesProfessionels(String techniquesProfessionels) {
            this.techniquesProfessionels = techniquesProfessionels;
        }
    }

    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;duration_value_3d"&gt;
     *       &lt;attribute name="TYPE" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="MONTH" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "value"
    })
    @ToString
    @EqualsAndHashCode
    public static class DURATIONTENDERVALID {

        @XmlValue
        protected BigInteger value;
        @XmlAttribute(name = "TYPE", required = true)
        @XmlSchemaType(name = "anySimpleType")
        protected String type;

        /**
         * Format 999
         *
         * @return possible object is
         * {@link BigInteger }
         */
        public BigInteger getValue() {
            return value;
        }

        /**
         * Définit la valeur de la propriété value.
         *
         * @param value allowed object is
         *              {@link BigInteger }
         */
        public void setValue(BigInteger value) {
            this.value = value;
        }

        /**
         * Obtient la valeur de la propriété type.
         *
         * @return possible object is
         * {@link String }
         */
        public String getTYPE() {
            if (type == null) {
                return "MONTH";
            } else {
                return type;
            }
        }

        /**
         * Définit la valeur de la propriété type.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setTYPE(String value) {
            this.type = value;
        }

    }

}
