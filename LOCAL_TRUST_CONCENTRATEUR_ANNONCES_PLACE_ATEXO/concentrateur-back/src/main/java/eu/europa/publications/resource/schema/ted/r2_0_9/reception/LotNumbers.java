//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java pour lot_numbers complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="lot_numbers"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LOT_ALL"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LOT_MAX_NUMBER"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LOT_ONE_ONLY"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LOT_MAX_ONE_TENDERER" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LOT_COMBINING_CONTRACT_RIGHT" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "lot_numbers", propOrder = {
        "lotall",
        "lotmaxnumber",
        "lotoneonly",
        "lotmaxonetenderer",
        "lotcombiningcontractright"
})
@XmlSeeAlso({
        LotDivisionF01.class,
        LotDivisionF02.class,
        LotDivisionF04.class,
        LotDivisionF05.class,
        LotDivisionF21.class,
        LotDivisionF22.class,
        LotDivisionF23.class,
        LotDivisionF24.class
})
@ToString
@EqualsAndHashCode
public class LotNumbers {

    @XmlElement(name = "LOT_ALL")
    protected Empty lotall;
    @XmlElement(name = "LOT_MAX_NUMBER")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected Integer lotmaxnumber;
    @XmlElement(name = "LOT_ONE_ONLY")
    protected Empty lotoneonly;
    @XmlElement(name = "LOT_MAX_ONE_TENDERER")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected Integer lotmaxonetenderer;
    @XmlElement(name = "LOT_COMBINING_CONTRACT_RIGHT")
    protected TextFtMultiLines lotcombiningcontractright;

    /**
     * Obtient la valeur de la propriété lotall.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getLOTALL() {
        return lotall;
    }

    /**
     * Définit la valeur de la propriété lotall.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setLOTALL(Empty value) {
        this.lotall = value;
    }

    /**
     * Obtient la valeur de la propriété lotmaxnumber.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getLOTMAXNUMBER() {
        return lotmaxnumber;
    }

    /**
     * Définit la valeur de la propriété lotmaxnumber.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setLOTMAXNUMBER(Integer value) {
        this.lotmaxnumber = value;
    }

    /**
     * Obtient la valeur de la propriété lotoneonly.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getLOTONEONLY() {
        return lotoneonly;
    }

    /**
     * Définit la valeur de la propriété lotoneonly.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setLOTONEONLY(Empty value) {
        this.lotoneonly = value;
    }

    /**
     * Obtient la valeur de la propriété lotmaxonetenderer.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getLOTMAXONETENDERER() {
        return lotmaxonetenderer;
    }

    /**
     * Définit la valeur de la propriété lotmaxonetenderer.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setLOTMAXONETENDERER(Integer value) {
        this.lotmaxonetenderer = value;
    }

    /**
     * Obtient la valeur de la propriété lotcombiningcontractright.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getLOTCOMBININGCONTRACTRIGHT() {
        return lotcombiningcontractright;
    }

    /**
     * Définit la valeur de la propriété lotcombiningcontractright.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setLOTCOMBININGCONTRACTRIGHT(TextFtMultiLines value) {
        this.lotcombiningcontractright = value;
    }

}
