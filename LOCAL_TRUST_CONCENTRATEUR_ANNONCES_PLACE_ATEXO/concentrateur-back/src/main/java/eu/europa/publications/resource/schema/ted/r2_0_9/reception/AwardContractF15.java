//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * Section V: AWARD OF CONTRACT
 *
 * <p>Classe Java pour award_contract_f15 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="award_contract_f15"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contract_number"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TITLE" minOccurs="0"/&gt;
 *         &lt;element name="AWARDED_CONTRACT"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DATE_CONCLUSION_CONTRACT"/&gt;
 *                   &lt;element name="CONTRACTORS"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_opt"&gt;
 *                           &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contractor"/&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="VALUES"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_opt"&gt;
 *                           &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_contract_value"/&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;sequence minOccurs="0"&gt;
 *                     &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LIKELY_SUBCONTRACTED"/&gt;
 *                     &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}subcontracting"/&gt;
 *                     &lt;element name="DIRECTIVE_2009_81_EC" minOccurs="0"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;choice&gt;
 *                               &lt;sequence&gt;
 *                                 &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}AWARDED_SUBCONTRACTING"/&gt;
 *                                 &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PCT_RANGE_SHARE_SUBCONTRACTING" minOccurs="0"/&gt;
 *                               &lt;/sequence&gt;
 *                               &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PCT_RANGE_SHARE_SUBCONTRACTING"/&gt;
 *                             &lt;/choice&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                   &lt;/sequence&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ITEM" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}nb_contract" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "award_contract_f15", propOrder = {
        "contractno",
        "lotno",
        "title",
        "awardedcontract"
})
@ToString
@EqualsAndHashCode
public class AwardContractF15 {

    @XmlElement(name = "CONTRACT_NO")
    protected String contractno;
    @XmlElement(name = "LOT_NO")
    protected String lotno;
    @XmlElement(name = "TITLE")
    protected TextFtSingleLine title;
    @XmlElement(name = "AWARDED_CONTRACT", required = true)
    protected AwardContractF15.AWARDEDCONTRACT awardedcontract;
    @XmlAttribute(name = "ITEM", required = true)
    protected int item;

    /**
     * Obtient la valeur de la propriété contractno.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCONTRACTNO() {
        return contractno;
    }

    /**
     * Définit la valeur de la propriété contractno.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCONTRACTNO(String value) {
        this.contractno = value;
    }

    /**
     * Obtient la valeur de la propriété lotno.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLOTNO() {
        return lotno;
    }

    /**
     * Définit la valeur de la propriété lotno.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLOTNO(String value) {
        this.lotno = value;
    }

    /**
     * Obtient la valeur de la propriété title.
     *
     * @return possible object is
     * {@link TextFtSingleLine }
     */
    public TextFtSingleLine getTITLE() {
        return title;
    }

    /**
     * Définit la valeur de la propriété title.
     *
     * @param value allowed object is
     *              {@link TextFtSingleLine }
     */
    public void setTITLE(TextFtSingleLine value) {
        this.title = value;
    }

    /**
     * Obtient la valeur de la propriété awardedcontract.
     *
     * @return possible object is
     * {@link AwardContractF15 .AWARDEDCONTRACT }
     */
    public AwardContractF15.AWARDEDCONTRACT getAWARDEDCONTRACT() {
        return awardedcontract;
    }

    /**
     * Définit la valeur de la propriété awardedcontract.
     *
     * @param value allowed object is
     *              {@link AwardContractF15 .AWARDEDCONTRACT }
     */
    public void setAWARDEDCONTRACT(AwardContractF15.AWARDEDCONTRACT value) {
        this.awardedcontract = value;
    }

    /**
     * Obtient la valeur de la propriété item.
     */
    public int getITEM() {
        return item;
    }

    /**
     * Définit la valeur de la propriété item.
     */
    public void setITEM(int value) {
        this.item = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DATE_CONCLUSION_CONTRACT"/&gt;
     *         &lt;element name="CONTRACTORS"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_opt"&gt;
     *                 &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contractor"/&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="VALUES"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_opt"&gt;
     *                 &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_contract_value"/&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;sequence minOccurs="0"&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LIKELY_SUBCONTRACTED"/&gt;
     *           &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}subcontracting"/&gt;
     *           &lt;element name="DIRECTIVE_2009_81_EC" minOccurs="0"&gt;
     *             &lt;complexType&gt;
     *               &lt;complexContent&gt;
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                   &lt;choice&gt;
     *                     &lt;sequence&gt;
     *                       &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}AWARDED_SUBCONTRACTING"/&gt;
     *                       &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PCT_RANGE_SHARE_SUBCONTRACTING" minOccurs="0"/&gt;
     *                     &lt;/sequence&gt;
     *                     &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PCT_RANGE_SHARE_SUBCONTRACTING"/&gt;
     *                   &lt;/choice&gt;
     *                 &lt;/restriction&gt;
     *               &lt;/complexContent&gt;
     *             &lt;/complexType&gt;
     *           &lt;/element&gt;
     *         &lt;/sequence&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "dateconclusioncontract",
            "contractors",
            "values",
            "likelysubcontracted",
            "valsubcontracting",
            "pctsubcontracting",
            "infoaddsubcontracting",
            "directive200981EC"
    })
    @ToString
    @EqualsAndHashCode
    public static class AWARDEDCONTRACT {

        @XmlElement(name = "DATE_CONCLUSION_CONTRACT", required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dateconclusioncontract;
        @XmlElement(name = "CONTRACTORS", required = true)
        protected AwardContractF15.AWARDEDCONTRACT.CONTRACTORS contractors;
        @XmlElement(name = "VALUES", required = true)
        protected AwardContractF15.AWARDEDCONTRACT.VALUES values;
        @XmlElement(name = "LIKELY_SUBCONTRACTED")
        protected Empty likelysubcontracted;
        @XmlElement(name = "VAL_SUBCONTRACTING")
        protected Val valsubcontracting;
        @XmlElement(name = "PCT_SUBCONTRACTING")
        @XmlSchemaType(name = "integer")
        protected Integer pctsubcontracting;
        @XmlElement(name = "INFO_ADD_SUBCONTRACTING")
        protected TextFtMultiLines infoaddsubcontracting;
        @XmlElement(name = "DIRECTIVE_2009_81_EC")
        protected AwardContractF15.AWARDEDCONTRACT.DIRECTIVE200981EC directive200981EC;

        /**
         * Obtient la valeur de la propriété dateconclusioncontract.
         *
         * @return possible object is
         * {@link XMLGregorianCalendar }
         */
        public XMLGregorianCalendar getDATECONCLUSIONCONTRACT() {
            return dateconclusioncontract;
        }

        /**
         * Définit la valeur de la propriété dateconclusioncontract.
         *
         * @param value allowed object is
         *              {@link XMLGregorianCalendar }
         */
        public void setDATECONCLUSIONCONTRACT(XMLGregorianCalendar value) {
            this.dateconclusioncontract = value;
        }

        /**
         * Obtient la valeur de la propriété contractors.
         *
         * @return possible object is
         * {@link AwardContractF15 .AWARDEDCONTRACT.CONTRACTORS }
         */
        public AwardContractF15.AWARDEDCONTRACT.CONTRACTORS getCONTRACTORS() {
            return contractors;
        }

        /**
         * Définit la valeur de la propriété contractors.
         *
         * @param value allowed object is
         *              {@link AwardContractF15 .AWARDEDCONTRACT.CONTRACTORS }
         */
        public void setCONTRACTORS(AwardContractF15.AWARDEDCONTRACT.CONTRACTORS value) {
            this.contractors = value;
        }

        /**
         * Obtient la valeur de la propriété values.
         *
         * @return possible object is
         * {@link AwardContractF15 .AWARDEDCONTRACT.VALUES }
         */
        public AwardContractF15.AWARDEDCONTRACT.VALUES getVALUES() {
            return values;
        }

        /**
         * Définit la valeur de la propriété values.
         *
         * @param value allowed object is
         *              {@link AwardContractF15 .AWARDEDCONTRACT.VALUES }
         */
        public void setVALUES(AwardContractF15.AWARDEDCONTRACT.VALUES value) {
            this.values = value;
        }

        /**
         * Obtient la valeur de la propriété likelysubcontracted.
         *
         * @return possible object is
         * {@link Empty }
         */
        public Empty getLIKELYSUBCONTRACTED() {
            return likelysubcontracted;
        }

        /**
         * Définit la valeur de la propriété likelysubcontracted.
         *
         * @param value allowed object is
         *              {@link Empty }
         */
        public void setLIKELYSUBCONTRACTED(Empty value) {
            this.likelysubcontracted = value;
        }

        /**
         * Obtient la valeur de la propriété valsubcontracting.
         *
         * @return possible object is
         * {@link Val }
         */
        public Val getVALSUBCONTRACTING() {
            return valsubcontracting;
        }

        /**
         * Définit la valeur de la propriété valsubcontracting.
         *
         * @param value allowed object is
         *              {@link Val }
         */
        public void setVALSUBCONTRACTING(Val value) {
            this.valsubcontracting = value;
        }

        /**
         * Obtient la valeur de la propriété pctsubcontracting.
         *
         * @return possible object is
         * {@link Integer }
         */
        public Integer getPCTSUBCONTRACTING() {
            return pctsubcontracting;
        }

        /**
         * Définit la valeur de la propriété pctsubcontracting.
         *
         * @param value allowed object is
         *              {@link Integer }
         */
        public void setPCTSUBCONTRACTING(Integer value) {
            this.pctsubcontracting = value;
        }

        /**
         * Obtient la valeur de la propriété infoaddsubcontracting.
         *
         * @return possible object is
         * {@link TextFtMultiLines }
         */
        public TextFtMultiLines getINFOADDSUBCONTRACTING() {
            return infoaddsubcontracting;
        }

        /**
         * Définit la valeur de la propriété infoaddsubcontracting.
         *
         * @param value allowed object is
         *              {@link TextFtMultiLines }
         */
        public void setINFOADDSUBCONTRACTING(TextFtMultiLines value) {
            this.infoaddsubcontracting = value;
        }

        /**
         * Obtient la valeur de la propriété directive200981EC.
         *
         * @return possible object is
         * {@link AwardContractF15 .AWARDEDCONTRACT.DIRECTIVE200981EC }
         */
        public AwardContractF15.AWARDEDCONTRACT.DIRECTIVE200981EC getDIRECTIVE200981EC() {
            return directive200981EC;
        }

        /**
         * Définit la valeur de la propriété directive200981EC.
         *
         * @param value allowed object is
         *              {@link AwardContractF15 .AWARDEDCONTRACT.DIRECTIVE200981EC }
         */
        public void setDIRECTIVE200981EC(AwardContractF15.AWARDEDCONTRACT.DIRECTIVE200981EC value) {
            this.directive200981EC = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_opt"&gt;
         *       &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contractor"/&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "awardedtogroup",
                "contractorSmeMan",
                "noawardedtogroup",
                "contractor"
        })
        @ToString(callSuper = true)
        @EqualsAndHashCode(callSuper = true)
        public static class CONTRACTORS
                extends AgreeToPublicationOpt {

            @XmlElement(name = "AWARDED_TO_GROUP")
            protected Empty awardedtogroup;
            @XmlElement(name = "CONTRACTOR")
            protected List<eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR> contractorSmeMan;
            @XmlElement(name = "NO_AWARDED_TO_GROUP")
            protected Empty noawardedtogroup;
            @XmlElement(name = "CONTRACTOR")
            protected eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR contractor;

            /**
             * Obtient la valeur de la propriété awardedtogroup.
             *
             * @return possible object is
             * {@link Empty }
             */
            public Empty getAWARDEDTOGROUP() {
                return awardedtogroup;
            }

            /**
             * Définit la valeur de la propriété awardedtogroup.
             *
             * @param value allowed object is
             *              {@link Empty }
             */
            public void setAWARDEDTOGROUP(Empty value) {
                this.awardedtogroup = value;
            }

            /**
             * Gets the value of the contractorSmeMan property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the contractorSmeMan property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getContractorSmeMan().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25 .AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR }
             */
            public List<eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR> getContractorSmeMan() {
                if (contractorSmeMan == null) {
                    contractorSmeMan = new ArrayList<eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR>();
                }
                return this.contractorSmeMan;
            }

            /**
             * Obtient la valeur de la propriété noawardedtogroup.
             *
             * @return possible object is
             * {@link Empty }
             */
            public Empty getNOAWARDEDTOGROUP() {
                return noawardedtogroup;
            }

            /**
             * Définit la valeur de la propriété noawardedtogroup.
             *
             * @param value allowed object is
             *              {@link Empty }
             */
            public void setNOAWARDEDTOGROUP(Empty value) {
                this.noawardedtogroup = value;
            }

            /**
             * Obtient la valeur de la propriété contractor.
             *
             * @return possible object is
             * {@link eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25 .AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR }
             */
            public eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR getCONTRACTOR() {
                return contractor;
            }

            /**
             * Définit la valeur de la propriété contractor.
             *
             * @param value allowed object is
             *              {@link eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25 .AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR }
             */
            public void setCONTRACTOR(eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF25.AWARDEDCONTRACT.CONTRACTORS.CONTRACTOR value) {
                this.contractor = value;
            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;choice&gt;
         *         &lt;sequence&gt;
         *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}AWARDED_SUBCONTRACTING"/&gt;
         *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PCT_RANGE_SHARE_SUBCONTRACTING" minOccurs="0"/&gt;
         *         &lt;/sequence&gt;
         *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PCT_RANGE_SHARE_SUBCONTRACTING"/&gt;
         *       &lt;/choice&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "content"
        })
        @ToString
        @EqualsAndHashCode
        public static class DIRECTIVE200981EC {

            @XmlElementRefs({
                    @XmlElementRef(name = "AWARDED_SUBCONTRACTING", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "PCT_RANGE_SHARE_SUBCONTRACTING", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false)
            })
            protected List<JAXBElement<?>> content;

            /**
             * Obtient le reste du modèle de contenu.
             *
             * <p>
             * Vous obtenez la propriété "catch-all" pour la raison suivante :
             * Le nom de champ "PCTRANGESHARESUBCONTRACTING" est utilisé par deux parties différentes d'un schéma. Reportez-vous à :
             * ligne 73 sur file:/home/iat-atx/Bureau/ATEXO/WORKSPACE/lt_concentrateur_annonces/concentrateur-back/src/main/resources/simplified-xsd/joue/F15_2014.xsd
             * ligne 71 sur file:/home/iat-atx/Bureau/ATEXO/WORKSPACE/lt_concentrateur_annonces/concentrateur-back/src/main/resources/simplified-xsd/joue/F15_2014.xsd
             * <p>
             * Pour vous débarrasser de cette propriété, appliquez une personnalisation de propriété à l'une
             * des deux déclarations suivantes afin de modifier leurs noms :
             * Gets the value of the content property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the content property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getContent().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link JAXBElement }{@code <}{@link Empty }{@code >}
             * {@link JAXBElement }{@code <}{@link PrctRange }{@code >}
             */
            public List<JAXBElement<?>> getContent() {
                if (content == null) {
                    content = new ArrayList<JAXBElement<?>>();
                }
                return this.content;
            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}agree_to_publication_opt"&gt;
         *       &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}award_contract_value"/&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "valestimatedtotal",
                "valtotal",
                "valrangetotal"
        })
        @ToString(callSuper = true)
        @EqualsAndHashCode(callSuper = true)
        public static class VALUES
                extends AgreeToPublicationOpt {

            @XmlElement(name = "VAL_ESTIMATED_TOTAL")
            protected Val valestimatedtotal;
            @XmlElement(name = "VAL_TOTAL")
            protected Val valtotal;
            @XmlElement(name = "VAL_RANGE_TOTAL")
            protected ValRange valrangetotal;

            /**
             * Obtient la valeur de la propriété valestimatedtotal.
             *
             * @return possible object is
             * {@link Val }
             */
            public Val getVALESTIMATEDTOTAL() {
                return valestimatedtotal;
            }

            /**
             * Définit la valeur de la propriété valestimatedtotal.
             *
             * @param value allowed object is
             *              {@link Val }
             */
            public void setVALESTIMATEDTOTAL(Val value) {
                this.valestimatedtotal = value;
            }

            /**
             * Obtient la valeur de la propriété valtotal.
             *
             * @return possible object is
             * {@link Val }
             */
            public Val getVALTOTAL() {
                return valtotal;
            }

            /**
             * Définit la valeur de la propriété valtotal.
             *
             * @param value allowed object is
             *              {@link Val }
             */
            public void setVALTOTAL(Val value) {
                this.valtotal = value;
            }

            /**
             * Obtient la valeur de la propriété valrangetotal.
             *
             * @return possible object is
             * {@link ValRange }
             */
            public ValRange getVALRANGETOTAL() {
                return valrangetotal;
            }

            /**
             * Définit la valeur de la propriété valrangetotal.
             *
             * @param value allowed object is
             *              {@link ValRange }
             */
            public void setVALRANGETOTAL(ValRange value) {
                this.valrangetotal = value;
            }

        }

    }

}
