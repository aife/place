//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Section I: CONTRACTING ENTITY
 *
 * <p>Classe Java pour body_f05 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="body_f05"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ADDRESS_CONTRACTING_BODY"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}OFFRE" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}procurement_address"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}document_url_man"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}information"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}tenders_request"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}URL_TOOL" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}entity"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "body_f05", propOrder = {
        "addressContractingBody",
        "offre",
        "addressContractingBodyAdditional",
        "jointProcurementInvolved",
        "procurementLaw",
        "centralPurchasing",
        "documentFull",
        "documentRestricted",
        "urlDocument",
        "addressFurtherInfoIdem",
        "addressFurtherInfo",
        "urlParticipation",
        "addressParticipationIdem",
        "addressParticipation",
        "urlTool",
        "ceActivity",
        "ceActivityOther"
})
@ToString
@EqualsAndHashCode
@Getter
@Setter
public class BodyF05 {
    @SuppressWarnings("checkstyle:MemberName")
    @XmlElement(name = "ADDRESS_CONTRACTING_BODY")
    private ContactContractingBody addressContractingBody;
    @XmlElement(name = "OFFRE")
    private Offre offre;
    @XmlElement(name = "ADDRESS_CONTRACTING_BODY_ADDITIONAL")
    private ContactContractingBody addressContractingBodyAdditional;
    @XmlElement(name = "JOINT_PROCUREMENT_INVOLVED")
    private Empty jointProcurementInvolved;
    @XmlElement(name = "PROCUREMENT_LAW")
    private TextFtMultiLines procurementLaw;
    @XmlElement(name = "CENTRAL_PURCHASING")
    private Empty centralPurchasing;
    @XmlElement(name = "DOCUMENT_FULL")
    private Empty documentFull;
    @XmlElement(name = "DOCUMENT_RESTRICTED")
    private Empty documentRestricted;
    @XmlElement(name = "URL_DOCUMENT")
    private String urlDocument;
    @XmlElement(name = "ADDRESS_FURTHER_INFO_IDEM")
    private Empty addressFurtherInfoIdem;
    @XmlElement(name = "ADDRESS_FURTHER_INFO")
    private ContactContractingBody addressFurtherInfo;
    @XmlElement(name = "URL_PARTICIPATION")
    private String urlParticipation;
    @XmlElement(name = "ADDRESS_PARTICIPATION_IDEM")
    private Empty addressParticipationIdem;
    @XmlElement(name = "ADDRESS_PARTICIPATION")
    private ContactContractingBody addressParticipation;
    @XmlElement(name = "URL_TOOL")
    private String urlTool;
    @XmlElement(name = "CE_ACTIVITY")
    private CeActivity ceActivity;
    @XmlElement(name = "CE_ACTIVITY_OTHER")
    private String ceActivityOther;
}
