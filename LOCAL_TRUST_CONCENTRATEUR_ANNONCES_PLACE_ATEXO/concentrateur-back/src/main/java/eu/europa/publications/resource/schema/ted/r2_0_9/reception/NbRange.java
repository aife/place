//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.math.BigInteger;


/**
 * <p>Classe Java pour nb_range complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="nb_range"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MIN" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}nb"/&gt;
 *         &lt;element name="MAX" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}nb"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nb_range", propOrder = {
        "min",
        "max"
})
@ToString
@EqualsAndHashCode
public class NbRange {

    @XmlElement(name = "MIN", required = true)
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger min;
    @XmlElement(name = "MAX", required = true)
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger max;

    /**
     * Obtient la valeur de la propriété min.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public BigInteger getMIN() {
        return min;
    }

    /**
     * Définit la valeur de la propriété min.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public void setMIN(BigInteger value) {
        this.min = value;
    }

    /**
     * Obtient la valeur de la propriété max.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public BigInteger getMAX() {
        return max;
    }

    /**
     * Définit la valeur de la propriété max.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public void setMAX(BigInteger value) {
        this.max = value;
    }

}
