//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * Section IV: PROCEDURE
 *
 * <p>Classe Java pour procedure_f02 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="procedure_f02"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;sequence&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_OPEN"/&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ACCELERATED_PROC" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *           &lt;sequence&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_RESTRICTED"/&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ACCELERATED_PROC" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *           &lt;sequence&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_COMPETITIVE_NEGOTIATION"/&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ACCELERATED_PROC" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_COMPETITIVE_DIALOGUE"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_INNOVATION_PARTNERSHIP"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="FRAMEWORK" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}framework_info" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}dps_purchasers" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}REDUCTION_RECOURSE" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}RIGHT_CONTRACT_INITIAL_TENDERS" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}eauction" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}gpa"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NOTICE_NUMBER_OJ" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}receipt_tenders"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DATE_DISPATCH_INVITATIONS" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LANGUAGES"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}time_frame_tender_valid" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}OPENING_CONDITION" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "procedure_f02", propOrder = {
        "content"
})
@ToString
@EqualsAndHashCode
public class ProcedureF02 {

    @XmlElementRefs({
            @XmlElementRef(name = "PT_OPEN", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "ACCELERATED_PROC", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "PT_RESTRICTED", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "PT_COMPETITIVE_NEGOTIATION", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "PT_COMPETITIVE_DIALOGUE", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "PT_INNOVATION_PARTNERSHIP", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "FRAMEWORK", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "DPS", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "DPS_ADDITIONAL_PURCHASERS", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "REDUCTION_RECOURSE", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "RIGHT_CONTRACT_INITIAL_TENDERS", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "EAUCTION_USED", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "INFO_ADD_EAUCTION", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "CONTRACT_COVERED_GPA", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "NO_CONTRACT_COVERED_GPA", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "NOTICE_NUMBER_OJ", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "DATE_RECEIPT_TENDERS", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "TIME_RECEIPT_TENDERS", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "DATE_DISPATCH_INVITATIONS", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "LANGUAGES", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = LANGUAGES.class, required = false),
            @XmlElementRef(name = "DATE_TENDER_VALID", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "DURATION_TENDER_VALID", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "OPENING_CONDITION", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false)
    })
    protected List<Object> content;

    /**
     * Obtient le reste du modèle de contenu.
     *
     * <p>
     * Vous obtenez la propriété "catch-all" pour la raison suivante :
     * Le nom de champ "ACCELERATEDPROC" est utilisé par deux parties différentes d'un schéma. Reportez-vous à :
     * ligne 53 sur file:/home/iat-atx/Bureau/ATEXO/WORKSPACE/lt_concentrateur_annonces/concentrateur-back/src/main/resources/simplified-xsd/joue/F02_2014.xsd
     * ligne 49 sur file:/home/iat-atx/Bureau/ATEXO/WORKSPACE/lt_concentrateur_annonces/concentrateur-back/src/main/resources/simplified-xsd/joue/F02_2014.xsd
     * <p>
     * Pour vous débarrasser de cette propriété, appliquez une personnalisation de propriété à l'une
     * des deux déclarations suivantes afin de modifier leurs noms :
     * Gets the value of the content property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link FrameworkInfo }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link LANGUAGES }
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link eu.europa.publications.resource.schema.ted.r2_0_9.reception.ProcedureF05 .DURATIONTENDERVALID }{@code >}
     * {@link JAXBElement }{@code <}{@link CondForOpeningTenders }{@code >}
     */
    public List<Object> getContent() {
        if (content == null) {
            content = new ArrayList<Object>();
        }
        return this.content;
    }

}
