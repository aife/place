//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Section IV: PROCEDURE
 *
 * <p>Classe Java pour procedure_f06 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="procedure_f06"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_OPEN"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_RESTRICTED"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_NEGOTIATED_WITH_PRIOR_CALL"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_COMPETITIVE_DIALOGUE"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_INNOVATION_PARTNERSHIP"/&gt;
 *           &lt;element name="PT_AWARD_CONTRACT_WITHOUT_CALL" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d2"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="FRAMEWORK" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DPS" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}EAUCTION_USED" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}gpa"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NOTICE_NUMBER_OJ" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TERMINATION_DPS" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TERMINATION_PIN" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "procedure_f06", propOrder = {
        "ptopen",
        "ptrestricted",
        "ptnegotiatedwithpriorcall",
        "ptcompetitivedialogue",
        "ptinnovationpartnership",
        "ptawardcontractwithoutcall",
        "framework",
        "dps",
        "eauctionused",
        "contractcoveredgpa",
        "nocontractcoveredgpa",
        "noticenumberoj",
        "terminationdps",
        "terminationpin"
})
@ToString
@EqualsAndHashCode
public class ProcedureF06 {

    @XmlElement(name = "PT_OPEN")
    protected Empty ptopen;
    @XmlElement(name = "PT_RESTRICTED")
    protected Empty ptrestricted;
    @XmlElement(name = "PT_NEGOTIATED_WITH_PRIOR_CALL")
    protected Empty ptnegotiatedwithpriorcall;
    @XmlElement(name = "PT_COMPETITIVE_DIALOGUE")
    protected Empty ptcompetitivedialogue;
    @XmlElement(name = "PT_INNOVATION_PARTNERSHIP")
    protected Empty ptinnovationpartnership;
    @XmlElement(name = "PT_AWARD_CONTRACT_WITHOUT_CALL")
    protected AnnexD2 ptawardcontractwithoutcall;
    @XmlElement(name = "FRAMEWORK")
    protected Empty framework;
    @XmlElement(name = "DPS")
    protected Empty dps;
    @XmlElement(name = "EAUCTION_USED")
    protected Empty eauctionused;
    @XmlElement(name = "CONTRACT_COVERED_GPA")
    protected Empty contractcoveredgpa;
    @XmlElement(name = "NO_CONTRACT_COVERED_GPA")
    protected Empty nocontractcoveredgpa;
    @XmlElement(name = "NOTICE_NUMBER_OJ")
    protected String noticenumberoj;
    @XmlElement(name = "TERMINATION_DPS")
    protected Empty terminationdps;
    @XmlElement(name = "TERMINATION_PIN")
    protected Empty terminationpin;

    /**
     * Obtient la valeur de la propriété ptopen.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTOPEN() {
        return ptopen;
    }

    /**
     * Définit la valeur de la propriété ptopen.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTOPEN(Empty value) {
        this.ptopen = value;
    }

    /**
     * Obtient la valeur de la propriété ptrestricted.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTRESTRICTED() {
        return ptrestricted;
    }

    /**
     * Définit la valeur de la propriété ptrestricted.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTRESTRICTED(Empty value) {
        this.ptrestricted = value;
    }

    /**
     * Obtient la valeur de la propriété ptnegotiatedwithpriorcall.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTNEGOTIATEDWITHPRIORCALL() {
        return ptnegotiatedwithpriorcall;
    }

    /**
     * Définit la valeur de la propriété ptnegotiatedwithpriorcall.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTNEGOTIATEDWITHPRIORCALL(Empty value) {
        this.ptnegotiatedwithpriorcall = value;
    }

    /**
     * Obtient la valeur de la propriété ptcompetitivedialogue.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTCOMPETITIVEDIALOGUE() {
        return ptcompetitivedialogue;
    }

    /**
     * Définit la valeur de la propriété ptcompetitivedialogue.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTCOMPETITIVEDIALOGUE(Empty value) {
        this.ptcompetitivedialogue = value;
    }

    /**
     * Obtient la valeur de la propriété ptinnovationpartnership.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTINNOVATIONPARTNERSHIP() {
        return ptinnovationpartnership;
    }

    /**
     * Définit la valeur de la propriété ptinnovationpartnership.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTINNOVATIONPARTNERSHIP(Empty value) {
        this.ptinnovationpartnership = value;
    }

    /**
     * Obtient la valeur de la propriété ptawardcontractwithoutcall.
     *
     * @return possible object is
     * {@link AnnexD2 }
     */
    public AnnexD2 getPTAWARDCONTRACTWITHOUTCALL() {
        return ptawardcontractwithoutcall;
    }

    /**
     * Définit la valeur de la propriété ptawardcontractwithoutcall.
     *
     * @param value allowed object is
     *              {@link AnnexD2 }
     */
    public void setPTAWARDCONTRACTWITHOUTCALL(AnnexD2 value) {
        this.ptawardcontractwithoutcall = value;
    }

    /**
     * Obtient la valeur de la propriété framework.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getFRAMEWORK() {
        return framework;
    }

    /**
     * Définit la valeur de la propriété framework.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setFRAMEWORK(Empty value) {
        this.framework = value;
    }

    /**
     * Obtient la valeur de la propriété dps.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getDPS() {
        return dps;
    }

    /**
     * Définit la valeur de la propriété dps.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setDPS(Empty value) {
        this.dps = value;
    }

    /**
     * Obtient la valeur de la propriété eauctionused.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getEAUCTIONUSED() {
        return eauctionused;
    }

    /**
     * Définit la valeur de la propriété eauctionused.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setEAUCTIONUSED(Empty value) {
        this.eauctionused = value;
    }

    /**
     * Obtient la valeur de la propriété contractcoveredgpa.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getCONTRACTCOVEREDGPA() {
        return contractcoveredgpa;
    }

    /**
     * Définit la valeur de la propriété contractcoveredgpa.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setCONTRACTCOVEREDGPA(Empty value) {
        this.contractcoveredgpa = value;
    }

    /**
     * Obtient la valeur de la propriété nocontractcoveredgpa.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getNOCONTRACTCOVEREDGPA() {
        return nocontractcoveredgpa;
    }

    /**
     * Définit la valeur de la propriété nocontractcoveredgpa.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setNOCONTRACTCOVEREDGPA(Empty value) {
        this.nocontractcoveredgpa = value;
    }

    /**
     * Obtient la valeur de la propriété noticenumberoj.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNOTICENUMBEROJ() {
        return noticenumberoj;
    }

    /**
     * Définit la valeur de la propriété noticenumberoj.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNOTICENUMBEROJ(String value) {
        this.noticenumberoj = value;
    }

    /**
     * Obtient la valeur de la propriété terminationdps.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getTERMINATIONDPS() {
        return terminationdps;
    }

    /**
     * Définit la valeur de la propriété terminationdps.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setTERMINATIONDPS(Empty value) {
        this.terminationdps = value;
    }

    /**
     * Obtient la valeur de la propriété terminationpin.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getTERMINATIONPIN() {
        return terminationpin;
    }

    /**
     * Définit la valeur de la propriété terminationpin.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setTERMINATIONPIN(Empty value) {
        this.terminationpin = value;
    }

}
