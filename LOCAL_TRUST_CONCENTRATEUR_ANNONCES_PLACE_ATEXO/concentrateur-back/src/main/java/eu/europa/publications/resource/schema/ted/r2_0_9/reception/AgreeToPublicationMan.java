//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java pour agree_to_publication_man complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="agree_to_publication_man"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="PUBLICATION" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}publication" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "agree_to_publication_man")
@XmlSeeAlso({
        eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF06.AWARDEDCONTRACT.TENDERS.class,
        eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF06.AWARDEDCONTRACT.CONTRACTORS.class,
        eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF06.AWARDEDCONTRACT.VALUES.class,
        eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF22.AWARDEDCONTRACT.TENDERS.class,
        eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF22.AWARDEDCONTRACT.CONTRACTORS.class,
        eu.europa.publications.resource.schema.ted.r2_0_9.reception.AwardContractF22.AWARDEDCONTRACT.VALUES.class,
        eu.europa.publications.resource.schema.ted.r2_0_9.reception.ObjectF06.AC.class,
        eu.europa.publications.resource.schema.ted.r2_0_9.reception.ObjectF15.DIRECTIVE201425EU.AC.class
})
@ToString
@EqualsAndHashCode
public class AgreeToPublicationMan {

    @XmlAttribute(name = "PUBLICATION", required = true)
    protected Publication publication;

    /**
     * Obtient la valeur de la propriété publication.
     *
     * @return possible object is
     * {@link Publication }
     */
    public Publication getPUBLICATION() {
        return publication;
    }

    /**
     * Définit la valeur de la propriété publication.
     *
     * @param value allowed object is
     *              {@link Publication }
     */
    public void setPUBLICATION(Publication value) {
        this.publication = value;
    }

}
