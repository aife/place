//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Section IV: PROCEDURE
 *
 * <p>Classe Java pour procedure_f01 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="procedure_f01"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_RESTRICTED"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_COMPETITIVE_NEGOTIATION"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="FRAMEWORK" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}framework_info" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}dps_purchasers" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}eauction" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}gpa"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}receipt_tenders" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LANGUAGES" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DATE_AWARD_SCHEDULED" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "procedure_f01", propOrder = {
        "ptrestricted",
        "ptcompetitivenegotiation",
        "framework",
        "dps",
        "dpsadditionalpurchasers",
        "eauctionused",
        "infoaddeauction",
        "contractcoveredgpa",
        "nocontractcoveredgpa",
        "datereceipttenders",
        "timereceipttenders",
        "languages",
        "dateawardscheduled"
})
@ToString
@EqualsAndHashCode
public class ProcedureF01 {

    @XmlElement(name = "PT_RESTRICTED")
    protected Empty ptrestricted;
    @XmlElement(name = "PT_COMPETITIVE_NEGOTIATION")
    protected Empty ptcompetitivenegotiation;
    @XmlElement(name = "FRAMEWORK")
    protected FrameworkInfo framework;
    @XmlElement(name = "DPS")
    protected Empty dps;
    @XmlElement(name = "DPS_ADDITIONAL_PURCHASERS")
    protected Empty dpsadditionalpurchasers;
    @XmlElement(name = "EAUCTION_USED")
    protected Empty eauctionused;
    @XmlElement(name = "INFO_ADD_EAUCTION")
    protected TextFtMultiLines infoaddeauction;
    @XmlElement(name = "CONTRACT_COVERED_GPA")
    protected Empty contractcoveredgpa;
    @XmlElement(name = "NO_CONTRACT_COVERED_GPA")
    protected Empty nocontractcoveredgpa;
    @XmlElement(name = "DATE_RECEIPT_TENDERS")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datereceipttenders;
    @XmlElement(name = "TIME_RECEIPT_TENDERS")
    protected String timereceipttenders;
    @XmlElement(name = "LANGUAGES")
    protected LANGUAGES languages;
    @XmlElement(name = "DATE_AWARD_SCHEDULED")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateawardscheduled;

    /**
     * Obtient la valeur de la propriété ptrestricted.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTRESTRICTED() {
        return ptrestricted;
    }

    /**
     * Définit la valeur de la propriété ptrestricted.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTRESTRICTED(Empty value) {
        this.ptrestricted = value;
    }

    /**
     * Obtient la valeur de la propriété ptcompetitivenegotiation.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getPTCOMPETITIVENEGOTIATION() {
        return ptcompetitivenegotiation;
    }

    /**
     * Définit la valeur de la propriété ptcompetitivenegotiation.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setPTCOMPETITIVENEGOTIATION(Empty value) {
        this.ptcompetitivenegotiation = value;
    }

    /**
     * Obtient la valeur de la propriété framework.
     *
     * @return possible object is
     * {@link FrameworkInfo }
     */
    public FrameworkInfo getFRAMEWORK() {
        return framework;
    }

    /**
     * Définit la valeur de la propriété framework.
     *
     * @param value allowed object is
     *              {@link FrameworkInfo }
     */
    public void setFRAMEWORK(FrameworkInfo value) {
        this.framework = value;
    }

    /**
     * Obtient la valeur de la propriété dps.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getDPS() {
        return dps;
    }

    /**
     * Définit la valeur de la propriété dps.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setDPS(Empty value) {
        this.dps = value;
    }

    /**
     * Obtient la valeur de la propriété dpsadditionalpurchasers.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getDPSADDITIONALPURCHASERS() {
        return dpsadditionalpurchasers;
    }

    /**
     * Définit la valeur de la propriété dpsadditionalpurchasers.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setDPSADDITIONALPURCHASERS(Empty value) {
        this.dpsadditionalpurchasers = value;
    }

    /**
     * Obtient la valeur de la propriété eauctionused.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getEAUCTIONUSED() {
        return eauctionused;
    }

    /**
     * Définit la valeur de la propriété eauctionused.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setEAUCTIONUSED(Empty value) {
        this.eauctionused = value;
    }

    /**
     * Obtient la valeur de la propriété infoaddeauction.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getINFOADDEAUCTION() {
        return infoaddeauction;
    }

    /**
     * Définit la valeur de la propriété infoaddeauction.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setINFOADDEAUCTION(TextFtMultiLines value) {
        this.infoaddeauction = value;
    }

    /**
     * Obtient la valeur de la propriété contractcoveredgpa.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getCONTRACTCOVEREDGPA() {
        return contractcoveredgpa;
    }

    /**
     * Définit la valeur de la propriété contractcoveredgpa.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setCONTRACTCOVEREDGPA(Empty value) {
        this.contractcoveredgpa = value;
    }

    /**
     * Obtient la valeur de la propriété nocontractcoveredgpa.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getNOCONTRACTCOVEREDGPA() {
        return nocontractcoveredgpa;
    }

    /**
     * Définit la valeur de la propriété nocontractcoveredgpa.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setNOCONTRACTCOVEREDGPA(Empty value) {
        this.nocontractcoveredgpa = value;
    }

    /**
     * Obtient la valeur de la propriété datereceipttenders.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDATERECEIPTTENDERS() {
        return datereceipttenders;
    }

    /**
     * Définit la valeur de la propriété datereceipttenders.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDATERECEIPTTENDERS(XMLGregorianCalendar value) {
        this.datereceipttenders = value;
    }

    /**
     * Obtient la valeur de la propriété timereceipttenders.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTIMERECEIPTTENDERS() {
        return timereceipttenders;
    }

    /**
     * Définit la valeur de la propriété timereceipttenders.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTIMERECEIPTTENDERS(String value) {
        this.timereceipttenders = value;
    }

    /**
     * Obtient la valeur de la propriété languages.
     *
     * @return possible object is
     * {@link LANGUAGES }
     */
    public LANGUAGES getLANGUAGES() {
        return languages;
    }

    /**
     * Définit la valeur de la propriété languages.
     *
     * @param value allowed object is
     *              {@link LANGUAGES }
     */
    public void setLANGUAGES(LANGUAGES value) {
        this.languages = value;
    }

    /**
     * Obtient la valeur de la propriété dateawardscheduled.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDATEAWARDSCHEDULED() {
        return dateawardscheduled;
    }

    /**
     * Définit la valeur de la propriété dateawardscheduled.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDATEAWARDSCHEDULED(XMLGregorianCalendar value) {
        this.dateawardscheduled = value;
    }

}
