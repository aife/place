//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Section I: CONTRACTING AUTHORITY
 *
 * <p>Classe Java pour body_f14 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="body_f14"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ADDRESS_CONTRACTING_BODY" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contact_contracting_body_f14"/&gt;
 *         &lt;element name="ADDRESS_CONTRACTING_BODY_ADDITIONAL" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}contact_add_contracting_body_f14" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "body_f14", propOrder = {
        "addresscontractingbody",
        "addresscontractingbodyadditional"
})
@ToString
@EqualsAndHashCode
public class BodyF14 {

    @XmlElement(name = "ADDRESS_CONTRACTING_BODY", required = true)
    protected ContactContractingBodyF14 addresscontractingbody;
    @XmlElement(name = "ADDRESS_CONTRACTING_BODY_ADDITIONAL")
    protected List<ContactAddContractingBodyF14> addresscontractingbodyadditional;

    /**
     * Obtient la valeur de la propriété addresscontractingbody.
     *
     * @return possible object is
     * {@link ContactContractingBodyF14 }
     */
    public ContactContractingBodyF14 getADDRESSCONTRACTINGBODY() {
        return addresscontractingbody;
    }

    /**
     * Définit la valeur de la propriété addresscontractingbody.
     *
     * @param value allowed object is
     *              {@link ContactContractingBodyF14 }
     */
    public void setADDRESSCONTRACTINGBODY(ContactContractingBodyF14 value) {
        this.addresscontractingbody = value;
    }

    /**
     * Gets the value of the addresscontractingbodyadditional property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addresscontractingbodyadditional property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getADDRESSCONTRACTINGBODYADDITIONAL().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactAddContractingBodyF14 }
     */
    public List<ContactAddContractingBodyF14> getADDRESSCONTRACTINGBODYADDITIONAL() {
        if (addresscontractingbodyadditional == null) {
            addresscontractingbodyadditional = new ArrayList<ContactAddContractingBodyF14>();
        }
        return this.addresscontractingbodyadditional;
    }

}
