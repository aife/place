//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java pour anonymous complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;sequence&gt;
 *             &lt;element name="LEGAL_BASIS" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}legal_basis_f04"/&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LEGAL_BASIS_OTHER" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LEGAL_BASIS_OTHER"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="NOTICE" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}notice_f04"/&gt;
 *         &lt;element name="CONTRACTING_BODY" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}body_f04"/&gt;
 *         &lt;element name="OBJECT_CONTRACT" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}object_contract_f04" maxOccurs="100"/&gt;
 *         &lt;element name="LEFTI" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}lefti_f04" minOccurs="0"/&gt;
 *         &lt;element name="PROCEDURE" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}procedure_f04"/&gt;
 *         &lt;element name="COMPLEMENTARY_INFO" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ci_f04"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="LG" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}t_ce_language_list" /&gt;
 *       &lt;attribute name="CATEGORY" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}original_translation" /&gt;
 *       &lt;attribute name="FORM" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="F04" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "content"
})
@XmlRootElement(name = "F04_2014")
@ToString
@EqualsAndHashCode
public class F042014 {

    @XmlElementRefs({
            @XmlElementRef(name = "LEGAL_BASIS", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "LEGAL_BASIS_OTHER", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "NOTICE", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "CONTRACTING_BODY", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "OBJECT_CONTRACT", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "LEFTI", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "PROCEDURE", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "COMPLEMENTARY_INFO", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false)
    })
    protected List<JAXBElement<?>> content;
    @XmlAttribute(name = "LG", required = true)
    protected TCeLanguageList lg;
    @XmlAttribute(name = "CATEGORY", required = true)
    protected OriginalTranslation category;
    @XmlAttribute(name = "FORM", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String form;

    /**
     * Obtient le reste du modèle de contenu.
     *
     * <p>
     * Vous obtenez la propriété "catch-all" pour la raison suivante :
     * Le nom de champ "LEGALBASISOTHER" est utilisé par deux parties différentes d'un schéma. Reportez-vous à :
     * ligne 196 sur file:/home/iat-atx/Bureau/ATEXO/WORKSPACE/lt_concentrateur_annonces/concentrateur-back/src/main/resources/simplified-xsd/joue/F04_2014.xsd
     * ligne 194 sur file:/home/iat-atx/Bureau/ATEXO/WORKSPACE/lt_concentrateur_annonces/concentrateur-back/src/main/resources/simplified-xsd/joue/F04_2014.xsd
     * <p>
     * Pour vous débarrasser de cette propriété, appliquez une personnalisation de propriété à l'une
     * des deux déclarations suivantes afin de modifier leurs noms :
     * Gets the value of the content property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link LegalBasisF04 }{@code >}
     * {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     * {@link JAXBElement }{@code <}{@link NoticeF04 }{@code >}
     * {@link JAXBElement }{@code <}{@link BodyF04 }{@code >}
     * {@link JAXBElement }{@code <}{@link ObjectContractF04 }{@code >}
     * {@link JAXBElement }{@code <}{@link LeftiF04 }{@code >}
     * {@link JAXBElement }{@code <}{@link ProcedureF04 }{@code >}
     * {@link JAXBElement }{@code <}{@link CiF04 }{@code >}
     */
    public List<JAXBElement<?>> getContent() {
        if (content == null) {
            content = new ArrayList<JAXBElement<?>>();
        }
        return this.content;
    }

    /**
     * Obtient la valeur de la propriété lg.
     *
     * @return possible object is
     * {@link TCeLanguageList }
     */
    public TCeLanguageList getLG() {
        return lg;
    }

    /**
     * Définit la valeur de la propriété lg.
     *
     * @param value allowed object is
     *              {@link TCeLanguageList }
     */
    public void setLG(TCeLanguageList value) {
        this.lg = value;
    }

    /**
     * Obtient la valeur de la propriété category.
     *
     * @return possible object is
     * {@link OriginalTranslation }
     */
    public OriginalTranslation getCATEGORY() {
        return category;
    }

    /**
     * Définit la valeur de la propriété category.
     *
     * @param value allowed object is
     *              {@link OriginalTranslation }
     */
    public void setCATEGORY(OriginalTranslation value) {
        this.category = value;
    }

    /**
     * Obtient la valeur de la propriété form.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFORM() {
        if (form == null) {
            return "F04";
        } else {
            return form;
        }
    }

    /**
     * Définit la valeur de la propriété form.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFORM(String value) {
        this.form = value;
    }

}
