//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import eu.europa.publications.resource.schema.ted._2021.nuts.Nuts;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * Section II: OBJECT OF THE CONTRACT
 *
 * <p>Classe Java pour object_contract_move complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="object_contract_move"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TITLE"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}REFERENCE_NUMBER" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CPV_MAIN"/&gt;
 *         &lt;element name="TYPE_CONTRACT" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}services"/&gt;
 *         &lt;element name="CATEGORY" maxOccurs="9"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;category_service_move"&gt;
 *                 &lt;attribute name="CTYPE" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="SERVICES" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="OBJECT_DESCR"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CPV_ADDITIONAL" maxOccurs="100" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/2016/nuts}NUTS" maxOccurs="250"/&gt;
 *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}MAIN_SITE"/&gt;
 *                   &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SHORT_DESCR"/&gt;
 *                   &lt;sequence&gt;
 *                     &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DATE_START"/&gt;
 *                     &lt;element name="DURATION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}duration_m_d"/&gt;
 *                   &lt;/sequence&gt;
 *                   &lt;element name="ESSENTIAL_ASSETS" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;choice&gt;
 *                             &lt;sequence&gt;
 *                               &lt;element name="EXTENDED_CONTRACT_DURATION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty"/&gt;
 *                               &lt;element name="LIST" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines"/&gt;
 *                               &lt;element name="SIGNIFICANCE" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines"/&gt;
 *                               &lt;element name="PREDOMINANCE" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines"/&gt;
 *                             &lt;/sequence&gt;
 *                             &lt;element name="NO_EXTENDED_CONTRACT_DURATION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty"/&gt;
 *                           &lt;/choice&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "object_contract_move", propOrder = {
        "title",
        "referencenumber",
        "cpvmain",
        "typecontract",
        "category",
        "objectdescr"
})
@ToString
@EqualsAndHashCode
public class ObjectContractMove {

    @XmlElement(name = "TITLE", required = true)
    protected TextFtSingleLine title;
    @XmlElement(name = "REFERENCE_NUMBER")
    protected String referencenumber;
    @XmlElement(name = "CPV_MAIN", required = true)
    protected CpvSet cpvmain;
    @XmlElement(name = "TYPE_CONTRACT", required = true)
    protected Services typecontract;
    @XmlElement(name = "CATEGORY", required = true)
    protected List<ObjectContractMove.CATEGORY> category;
    @XmlElement(name = "OBJECT_DESCR", required = true)
    protected ObjectContractMove.OBJECTDESCR objectdescr;

    /**
     * Obtient la valeur de la propriété title.
     *
     * @return possible object is
     * {@link TextFtSingleLine }
     */
    public TextFtSingleLine getTITLE() {
        return title;
    }

    /**
     * Définit la valeur de la propriété title.
     *
     * @param value allowed object is
     *              {@link TextFtSingleLine }
     */
    public void setTITLE(TextFtSingleLine value) {
        this.title = value;
    }

    /**
     * Obtient la valeur de la propriété referencenumber.
     *
     * @return possible object is
     * {@link String }
     */
    public String getREFERENCENUMBER() {
        return referencenumber;
    }

    /**
     * Définit la valeur de la propriété referencenumber.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setREFERENCENUMBER(String value) {
        this.referencenumber = value;
    }

    /**
     * Obtient la valeur de la propriété cpvmain.
     *
     * @return possible object is
     * {@link CpvSet }
     */
    public CpvSet getCPVMAIN() {
        return cpvmain;
    }

    /**
     * Définit la valeur de la propriété cpvmain.
     *
     * @param value allowed object is
     *              {@link CpvSet }
     */
    public void setCPVMAIN(CpvSet value) {
        this.cpvmain = value;
    }

    /**
     * Obtient la valeur de la propriété typecontract.
     *
     * @return possible object is
     * {@link Services }
     */
    public Services getTYPECONTRACT() {
        return typecontract;
    }

    /**
     * Définit la valeur de la propriété typecontract.
     *
     * @param value allowed object is
     *              {@link Services }
     */
    public void setTYPECONTRACT(Services value) {
        this.typecontract = value;
    }

    /**
     * Gets the value of the category property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the category property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCATEGORY().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectContractMove.CATEGORY }
     */
    public List<ObjectContractMove.CATEGORY> getCATEGORY() {
        if (category == null) {
            category = new ArrayList<ObjectContractMove.CATEGORY>();
        }
        return this.category;
    }

    /**
     * Obtient la valeur de la propriété objectdescr.
     *
     * @return possible object is
     * {@link ObjectContractMove.OBJECTDESCR }
     */
    public ObjectContractMove.OBJECTDESCR getOBJECTDESCR() {
        return objectdescr;
    }

    /**
     * Définit la valeur de la propriété objectdescr.
     *
     * @param value allowed object is
     *              {@link ObjectContractMove.OBJECTDESCR }
     */
    public void setOBJECTDESCR(ObjectContractMove.OBJECTDESCR value) {
        this.objectdescr = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;category_service_move"&gt;
     *       &lt;attribute name="CTYPE" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="SERVICES" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "value"
    })
    @ToString
    @EqualsAndHashCode
    public static class CATEGORY {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "CTYPE", required = true)
        @XmlSchemaType(name = "anySimpleType")
        protected String ctype;

        /**
         * Obtient la valeur de la propriété value.
         *
         * @return possible object is
         * {@link String }
         */
        public String getValue() {
            return value;
        }

        /**
         * Définit la valeur de la propriété value.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Obtient la valeur de la propriété ctype.
         *
         * @return possible object is
         * {@link String }
         */
        public String getCTYPE() {
            if (ctype == null) {
                return "SERVICES";
            } else {
                return ctype;
            }
        }

        /**
         * Définit la valeur de la propriété ctype.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setCTYPE(String value) {
            this.ctype = value;
        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}CPV_ADDITIONAL" maxOccurs="100" minOccurs="0"/&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/2016/nuts}NUTS" maxOccurs="250"/&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}MAIN_SITE"/&gt;
     *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}SHORT_DESCR"/&gt;
     *         &lt;sequence&gt;
     *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DATE_START"/&gt;
     *           &lt;element name="DURATION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}duration_m_d"/&gt;
     *         &lt;/sequence&gt;
     *         &lt;element name="ESSENTIAL_ASSETS" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;choice&gt;
     *                   &lt;sequence&gt;
     *                     &lt;element name="EXTENDED_CONTRACT_DURATION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty"/&gt;
     *                     &lt;element name="LIST" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines"/&gt;
     *                     &lt;element name="SIGNIFICANCE" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines"/&gt;
     *                     &lt;element name="PREDOMINANCE" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines"/&gt;
     *                   &lt;/sequence&gt;
     *                   &lt;element name="NO_EXTENDED_CONTRACT_DURATION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty"/&gt;
     *                 &lt;/choice&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "cpvadditional",
            "nuts",
            "mainsite",
            "shortdescr",
            "datestart",
            "duration",
            "essentialassets"
    })
    @ToString
    @EqualsAndHashCode
    public static class OBJECTDESCR {

        @XmlElement(name = "CPV_ADDITIONAL")
        protected List<CpvSet> cpvadditional;
        @XmlElement(name = "NUTS", namespace = "http://publications.europa.eu/resource/schema/ted/2016/nuts", required = true)
        protected List<Nuts> nuts;
        @XmlElement(name = "MAIN_SITE", required = true)
        protected TextFtMultiLines mainsite;
        @XmlElement(name = "SHORT_DESCR", required = true)
        protected TextFtMultiLines shortdescr;
        @XmlElement(name = "DATE_START", required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar datestart;
        @XmlElement(name = "DURATION", required = true)
        protected DurationMD duration;
        @XmlElement(name = "ESSENTIAL_ASSETS")
        protected ObjectContractMove.OBJECTDESCR.ESSENTIALASSETS essentialassets;

        /**
         * Gets the value of the cpvadditional property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the cpvadditional property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCPVADDITIONAL().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CpvSet }
         */
        public List<CpvSet> getCPVADDITIONAL() {
            if (cpvadditional == null) {
                cpvadditional = new ArrayList<CpvSet>();
            }
            return this.cpvadditional;
        }

        /**
         * Gets the value of the nuts property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the nuts property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getNUTS().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Nuts }
         */
        public List<Nuts> getNUTS() {
            if (nuts == null) {
                nuts = new ArrayList<Nuts>();
            }
            return this.nuts;
        }

        /**
         * Obtient la valeur de la propriété mainsite.
         *
         * @return possible object is
         * {@link TextFtMultiLines }
         */
        public TextFtMultiLines getMAINSITE() {
            return mainsite;
        }

        /**
         * Définit la valeur de la propriété mainsite.
         *
         * @param value allowed object is
         *              {@link TextFtMultiLines }
         */
        public void setMAINSITE(TextFtMultiLines value) {
            this.mainsite = value;
        }

        /**
         * Obtient la valeur de la propriété shortdescr.
         *
         * @return possible object is
         * {@link TextFtMultiLines }
         */
        public TextFtMultiLines getSHORTDESCR() {
            return shortdescr;
        }

        /**
         * Définit la valeur de la propriété shortdescr.
         *
         * @param value allowed object is
         *              {@link TextFtMultiLines }
         */
        public void setSHORTDESCR(TextFtMultiLines value) {
            this.shortdescr = value;
        }

        /**
         * Obtient la valeur de la propriété datestart.
         *
         * @return possible object is
         * {@link XMLGregorianCalendar }
         */
        public XMLGregorianCalendar getDATESTART() {
            return datestart;
        }

        /**
         * Définit la valeur de la propriété datestart.
         *
         * @param value allowed object is
         *              {@link XMLGregorianCalendar }
         */
        public void setDATESTART(XMLGregorianCalendar value) {
            this.datestart = value;
        }

        /**
         * Obtient la valeur de la propriété duration.
         *
         * @return possible object is
         * {@link DurationMD }
         */
        public DurationMD getDURATION() {
            return duration;
        }

        /**
         * Définit la valeur de la propriété duration.
         *
         * @param value allowed object is
         *              {@link DurationMD }
         */
        public void setDURATION(DurationMD value) {
            this.duration = value;
        }

        /**
         * Obtient la valeur de la propriété essentialassets.
         *
         * @return possible object is
         * {@link ObjectContractMove.OBJECTDESCR.ESSENTIALASSETS }
         */
        public ObjectContractMove.OBJECTDESCR.ESSENTIALASSETS getESSENTIALASSETS() {
            return essentialassets;
        }

        /**
         * Définit la valeur de la propriété essentialassets.
         *
         * @param value allowed object is
         *              {@link ObjectContractMove.OBJECTDESCR.ESSENTIALASSETS }
         */
        public void setESSENTIALASSETS(ObjectContractMove.OBJECTDESCR.ESSENTIALASSETS value) {
            this.essentialassets = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;choice&gt;
         *         &lt;sequence&gt;
         *           &lt;element name="EXTENDED_CONTRACT_DURATION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty"/&gt;
         *           &lt;element name="LIST" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines"/&gt;
         *           &lt;element name="SIGNIFICANCE" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines"/&gt;
         *           &lt;element name="PREDOMINANCE" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}text_ft_multi_lines"/&gt;
         *         &lt;/sequence&gt;
         *         &lt;element name="NO_EXTENDED_CONTRACT_DURATION" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty"/&gt;
         *       &lt;/choice&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "extendedcontractduration",
                "list",
                "significance",
                "predominance",
                "noextendedcontractduration"
        })
        @ToString
        @EqualsAndHashCode
        public static class ESSENTIALASSETS {

            @XmlElement(name = "EXTENDED_CONTRACT_DURATION")
            protected Empty extendedcontractduration;
            @XmlElement(name = "LIST")
            protected TextFtMultiLines list;
            @XmlElement(name = "SIGNIFICANCE")
            protected TextFtMultiLines significance;
            @XmlElement(name = "PREDOMINANCE")
            protected TextFtMultiLines predominance;
            @XmlElement(name = "NO_EXTENDED_CONTRACT_DURATION")
            protected Empty noextendedcontractduration;

            /**
             * Obtient la valeur de la propriété extendedcontractduration.
             *
             * @return possible object is
             * {@link Empty }
             */
            public Empty getEXTENDEDCONTRACTDURATION() {
                return extendedcontractduration;
            }

            /**
             * Définit la valeur de la propriété extendedcontractduration.
             *
             * @param value allowed object is
             *              {@link Empty }
             */
            public void setEXTENDEDCONTRACTDURATION(Empty value) {
                this.extendedcontractduration = value;
            }

            /**
             * Obtient la valeur de la propriété list.
             *
             * @return possible object is
             * {@link TextFtMultiLines }
             */
            public TextFtMultiLines getLIST() {
                return list;
            }

            /**
             * Définit la valeur de la propriété list.
             *
             * @param value allowed object is
             *              {@link TextFtMultiLines }
             */
            public void setLIST(TextFtMultiLines value) {
                this.list = value;
            }

            /**
             * Obtient la valeur de la propriété significance.
             *
             * @return possible object is
             * {@link TextFtMultiLines }
             */
            public TextFtMultiLines getSIGNIFICANCE() {
                return significance;
            }

            /**
             * Définit la valeur de la propriété significance.
             *
             * @param value allowed object is
             *              {@link TextFtMultiLines }
             */
            public void setSIGNIFICANCE(TextFtMultiLines value) {
                this.significance = value;
            }

            /**
             * Obtient la valeur de la propriété predominance.
             *
             * @return possible object is
             * {@link TextFtMultiLines }
             */
            public TextFtMultiLines getPREDOMINANCE() {
                return predominance;
            }

            /**
             * Définit la valeur de la propriété predominance.
             *
             * @param value allowed object is
             *              {@link TextFtMultiLines }
             */
            public void setPREDOMINANCE(TextFtMultiLines value) {
                this.predominance = value;
            }

            /**
             * Obtient la valeur de la propriété noextendedcontractduration.
             *
             * @return possible object is
             * {@link Empty }
             */
            public Empty getNOEXTENDEDCONTRACTDURATION() {
                return noextendedcontractduration;
            }

            /**
             * Définit la valeur de la propriété noextendedcontractduration.
             *
             * @param value allowed object is
             *              {@link Empty }
             */
            public void setNOEXTENDEDCONTRACTDURATION(Empty value) {
                this.noextendedcontractduration = value;
            }

        }

    }

}
