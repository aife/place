//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour prct_range complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="prct_range"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence minOccurs="0"&gt;
 *         &lt;element name="MIN"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}prct"&gt;
 *               &lt;totalDigits value="2"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="MAX"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}prct"&gt;
 *               &lt;totalDigits value="2"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prct_range", propOrder = {
        "min",
        "max"
})
@ToString
@EqualsAndHashCode
public class PrctRange {

    @XmlElement(name = "MIN")
    protected Integer min;
    @XmlElement(name = "MAX")
    protected Integer max;

    /**
     * Obtient la valeur de la propriété min.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getMIN() {
        return min;
    }

    /**
     * Définit la valeur de la propriété min.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setMIN(Integer value) {
        this.min = value;
    }

    /**
     * Obtient la valeur de la propriété max.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getMAX() {
        return max;
    }

    /**
     * Définit la valeur de la propriété max.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setMAX(Integer value) {
        this.max = value;
    }

}
