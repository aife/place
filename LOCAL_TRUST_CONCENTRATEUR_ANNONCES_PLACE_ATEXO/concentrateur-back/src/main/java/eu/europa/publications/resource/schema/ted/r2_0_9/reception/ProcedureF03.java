//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Section IV: PROCEDURE
 *
 * <p>Classe Java pour procedure_f03 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="procedure_f03"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;sequence&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_OPEN"/&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ACCELERATED_PROC" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *           &lt;sequence&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_RESTRICTED"/&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ACCELERATED_PROC" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *           &lt;sequence&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_COMPETITIVE_NEGOTIATION"/&gt;
 *             &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ACCELERATED_PROC" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_COMPETITIVE_DIALOGUE"/&gt;
 *           &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}PT_INNOVATION_PARTNERSHIP"/&gt;
 *           &lt;element name="PT_AWARD_CONTRACT_WITHOUT_CALL" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}annex_d1"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="FRAMEWORK" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}empty" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DPS" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}EAUCTION_USED" minOccurs="0"/&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}gpa"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NOTICE_NUMBER_OJ" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TERMINATION_DPS" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}TERMINATION_PIN" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "procedure_f03", propOrder = {
        "content"
})
@ToString
@EqualsAndHashCode
public class ProcedureF03 {

    @XmlElementRefs({
            @XmlElementRef(name = "PT_OPEN", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "ACCELERATED_PROC", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "PT_RESTRICTED", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "PT_COMPETITIVE_NEGOTIATION", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "PT_COMPETITIVE_DIALOGUE", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "PT_INNOVATION_PARTNERSHIP", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "PT_AWARD_CONTRACT_WITHOUT_CALL", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "FRAMEWORK", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "DPS", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "EAUCTION_USED", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "CONTRACT_COVERED_GPA", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "NO_CONTRACT_COVERED_GPA", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "NOTICE_NUMBER_OJ", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "TERMINATION_DPS", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "TERMINATION_PIN", namespace = "http://publications.europa.eu/resource/schema/ted/R2.0.9/reception", type = JAXBElement.class, required = false)
    })
    protected List<JAXBElement<?>> content;

    /**
     * Obtient le reste du modèle de contenu.
     *
     * <p>
     * Vous obtenez la propriété "catch-all" pour la raison suivante :
     * Le nom de champ "ACCELERATEDPROC" est utilisé par deux parties différentes d'un schéma. Reportez-vous à :
     * ligne 82 sur file:/home/iat-atx/Bureau/ATEXO/WORKSPACE/lt_concentrateur_annonces/concentrateur-back/src/main/resources/simplified-xsd/joue/F03_2014.xsd
     * ligne 78 sur file:/home/iat-atx/Bureau/ATEXO/WORKSPACE/lt_concentrateur_annonces/concentrateur-back/src/main/resources/simplified-xsd/joue/F03_2014.xsd
     * <p>
     * Pour vous débarrasser de cette propriété, appliquez une personnalisation de propriété à l'une
     * des deux déclarations suivantes afin de modifier leurs noms :
     * Gets the value of the content property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link TextFtMultiLines }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link AnnexD1 }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     * {@link JAXBElement }{@code <}{@link Empty }{@code >}
     */
    public List<JAXBElement<?>> getContent() {
        if (content == null) {
            content = new ArrayList<JAXBElement<?>>();
        }
        return this.content;
    }

}
