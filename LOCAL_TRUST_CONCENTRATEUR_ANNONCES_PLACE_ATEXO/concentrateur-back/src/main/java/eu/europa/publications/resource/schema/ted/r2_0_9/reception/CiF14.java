//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Section VI: COMPLEMENTARY INFORMATION
 *
 * <p>Classe Java pour ci_f14 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="ci_f14"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DATE_DISPATCH_NOTICE"/&gt;
 *         &lt;choice&gt;
 *           &lt;sequence&gt;
 *             &lt;choice&gt;
 *               &lt;element name="ORIGINAL_ENOTICES" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}non_published"/&gt;
 *               &lt;element name="ORIGINAL_TED_ESENDER" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}non_published"/&gt;
 *             &lt;/choice&gt;
 *             &lt;sequence&gt;
 *               &lt;element name="ESENDER_LOGIN"&gt;
 *                 &lt;complexType&gt;
 *                   &lt;simpleContent&gt;
 *                     &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;esender_login"&gt;
 *                       &lt;attribute name="PUBLICATION" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="NO" /&gt;
 *                     &lt;/extension&gt;
 *                   &lt;/simpleContent&gt;
 *                 &lt;/complexType&gt;
 *               &lt;/element&gt;
 *               &lt;element name="CUSTOMER_LOGIN" minOccurs="0"&gt;
 *                 &lt;complexType&gt;
 *                   &lt;simpleContent&gt;
 *                     &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;customer_login"&gt;
 *                       &lt;attribute name="PUBLICATION" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="NO" /&gt;
 *                     &lt;/extension&gt;
 *                   &lt;/simpleContent&gt;
 *                 &lt;/complexType&gt;
 *               &lt;/element&gt;
 *               &lt;element name="NO_DOC_EXT"&gt;
 *                 &lt;complexType&gt;
 *                   &lt;simpleContent&gt;
 *                     &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;no_doc_ext"&gt;
 *                       &lt;attribute name="PUBLICATION" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="NO" /&gt;
 *                     &lt;/extension&gt;
 *                   &lt;/simpleContent&gt;
 *                 &lt;/complexType&gt;
 *               &lt;/element&gt;
 *             &lt;/sequence&gt;
 *           &lt;/sequence&gt;
 *           &lt;sequence&gt;
 *             &lt;element name="ORIGINAL_OTHER_MEANS"&gt;
 *               &lt;complexType&gt;
 *                 &lt;simpleContent&gt;
 *                   &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;string_200"&gt;
 *                     &lt;attribute name="PUBLICATION" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="NO" /&gt;
 *                   &lt;/extension&gt;
 *                 &lt;/simpleContent&gt;
 *               &lt;/complexType&gt;
 *             &lt;/element&gt;
 *           &lt;/sequence&gt;
 *         &lt;/choice&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NOTICE_NUMBER_OJ" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}DATE_DISPATCH_ORIGINAL"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ci_f14", propOrder = {
        "datedispatchnotice",
        "originalenotices",
        "originaltedesender",
        "esenderlogin",
        "customerlogin",
        "nodocext",
        "originalothermeans",
        "noticenumberoj",
        "datedispatchoriginal"
})
@ToString
@EqualsAndHashCode
public class CiF14 {

    @XmlElement(name = "DATE_DISPATCH_NOTICE", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datedispatchnotice;
    @XmlElement(name = "ORIGINAL_ENOTICES")
    protected NonPublished originalenotices;
    @XmlElement(name = "ORIGINAL_TED_ESENDER")
    protected NonPublished originaltedesender;
    @XmlElement(name = "ESENDER_LOGIN")
    protected CiF14.ESENDERLOGIN esenderlogin;
    @XmlElement(name = "CUSTOMER_LOGIN")
    protected CiF14.CUSTOMERLOGIN customerlogin;
    @XmlElement(name = "NO_DOC_EXT")
    protected CiF14.NODOCEXT nodocext;
    @XmlElement(name = "ORIGINAL_OTHER_MEANS")
    protected CiF14.ORIGINALOTHERMEANS originalothermeans;
    @XmlElement(name = "NOTICE_NUMBER_OJ")
    protected String noticenumberoj;
    @XmlElement(name = "DATE_DISPATCH_ORIGINAL", required = true)
    protected DATEDISPATCHORIGINAL datedispatchoriginal;

    /**
     * Obtient la valeur de la propriété datedispatchnotice.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDATEDISPATCHNOTICE() {
        return datedispatchnotice;
    }

    /**
     * Définit la valeur de la propriété datedispatchnotice.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDATEDISPATCHNOTICE(XMLGregorianCalendar value) {
        this.datedispatchnotice = value;
    }

    /**
     * Obtient la valeur de la propriété originalenotices.
     *
     * @return possible object is
     * {@link NonPublished }
     */
    public NonPublished getORIGINALENOTICES() {
        return originalenotices;
    }

    /**
     * Définit la valeur de la propriété originalenotices.
     *
     * @param value allowed object is
     *              {@link NonPublished }
     */
    public void setORIGINALENOTICES(NonPublished value) {
        this.originalenotices = value;
    }

    /**
     * Obtient la valeur de la propriété originaltedesender.
     *
     * @return possible object is
     * {@link NonPublished }
     */
    public NonPublished getORIGINALTEDESENDER() {
        return originaltedesender;
    }

    /**
     * Définit la valeur de la propriété originaltedesender.
     *
     * @param value allowed object is
     *              {@link NonPublished }
     */
    public void setORIGINALTEDESENDER(NonPublished value) {
        this.originaltedesender = value;
    }

    /**
     * Obtient la valeur de la propriété esenderlogin.
     *
     * @return possible object is
     * {@link CiF14 .ESENDERLOGIN }
     */
    public CiF14.ESENDERLOGIN getESENDERLOGIN() {
        return esenderlogin;
    }

    /**
     * Définit la valeur de la propriété esenderlogin.
     *
     * @param value allowed object is
     *              {@link CiF14 .ESENDERLOGIN }
     */
    public void setESENDERLOGIN(CiF14.ESENDERLOGIN value) {
        this.esenderlogin = value;
    }

    /**
     * Obtient la valeur de la propriété customerlogin.
     *
     * @return possible object is
     * {@link CiF14 .CUSTOMERLOGIN }
     */
    public CiF14.CUSTOMERLOGIN getCUSTOMERLOGIN() {
        return customerlogin;
    }

    /**
     * Définit la valeur de la propriété customerlogin.
     *
     * @param value allowed object is
     *              {@link CiF14 .CUSTOMERLOGIN }
     */
    public void setCUSTOMERLOGIN(CiF14.CUSTOMERLOGIN value) {
        this.customerlogin = value;
    }

    /**
     * Obtient la valeur de la propriété nodocext.
     *
     * @return possible object is
     * {@link CiF14 .NODOCEXT }
     */
    public CiF14.NODOCEXT getNODOCEXT() {
        return nodocext;
    }

    /**
     * Définit la valeur de la propriété nodocext.
     *
     * @param value allowed object is
     *              {@link CiF14 .NODOCEXT }
     */
    public void setNODOCEXT(CiF14.NODOCEXT value) {
        this.nodocext = value;
    }

    /**
     * Obtient la valeur de la propriété originalothermeans.
     *
     * @return possible object is
     * {@link CiF14 .ORIGINALOTHERMEANS }
     */
    public CiF14.ORIGINALOTHERMEANS getORIGINALOTHERMEANS() {
        return originalothermeans;
    }

    /**
     * Définit la valeur de la propriété originalothermeans.
     *
     * @param value allowed object is
     *              {@link CiF14 .ORIGINALOTHERMEANS }
     */
    public void setORIGINALOTHERMEANS(CiF14.ORIGINALOTHERMEANS value) {
        this.originalothermeans = value;
    }

    /**
     * Obtient la valeur de la propriété noticenumberoj.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNOTICENUMBEROJ() {
        return noticenumberoj;
    }

    /**
     * Définit la valeur de la propriété noticenumberoj.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNOTICENUMBEROJ(String value) {
        this.noticenumberoj = value;
    }

    /**
     * Obtient la valeur de la propriété datedispatchoriginal.
     *
     * @return possible object is
     * {@link DATEDISPATCHORIGINAL }
     */
    public DATEDISPATCHORIGINAL getDATEDISPATCHORIGINAL() {
        return datedispatchoriginal;
    }

    /**
     * Définit la valeur de la propriété datedispatchoriginal.
     *
     * @param value allowed object is
     *              {@link DATEDISPATCHORIGINAL }
     */
    public void setDATEDISPATCHORIGINAL(DATEDISPATCHORIGINAL value) {
        this.datedispatchoriginal = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;customer_login"&gt;
     *       &lt;attribute name="PUBLICATION" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="NO" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "value"
    })
    @ToString
    @EqualsAndHashCode
    public static class CUSTOMERLOGIN {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "PUBLICATION", required = true)
        @XmlSchemaType(name = "anySimpleType")
        protected String publication;

        /**
         * Obtient la valeur de la propriété value.
         *
         * @return possible object is
         * {@link String }
         */
        public String getValue() {
            return value;
        }

        /**
         * Définit la valeur de la propriété value.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Obtient la valeur de la propriété publication.
         *
         * @return possible object is
         * {@link String }
         */
        public String getPUBLICATION() {
            if (publication == null) {
                return "NO";
            } else {
                return publication;
            }
        }

        /**
         * Définit la valeur de la propriété publication.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setPUBLICATION(String value) {
            this.publication = value;
        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;esender_login"&gt;
     *       &lt;attribute name="PUBLICATION" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="NO" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "value"
    })
    @ToString
    @EqualsAndHashCode
    public static class ESENDERLOGIN {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "PUBLICATION", required = true)
        @XmlSchemaType(name = "anySimpleType")
        protected String publication;

        /**
         * TED + 5 random digits/letters in upper case, or country ISO2 + 3 digits, or TED + 2
         * digits/letters or ENOTICES
         *
         * @return possible object is
         * {@link String }
         */
        public String getValue() {
            return value;
        }

        /**
         * Définit la valeur de la propriété value.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Obtient la valeur de la propriété publication.
         *
         * @return possible object is
         * {@link String }
         */
        public String getPUBLICATION() {
            if (publication == null) {
                return "NO";
            } else {
                return publication;
            }
        }

        /**
         * Définit la valeur de la propriété publication.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setPUBLICATION(String value) {
            this.publication = value;
        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;no_doc_ext"&gt;
     *       &lt;attribute name="PUBLICATION" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="NO" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "value"
    })
    @ToString
    @EqualsAndHashCode
    public static class NODOCEXT {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "PUBLICATION", required = true)
        @XmlSchemaType(name = "anySimpleType")
        protected String publication;

        /**
         * Obtient la valeur de la propriété value.
         *
         * @return possible object is
         * {@link String }
         */
        public String getValue() {
            return value;
        }

        /**
         * Définit la valeur de la propriété value.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Obtient la valeur de la propriété publication.
         *
         * @return possible object is
         * {@link String }
         */
        public String getPUBLICATION() {
            if (publication == null) {
                return "NO";
            } else {
                return publication;
            }
        }

        /**
         * Définit la valeur de la propriété publication.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setPUBLICATION(String value) {
            this.publication = value;
        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://publications.europa.eu/resource/schema/ted/R2.0.9/reception&gt;string_200"&gt;
     *       &lt;attribute name="PUBLICATION" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="NO" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "value"
    })
    @ToString
    @EqualsAndHashCode
    public static class ORIGINALOTHERMEANS {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "PUBLICATION", required = true)
        @XmlSchemaType(name = "anySimpleType")
        protected String publication;

        /**
         * Obtient la valeur de la propriété value.
         *
         * @return possible object is
         * {@link String }
         */
        public String getValue() {
            return value;
        }

        /**
         * Définit la valeur de la propriété value.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Obtient la valeur de la propriété publication.
         *
         * @return possible object is
         * {@link String }
         */
        public String getPUBLICATION() {
            if (publication == null) {
                return "NO";
            } else {
                return publication;
            }
        }

        /**
         * Définit la valeur de la propriété publication.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setPUBLICATION(String value) {
            this.publication = value;
        }

    }

}
