//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.authority.currency;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour t_currency_tedschema.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * <pre>
 * &lt;simpleType name="t_currency_tedschema"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="AED"/&gt;
 *     &lt;enumeration value="AFN"/&gt;
 *     &lt;enumeration value="ALL"/&gt;
 *     &lt;enumeration value="AMD"/&gt;
 *     &lt;enumeration value="ANG"/&gt;
 *     &lt;enumeration value="AOA"/&gt;
 *     &lt;enumeration value="ARS"/&gt;
 *     &lt;enumeration value="AUD"/&gt;
 *     &lt;enumeration value="AWG"/&gt;
 *     &lt;enumeration value="AZN"/&gt;
 *     &lt;enumeration value="BAM"/&gt;
 *     &lt;enumeration value="BBD"/&gt;
 *     &lt;enumeration value="BDT"/&gt;
 *     &lt;enumeration value="BGN"/&gt;
 *     &lt;enumeration value="BHD"/&gt;
 *     &lt;enumeration value="BIF"/&gt;
 *     &lt;enumeration value="BMD"/&gt;
 *     &lt;enumeration value="BND"/&gt;
 *     &lt;enumeration value="BOB"/&gt;
 *     &lt;enumeration value="BRL"/&gt;
 *     &lt;enumeration value="BSD"/&gt;
 *     &lt;enumeration value="BTN"/&gt;
 *     &lt;enumeration value="BWP"/&gt;
 *     &lt;enumeration value="BYN"/&gt;
 *     &lt;enumeration value="BZD"/&gt;
 *     &lt;enumeration value="CAD"/&gt;
 *     &lt;enumeration value="CDF"/&gt;
 *     &lt;enumeration value="CHF"/&gt;
 *     &lt;enumeration value="CLP"/&gt;
 *     &lt;enumeration value="CNY"/&gt;
 *     &lt;enumeration value="COP"/&gt;
 *     &lt;enumeration value="CRC"/&gt;
 *     &lt;enumeration value="CUC"/&gt;
 *     &lt;enumeration value="CUP"/&gt;
 *     &lt;enumeration value="CVE"/&gt;
 *     &lt;enumeration value="CZK"/&gt;
 *     &lt;enumeration value="DJF"/&gt;
 *     &lt;enumeration value="DKK"/&gt;
 *     &lt;enumeration value="DOP"/&gt;
 *     &lt;enumeration value="DZD"/&gt;
 *     &lt;enumeration value="EGP"/&gt;
 *     &lt;enumeration value="ERN"/&gt;
 *     &lt;enumeration value="ETB"/&gt;
 *     &lt;enumeration value="EUR"/&gt;
 *     &lt;enumeration value="FJD"/&gt;
 *     &lt;enumeration value="FKP"/&gt;
 *     &lt;enumeration value="GBP"/&gt;
 *     &lt;enumeration value="GEL"/&gt;
 *     &lt;enumeration value="GHS"/&gt;
 *     &lt;enumeration value="GIP"/&gt;
 *     &lt;enumeration value="GMD"/&gt;
 *     &lt;enumeration value="GNF"/&gt;
 *     &lt;enumeration value="GTQ"/&gt;
 *     &lt;enumeration value="GYD"/&gt;
 *     &lt;enumeration value="HKD"/&gt;
 *     &lt;enumeration value="HNL"/&gt;
 *     &lt;enumeration value="HRK"/&gt;
 *     &lt;enumeration value="HTG"/&gt;
 *     &lt;enumeration value="HUF"/&gt;
 *     &lt;enumeration value="IDR"/&gt;
 *     &lt;enumeration value="ILS"/&gt;
 *     &lt;enumeration value="INR"/&gt;
 *     &lt;enumeration value="IQD"/&gt;
 *     &lt;enumeration value="IRR"/&gt;
 *     &lt;enumeration value="ISK"/&gt;
 *     &lt;enumeration value="JMD"/&gt;
 *     &lt;enumeration value="JOD"/&gt;
 *     &lt;enumeration value="JPY"/&gt;
 *     &lt;enumeration value="KES"/&gt;
 *     &lt;enumeration value="KGS"/&gt;
 *     &lt;enumeration value="KHR"/&gt;
 *     &lt;enumeration value="KMF"/&gt;
 *     &lt;enumeration value="KPW"/&gt;
 *     &lt;enumeration value="KRW"/&gt;
 *     &lt;enumeration value="KWD"/&gt;
 *     &lt;enumeration value="KYD"/&gt;
 *     &lt;enumeration value="KZT"/&gt;
 *     &lt;enumeration value="LAK"/&gt;
 *     &lt;enumeration value="LBP"/&gt;
 *     &lt;enumeration value="LKR"/&gt;
 *     &lt;enumeration value="LRD"/&gt;
 *     &lt;enumeration value="LSL"/&gt;
 *     &lt;enumeration value="LYD"/&gt;
 *     &lt;enumeration value="MAD"/&gt;
 *     &lt;enumeration value="MDL"/&gt;
 *     &lt;enumeration value="MGA"/&gt;
 *     &lt;enumeration value="MKD"/&gt;
 *     &lt;enumeration value="MMK"/&gt;
 *     &lt;enumeration value="MNT"/&gt;
 *     &lt;enumeration value="MOP"/&gt;
 *     &lt;enumeration value="MRU"/&gt;
 *     &lt;enumeration value="MUR"/&gt;
 *     &lt;enumeration value="MVR"/&gt;
 *     &lt;enumeration value="MWK"/&gt;
 *     &lt;enumeration value="MXN"/&gt;
 *     &lt;enumeration value="MYR"/&gt;
 *     &lt;enumeration value="MZN"/&gt;
 *     &lt;enumeration value="NAD"/&gt;
 *     &lt;enumeration value="NGN"/&gt;
 *     &lt;enumeration value="NIO"/&gt;
 *     &lt;enumeration value="NOK"/&gt;
 *     &lt;enumeration value="NPR"/&gt;
 *     &lt;enumeration value="NZD"/&gt;
 *     &lt;enumeration value="OMR"/&gt;
 *     &lt;enumeration value="PAB"/&gt;
 *     &lt;enumeration value="PEN"/&gt;
 *     &lt;enumeration value="PGK"/&gt;
 *     &lt;enumeration value="PHP"/&gt;
 *     &lt;enumeration value="PKR"/&gt;
 *     &lt;enumeration value="PLN"/&gt;
 *     &lt;enumeration value="PYG"/&gt;
 *     &lt;enumeration value="QAR"/&gt;
 *     &lt;enumeration value="RON"/&gt;
 *     &lt;enumeration value="RSD"/&gt;
 *     &lt;enumeration value="RUB"/&gt;
 *     &lt;enumeration value="RWF"/&gt;
 *     &lt;enumeration value="SAR"/&gt;
 *     &lt;enumeration value="SBD"/&gt;
 *     &lt;enumeration value="SCR"/&gt;
 *     &lt;enumeration value="SDG"/&gt;
 *     &lt;enumeration value="SEK"/&gt;
 *     &lt;enumeration value="SGD"/&gt;
 *     &lt;enumeration value="SHP"/&gt;
 *     &lt;enumeration value="SLL"/&gt;
 *     &lt;enumeration value="SOS"/&gt;
 *     &lt;enumeration value="SRD"/&gt;
 *     &lt;enumeration value="SSP"/&gt;
 *     &lt;enumeration value="STN"/&gt;
 *     &lt;enumeration value="SVC"/&gt;
 *     &lt;enumeration value="SYP"/&gt;
 *     &lt;enumeration value="SZL"/&gt;
 *     &lt;enumeration value="THB"/&gt;
 *     &lt;enumeration value="TJS"/&gt;
 *     &lt;enumeration value="TMT"/&gt;
 *     &lt;enumeration value="TND"/&gt;
 *     &lt;enumeration value="TOP"/&gt;
 *     &lt;enumeration value="TRY"/&gt;
 *     &lt;enumeration value="TTD"/&gt;
 *     &lt;enumeration value="TWD"/&gt;
 *     &lt;enumeration value="TZS"/&gt;
 *     &lt;enumeration value="UAH"/&gt;
 *     &lt;enumeration value="UGX"/&gt;
 *     &lt;enumeration value="USD"/&gt;
 *     &lt;enumeration value="USN"/&gt;
 *     &lt;enumeration value="UYU"/&gt;
 *     &lt;enumeration value="UZS"/&gt;
 *     &lt;enumeration value="VES"/&gt;
 *     &lt;enumeration value="VND"/&gt;
 *     &lt;enumeration value="VUV"/&gt;
 *     &lt;enumeration value="WST"/&gt;
 *     &lt;enumeration value="XAF"/&gt;
 *     &lt;enumeration value="XCD"/&gt;
 *     &lt;enumeration value="XOF"/&gt;
 *     &lt;enumeration value="XPF"/&gt;
 *     &lt;enumeration value="YER"/&gt;
 *     &lt;enumeration value="ZAR"/&gt;
 *     &lt;enumeration value="ZMW"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "t_currency_tedschema", namespace = "http://publications.europa.eu/resource/authority/currency")
@XmlEnum
public enum TCurrencyTedschema {


    /**
     * UAE dirham UAE dirhams  / adm.status[current]
     */
    AED,

    /**
     * Afghani  / adm.status[current]
     */
    AFN,

    /**
     * Lek Leks  / adm.status[current]
     */
    ALL,

    /**
     * Dram  / adm.status[current]
     */
    AMD,

    /**
     * Netherlands Antillean guilder Netherlands Antillean guilders  / adm.status[current]
     */
    ANG,

    /**
     * Angolan kwanza  / adm.status[current]
     */
    AOA,

    /**
     * Argentine peso Argentine pesos  / adm.status[current]
     */
    ARS,

    /**
     * Australian dollar Australian dollars  / adm.status[current]
     */
    AUD,

    /**
     * Aruban guilder Aruban guilders  / adm.status[current]
     */
    AWG,

    /**
     * Azerbaijan manat  / adm.status[current]
     */
    AZN,

    /**
     * Bosnia and Herzegovina convertible mark Bosnia and Herzegovina convertible marks  / adm.status[current]
     */
    BAM,

    /**
     * Barbados dollar Barbados dollars  / adm.status[current]
     */
    BBD,

    /**
     * Taka  / adm.status[current]
     */
    BDT,

    /**
     * Lev Leva  / adm.status[current]
     */
    BGN,

    /**
     * Bahraini dinar Bahraini dinars  / adm.status[current]
     */
    BHD,

    /**
     * Burundi franc Burundi francs  / adm.status[current]
     */
    BIF,

    /**
     * Bermuda dollar Bermuda dollars  / adm.status[current]
     */
    BMD,

    /**
     * Brunei dollar Brunei dollars  / adm.status[current]
     */
    BND,

    /**
     * Boliviano Bolivianos  / adm.status[current]
     */
    BOB,

    /**
     * Real Reais  / adm.status[current]
     */
    BRL,

    /**
     * Bahamian dollar Bahamian dollars  / adm.status[current]
     */
    BSD,

    /**
     * Ngultrum  / adm.status[current]
     */
    BTN,

    /**
     * Pula  / adm.status[current]
     */
    BWP,

    /**
     * Belarusian rouble Belarusian roubles  / adm.status[current]
     */
    BYN,

    /**
     * Belize dollar Belize dollars  / adm.status[current]
     */
    BZD,

    /**
     * Canadian dollar Canadian dollars  / adm.status[current]
     */
    CAD,

    /**
     * Congolese franc Congolese francs  / adm.status[current]
     */
    CDF,

    /**
     * Swiss franc Swiss francs  / adm.status[current]
     */
    CHF,

    /**
     * Chilean peso Chilean pesos  / adm.status[current]
     */
    CLP,

    /**
     * Renminbi-yuan  / adm.status[current]
     */
    CNY,

    /**
     * Colombian peso Colombian pesos  / adm.status[current]
     */
    COP,

    /**
     * Costa Rican colon Costa Rican colones  / adm.status[current]
     */
    CRC,

    /**
     * Convertible peso Convertible pesos  / adm.status[current]
     */
    CUC,

    /**
     * Cuban peso Cuban pesos  / adm.status[current]
     */
    CUP,

    /**
     * Cape Verde escudo Cape Verde escudos  / adm.status[current]
     */
    CVE,

    /**
     * Czech koruna Czech koruny  / adm.status[current]
     */
    CZK,

    /**
     * Djibouti franc Djibouti francs  / adm.status[current]
     */
    DJF,

    /**
     * Danish krone Danish kroner  / adm.status[current]
     */
    DKK,

    /**
     * Dominican peso Dominican pesos  / adm.status[current]
     */
    DOP,

    /**
     * Algerian dinar Algerian dinars  / adm.status[current]
     */
    DZD,

    /**
     * Egyptian pound Egyptian pounds  / adm.status[current]
     */
    EGP,

    /**
     * Nakfa Nakfas  / adm.status[current]
     */
    ERN,

    /**
     * Birr  / adm.status[current]
     */
    ETB,

    /**
     * Euro  / adm.status[current]
     */
    EUR,

    /**
     * Fiji dollar Fiji dollars  / adm.status[current]
     */
    FJD,

    /**
     * Falkland Islands pound Falkland Islands pounds  / adm.status[current]
     */
    FKP,

    /**
     * Pound sterling Pounds sterling  / adm.status[current]
     */
    GBP,

    /**
     * Lari Laris  / adm.status[current]
     */
    GEL,

    /**
     * Ghana cedi Ghana cedis  / adm.status[current]
     */
    GHS,

    /**
     * Gibraltar pound  / adm.status[current]
     */
    GIP,

    /**
     * Dalasi  / adm.status[current]
     */
    GMD,

    /**
     * Guinean franc Guinean francs  / adm.status[current]
     */
    GNF,

    /**
     * Quetzal Quetzales  / adm.status[current]
     */
    GTQ,

    /**
     * Guyana dollar Guyana dollars  / adm.status[current]
     */
    GYD,

    /**
     * Hong Kong dollar Hong Kong dollars  / adm.status[current]
     */
    HKD,

    /**
     * Lempira  / adm.status[current]
     */
    HNL,

    /**
     * Kuna  / adm.status[current]
     */
    HRK,

    /**
     * Gourde Gourdes  / adm.status[current]
     */
    HTG,

    /**
     * Forint  / adm.status[current]
     */
    HUF,

    /**
     * Indonesian rupiah  / adm.status[current]
     */
    IDR,

    /**
     * Shekel Shekels  / adm.status[current]
     */
    ILS,

    /**
     * Indian rupee Indian rupees  / adm.status[current]
     */
    INR,

    /**
     * Iraqi dinar Iraqi dinars  / adm.status[current]
     */
    IQD,

    /**
     * Iranian rial Iranian rials  / adm.status[current]
     */
    IRR,

    /**
     * Iceland króna Iceland krónur  / adm.status[current]
     */
    ISK,

    /**
     * Jamaica dollar Jamaica dollars  / adm.status[current]
     */
    JMD,

    /**
     * Jordanian dinar Jordanian dinars  / adm.status[current]
     */
    JOD,

    /**
     * Yen  / adm.status[current]
     */
    JPY,

    /**
     * Kenyan shilling Kenyan shillingq  / adm.status[current]
     */
    KES,

    /**
     * Som  / adm.status[current]
     */
    KGS,

    /**
     * Riel  / adm.status[current]
     */
    KHR,

    /**
     * Comorian franc Comorian francs  / adm.status[current]
     */
    KMF,

    /**
     * North Korean won  / adm.status[current]
     */
    KPW,

    /**
     * South Korean won  / adm.status[current]
     */
    KRW,

    /**
     * Kuwaiti dinar Kuwaiti dinars  / adm.status[current]
     */
    KWD,

    /**
     * Cayman Islands dollar Cayman Islands dollars  / adm.status[current]
     */
    KYD,

    /**
     * Tenge  / adm.status[current]
     */
    KZT,

    /**
     * Kip  / adm.status[current]
     */
    LAK,

    /**
     * Lebanese pound Lebanese pounds  / adm.status[current]
     */
    LBP,

    /**
     * Sri Lankan rupee Sri Lankan rupees  / adm.status[current]
     */
    LKR,

    /**
     * Liberian dollar Liberian dollars  / adm.status[current]
     */
    LRD,

    /**
     * Loti Maloti  / adm.status[current]
     */
    LSL,

    /**
     * Libyan dinar Libyan dinars  / adm.status[current]
     */
    LYD,

    /**
     * Moroccan dirham Moroccan dirhams  / adm.status[current]
     */
    MAD,

    /**
     * Moldovan leu Moldovan lei  / adm.status[current]
     */
    MDL,

    /**
     * Ariary  / adm.status[current]
     */
    MGA,

    /**
     * Denar Denars  / adm.status[current]
     */
    MKD,

    /**
     * Kyat Kyats  / adm.status[current]
     */
    MMK,

    /**
     * Tugrik  / adm.status[current]
     */
    MNT,

    /**
     * Pataca Patacas  / adm.status[current]
     */
    MOP,

    /**
     * Ouguiya Ouguiyas  / adm.status[current]
     */
    MRU,

    /**
     * Mauritian rupee Mauritian rupees  / adm.status[current]
     */
    MUR,

    /**
     * Rufiyaa  / adm.status[current]
     */
    MVR,

    /**
     * Malawian kwacha  / adm.status[current]
     */
    MWK,

    /**
     * Mexican peso Mexican pesos  / adm.status[current]
     */
    MXN,

    /**
     * Ringgit  / adm.status[current]
     */
    MYR,

    /**
     * Metical Meticais  / adm.status[current]
     */
    MZN,

    /**
     * Namibian dollar Namibian dollars  / adm.status[current]
     */
    NAD,

    /**
     * Naira  / adm.status[current]
     */
    NGN,

    /**
     * Córdoba oro Córdobas oro  / adm.status[current]
     */
    NIO,

    /**
     * Norwegian krone Norwegian kroner  / adm.status[current]
     */
    NOK,

    /**
     * Nepalese rupee Nepalese rupees  / adm.status[current]
     */
    NPR,

    /**
     * New Zealand dollar New Zealand dollars  / adm.status[current]
     */
    NZD,

    /**
     * Omani rial Omani rials  / adm.status[current]
     */
    OMR,

    /**
     * Balboa  / adm.status[current]
     */
    PAB,

    /**
     * Sol Sols  / adm.status[current]
     */
    PEN,

    /**
     * Kina  / adm.status[current]
     */
    PGK,

    /**
     * Philippine peso Philippine pesos  / adm.status[current]
     */
    PHP,

    /**
     * Pakistani rupee Pakistani rupees  / adm.status[current]
     */
    PKR,

    /**
     * Zloty Zlotys  / adm.status[current]
     */
    PLN,

    /**
     * Guaraní Guaraníes  / adm.status[current]
     */
    PYG,

    /**
     * Qatari rial Qatari rials  / adm.status[current]
     */
    QAR,

    /**
     * Romanian leu Romanian lei  / adm.status[current]
     */
    RON,

    /**
     * Serbian dinar Serbian dinars  / adm.status[current]
     */
    RSD,

    /**
     * Russian rouble Russian roubles  / adm.status[current]
     */
    RUB,

    /**
     * Rwandese franc Rwandese francs  / adm.status[current]
     */
    RWF,

    /**
     * Saudi riyal Saudi riyals  / adm.status[current]
     */
    SAR,

    /**
     * Solomon Islands dollar Solomon Islands dollars  / adm.status[current]
     */
    SBD,

    /**
     * Seychelles rupee Seychelles rupees  / adm.status[current]
     */
    SCR,

    /**
     * Sudanese pound Sudanese pounds  / adm.status[current]
     */
    SDG,

    /**
     * Swedish krona Swedish kronor  / adm.status[current]
     */
    SEK,

    /**
     * Singapore dollar Singapore dollars  / adm.status[current]
     */
    SGD,

    /**
     * Saint Helena pound Saint Helena pounds  / adm.status[current]
     */
    SHP,

    /**
     * Leone Leones  / adm.status[current]
     */
    SLL,

    /**
     * Somali shilling Somali shillings  / adm.status[current]
     */
    SOS,

    /**
     * Surinamese dollar Surinamese dollars  / adm.status[current]
     */
    SRD,

    /**
     * South Sudanese pound South Sudanese pounds  / adm.status[current]
     */
    SSP,

    /**
     * Dobra Dobras  / adm.status[current]
     */
    STN,

    /**
     * Salvadorian colón Salvadorian colones  / adm.status[current]
     */
    SVC,

    /**
     * Syrian pound Syrian pounds  / adm.status[current]
     */
    SYP,

    /**
     * Lilangeni Emalangeni  / adm.status[current]
     */
    SZL,

    /**
     * Baht  / adm.status[current]
     */
    THB,

    /**
     * Somoni Somonis  / adm.status[current]
     */
    TJS,

    /**
     * Turkmen manat  / adm.status[current]
     */
    TMT,

    /**
     * Tunisian dinar Tunisian dinars  / adm.status[current]
     */
    TND,

    /**
     * Pa’anga  / adm.status[current]
     */
    TOP,

    /**
     * Turkish lira  / adm.status[current]
     */
    TRY,

    /**
     * Trinidad and Tobago dollar Trinidad and Tobago dollars  / adm.status[current]
     */
    TTD,

    /**
     * New Taiwan dollar New Taiwan dollars  / adm.status[current]
     */
    TWD,

    /**
     * Tanzanian shilling Tanzanian shillings  / adm.status[current]
     */
    TZS,

    /**
     * Hryvnia Hryvnen  / adm.status[current]
     */
    UAH,

    /**
     * Uganda shilling Uganda shillings  / adm.status[current]
     */
    UGX,

    /**
     * US dollar US dollars  / adm.status[current]
     */
    USD,

    /**
     * US dollar US dollars  / adm.status[current]
     */
    USN,

    /**
     * Uruguayan peso  / adm.status[current]
     */
    UYU,

    /**
     * Sum  / adm.status[current]
     */
    UZS,

    /**
     * Bolívar soberano Bolívares soberanos  / adm.status[current]
     */
    VES,

    /**
     * Dong  / adm.status[current]
     */
    VND,

    /**
     * Vatu  / adm.status[current]
     */
    VUV,

    /**
     * Tala  / adm.status[current]
     */
    WST,

    /**
     * CFA franc (BEAC)  / adm.status[current]
     */
    XAF,

    /**
     * East Caribbean dollar East Caribbean dollars  / adm.status[current]
     */
    XCD,

    /**
     * CFA Franc (BCEAO)  / adm.status[current]
     */
    XOF,

    /**
     * CFP franc CFP francs  / adm.status[current]
     */
    XPF,

    /**
     * Yemeni rial Yemeni rials  / adm.status[current]
     */
    YER,

    /**
     * Rand  / adm.status[current]
     */
    ZAR,

    /**
     * Zambian kwacha Zambian kwacha  / adm.status[current]
     */
    ZMW;

    public static TCurrencyTedschema fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
