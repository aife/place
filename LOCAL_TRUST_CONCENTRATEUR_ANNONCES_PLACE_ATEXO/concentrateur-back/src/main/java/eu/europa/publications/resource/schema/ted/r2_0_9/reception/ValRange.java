//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import eu.europa.publications.resource.authority.currency.TCurrencyTedschema;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.math.BigDecimal;


/**
 * <p>Classe Java pour val_range complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="val_range"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LOW" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}cost"/&gt;
 *         &lt;element name="HIGH" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}cost"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="CURRENCY" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}currencies" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "val_range", propOrder = {
        "low",
        "high"
})
@XmlSeeAlso({
        eu.europa.publications.resource.schema.ted.r2_0_9.reception.ObjectContractF06.VALRANGETOTAL.class,
        eu.europa.publications.resource.schema.ted.r2_0_9.reception.ObjectContractF15.VALRANGETOTAL.class,
        eu.europa.publications.resource.schema.ted.r2_0_9.reception.ObjectContractF22.VALRANGETOTAL.class
})
@ToString
@EqualsAndHashCode
public class ValRange {

    @XmlElement(name = "LOW", required = true)
    protected BigDecimal low;
    @XmlElement(name = "HIGH", required = true)
    protected BigDecimal high;
    @XmlAttribute(name = "CURRENCY", required = true)
    protected TCurrencyTedschema currency;

    /**
     * Obtient la valeur de la propriété low.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getLOW() {
        return low;
    }

    /**
     * Définit la valeur de la propriété low.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setLOW(BigDecimal value) {
        this.low = value;
    }

    /**
     * Obtient la valeur de la propriété high.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getHIGH() {
        return high;
    }

    /**
     * Définit la valeur de la propriété high.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setHIGH(BigDecimal value) {
        this.high = value;
    }

    /**
     * Obtient la valeur de la propriété currency.
     *
     * @return possible object is
     * {@link TCurrencyTedschema }
     */
    public TCurrencyTedschema getCURRENCY() {
        return currency;
    }

    /**
     * Définit la valeur de la propriété currency.
     *
     * @param value allowed object is
     *              {@link TCurrencyTedschema }
     */
    public void setCURRENCY(TCurrencyTedschema value) {
        this.currency = value;
    }

}
