//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.03 à 10:34:48 AM CET
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java pour anonymous complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LEGAL_BASIS" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}legal_basis_f05"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LEGAL_BASIS_OTHER" minOccurs="0"/&gt;
 *         &lt;element name="CONTRACTING_BODY" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}body_f05"/&gt;
 *         &lt;element name="OBJECT_CONTRACT" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}object_contract_f05"/&gt;
 *         &lt;element name="LEFTI" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}lefti_f05" minOccurs="0"/&gt;
 *         &lt;element name="PROCEDURE" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}procedure_f05"/&gt;
 *         &lt;element name="COMPLEMENTARY_INFO" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}ci_f05"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="LG" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}t_ce_language_list" /&gt;
 *       &lt;attribute name="CATEGORY" use="required" type="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}original_translation" /&gt;
 *       &lt;attribute name="FORM" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="F05" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "legalBasis",
        "legalBasisOther",
        "contractingBody",
        "objectContract",
        "lefti",
        "procedure",
        "complementaryInfo"
})
@XmlRootElement(name = "F05_2014")
@ToString
@EqualsAndHashCode
public class F052014 {

    @XmlElement(name = "LEGAL_BASIS")
    protected LegalBasisF05 legalBasis;
    @XmlElement(name = "LEGAL_BASIS_OTHER")
    protected TextFtMultiLines legalBasisOther;
    @XmlElement(name = "CONTRACTING_BODY", required = true)
    protected BodyF05 contractingBody;
    @XmlElement(name = "OBJECT_CONTRACT", required = true)
    protected ObjectContractF05 objectContract;
    @XmlElement(name = "LEFTI")
    protected LeftiF05 lefti;
    @XmlElement(name = "PROCEDURE", required = true)
    protected ProcedureF05 procedure;
    @XmlElement(name = "COMPLEMENTARY_INFO", required = true)
    protected CiF05 complementaryInfo;
    @XmlAttribute(name = "LG", required = true)
    protected TCeLanguageList lg;
    @XmlAttribute(name = "CATEGORY", required = true)
    protected OriginalTranslation category;
    @XmlAttribute(name = "FORM", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String form;

    /**
     * Obtient la valeur de la propriété legalbasis.
     *
     * @return possible object is
     * {@link LegalBasisF05 }
     */
    public LegalBasisF05 getLEGALBASIS() {
        return legalBasis;
    }

    /**
     * Définit la valeur de la propriété legalbasis.
     *
     * @param value allowed object is
     *              {@link LegalBasisF05 }
     */
    public void setLEGALBASIS(LegalBasisF05 value) {
        this.legalBasis = value;
    }

    /**
     * Obtient la valeur de la propriété legalbasisother.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getLEGALBASISOTHER() {
        return legalBasisOther;
    }

    /**
     * Définit la valeur de la propriété legalbasisother.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setLEGALBASISOTHER(TextFtMultiLines value) {
        this.legalBasisOther = value;
    }

    /**
     * Obtient la valeur de la propriété contractingbody.
     *
     * @return possible object is
     * {@link BodyF05 }
     */
    public BodyF05 getCONTRACTINGBODY() {
        return contractingBody;
    }

    /**
     * Définit la valeur de la propriété contractingbody.
     *
     * @param value allowed object is
     *              {@link BodyF05 }
     */
    public void setCONTRACTINGBODY(BodyF05 value) {
        this.contractingBody = value;
    }

    /**
     * Obtient la valeur de la propriété objectcontract.
     *
     * @return possible object is
     * {@link ObjectContractF05 }
     */
    public ObjectContractF05 getOBJECTCONTRACT() {
        return objectContract;
    }

    /**
     * Définit la valeur de la propriété objectcontract.
     *
     * @param value allowed object is
     *              {@link ObjectContractF05 }
     */
    public void setOBJECTCONTRACT(ObjectContractF05 value) {
        this.objectContract = value;
    }

    /**
     * Obtient la valeur de la propriété lefti.
     *
     * @return possible object is
     * {@link LeftiF05 }
     */
    public LeftiF05 getLefti() {
        return lefti;
    }

    /**
     * Définit la valeur de la propriété lefti.
     *
     * @param value allowed object is
     *              {@link LeftiF05 }
     */
    public void setLefti(LeftiF05 value) {
        this.lefti = value;
    }

    /**
     * Obtient la valeur de la propriété procedure.
     *
     * @return possible object is
     * {@link ProcedureF05 }
     */
    public ProcedureF05 getProcedure() {
        return procedure;
    }

    /**
     * Définit la valeur de la propriété procedure.
     *
     * @param value allowed object is
     *              {@link ProcedureF05 }
     */
    public void setProcedure(ProcedureF05 value) {
        this.procedure = value;
    }

    /**
     * Obtient la valeur de la propriété complementaryinfo.
     *
     * @return possible object is
     * {@link CiF05 }
     */
    public CiF05 getCOMPLEMENTARYINFO() {
        return complementaryInfo;
    }

    /**
     * Définit la valeur de la propriété complementaryinfo.
     *
     * @param value allowed object is
     *              {@link CiF05 }
     */
    public void setCOMPLEMENTARYINFO(CiF05 value) {
        this.complementaryInfo = value;
    }

    /**
     * Obtient la valeur de la propriété lg.
     *
     * @return possible object is
     * {@link TCeLanguageList }
     */
    public TCeLanguageList getLg() {
        return lg;
    }

    /**
     * Définit la valeur de la propriété lg.
     *
     * @param value allowed object is
     *              {@link TCeLanguageList }
     */
    public void setLg(TCeLanguageList value) {
        this.lg = value;
    }

    /**
     * Obtient la valeur de la propriété category.
     *
     * @return possible object is
     * {@link OriginalTranslation }
     */
    public OriginalTranslation getCategory() {
        return category;
    }

    /**
     * Définit la valeur de la propriété category.
     *
     * @param value allowed object is
     *              {@link OriginalTranslation }
     */
    public void setCategory(OriginalTranslation value) {
        this.category = value;
    }

    /**
     * Obtient la valeur de la propriété form.
     *
     * @return possible object is
     * {@link String }
     */
    public String getForm() {
        if (form == null) {
            return "F05";
        } else {
            return form;
        }
    }

    /**
     * Définit la valeur de la propriété form.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setForm(String value) {
        this.form = value;
    }

}
