//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.02.02 à 06:36:32 PM CET 
//


package eu.europa.publications.resource.schema.ted.r2_0_9.reception;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Section IV: PROCEDURE
 *
 * <p>Classe Java pour procedure_f07 complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="procedure_f07"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;group ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}eauction" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}NOTICE_NUMBER_OJ" minOccurs="0"/&gt;
 *         &lt;element ref="{http://publications.europa.eu/resource/schema/ted/R2.0.9/reception}LANGUAGES"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "procedure_f07", propOrder = {
        "eauctionused",
        "infoaddeauction",
        "noticenumberoj",
        "languages"
})
@ToString
@EqualsAndHashCode
public class ProcedureF07 {

    @XmlElement(name = "EAUCTION_USED")
    protected Empty eauctionused;
    @XmlElement(name = "INFO_ADD_EAUCTION")
    protected TextFtMultiLines infoaddeauction;
    @XmlElement(name = "NOTICE_NUMBER_OJ")
    protected String noticenumberoj;
    @XmlElement(name = "LANGUAGES", required = true)
    protected LANGUAGES languages;

    /**
     * Obtient la valeur de la propriété eauctionused.
     *
     * @return possible object is
     * {@link Empty }
     */
    public Empty getEAUCTIONUSED() {
        return eauctionused;
    }

    /**
     * Définit la valeur de la propriété eauctionused.
     *
     * @param value allowed object is
     *              {@link Empty }
     */
    public void setEAUCTIONUSED(Empty value) {
        this.eauctionused = value;
    }

    /**
     * Obtient la valeur de la propriété infoaddeauction.
     *
     * @return possible object is
     * {@link TextFtMultiLines }
     */
    public TextFtMultiLines getINFOADDEAUCTION() {
        return infoaddeauction;
    }

    /**
     * Définit la valeur de la propriété infoaddeauction.
     *
     * @param value allowed object is
     *              {@link TextFtMultiLines }
     */
    public void setINFOADDEAUCTION(TextFtMultiLines value) {
        this.infoaddeauction = value;
    }

    /**
     * Obtient la valeur de la propriété noticenumberoj.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNOTICENUMBEROJ() {
        return noticenumberoj;
    }

    /**
     * Définit la valeur de la propriété noticenumberoj.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNOTICENUMBEROJ(String value) {
        this.noticenumberoj = value;
    }

    /**
     * Obtient la valeur de la propriété languages.
     *
     * @return possible object is
     * {@link LANGUAGES }
     */
    public LANGUAGES getLANGUAGES() {
        return languages;
    }

    /**
     * Définit la valeur de la propriété languages.
     *
     * @param value allowed object is
     *              {@link LANGUAGES }
     */
    public void setLANGUAGES(LANGUAGES value) {
        this.languages = value;
    }

}
