//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.ProcurementCategoryCodeType;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * <p>Classe Java pour StrategicProcurementInformationType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="StrategicProcurementInformationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}ProcurementCategoryCode" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}ProcurementDetails" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StrategicProcurementInformationType", propOrder = {
    "procurementCategoryCode",
    "procurementDetails"
})
@ToString
@EqualsAndHashCode
public class StrategicProcurementInformationType {

    @XmlElement(name = "ProcurementCategoryCode", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected ProcurementCategoryCodeType procurementCategoryCode;
    @XmlElement(name = "ProcurementDetails")
    protected List<ProcurementDetailsType> procurementDetails;

    /**
     * Obtient la valeur de la propriété procurementCategoryCode.
     * 
     * @return
     *     possible object is
     *     {@link ProcurementCategoryCodeType }
     *     
     */
    public ProcurementCategoryCodeType getProcurementCategoryCode() {
        return procurementCategoryCode;
    }

    /**
     * Définit la valeur de la propriété procurementCategoryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcurementCategoryCodeType }
     *     
     */
    public void setProcurementCategoryCode(ProcurementCategoryCodeType value) {
        this.procurementCategoryCode = value;
    }

    /**
     * Gets the value of the procurementDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the procurementDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProcurementDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProcurementDetailsType }
     * 
     * 
     */
    public List<ProcurementDetailsType> getProcurementDetails() {
        if (procurementDetails == null) {
            procurementDetails = new ArrayList<ProcurementDetailsType>();
        }
        return this.procurementDetails;
    }

}
