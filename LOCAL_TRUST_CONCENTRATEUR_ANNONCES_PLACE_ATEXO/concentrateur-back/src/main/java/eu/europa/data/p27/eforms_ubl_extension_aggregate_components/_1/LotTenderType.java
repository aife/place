//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.PublicTransportationCumulatedDistanceType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.TenderRankedIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.TenderVariantIndicatorType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.MonetaryTotalType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.RankCodeType;


/**
 * <p>Classe Java pour LotTenderType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="LotTenderType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}FieldsPrivacy" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ID"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}RankCode" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}PublicTransportationCumulatedDistance" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}TenderRankedIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}TenderVariantIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}LegalMonetaryTotal" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}AggregatedAmounts" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}ConcessionRevenue" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}ContractTerm" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}Origin" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}SubcontractingTerm" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}TenderingParty" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}TenderLot" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}TenderReference" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LotTenderType", propOrder = {
    "fieldsPrivacy",
    "id",
    "rankCode",
    "publicTransportationCumulatedDistance",
    "tenderRankedIndicator",
    "tenderVariantIndicator",
    "legalMonetaryTotal",
    "aggregatedAmounts",
    "concessionRevenue",
    "contractTerm",
    "origin",
    "subcontractingTerm",
    "tenderingParty",
    "tenderLot",
    "tenderReference"
})
@ToString
@EqualsAndHashCode
public class LotTenderType {

    @XmlElement(name = "FieldsPrivacy")
    protected List<FieldsPrivacyType> fieldsPrivacy;
    @XmlElement(name = "ID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2", required = true)
    protected IDType id;
    @XmlElement(name = "RankCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected RankCodeType rankCode;
    @XmlElement(name = "PublicTransportationCumulatedDistance", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected PublicTransportationCumulatedDistanceType publicTransportationCumulatedDistance;
    @XmlElement(name = "TenderRankedIndicator", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected TenderRankedIndicatorType tenderRankedIndicator;
    @XmlElement(name = "TenderVariantIndicator", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected TenderVariantIndicatorType tenderVariantIndicator;
    @XmlElement(name = "LegalMonetaryTotal", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2")
    protected MonetaryTotalType legalMonetaryTotal;
    @XmlElement(name = "AggregatedAmounts")
    protected AggregatedAmountsType aggregatedAmounts;
    @XmlElement(name = "ConcessionRevenue")
    protected ConcessionRevenueType concessionRevenue;
    @XmlElement(name = "ContractTerm")
    protected List<ContractTermType> contractTerm;
    @XmlElement(name = "Origin")
    protected List<OriginType> origin;
    @XmlElement(name = "SubcontractingTerm")
    protected List<SubcontractingTermType> subcontractingTerm;
    @XmlElement(name = "TenderingParty")
    protected List<TenderingPartyType> tenderingParty;
    @XmlElement(name = "TenderLot")
    protected List<TenderLotType> tenderLot;
    @XmlElement(name = "TenderReference")
    protected List<TenderReferenceType> tenderReference;

    /**
     * Gets the value of the fieldsPrivacy property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fieldsPrivacy property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFieldsPrivacy().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FieldsPrivacyType }
     * 
     * 
     */
    public List<FieldsPrivacyType> getFieldsPrivacy() {
        if (fieldsPrivacy == null) {
            fieldsPrivacy = new ArrayList<FieldsPrivacyType>();
        }
        return this.fieldsPrivacy;
    }

    /**
     * Obtient la valeur de la propriété id.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getID() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setID(IDType value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propriété rankCode.
     * 
     * @return
     *     possible object is
     *     {@link RankCodeType }
     *     
     */
    public RankCodeType getRankCode() {
        return rankCode;
    }

    /**
     * Définit la valeur de la propriété rankCode.
     * 
     * @param value
     *     allowed object is
     *     {@link RankCodeType }
     *     
     */
    public void setRankCode(RankCodeType value) {
        this.rankCode = value;
    }

    /**
     * Obtient la valeur de la propriété publicTransportationCumulatedDistance.
     * 
     * @return
     *     possible object is
     *     {@link PublicTransportationCumulatedDistanceType }
     *     
     */
    public PublicTransportationCumulatedDistanceType getPublicTransportationCumulatedDistance() {
        return publicTransportationCumulatedDistance;
    }

    /**
     * Définit la valeur de la propriété publicTransportationCumulatedDistance.
     * 
     * @param value
     *     allowed object is
     *     {@link PublicTransportationCumulatedDistanceType }
     *     
     */
    public void setPublicTransportationCumulatedDistance(PublicTransportationCumulatedDistanceType value) {
        this.publicTransportationCumulatedDistance = value;
    }

    /**
     * Obtient la valeur de la propriété tenderRankedIndicator.
     * 
     * @return
     *     possible object is
     *     {@link TenderRankedIndicatorType }
     *     
     */
    public TenderRankedIndicatorType getTenderRankedIndicator() {
        return tenderRankedIndicator;
    }

    /**
     * Définit la valeur de la propriété tenderRankedIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link TenderRankedIndicatorType }
     *     
     */
    public void setTenderRankedIndicator(TenderRankedIndicatorType value) {
        this.tenderRankedIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété tenderVariantIndicator.
     * 
     * @return
     *     possible object is
     *     {@link TenderVariantIndicatorType }
     *     
     */
    public TenderVariantIndicatorType getTenderVariantIndicator() {
        return tenderVariantIndicator;
    }

    /**
     * Définit la valeur de la propriété tenderVariantIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link TenderVariantIndicatorType }
     *     
     */
    public void setTenderVariantIndicator(TenderVariantIndicatorType value) {
        this.tenderVariantIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété legalMonetaryTotal.
     * 
     * @return
     *     possible object is
     *     {@link MonetaryTotalType }
     *     
     */
    public MonetaryTotalType getLegalMonetaryTotal() {
        return legalMonetaryTotal;
    }

    /**
     * Définit la valeur de la propriété legalMonetaryTotal.
     * 
     * @param value
     *     allowed object is
     *     {@link MonetaryTotalType }
     *     
     */
    public void setLegalMonetaryTotal(MonetaryTotalType value) {
        this.legalMonetaryTotal = value;
    }

    /**
     * Obtient la valeur de la propriété aggregatedAmounts.
     * 
     * @return
     *     possible object is
     *     {@link AggregatedAmountsType }
     *     
     */
    public AggregatedAmountsType getAggregatedAmounts() {
        return aggregatedAmounts;
    }

    /**
     * Définit la valeur de la propriété aggregatedAmounts.
     * 
     * @param value
     *     allowed object is
     *     {@link AggregatedAmountsType }
     *     
     */
    public void setAggregatedAmounts(AggregatedAmountsType value) {
        this.aggregatedAmounts = value;
    }

    /**
     * Obtient la valeur de la propriété concessionRevenue.
     * 
     * @return
     *     possible object is
     *     {@link ConcessionRevenueType }
     *     
     */
    public ConcessionRevenueType getConcessionRevenue() {
        return concessionRevenue;
    }

    /**
     * Définit la valeur de la propriété concessionRevenue.
     * 
     * @param value
     *     allowed object is
     *     {@link ConcessionRevenueType }
     *     
     */
    public void setConcessionRevenue(ConcessionRevenueType value) {
        this.concessionRevenue = value;
    }

    /**
     * Gets the value of the contractTerm property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contractTerm property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContractTerm().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContractTermType }
     * 
     * 
     */
    public List<ContractTermType> getContractTerm() {
        if (contractTerm == null) {
            contractTerm = new ArrayList<ContractTermType>();
        }
        return this.contractTerm;
    }

    /**
     * Gets the value of the origin property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the origin property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrigin().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OriginType }
     * 
     * 
     */
    public List<OriginType> getOrigin() {
        if (origin == null) {
            origin = new ArrayList<OriginType>();
        }
        return this.origin;
    }

    /**
     * Gets the value of the subcontractingTerm property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subcontractingTerm property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubcontractingTerm().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubcontractingTermType }
     * 
     * 
     */
    public List<SubcontractingTermType> getSubcontractingTerm() {
        if (subcontractingTerm == null) {
            subcontractingTerm = new ArrayList<SubcontractingTermType>();
        }
        return this.subcontractingTerm;
    }

    /**
     * Gets the value of the tenderingParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tenderingParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTenderingParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TenderingPartyType }
     * 
     * 
     */
    public List<TenderingPartyType> getTenderingParty() {
        if (tenderingParty == null) {
            tenderingParty = new ArrayList<TenderingPartyType>();
        }
        return this.tenderingParty;
    }

    /**
     * Gets the value of the tenderLot property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tenderLot property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTenderLot().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TenderLotType }
     * 
     * 
     */
    public List<TenderLotType> getTenderLot() {
        if (tenderLot == null) {
            tenderLot = new ArrayList<TenderLotType>();
        }
        return this.tenderLot;
    }

    /**
     * Gets the value of the tenderReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tenderReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTenderReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TenderReferenceType }
     * 
     * 
     */
    public List<TenderReferenceType> getTenderReference() {
        if (tenderReference == null) {
            tenderReference = new ArrayList<TenderReferenceType>();
        }
        return this.tenderReference;
    }

}
