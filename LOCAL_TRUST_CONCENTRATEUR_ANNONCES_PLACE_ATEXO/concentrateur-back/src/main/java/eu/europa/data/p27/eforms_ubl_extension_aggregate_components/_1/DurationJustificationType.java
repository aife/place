//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.ExtendedDurationIndicatorType;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * <p>Classe Java pour DurationJustificationType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="DurationJustificationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}ExtendedDurationIndicator"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}AssetsList" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DurationJustificationType", propOrder = {
    "extendedDurationIndicator",
    "assetsList"
})
@ToString
@EqualsAndHashCode
public class DurationJustificationType {

    @XmlElement(name = "ExtendedDurationIndicator", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1", required = true)
    protected ExtendedDurationIndicatorType extendedDurationIndicator;
    @XmlElement(name = "AssetsList")
    protected AssetsListType assetsList;

    /**
     * Obtient la valeur de la propriété extendedDurationIndicator.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedDurationIndicatorType }
     *     
     */
    public ExtendedDurationIndicatorType getExtendedDurationIndicator() {
        return extendedDurationIndicator;
    }

    /**
     * Définit la valeur de la propriété extendedDurationIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedDurationIndicatorType }
     *     
     */
    public void setExtendedDurationIndicator(ExtendedDurationIndicatorType value) {
        this.extendedDurationIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété assetsList.
     * 
     * @return
     *     possible object is
     *     {@link AssetsListType }
     *     
     */
    public AssetsListType getAssetsList() {
        return assetsList;
    }

    /**
     * Définit la valeur de la propriété assetsList.
     * 
     * @param value
     *     allowed object is
     *     {@link AssetsListType }
     *     
     */
    public void setAssetsList(AssetsListType value) {
        this.assetsList = value;
    }

}
