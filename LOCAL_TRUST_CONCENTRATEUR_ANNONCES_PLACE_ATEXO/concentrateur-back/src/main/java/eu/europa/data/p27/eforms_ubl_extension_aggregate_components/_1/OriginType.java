//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.AreaCodeType;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * <p>Classe Java pour OriginType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="OriginType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}FieldsPrivacy" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}AreaCode"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OriginType", propOrder = {
    "fieldsPrivacy",
    "areaCode"
})
@ToString
@EqualsAndHashCode
public class OriginType {

    @XmlElement(name = "FieldsPrivacy")
    protected FieldsPrivacyType fieldsPrivacy;
    @XmlElement(name = "AreaCode", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1", required = true)
    protected AreaCodeType areaCode;

    /**
     * Obtient la valeur de la propriété fieldsPrivacy.
     * 
     * @return
     *     possible object is
     *     {@link FieldsPrivacyType }
     *     
     */
    public FieldsPrivacyType getFieldsPrivacy() {
        return fieldsPrivacy;
    }

    /**
     * Définit la valeur de la propriété fieldsPrivacy.
     * 
     * @param value
     *     allowed object is
     *     {@link FieldsPrivacyType }
     *     
     */
    public void setFieldsPrivacy(FieldsPrivacyType value) {
        this.fieldsPrivacy = value;
    }

    /**
     * Obtient la valeur de la propriété areaCode.
     * 
     * @return
     *     possible object is
     *     {@link AreaCodeType }
     *     
     */
    public AreaCodeType getAreaCode() {
        return areaCode;
    }

    /**
     * Définit la valeur de la propriété areaCode.
     * 
     * @param value
     *     allowed object is
     *     {@link AreaCodeType }
     *     
     */
    public void setAreaCode(AreaCodeType value) {
        this.areaCode = value;
    }

}
