//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.DecisionTypeCodeType;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * <p>Classe Java pour AppealDecisionType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="AppealDecisionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}DecisionTypeCode"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AppealDecisionType", propOrder = {
    "decisionTypeCode"
})
@ToString
@EqualsAndHashCode
public class AppealDecisionType {

    @XmlElement(name = "DecisionTypeCode", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1", required = true)
    protected DecisionTypeCodeType decisionTypeCode;

    /**
     * Obtient la valeur de la propriété decisionTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link DecisionTypeCodeType }
     *     
     */
    public DecisionTypeCodeType getDecisionTypeCode() {
        return decisionTypeCode;
    }

    /**
     * Définit la valeur de la propriété decisionTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link DecisionTypeCodeType }
     *     
     */
    public void setDecisionTypeCode(DecisionTypeCodeType value) {
        this.decisionTypeCode = value;
    }

}
