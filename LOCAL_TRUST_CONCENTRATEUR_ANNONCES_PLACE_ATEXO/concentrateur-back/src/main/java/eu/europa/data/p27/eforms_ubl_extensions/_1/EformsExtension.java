//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extensions._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1.AppealRequestsStatisticsType;
import eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1.AppealsInformationType;
import eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1.AwardCriterionParameterType;
import eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1.BuyingPartyReferenceType;
import eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1.ChangesType;
import eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1.CriterionType;
import eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1.FieldsPrivacyType;
import eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1.FundingType;
import eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1.NonOfficialLanguagesType;
import eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1.NoticeResultType;
import eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1.NoticeSubTypeType;
import eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1.OfficialLanguagesType;
import eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1.OrganizationsType;
import eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1.PublicationType;
import eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1.ReferencedDocumentPartType;
import eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1.StrategicProcurementType;
import eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1.SubsidiaryClassificationType;
import eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1.TenderSubcontractingRequirementsType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.AccessToolNameType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.FrameworkMaximumAmountType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.ProcedureRelaunchIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.TransmissionDateType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.TransmissionTimeType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PeriodType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}AccessToolName" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}FrameworkMaximumAmount" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}ProcedureRelaunchIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}TransmissionDate" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}TransmissionTime" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}AnswerReceptionPeriod" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}AppealRequestsStatistics" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}AppealsInformation" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}AwardCriterionParameter" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}BuyingPartyReference" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}Changes" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}ContractModification" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}FieldsPrivacy" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}Funding" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}InterestExpressionReceptionPeriod" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}NonOfficialLanguages" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}NoticeResult" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}NoticeSubType" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}OfficialLanguages" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}Organizations" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}Publication" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}SelectionCriteria" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}StrategicProcurement" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}SubsidiaryClassification" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}ReferencedDocumentPart" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}TenderSubcontractingRequirements" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "accessToolName",
    "frameworkMaximumAmount",
    "procedureRelaunchIndicator",
    "transmissionDate",
    "transmissionTime",
    "answerReceptionPeriod",
    "appealRequestsStatistics",
    "appealsInformation",
    "awardCriterionParameter",
    "buyingPartyReference",
    "changes",
    "contractModification",
    "fieldsPrivacy",
    "funding",
    "interestExpressionReceptionPeriod",
    "nonOfficialLanguages",
    "noticeResult",
    "noticeSubType",
    "officialLanguages",
    "organizations",
    "publication",
    "selectionCriteria",
    "strategicProcurement",
    "subsidiaryClassification",
    "referencedDocumentPart",
    "tenderSubcontractingRequirements"
})
@XmlRootElement(name = "EformsExtension")
@ToString
@EqualsAndHashCode
public class EformsExtension {

    @XmlElement(name = "AccessToolName", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected List<AccessToolNameType> accessToolName;
    @XmlElement(name = "FrameworkMaximumAmount", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected FrameworkMaximumAmountType frameworkMaximumAmount;
    @XmlElement(name = "ProcedureRelaunchIndicator", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected ProcedureRelaunchIndicatorType procedureRelaunchIndicator;
    @XmlElement(name = "TransmissionDate", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected TransmissionDateType transmissionDate;
    @XmlElement(name = "TransmissionTime", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected TransmissionTimeType transmissionTime;
    @XmlElement(name = "AnswerReceptionPeriod", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1")
    protected PeriodType answerReceptionPeriod;
    @XmlElement(name = "AppealRequestsStatistics", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1")
    protected List<AppealRequestsStatisticsType> appealRequestsStatistics;
    @XmlElement(name = "AppealsInformation", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1")
    protected List<AppealsInformationType> appealsInformation;
    @XmlElement(name = "AwardCriterionParameter", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1")
    protected List<AwardCriterionParameterType> awardCriterionParameter;
    @XmlElement(name = "BuyingPartyReference", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1")
    protected List<BuyingPartyReferenceType> buyingPartyReference;
    @XmlElement(name = "Changes", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1")
    protected ChangesType changes;
    @XmlElement(name = "ContractModification", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1")
    protected List<ChangesType> contractModification;
    @XmlElement(name = "FieldsPrivacy", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1")
    protected List<FieldsPrivacyType> fieldsPrivacy;
    @XmlElement(name = "Funding", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1")
    protected List<FundingType> funding;
    @XmlElement(name = "InterestExpressionReceptionPeriod", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1")
    protected PeriodType interestExpressionReceptionPeriod;
    @XmlElement(name = "NonOfficialLanguages", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1")
    protected NonOfficialLanguagesType nonOfficialLanguages;
    @XmlElement(name = "NoticeResult", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1")
    protected NoticeResultType noticeResult;
    @XmlElement(name = "NoticeSubType", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1")
    protected NoticeSubTypeType noticeSubType;
    @XmlElement(name = "OfficialLanguages", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1")
    protected OfficialLanguagesType officialLanguages;
    @XmlElement(name = "Organizations", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1")
    protected OrganizationsType organizations;
    @XmlElement(name = "Publication", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1")
    protected PublicationType publication;
    @XmlElement(name = "SelectionCriteria", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1")
    protected List<CriterionType> selectionCriteria;
    @XmlElement(name = "StrategicProcurement", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1")
    protected List<StrategicProcurementType> strategicProcurement;
    @XmlElement(name = "SubsidiaryClassification", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1")
    protected List<SubsidiaryClassificationType> subsidiaryClassification;
    @XmlElement(name = "ReferencedDocumentPart", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1")
    protected List<ReferencedDocumentPartType> referencedDocumentPart;
    @XmlElement(name = "TenderSubcontractingRequirements", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1")
    protected List<TenderSubcontractingRequirementsType> tenderSubcontractingRequirements;

    /**
     * Gets the value of the accessToolName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accessToolName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccessToolName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccessToolNameType }
     * 
     * 
     */
    public List<AccessToolNameType> getAccessToolName() {
        if (accessToolName == null) {
            accessToolName = new ArrayList<AccessToolNameType>();
        }
        return this.accessToolName;
    }

    /**
     * Obtient la valeur de la propriété frameworkMaximumAmount.
     * 
     * @return
     *     possible object is
     *     {@link FrameworkMaximumAmountType }
     *     
     */
    public FrameworkMaximumAmountType getFrameworkMaximumAmount() {
        return frameworkMaximumAmount;
    }

    /**
     * Définit la valeur de la propriété frameworkMaximumAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link FrameworkMaximumAmountType }
     *     
     */
    public void setFrameworkMaximumAmount(FrameworkMaximumAmountType value) {
        this.frameworkMaximumAmount = value;
    }

    /**
     * Obtient la valeur de la propriété procedureRelaunchIndicator.
     * 
     * @return
     *     possible object is
     *     {@link ProcedureRelaunchIndicatorType }
     *     
     */
    public ProcedureRelaunchIndicatorType getProcedureRelaunchIndicator() {
        return procedureRelaunchIndicator;
    }

    /**
     * Définit la valeur de la propriété procedureRelaunchIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcedureRelaunchIndicatorType }
     *     
     */
    public void setProcedureRelaunchIndicator(ProcedureRelaunchIndicatorType value) {
        this.procedureRelaunchIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété transmissionDate.
     * 
     * @return
     *     possible object is
     *     {@link TransmissionDateType }
     *     
     */
    public TransmissionDateType getTransmissionDate() {
        return transmissionDate;
    }

    /**
     * Définit la valeur de la propriété transmissionDate.
     * 
     * @param value
     *     allowed object is
     *     {@link TransmissionDateType }
     *     
     */
    public void setTransmissionDate(TransmissionDateType value) {
        this.transmissionDate = value;
    }

    /**
     * Obtient la valeur de la propriété transmissionTime.
     * 
     * @return
     *     possible object is
     *     {@link TransmissionTimeType }
     *     
     */
    public TransmissionTimeType getTransmissionTime() {
        return transmissionTime;
    }

    /**
     * Définit la valeur de la propriété transmissionTime.
     * 
     * @param value
     *     allowed object is
     *     {@link TransmissionTimeType }
     *     
     */
    public void setTransmissionTime(TransmissionTimeType value) {
        this.transmissionTime = value;
    }

    /**
     * Obtient la valeur de la propriété answerReceptionPeriod.
     * 
     * @return
     *     possible object is
     *     {@link PeriodType }
     *     
     */
    public PeriodType getAnswerReceptionPeriod() {
        return answerReceptionPeriod;
    }

    /**
     * Définit la valeur de la propriété answerReceptionPeriod.
     * 
     * @param value
     *     allowed object is
     *     {@link PeriodType }
     *     
     */
    public void setAnswerReceptionPeriod(PeriodType value) {
        this.answerReceptionPeriod = value;
    }

    /**
     * Gets the value of the appealRequestsStatistics property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the appealRequestsStatistics property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppealRequestsStatistics().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AppealRequestsStatisticsType }
     * 
     * 
     */
    public List<AppealRequestsStatisticsType> getAppealRequestsStatistics() {
        if (appealRequestsStatistics == null) {
            appealRequestsStatistics = new ArrayList<AppealRequestsStatisticsType>();
        }
        return this.appealRequestsStatistics;
    }

    /**
     * Gets the value of the appealsInformation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the appealsInformation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppealsInformation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AppealsInformationType }
     * 
     * 
     */
    public List<AppealsInformationType> getAppealsInformation() {
        if (appealsInformation == null) {
            appealsInformation = new ArrayList<AppealsInformationType>();
        }
        return this.appealsInformation;
    }

    /**
     * Gets the value of the awardCriterionParameter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the awardCriterionParameter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAwardCriterionParameter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AwardCriterionParameterType }
     * 
     * 
     */
    public List<AwardCriterionParameterType> getAwardCriterionParameter() {
        if (awardCriterionParameter == null) {
            awardCriterionParameter = new ArrayList<AwardCriterionParameterType>();
        }
        return this.awardCriterionParameter;
    }

    /**
     * Gets the value of the buyingPartyReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the buyingPartyReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBuyingPartyReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BuyingPartyReferenceType }
     * 
     * 
     */
    public List<BuyingPartyReferenceType> getBuyingPartyReference() {
        if (buyingPartyReference == null) {
            buyingPartyReference = new ArrayList<BuyingPartyReferenceType>();
        }
        return this.buyingPartyReference;
    }

    /**
     * Obtient la valeur de la propriété changes.
     * 
     * @return
     *     possible object is
     *     {@link ChangesType }
     *     
     */
    public ChangesType getChanges() {
        return changes;
    }

    /**
     * Définit la valeur de la propriété changes.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangesType }
     *     
     */
    public void setChanges(ChangesType value) {
        this.changes = value;
    }

    /**
     * Gets the value of the contractModification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contractModification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContractModification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChangesType }
     * 
     * 
     */
    public List<ChangesType> getContractModification() {
        if (contractModification == null) {
            contractModification = new ArrayList<ChangesType>();
        }
        return this.contractModification;
    }

    /**
     * Gets the value of the fieldsPrivacy property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fieldsPrivacy property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFieldsPrivacy().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FieldsPrivacyType }
     * 
     * 
     */
    public List<FieldsPrivacyType> getFieldsPrivacy() {
        if (fieldsPrivacy == null) {
            fieldsPrivacy = new ArrayList<FieldsPrivacyType>();
        }
        return this.fieldsPrivacy;
    }

    /**
     * Gets the value of the funding property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the funding property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFunding().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FundingType }
     * 
     * 
     */
    public List<FundingType> getFunding() {
        if (funding == null) {
            funding = new ArrayList<FundingType>();
        }
        return this.funding;
    }

    /**
     * Obtient la valeur de la propriété interestExpressionReceptionPeriod.
     * 
     * @return
     *     possible object is
     *     {@link PeriodType }
     *     
     */
    public PeriodType getInterestExpressionReceptionPeriod() {
        return interestExpressionReceptionPeriod;
    }

    /**
     * Définit la valeur de la propriété interestExpressionReceptionPeriod.
     * 
     * @param value
     *     allowed object is
     *     {@link PeriodType }
     *     
     */
    public void setInterestExpressionReceptionPeriod(PeriodType value) {
        this.interestExpressionReceptionPeriod = value;
    }

    /**
     * Obtient la valeur de la propriété nonOfficialLanguages.
     * 
     * @return
     *     possible object is
     *     {@link NonOfficialLanguagesType }
     *     
     */
    public NonOfficialLanguagesType getNonOfficialLanguages() {
        return nonOfficialLanguages;
    }

    /**
     * Définit la valeur de la propriété nonOfficialLanguages.
     * 
     * @param value
     *     allowed object is
     *     {@link NonOfficialLanguagesType }
     *     
     */
    public void setNonOfficialLanguages(NonOfficialLanguagesType value) {
        this.nonOfficialLanguages = value;
    }

    /**
     * Obtient la valeur de la propriété noticeResult.
     * 
     * @return
     *     possible object is
     *     {@link NoticeResultType }
     *     
     */
    public NoticeResultType getNoticeResult() {
        return noticeResult;
    }

    /**
     * Définit la valeur de la propriété noticeResult.
     * 
     * @param value
     *     allowed object is
     *     {@link NoticeResultType }
     *     
     */
    public void setNoticeResult(NoticeResultType value) {
        this.noticeResult = value;
    }

    /**
     * Obtient la valeur de la propriété noticeSubType.
     * 
     * @return
     *     possible object is
     *     {@link NoticeSubTypeType }
     *     
     */
    public NoticeSubTypeType getNoticeSubType() {
        return noticeSubType;
    }

    /**
     * Définit la valeur de la propriété noticeSubType.
     * 
     * @param value
     *     allowed object is
     *     {@link NoticeSubTypeType }
     *     
     */
    public void setNoticeSubType(NoticeSubTypeType value) {
        this.noticeSubType = value;
    }

    /**
     * Obtient la valeur de la propriété officialLanguages.
     * 
     * @return
     *     possible object is
     *     {@link OfficialLanguagesType }
     *     
     */
    public OfficialLanguagesType getOfficialLanguages() {
        return officialLanguages;
    }

    /**
     * Définit la valeur de la propriété officialLanguages.
     * 
     * @param value
     *     allowed object is
     *     {@link OfficialLanguagesType }
     *     
     */
    public void setOfficialLanguages(OfficialLanguagesType value) {
        this.officialLanguages = value;
    }

    /**
     * Obtient la valeur de la propriété organizations.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationsType }
     *     
     */
    public OrganizationsType getOrganizations() {
        return organizations;
    }

    /**
     * Définit la valeur de la propriété organizations.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationsType }
     *     
     */
    public void setOrganizations(OrganizationsType value) {
        this.organizations = value;
    }

    /**
     * Obtient la valeur de la propriété publication.
     * 
     * @return
     *     possible object is
     *     {@link PublicationType }
     *     
     */
    public PublicationType getPublication() {
        return publication;
    }

    /**
     * Définit la valeur de la propriété publication.
     * 
     * @param value
     *     allowed object is
     *     {@link PublicationType }
     *     
     */
    public void setPublication(PublicationType value) {
        this.publication = value;
    }

    /**
     * Gets the value of the selectionCriteria property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectionCriteria property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectionCriteria().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CriterionType }
     * 
     * 
     */
    public List<CriterionType> getSelectionCriteria() {
        if (selectionCriteria == null) {
            selectionCriteria = new ArrayList<CriterionType>();
        }
        return this.selectionCriteria;
    }

    /**
     * Gets the value of the strategicProcurement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the strategicProcurement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStrategicProcurement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StrategicProcurementType }
     * 
     * 
     */
    public List<StrategicProcurementType> getStrategicProcurement() {
        if (strategicProcurement == null) {
            strategicProcurement = new ArrayList<StrategicProcurementType>();
        }
        return this.strategicProcurement;
    }

    /**
     * Gets the value of the subsidiaryClassification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subsidiaryClassification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubsidiaryClassification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubsidiaryClassificationType }
     * 
     * 
     */
    public List<SubsidiaryClassificationType> getSubsidiaryClassification() {
        if (subsidiaryClassification == null) {
            subsidiaryClassification = new ArrayList<SubsidiaryClassificationType>();
        }
        return this.subsidiaryClassification;
    }

    /**
     * Gets the value of the referencedDocumentPart property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the referencedDocumentPart property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReferencedDocumentPart().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReferencedDocumentPartType }
     * 
     * 
     */
    public List<ReferencedDocumentPartType> getReferencedDocumentPart() {
        if (referencedDocumentPart == null) {
            referencedDocumentPart = new ArrayList<ReferencedDocumentPartType>();
        }
        return this.referencedDocumentPart;
    }

    /**
     * Gets the value of the tenderSubcontractingRequirements property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tenderSubcontractingRequirements property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTenderSubcontractingRequirements().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TenderSubcontractingRequirementsType }
     * 
     * 
     */
    public List<TenderSubcontractingRequirementsType> getTenderSubcontractingRequirements() {
        if (tenderSubcontractingRequirements == null) {
            tenderSubcontractingRequirements = new ArrayList<TenderSubcontractingRequirementsType>();
        }
        return this.tenderSubcontractingRequirements;
    }

}
