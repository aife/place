//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.OverallApproximateFrameworkContractsAmountType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.OverallMaximumFrameworkContractsAmountType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TotalAmountType;


/**
 * <p>Classe Java pour NoticeResultType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="NoticeResultType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}FieldsPrivacy" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}TotalAmount" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}OverallApproximateFrameworkContractsAmount" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}OverallMaximumFrameworkContractsAmount" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}GroupFramework" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}LotResult" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}LotTender" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}SettledContract" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}TenderingParty" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NoticeResultType", propOrder = {
    "fieldsPrivacy",
    "totalAmount",
    "overallApproximateFrameworkContractsAmount",
    "overallMaximumFrameworkContractsAmount",
    "groupFramework",
    "lotResult",
    "lotTender",
    "settledContract",
    "tenderingParty"
})
@ToString
@EqualsAndHashCode
public class NoticeResultType {

    @XmlElement(name = "FieldsPrivacy")
    protected List<FieldsPrivacyType> fieldsPrivacy;
    @XmlElement(name = "TotalAmount", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected TotalAmountType totalAmount;
    @XmlElement(name = "OverallApproximateFrameworkContractsAmount", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected OverallApproximateFrameworkContractsAmountType overallApproximateFrameworkContractsAmount;
    @XmlElement(name = "OverallMaximumFrameworkContractsAmount", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected OverallMaximumFrameworkContractsAmountType overallMaximumFrameworkContractsAmount;
    @XmlElement(name = "GroupFramework")
    protected List<GroupFrameworkType> groupFramework;
    @XmlElement(name = "LotResult")
    protected List<LotResultType> lotResult;
    @XmlElement(name = "LotTender")
    protected List<LotTenderType> lotTender;
    @XmlElement(name = "SettledContract")
    protected List<SettledContractType> settledContract;
    @XmlElement(name = "TenderingParty")
    protected List<TenderingPartyType> tenderingParty;

    /**
     * Gets the value of the fieldsPrivacy property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fieldsPrivacy property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFieldsPrivacy().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FieldsPrivacyType }
     * 
     * 
     */
    public List<FieldsPrivacyType> getFieldsPrivacy() {
        if (fieldsPrivacy == null) {
            fieldsPrivacy = new ArrayList<FieldsPrivacyType>();
        }
        return this.fieldsPrivacy;
    }

    /**
     * Obtient la valeur de la propriété totalAmount.
     * 
     * @return
     *     possible object is
     *     {@link TotalAmountType }
     *     
     */
    public TotalAmountType getTotalAmount() {
        return totalAmount;
    }

    /**
     * Définit la valeur de la propriété totalAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link TotalAmountType }
     *     
     */
    public void setTotalAmount(TotalAmountType value) {
        this.totalAmount = value;
    }

    /**
     * Obtient la valeur de la propriété overallApproximateFrameworkContractsAmount.
     * 
     * @return
     *     possible object is
     *     {@link OverallApproximateFrameworkContractsAmountType }
     *     
     */
    public OverallApproximateFrameworkContractsAmountType getOverallApproximateFrameworkContractsAmount() {
        return overallApproximateFrameworkContractsAmount;
    }

    /**
     * Définit la valeur de la propriété overallApproximateFrameworkContractsAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link OverallApproximateFrameworkContractsAmountType }
     *     
     */
    public void setOverallApproximateFrameworkContractsAmount(OverallApproximateFrameworkContractsAmountType value) {
        this.overallApproximateFrameworkContractsAmount = value;
    }

    /**
     * Obtient la valeur de la propriété overallMaximumFrameworkContractsAmount.
     * 
     * @return
     *     possible object is
     *     {@link OverallMaximumFrameworkContractsAmountType }
     *     
     */
    public OverallMaximumFrameworkContractsAmountType getOverallMaximumFrameworkContractsAmount() {
        return overallMaximumFrameworkContractsAmount;
    }

    /**
     * Définit la valeur de la propriété overallMaximumFrameworkContractsAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link OverallMaximumFrameworkContractsAmountType }
     *     
     */
    public void setOverallMaximumFrameworkContractsAmount(OverallMaximumFrameworkContractsAmountType value) {
        this.overallMaximumFrameworkContractsAmount = value;
    }

    /**
     * Gets the value of the groupFramework property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the groupFramework property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroupFramework().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GroupFrameworkType }
     * 
     * 
     */
    public List<GroupFrameworkType> getGroupFramework() {
        if (groupFramework == null) {
            groupFramework = new ArrayList<GroupFrameworkType>();
        }
        return this.groupFramework;
    }

    /**
     * Gets the value of the lotResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lotResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLotResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LotResultType }
     * 
     * 
     */
    public List<LotResultType> getLotResult() {
        if (lotResult == null) {
            lotResult = new ArrayList<LotResultType>();
        }
        return this.lotResult;
    }

    /**
     * Gets the value of the lotTender property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lotTender property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLotTender().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LotTenderType }
     * 
     * 
     */
    public List<LotTenderType> getLotTender() {
        if (lotTender == null) {
            lotTender = new ArrayList<LotTenderType>();
        }
        return this.lotTender;
    }

    /**
     * Gets the value of the settledContract property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the settledContract property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSettledContract().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SettledContractType }
     * 
     * 
     */
    public List<SettledContractType> getSettledContract() {
        if (settledContract == null) {
            settledContract = new ArrayList<SettledContractType>();
        }
        return this.settledContract;
    }

    /**
     * Gets the value of the tenderingParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tenderingParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTenderingParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TenderingPartyType }
     * 
     * 
     */
    public List<TenderingPartyType> getTenderingParty() {
        if (tenderingParty == null) {
            tenderingParty = new ArrayList<TenderingPartyType>();
        }
        return this.tenderingParty;
    }

}
