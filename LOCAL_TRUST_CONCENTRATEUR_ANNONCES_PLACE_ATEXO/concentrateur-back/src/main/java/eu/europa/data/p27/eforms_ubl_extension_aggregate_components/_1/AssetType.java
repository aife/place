//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.AssetDescriptionType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.AssetPredominanceType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.AssetSignificanceType;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * <p>Classe Java pour AssetType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="AssetType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}AssetDescription" maxOccurs="unbounded"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}AssetSignificance"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}AssetPredominance"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AssetType", propOrder = {
    "assetDescription",
    "assetSignificance",
    "assetPredominance"
})
@ToString
@EqualsAndHashCode
public class AssetType {

    @XmlElement(name = "AssetDescription", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1", required = true)
    protected List<AssetDescriptionType> assetDescription;
    @XmlElement(name = "AssetSignificance", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1", required = true)
    protected AssetSignificanceType assetSignificance;
    @XmlElement(name = "AssetPredominance", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1", required = true)
    protected AssetPredominanceType assetPredominance;

    /**
     * Gets the value of the assetDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the assetDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssetDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssetDescriptionType }
     * 
     * 
     */
    public List<AssetDescriptionType> getAssetDescription() {
        if (assetDescription == null) {
            assetDescription = new ArrayList<AssetDescriptionType>();
        }
        return this.assetDescription;
    }

    /**
     * Obtient la valeur de la propriété assetSignificance.
     * 
     * @return
     *     possible object is
     *     {@link AssetSignificanceType }
     *     
     */
    public AssetSignificanceType getAssetSignificance() {
        return assetSignificance;
    }

    /**
     * Définit la valeur de la propriété assetSignificance.
     * 
     * @param value
     *     allowed object is
     *     {@link AssetSignificanceType }
     *     
     */
    public void setAssetSignificance(AssetSignificanceType value) {
        this.assetSignificance = value;
    }

    /**
     * Obtient la valeur de la propriété assetPredominance.
     * 
     * @return
     *     possible object is
     *     {@link AssetPredominanceType }
     *     
     */
    public AssetPredominanceType getAssetPredominance() {
        return assetPredominance;
    }

    /**
     * Définit la valeur de la propriété assetPredominance.
     * 
     * @param value
     *     allowed object is
     *     {@link AssetPredominanceType }
     *     
     */
    public void setAssetPredominance(AssetPredominanceType value) {
        this.assetPredominance = value;
    }

}
