//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.GroupTypeCodeType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.GroupTypeType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PartyType;


/**
 * <p>Classe Java pour BusinessPartyGroupType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="BusinessPartyGroupType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}GroupTypeCode" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}GroupType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}Party" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusinessPartyGroupType", propOrder = {
    "groupTypeCode",
    "groupType",
    "party"
})
@ToString
@EqualsAndHashCode
public class BusinessPartyGroupType {

    @XmlElement(name = "GroupTypeCode", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected GroupTypeCodeType groupTypeCode;
    @XmlElement(name = "GroupType", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected List<GroupTypeType> groupType;
    @XmlElement(name = "Party", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2", required = true)
    protected List<PartyType> party;

    /**
     * Obtient la valeur de la propriété groupTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link GroupTypeCodeType }
     *     
     */
    public GroupTypeCodeType getGroupTypeCode() {
        return groupTypeCode;
    }

    /**
     * Définit la valeur de la propriété groupTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupTypeCodeType }
     *     
     */
    public void setGroupTypeCode(GroupTypeCodeType value) {
        this.groupTypeCode = value;
    }

    /**
     * Gets the value of the groupType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the groupType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroupType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GroupTypeType }
     * 
     * 
     */
    public List<GroupTypeType> getGroupType() {
        if (groupType == null) {
            groupType = new ArrayList<GroupTypeType>();
        }
        return this.groupType;
    }

    /**
     * Gets the value of the party property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the party property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyType }
     * 
     * 
     */
    public List<PartyType> getParty() {
        if (party == null) {
            party = new ArrayList<PartyType>();
        }
        return this.party;
    }

}
