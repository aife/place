//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.ParameterCodeType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.ParameterNumericType;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * <p>Classe Java pour AwardCriterionParameterType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="AwardCriterionParameterType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}FieldsPrivacy" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}ParameterCode" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}ParameterNumeric"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AwardCriterionParameterType", propOrder = {
    "fieldsPrivacy",
    "parameterCode",
    "parameterNumeric"
})
@ToString
@EqualsAndHashCode
public class AwardCriterionParameterType {

    @XmlElement(name = "FieldsPrivacy")
    protected List<FieldsPrivacyType> fieldsPrivacy;
    @XmlElement(name = "ParameterCode", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected ParameterCodeType parameterCode;
    @XmlElement(name = "ParameterNumeric", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1", required = true)
    protected ParameterNumericType parameterNumeric;

    /**
     * Gets the value of the fieldsPrivacy property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fieldsPrivacy property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFieldsPrivacy().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FieldsPrivacyType }
     * 
     * 
     */
    public List<FieldsPrivacyType> getFieldsPrivacy() {
        if (fieldsPrivacy == null) {
            fieldsPrivacy = new ArrayList<FieldsPrivacyType>();
        }
        return this.fieldsPrivacy;
    }

    /**
     * Obtient la valeur de la propriété parameterCode.
     * 
     * @return
     *     possible object is
     *     {@link ParameterCodeType }
     *     
     */
    public ParameterCodeType getParameterCode() {
        return parameterCode;
    }

    /**
     * Définit la valeur de la propriété parameterCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ParameterCodeType }
     *     
     */
    public void setParameterCode(ParameterCodeType value) {
        this.parameterCode = value;
    }

    /**
     * Obtient la valeur de la propriété parameterNumeric.
     * 
     * @return
     *     possible object is
     *     {@link ParameterNumericType }
     *     
     */
    public ParameterNumericType getParameterNumeric() {
        return parameterNumeric;
    }

    /**
     * Définit la valeur de la propriété parameterNumeric.
     * 
     * @param value
     *     allowed object is
     *     {@link ParameterNumericType }
     *     
     */
    public void setParameterNumeric(ParameterNumericType value) {
        this.parameterNumeric = value;
    }

}
