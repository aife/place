//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.AppealPreviousStageIDType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.AppealStageCodeType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.AppealStageIDType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.RemedyAmountType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.WithdrawnAppealDateType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.WithdrawnAppealIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.WithdrawnAppealReasonsType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DateType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.FeeAmountType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TitleType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.URIType;


/**
 * <p>Classe Java pour AppealStatusType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="AppealStatusType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}Date"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}Title" maxOccurs="unbounded"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}Description" maxOccurs="unbounded"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}URI" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}FeeAmount" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}AppealStageCode"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}AppealStageID"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}AppealPreviousStageID" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}RemedyAmount"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}WithdrawnAppealIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}WithdrawnAppealDate" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}WithdrawnAppealReasons" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}AppealDecision" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}AppealIrregularity" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}AppealProcessingParty"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}AppealRemedy" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}AppealedItem" maxOccurs="unbounded"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}AppealingParty" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AppealStatusType", propOrder = {
    "date",
    "title",
    "description",
    "uri",
    "feeAmount",
    "appealStageCode",
    "appealStageID",
    "appealPreviousStageID",
    "remedyAmount",
    "withdrawnAppealIndicator",
    "withdrawnAppealDate",
    "withdrawnAppealReasons",
    "appealDecision",
    "appealIrregularity",
    "appealProcessingParty",
    "appealRemedy",
    "appealedItem",
    "appealingParty"
})
@ToString
@EqualsAndHashCode
public class AppealStatusType {

    @XmlElement(name = "Date", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2", required = true)
    protected DateType date;
    @XmlElement(name = "Title", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2", required = true)
    protected List<TitleType> title;
    @XmlElement(name = "Description", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2", required = true)
    protected List<DescriptionType> description;
    @XmlElement(name = "URI", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected URIType uri;
    @XmlElement(name = "FeeAmount", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected FeeAmountType feeAmount;
    @XmlElement(name = "AppealStageCode", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1", required = true)
    protected AppealStageCodeType appealStageCode;
    @XmlElement(name = "AppealStageID", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1", required = true)
    protected AppealStageIDType appealStageID;
    @XmlElement(name = "AppealPreviousStageID", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected AppealPreviousStageIDType appealPreviousStageID;
    @XmlElement(name = "RemedyAmount", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1", required = true)
    protected RemedyAmountType remedyAmount;
    @XmlElement(name = "WithdrawnAppealIndicator", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected WithdrawnAppealIndicatorType withdrawnAppealIndicator;
    @XmlElement(name = "WithdrawnAppealDate", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected WithdrawnAppealDateType withdrawnAppealDate;
    @XmlElement(name = "WithdrawnAppealReasons", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected List<WithdrawnAppealReasonsType> withdrawnAppealReasons;
    @XmlElement(name = "AppealDecision")
    protected List<AppealDecisionType> appealDecision;
    @XmlElement(name = "AppealIrregularity")
    protected List<AppealIrregularityType> appealIrregularity;
    @XmlElement(name = "AppealProcessingParty", required = true)
    protected AppealProcessingPartyType appealProcessingParty;
    @XmlElement(name = "AppealRemedy")
    protected List<AppealRemedyType> appealRemedy;
    @XmlElement(name = "AppealedItem", required = true)
    protected List<AppealedItemType> appealedItem;
    @XmlElement(name = "AppealingParty", required = true)
    protected List<AppealingPartyType> appealingParty;

    /**
     * Obtient la valeur de la propriété date.
     * 
     * @return
     *     possible object is
     *     {@link DateType }
     *     
     */
    public DateType getDate() {
        return date;
    }

    /**
     * Définit la valeur de la propriété date.
     * 
     * @param value
     *     allowed object is
     *     {@link DateType }
     *     
     */
    public void setDate(DateType value) {
        this.date = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the title property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTitle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TitleType }
     * 
     * 
     */
    public List<TitleType> getTitle() {
        if (title == null) {
            title = new ArrayList<TitleType>();
        }
        return this.title;
    }

    /**
     * Gets the value of the description property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the description property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DescriptionType }
     * 
     * 
     */
    public List<DescriptionType> getDescription() {
        if (description == null) {
            description = new ArrayList<DescriptionType>();
        }
        return this.description;
    }

    /**
     * Obtient la valeur de la propriété uri.
     * 
     * @return
     *     possible object is
     *     {@link URIType }
     *     
     */
    public URIType getURI() {
        return uri;
    }

    /**
     * Définit la valeur de la propriété uri.
     * 
     * @param value
     *     allowed object is
     *     {@link URIType }
     *     
     */
    public void setURI(URIType value) {
        this.uri = value;
    }

    /**
     * Obtient la valeur de la propriété feeAmount.
     * 
     * @return
     *     possible object is
     *     {@link FeeAmountType }
     *     
     */
    public FeeAmountType getFeeAmount() {
        return feeAmount;
    }

    /**
     * Définit la valeur de la propriété feeAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link FeeAmountType }
     *     
     */
    public void setFeeAmount(FeeAmountType value) {
        this.feeAmount = value;
    }

    /**
     * Obtient la valeur de la propriété appealStageCode.
     * 
     * @return
     *     possible object is
     *     {@link AppealStageCodeType }
     *     
     */
    public AppealStageCodeType getAppealStageCode() {
        return appealStageCode;
    }

    /**
     * Définit la valeur de la propriété appealStageCode.
     * 
     * @param value
     *     allowed object is
     *     {@link AppealStageCodeType }
     *     
     */
    public void setAppealStageCode(AppealStageCodeType value) {
        this.appealStageCode = value;
    }

    /**
     * Obtient la valeur de la propriété appealStageID.
     * 
     * @return
     *     possible object is
     *     {@link AppealStageIDType }
     *     
     */
    public AppealStageIDType getAppealStageID() {
        return appealStageID;
    }

    /**
     * Définit la valeur de la propriété appealStageID.
     * 
     * @param value
     *     allowed object is
     *     {@link AppealStageIDType }
     *     
     */
    public void setAppealStageID(AppealStageIDType value) {
        this.appealStageID = value;
    }

    /**
     * Obtient la valeur de la propriété appealPreviousStageID.
     * 
     * @return
     *     possible object is
     *     {@link AppealPreviousStageIDType }
     *     
     */
    public AppealPreviousStageIDType getAppealPreviousStageID() {
        return appealPreviousStageID;
    }

    /**
     * Définit la valeur de la propriété appealPreviousStageID.
     * 
     * @param value
     *     allowed object is
     *     {@link AppealPreviousStageIDType }
     *     
     */
    public void setAppealPreviousStageID(AppealPreviousStageIDType value) {
        this.appealPreviousStageID = value;
    }

    /**
     * Obtient la valeur de la propriété remedyAmount.
     * 
     * @return
     *     possible object is
     *     {@link RemedyAmountType }
     *     
     */
    public RemedyAmountType getRemedyAmount() {
        return remedyAmount;
    }

    /**
     * Définit la valeur de la propriété remedyAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link RemedyAmountType }
     *     
     */
    public void setRemedyAmount(RemedyAmountType value) {
        this.remedyAmount = value;
    }

    /**
     * Obtient la valeur de la propriété withdrawnAppealIndicator.
     * 
     * @return
     *     possible object is
     *     {@link WithdrawnAppealIndicatorType }
     *     
     */
    public WithdrawnAppealIndicatorType getWithdrawnAppealIndicator() {
        return withdrawnAppealIndicator;
    }

    /**
     * Définit la valeur de la propriété withdrawnAppealIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link WithdrawnAppealIndicatorType }
     *     
     */
    public void setWithdrawnAppealIndicator(WithdrawnAppealIndicatorType value) {
        this.withdrawnAppealIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété withdrawnAppealDate.
     * 
     * @return
     *     possible object is
     *     {@link WithdrawnAppealDateType }
     *     
     */
    public WithdrawnAppealDateType getWithdrawnAppealDate() {
        return withdrawnAppealDate;
    }

    /**
     * Définit la valeur de la propriété withdrawnAppealDate.
     * 
     * @param value
     *     allowed object is
     *     {@link WithdrawnAppealDateType }
     *     
     */
    public void setWithdrawnAppealDate(WithdrawnAppealDateType value) {
        this.withdrawnAppealDate = value;
    }

    /**
     * Gets the value of the withdrawnAppealReasons property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the withdrawnAppealReasons property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWithdrawnAppealReasons().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WithdrawnAppealReasonsType }
     * 
     * 
     */
    public List<WithdrawnAppealReasonsType> getWithdrawnAppealReasons() {
        if (withdrawnAppealReasons == null) {
            withdrawnAppealReasons = new ArrayList<WithdrawnAppealReasonsType>();
        }
        return this.withdrawnAppealReasons;
    }

    /**
     * Gets the value of the appealDecision property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the appealDecision property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppealDecision().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AppealDecisionType }
     * 
     * 
     */
    public List<AppealDecisionType> getAppealDecision() {
        if (appealDecision == null) {
            appealDecision = new ArrayList<AppealDecisionType>();
        }
        return this.appealDecision;
    }

    /**
     * Gets the value of the appealIrregularity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the appealIrregularity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppealIrregularity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AppealIrregularityType }
     * 
     * 
     */
    public List<AppealIrregularityType> getAppealIrregularity() {
        if (appealIrregularity == null) {
            appealIrregularity = new ArrayList<AppealIrregularityType>();
        }
        return this.appealIrregularity;
    }

    /**
     * Obtient la valeur de la propriété appealProcessingParty.
     * 
     * @return
     *     possible object is
     *     {@link AppealProcessingPartyType }
     *     
     */
    public AppealProcessingPartyType getAppealProcessingParty() {
        return appealProcessingParty;
    }

    /**
     * Définit la valeur de la propriété appealProcessingParty.
     * 
     * @param value
     *     allowed object is
     *     {@link AppealProcessingPartyType }
     *     
     */
    public void setAppealProcessingParty(AppealProcessingPartyType value) {
        this.appealProcessingParty = value;
    }

    /**
     * Gets the value of the appealRemedy property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the appealRemedy property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppealRemedy().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AppealRemedyType }
     * 
     * 
     */
    public List<AppealRemedyType> getAppealRemedy() {
        if (appealRemedy == null) {
            appealRemedy = new ArrayList<AppealRemedyType>();
        }
        return this.appealRemedy;
    }

    /**
     * Gets the value of the appealedItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the appealedItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppealedItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AppealedItemType }
     * 
     * 
     */
    public List<AppealedItemType> getAppealedItem() {
        if (appealedItem == null) {
            appealedItem = new ArrayList<AppealedItemType>();
        }
        return this.appealedItem;
    }

    /**
     * Gets the value of the appealingParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the appealingParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppealingParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AppealingPartyType }
     * 
     * 
     */
    public List<AppealingPartyType> getAppealingParty() {
        if (appealingParty == null) {
            appealingParty = new ArrayList<AppealingPartyType>();
        }
        return this.appealingParty;
    }

}
