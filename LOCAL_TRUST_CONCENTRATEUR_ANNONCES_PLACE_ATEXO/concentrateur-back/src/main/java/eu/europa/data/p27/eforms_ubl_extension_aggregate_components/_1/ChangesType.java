//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.ChangedNoticeIdentifierType;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * <p>Classe Java pour ChangesType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ChangesType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}ChangedNoticeIdentifier" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}Change" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}ChangeReason" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangesType", propOrder = {
    "changedNoticeIdentifier",
    "change",
    "changeReason"
})
@ToString
@EqualsAndHashCode
public class ChangesType {

    @XmlElement(name = "ChangedNoticeIdentifier", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected ChangedNoticeIdentifierType changedNoticeIdentifier;
    @XmlElement(name = "Change")
    protected List<ChangeType> change;
    @XmlElement(name = "ChangeReason")
    protected ReasonType changeReason;

    /**
     * Obtient la valeur de la propriété changedNoticeIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link ChangedNoticeIdentifierType }
     *     
     */
    public ChangedNoticeIdentifierType getChangedNoticeIdentifier() {
        return changedNoticeIdentifier;
    }

    /**
     * Définit la valeur de la propriété changedNoticeIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangedNoticeIdentifierType }
     *     
     */
    public void setChangedNoticeIdentifier(ChangedNoticeIdentifierType value) {
        this.changedNoticeIdentifier = value;
    }

    /**
     * Gets the value of the change property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the change property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChange().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChangeType }
     * 
     * 
     */
    public List<ChangeType> getChange() {
        if (change == null) {
            change = new ArrayList<ChangeType>();
        }
        return this.change;
    }

    /**
     * Obtient la valeur de la propriété changeReason.
     * 
     * @return
     *     possible object is
     *     {@link ReasonType }
     *     
     */
    public ReasonType getChangeReason() {
        return changeReason;
    }

    /**
     * Définit la valeur de la propriété changeReason.
     * 
     * @param value
     *     allowed object is
     *     {@link ReasonType }
     *     
     */
    public void setChangeReason(ReasonType value) {
        this.changeReason = value;
    }

}
