//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.AppealingPartyTypeCodeType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.AppealingPartyTypeDescriptionType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PartyType;


/**
 * <p>Classe Java pour AppealingPartyType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="AppealingPartyType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}AppealingPartyTypeCode"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}AppealingPartyTypeDescription" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}Party"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AppealingPartyType", propOrder = {
    "appealingPartyTypeCode",
    "appealingPartyTypeDescription",
    "party"
})
@ToString
@EqualsAndHashCode
public class AppealingPartyType {

    @XmlElement(name = "AppealingPartyTypeCode", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1", required = true)
    protected AppealingPartyTypeCodeType appealingPartyTypeCode;
    @XmlElement(name = "AppealingPartyTypeDescription", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected List<AppealingPartyTypeDescriptionType> appealingPartyTypeDescription;
    @XmlElement(name = "Party", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2", required = true)
    protected PartyType party;

    /**
     * Obtient la valeur de la propriété appealingPartyTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link AppealingPartyTypeCodeType }
     *     
     */
    public AppealingPartyTypeCodeType getAppealingPartyTypeCode() {
        return appealingPartyTypeCode;
    }

    /**
     * Définit la valeur de la propriété appealingPartyTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link AppealingPartyTypeCodeType }
     *     
     */
    public void setAppealingPartyTypeCode(AppealingPartyTypeCodeType value) {
        this.appealingPartyTypeCode = value;
    }

    /**
     * Gets the value of the appealingPartyTypeDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the appealingPartyTypeDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppealingPartyTypeDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AppealingPartyTypeDescriptionType }
     * 
     * 
     */
    public List<AppealingPartyTypeDescriptionType> getAppealingPartyTypeDescription() {
        if (appealingPartyTypeDescription == null) {
            appealingPartyTypeDescription = new ArrayList<AppealingPartyTypeDescriptionType>();
        }
        return this.appealingPartyTypeDescription;
    }

    /**
     * Obtient la valeur de la propriété party.
     * 
     * @return
     *     possible object is
     *     {@link PartyType }
     *     
     */
    public PartyType getParty() {
        return party;
    }

    /**
     * Définit la valeur de la propriété party.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyType }
     *     
     */
    public void setParty(PartyType value) {
        this.party = value;
    }

}
