//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.AcquiringCPBIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.AwardingCPBIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.GroupLeadIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.ListedOnRegulatedMarketIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.NaturalPersonIndicatorType;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * <p>Classe Java pour OrganizationType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="OrganizationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}GroupLeadIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}AcquiringCPBIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}AwardingCPBIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}ListedOnRegulatedMarketIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}NaturalPersonIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}UltimateBeneficialOwner" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}Company" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}TouchPoint" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrganizationType", propOrder = {
    "groupLeadIndicator",
    "acquiringCPBIndicator",
    "awardingCPBIndicator",
    "listedOnRegulatedMarketIndicator",
    "naturalPersonIndicator",
    "ultimateBeneficialOwner",
    "company",
    "touchPoint"
})
@ToString
@EqualsAndHashCode
public class OrganizationType {

    @XmlElement(name = "GroupLeadIndicator", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected GroupLeadIndicatorType groupLeadIndicator;
    @XmlElement(name = "AcquiringCPBIndicator", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected AcquiringCPBIndicatorType acquiringCPBIndicator;
    @XmlElement(name = "AwardingCPBIndicator", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected AwardingCPBIndicatorType awardingCPBIndicator;
    @XmlElement(name = "ListedOnRegulatedMarketIndicator", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected ListedOnRegulatedMarketIndicatorType listedOnRegulatedMarketIndicator;
    @XmlElement(name = "NaturalPersonIndicator", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected NaturalPersonIndicatorType naturalPersonIndicator;
    @XmlElement(name = "UltimateBeneficialOwner")
    protected List<UltimateBeneficialOwnerType> ultimateBeneficialOwner;
    @XmlElement(name = "Company")
    protected CompanyType company;
    @XmlElement(name = "TouchPoint")
    protected List<TouchPointType> touchPoint;

    /**
     * Obtient la valeur de la propriété groupLeadIndicator.
     * 
     * @return
     *     possible object is
     *     {@link GroupLeadIndicatorType }
     *     
     */
    public GroupLeadIndicatorType getGroupLeadIndicator() {
        return groupLeadIndicator;
    }

    /**
     * Définit la valeur de la propriété groupLeadIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupLeadIndicatorType }
     *     
     */
    public void setGroupLeadIndicator(GroupLeadIndicatorType value) {
        this.groupLeadIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété acquiringCPBIndicator.
     * 
     * @return
     *     possible object is
     *     {@link AcquiringCPBIndicatorType }
     *     
     */
    public AcquiringCPBIndicatorType getAcquiringCPBIndicator() {
        return acquiringCPBIndicator;
    }

    /**
     * Définit la valeur de la propriété acquiringCPBIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link AcquiringCPBIndicatorType }
     *     
     */
    public void setAcquiringCPBIndicator(AcquiringCPBIndicatorType value) {
        this.acquiringCPBIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété awardingCPBIndicator.
     * 
     * @return
     *     possible object is
     *     {@link AwardingCPBIndicatorType }
     *     
     */
    public AwardingCPBIndicatorType getAwardingCPBIndicator() {
        return awardingCPBIndicator;
    }

    /**
     * Définit la valeur de la propriété awardingCPBIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link AwardingCPBIndicatorType }
     *     
     */
    public void setAwardingCPBIndicator(AwardingCPBIndicatorType value) {
        this.awardingCPBIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété listedOnRegulatedMarketIndicator.
     * 
     * @return
     *     possible object is
     *     {@link ListedOnRegulatedMarketIndicatorType }
     *     
     */
    public ListedOnRegulatedMarketIndicatorType getListedOnRegulatedMarketIndicator() {
        return listedOnRegulatedMarketIndicator;
    }

    /**
     * Définit la valeur de la propriété listedOnRegulatedMarketIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link ListedOnRegulatedMarketIndicatorType }
     *     
     */
    public void setListedOnRegulatedMarketIndicator(ListedOnRegulatedMarketIndicatorType value) {
        this.listedOnRegulatedMarketIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété naturalPersonIndicator.
     * 
     * @return
     *     possible object is
     *     {@link NaturalPersonIndicatorType }
     *     
     */
    public NaturalPersonIndicatorType getNaturalPersonIndicator() {
        return naturalPersonIndicator;
    }

    /**
     * Définit la valeur de la propriété naturalPersonIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link NaturalPersonIndicatorType }
     *     
     */
    public void setNaturalPersonIndicator(NaturalPersonIndicatorType value) {
        this.naturalPersonIndicator = value;
    }

    /**
     * Gets the value of the ultimateBeneficialOwner property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ultimateBeneficialOwner property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUltimateBeneficialOwner().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UltimateBeneficialOwnerType }
     * 
     * 
     */
    public List<UltimateBeneficialOwnerType> getUltimateBeneficialOwner() {
        if (ultimateBeneficialOwner == null) {
            ultimateBeneficialOwner = new ArrayList<UltimateBeneficialOwnerType>();
        }
        return this.ultimateBeneficialOwner;
    }

    /**
     * Obtient la valeur de la propriété company.
     * 
     * @return
     *     possible object is
     *     {@link CompanyType }
     *     
     */
    public CompanyType getCompany() {
        return company;
    }

    /**
     * Définit la valeur de la propriété company.
     * 
     * @param value
     *     allowed object is
     *     {@link CompanyType }
     *     
     */
    public void setCompany(CompanyType value) {
        this.company = value;
    }

    /**
     * Gets the value of the touchPoint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the touchPoint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTouchPoint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TouchPointType }
     * 
     * 
     */
    public List<TouchPointType> getTouchPoint() {
        if (touchPoint == null) {
            touchPoint = new ArrayList<TouchPointType>();
        }
        return this.touchPoint;
    }

}
