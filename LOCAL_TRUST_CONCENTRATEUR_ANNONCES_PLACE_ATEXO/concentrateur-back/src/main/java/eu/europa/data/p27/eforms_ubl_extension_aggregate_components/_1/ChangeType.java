//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.ChangeDescriptionType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.ProcurementDocumentsChangeDateType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.ProcurementDocumentsChangeIndicatorType;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * <p>Classe Java pour ChangeType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ChangeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}ChangeDescription" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}ProcurementDocumentsChangeDate" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}ProcurementDocumentsChangeIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}ChangedSection" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeType", propOrder = {
    "changeDescription",
    "procurementDocumentsChangeDate",
    "procurementDocumentsChangeIndicator",
    "changedSection"
})
@ToString
@EqualsAndHashCode
public class ChangeType {

    @XmlElement(name = "ChangeDescription", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected List<ChangeDescriptionType> changeDescription;
    @XmlElement(name = "ProcurementDocumentsChangeDate", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected ProcurementDocumentsChangeDateType procurementDocumentsChangeDate;
    @XmlElement(name = "ProcurementDocumentsChangeIndicator", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected ProcurementDocumentsChangeIndicatorType procurementDocumentsChangeIndicator;
    @XmlElement(name = "ChangedSection")
    protected List<ChangedSectionType> changedSection;

    /**
     * Gets the value of the changeDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the changeDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChangeDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChangeDescriptionType }
     * 
     * 
     */
    public List<ChangeDescriptionType> getChangeDescription() {
        if (changeDescription == null) {
            changeDescription = new ArrayList<ChangeDescriptionType>();
        }
        return this.changeDescription;
    }

    /**
     * Obtient la valeur de la propriété procurementDocumentsChangeDate.
     * 
     * @return
     *     possible object is
     *     {@link ProcurementDocumentsChangeDateType }
     *     
     */
    public ProcurementDocumentsChangeDateType getProcurementDocumentsChangeDate() {
        return procurementDocumentsChangeDate;
    }

    /**
     * Définit la valeur de la propriété procurementDocumentsChangeDate.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcurementDocumentsChangeDateType }
     *     
     */
    public void setProcurementDocumentsChangeDate(ProcurementDocumentsChangeDateType value) {
        this.procurementDocumentsChangeDate = value;
    }

    /**
     * Obtient la valeur de la propriété procurementDocumentsChangeIndicator.
     * 
     * @return
     *     possible object is
     *     {@link ProcurementDocumentsChangeIndicatorType }
     *     
     */
    public ProcurementDocumentsChangeIndicatorType getProcurementDocumentsChangeIndicator() {
        return procurementDocumentsChangeIndicator;
    }

    /**
     * Définit la valeur de la propriété procurementDocumentsChangeIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcurementDocumentsChangeIndicatorType }
     *     
     */
    public void setProcurementDocumentsChangeIndicator(ProcurementDocumentsChangeIndicatorType value) {
        this.procurementDocumentsChangeIndicator = value;
    }

    /**
     * Gets the value of the changedSection property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the changedSection property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChangedSection().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChangedSectionType }
     * 
     * 
     */
    public List<ChangedSectionType> getChangedSection() {
        if (changedSection == null) {
            changedSection = new ArrayList<ChangedSectionType>();
        }
        return this.changedSection;
    }

}
