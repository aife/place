//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.TermCodeType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.TermDescriptionType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.TermIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.TermPercentType;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * <p>Classe Java pour ContractTermType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ContractTermType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}TermCode"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}TermDescription" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}TermIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}TermPercent" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractTermType", propOrder = {
    "termCode",
    "termDescription",
    "termIndicator",
    "termPercent"
})
@ToString
@EqualsAndHashCode
public class ContractTermType {

    @XmlElement(name = "TermCode", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1", required = true)
    protected TermCodeType termCode;
    @XmlElement(name = "TermDescription", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected List<TermDescriptionType> termDescription;
    @XmlElement(name = "TermIndicator", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected TermIndicatorType termIndicator;
    @XmlElement(name = "TermPercent", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected TermPercentType termPercent;

    /**
     * Obtient la valeur de la propriété termCode.
     * 
     * @return
     *     possible object is
     *     {@link TermCodeType }
     *     
     */
    public TermCodeType getTermCode() {
        return termCode;
    }

    /**
     * Définit la valeur de la propriété termCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TermCodeType }
     *     
     */
    public void setTermCode(TermCodeType value) {
        this.termCode = value;
    }

    /**
     * Gets the value of the termDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the termDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTermDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TermDescriptionType }
     * 
     * 
     */
    public List<TermDescriptionType> getTermDescription() {
        if (termDescription == null) {
            termDescription = new ArrayList<TermDescriptionType>();
        }
        return this.termDescription;
    }

    /**
     * Obtient la valeur de la propriété termIndicator.
     * 
     * @return
     *     possible object is
     *     {@link TermIndicatorType }
     *     
     */
    public TermIndicatorType getTermIndicator() {
        return termIndicator;
    }

    /**
     * Définit la valeur de la propriété termIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link TermIndicatorType }
     *     
     */
    public void setTermIndicator(TermIndicatorType value) {
        this.termIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété termPercent.
     * 
     * @return
     *     possible object is
     *     {@link TermPercentType }
     *     
     */
    public TermPercentType getTermPercent() {
        return termPercent;
    }

    /**
     * Définit la valeur de la propriété termPercent.
     * 
     * @param value
     *     allowed object is
     *     {@link TermPercentType }
     *     
     */
    public void setTermPercent(TermPercentType value) {
        this.termPercent = value;
    }

}
