//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.GroupFrameworkMaximumValueAmountType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.GroupFrameworkReestimatedValueAmountType;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * <p>Classe Java pour GroupFrameworkType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="GroupFrameworkType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}FieldsPrivacy" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}GroupFrameworkMaximumValueAmount" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}GroupFrameworkReestimatedValueAmount" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}TenderLot"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupFrameworkType", propOrder = {
    "fieldsPrivacy",
    "groupFrameworkMaximumValueAmount",
    "groupFrameworkReestimatedValueAmount",
    "tenderLot"
})
@ToString
@EqualsAndHashCode
public class GroupFrameworkType {

    @XmlElement(name = "FieldsPrivacy")
    protected List<FieldsPrivacyType> fieldsPrivacy;
    @XmlElement(name = "GroupFrameworkMaximumValueAmount", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected GroupFrameworkMaximumValueAmountType groupFrameworkMaximumValueAmount;
    @XmlElement(name = "GroupFrameworkReestimatedValueAmount", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected GroupFrameworkReestimatedValueAmountType groupFrameworkReestimatedValueAmount;
    @XmlElement(name = "TenderLot", required = true)
    protected TenderLotType tenderLot;

    /**
     * Gets the value of the fieldsPrivacy property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fieldsPrivacy property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFieldsPrivacy().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FieldsPrivacyType }
     * 
     * 
     */
    public List<FieldsPrivacyType> getFieldsPrivacy() {
        if (fieldsPrivacy == null) {
            fieldsPrivacy = new ArrayList<FieldsPrivacyType>();
        }
        return this.fieldsPrivacy;
    }

    /**
     * Obtient la valeur de la propriété groupFrameworkMaximumValueAmount.
     * 
     * @return
     *     possible object is
     *     {@link GroupFrameworkMaximumValueAmountType }
     *     
     */
    public GroupFrameworkMaximumValueAmountType getGroupFrameworkMaximumValueAmount() {
        return groupFrameworkMaximumValueAmount;
    }

    /**
     * Définit la valeur de la propriété groupFrameworkMaximumValueAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupFrameworkMaximumValueAmountType }
     *     
     */
    public void setGroupFrameworkMaximumValueAmount(GroupFrameworkMaximumValueAmountType value) {
        this.groupFrameworkMaximumValueAmount = value;
    }

    /**
     * Obtient la valeur de la propriété groupFrameworkReestimatedValueAmount.
     * 
     * @return
     *     possible object is
     *     {@link GroupFrameworkReestimatedValueAmountType }
     *     
     */
    public GroupFrameworkReestimatedValueAmountType getGroupFrameworkReestimatedValueAmount() {
        return groupFrameworkReestimatedValueAmount;
    }

    /**
     * Définit la valeur de la propriété groupFrameworkReestimatedValueAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupFrameworkReestimatedValueAmountType }
     *     
     */
    public void setGroupFrameworkReestimatedValueAmount(GroupFrameworkReestimatedValueAmountType value) {
        this.groupFrameworkReestimatedValueAmount = value;
    }

    /**
     * Obtient la valeur de la propriété tenderLot.
     * 
     * @return
     *     possible object is
     *     {@link TenderLotType }
     *     
     */
    public TenderLotType getTenderLot() {
        return tenderLot;
    }

    /**
     * Définit la valeur de la propriété tenderLot.
     * 
     * @param value
     *     allowed object is
     *     {@link TenderLotType }
     *     
     */
    public void setTenderLot(TenderLotType value) {
        this.tenderLot = value;
    }

}
