//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//

@javax.xml.bind.annotation.XmlSchema(namespace = "http://data.europa.eu/p27/eforms-ubl-extensions/1", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package eu.europa.data.p27.eforms_ubl_extensions._1;
