//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.GazetteIDType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.NoticePublicationIDType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.PublicationDateType;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * <p>Classe Java pour PublicationType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="PublicationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}NoticePublicationID"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}GazetteID"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}PublicationDate"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PublicationType", propOrder = {
    "noticePublicationID",
    "gazetteID",
    "publicationDate"
})
@ToString
@EqualsAndHashCode
public class PublicationType {

    @XmlElement(name = "NoticePublicationID", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1", required = true)
    protected NoticePublicationIDType noticePublicationID;
    @XmlElement(name = "GazetteID", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1", required = true)
    protected GazetteIDType gazetteID;
    @XmlElement(name = "PublicationDate", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1", required = true)
    protected PublicationDateType publicationDate;

    /**
     * Obtient la valeur de la propriété noticePublicationID.
     * 
     * @return
     *     possible object is
     *     {@link NoticePublicationIDType }
     *     
     */
    public NoticePublicationIDType getNoticePublicationID() {
        return noticePublicationID;
    }

    /**
     * Définit la valeur de la propriété noticePublicationID.
     * 
     * @param value
     *     allowed object is
     *     {@link NoticePublicationIDType }
     *     
     */
    public void setNoticePublicationID(NoticePublicationIDType value) {
        this.noticePublicationID = value;
    }

    /**
     * Obtient la valeur de la propriété gazetteID.
     * 
     * @return
     *     possible object is
     *     {@link GazetteIDType }
     *     
     */
    public GazetteIDType getGazetteID() {
        return gazetteID;
    }

    /**
     * Définit la valeur de la propriété gazetteID.
     * 
     * @param value
     *     allowed object is
     *     {@link GazetteIDType }
     *     
     */
    public void setGazetteID(GazetteIDType value) {
        this.gazetteID = value;
    }

    /**
     * Obtient la valeur de la propriété publicationDate.
     * 
     * @return
     *     possible object is
     *     {@link PublicationDateType }
     *     
     */
    public PublicationDateType getPublicationDate() {
        return publicationDate;
    }

    /**
     * Définit la valeur de la propriété publicationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link PublicationDateType }
     *     
     */
    public void setPublicationDate(PublicationDateType value) {
        this.publicationDate = value;
    }

}
