//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.AssetCategoryCodeType;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * <p>Classe Java pour ProcurementDetailsType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ProcurementDetailsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}AssetCategoryCode" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}StrategicProcurementStatistics" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcurementDetailsType", propOrder = {
    "assetCategoryCode",
    "strategicProcurementStatistics"
})
@ToString
@EqualsAndHashCode
public class ProcurementDetailsType {

    @XmlElement(name = "AssetCategoryCode", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected AssetCategoryCodeType assetCategoryCode;
    @XmlElement(name = "StrategicProcurementStatistics")
    protected List<StatisticsType> strategicProcurementStatistics;

    /**
     * Obtient la valeur de la propriété assetCategoryCode.
     * 
     * @return
     *     possible object is
     *     {@link AssetCategoryCodeType }
     *     
     */
    public AssetCategoryCodeType getAssetCategoryCode() {
        return assetCategoryCode;
    }

    /**
     * Définit la valeur de la propriété assetCategoryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link AssetCategoryCodeType }
     *     
     */
    public void setAssetCategoryCode(AssetCategoryCodeType value) {
        this.assetCategoryCode = value;
    }

    /**
     * Gets the value of the strategicProcurementStatistics property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the strategicProcurementStatistics property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStrategicProcurementStatistics().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatisticsType }
     * 
     * 
     */
    public List<StatisticsType> getStrategicProcurementStatistics() {
        if (strategicProcurementStatistics == null) {
            strategicProcurementStatistics = new ArrayList<StatisticsType>();
        }
        return this.strategicProcurementStatistics;
    }

}
