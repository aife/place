//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.FieldIdentifierCodeType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.PublicationDateType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.ReasonDescriptionType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ReasonCodeType;


/**
 * <p>Classe Java pour FieldsPrivacyType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="FieldsPrivacyType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}FieldIdentifierCode" maxOccurs="unbounded"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ReasonCode"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}ReasonDescription" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}PublicationDate" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FieldsPrivacyType", propOrder = {
    "fieldIdentifierCode",
    "reasonCode",
    "reasonDescription",
    "publicationDate"
})
@ToString
@EqualsAndHashCode
public class FieldsPrivacyType {

    @XmlElement(name = "FieldIdentifierCode", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1", required = true)
    protected List<FieldIdentifierCodeType> fieldIdentifierCode;
    @XmlElement(name = "ReasonCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2", required = true)
    protected ReasonCodeType reasonCode;
    @XmlElement(name = "ReasonDescription", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected List<ReasonDescriptionType> reasonDescription;
    @XmlElement(name = "PublicationDate", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected PublicationDateType publicationDate;

    /**
     * Gets the value of the fieldIdentifierCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fieldIdentifierCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFieldIdentifierCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FieldIdentifierCodeType }
     * 
     * 
     */
    public List<FieldIdentifierCodeType> getFieldIdentifierCode() {
        if (fieldIdentifierCode == null) {
            fieldIdentifierCode = new ArrayList<FieldIdentifierCodeType>();
        }
        return this.fieldIdentifierCode;
    }

    /**
     * Obtient la valeur de la propriété reasonCode.
     * 
     * @return
     *     possible object is
     *     {@link ReasonCodeType }
     *     
     */
    public ReasonCodeType getReasonCode() {
        return reasonCode;
    }

    /**
     * Définit la valeur de la propriété reasonCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ReasonCodeType }
     *     
     */
    public void setReasonCode(ReasonCodeType value) {
        this.reasonCode = value;
    }

    /**
     * Gets the value of the reasonDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reasonDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReasonDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReasonDescriptionType }
     * 
     * 
     */
    public List<ReasonDescriptionType> getReasonDescription() {
        if (reasonDescription == null) {
            reasonDescription = new ArrayList<ReasonDescriptionType>();
        }
        return this.reasonDescription;
    }

    /**
     * Obtient la valeur de la propriété publicationDate.
     * 
     * @return
     *     possible object is
     *     {@link PublicationDateType }
     *     
     */
    public PublicationDateType getPublicationDate() {
        return publicationDate;
    }

    /**
     * Définit la valeur de la propriété publicationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link PublicationDateType }
     *     
     */
    public void setPublicationDate(PublicationDateType value) {
        this.publicationDate = value;
    }

}
