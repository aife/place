//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.DPSTerminationIndicatorType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PartyType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.HigherTenderAmountType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.LowerTenderAmountType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TenderResultCodeType;


/**
 * <p>Classe Java pour LotResultType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="LotResultType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}FieldsPrivacy" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}HigherTenderAmount" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}LowerTenderAmount" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}TenderResultCode" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}DPSTerminationIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}FinancingParty" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}PayerParty" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}AppealRequestsStatistics" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}DecisionReason" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}LotTender" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}FrameworkAgreementValues" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}ReceivedSubmissionsStatistics" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}SettledContract" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}StrategicProcurement" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}TenderLot"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LotResultType", propOrder = {
    "fieldsPrivacy",
    "id",
    "higherTenderAmount",
    "lowerTenderAmount",
    "tenderResultCode",
    "dpsTerminationIndicator",
    "financingParty",
    "payerParty",
    "appealRequestsStatistics",
    "decisionReason",
    "lotTender",
    "frameworkAgreementValues",
    "receivedSubmissionsStatistics",
    "settledContract",
    "strategicProcurement",
    "tenderLot"
})
@ToString
@EqualsAndHashCode
public class LotResultType {

    @XmlElement(name = "FieldsPrivacy")
    protected List<FieldsPrivacyType> fieldsPrivacy;
    @XmlElement(name = "ID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected IDType id;
    @XmlElement(name = "HigherTenderAmount", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected HigherTenderAmountType higherTenderAmount;
    @XmlElement(name = "LowerTenderAmount", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected LowerTenderAmountType lowerTenderAmount;
    @XmlElement(name = "TenderResultCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected TenderResultCodeType tenderResultCode;
    @XmlElement(name = "DPSTerminationIndicator", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected DPSTerminationIndicatorType dpsTerminationIndicator;
    @XmlElement(name = "FinancingParty", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2")
    protected List<PartyType> financingParty;
    @XmlElement(name = "PayerParty", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2")
    protected List<PartyType> payerParty;
    @XmlElement(name = "AppealRequestsStatistics")
    protected List<AppealRequestsStatisticsType> appealRequestsStatistics;
    @XmlElement(name = "DecisionReason")
    protected DecisionReasonType decisionReason;
    @XmlElement(name = "LotTender")
    protected List<LotTenderType> lotTender;
    @XmlElement(name = "FrameworkAgreementValues")
    protected FrameworkAgreementValuesType frameworkAgreementValues;
    @XmlElement(name = "ReceivedSubmissionsStatistics")
    protected List<ReceivedSubmissionsStatisticsType> receivedSubmissionsStatistics;
    @XmlElement(name = "SettledContract")
    protected List<SettledContractType> settledContract;
    @XmlElement(name = "StrategicProcurement")
    protected List<StrategicProcurementType> strategicProcurement;
    @XmlElement(name = "TenderLot", required = true)
    protected TenderLotType tenderLot;

    /**
     * Gets the value of the fieldsPrivacy property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fieldsPrivacy property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFieldsPrivacy().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FieldsPrivacyType }
     * 
     * 
     */
    public List<FieldsPrivacyType> getFieldsPrivacy() {
        if (fieldsPrivacy == null) {
            fieldsPrivacy = new ArrayList<FieldsPrivacyType>();
        }
        return this.fieldsPrivacy;
    }

    /**
     * Obtient la valeur de la propriété id.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getID() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setID(IDType value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propriété higherTenderAmount.
     * 
     * @return
     *     possible object is
     *     {@link HigherTenderAmountType }
     *     
     */
    public HigherTenderAmountType getHigherTenderAmount() {
        return higherTenderAmount;
    }

    /**
     * Définit la valeur de la propriété higherTenderAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link HigherTenderAmountType }
     *     
     */
    public void setHigherTenderAmount(HigherTenderAmountType value) {
        this.higherTenderAmount = value;
    }

    /**
     * Obtient la valeur de la propriété lowerTenderAmount.
     * 
     * @return
     *     possible object is
     *     {@link LowerTenderAmountType }
     *     
     */
    public LowerTenderAmountType getLowerTenderAmount() {
        return lowerTenderAmount;
    }

    /**
     * Définit la valeur de la propriété lowerTenderAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link LowerTenderAmountType }
     *     
     */
    public void setLowerTenderAmount(LowerTenderAmountType value) {
        this.lowerTenderAmount = value;
    }

    /**
     * Obtient la valeur de la propriété tenderResultCode.
     * 
     * @return
     *     possible object is
     *     {@link TenderResultCodeType }
     *     
     */
    public TenderResultCodeType getTenderResultCode() {
        return tenderResultCode;
    }

    /**
     * Définit la valeur de la propriété tenderResultCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TenderResultCodeType }
     *     
     */
    public void setTenderResultCode(TenderResultCodeType value) {
        this.tenderResultCode = value;
    }

    /**
     * Obtient la valeur de la propriété dpsTerminationIndicator.
     * 
     * @return
     *     possible object is
     *     {@link DPSTerminationIndicatorType }
     *     
     */
    public DPSTerminationIndicatorType getDPSTerminationIndicator() {
        return dpsTerminationIndicator;
    }

    /**
     * Définit la valeur de la propriété dpsTerminationIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link DPSTerminationIndicatorType }
     *     
     */
    public void setDPSTerminationIndicator(DPSTerminationIndicatorType value) {
        this.dpsTerminationIndicator = value;
    }

    /**
     * Gets the value of the financingParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the financingParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFinancingParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyType }
     * 
     * 
     */
    public List<PartyType> getFinancingParty() {
        if (financingParty == null) {
            financingParty = new ArrayList<PartyType>();
        }
        return this.financingParty;
    }

    /**
     * Gets the value of the payerParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the payerParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPayerParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyType }
     * 
     * 
     */
    public List<PartyType> getPayerParty() {
        if (payerParty == null) {
            payerParty = new ArrayList<PartyType>();
        }
        return this.payerParty;
    }

    /**
     * Gets the value of the appealRequestsStatistics property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the appealRequestsStatistics property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppealRequestsStatistics().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AppealRequestsStatisticsType }
     * 
     * 
     */
    public List<AppealRequestsStatisticsType> getAppealRequestsStatistics() {
        if (appealRequestsStatistics == null) {
            appealRequestsStatistics = new ArrayList<AppealRequestsStatisticsType>();
        }
        return this.appealRequestsStatistics;
    }

    /**
     * Obtient la valeur de la propriété decisionReason.
     * 
     * @return
     *     possible object is
     *     {@link DecisionReasonType }
     *     
     */
    public DecisionReasonType getDecisionReason() {
        return decisionReason;
    }

    /**
     * Définit la valeur de la propriété decisionReason.
     * 
     * @param value
     *     allowed object is
     *     {@link DecisionReasonType }
     *     
     */
    public void setDecisionReason(DecisionReasonType value) {
        this.decisionReason = value;
    }

    /**
     * Gets the value of the lotTender property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lotTender property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLotTender().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LotTenderType }
     * 
     * 
     */
    public List<LotTenderType> getLotTender() {
        if (lotTender == null) {
            lotTender = new ArrayList<LotTenderType>();
        }
        return this.lotTender;
    }

    /**
     * Obtient la valeur de la propriété frameworkAgreementValues.
     * 
     * @return
     *     possible object is
     *     {@link FrameworkAgreementValuesType }
     *     
     */
    public FrameworkAgreementValuesType getFrameworkAgreementValues() {
        return frameworkAgreementValues;
    }

    /**
     * Définit la valeur de la propriété frameworkAgreementValues.
     * 
     * @param value
     *     allowed object is
     *     {@link FrameworkAgreementValuesType }
     *     
     */
    public void setFrameworkAgreementValues(FrameworkAgreementValuesType value) {
        this.frameworkAgreementValues = value;
    }

    /**
     * Gets the value of the receivedSubmissionsStatistics property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the receivedSubmissionsStatistics property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReceivedSubmissionsStatistics().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReceivedSubmissionsStatisticsType }
     * 
     * 
     */
    public List<ReceivedSubmissionsStatisticsType> getReceivedSubmissionsStatistics() {
        if (receivedSubmissionsStatistics == null) {
            receivedSubmissionsStatistics = new ArrayList<ReceivedSubmissionsStatisticsType>();
        }
        return this.receivedSubmissionsStatistics;
    }

    /**
     * Gets the value of the settledContract property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the settledContract property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSettledContract().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SettledContractType }
     * 
     * 
     */
    public List<SettledContractType> getSettledContract() {
        if (settledContract == null) {
            settledContract = new ArrayList<SettledContractType>();
        }
        return this.settledContract;
    }

    /**
     * Gets the value of the strategicProcurement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the strategicProcurement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStrategicProcurement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StrategicProcurementType }
     * 
     * 
     */
    public List<StrategicProcurementType> getStrategicProcurement() {
        if (strategicProcurement == null) {
            strategicProcurement = new ArrayList<StrategicProcurementType>();
        }
        return this.strategicProcurement;
    }

    /**
     * Obtient la valeur de la propriété tenderLot.
     * 
     * @return
     *     possible object is
     *     {@link TenderLotType }
     *     
     */
    public TenderLotType getTenderLot() {
        return tenderLot;
    }

    /**
     * Définit la valeur de la propriété tenderLot.
     * 
     * @param value
     *     allowed object is
     *     {@link TenderLotType }
     *     
     */
    public void setTenderLot(TenderLotType value) {
        this.tenderLot = value;
    }

}
