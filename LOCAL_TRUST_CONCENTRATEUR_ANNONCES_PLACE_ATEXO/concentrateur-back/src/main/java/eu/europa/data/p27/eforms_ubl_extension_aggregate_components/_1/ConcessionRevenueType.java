//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.RevenueBuyerAmountType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.RevenueUserAmountType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.ValueDescriptionType;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * <p>Classe Java pour ConcessionRevenueType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ConcessionRevenueType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1}FieldsPrivacy" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}RevenueBuyerAmount" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}RevenueUserAmount" minOccurs="0"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}ValueDescription" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConcessionRevenueType", propOrder = {
    "fieldsPrivacy",
    "revenueBuyerAmount",
    "revenueUserAmount",
    "valueDescription"
})
@ToString
@EqualsAndHashCode
public class ConcessionRevenueType {

    @XmlElement(name = "FieldsPrivacy")
    protected List<FieldsPrivacyType> fieldsPrivacy;
    @XmlElement(name = "RevenueBuyerAmount", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected List<RevenueBuyerAmountType> revenueBuyerAmount;
    @XmlElement(name = "RevenueUserAmount", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected RevenueUserAmountType revenueUserAmount;
    @XmlElement(name = "ValueDescription", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1")
    protected List<ValueDescriptionType> valueDescription;

    /**
     * Gets the value of the fieldsPrivacy property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fieldsPrivacy property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFieldsPrivacy().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FieldsPrivacyType }
     * 
     * 
     */
    public List<FieldsPrivacyType> getFieldsPrivacy() {
        if (fieldsPrivacy == null) {
            fieldsPrivacy = new ArrayList<FieldsPrivacyType>();
        }
        return this.fieldsPrivacy;
    }

    /**
     * Gets the value of the revenueBuyerAmount property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the revenueBuyerAmount property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRevenueBuyerAmount().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RevenueBuyerAmountType }
     * 
     * 
     */
    public List<RevenueBuyerAmountType> getRevenueBuyerAmount() {
        if (revenueBuyerAmount == null) {
            revenueBuyerAmount = new ArrayList<RevenueBuyerAmountType>();
        }
        return this.revenueBuyerAmount;
    }

    /**
     * Obtient la valeur de la propriété revenueUserAmount.
     * 
     * @return
     *     possible object is
     *     {@link RevenueUserAmountType }
     *     
     */
    public RevenueUserAmountType getRevenueUserAmount() {
        return revenueUserAmount;
    }

    /**
     * Définit la valeur de la propriété revenueUserAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link RevenueUserAmountType }
     *     
     */
    public void setRevenueUserAmount(RevenueUserAmountType value) {
        this.revenueUserAmount = value;
    }

    /**
     * Gets the value of the valueDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the valueDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValueDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ValueDescriptionType }
     * 
     * 
     */
    public List<ValueDescriptionType> getValueDescription() {
        if (valueDescription == null) {
            valueDescription = new ArrayList<ValueDescriptionType>();
        }
        return this.valueDescription;
    }

}
