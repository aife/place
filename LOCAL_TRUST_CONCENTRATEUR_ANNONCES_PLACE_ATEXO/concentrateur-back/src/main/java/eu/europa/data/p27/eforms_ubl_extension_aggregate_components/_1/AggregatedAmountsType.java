//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package eu.europa.data.p27.eforms_ubl_extension_aggregate_components._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.PaidAmountDescriptionType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.PenaltiesAmountType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PaidAmountType;


/**
 * <p>Classe Java pour AggregatedAmountsType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="AggregatedAmountsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}PaidAmount"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}PaidAmountDescription" maxOccurs="unbounded"/&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1}PenaltiesAmount"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AggregatedAmountsType", propOrder = {
    "paidAmount",
    "paidAmountDescription",
    "penaltiesAmount"
})
@ToString
@EqualsAndHashCode
public class AggregatedAmountsType {

    @XmlElement(name = "PaidAmount", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2", required = true)
    protected PaidAmountType paidAmount;
    @XmlElement(name = "PaidAmountDescription", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1", required = true)
    protected List<PaidAmountDescriptionType> paidAmountDescription;
    @XmlElement(name = "PenaltiesAmount", namespace = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1", required = true)
    protected PenaltiesAmountType penaltiesAmount;

    /**
     * Obtient la valeur de la propriété paidAmount.
     * 
     * @return
     *     possible object is
     *     {@link PaidAmountType }
     *     
     */
    public PaidAmountType getPaidAmount() {
        return paidAmount;
    }

    /**
     * Définit la valeur de la propriété paidAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link PaidAmountType }
     *     
     */
    public void setPaidAmount(PaidAmountType value) {
        this.paidAmount = value;
    }

    /**
     * Gets the value of the paidAmountDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paidAmountDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaidAmountDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaidAmountDescriptionType }
     * 
     * 
     */
    public List<PaidAmountDescriptionType> getPaidAmountDescription() {
        if (paidAmountDescription == null) {
            paidAmountDescription = new ArrayList<PaidAmountDescriptionType>();
        }
        return this.paidAmountDescription;
    }

    /**
     * Obtient la valeur de la propriété penaltiesAmount.
     * 
     * @return
     *     possible object is
     *     {@link PenaltiesAmountType }
     *     
     */
    public PenaltiesAmountType getPenaltiesAmount() {
        return penaltiesAmount;
    }

    /**
     * Définit la valeur de la propriété penaltiesAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link PenaltiesAmountType }
     *     
     */
    public void setPenaltiesAmount(PenaltiesAmountType value) {
        this.penaltiesAmount = value;
    }

}
