//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.priorinformationnotice_2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the oasis.names.specification.ubl.schema.xsd.priorinformationnotice_2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PriorInformationNotice_QNAME = new QName("urn:oasis:names:specification:ubl:schema:xsd:PriorInformationNotice-2", "PriorInformationNotice");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: oasis.names.specification.ubl.schema.xsd.priorinformationnotice_2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PriorInformationNoticeType }
     * 
     */
    public PriorInformationNoticeType createPriorInformationNoticeType() {
        return new PriorInformationNoticeType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PriorInformationNoticeType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PriorInformationNoticeType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:oasis:names:specification:ubl:schema:xsd:PriorInformationNotice-2", name = "PriorInformationNotice")
    public JAXBElement<PriorInformationNoticeType> createPriorInformationNotice(PriorInformationNoticeType value) {
        return new JAXBElement<PriorInformationNoticeType>(_PriorInformationNotice_QNAME, PriorInformationNoticeType.class, null, value);
    }

}
