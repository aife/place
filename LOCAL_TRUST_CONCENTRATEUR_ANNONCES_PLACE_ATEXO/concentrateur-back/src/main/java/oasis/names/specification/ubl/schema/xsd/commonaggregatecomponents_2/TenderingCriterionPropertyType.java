//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.CertificationLevelDescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.CopyQualityTypeCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ExpectedAmountType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ExpectedCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ExpectedDescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ExpectedIDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ExpectedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ExpectedURIType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ExpectedValueNumericType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MaximumAmountType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MaximumQuantityType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MaximumValueNumericType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MinimumAmountType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MinimumQuantityType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MinimumValueNumericType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.NameType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TranslationTypeCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TypeCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ValueCurrencyCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ValueDataTypeCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ValueUnitCodeType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour TenderingCriterionPropertyType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="TenderingCriterionPropertyType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}Name" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}Description" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}TypeCode" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ValueDataTypeCode" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ValueUnitCode" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ValueCurrencyCode" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ExpectedAmount" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ExpectedID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ExpectedIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ExpectedCode" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ExpectedValueNumeric" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ExpectedDescription" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ExpectedURI" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}MaximumAmount" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}MinimumAmount" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}MaximumValueNumeric" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}MinimumValueNumeric" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}MaximumQuantity" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}MinimumQuantity" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}TranslationTypeCode" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}CertificationLevelDescription" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}CopyQualityTypeCode" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}ApplicablePeriod" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}TemplateEvidence" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TenderingCriterionPropertyType", propOrder = {
    "ublExtensions",
    "id",
    "name",
    "description",
    "typeCode",
    "valueDataTypeCode",
    "valueUnitCode",
    "valueCurrencyCode",
    "expectedAmount",
    "expectedID",
    "expectedIndicator",
    "expectedCode",
    "expectedValueNumeric",
    "expectedDescription",
    "expectedURI",
    "maximumAmount",
    "minimumAmount",
    "maximumValueNumeric",
    "minimumValueNumeric",
    "maximumQuantity",
    "minimumQuantity",
    "translationTypeCode",
    "certificationLevelDescription",
    "copyQualityTypeCode",
    "applicablePeriod",
    "templateEvidence"
})
@ToString
@EqualsAndHashCode
public class TenderingCriterionPropertyType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "ID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected IDType id;
    @XmlElement(name = "Name", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected NameType name;
    @XmlElement(name = "Description", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<DescriptionType> description;
    @XmlElement(name = "TypeCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected TypeCodeType typeCode;
    @XmlElement(name = "ValueDataTypeCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ValueDataTypeCodeType valueDataTypeCode;
    @XmlElement(name = "ValueUnitCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ValueUnitCodeType valueUnitCode;
    @XmlElement(name = "ValueCurrencyCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ValueCurrencyCodeType valueCurrencyCode;
    @XmlElement(name = "ExpectedAmount", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ExpectedAmountType expectedAmount;
    @XmlElement(name = "ExpectedID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ExpectedIDType expectedID;
    @XmlElement(name = "ExpectedIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ExpectedIndicatorType expectedIndicator;
    @XmlElement(name = "ExpectedCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ExpectedCodeType expectedCode;
    @XmlElement(name = "ExpectedValueNumeric", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ExpectedValueNumericType expectedValueNumeric;
    @XmlElement(name = "ExpectedDescription", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ExpectedDescriptionType expectedDescription;
    @XmlElement(name = "ExpectedURI", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ExpectedURIType expectedURI;
    @XmlElement(name = "MaximumAmount", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected MaximumAmountType maximumAmount;
    @XmlElement(name = "MinimumAmount", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected MinimumAmountType minimumAmount;
    @XmlElement(name = "MaximumValueNumeric", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected MaximumValueNumericType maximumValueNumeric;
    @XmlElement(name = "MinimumValueNumeric", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected MinimumValueNumericType minimumValueNumeric;
    @XmlElement(name = "MaximumQuantity", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected MaximumQuantityType maximumQuantity;
    @XmlElement(name = "MinimumQuantity", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected MinimumQuantityType minimumQuantity;
    @XmlElement(name = "TranslationTypeCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected TranslationTypeCodeType translationTypeCode;
    @XmlElement(name = "CertificationLevelDescription", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<CertificationLevelDescriptionType> certificationLevelDescription;
    @XmlElement(name = "CopyQualityTypeCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected CopyQualityTypeCodeType copyQualityTypeCode;
    @XmlElement(name = "ApplicablePeriod")
    protected List<PeriodType> applicablePeriod;
    @XmlElement(name = "TemplateEvidence")
    protected List<EvidenceType> templateEvidence;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété id.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getID() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setID(IDType value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propriété name.
     * 
     * @return
     *     possible object is
     *     {@link NameType }
     *     
     */
    public NameType getName() {
        return name;
    }

    /**
     * Définit la valeur de la propriété name.
     * 
     * @param value
     *     allowed object is
     *     {@link NameType }
     *     
     */
    public void setName(NameType value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the description property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DescriptionType }
     * 
     * 
     */
    public List<DescriptionType> getDescription() {
        if (description == null) {
            description = new ArrayList<DescriptionType>();
        }
        return this.description;
    }

    /**
     * Obtient la valeur de la propriété typeCode.
     * 
     * @return
     *     possible object is
     *     {@link TypeCodeType }
     *     
     */
    public TypeCodeType getTypeCode() {
        return typeCode;
    }

    /**
     * Définit la valeur de la propriété typeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeCodeType }
     *     
     */
    public void setTypeCode(TypeCodeType value) {
        this.typeCode = value;
    }

    /**
     * Obtient la valeur de la propriété valueDataTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link ValueDataTypeCodeType }
     *     
     */
    public ValueDataTypeCodeType getValueDataTypeCode() {
        return valueDataTypeCode;
    }

    /**
     * Définit la valeur de la propriété valueDataTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueDataTypeCodeType }
     *     
     */
    public void setValueDataTypeCode(ValueDataTypeCodeType value) {
        this.valueDataTypeCode = value;
    }

    /**
     * Obtient la valeur de la propriété valueUnitCode.
     * 
     * @return
     *     possible object is
     *     {@link ValueUnitCodeType }
     *     
     */
    public ValueUnitCodeType getValueUnitCode() {
        return valueUnitCode;
    }

    /**
     * Définit la valeur de la propriété valueUnitCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueUnitCodeType }
     *     
     */
    public void setValueUnitCode(ValueUnitCodeType value) {
        this.valueUnitCode = value;
    }

    /**
     * Obtient la valeur de la propriété valueCurrencyCode.
     * 
     * @return
     *     possible object is
     *     {@link ValueCurrencyCodeType }
     *     
     */
    public ValueCurrencyCodeType getValueCurrencyCode() {
        return valueCurrencyCode;
    }

    /**
     * Définit la valeur de la propriété valueCurrencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueCurrencyCodeType }
     *     
     */
    public void setValueCurrencyCode(ValueCurrencyCodeType value) {
        this.valueCurrencyCode = value;
    }

    /**
     * Obtient la valeur de la propriété expectedAmount.
     * 
     * @return
     *     possible object is
     *     {@link ExpectedAmountType }
     *     
     */
    public ExpectedAmountType getExpectedAmount() {
        return expectedAmount;
    }

    /**
     * Définit la valeur de la propriété expectedAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpectedAmountType }
     *     
     */
    public void setExpectedAmount(ExpectedAmountType value) {
        this.expectedAmount = value;
    }

    /**
     * Obtient la valeur de la propriété expectedID.
     * 
     * @return
     *     possible object is
     *     {@link ExpectedIDType }
     *     
     */
    public ExpectedIDType getExpectedID() {
        return expectedID;
    }

    /**
     * Définit la valeur de la propriété expectedID.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpectedIDType }
     *     
     */
    public void setExpectedID(ExpectedIDType value) {
        this.expectedID = value;
    }

    /**
     * Obtient la valeur de la propriété expectedIndicator.
     * 
     * @return
     *     possible object is
     *     {@link ExpectedIndicatorType }
     *     
     */
    public ExpectedIndicatorType getExpectedIndicator() {
        return expectedIndicator;
    }

    /**
     * Définit la valeur de la propriété expectedIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpectedIndicatorType }
     *     
     */
    public void setExpectedIndicator(ExpectedIndicatorType value) {
        this.expectedIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété expectedCode.
     * 
     * @return
     *     possible object is
     *     {@link ExpectedCodeType }
     *     
     */
    public ExpectedCodeType getExpectedCode() {
        return expectedCode;
    }

    /**
     * Définit la valeur de la propriété expectedCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpectedCodeType }
     *     
     */
    public void setExpectedCode(ExpectedCodeType value) {
        this.expectedCode = value;
    }

    /**
     * Obtient la valeur de la propriété expectedValueNumeric.
     * 
     * @return
     *     possible object is
     *     {@link ExpectedValueNumericType }
     *     
     */
    public ExpectedValueNumericType getExpectedValueNumeric() {
        return expectedValueNumeric;
    }

    /**
     * Définit la valeur de la propriété expectedValueNumeric.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpectedValueNumericType }
     *     
     */
    public void setExpectedValueNumeric(ExpectedValueNumericType value) {
        this.expectedValueNumeric = value;
    }

    /**
     * Obtient la valeur de la propriété expectedDescription.
     * 
     * @return
     *     possible object is
     *     {@link ExpectedDescriptionType }
     *     
     */
    public ExpectedDescriptionType getExpectedDescription() {
        return expectedDescription;
    }

    /**
     * Définit la valeur de la propriété expectedDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpectedDescriptionType }
     *     
     */
    public void setExpectedDescription(ExpectedDescriptionType value) {
        this.expectedDescription = value;
    }

    /**
     * Obtient la valeur de la propriété expectedURI.
     * 
     * @return
     *     possible object is
     *     {@link ExpectedURIType }
     *     
     */
    public ExpectedURIType getExpectedURI() {
        return expectedURI;
    }

    /**
     * Définit la valeur de la propriété expectedURI.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpectedURIType }
     *     
     */
    public void setExpectedURI(ExpectedURIType value) {
        this.expectedURI = value;
    }

    /**
     * Obtient la valeur de la propriété maximumAmount.
     * 
     * @return
     *     possible object is
     *     {@link MaximumAmountType }
     *     
     */
    public MaximumAmountType getMaximumAmount() {
        return maximumAmount;
    }

    /**
     * Définit la valeur de la propriété maximumAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link MaximumAmountType }
     *     
     */
    public void setMaximumAmount(MaximumAmountType value) {
        this.maximumAmount = value;
    }

    /**
     * Obtient la valeur de la propriété minimumAmount.
     * 
     * @return
     *     possible object is
     *     {@link MinimumAmountType }
     *     
     */
    public MinimumAmountType getMinimumAmount() {
        return minimumAmount;
    }

    /**
     * Définit la valeur de la propriété minimumAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link MinimumAmountType }
     *     
     */
    public void setMinimumAmount(MinimumAmountType value) {
        this.minimumAmount = value;
    }

    /**
     * Obtient la valeur de la propriété maximumValueNumeric.
     * 
     * @return
     *     possible object is
     *     {@link MaximumValueNumericType }
     *     
     */
    public MaximumValueNumericType getMaximumValueNumeric() {
        return maximumValueNumeric;
    }

    /**
     * Définit la valeur de la propriété maximumValueNumeric.
     * 
     * @param value
     *     allowed object is
     *     {@link MaximumValueNumericType }
     *     
     */
    public void setMaximumValueNumeric(MaximumValueNumericType value) {
        this.maximumValueNumeric = value;
    }

    /**
     * Obtient la valeur de la propriété minimumValueNumeric.
     * 
     * @return
     *     possible object is
     *     {@link MinimumValueNumericType }
     *     
     */
    public MinimumValueNumericType getMinimumValueNumeric() {
        return minimumValueNumeric;
    }

    /**
     * Définit la valeur de la propriété minimumValueNumeric.
     * 
     * @param value
     *     allowed object is
     *     {@link MinimumValueNumericType }
     *     
     */
    public void setMinimumValueNumeric(MinimumValueNumericType value) {
        this.minimumValueNumeric = value;
    }

    /**
     * Obtient la valeur de la propriété maximumQuantity.
     * 
     * @return
     *     possible object is
     *     {@link MaximumQuantityType }
     *     
     */
    public MaximumQuantityType getMaximumQuantity() {
        return maximumQuantity;
    }

    /**
     * Définit la valeur de la propriété maximumQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link MaximumQuantityType }
     *     
     */
    public void setMaximumQuantity(MaximumQuantityType value) {
        this.maximumQuantity = value;
    }

    /**
     * Obtient la valeur de la propriété minimumQuantity.
     * 
     * @return
     *     possible object is
     *     {@link MinimumQuantityType }
     *     
     */
    public MinimumQuantityType getMinimumQuantity() {
        return minimumQuantity;
    }

    /**
     * Définit la valeur de la propriété minimumQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link MinimumQuantityType }
     *     
     */
    public void setMinimumQuantity(MinimumQuantityType value) {
        this.minimumQuantity = value;
    }

    /**
     * Obtient la valeur de la propriété translationTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link TranslationTypeCodeType }
     *     
     */
    public TranslationTypeCodeType getTranslationTypeCode() {
        return translationTypeCode;
    }

    /**
     * Définit la valeur de la propriété translationTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TranslationTypeCodeType }
     *     
     */
    public void setTranslationTypeCode(TranslationTypeCodeType value) {
        this.translationTypeCode = value;
    }

    /**
     * Gets the value of the certificationLevelDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the certificationLevelDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCertificationLevelDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CertificationLevelDescriptionType }
     * 
     * 
     */
    public List<CertificationLevelDescriptionType> getCertificationLevelDescription() {
        if (certificationLevelDescription == null) {
            certificationLevelDescription = new ArrayList<CertificationLevelDescriptionType>();
        }
        return this.certificationLevelDescription;
    }

    /**
     * Obtient la valeur de la propriété copyQualityTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link CopyQualityTypeCodeType }
     *     
     */
    public CopyQualityTypeCodeType getCopyQualityTypeCode() {
        return copyQualityTypeCode;
    }

    /**
     * Définit la valeur de la propriété copyQualityTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link CopyQualityTypeCodeType }
     *     
     */
    public void setCopyQualityTypeCode(CopyQualityTypeCodeType value) {
        this.copyQualityTypeCode = value;
    }

    /**
     * Gets the value of the applicablePeriod property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the applicablePeriod property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getApplicablePeriod().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PeriodType }
     * 
     * 
     */
    public List<PeriodType> getApplicablePeriod() {
        if (applicablePeriod == null) {
            applicablePeriod = new ArrayList<PeriodType>();
        }
        return this.applicablePeriod;
    }

    /**
     * Gets the value of the templateEvidence property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the templateEvidence property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTemplateEvidence().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EvidenceType }
     * 
     * 
     */
    public List<EvidenceType> getTemplateEvidence() {
        if (templateEvidence == null) {
            templateEvidence = new ArrayList<EvidenceType>();
        }
        return this.templateEvidence;
    }

}
