//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IMOGuidelinesOnBoardIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ManagementPlanImplementedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ManagementPlanOnBoardIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.NoControlActionsReasonType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.OtherControlActionsType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TanksExchangedQuantityType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TanksInBallastQuantityType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TanksNotExchangedQuantityType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TotalBallastTanksOnBoardQuantityType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TotalBallastWaterCapacityMeasureType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TotalBallastWaterOnBoardMeasureType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour BallastWaterSummaryType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="BallastWaterSummaryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ManagementPlanOnBoardIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ManagementPlanImplementedIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}IMOGuidelinesOnBoardIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}TotalBallastTanksOnBoardQuantity" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}TanksInBallastQuantity" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}TanksExchangedQuantity" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}TanksNotExchangedQuantity" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}TotalBallastWaterOnBoardMeasure" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}TotalBallastWaterCapacityMeasure" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}OtherControlActions" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}NoControlActionsReason" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}UptakeBallastWaterTransaction" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}ExchangeBallastWaterTransaction" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}DischargeBallastWaterTransaction" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}ResponsibleOfficerPerson" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BallastWaterSummaryType", propOrder = {
    "ublExtensions",
    "id",
    "managementPlanOnBoardIndicator",
    "managementPlanImplementedIndicator",
    "imoGuidelinesOnBoardIndicator",
    "totalBallastTanksOnBoardQuantity",
    "tanksInBallastQuantity",
    "tanksExchangedQuantity",
    "tanksNotExchangedQuantity",
    "totalBallastWaterOnBoardMeasure",
    "totalBallastWaterCapacityMeasure",
    "otherControlActions",
    "noControlActionsReason",
    "uptakeBallastWaterTransaction",
    "exchangeBallastWaterTransaction",
    "dischargeBallastWaterTransaction",
    "responsibleOfficerPerson"
})
@ToString
@EqualsAndHashCode
public class BallastWaterSummaryType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "ID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected IDType id;
    @XmlElement(name = "ManagementPlanOnBoardIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ManagementPlanOnBoardIndicatorType managementPlanOnBoardIndicator;
    @XmlElement(name = "ManagementPlanImplementedIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ManagementPlanImplementedIndicatorType managementPlanImplementedIndicator;
    @XmlElement(name = "IMOGuidelinesOnBoardIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected IMOGuidelinesOnBoardIndicatorType imoGuidelinesOnBoardIndicator;
    @XmlElement(name = "TotalBallastTanksOnBoardQuantity", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected TotalBallastTanksOnBoardQuantityType totalBallastTanksOnBoardQuantity;
    @XmlElement(name = "TanksInBallastQuantity", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected TanksInBallastQuantityType tanksInBallastQuantity;
    @XmlElement(name = "TanksExchangedQuantity", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected TanksExchangedQuantityType tanksExchangedQuantity;
    @XmlElement(name = "TanksNotExchangedQuantity", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected TanksNotExchangedQuantityType tanksNotExchangedQuantity;
    @XmlElement(name = "TotalBallastWaterOnBoardMeasure", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected TotalBallastWaterOnBoardMeasureType totalBallastWaterOnBoardMeasure;
    @XmlElement(name = "TotalBallastWaterCapacityMeasure", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected TotalBallastWaterCapacityMeasureType totalBallastWaterCapacityMeasure;
    @XmlElement(name = "OtherControlActions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<OtherControlActionsType> otherControlActions;
    @XmlElement(name = "NoControlActionsReason", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<NoControlActionsReasonType> noControlActionsReason;
    @XmlElement(name = "UptakeBallastWaterTransaction")
    protected List<BallastWaterTransactionType> uptakeBallastWaterTransaction;
    @XmlElement(name = "ExchangeBallastWaterTransaction")
    protected List<BallastWaterTransactionType> exchangeBallastWaterTransaction;
    @XmlElement(name = "DischargeBallastWaterTransaction")
    protected List<BallastWaterTransactionType> dischargeBallastWaterTransaction;
    @XmlElement(name = "ResponsibleOfficerPerson")
    protected PersonType responsibleOfficerPerson;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété id.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getID() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setID(IDType value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propriété managementPlanOnBoardIndicator.
     * 
     * @return
     *     possible object is
     *     {@link ManagementPlanOnBoardIndicatorType }
     *     
     */
    public ManagementPlanOnBoardIndicatorType getManagementPlanOnBoardIndicator() {
        return managementPlanOnBoardIndicator;
    }

    /**
     * Définit la valeur de la propriété managementPlanOnBoardIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link ManagementPlanOnBoardIndicatorType }
     *     
     */
    public void setManagementPlanOnBoardIndicator(ManagementPlanOnBoardIndicatorType value) {
        this.managementPlanOnBoardIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété managementPlanImplementedIndicator.
     * 
     * @return
     *     possible object is
     *     {@link ManagementPlanImplementedIndicatorType }
     *     
     */
    public ManagementPlanImplementedIndicatorType getManagementPlanImplementedIndicator() {
        return managementPlanImplementedIndicator;
    }

    /**
     * Définit la valeur de la propriété managementPlanImplementedIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link ManagementPlanImplementedIndicatorType }
     *     
     */
    public void setManagementPlanImplementedIndicator(ManagementPlanImplementedIndicatorType value) {
        this.managementPlanImplementedIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété imoGuidelinesOnBoardIndicator.
     * 
     * @return
     *     possible object is
     *     {@link IMOGuidelinesOnBoardIndicatorType }
     *     
     */
    public IMOGuidelinesOnBoardIndicatorType getIMOGuidelinesOnBoardIndicator() {
        return imoGuidelinesOnBoardIndicator;
    }

    /**
     * Définit la valeur de la propriété imoGuidelinesOnBoardIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link IMOGuidelinesOnBoardIndicatorType }
     *     
     */
    public void setIMOGuidelinesOnBoardIndicator(IMOGuidelinesOnBoardIndicatorType value) {
        this.imoGuidelinesOnBoardIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété totalBallastTanksOnBoardQuantity.
     * 
     * @return
     *     possible object is
     *     {@link TotalBallastTanksOnBoardQuantityType }
     *     
     */
    public TotalBallastTanksOnBoardQuantityType getTotalBallastTanksOnBoardQuantity() {
        return totalBallastTanksOnBoardQuantity;
    }

    /**
     * Définit la valeur de la propriété totalBallastTanksOnBoardQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link TotalBallastTanksOnBoardQuantityType }
     *     
     */
    public void setTotalBallastTanksOnBoardQuantity(TotalBallastTanksOnBoardQuantityType value) {
        this.totalBallastTanksOnBoardQuantity = value;
    }

    /**
     * Obtient la valeur de la propriété tanksInBallastQuantity.
     * 
     * @return
     *     possible object is
     *     {@link TanksInBallastQuantityType }
     *     
     */
    public TanksInBallastQuantityType getTanksInBallastQuantity() {
        return tanksInBallastQuantity;
    }

    /**
     * Définit la valeur de la propriété tanksInBallastQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link TanksInBallastQuantityType }
     *     
     */
    public void setTanksInBallastQuantity(TanksInBallastQuantityType value) {
        this.tanksInBallastQuantity = value;
    }

    /**
     * Obtient la valeur de la propriété tanksExchangedQuantity.
     * 
     * @return
     *     possible object is
     *     {@link TanksExchangedQuantityType }
     *     
     */
    public TanksExchangedQuantityType getTanksExchangedQuantity() {
        return tanksExchangedQuantity;
    }

    /**
     * Définit la valeur de la propriété tanksExchangedQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link TanksExchangedQuantityType }
     *     
     */
    public void setTanksExchangedQuantity(TanksExchangedQuantityType value) {
        this.tanksExchangedQuantity = value;
    }

    /**
     * Obtient la valeur de la propriété tanksNotExchangedQuantity.
     * 
     * @return
     *     possible object is
     *     {@link TanksNotExchangedQuantityType }
     *     
     */
    public TanksNotExchangedQuantityType getTanksNotExchangedQuantity() {
        return tanksNotExchangedQuantity;
    }

    /**
     * Définit la valeur de la propriété tanksNotExchangedQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link TanksNotExchangedQuantityType }
     *     
     */
    public void setTanksNotExchangedQuantity(TanksNotExchangedQuantityType value) {
        this.tanksNotExchangedQuantity = value;
    }

    /**
     * Obtient la valeur de la propriété totalBallastWaterOnBoardMeasure.
     * 
     * @return
     *     possible object is
     *     {@link TotalBallastWaterOnBoardMeasureType }
     *     
     */
    public TotalBallastWaterOnBoardMeasureType getTotalBallastWaterOnBoardMeasure() {
        return totalBallastWaterOnBoardMeasure;
    }

    /**
     * Définit la valeur de la propriété totalBallastWaterOnBoardMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link TotalBallastWaterOnBoardMeasureType }
     *     
     */
    public void setTotalBallastWaterOnBoardMeasure(TotalBallastWaterOnBoardMeasureType value) {
        this.totalBallastWaterOnBoardMeasure = value;
    }

    /**
     * Obtient la valeur de la propriété totalBallastWaterCapacityMeasure.
     * 
     * @return
     *     possible object is
     *     {@link TotalBallastWaterCapacityMeasureType }
     *     
     */
    public TotalBallastWaterCapacityMeasureType getTotalBallastWaterCapacityMeasure() {
        return totalBallastWaterCapacityMeasure;
    }

    /**
     * Définit la valeur de la propriété totalBallastWaterCapacityMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link TotalBallastWaterCapacityMeasureType }
     *     
     */
    public void setTotalBallastWaterCapacityMeasure(TotalBallastWaterCapacityMeasureType value) {
        this.totalBallastWaterCapacityMeasure = value;
    }

    /**
     * Gets the value of the otherControlActions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the otherControlActions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOtherControlActions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OtherControlActionsType }
     * 
     * 
     */
    public List<OtherControlActionsType> getOtherControlActions() {
        if (otherControlActions == null) {
            otherControlActions = new ArrayList<OtherControlActionsType>();
        }
        return this.otherControlActions;
    }

    /**
     * Gets the value of the noControlActionsReason property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the noControlActionsReason property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNoControlActionsReason().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NoControlActionsReasonType }
     * 
     * 
     */
    public List<NoControlActionsReasonType> getNoControlActionsReason() {
        if (noControlActionsReason == null) {
            noControlActionsReason = new ArrayList<NoControlActionsReasonType>();
        }
        return this.noControlActionsReason;
    }

    /**
     * Gets the value of the uptakeBallastWaterTransaction property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the uptakeBallastWaterTransaction property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUptakeBallastWaterTransaction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BallastWaterTransactionType }
     * 
     * 
     */
    public List<BallastWaterTransactionType> getUptakeBallastWaterTransaction() {
        if (uptakeBallastWaterTransaction == null) {
            uptakeBallastWaterTransaction = new ArrayList<BallastWaterTransactionType>();
        }
        return this.uptakeBallastWaterTransaction;
    }

    /**
     * Gets the value of the exchangeBallastWaterTransaction property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the exchangeBallastWaterTransaction property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExchangeBallastWaterTransaction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BallastWaterTransactionType }
     * 
     * 
     */
    public List<BallastWaterTransactionType> getExchangeBallastWaterTransaction() {
        if (exchangeBallastWaterTransaction == null) {
            exchangeBallastWaterTransaction = new ArrayList<BallastWaterTransactionType>();
        }
        return this.exchangeBallastWaterTransaction;
    }

    /**
     * Gets the value of the dischargeBallastWaterTransaction property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dischargeBallastWaterTransaction property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDischargeBallastWaterTransaction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BallastWaterTransactionType }
     * 
     * 
     */
    public List<BallastWaterTransactionType> getDischargeBallastWaterTransaction() {
        if (dischargeBallastWaterTransaction == null) {
            dischargeBallastWaterTransaction = new ArrayList<BallastWaterTransactionType>();
        }
        return this.dischargeBallastWaterTransaction;
    }

    /**
     * Obtient la valeur de la propriété responsibleOfficerPerson.
     * 
     * @return
     *     possible object is
     *     {@link PersonType }
     *     
     */
    public PersonType getResponsibleOfficerPerson() {
        return responsibleOfficerPerson;
    }

    /**
     * Définit la valeur de la propriété responsibleOfficerPerson.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonType }
     *     
     */
    public void setResponsibleOfficerPerson(PersonType value) {
        this.responsibleOfficerPerson = value;
    }

}
