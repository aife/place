//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.FumigatedCargoTransportIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.InfectiousDiseaseCaseOnBoardIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.LastDrinkingWaterAnalysisDateType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MedicalPractitionerConsultedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MoreIllThanExpectedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ReinspectionRequiredIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SanitaryMeasuresAppliedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SickAnimalDescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SickAnimalOnBoardIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.StowawayDescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.StowawaysFoundOnBoardIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TotalDeadPersonQuantityType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TotalIllPersonQuantityType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ValidSanitationCertificateOnBoardIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour MaritimeHealthDeclarationType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="MaritimeHealthDeclarationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}InfectiousDiseaseCaseOnBoardIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}MoreIllThanExpectedIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}MedicalPractitionerConsultedIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}StowawaysFoundOnBoardIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}SickAnimalOnBoardIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}FumigatedCargoTransportIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}SanitaryMeasuresAppliedIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ValidSanitationCertificateOnBoardIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ReinspectionRequiredIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}TotalDeadPersonQuantity" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}TotalIllPersonQuantity" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}SickAnimalDescription" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}StowawayDescription" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}LastDrinkingWaterAnalysisDate" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}WHOAffectedAreaVisit" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}PersonnelHealthIncident" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}SanitaryMeasure" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}PlaceOfReportLocation" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}MedicalCertificate" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}ShipSanitationControlCertificate" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}ShipSanitationControlExemptionDocumentReference" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MaritimeHealthDeclarationType", propOrder = {
    "ublExtensions",
    "id",
    "infectiousDiseaseCaseOnBoardIndicator",
    "moreIllThanExpectedIndicator",
    "medicalPractitionerConsultedIndicator",
    "stowawaysFoundOnBoardIndicator",
    "sickAnimalOnBoardIndicator",
    "fumigatedCargoTransportIndicator",
    "sanitaryMeasuresAppliedIndicator",
    "validSanitationCertificateOnBoardIndicator",
    "reinspectionRequiredIndicator",
    "totalDeadPersonQuantity",
    "totalIllPersonQuantity",
    "sickAnimalDescription",
    "stowawayDescription",
    "lastDrinkingWaterAnalysisDate",
    "whoAffectedAreaVisit",
    "personnelHealthIncident",
    "sanitaryMeasure",
    "placeOfReportLocation",
    "medicalCertificate",
    "shipSanitationControlCertificate",
    "shipSanitationControlExemptionDocumentReference"
})
@ToString
@EqualsAndHashCode
public class MaritimeHealthDeclarationType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "ID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected IDType id;
    @XmlElement(name = "InfectiousDiseaseCaseOnBoardIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected InfectiousDiseaseCaseOnBoardIndicatorType infectiousDiseaseCaseOnBoardIndicator;
    @XmlElement(name = "MoreIllThanExpectedIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected MoreIllThanExpectedIndicatorType moreIllThanExpectedIndicator;
    @XmlElement(name = "MedicalPractitionerConsultedIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected MedicalPractitionerConsultedIndicatorType medicalPractitionerConsultedIndicator;
    @XmlElement(name = "StowawaysFoundOnBoardIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected StowawaysFoundOnBoardIndicatorType stowawaysFoundOnBoardIndicator;
    @XmlElement(name = "SickAnimalOnBoardIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected SickAnimalOnBoardIndicatorType sickAnimalOnBoardIndicator;
    @XmlElement(name = "FumigatedCargoTransportIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected FumigatedCargoTransportIndicatorType fumigatedCargoTransportIndicator;
    @XmlElement(name = "SanitaryMeasuresAppliedIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected SanitaryMeasuresAppliedIndicatorType sanitaryMeasuresAppliedIndicator;
    @XmlElement(name = "ValidSanitationCertificateOnBoardIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ValidSanitationCertificateOnBoardIndicatorType validSanitationCertificateOnBoardIndicator;
    @XmlElement(name = "ReinspectionRequiredIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ReinspectionRequiredIndicatorType reinspectionRequiredIndicator;
    @XmlElement(name = "TotalDeadPersonQuantity", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected TotalDeadPersonQuantityType totalDeadPersonQuantity;
    @XmlElement(name = "TotalIllPersonQuantity", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected TotalIllPersonQuantityType totalIllPersonQuantity;
    @XmlElement(name = "SickAnimalDescription", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<SickAnimalDescriptionType> sickAnimalDescription;
    @XmlElement(name = "StowawayDescription", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<StowawayDescriptionType> stowawayDescription;
    @XmlElement(name = "LastDrinkingWaterAnalysisDate", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected LastDrinkingWaterAnalysisDateType lastDrinkingWaterAnalysisDate;
    @XmlElement(name = "WHOAffectedAreaVisit")
    protected List<WHOAffectedAreaVisitType> whoAffectedAreaVisit;
    @XmlElement(name = "PersonnelHealthIncident")
    protected List<PersonnelHealthIncidentType> personnelHealthIncident;
    @XmlElement(name = "SanitaryMeasure")
    protected List<SanitaryMeasureType> sanitaryMeasure;
    @XmlElement(name = "PlaceOfReportLocation")
    protected LocationType placeOfReportLocation;
    @XmlElement(name = "MedicalCertificate")
    protected CertificateType medicalCertificate;
    @XmlElement(name = "ShipSanitationControlCertificate")
    protected CertificateType shipSanitationControlCertificate;
    @XmlElement(name = "ShipSanitationControlExemptionDocumentReference")
    protected List<DocumentReferenceType> shipSanitationControlExemptionDocumentReference;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété id.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getID() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setID(IDType value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propriété infectiousDiseaseCaseOnBoardIndicator.
     * 
     * @return
     *     possible object is
     *     {@link InfectiousDiseaseCaseOnBoardIndicatorType }
     *     
     */
    public InfectiousDiseaseCaseOnBoardIndicatorType getInfectiousDiseaseCaseOnBoardIndicator() {
        return infectiousDiseaseCaseOnBoardIndicator;
    }

    /**
     * Définit la valeur de la propriété infectiousDiseaseCaseOnBoardIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link InfectiousDiseaseCaseOnBoardIndicatorType }
     *     
     */
    public void setInfectiousDiseaseCaseOnBoardIndicator(InfectiousDiseaseCaseOnBoardIndicatorType value) {
        this.infectiousDiseaseCaseOnBoardIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété moreIllThanExpectedIndicator.
     * 
     * @return
     *     possible object is
     *     {@link MoreIllThanExpectedIndicatorType }
     *     
     */
    public MoreIllThanExpectedIndicatorType getMoreIllThanExpectedIndicator() {
        return moreIllThanExpectedIndicator;
    }

    /**
     * Définit la valeur de la propriété moreIllThanExpectedIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link MoreIllThanExpectedIndicatorType }
     *     
     */
    public void setMoreIllThanExpectedIndicator(MoreIllThanExpectedIndicatorType value) {
        this.moreIllThanExpectedIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété medicalPractitionerConsultedIndicator.
     * 
     * @return
     *     possible object is
     *     {@link MedicalPractitionerConsultedIndicatorType }
     *     
     */
    public MedicalPractitionerConsultedIndicatorType getMedicalPractitionerConsultedIndicator() {
        return medicalPractitionerConsultedIndicator;
    }

    /**
     * Définit la valeur de la propriété medicalPractitionerConsultedIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link MedicalPractitionerConsultedIndicatorType }
     *     
     */
    public void setMedicalPractitionerConsultedIndicator(MedicalPractitionerConsultedIndicatorType value) {
        this.medicalPractitionerConsultedIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété stowawaysFoundOnBoardIndicator.
     * 
     * @return
     *     possible object is
     *     {@link StowawaysFoundOnBoardIndicatorType }
     *     
     */
    public StowawaysFoundOnBoardIndicatorType getStowawaysFoundOnBoardIndicator() {
        return stowawaysFoundOnBoardIndicator;
    }

    /**
     * Définit la valeur de la propriété stowawaysFoundOnBoardIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link StowawaysFoundOnBoardIndicatorType }
     *     
     */
    public void setStowawaysFoundOnBoardIndicator(StowawaysFoundOnBoardIndicatorType value) {
        this.stowawaysFoundOnBoardIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété sickAnimalOnBoardIndicator.
     * 
     * @return
     *     possible object is
     *     {@link SickAnimalOnBoardIndicatorType }
     *     
     */
    public SickAnimalOnBoardIndicatorType getSickAnimalOnBoardIndicator() {
        return sickAnimalOnBoardIndicator;
    }

    /**
     * Définit la valeur de la propriété sickAnimalOnBoardIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link SickAnimalOnBoardIndicatorType }
     *     
     */
    public void setSickAnimalOnBoardIndicator(SickAnimalOnBoardIndicatorType value) {
        this.sickAnimalOnBoardIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété fumigatedCargoTransportIndicator.
     * 
     * @return
     *     possible object is
     *     {@link FumigatedCargoTransportIndicatorType }
     *     
     */
    public FumigatedCargoTransportIndicatorType getFumigatedCargoTransportIndicator() {
        return fumigatedCargoTransportIndicator;
    }

    /**
     * Définit la valeur de la propriété fumigatedCargoTransportIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link FumigatedCargoTransportIndicatorType }
     *     
     */
    public void setFumigatedCargoTransportIndicator(FumigatedCargoTransportIndicatorType value) {
        this.fumigatedCargoTransportIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété sanitaryMeasuresAppliedIndicator.
     * 
     * @return
     *     possible object is
     *     {@link SanitaryMeasuresAppliedIndicatorType }
     *     
     */
    public SanitaryMeasuresAppliedIndicatorType getSanitaryMeasuresAppliedIndicator() {
        return sanitaryMeasuresAppliedIndicator;
    }

    /**
     * Définit la valeur de la propriété sanitaryMeasuresAppliedIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link SanitaryMeasuresAppliedIndicatorType }
     *     
     */
    public void setSanitaryMeasuresAppliedIndicator(SanitaryMeasuresAppliedIndicatorType value) {
        this.sanitaryMeasuresAppliedIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété validSanitationCertificateOnBoardIndicator.
     * 
     * @return
     *     possible object is
     *     {@link ValidSanitationCertificateOnBoardIndicatorType }
     *     
     */
    public ValidSanitationCertificateOnBoardIndicatorType getValidSanitationCertificateOnBoardIndicator() {
        return validSanitationCertificateOnBoardIndicator;
    }

    /**
     * Définit la valeur de la propriété validSanitationCertificateOnBoardIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidSanitationCertificateOnBoardIndicatorType }
     *     
     */
    public void setValidSanitationCertificateOnBoardIndicator(ValidSanitationCertificateOnBoardIndicatorType value) {
        this.validSanitationCertificateOnBoardIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété reinspectionRequiredIndicator.
     * 
     * @return
     *     possible object is
     *     {@link ReinspectionRequiredIndicatorType }
     *     
     */
    public ReinspectionRequiredIndicatorType getReinspectionRequiredIndicator() {
        return reinspectionRequiredIndicator;
    }

    /**
     * Définit la valeur de la propriété reinspectionRequiredIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link ReinspectionRequiredIndicatorType }
     *     
     */
    public void setReinspectionRequiredIndicator(ReinspectionRequiredIndicatorType value) {
        this.reinspectionRequiredIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété totalDeadPersonQuantity.
     * 
     * @return
     *     possible object is
     *     {@link TotalDeadPersonQuantityType }
     *     
     */
    public TotalDeadPersonQuantityType getTotalDeadPersonQuantity() {
        return totalDeadPersonQuantity;
    }

    /**
     * Définit la valeur de la propriété totalDeadPersonQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link TotalDeadPersonQuantityType }
     *     
     */
    public void setTotalDeadPersonQuantity(TotalDeadPersonQuantityType value) {
        this.totalDeadPersonQuantity = value;
    }

    /**
     * Obtient la valeur de la propriété totalIllPersonQuantity.
     * 
     * @return
     *     possible object is
     *     {@link TotalIllPersonQuantityType }
     *     
     */
    public TotalIllPersonQuantityType getTotalIllPersonQuantity() {
        return totalIllPersonQuantity;
    }

    /**
     * Définit la valeur de la propriété totalIllPersonQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link TotalIllPersonQuantityType }
     *     
     */
    public void setTotalIllPersonQuantity(TotalIllPersonQuantityType value) {
        this.totalIllPersonQuantity = value;
    }

    /**
     * Gets the value of the sickAnimalDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sickAnimalDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSickAnimalDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SickAnimalDescriptionType }
     * 
     * 
     */
    public List<SickAnimalDescriptionType> getSickAnimalDescription() {
        if (sickAnimalDescription == null) {
            sickAnimalDescription = new ArrayList<SickAnimalDescriptionType>();
        }
        return this.sickAnimalDescription;
    }

    /**
     * Gets the value of the stowawayDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the stowawayDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStowawayDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StowawayDescriptionType }
     * 
     * 
     */
    public List<StowawayDescriptionType> getStowawayDescription() {
        if (stowawayDescription == null) {
            stowawayDescription = new ArrayList<StowawayDescriptionType>();
        }
        return this.stowawayDescription;
    }

    /**
     * Obtient la valeur de la propriété lastDrinkingWaterAnalysisDate.
     * 
     * @return
     *     possible object is
     *     {@link LastDrinkingWaterAnalysisDateType }
     *     
     */
    public LastDrinkingWaterAnalysisDateType getLastDrinkingWaterAnalysisDate() {
        return lastDrinkingWaterAnalysisDate;
    }

    /**
     * Définit la valeur de la propriété lastDrinkingWaterAnalysisDate.
     * 
     * @param value
     *     allowed object is
     *     {@link LastDrinkingWaterAnalysisDateType }
     *     
     */
    public void setLastDrinkingWaterAnalysisDate(LastDrinkingWaterAnalysisDateType value) {
        this.lastDrinkingWaterAnalysisDate = value;
    }

    /**
     * Gets the value of the whoAffectedAreaVisit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the whoAffectedAreaVisit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWHOAffectedAreaVisit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WHOAffectedAreaVisitType }
     * 
     * 
     */
    public List<WHOAffectedAreaVisitType> getWHOAffectedAreaVisit() {
        if (whoAffectedAreaVisit == null) {
            whoAffectedAreaVisit = new ArrayList<WHOAffectedAreaVisitType>();
        }
        return this.whoAffectedAreaVisit;
    }

    /**
     * Gets the value of the personnelHealthIncident property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the personnelHealthIncident property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPersonnelHealthIncident().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PersonnelHealthIncidentType }
     * 
     * 
     */
    public List<PersonnelHealthIncidentType> getPersonnelHealthIncident() {
        if (personnelHealthIncident == null) {
            personnelHealthIncident = new ArrayList<PersonnelHealthIncidentType>();
        }
        return this.personnelHealthIncident;
    }

    /**
     * Gets the value of the sanitaryMeasure property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sanitaryMeasure property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSanitaryMeasure().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SanitaryMeasureType }
     * 
     * 
     */
    public List<SanitaryMeasureType> getSanitaryMeasure() {
        if (sanitaryMeasure == null) {
            sanitaryMeasure = new ArrayList<SanitaryMeasureType>();
        }
        return this.sanitaryMeasure;
    }

    /**
     * Obtient la valeur de la propriété placeOfReportLocation.
     * 
     * @return
     *     possible object is
     *     {@link LocationType }
     *     
     */
    public LocationType getPlaceOfReportLocation() {
        return placeOfReportLocation;
    }

    /**
     * Définit la valeur de la propriété placeOfReportLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationType }
     *     
     */
    public void setPlaceOfReportLocation(LocationType value) {
        this.placeOfReportLocation = value;
    }

    /**
     * Obtient la valeur de la propriété medicalCertificate.
     * 
     * @return
     *     possible object is
     *     {@link CertificateType }
     *     
     */
    public CertificateType getMedicalCertificate() {
        return medicalCertificate;
    }

    /**
     * Définit la valeur de la propriété medicalCertificate.
     * 
     * @param value
     *     allowed object is
     *     {@link CertificateType }
     *     
     */
    public void setMedicalCertificate(CertificateType value) {
        this.medicalCertificate = value;
    }

    /**
     * Obtient la valeur de la propriété shipSanitationControlCertificate.
     * 
     * @return
     *     possible object is
     *     {@link CertificateType }
     *     
     */
    public CertificateType getShipSanitationControlCertificate() {
        return shipSanitationControlCertificate;
    }

    /**
     * Définit la valeur de la propriété shipSanitationControlCertificate.
     * 
     * @param value
     *     allowed object is
     *     {@link CertificateType }
     *     
     */
    public void setShipSanitationControlCertificate(CertificateType value) {
        this.shipSanitationControlCertificate = value;
    }

    /**
     * Gets the value of the shipSanitationControlExemptionDocumentReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipSanitationControlExemptionDocumentReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipSanitationControlExemptionDocumentReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentReferenceType }
     * 
     * 
     */
    public List<DocumentReferenceType> getShipSanitationControlExemptionDocumentReference() {
        if (shipSanitationControlExemptionDocumentReference == null) {
            shipSanitationControlExemptionDocumentReference = new ArrayList<DocumentReferenceType>();
        }
        return this.shipSanitationControlExemptionDocumentReference;
    }

}
