//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.InitiatingPartyIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PrivatePartyIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PublicPartyIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ServiceProviderPartyIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour ParticipantPartyType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ParticipantPartyType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}InitiatingPartyIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}PrivatePartyIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}PublicPartyIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ServiceProviderPartyIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}Party"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}LegalContact" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}TechnicalContact" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}SupportContact" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}CommercialContact" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParticipantPartyType", propOrder = {
    "ublExtensions",
    "initiatingPartyIndicator",
    "privatePartyIndicator",
    "publicPartyIndicator",
    "serviceProviderPartyIndicator",
    "party",
    "legalContact",
    "technicalContact",
    "supportContact",
    "commercialContact"
})
@ToString
@EqualsAndHashCode
public class ParticipantPartyType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "InitiatingPartyIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected InitiatingPartyIndicatorType initiatingPartyIndicator;
    @XmlElement(name = "PrivatePartyIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected PrivatePartyIndicatorType privatePartyIndicator;
    @XmlElement(name = "PublicPartyIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected PublicPartyIndicatorType publicPartyIndicator;
    @XmlElement(name = "ServiceProviderPartyIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ServiceProviderPartyIndicatorType serviceProviderPartyIndicator;
    @XmlElement(name = "Party", required = true)
    protected PartyType party;
    @XmlElement(name = "LegalContact")
    protected ContactType legalContact;
    @XmlElement(name = "TechnicalContact")
    protected ContactType technicalContact;
    @XmlElement(name = "SupportContact")
    protected ContactType supportContact;
    @XmlElement(name = "CommercialContact")
    protected ContactType commercialContact;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété initiatingPartyIndicator.
     * 
     * @return
     *     possible object is
     *     {@link InitiatingPartyIndicatorType }
     *     
     */
    public InitiatingPartyIndicatorType getInitiatingPartyIndicator() {
        return initiatingPartyIndicator;
    }

    /**
     * Définit la valeur de la propriété initiatingPartyIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link InitiatingPartyIndicatorType }
     *     
     */
    public void setInitiatingPartyIndicator(InitiatingPartyIndicatorType value) {
        this.initiatingPartyIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété privatePartyIndicator.
     * 
     * @return
     *     possible object is
     *     {@link PrivatePartyIndicatorType }
     *     
     */
    public PrivatePartyIndicatorType getPrivatePartyIndicator() {
        return privatePartyIndicator;
    }

    /**
     * Définit la valeur de la propriété privatePartyIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link PrivatePartyIndicatorType }
     *     
     */
    public void setPrivatePartyIndicator(PrivatePartyIndicatorType value) {
        this.privatePartyIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété publicPartyIndicator.
     * 
     * @return
     *     possible object is
     *     {@link PublicPartyIndicatorType }
     *     
     */
    public PublicPartyIndicatorType getPublicPartyIndicator() {
        return publicPartyIndicator;
    }

    /**
     * Définit la valeur de la propriété publicPartyIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link PublicPartyIndicatorType }
     *     
     */
    public void setPublicPartyIndicator(PublicPartyIndicatorType value) {
        this.publicPartyIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété serviceProviderPartyIndicator.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderPartyIndicatorType }
     *     
     */
    public ServiceProviderPartyIndicatorType getServiceProviderPartyIndicator() {
        return serviceProviderPartyIndicator;
    }

    /**
     * Définit la valeur de la propriété serviceProviderPartyIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderPartyIndicatorType }
     *     
     */
    public void setServiceProviderPartyIndicator(ServiceProviderPartyIndicatorType value) {
        this.serviceProviderPartyIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété party.
     * 
     * @return
     *     possible object is
     *     {@link PartyType }
     *     
     */
    public PartyType getParty() {
        return party;
    }

    /**
     * Définit la valeur de la propriété party.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyType }
     *     
     */
    public void setParty(PartyType value) {
        this.party = value;
    }

    /**
     * Obtient la valeur de la propriété legalContact.
     * 
     * @return
     *     possible object is
     *     {@link ContactType }
     *     
     */
    public ContactType getLegalContact() {
        return legalContact;
    }

    /**
     * Définit la valeur de la propriété legalContact.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactType }
     *     
     */
    public void setLegalContact(ContactType value) {
        this.legalContact = value;
    }

    /**
     * Obtient la valeur de la propriété technicalContact.
     * 
     * @return
     *     possible object is
     *     {@link ContactType }
     *     
     */
    public ContactType getTechnicalContact() {
        return technicalContact;
    }

    /**
     * Définit la valeur de la propriété technicalContact.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactType }
     *     
     */
    public void setTechnicalContact(ContactType value) {
        this.technicalContact = value;
    }

    /**
     * Obtient la valeur de la propriété supportContact.
     * 
     * @return
     *     possible object is
     *     {@link ContactType }
     *     
     */
    public ContactType getSupportContact() {
        return supportContact;
    }

    /**
     * Définit la valeur de la propriété supportContact.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactType }
     *     
     */
    public void setSupportContact(ContactType value) {
        this.supportContact = value;
    }

    /**
     * Obtient la valeur de la propriété commercialContact.
     * 
     * @return
     *     possible object is
     *     {@link ContactType }
     *     
     */
    public ContactType getCommercialContact() {
        return commercialContact;
    }

    /**
     * Définit la valeur de la propriété commercialContact.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactType }
     *     
     */
    public void setCommercialContact(ContactType value) {
        this.commercialContact = value;
    }

}
