//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.europa.data.p27.eforms_ubl_extensions._1.EformsExtension;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * <p>Classe Java pour ExtensionContentType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ExtensionContentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice minOccurs="0"&gt;
 *         &lt;element ref="{http://data.europa.eu/p27/eforms-ubl-extensions/1}EformsExtension"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExtensionContentType", propOrder = {
    "eformsExtension"
})
@ToString
@EqualsAndHashCode
public class ExtensionContentType {

    @XmlElement(name = "EformsExtension", namespace = "http://data.europa.eu/p27/eforms-ubl-extensions/1")
    protected EformsExtension eformsExtension;

    /**
     * Obtient la valeur de la propriété eformsExtension.
     * 
     * @return
     *     possible object is
     *     {@link EformsExtension }
     *     
     */
    public EformsExtension getEformsExtension() {
        return eformsExtension;
    }

    /**
     * Définit la valeur de la propriété eformsExtension.
     * 
     * @param value
     *     allowed object is
     *     {@link EformsExtension }
     *     
     */
    public void setEformsExtension(EformsExtension value) {
        this.eformsExtension = value;
    }

}
