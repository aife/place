//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.AvailabilityTimePercentType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.FridayAvailabilityIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MaximumDataLossDurationMeasureType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MaximumIncidentNotificationDurationMeasureType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MeanTimeToRecoverDurationMeasureType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MinimumDownTimeScheduleDurationMeasureType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MinimumResponseTimeDurationMeasureType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MondayAvailabilityIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SaturdayAvailabilityIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ServiceTypeCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ServiceTypeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SundayAvailabilityIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ThursdayAvailabilityIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TuesdayAvailabilityIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.WednesdayAvailabilityIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour ServiceLevelAgreementType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ServiceLevelAgreementType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ServiceTypeCode" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ServiceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}AvailabilityTimePercent" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}MondayAvailabilityIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}TuesdayAvailabilityIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}WednesdayAvailabilityIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ThursdayAvailabilityIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}FridayAvailabilityIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}SaturdayAvailabilityIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}SundayAvailabilityIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}MinimumResponseTimeDurationMeasure" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}MinimumDownTimeScheduleDurationMeasure" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}MaximumIncidentNotificationDurationMeasure" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}MaximumDataLossDurationMeasure" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}MeanTimeToRecoverDurationMeasure" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}ServiceAvailabilityPeriod" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}ServiceMaintenancePeriod" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceLevelAgreementType", propOrder = {
    "ublExtensions",
    "id",
    "serviceTypeCode",
    "serviceType",
    "availabilityTimePercent",
    "mondayAvailabilityIndicator",
    "tuesdayAvailabilityIndicator",
    "wednesdayAvailabilityIndicator",
    "thursdayAvailabilityIndicator",
    "fridayAvailabilityIndicator",
    "saturdayAvailabilityIndicator",
    "sundayAvailabilityIndicator",
    "minimumResponseTimeDurationMeasure",
    "minimumDownTimeScheduleDurationMeasure",
    "maximumIncidentNotificationDurationMeasure",
    "maximumDataLossDurationMeasure",
    "meanTimeToRecoverDurationMeasure",
    "serviceAvailabilityPeriod",
    "serviceMaintenancePeriod"
})
@ToString
@EqualsAndHashCode
public class ServiceLevelAgreementType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "ID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected IDType id;
    @XmlElement(name = "ServiceTypeCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ServiceTypeCodeType serviceTypeCode;
    @XmlElement(name = "ServiceType", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<ServiceTypeType> serviceType;
    @XmlElement(name = "AvailabilityTimePercent", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected AvailabilityTimePercentType availabilityTimePercent;
    @XmlElement(name = "MondayAvailabilityIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected MondayAvailabilityIndicatorType mondayAvailabilityIndicator;
    @XmlElement(name = "TuesdayAvailabilityIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected TuesdayAvailabilityIndicatorType tuesdayAvailabilityIndicator;
    @XmlElement(name = "WednesdayAvailabilityIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected WednesdayAvailabilityIndicatorType wednesdayAvailabilityIndicator;
    @XmlElement(name = "ThursdayAvailabilityIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ThursdayAvailabilityIndicatorType thursdayAvailabilityIndicator;
    @XmlElement(name = "FridayAvailabilityIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected FridayAvailabilityIndicatorType fridayAvailabilityIndicator;
    @XmlElement(name = "SaturdayAvailabilityIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected SaturdayAvailabilityIndicatorType saturdayAvailabilityIndicator;
    @XmlElement(name = "SundayAvailabilityIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected SundayAvailabilityIndicatorType sundayAvailabilityIndicator;
    @XmlElement(name = "MinimumResponseTimeDurationMeasure", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected MinimumResponseTimeDurationMeasureType minimumResponseTimeDurationMeasure;
    @XmlElement(name = "MinimumDownTimeScheduleDurationMeasure", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected MinimumDownTimeScheduleDurationMeasureType minimumDownTimeScheduleDurationMeasure;
    @XmlElement(name = "MaximumIncidentNotificationDurationMeasure", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected MaximumIncidentNotificationDurationMeasureType maximumIncidentNotificationDurationMeasure;
    @XmlElement(name = "MaximumDataLossDurationMeasure", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected MaximumDataLossDurationMeasureType maximumDataLossDurationMeasure;
    @XmlElement(name = "MeanTimeToRecoverDurationMeasure", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected MeanTimeToRecoverDurationMeasureType meanTimeToRecoverDurationMeasure;
    @XmlElement(name = "ServiceAvailabilityPeriod")
    protected List<PeriodType> serviceAvailabilityPeriod;
    @XmlElement(name = "ServiceMaintenancePeriod")
    protected List<PeriodType> serviceMaintenancePeriod;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété id.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getID() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setID(IDType value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propriété serviceTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link ServiceTypeCodeType }
     *     
     */
    public ServiceTypeCodeType getServiceTypeCode() {
        return serviceTypeCode;
    }

    /**
     * Définit la valeur de la propriété serviceTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceTypeCodeType }
     *     
     */
    public void setServiceTypeCode(ServiceTypeCodeType value) {
        this.serviceTypeCode = value;
    }

    /**
     * Gets the value of the serviceType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceTypeType }
     * 
     * 
     */
    public List<ServiceTypeType> getServiceType() {
        if (serviceType == null) {
            serviceType = new ArrayList<ServiceTypeType>();
        }
        return this.serviceType;
    }

    /**
     * Obtient la valeur de la propriété availabilityTimePercent.
     * 
     * @return
     *     possible object is
     *     {@link AvailabilityTimePercentType }
     *     
     */
    public AvailabilityTimePercentType getAvailabilityTimePercent() {
        return availabilityTimePercent;
    }

    /**
     * Définit la valeur de la propriété availabilityTimePercent.
     * 
     * @param value
     *     allowed object is
     *     {@link AvailabilityTimePercentType }
     *     
     */
    public void setAvailabilityTimePercent(AvailabilityTimePercentType value) {
        this.availabilityTimePercent = value;
    }

    /**
     * Obtient la valeur de la propriété mondayAvailabilityIndicator.
     * 
     * @return
     *     possible object is
     *     {@link MondayAvailabilityIndicatorType }
     *     
     */
    public MondayAvailabilityIndicatorType getMondayAvailabilityIndicator() {
        return mondayAvailabilityIndicator;
    }

    /**
     * Définit la valeur de la propriété mondayAvailabilityIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link MondayAvailabilityIndicatorType }
     *     
     */
    public void setMondayAvailabilityIndicator(MondayAvailabilityIndicatorType value) {
        this.mondayAvailabilityIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété tuesdayAvailabilityIndicator.
     * 
     * @return
     *     possible object is
     *     {@link TuesdayAvailabilityIndicatorType }
     *     
     */
    public TuesdayAvailabilityIndicatorType getTuesdayAvailabilityIndicator() {
        return tuesdayAvailabilityIndicator;
    }

    /**
     * Définit la valeur de la propriété tuesdayAvailabilityIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link TuesdayAvailabilityIndicatorType }
     *     
     */
    public void setTuesdayAvailabilityIndicator(TuesdayAvailabilityIndicatorType value) {
        this.tuesdayAvailabilityIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété wednesdayAvailabilityIndicator.
     * 
     * @return
     *     possible object is
     *     {@link WednesdayAvailabilityIndicatorType }
     *     
     */
    public WednesdayAvailabilityIndicatorType getWednesdayAvailabilityIndicator() {
        return wednesdayAvailabilityIndicator;
    }

    /**
     * Définit la valeur de la propriété wednesdayAvailabilityIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link WednesdayAvailabilityIndicatorType }
     *     
     */
    public void setWednesdayAvailabilityIndicator(WednesdayAvailabilityIndicatorType value) {
        this.wednesdayAvailabilityIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété thursdayAvailabilityIndicator.
     * 
     * @return
     *     possible object is
     *     {@link ThursdayAvailabilityIndicatorType }
     *     
     */
    public ThursdayAvailabilityIndicatorType getThursdayAvailabilityIndicator() {
        return thursdayAvailabilityIndicator;
    }

    /**
     * Définit la valeur de la propriété thursdayAvailabilityIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link ThursdayAvailabilityIndicatorType }
     *     
     */
    public void setThursdayAvailabilityIndicator(ThursdayAvailabilityIndicatorType value) {
        this.thursdayAvailabilityIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété fridayAvailabilityIndicator.
     * 
     * @return
     *     possible object is
     *     {@link FridayAvailabilityIndicatorType }
     *     
     */
    public FridayAvailabilityIndicatorType getFridayAvailabilityIndicator() {
        return fridayAvailabilityIndicator;
    }

    /**
     * Définit la valeur de la propriété fridayAvailabilityIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link FridayAvailabilityIndicatorType }
     *     
     */
    public void setFridayAvailabilityIndicator(FridayAvailabilityIndicatorType value) {
        this.fridayAvailabilityIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété saturdayAvailabilityIndicator.
     * 
     * @return
     *     possible object is
     *     {@link SaturdayAvailabilityIndicatorType }
     *     
     */
    public SaturdayAvailabilityIndicatorType getSaturdayAvailabilityIndicator() {
        return saturdayAvailabilityIndicator;
    }

    /**
     * Définit la valeur de la propriété saturdayAvailabilityIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link SaturdayAvailabilityIndicatorType }
     *     
     */
    public void setSaturdayAvailabilityIndicator(SaturdayAvailabilityIndicatorType value) {
        this.saturdayAvailabilityIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété sundayAvailabilityIndicator.
     * 
     * @return
     *     possible object is
     *     {@link SundayAvailabilityIndicatorType }
     *     
     */
    public SundayAvailabilityIndicatorType getSundayAvailabilityIndicator() {
        return sundayAvailabilityIndicator;
    }

    /**
     * Définit la valeur de la propriété sundayAvailabilityIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link SundayAvailabilityIndicatorType }
     *     
     */
    public void setSundayAvailabilityIndicator(SundayAvailabilityIndicatorType value) {
        this.sundayAvailabilityIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété minimumResponseTimeDurationMeasure.
     * 
     * @return
     *     possible object is
     *     {@link MinimumResponseTimeDurationMeasureType }
     *     
     */
    public MinimumResponseTimeDurationMeasureType getMinimumResponseTimeDurationMeasure() {
        return minimumResponseTimeDurationMeasure;
    }

    /**
     * Définit la valeur de la propriété minimumResponseTimeDurationMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link MinimumResponseTimeDurationMeasureType }
     *     
     */
    public void setMinimumResponseTimeDurationMeasure(MinimumResponseTimeDurationMeasureType value) {
        this.minimumResponseTimeDurationMeasure = value;
    }

    /**
     * Obtient la valeur de la propriété minimumDownTimeScheduleDurationMeasure.
     * 
     * @return
     *     possible object is
     *     {@link MinimumDownTimeScheduleDurationMeasureType }
     *     
     */
    public MinimumDownTimeScheduleDurationMeasureType getMinimumDownTimeScheduleDurationMeasure() {
        return minimumDownTimeScheduleDurationMeasure;
    }

    /**
     * Définit la valeur de la propriété minimumDownTimeScheduleDurationMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link MinimumDownTimeScheduleDurationMeasureType }
     *     
     */
    public void setMinimumDownTimeScheduleDurationMeasure(MinimumDownTimeScheduleDurationMeasureType value) {
        this.minimumDownTimeScheduleDurationMeasure = value;
    }

    /**
     * Obtient la valeur de la propriété maximumIncidentNotificationDurationMeasure.
     * 
     * @return
     *     possible object is
     *     {@link MaximumIncidentNotificationDurationMeasureType }
     *     
     */
    public MaximumIncidentNotificationDurationMeasureType getMaximumIncidentNotificationDurationMeasure() {
        return maximumIncidentNotificationDurationMeasure;
    }

    /**
     * Définit la valeur de la propriété maximumIncidentNotificationDurationMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link MaximumIncidentNotificationDurationMeasureType }
     *     
     */
    public void setMaximumIncidentNotificationDurationMeasure(MaximumIncidentNotificationDurationMeasureType value) {
        this.maximumIncidentNotificationDurationMeasure = value;
    }

    /**
     * Obtient la valeur de la propriété maximumDataLossDurationMeasure.
     * 
     * @return
     *     possible object is
     *     {@link MaximumDataLossDurationMeasureType }
     *     
     */
    public MaximumDataLossDurationMeasureType getMaximumDataLossDurationMeasure() {
        return maximumDataLossDurationMeasure;
    }

    /**
     * Définit la valeur de la propriété maximumDataLossDurationMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link MaximumDataLossDurationMeasureType }
     *     
     */
    public void setMaximumDataLossDurationMeasure(MaximumDataLossDurationMeasureType value) {
        this.maximumDataLossDurationMeasure = value;
    }

    /**
     * Obtient la valeur de la propriété meanTimeToRecoverDurationMeasure.
     * 
     * @return
     *     possible object is
     *     {@link MeanTimeToRecoverDurationMeasureType }
     *     
     */
    public MeanTimeToRecoverDurationMeasureType getMeanTimeToRecoverDurationMeasure() {
        return meanTimeToRecoverDurationMeasure;
    }

    /**
     * Définit la valeur de la propriété meanTimeToRecoverDurationMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link MeanTimeToRecoverDurationMeasureType }
     *     
     */
    public void setMeanTimeToRecoverDurationMeasure(MeanTimeToRecoverDurationMeasureType value) {
        this.meanTimeToRecoverDurationMeasure = value;
    }

    /**
     * Gets the value of the serviceAvailabilityPeriod property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceAvailabilityPeriod property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceAvailabilityPeriod().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PeriodType }
     * 
     * 
     */
    public List<PeriodType> getServiceAvailabilityPeriod() {
        if (serviceAvailabilityPeriod == null) {
            serviceAvailabilityPeriod = new ArrayList<PeriodType>();
        }
        return this.serviceAvailabilityPeriod;
    }

    /**
     * Gets the value of the serviceMaintenancePeriod property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceMaintenancePeriod property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceMaintenancePeriod().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PeriodType }
     * 
     * 
     */
    public List<PeriodType> getServiceMaintenancePeriod() {
        if (serviceMaintenancePeriod == null) {
            serviceMaintenancePeriod = new ArrayList<PeriodType>();
        }
        return this.serviceMaintenancePeriod;
    }

}
