//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.AtAnchorageIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.CourseOverGroundDirectionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.NavigationStatusCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.RateOfTurnMeasureType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SpeedOverGroundMeasureType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour VesselDynamicsType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="VesselDynamicsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}NavigationStatusCode" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}AtAnchorageIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}CourseOverGroundDirection" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}SpeedOverGroundMeasure" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}RateOfTurnMeasure" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VesselDynamicsType", propOrder = {
    "ublExtensions",
    "navigationStatusCode",
    "atAnchorageIndicator",
    "courseOverGroundDirection",
    "speedOverGroundMeasure",
    "rateOfTurnMeasure"
})
@ToString
@EqualsAndHashCode
public class VesselDynamicsType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "NavigationStatusCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected NavigationStatusCodeType navigationStatusCode;
    @XmlElement(name = "AtAnchorageIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected AtAnchorageIndicatorType atAnchorageIndicator;
    @XmlElement(name = "CourseOverGroundDirection", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected CourseOverGroundDirectionType courseOverGroundDirection;
    @XmlElement(name = "SpeedOverGroundMeasure", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected SpeedOverGroundMeasureType speedOverGroundMeasure;
    @XmlElement(name = "RateOfTurnMeasure", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected RateOfTurnMeasureType rateOfTurnMeasure;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété navigationStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link NavigationStatusCodeType }
     *     
     */
    public NavigationStatusCodeType getNavigationStatusCode() {
        return navigationStatusCode;
    }

    /**
     * Définit la valeur de la propriété navigationStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link NavigationStatusCodeType }
     *     
     */
    public void setNavigationStatusCode(NavigationStatusCodeType value) {
        this.navigationStatusCode = value;
    }

    /**
     * Obtient la valeur de la propriété atAnchorageIndicator.
     * 
     * @return
     *     possible object is
     *     {@link AtAnchorageIndicatorType }
     *     
     */
    public AtAnchorageIndicatorType getAtAnchorageIndicator() {
        return atAnchorageIndicator;
    }

    /**
     * Définit la valeur de la propriété atAnchorageIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link AtAnchorageIndicatorType }
     *     
     */
    public void setAtAnchorageIndicator(AtAnchorageIndicatorType value) {
        this.atAnchorageIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété courseOverGroundDirection.
     * 
     * @return
     *     possible object is
     *     {@link CourseOverGroundDirectionType }
     *     
     */
    public CourseOverGroundDirectionType getCourseOverGroundDirection() {
        return courseOverGroundDirection;
    }

    /**
     * Définit la valeur de la propriété courseOverGroundDirection.
     * 
     * @param value
     *     allowed object is
     *     {@link CourseOverGroundDirectionType }
     *     
     */
    public void setCourseOverGroundDirection(CourseOverGroundDirectionType value) {
        this.courseOverGroundDirection = value;
    }

    /**
     * Obtient la valeur de la propriété speedOverGroundMeasure.
     * 
     * @return
     *     possible object is
     *     {@link SpeedOverGroundMeasureType }
     *     
     */
    public SpeedOverGroundMeasureType getSpeedOverGroundMeasure() {
        return speedOverGroundMeasure;
    }

    /**
     * Définit la valeur de la propriété speedOverGroundMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link SpeedOverGroundMeasureType }
     *     
     */
    public void setSpeedOverGroundMeasure(SpeedOverGroundMeasureType value) {
        this.speedOverGroundMeasure = value;
    }

    /**
     * Obtient la valeur de la propriété rateOfTurnMeasure.
     * 
     * @return
     *     possible object is
     *     {@link RateOfTurnMeasureType }
     *     
     */
    public RateOfTurnMeasureType getRateOfTurnMeasure() {
        return rateOfTurnMeasure;
    }

    /**
     * Définit la valeur de la propriété rateOfTurnMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link RateOfTurnMeasureType }
     *     
     */
    public void setRateOfTurnMeasure(RateOfTurnMeasureType value) {
        this.rateOfTurnMeasure = value;
    }

}
