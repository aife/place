//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.BuriedAtSeaIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DiedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.EvacuatedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.GivenTreatmentDescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.JoinedShipDateType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.NatureOfIllnessDescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.NoteType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.OnsetDateType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ReportedToMedicalOfficerIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.StillIllIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.StillOnBoardIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour PersonnelHealthIncidentType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="PersonnelHealthIncidentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ID"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}JoinedShipDate" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}NatureOfIllnessDescription" maxOccurs="unbounded"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}OnsetDate" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ReportedToMedicalOfficerIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}GivenTreatmentDescription" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}StillIllIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}DiedIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}StillOnBoardIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}EvacuatedIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}BuriedAtSeaIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}Note" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}Person" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonnelHealthIncidentType", propOrder = {
    "ublExtensions",
    "id",
    "joinedShipDate",
    "natureOfIllnessDescription",
    "onsetDate",
    "reportedToMedicalOfficerIndicator",
    "givenTreatmentDescription",
    "stillIllIndicator",
    "diedIndicator",
    "stillOnBoardIndicator",
    "evacuatedIndicator",
    "buriedAtSeaIndicator",
    "note",
    "person"
})
@ToString
@EqualsAndHashCode
public class PersonnelHealthIncidentType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "ID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2", required = true)
    protected IDType id;
    @XmlElement(name = "JoinedShipDate", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected JoinedShipDateType joinedShipDate;
    @XmlElement(name = "NatureOfIllnessDescription", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2", required = true)
    protected List<NatureOfIllnessDescriptionType> natureOfIllnessDescription;
    @XmlElement(name = "OnsetDate", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected OnsetDateType onsetDate;
    @XmlElement(name = "ReportedToMedicalOfficerIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ReportedToMedicalOfficerIndicatorType reportedToMedicalOfficerIndicator;
    @XmlElement(name = "GivenTreatmentDescription", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<GivenTreatmentDescriptionType> givenTreatmentDescription;
    @XmlElement(name = "StillIllIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected StillIllIndicatorType stillIllIndicator;
    @XmlElement(name = "DiedIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected DiedIndicatorType diedIndicator;
    @XmlElement(name = "StillOnBoardIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected StillOnBoardIndicatorType stillOnBoardIndicator;
    @XmlElement(name = "EvacuatedIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected EvacuatedIndicatorType evacuatedIndicator;
    @XmlElement(name = "BuriedAtSeaIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected BuriedAtSeaIndicatorType buriedAtSeaIndicator;
    @XmlElement(name = "Note", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<NoteType> note;
    @XmlElement(name = "Person")
    protected PersonType person;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété id.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getID() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setID(IDType value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propriété joinedShipDate.
     * 
     * @return
     *     possible object is
     *     {@link JoinedShipDateType }
     *     
     */
    public JoinedShipDateType getJoinedShipDate() {
        return joinedShipDate;
    }

    /**
     * Définit la valeur de la propriété joinedShipDate.
     * 
     * @param value
     *     allowed object is
     *     {@link JoinedShipDateType }
     *     
     */
    public void setJoinedShipDate(JoinedShipDateType value) {
        this.joinedShipDate = value;
    }

    /**
     * Gets the value of the natureOfIllnessDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the natureOfIllnessDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNatureOfIllnessDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NatureOfIllnessDescriptionType }
     * 
     * 
     */
    public List<NatureOfIllnessDescriptionType> getNatureOfIllnessDescription() {
        if (natureOfIllnessDescription == null) {
            natureOfIllnessDescription = new ArrayList<NatureOfIllnessDescriptionType>();
        }
        return this.natureOfIllnessDescription;
    }

    /**
     * Obtient la valeur de la propriété onsetDate.
     * 
     * @return
     *     possible object is
     *     {@link OnsetDateType }
     *     
     */
    public OnsetDateType getOnsetDate() {
        return onsetDate;
    }

    /**
     * Définit la valeur de la propriété onsetDate.
     * 
     * @param value
     *     allowed object is
     *     {@link OnsetDateType }
     *     
     */
    public void setOnsetDate(OnsetDateType value) {
        this.onsetDate = value;
    }

    /**
     * Obtient la valeur de la propriété reportedToMedicalOfficerIndicator.
     * 
     * @return
     *     possible object is
     *     {@link ReportedToMedicalOfficerIndicatorType }
     *     
     */
    public ReportedToMedicalOfficerIndicatorType getReportedToMedicalOfficerIndicator() {
        return reportedToMedicalOfficerIndicator;
    }

    /**
     * Définit la valeur de la propriété reportedToMedicalOfficerIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link ReportedToMedicalOfficerIndicatorType }
     *     
     */
    public void setReportedToMedicalOfficerIndicator(ReportedToMedicalOfficerIndicatorType value) {
        this.reportedToMedicalOfficerIndicator = value;
    }

    /**
     * Gets the value of the givenTreatmentDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the givenTreatmentDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGivenTreatmentDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GivenTreatmentDescriptionType }
     * 
     * 
     */
    public List<GivenTreatmentDescriptionType> getGivenTreatmentDescription() {
        if (givenTreatmentDescription == null) {
            givenTreatmentDescription = new ArrayList<GivenTreatmentDescriptionType>();
        }
        return this.givenTreatmentDescription;
    }

    /**
     * Obtient la valeur de la propriété stillIllIndicator.
     * 
     * @return
     *     possible object is
     *     {@link StillIllIndicatorType }
     *     
     */
    public StillIllIndicatorType getStillIllIndicator() {
        return stillIllIndicator;
    }

    /**
     * Définit la valeur de la propriété stillIllIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link StillIllIndicatorType }
     *     
     */
    public void setStillIllIndicator(StillIllIndicatorType value) {
        this.stillIllIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété diedIndicator.
     * 
     * @return
     *     possible object is
     *     {@link DiedIndicatorType }
     *     
     */
    public DiedIndicatorType getDiedIndicator() {
        return diedIndicator;
    }

    /**
     * Définit la valeur de la propriété diedIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link DiedIndicatorType }
     *     
     */
    public void setDiedIndicator(DiedIndicatorType value) {
        this.diedIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété stillOnBoardIndicator.
     * 
     * @return
     *     possible object is
     *     {@link StillOnBoardIndicatorType }
     *     
     */
    public StillOnBoardIndicatorType getStillOnBoardIndicator() {
        return stillOnBoardIndicator;
    }

    /**
     * Définit la valeur de la propriété stillOnBoardIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link StillOnBoardIndicatorType }
     *     
     */
    public void setStillOnBoardIndicator(StillOnBoardIndicatorType value) {
        this.stillOnBoardIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété evacuatedIndicator.
     * 
     * @return
     *     possible object is
     *     {@link EvacuatedIndicatorType }
     *     
     */
    public EvacuatedIndicatorType getEvacuatedIndicator() {
        return evacuatedIndicator;
    }

    /**
     * Définit la valeur de la propriété evacuatedIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link EvacuatedIndicatorType }
     *     
     */
    public void setEvacuatedIndicator(EvacuatedIndicatorType value) {
        this.evacuatedIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété buriedAtSeaIndicator.
     * 
     * @return
     *     possible object is
     *     {@link BuriedAtSeaIndicatorType }
     *     
     */
    public BuriedAtSeaIndicatorType getBuriedAtSeaIndicator() {
        return buriedAtSeaIndicator;
    }

    /**
     * Définit la valeur de la propriété buriedAtSeaIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link BuriedAtSeaIndicatorType }
     *     
     */
    public void setBuriedAtSeaIndicator(BuriedAtSeaIndicatorType value) {
        this.buriedAtSeaIndicator = value;
    }

    /**
     * Gets the value of the note property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the note property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNote().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NoteType }
     * 
     * 
     */
    public List<NoteType> getNote() {
        if (note == null) {
            note = new ArrayList<NoteType>();
        }
        return this.note;
    }

    /**
     * Obtient la valeur de la propriété person.
     * 
     * @return
     *     possible object is
     *     {@link PersonType }
     *     
     */
    public PersonType getPerson() {
        return person;
    }

    /**
     * Définit la valeur de la propriété person.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonType }
     *     
     */
    public void setPerson(PersonType value) {
        this.person = value;
    }

}
