//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ApplicationDateType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SanitaryMeasureTypeCodeType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour SanitaryMeasureType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="SanitaryMeasureType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}SanitaryMeasureTypeCode"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ApplicationDate" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SanitaryMeasureType", propOrder = {
    "ublExtensions",
    "sanitaryMeasureTypeCode",
    "applicationDate"
})
@ToString
@EqualsAndHashCode
public class SanitaryMeasureType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "SanitaryMeasureTypeCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2", required = true)
    protected SanitaryMeasureTypeCodeType sanitaryMeasureTypeCode;
    @XmlElement(name = "ApplicationDate", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ApplicationDateType applicationDate;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété sanitaryMeasureTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link SanitaryMeasureTypeCodeType }
     *     
     */
    public SanitaryMeasureTypeCodeType getSanitaryMeasureTypeCode() {
        return sanitaryMeasureTypeCode;
    }

    /**
     * Définit la valeur de la propriété sanitaryMeasureTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link SanitaryMeasureTypeCodeType }
     *     
     */
    public void setSanitaryMeasureTypeCode(SanitaryMeasureTypeCodeType value) {
        this.sanitaryMeasureTypeCode = value;
    }

    /**
     * Obtient la valeur de la propriété applicationDate.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationDateType }
     *     
     */
    public ApplicationDateType getApplicationDate() {
        return applicationDate;
    }

    /**
     * Définit la valeur de la propriété applicationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationDateType }
     *     
     */
    public void setApplicationDate(ApplicationDateType value) {
        this.applicationDate = value;
    }

}
