//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.FinalReexportationDateType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.GoodsItemPassportIDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.NoteType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour GoodsItemPassportCounterfoilType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="GoodsItemPassportCounterfoilType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ID"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}GoodsItemPassportID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}FinalReexportationDate" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}Note" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}CustomsOfficeLocation" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}GoodsItem" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}ExportationDocumentReference" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}ImportationDocumentReference" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}ReexportationDocumentReference" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}ReimportationDocumentReference" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}VoucherDocumentReference" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GoodsItemPassportCounterfoilType", propOrder = {
    "ublExtensions",
    "id",
    "goodsItemPassportID",
    "finalReexportationDate",
    "note",
    "customsOfficeLocation",
    "goodsItem",
    "exportationDocumentReference",
    "importationDocumentReference",
    "reexportationDocumentReference",
    "reimportationDocumentReference",
    "voucherDocumentReference"
})
@ToString
@EqualsAndHashCode
public class GoodsItemPassportCounterfoilType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "ID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2", required = true)
    protected IDType id;
    @XmlElement(name = "GoodsItemPassportID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected GoodsItemPassportIDType goodsItemPassportID;
    @XmlElement(name = "FinalReexportationDate", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected FinalReexportationDateType finalReexportationDate;
    @XmlElement(name = "Note", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<NoteType> note;
    @XmlElement(name = "CustomsOfficeLocation")
    protected LocationType customsOfficeLocation;
    @XmlElement(name = "GoodsItem")
    protected GoodsItemType goodsItem;
    @XmlElement(name = "ExportationDocumentReference")
    protected List<DocumentReferenceType> exportationDocumentReference;
    @XmlElement(name = "ImportationDocumentReference")
    protected List<DocumentReferenceType> importationDocumentReference;
    @XmlElement(name = "ReexportationDocumentReference")
    protected List<DocumentReferenceType> reexportationDocumentReference;
    @XmlElement(name = "ReimportationDocumentReference")
    protected List<DocumentReferenceType> reimportationDocumentReference;
    @XmlElement(name = "VoucherDocumentReference")
    protected List<DocumentReferenceType> voucherDocumentReference;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété id.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getID() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setID(IDType value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propriété goodsItemPassportID.
     * 
     * @return
     *     possible object is
     *     {@link GoodsItemPassportIDType }
     *     
     */
    public GoodsItemPassportIDType getGoodsItemPassportID() {
        return goodsItemPassportID;
    }

    /**
     * Définit la valeur de la propriété goodsItemPassportID.
     * 
     * @param value
     *     allowed object is
     *     {@link GoodsItemPassportIDType }
     *     
     */
    public void setGoodsItemPassportID(GoodsItemPassportIDType value) {
        this.goodsItemPassportID = value;
    }

    /**
     * Obtient la valeur de la propriété finalReexportationDate.
     * 
     * @return
     *     possible object is
     *     {@link FinalReexportationDateType }
     *     
     */
    public FinalReexportationDateType getFinalReexportationDate() {
        return finalReexportationDate;
    }

    /**
     * Définit la valeur de la propriété finalReexportationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link FinalReexportationDateType }
     *     
     */
    public void setFinalReexportationDate(FinalReexportationDateType value) {
        this.finalReexportationDate = value;
    }

    /**
     * Gets the value of the note property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the note property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNote().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NoteType }
     * 
     * 
     */
    public List<NoteType> getNote() {
        if (note == null) {
            note = new ArrayList<NoteType>();
        }
        return this.note;
    }

    /**
     * Obtient la valeur de la propriété customsOfficeLocation.
     * 
     * @return
     *     possible object is
     *     {@link LocationType }
     *     
     */
    public LocationType getCustomsOfficeLocation() {
        return customsOfficeLocation;
    }

    /**
     * Définit la valeur de la propriété customsOfficeLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationType }
     *     
     */
    public void setCustomsOfficeLocation(LocationType value) {
        this.customsOfficeLocation = value;
    }

    /**
     * Obtient la valeur de la propriété goodsItem.
     * 
     * @return
     *     possible object is
     *     {@link GoodsItemType }
     *     
     */
    public GoodsItemType getGoodsItem() {
        return goodsItem;
    }

    /**
     * Définit la valeur de la propriété goodsItem.
     * 
     * @param value
     *     allowed object is
     *     {@link GoodsItemType }
     *     
     */
    public void setGoodsItem(GoodsItemType value) {
        this.goodsItem = value;
    }

    /**
     * Gets the value of the exportationDocumentReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the exportationDocumentReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExportationDocumentReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentReferenceType }
     * 
     * 
     */
    public List<DocumentReferenceType> getExportationDocumentReference() {
        if (exportationDocumentReference == null) {
            exportationDocumentReference = new ArrayList<DocumentReferenceType>();
        }
        return this.exportationDocumentReference;
    }

    /**
     * Gets the value of the importationDocumentReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the importationDocumentReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getImportationDocumentReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentReferenceType }
     * 
     * 
     */
    public List<DocumentReferenceType> getImportationDocumentReference() {
        if (importationDocumentReference == null) {
            importationDocumentReference = new ArrayList<DocumentReferenceType>();
        }
        return this.importationDocumentReference;
    }

    /**
     * Gets the value of the reexportationDocumentReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reexportationDocumentReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReexportationDocumentReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentReferenceType }
     * 
     * 
     */
    public List<DocumentReferenceType> getReexportationDocumentReference() {
        if (reexportationDocumentReference == null) {
            reexportationDocumentReference = new ArrayList<DocumentReferenceType>();
        }
        return this.reexportationDocumentReference;
    }

    /**
     * Gets the value of the reimportationDocumentReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reimportationDocumentReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReimportationDocumentReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentReferenceType }
     * 
     * 
     */
    public List<DocumentReferenceType> getReimportationDocumentReference() {
        if (reimportationDocumentReference == null) {
            reimportationDocumentReference = new ArrayList<DocumentReferenceType>();
        }
        return this.reimportationDocumentReference;
    }

    /**
     * Gets the value of the voucherDocumentReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the voucherDocumentReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVoucherDocumentReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentReferenceType }
     * 
     * 
     */
    public List<DocumentReferenceType> getVoucherDocumentReference() {
        if (voucherDocumentReference == null) {
            voucherDocumentReference = new ArrayList<DocumentReferenceType>();
        }
        return this.voucherDocumentReference;
    }

}
