//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.EndpointURIType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.EnvelopeTypeCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ProtocolIDType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour MessageDeliveryType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="MessageDeliveryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ProtocolID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}EnvelopeTypeCode" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}EndpointURI" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageDeliveryType", propOrder = {
    "ublExtensions",
    "protocolID",
    "envelopeTypeCode",
    "endpointURI"
})
@ToString
@EqualsAndHashCode
public class MessageDeliveryType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "ProtocolID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ProtocolIDType protocolID;
    @XmlElement(name = "EnvelopeTypeCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected EnvelopeTypeCodeType envelopeTypeCode;
    @XmlElement(name = "EndpointURI", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected EndpointURIType endpointURI;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété protocolID.
     * 
     * @return
     *     possible object is
     *     {@link ProtocolIDType }
     *     
     */
    public ProtocolIDType getProtocolID() {
        return protocolID;
    }

    /**
     * Définit la valeur de la propriété protocolID.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtocolIDType }
     *     
     */
    public void setProtocolID(ProtocolIDType value) {
        this.protocolID = value;
    }

    /**
     * Obtient la valeur de la propriété envelopeTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link EnvelopeTypeCodeType }
     *     
     */
    public EnvelopeTypeCodeType getEnvelopeTypeCode() {
        return envelopeTypeCode;
    }

    /**
     * Définit la valeur de la propriété envelopeTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link EnvelopeTypeCodeType }
     *     
     */
    public void setEnvelopeTypeCode(EnvelopeTypeCodeType value) {
        this.envelopeTypeCode = value;
    }

    /**
     * Obtient la valeur de la propriété endpointURI.
     * 
     * @return
     *     possible object is
     *     {@link EndpointURIType }
     *     
     */
    public EndpointURIType getEndpointURI() {
        return endpointURI;
    }

    /**
     * Définit la valeur de la propriété endpointURI.
     * 
     * @param value
     *     allowed object is
     *     {@link EndpointURIType }
     *     
     */
    public void setEndpointURI(EndpointURIType value) {
        this.endpointURI = value;
    }

}
