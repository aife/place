//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//

@javax.xml.bind.annotation.XmlSchema(namespace = "urn:oasis:names:specification:ubl:schema:xsd:ContractAwardNotice-2",
        elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED, xmlns = {
        @javax.xml.bind.annotation.XmlNs(namespaceURI = "urn:oasis:names:specification:ubl:schema:xsd:ContractAwardNotice-2", prefix = ""),
        @javax.xml.bind.annotation.XmlNs(namespaceURI = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2", prefix = "cac"),
        @javax.xml.bind.annotation.XmlNs(namespaceURI = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2", prefix = "cbc"),
        @javax.xml.bind.annotation.XmlNs(namespaceURI = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2", prefix = "ext"),
        @javax.xml.bind.annotation.XmlNs(namespaceURI = "http://data.europa.eu/p27/eforms-ubl-extensions/1", prefix = "efext"),
        @javax.xml.bind.annotation.XmlNs(namespaceURI = "http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1", prefix = "efac"),
        @javax.xml.bind.annotation.XmlNs(namespaceURI = "http://www.w3.org/2001/XMLSchema-instance", prefix = "xsi"),
        @javax.xml.bind.annotation.XmlNs(namespaceURI = "http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1", prefix = "efbc")})
package oasis.names.specification.ubl.schema.xsd.contractawardnotice_2;
