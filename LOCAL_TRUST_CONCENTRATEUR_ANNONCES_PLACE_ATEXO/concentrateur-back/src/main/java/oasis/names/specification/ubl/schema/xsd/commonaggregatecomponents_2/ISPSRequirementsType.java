//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.AdditionalMattersDescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.CurrentOperatingSecurityLevelCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ISSCAbsenceReasonType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ISSCExpiryDateType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SSPOnBoardIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SSPSecurityMeasuresAppliedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ValidISSCIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour ISPSRequirementsType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ISPSRequirementsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ID"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ValidISSCIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ISSCAbsenceReason" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ISSCExpiryDate" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}SSPOnBoardIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}SSPSecurityMeasuresAppliedIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}CurrentOperatingSecurityLevelCode" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}AdditionalMattersDescription" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}AdditionalSecurityMeasure" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}PortCallRecord" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}ShipToShipActivityRecord" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}ReportLocation" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}ISSCIssuerParty" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}SecurityOfficerPerson" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ISPSRequirementsType", propOrder = {
    "ublExtensions",
    "id",
    "validISSCIndicator",
    "isscAbsenceReason",
    "isscExpiryDate",
    "sspOnBoardIndicator",
    "sspSecurityMeasuresAppliedIndicator",
    "currentOperatingSecurityLevelCode",
    "additionalMattersDescription",
    "additionalSecurityMeasure",
    "portCallRecord",
    "shipToShipActivityRecord",
    "reportLocation",
    "isscIssuerParty",
    "securityOfficerPerson"
})
@ToString
@EqualsAndHashCode
public class ISPSRequirementsType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "ID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2", required = true)
    protected IDType id;
    @XmlElement(name = "ValidISSCIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ValidISSCIndicatorType validISSCIndicator;
    @XmlElement(name = "ISSCAbsenceReason", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<ISSCAbsenceReasonType> isscAbsenceReason;
    @XmlElement(name = "ISSCExpiryDate", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ISSCExpiryDateType isscExpiryDate;
    @XmlElement(name = "SSPOnBoardIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected SSPOnBoardIndicatorType sspOnBoardIndicator;
    @XmlElement(name = "SSPSecurityMeasuresAppliedIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected SSPSecurityMeasuresAppliedIndicatorType sspSecurityMeasuresAppliedIndicator;
    @XmlElement(name = "CurrentOperatingSecurityLevelCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected CurrentOperatingSecurityLevelCodeType currentOperatingSecurityLevelCode;
    @XmlElement(name = "AdditionalMattersDescription", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<AdditionalMattersDescriptionType> additionalMattersDescription;
    @XmlElement(name = "AdditionalSecurityMeasure")
    protected List<SecurityMeasureType> additionalSecurityMeasure;
    @XmlElement(name = "PortCallRecord")
    protected List<PortCallRecordType> portCallRecord;
    @XmlElement(name = "ShipToShipActivityRecord")
    protected List<ShipToShipActivityRecordType> shipToShipActivityRecord;
    @XmlElement(name = "ReportLocation")
    protected LocationType reportLocation;
    @XmlElement(name = "ISSCIssuerParty")
    protected PartyType isscIssuerParty;
    @XmlElement(name = "SecurityOfficerPerson")
    protected PersonType securityOfficerPerson;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété id.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getID() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setID(IDType value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propriété validISSCIndicator.
     * 
     * @return
     *     possible object is
     *     {@link ValidISSCIndicatorType }
     *     
     */
    public ValidISSCIndicatorType getValidISSCIndicator() {
        return validISSCIndicator;
    }

    /**
     * Définit la valeur de la propriété validISSCIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidISSCIndicatorType }
     *     
     */
    public void setValidISSCIndicator(ValidISSCIndicatorType value) {
        this.validISSCIndicator = value;
    }

    /**
     * Gets the value of the isscAbsenceReason property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the isscAbsenceReason property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getISSCAbsenceReason().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ISSCAbsenceReasonType }
     * 
     * 
     */
    public List<ISSCAbsenceReasonType> getISSCAbsenceReason() {
        if (isscAbsenceReason == null) {
            isscAbsenceReason = new ArrayList<ISSCAbsenceReasonType>();
        }
        return this.isscAbsenceReason;
    }

    /**
     * Obtient la valeur de la propriété isscExpiryDate.
     * 
     * @return
     *     possible object is
     *     {@link ISSCExpiryDateType }
     *     
     */
    public ISSCExpiryDateType getISSCExpiryDate() {
        return isscExpiryDate;
    }

    /**
     * Définit la valeur de la propriété isscExpiryDate.
     * 
     * @param value
     *     allowed object is
     *     {@link ISSCExpiryDateType }
     *     
     */
    public void setISSCExpiryDate(ISSCExpiryDateType value) {
        this.isscExpiryDate = value;
    }

    /**
     * Obtient la valeur de la propriété sspOnBoardIndicator.
     * 
     * @return
     *     possible object is
     *     {@link SSPOnBoardIndicatorType }
     *     
     */
    public SSPOnBoardIndicatorType getSSPOnBoardIndicator() {
        return sspOnBoardIndicator;
    }

    /**
     * Définit la valeur de la propriété sspOnBoardIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link SSPOnBoardIndicatorType }
     *     
     */
    public void setSSPOnBoardIndicator(SSPOnBoardIndicatorType value) {
        this.sspOnBoardIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété sspSecurityMeasuresAppliedIndicator.
     * 
     * @return
     *     possible object is
     *     {@link SSPSecurityMeasuresAppliedIndicatorType }
     *     
     */
    public SSPSecurityMeasuresAppliedIndicatorType getSSPSecurityMeasuresAppliedIndicator() {
        return sspSecurityMeasuresAppliedIndicator;
    }

    /**
     * Définit la valeur de la propriété sspSecurityMeasuresAppliedIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link SSPSecurityMeasuresAppliedIndicatorType }
     *     
     */
    public void setSSPSecurityMeasuresAppliedIndicator(SSPSecurityMeasuresAppliedIndicatorType value) {
        this.sspSecurityMeasuresAppliedIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété currentOperatingSecurityLevelCode.
     * 
     * @return
     *     possible object is
     *     {@link CurrentOperatingSecurityLevelCodeType }
     *     
     */
    public CurrentOperatingSecurityLevelCodeType getCurrentOperatingSecurityLevelCode() {
        return currentOperatingSecurityLevelCode;
    }

    /**
     * Définit la valeur de la propriété currentOperatingSecurityLevelCode.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrentOperatingSecurityLevelCodeType }
     *     
     */
    public void setCurrentOperatingSecurityLevelCode(CurrentOperatingSecurityLevelCodeType value) {
        this.currentOperatingSecurityLevelCode = value;
    }

    /**
     * Gets the value of the additionalMattersDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalMattersDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalMattersDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdditionalMattersDescriptionType }
     * 
     * 
     */
    public List<AdditionalMattersDescriptionType> getAdditionalMattersDescription() {
        if (additionalMattersDescription == null) {
            additionalMattersDescription = new ArrayList<AdditionalMattersDescriptionType>();
        }
        return this.additionalMattersDescription;
    }

    /**
     * Gets the value of the additionalSecurityMeasure property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalSecurityMeasure property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalSecurityMeasure().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SecurityMeasureType }
     * 
     * 
     */
    public List<SecurityMeasureType> getAdditionalSecurityMeasure() {
        if (additionalSecurityMeasure == null) {
            additionalSecurityMeasure = new ArrayList<SecurityMeasureType>();
        }
        return this.additionalSecurityMeasure;
    }

    /**
     * Gets the value of the portCallRecord property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the portCallRecord property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPortCallRecord().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PortCallRecordType }
     * 
     * 
     */
    public List<PortCallRecordType> getPortCallRecord() {
        if (portCallRecord == null) {
            portCallRecord = new ArrayList<PortCallRecordType>();
        }
        return this.portCallRecord;
    }

    /**
     * Gets the value of the shipToShipActivityRecord property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipToShipActivityRecord property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipToShipActivityRecord().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipToShipActivityRecordType }
     * 
     * 
     */
    public List<ShipToShipActivityRecordType> getShipToShipActivityRecord() {
        if (shipToShipActivityRecord == null) {
            shipToShipActivityRecord = new ArrayList<ShipToShipActivityRecordType>();
        }
        return this.shipToShipActivityRecord;
    }

    /**
     * Obtient la valeur de la propriété reportLocation.
     * 
     * @return
     *     possible object is
     *     {@link LocationType }
     *     
     */
    public LocationType getReportLocation() {
        return reportLocation;
    }

    /**
     * Définit la valeur de la propriété reportLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationType }
     *     
     */
    public void setReportLocation(LocationType value) {
        this.reportLocation = value;
    }

    /**
     * Obtient la valeur de la propriété isscIssuerParty.
     * 
     * @return
     *     possible object is
     *     {@link PartyType }
     *     
     */
    public PartyType getISSCIssuerParty() {
        return isscIssuerParty;
    }

    /**
     * Définit la valeur de la propriété isscIssuerParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyType }
     *     
     */
    public void setISSCIssuerParty(PartyType value) {
        this.isscIssuerParty = value;
    }

    /**
     * Obtient la valeur de la propriété securityOfficerPerson.
     * 
     * @return
     *     possible object is
     *     {@link PersonType }
     *     
     */
    public PersonType getSecurityOfficerPerson() {
        return securityOfficerPerson;
    }

    /**
     * Définit la valeur de la propriété securityOfficerPerson.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonType }
     *     
     */
    public void setSecurityOfficerPerson(PersonType value) {
        this.securityOfficerPerson = value;
    }

}
