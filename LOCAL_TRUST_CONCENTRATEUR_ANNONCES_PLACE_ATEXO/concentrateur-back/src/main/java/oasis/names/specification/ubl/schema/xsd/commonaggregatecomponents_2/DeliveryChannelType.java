//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.NetworkIDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ParticipantIDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TestIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour DeliveryChannelType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="DeliveryChannelType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}NetworkID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ParticipantID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}TestIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}DigitalCertificate" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}DigitalMessageDelivery" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeliveryChannelType", propOrder = {
    "ublExtensions",
    "networkID",
    "participantID",
    "testIndicator",
    "digitalCertificate",
    "digitalMessageDelivery"
})
@ToString
@EqualsAndHashCode
public class DeliveryChannelType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "NetworkID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected NetworkIDType networkID;
    @XmlElement(name = "ParticipantID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ParticipantIDType participantID;
    @XmlElement(name = "TestIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected TestIndicatorType testIndicator;
    @XmlElement(name = "DigitalCertificate")
    protected CertificateType digitalCertificate;
    @XmlElement(name = "DigitalMessageDelivery")
    protected MessageDeliveryType digitalMessageDelivery;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété networkID.
     * 
     * @return
     *     possible object is
     *     {@link NetworkIDType }
     *     
     */
    public NetworkIDType getNetworkID() {
        return networkID;
    }

    /**
     * Définit la valeur de la propriété networkID.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkIDType }
     *     
     */
    public void setNetworkID(NetworkIDType value) {
        this.networkID = value;
    }

    /**
     * Obtient la valeur de la propriété participantID.
     * 
     * @return
     *     possible object is
     *     {@link ParticipantIDType }
     *     
     */
    public ParticipantIDType getParticipantID() {
        return participantID;
    }

    /**
     * Définit la valeur de la propriété participantID.
     * 
     * @param value
     *     allowed object is
     *     {@link ParticipantIDType }
     *     
     */
    public void setParticipantID(ParticipantIDType value) {
        this.participantID = value;
    }

    /**
     * Obtient la valeur de la propriété testIndicator.
     * 
     * @return
     *     possible object is
     *     {@link TestIndicatorType }
     *     
     */
    public TestIndicatorType getTestIndicator() {
        return testIndicator;
    }

    /**
     * Définit la valeur de la propriété testIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link TestIndicatorType }
     *     
     */
    public void setTestIndicator(TestIndicatorType value) {
        this.testIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété digitalCertificate.
     * 
     * @return
     *     possible object is
     *     {@link CertificateType }
     *     
     */
    public CertificateType getDigitalCertificate() {
        return digitalCertificate;
    }

    /**
     * Définit la valeur de la propriété digitalCertificate.
     * 
     * @param value
     *     allowed object is
     *     {@link CertificateType }
     *     
     */
    public void setDigitalCertificate(CertificateType value) {
        this.digitalCertificate = value;
    }

    /**
     * Obtient la valeur de la propriété digitalMessageDelivery.
     * 
     * @return
     *     possible object is
     *     {@link MessageDeliveryType }
     *     
     */
    public MessageDeliveryType getDigitalMessageDelivery() {
        return digitalMessageDelivery;
    }

    /**
     * Définit la valeur de la propriété digitalMessageDelivery.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageDeliveryType }
     *     
     */
    public void setDigitalMessageDelivery(MessageDeliveryType value) {
        this.digitalMessageDelivery = value;
    }

}
