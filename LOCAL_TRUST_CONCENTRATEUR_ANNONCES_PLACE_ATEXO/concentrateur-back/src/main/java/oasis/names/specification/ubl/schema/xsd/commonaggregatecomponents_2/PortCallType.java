//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.CargoAndBallastTankConditionDescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ExpectedAnchorageIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PlannedInspectionsDescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PlannedOperationsDescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PlannedWorksDescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PositionInPortIDType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour PortCallType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="PortCallType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}PlannedOperationsDescription" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}PlannedWorksDescription" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}PlannedInspectionsDescription" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ExpectedAnchorageIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}PositionInPortID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}CargoAndBallastTankConditionDescription" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}ShipRequirement" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}PrimaryPortCallPurpose" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}AdditionalPortCallPurpose" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}RequestedArrivalEvent" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PortCallType", propOrder = {
    "ublExtensions",
    "id",
    "plannedOperationsDescription",
    "plannedWorksDescription",
    "plannedInspectionsDescription",
    "expectedAnchorageIndicator",
    "positionInPortID",
    "cargoAndBallastTankConditionDescription",
    "shipRequirement",
    "primaryPortCallPurpose",
    "additionalPortCallPurpose",
    "requestedArrivalEvent"
})
@ToString
@EqualsAndHashCode
public class PortCallType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "ID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected IDType id;
    @XmlElement(name = "PlannedOperationsDescription", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<PlannedOperationsDescriptionType> plannedOperationsDescription;
    @XmlElement(name = "PlannedWorksDescription", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<PlannedWorksDescriptionType> plannedWorksDescription;
    @XmlElement(name = "PlannedInspectionsDescription", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<PlannedInspectionsDescriptionType> plannedInspectionsDescription;
    @XmlElement(name = "ExpectedAnchorageIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ExpectedAnchorageIndicatorType expectedAnchorageIndicator;
    @XmlElement(name = "PositionInPortID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected PositionInPortIDType positionInPortID;
    @XmlElement(name = "CargoAndBallastTankConditionDescription", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<CargoAndBallastTankConditionDescriptionType> cargoAndBallastTankConditionDescription;
    @XmlElement(name = "ShipRequirement")
    protected List<ShipRequirementType> shipRequirement;
    @XmlElement(name = "PrimaryPortCallPurpose")
    protected PortCallPurposeType primaryPortCallPurpose;
    @XmlElement(name = "AdditionalPortCallPurpose")
    protected List<PortCallPurposeType> additionalPortCallPurpose;
    @XmlElement(name = "RequestedArrivalEvent")
    protected EventType requestedArrivalEvent;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété id.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getID() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setID(IDType value) {
        this.id = value;
    }

    /**
     * Gets the value of the plannedOperationsDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the plannedOperationsDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPlannedOperationsDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PlannedOperationsDescriptionType }
     * 
     * 
     */
    public List<PlannedOperationsDescriptionType> getPlannedOperationsDescription() {
        if (plannedOperationsDescription == null) {
            plannedOperationsDescription = new ArrayList<PlannedOperationsDescriptionType>();
        }
        return this.plannedOperationsDescription;
    }

    /**
     * Gets the value of the plannedWorksDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the plannedWorksDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPlannedWorksDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PlannedWorksDescriptionType }
     * 
     * 
     */
    public List<PlannedWorksDescriptionType> getPlannedWorksDescription() {
        if (plannedWorksDescription == null) {
            plannedWorksDescription = new ArrayList<PlannedWorksDescriptionType>();
        }
        return this.plannedWorksDescription;
    }

    /**
     * Gets the value of the plannedInspectionsDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the plannedInspectionsDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPlannedInspectionsDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PlannedInspectionsDescriptionType }
     * 
     * 
     */
    public List<PlannedInspectionsDescriptionType> getPlannedInspectionsDescription() {
        if (plannedInspectionsDescription == null) {
            plannedInspectionsDescription = new ArrayList<PlannedInspectionsDescriptionType>();
        }
        return this.plannedInspectionsDescription;
    }

    /**
     * Obtient la valeur de la propriété expectedAnchorageIndicator.
     * 
     * @return
     *     possible object is
     *     {@link ExpectedAnchorageIndicatorType }
     *     
     */
    public ExpectedAnchorageIndicatorType getExpectedAnchorageIndicator() {
        return expectedAnchorageIndicator;
    }

    /**
     * Définit la valeur de la propriété expectedAnchorageIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpectedAnchorageIndicatorType }
     *     
     */
    public void setExpectedAnchorageIndicator(ExpectedAnchorageIndicatorType value) {
        this.expectedAnchorageIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété positionInPortID.
     * 
     * @return
     *     possible object is
     *     {@link PositionInPortIDType }
     *     
     */
    public PositionInPortIDType getPositionInPortID() {
        return positionInPortID;
    }

    /**
     * Définit la valeur de la propriété positionInPortID.
     * 
     * @param value
     *     allowed object is
     *     {@link PositionInPortIDType }
     *     
     */
    public void setPositionInPortID(PositionInPortIDType value) {
        this.positionInPortID = value;
    }

    /**
     * Gets the value of the cargoAndBallastTankConditionDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cargoAndBallastTankConditionDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCargoAndBallastTankConditionDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CargoAndBallastTankConditionDescriptionType }
     * 
     * 
     */
    public List<CargoAndBallastTankConditionDescriptionType> getCargoAndBallastTankConditionDescription() {
        if (cargoAndBallastTankConditionDescription == null) {
            cargoAndBallastTankConditionDescription = new ArrayList<CargoAndBallastTankConditionDescriptionType>();
        }
        return this.cargoAndBallastTankConditionDescription;
    }

    /**
     * Gets the value of the shipRequirement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipRequirement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipRequirement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipRequirementType }
     * 
     * 
     */
    public List<ShipRequirementType> getShipRequirement() {
        if (shipRequirement == null) {
            shipRequirement = new ArrayList<ShipRequirementType>();
        }
        return this.shipRequirement;
    }

    /**
     * Obtient la valeur de la propriété primaryPortCallPurpose.
     * 
     * @return
     *     possible object is
     *     {@link PortCallPurposeType }
     *     
     */
    public PortCallPurposeType getPrimaryPortCallPurpose() {
        return primaryPortCallPurpose;
    }

    /**
     * Définit la valeur de la propriété primaryPortCallPurpose.
     * 
     * @param value
     *     allowed object is
     *     {@link PortCallPurposeType }
     *     
     */
    public void setPrimaryPortCallPurpose(PortCallPurposeType value) {
        this.primaryPortCallPurpose = value;
    }

    /**
     * Gets the value of the additionalPortCallPurpose property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalPortCallPurpose property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalPortCallPurpose().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PortCallPurposeType }
     * 
     * 
     */
    public List<PortCallPurposeType> getAdditionalPortCallPurpose() {
        if (additionalPortCallPurpose == null) {
            additionalPortCallPurpose = new ArrayList<PortCallPurposeType>();
        }
        return this.additionalPortCallPurpose;
    }

    /**
     * Obtient la valeur de la propriété requestedArrivalEvent.
     * 
     * @return
     *     possible object is
     *     {@link EventType }
     *     
     */
    public EventType getRequestedArrivalEvent() {
        return requestedArrivalEvent;
    }

    /**
     * Définit la valeur de la propriété requestedArrivalEvent.
     * 
     * @param value
     *     allowed object is
     *     {@link EventType }
     *     
     */
    public void setRequestedArrivalEvent(EventType value) {
        this.requestedArrivalEvent = value;
    }

}
