//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.bdndr.schema.xsd.unqualifieddatatypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.AcquiringCPBIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.AwardingCPBIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.ContractFrameworkIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.DPSTerminationIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.ExtendedDurationIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.GroupLeadIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.ListedOnRegulatedMarketIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.NaturalPersonIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.PercentageKnownIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.ProcedureRelaunchIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.ProcurementDocumentsChangeIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.SecondStageIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.TenderRankedIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.TenderVariantIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.TermIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.ValueKnownIndicatorType;
import eu.europa.data.p27.eforms_ubl_extension_basic_components._1.WithdrawnAppealIndicatorType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.AcceptanceIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.AcceptedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.AdValoremIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.AnimalFoodApprovedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.AnimalFoodIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.AtAnchorageIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.AuctionConstraintIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.BackOrderAllowedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.BalanceBroughtForwardIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.BasedOnConsensusIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.BindingOnBuyerIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.BulkCargoIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.BuriedAtSeaIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.CabotageIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.CandidateReductionConstraintIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.CatalogueIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ChargeIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.CompletionIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ConsolidatableIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ContainerizedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.CopyIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.CustomsImportClassifiedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DangerousGoodsApprovedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DiedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ElectronicCatalogueUsageIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ElectronicInvoiceAcceptedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ElectronicOrderUsageIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ElectronicPaymentUsageIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.EvacuatedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ExpectedAnchorageIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ExpectedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.FollowupContractIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.FreeOfChargeIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.FridayAvailabilityIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.FrozenDocumentIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.FulfilmentIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.FullyPaidSharesIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.FumigatedCargoTransportIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.GeneralCargoIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.GovernmentAgreementConstraintIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.HazardousRiskIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.HumanFoodApprovedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.HumanFoodIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IMOGuidelinesOnBoardIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IndicationIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.InfectiousDiseaseCaseOnBoardIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.InitiatingPartyIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ItemUpdateRequestIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.LegalStatusIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.LivestockIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ManagementPlanImplementedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ManagementPlanOnBoardIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MarkAttentionIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MarkCareIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MedicalPractitionerConsultedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MondayAvailabilityIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MoreIllThanExpectedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.NoFurtherNegotiationIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.OnCarriageIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.OptionalLineItemIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.OrderableIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.OtherConditionsIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PartialDeliveryIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PowerIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PreCarriageIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PrepaidIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PricingUpdateRequestIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PrivatePartyIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PrizeIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PublicPartyIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PublishAwardIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.RecurringProcurementIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.RefrigeratedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.RefrigerationOnIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ReinspectionRequiredIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.RenewalsIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ReportedToMedicalOfficerIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.RequiredCurriculaIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ResponseIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ReturnabilityIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ReturnableMaterialIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SMESuitableIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SSPOnBoardIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SSPSecurityMeasuresAppliedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SanitaryMeasuresAppliedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SaturdayAvailabilityIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ServiceProviderPartyIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SickAnimalOnBoardIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SoleProprietorshipIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SpecialSecurityIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SplitConsignmentIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.StatusAvailableIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.StillIllIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.StillOnBoardIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.StowawaysFoundOnBoardIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SundayAvailabilityIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TaxEvidenceIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TaxIncludedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TerminatedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TestIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ThirdPartyPayerIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ThursdayAvailabilityIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ToOrderIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TuesdayAvailabilityIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.UnknownPriceIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ValidISSCIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ValidSanitationCertificateOnBoardIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.VariantConstraintIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.WednesdayAvailabilityIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.WithdrawOfferIndicatorType;


/**
 * 
 *         
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;ccts:UniqueID xmlns:ccts="urn:un:unece:uncefact:documentation:2" xmlns:ccts-cct="urn:un:unece:uncefact:data:specification:CoreComponentTypeSchemaModule:2" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;BDNDRUDT0000012&lt;/ccts:UniqueID&gt;
 * </pre>
 * 
 *         
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;ccts:CategoryCode xmlns:ccts="urn:un:unece:uncefact:documentation:2" xmlns:ccts-cct="urn:un:unece:uncefact:data:specification:CoreComponentTypeSchemaModule:2" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;UDT&lt;/ccts:CategoryCode&gt;
 * </pre>
 * 
 *         
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;ccts:DictionaryEntryName xmlns:ccts="urn:un:unece:uncefact:documentation:2" xmlns:ccts-cct="urn:un:unece:uncefact:data:specification:CoreComponentTypeSchemaModule:2" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;Indicator. Type&lt;/ccts:DictionaryEntryName&gt;
 * </pre>
 * 
 *         
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;ccts:VersionID xmlns:ccts="urn:un:unece:uncefact:documentation:2" xmlns:ccts-cct="urn:un:unece:uncefact:data:specification:CoreComponentTypeSchemaModule:2" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;1.0&lt;/ccts:VersionID&gt;
 * </pre>
 * 
 *         
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;ccts:Definition xmlns:ccts="urn:un:unece:uncefact:documentation:2" xmlns:ccts-cct="urn:un:unece:uncefact:data:specification:CoreComponentTypeSchemaModule:2" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;A list of two mutually exclusive Boolean values that express the only possible states of a property.&lt;/ccts:Definition&gt;
 * </pre>
 * 
 *         
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;ccts:RepresentationTermName xmlns:ccts="urn:un:unece:uncefact:documentation:2" xmlns:ccts-cct="urn:un:unece:uncefact:data:specification:CoreComponentTypeSchemaModule:2" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;Indicator&lt;/ccts:RepresentationTermName&gt;
 * </pre>
 * 
 *         
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;ccts:PrimitiveType xmlns:ccts="urn:un:unece:uncefact:documentation:2" xmlns:ccts-cct="urn:un:unece:uncefact:data:specification:CoreComponentTypeSchemaModule:2" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;string&lt;/ccts:PrimitiveType&gt;
 * </pre>
 * 
 *       
 * 
 * <p>Classe Java pour IndicatorType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="IndicatorType"&gt;
 *   &lt;simpleContent&gt;
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;boolean"&gt;
 *     &lt;/extension&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IndicatorType", propOrder = {
    "value"
})
@XmlSeeAlso({
    AcceptanceIndicatorType.class,
    AcceptedIndicatorType.class,
    AdValoremIndicatorType.class,
    AnimalFoodApprovedIndicatorType.class,
    AnimalFoodIndicatorType.class,
    AtAnchorageIndicatorType.class,
    AuctionConstraintIndicatorType.class,
    BackOrderAllowedIndicatorType.class,
    BalanceBroughtForwardIndicatorType.class,
    BasedOnConsensusIndicatorType.class,
    BindingOnBuyerIndicatorType.class,
    BulkCargoIndicatorType.class,
    BuriedAtSeaIndicatorType.class,
    CabotageIndicatorType.class,
    CandidateReductionConstraintIndicatorType.class,
    CatalogueIndicatorType.class,
    ChargeIndicatorType.class,
    CompletionIndicatorType.class,
    ConsolidatableIndicatorType.class,
    ContainerizedIndicatorType.class,
    CopyIndicatorType.class,
    CustomsImportClassifiedIndicatorType.class,
    DangerousGoodsApprovedIndicatorType.class,
    DiedIndicatorType.class,
    ElectronicCatalogueUsageIndicatorType.class,
    ElectronicInvoiceAcceptedIndicatorType.class,
    ElectronicOrderUsageIndicatorType.class,
    ElectronicPaymentUsageIndicatorType.class,
    EvacuatedIndicatorType.class,
    ExpectedAnchorageIndicatorType.class,
    ExpectedIndicatorType.class,
    FollowupContractIndicatorType.class,
    FreeOfChargeIndicatorType.class,
    FridayAvailabilityIndicatorType.class,
    FrozenDocumentIndicatorType.class,
    FulfilmentIndicatorType.class,
    FullyPaidSharesIndicatorType.class,
    FumigatedCargoTransportIndicatorType.class,
    GeneralCargoIndicatorType.class,
    GovernmentAgreementConstraintIndicatorType.class,
    HazardousRiskIndicatorType.class,
    HumanFoodApprovedIndicatorType.class,
    HumanFoodIndicatorType.class,
    IMOGuidelinesOnBoardIndicatorType.class,
    IndicationIndicatorType.class,
    InfectiousDiseaseCaseOnBoardIndicatorType.class,
    InitiatingPartyIndicatorType.class,
    ItemUpdateRequestIndicatorType.class,
    LegalStatusIndicatorType.class,
    LivestockIndicatorType.class,
    ManagementPlanImplementedIndicatorType.class,
    ManagementPlanOnBoardIndicatorType.class,
    MarkAttentionIndicatorType.class,
    MarkCareIndicatorType.class,
    MedicalPractitionerConsultedIndicatorType.class,
    MondayAvailabilityIndicatorType.class,
    MoreIllThanExpectedIndicatorType.class,
    NoFurtherNegotiationIndicatorType.class,
    OnCarriageIndicatorType.class,
    OptionalLineItemIndicatorType.class,
    OrderableIndicatorType.class,
    OtherConditionsIndicatorType.class,
    PartialDeliveryIndicatorType.class,
    PowerIndicatorType.class,
    PreCarriageIndicatorType.class,
    PrepaidIndicatorType.class,
    PricingUpdateRequestIndicatorType.class,
    PrivatePartyIndicatorType.class,
    PrizeIndicatorType.class,
    PublicPartyIndicatorType.class,
    PublishAwardIndicatorType.class,
    RecurringProcurementIndicatorType.class,
    RefrigeratedIndicatorType.class,
    RefrigerationOnIndicatorType.class,
    ReinspectionRequiredIndicatorType.class,
    RenewalsIndicatorType.class,
    ReportedToMedicalOfficerIndicatorType.class,
    RequiredCurriculaIndicatorType.class,
    ResponseIndicatorType.class,
    ReturnabilityIndicatorType.class,
    ReturnableMaterialIndicatorType.class,
    SMESuitableIndicatorType.class,
    SSPOnBoardIndicatorType.class,
    SSPSecurityMeasuresAppliedIndicatorType.class,
    SanitaryMeasuresAppliedIndicatorType.class,
    SaturdayAvailabilityIndicatorType.class,
    ServiceProviderPartyIndicatorType.class,
    SickAnimalOnBoardIndicatorType.class,
    SoleProprietorshipIndicatorType.class,
    SpecialSecurityIndicatorType.class,
    SplitConsignmentIndicatorType.class,
    StatusAvailableIndicatorType.class,
    StillIllIndicatorType.class,
    StillOnBoardIndicatorType.class,
    StowawaysFoundOnBoardIndicatorType.class,
    SundayAvailabilityIndicatorType.class,
    TaxEvidenceIndicatorType.class,
    TaxIncludedIndicatorType.class,
    TerminatedIndicatorType.class,
    TestIndicatorType.class,
    ThirdPartyPayerIndicatorType.class,
    ThursdayAvailabilityIndicatorType.class,
    ToOrderIndicatorType.class,
    TuesdayAvailabilityIndicatorType.class,
    UnknownPriceIndicatorType.class,
    ValidISSCIndicatorType.class,
    ValidSanitationCertificateOnBoardIndicatorType.class,
    VariantConstraintIndicatorType.class,
    WednesdayAvailabilityIndicatorType.class,
    WithdrawOfferIndicatorType.class,
    ProcedureRelaunchIndicatorType.class,
    AcquiringCPBIndicatorType.class,
    AwardingCPBIndicatorType.class,
    ContractFrameworkIndicatorType.class,
    DPSTerminationIndicatorType.class,
    ExtendedDurationIndicatorType.class,
    GroupLeadIndicatorType.class,
    ListedOnRegulatedMarketIndicatorType.class,
    NaturalPersonIndicatorType.class,
    PercentageKnownIndicatorType.class,
    ProcurementDocumentsChangeIndicatorType.class,
    SecondStageIndicatorType.class,
    TenderRankedIndicatorType.class,
    TenderVariantIndicatorType.class,
    TermIndicatorType.class,
    ValueKnownIndicatorType.class,
    WithdrawnAppealIndicatorType.class
})
@ToString
@EqualsAndHashCode
public class IndicatorType {

    @XmlValue
    protected boolean value;

    /**
     * Obtient la valeur de la propriété value.
     * 
     */
    public boolean isValue() {
        return value;
    }

    /**
     * Définit la valeur de la propriété value.
     * 
     */
    public void setValue(boolean value) {
        this.value = value;
    }

}
