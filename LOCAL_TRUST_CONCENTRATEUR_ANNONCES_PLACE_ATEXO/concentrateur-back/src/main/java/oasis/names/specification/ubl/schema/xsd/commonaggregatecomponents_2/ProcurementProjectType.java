//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.EstimatedOverallContractQuantityType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.FeeDescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.NameType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.NoteType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ProcurementSubTypeCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ProcurementTypeCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.QualityControlCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.RequestedDeliveryDateType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.RequiredFeeAmountType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SMESuitableIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour ProcurementProjectType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ProcurementProjectType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}Name" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}Description" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ProcurementTypeCode" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ProcurementSubTypeCode" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}QualityControlCode" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}RequiredFeeAmount" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}FeeDescription" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}RequestedDeliveryDate" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}EstimatedOverallContractQuantity" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}Note" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}SMESuitableIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}ProcurementAdditionalType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}RequestedTenderTotal" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}MainCommodityClassification" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}AdditionalCommodityClassification" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}RealizedLocation" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}PlannedPeriod" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}ContractExtension" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}RequestForTenderLine" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcurementProjectType", propOrder = {
    "ublExtensions",
    "id",
    "name",
    "description",
    "procurementTypeCode",
    "procurementSubTypeCode",
    "qualityControlCode",
    "requiredFeeAmount",
    "feeDescription",
    "requestedDeliveryDate",
    "estimatedOverallContractQuantity",
    "note",
    "smeSuitableIndicator",
    "procurementAdditionalType",
    "requestedTenderTotal",
    "mainCommodityClassification",
    "additionalCommodityClassification",
    "realizedLocation",
    "plannedPeriod",
    "contractExtension",
    "requestForTenderLine"
})
@ToString
@EqualsAndHashCode
public class ProcurementProjectType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "ID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected IDType id;
    @XmlElement(name = "Name", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<NameType> name;
    @XmlElement(name = "Description", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<DescriptionType> description;
    @XmlElement(name = "ProcurementTypeCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ProcurementTypeCodeType procurementTypeCode;
    @XmlElement(name = "ProcurementSubTypeCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ProcurementSubTypeCodeType procurementSubTypeCode;
    @XmlElement(name = "QualityControlCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected QualityControlCodeType qualityControlCode;
    @XmlElement(name = "RequiredFeeAmount", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected RequiredFeeAmountType requiredFeeAmount;
    @XmlElement(name = "FeeDescription", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<FeeDescriptionType> feeDescription;
    @XmlElement(name = "RequestedDeliveryDate", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected RequestedDeliveryDateType requestedDeliveryDate;
    @XmlElement(name = "EstimatedOverallContractQuantity", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected EstimatedOverallContractQuantityType estimatedOverallContractQuantity;
    @XmlElement(name = "Note", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<NoteType> note;
    @XmlElement(name = "SMESuitableIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected SMESuitableIndicatorType smeSuitableIndicator;
    @XmlElement(name = "ProcurementAdditionalType")
    protected List<ProcurementAdditionalTypeType> procurementAdditionalType;
    @XmlElement(name = "RequestedTenderTotal")
    protected RequestedTenderTotalType requestedTenderTotal;
    @XmlElement(name = "MainCommodityClassification")
    protected List<CommodityClassificationType> mainCommodityClassification;
    @XmlElement(name = "AdditionalCommodityClassification")
    protected List<CommodityClassificationType> additionalCommodityClassification;
    @XmlElement(name = "RealizedLocation")
    protected List<LocationType> realizedLocation;
    @XmlElement(name = "PlannedPeriod")
    protected PeriodType plannedPeriod;
    @XmlElement(name = "ContractExtension")
    protected ContractExtensionType contractExtension;
    @XmlElement(name = "RequestForTenderLine")
    protected List<RequestForTenderLineType> requestForTenderLine;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété id.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getID() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setID(IDType value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the name property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NameType }
     * 
     * 
     */
    public List<NameType> getName() {
        if (name == null) {
            name = new ArrayList<NameType>();
        }
        return this.name;
    }

    /**
     * Gets the value of the description property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the description property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DescriptionType }
     * 
     * 
     */
    public List<DescriptionType> getDescription() {
        if (description == null) {
            description = new ArrayList<DescriptionType>();
        }
        return this.description;
    }

    /**
     * Obtient la valeur de la propriété procurementTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link ProcurementTypeCodeType }
     *     
     */
    public ProcurementTypeCodeType getProcurementTypeCode() {
        return procurementTypeCode;
    }

    /**
     * Définit la valeur de la propriété procurementTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcurementTypeCodeType }
     *     
     */
    public void setProcurementTypeCode(ProcurementTypeCodeType value) {
        this.procurementTypeCode = value;
    }

    /**
     * Obtient la valeur de la propriété procurementSubTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link ProcurementSubTypeCodeType }
     *     
     */
    public ProcurementSubTypeCodeType getProcurementSubTypeCode() {
        return procurementSubTypeCode;
    }

    /**
     * Définit la valeur de la propriété procurementSubTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcurementSubTypeCodeType }
     *     
     */
    public void setProcurementSubTypeCode(ProcurementSubTypeCodeType value) {
        this.procurementSubTypeCode = value;
    }

    /**
     * Obtient la valeur de la propriété qualityControlCode.
     * 
     * @return
     *     possible object is
     *     {@link QualityControlCodeType }
     *     
     */
    public QualityControlCodeType getQualityControlCode() {
        return qualityControlCode;
    }

    /**
     * Définit la valeur de la propriété qualityControlCode.
     * 
     * @param value
     *     allowed object is
     *     {@link QualityControlCodeType }
     *     
     */
    public void setQualityControlCode(QualityControlCodeType value) {
        this.qualityControlCode = value;
    }

    /**
     * Obtient la valeur de la propriété requiredFeeAmount.
     * 
     * @return
     *     possible object is
     *     {@link RequiredFeeAmountType }
     *     
     */
    public RequiredFeeAmountType getRequiredFeeAmount() {
        return requiredFeeAmount;
    }

    /**
     * Définit la valeur de la propriété requiredFeeAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link RequiredFeeAmountType }
     *     
     */
    public void setRequiredFeeAmount(RequiredFeeAmountType value) {
        this.requiredFeeAmount = value;
    }

    /**
     * Gets the value of the feeDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the feeDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeeDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FeeDescriptionType }
     * 
     * 
     */
    public List<FeeDescriptionType> getFeeDescription() {
        if (feeDescription == null) {
            feeDescription = new ArrayList<FeeDescriptionType>();
        }
        return this.feeDescription;
    }

    /**
     * Obtient la valeur de la propriété requestedDeliveryDate.
     * 
     * @return
     *     possible object is
     *     {@link RequestedDeliveryDateType }
     *     
     */
    public RequestedDeliveryDateType getRequestedDeliveryDate() {
        return requestedDeliveryDate;
    }

    /**
     * Définit la valeur de la propriété requestedDeliveryDate.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestedDeliveryDateType }
     *     
     */
    public void setRequestedDeliveryDate(RequestedDeliveryDateType value) {
        this.requestedDeliveryDate = value;
    }

    /**
     * Obtient la valeur de la propriété estimatedOverallContractQuantity.
     * 
     * @return
     *     possible object is
     *     {@link EstimatedOverallContractQuantityType }
     *     
     */
    public EstimatedOverallContractQuantityType getEstimatedOverallContractQuantity() {
        return estimatedOverallContractQuantity;
    }

    /**
     * Définit la valeur de la propriété estimatedOverallContractQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link EstimatedOverallContractQuantityType }
     *     
     */
    public void setEstimatedOverallContractQuantity(EstimatedOverallContractQuantityType value) {
        this.estimatedOverallContractQuantity = value;
    }

    /**
     * Gets the value of the note property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the note property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNote().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NoteType }
     * 
     * 
     */
    public List<NoteType> getNote() {
        if (note == null) {
            note = new ArrayList<NoteType>();
        }
        return this.note;
    }

    /**
     * Obtient la valeur de la propriété smeSuitableIndicator.
     * 
     * @return
     *     possible object is
     *     {@link SMESuitableIndicatorType }
     *     
     */
    public SMESuitableIndicatorType getSMESuitableIndicator() {
        return smeSuitableIndicator;
    }

    /**
     * Définit la valeur de la propriété smeSuitableIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link SMESuitableIndicatorType }
     *     
     */
    public void setSMESuitableIndicator(SMESuitableIndicatorType value) {
        this.smeSuitableIndicator = value;
    }

    /**
     * Gets the value of the procurementAdditionalType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the procurementAdditionalType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProcurementAdditionalType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProcurementAdditionalTypeType }
     * 
     * 
     */
    public List<ProcurementAdditionalTypeType> getProcurementAdditionalType() {
        if (procurementAdditionalType == null) {
            procurementAdditionalType = new ArrayList<ProcurementAdditionalTypeType>();
        }
        return this.procurementAdditionalType;
    }

    /**
     * Obtient la valeur de la propriété requestedTenderTotal.
     * 
     * @return
     *     possible object is
     *     {@link RequestedTenderTotalType }
     *     
     */
    public RequestedTenderTotalType getRequestedTenderTotal() {
        return requestedTenderTotal;
    }

    /**
     * Définit la valeur de la propriété requestedTenderTotal.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestedTenderTotalType }
     *     
     */
    public void setRequestedTenderTotal(RequestedTenderTotalType value) {
        this.requestedTenderTotal = value;
    }

    /**
     * Gets the value of the mainCommodityClassification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mainCommodityClassification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMainCommodityClassification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommodityClassificationType }
     * 
     * 
     */
    public List<CommodityClassificationType> getMainCommodityClassification() {
        if (mainCommodityClassification == null) {
            mainCommodityClassification = new ArrayList<CommodityClassificationType>();
        }
        return this.mainCommodityClassification;
    }

    /**
     * Gets the value of the additionalCommodityClassification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalCommodityClassification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalCommodityClassification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommodityClassificationType }
     * 
     * 
     */
    public List<CommodityClassificationType> getAdditionalCommodityClassification() {
        if (additionalCommodityClassification == null) {
            additionalCommodityClassification = new ArrayList<CommodityClassificationType>();
        }
        return this.additionalCommodityClassification;
    }

    /**
     * Gets the value of the realizedLocation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the realizedLocation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRealizedLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationType }
     * 
     * 
     */
    public List<LocationType> getRealizedLocation() {
        if (realizedLocation == null) {
            realizedLocation = new ArrayList<LocationType>();
        }
        return this.realizedLocation;
    }

    /**
     * Obtient la valeur de la propriété plannedPeriod.
     * 
     * @return
     *     possible object is
     *     {@link PeriodType }
     *     
     */
    public PeriodType getPlannedPeriod() {
        return plannedPeriod;
    }

    /**
     * Définit la valeur de la propriété plannedPeriod.
     * 
     * @param value
     *     allowed object is
     *     {@link PeriodType }
     *     
     */
    public void setPlannedPeriod(PeriodType value) {
        this.plannedPeriod = value;
    }

    /**
     * Obtient la valeur de la propriété contractExtension.
     * 
     * @return
     *     possible object is
     *     {@link ContractExtensionType }
     *     
     */
    public ContractExtensionType getContractExtension() {
        return contractExtension;
    }

    /**
     * Définit la valeur de la propriété contractExtension.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractExtensionType }
     *     
     */
    public void setContractExtension(ContractExtensionType value) {
        this.contractExtension = value;
    }

    /**
     * Gets the value of the requestForTenderLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the requestForTenderLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRequestForTenderLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RequestForTenderLineType }
     * 
     * 
     */
    public List<RequestForTenderLineType> getRequestForTenderLine() {
        if (requestForTenderLine == null) {
            requestForTenderLine = new ArrayList<RequestForTenderLineType>();
        }
        return this.requestForTenderLine;
    }

}
