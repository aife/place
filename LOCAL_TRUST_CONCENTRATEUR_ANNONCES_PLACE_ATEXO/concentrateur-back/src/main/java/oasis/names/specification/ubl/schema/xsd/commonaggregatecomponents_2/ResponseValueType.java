//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ResponseAmountType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ResponseBinaryObjectType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ResponseCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ResponseDateType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ResponseIDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ResponseIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ResponseMeasureType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ResponseNumericType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ResponseQuantityType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ResponseTimeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ResponseType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ResponseURIType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour ResponseValueType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ResponseValueType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}Description" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}Response" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ResponseAmount" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ResponseBinaryObject" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ResponseCode" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ResponseDate" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ResponseID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ResponseIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ResponseMeasure" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ResponseNumeric" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ResponseQuantity" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ResponseTime" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ResponseURI" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseValueType", propOrder = {
    "ublExtensions",
    "id",
    "description",
    "response",
    "responseAmount",
    "responseBinaryObject",
    "responseCode",
    "responseDate",
    "responseID",
    "responseIndicator",
    "responseMeasure",
    "responseNumeric",
    "responseQuantity",
    "responseTime",
    "responseURI"
})
@ToString
@EqualsAndHashCode
public class ResponseValueType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "ID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected IDType id;
    @XmlElement(name = "Description", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<DescriptionType> description;
    @XmlElement(name = "Response", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<ResponseType> response;
    @XmlElement(name = "ResponseAmount", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ResponseAmountType responseAmount;
    @XmlElement(name = "ResponseBinaryObject", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ResponseBinaryObjectType responseBinaryObject;
    @XmlElement(name = "ResponseCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ResponseCodeType responseCode;
    @XmlElement(name = "ResponseDate", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ResponseDateType responseDate;
    @XmlElement(name = "ResponseID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ResponseIDType responseID;
    @XmlElement(name = "ResponseIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ResponseIndicatorType responseIndicator;
    @XmlElement(name = "ResponseMeasure", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ResponseMeasureType responseMeasure;
    @XmlElement(name = "ResponseNumeric", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ResponseNumericType responseNumeric;
    @XmlElement(name = "ResponseQuantity", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ResponseQuantityType responseQuantity;
    @XmlElement(name = "ResponseTime", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ResponseTimeType responseTime;
    @XmlElement(name = "ResponseURI", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ResponseURIType responseURI;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété id.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getID() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setID(IDType value) {
        this.id = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the description property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DescriptionType }
     * 
     * 
     */
    public List<DescriptionType> getDescription() {
        if (description == null) {
            description = new ArrayList<DescriptionType>();
        }
        return this.description;
    }

    /**
     * Gets the value of the response property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the response property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResponse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResponseType }
     * 
     * 
     */
    public List<ResponseType> getResponse() {
        if (response == null) {
            response = new ArrayList<ResponseType>();
        }
        return this.response;
    }

    /**
     * Obtient la valeur de la propriété responseAmount.
     * 
     * @return
     *     possible object is
     *     {@link ResponseAmountType }
     *     
     */
    public ResponseAmountType getResponseAmount() {
        return responseAmount;
    }

    /**
     * Définit la valeur de la propriété responseAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseAmountType }
     *     
     */
    public void setResponseAmount(ResponseAmountType value) {
        this.responseAmount = value;
    }

    /**
     * Obtient la valeur de la propriété responseBinaryObject.
     * 
     * @return
     *     possible object is
     *     {@link ResponseBinaryObjectType }
     *     
     */
    public ResponseBinaryObjectType getResponseBinaryObject() {
        return responseBinaryObject;
    }

    /**
     * Définit la valeur de la propriété responseBinaryObject.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseBinaryObjectType }
     *     
     */
    public void setResponseBinaryObject(ResponseBinaryObjectType value) {
        this.responseBinaryObject = value;
    }

    /**
     * Obtient la valeur de la propriété responseCode.
     * 
     * @return
     *     possible object is
     *     {@link ResponseCodeType }
     *     
     */
    public ResponseCodeType getResponseCode() {
        return responseCode;
    }

    /**
     * Définit la valeur de la propriété responseCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseCodeType }
     *     
     */
    public void setResponseCode(ResponseCodeType value) {
        this.responseCode = value;
    }

    /**
     * Obtient la valeur de la propriété responseDate.
     * 
     * @return
     *     possible object is
     *     {@link ResponseDateType }
     *     
     */
    public ResponseDateType getResponseDate() {
        return responseDate;
    }

    /**
     * Définit la valeur de la propriété responseDate.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseDateType }
     *     
     */
    public void setResponseDate(ResponseDateType value) {
        this.responseDate = value;
    }

    /**
     * Obtient la valeur de la propriété responseID.
     * 
     * @return
     *     possible object is
     *     {@link ResponseIDType }
     *     
     */
    public ResponseIDType getResponseID() {
        return responseID;
    }

    /**
     * Définit la valeur de la propriété responseID.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseIDType }
     *     
     */
    public void setResponseID(ResponseIDType value) {
        this.responseID = value;
    }

    /**
     * Obtient la valeur de la propriété responseIndicator.
     * 
     * @return
     *     possible object is
     *     {@link ResponseIndicatorType }
     *     
     */
    public ResponseIndicatorType getResponseIndicator() {
        return responseIndicator;
    }

    /**
     * Définit la valeur de la propriété responseIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseIndicatorType }
     *     
     */
    public void setResponseIndicator(ResponseIndicatorType value) {
        this.responseIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété responseMeasure.
     * 
     * @return
     *     possible object is
     *     {@link ResponseMeasureType }
     *     
     */
    public ResponseMeasureType getResponseMeasure() {
        return responseMeasure;
    }

    /**
     * Définit la valeur de la propriété responseMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseMeasureType }
     *     
     */
    public void setResponseMeasure(ResponseMeasureType value) {
        this.responseMeasure = value;
    }

    /**
     * Obtient la valeur de la propriété responseNumeric.
     * 
     * @return
     *     possible object is
     *     {@link ResponseNumericType }
     *     
     */
    public ResponseNumericType getResponseNumeric() {
        return responseNumeric;
    }

    /**
     * Définit la valeur de la propriété responseNumeric.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseNumericType }
     *     
     */
    public void setResponseNumeric(ResponseNumericType value) {
        this.responseNumeric = value;
    }

    /**
     * Obtient la valeur de la propriété responseQuantity.
     * 
     * @return
     *     possible object is
     *     {@link ResponseQuantityType }
     *     
     */
    public ResponseQuantityType getResponseQuantity() {
        return responseQuantity;
    }

    /**
     * Définit la valeur de la propriété responseQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseQuantityType }
     *     
     */
    public void setResponseQuantity(ResponseQuantityType value) {
        this.responseQuantity = value;
    }

    /**
     * Obtient la valeur de la propriété responseTime.
     * 
     * @return
     *     possible object is
     *     {@link ResponseTimeType }
     *     
     */
    public ResponseTimeType getResponseTime() {
        return responseTime;
    }

    /**
     * Définit la valeur de la propriété responseTime.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseTimeType }
     *     
     */
    public void setResponseTime(ResponseTimeType value) {
        this.responseTime = value;
    }

    /**
     * Obtient la valeur de la propriété responseURI.
     * 
     * @return
     *     possible object is
     *     {@link ResponseURIType }
     *     
     */
    public ResponseURIType getResponseURI() {
        return responseURI;
    }

    /**
     * Définit la valeur de la propriété responseURI.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseURIType }
     *     
     */
    public void setResponseURI(ResponseURIType value) {
        this.responseURI = value;
    }

}
