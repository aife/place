//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.GroupingLotsType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MaximumLotsAwardedNumericType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MaximumLotsSubmittedNumericType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour LotDistributionType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="LotDistributionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}MaximumLotsAwardedNumeric" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}MaximumLotsSubmittedNumeric" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}GroupingLots" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}LotsGroup" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LotDistributionType", propOrder = {
    "ublExtensions",
    "maximumLotsAwardedNumeric",
    "maximumLotsSubmittedNumeric",
    "groupingLots",
    "lotsGroup"
})
@ToString
@EqualsAndHashCode
public class LotDistributionType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "MaximumLotsAwardedNumeric", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected MaximumLotsAwardedNumericType maximumLotsAwardedNumeric;
    @XmlElement(name = "MaximumLotsSubmittedNumeric", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected MaximumLotsSubmittedNumericType maximumLotsSubmittedNumeric;
    @XmlElement(name = "GroupingLots", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<GroupingLotsType> groupingLots;
    @XmlElement(name = "LotsGroup")
    protected List<LotsGroupType> lotsGroup;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété maximumLotsAwardedNumeric.
     * 
     * @return
     *     possible object is
     *     {@link MaximumLotsAwardedNumericType }
     *     
     */
    public MaximumLotsAwardedNumericType getMaximumLotsAwardedNumeric() {
        return maximumLotsAwardedNumeric;
    }

    /**
     * Définit la valeur de la propriété maximumLotsAwardedNumeric.
     * 
     * @param value
     *     allowed object is
     *     {@link MaximumLotsAwardedNumericType }
     *     
     */
    public void setMaximumLotsAwardedNumeric(MaximumLotsAwardedNumericType value) {
        this.maximumLotsAwardedNumeric = value;
    }

    /**
     * Obtient la valeur de la propriété maximumLotsSubmittedNumeric.
     * 
     * @return
     *     possible object is
     *     {@link MaximumLotsSubmittedNumericType }
     *     
     */
    public MaximumLotsSubmittedNumericType getMaximumLotsSubmittedNumeric() {
        return maximumLotsSubmittedNumeric;
    }

    /**
     * Définit la valeur de la propriété maximumLotsSubmittedNumeric.
     * 
     * @param value
     *     allowed object is
     *     {@link MaximumLotsSubmittedNumericType }
     *     
     */
    public void setMaximumLotsSubmittedNumeric(MaximumLotsSubmittedNumericType value) {
        this.maximumLotsSubmittedNumeric = value;
    }

    /**
     * Gets the value of the groupingLots property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the groupingLots property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroupingLots().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GroupingLotsType }
     * 
     * 
     */
    public List<GroupingLotsType> getGroupingLots() {
        if (groupingLots == null) {
            groupingLots = new ArrayList<GroupingLotsType>();
        }
        return this.groupingLots;
    }

    /**
     * Gets the value of the lotsGroup property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lotsGroup property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLotsGroup().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LotsGroupType }
     * 
     * 
     */
    public List<LotsGroupType> getLotsGroup() {
        if (lotsGroup == null) {
            lotsGroup = new ArrayList<LotsGroupType>();
        }
        return this.lotsGroup;
    }

}
