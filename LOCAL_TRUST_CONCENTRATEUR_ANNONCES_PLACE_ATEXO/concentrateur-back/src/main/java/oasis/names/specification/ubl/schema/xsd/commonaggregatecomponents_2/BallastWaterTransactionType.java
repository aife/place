//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ExchangeMethodCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ExchangedPercentType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SalinityMeasureType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SeaHeightMeasureType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TankIDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TankTypeCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TransactionDateType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.VolumeMeasureType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour BallastWaterTransactionType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="BallastWaterTransactionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}TankID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}TankTypeCode" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ExchangeMethodCode" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ExchangedPercent" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}VolumeMeasure" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}SeaHeightMeasure" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}SalinityMeasure" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}TransactionDate" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}Location" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}BallastWaterTemperature" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BallastWaterTransactionType", propOrder = {
    "ublExtensions",
    "tankID",
    "tankTypeCode",
    "exchangeMethodCode",
    "exchangedPercent",
    "volumeMeasure",
    "seaHeightMeasure",
    "salinityMeasure",
    "transactionDate",
    "location",
    "ballastWaterTemperature"
})
@ToString
@EqualsAndHashCode
public class BallastWaterTransactionType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "TankID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected TankIDType tankID;
    @XmlElement(name = "TankTypeCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected TankTypeCodeType tankTypeCode;
    @XmlElement(name = "ExchangeMethodCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ExchangeMethodCodeType exchangeMethodCode;
    @XmlElement(name = "ExchangedPercent", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ExchangedPercentType exchangedPercent;
    @XmlElement(name = "VolumeMeasure", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected VolumeMeasureType volumeMeasure;
    @XmlElement(name = "SeaHeightMeasure", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected SeaHeightMeasureType seaHeightMeasure;
    @XmlElement(name = "SalinityMeasure", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected SalinityMeasureType salinityMeasure;
    @XmlElement(name = "TransactionDate", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected TransactionDateType transactionDate;
    @XmlElement(name = "Location")
    protected LocationType location;
    @XmlElement(name = "BallastWaterTemperature")
    protected TemperatureType ballastWaterTemperature;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété tankID.
     * 
     * @return
     *     possible object is
     *     {@link TankIDType }
     *     
     */
    public TankIDType getTankID() {
        return tankID;
    }

    /**
     * Définit la valeur de la propriété tankID.
     * 
     * @param value
     *     allowed object is
     *     {@link TankIDType }
     *     
     */
    public void setTankID(TankIDType value) {
        this.tankID = value;
    }

    /**
     * Obtient la valeur de la propriété tankTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link TankTypeCodeType }
     *     
     */
    public TankTypeCodeType getTankTypeCode() {
        return tankTypeCode;
    }

    /**
     * Définit la valeur de la propriété tankTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TankTypeCodeType }
     *     
     */
    public void setTankTypeCode(TankTypeCodeType value) {
        this.tankTypeCode = value;
    }

    /**
     * Obtient la valeur de la propriété exchangeMethodCode.
     * 
     * @return
     *     possible object is
     *     {@link ExchangeMethodCodeType }
     *     
     */
    public ExchangeMethodCodeType getExchangeMethodCode() {
        return exchangeMethodCode;
    }

    /**
     * Définit la valeur de la propriété exchangeMethodCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangeMethodCodeType }
     *     
     */
    public void setExchangeMethodCode(ExchangeMethodCodeType value) {
        this.exchangeMethodCode = value;
    }

    /**
     * Obtient la valeur de la propriété exchangedPercent.
     * 
     * @return
     *     possible object is
     *     {@link ExchangedPercentType }
     *     
     */
    public ExchangedPercentType getExchangedPercent() {
        return exchangedPercent;
    }

    /**
     * Définit la valeur de la propriété exchangedPercent.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangedPercentType }
     *     
     */
    public void setExchangedPercent(ExchangedPercentType value) {
        this.exchangedPercent = value;
    }

    /**
     * Obtient la valeur de la propriété volumeMeasure.
     * 
     * @return
     *     possible object is
     *     {@link VolumeMeasureType }
     *     
     */
    public VolumeMeasureType getVolumeMeasure() {
        return volumeMeasure;
    }

    /**
     * Définit la valeur de la propriété volumeMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link VolumeMeasureType }
     *     
     */
    public void setVolumeMeasure(VolumeMeasureType value) {
        this.volumeMeasure = value;
    }

    /**
     * Obtient la valeur de la propriété seaHeightMeasure.
     * 
     * @return
     *     possible object is
     *     {@link SeaHeightMeasureType }
     *     
     */
    public SeaHeightMeasureType getSeaHeightMeasure() {
        return seaHeightMeasure;
    }

    /**
     * Définit la valeur de la propriété seaHeightMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link SeaHeightMeasureType }
     *     
     */
    public void setSeaHeightMeasure(SeaHeightMeasureType value) {
        this.seaHeightMeasure = value;
    }

    /**
     * Obtient la valeur de la propriété salinityMeasure.
     * 
     * @return
     *     possible object is
     *     {@link SalinityMeasureType }
     *     
     */
    public SalinityMeasureType getSalinityMeasure() {
        return salinityMeasure;
    }

    /**
     * Définit la valeur de la propriété salinityMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link SalinityMeasureType }
     *     
     */
    public void setSalinityMeasure(SalinityMeasureType value) {
        this.salinityMeasure = value;
    }

    /**
     * Obtient la valeur de la propriété transactionDate.
     * 
     * @return
     *     possible object is
     *     {@link TransactionDateType }
     *     
     */
    public TransactionDateType getTransactionDate() {
        return transactionDate;
    }

    /**
     * Définit la valeur de la propriété transactionDate.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionDateType }
     *     
     */
    public void setTransactionDate(TransactionDateType value) {
        this.transactionDate = value;
    }

    /**
     * Obtient la valeur de la propriété location.
     * 
     * @return
     *     possible object is
     *     {@link LocationType }
     *     
     */
    public LocationType getLocation() {
        return location;
    }

    /**
     * Définit la valeur de la propriété location.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationType }
     *     
     */
    public void setLocation(LocationType value) {
        this.location = value;
    }

    /**
     * Obtient la valeur de la propriété ballastWaterTemperature.
     * 
     * @return
     *     possible object is
     *     {@link TemperatureType }
     *     
     */
    public TemperatureType getBallastWaterTemperature() {
        return ballastWaterTemperature;
    }

    /**
     * Définit la valeur de la propriété ballastWaterTemperature.
     * 
     * @param value
     *     allowed object is
     *     {@link TemperatureType }
     *     
     */
    public void setBallastWaterTemperature(TemperatureType value) {
        this.ballastWaterTemperature = value;
    }

}
