//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.EstimatedGeneratedUntilNextPortMeasureType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MaxDedicatedStorageCapacityMeasureType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.RetainedOnBoardMeasureType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ToBeDeliveredMeasureType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.WasteTypeCodeType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour MaritimeWasteType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="MaritimeWasteType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}Description" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}WasteTypeCode" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ToBeDeliveredMeasure" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}RetainedOnBoardMeasure" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}MaxDedicatedStorageCapacityMeasure" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}EstimatedGeneratedUntilNextPortMeasure" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}RemainingWasteDeliveryPortLocation" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MaritimeWasteType", propOrder = {
    "ublExtensions",
    "id",
    "description",
    "wasteTypeCode",
    "toBeDeliveredMeasure",
    "retainedOnBoardMeasure",
    "maxDedicatedStorageCapacityMeasure",
    "estimatedGeneratedUntilNextPortMeasure",
    "remainingWasteDeliveryPortLocation"
})
@ToString
@EqualsAndHashCode
public class MaritimeWasteType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "ID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected IDType id;
    @XmlElement(name = "Description", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<DescriptionType> description;
    @XmlElement(name = "WasteTypeCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected WasteTypeCodeType wasteTypeCode;
    @XmlElement(name = "ToBeDeliveredMeasure", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ToBeDeliveredMeasureType toBeDeliveredMeasure;
    @XmlElement(name = "RetainedOnBoardMeasure", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected RetainedOnBoardMeasureType retainedOnBoardMeasure;
    @XmlElement(name = "MaxDedicatedStorageCapacityMeasure", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected MaxDedicatedStorageCapacityMeasureType maxDedicatedStorageCapacityMeasure;
    @XmlElement(name = "EstimatedGeneratedUntilNextPortMeasure", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected EstimatedGeneratedUntilNextPortMeasureType estimatedGeneratedUntilNextPortMeasure;
    @XmlElement(name = "RemainingWasteDeliveryPortLocation")
    protected List<LocationType> remainingWasteDeliveryPortLocation;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété id.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getID() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setID(IDType value) {
        this.id = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the description property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DescriptionType }
     * 
     * 
     */
    public List<DescriptionType> getDescription() {
        if (description == null) {
            description = new ArrayList<DescriptionType>();
        }
        return this.description;
    }

    /**
     * Obtient la valeur de la propriété wasteTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link WasteTypeCodeType }
     *     
     */
    public WasteTypeCodeType getWasteTypeCode() {
        return wasteTypeCode;
    }

    /**
     * Définit la valeur de la propriété wasteTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link WasteTypeCodeType }
     *     
     */
    public void setWasteTypeCode(WasteTypeCodeType value) {
        this.wasteTypeCode = value;
    }

    /**
     * Obtient la valeur de la propriété toBeDeliveredMeasure.
     * 
     * @return
     *     possible object is
     *     {@link ToBeDeliveredMeasureType }
     *     
     */
    public ToBeDeliveredMeasureType getToBeDeliveredMeasure() {
        return toBeDeliveredMeasure;
    }

    /**
     * Définit la valeur de la propriété toBeDeliveredMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link ToBeDeliveredMeasureType }
     *     
     */
    public void setToBeDeliveredMeasure(ToBeDeliveredMeasureType value) {
        this.toBeDeliveredMeasure = value;
    }

    /**
     * Obtient la valeur de la propriété retainedOnBoardMeasure.
     * 
     * @return
     *     possible object is
     *     {@link RetainedOnBoardMeasureType }
     *     
     */
    public RetainedOnBoardMeasureType getRetainedOnBoardMeasure() {
        return retainedOnBoardMeasure;
    }

    /**
     * Définit la valeur de la propriété retainedOnBoardMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link RetainedOnBoardMeasureType }
     *     
     */
    public void setRetainedOnBoardMeasure(RetainedOnBoardMeasureType value) {
        this.retainedOnBoardMeasure = value;
    }

    /**
     * Obtient la valeur de la propriété maxDedicatedStorageCapacityMeasure.
     * 
     * @return
     *     possible object is
     *     {@link MaxDedicatedStorageCapacityMeasureType }
     *     
     */
    public MaxDedicatedStorageCapacityMeasureType getMaxDedicatedStorageCapacityMeasure() {
        return maxDedicatedStorageCapacityMeasure;
    }

    /**
     * Définit la valeur de la propriété maxDedicatedStorageCapacityMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link MaxDedicatedStorageCapacityMeasureType }
     *     
     */
    public void setMaxDedicatedStorageCapacityMeasure(MaxDedicatedStorageCapacityMeasureType value) {
        this.maxDedicatedStorageCapacityMeasure = value;
    }

    /**
     * Obtient la valeur de la propriété estimatedGeneratedUntilNextPortMeasure.
     * 
     * @return
     *     possible object is
     *     {@link EstimatedGeneratedUntilNextPortMeasureType }
     *     
     */
    public EstimatedGeneratedUntilNextPortMeasureType getEstimatedGeneratedUntilNextPortMeasure() {
        return estimatedGeneratedUntilNextPortMeasure;
    }

    /**
     * Définit la valeur de la propriété estimatedGeneratedUntilNextPortMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link EstimatedGeneratedUntilNextPortMeasureType }
     *     
     */
    public void setEstimatedGeneratedUntilNextPortMeasure(EstimatedGeneratedUntilNextPortMeasureType value) {
        this.estimatedGeneratedUntilNextPortMeasure = value;
    }

    /**
     * Gets the value of the remainingWasteDeliveryPortLocation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remainingWasteDeliveryPortLocation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemainingWasteDeliveryPortLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationType }
     * 
     * 
     */
    public List<LocationType> getRemainingWasteDeliveryPortLocation() {
        if (remainingWasteDeliveryPortLocation == null) {
            remainingWasteDeliveryPortLocation = new ArrayList<LocationType>();
        }
        return this.remainingWasteDeliveryPortLocation;
    }

}
