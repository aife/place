//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.GrossMassMeasureType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.WeighingDateType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.WeighingDeviceIDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.WeighingDeviceTypeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.WeighingMethodCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.WeighingTimeType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour VerifiedGrossMassType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="VerifiedGrossMassType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}WeighingDate" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}WeighingTime" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}WeighingMethodCode"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}WeighingDeviceID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}WeighingDeviceType" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}GrossMassMeasure"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}WeighingParty" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}ShipperParty" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}ResponsibleParty" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}DocumentReference" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerifiedGrossMassType", propOrder = {
    "ublExtensions",
    "id",
    "weighingDate",
    "weighingTime",
    "weighingMethodCode",
    "weighingDeviceID",
    "weighingDeviceType",
    "grossMassMeasure",
    "weighingParty",
    "shipperParty",
    "responsibleParty",
    "documentReference"
})
@ToString
@EqualsAndHashCode
public class VerifiedGrossMassType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "ID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected IDType id;
    @XmlElement(name = "WeighingDate", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected WeighingDateType weighingDate;
    @XmlElement(name = "WeighingTime", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected WeighingTimeType weighingTime;
    @XmlElement(name = "WeighingMethodCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2", required = true)
    protected WeighingMethodCodeType weighingMethodCode;
    @XmlElement(name = "WeighingDeviceID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected WeighingDeviceIDType weighingDeviceID;
    @XmlElement(name = "WeighingDeviceType", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected WeighingDeviceTypeType weighingDeviceType;
    @XmlElement(name = "GrossMassMeasure", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2", required = true)
    protected GrossMassMeasureType grossMassMeasure;
    @XmlElement(name = "WeighingParty")
    protected PartyType weighingParty;
    @XmlElement(name = "ShipperParty")
    protected PartyType shipperParty;
    @XmlElement(name = "ResponsibleParty")
    protected PartyType responsibleParty;
    @XmlElement(name = "DocumentReference", required = true)
    protected List<DocumentReferenceType> documentReference;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété id.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getID() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setID(IDType value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propriété weighingDate.
     * 
     * @return
     *     possible object is
     *     {@link WeighingDateType }
     *     
     */
    public WeighingDateType getWeighingDate() {
        return weighingDate;
    }

    /**
     * Définit la valeur de la propriété weighingDate.
     * 
     * @param value
     *     allowed object is
     *     {@link WeighingDateType }
     *     
     */
    public void setWeighingDate(WeighingDateType value) {
        this.weighingDate = value;
    }

    /**
     * Obtient la valeur de la propriété weighingTime.
     * 
     * @return
     *     possible object is
     *     {@link WeighingTimeType }
     *     
     */
    public WeighingTimeType getWeighingTime() {
        return weighingTime;
    }

    /**
     * Définit la valeur de la propriété weighingTime.
     * 
     * @param value
     *     allowed object is
     *     {@link WeighingTimeType }
     *     
     */
    public void setWeighingTime(WeighingTimeType value) {
        this.weighingTime = value;
    }

    /**
     * Obtient la valeur de la propriété weighingMethodCode.
     * 
     * @return
     *     possible object is
     *     {@link WeighingMethodCodeType }
     *     
     */
    public WeighingMethodCodeType getWeighingMethodCode() {
        return weighingMethodCode;
    }

    /**
     * Définit la valeur de la propriété weighingMethodCode.
     * 
     * @param value
     *     allowed object is
     *     {@link WeighingMethodCodeType }
     *     
     */
    public void setWeighingMethodCode(WeighingMethodCodeType value) {
        this.weighingMethodCode = value;
    }

    /**
     * Obtient la valeur de la propriété weighingDeviceID.
     * 
     * @return
     *     possible object is
     *     {@link WeighingDeviceIDType }
     *     
     */
    public WeighingDeviceIDType getWeighingDeviceID() {
        return weighingDeviceID;
    }

    /**
     * Définit la valeur de la propriété weighingDeviceID.
     * 
     * @param value
     *     allowed object is
     *     {@link WeighingDeviceIDType }
     *     
     */
    public void setWeighingDeviceID(WeighingDeviceIDType value) {
        this.weighingDeviceID = value;
    }

    /**
     * Obtient la valeur de la propriété weighingDeviceType.
     * 
     * @return
     *     possible object is
     *     {@link WeighingDeviceTypeType }
     *     
     */
    public WeighingDeviceTypeType getWeighingDeviceType() {
        return weighingDeviceType;
    }

    /**
     * Définit la valeur de la propriété weighingDeviceType.
     * 
     * @param value
     *     allowed object is
     *     {@link WeighingDeviceTypeType }
     *     
     */
    public void setWeighingDeviceType(WeighingDeviceTypeType value) {
        this.weighingDeviceType = value;
    }

    /**
     * Obtient la valeur de la propriété grossMassMeasure.
     * 
     * @return
     *     possible object is
     *     {@link GrossMassMeasureType }
     *     
     */
    public GrossMassMeasureType getGrossMassMeasure() {
        return grossMassMeasure;
    }

    /**
     * Définit la valeur de la propriété grossMassMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link GrossMassMeasureType }
     *     
     */
    public void setGrossMassMeasure(GrossMassMeasureType value) {
        this.grossMassMeasure = value;
    }

    /**
     * Obtient la valeur de la propriété weighingParty.
     * 
     * @return
     *     possible object is
     *     {@link PartyType }
     *     
     */
    public PartyType getWeighingParty() {
        return weighingParty;
    }

    /**
     * Définit la valeur de la propriété weighingParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyType }
     *     
     */
    public void setWeighingParty(PartyType value) {
        this.weighingParty = value;
    }

    /**
     * Obtient la valeur de la propriété shipperParty.
     * 
     * @return
     *     possible object is
     *     {@link PartyType }
     *     
     */
    public PartyType getShipperParty() {
        return shipperParty;
    }

    /**
     * Définit la valeur de la propriété shipperParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyType }
     *     
     */
    public void setShipperParty(PartyType value) {
        this.shipperParty = value;
    }

    /**
     * Obtient la valeur de la propriété responsibleParty.
     * 
     * @return
     *     possible object is
     *     {@link PartyType }
     *     
     */
    public PartyType getResponsibleParty() {
        return responsibleParty;
    }

    /**
     * Définit la valeur de la propriété responsibleParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyType }
     *     
     */
    public void setResponsibleParty(PartyType value) {
        this.responsibleParty = value;
    }

    /**
     * Gets the value of the documentReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documentReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumentReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentReferenceType }
     * 
     * 
     */
    public List<DocumentReferenceType> getDocumentReference() {
        if (documentReference == null) {
            documentReference = new ArrayList<DocumentReferenceType>();
        }
        return this.documentReference;
    }

}
