//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.AntennaLocusType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.GrossTonnageMeasureType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.INFShipClassCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MMSIRegistrationIDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.NetTonnageMeasureType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.RadioCallSignIDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.SegregatedBallastMeasureType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ShipConfigurationCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ShipsRequirementsType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.VesselIDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.VesselNameType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour MaritimeTransportType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="MaritimeTransportType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}VesselID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}VesselName" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}RadioCallSignID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}MMSIRegistrationID" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ShipsRequirements" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}GrossTonnageMeasure" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}NetTonnageMeasure" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}SegregatedBallastMeasure" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ShipConfigurationCode" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}INFShipClassCode" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}AntennaLocus" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}RegistryCertificateDocumentReference" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}RegistryPortLocation" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}VesselDynamics" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MaritimeTransportType", propOrder = {
    "ublExtensions",
    "vesselID",
    "vesselName",
    "radioCallSignID",
    "mmsiRegistrationID",
    "shipsRequirements",
    "grossTonnageMeasure",
    "netTonnageMeasure",
    "segregatedBallastMeasure",
    "shipConfigurationCode",
    "infShipClassCode",
    "antennaLocus",
    "registryCertificateDocumentReference",
    "registryPortLocation",
    "vesselDynamics"
})
@ToString
@EqualsAndHashCode
public class MaritimeTransportType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "VesselID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected VesselIDType vesselID;
    @XmlElement(name = "VesselName", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected VesselNameType vesselName;
    @XmlElement(name = "RadioCallSignID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected RadioCallSignIDType radioCallSignID;
    @XmlElement(name = "MMSIRegistrationID", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected MMSIRegistrationIDType mmsiRegistrationID;
    @XmlElement(name = "ShipsRequirements", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<ShipsRequirementsType> shipsRequirements;
    @XmlElement(name = "GrossTonnageMeasure", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected GrossTonnageMeasureType grossTonnageMeasure;
    @XmlElement(name = "NetTonnageMeasure", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected NetTonnageMeasureType netTonnageMeasure;
    @XmlElement(name = "SegregatedBallastMeasure", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected SegregatedBallastMeasureType segregatedBallastMeasure;
    @XmlElement(name = "ShipConfigurationCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ShipConfigurationCodeType shipConfigurationCode;
    @XmlElement(name = "INFShipClassCode", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected INFShipClassCodeType infShipClassCode;
    @XmlElement(name = "AntennaLocus", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected AntennaLocusType antennaLocus;
    @XmlElement(name = "RegistryCertificateDocumentReference")
    protected DocumentReferenceType registryCertificateDocumentReference;
    @XmlElement(name = "RegistryPortLocation")
    protected LocationType registryPortLocation;
    @XmlElement(name = "VesselDynamics")
    protected VesselDynamicsType vesselDynamics;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété vesselID.
     * 
     * @return
     *     possible object is
     *     {@link VesselIDType }
     *     
     */
    public VesselIDType getVesselID() {
        return vesselID;
    }

    /**
     * Définit la valeur de la propriété vesselID.
     * 
     * @param value
     *     allowed object is
     *     {@link VesselIDType }
     *     
     */
    public void setVesselID(VesselIDType value) {
        this.vesselID = value;
    }

    /**
     * Obtient la valeur de la propriété vesselName.
     * 
     * @return
     *     possible object is
     *     {@link VesselNameType }
     *     
     */
    public VesselNameType getVesselName() {
        return vesselName;
    }

    /**
     * Définit la valeur de la propriété vesselName.
     * 
     * @param value
     *     allowed object is
     *     {@link VesselNameType }
     *     
     */
    public void setVesselName(VesselNameType value) {
        this.vesselName = value;
    }

    /**
     * Obtient la valeur de la propriété radioCallSignID.
     * 
     * @return
     *     possible object is
     *     {@link RadioCallSignIDType }
     *     
     */
    public RadioCallSignIDType getRadioCallSignID() {
        return radioCallSignID;
    }

    /**
     * Définit la valeur de la propriété radioCallSignID.
     * 
     * @param value
     *     allowed object is
     *     {@link RadioCallSignIDType }
     *     
     */
    public void setRadioCallSignID(RadioCallSignIDType value) {
        this.radioCallSignID = value;
    }

    /**
     * Obtient la valeur de la propriété mmsiRegistrationID.
     * 
     * @return
     *     possible object is
     *     {@link MMSIRegistrationIDType }
     *     
     */
    public MMSIRegistrationIDType getMMSIRegistrationID() {
        return mmsiRegistrationID;
    }

    /**
     * Définit la valeur de la propriété mmsiRegistrationID.
     * 
     * @param value
     *     allowed object is
     *     {@link MMSIRegistrationIDType }
     *     
     */
    public void setMMSIRegistrationID(MMSIRegistrationIDType value) {
        this.mmsiRegistrationID = value;
    }

    /**
     * Gets the value of the shipsRequirements property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipsRequirements property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipsRequirements().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipsRequirementsType }
     * 
     * 
     */
    public List<ShipsRequirementsType> getShipsRequirements() {
        if (shipsRequirements == null) {
            shipsRequirements = new ArrayList<ShipsRequirementsType>();
        }
        return this.shipsRequirements;
    }

    /**
     * Obtient la valeur de la propriété grossTonnageMeasure.
     * 
     * @return
     *     possible object is
     *     {@link GrossTonnageMeasureType }
     *     
     */
    public GrossTonnageMeasureType getGrossTonnageMeasure() {
        return grossTonnageMeasure;
    }

    /**
     * Définit la valeur de la propriété grossTonnageMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link GrossTonnageMeasureType }
     *     
     */
    public void setGrossTonnageMeasure(GrossTonnageMeasureType value) {
        this.grossTonnageMeasure = value;
    }

    /**
     * Obtient la valeur de la propriété netTonnageMeasure.
     * 
     * @return
     *     possible object is
     *     {@link NetTonnageMeasureType }
     *     
     */
    public NetTonnageMeasureType getNetTonnageMeasure() {
        return netTonnageMeasure;
    }

    /**
     * Définit la valeur de la propriété netTonnageMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link NetTonnageMeasureType }
     *     
     */
    public void setNetTonnageMeasure(NetTonnageMeasureType value) {
        this.netTonnageMeasure = value;
    }

    /**
     * Obtient la valeur de la propriété segregatedBallastMeasure.
     * 
     * @return
     *     possible object is
     *     {@link SegregatedBallastMeasureType }
     *     
     */
    public SegregatedBallastMeasureType getSegregatedBallastMeasure() {
        return segregatedBallastMeasure;
    }

    /**
     * Définit la valeur de la propriété segregatedBallastMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link SegregatedBallastMeasureType }
     *     
     */
    public void setSegregatedBallastMeasure(SegregatedBallastMeasureType value) {
        this.segregatedBallastMeasure = value;
    }

    /**
     * Obtient la valeur de la propriété shipConfigurationCode.
     * 
     * @return
     *     possible object is
     *     {@link ShipConfigurationCodeType }
     *     
     */
    public ShipConfigurationCodeType getShipConfigurationCode() {
        return shipConfigurationCode;
    }

    /**
     * Définit la valeur de la propriété shipConfigurationCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipConfigurationCodeType }
     *     
     */
    public void setShipConfigurationCode(ShipConfigurationCodeType value) {
        this.shipConfigurationCode = value;
    }

    /**
     * Obtient la valeur de la propriété infShipClassCode.
     * 
     * @return
     *     possible object is
     *     {@link INFShipClassCodeType }
     *     
     */
    public INFShipClassCodeType getINFShipClassCode() {
        return infShipClassCode;
    }

    /**
     * Définit la valeur de la propriété infShipClassCode.
     * 
     * @param value
     *     allowed object is
     *     {@link INFShipClassCodeType }
     *     
     */
    public void setINFShipClassCode(INFShipClassCodeType value) {
        this.infShipClassCode = value;
    }

    /**
     * Obtient la valeur de la propriété antennaLocus.
     * 
     * @return
     *     possible object is
     *     {@link AntennaLocusType }
     *     
     */
    public AntennaLocusType getAntennaLocus() {
        return antennaLocus;
    }

    /**
     * Définit la valeur de la propriété antennaLocus.
     * 
     * @param value
     *     allowed object is
     *     {@link AntennaLocusType }
     *     
     */
    public void setAntennaLocus(AntennaLocusType value) {
        this.antennaLocus = value;
    }

    /**
     * Obtient la valeur de la propriété registryCertificateDocumentReference.
     * 
     * @return
     *     possible object is
     *     {@link DocumentReferenceType }
     *     
     */
    public DocumentReferenceType getRegistryCertificateDocumentReference() {
        return registryCertificateDocumentReference;
    }

    /**
     * Définit la valeur de la propriété registryCertificateDocumentReference.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentReferenceType }
     *     
     */
    public void setRegistryCertificateDocumentReference(DocumentReferenceType value) {
        this.registryCertificateDocumentReference = value;
    }

    /**
     * Obtient la valeur de la propriété registryPortLocation.
     * 
     * @return
     *     possible object is
     *     {@link LocationType }
     *     
     */
    public LocationType getRegistryPortLocation() {
        return registryPortLocation;
    }

    /**
     * Définit la valeur de la propriété registryPortLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationType }
     *     
     */
    public void setRegistryPortLocation(LocationType value) {
        this.registryPortLocation = value;
    }

    /**
     * Obtient la valeur de la propriété vesselDynamics.
     * 
     * @return
     *     possible object is
     *     {@link VesselDynamicsType }
     *     
     */
    public VesselDynamicsType getVesselDynamics() {
        return vesselDynamics;
    }

    /**
     * Définit la valeur de la propriété vesselDynamics.
     * 
     * @param value
     *     allowed object is
     *     {@link VesselDynamicsType }
     *     
     */
    public void setVesselDynamics(VesselDynamicsType value) {
        this.vesselDynamics = value;
    }

}
