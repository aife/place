//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MessageFormatType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour EncryptionDataType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="EncryptionDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}MessageFormat"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}EncryptionCertificateAttachment" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}EncryptionCertificatePathChain" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}EncryptionSymmetricAlgorithm" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EncryptionDataType", propOrder = {
    "ublExtensions",
    "messageFormat",
    "encryptionCertificateAttachment",
    "encryptionCertificatePathChain",
    "encryptionSymmetricAlgorithm"
})
@ToString
@EqualsAndHashCode
public class EncryptionDataType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "MessageFormat", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2", required = true)
    protected MessageFormatType messageFormat;
    @XmlElement(name = "EncryptionCertificateAttachment")
    protected AttachmentType encryptionCertificateAttachment;
    @XmlElement(name = "EncryptionCertificatePathChain")
    protected List<EncryptionCertificatePathChainType> encryptionCertificatePathChain;
    @XmlElement(name = "EncryptionSymmetricAlgorithm")
    protected List<EncryptionSymmetricAlgorithmType> encryptionSymmetricAlgorithm;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété messageFormat.
     * 
     * @return
     *     possible object is
     *     {@link MessageFormatType }
     *     
     */
    public MessageFormatType getMessageFormat() {
        return messageFormat;
    }

    /**
     * Définit la valeur de la propriété messageFormat.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageFormatType }
     *     
     */
    public void setMessageFormat(MessageFormatType value) {
        this.messageFormat = value;
    }

    /**
     * Obtient la valeur de la propriété encryptionCertificateAttachment.
     * 
     * @return
     *     possible object is
     *     {@link AttachmentType }
     *     
     */
    public AttachmentType getEncryptionCertificateAttachment() {
        return encryptionCertificateAttachment;
    }

    /**
     * Définit la valeur de la propriété encryptionCertificateAttachment.
     * 
     * @param value
     *     allowed object is
     *     {@link AttachmentType }
     *     
     */
    public void setEncryptionCertificateAttachment(AttachmentType value) {
        this.encryptionCertificateAttachment = value;
    }

    /**
     * Gets the value of the encryptionCertificatePathChain property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the encryptionCertificatePathChain property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEncryptionCertificatePathChain().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EncryptionCertificatePathChainType }
     * 
     * 
     */
    public List<EncryptionCertificatePathChainType> getEncryptionCertificatePathChain() {
        if (encryptionCertificatePathChain == null) {
            encryptionCertificatePathChain = new ArrayList<EncryptionCertificatePathChainType>();
        }
        return this.encryptionCertificatePathChain;
    }

    /**
     * Gets the value of the encryptionSymmetricAlgorithm property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the encryptionSymmetricAlgorithm property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEncryptionSymmetricAlgorithm().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EncryptionSymmetricAlgorithmType }
     * 
     * 
     */
    public List<EncryptionSymmetricAlgorithmType> getEncryptionSymmetricAlgorithm() {
        if (encryptionSymmetricAlgorithm == null) {
            encryptionSymmetricAlgorithm = new ArrayList<EncryptionSymmetricAlgorithmType>();
        }
        return this.encryptionSymmetricAlgorithm;
    }

}
