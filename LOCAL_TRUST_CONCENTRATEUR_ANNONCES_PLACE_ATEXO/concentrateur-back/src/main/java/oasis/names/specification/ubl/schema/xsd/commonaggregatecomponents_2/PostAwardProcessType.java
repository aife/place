//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ElectronicCatalogueUsageIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ElectronicInvoiceAcceptedIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ElectronicOrderUsageIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ElectronicPaymentUsageIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;


/**
 * <p>Classe Java pour PostAwardProcessType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="PostAwardProcessType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}UBLExtensions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ElectronicCatalogueUsageIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ElectronicInvoiceAcceptedIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ElectronicOrderUsageIndicator" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}ElectronicPaymentUsageIndicator" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PostAwardProcessType", propOrder = {
    "ublExtensions",
    "electronicCatalogueUsageIndicator",
    "electronicInvoiceAcceptedIndicator",
    "electronicOrderUsageIndicator",
    "electronicPaymentUsageIndicator"
})
@ToString
@EqualsAndHashCode
public class PostAwardProcessType {

    @XmlElement(name = "UBLExtensions", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
    protected UBLExtensionsType ublExtensions;
    @XmlElement(name = "ElectronicCatalogueUsageIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ElectronicCatalogueUsageIndicatorType electronicCatalogueUsageIndicator;
    @XmlElement(name = "ElectronicInvoiceAcceptedIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ElectronicInvoiceAcceptedIndicatorType electronicInvoiceAcceptedIndicator;
    @XmlElement(name = "ElectronicOrderUsageIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected ElectronicOrderUsageIndicatorType electronicOrderUsageIndicator;
    @XmlElement(name = "ElectronicPaymentUsageIndicator", namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
    protected List<ElectronicPaymentUsageIndicatorType> electronicPaymentUsageIndicator;

    /**
     * Obtient la valeur de la propriété ublExtensions.
     * 
     * @return
     *     possible object is
     *     {@link UBLExtensionsType }
     *     
     */
    public UBLExtensionsType getUBLExtensions() {
        return ublExtensions;
    }

    /**
     * Définit la valeur de la propriété ublExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link UBLExtensionsType }
     *     
     */
    public void setUBLExtensions(UBLExtensionsType value) {
        this.ublExtensions = value;
    }

    /**
     * Obtient la valeur de la propriété electronicCatalogueUsageIndicator.
     * 
     * @return
     *     possible object is
     *     {@link ElectronicCatalogueUsageIndicatorType }
     *     
     */
    public ElectronicCatalogueUsageIndicatorType getElectronicCatalogueUsageIndicator() {
        return electronicCatalogueUsageIndicator;
    }

    /**
     * Définit la valeur de la propriété electronicCatalogueUsageIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link ElectronicCatalogueUsageIndicatorType }
     *     
     */
    public void setElectronicCatalogueUsageIndicator(ElectronicCatalogueUsageIndicatorType value) {
        this.electronicCatalogueUsageIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété electronicInvoiceAcceptedIndicator.
     * 
     * @return
     *     possible object is
     *     {@link ElectronicInvoiceAcceptedIndicatorType }
     *     
     */
    public ElectronicInvoiceAcceptedIndicatorType getElectronicInvoiceAcceptedIndicator() {
        return electronicInvoiceAcceptedIndicator;
    }

    /**
     * Définit la valeur de la propriété electronicInvoiceAcceptedIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link ElectronicInvoiceAcceptedIndicatorType }
     *     
     */
    public void setElectronicInvoiceAcceptedIndicator(ElectronicInvoiceAcceptedIndicatorType value) {
        this.electronicInvoiceAcceptedIndicator = value;
    }

    /**
     * Obtient la valeur de la propriété electronicOrderUsageIndicator.
     * 
     * @return
     *     possible object is
     *     {@link ElectronicOrderUsageIndicatorType }
     *     
     */
    public ElectronicOrderUsageIndicatorType getElectronicOrderUsageIndicator() {
        return electronicOrderUsageIndicator;
    }

    /**
     * Définit la valeur de la propriété electronicOrderUsageIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link ElectronicOrderUsageIndicatorType }
     *     
     */
    public void setElectronicOrderUsageIndicator(ElectronicOrderUsageIndicatorType value) {
        this.electronicOrderUsageIndicator = value;
    }

    /**
     * Gets the value of the electronicPaymentUsageIndicator property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the electronicPaymentUsageIndicator property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElectronicPaymentUsageIndicator().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ElectronicPaymentUsageIndicatorType }
     * 
     * 
     */
    public List<ElectronicPaymentUsageIndicatorType> getElectronicPaymentUsageIndicator() {
        if (electronicPaymentUsageIndicator == null) {
            electronicPaymentUsageIndicator = new ArrayList<ElectronicPaymentUsageIndicatorType>();
        }
        return this.electronicPaymentUsageIndicator;
    }

}
