//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2023.12.20 à 02:15:50 PM CET 
//


package oasis.names.specification.ubl.schema.xsd.qualifieddatatypes_2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oasis.names.specification.bdndr.schema.xsd.unqualifieddatatypes_1.CodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.LocaleCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.NoticeLanguageCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PreferredLanguageLocaleCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TenderLanguageLocaleCodeType;


/**
 * <p>Classe Java pour LanguageCodeType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="LanguageCodeType"&gt;
 *   &lt;simpleContent&gt;
 *     &lt;restriction base="&lt;urn:oasis:names:specification:bdndr:schema:xsd:UnqualifiedDataTypes-1&gt;CodeType"&gt;
 *     &lt;/restriction&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LanguageCodeType")
@XmlSeeAlso({
    LocaleCodeType.class,
    NoticeLanguageCodeType.class,
    PreferredLanguageLocaleCodeType.class,
    TenderLanguageLocaleCodeType.class
})
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class LanguageCodeType
    extends CodeType
{


}
