package fr.atexo.annonces.boot.core.domain.xlsx.services;


import fr.atexo.annonces.boot.core.domain.xlsx.model.XlsxColumn;
import fr.atexo.annonces.boot.core.domain.xlsx.model.XlsxHeader;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.List;

@Service
public interface IXlsxService {

    XSSFSheet createXLSX(XSSFWorkbook workbook, XSSFSheet sheet);

    XSSFSheet createXLSX(XSSFWorkbook workbook, String sheetname, List<XlsxHeader> headers);

    void setData(XSSFSheet sheet, int rownum, int column, String value, CellType type);

    void setData(XSSFSheet sheet, List<List<XlsxColumn>> rowList, int start);

    void autoSize(XSSFSheet sheet, int end);

    XSSFWorkbook openFile(InputStream inputStream);

    List<List<String>> extractValues(XSSFWorkbook xlsx, int sheetIndex, int rownumStart);
}

