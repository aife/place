package fr.atexo.annonces.helpers;

import com.atexo.annonces.commun.mpe.AcheteurMpe;
import com.atexo.annonces.commun.mpe.ConsultationMpe;
import com.atexo.annonces.commun.mpe.LotMpe;
import org.springframework.core.ParameterizedTypeReference;

import java.util.Optional;
import java.util.Set;

public class TypeHelper {

	public ParameterizedTypeReference<Set<AcheteurMpe>> getAcheteursType() {
		return new ParameterizedTypeReference<>() {};
	}

	public ParameterizedTypeReference<Set<LotMpe>> getLotsType() {
		return new ParameterizedTypeReference<>() {};
	}

	public ParameterizedTypeReference<Optional<ConsultationMpe>> getConsultationType() {
		return new ParameterizedTypeReference<>() {};
	}
}
