package fr.atexo.annonces.reseau;

import fr.atexo.annonces.boot.configuration.http_logging.LogEntryExit;
import fr.atexo.annonces.dto.DepartementDTO;
import fr.atexo.annonces.service.DepartementService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Point d'entrée web pour manipuler les départements
 */
@RestController
@RequestMapping("/rest/v2/departements")
@Tag(name = "Départements")
public class DepartementController {

    private final DepartementService departementService;

    public DepartementController(DepartementService departementService) {
        this.departementService = departementService;
    }

    @GetMapping
    @LogEntryExit(skip = true)
    @Operation(summary = "swagger.departements.get.operation.summary",
            responses = {@ApiResponse(responseCode = HttpServletResponse.SC_UNAUTHORIZED + "",
                    description = "swagger.response.unauthorized")})
    public List<DepartementDTO> getDepartements() {
        return departementService.getDepartements();
    }
}
