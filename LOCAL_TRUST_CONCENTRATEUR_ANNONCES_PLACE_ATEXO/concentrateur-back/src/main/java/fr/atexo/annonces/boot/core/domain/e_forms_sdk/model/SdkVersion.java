package fr.atexo.annonces.boot.core.domain.e_forms_sdk.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SuperBuilder
public class SdkVersion {
    private String ublVersion;
    private String sdkVersion;
    private MetadataDatabase metadataDatabase;
}
