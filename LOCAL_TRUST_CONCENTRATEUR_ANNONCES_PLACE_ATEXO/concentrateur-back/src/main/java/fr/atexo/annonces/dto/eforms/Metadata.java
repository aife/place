package fr.atexo.annonces.dto.eforms;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Metadata {
    private String noticeAuthorEmail;
    private String noticeAuthorLocale;
}
