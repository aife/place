package fr.atexo.annonces.dto;

import fr.atexo.annonces.modeles.StatutSuiviAnnonceEnum;
import lombok.*;

import java.time.ZonedDateTime;

/**
 * DTO - Représente une demande de publication
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class HistoriqueDTO {

    private StatutSuiviAnnonceEnum statut;

    private ZonedDateTime date;

}
