package fr.atexo.annonces.service;


import fr.atexo.annonces.boot.entity.PieceJointe;
import fr.atexo.annonces.boot.repository.PieceJointeRepository;
import fr.atexo.annonces.service.exception.PieceJointeException;
import fr.atexo.annonces.service.exception.PieceJointeExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
public class PieceJointeServiceImpl implements PieceJointeService {

    private final String path;
    private final PieceJointeRepository pieceJointeRepository;

    private static final String ERROR_MESSAGE_PARAMS = "les parametres de la piece jointe ne doivent pas être null";
    private static final String ERROR_MESSAGE = "la piece jointe n'existe pas";

    public PieceJointeServiceImpl(@Value("${upload.folder.path}") String path, PieceJointeRepository pieceJointeRepository) {
        this.path = path;
        this.pieceJointeRepository = pieceJointeRepository;
    }

    @Override
    public PieceJointe save(PieceJointe pieceJointe, InputStream inputStream) {
        if (pieceJointe == null) {
            throw new PieceJointeException(PieceJointeExceptionEnum.MANDATORY, ERROR_MESSAGE_PARAMS);
        }
        String relativePath = this.createPathToUpload();
        return getPieceJointe(pieceJointe, inputStream, relativePath);
    }

    @Override
    public PieceJointe save(MultipartFile file) {
        if (file == null) {
            throw new PieceJointeException(PieceJointeExceptionEnum.MANDATORY, ERROR_MESSAGE_PARAMS);
        }
        PieceJointe pieceJointe = PieceJointe.builder().contentType(file.getContentType()).name(file.getOriginalFilename()).size(file.getSize()).build();
        String relativePath = this.createPathToUpload();
        return getPieceJointe(pieceJointe, file, relativePath);
    }

    @Override
    public PieceJointe save(InputStream file, String contentType, String name) {
        if (file == null) {
            throw new PieceJointeException(PieceJointeExceptionEnum.MANDATORY, ERROR_MESSAGE_PARAMS);
        }
        PieceJointe pieceJointe = PieceJointe.builder().contentType(contentType).name(name).size(0L).build();
        String relativePath = this.createPathToUpload();
        return getPieceJointe(pieceJointe, file, relativePath);
    }


    @Override
    public String getExtension(String filename) {
        return Optional.ofNullable(filename).filter(f -> f.contains(".")).map(f -> f.substring(filename.lastIndexOf(".") + 1)).orElse(null);
    }

    @Override
    public void deleteById(Long id) {
        try {
            PieceJointe pieceJointe = pieceJointeRepository.findById(id).orElse(null);
            if (pieceJointe != null) {
                this.pieceJointeRepository.deleteById(id);
                this.cleanUpFile(pieceJointe.getPath());
            }
        } catch (Exception e) {
            throw new PieceJointeException(PieceJointeExceptionEnum.FOLDER_ERROR, e.getMessage());
        }
    }

    @Override
    public PieceJointe getById(Long id) {
        PieceJointe pieceJointe = pieceJointeRepository.findById(id).orElse(null);
        if (pieceJointe != null) {
            return pieceJointe;
        } else {
            throw new PieceJointeException(PieceJointeExceptionEnum.NOT_FOUND, "La pièce jointe avec id " + id + " n'existe pas");
        }
    }

    @Override
    public Resource loadFileAsResource(String chemin) {
        try {
            Path pathResource = Paths.get(this.path, chemin);
            Resource resource = new UrlResource(pathResource.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new PieceJointeException(PieceJointeExceptionEnum.NOT_FOUND, ERROR_MESSAGE);
            }
        } catch (MalformedURLException ex) {
            throw new PieceJointeException(PieceJointeExceptionEnum.NOT_FOUND, ERROR_MESSAGE);
        }
    }

    @Override
    public PieceJointe clone(PieceJointe pieceJointe) {
        if (pieceJointe == null) return null;
        log.info("Copie de la pièce jointe {}", pieceJointe.getName());
        PieceJointe clone = PieceJointe.builder().contentType(pieceJointe.getContentType()).size(pieceJointe.getSize()).pieceJointeParente(pieceJointe).name(pieceJointe.getName()).build();
        clone.setId(null);
        try {
            return this.save(clone, this.loadFileAsResource(pieceJointe.getPath()).getInputStream());
        } catch (Exception e) {
            log.error("Erreur lors de la copie de la pièce jointe {}", e.getMessage());
        }
        return null;
    }

    public PieceJointe getPieceJointe(PieceJointe pieceJointe, MultipartFile inputStream, String relativePath) {
        File file = new File(path, relativePath);
        try {
            Files.createDirectories(file.toPath());
        } catch (IOException e) {
            throw new PieceJointeException(PieceJointeExceptionEnum.FOLDER_ERROR, e.getMessage());
        }
        try {
            this.storageFile(inputStream.getInputStream(), file.getAbsolutePath());
            pieceJointe.setPath(relativePath);
            pieceJointe.setSize(file.length());
            try (var stream = new FileInputStream(path + File.separator + relativePath)) {
                pieceJointe.setMd5(DigestUtils.md5DigestAsHex(stream));
            }
            log.info("Sauvegarde pièce jointe : name = {}, path = {}", pieceJointe.getName(), pieceJointe.getPath());
            return this.pieceJointeRepository.save(pieceJointe);
        } catch (Exception e) {
            this.cleanUpFile(file.getAbsolutePath());
            throw new PieceJointeException(PieceJointeExceptionEnum.SAVE_ERROR, e.getMessage(), e);
        }
    }

    public PieceJointe getPieceJointe(PieceJointe pieceJointe, InputStream inputStream, String relativePath) {
        File file = new File(path, relativePath);
        try {
            Files.createDirectories(file.toPath());
        } catch (IOException e) {
            throw new PieceJointeException(PieceJointeExceptionEnum.FOLDER_ERROR, e.getMessage());
        }
        try {
            this.storageFile(inputStream, file.getAbsolutePath());
            pieceJointe.setPath(relativePath);
            pieceJointe.setSize(file.length());
            try (var stream = new FileInputStream(path + File.separator + relativePath)) {
                pieceJointe.setMd5(DigestUtils.md5DigestAsHex(stream));
            }
            log.info("Sauvegarde pièce jointe : name = {}, path = {}", pieceJointe.getName(), pieceJointe.getPath());
            return this.pieceJointeRepository.save(pieceJointe);
        } catch (Exception e) {
            this.cleanUpFile(file.getAbsolutePath());
            throw new PieceJointeException(PieceJointeExceptionEnum.SAVE_ERROR, e.getMessage(), e);
        }
    }

    private void storageFile(InputStream inputStream, String pathFile) {
        if (inputStream == null) {
            throw new PieceJointeException(PieceJointeExceptionEnum.MANDATORY, "la piece jointe ne doit pas être null");
        }

        try {
            Path path = Path.of(pathFile).getParent();
            if (!path.toFile().exists()) {
                boolean created = path.toFile().mkdirs();
                if (!created)
                    throw new PieceJointeException(PieceJointeExceptionEnum.FOLDER_ERROR, "Erreur lors de la création du dossier parent");
            }
            log.info("Sauvegarde du fichier : {}", pathFile);
            Files.copy(inputStream, Paths.get(pathFile), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new PieceJointeException(PieceJointeExceptionEnum.SAVE_ERROR, e.getMessage(), e);
        }
    }

    private String createPathToUpload() {
        StringBuilder result = new StringBuilder();
        LocalDate now = LocalDate.now();
        result.append(now.getYear());
        result.append(File.separator);
        result.append(now.getMonthValue());
        result.append(File.separator);
        result.append(now.getDayOfMonth());
        result.append(File.separator);
        UUID uuid = UUID.randomUUID();
        result.append(uuid);
        return result.toString();
    }

    private void cleanUpFile(String path) {
        Path pathFileToDelete = new File(path).toPath();
        if (Files.exists(pathFileToDelete)) {
            try {
                Files.delete(pathFileToDelete);
            } catch (IOException e) {
                throw new PieceJointeException(PieceJointeExceptionEnum.FOLDER_ERROR, e.getMessage());
            }
        }
    }

}
