package fr.atexo.annonces.dto;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AvisValidationPatch implements Serializable {


    private boolean valid;

    private String raisonRefus;

}
