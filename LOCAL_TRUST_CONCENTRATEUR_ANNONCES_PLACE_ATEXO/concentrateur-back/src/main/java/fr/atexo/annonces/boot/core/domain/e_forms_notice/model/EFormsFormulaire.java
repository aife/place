package fr.atexo.annonces.boot.core.domain.e_forms_notice.model;

import fr.atexo.annonces.dto.AgentDTO;
import fr.atexo.annonces.dto.OrganismeDTO;
import fr.atexo.annonces.modeles.StatutENoticeEnum;
import lombok.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
public class EFormsFormulaire implements Serializable {

    private Long id;

    private String uuid;

    private String idNotice;

    private String lang;
    private String tedVersion;

    private Integer idConsultation;
    private String intituleConsultation;
    private String referenceConsultation;

    private Integer noticeVersion;
    private StatutENoticeEnum statut;

    private String xmlGenere;
    private String svrlString;
    private String formulaire;

    private boolean valid;

    private ZonedDateTime dateCreation;
    private ZonedDateTime dateEnvoiSoumission;

    private ZonedDateTime dateModificationStatut;

    private ZonedDateTime dateSoumission;

    private ZonedDateTime dateEnvoiAnnulation;
    private ZonedDateTime dateModification;
    private OrganismeDTO organisme;

    private String plateforme;

    private String auteurEmail;
    private String publicationId;
    private Date publicationDate;
    private String versionSdk;
    private String descriptionOrganisme;

    private Integer idAnnonce;
    private Long idTypeAvisPub;
    private Long previousId;
    private Long nextId;

    private AgentDTO creerPar;
    private AgentDTO modifierPar;


}
