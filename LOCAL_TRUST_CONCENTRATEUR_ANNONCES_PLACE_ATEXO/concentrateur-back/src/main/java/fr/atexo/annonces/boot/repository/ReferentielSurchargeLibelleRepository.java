package fr.atexo.annonces.boot.repository;


import fr.atexo.annonces.boot.entity.ReferentielSurchargeLibelle;
import fr.atexo.annonces.config.DaoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReferentielSurchargeLibelleRepository extends DaoRepository<ReferentielSurchargeLibelle, Long> {
    List<ReferentielSurchargeLibelle> findAllByOrganismeIdAndLang(Integer idOrganisme, String lang);
}
