package fr.atexo.annonces.service.exception;

import lombok.Getter;

@Getter
public class ConcentraturException extends AtexoException {
    public ConcentraturException(ExceptionEnum exceptionEnum, String message) {
        super(exceptionEnum, message);
    }


    public ConcentraturException(ExceptionEnum exceptionEnum, String message, Throwable e) {
        super(exceptionEnum, message, e);

    }


}
