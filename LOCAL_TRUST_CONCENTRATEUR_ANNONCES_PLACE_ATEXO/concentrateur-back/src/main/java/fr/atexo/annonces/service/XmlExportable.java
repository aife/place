package fr.atexo.annonces.service;

/**
 * Définit une méthode qui exporte un objet vers une chaîne XML
 */
public interface XmlExportable<T> {
    String toXml(T object);
    String toXmlTransaction(T object);

}
