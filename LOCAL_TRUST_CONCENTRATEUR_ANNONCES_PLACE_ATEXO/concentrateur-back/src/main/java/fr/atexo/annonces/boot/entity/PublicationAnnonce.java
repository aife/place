package fr.atexo.annonces.boot.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "publication_annonces")
@Getter
@Setter
public class PublicationAnnonce implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private int id;

    @Column(name = "ID_ANNONCE_APPLICATION")
    private String idAnnonceApplication;

    @Column(name = "CANAL")
    private String canal;

    @Column(name = "XML")
    @Type(type = "text")
    private String xml;

    @Column(name = "DATE_ENVOI")
    private Date dateEnvoi;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ID_SUIVI_ANNONCE", nullable = false)
    private SuiviAnnonce suiviAnnonce;
}
