package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.entity.PieceJointe;
import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.boot.entity.SuiviTypeAvisPub;
import fr.atexo.annonces.dto.*;
import fr.atexo.annonces.modeles.DashboardSuiviTypeAvisSpecification;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * Couche service pour manipuler les demandes de publications et leur suivi
 */
public interface SuiviTypeAvisPubService {


    PageDTO<SuiviTypeAvisPubDTO> getFormulaires(String plateforme, DashboardSuiviTypeAvisSpecification specification, Pageable pageable);

    SuiviTypeAvisPubDTO updateValidationSip(String plateforme, Long id, AvisValidationPatch valid, String bearer);

    SuiviTypeAvisPubDTO updateValidationEco(String plateforme, Long id, AvisValidationPatch valid, String bearer);
    SuiviTypeAvisPubDTO invalidationEco(String plateforme, Long id, AvisValidationPatch valid, String bearer);

    SuiviTypeAvisPubDTO demandeValidation(Long id, Integer idConsultation, String idPlatform, String token);

    List<SuiviTypeAvisPubDTO> updateDateDeMiseEnLigne(Integer idConsultation, String plateforme, String dmls);

    ByteArrayOutputStream exporterFormulaires(HttpServletResponse response, DashboardSuiviTypeAvisSpecification facturationSearch, String plateforme);

    void updateAnnoncesFromTypeAvis(SuiviTypeAvisPub facturation, String token);

    ByteArrayOutputStream exportStatistiqueSuiviAnnonces(HttpServletResponse response, String plateforme, Boolean sip);

    List<AnnonceStatistique> getStatistiqueSuiviAnnonces(String plateforme, Boolean sip);

    List<SuiviTypeAvisPubDTO> getConsultationAnnonces(String plateforme, Integer idConsultation);

    SuiviTypeAvisPubDTO addSuiviTypeAvisPub(SuiviTypeAvisPubAdd avisPub, String token, String plateforme);

    List<SuiviOffreDTO> getAllAvis(String idPlatform, Integer idConsultation);

    SuiviAnnonce getPortailAnnonce(Long id, String idPlatform, Integer idConsultation);

    SuiviAnnonce getLastPortailPdf(String idPlatform, Integer idConsultation);

    boolean deleteSuiviTypeAvisPub(Long id, String idAgent, String plateforme, Integer idConsultation);

    SuiviTypeAvisPubDTO modifySuiviTypeAvisPub(Long id, SuiviTypeAvisPubPatch avisPub, String token, String idPlatform, Integer idConsultation);

    SuiviTypeAvisPubDTO reprendre(Long id, String token, String idPlatform, Integer idConsultation);

    SuiviTypeAvisPubDTO change(Long id, String token, String plateforme, Integer idConsultation);

    SuiviTypeAvisPubDTO get(Long id, String idPlatform, Integer idConsultation);

    List<RevisionDTO<SuiviTypeAvisPubRevisionDTO>> getHistoriques(Long id, String idPlatform, Integer idConsultation);

    EditionRequest modifyAvisDocument(Long id, String plateforme, Integer idConsultation, String idAgent);

    TrackDocumentResponse getDocumentCallback(Long id, FileStatus status);

    List<SuiviTypeAvisPubPieceJointeAssoDTO> getDocuments(Long id, String idPlatform, Integer idConsultation, String token);

    SuiviTypeAvisPubDTO deletePieceJointeByIdTypeAvisAndIdAsso(Long id, Long idAsso, String idPlatform, Integer idConsultation);

    SuiviTypeAvisPubPieceJointeAssoDTO importerDocument(Long id, String idPlatform, Integer idConsultation, PieceJointe pieceJointe, InputStream inputStream);

    SuiviTypeAvisPubDTO updateMail(Long id, MailUpdate update, String idPlatform, Integer idConsultation);

    SuiviTypeAvisPubDTO importerAvisExterne(Long id, String idPlatform, Integer idConsultation, String bearer, PieceJointe pieceJointe, InputStream inputStream);

    SuiviTypeAvisPubDTO deleteAvisExterne(Long id, String idPlatform, Integer idConsultation, String bearer);

    PieceJointe getPieceJointe(Long id, Long idPieceJointe, String idPlatform, Integer idConsultation, String bearer);

    SuiviTypeAvisPubDTO fixStatus(String bearer, String idPlatform);

    List<SuiviOffreDTO> synchronize(String idPlatform, ZonedDateTime dateDebut, ZonedDateTime dateFin);

    List<SuiviOffreDTO> getLastAvis(String idPlatform, Integer idConsultation);

    void suiviDocument(SuiviTypeAvisPub avisPub);
}
