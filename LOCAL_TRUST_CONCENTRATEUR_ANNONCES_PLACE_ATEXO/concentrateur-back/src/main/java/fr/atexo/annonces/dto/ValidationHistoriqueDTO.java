package fr.atexo.annonces.dto;

import fr.atexo.annonces.modeles.StatutSuiviAnnonceEnum;
import fr.atexo.annonces.modeles.StatutTypeAvisAnnonceEnum;
import lombok.*;

import java.time.ZonedDateTime;

/**
 * DTO - Représente une demande de publication
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ValidationHistoriqueDTO {

    private StatutSuiviAnnonceEnum statutAnnonce;
    private Integer idAnnonce;
    private StatutTypeAvisAnnonceEnum statut;

    private ZonedDateTime date;

}
