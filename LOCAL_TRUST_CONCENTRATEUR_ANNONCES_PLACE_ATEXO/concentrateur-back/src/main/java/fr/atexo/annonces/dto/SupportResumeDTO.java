package fr.atexo.annonces.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;


@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class SupportResumeDTO {

    private String libelle;
    private String code;
    private String codeGroupe;
    private String description;


}
