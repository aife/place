package fr.atexo.annonces.boot.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Table(name = "CONSULTATION_FACTURATION")
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter
@Getter
@Entity
@EqualsAndHashCode
public class ConsultationFacturation implements Serializable {
    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "ADRESSE")
    private String adresse;


    @Column(name = "SIP")
    private boolean sip;

    @Column(name = "MAIL")
    private String mail;


}
