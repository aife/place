package fr.atexo.annonces.boot.ws.eforms;


import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("schematron")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SchematronConfiguration {
    private String downloadDir;
    private String resourcesUrl;
    private String resourcesTag;
    private String libelleTag;
    private String internationalizationDir;
    private String prefixZipFile;
}
