package fr.atexo.annonces.boot.oauth;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

/**
 * Configuration du serveur de ressources qui protège toutes les ressources de /api/v2
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/rest/v2/eforms-sdk/**", "/rest/v2/avis-publie/**", "/actuator/**")
                .permitAll()
                .antMatchers("/rest/v2/**").authenticated();
    }

}
