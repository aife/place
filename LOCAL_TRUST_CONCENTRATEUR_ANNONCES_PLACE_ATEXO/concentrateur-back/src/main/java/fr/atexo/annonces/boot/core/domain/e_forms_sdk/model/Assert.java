package fr.atexo.annonces.boot.core.domain.e_forms_sdk.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Assert {
    private String value;
    private String severity;
    private List<ConstraintField> constraints;
}
