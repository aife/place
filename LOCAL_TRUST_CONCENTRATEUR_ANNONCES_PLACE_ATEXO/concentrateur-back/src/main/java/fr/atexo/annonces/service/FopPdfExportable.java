package fr.atexo.annonces.service;

/**
 * Interface pour les classes permettant d'exporter un objet au format PDF via Apache FOP
 */
public interface FopPdfExportable<T> extends PdfExportable<T>, XmlExportable<T> {
}
