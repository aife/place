package fr.atexo.annonces.boot.repository;

import fr.atexo.annonces.boot.entity.ConsultationFacturation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Repository pour accéder aux objets Support
 */
@Repository
public interface ConsultationFacturationRepository extends JpaRepository<ConsultationFacturation, Integer>, JpaSpecificationExecutor<ConsultationFacturation> {

    List<ConsultationFacturation> findBySipAndMailAndAdresse(boolean sip, String mail, String adresse);

}
