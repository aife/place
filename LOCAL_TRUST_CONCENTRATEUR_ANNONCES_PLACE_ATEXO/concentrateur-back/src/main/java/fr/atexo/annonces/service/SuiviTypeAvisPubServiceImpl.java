package fr.atexo.annonces.service;

import com.atexo.annonces.commun.mpe.*;
import eu.europa.publications.resource.schema.ted.r2_0_9.reception.TedEsenders;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.model.FailedAssertRepresentation;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.model.ValidationResult;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.IEFormsNoticeServices;
import fr.atexo.annonces.boot.core.domain.xlsx.model.XlsxAnnoncesStatistiqueHeaderEnum;
import fr.atexo.annonces.boot.core.domain.xlsx.model.XlsxColumn;
import fr.atexo.annonces.boot.core.domain.xlsx.services.IXlsxService;
import fr.atexo.annonces.boot.entity.*;
import fr.atexo.annonces.boot.entity.tncp.SuiviAvisPublie;
import fr.atexo.annonces.boot.repository.*;
import fr.atexo.annonces.boot.ws.docgen.KeyValueRequest;
import fr.atexo.annonces.boot.ws.mpe.model.ConsultationContexte;
import fr.atexo.annonces.dto.*;
import fr.atexo.annonces.mapper.*;
import fr.atexo.annonces.modeles.*;
import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.util.Base64;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.RevisionType;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static fr.atexo.annonces.config.EFormsHelper.getObjectFromString;

@Component
@Slf4j
public class SuiviTypeAvisPubServiceImpl implements SuiviTypeAvisPubService {


    private final SuiviAnnonceRepository suiviAnnonceRepository;
    private final OffreRepository offreRepository;
    private final IXlsxService xlsxService;
    private final AgentService agentService;
    private final SuiviAnnonceService suiviAnnonceService;
    private final SuiviTypeAvisPubRepository suiviTypeAvisPubRepository;
    private final SuiviTypeAvisPubPieceJointeAssoRepository suiviTypeAvisPubPieceJointeAssoRepository;
    private final SuiviTypeAvisPubMapper suiviTypeAvisPubMapper;
    private final SuiviAnnonceMapper suiviAnnonceMapper;
    private final SuiviTypeAvisPubPieceJointeAssoMapper suiviTypeAvisPubPieceJointeAssoMapper;
    private final PlateformeConfigurationRepository plateformeConfigurationRepository;
    private final EspaceDocumentaireService espaceDocumentaireService;
    private final SupportRepository supportRepository;
    private final IEFormsNoticeServices ieFormsNoticeServices;
    private final EntityManagerFactory entityManagerFactory;
    private final ReferentielMpeService referentielMpeService;

    private final TypeProcedureRepository typeProcedureRepository;
    private final ConsultationFacturationRepository consultationFacturationRepository;

    private final ConsultationFacturationMapper consultationFacturationMapper;
    private final OffreMapper offreMapper;
    private final OrganismeService organismeService;
    private final PieceJointeService pieceJointeService;

    @Value("classpath:templates/export_avis_dashboard.xlsx")
    Resource resourceExcelFile;
    @Value("${annonces.url}")
    private String urlEforms;

    public SuiviTypeAvisPubServiceImpl(SuiviAnnonceRepository suiviAnnonceRepository, OffreRepository offreRepository, IXlsxService xlsxService, AgentService agentService, SuiviAnnonceService suiviAnnonceService, SuiviTypeAvisPubRepository suiviTypeAvisPubRepository, SuiviTypeAvisPubPieceJointeAssoRepository suiviTypeAvisPubPieceJointeAssoRepository, SuiviTypeAvisPubMapper suiviTypeAvisPubMapper, SuiviAnnonceMapper suiviAnnonceMapper, PlateformeConfigurationRepository plateformeConfigurationRepository, SuiviTypeAvisPubPieceJointeAssoMapper suiviTypeAvisPubPieceJointeAssoMapper, EspaceDocumentaireService espaceDocumentaireService, SupportRepository supportRepository, IEFormsNoticeServices ieFormsNoticeServices, EntityManagerFactory entityManagerFactory, ReferentielMpeService referentielMpeService, TypeProcedureRepository typeProcedureRepository, ConsultationFacturationRepository consultationFacturationRepository, ConsultationFacturationMapper consultationFacturationMapper, OffreMapper offreMapper, OrganismeService organismeService, PieceJointeService pieceJointeService) {
        this.suiviAnnonceRepository = suiviAnnonceRepository;
        this.offreRepository = offreRepository;
        this.xlsxService = xlsxService;
        this.agentService = agentService;
        this.suiviAnnonceService = suiviAnnonceService;
        this.suiviTypeAvisPubRepository = suiviTypeAvisPubRepository;
        this.suiviTypeAvisPubPieceJointeAssoRepository = suiviTypeAvisPubPieceJointeAssoRepository;
        this.suiviTypeAvisPubMapper = suiviTypeAvisPubMapper;
        this.suiviAnnonceMapper = suiviAnnonceMapper;
        this.plateformeConfigurationRepository = plateformeConfigurationRepository;
        this.suiviTypeAvisPubPieceJointeAssoMapper = suiviTypeAvisPubPieceJointeAssoMapper;
        this.espaceDocumentaireService = espaceDocumentaireService;
        this.supportRepository = supportRepository;
        this.ieFormsNoticeServices = ieFormsNoticeServices;
        this.entityManagerFactory = entityManagerFactory;
        this.referentielMpeService = referentielMpeService;
        this.typeProcedureRepository = typeProcedureRepository;
        this.consultationFacturationRepository = consultationFacturationRepository;
        this.consultationFacturationMapper = consultationFacturationMapper;
        this.offreMapper = offreMapper;
        this.organismeService = organismeService;
        this.pieceJointeService = pieceJointeService;
    }


    @Override
    public PageDTO<SuiviTypeAvisPubDTO> getFormulaires(String plateforme, DashboardSuiviTypeAvisSpecification specification, Pageable pageable) {
        Page<SuiviTypeAvisPub> annonces = suiviTypeAvisPubRepository.findAll(specification, pageable);
        PageDTO<SuiviTypeAvisPubDTO> consultationFacturationDTOPageDTO = suiviTypeAvisPubMapper.mapToPageModel(annonces);
        consultationFacturationDTOPageDTO.getContent().forEach(facturationDTO -> facturationDTO.setUrlEforms(urlEforms));
        return consultationFacturationDTOPageDTO;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SuiviTypeAvisPubDTO updateValidationSip(String plateforme, Long id, AvisValidationPatch patch, String bearer) {
        SuiviTypeAvisPub typeAvisPub = suiviTypeAvisPubRepository.findById(id).orElseThrow(() -> new AtexoException(ExceptionEnum.NOT_FOUND, "Avis non trouvée " + id));
        if (!typeAvisPub.getOrganisme().getPlateformeUuid().equals(plateforme)) {
            throw new AtexoException(ExceptionEnum.FORBIDDEN, "Avis hors périmetre");
        }
        ConsultationFacturation facturation = typeAvisPub.getFacturation();
        if (!facturation.isSip()) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Validation SIP non requise");
        }

        boolean valid = patch.isValid();
        if (!valid && !StringUtils.hasText(patch.getRaisonRefus())) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Le message de rejet est obligatoire");
        }
        typeAvisPub.setValidateurSip(agentService.findByToken(bearer));
        if (!valid) {
            typeAvisPub.setRaisonRefus(patch.getRaisonRefus());
        }
        typeAvisPub.setValiderSip(valid);
        ZonedDateTime now = ZonedDateTime.now();
        typeAvisPub.setDateValidationSip(now);
        if (valid) {
            ZonedDateTime dateValidation = typeAvisPub.getDateValidationSip();
            setDateMiseEnLigne(typeAvisPub, dateValidation);
        } else {
            typeAvisPub.setStatut(StatutTypeAvisAnnonceEnum.REJETE_SIP);
            typeAvisPub.setDateModificationStatut(ZonedDateTime.now());
        }
        typeAvisPub = suiviTypeAvisPubRepository.save(typeAvisPub);

        updateAnnoncesFromTypeAvis(typeAvisPub, bearer);

        return suiviTypeAvisPubMapper.mapToModel(typeAvisPub);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SuiviTypeAvisPubDTO updateValidationEco(String plateforme, Long id, AvisValidationPatch patch, String bearer) {
        SuiviTypeAvisPub avisPub = suiviTypeAvisPubRepository.findById(id).orElseThrow(() -> new AtexoException(ExceptionEnum.NOT_FOUND, "Avis non trouvée " + id));
        if (!avisPub.getOrganisme().getPlateformeUuid().equals(plateforme)) {
            throw new AtexoException(ExceptionEnum.FORBIDDEN, "Avis hors périmetre");
        }
        ConsultationFacturation facturation = avisPub.getFacturation();
        if (facturation == null && avisPub.getOffreRacine().getOffreAssociee() != null) {
            throw new AtexoException(ExceptionEnum.FORBIDDEN, "Facturation non renseignée");
        }
        boolean valid = patch.isValid();


        if (!valid && !StringUtils.hasText(patch.getRaisonRefus())) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Le message de rejet est obligatoire");
        }
        avisPub.setValidateurEco(agentService.findByToken(bearer));
        avisPub.setValiderEco(valid);
        if (!valid) {
            avisPub.setRaisonRefus(patch.getRaisonRefus());
        }
        ZonedDateTime now = ZonedDateTime.now();
        avisPub.setDateValidationEco(now);

        ZonedDateTime dateMiseEnLigneCalcule = avisPub.getDateMiseEnLigneCalcule();
        if (valid) {
            if (facturation != null && facturation.isSip()) {
                avisPub.setStatut(StatutTypeAvisAnnonceEnum.EN_ATTENTE_VALIDATION_SIP);
                avisPub.setDateModificationStatut(ZonedDateTime.now());
            } else {
                ZonedDateTime dateValidation = avisPub.getDateValidationEco();
                setDateMiseEnLigne(avisPub, dateValidation);
            }
        } else {
            avisPub.setStatut(StatutTypeAvisAnnonceEnum.REJETE_ECO);
            avisPub.setDateModificationStatut(ZonedDateTime.now());
            if (dateMiseEnLigneCalcule != null && dateMiseEnLigneCalcule.isAfter(ZonedDateTime.now())) {
                log.info("Date de mise en ligne calculée après la date de rejet, suppression de la date de mise en ligne calculée");
                avisPub.setDateMiseEnLigneCalcule(null);
                Long avisPubId = avisPub.getId();
                List<SuiviTypeAvisPub> avisPubs = suiviTypeAvisPubRepository.findByOrganismePlateformeUuidAndIdConsultation(avisPub.getOrganisme().getPlateformeUuid(), avisPub.getIdConsultation())
                        .stream().filter(suiviTypeAvisPub -> !suiviTypeAvisPub.getId().equals(avisPubId))
                        .filter(suiviTypeAvisPub -> suiviTypeAvisPub.getDateMiseEnLigneCalcule() != null)
                        .peek(suiviTypeAvisPub -> suiviTypeAvisPub.setDateMiseEnLigneCalcule(null))
                        .toList();
                if (!CollectionUtils.isEmpty(avisPubs)) {
                    suiviTypeAvisPubRepository.saveAll(avisPubs);
                }
            }
        }

        avisPub = suiviTypeAvisPubRepository.save(avisPub);
        if (dateMiseEnLigneCalcule != null) {
            Long avisPubId = avisPub.getId();
            List<SuiviTypeAvisPub> avisPubs = suiviTypeAvisPubRepository.findByOrganismePlateformeUuidAndIdConsultation(avisPub.getOrganisme().getPlateformeUuid(), avisPub.getIdConsultation())
                    .stream().filter(suiviTypeAvisPub -> !suiviTypeAvisPub.getId().equals(avisPubId))
                    .filter(suiviTypeAvisPub -> suiviTypeAvisPub.getDateMiseEnLigneCalcule() == null)
                    .peek(suiviTypeAvisPub -> suiviTypeAvisPub.setDateMiseEnLigneCalcule(dateMiseEnLigneCalcule))
                    .toList();
            if (!CollectionUtils.isEmpty(avisPubs)) {
                suiviTypeAvisPubRepository.saveAll(avisPubs);
            }

        }

        updateAnnoncesFromTypeAvis(avisPub, bearer);

        return suiviTypeAvisPubMapper.mapToModel(avisPub);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SuiviTypeAvisPubDTO invalidationEco(String plateforme, Long id, AvisValidationPatch patch, String bearer) {
        SuiviTypeAvisPub avisPub = suiviTypeAvisPubRepository.findById(id).orElseThrow(() -> new AtexoException(ExceptionEnum.NOT_FOUND, "Avis non trouvée " + id));
        if (!avisPub.getOrganisme().getPlateformeUuid().equals(plateforme)) {
            throw new AtexoException(ExceptionEnum.FORBIDDEN, "Avis hors périmetre");
        }
        ConsultationFacturation facturation = avisPub.getFacturation();
        if (facturation == null && avisPub.getOffreRacine().getOffreAssociee() != null) {
            log.error("Facturation non renseignée");
            throw new AtexoException(ExceptionEnum.FORBIDDEN, "Facturation non renseignée");
        }
        if (Boolean.FALSE.equals(avisPub.getValiderEco())) {
            log.error("Avis déjà rejeté");
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Avis déjà rejeté");
        }
        if (facturation != null && facturation.isSip() && Boolean.FALSE.equals(avisPub.getValiderSip())) {
            log.error("Avis déjà rejeté par le SIP");
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Avis déjà rejeté par le SIP");
        }
        if (!StatutTypeAvisAnnonceEnum.ENVOI_PLANIFIER.equals(avisPub.getStatut())) {
            log.error("Avis non éligible à l'invalidation");
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Avis non éligible à l'invalidation");
        }


        if (!StringUtils.hasText(patch.getRaisonRefus())) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Le message de rejet est obligatoire");
        }
        avisPub.setInvalidateurEco(agentService.findByToken(bearer));
        avisPub.setInvaliderEco(true);
        avisPub.setRaisonRefus(patch.getRaisonRefus());
        ZonedDateTime now = ZonedDateTime.now();
        avisPub.setDateInvalidationEco(now);

        ZonedDateTime dateMiseEnLigneCalcule = avisPub.getDateMiseEnLigneCalcule();

        avisPub.setStatut(StatutTypeAvisAnnonceEnum.REJETE_ECO);
        avisPub.setDateModificationStatut(ZonedDateTime.now());
        if (dateMiseEnLigneCalcule != null && dateMiseEnLigneCalcule.isAfter(ZonedDateTime.now())) {
            log.info("Date de mise en ligne calculée après la date de rejet, suppression de la date de mise en ligne calculée");
            avisPub.setDateMiseEnLigneCalcule(null);
            Long avisPubId = avisPub.getId();
            List<SuiviTypeAvisPub> avisPubs = suiviTypeAvisPubRepository.findByOrganismePlateformeUuidAndIdConsultation(avisPub.getOrganisme().getPlateformeUuid(), avisPub.getIdConsultation())
                    .stream().filter(suiviTypeAvisPub -> !suiviTypeAvisPub.getId().equals(avisPubId))
                    .filter(suiviTypeAvisPub -> suiviTypeAvisPub.getDateMiseEnLigneCalcule() != null)
                    .peek(suiviTypeAvisPub -> suiviTypeAvisPub.setDateMiseEnLigneCalcule(null))
                    .toList();
            if (!CollectionUtils.isEmpty(avisPubs)) {
                suiviTypeAvisPubRepository.saveAll(avisPubs);
            }
        }
        avisPub = suiviTypeAvisPubRepository.save(avisPub);

        updateAnnoncesFromTypeAvis(avisPub, bearer);

        return suiviTypeAvisPubMapper.mapToModel(avisPub);
    }

    @Override
    public SuiviTypeAvisPubDTO demandeValidation(Long id, Integer idConsultation, String idPlatform, String token) {
        ConfigurationConsultationDTO config = agentService.getConfiguration(token, idConsultation, idPlatform);
        if (config == null || config.getContexte() == null || config.getContexte().getConsultation() == null) {
            throw new AtexoException(ExceptionEnum.FORBIDDEN, "Aucun contexte est associé à cette cette consultation");
        }

        SuiviTypeAvisPub typeAvisPub = getSuiviTypeAvisPub(id, idPlatform, idConsultation);
        if (!StatutTypeAvisAnnonceEnum.BROUILLON.equals(typeAvisPub.getStatut())) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Avis en cours de validation");
        }
        ConsultationMpe consultation = config.getContexte().getConsultation();
        if (!Boolean.TRUE.equals(consultation.getEtatValidation())) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Vous ne pouvez pas transmettre l'avis de publicité \"" + typeAvisPub.getOffreRacine().getLibelle() + "\" car la consultation n'a pas été validée.");
        }

        List<SuiviAnnonce> annonces = typeAvisPub.getAnnonces();
        if (annonces.stream().noneMatch(suiviAnnonce -> suiviAnnonce.getSupport() != null && suiviAnnonce.getSupport().isEuropeen() && suiviAnnonce.getAvisFichier() != null)) {
            List<ValidationResult> validationResults = annonces.stream().filter(annonce -> annonce.getFormulaire() != null).map(annonce -> {
                EFormsFormulaireEntity formulaire = annonce.getFormulaire();
                String formulaireBase64 = formulaire.getFormulaire();
                return ieFormsNoticeServices.verify(formulaire.getId(), getObjectFromString(new String(org.apache.commons.net.util.Base64.decodeBase64(formulaireBase64)), Object.class), annonce.getIdPlatform(), "static");
            }).filter(validationResult -> !validationResult.isValid()).toList();
            if (!CollectionUtils.isEmpty(validationResults)) {
                throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Le formulaire européen n'est pas valide : " + validationResults.stream().map(ValidationResult::getErrors).flatMap(List::stream).map(FailedAssertRepresentation::getText).collect(Collectors.joining(", ")));
            }
        }

        typeAvisPub.setStatut(StatutTypeAvisAnnonceEnum.EN_ATTENTE_VALIDATION_ECO);
        typeAvisPub.setDateModificationStatut(ZonedDateTime.now());
        typeAvisPub.setValidateur(agentService.findByToken(token));
        typeAvisPub.setDateValidation(ZonedDateTime.now());
        typeAvisPub = suiviTypeAvisPubRepository.save(typeAvisPub);
        this.updateAnnoncesFromTypeAvis(typeAvisPub, token);
        return suiviTypeAvisPubMapper.mapToModel(typeAvisPub);
    }

    @Override
    public ByteArrayOutputStream exportStatistiqueSuiviAnnonces(HttpServletResponse response, String plateforme, Boolean sip) {
        List<AnnonceStatistique> annonceStatistiques = this.getStatistiqueSuiviAnnonces(plateforme, sip);
        try (XSSFWorkbook workbook = new XSSFWorkbook()) {
            XSSFSheet xssfSheet = xlsxService.createXLSX(workbook, "Avis Nationaux", XlsxAnnoncesStatistiqueHeaderEnum.transform(sip));

            xlsxService.setData(xssfSheet, getXlsxData(annonceStatistiques.stream().filter(annonceStatistique -> !Boolean.TRUE.equals(annonceStatistique.getOffre().getEuropeen())).collect(Collectors.toList()), sip), 1);

            xlsxService.autoSize(xssfSheet, XlsxAnnoncesStatistiqueHeaderEnum.values().length);

            xssfSheet = xlsxService.createXLSX(workbook, "Avis Européens", XlsxAnnoncesStatistiqueHeaderEnum.transform(sip));

            xlsxService.setData(xssfSheet, getXlsxData(annonceStatistiques.stream().filter(annonceStatistique -> Boolean.TRUE.equals(annonceStatistique.getOffre().getEuropeen())).collect(Collectors.toList()), sip), 1);

            xlsxService.autoSize(xssfSheet, XlsxAnnoncesStatistiqueHeaderEnum.values().length);

            ByteArrayOutputStream outFile = new ByteArrayOutputStream();
            workbook.write(outFile);
            return outFile;
        } catch (IOException e) {
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, "Erreur lors de l'export " + e.getMessage(), e);
        }
    }

    @Override
    public List<AnnonceStatistique> getStatistiqueSuiviAnnonces(String idPlatform, Boolean sip) {
        DashboardSuiviTypeAvisSpecification annonceSpecification = new DashboardSuiviTypeAvisSpecification(Set.of(idPlatform), null, null, null, null, null, null, null, sip, null, null, null, null, null, null, null, null, null, null, null, null, null);
        List<SuiviTypeAvisPub> suiviAnnonces = suiviTypeAvisPubRepository.findAll(annonceSpecification);
        Map<TypeAvisPubDTO, List<SuiviTypeAvisPub>> collect = suiviAnnonces.stream().filter(suiviAnnonce -> suiviAnnonce.getStatut() != StatutTypeAvisAnnonceEnum.BROUILLON).collect(Collectors.groupingBy(obj -> TypeAvisPubDTO.builder().europeen(obj.isEuropeen()).libelle(obj.getOffreRacine().getLibelle()).code(obj.getOffreRacine().getCode()).build()));
        return collect.entrySet().stream().map(stringListEntry -> {
            var list = stringListEntry.getValue().stream().collect(Collectors.groupingBy(SuiviTypeAvisPub::getStatut, Collectors.counting())).entrySet().stream().map(statutSuiviAnnonceEnumLongEntry -> new StatutStatistique(statutSuiviAnnonceEnumLongEntry.getKey(), statutSuiviAnnonceEnumLongEntry.getValue())).collect(Collectors.toList());
            return new AnnonceStatistique(stringListEntry.getKey(), list);
        }).toList();
    }

    @Override
    public List<SuiviTypeAvisPubDTO> getConsultationAnnonces(String plateforme, Integer idConsultation) {
        return suiviTypeAvisPubMapper.mapToModels(suiviTypeAvisPubRepository.findByOrganismePlateformeUuidAndIdConsultationAndChildrenIsEmpty(plateforme, idConsultation));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SuiviTypeAvisPubDTO addSuiviTypeAvisPub(SuiviTypeAvisPubAdd avisPub, String token, String plateforme) {
        if (avisPub == null || avisPub.getOffreRacine() == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Type d'avis obligatoire");
        }
        var agent = agentService.findByToken(token);
        if (agent == null) {
            throw new AtexoException(ExceptionEnum.FORBIDDEN, "Agent non trouvé");
        }
        Organisme agentOrganisme = agent.getOrganisme();
        if (!plateforme.equals(agentOrganisme.getPlateformeUuid())) {
            throw new AtexoException(ExceptionEnum.FORBIDDEN, "Hors périmetre");
        }
        OffreDTO offreDTO = avisPub.getOffreRacine();

        Offre offre = offreRepository.findByCode(offreDTO.getCode()).orElseThrow(() -> new AtexoException(ExceptionEnum.NOT_FOUND, "Offre non trouvée"));
        Offre offreAssociee = offre.getOffreAssociee();
        boolean withNational = offreAssociee != null || !avisPub.isEuropeen();
        Offre offreNational = avisPub.isEuropeen() ? offreAssociee : offre;
        if (avisPub.getFacturation() == null && withNational) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Facturation obligatoire");
        }


        List<JalDTO> jalList = avisPub.getJalList();

        if (CollectionUtils.isEmpty(jalList) && withNational) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Support obligatoire");
        }
        SuiviTypeAvisPub avisPubEntity = suiviTypeAvisPubMapper.mapToEntity(avisPub);
        avisPubEntity.setCreerPar(agent);
        avisPubEntity.setModifierPar(agent);
        avisPubEntity.setStatut(StatutTypeAvisAnnonceEnum.BROUILLON);
        avisPubEntity.setValiderEco(null);
        avisPubEntity.setValiderSip(null);
        avisPubEntity.setDateCreation(ZonedDateTime.now());
        avisPubEntity.setDateModification(ZonedDateTime.now());
        avisPubEntity.setOffreRacine(offre);
        if (avisPub.getDateMiseEnLigneCalcule() != null) {
            avisPubEntity.setDateMiseEnLigneCalcule(avisPub.getDateMiseEnLigneCalcule());
        } else {
            suiviTypeAvisPubRepository.findMaxDateMiseEnLigneCalcule(avisPub.getIdConsultation(), agent.getOrganisme().getPlateformeUuid()).ifPresent(avisPubEntity::setDateMiseEnLigneCalcule);
        }
        if (withNational) {
            avisPubEntity.setMailObjet("Avis d’adjudication " + avisPubEntity.getReference() + " à publier dans [organe de presse]");
            avisPubEntity.setMailContenu("Mesdames, Messieurs,\n" + "Veuillez trouver ci-joint un " + offreNational.getLibelle() + " à faire publier dans votre prochaine édition.");
        }

        Organisme organisme = null;
        OrganismeMpe organismeMpe = avisPub.getOrganisme();
        if (organismeMpe != null) {
            organisme = this.organismeService.savePlateformeOrganismeIfNotExist(organismeMpe, agentOrganisme.getPlateformeUrl(), agentOrganisme.getPlateformeUuid());
            avisPubEntity.setOrganisme(organisme);
        }
        ServiceMpe directionService = avisPub.getService();
        if (directionService != null) {
            ReferentielServiceMpe service = ReferentielServiceMpe.builder().idMpe(directionService.getId()).libelle(directionService.getLibelle()).organisme(organisme).code(directionService.getSigle()).actif(true).build();
            service = referentielMpeService.getReferential(service);
            avisPubEntity.setService(service);
        }
        ReferentielMpe typeProcedure = avisPub.getProcedure();
        if (typeProcedure != null && StringUtils.hasText(typeProcedure.getAbreviation()) && StringUtils.hasText(typeProcedure.getLibelle())) {
            TypeProcedure procedure = typeProcedureRepository.findByAbbreviationAndLibelle(typeProcedure.getAbreviation(), typeProcedure.getLibelle());
            if (procedure == null) {
                procedure = new TypeProcedure();
                procedure.setAbbreviation(typeProcedure.getAbreviation());
                procedure.setLibelle(typeProcedure.getLibelle());
                procedure.setAvisExterne(false);
                procedure = typeProcedureRepository.save(procedure);

            }
            avisPubEntity.setProcedure(procedure);

        }
        ConsultationFacturation facturation = avisPubEntity.getFacturation();
        if (facturation != null) {
            List<ConsultationFacturation> savedFacturation = consultationFacturationRepository.findBySipAndMailAndAdresse(facturation.isSip(), facturation.getMail(), facturation.getAdresse());
            if (CollectionUtils.isEmpty(savedFacturation)) {
                facturation = consultationFacturationRepository.save(facturation);
                avisPubEntity.setFacturation(facturation);
            } else {
                avisPubEntity.setFacturation(savedFacturation.get(0));
            }
        }


        SuiviTypeAvisPub save = suiviTypeAvisPubRepository.save(avisPubEntity);
        Organisme organisme1 = save.getOrganisme();
        if (!CollectionUtils.isEmpty(jalList) && withNational) {
            final var offreToAdd = offreAssociee != null ? offreAssociee : offre;
            PlateformeConfiguration plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(plateforme);
            List<Support> supports = this.addSupportJal(jalList, plateformeConfiguration, offreToAdd);
            if (!CollectionUtils.isEmpty(supports)) {
                supports.forEach(support -> {
                    SuiviAnnonceDTO annonceDTO = SuiviAnnonceDTO.builder().idPlatform(plateforme).statut(StatutSuiviAnnonceEnum.BROUILLON).idConsultation(avisPub.getIdConsultation()).offre(OffreDTO.builder().code(offreToAdd.getCode()).build()).idTypeAvisPub(save.getId()).organismeMpe(organisme1 == null ? null : OrganismeResumeDTO.builder().id(organisme1.getId()).build()).organisme(organisme1 == null ? null : organisme1.getAcronymeOrganisme()).build();
                    suiviAnnonceService.createOrUpdateSuiviAnnonce(plateforme, avisPub.getIdConsultation(), support.getCode(), offreToAdd.getCode(), annonceDTO, token);
                });
            }
        }

        if (avisPub.isEuropeen()) {
            SuiviAnnonceDTO annonceDTO = SuiviAnnonceDTO.builder().idPlatform(plateforme).statut(StatutSuiviAnnonceEnum.BROUILLON).idConsultation(avisPub.getIdConsultation()).offre(OffreDTO.builder().code(offre.getCode()).build()).idTypeAvisPub(save.getId()).organismeMpe(organisme1 == null ? null : OrganismeResumeDTO.builder().id(organisme1.getId()).build()).organisme(organisme1 == null ? null : organisme1.getAcronymeOrganisme()).build();
            suiviAnnonceService.createOrUpdateSuiviAnnonce(plateforme, avisPub.getIdConsultation(), "ENOTICES", offre.getCode(), annonceDTO, token);
        }
        return suiviTypeAvisPubMapper.mapToModel(save);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SuiviTypeAvisPubDTO modifySuiviTypeAvisPub(Long id, SuiviTypeAvisPubPatch avisPubPatch, String token, String plateforme, Integer idConsultation) {


        if (avisPubPatch == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Type d'avis obligatoire");
        }
        SuiviTypeAvisPub avisPubEntity = getSuiviTypeAvisPub(id, plateforme, idConsultation);
        Offre offre = avisPubEntity.getOffreRacine();
        Offre offreAssociee = offre.getOffreAssociee();
        boolean withNational = offreAssociee != null || !avisPubEntity.isEuropeen();
        if (!withNational) {
            throw new AtexoException(ExceptionEnum.FORBIDDEN, "Avis non modifiable");
        }

        ConsultationFacturationDTO facturation = avisPubPatch.getFacturation();
        if (facturation != null) {
            var savedFacturation = avisPubEntity.getFacturation();
            if (savedFacturation != null && (!savedFacturation.getAdresse().equals(facturation.getAdresse()) || !savedFacturation.getMail().equals(facturation.getMail()) || savedFacturation.isSip() != facturation.isSip())) {

                ConsultationFacturation consultationFacturation = consultationFacturationMapper.mapToEntity(facturation);
                List<ConsultationFacturation> toSavedFacturation = consultationFacturationRepository.findBySipAndMailAndAdresse(facturation.isSip(), facturation.getMail(), facturation.getAdresse());
                if (CollectionUtils.isEmpty(toSavedFacturation)) {
                    consultationFacturation = consultationFacturationRepository.save(consultationFacturation);
                    avisPubEntity.setFacturation(consultationFacturation);
                } else {
                    avisPubEntity.setFacturation(toSavedFacturation.get(0));
                }
                savedFacturation = consultationFacturationRepository.save(consultationFacturation);
                avisPubEntity.setFacturation(savedFacturation);
                avisPubEntity = suiviTypeAvisPubRepository.save(avisPubEntity);
            }
        }

        Organisme organisme1 = avisPubEntity.getOrganisme();

        List<JalDTO> jalList = avisPubPatch.getJalList();
        List<SuiviAnnonce> annonces = avisPubEntity.getNationalAnnonces();
        if (!CollectionUtils.isEmpty(jalList)) {
            final var offreToAdd = offreAssociee != null ? offreAssociee : offre;
            PlateformeConfiguration plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(plateforme);
            List<Support> supports = this.addSupportJal(jalList, plateformeConfiguration, offreToAdd);
            if (!CollectionUtils.isEmpty(supports)) {
                PieceJointe pieceJointe = avisPubEntity.getAvisNational();
                List<SuiviAnnonce> finalAnnonces = annonces.stream().map(suiviAnnonce -> {
                    if (!Boolean.TRUE.equals(suiviAnnonce.getOffre().getAssociationJal())) {
                        return suiviAnnonce;
                    }
                    var exist = supports.stream().anyMatch(support -> suiviAnnonce.getSupport().getId().equals(support.getId()));
                    if (!exist) {
                        suiviAnnonceService.deleteSuiviAnnonce(suiviAnnonce.getId(), suiviAnnonce.getIdPlatform(), suiviAnnonce.getIdConsultation());
                        return null;
                    } else {
                        suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.BROUILLON);
                        suiviAnnonce.setDateValidation(null);
                        return suiviAnnonceRepository.save(suiviAnnonce);
                    }
                }).filter(Objects::nonNull).toList();
                SuiviTypeAvisPub finalAvisPubEntity = avisPubEntity;
                List<SuiviAnnonce> suiviAnnonces = new ArrayList<>(supports.stream().map(support -> {
                    var exist = finalAnnonces.stream().anyMatch(suiviAnnonce -> suiviAnnonce.getSupport().getId().equals(support.getId()));
                    if (!exist) {
                        var result = suiviAnnonceService.createOrUpdateSuiviAnnonce(plateforme, finalAvisPubEntity.getIdConsultation(), support.getCode(), offreToAdd.getCode(), SuiviAnnonceDTO.builder().idPlatform(plateforme).statut(StatutSuiviAnnonceEnum.BROUILLON).idConsultation(finalAvisPubEntity.getIdConsultation()).offre(OffreDTO.builder().code(offreToAdd.getCode()).build()).statut(StatutSuiviAnnonceEnum.getStatutSuiviAnnonceFromTypeAvis(finalAvisPubEntity.getStatut(), false, finalAvisPubEntity.getDateValidationNational() != null)).idTypeAvisPub(finalAvisPubEntity.getId()).organismeMpe(organisme1 == null ? null : OrganismeResumeDTO.builder().id(organisme1.getId()).build()).organisme(organisme1 == null ? null : organisme1.getAcronymeOrganisme()).build(), token);
                        Integer id1 = result.getDtoObject().getId();
                        if (pieceJointe != null) {
                            suiviAnnonceService.addPieceJointe(id1, pieceJointe);
                        }
                        return suiviAnnonceRepository.findById(id1).orElse(null);
                    } else {
                        SuiviAnnonce annonce = finalAnnonces.stream().filter(suiviAnnonce -> suiviAnnonce.getSupport().getId().equals(support.getId())).findFirst().orElseThrow(() -> new AtexoException(ExceptionEnum.NOT_FOUND, "Annonce non trouvée"));
                        annonce.setDateValidation(null);
                        annonce.setStatut(StatutSuiviAnnonceEnum.BROUILLON);
                        return suiviAnnonceRepository.save(annonce);
                    }
                }).filter(Objects::nonNull).toList());
                SuiviAnnonce europeenAnnonce = avisPubEntity.getEuropeenAnnonce();
                if (europeenAnnonce != null) {
                    suiviAnnonces.add(europeenAnnonce);
                }
                avisPubEntity.setAnnonces(suiviAnnonces);
            }
        }
        avisPubEntity.setDateValidationNational(null);
        avisPubEntity.setStatutRedaction(null);
        avisPubEntity.setStatutEdition(null);
        avisPubEntity.setTokenEdition(null);
        return suiviTypeAvisPubMapper.mapToModel(suiviTypeAvisPubRepository.save(avisPubEntity));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SuiviTypeAvisPubDTO reprendre(Long id, String token, String plateforme, Integer idConsultation) {

        SuiviTypeAvisPub avisPubEntity = getSuiviTypeAvisPub(id, plateforme, idConsultation);
        List<SuiviTypeAvisPubPieceJointeAssoEntity> mailPiecejointeList = avisPubEntity.getMailPiecejointeList();
        SuiviTypeAvisPub cloneAvis = SuiviTypeAvisPub.builder().statut(StatutTypeAvisAnnonceEnum.BROUILLON)
                .dateMiseEnLigneCalcule(avisPubEntity.getDateMiseEnLigneCalcule())
                .dlro(avisPubEntity.getDlro()).dateMiseEnLigne(avisPubEntity.getDateMiseEnLigne()).mailContenu(avisPubEntity.getMailContenu()).mailObjet(avisPubEntity.getMailObjet()).idConsultation(avisPubEntity.getIdConsultation()).organisme(avisPubEntity.getOrganisme()).service(avisPubEntity.getService()).procedure(avisPubEntity.getProcedure()).dateMiseEnLigne(avisPubEntity.getDateMiseEnLigne()).europeen(avisPubEntity.isEuropeen()).facturation(avisPubEntity.getFacturation()).offreRacine(avisPubEntity.getOffreRacine()).titre(avisPubEntity.getTitre()).reference(avisPubEntity.getReference()).objet(avisPubEntity.getObjet()).parent(avisPubEntity).build();
        cloneAvis = suiviTypeAvisPubRepository.save(cloneAvis);
        SuiviTypeAvisPub finalCloneAvis = cloneAvis;
        if (!CollectionUtils.isEmpty(mailPiecejointeList)) {
            cloneAvis.setMailPiecejointeList(mailPiecejointeList.stream().map(suiviTypeAvisPubPieceJointeAssoEntity -> suiviTypeAvisPubPieceJointeAssoEntity.toBuilder().suiviTypeAvisPub(finalCloneAvis).pieceJointe(pieceJointeService.clone(suiviTypeAvisPubPieceJointeAssoEntity.getPieceJointe())).build()).filter(suiviTypeAvisPubPieceJointeAssoEntity -> suiviTypeAvisPubPieceJointeAssoEntity.getPieceJointe() != null).map(suiviTypeAvisPubPieceJointeAssoRepository::save).toList());
        }
        List<SuiviAnnonce> annonces = avisPubEntity.getAnnonces();
        if (!CollectionUtils.isEmpty(annonces)) {
            List<SuiviAnnonce> suiviAnnonces = annonces.stream().map(suiviAnnonce -> suiviAnnonceService.reprendre(suiviAnnonce, finalCloneAvis, token)).toList();
            cloneAvis.setAnnonces(suiviAnnonces);
        }
        return suiviTypeAvisPubMapper.mapToModel(cloneAvis);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SuiviTypeAvisPubDTO change(Long id, String token, String plateforme, Integer idConsultation) {
        SuiviTypeAvisPub avisPubEntity = getSuiviTypeAvisPub(id, plateforme, idConsultation);
        List<SuiviTypeAvisPubPieceJointeAssoEntity> mailPiecejointeList = avisPubEntity.getMailPiecejointeList();
        SuiviTypeAvisPub cloneAvis = SuiviTypeAvisPub.builder()
                .statut(StatutTypeAvisAnnonceEnum.BROUILLON)
                .dlro(avisPubEntity.getDlro())
                .dateMiseEnLigne(avisPubEntity.getDateMiseEnLigne())
                .dateMiseEnLigneCalcule(avisPubEntity.getDateMiseEnLigneCalcule())
                .mailContenu(avisPubEntity.getMailContenu())
                .mailObjet(avisPubEntity.getMailObjet())
                .idConsultation(avisPubEntity.getIdConsultation())
                .organisme(avisPubEntity.getOrganisme())
                .service(avisPubEntity.getService())
                .procedure(avisPubEntity.getProcedure())
                .europeen(avisPubEntity.isEuropeen())
                .facturation(avisPubEntity.getFacturation())
                .offreRacine(avisPubEntity.getOffreRacine())
                .titre(avisPubEntity.getTitre())
                .reference(avisPubEntity.getReference())
                .objet(avisPubEntity.getObjet())
                .parent(avisPubEntity)
                .build();
        cloneAvis = suiviTypeAvisPubRepository.save(cloneAvis);
        SuiviTypeAvisPub finalCloneAvis = cloneAvis;
        if (!CollectionUtils.isEmpty(mailPiecejointeList)) {
            cloneAvis.setMailPiecejointeList(mailPiecejointeList.stream().map(suiviTypeAvisPubPieceJointeAssoEntity -> suiviTypeAvisPubPieceJointeAssoEntity.toBuilder().suiviTypeAvisPub(finalCloneAvis).pieceJointe(pieceJointeService.clone(suiviTypeAvisPubPieceJointeAssoEntity.getPieceJointe())).build()).map(suiviTypeAvisPubPieceJointeAssoRepository::save).toList());
        }
        List<SuiviAnnonce> annonces = avisPubEntity.getAnnonces();
        if (!CollectionUtils.isEmpty(annonces)) {
            List<SuiviAnnonce> suiviAnnonces = annonces.stream().map(suiviAnnonce -> suiviAnnonceService.change(suiviAnnonce, finalCloneAvis, token)).toList();
            cloneAvis.setAnnonces(suiviAnnonces);
        }
        return suiviTypeAvisPubMapper.mapToModel(cloneAvis);
    }

    @Override
    public SuiviTypeAvisPubDTO get(Long id, String idPlatform, Integer idConsultation) {
        SuiviTypeAvisPub avisPubEntity = getSuiviTypeAvisPub(id, idPlatform, idConsultation);
        return suiviTypeAvisPubMapper.mapToModel(avisPubEntity);
    }


    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public List<RevisionDTO<SuiviTypeAvisPubRevisionDTO>> getHistoriques(Long id, String idPlatform, Integer idConsultation) {
        SuiviTypeAvisPub suiviTypeAvisPub = getSuiviTypeAvisPub(id, idPlatform, idConsultation);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        var auditReader = AuditReaderFactory.get(entityManager);
        List<RevisionDTO<SuiviTypeAvisPubRevisionDTO>> revisionDTOS = new ArrayList<>();
        do {
            revisionDTOS.addAll(getRevisions(suiviTypeAvisPub.getId(), true, auditReader));
            suiviTypeAvisPub = suiviTypeAvisPub.getParent();
        } while (suiviTypeAvisPub != null);
        entityManager.clear();
        entityManager.close();
        return revisionDTOS.stream().filter(suiviTypeAvisPubDTORevisionDTO -> suiviTypeAvisPubDTORevisionDTO.getItem().getDateModification() != null).sorted(Comparator.comparing(o -> o.getItem().getDateModification())).toList();
    }


    public List<RevisionDTO<SuiviTypeAvisPubRevisionDTO>> getRevisions(Long id, boolean fetchChanges, AuditReader auditReader) {

        AuditQuery auditQuery;

        if (fetchChanges) {
            auditQuery = auditReader.createQuery().forRevisionsOfEntityWithChanges(SuiviTypeAvisPub.class, false);
        } else {
            auditQuery = auditReader.createQuery().forRevisionsOfEntity(SuiviTypeAvisPub.class, false);
        }
        auditQuery.add(AuditEntity.id().eq(id));
        final List<Object[]> revisions = auditQuery.getResultList();
        final List<RevisionDTO<SuiviTypeAvisPubRevisionDTO>> result = new ArrayList<>();
        revisions.forEach(objects -> {
            SuiviTypeAvisPub buDa = (SuiviTypeAvisPub) objects[0];
            RevisionType revType = (RevisionType) objects[2];
            HashSet<String> champs = (HashSet<String>) objects[3];
            RevisionDTO<SuiviTypeAvisPubRevisionDTO> avisPubDTORevisionDTO = RevisionDTO.<SuiviTypeAvisPubRevisionDTO>builder().item(suiviTypeAvisPubMapper.mapToRevision(buDa)).type(revType.name()).champs(champs).build();
            result.add(avisPubDTORevisionDTO);
        });


        return result;
    }

    @Override
    public EditionRequest modifyAvisDocument(Long id, String plateforme, Integer idConsultation, String token) {

        SuiviTypeAvisPub avisPubEntity = getSuiviTypeAvisPub(id, plateforme, idConsultation);
        var agent = agentService.findByToken(token);
        if (agent == null) {
            throw new AtexoException(ExceptionEnum.FORBIDDEN, "Agent non trouvé");
        }

        List<SuiviAnnonce> annonces = avisPubEntity.getNationalAnnonces();
        PieceJointe document = avisPubEntity.getAvisNational();
        if (document == null || !espaceDocumentaireService.fileExists(document)) {
            PieceJointe pieceJointe = initAvisDocument(annonces.stream().findFirst().orElse(null));
            annonces.forEach(suiviAnnonce -> {
                suiviAnnonce.setAvisFichier(pieceJointe);
                suiviAnnonce = suiviAnnonceRepository.save(suiviAnnonce);
            });

        }
        return this.editDocument(agent, avisPubEntity, token);
    }

    private SuiviTypeAvisPub getSuiviTypeAvisPub(Long id, String plateforme, Integer idConsultation) {
        SuiviTypeAvisPub avisPubEntity = suiviTypeAvisPubRepository.findById(id).orElseThrow(() -> new AtexoException(ExceptionEnum.NOT_FOUND, "Avis non trouvée " + id));

        Organisme agentOrganisme = avisPubEntity.getOrganisme();
        if (!plateforme.equals(agentOrganisme.getPlateformeUuid())) {
            throw new AtexoException(ExceptionEnum.FORBIDDEN, "Hors périmetre");
        }
        if (!idConsultation.equals(avisPubEntity.getIdConsultation())) {
            throw new AtexoException(ExceptionEnum.FORBIDDEN, "Hors périmetre");
        }
        return avisPubEntity;
    }

    @Override
    public TrackDocumentResponse getDocumentCallback(Long id, FileStatus status) {
        log.info("Réception du statut {} de l'annonce {}", status.getStatus(), id);
        SuiviTypeAvisPub typeAvisPub = suiviTypeAvisPubRepository.findById(id).orElse(null);
        if (typeAvisPub == null) {
            return TrackDocumentResponse.builder().error("1").build();
        }
        try {
            this.updateDocumentEdition(status, typeAvisPub);
        } catch (Exception e) {
            log.error("Erreur lors de la mise à jour du document", e);
            return TrackDocumentResponse.builder().error("1").build();
        }
        return TrackDocumentResponse.builder().error("0").build();
    }

    @Override
    public List<SuiviTypeAvisPubPieceJointeAssoDTO> getDocuments(Long id, String idPlatform, Integer idConsultation, String token) {
        SuiviTypeAvisPub avisPub = getSuiviTypeAvisPub(id, idPlatform, idConsultation);
        return suiviTypeAvisPubPieceJointeAssoMapper.mapToModels(suiviTypeAvisPubPieceJointeAssoRepository.findAllBySuiviTypeAvisPubId(avisPub.getId()));
    }

    @Override
    public SuiviTypeAvisPubDTO deletePieceJointeByIdTypeAvisAndIdAsso(Long id, Long idAsso, String idPlatform, Integer idConsultation) {
        SuiviTypeAvisPub typeAvisPub = getSuiviTypeAvisPub(id, idPlatform, idConsultation);
        SuiviTypeAvisPubPieceJointeAssoEntity suiviTypeAvisPubPieceJointeAssoEntity = suiviTypeAvisPubPieceJointeAssoRepository.findById(idAsso).orElse(null);
        if (suiviTypeAvisPubPieceJointeAssoEntity == null) {
            throw new AtexoException(ExceptionEnum.NOT_FOUND, "Le document n'existe pas");
        }
        SuiviTypeAvisPub suiviTypeAvisPub = suiviTypeAvisPubPieceJointeAssoEntity.getSuiviTypeAvisPub();
        if (!suiviTypeAvisPub.getId().equals(typeAvisPub.getId())) {
            throw new AtexoException(ExceptionEnum.FORBIDDEN, "Le document n'est pas associé à l'avis");
        }
        SuiviTypeAvisPubDTO suiviTypeAvisPubDTO = suiviTypeAvisPubMapper.mapToModel(suiviTypeAvisPub);
        suiviTypeAvisPubPieceJointeAssoRepository.deleteById(idAsso);
        pieceJointeService.deleteById(suiviTypeAvisPubPieceJointeAssoEntity.getPieceJointe().getId());
        return suiviTypeAvisPubDTO;
    }

    @Override
    public SuiviTypeAvisPubPieceJointeAssoDTO importerDocument(Long id, String idPlatform, Integer idConsultation, PieceJointe pieceJointe, InputStream inputStream) {
        SuiviTypeAvisPub typeAvisPub = getSuiviTypeAvisPub(id, idPlatform, idConsultation);
        PieceJointe savedPieceJointe = pieceJointeService.save(pieceJointe, inputStream);
        SuiviTypeAvisPubPieceJointeAssoEntity suiviTypeAvisPubPieceJointeAssoEntity = SuiviTypeAvisPubPieceJointeAssoEntity.builder().pieceJointe(savedPieceJointe).suiviTypeAvisPub(typeAvisPub).build();
        return suiviTypeAvisPubPieceJointeAssoMapper.mapToModel(suiviTypeAvisPubPieceJointeAssoRepository.save(suiviTypeAvisPubPieceJointeAssoEntity));
    }

    @Override
    public SuiviTypeAvisPubDTO updateMail(Long id, MailUpdate update, String idPlatform, Integer idConsultation) {
        SuiviTypeAvisPub typeAvisPub = getSuiviTypeAvisPub(id, idPlatform, idConsultation);
        typeAvisPub.setMailObjet(update.getObjet());
        typeAvisPub.setMailContenu(update.getContenu());
        return suiviTypeAvisPubMapper.mapToModel(suiviTypeAvisPubRepository.save(typeAvisPub));
    }

    @Override
    public SuiviTypeAvisPubDTO importerAvisExterne(Long id, String idPlatform, Integer idConsultation, String token, PieceJointe pieceJointe, InputStream inputStream) {

        SuiviTypeAvisPub typeAvisPub = getSuiviTypeAvisPub(id, idPlatform, idConsultation);
        PieceJointe savedPieceJointe = pieceJointeService.save(pieceJointe, inputStream);

        typeAvisPub.getAnnonces().stream().filter(suiviAnnonce -> suiviAnnonce.getSupport().isEuropeen()).forEach(suiviAnnonce -> {
            //suppression de l'ancien avis externe dans table piece jointe
            PieceJointe toDelete = suiviAnnonce.getAvisFichier();
            suiviAnnonce.setAvisFichier(savedPieceJointe);
            suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.COMPLET);
            typeAvisPub.setDateValidationEuropeen(ZonedDateTime.now());
            suiviAnnonceRepository.save(suiviAnnonce);
            if (toDelete != null) pieceJointeService.deleteById(toDelete.getId());
        });
        return suiviTypeAvisPubMapper.mapToModel(suiviTypeAvisPubRepository.save(typeAvisPub));
    }

    @Override
    public SuiviTypeAvisPubDTO deleteAvisExterne(Long id, String idPlatform, Integer idConsultation, String token) {
        SuiviTypeAvisPub suiviTypeAvisPub = getSuiviTypeAvisPub(id, idPlatform, idConsultation);

        suiviTypeAvisPub.setDateValidationEuropeen(null);
        suiviTypeAvisPub.getAnnonces().stream().filter(suiviAnnonce -> suiviAnnonce.getSupport().isEuropeen()).forEach(suiviAnnonce -> {
            if (suiviAnnonce.getAvisFichier() == null) {
                throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucun avis associé");
            }
            PieceJointe toDelete = suiviAnnonce.getAvisFichier();
            suiviAnnonce.setAvisFichier(null);
            suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.BROUILLON);
            suiviTypeAvisPub.setDateValidationEuropeen(null);
            suiviAnnonceRepository.save(suiviAnnonce);
            pieceJointeService.deleteById(toDelete.getId());
        });
        return suiviTypeAvisPubMapper.mapToModel(suiviTypeAvisPubRepository.save(suiviTypeAvisPub));
    }

    @Override
    public PieceJointe getPieceJointe(Long id, Long idPieceJointe, String idPlatform, Integer idConsultation, String bearer) {
        SuiviTypeAvisPub suiviTypeAvisPub = getSuiviTypeAvisPub(id, idPlatform, idConsultation);

        return suiviTypeAvisPub.getMailPiecejointeList().stream().map(SuiviTypeAvisPubPieceJointeAssoEntity::getPieceJointe).filter(pieceJointe -> pieceJointe.getId().equals(idPieceJointe)).findFirst().orElseThrow(() -> new AtexoException(ExceptionEnum.NOT_FOUND, "Pièce jointe non trouvée"));
    }

    @Override
    public SuiviTypeAvisPubDTO fixStatus(String bearer, String idPlatform) {
        List<SuiviTypeAvisPub> typeAvisPubs = suiviTypeAvisPubRepository.findByOrganismePlateformeUuid(idPlatform);

        typeAvisPubs.forEach(avisPub -> {
            List<SuiviAnnonce> annonces = avisPub.getAnnonces();
            annonces.forEach(suiviAnnonce -> suiviAnnonceService.fixStatut(suiviAnnonce, StatutSuiviAnnonceEnum.getStatutSuivisAnnonceFromTypeAvis(avisPub.getStatut())));
        });
        return null;
    }

    @Override
    public List<SuiviOffreDTO> synchronize(String idPlatform, ZonedDateTime dateDebut, ZonedDateTime dateFin) {
        PlateformeConfiguration plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(idPlatform);
        List<SuiviAnnonce> suiviAnnonces;
        if (plateformeConfiguration != null && plateformeConfiguration.isGroupement()) {
            SuiviTypeAvisSpecification dashboardSuiviTypeAvisSpecification = new SuiviTypeAvisSpecification(Set.of(idPlatform), Set.of(StatutTypeAvisAnnonceEnum.REJETE, StatutTypeAvisAnnonceEnum.REJETE_SIP, StatutTypeAvisAnnonceEnum.REJETE_ECO, StatutTypeAvisAnnonceEnum.ENVOYE, StatutTypeAvisAnnonceEnum.ENVOI_PLANIFIER), dateFin, dateDebut);
            suiviAnnonces = getSuiviAnnoncesFromAvis(suiviTypeAvisPubRepository.findAll(dashboardSuiviTypeAvisSpecification));

        } else {
            SuiviAnnonceSpecification specification = new SuiviAnnonceSpecification(Set.of(idPlatform), Set.of(StatutSuiviAnnonceEnum.ENVOI_PLANIFIER, StatutSuiviAnnonceEnum.ARRETER, StatutSuiviAnnonceEnum.EN_COURS_DE_PUBLICATION, StatutSuiviAnnonceEnum.REJETER_CONCENTRATEUR, StatutSuiviAnnonceEnum.REJETER_SUPPORT, StatutSuiviAnnonceEnum.PUBLIER, StatutSuiviAnnonceEnum.REJETER_CONCENTRATEUR_VALIDATION_ECO, StatutSuiviAnnonceEnum.REJETER_CONCENTRATEUR_VALIDATION_SIP), dateFin, dateDebut);
            suiviAnnonces = suiviAnnonceRepository.findAll(specification);
        }
        return getSuiviOffreDTOS(suiviAnnonces);
    }

    private SuiviOffreDTO getSuiviOffreDTO(SuiviAnnonce annonce) {
        SuiviOffreDTO suiviOffreDTO = suiviAnnonceMapper.suiviAnnonceToSuiviOffreDTO(annonce);
        if (StringUtils.hasText(annonce.getOrganismeMpe().getPlateformeUrl()) && (StatutSuiviAnnonceEnum.EN_COURS_DE_PUBLICATION.equals(suiviOffreDTO.getStatut()) || StatutSuiviAnnonceEnum.PUBLIER.equals(suiviOffreDTO.getStatut()))) {
            SuiviTypeAvisPub typeAvisPub = annonce.getTypeAvisPub();
            if (suiviOffreDTO.getId() == 0 && typeAvisPub != null)
                suiviOffreDTO.setLienTelechargement("/concentrateur-annonces/rest/v2/avis-publie/" + typeAvisPub.getId() + "/download?idPlatform=" + annonce.getIdPlatform() + "&idConsultation=" + annonce.getIdConsultation());
            else if (suiviOffreDTO.getId() != 0) {
                if (!StringUtils.hasText(annonce.getLienPublication()) && annonce.getOffre() != null && annonce.getSupport() != null && !annonce.getSupport().isExternal())
                    suiviOffreDTO.setLienTelechargement("/concentrateur-annonces/rest/v2/avis-publie/annonces/" + annonce.getId() + "/download?idPlatform=" + annonce.getIdPlatform() + "&idConsultation=" + annonce.getIdConsultation());
            }
        }
        return suiviOffreDTO;
    }


    @Override
    public List<SuiviOffreDTO> getLastAvis(String idPlatform, Integer idConsultation) {
        PlateformeConfiguration plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(idPlatform);
        List<SuiviAnnonce> suiviAnnonces = new ArrayList<>();
        if (plateformeConfiguration != null && plateformeConfiguration.isGroupement()) {
            SuiviTypeAvisPub avisPub = suiviTypeAvisPubRepository.findByOrganismePlateformeUuidAndIdConsultationAndStatutIn(idPlatform, idConsultation, List.of(StatutTypeAvisAnnonceEnum.REJETE, StatutTypeAvisAnnonceEnum.REJETE_SIP, StatutTypeAvisAnnonceEnum.REJETE_ECO, StatutTypeAvisAnnonceEnum.ENVOYE, StatutTypeAvisAnnonceEnum.ENVOI_PLANIFIER)).stream().max(Comparator.comparing(SuiviTypeAvisPub::getDateCreation)).orElseThrow(() -> new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucune avis associée à cette consultation"));
            SuiviAnnonce europeen = avisPub.getAnnonces().stream().filter(suiviAnnonce -> suiviAnnonce.getSupport().isEuropeen()).findFirst().orElse(null);
            SuiviAnnonce national = avisPub.getAnnonces().stream().filter(suiviAnnonce -> !suiviAnnonce.getSupport().isEuropeen()).findFirst().orElse(null);
            if (europeen != null) {
                if (europeen.getStatut().equals(StatutSuiviAnnonceEnum.PUBLIER) && europeen.getAvisFichier() == null && !StringUtils.hasText(europeen.getLienPublication())) {
                    log.info("Aucun avis externe associé à l'avis {}", avisPub.getId());
                } else {
                    suiviAnnonces.add(europeen);
                }
            }
            SuiviAnnonce annoncePortail = getSuiviAnnoncePortail(avisPub, europeen, national);
            suiviAnnonces.add(annoncePortail);

        } else {
            suiviAnnonces = suiviAnnonceRepository.findAllByIdPlatformAndIdConsultationAndStatutIn(idPlatform, idConsultation, List.of(StatutSuiviAnnonceEnum.ENVOI_PLANIFIER, StatutSuiviAnnonceEnum.ARRETER, StatutSuiviAnnonceEnum.EN_COURS_DE_PUBLICATION, StatutSuiviAnnonceEnum.REJETER_CONCENTRATEUR, StatutSuiviAnnonceEnum.REJETER_SUPPORT, StatutSuiviAnnonceEnum.PUBLIER, StatutSuiviAnnonceEnum.REJETER_CONCENTRATEUR_VALIDATION_ECO, StatutSuiviAnnonceEnum.REJETER_CONCENTRATEUR_VALIDATION_SIP));

        }
        return getSuiviOffreDTOS(suiviAnnonces);

    }

    private List<SuiviOffreDTO> getSuiviOffreDTOS(List<SuiviAnnonce> suiviAnnonces) {
        Map<String, List<SuiviAnnonce>> list = suiviAnnonces.stream().filter(suiviAnnonce -> suiviAnnonce.getOffre() != null && suiviAnnonce.getSupport() != null)
                .collect(Collectors.groupingBy(suiviAnnonce -> suiviAnnonce.getIdConsultation() + suiviAnnonce.getOffre().getCode() + suiviAnnonce.getSupport().getCode()));

        List<SuiviOffreDTO> annoncesPub = list.values().stream().map(annonces -> {
            SuiviAnnonce lastSuiviAnnonce = annonces.stream().max(Comparator.comparing(SuiviAnnonce::getDateCreation)).orElseThrow(() -> new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucune annonce associée à cette avis"));
            return getSuiviOffreDTO(lastSuiviAnnonce);
        }).collect(Collectors.toList());

        Map<String, List<SuiviAnnonce>> listAcheteur = suiviAnnonces.stream()
                .filter(suiviAnnonce -> suiviAnnonce.getSuiviList() != null && suiviAnnonce.getSuiviList().stream().anyMatch(suiviAvisPublie -> suiviAvisPublie.getAcheteur() != null) && suiviAnnonce.getSupport() != null)
                .collect(Collectors.groupingBy(suiviAnnonce -> {
                    SuiviAvisPublie suivi = suiviAnnonce.getSuiviList().stream().filter(suiviAvisPublie -> suiviAvisPublie.getAcheteur() != null).findFirst().orElseThrow(() -> new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucun acheteur associé à cette avis"));
                    return suiviAnnonce.getIdConsultation() + suivi.getAcheteur().getLogin() + suivi.getAcheteur().getEmail() + suiviAnnonce.getSupport().getCode();
                }));

        annoncesPub.addAll(listAcheteur.values().stream().map(annonces -> {
            SuiviAnnonce lastSuiviAnnonce = annonces.stream().max(Comparator.comparing(SuiviAnnonce::getDateCreation)).orElseThrow(() -> new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucune annonce associée à cette avis"));
            return getSuiviOffreDTO(lastSuiviAnnonce);
        }).collect(Collectors.toList()));
        annoncesPub.sort(Comparator.comparing(SuiviOffreDTO::getDateModification));
        return annoncesPub;
    }

    @Override
    public void suiviDocument(SuiviTypeAvisPub avisPub) {
        String tokenEdition = avisPub.getTokenEdition();
        Long id = avisPub.getId();
        if (tokenEdition == null) {
            log.info("Le document de l'avis {} semble être fermé", id);
            avisPub.setStatutEdition(FileStatusEnum.CLOSED_WITHOUT_EDITING);
            suiviTypeAvisPubRepository.save(avisPub);
            return;
        }
        try {

            FileStatus status = espaceDocumentaireService.getDocumentStatus(tokenEdition);
            if (status == null || status.getStatus() == null) {
                log.info("Le document de l'avis {} semble être fermé", id);
                avisPub.setStatutEdition(FileStatusEnum.CLOSED_WITHOUT_EDITING);
                suiviTypeAvisPubRepository.save(avisPub);
                return;
            }
            log.info("Passage du statut du document {} du statut {} au statut {}", id, avisPub.getStatutEdition(), status.getStatus());

            this.updateDocumentEdition(status, avisPub);

        } catch (Exception e) {
            log.error("Erreur lors de la récupération du statut du document : {}", e.getMessage());
            avisPub.setStatutEdition(FileStatusEnum.CLOSED_WITHOUT_EDITING);
            suiviTypeAvisPubRepository.save(avisPub);
        }
    }


    private void updateDocumentEdition(FileStatus status, SuiviTypeAvisPub avisPub) {

        avisPub.setStatutEdition(status.getStatus());
        avisPub.setVersionEdition(status.getVersion());
        FileRedacStatusEnum fileRedacStatusEnum = FileRedacStatusEnum.fromValue(status.getRedacStatus());
        avisPub.setStatutRedaction(fileRedacStatusEnum.name());
        List<SuiviAnnonce> annonces = avisPub.getNationalAnnonces();
        if (CollectionUtils.isEmpty(annonces)) {
            return;
        }
        Long id = avisPub.getId();
        if (avisPub.getDateValidationNational() == null && fileRedacStatusEnum.equals(FileRedacStatusEnum.VALIDER)) {
            log.info("Date de validation de l'avis {} : {}", id, status.getModificationDate());
            Timestamp modificationDate = status.getModificationDate();
            if (modificationDate != null) {
                Instant instant = Instant.ofEpochSecond(modificationDate.getTime());
                String timeZoneId = "Europe/Paris";
                ZonedDateTime zonedDateTime = instant.atZone(ZoneId.of(timeZoneId));
                avisPub.setDateValidationNational(zonedDateTime);

            } else {
                ZonedDateTime now = ZonedDateTime.now();
                avisPub.setDateValidationNational(now);
            }
        } else if (avisPub.getDateValidationNational() != null && !fileRedacStatusEnum.equals(FileRedacStatusEnum.VALIDER)) {
            log.info("Date d'invalidation de l'avis {} ", id);
            avisPub.setDateValidationNational(null);
        }
        final SuiviTypeAvisPub saved = suiviTypeAvisPubRepository.save(avisPub);
        PieceJointe document = saved.getNationalAnnonces().stream().map(SuiviAnnonce::getAvisFichier).findFirst().orElseThrow(() -> new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucun document d'annonce "));
        PieceJointe pieceJointe = espaceDocumentaireService.getPieceJointeFromDocgenValidation(document, saved.getTokenEdition(), "type-avis-" + saved.getId());
        suiviAnnonceRepository.saveAll(saved.getNationalAnnonces().stream().peek(suiviAnnonce -> {
            suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.getStatutSuiviAnnonceFromTypeAvis(saved.getStatut(), false, saved.getDateValidationNational() != null));
            suiviAnnonce.setDateValidation(saved.getDateValidationNational());
            if (pieceJointe != null) suiviAnnonce.setAvisFichier(pieceJointe);
        }).toList());

    }

    private SuiviAnnonce getSuiviAnnoncePortail(SuiviTypeAvisPub avisPub, SuiviAnnonce europeen, SuiviAnnonce national) {
        Support support = Support.builder().code("PORTAIL").codeGroupe("JAL").libelle("Portail des marchés publics").build();
        ZonedDateTime dateEnvoiEuropeen = avisPub.getDateEnvoiEuropeen();
        ZonedDateTime dateMiseEnLigne = avisPub.getDateMiseEnLigne();
        SuiviAnnonce annoncePortail = SuiviAnnonce.builder().statut(StatutSuiviAnnonceEnum.getStatutSuiviAnnonceFromTypeAvis(avisPub.getStatut(), false, avisPub.getDateValidationNational() != null)).support(support).dateCreation(avisPub.getDateCreation()).idConsultation(avisPub.getIdConsultation()).idPlatform(avisPub.getOrganisme().getPlateformeUuid()).dateModification(avisPub.getDateModification()).organismeMpe(avisPub.getOrganisme()).service(avisPub.getService()).dateDemandeEnvoi(dateEnvoiEuropeen != null ? dateEnvoiEuropeen.toInstant() : null).dateEnvoi(dateEnvoiEuropeen != null ? dateEnvoiEuropeen.toInstant() : null).datePublication(dateMiseEnLigne != null ? dateMiseEnLigne.toInstant() : null).build();
        if (national == null) {
            Offre offre = offreRepository.findByCode("LUAVIS").orElseThrow(() -> new AtexoException(ExceptionEnum.NOT_FOUND, "Offre non trouvée"));
            annoncePortail.setOffre(offre);
            if (europeen != null) {
                annoncePortail.setStatut(europeen.getStatut());
                annoncePortail.setDateDemandeEnvoi(europeen.getDateDemandeEnvoi());
                annoncePortail.setDatePublication(europeen.getDatePublication());
                annoncePortail.setDateDebutEnvoi(europeen.getDateDebutEnvoi());
                annoncePortail.setDateEnvoi(europeen.getDateEnvoi());
            }
        } else {
            annoncePortail.setOffre(national.getOffre());
            annoncePortail.setStatut(national.getStatut());
            annoncePortail.setDateDemandeEnvoi(national.getDateDemandeEnvoi());
            annoncePortail.setDatePublication(national.getDatePublication());
            annoncePortail.setDateDebutEnvoi(national.getDateDebutEnvoi());
            annoncePortail.setDateEnvoi(national.getDateEnvoi());
        }
        annoncePortail.setAvisFichier(avisPub.getAvisNational());
        annoncePortail.setTypeAvisPub(avisPub);
        return annoncePortail;
    }

    @Override
    public List<SuiviOffreDTO> getAllAvis(String idPlatform, Integer idConsultation) {
        PlateformeConfiguration plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(idPlatform);
        List<SuiviAnnonce> suiviAnnonces;
        if (plateformeConfiguration != null && plateformeConfiguration.isGroupement()) {
            List<SuiviTypeAvisPub> avisPubs = suiviTypeAvisPubRepository.findByOrganismePlateformeUuidAndIdConsultationAndStatutIn(idPlatform, idConsultation, List.of(StatutTypeAvisAnnonceEnum.ENVOYE));
            suiviAnnonces = getSuiviAnnoncesFromAvis(avisPubs);

        } else {
            suiviAnnonces = suiviAnnonceRepository.findAllByIdPlatformAndIdConsultationAndStatutIn(idPlatform, idConsultation, List.of(StatutSuiviAnnonceEnum.EN_COURS_DE_PUBLICATION, StatutSuiviAnnonceEnum.PUBLIER));
        }
        return suiviAnnonces.stream().map(this::getSuiviOffreDTO).sorted(Comparator.comparing(SuiviOffreDTO::getDateModification)).toList();

    }

    private List<SuiviAnnonce> getSuiviAnnoncesFromAvis(List<SuiviTypeAvisPub> avisPubs) {
        List<SuiviAnnonce> suiviAnnonces = new ArrayList<>();
        for (SuiviTypeAvisPub avisPub : avisPubs) {

            SuiviAnnonce europeen = avisPub.getEuropeenAnnonce();
            SuiviAnnonce national = avisPub.getNationalAnnonces().stream().findFirst().orElse(null);
            if (europeen != null) {
                if (europeen.getStatut().equals(StatutSuiviAnnonceEnum.PUBLIER) && europeen.getAvisFichier() == null && !StringUtils.hasText(europeen.getLienPublication())) {
                    log.info("Aucun avis externe associé à l'avis {}", avisPub.getId());
                } else {
                    suiviAnnonces.add(europeen);
                }
            }

            if (national == null) {
                SuiviAnnonce annoncePortail = getSuiviAnnoncePortail(avisPub, europeen, national);
                suiviAnnonces.add(annoncePortail);
            } else {
                suiviAnnonces.add(national);
            }
        }
        return suiviAnnonces;
    }


    @Override
    public SuiviAnnonce getPortailAnnonce(Long id, String idPlatform, Integer idConsultation) {
        SuiviTypeAvisPub avisPub = getSuiviTypeAvisPub(id, idPlatform, idConsultation);
        SuiviAnnonce europeen = avisPub.getEuropeenAnnonce();
        SuiviAnnonce national = avisPub.getNationalAnnonces().stream().findFirst().orElse(null);
        return getSuiviAnnoncePortail(avisPub, europeen, national);
    }

    @Override
    public SuiviAnnonce getLastPortailPdf(String idPlatform, Integer idConsultation) {
        SuiviTypeAvisPub avisPub = suiviTypeAvisPubRepository.findByOrganismePlateformeUuidAndIdConsultationAndChildrenIsEmpty(idPlatform, idConsultation).stream().min((o1, o2) -> o2.getDateCreation().compareTo(o1.getDateCreation())).orElseThrow(() -> new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucune avis associée à cette consultation"));
        SuiviAnnonce europeen = avisPub.getEuropeenAnnonce();
        SuiviAnnonce national = avisPub.getNationalAnnonces().stream().findFirst().orElse(null);
        return getSuiviAnnoncePortail(avisPub, europeen, national);

    }

    public EditionRequest editDocument(Agent agent, SuiviTypeAvisPub avisPubEntity, String token) {

        String endpoint = "/concentrateur-annonces/rest/v2/type-avis-annonces/" + avisPubEntity.getId() + "/editeur-en-ligne/callback";
        String callback = agent.getOrganisme().getPlateformeUrl() + endpoint;
        this.getDocumentWithEditionToken(agent, callback, avisPubEntity, token);
        avisPubEntity = suiviTypeAvisPubRepository.save(avisPubEntity);
        return espaceDocumentaireService.getUrlEdition(avisPubEntity.getTokenEdition());
    }


    private void getDocumentWithEditionToken(Agent agent, String callback, SuiviTypeAvisPub avisPub, String token) {
        PieceJointe document = avisPub.getAvisNational();
        Long id = avisPub.getId();
        String statutRedaction = avisPub.getStatutRedaction();
        String editionToken = espaceDocumentaireService.getValidationToken(agent, "concentrateur@type-avis-", callback, String.valueOf(id), token, document, statutRedaction);
        avisPub.setTokenEdition(editionToken);
        avisPub.setStatutEdition(FileStatusEnum.REQUEST_TO_OPEN);
    }

    private PieceJointe initAvisDocument(SuiviAnnonce annonce) {
        if (annonce == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucune annonce associée à cette avis");
        }
        SuiviTypeAvisPub avisPubEntity = annonce.getTypeAvisPub();

        String xmlBase64 = annonce.getXml();
        if (StringUtils.hasText(xmlBase64)) {
            String xml = new String(Base64.decodeBase64(xmlBase64));
            try (ByteArrayInputStream inputStream = new ByteArrayInputStream(xml.getBytes())) {
                JAXBContext jaxbContext = JAXBContext.newInstance(TedEsenders.class);
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                JAXBElement<TedEsenders> unmarshal = (JAXBElement<TedEsenders>) jaxbUnmarshaller.unmarshal(inputStream);
                TedEsenders tedEsenders = unmarshal.getValue();
                if (tedEsenders != null && tedEsenders.getFORMSECTION() != null && tedEsenders.getFORMSECTION().getF052014() != null && tedEsenders.getSENDER() != null) {

                    List<KeyValueRequest> map = new ArrayList<>();
                    map.add(KeyValueRequest.builder().key("forms").value(tedEsenders.getFORMSECTION().getF052014()).build());
                    map.add(KeyValueRequest.builder().key("sender").value(tedEsenders.getSENDER()).build());
                    ConfigurationConsultationDTO configurationByPlateforme = agentService.getConfigurationByPlateforme(annonce.getIdConsultation(), annonce.getOrganismeMpe().getPlateformeUuid());
                    if (configurationByPlateforme != null) {
                        ConsultationContexte contexte = configurationByPlateforme.getContexte();
                        map.add(KeyValueRequest.builder().key("ctx").value(contexte).build());
                    }

                    map.add(KeyValueRequest.builder().key("typeAvis").value(suiviTypeAvisPubMapper.mapToModel(avisPubEntity)).build());
                    Offre offre = annonce.getOffre();
                    map.add(KeyValueRequest.builder().key("offre").value(offreMapper.offreToOffreDTO(offre)).build());

                    File template = espaceDocumentaireService.downloadAvisTemplate(offre, false);
                    String libelle = offre.getLibelle();
                    return espaceDocumentaireService.getPieceJointe(libelle, map, template, offre.getDocumentExtension());
                }
            } catch (Exception e) {
                log.error("Erreur lors de la récupération du formulaire : {}", e.getMessage());
                throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, "Erreur lors de la récupération du formulaire : " + e.getMessage());
            }
        }

        return null;
    }


    private List<Support> addSupportJal(List<JalDTO> jalList, PlateformeConfiguration plateformeConfiguration, Offre offreAssociee) {
        String codeGroupe = "JAL";
        try {
            String pays = plateformeConfiguration == null ? "FRA" : plateformeConfiguration.getPays();
            return jalList.stream().map(jalDTO -> {
                String codeSupport = pays + "-" + jalDTO.getEmail() + "-" + Base64.encodeBase64StringUnChunked(jalDTO.getNom().getBytes());
                Support support = supportRepository.findByCode(codeSupport);
                if (support == null) {
                    support = Support.builder().codeGroupe(codeGroupe).actif(true).pays(pays).choixUnique(true).libelle(jalDTO.getNom()).code(codeSupport).codeDestination("MAIL").mail(jalDTO.getEmail()).poids(0).national(true).europeen(false).description("Organe de presse : " + jalDTO.getNom()).url(jalDTO.getEmail()).external(false).build();

                } else {
                    support.setActif(true);
                    support.setLibelle(jalDTO.getNom());
                    support.setDescription("Organe de presse : " + jalDTO.getNom());
                }
                if (support.getOffres() == null) support.setOffres(new HashSet<>());
                if (support.getOffres().stream().noneMatch(offre -> offre.getCode().equals(offreAssociee.getCode())))
                    support.getOffres().add(offreAssociee);

                return supportRepository.save(support);


            }).collect(Collectors.toList());
        } catch (Exception e) {
            log.error("Erreur lors de la mise à jour des supports JAL {}", e.getMessage());
        }
        return Collections.emptyList();
    }

    @Transactional(rollbackFor = Exception.class)
    public boolean deleteSuiviTypeAvisPub(Long id, String token, String plateforme, Integer idConsultation) {

        var suiviTypeAvisPub = getSuiviTypeAvisPub(id, plateforme, idConsultation);

        List<SuiviAnnonce> annonces = suiviTypeAvisPub.getAnnonces();

        if (!CollectionUtils.isEmpty(annonces)) {
            for (SuiviAnnonce annonce : annonces) {
                suiviAnnonceService.deleteSuiviAnnonce(annonce.getId(), annonce.getIdPlatform(), annonce.getIdConsultation());
            }
        }
        suiviTypeAvisPub.getMailPiecejointeList().forEach(suiviTypeAvisPubPieceJointeAssoEntity -> {
            this.deletePieceJointeByIdTypeAvisAndIdAsso(id, suiviTypeAvisPubPieceJointeAssoEntity.getId(), plateforme, idConsultation);
        });
        suiviTypeAvisPubRepository.delete(suiviTypeAvisPub);
        return true;
    }

    private void setDateMiseEnLigne(SuiviTypeAvisPub typeAvisPub, ZonedDateTime dateValidation) {
        if (typeAvisPub.getDateMiseEnLigne() == null) {
            return;
        }

        if (typeAvisPub.isEuropeen() && typeAvisPub.getAnnonces().stream().noneMatch(suiviAnnonce -> suiviAnnonce.getSupport().isEuropeen())) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucun avis européen est associé à cette consultation");
        }
        ZonedDateTime dateMiseEnLigne = typeAvisPub.getDateMiseEnLigne();
        ZonedDateTime dateLimie = dateValidation.isBefore(dateMiseEnLigne) ? dateMiseEnLigne : dateValidation;
        ZonedDateTime dateEnvoiEuropeenPrevisionnel = null;
        ZonedDateTime dateEnvoiNationalPrevisionnel = null;
        ZonedDateTime dateMiseEnLignCalcule;

        if (typeAvisPub.isEuropeen()) {
            dateEnvoiEuropeenPrevisionnel = dateLimie;
            dateMiseEnLignCalcule = dateLimie.plusHours(48);
            if (!CollectionUtils.isEmpty(typeAvisPub.getNationalAnnonces()))
                dateEnvoiNationalPrevisionnel = dateMiseEnLignCalcule;
            typeAvisPub.getAnnonces().stream().map(SuiviAnnonce::getFormulaire).filter(Objects::nonNull).forEach(eFormsFormulaireEntity -> ieFormsNoticeServices.updateNoticeDatePublication(dateLimie, eFormsFormulaireEntity));
        } else {
            dateMiseEnLignCalcule = dateLimie;
            dateEnvoiNationalPrevisionnel = dateLimie;
        }
        typeAvisPub.setDateEnvoiEuropeenPrevisionnel(dateEnvoiEuropeenPrevisionnel);
        typeAvisPub.setDateEnvoiNationalPrevisionnel(dateEnvoiNationalPrevisionnel);
        ZonedDateTime dateTime = typeAvisPub.getDateMiseEnLigneCalcule();
        if (dateTime == null) {
            dateTime = suiviTypeAvisPubRepository.findMaxDateMiseEnLigneCalcule(typeAvisPub.getIdConsultation(), typeAvisPub.getOrganisme().getPlateformeUuid()).orElse(null);
        }
        if (dateTime == null || dateTime.isAfter(ZonedDateTime.now())) {
            typeAvisPub.setDateMiseEnLigneCalcule(dateMiseEnLignCalcule);
        }
        typeAvisPub.setStatut(StatutTypeAvisAnnonceEnum.ENVOI_PLANIFIER);
        typeAvisPub.setDateModificationStatut(ZonedDateTime.now());

    }

    @Override
    public void updateAnnoncesFromTypeAvis(SuiviTypeAvisPub avisPub, String token) {
        List<SuiviAnnonce> annonces = avisPub.getAnnonces();
        annonces.stream().filter(Objects::nonNull).filter(suiviAnnonce -> suiviAnnonce.getSupport() != null).forEach(suiviAnnonce -> {
            Support support = suiviAnnonce.getSupport();
            StatutSuiviAnnonceEnum statut = StatutSuiviAnnonceEnum.getStatutSuiviAnnonceFromTypeAvis(avisPub.getStatut(), support.isExternal(), avisPub.getDateValidationNational() != null);
            suiviAnnonceService.modifyAnnonceStatut(suiviAnnonce, SuiviAnnonceUpdate.builder().statut(statut).build(), token);
        });
        suiviAnnonceRepository.saveAll(annonces);
    }

    @Override
    public List<SuiviTypeAvisPubDTO> updateDateDeMiseEnLigne(Integer idConsultation, String plateforme, String dmls) {
        List<SuiviTypeAvisPub> facturation = suiviTypeAvisPubRepository.findByOrganismePlateformeUuidAndIdConsultationAndChildrenIsEmpty(plateforme, idConsultation);
        Date date1;
        try {
            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(dmls);
        } catch (ParseException e) {
            log.error("Erreur lors de la conversion de la date {} : {}", dmls, e.getMessage());
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, "Erreur lors de la conversion de la date " + dmls + " : " + e.getMessage());
        }
        for (SuiviTypeAvisPub typeAvisPub : facturation) {
            typeAvisPub.setDateMiseEnLigne(ZonedDateTime.ofInstant(date1.toInstant(), ZoneId.systemDefault()));
        }
        Iterable<SuiviTypeAvisPub> all = suiviTypeAvisPubRepository.saveAll(facturation);
        List<SuiviTypeAvisPubDTO> suiviTypeAvisPubDTOS = new ArrayList<>();
        all.forEach(suiviTypeAvisPub -> suiviTypeAvisPubDTOS.add(suiviTypeAvisPubMapper.mapToModel(suiviTypeAvisPub)));
        return suiviTypeAvisPubDTOS;
    }

    @Override
    public ByteArrayOutputStream exporterFormulaires(HttpServletResponse response, DashboardSuiviTypeAvisSpecification facturationSearch, String plateforme) {

        try {
            Path copied = Files.createTempFile(null, null);
            Path originalPath = resourceExcelFile.getFile().toPath();
            Files.copy(originalPath, copied, StandardCopyOption.REPLACE_EXISTING);
            XSSFWorkbook workbook = new XSSFWorkbook(copied.toFile());
            setXlsxData(facturationSearch, workbook);
            ByteArrayOutputStream outFile = new ByteArrayOutputStream();
            workbook.write(outFile);
            workbook.close();
            Files.delete(copied);
            return outFile;
        } catch (InvalidFormatException | IOException e) {
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage());
        }
    }


    private List<List<XlsxColumn>> getXlsxData(List<AnnonceStatistique> content, Boolean sip) {
        return content.stream().map(annonceStatistique -> {
            List<XlsxColumn> xlsxColumns = new ArrayList<>();
            int index = 0;
            if (annonceStatistique.getOffre() != null) {
                xlsxColumns.add(XlsxColumn.builder().index(index).value(annonceStatistique.getOffre().getCode() + " - " + annonceStatistique.getOffre().getLibelle()).type(CellType.STRING).build());
            }
            index++;
            if (!Boolean.TRUE.equals(sip)) {
                xlsxColumns.add(XlsxColumn.builder().index(index).value(String.valueOf(annonceStatistique.getStatuts().stream().filter(statutStatistique -> StatutTypeAvisAnnonceEnum.EN_ATTENTE_VALIDATION_ECO.equals(statutStatistique.getStatut())).findFirst().map(StatutStatistique::getTotal).orElse(0L))).type(CellType.NUMERIC).build());
                index++;
            }
            xlsxColumns.add(XlsxColumn.builder().index(index).value(String.valueOf(annonceStatistique.getStatuts().stream().filter(statutStatistique -> StatutTypeAvisAnnonceEnum.EN_ATTENTE_VALIDATION_SIP.equals(statutStatistique.getStatut())).findFirst().map(StatutStatistique::getTotal).orElse(0L))).type(CellType.NUMERIC).build());
            index++;
            xlsxColumns.add(XlsxColumn.builder().index(index).value(String.valueOf(annonceStatistique.getStatuts().stream().filter(statutStatistique -> StatutTypeAvisAnnonceEnum.ENVOI_PLANIFIER.equals(statutStatistique.getStatut())).findFirst().map(StatutStatistique::getTotal).orElse(0L))).type(CellType.NUMERIC).build());
            index++;
            xlsxColumns.add(XlsxColumn.builder().index(index).value(String.valueOf(annonceStatistique.getStatuts().stream().filter(statutStatistique -> StatutTypeAvisAnnonceEnum.ENVOYE.equals(statutStatistique.getStatut())).findFirst().map(StatutStatistique::getTotal).orElse(0L))).type(CellType.NUMERIC).build());
            index++;
            xlsxColumns.add(XlsxColumn.builder().index(index).value(String.valueOf(annonceStatistique.getStatuts().stream().filter(statutStatistique -> (Boolean.TRUE.equals(sip) ? StatutTypeAvisAnnonceEnum.REJETE_SIP : StatutTypeAvisAnnonceEnum.REJETE_ECO).equals(statutStatistique.getStatut())).findFirst().map(StatutStatistique::getTotal).orElse(0L))).type(CellType.NUMERIC).build());
            index++;
            xlsxColumns.add(XlsxColumn.builder().index(index).value(String.valueOf(annonceStatistique.getStatuts().stream().filter(statutStatistique -> StatutTypeAvisAnnonceEnum.REJETE.equals(statutStatistique.getStatut())).findFirst().map(StatutStatistique::getTotal).orElse(0L))).type(CellType.NUMERIC).build());
            return xlsxColumns;
        }).collect(Collectors.toList());

    }

    private void setXlsxData(DashboardSuiviTypeAvisSpecification specification, XSSFWorkbook workbook) {
        //Entreprises
        XSSFSheet xssfSheet = xlsxService.createXLSX(workbook, workbook.getSheet(workbook.getSheetName(0)));
        Set<String> offres = specification.getOffres();
        if (!CollectionUtils.isEmpty(offres)) {
            xlsxService.setData(xssfSheet, 3, 1, String.join(",", offres), CellType.STRING);
        }
        Set<StatutTypeAvisAnnonceEnum> statuts = specification.getStatuts();
        if (!CollectionUtils.isEmpty(statuts)) {
            xlsxService.setData(xssfSheet, 4, 1, statuts.stream().map(Enum::name).collect(Collectors.joining(",")), CellType.STRING);
        }
        PageRequest pageable = PageRequest.of(0, 10, Sort.unsorted());
        Page<SuiviTypeAvisPub> avisPubs;
        int start = 7;
        do {
            avisPubs = suiviTypeAvisPubRepository.findAll(specification, pageable);
            List<SuiviTypeAvisPub> content = avisPubs.getContent();
            xlsxService.setData(xssfSheet, getXlsxDataAvisPub(content, specification.getSip()), start);
            start += content.size();

            pageable = pageable.next();
        } while (!avisPubs.isLast());

        xlsxService.autoSize(xssfSheet, 13);
        workbook.setActiveSheet(0);
    }


    private List<List<XlsxColumn>> getXlsxDataAvisPub(List<SuiviTypeAvisPub> content, Boolean sip) {
        return content.stream().map(typeAvisPub -> {
            ConfigurationConsultationDTO configuration = agentService.getConfiguration(null, typeAvisPub.getIdConsultation(), typeAvisPub.getOrganisme().getPlateformeUuid());
            List<XlsxColumn> xlsxColumns = new ArrayList<>();
            TypeProcedure procedure = typeAvisPub.getProcedure();
            if (procedure != null)
                xlsxColumns.add(XlsxColumn.builder().index(0).value(procedure.getLibelle()).type(CellType.STRING).build());
            if (configuration != null && configuration.getContexte() != null) {
                ConsultationContexte contexte = configuration.getContexte();
                AgentMpe agentAuteur = contexte.getAgent();
                String auteur;
                if (agentAuteur != null) {
                    auteur = agentAuteur.getPrenom() + " " + agentAuteur.getNom();
                } else {
                    auteur = "-";
                }
                xlsxColumns.add(XlsxColumn.builder().index(1).value(auteur).type(CellType.STRING).build());
            }
            Agent agent = typeAvisPub.getCreerPar();
            String createur;
            if (agent != null) {
                createur = agent.getPrenom() + " " + agent.getNom();
            } else {
                createur = "-";
            }
            xlsxColumns.add(XlsxColumn.builder().index(2).value(createur).type(CellType.STRING).build());
            if (Boolean.TRUE.equals(sip)) {
                Agent validateurSip = typeAvisPub.getValidateurSip();
                String validateur;
                if (validateurSip != null) {
                    validateur = validateurSip.getPrenom() + " " + validateurSip.getNom();
                } else {
                    validateur = "-";
                }
                xlsxColumns.add(XlsxColumn.builder().index(3).value(validateur).type(CellType.STRING).build());

            } else {
                Agent validateurEco = typeAvisPub.getValidateurEco();
                String validateur;
                if (validateurEco != null) {
                    validateur = validateurEco.getPrenom() + " " + validateurEco.getNom();
                } else {
                    validateur = "-";
                }
                xlsxColumns.add(XlsxColumn.builder().index(3).value(validateur).type(CellType.STRING).build());
            }

            if (Boolean.TRUE.equals(sip)) {
                xlsxColumns.add(XlsxColumn.builder().index(4).value(Boolean.TRUE.equals(typeAvisPub.getValiderSip()) ? "Oui" : "Non").type(CellType.STRING).build());
            } else {
                xlsxColumns.add(XlsxColumn.builder().index(4).value(Boolean.TRUE.equals(typeAvisPub.getValiderEco()) ? "Oui" : "Non").type(CellType.STRING).build());
            }
            xlsxColumns.add(XlsxColumn.builder().index(5).value(typeAvisPub.getReference()).type(CellType.STRING).build());
            xlsxColumns.add(XlsxColumn.builder().index(6).value(typeAvisPub.getTitre()).type(CellType.STRING).build());
            xlsxColumns.add(XlsxColumn.builder().index(7).value(typeAvisPub.getObjet()).type(CellType.STRING).build());
            xlsxColumns.add(XlsxColumn.builder().index(8).value(typeAvisPub.getOrganisme().getLibelleOrganisme()).type(CellType.STRING).build());
            ReferentielServiceMpe service = typeAvisPub.getService();
            if (service != null)
                xlsxColumns.add(XlsxColumn.builder().index(9).value(service.getLibelle()).type(CellType.STRING).build());

            xlsxColumns.add(XlsxColumn.builder().index(10).value(typeAvisPub.getOffreRacine().getLibelle()).type(CellType.STRING).build());
            xlsxColumns.add(XlsxColumn.builder().index(11).value(typeAvisPub.getStatut().name()).type(CellType.STRING).build());

            ZonedDateTime sendDate;
            if (typeAvisPub.isEuropeen()) {
                sendDate = typeAvisPub.getDateEnvoiEuropeen();
            } else {
                sendDate = typeAvisPub.getDateEnvoiNational();
            }
            if (sendDate != null) {
                xlsxColumns.add(XlsxColumn.builder().index(12).value(sendDate.toLocalDate().toString()).type(CellType.STRING).build());
            } else {
                xlsxColumns.add(XlsxColumn.builder().index(12).value("-").type(CellType.STRING).build());
            }
            ZonedDateTime dateMiseEnLigne = typeAvisPub.getDateMiseEnLigne();
            if (dateMiseEnLigne != null) {
                xlsxColumns.add(XlsxColumn.builder().index(13).value(dateMiseEnLigne.toLocalDate().toString()).type(CellType.STRING).build());
            } else {
                xlsxColumns.add(XlsxColumn.builder().index(13).value("-").type(CellType.STRING).build());

            }

            ZonedDateTime dlro = typeAvisPub.getDlro();
            if (sendDate != null && dlro != null) {
                Period period = Period.between(sendDate.toLocalDate(), dlro.toLocalDate());
                xlsxColumns.add(XlsxColumn.builder().index(14).value(Math.abs(period.getDays()) + " jour(s)").type(CellType.STRING).build());

            } else {
                xlsxColumns.add(XlsxColumn.builder().index(14).value("- jour(s)").type(CellType.STRING).build());
            }

            xlsxColumns.add(XlsxColumn.builder().index(15).value(typeAvisPub.getAnnonces().stream().map(SuiviAnnonce::getSupport).filter(Objects::nonNull).map(support -> support.isEuropeen() ? "JOUE" : "Presse").distinct().collect(Collectors.joining("/")) + "/PORTAIL"

            ).type(CellType.STRING).build());

            return xlsxColumns;
        }).toList();
    }


}
