package fr.atexo.annonces.boot.repository;

import fr.atexo.annonces.boot.entity.Organisme;
import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.config.DaoRepository;
import fr.atexo.annonces.dto.FileStatusEnum;
import fr.atexo.annonces.modeles.StatutSuiviAnnonceEnum;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Repository
public interface SuiviAnnonceRepository extends DaoRepository<SuiviAnnonce, Integer> {

    List<SuiviAnnonce> findAllByIdPlatformAndIdConsultation(String idPlatform, Integer idConsultation);
    List<SuiviAnnonce> findAllByIdPlatformAndIdConsultationAndChildrenIsEmpty(String idPlatform, Integer idConsultation);

    List<SuiviAnnonce> findAllByIdPlatformAndIdConsultationAndStatutIn(String idPlatform, Integer idConsultation, List<StatutSuiviAnnonceEnum> statuts);

    @Query("select distinct sa from SuiviAnnonce sa inner join fetch sa.offre o inner join fetch sa.support s left join fetch s.jal left join fetch s.pqr where sa.statut = 'EN_ATTENTE' AND s.compteObligatoire is false")
    List<SuiviAnnonce> getDemandesPublicationEnAttente();

    @Modifying
    @Transactional
    @Query("update SuiviAnnonce c set c.organismeMpe=:organisme where c.idPlatform = :idPlatform and c.idConsultation = :idConsultation")
    void updateOrganismeByIdPlatformAndIdConsultation(@Param("idPlatform") String idPlatform, @Param("idConsultation") Integer idConsultation, @Param("organisme") Organisme organisme);

    @Modifying
    @Transactional
    @Query("update SuiviAnnonce c set c.xml=:xml where c.idPlatform = :idPlatform and c.idConsultation = :idConsultation")
    void updateXmlByIdPlatformAndIdConsultation(@Param("idPlatform") String idPlatform, @Param("idConsultation") Integer idConsultation, @Param("xml") String xml);

    List<SuiviAnnonce> findByIdPlatformAndIdConsultationAndSupportCodeAndOffreCode(String idPlatform, Integer idConsultation, String supportCode, String offreCode);

    List<SuiviAnnonce> findByIdPlatformAndIdConsultationAndSupportCode(String idPlatform, Integer idConsultation, String supportCode);

    List<SuiviAnnonce> findByIdPlatformAndIdConsultationAndSupportCodeGroupeIn(String idPlatform, Integer idConsultation, List<String> supportCodesGroupes);

    SuiviAnnonce findByNumeroAvis(String numeroAvis);

    SuiviAnnonce findByNumeroAvisAndIdPlatformAndIdConsultationAndSupportCodeGroupeIn(String idAnnonce, String idPlatform, Integer idConsultation, List<String> supportCodesGroupes);

    Page<SuiviAnnonce> findByStatutEditionInAndStatutIn(List<FileStatusEnum> status, List<StatutSuiviAnnonceEnum> statutTypeAvisAnnonceEnum, PageRequest pageable);

    List<SuiviAnnonce> findAllByIdConsultationAndSupportCodeGroupe(Integer idConsultation, String supportCodeGroupe);

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Modifying
    @Query(value = "update SuiviAnnonce s set s.statut = 'EN_ATTENTE', s.dateDebutEnvoi = null WHERE s.statut = 'RETRY' and s.dateDebutEnvoi < :dateDebutEnvoi")
    void updateRetryAnnonces(@Param("dateDebutEnvoi") Instant dateDebutEnvoi);

}
