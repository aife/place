package fr.atexo.annonces.boot;

import fr.atexo.annonces.reseau.SupportIncludeParameterEnum;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Permet de convertir le paramètre d'appel "include" du controller de Support en un Enum de manière insensible à la
 * casse
 */
@Component
public class SupportIncludeParameterConverter implements Converter<String, SupportIncludeParameterEnum> {
    @Override
    public SupportIncludeParameterEnum convert(String source) {
        return SupportIncludeParameterEnum.valueOf(source.toUpperCase());
    }
}
