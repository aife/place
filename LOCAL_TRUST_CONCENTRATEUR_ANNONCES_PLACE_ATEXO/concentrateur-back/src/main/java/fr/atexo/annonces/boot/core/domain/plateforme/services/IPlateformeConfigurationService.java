package fr.atexo.annonces.boot.core.domain.plateforme.services;

import fr.atexo.annonces.dto.PlateformeConfigurationDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.util.List;

public interface IPlateformeConfigurationService {
    List<PlateformeConfigurationDTO> getPlateformes();

    PlateformeConfigurationDTO getPlateforme(Long id);

    PlateformeConfigurationDTO put(Long id, PlateformeConfigurationDTO configuration);

    boolean patchAssociationProcedureAvisOrganisme(Long id, MultipartFile document);

    ByteArrayOutputStream exportConfiguration(Long id);

    PlateformeConfigurationDTO addOrUpdatePlateformConfiguration(PlateformeConfigurationDTO plateforme);

    void delete(Long id);
}
