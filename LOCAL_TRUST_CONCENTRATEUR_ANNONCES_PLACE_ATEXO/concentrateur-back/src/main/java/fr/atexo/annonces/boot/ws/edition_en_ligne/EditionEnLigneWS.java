package fr.atexo.annonces.boot.ws.edition_en_ligne;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class EditionEnLigneWS {

    @Value("${external-apis.edition-en-ligne.base-path}")
    private String editionEnLigneBaseUrl;


    public String getEditionUrl(String token) {
        return editionEnLigneBaseUrl + "?token=" + token;
    }

}
