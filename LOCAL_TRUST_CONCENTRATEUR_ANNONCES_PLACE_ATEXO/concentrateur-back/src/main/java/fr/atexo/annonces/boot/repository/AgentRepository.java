package fr.atexo.annonces.boot.repository;


import fr.atexo.annonces.boot.entity.Agent;
import fr.atexo.annonces.config.DaoRepository;
import fr.atexo.annonces.dto.AgentDTO;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AgentRepository extends DaoRepository<Agent, String> {

    Optional<Agent> findByIdentifiantAndOrganismeId(String identifiant, Integer idOrganisme);

    List<Agent> findByOrganismePlateformeUuid(String idPlatform);
}
