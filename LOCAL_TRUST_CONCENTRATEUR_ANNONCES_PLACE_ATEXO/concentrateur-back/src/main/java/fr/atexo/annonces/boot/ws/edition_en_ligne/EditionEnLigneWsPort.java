package fr.atexo.annonces.boot.ws.edition_en_ligne;


public interface EditionEnLigneWsPort {

    String getEditionUrl(String token);
}
