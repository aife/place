package fr.atexo.annonces.boot.core.domain.e_forms_notice.services.impl;

import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.SubmitNoticeFormatHandler;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.FieldDetails;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNoticeMetadata;
import fr.atexo.annonces.boot.entity.EFormsFormulaireEntity;
import fr.atexo.annonces.boot.repository.EformsSurchargeRepository;
import fr.atexo.annonces.boot.ws.eforms.SchematronConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

@Slf4j
public class SubmitRootFormatHandler extends GenericSdkService implements SubmitNoticeFormatHandler {


    public SubmitRootFormatHandler(EformsSurchargeRepository eformsSurchargeRepository, SchematronConfiguration schematronConfiguration) {
        super(eformsSurchargeRepository, schematronConfiguration);
    }


    @Override
    public Object processGroup(Object formulaireObjet, SubNoticeMetadata group, EFormsFormulaireEntity saved, String nodeStop, Map<String, List<SubmitNoticeFormatHandler>> kownHandlers, ZonedDateTime dateEnvoi) {
        for (SubNoticeMetadata item : group.getContent()) {
            String nodeId = (item.is_repeatable() || item.get_identifierFieldId() != null) ? item.getNodeId() : null;

            List<SubmitNoticeFormatHandler> handler = kownHandlers.get(nodeId);
            if (handler != null) {

                for (SubmitNoticeFormatHandler noticeFormatHandler : handler) {
                    Object value = noticeFormatHandler.processGroup(new Object(), item, saved, nodeId, kownHandlers, dateEnvoi);
                    formulaireObjet = addToObject(formulaireObjet, null, value, nodeStop, nodeId, saved.getVersionSdk());
                }
            } else if (nodeId != null) {
                if (("field".equals(item.getContentType()))) {
                    formulaireObjet = processElement(item, formulaireObjet, item.getId(), saved, nodeStop, dateEnvoi);
                }
                if (!CollectionUtils.isEmpty(item.getContent())) {
                    Object o = processGroup(new Object(), item, saved, nodeId, kownHandlers, dateEnvoi);
                    formulaireObjet = addToObject(formulaireObjet, null, o, nodeStop, nodeId, saved.getVersionSdk());
                }
            } else {
                if (("field".equals(item.getContentType()))) {
                    formulaireObjet = processElement(item, formulaireObjet, item.getId(), saved, nodeStop, dateEnvoi);
                }
                if (!CollectionUtils.isEmpty(item.getContent())) {
                    formulaireObjet = processGroup(formulaireObjet, item, saved, nodeStop, kownHandlers, dateEnvoi);
                }
            }
        }
        return setGroupId(formulaireObjet, nodeStop, group, group.getNodeId(), saved.getVersionSdk(), null);


    }

    @Override
    public Object processElement(SubNoticeMetadata item, Object formulaireObjet, String id, EFormsFormulaireEntity saved, String nodeStop, ZonedDateTime dateEnvoi) {


        String versionSdk = saved.getVersionSdk();
        FieldDetails fieldDetails = getFields(versionSdk).getFields().stream().filter(fieldDetails1 -> fieldDetails1.getId().equals(id)).findFirst().orElse(null);
        if (fieldDetails == null) {
            return formulaireObjet;
        }
        String parentNodeId = fieldDetails.getParentNodeId();

        boolean repeatable = item.is_repeatable();
        switch (id) {
            case "BT-05(a)-notice" -> {
                Object date = addToObject(new Object(), "type", "Europe/Paris", null, null, versionSdk);
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                String formatted = dateEnvoi.format(formatter);
                log.info("Date de soumission {}", formatted);
                date = addToObject(date, "value", formatted, null, null, versionSdk);

                return addToObject(formulaireObjet, id, repeatable ? List.of(date) : date, nodeStop, parentNodeId, versionSdk);
            }
            case "BT-05(b)-notice" -> {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
                Object date = addToObject(new Object(), "type", "Europe/Paris", null, null, versionSdk);
                String formatted = dateEnvoi.format(formatter);
                date = addToObject(date, "value", formatted, null, null, versionSdk);
                log.info("Heure de soumission {}", formatted);
                return addToObject(formulaireObjet, id, repeatable ? List.of(date) : date, nodeStop, parentNodeId, versionSdk);
            }
            case "BT-738-notice" -> {
                Object date = addToObject(new Object(), "type", "Europe/Paris", null, null, versionSdk);
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                String formatted = dateEnvoi.plusDays(3).format(formatter);
                log.info("Date de publication préférée {}", formatted);
                date = addToObject(date, "value", formatted, null, null, versionSdk);

                return addToObject(formulaireObjet, id, repeatable ? List.of(date) : date, nodeStop, parentNodeId, versionSdk);
            }

            default -> log.debug("Pas de correspondance pour {}", id);
        }
        return formulaireObjet;
    }


}
