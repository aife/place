package fr.atexo.annonces.boot.core.domain.xlsx.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@Getter
@AllArgsConstructor
public enum XlsxAssociationAvisHeaderEnum {

    CODE_AVIS_EUROPEEN(0, "Code Avis Europeen"),
    LIBELLE_AVIS_EUROPEEN(1, "Libellé Avis Européen"),
    CODE_AVIS_NATIONAL(2, "Code Avis National"),
    LIBELLE_AVIS_NATIONAL(3, "Libellé Avis National");

    private final int index;
    private final String title;

    public static List<XlsxHeader> transform() {
        return Arrays
                .stream(XlsxAssociationAvisHeaderEnum.values())
                .map(xlsxProjetAchatHeaderEnum -> XlsxHeader
                        .builder()
                        .index(xlsxProjetAchatHeaderEnum.getIndex())
                        .title(xlsxProjetAchatHeaderEnum.getTitle())
                        .build())
                .toList();
    }
}
