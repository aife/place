package fr.atexo.annonces.boot.core.domain.e_forms_sdk.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class EformsSurchargePatch implements Serializable {

    private String defaultValue;

    private String dynamicListFrom;

    private String staticList;

    private String idNotice;

    private Boolean hidden;

    private Boolean readonly;

    private Boolean obligatoire;

    private Integer ordre;


}
