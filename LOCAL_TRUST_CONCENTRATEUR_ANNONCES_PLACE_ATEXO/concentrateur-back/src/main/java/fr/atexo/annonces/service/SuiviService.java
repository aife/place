package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.entity.tncp.SuiviAvisPublie;
import fr.atexo.annonces.dto.SuiviDTO;

import java.util.Optional;
import java.util.UUID;

public interface SuiviService {

    SuiviDTO sauvegarder(SuiviAvisPublie suivi);

    SuiviAvisPublie recuperer(String uuid);

    Optional<SuiviAvisPublie> rechercher(String uuid);
}
