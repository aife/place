package fr.atexo.annonces.boot.repository;

import fr.atexo.annonces.boot.entity.ConsultationFiltre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repository pour accéder aux objets Support
 */
@Repository
public interface ConsultationFiltreRepository extends JpaRepository<ConsultationFiltre, Integer>, JpaSpecificationExecutor<ConsultationFiltre> {

    ConsultationFiltre findByIdPlatformAndIdConsultation(String idPlateforme, Integer idConsultation);

    @Modifying
    @Transactional
    @Query("update ConsultationFiltre c set c.pqr=false where c.idPlatform = :idPlatform")
    void updatePqrToFalse(@Param("idPlatform") String idPlatform);

    @Modifying
    @Transactional
    @Query("update ConsultationFiltre c set c.jal=false where c.idPlatform = :idPlatform")
    void updateJalToFalse(@Param("idPlatform") String idPlatform);

    @Modifying
    @Transactional
    @Query("update ConsultationFiltre c set c.europeen=false where c.idPlatform = :idPlatform")
    void updateEuropeenToFalse(@Param("idPlatform") String idPlatform);

    @Modifying
    @Transactional
    @Query("update ConsultationFiltre c set c.national=false where c.idPlatform = :idPlatform")
    void updateNationalToFalse(@Param("idPlatform") String idPlatform);
}
