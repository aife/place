package fr.atexo.annonces.dto;

import fr.atexo.annonces.boot.ws.mpe.model.ConsultationContexte;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;


@NoArgsConstructor
@AllArgsConstructor
@Builder(builderMethodName = "with")
@Getter
@Setter
public class ConfigurationConsultationDTO implements Serializable {
    private BigDecimal montant;
    private String urlEforms;
    private List<String> departements = new ArrayList<>();
    private List<String> codeNuts = new ArrayList<>();
    private List<String> supports;
    private List<ConsultationFacturationDTO> facturationList;
    private List<String> blocList;
    private List<JalDTO> jalList;
    private String dmls;
    private ZonedDateTime dlro;
    private String xml;
    private boolean europeen;
    private boolean eformsModal;
    private boolean national;
    private boolean pqr;
    private boolean jal;
    private boolean groupement;
    private Boolean validationObligatoire;
    private Boolean associationProcedure;

    private ConsultationContexte contexte;
}
