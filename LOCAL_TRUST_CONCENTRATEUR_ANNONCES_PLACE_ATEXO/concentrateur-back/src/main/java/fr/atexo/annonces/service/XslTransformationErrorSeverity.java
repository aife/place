package fr.atexo.annonces.service;

/**
 * Décrit les trois niveaux d'erreurs qui peuvent survenir lors d'une transformation XSL.
 */
public enum XslTransformationErrorSeverity {
    WARN, ERROR, FATAL
}
