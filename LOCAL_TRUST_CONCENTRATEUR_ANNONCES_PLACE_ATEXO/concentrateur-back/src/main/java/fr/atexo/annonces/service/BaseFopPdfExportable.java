package fr.atexo.annonces.service;

import fr.atexo.annonces.service.exception.XslTransformationException;
import fr.atexo.annonces.transformation.ClasspathResolverURIAdapter;
import org.apache.fop.apps.*;
import org.slf4j.Logger;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.xml.transform.*;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.Map;

import static org.apache.xmlgraphics.util.MimeConstants.MIME_PDF;

/**
 * Classe abstraite pour définir des méthodes communes d'export d'un objet vers le format PDF
 */
public abstract class BaseFopPdfExportable<T> implements XmlExportable<T> {

    private final FopFactory fopFactory = new FopFactoryBuilder(new File(".").toURI(), new ClasspathResolverURIAdapter()).build();

    protected abstract Logger getLogger();

    protected void exportToPdf(T object, File xslFile, OutputStream exportPdf, Map<String, Object> xslParameters)
            throws FOPException, TransformerConfigurationException {

        try (StringReader xmlReader = new StringReader(toXml(object))) {
            // configure foUserAgent as desired
            FOUserAgent foUserAgent = fopFactory.newFOUserAgent();

            // Construct fop with desired output format
            Fop fop = fopFactory.newFop(MIME_PDF, foUserAgent, exportPdf);

            // Setup JAXP
            TransformerFactory factory = new net.sf.saxon.BasicTransformerFactory();
            Transformer transformer = factory.newTransformer(new StreamSource(xslFile));

            // Setup XSL parameters
            if (xslParameters != null) {
                xslParameters.forEach(transformer::setParameter);
            }

            // Setup input stream
            Source src = new StreamSource(xmlReader);

            // Resulting SAX events (the generated FO) must be piped through to FOP
            Result res = new SAXResult(fop.getDefaultHandler());

            // Collecteur d'erreurs de transformation
            MultiValueMap<XslTransformationErrorSeverity, String> errorCollector = new LinkedMultiValueMap<>();
            transformer.setErrorListener(new XslTransformationErrorListener(errorCollector));

            // Start XSLT transformation and FOP processing
            try {
                transformer.transform(src, res);
            } catch (TransformerException e) {
                throw new XslTransformationException(e, errorCollector);
            }

            // Result processing
            FormattingResults foResults = fop.getResults();

            if (foResults.getPageSequences() == null) {
                throw new XslTransformationException(errorCollector);
            }

            for (Object pageSequence : foResults.getPageSequences()) {
                PageSequenceResults pageSequenceResults = (PageSequenceResults) pageSequence;
                getLogger().debug("PageSequence {} generated {} pages.", (String.valueOf(pageSequenceResults.getID()).length() > 0
                        ? pageSequenceResults.getID() : "<no id>"), pageSequenceResults.getPageCount());
            }
            getLogger().debug("Generated {} pages in total.", foResults.getPageCount());

        }
    }
}
