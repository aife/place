package fr.atexo.annonces.boot.repository;

import fr.atexo.annonces.boot.entity.Support;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository pour accéder aux objets Support
 */
@Repository
public interface SupportRepository extends JpaRepository<Support, Integer>, JpaSpecificationExecutor<Support> {

    boolean existsByCompteObligatoireIsTrueAndActifIsTrue();
    boolean existsByCodeInAndCompteObligatoireIsTrueAndActifIsTrue(List<String> codes);
    boolean existsByCodeInAndActifIsTrue(List<String> codes);
    boolean existsByCodeDestinationInAndActifIsTrue(List<String> codeDestinations);
    boolean existsByCodeInAndCodeDestinationAndActifIsTrue(List<String> codes, String codeDestination);

    Support findByCode(String code);

    List<Support> findByCodeGroupeAndPays(String codeGroupe, String pays);
}
