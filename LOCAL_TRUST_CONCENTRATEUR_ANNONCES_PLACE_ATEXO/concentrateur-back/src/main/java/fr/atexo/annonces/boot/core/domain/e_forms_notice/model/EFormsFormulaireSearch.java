package fr.atexo.annonces.boot.core.domain.e_forms_notice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EFormsFormulaireSearch extends EFormsFormulaireId {
    private List<String> statuses;
    private Boolean onlyOrganisme;
}
