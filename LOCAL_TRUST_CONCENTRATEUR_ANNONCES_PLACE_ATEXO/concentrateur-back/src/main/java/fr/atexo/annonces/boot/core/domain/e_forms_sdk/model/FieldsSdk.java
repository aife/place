package fr.atexo.annonces.boot.core.domain.e_forms_sdk.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class FieldsSdk extends SdkVersion {
    private List<FieldDetails> fields;
    private List<XmlStructureDetails> xmlStructure;
}
