package fr.atexo.annonces.boot.core.domain.e_forms_sdk.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SubNotice {
    private String documentType;
    private String legalBasis;
    private String formType;
    private String type;
    private String description;
    private String subTypeId;
    private String _label;
    private List<String> viewTemplateIds;
    private List<SubNoticeMetadata> metadata;
    private List<SubNoticeMetadata> content;

}
