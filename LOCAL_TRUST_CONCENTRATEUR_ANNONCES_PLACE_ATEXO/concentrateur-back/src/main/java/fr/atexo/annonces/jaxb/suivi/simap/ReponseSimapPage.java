package fr.atexo.annonces.jaxb.suivi.simap;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReponseSimapPage {

    private List<ReponseSimap> content;
    private boolean first;
    private boolean last;
    @JsonProperty("total_elements")
    private int totalElements;
    private int number;
    @JsonProperty("number_of_elements")
    private int numberOfElements;
    @JsonProperty("total_pages")
    private int totalPages;
}
