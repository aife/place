package fr.atexo.annonces.mapper.tncp;

import com.atexo.annonces.commun.mpe.DonneesComplementaires;
import eu.europa.publications.resource.schema.ted.r2_0_9.reception.TedEsenders;
import fr.atexo.annonces.commun.tncp.TncpVarianteRequiseEnum;
import fr.atexo.annonces.mapper.Mappable;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class VarianteMapper implements Mappable<DonneesComplementaires, TncpVarianteRequiseEnum> {

    @Override
    public TncpVarianteRequiseEnum map(final DonneesComplementaires donneesComplementaires, TedEsenders tedEsenders, boolean rie) {
        if (Boolean.TRUE.equals(donneesComplementaires.getVarianteExigee())) {
            return TncpVarianteRequiseEnum.REQUIS;
        } else {
            if (Boolean.TRUE.equals(donneesComplementaires.getVariantesAutorisees())) {
                return TncpVarianteRequiseEnum.AUTORISE;
            } else if (Boolean.FALSE.equals(donneesComplementaires.getVariantesAutorisees())) {
                return TncpVarianteRequiseEnum.NON_AUTORISE;
            } else {
                return TncpVarianteRequiseEnum.SANS_OBJET;
            }
        }
    }
}
