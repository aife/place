package fr.atexo.annonces.boot.ws.eforms;


import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@ConfigurationProperties("external-apis.e-forms")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EformsConfiguration {
    private String basePath;
    private String apiKey;
    private Map<String, String> ws;

}
