package fr.atexo.annonces.modeles;

import fr.atexo.annonces.boot.entity.Departement;
import fr.atexo.annonces.boot.entity.Offre;
import fr.atexo.annonces.boot.entity.OffreTypeProcedure;
import fr.atexo.annonces.boot.entity.Support;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SupportSpecification implements Specification<Support> {

    private static final long serialVersionUID = 1222004680882504439L;
    private final Double montant;
    private final Boolean national;
    private final Boolean europeen;
    private final Boolean allJal;
    private final Boolean allPqr;
    private final String pays;
    private final List<String> jal;
    private final List<String> pqr;
    private final List<String> supportsCode;
    private final List<Integer> idProcedure;
    private final List<String> organisme;
    private final Set<String> codesGroupes;

    public SupportSpecification(Double montant, Boolean national, Boolean europeen, List<String> jal, List<String> pqr, String pays, List<String> supportsCode, List<Integer> idProcedure, List<String> organisme, List<String> codesGroupes, Boolean allJal, Boolean allPqr) {
        this.montant = montant;
        this.national = national;
        this.europeen = europeen;
        this.jal = jal;
        this.pqr = pqr;
        this.pays = pays;
        this.supportsCode = supportsCode;
        this.idProcedure = idProcedure;
        this.organisme = organisme;
        if (!CollectionUtils.isEmpty(codesGroupes)) {
            Set<String> list = codesGroupes.stream().filter(StringUtils::hasText).map(s -> s.split("_")[0]).collect(Collectors.toSet());
            if (Boolean.TRUE.equals(this.europeen)) {
                list.add("TED");
            }
            if (Boolean.TRUE.equals(this.national) && list.contains("MOL")) {
                list.add("BOAMPJOUE");
            }
            this.codesGroupes = list;
        } else {
            Set<String> list = new HashSet<>();
            if (Boolean.TRUE.equals(this.europeen)) {
                list.add("TED");
            }
            this.codesGroupes = list;
        }
        this.allJal = allJal;
        this.allPqr = allPqr;
    }

    @Override
    public Predicate toPredicate(Root<Support> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        criteriaQuery.distinct(true);
        criteriaQuery.orderBy(criteriaBuilder.asc(root.get("poids")));
        FetchParent<Support, Offre> offresFetch = root.fetch("offres", JoinType.LEFT);
        Join<Support, Offre> offreJoin = (Join<Support, Offre>) offresFetch;

        Predicate predicate = criteriaBuilder.equal(root.get("actif"), true);
        predicate = addOffreComptesFilter(root, criteriaBuilder, predicate);

        if (!CollectionUtils.isEmpty(this.supportsCode)) {
            predicate = addSupportCodesFilter(root, criteriaBuilder, predicate);
        }
        if (!CollectionUtils.isEmpty(this.codesGroupes)) {
            predicate = addSupportCodesGroupesFilter(root, criteriaBuilder, predicate);
        }
        if (!CollectionUtils.isEmpty(this.idProcedure) || !CollectionUtils.isEmpty(this.organisme)) {

            FetchParent<Offre, OffreTypeProcedure> procedureFetch = offreJoin.fetch("typeProceduresOrganisme", JoinType.LEFT);
            Join<Offre, OffreTypeProcedure> procedureJoin = (Join<Offre, OffreTypeProcedure>) procedureFetch;
            if (!CollectionUtils.isEmpty(this.idProcedure)) {
                predicate = addProcedureFilter(procedureJoin, criteriaBuilder, predicate);
            }
            if (!CollectionUtils.isEmpty(this.organisme)) {
                predicate = addOrganismeFilter(procedureJoin, criteriaBuilder, predicate);
            }
        }
        if (montant != null && montant > 0) {
            predicate = addMontantFilter(offreJoin, criteriaBuilder, predicate);
        }
        if (StringUtils.hasText(this.pays)) {
            predicate = addPaysFilter(root, criteriaBuilder, predicate);
        }
        if (jal != null || pqr != null || Boolean.TRUE.equals(national) || Boolean.TRUE.equals(europeen)) {
            predicate = addLocalisationFilter(root, criteriaBuilder, predicate);
        }

        return predicate;
    }

    private Predicate addOffreComptesFilter(Root<Support> root, CriteriaBuilder criteriaBuilder, Predicate predicate) {
        Predicate offresComptePredict = criteriaBuilder.disjunction();
        offresComptePredict.getExpressions().add(criteriaBuilder.isTrue(root.get("compteObligatoire")));
        offresComptePredict.getExpressions().add(criteriaBuilder.isNotEmpty(root.get("offres")));

        return criteriaBuilder.and(predicate, offresComptePredict);
    }

    private Predicate addMontantFilter(Join<Support, Offre> offreJoin, CriteriaBuilder criteriaBuilder, Predicate predicate) {
        Predicate montantMin = criteriaBuilder.or(
                criteriaBuilder.lessThanOrEqualTo(offreJoin.get("montantMin"), montant),
                criteriaBuilder.isNull(offreJoin.get("montantMin")));
        Predicate montantMax = criteriaBuilder.or(
                criteriaBuilder.greaterThan(offreJoin.get("montantMax"), montant),
                criteriaBuilder.isNull(offreJoin.get("montantMax")));
        return criteriaBuilder.and(predicate, montantMin, montantMax);
    }

    private Predicate addLocalisationFilter(Root<Support> root, CriteriaBuilder criteriaBuilder,
                                            Predicate predicate) {
        Predicate localisationFilter = criteriaBuilder.disjunction();
        if (!CollectionUtils.isEmpty(jal)) {
            FetchParent<Support, Departement> jalFetch = root.fetch("jal", JoinType.LEFT);
            Join<Support, Departement> jalJoin = (Join<Support, Departement>) jalFetch;
            localisationFilter.getExpressions().add(jalJoin.get("code").in(jal));
        } else if (Boolean.TRUE.equals(allJal)) {
            localisationFilter.getExpressions().add(criteriaBuilder.isNotEmpty(root.get("jal")));

        }
        if (!CollectionUtils.isEmpty(pqr)) {
            FetchParent<Support, Departement> pqrFetch = root.fetch("pqr", JoinType.LEFT);
            Join<Support, Departement> pqrJoin = (Join<Support, Departement>) pqrFetch;
            localisationFilter.getExpressions().add(pqrJoin.get("code").in(pqr));
        } else if (Boolean.TRUE.equals(allPqr)) {
            localisationFilter.getExpressions().add(criteriaBuilder.isNotEmpty(root.get("pqr")));
        }
        if (Boolean.TRUE.equals(national)) {
            localisationFilter.getExpressions().add(criteriaBuilder.equal(root.get("national"), national));
        }
        if (Boolean.TRUE.equals(europeen)) {
            localisationFilter.getExpressions().add(criteriaBuilder.equal(root.get("europeen"), europeen));
        }
        return criteriaBuilder.and(predicate, localisationFilter);
    }

    private Predicate addPaysFilter(Root<Support> root, CriteriaBuilder criteriaBuilder,
                                    Predicate predicate) {
        Predicate paysPredict = criteriaBuilder.disjunction();
        paysPredict.getExpressions().add(criteriaBuilder.equal(root.get("pays"), pays));
        paysPredict.getExpressions().add(criteriaBuilder.isNull(root.get("pays")));

        return criteriaBuilder.and(predicate, paysPredict);
    }

    private Predicate addSupportCodesFilter(Root<Support> root, CriteriaBuilder criteriaBuilder,
                                            Predicate predicate) {
        Predicate paysPredict = criteriaBuilder.disjunction();
        paysPredict.getExpressions().add(root.get("code").in(supportsCode));
        return criteriaBuilder.and(predicate, paysPredict);
    }

    private Predicate addSupportCodesGroupesFilter(Root<Support> root, CriteriaBuilder criteriaBuilder,
                                                   Predicate predicate) {
        Predicate paysPredict = criteriaBuilder.disjunction();
        paysPredict.getExpressions().add(root.get("codeGroupe").in(codesGroupes));
        paysPredict.getExpressions().add(criteriaBuilder.isNull(root.get("codeGroupe")));
        return criteriaBuilder.and(predicate, paysPredict);
    }

    private Predicate addProcedureFilter(Join<Offre, OffreTypeProcedure> procedureJoin, CriteriaBuilder criteriaBuilder,
                                         Predicate predicate) {
        return criteriaBuilder.and(predicate, procedureJoin.get("procedure").get("id").in(idProcedure));
    }

    private Predicate addOrganismeFilter(Join<Offre, OffreTypeProcedure> procedureJoin, CriteriaBuilder criteriaBuilder,
                                         Predicate predicate) {
        return criteriaBuilder.and(predicate, procedureJoin.get("acronymeOrganisme").in(organisme));
    }
}
