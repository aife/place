package fr.atexo.annonces.boot.core.domain.e_forms_notice.services;

import fr.atexo.annonces.boot.core.domain.e_forms_notice.model.*;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.EformsSurchargePatch;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNotice;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNoticeMetadata;
import fr.atexo.annonces.boot.entity.EFormsFormulaireEntity;
import fr.atexo.annonces.boot.entity.EFormsProcedureEntity;
import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.boot.ws.mpe.model.ConsultationContexte;
import fr.atexo.annonces.dto.EformsSurchargeDTO;
import fr.atexo.annonces.dto.LangPatch;
import fr.atexo.annonces.dto.RevisionDTO;
import fr.atexo.annonces.jaxb.suivi.simap.ReponseSimap;
import fr.atexo.annonces.modeles.StatutENoticeEnum;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import java.time.ZonedDateTime;
import java.util.List;

public interface IEFormsNoticeServices {

    EFormsFormulaire getFormulaireEForms(Long id, String plateforme);

    SubNotice getSubNoticeStructure(Long id, String plateforme);

    SubNotice getSubNotice(String plateforme, String idNotice, String versionSdk, String formulaire);

    SubNotice getSubNotice(String idNotice, String versionSdk, String plateforme);

    EFormsFormulaire save(Long id, Object formulaire, String plateforme);

    ValidationResult verify(Long id, Object objet, String plateforme, String mode);

    EFormsFormulaire submit(Long id, String plateforme, boolean updateDateEnvoi);

    EFormsFormulaireEntity updateNoticeDatePublication(ZonedDateTime dateEnvoi, EFormsFormulaireEntity eFormsFormulaireEntity);

    EFormsFormulaire changeOfficialLang(Long id, LangPatch patch, String plateforme);

    List<RevisionDTO<EFormsFormulaire>> getRevisions(Long id, boolean fetchChanges);

    EFormsFormulaire stopPublication(Long id, String plateforme);

    ValidationResult getValidationReport(Long id, String plateforme);

    void setIdRef(List<SubNoticeMetadata> metadata, String idPlateforme, String version);

    EFormsFormulaire validate(Long id, String plateforme, boolean fromAnnonce);

    EFormsFormulaire addFormulaire(String token, EFormsFormulaireAdd formulaire, String plateforme, Integer organismeId);

    EFormsProcedureEntity getProcedureEntity(String code, String libelle);

    ConsultationContexte getConsultationContexte(String serveurUrl, String serveurToken, Integer idConsultation, String idPlatform, boolean europeen, String idMpe);

    List<EFormsFormulaire> getFormulaires(EFormsFormulaireSearch formulaire, String plateforme, String organisme);

    EFormsFormulaire change(Long id, String plateforme, String token);

    EFormsFormulaire changeEFormsFormulaire(EFormsFormulaireEntity eFormsFormulaireEntity, SuiviAnnonce suiviAnnonce, String token);

    void exportToPdf(Long id, ServletOutputStream outputStream, String idPlatform);

    void setSurcharge(List<SubNoticeMetadata> content, String noticeId, String idPlatform);

    EFormsFormulaire reprendre(Long id, String plateforme, String token);

    EFormsFormulaire reprendreEFormsFormulaire(EFormsFormulaireEntity eFormsFormulaireEntity, SuiviAnnonce suiviAnnonce, String token);

    EformsSurchargeDTO surchargerAtexo(String code, String versionSdk, EformsSurchargePatch patch);

    void updateEForms(EFormsFormulaireEntity eFormsFormulaireEntity);

    EformsSurchargeDTO surchargerPlateforme(String plateforme, String id, String resourcesTag, EformsSurchargePatch patch);
    void updateTedFromList(List<ReponseSimap> reponseSimaps, String pattern);
}
