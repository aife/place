package fr.atexo.annonces.boot.repository;

import fr.atexo.annonces.boot.entity.SuiviTypeAvisPub;
import fr.atexo.annonces.config.DaoRepository;
import fr.atexo.annonces.dto.FileStatusEnum;
import fr.atexo.annonces.modeles.StatutTypeAvisAnnonceEnum;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface SuiviTypeAvisPubRepository extends DaoRepository<SuiviTypeAvisPub, Long> {
    List<SuiviTypeAvisPub> findByOrganismePlateformeUuidAndIdConsultationAndStatutIn(String plateforme, Integer idConsultation, List<StatutTypeAvisAnnonceEnum> statuts);

    List<SuiviTypeAvisPub> findByOrganismePlateformeUuidAndIdConsultationAndChildrenIsEmpty(String plateforme, Integer idConsultation);

    List<SuiviTypeAvisPub> findByOrganismePlateformeUuidAndIdConsultation(String plateforme, Integer idConsultation);

    Page<SuiviTypeAvisPub> findByStatutEditionInAndStatut(List<FileStatusEnum> statusEdition, StatutTypeAvisAnnonceEnum statut, Pageable pageable);

    List<SuiviTypeAvisPub> findByOrganismePlateformeUuid(String plateforme);


    @Query("select max(s.dateMiseEnLigneCalcule) from SuiviTypeAvisPub s where s.dateMiseEnLigneCalcule is null and s.idConsultation = :idConsultation and s.organisme.plateformeUuid = :plateformeUuid")
    Optional<ZonedDateTime> findMaxDateMiseEnLigneCalcule(Integer idConsultation, String plateformeUuid);
}
