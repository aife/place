package fr.atexo.annonces.config;

import fr.atexo.annonces.boot.configuration.AtexoAuditorAware;
import fr.atexo.annonces.boot.entity.Agent;
import fr.atexo.annonces.service.AgentService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.*;
import org.springframework.data.auditing.DateTimeProvider;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.time.ZonedDateTime;
import java.util.Optional;

@Configuration
@EnableTransactionManagement
@EntityScan({"fr.atexo.annonces.boot.entity"})
@EnableJpaRepositories({"fr.atexo.annonces.boot.repository"})
@ComponentScan
@EnableJpaAuditing(dateTimeProviderRef = "auditingDateTimeProvider", auditorAwareRef = "agentAuditorAware")
public class DatabaseConfiguration {

    @Bean
    @Primary
    @Profile({"local", "annonces"})
    public DataSource getDataSourceBatch(@Value("${spring.datasource.jndi-name}") String jndiName) {
        JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
        return dataSourceLookup.getDataSource(jndiName);
    }

    @Bean
    public DateTimeProvider auditingDateTimeProvider() {
        return () -> Optional.of(ZonedDateTime.now());
    }


    @Bean
    public AuditorAware<Agent> agentAuditorAware(AgentService agentService) {
        return new AtexoAuditorAware(agentService);
    }
}
