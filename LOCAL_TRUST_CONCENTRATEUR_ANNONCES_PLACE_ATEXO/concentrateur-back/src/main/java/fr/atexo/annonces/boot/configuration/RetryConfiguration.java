package fr.atexo.annonces.boot.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.RetryPolicy;
import org.springframework.retry.backoff.BackOffPolicy;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

@Configuration
public class RetryConfiguration {

	@Bean
	public BackOffPolicy backOffPolicy(@Value("${tncp.mpe.rest.delai}") final Long delai) {
		final var fixedBackOffPolicy = new FixedBackOffPolicy();
		fixedBackOffPolicy.setBackOffPeriod(delai);
		return fixedBackOffPolicy;
	}

	@Bean
	public RetryPolicy retryPolicy(@Value("${tncp.mpe.rest.tentatives}") final Integer tentatives) {
		final var retryPolicy = new SimpleRetryPolicy();

		retryPolicy.setMaxAttempts(tentatives);

		return retryPolicy;
	}

	@Bean
	public RetryTemplate retryTemplate(final BackOffPolicy backOffPolicy, final RetryPolicy retryPolicy) {
		final var retryTemplate = new RetryTemplate();

		retryTemplate.setBackOffPolicy(backOffPolicy);
		retryTemplate.setRetryPolicy(retryPolicy);

		return retryTemplate;
	}
}
