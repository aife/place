//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.06.02 à 03:22:05 PM CEST 
//


package fr.atexo.annonces.dto.transaction;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java pour anonymous complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PLATEFORME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ORGANISME_ACHETEUR"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ORGANISATION" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="PAYS"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;attribute name="VALUE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="E_MAIL" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="COORDONNEE_ACHETEUR"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="NOM_OFFICIEL" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="ADDRESSE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="VILLE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="POSTAL_CODE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="PAYS"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;attribute name="VALUE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="URL" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="OFFRE"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="CODE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="LIBELLE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="OBJET_CONSULTATION" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="REFERENCE_CONSULTATION" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="DLRO" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "plateforme",
        "organismeacheteur",
        "coordonneeacheteur",
        "offre",
        "objetconsultation",
        "referenceconsultation",
        "dlro"
})
@XmlRootElement(name = "TRANSACTION")
@ToString
@EqualsAndHashCode
public class TRANSACTION {

    @XmlElement(name = "PLATEFORME", required = true)
    protected String plateforme;
    @XmlElement(name = "ORGANISME_ACHETEUR", required = true)
    protected ORGANISMEACHETEUR organismeacheteur;
    @XmlElement(name = "COORDONNEE_ACHETEUR", required = true)
    protected COORDONNEEACHETEUR coordonneeacheteur;
    @XmlElement(name = "OFFRE", required = true)
    protected OFFRE offre;
    @XmlElement(name = "OBJET_CONSULTATION", required = true)
    protected String objetconsultation;
    @XmlElement(name = "REFERENCE_CONSULTATION", required = true)
    protected String referenceconsultation;
    @XmlElement(name = "DLRO", required = true)
    protected String dlro;

    /**
     * Obtient la valeur de la propriété plateforme.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPLATEFORME() {
        return plateforme;
    }

    /**
     * Définit la valeur de la propriété plateforme.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPLATEFORME(String value) {
        this.plateforme = value;
    }

    /**
     * Obtient la valeur de la propriété organismeacheteur.
     *
     * @return possible object is
     * {@link ORGANISMEACHETEUR }
     */
    public ORGANISMEACHETEUR getORGANISMEACHETEUR() {
        return organismeacheteur;
    }

    /**
     * Définit la valeur de la propriété organismeacheteur.
     *
     * @param value allowed object is
     *              {@link ORGANISMEACHETEUR }
     */
    public void setORGANISMEACHETEUR(ORGANISMEACHETEUR value) {
        this.organismeacheteur = value;
    }

    /**
     * Obtient la valeur de la propriété coordonneeacheteur.
     *
     * @return possible object is
     * {@link COORDONNEEACHETEUR }
     */
    public COORDONNEEACHETEUR getCOORDONNEEACHETEUR() {
        return coordonneeacheteur;
    }

    /**
     * Définit la valeur de la propriété coordonneeacheteur.
     *
     * @param value allowed object is
     *              {@link COORDONNEEACHETEUR }
     */
    public void setCOORDONNEEACHETEUR(COORDONNEEACHETEUR value) {
        this.coordonneeacheteur = value;
    }

    /**
     * Obtient la valeur de la propriété offre.
     *
     * @return possible object is
     * {@link OFFRE }
     */
    public OFFRE getOFFRE() {
        return offre;
    }

    /**
     * Définit la valeur de la propriété offre.
     *
     * @param value allowed object is
     *              {@link OFFRE }
     */
    public void setOFFRE(OFFRE value) {
        this.offre = value;
    }

    /**
     * Obtient la valeur de la propriété objetconsultation.
     *
     * @return possible object is
     * {@link String }
     */
    public String getOBJETCONSULTATION() {
        return objetconsultation;
    }

    /**
     * Définit la valeur de la propriété objetconsultation.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setOBJETCONSULTATION(String value) {
        this.objetconsultation = value;
    }

    /**
     * Obtient la valeur de la propriété referenceconsultation.
     *
     * @return possible object is
     * {@link String }
     */
    public String getREFERENCECONSULTATION() {
        return referenceconsultation;
    }

    /**
     * Définit la valeur de la propriété referenceconsultation.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setREFERENCECONSULTATION(String value) {
        this.referenceconsultation = value;
    }

    /**
     * Obtient la valeur de la propriété dlro.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDLRO() {
        return dlro;
    }

    /**
     * Définit la valeur de la propriété dlro.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDLRO(String value) {
        this.dlro = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="NOM_OFFICIEL" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="ADDRESSE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="VILLE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="POSTAL_CODE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="PAYS"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;attribute name="VALUE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="URL" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "nomofficiel",
            "addresse",
            "ville",
            "postalcode",
            "pays",
            "url"
    })
    @ToString
    @EqualsAndHashCode
    public static class COORDONNEEACHETEUR {

        @XmlElement(name = "NOM_OFFICIEL", required = true)
        protected String nomofficiel;
        @XmlElement(name = "ADDRESSE", required = true)
        protected String addresse;
        @XmlElement(name = "VILLE", required = true)
        protected String ville;
        @XmlElement(name = "POSTAL_CODE", required = true)
        protected String postalcode;
        @XmlElement(name = "PAYS", required = true)
        protected PAYS pays;
        @XmlElement(name = "URL", required = true)
        protected String url;

        /**
         * Obtient la valeur de la propriété nomofficiel.
         *
         * @return possible object is
         * {@link String }
         */
        public String getNOMOFFICIEL() {
            return nomofficiel;
        }

        /**
         * Définit la valeur de la propriété nomofficiel.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setNOMOFFICIEL(String value) {
            this.nomofficiel = value;
        }

        /**
         * Obtient la valeur de la propriété addresse.
         *
         * @return possible object is
         * {@link String }
         */
        public String getADDRESSE() {
            return addresse;
        }

        /**
         * Définit la valeur de la propriété addresse.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setADDRESSE(String value) {
            this.addresse = value;
        }

        /**
         * Obtient la valeur de la propriété ville.
         *
         * @return possible object is
         * {@link String }
         */
        public String getVILLE() {
            return ville;
        }

        /**
         * Définit la valeur de la propriété ville.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setVILLE(String value) {
            this.ville = value;
        }

        /**
         * Obtient la valeur de la propriété postalcode.
         *
         * @return possible object is
         * {@link String }
         */
        public String getPOSTALCODE() {
            return postalcode;
        }

        /**
         * Définit la valeur de la propriété postalcode.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setPOSTALCODE(String value) {
            this.postalcode = value;
        }

        /**
         * Obtient la valeur de la propriété pays.
         *
         * @return possible object is
         * {@link PAYS }
         */
        public PAYS getPAYS() {
            return pays;
        }

        /**
         * Définit la valeur de la propriété pays.
         *
         * @param value allowed object is
         *              {@link PAYS }
         */
        public void setPAYS(PAYS value) {
            this.pays = value;
        }

        /**
         * Obtient la valeur de la propriété url.
         *
         * @return possible object is
         * {@link String }
         */
        public String getURL() {
            return url;
        }

        /**
         * Définit la valeur de la propriété url.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setURL(String value) {
            this.url = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;attribute name="VALUE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        @ToString
        @EqualsAndHashCode
        public static class PAYS {

            @XmlAttribute(name = "VALUE", required = true)
            protected String value;

            /**
             * Obtient la valeur de la propriété value.
             *
             * @return possible object is
             * {@link String }
             */
            public String getVALUE() {
                return value;
            }

            /**
             * Définit la valeur de la propriété value.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setVALUE(String value) {
                this.value = value;
            }

        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="CODE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="LIBELLE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "code",
            "libelle"
    })
    @ToString
    @EqualsAndHashCode
    public static class OFFRE {

        @XmlElement(name = "CODE", required = true)
        protected String code;
        @XmlElement(name = "LIBELLE", required = true)
        protected String libelle;

        /**
         * Obtient la valeur de la propriété code.
         *
         * @return possible object is
         * {@link String }
         */
        public String getCODE() {
            return code;
        }

        /**
         * Définit la valeur de la propriété code.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setCODE(String value) {
            this.code = value;
        }

        /**
         * Obtient la valeur de la propriété libelle.
         *
         * @return possible object is
         * {@link String }
         */
        public String getLIBELLE() {
            return libelle;
        }

        /**
         * Définit la valeur de la propriété libelle.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setLIBELLE(String value) {
            this.libelle = value;
        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ORGANISATION" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="PAYS"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;attribute name="VALUE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="E_MAIL" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "organisation",
            "pays",
            "email"
    })
    @ToString
    @EqualsAndHashCode
    public static class ORGANISMEACHETEUR {

        @XmlElement(name = "ORGANISATION", required = true)
        protected String organisation;
        @XmlElement(name = "PAYS", required = true)
        protected PAYS pays;
        @XmlElement(name = "E_MAIL", required = true)
        protected String email;

        /**
         * Obtient la valeur de la propriété organisation.
         *
         * @return possible object is
         * {@link String }
         */
        public String getORGANISATION() {
            return organisation;
        }

        /**
         * Définit la valeur de la propriété organisation.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setORGANISATION(String value) {
            this.organisation = value;
        }

        /**
         * Obtient la valeur de la propriété pays.
         *
         * @return possible object is
         * {@link PAYS }
         */
        public PAYS getPAYS() {
            return pays;
        }

        /**
         * Définit la valeur de la propriété pays.
         *
         * @param value allowed object is
         *              {@link PAYS }
         */
        public void setPAYS(PAYS value) {
            this.pays = value;
        }

        /**
         * Obtient la valeur de la propriété email.
         *
         * @return possible object is
         * {@link String }
         */
        public String getEMAIL() {
            return email;
        }

        /**
         * Définit la valeur de la propriété email.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setEMAIL(String value) {
            this.email = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;attribute name="VALUE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        @ToString
        @EqualsAndHashCode
        public static class PAYS {

            @XmlAttribute(name = "VALUE", required = true)
            protected String value;

            /**
             * Obtient la valeur de la propriété value.
             *
             * @return possible object is
             * {@link String }
             */
            public String getVALUE() {
                return value;
            }

            /**
             * Définit la valeur de la propriété value.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setVALUE(String value) {
                this.value = value;
            }

        }

    }

}
