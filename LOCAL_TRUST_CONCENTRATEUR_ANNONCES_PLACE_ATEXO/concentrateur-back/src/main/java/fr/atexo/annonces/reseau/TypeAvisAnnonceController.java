package fr.atexo.annonces.reseau;

import fr.atexo.annonces.boot.configuration.http_logging.LogEntryExit;
import fr.atexo.annonces.boot.entity.PieceJointe;
import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.dto.*;
import fr.atexo.annonces.modeles.DashboardSuiviTypeAvisSpecification;
import fr.atexo.annonces.service.PieceJointeService;
import fr.atexo.annonces.service.SuiviAnnonceService;
import fr.atexo.annonces.service.SuiviTypeAvisPubService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/rest/v2/type-avis-annonces")
@Tag(name = "TypeAvisAnnonceController")
@Slf4j
public class TypeAvisAnnonceController {


    private final SuiviTypeAvisPubService suiviTypeAvisPubService;
    private final SuiviAnnonceService suiviAnnonceService;
    private final PieceJointeService pieceJointeService;

    public TypeAvisAnnonceController(SuiviTypeAvisPubService suiviTypeAvisPubService, SuiviAnnonceService suiviAnnonceService, PieceJointeService pieceJointeService) {
        this.suiviTypeAvisPubService = suiviTypeAvisPubService;
        this.suiviAnnonceService = suiviAnnonceService;
        this.pieceJointeService = pieceJointeService;
    }


    @GetMapping
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('read','write')")
    public PageDTO<SuiviTypeAvisPubDTO> getFormulaires(Pageable pageable, @RequestParam String idPlatform, DashboardSuiviTypeAvisSpecification facturationSearch) {
        facturationSearch.setPlateformes(Set.of(idPlatform));
        return suiviTypeAvisPubService.getFormulaires(idPlatform, facturationSearch, pageable);
    }

    @GetMapping("consultation/{idConsultation}")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public List<SuiviTypeAvisPubDTO> getFormulairesList(@RequestParam String idPlatform, @PathVariable Integer idConsultation) {
        return suiviTypeAvisPubService.getConsultationAnnonces(idPlatform, idConsultation);
    }

    @PostMapping
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #avisPub.getIdConsultation())")
    public SuiviTypeAvisPubDTO addSuiviTypeAvisPub(@RequestParam String idPlatform, @RequestBody @Valid @NotNull SuiviTypeAvisPubAdd avisPub, @RequestHeader("Authorization") String token) {
        return suiviTypeAvisPubService.addSuiviTypeAvisPub(avisPub, token.replace("Bearer ", ""), idPlatform);
    }

    @PatchMapping("{id}")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and (#oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation) or #oauth2.hasAnyScope('validation:sip','validation'))")
    public SuiviTypeAvisPubDTO modifySuiviTypeAvisPub(@PathVariable Long id, @RequestParam String idPlatform, @RequestParam Integer idConsultation, @RequestBody @Valid @NotNull SuiviTypeAvisPubPatch avisPub, @RequestHeader("Authorization") String token) {
        return suiviTypeAvisPubService.modifySuiviTypeAvisPub(id, avisPub, token.replace("Bearer ", ""), idPlatform, idConsultation);
    }

    @PatchMapping("{id}/reprendre")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public SuiviTypeAvisPubDTO reprendre(@PathVariable Long id, @RequestParam String idPlatform, @RequestParam Integer idConsultation, @RequestHeader("Authorization") String token) {
        return suiviTypeAvisPubService.reprendre(id, token.replace("Bearer ", ""), idPlatform, idConsultation);
    }

    @PatchMapping("{id}/change")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public SuiviTypeAvisPubDTO change(@PathVariable Long id, @RequestParam String idPlatform, @RequestParam Integer idConsultation, @RequestHeader("Authorization") String token) {
        return suiviTypeAvisPubService.change(id, token.replace("Bearer ", ""), idPlatform, idConsultation);
    }

    @GetMapping("{id}")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    @LogEntryExit(skip = true)
    public SuiviTypeAvisPubDTO get(@PathVariable Long id, @RequestParam String idPlatform, @RequestParam Integer idConsultation, @RequestHeader("Authorization") String token) {
        return suiviTypeAvisPubService.get(id, idPlatform, idConsultation);
    }

    @GetMapping("{id}/historiques")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public List<RevisionDTO<SuiviTypeAvisPubRevisionDTO>> getHistoriques(@PathVariable Long id, @RequestParam String idPlatform, @RequestParam Integer idConsultation) {
        return suiviTypeAvisPubService.getHistoriques(id, idPlatform, idConsultation);
    }


    @DeleteMapping("{id}")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public boolean deleteSuiviTypeAvisPub(@PathVariable Long id, @RequestParam String idPlatform, @RequestHeader("Authorization") String token, @RequestParam Integer idConsultation) {
        return suiviTypeAvisPubService.deleteSuiviTypeAvisPub(id, token.replace("Bearer ", ""), idPlatform, idConsultation);
    }

    @GetMapping("{id}/pdf-portail")
    @Operation(description = "swagger.annonces.pdf.post.operation.summary", summary = "swagger.annonces.pdf.post.operation.notes", responses = {@ApiResponse(responseCode = HttpServletResponse.SC_UNAUTHORIZED + "", description = "swagger.response.unauthorized"), @ApiResponse(responseCode = HttpServletResponse.SC_BAD_REQUEST + "", description = "swagger.response.badRequest")})
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public void getPortailPdf(@PathVariable Long id, @RequestParam String idPlatform, @RequestParam Integer idConsultation, HttpServletResponse response) throws IOException {
        try {
            SuiviAnnonce annonce = suiviTypeAvisPubService.getPortailAnnonce(id, idPlatform, idConsultation);
            String filename = suiviAnnonceService.geDocumentName(annonce, true);
            response.setHeader("Content-Disposition", "attachment; filename=" + filename);
            response.setContentType(MediaType.APPLICATION_PDF_VALUE);
            suiviAnnonceService.exportToDocument(annonce, response.getOutputStream(), false, true);
        } catch (Exception e) {
            resetServletResponse(response, e);
            throw e;
        }
    }

    @GetMapping("consultation/{idConsultation}/pdf-portail")
    @Operation(description = "swagger.annonces.pdf.post.operation.summary", summary = "swagger.annonces.pdf.post.operation.notes", responses = {@ApiResponse(responseCode = HttpServletResponse.SC_UNAUTHORIZED + "", description = "swagger.response.unauthorized"), @ApiResponse(responseCode = HttpServletResponse.SC_BAD_REQUEST + "", description = "swagger.response.badRequest")})
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public void getLastPortailPdf(@RequestParam String idPlatform, @PathVariable Integer idConsultation, HttpServletResponse response) throws IOException {

        try {
            SuiviAnnonce annonce = suiviTypeAvisPubService.getLastPortailPdf(idPlatform, idConsultation);
            String filename = suiviAnnonceService.geDocumentName(annonce, true);
            response.setHeader("Content-Disposition", "attachment; filename=" + filename);
            response.setContentType(MediaType.APPLICATION_PDF_VALUE);
            suiviAnnonceService.exportToDocument(annonce, response.getOutputStream(), false, true);
        } catch (Exception e) {
            resetServletResponse(response, e);
            throw e;
        }
    }

    @GetMapping("consultation/{idConsultation}/last")
    @Operation(description = "swagger.annonces.pdf.post.operation.summary", summary = "swagger.annonces.pdf.post.operation.notes", responses = {@ApiResponse(responseCode = HttpServletResponse.SC_UNAUTHORIZED + "", description = "swagger.response.unauthorized"), @ApiResponse(responseCode = HttpServletResponse.SC_BAD_REQUEST + "", description = "swagger.response.badRequest")})
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public List<SuiviOffreDTO> getLastAvis(@RequestParam String idPlatform, @PathVariable Integer idConsultation) {
        return suiviTypeAvisPubService.getLastAvis(idPlatform, idConsultation);

    }

    @GetMapping("consultation/{idConsultation}/all")
    @Operation(description = "swagger.annonces.pdf.post.operation.summary", summary = "swagger.annonces.pdf.post.operation.notes", responses = {@ApiResponse(responseCode = HttpServletResponse.SC_UNAUTHORIZED + "", description = "swagger.response.unauthorized"), @ApiResponse(responseCode = HttpServletResponse.SC_BAD_REQUEST + "", description = "swagger.response.badRequest")})
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public List<SuiviOffreDTO> getAllAvis(@RequestParam String idPlatform, @PathVariable Integer idConsultation) {

        return suiviTypeAvisPubService.getAllAvis(idPlatform, idConsultation);

    }

    @GetMapping("consultation/synchronize")
    @Operation(description = "swagger.annonces.pdf.post.operation.summary", summary = "swagger.annonces.pdf.post.operation.notes", responses = {@ApiResponse(responseCode = HttpServletResponse.SC_UNAUTHORIZED + "", description = "swagger.response.unauthorized"), @ApiResponse(responseCode = HttpServletResponse.SC_BAD_REQUEST + "", description = "swagger.response.badRequest")})
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform)")
    public List<SuiviOffreDTO> synchronize(@RequestParam String idPlatform, @RequestParam String dateDebut, @RequestParam String dateFin) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");


        LocalDateTime zonedDateDebut = LocalDateTime.parse(dateDebut, formatter);
        LocalDateTime parsedDateFin = LocalDateTime.parse(dateFin, formatter);
        if (parsedDateFin.isBefore(zonedDateDebut)) {
            throw new IllegalArgumentException("La date de fin doit être supérieure à la date de début");
        }
        return suiviTypeAvisPubService.synchronize(idPlatform, zonedDateDebut.atZone(ZoneId.of("Europe/Paris")), parsedDateFin.atZone(ZoneId.of("Europe/Paris")));

    }


    @GetMapping("export")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('read','write')")
    public ResponseEntity<InputStreamResource> exporterFormulaires(HttpServletResponse response, @RequestParam String idPlatform, DashboardSuiviTypeAvisSpecification facturationSearch) throws IOException {
        facturationSearch.setPlateformes(Set.of(idPlatform));
        ByteArrayOutputStream file = suiviTypeAvisPubService.exporterFormulaires(response, facturationSearch, idPlatform);
        Resource resource = new ByteArrayResource(file.toByteArray());
        return ResponseEntity.ok().contentType(MediaType.valueOf("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")).header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=output.xlsx").body(new InputStreamResource(resource.getInputStream()));
    }

    @PatchMapping("{id}/validation-sip")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('validation:sip','validation')")
    public SuiviTypeAvisPubDTO updateValidationSip(@RequestParam String idPlatform, @PathVariable Long id, @RequestHeader("Authorization") String token, @RequestBody @Valid @NotNull AvisValidationPatch patch) {
        return suiviTypeAvisPubService.updateValidationSip(idPlatform, id, patch, token.replace("Bearer ", ""));
    }

    @PatchMapping("{id}/validation-eco")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('validation:eco','validation')")
    public SuiviTypeAvisPubDTO updateValidationEco(@RequestParam String idPlatform, @PathVariable Long id, @RequestHeader("Authorization") String token, @RequestBody @Valid @NotNull AvisValidationPatch patch) {
        return suiviTypeAvisPubService.updateValidationEco(idPlatform, id, patch, token.replace("Bearer ", ""));
    }
    @PatchMapping("{id}/invalidation-eco")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('validation:eco','validation')")
    public SuiviTypeAvisPubDTO invalidationEco(@RequestParam String idPlatform, @PathVariable Long id, @RequestHeader("Authorization") String token, @RequestBody @Valid @NotNull AvisValidationPatch patch) {
        return suiviTypeAvisPubService.invalidationEco(idPlatform, id, patch, token.replace("Bearer ", ""));
    }

    @GetMapping("{id}/editeur-en-ligne")
    public EditionRequest modifyAvisDocument(@PathVariable Long id, @RequestHeader("Authorization") String token, @RequestParam String idPlatform, @RequestParam Integer idConsultation) {
        return suiviTypeAvisPubService.modifyAvisDocument(id, idPlatform, idConsultation, token.replace("Bearer ", ""));

    }

    @PostMapping("{id}/editeur-en-ligne/callback")
    public TrackDocumentResponse trackDocument(@PathVariable Long id, @RequestBody FileStatus status) {
        return suiviTypeAvisPubService.getDocumentCallback(id, status);
    }

    @PatchMapping("{idConsultation}/dmls")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('validation:eco','validation')")
    public List<SuiviTypeAvisPubDTO> updateDateDeMiseEnLigne(@RequestParam String plateforme, @RequestParam String dmls, @PathVariable Integer idConsultation) {
        return suiviTypeAvisPubService.updateDateDeMiseEnLigne(idConsultation, plateforme, dmls);
    }

    @PatchMapping("{id}/mail")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and (#oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation) or #oauth2.hasAnyScope('validation:sip','validation'))")
    public SuiviTypeAvisPubDTO updateMail(@RequestParam String idPlatform, @RequestParam Integer idConsultation, @RequestBody @Valid @NotNull MailUpdate update, @PathVariable Long id) {
        return suiviTypeAvisPubService.updateMail(id, update, idPlatform, idConsultation);
    }


    @PatchMapping("{id}/demande-validation")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public ResponseEntity<SuiviTypeAvisPubDTO> damandeValidationFacturation(@PathVariable Long id, @RequestParam String idPlatform, @RequestParam Integer idConsultation, @RequestHeader("Authorization") String token) {
        return new ResponseEntity<>(suiviTypeAvisPubService.demandeValidation(id, idConsultation, idPlatform, token.replace("Bearer ", "")), HttpStatus.OK);

    }

    @GetMapping("/statistiques")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform)")
    public List<AnnonceStatistique> getStatistiqueSuiviAnnonces(@RequestParam String idPlatform, @RequestParam(required = false) Boolean sip) {
        return suiviTypeAvisPubService.getStatistiqueSuiviAnnonces(idPlatform, sip);

    }

    @GetMapping("/export-statistiques")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform)")
    public ResponseEntity<InputStreamResource> getStatistiqueSuiviAnnonces(HttpServletResponse response, @RequestParam String idPlatform, @RequestParam(required = false) Boolean sip) throws IOException {
        ByteArrayOutputStream file = suiviTypeAvisPubService.exportStatistiqueSuiviAnnonces(response, idPlatform, sip);
        Resource resource = new ByteArrayResource(file.toByteArray());
        return ResponseEntity.ok().contentType(MediaType.valueOf("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")).header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=output.xlsx").body(new InputStreamResource(resource.getInputStream()));
    }

    @PostMapping("{id}/avis-externe-import")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public SuiviTypeAvisPubDTO importerAvisExterne(@PathVariable Long id, @RequestParam String idPlatform, @RequestParam Integer idConsultation, @RequestHeader("Authorization") String token, @RequestPart @NotEmpty(message = "la piece jointe ne doit pas être vide") @Valid MultipartFile document) throws IOException {
        LocalDateTime now = LocalDateTime.now();
        PieceJointe pieceJointe = PieceJointe.builder().contentType(document.getContentType()).name(document.getOriginalFilename()).size(document.getSize()).build();
        return suiviTypeAvisPubService.importerAvisExterne(id, idPlatform, idConsultation, token.replace("Bearer ", ""), pieceJointe, document.getInputStream());
    }

    @PostMapping("{id}/document-import")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public SuiviTypeAvisPubPieceJointeAssoDTO importerDocument(@PathVariable Long id, @RequestParam String idPlatform, @RequestParam Integer idConsultation, @RequestPart @NotEmpty(message = "la piece jointe ne doit pas être vide") @Valid MultipartFile document) throws IOException {
        PieceJointe pieceJointe = PieceJointe.builder().contentType(document.getContentType()).name(document.getOriginalFilename()).size(document.getSize()).build();
        return suiviTypeAvisPubService.importerDocument(id, idPlatform, idConsultation, pieceJointe, document.getInputStream());
    }

    @GetMapping("{id}/documents")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public List<SuiviTypeAvisPubPieceJointeAssoDTO> getDocuments(@PathVariable Long id, @RequestParam String idPlatform, @RequestParam Integer idConsultation, @RequestHeader("Authorization") String token) {
        return suiviTypeAvisPubService.getDocuments(id, idPlatform, idConsultation, token.replace("Bearer ", ""));
    }

    @DeleteMapping("{id}/piece-jointe/{idAsso}")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public SuiviTypeAvisPubDTO deletePieceJointe(@PathVariable("id") Long id, @PathVariable("idAsso") Long idAsso, @RequestParam String idPlatform, @RequestParam Integer idConsultation, @RequestHeader("Authorization") String token) {
        return suiviTypeAvisPubService.deletePieceJointeByIdTypeAvisAndIdAsso(id, idAsso, idPlatform, idConsultation);

    }

    @DeleteMapping("{id}/avis-externe")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public SuiviTypeAvisPubDTO deleteAvisExterne(@PathVariable("id") Long id, @RequestParam Integer idConsultation, @RequestParam String idPlatform, @RequestHeader("Authorization") String token) {
        return suiviTypeAvisPubService.deleteAvisExterne(id, idPlatform, idConsultation, token.replace("Bearer ", ""));

    }


    @GetMapping("/{id}/download/{idPieceJointe}")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public ResponseEntity<Resource> downloadFile(@PathVariable Long id, @PathVariable Long idPieceJointe, HttpServletRequest request, @RequestParam Integer idConsultation, @RequestParam String idPlatform, @RequestHeader("Authorization") String token) {
        PieceJointe document = suiviTypeAvisPubService.getPieceJointe(id, idPieceJointe, idPlatform, idConsultation, token.replace("Bearer ", ""));
        Resource resource = pieceJointeService.loadFileAsResource(document.getPath());
        String contentType = document.getContentType();
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException e) {
            log.info("Could not determine file type.");
        }
        if (contentType == null) {
            contentType = "application/octet-stream";
        }
        String resultFileName = document.getName();
        if (resultFileName == null) {
            resultFileName = resource.getFilename();
        }
        return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType)).header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resultFileName + "\"").body(resource);
    }

    @PostMapping("{idPlatform}/statut")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform)")
    public SuiviTypeAvisPubDTO fixStatus(@PathVariable String idPlatform, @RequestHeader("Authorization") String token) {
        return suiviTypeAvisPubService.fixStatus(token.replace("Bearer ", ""), idPlatform);
    }


    private void resetServletResponse(HttpServletResponse response, Exception e) {
        try {
            response.reset();
        } catch (IllegalStateException f) {
            log.error("Erreur survenue lors de la génération du PDF ; impossible de transmettre cette erreur" + " au client car des données du PDF ont déjà été envoyées", e);
            throw f;
        }
    }

}
