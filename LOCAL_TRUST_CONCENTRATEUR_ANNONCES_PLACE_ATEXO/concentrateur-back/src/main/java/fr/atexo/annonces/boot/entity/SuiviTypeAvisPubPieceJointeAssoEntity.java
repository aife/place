package fr.atexo.annonces.boot.entity;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "suivi_type_avis_pub_piece_jointe_asso")
@Builder(toBuilder = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class SuiviTypeAvisPubPieceJointeAssoEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ID_SUIVI_TYPE_PUB")
    private SuiviTypeAvisPub suiviTypeAvisPub;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ID_PIECE_JOINTE")
    private PieceJointe pieceJointe;

}
