package fr.atexo.annonces.dto.eforms;

import lombok.*;

import java.io.Serializable;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class EformsSubmitResponseDTO implements Serializable {

    private boolean success;

    private String cvsLink;
}
