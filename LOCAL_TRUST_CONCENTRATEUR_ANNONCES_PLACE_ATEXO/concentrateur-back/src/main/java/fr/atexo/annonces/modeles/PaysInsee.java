package fr.atexo.annonces.modeles;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaysInsee {
    private String code;
    private String pays;
}
