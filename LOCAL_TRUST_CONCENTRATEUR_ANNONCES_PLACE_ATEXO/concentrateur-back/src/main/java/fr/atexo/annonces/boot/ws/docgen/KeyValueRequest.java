package fr.atexo.annonces.boot.ws.docgen;

import lombok.*;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class KeyValueRequest {
    @NotEmpty
    private String key;
    private Object value;
}
