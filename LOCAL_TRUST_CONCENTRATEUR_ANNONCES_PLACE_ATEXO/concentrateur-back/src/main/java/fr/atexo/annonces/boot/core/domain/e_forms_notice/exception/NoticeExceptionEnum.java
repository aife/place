package fr.atexo.annonces.boot.core.domain.e_forms_notice.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum NoticeExceptionEnum {
    NOT_FOUND(404, "Formulaire non existant"), SAVE_ERROR(500, "Erreur lors de l'enregistrement de Formulaire"),
    NOT_AUTHORIZED(401, "Formulaire non autorisé"),
    MANDATORY(400, "Champs obligatoires"),
    ERREUR(500, "Erreur lors de la récupération de Formulaire");

    private final int code;
    private final String type;

}
