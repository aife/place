package fr.atexo.annonces.boot.repository;

import fr.atexo.annonces.boot.entity.AgentConsultationSessionEntity;
import fr.atexo.annonces.config.DaoRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface AgentConsultationSessionRepository extends DaoRepository<AgentConsultationSessionEntity, Integer> {
    Optional<AgentConsultationSessionEntity> findByAgentIdAndIdConsultation(String idAgent, Integer idConsultation);

    List<AgentConsultationSessionEntity> findByIdConsultation(Integer idConsultation);

    List<AgentConsultationSessionEntity> findByAgentOrganismePlateformeUuidAndIdConsultation(String plateformeUuid, Integer idConsultation);

    @Modifying
    @Transactional
    @Query("update AgentConsultationSessionEntity c set c.contexte=:contexteString where c.agent.id = :idAgent and c.idConsultation = :idConsultation")
    void updateContexte(@Param("idConsultation") Integer idConsultation, @Param("idAgent") String idAgent, @Param("contexteString") String contexteString);
}

