package fr.atexo.annonces.jaxb.suivi.simap;

/**
 * Created by qba on 29/08/16.
 */

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "submission_id",
        "received_at",
        "status",
        "reason_code",
        "status_updated_at",
        "no_doc_ext",
        "form",
        "languages",
        "publication_info",
        "technical_validation_report",
        "validation_rules_report",
        "quality_control_report",
        "ref_submission_id",
        "ref_no_doc_ojs"
})
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReponseSimap {

    @JsonProperty("submission_id")
    private String submissionId;
    @JsonProperty("received_at")
    private String receivedAt;
    @JsonProperty("status")
    private String status;
    @JsonProperty("reason_code")
    private Object reasonCode;
    @JsonProperty("status_updated_at")
    private String statusUpdatedAt;
    @JsonProperty("no_doc_ext")
    private String noDocExt;
    @JsonProperty("form")
    private String form;
    @JsonProperty("languages")
    private List<String> languages = null;
    @JsonProperty("publication_info")
    private PublicationInfo publicationInfo;
    @JsonProperty("technical_validation_report")
    private TechnicalValidationReport technicalValidationReport;
    @JsonProperty("validation_rules_report")
    private ValidationRulesReport validationRulesReport;
    @JsonProperty("quality_control_report")
    private Object qualityControlReport;
    @JsonProperty("ref_submission_id")
    private Object refSubmissionId;
    @JsonProperty("ref_no_doc_ojs")
    private Object refNoDocOjs;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("submission_id")
    public String getSubmissionId() {
        return submissionId;
    }

    @JsonProperty("submission_id")
    public void setSubmissionId(String submissionId) {
        this.submissionId = submissionId;
    }

    @JsonProperty("received_at")
    public String getReceivedAt() {
        return receivedAt;
    }

    @JsonProperty("received_at")
    public void setReceivedAt(String receivedAt) {
        this.receivedAt = receivedAt;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("reason_code")
    public Object getReasonCode() {
        return reasonCode;
    }

    @JsonProperty("reason_code")
    public void setReasonCode(Object reasonCode) {
        this.reasonCode = reasonCode;
    }

    @JsonProperty("status_updated_at")
    public String getStatusUpdatedAt() {
        return statusUpdatedAt;
    }

    @JsonProperty("status_updated_at")
    public void setStatusUpdatedAt(String statusUpdatedAt) {
        this.statusUpdatedAt = statusUpdatedAt;
    }

    @JsonProperty("no_doc_ext")
    public String getNoDocExt() {
        return noDocExt;
    }

    @JsonProperty("no_doc_ext")
    public void setNoDocExt(String noDocExt) {
        this.noDocExt = noDocExt;
    }

    @JsonProperty("form")
    public String getForm() {
        return form;
    }

    @JsonProperty("form")
    public void setForm(String form) {
        this.form = form;
    }

    @JsonProperty("languages")
    public List<String> getLanguages() {
        return languages;
    }

    @JsonProperty("languages")
    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    @JsonProperty("publication_info")
    public PublicationInfo getPublicationInfo() {
        return publicationInfo;
    }

    @JsonProperty("publication_info")
    public void setPublicationInfo(PublicationInfo publicationInfo) {
        this.publicationInfo = publicationInfo;
    }

    @JsonProperty("technical_validation_report")
    public TechnicalValidationReport getTechnicalValidationReport() {
        return technicalValidationReport;
    }

    @JsonProperty("technical_validation_report")
    public void setTechnicalValidationReport(TechnicalValidationReport technicalValidationReport) {
        this.technicalValidationReport = technicalValidationReport;
    }

    @JsonProperty("validation_rules_report")
    public ValidationRulesReport getValidationRulesReport() {
        return validationRulesReport;
    }

    @JsonProperty("validation_rules_report")
    public void setValidationRulesReport(ValidationRulesReport validationRulesReport) {
        this.validationRulesReport = validationRulesReport;
    }

    @JsonProperty("quality_control_report")
    public Object getQualityControlReport() {
        return qualityControlReport;
    }

    @JsonProperty("quality_control_report")
    public void setQualityControlReport(Object qualityControlReport) {
        this.qualityControlReport = qualityControlReport;
    }

    @JsonProperty("ref_submission_id")
    public Object getRefSubmissionId() {
        return refSubmissionId;
    }

    @JsonProperty("ref_submission_id")
    public void setRefSubmissionId(Object refSubmissionId) {
        this.refSubmissionId = refSubmissionId;
    }

    @JsonProperty("ref_no_doc_ojs")
    public Object getRefNoDocOjs() {
        return refNoDocOjs;
    }

    @JsonProperty("ref_no_doc_ojs")
    public void setRefNoDocOjs(Object refNoDocOjs) {
        this.refNoDocOjs = refNoDocOjs;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
