package fr.atexo.annonces.boot.repository;

import fr.atexo.annonces.boot.entity.TypeProcedure;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeProcedureRepository extends JpaRepository<TypeProcedure, Integer>, JpaSpecificationExecutor<TypeProcedure> {

    TypeProcedure findByAbbreviationAndLibelle(String abbreviationProcedure, String libelle);

}
