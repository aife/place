package fr.atexo.annonces.boot.core.domain.e_forms_sdk.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FieldDetails {
    private String id;
    private String parentNodeId;
    private String presetValue;
    private String name;
    private String btId;
    private String xpathAbsolute;
    private String xpathRelative;
    private String type;
    private String legalType;
    private Privacy privacy;
    private RepeatableDetails repeatable;
    private MandatoryForbiddenDetails forbidden;
    private MandatoryForbiddenDetails mandatory;
    private CodeList codeList;
    private List<String> idSchemes;
    private String idScheme;
    private String attributeName;
    private String attributeOf;
    private List<String> attributes;
    private Integer maxLength;
    @JsonProperty("assert")
    private Assert assertSdk;
    private PatternField pattern;
    private List<XmlStructureDetails> nodeIds;


}
