package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.repository.DepartementRepository;
import fr.atexo.annonces.dto.DepartementDTO;
import fr.atexo.annonces.mapper.DepartementMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Gestion des départements
 */
@Service
public class DepartementServiceImpl implements DepartementService {

    private final DepartementRepository departementRepository;
    private final DepartementMapper departementMapper;

    public DepartementServiceImpl(DepartementRepository departementRepository, DepartementMapper departementMapper) {
        this.departementRepository = departementRepository;
        this.departementMapper = departementMapper;
    }

    @Override
    public List<DepartementDTO> getDepartements() {
        return departementMapper.departementsToDepartementDTOs(departementRepository.findAll());
    }
}
