package fr.atexo.annonces.boot.repository;

import fr.atexo.annonces.boot.entity.AgentSessionEntity;
import fr.atexo.annonces.config.DaoRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;

@Repository
public interface AgentSessionRepository extends DaoRepository<AgentSessionEntity, Integer> {
    AgentSessionEntity findByToken(String token);

    boolean existsByToken(String token);

    Page<AgentSessionEntity> findAll(Pageable pageable);
}

