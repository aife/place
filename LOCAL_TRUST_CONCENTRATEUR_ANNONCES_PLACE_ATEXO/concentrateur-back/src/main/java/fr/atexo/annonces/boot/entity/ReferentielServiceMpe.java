package fr.atexo.annonces.boot.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Entity
@Table(name = "referential_service_mpe", uniqueConstraints = {@UniqueConstraint(columnNames = {"code", "id_organisme"})}, indexes = {@Index(name = "referentiel_mpe_type_code_index", columnList = "code,id_organisme")})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor

public class ReferentielServiceMpe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_organisme")
    private Organisme organisme;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "libelle", columnDefinition = "TEXT", nullable = false)
    private String libelle;

    @Column(name = "ACTIF", nullable = false)
    private boolean actif;

    @Column(name = "ID_MPE")
    private Long idMpe;

}
