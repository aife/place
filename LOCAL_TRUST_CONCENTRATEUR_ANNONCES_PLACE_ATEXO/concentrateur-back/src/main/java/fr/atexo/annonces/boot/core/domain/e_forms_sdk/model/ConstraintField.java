package fr.atexo.annonces.boot.core.domain.e_forms_sdk.model;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ConstraintField {
    private String severity;
    private String condition;
    private String message;
    private String value;
}
