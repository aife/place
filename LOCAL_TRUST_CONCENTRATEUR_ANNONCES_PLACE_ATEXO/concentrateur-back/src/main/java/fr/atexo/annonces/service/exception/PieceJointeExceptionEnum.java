package fr.atexo.annonces.service.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PieceJointeExceptionEnum {
    NOT_FOUND(404, "Piece jointe non existante"), SAVE_ERROR(500, "Erreur lors de l'enregistrement de la pièce jointe"),
    MANDATORY(400, "Champs obligatoires"), FOLDER_ERROR(500, "Erreur lors de la création du dossier");

    private final int code;
    private final String type;

}
