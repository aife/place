package fr.atexo.annonces.boot.core.domain.e_forms_notice.services.impl;

import com.atexo.annonces.commun.mpe.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.helger.commons.state.EValidity;
import com.helger.schematron.pure.SchematronResourcePure;
import com.helger.schematron.pure.errorhandler.CollectingPSErrorHandler;
import com.helger.schematron.pure.errorhandler.IPSErrorHandler;
import com.helger.schematron.svrl.SVRLHelper;
import com.helger.schematron.svrl.SVRLMarshaller;
import com.helger.schematron.svrl.jaxb.FailedAssert;
import com.helger.schematron.svrl.jaxb.SchematronOutputType;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.model.*;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.IEFormsNoticeServices;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.NoticeFormatHandler;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.PreviousNoticeFormatHandler;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.SubmitNoticeFormatHandler;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.*;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.services.IEFormsSdkService;
import fr.atexo.annonces.boot.entity.*;
import fr.atexo.annonces.boot.repository.*;
import fr.atexo.annonces.boot.ws.eforms.EFormsWS;
import fr.atexo.annonces.boot.ws.eforms.ESentoolWS;
import fr.atexo.annonces.boot.ws.eforms.SchematronConfiguration;
import fr.atexo.annonces.boot.ws.mpe.MpeWs;
import fr.atexo.annonces.boot.ws.mpe.model.ConsultationContexte;
import fr.atexo.annonces.boot.ws.viewer.ViewerWS;
import fr.atexo.annonces.config.EFormsHelper;
import fr.atexo.annonces.config.SearchCriteria;
import fr.atexo.annonces.config.SearchOperatorEnum;
import fr.atexo.annonces.config.XmlElement;
import fr.atexo.annonces.dto.ConfigurationConsultationDTO;
import fr.atexo.annonces.dto.EformsSurchargeDTO;
import fr.atexo.annonces.dto.LangPatch;
import fr.atexo.annonces.dto.RevisionDTO;
import fr.atexo.annonces.dto.eforms.EformsSearch;
import fr.atexo.annonces.dto.eforms.EsentoolNoticeDTO;
import fr.atexo.annonces.dto.eforms.EsentoolPageDTO;
import fr.atexo.annonces.dto.eforms.Metadata;
import fr.atexo.annonces.dto.viewer.ViewerDTO;
import fr.atexo.annonces.jaxb.suivi.simap.Item;
import fr.atexo.annonces.jaxb.suivi.simap.PublicationInfo;
import fr.atexo.annonces.jaxb.suivi.simap.ReponseSimap;
import fr.atexo.annonces.mapper.EformsSurchargeMapper;
import fr.atexo.annonces.mapper.FormulaireMapper;
import fr.atexo.annonces.modeles.*;
import fr.atexo.annonces.service.AgentService;
import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import fr.atexo.annonces.service.exception.PdfExportException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.util.Base64;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.RevisionType;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletOutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static fr.atexo.annonces.config.EFormsHelper.*;
import static fr.atexo.annonces.modeles.ModeEformsEnum.ONLY_OBLIGATOIRE;
import static fr.atexo.annonces.modeles.ModeEformsEnum.ONLY_OBLIGATOIRE_AND_FILLED;
import static fr.atexo.annonces.service.SuiviAnnonceServiceImpl.IDENTIFIANT_CONSULTATION_SUPPORT_MESSAGE;

@Slf4j
@Service
public class EFormsNoticeServicesImpl extends GenericSdkService implements IEFormsNoticeServices {

    private final EntityManagerFactory entityManagerFactory;
    private final IEFormsSdkService ieFormsSdkService;
    private final EFormsFormulaireRepository eFormsFormulaireRepository;
    private final EFormsProcedureRepository eFormsProcedureRepository;
    private final PlateformeConfigurationRepository plateformeConfigurationRepository;
    private final OrganismeRepository organismeRepository;
    private final SuiviAnnonceRepository suiviAnnonceRepository;
    private final SuiviTypeAvisPubRepository suiviTypeAvisPubRepository;
    private final FormulaireMapper formulaireMapper;
    private final AgentService agentService;
    private final ViewerWS viewerWS;
    private final EFormsWS eFormsWS;
    private final MpeWs mpeWs;
    private final EformsSurchargeMapper eformsSurchargeMapper;


    public EFormsNoticeServicesImpl(SchematronConfiguration schematronConfiguration, EntityManagerFactory entityManagerFactory, IEFormsSdkService ieFormsSdkService, EformsSurchargeRepository eformsSurchargeRepository, EFormsFormulaireRepository eFormsFormulaireRepository, EFormsProcedureRepository eFormsProcedureRepository, PlateformeConfigurationRepository plateformeConfigurationRepository, OrganismeRepository organismeRepository, SuiviAnnonceRepository suiviAnnonceRepository, SuiviTypeAvisPubRepository suiviTypeAvisPubRepository, FormulaireMapper formulaireMapper, AgentService agentService, ViewerWS viewerWS, EFormsWS eFormsWS, ESentoolWS esentoolWS, MpeWs mpeWs, EformsSurchargeMapper eformsSurchargeMapper) {
        super(eformsSurchargeRepository, schematronConfiguration);
        this.entityManagerFactory = entityManagerFactory;
        this.eFormsFormulaireRepository = eFormsFormulaireRepository;
        this.eFormsProcedureRepository = eFormsProcedureRepository;
        this.plateformeConfigurationRepository = plateformeConfigurationRepository;
        this.organismeRepository = organismeRepository;
        this.suiviAnnonceRepository = suiviAnnonceRepository;
        this.suiviTypeAvisPubRepository = suiviTypeAvisPubRepository;
        this.formulaireMapper = formulaireMapper;
        this.agentService = agentService;
        this.viewerWS = viewerWS;
        this.eFormsWS = eFormsWS;
        this.ieFormsSdkService = ieFormsSdkService;
        this.mpeWs = mpeWs;
        this.eformsSurchargeMapper = eformsSurchargeMapper;
    }


    @Override
    public EFormsFormulaire getFormulaireEForms(Long id, String plateforme) {
        EFormsFormulaireEntity eFormsFormulaireEntity = getFormsFormulaire(id, plateforme);
        return formulaireMapper.mapToModel(eFormsFormulaireEntity);
    }

    @Override
    public SubNotice getSubNoticeStructure(Long id, String plateforme) {
        EFormsFormulaireEntity eFormsFormulaireEntity = getFormsFormulaire(id, plateforme);
        String idNotice = eFormsFormulaireEntity.getIdNotice();
        String versionSdk = eFormsFormulaireEntity.getVersionSdk();
        String formulaire = null;
        if (eFormsFormulaireEntity.getFormulaire() != null) {
            formulaire = new String(Base64.decodeBase64(eFormsFormulaireEntity.getFormulaire()));
        }
        return getSubNotice(plateforme, idNotice, versionSdk, formulaire);
    }

    @Override
    public SubNotice getSubNotice(String plateforme, String idNotice, String versionSdk, String formulaire) {
        SubNotice notice = getSubNotice(idNotice, versionSdk, plateforme);
        PlateformeConfiguration configuration = plateformeConfigurationRepository.findByIdPlateforme(plateforme);
        if (configuration != null) {
            ModeEformsEnum modeEforms = configuration.getModeEforms() == null ? ModeEformsEnum.ALL : configuration.getModeEforms();
            var meta = this.getListFromMode(notice.getMetadata(), modeEforms, versionSdk, idNotice, formulaire, null);
            var content = this.getListFromMode(notice.getContent(), modeEforms, versionSdk, idNotice, formulaire, null);
            notice.setContent(content);
            notice.setMetadata(meta);
        }
        return notice;
    }

    @Override
    public SubNotice getSubNotice(String idNotice, String versionSdk, String plateforme) {
        SubNotice notice = ieFormsSdkService.getNotice(idNotice, versionSdk);
        this.setSurcharge(notice.getMetadata(), idNotice, plateforme);
        this.setSurcharge(notice.getContent(), idNotice, plateforme);
        this.setIdRef(notice.getMetadata(), plateforme, versionSdk);
        this.setIdRef(notice.getContent(), plateforme, versionSdk);
        return notice;
    }

    private List<SubNoticeMetadata> getListFromMode(List<SubNoticeMetadata> metadata, ModeEformsEnum modeEforms, String versionSdk, String idNotice, String formulaire, String identifierFieldId) {
        if (metadata == null) {
            return List.of();
        }
        List<SubNoticeMetadata> result = new ArrayList<>();
        for (SubNoticeMetadata subNoticeMetadata : metadata) {
            List<SubNoticeMetadata> content = subNoticeMetadata.getContent();
            if (!CollectionUtils.isEmpty(content)) {
                String identifierFieldId1 = subNoticeMetadata.get_identifierFieldId() == null ? identifierFieldId : subNoticeMetadata.get_identifierFieldId();
                List<SubNoticeMetadata> list = getListFromMode(content, modeEforms, versionSdk, idNotice, formulaire, identifierFieldId1);
                if (!CollectionUtils.isEmpty(list)) {
                    this.getFields(versionSdk).getXmlStructure().stream().filter(element -> {
                        String id = element.getId();
                        return id != null && id.equals(subNoticeMetadata.getNodeId());
                    }).findFirst().ifPresent(xmlStructureDetails -> subNoticeMetadata.setXpathAbsolute(xmlStructureDetails.getXpathAbsolute()));
                    subNoticeMetadata.setContent(list);
                    subNoticeMetadata.setObligatoire(true);
                    subNoticeMetadata.setHidden(false);
                    subNoticeMetadata.setElements(EFormsHelper.getXpathElementList(subNoticeMetadata.getXpathAbsolute()));
                    if (CollectionUtils.isEmpty(subNoticeMetadata.getElements())) {
                        Map<String, List<XmlElement>> subElements = list.stream().filter(item -> !CollectionUtils.isEmpty(item.getElements())).collect(Collectors.toMap(SubNoticeMetadata::getId, SubNoticeMetadata::getElements));
                        list.forEach(item -> {
                            if (!CollectionUtils.isEmpty(item.getSubElements())) {
                                subElements.putAll(item.getSubElements());
                            }
                        });
                        subNoticeMetadata.setSubElements(subElements);
                    }
                    result.add(subNoticeMetadata);
                }
            } else {
                FieldDetails fieldDetails = this.getFields(versionSdk).getFields().stream().filter(element -> {
                    String id = element.getId();
                    return id != null && id.equals(subNoticeMetadata.getId());
                }).findFirst().orElseThrow(() -> new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Le champ " + subNoticeMetadata.getId() + " n'existe pas"));
                subNoticeMetadata.setXpathAbsolute(fieldDetails.getXpathAbsolute());

                if (subNoticeMetadata.getId().equals(identifierFieldId)) {
                    subNoticeMetadata.setObligatoire(true);
                    subNoticeMetadata.setHidden(false);
                    subNoticeMetadata.setReadOnly(true);
                    subNoticeMetadata.setElements(EFormsHelper.getXpathElementList(subNoticeMetadata.getXpathAbsolute()));
                    result.add(subNoticeMetadata);
                } else {
                    boolean obligatoire = estObligatoire(idNotice, subNoticeMetadata.getSurcharge(), fieldDetails.getMandatory(), ONLY_OBLIGATOIRE.equals(modeEforms) || ONLY_OBLIGATOIRE_AND_FILLED.equals(modeEforms)) && !estForbidden(idNotice, subNoticeMetadata.getSurcharge(), fieldDetails.getForbidden(), ONLY_OBLIGATOIRE.equals(modeEforms) || ONLY_OBLIGATOIRE_AND_FILLED.equals(modeEforms));
                    subNoticeMetadata.setObligatoire(obligatoire);
                    switch (modeEforms) {
                        case ONLY_OBLIGATOIRE, ONLY_OBLIGATOIRE_CONDITION -> {
                            if (obligatoire) {
                                subNoticeMetadata.setHidden(false);
                                subNoticeMetadata.setElements(EFormsHelper.getXpathElementList(subNoticeMetadata.getXpathAbsolute()));
                                result.add(subNoticeMetadata);
                            }
                        }
                        case ONLY_OBLIGATOIRE_AND_FILLED, ONLY_OBLIGATOIRE_CONDITION_AND_FILLED -> {
                            if (obligatoire) {
                                subNoticeMetadata.setHidden(false);
                                subNoticeMetadata.setElements(EFormsHelper.getXpathElementList(subNoticeMetadata.getXpathAbsolute()));
                                result.add(subNoticeMetadata);
                            } else if (formulaire != null && formulaire.contains("\"" + fieldDetails.getId() + "\"")) {
                                subNoticeMetadata.setHidden(false);
                                subNoticeMetadata.setElements(EFormsHelper.getXpathElementList(subNoticeMetadata.getXpathAbsolute()));
                                result.add(subNoticeMetadata);
                            }
                        }
                        default -> {
                            if (!subNoticeMetadata.isHidden() || obligatoire) {
                                subNoticeMetadata.setHidden(false);
                                subNoticeMetadata.setElements(EFormsHelper.getXpathElementList(subNoticeMetadata.getXpathAbsolute()));
                                result.add(subNoticeMetadata);
                            }
                        }

                    }

                }
            }
        }
        result.sort((o1, o2) -> {
            if (o1.getSurcharge() == null || o1.getSurcharge().getOrdre() == null) {
                return 1;
            }
            if (o2.getSurcharge() == null || o2.getSurcharge().getOrdre() == null) {
                return -1;
            }
            return o1.getSurcharge().getOrdre() - o2.getSurcharge().getOrdre();
        });
        return result;
    }

    public boolean estObligatoire(String idNotice, EformsSurchargeDTO surcharge, MandatoryForbiddenDetails mandatory, boolean mandatoryWithoutCondition) {
        if (surcharge != null && Boolean.TRUE.equals(surcharge.getObligatoire())) {
            return true;
        }
        if (mandatory == null) {
            return false;
        }
        List<Constraint> constraints = mandatory.getConstraints().stream().filter(item -> item.getNoticeTypes().contains(idNotice)).toList();
        if (CollectionUtils.isEmpty(constraints)) {
            return mandatory.isValue();
        }
        if (mandatoryWithoutCondition && constraints.stream().anyMatch(item -> item.isValue() && !StringUtils.hasText(item.getCondition()))) {
            return true;
        }
        if (!mandatoryWithoutCondition && constraints.stream().anyMatch(Constraint::isValue)) {
            return true;
        }
        if (mandatoryWithoutCondition && constraints.stream().anyMatch(item -> !item.isValue() && !StringUtils.hasText(item.getCondition()))) {
            return false;
        }
        return mandatory.isValue();


    }

    public boolean estForbidden(String idNotice, EformsSurchargeDTO surcharge, MandatoryForbiddenDetails forbidden, boolean forbiddenWithoutCondition) {
        if (surcharge != null && Boolean.TRUE.equals(surcharge.getObligatoire())) {
            return true;
        }
        if (forbidden == null) {
            return false;
        }
        List<Constraint> constraints = forbidden.getConstraints().stream().filter(item -> item.getNoticeTypes().contains(idNotice)).toList();
        if (CollectionUtils.isEmpty(constraints)) {
            return forbidden.isValue();
        }
        if (forbiddenWithoutCondition && constraints.stream().anyMatch(item -> item.isValue() && !StringUtils.hasText(item.getCondition()))) {
            return true;
        }
        if (!forbiddenWithoutCondition && constraints.stream().anyMatch(Constraint::isValue)) {
            return true;
        }
        if (forbiddenWithoutCondition && constraints.stream().anyMatch(item -> !item.isValue() && StringUtils.hasText(item.getCondition()))) {
            return false;
        }
        return forbidden.isValue();


    }

    @Override
    public EFormsFormulaire save(Long id, Object formulaire, String plateforme) {
        EFormsFormulaireEntity eFormsFormulaireEntity = getFormsFormulaire(id, plateforme);
        if ("esentool".equals(eFormsFormulaireEntity.getTedVersion())) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "La fonctionnalité n'est pas disponible pour esentool");
        }
        SubNotice notice = getSubNotice(eFormsFormulaireEntity.getIdNotice(), eFormsFormulaireEntity.getVersionSdk(), plateforme);
        return getEFormsFormulaire(plateforme, eFormsFormulaireEntity, eFormsFormulaireEntity, formulaire, notice);
    }

    @Override
    public EFormsFormulaire submit(Long id, String plateforme, boolean updateDateEnvoi) {
        EFormsFormulaireEntity eFormsFormulaireEntity = getFormsFormulaire(id, plateforme);
        if (!eFormsFormulaireEntity.getStatut().equals(StatutENoticeEnum.COMPLETED) && !eFormsFormulaireEntity.getStatut().equals(StatutENoticeEnum.DRAFT) && !eFormsFormulaireEntity.getStatut().equals(StatutENoticeEnum.CONCENTRATEUR_VALIDATED) && !eFormsFormulaireEntity.getStatut().equals(StatutENoticeEnum.VALIDATION_FAILED)) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Le formulaire doit être au statut DRAFT ou CONCENTRATEUR_VALIDATED ou COMPLETED");
        }
        if ("esentool".equals(eFormsFormulaireEntity.getTedVersion())) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "La fonctionnalité n'est pas disponible pour esentool");
        } else {
            if (updateDateEnvoi) {
                eFormsFormulaireEntity = this.updateNoticeDatePublication(ZonedDateTime.now(), eFormsFormulaireEntity);
            }
            var sent = submitV2(plateforme, eFormsFormulaireEntity);
            if (!sent) {
                return null;
            }
        }
        eFormsFormulaireEntity.setStatut(StatutENoticeEnum.SUBMITTING);
        eFormsFormulaireEntity.setDateModificationStatut(ZonedDateTime.now());
        eFormsFormulaireEntity.setDateEnvoiSoumission(ZonedDateTime.now());
        return formulaireMapper.mapToModel(eFormsFormulaireRepository.save(eFormsFormulaireEntity));
    }

    private boolean submitV2(String plateforme, EFormsFormulaireEntity eFormsFormulaireEntity) {
        log.info("Publication du formulaire {} sur la plateforme {}", eFormsFormulaireEntity.getId(), plateforme);
        byte[] binaryData = Base64.decodeBase64(eFormsFormulaireEntity.getXmlGenere().getBytes());

        String xmlString = new String(binaryData);
        String lang = eFormsFormulaireEntity.getLang();
        File tempFile = EFormsHelper.getStringFile(eFormsFormulaireEntity.getReferenceConsultation(), xmlString);
        try {
            var plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(plateforme);
            String eformsToken = plateformeConfiguration == null ? null : plateformeConfiguration.getEformsToken();
            var result = eFormsWS.submit(tempFile, Metadata.builder().noticeAuthorEmail(eFormsFormulaireEntity.getAuteurEmail()).noticeAuthorLocale(lang.substring(0, 2).toLowerCase()).build(), eformsToken);
            return result != null;
        } catch (Exception e) {
            log.error("Error while submitting notice to eforms : {}", xmlString, e);
            tempFile.deleteOnExit();
            throw e;
        }
    }

    @Override
    public EFormsFormulaireEntity updateNoticeDatePublication(ZonedDateTime dateEnvoi, EFormsFormulaireEntity eFormsFormulaireEntity) {
        log.info("Publication du formulaire {} sur la plateforme {}", eFormsFormulaireEntity.getId(), eFormsFormulaireEntity.getPlateforme());
        String versionSdk = eFormsFormulaireEntity.getVersionSdk();
        String idNotice = eFormsFormulaireEntity.getIdNotice();
        String plateforme = eFormsFormulaireEntity.getPlateforme();
        SubNotice notice = this.getSubNotice(idNotice, versionSdk, plateforme);
        DocumentType documentType = getSubNoticeList(versionSdk).getDocumentTypes().stream().filter(item -> item.getId().equals(notice.getDocumentType())).findFirst().orElseThrow(null);
        Document xmlDocument;
        String formulaireBase64 = eFormsFormulaireEntity.getFormulaire();
        Object formulaireObject = EFormsHelper.getObjectFromString(new String(Base64.decodeBase64(formulaireBase64)), Object.class);
        formulaireObject = buildFormulaireSubmit(notice, formulaireObject, eFormsFormulaireEntity, dateEnvoi);
        String lang = eFormsFormulaireEntity.getLang();
        xmlDocument = buildXmlDocument(formulaireObject, documentType, idNotice, lang, plateforme, versionSdk);
        String xmlString = EFormsHelper.getXmlString(xmlDocument);
        String formulaireString = getString(formulaireObject);
        eFormsFormulaireEntity.setFormulaire(Base64.encodeBase64StringUnChunked(formulaireString.getBytes()));
        eFormsFormulaireEntity.setXmlGenere(Base64.encodeBase64StringUnChunked(xmlString.getBytes()));
        return eFormsFormulaireRepository.save(eFormsFormulaireEntity);
    }

    @Override
    public EFormsFormulaire changeOfficialLang(Long id, LangPatch patch, String plateforme) {
        EFormsFormulaireEntity eFormsFormulaireEntity = getFormsFormulaire(id, plateforme);
        if ("esentool".equals(eFormsFormulaireEntity.getTedVersion())) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "La fonctionnalité n'est pas disponible pour esentool");
        }
        if (!eFormsFormulaireEntity.getStatut().equals(StatutENoticeEnum.DRAFT) && !eFormsFormulaireEntity.getStatut().equals(StatutENoticeEnum.CONCENTRATEUR_VALIDATED) && !eFormsFormulaireEntity.getStatut().equals(StatutENoticeEnum.VALIDATION_FAILED)) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Le formulaire doit être au statut DRAFT ou CONCENTRATEUR_VALIDATED");
        }
        eFormsFormulaireEntity.setLang(patch.getLang());
        return formulaireMapper.mapToModel(eFormsFormulaireRepository.save(eFormsFormulaireEntity));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public List<RevisionDTO<EFormsFormulaire>> getRevisions(Long id, boolean fetchChanges) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        var auditReader = AuditReaderFactory.get(entityManager);
        AuditQuery auditQuery;

        if (fetchChanges) {
            auditQuery = auditReader.createQuery().forRevisionsOfEntityWithChanges(EFormsFormulaireEntity.class, true);
        } else {
            auditQuery = auditReader.createQuery().forRevisionsOfEntity(EFormsFormulaireEntity.class, true);
        }
        auditQuery.add(AuditEntity.id().eq(id));
        final List<Object[]> revisions = auditQuery.getResultList();
        final List<RevisionDTO<EFormsFormulaire>> result = new ArrayList<>();
        revisions.forEach(objects -> {
            EFormsFormulaireEntity buDa = (EFormsFormulaireEntity) objects[0];
            RevisionType revType = (RevisionType) objects[2];
            HashSet<String> champs = (HashSet<String>) objects[3];
            RevisionDTO<EFormsFormulaire> buDaRevision = RevisionDTO.<EFormsFormulaire>builder().item(formulaireMapper.mapToModel(buDa)).type(revType.name()).champs(champs).build();
            result.add(buDaRevision);
        });
        entityManager.clear();
        entityManager.close();
        return result;
    }

    @Override
    public EFormsFormulaire stopPublication(Long id, String plateforme) {
        EFormsFormulaireEntity eFormsFormulaireEntity = getFormsFormulaire(id, plateforme);
        if (!eFormsFormulaireEntity.getStatut().equals(StatutENoticeEnum.PUBLISHING) && !eFormsFormulaireEntity.getStatut().equals(StatutENoticeEnum.SUBMITTED)) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Le formulaire doit être au statut SUBMITTED ou PUBLISHING");
        }
        if ("esentool".equals(eFormsFormulaireEntity.getTedVersion())) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "La fonctionnalité n'est pas disponible pour esentool");
        }
        Integer noticeVersion = eFormsFormulaireEntity.getNoticeVersion();
        String value = String.valueOf(noticeVersion);
        if (noticeVersion < 10) {
            value = "0" + value;
        }
        var plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(plateforme);
        String eformsToken = plateformeConfiguration == null ? null : plateformeConfiguration.getEformsToken();
        var result = eFormsWS.stopPublication(eFormsFormulaireEntity.getUuid(), value, eformsToken);
        log.info("Stop publication du formulaire {} sur la plateforme {}", result.getId(), plateforme);

        eFormsFormulaireEntity.setDateEnvoiAnnulation(ZonedDateTime.now());
        eFormsFormulaireEntity.setStatut(StatutENoticeEnum.STOPPING);
        eFormsFormulaireEntity.setDateModificationStatut(ZonedDateTime.now());
        return formulaireMapper.mapToModel(eFormsFormulaireRepository.save(eFormsFormulaireEntity));
    }

    @Override
    public ValidationResult getValidationReport(Long id, String plateforme) {
        EFormsFormulaireEntity eFormsFormulaireEntity = getFormsFormulaire(id, plateforme);
        if (eFormsFormulaireEntity.getStatut().equals(StatutENoticeEnum.CONCENTRATEUR_VALIDATING_SIP) || eFormsFormulaireEntity.getStatut().equals(StatutENoticeEnum.CONCENTRATEUR_VALIDATING_ECO) || eFormsFormulaireEntity.getStatut().equals(StatutENoticeEnum.DRAFT) || eFormsFormulaireEntity.getStatut().equals(StatutENoticeEnum.SUBMITTED)) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Le formulaire est un brouillon/ soumis / en attente de validation");
        }
        if ("esentool".equals(eFormsFormulaireEntity.getTedVersion())) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "La fonctionnalité n'est pas disponible pour esentool");
        }
        Integer noticeVersion = eFormsFormulaireEntity.getNoticeVersion();
        String value = String.valueOf(noticeVersion);
        if (noticeVersion < 10) {
            value = "0" + value;
        }
        var plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(plateforme);
        String eformsToken = plateformeConfiguration == null ? null : plateformeConfiguration.getEformsToken();

        File rapport = eFormsWS.getValidationReport(eFormsFormulaireEntity.getUuid(), value, eformsToken);
        SchematronOutputType reportResult = new SVRLMarshaller().read(rapport);
        String xmlString = eFormsFormulaireEntity.getXmlGenere();
        if (reportResult == null) {
            return ValidationResult.builder().valid(false).errors(Collections.singletonList(FailedAssertRepresentation.builder().text("Le rapport de validation n'est pas accessible. Veuillez contacter le support.").location("/*").build())).xml(xmlString).build();
        }
        String svrlString = new SVRLMarshaller().getAsString(reportResult);
        ValidationResult result;

        List<FailedAssertRepresentation> erros = getErrors(reportResult);
        result = ValidationResult.builder().valid(CollectionUtils.isEmpty(erros)).xml(xmlString).svrlString(svrlString).errors(erros).build();
        rapport.deleteOnExit();
        return result;
    }


    @Override
    public void setSurcharge(List<SubNoticeMetadata> metadata, String noticeId, String idPlateforme) {
        metadata.forEach(subNoticeMetadata -> {
            List<SubNoticeMetadata> content = subNoticeMetadata.getContent();
            if (!CollectionUtils.isEmpty(content)) {
                setSurcharge(content, noticeId, idPlateforme);
            }
            String id = subNoticeMetadata.getId();
            EFormsSurcharge surcharge = geteFormsSurcharge(noticeId, idPlateforme, id);
            if (surcharge != null) {
                subNoticeMetadata.setSurcharge(eformsSurchargeMapper.mapToModel(surcharge));
            }

        });
    }

    private EFormsSurcharge geteFormsSurcharge(String noticeId, String idPlateforme, String id) {
        EFormsSurcharge surcharge = null;
        try {

            if (idPlateforme != null && noticeId != null) {
                surcharge = eformsSurchargeRepository.findByCodeAndIdPlatformAndNoticeIdAndActifIsTrue(id, idPlateforme, noticeId);
            }

            if (surcharge == null && noticeId != null) {
                surcharge = eformsSurchargeRepository.findByCodeAndIdPlatformIsNullAndNoticeIdAndActifIsTrue(id, noticeId);
            }

            if (surcharge == null && idPlateforme != null) {
                surcharge = eformsSurchargeRepository.findByCodeAndIdPlatformAndNoticeIdIsNullAndActifIsTrue(id, idPlateforme);
            }
            if (surcharge == null) {
                surcharge = eformsSurchargeRepository.findByCodeAndIdPlatformIsNullAndNoticeIdIsNullAndActifIsTrue(id);
            }
        } catch (Exception e) {
            log.error("Erreur lors de la récupération de la surcharge de {} / {} / {} => {}", id, idPlateforme, noticeId, e.getMessage());
        }
        return surcharge;
    }

    @Override
    public void setIdRef(List<SubNoticeMetadata> metadata, String idPlateforme, String version) {
        metadata.forEach(subNoticeMetadata -> {
            List<SubNoticeMetadata> content = subNoticeMetadata.getContent();
            if (!CollectionUtils.isEmpty(content)) {
                setIdRef(content, idPlateforme, version);
            }
            List<String> idSchemes = subNoticeMetadata.get_idSchemes();
            if (!CollectionUtils.isEmpty(idSchemes)) {
                List<String> list = this.getFields(version).getFields().stream().filter(fieldDetails -> {
                    String id = fieldDetails.getIdScheme();
                    return id != null && idSchemes.contains(id);
                }).map(FieldDetails::getId).collect(Collectors.toList());
                subNoticeMetadata.setFieldIds(list);

            }

        });
    }

    @Override
    public EFormsFormulaire validate(Long id, String plateforme, boolean fromAnnonce) {
        EFormsFormulaireEntity eFormsFormulaireEntity = getFormsFormulaire(id, plateforme);
        if ("esentool".equals(eFormsFormulaireEntity.getTedVersion())) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "La fonctionnalité n'est pas disponible pour esentool");
        }
        String versionSdk = eFormsFormulaireEntity.getVersionSdk();
        String idNotice = eFormsFormulaireEntity.getIdNotice();
        SubNotice notice = this.getSubNotice(idNotice, versionSdk, plateforme);
        DocumentType documentType = getSubNoticeList(eFormsFormulaireEntity.getVersionSdk()).getDocumentTypes().stream().filter(item -> item.getId().equals(notice.getDocumentType())).findFirst().orElseThrow(null);
        String formulaireBase64 = eFormsFormulaireEntity.getFormulaire();
        String lang = eFormsFormulaireEntity.getLang();
        Object objet = EFormsHelper.getObjectFromString(new String(Base64.decodeBase64(formulaireBase64)), Object.class);
        Document xmlDocument = buildXmlDocument(objet, documentType, idNotice, lang, plateforme, versionSdk);
        File tempFile = EFormsHelper.getXmlFile(xmlDocument);
        String xsdPath = getEformsFolderSdk(versionSdk) + "/" + documentType.getSchemaLocation();
        String validXmlXsd = validateXMLSchema(xsdPath, tempFile.getAbsolutePath());
        String xmlString = EFormsHelper.getXmlString(xmlDocument);
        ValidationResult validationResult = getValidationResult(tempFile, xmlString, "static", versionSdk);
        tempFile.deleteOnExit();
        validationResult.setValidXml(validXmlXsd == null);
        Result result = new Result(validXmlXsd, xmlString, validationResult);

        eFormsFormulaireEntity.setXmlGenere(Base64.encodeBase64StringUnChunked(result.xmlString().getBytes()));
        boolean valid = result.validationResult().isValid() && result.validXmlXsd() == null;
        if (!valid) {
            log.debug("Le rapport SVRL est : {}", result.validationResult().getSvrlString());
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Le formulaire n'est pas valide");
        }
        eFormsFormulaireEntity.setValid(valid);
        eFormsFormulaireEntity.setStatut(StatutENoticeEnum.COMPLETED);
        eFormsFormulaireEntity.setDateModificationStatut(ZonedDateTime.now());
        SuiviAnnonce suiviAnnonce = eFormsFormulaireEntity.getSuiviAnnonce();
        if (suiviAnnonce != null && !fromAnnonce) {
            suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.COMPLET);
            suiviAnnonceRepository.save(suiviAnnonce);
            SuiviTypeAvisPub typeAvisPub = suiviAnnonce.getTypeAvisPub();
            if (typeAvisPub != null) {
                typeAvisPub.setDateValidationEuropeen(ZonedDateTime.now());
                suiviTypeAvisPubRepository.save(typeAvisPub);
            }
        }

        return formulaireMapper.mapToModel(eFormsFormulaireRepository.save(eFormsFormulaireEntity));
    }

    private record Result(String validXmlXsd, String xmlString, ValidationResult validationResult) {
    }

    private List<FailedAssertRepresentation> getErrors(SchematronOutputType reportResult) {
        List<Object> failedAsserts = reportResult.getActivePatternAndFiredRuleAndFailedAssert();

        return failedAsserts.stream().filter(FailedAssert.class::isInstance).map(o -> {
            FailedAssert failedAssert = (FailedAssert) o;
            return FailedAssertRepresentation.builder().location(failedAssert.getLocation()).text(SVRLHelper.getAsString(failedAssert.getText())).id(failedAssert.getId()).test(failedAssert.getTest()).build();

        }).collect(Collectors.toList());
    }

    @Override
    public EFormsFormulaire addFormulaire(String token, EFormsFormulaireAdd formulaire, String plateforme, Integer organismeId) {
        SuiviAnnonce suiviAnnonce = null;
        if (formulaire.getIdAnnonce() == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Le formulaire doit être lié à une annonce");
        }
        suiviAnnonce = suiviAnnonceRepository.findById(formulaire.getIdAnnonce()).orElseThrow(() -> new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "L'annonce n'existe pas"));


        String formulaireString;
        String resourcesTag = schematronConfiguration.getResourcesTag();
        SubNotice notice = this.getSubNotice(formulaire.getIdNotice(), resourcesTag, plateforme);

        FieldDetails noticeId = getFields(resourcesTag).getFields().stream().filter(fieldDetails1 -> fieldDetails1.getId().equals("BT-701-notice")).findFirst().orElse(null);
        String uuid = getId(noticeId, null);
        int i = 0;
        while (eFormsFormulaireRepository.existsByUuid(uuid)) {
            uuid = getId(noticeId, null);
            i++;
            if (i > 100) {
                log.error("Erreur lors de la génération de l'uuid");
                throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, "Erreur lors de la génération de l'uuid");
            }
        }

        ConfigurationConsultationDTO configurationConsultationDTO = agentService.getConfiguration(token, formulaire.getIdConsultation(), plateforme);
        EFormsFormulaireEntity eFormsFormulaire = geteFormsFormulaireFromMpe(formulaire, plateforme, organismeId, uuid, configurationConsultationDTO.getContexte(), suiviAnnonce);
        eFormsFormulaire.setStatut(StatutENoticeEnum.DRAFT);
        eFormsFormulaire = eFormsFormulaireRepository.save(eFormsFormulaire);


        Object formulaireObjet = buildFormulaire(notice, eFormsFormulaire, configurationConsultationDTO);
        formulaireString = getString(formulaireObjet);
        eFormsFormulaire.setFormulaire(Base64.encodeBase64StringUnChunked(formulaireString.getBytes()));

        return formulaireMapper.mapToModel(eFormsFormulaireRepository.save(eFormsFormulaire));
    }

    private EFormsFormulaireEntity geteFormsFormulaireFromMpe(EFormsFormulaireAdd formulaire, String plateforme, Integer organismeId, String uuid, ConsultationContexte consultationContexte, SuiviAnnonce suiviAnnonce) {
        EFormsFormulaireEntity eFormsFormulaire;
        ReferentielMpe procedure = consultationContexte.getTypeProcedureConsultation();
        if (procedure == null) {
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, "Procedure est null");
        }
        ConsultationMpe consultation = consultationContexte.getConsultation();
        String intitule = consultation.getIntitule();
        EFormsProcedureEntity eFormsProcedureEntity = getProcedureEntity(String.valueOf(consultation.getId()), intitule);
        eFormsFormulaire = EFormsFormulaireEntity.builder().uuid(uuid).procedure(eFormsProcedureEntity).versionSdk(getFields(schematronConfiguration.getResourcesTag()).getSdkVersion()).organisme(organismeRepository.findById(organismeId).orElse(null)).plateforme(plateforme).idNotice(formulaire.getIdNotice()).idConsultation(formulaire.getIdConsultation()).noticeVersion(1).lang(formulaire.getLang()).dateCreation(ZonedDateTime.now()).auteurEmail(formulaire.getAuteurEmail()).intituleConsultation(intitule).referenceConsultation(consultation.getReference()).suiviAnnonce(suiviAnnonce)
                .tedVersion("enotices2")
                .build();
        return eFormsFormulaire;
    }

    @Override
    public EFormsProcedureEntity getProcedureEntity(String code, String libelle) {
        EFormsProcedureEntity eFormsProcedureEntity = eFormsProcedureRepository.findByCode(code);
        if (eFormsProcedureEntity == null) {
            FieldDetails fieldDetails = getFields(schematronConfiguration.getResourcesTag()).getFields().stream().filter(fieldDetails1 -> fieldDetails1.getId().equals("BT-04-notice")).findFirst().orElse(null);
            eFormsProcedureEntity = EFormsProcedureEntity.builder().code(code).libelle(libelle).uuid(getId(fieldDetails, null)).build();
            eFormsProcedureEntity = eFormsProcedureRepository.save(eFormsProcedureEntity);
        }
        return eFormsProcedureEntity;
    }

    @Override
    public ConsultationContexte getConsultationContexte(String serveurUrl, String serveurToken, Integer idConsultation, String idPlatform, boolean europeen, String idAgentConnecteMpe) {

        ConsultationMpe consultation = mpeWs.getConsultation(idConsultation, serveurUrl, serveurToken);
        if (consultation != null) {

            NatureMpe natureMpe = mpeWs.getNaturePrestation(consultation.getNaturePrestation(), serveurUrl, serveurToken);
            DonneesComplementaires donneesComplementaires = mpeWs.getDonneComplementaire(idConsultation, serveurUrl, serveurToken);
            List<CritereAttributionMpe> critereAttributionMpe = new ArrayList<>();
            if (donneesComplementaires != null && !CollectionUtils.isEmpty(donneesComplementaires.getCriteresAttribution())) {
                critereAttributionMpe = donneesComplementaires.getCriteresAttribution().stream().map(critereId -> mpeWs.getCritereAttribution(critereId, serveurUrl, serveurToken)).collect(Collectors.toList());
            }
            AgentMpe agent = mpeWs.getAgent(consultation.getIdCreateur(), serveurUrl, serveurToken);
            ReferentielMpe typeProcedure = mpeWs.getReferentiel(consultation.getTypeProcedure(), serveurUrl, serveurToken);
            OrganismeMpe mpeOrganisme = mpeWs.getOrganisme(consultation.getOrganisme(), serveurUrl, serveurToken);
            ServiceMpe directionService = null;
            List<ServiceMpe> serviceMpes = new ArrayList<>();

            if (StringUtils.hasText(consultation.getDirectionService())) {
                directionService = mpeWs.getService(consultation.getDirectionService(), serveurUrl, serveurToken);
                String[] tab = consultation.getDirectionService().split("services/");
                if (tab.length == 2 && directionService != null) {
                    directionService.setId(Long.valueOf(tab[1]));
                    serviceMpes.add(0, directionService);
                    getServiceLibelle(serveurUrl, serveurToken, directionService.getIdParent(), serviceMpes);
                }
            }

            if (serviceMpes.size() > 3) {
                serviceMpes = serviceMpes.subList(0, 3);
            }
            ContratMpe referentiel = mpeWs.getContrat(consultation.getTypeContrat(), serveurUrl, serveurToken);
            ServiceMpe service = null;
            if (agent != null && agent.getService() != null) {
                service = mpeWs.getService(agent.getService(), serveurUrl, serveurToken);
                String[] tab = agent.getService().split("services/");
                if (tab.length == 2) {
                    service.setId(Long.valueOf(tab[1]));
                }
            }
            OrganismeMpe mpeOrganismeAgent = null;
            if (agent != null && agent.getOrganisme() != null) {
                mpeOrganismeAgent = mpeWs.getOrganisme(agent.getOrganisme(), serveurUrl, serveurToken);
            }
            ProfilJoueMpe profilJoue = null;
            if (europeen && agent != null) {
                profilJoue = mpeWs.getComptesJoues(idAgentConnecteMpe, mpeOrganisme.getAcronyme(), serveurUrl, serveurToken).stream().findFirst().orElse(null);
            }
            List<LotMpe> lots = Boolean.TRUE.equals(consultation.getAlloti()) ? mpeWs.getListeLots(idConsultation, serveurUrl, serveurToken) : List.of();
            if (!CollectionUtils.isEmpty(lots)) {
                lots.forEach(lotMpe -> lotMpe.setDonneesComplementaires(mpeWs.getDonneComplementaireLot(lotMpe.getId(), serveurUrl, serveurToken)));
            }
            return ConsultationContexte.with().consultation(consultation).organismeConsultation(mpeOrganisme).serveurUrl(serveurUrl).typeContrat(referentiel).typeProcedureConsultation(typeProcedure).donneesComplementaires(donneesComplementaires).critereAttribution(critereAttributionMpe).profilJoue(profilJoue).lots(lots).serviceConsultation(directionService).serviceAgent(service).organismeAgent(mpeOrganismeAgent).agent(agent).naturePrestation(natureMpe).services(serviceMpes).build();
        }
        log.warn("La consultation {} n'existe pas", idConsultation);
        return null;
    }

    private void getServiceLibelle(String serveurUrl, String serveurToken, String id, List<ServiceMpe> serviceMpes) {
        if (serviceMpes.size() == 100) {
            return;
        }
        if (StringUtils.hasText(id)) {
            ServiceMpe parent = mpeWs.getService(id, serveurUrl, serveurToken);
            String[] tabParent = id.split("services/");
            if (tabParent.length == 2 && parent != null) {
                parent.setId(Long.valueOf(tabParent[1]));
                serviceMpes.add(0, parent);
                getServiceLibelle(serveurUrl, serveurToken, parent.getIdParent(), serviceMpes);
            }

        }
    }


    private Object buildFormulaire(SubNotice notice, EFormsFormulaireEntity saved, ConfigurationConsultationDTO configurationConsultationDTO) {
        Object formulaire = new Object();
        SuiviAnnonce suiviAnnonce = saved.getSuiviAnnonce();
        if (suiviAnnonce == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Le formulaire n'est pas lié à une annonce");
        }
        if (suiviAnnonce.getXml() == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Le xml n'est pas renseigné");
        }
        formulaire = this.buildFormulaireFromConsultation(formulaire, notice, saved, configurationConsultationDTO);
        return formulaire;
    }


    private Object buildFormulaireFromConsultation(Object formulaireObjet, SubNotice notice, EFormsFormulaireEntity saved, ConfigurationConsultationDTO configurationConsultationDTO) {
        Map<String, List<NoticeFormatHandler>> kownHandlers = EFormsHelper.knownTags(eformsSurchargeRepository, schematronConfiguration);
        List<NoticeFormatHandler> handler = kownHandlers.get("ND-Root");
        if (handler != null) {
            for (NoticeFormatHandler noticeFormatHandler : handler) {
                SubNoticeMetadata subNoticeMetadata = SubNoticeMetadata.builder().content(notice.getMetadata()).nodeId("ND-Root").build();
                formulaireObjet = noticeFormatHandler.processGroup(notice, configurationConsultationDTO, formulaireObjet, subNoticeMetadata, saved, null, kownHandlers);
                SubNoticeMetadata subNoticeContent = SubNoticeMetadata.builder().content(notice.getContent()).nodeId("ND-Root").build();
                formulaireObjet = noticeFormatHandler.processGroup(notice, configurationConsultationDTO, formulaireObjet, subNoticeContent, saved, null, kownHandlers);

            }
        }
        return formulaireObjet;
    }


    private Object buildFormulaireReprendre(SubNotice notice, EFormsFormulaireEntity previous, EFormsFormulaireEntity newFormulaire, Object formulaireObjet) {
        Map<String, List<PreviousNoticeFormatHandler>> kownHandlers = EFormsHelper.reprendreTags(eformsSurchargeRepository, schematronConfiguration);
        List<PreviousNoticeFormatHandler> handler = kownHandlers.get("ND-Root");
        if (handler != null) {
            for (PreviousNoticeFormatHandler noticeFormatHandler : handler) {
                SubNoticeMetadata subNoticeContent = SubNoticeMetadata.builder().content(notice.getContent()).nodeId("ND-Root").build();
                formulaireObjet = noticeFormatHandler.processGroup(notice, formulaireObjet, subNoticeContent, previous, newFormulaire, null, kownHandlers, true);
                SubNoticeMetadata subNoticeContentMetaData = SubNoticeMetadata.builder().content(notice.getMetadata()).nodeId("ND-Root").build();
                formulaireObjet = noticeFormatHandler.processGroup(notice, formulaireObjet, subNoticeContentMetaData, previous, newFormulaire, null, kownHandlers, true);

            }
        }
        return formulaireObjet;
    }

    private Object buildFormulaireChange(SubNotice notice, EFormsFormulaireEntity previous, EFormsFormulaireEntity newFormulaire, Object formulaireObjet) {
        Map<String, List<PreviousNoticeFormatHandler>> kownHandlers = EFormsHelper.changeTags(eformsSurchargeRepository, schematronConfiguration);
        List<PreviousNoticeFormatHandler> handler = kownHandlers.get("ND-Root");
        if (handler != null) {
            for (PreviousNoticeFormatHandler noticeFormatHandler : handler) {
                SubNoticeMetadata subNoticeContent = SubNoticeMetadata.builder().content(notice.getContent()).nodeId("ND-Root").build();
                formulaireObjet = noticeFormatHandler.processGroup(notice, formulaireObjet, subNoticeContent, previous, newFormulaire, null, kownHandlers, true);
                SubNoticeMetadata subNoticeContentMetaData = SubNoticeMetadata.builder().content(notice.getMetadata()).nodeId("ND-Root").build();
                formulaireObjet = noticeFormatHandler.processGroup(notice, formulaireObjet, subNoticeContentMetaData, previous, newFormulaire, null, kownHandlers, true);

            }
        }
        return formulaireObjet;
    }

    private Object buildFormulaireSubmit(SubNotice notice, Object formulaireObjet, EFormsFormulaireEntity saved, ZonedDateTime dateEnvoi) {
        Map<String, List<SubmitNoticeFormatHandler>> kownHandlers = EFormsHelper.submitTags(eformsSurchargeRepository, schematronConfiguration);
        List<SubmitNoticeFormatHandler> handler = kownHandlers.get("ND-Root");
        if (handler != null) {
            for (SubmitNoticeFormatHandler noticeFormatHandler : handler) {
                SubNoticeMetadata subNoticeMetadata = SubNoticeMetadata.builder().content(notice.getMetadata()).nodeId("ND-Root").build();
                formulaireObjet = noticeFormatHandler.processGroup(formulaireObjet, subNoticeMetadata, saved, null, kownHandlers, dateEnvoi);
                SubNoticeMetadata subNoticeContent = SubNoticeMetadata.builder().content(notice.getContent()).nodeId("ND-Root").build();
                formulaireObjet = noticeFormatHandler.processGroup(formulaireObjet, subNoticeContent, saved, null, kownHandlers, dateEnvoi);

            }
        }
        return formulaireObjet;
    }


    @Override
    public List<EFormsFormulaire> getFormulaires(EFormsFormulaireSearch formulaire, String plateforme, String organisme) {
        List<EFormsFormulaireEntity> list = eFormsFormulaireRepository.findAllByCriteria(getCriteria(formulaire, plateforme, organisme));
        return formulaireMapper.mapToModels(list);
    }

    private List<SearchCriteria> getCriteria(EFormsFormulaireSearch search, String plateforme, String organisme) {
        List<SearchCriteria> searchCriteria = getCriteriaFromId(search, plateforme);
        if (Boolean.TRUE.equals(search.getOnlyOrganisme())) {
            searchCriteria.add(SearchCriteria.builder().key("organisme").operation(SearchOperatorEnum.EQUAL).value(organisme).build());
        }
        if (!CollectionUtils.isEmpty(search.getStatuses())) {
            searchCriteria.add(SearchCriteria.builder().key("statuses").operation(SearchOperatorEnum.IN).value(search.getStatuses()).build());
        }
        return searchCriteria;
    }

    @Override
    public ValidationResult verify(Long id, Object objet, String plateforme, String mode) {
        EFormsFormulaireEntity eFormsFormulaireEntity = getFormsFormulaire(id, plateforme);
        if ("esentool".equals(eFormsFormulaireEntity.getTedVersion())) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "La fonctionnalité n'est pas disponible pour esentool");
        }
        String versionSdk = eFormsFormulaireEntity.getVersionSdk();
        String idNotice = eFormsFormulaireEntity.getIdNotice();
        SubNotice notice = this.getSubNotice(idNotice, versionSdk, plateforme);
        DocumentType documentType = getSubNoticeList(versionSdk).getDocumentTypes().stream().filter(item -> item.getId().equals(notice.getDocumentType())).findFirst().orElseThrow(null);
        String lang = eFormsFormulaireEntity.getLang();
        Document xmlDocument = buildXmlDocument(objet, documentType, idNotice, lang, plateforme, versionSdk);
        File tempFile = EFormsHelper.getXmlFile(xmlDocument);
        String xsdPath = getEformsFolderSdk(versionSdk) + "/" + documentType.getSchemaLocation();
        String validXmlXsd = validateXMLSchema(xsdPath, tempFile.getAbsolutePath());
        String xmlString = EFormsHelper.getXmlString(xmlDocument);
        ValidationResult validationResult = getValidationResult(tempFile, xmlString, mode, versionSdk);
        validationResult.setValidXml(validXmlXsd == null);
        if (validationResult.getErrors() == null && validXmlXsd != null) {
            validationResult.setErrors(List.of(FailedAssertRepresentation.builder().text(validXmlXsd).build()));
        }
        validationResult.getErrors().forEach(failedAssertRepresentation -> {
            String xPath = "";
            String location = failedAssertRepresentation.getLocation();
            if (StringUtils.hasText(location)) {
                xPath = location;
            }
            String test = extractXPathFromTest(failedAssertRepresentation.getTest());
            if (StringUtils.hasText(test)) {
                xPath = xPath + "/" + test;
            }
            failedAssertRepresentation.setElements(EFormsHelper.getXpathElementList(xPath));
        });

        tempFile.deleteOnExit();

        if (validationResult.getSvrlString() != null && !validationResult.getSvrlString().isEmpty()) {
            eFormsFormulaireEntity.setSvrlString(Base64.encodeBase64StringUnChunked(validationResult.getSvrlString().getBytes()));
            log.info("Sauvegarde du svrl dans la bdd : " + validationResult.getSvrlString());
            eFormsFormulaireRepository.save(eFormsFormulaireEntity);
        }
        return validationResult;

    }


    public Document buildXmlDocumentV2(Object objet, DocumentType documentType, String lang, String plateforme, String version) {
        Document xmlDocument;
        try {
            xmlDocument = getXmlDocument(documentType, version);
            JsonNode formulaireNode = EFormsHelper.valueToTree(objet);
            Iterator<Map.Entry<String, JsonNode>> iterator = formulaireNode.fields();
            Element parentNode = xmlDocument.getDocumentElement();
            while (iterator.hasNext()) {
                Map.Entry<String, JsonNode> next = iterator.next();
                addXPathsV2(next, xmlDocument, parentNode, lang, plateforme, version, "ND-Root");
            }
        } catch (Exception e) {
            throw new AtexoException(e);
        }
        Document document = correctSequence(xmlDocument, getPackage(documentType));
        return document == null ? xmlDocument : document;
    }


    public Document buildXmlDocument(Object objet, DocumentType documentType, String idNotice, String lang, String plateforme, String version) {
        Document xmlDocument;
        try {
            xmlDocument = getXmlDocument(documentType, version);
            JsonNode formulaireNode = EFormsHelper.valueToTree(objet);
            Iterator<Map.Entry<String, JsonNode>> iterator = formulaireNode.fields();
            Element parentNode = xmlDocument.getDocumentElement();
            while (iterator.hasNext()) {
                Map.Entry<String, JsonNode> next = iterator.next();
                addXPaths(next, xmlDocument, parentNode, idNotice, lang, plateforme, version);
            }
        } catch (Exception e) {
            log.error("Erreur lors de la génération du xml", e);
            throw new AtexoException(e);
        }
        Document document = correctSequence(xmlDocument, getPackage(documentType));
        return document == null ? xmlDocument : document;
    }

    private static String getPackage(DocumentType documentType) {

        return documentType.getNamespace().replace("urn:", "").replaceAll(":", ".").replaceAll("-", "_").toLowerCase();
    }

    private ValidationResult getValidationResult(File xml, String xmlString, String mode, String versionSdk) {
        try {
            ValidationResult result;
            SchematronOutputType reportResult = this.validateStaticSchematron(xml, mode, versionSdk);
            String svrlString = null;
            if (reportResult != null) svrlString = new SVRLMarshaller().getAsString(reportResult);
            if (reportResult == null) {
                result = ValidationResult.builder().valid(false).errors(Collections.singletonList(FailedAssertRepresentation.builder().text("Le rapport de validation n'est pas accessible. Veuillez contacter le support.").location("/*").build())).xml(xmlString).build();
            } else {
                List<FailedAssertRepresentation> erros = getErrors(reportResult);
                result = ValidationResult.builder().valid(CollectionUtils.isEmpty(erros)).xml(xmlString).svrlString(svrlString).errors(erros).build();
            }
            deleteFile(xml);
            return result;
        } catch (Exception e) {
            deleteFile(xml);
            throw new AtexoException(400, "Erreur lors de la validation", e);
        }
    }


    private void addXPaths(Map.Entry<String, JsonNode> field, Document xmlDocument, Element parentNode, String idNotice, String lang, String plateforme, String version) {
        Map.Entry<String, JsonNode> current;
        Iterator<Map.Entry<String, JsonNode>> iterator = field.getValue().fields();
        String key = field.getKey();
        FieldDetails fieldDetails = getFields(version).getFields().stream().filter(fieldDetails1 -> fieldDetails1.getId().equals(key)).findFirst().orElse(null);

        if (fieldDetails == null) {
            XmlStructureDetails xmlStructureDetails = getXmlStructure(key, version);

            if (xmlStructureDetails != null && xmlStructureDetails.isRepeatable() && field.getValue().isArray()) {
                ArrayNode arrayNode = (ArrayNode) field.getValue();
                addGroupList(xmlDocument, parentNode, idNotice, lang, field, xmlStructureDetails, arrayNode, plateforme, version);
            } else if (iterator.hasNext()) {
                if (xmlStructureDetails != null) {
                    String xpath = xmlStructureDetails.getXpathRelative();
                    Element node = EFormsHelper.addGroupXPathToXml(xpath, xmlStructureDetails.isRepeatable(), true, null, xmlDocument, parentNode);
                    while (iterator.hasNext()) {
                        current = iterator.next();
                        addXPaths(current, xmlDocument, node, idNotice, lang, plateforme, version);
                    }
                    List<Object> xmlStaticValue = getXmlStaticValue(xmlStructureDetails.getId(), idNotice, version);
                    if (!CollectionUtils.isEmpty(xmlStaticValue)) {
                        log.info("Ajout des valeurs statics pour le champs {}", xmlStructureDetails.getId());
                        for (Object objet : xmlStaticValue) {
                            JsonNode jsonNode = EFormsHelper.valueToTree(objet);
                            Iterator<Map.Entry<String, JsonNode>> iteratorStatic = jsonNode.fields();
                            while (iteratorStatic.hasNext()) {
                                current = iteratorStatic.next();
                                addXPaths(current, xmlDocument, node, idNotice, lang, plateforme, version);
                            }
                        }

                    }
                } else {
                    while (iterator.hasNext()) {
                        current = iterator.next();
                        addXPaths(current, xmlDocument, parentNode, idNotice, lang, plateforme, version);
                    }
                }
            }


        } else {
            Iterator<Map.Entry<String, JsonNode>> iteratorField = field.getValue().fields();


            String xPath = fieldDetails.getXpathRelative();
            if (iteratorField.hasNext() && !field.getValue().isArray()) {
                Element fieldAdded = null;
                String value = EFormsHelper.getObjectFromString(String.valueOf(field.getValue().get("value")), String.class);
                if (!StringUtils.hasText(value)) {
                    return;
                }
                String type = EFormsHelper.getObjectFromString(String.valueOf(field.getValue().get("type")), String.class);
                switch (fieldDetails.getType()) {
                    case "amount":
                        fieldAdded = EFormsHelper.addXPathToXml(xPath, false, false, new BigDecimal(value).toString(), xmlDocument, parentNode);
                        fieldAdded.setAttribute("currencyID", type);
                        break;
                    case "measure":
                        fieldAdded = EFormsHelper.addXPathToXml(xPath, false, false, new BigDecimal(value).toString(), xmlDocument, parentNode);
                        fieldAdded.setAttribute("unitCode", type);
                        break;
                    case "time":
                    case "zoned-time":
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ssXXXXX");
                        ZonedDateTime dateTime = ZonedDateTime.of(LocalDate.now().atTime(LocalTime.parse(value)), ZoneId.of(type));
                        if (dateTime.isBefore(ZonedDateTime.of(1997, 1, 1, 0, 0, 0, 0, ZoneId.of(type)))) {
                            break;
                        }
                        dateTime = dateTime.withZoneSameInstant(ZoneId.of(type));
                        String format = dateTime.format(formatter);
                        fieldAdded = EFormsHelper.addXPathToXml(xPath, false, false, format, xmlDocument, parentNode);
                        break;
                    case "date":
                    case "date-time":
                        formatter = DateTimeFormatter.ofPattern("yyyy-MM-ddXXXXX");
                        dateTime = ZonedDateTime.of(getLocalDate(value).atTime(0, 0, 0), ZoneId.of(type));
                        dateTime = dateTime.withZoneSameInstant(ZoneId.of(type));
                        if (dateTime.isBefore(ZonedDateTime.of(1997, 1, 1, 0, 0, 0, 0, ZoneId.of(type)))) {
                            break;
                        }
                        format = dateTime.format(formatter);
                        fieldAdded = EFormsHelper.addXPathToXml(xPath, false, false, format, xmlDocument, parentNode);
                        break;
                    default:
                        log.warn("Aucune description pour le champs {} de type {}", fieldDetails.getId(), fieldDetails.getType());
                        fieldAdded = EFormsHelper.addXPathToXml(xPath, false, false, value, xmlDocument, parentNode);
                }
                setElement(lang, fieldDetails, xPath, fieldAdded, plateforme, version);
            } else if (field.getValue().isArray()) {
                ArrayNode arrayNode = (ArrayNode) field.getValue();
                for (int i = 0; i < arrayNode.size(); i++) {
                    JsonNode node = arrayNode.get(i);
                    String nodeValue = EFormsHelper.getObjectFromString(String.valueOf(node), String.class);
                    if (StringUtils.hasText(nodeValue)) {
                        Element fieldAdded = EFormsHelper.addXPathListToXml(xPath, xmlDocument, parentNode, nodeValue);
                        setElement(lang, fieldDetails, xPath, fieldAdded, plateforme, version);
                    } else {
                        log.debug("Champs vide pour for {}", key);
                    }
                }
            } else {
                Element fieldAdded;
                String nodeValue = EFormsHelper.getObjectFromString(String.valueOf(field.getValue()), String.class);

                if (StringUtils.hasText(nodeValue)) {
                    if (field.getValue().isBoolean()) {
                        fieldAdded = EFormsHelper.addXPathToXml(xPath, false, false, String.valueOf(field.getValue().booleanValue()), xmlDocument, parentNode);
                    } else {
                        if ("code".equals(fieldDetails.getType()) && !xPath.endsWith("/@listName"))
                            fieldAdded = EFormsHelper.addRootXPathListToXml(xPath, xmlDocument, parentNode, nodeValue);
                        else {
                            fieldAdded = EFormsHelper.addXPathToXml(xPath, false, false, nodeValue, xmlDocument, parentNode);
                        }
                    }
                    setElement(lang, fieldDetails, xPath, fieldAdded, plateforme, version);
                } else {
                    log.debug("Champs vide pour for {}", key);
                }
            }
        }
    }

    private static LocalDate getLocalDate(String value) {
        if (value.contains("-"))
            return LocalDate.parse(value);
        else
            return LocalDate.parse(value, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }

    private void addXPathsV2(Map.Entry<String, JsonNode> field, Document xmlDocument, Element parentNode, String lang, String plateforme, String version, String nodeStop) {
        Map.Entry<String, JsonNode> current;
        Iterator<Map.Entry<String, JsonNode>> iterator = field.getValue().fields();
        String key = field.getKey();
        FieldDetails fieldDetails = getFields(version).getFields().stream().filter(fieldDetails1 -> fieldDetails1.getId().equals(key)).findFirst().orElse(null);

        if (fieldDetails == null) {
            XmlStructureDetails xmlStructureDetails = getXmlStructure(key, version);
            if (xmlStructureDetails != null) {
                List<XmlStructureDetails> nodeIds = getNodeIds(xmlStructureDetails.getParentId(), version);
                parentNode = EFormsHelper.buildGroupXmlFromXPath(nodeIds, nodeStop, parentNode);
            }
            if (xmlStructureDetails != null && xmlStructureDetails.isRepeatable() && field.getValue().isArray()) {
                ArrayNode arrayNode = (ArrayNode) field.getValue();
                addGroupListV2(xmlDocument, parentNode, lang, field, xmlStructureDetails, arrayNode, plateforme, version);

            } else if (iterator.hasNext()) {

                while (iterator.hasNext()) {
                    current = iterator.next();
                    if (xmlStructureDetails != null) {
                        Element node = EFormsHelper.addXPathToXml(xmlStructureDetails.getXpathRelative(), false, true, null, xmlDocument, parentNode);
                        addXPathsV2(current, xmlDocument, node, lang, plateforme, version, xmlStructureDetails.getId());
                    } else {
                        addXPathsV2(current, xmlDocument, parentNode, lang, plateforme, version, nodeStop);
                    }
                }
            }
        } else {
            addFieldWithXpath(field, xmlDocument, parentNode, lang, plateforme, version, nodeStop, key, fieldDetails);
        }
    }

    private void addFieldWithXpath(Map.Entry<String, JsonNode> field, Document xmlDocument, Element parentNode, String lang, String plateforme, String version, String nodeStop, String key, FieldDetails fieldDetails) {
        List<XmlStructureDetails> nodeIds = getNodeIds(fieldDetails.getParentNodeId(), version);
        parentNode = EFormsHelper.buildGroupXmlFromXPath(nodeIds, nodeStop, parentNode);

        JsonNode fieldValue = field.getValue();
        if (fieldValue == null) return;
        Iterator<Map.Entry<String, JsonNode>> iteratorField = fieldValue.fields();
        String xPath = fieldDetails.getXpathRelative();
        if (iteratorField.hasNext() && !fieldValue.isArray()) {
            Element fieldAdded = null;
            String value = EFormsHelper.getObjectFromString(String.valueOf(fieldValue.get("value")), String.class);
            String type = EFormsHelper.getObjectFromString(String.valueOf(fieldValue.get("type")), String.class);
            switch (fieldDetails.getType()) {
                case "amount":
                    fieldAdded = EFormsHelper.addXPathToXml(xPath, false, false, new BigDecimal(value).toString(), xmlDocument, parentNode);
                    fieldAdded.setAttribute("currencyID", type);
                    break;
                case "measure":
                    fieldAdded = EFormsHelper.addXPathToXml(xPath, false, false, new BigDecimal(value).toString(), xmlDocument, parentNode);
                    fieldAdded.setAttribute("unitCode", type);
                    break;
                case "time":
                case "zoned-time":
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ssXXXXX");
                    ZonedDateTime dateTime = ZonedDateTime.of(LocalDate.now().atTime(LocalTime.parse(value)), ZoneId.of(type));
                    if (dateTime.isBefore(ZonedDateTime.of(1997, 1, 1, 0, 0, 0, 0, ZoneId.of(type)))) {
                        break;
                    }
                    dateTime = dateTime.withZoneSameInstant(ZoneId.of(type));
                    String format = dateTime.format(formatter);
                    fieldAdded = EFormsHelper.addXPathToXml(xPath, false, false, format, xmlDocument, parentNode);
                    break;
                case "date":
                case "date-time":
                    formatter = DateTimeFormatter.ofPattern("yyyy-MM-ddXXXXX");
                    dateTime = ZonedDateTime.of(getLocalDate(value).atTime(0, 0, 0), ZoneId.of(type));
                    dateTime = dateTime.withZoneSameInstant(ZoneId.of(type));
                    if (dateTime.isBefore(ZonedDateTime.of(1997, 1, 1, 0, 0, 0, 0, ZoneId.of(type)))) {
                        break;
                    }
                    format = dateTime.format(formatter);
                    fieldAdded = EFormsHelper.addXPathToXml(xPath, false, false, format, xmlDocument, parentNode);
                    break;
                default:
                    log.warn("Aucune description pour le champs {} de type {}", fieldDetails.getId(), fieldDetails.getType());
                    fieldAdded = EFormsHelper.addXPathToXml(xPath, false, false, value, xmlDocument, parentNode);
            }
            setElement(lang, fieldDetails, xPath, fieldAdded, plateforme, version);
        } else if (fieldValue.isArray()) {
            ArrayNode arrayNode = (ArrayNode) fieldValue;
            for (int i = 0; i < arrayNode.size(); i++) {
                JsonNode node = arrayNode.get(i);
                String nodeValue = EFormsHelper.getObjectFromString(String.valueOf(node), String.class);
                if (StringUtils.hasText(nodeValue)) {
                    Element fieldAdded = EFormsHelper.addXPathValueListToXml(xPath, xmlDocument, parentNode, nodeValue);
                    setElement(lang, fieldDetails, xPath, fieldAdded, plateforme, version);
                } else {
                    log.debug("Champs vide pour for {}", key);
                }
            }
        } else {
            Element fieldAdded;
            String nodeValue = EFormsHelper.getObjectFromString(String.valueOf(fieldValue), String.class);

            if (StringUtils.hasText(nodeValue)) {
                if (fieldValue.isBoolean()) {
                    fieldAdded = EFormsHelper.addXPathToXml(xPath, false, false, String.valueOf(fieldValue.booleanValue()), xmlDocument, parentNode);
                } else {
                    if ("code".equals(fieldDetails.getType()) && !xPath.endsWith("/@listName"))
                        fieldAdded = EFormsHelper.addRootXPathListToXml(xPath, xmlDocument, parentNode, nodeValue);
                    else {
                        fieldAdded = EFormsHelper.addXPathToXml(xPath, false, false, fieldValue.asText(), xmlDocument, parentNode);
                    }
                }
                setElement(lang, fieldDetails, xPath, fieldAdded, plateforme, version);
            } else {
                log.debug("Champs vide pour for {}", key);
            }
        }
    }

    private void addGroupList(Document xmlDocument, Element parentNode, String idNotice, String lang, Map.Entry<String, JsonNode> current, XmlStructureDetails xmlStructureDetails, ArrayNode arrayNode, String plateforme, String version) {
        for (int i = 0; i < arrayNode.size(); i++) {
            Element iterationItem = EFormsHelper.addXPathListToXml(xmlStructureDetails.getXpathRelative(), xmlDocument, parentNode, null);
            JsonNode node = arrayNode.get(i);
            List<Object> xmlStaticValue = getXmlStaticValue(xmlStructureDetails.getId(), idNotice, version);
            if (!CollectionUtils.isEmpty(xmlStaticValue)) {
                log.info("Ajout des valeurs statics pour le champs {}", xmlStructureDetails.getId());
                xmlStaticValue.forEach(objet -> {
                    JsonNode jsonNode = EFormsHelper.valueToTree(objet);
                    current.setValue(jsonNode);
                    addXPaths(current, xmlDocument, iterationItem, idNotice, lang, plateforme, version);
                });
            }
            current.setValue(node);
            addXPaths(current, xmlDocument, iterationItem, idNotice, lang, plateforme, version);
        }
    }

    private void addGroupListV2(Document xmlDocument, Element parentNode, String lang, Map.Entry<String, JsonNode> current, XmlStructureDetails xmlStructureDetails, ArrayNode arrayNode, String plateforme, String version) {
        for (int i = 0; i < arrayNode.size(); i++) {
            Element iterationItem = EFormsHelper.addXPathListToXml(xmlStructureDetails.getXpathRelative(), xmlDocument, parentNode, null);
            JsonNode node = arrayNode.get(i);
            current.setValue(node);
            addXPathsV2(current, xmlDocument, iterationItem, lang, plateforme, version, xmlStructureDetails.getId());
        }
    }

    private void setElement(String lang, FieldDetails fieldDetails, String xPath, Element fieldAdded, String plateforme, String version) {
        if (fieldAdded == null) {
            return;
        }
        String match = EFormsHelper.getMatch(xPath, "@listName='([A-Za-z0-9].*)'");
        if (match != null) {
            fieldAdded.setAttribute("listName", match);
        }
        if (!StringUtils.hasText(fieldAdded.getAttribute("listName")) && fieldDetails.getCodeList() != null && fieldDetails.getCodeList().getValue() != null) {
            String id = fieldDetails.getCodeList().getValue().getId();
            String parentCode = this.ieFormsSdkService.getParentCode(id, plateforme, version);
            fieldAdded.setAttribute("listName", parentCode == null ? id : parentCode);

        }
        if ("text-multilingual".equals(fieldDetails.getType())) {
            fieldAdded.setAttribute("languageID", lang);
        }
        if (fieldDetails.getIdScheme() != null) {
            fieldAdded.setAttribute("schemeName", getIdScheme(fieldDetails));
        }
    }

    private static String getIdScheme(FieldDetails fieldDetails) {
        switch (fieldDetails.getIdScheme()) {
            case "LOT":
                return "Lot";
            case "GLO":
                return "LotsGroup";
            case "PAR":
                return "Part";
            case "ORG":
                return "organization";
            case "TPO":
                return "touchpoint";
            case "UBO":
                return "ubo";
            default:
                return fieldDetails.getIdScheme();
        }
    }

    private Document getXmlDocument(DocumentType documentType, String version) throws ParserConfigurationException {

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        Document xmlDocument = builder.newDocument();
        Element parentNode = xmlDocument.createElement(documentType.getRootElement());
        parentNode.setAttribute("xmlns", documentType.getNamespace());
        if (documentType.getAdditionalNamespaces() != null)
            documentType.getAdditionalNamespaces().forEach(namespaces1 -> parentNode.setAttribute("xmlns:" + namespaces1.getPrefix(), namespaces1.getUri()));
        else {
            Map<String, String> namespaces = this.getNamespame(version);
            namespaces.forEach((dsPrefix, namespace) -> parentNode.setAttribute("xmlns:" + dsPrefix, namespace));
        }
        xmlDocument.appendChild(parentNode);
        return xmlDocument;
    }

    private void deleteFile(File xml) {
        if (xml.exists()) {
            boolean deleted = xml.delete();
            if (!deleted) {
                log.warn("Fichier temporaire supprimé");
            } else {
                log.info("Fichier temporaire supprimé");
            }
        }
    }

    private SchematronResourcePure loadStaticValidator(String mode, String versionSdk) {
        File file = new File(getEformsFolderSdk(versionSdk) + "/schematrons/" + mode + "/complete-validation.sch");
        SchematronResourcePure schematronResourcePure = SchematronResourcePure.fromFile(file);
        IPSErrorHandler errorHandler = new CollectingPSErrorHandler();
        schematronResourcePure.setErrorHandler(errorHandler);
        return schematronResourcePure;
    }

    public List<SearchCriteria> getCriteriaFromId(EFormsFormulaireId search, String plateforme) {
        List<SearchCriteria> searchCriteria = new ArrayList<>();
        if (plateforme != null) {
            searchCriteria.add(SearchCriteria.builder().key("plateforme").operation(SearchOperatorEnum.EQUAL).value(plateforme).build());
        }
        if (search.getIdNotice() != null)
            searchCriteria.add(SearchCriteria.builder().key("idNotice").operation(SearchOperatorEnum.EQUAL).value(search.getIdNotice()).build());
        if (search.getLang() != null)
            searchCriteria.add(SearchCriteria.builder().key("lang").operation(SearchOperatorEnum.EQUAL).value(search.getLang()).build());
        if (search.getIdConsultation() != null) {
            searchCriteria.add(SearchCriteria.builder().key("idConsultation").operation(SearchOperatorEnum.EQUAL).value(search.getIdConsultation()).build());
        }
        return searchCriteria;
    }

    private SchematronOutputType validateStaticSchematron(@Nonnull final File xmlFile, String mode, String versionSdk) {


        final Source streamSource = new StreamSource(xmlFile);
        SchematronResourcePure staticValidator = this.loadStaticValidator(mode, versionSdk);

        return getSchematronOutputType(xmlFile, streamSource, staticValidator);
    }


    private SchematronOutputType getSchematronOutputType(@Nonnull File xmlFile, Source streamSource, SchematronResourcePure dynamicValidator) {
        final EValidity schematronValidity;
        try {
            schematronValidity = dynamicValidator.getSchematronValidity(streamSource);
            if (schematronValidity.isInvalid()) {
                log.info("Validation {}", xmlFile.getPath());
            }
            return dynamicValidator.applySchematronValidationToSVRL(streamSource);
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return null;
    }


    @Override
    public EFormsFormulaire change(Long id, String plateforme, String token) {
        EFormsFormulaireEntity eFormsFormulaireEntity = getFormsFormulaire(id, plateforme);

        SuiviAnnonce suiviAnnonce = eFormsFormulaireEntity.getSuiviAnnonce();
        return changeEFormsFormulaire(eFormsFormulaireEntity, suiviAnnonce, token);
    }

    @Override
    public EFormsFormulaire changeEFormsFormulaire(EFormsFormulaireEntity eFormsFormulaireEntity, SuiviAnnonce suiviAnnonce, String token) {
        if (!eFormsFormulaireEntity.getStatut().equals(StatutENoticeEnum.PUBLISHED)) {
            throw new AtexoException(ExceptionEnum.FORBIDDEN, "Le formulaire doit être au statut PUBLISHED");
        }
        String versionSdk = eFormsFormulaireEntity.getVersionSdk();
        if ("esentool".equals(eFormsFormulaireEntity.getTedVersion())) {
            versionSdk = schematronConfiguration.getResourcesTag();
        }
        FieldDetails noticeId = getFields(versionSdk).getFields().stream().filter(fieldDetails1 -> fieldDetails1.getId().equals("BT-701-notice")).findFirst().orElse(null);
        String uuid = getId(noticeId, null);
        String plateforme = eFormsFormulaireEntity.getPlateforme();
        String idNotice = eFormsFormulaireEntity.getIdNotice();
        Agent agent = agentService.findByToken(token);
        EFormsFormulaireEntity newFormulaire = EFormsFormulaireEntity.builder().suiviAnnonce(suiviAnnonce).idNotice(idNotice).versionSdk(versionSdk).organisme(eFormsFormulaireEntity.getOrganisme()).plateforme(plateforme).uuid(uuid).noticeVersion(1).idConsultation(eFormsFormulaireEntity.getIdConsultation()).intituleConsultation(eFormsFormulaireEntity.getIntituleConsultation()).referenceConsultation(eFormsFormulaireEntity.getReferenceConsultation()).previous(eFormsFormulaireEntity).auteurEmail(agent.getEmail()).lang(eFormsFormulaireEntity.getLang()).valid(false).statut(StatutENoticeEnum.DRAFT).procedure(eFormsFormulaireEntity.getProcedure()).creerPar(agent).modifierPar(agent).tedVersion("enotices2").dateCreation(ZonedDateTime.now()).dateModification(ZonedDateTime.now()).build();
        Object formulaire;
        SubNotice notice = this.getSubNotice(idNotice, versionSdk, plateforme);
        String formulaireBase64 = eFormsFormulaireEntity.getFormulaire();
        if (formulaireBase64 == null) {
            ConfigurationConsultationDTO configurationConsultationDTO = agentService.getConfiguration(token, eFormsFormulaireEntity.getIdConsultation(), plateforme);
            EFormsProcedureEntity eFormsProcedureEntity = getProcedureEntity(String.valueOf(eFormsFormulaireEntity.getIdConsultation()), eFormsFormulaireEntity.getIntituleConsultation());
            newFormulaire.setProcedure(eFormsProcedureEntity);
            formulaire = buildFormulaire(notice, newFormulaire, configurationConsultationDTO);
        } else {
            formulaire = getObjectFromString(new String(Base64.decodeBase64(formulaireBase64.getBytes())), Object.class);
        }
        formulaire = buildFormulaireChange(notice, eFormsFormulaireEntity, newFormulaire, formulaire);
        String formulaireString = getString(formulaire);
        newFormulaire.setFormulaire(Base64.encodeBase64StringUnChunked(formulaireString.getBytes()));
        return formulaireMapper.mapToModel(eFormsFormulaireRepository.save(newFormulaire));
    }

    @Override
    public void exportToPdf(Long id, ServletOutputStream exportPdf, String idPlatform) {

        EFormsFormulaireEntity formulaire = getFormsFormulaire(id, idPlatform);
        if ("esentool".equals(formulaire.getTedVersion())) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "La fonctionnalité n'est pas disponible pour esentool");
        }
        try {
            var plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(idPlatform);
            String eformsToken = plateformeConfiguration == null ? null : plateformeConfiguration.getEformsToken();
            String formulaireBase64 = formulaire.getFormulaire();
            String lang = formulaire.getLang();
            String versionSdk = formulaire.getVersionSdk();
            String idNotice = formulaire.getIdNotice();
            SubNotice notice = this.getSubNotice(idNotice, versionSdk, idPlatform);
            DocumentType documentType = getSubNoticeList(versionSdk).getDocumentTypes().stream().filter(item -> item.getId().equals(notice.getDocumentType())).findFirst().orElseThrow(null);

            Object objet = EFormsHelper.getObjectFromString(new String(Base64.decodeBase64(formulaireBase64)), Object.class);
            Document xmlDocument = buildXmlDocument(objet, documentType, idNotice, lang, idPlatform, versionSdk);
            String xmlString = EFormsHelper.getXmlString(xmlDocument);
            viewerWS.reportPDF(xmlString.getBytes(), ViewerDTO.builder().summary(formulaire.getSuiviAnnonce().isSimplifie()).language(formulaire.getLang().substring(0, 2).toLowerCase()).build(), eformsToken).getInputStream().transferTo(exportPdf);
        } catch (IOException e) {
            throw new PdfExportException(e, IDENTIFIANT_CONSULTATION_SUPPORT_MESSAGE, idPlatform + "/" + formulaire.getIdConsultation() + "/" + formulaire.getIdNotice() + "/" + formulaire.getLang());

        }
    }

    @Override
    public EFormsFormulaire reprendre(Long id, String plateforme, String token) {
        EFormsFormulaireEntity eFormsFormulaireEntity = getFormsFormulaire(id, plateforme);
        SuiviAnnonce suiviAnnonce = eFormsFormulaireEntity.getSuiviAnnonce();
        return reprendreEFormsFormulaire(eFormsFormulaireEntity, suiviAnnonce, token);
    }

    @Override
    public EFormsFormulaire reprendreEFormsFormulaire(EFormsFormulaireEntity eFormsFormulaireEntity, SuiviAnnonce suiviAnnonce, String token) {
        if (!eFormsFormulaireEntity.getStatut().equals(StatutENoticeEnum.VALIDATION_FAILED) && !eFormsFormulaireEntity.getStatut().equals(StatutENoticeEnum.CONCENTRATEUR_REJET_ECO) && !eFormsFormulaireEntity.getStatut().equals(StatutENoticeEnum.CONCENTRATEUR_REJET_SIP) && !eFormsFormulaireEntity.getStatut().equals(StatutENoticeEnum.STOPPED)) {
            throw new AtexoException(ExceptionEnum.FORBIDDEN, "Le formulaire doit être au statut STOPPED ou VALIDATION_FAILED");
        }
        String versionSdk = eFormsFormulaireEntity.getVersionSdk();
        if ("esentool".equals(eFormsFormulaireEntity.getTedVersion())) {
            versionSdk = schematronConfiguration.getResourcesTag();
        }
        Agent agent = agentService.findByToken(token);
        String plateforme = eFormsFormulaireEntity.getPlateforme();
        EFormsFormulaireEntity newFormulaire = EFormsFormulaireEntity.builder().suiviAnnonce(suiviAnnonce).idNotice(eFormsFormulaireEntity.getIdNotice()).versionSdk(versionSdk).organisme(eFormsFormulaireEntity.getOrganisme()).plateforme(plateforme).uuid(eFormsFormulaireEntity.getUuid()).noticeVersion(eFormsFormulaireEntity.getNoticeVersion() + 1).idConsultation(eFormsFormulaireEntity.getIdConsultation()).intituleConsultation(eFormsFormulaireEntity.getIntituleConsultation()).referenceConsultation(eFormsFormulaireEntity.getReferenceConsultation()).previous(eFormsFormulaireEntity).auteurEmail(agent.getEmail()).lang(eFormsFormulaireEntity.getLang()).valid(false).statut(StatutENoticeEnum.DRAFT).procedure(eFormsFormulaireEntity.getProcedure()).creerPar(agent).tedVersion("enotices2").modifierPar(agent).dateCreation(ZonedDateTime.now()).dateModification(ZonedDateTime.now()).build();

        Object formulaire;
        String idNotice = eFormsFormulaireEntity.getIdNotice();
        SubNotice notice = this.getSubNotice(idNotice, versionSdk, plateforme);
        String formulaireBase64 = eFormsFormulaireEntity.getFormulaire();
        if (formulaireBase64 == null) {
            ConfigurationConsultationDTO configurationConsultationDTO = agentService.getConfiguration(token, eFormsFormulaireEntity.getIdConsultation(), plateforme);
            EFormsProcedureEntity eFormsProcedureEntity = getProcedureEntity(String.valueOf(eFormsFormulaireEntity.getIdConsultation()), eFormsFormulaireEntity.getIntituleConsultation());
            newFormulaire.setProcedure(eFormsProcedureEntity);
            formulaire = buildFormulaire(notice, newFormulaire, configurationConsultationDTO);
        } else {
            formulaire = getObjectFromString(new String(Base64.decodeBase64(formulaireBase64.getBytes())), Object.class);
        }
        formulaire = buildFormulaireReprendre(notice, eFormsFormulaireEntity, newFormulaire, formulaire);
        String formulaireString = getString(formulaire);
        newFormulaire.setFormulaire(Base64.encodeBase64StringUnChunked(formulaireString.getBytes()));
        return formulaireMapper.mapToModel(eFormsFormulaireRepository.save(newFormulaire));
    }


    private EFormsFormulaireEntity getFormsFormulaire(Long id, String plateforme) {
        EFormsFormulaireEntity eFormsFormulaireEntity = eFormsFormulaireRepository.findById(id).orElse(null);
        if (eFormsFormulaireEntity == null) {
            throw new AtexoException(ExceptionEnum.NOT_FOUND, "Le formulaire n'existe pas");
        }
        if (!plateforme.equals(eFormsFormulaireEntity.getPlateforme())) {
            throw new AtexoException(ExceptionEnum.FORBIDDEN, "Le formulaire n'est pas dans votre périmetre");
        }
        return eFormsFormulaireEntity;
    }

    private EFormsFormulaire getEFormsFormulaire(String plateforme, EFormsFormulaireEntity previousFormulaire, EFormsFormulaireEntity newFormulaire, Object formulaire, SubNotice notice) {
        String versionSdk = newFormulaire.getVersionSdk();
        DocumentType documentType = getSubNoticeList(versionSdk).getDocumentTypes().stream().filter(item -> item.getId().equals(notice.getDocumentType())).findFirst().orElseThrow(null);
        String idNotice = previousFormulaire.getIdNotice();
        String lang = previousFormulaire.getLang();
        Document xmlDocument = buildXmlDocument(formulaire, documentType, idNotice, lang, plateforme, versionSdk);
        String xmlString = EFormsHelper.getXmlString(xmlDocument);
        String formulaireString = getString(formulaire);
        newFormulaire.setFormulaire(Base64.encodeBase64StringUnChunked(formulaireString.getBytes()));
        newFormulaire.setXmlGenere(Base64.encodeBase64StringUnChunked(xmlString.getBytes()));
        File tempFile = EFormsHelper.getXmlFile(xmlDocument);
        String xsdPath = getEformsFolderSdk(versionSdk) + "/" + documentType.getSchemaLocation();
        String validXmlXsd = validateXMLSchema(xsdPath, tempFile.getAbsolutePath());
        ValidationResult validationResult = getValidationResult(tempFile, xmlString, "static", versionSdk);
        validationResult.setValidXml(validXmlXsd == null);
        tempFile.deleteOnExit();
        boolean valid = validationResult.isValid() && validXmlXsd == null;
        newFormulaire.setValid(valid);
        newFormulaire.setSvrlString(Base64.encodeBase64StringUnChunked(validationResult.getSvrlString().getBytes()));

        return formulaireMapper.mapToModel(eFormsFormulaireRepository.save(newFormulaire));
    }

    private void updateEsendTool(EFormsFormulaireEntity eFormsFormulaireEntity, ReponseSimap reponseSimap, String pattern) {
        // insertion d'un suivi de l'annonce
        StatutENoticeEnum statut = StatutENoticeEnum.getStatut(reponseSimap.getStatus());
        if (statut == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucun statut est défini pour " + reponseSimap.getStatus());
        }
        log.debug("Mise à jour TED de {} ", eFormsFormulaireEntity.getUuid());
        eFormsFormulaireEntity.setStatut(statut);
        SuiviAnnonce suiviAnnonce = eFormsFormulaireEntity.getSuiviAnnonce();
        org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        suiviAnnonce.setSupportPublication(SupportPublicationEnum.TED);
        suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.getStatutSuiviAnnonce(reponseSimap.getStatus()));
        String noDocExt = reponseSimap.getNoDocExt();
        if (StringUtils.hasText(noDocExt)) {
            suiviAnnonce.setNumeroAvis(noDocExt);
            eFormsFormulaireEntity.setNoDocExt(noDocExt);
        }
        String submissionId = reponseSimap.getSubmissionId();
        if (StringUtils.hasText(submissionId)) eFormsFormulaireEntity.setUuid(submissionId);
        String etat = gestionEtat(reponseSimap);
        if (StringUtils.hasText(etat)) {
            suiviAnnonce.setMessageStatut(etat);
        } else {
            suiviAnnonce.setMessageStatut(reponseSimap.getStatus());
        }


        PublicationInfo publicationInfo = reponseSimap.getPublicationInfo();
        if (publicationInfo != null) {
            // pendant la recette ces infos ne sont pas transmise.
            if (publicationInfo.getTedLinks() != null && !CollectionUtils.isEmpty(publicationInfo.getTedLinks().getAdditionalProperties())) {
                String lienPublication = publicationInfo.getTedLinks().getAdditionalProperties().entrySet().stream().filter(stringStringEntry -> {
                    String key = stringStringEntry.getKey();
                    return key.equalsIgnoreCase(eFormsFormulaireEntity.getLang().substring(0, 2));
                }).findFirst().map(Map.Entry::getValue).orElse(null);
                if (StringUtils.hasText(lienPublication)) {
                    suiviAnnonce.setLienPublication(lienPublication);
                }
            }
            String noDocOjs = publicationInfo.getNoDocOjs();
            if (StringUtils.hasText(noDocOjs)) {
                suiviAnnonce.setNumeroJoue(noDocOjs);
                eFormsFormulaireEntity.setPublicationId(noDocOjs);
            }
            String datePublication = publicationInfo.getPublicationDate();
            if (StringUtils.hasText(datePublication)) {
                DateTime parsedDatePublication = formatter.parseDateTime(datePublication);
                suiviAnnonce.setDatePublication(parsedDatePublication.toDate().toInstant());
                suiviAnnonce.setDatePublicationTed(parsedDatePublication.toDate().toInstant());
                Timestamp timeStampDatePublication = new Timestamp(parsedDatePublication.getMillis());
                eFormsFormulaireEntity.setPublicationDate(timeStampDatePublication);
            }
        }

        String dateSubmission = reponseSimap.getReceivedAt();
        if (StringUtils.hasText(dateSubmission)) {
            DateTime parsedDateSubmission = formatter.parseDateTime(dateSubmission);
            Timestamp timeStampDateSubmission = new Timestamp(parsedDateSubmission.getMillis());
            ZonedDateTime dateSoumission = timeStampDateSubmission.toLocalDateTime().atZone(ZoneId.systemDefault());
            eFormsFormulaireEntity.setDateSoumission(dateSoumission);
            eFormsFormulaireEntity.setDateModificationStatut(dateSoumission);
            suiviAnnonce.setDateSoumission(dateSoumission.toInstant());
            suiviAnnonce.setDateSoumissionTed(dateSoumission.toInstant());
        }
        SuiviTypeAvisPub typeAvisPub = suiviAnnonce.getTypeAvisPub();
        int id = suiviAnnonce.getId();
        updateTypeAvis(statut, id, suiviAnnonce.getStatut(), typeAvisPub);

        eFormsFormulaireRepository.save(eFormsFormulaireEntity);
        suiviAnnonceRepository.save(suiviAnnonce);

    }

    @Override
    public EformsSurchargeDTO surchargerPlateforme(String plateforme, String code, String versionSdk, EformsSurchargePatch patch) {
        if (patch == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Le patch est vide");
        }

        EFormsSurcharge surcharge;
        if (patch.getIdNotice() != null) {
            surcharge = eformsSurchargeRepository.findByNoticeIdAndCodeAndIdPlatform(patch.getIdNotice(), code, plateforme).orElse(null);
        } else {
            eformsSurchargeRepository.disableByNoticeIdAndCodeAndIdPlatform(code, plateforme);
            surcharge = eformsSurchargeRepository.findByCodeAndIdPlatformAndNoticeIdIsNull(code, plateforme).orElse(null);
        }
        if (surcharge == null) {
            surcharge = EFormsSurcharge.builder().code(code).idPlatform(plateforme).noticeId(patch.getIdNotice()).actif(true).defaultValue(patch.getDefaultValue()).hidden(patch.getHidden()).dynamicListFrom(patch.getDynamicListFrom()).staticList(patch.getStaticList()).readonly(patch.getReadonly()).ordre(patch.getOrdre()).obligatoire(patch.getObligatoire()).build();
        } else {
            surcharge.setActif(true);
            surcharge.setDefaultValue(patch.getDefaultValue());
            surcharge.setHidden(patch.getHidden());
            surcharge.setDynamicListFrom(patch.getDynamicListFrom());
            surcharge.setStaticList(patch.getStaticList());
            surcharge.setReadonly(patch.getReadonly());
            surcharge.setOrdre(patch.getOrdre());
            surcharge.setObligatoire(patch.getObligatoire());
        }

        return eformsSurchargeMapper.mapToModel(eformsSurchargeRepository.save(surcharge));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateTedFromList(List<ReponseSimap> content, String pattern) {
        content.forEach(reponseSimap -> {
                try {
                    EFormsFormulaireEntity eFormsFormulaireEntity = eFormsFormulaireRepository.findByUuidAndSuiviAnnonceIsNotNull(reponseSimap.getSubmissionId());
                    if (eFormsFormulaireEntity == null)
                        eFormsFormulaireEntity = eFormsFormulaireRepository.findByUuidAndSuiviAnnonceIsNotNull("TED72-" + reponseSimap.getSubmissionId());
                    if (eFormsFormulaireEntity == null && reponseSimap.getNoDocExt() != null)
                        eFormsFormulaireEntity = eFormsFormulaireRepository.findByNoDocExtAndSuiviAnnonceIsNotNull(reponseSimap.getNoDocExt());
                    if (eFormsFormulaireEntity != null) {
                        updateEsendTool(eFormsFormulaireEntity, reponseSimap, pattern);
                    } else {
                        log.warn("Aucun formulaire trouvé pour la soumission {}", reponseSimap.getSubmissionId());
                    }

                } catch (Exception e) {
                    log.error("Erreur lors de la synchronisation de la notice {}", reponseSimap.getNoDocExt(), e);
                }

            });
    }


    @Override
    public EformsSurchargeDTO surchargerAtexo(String code, String versionSdk, EformsSurchargePatch patch) {
        if (patch == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Le patch est vide");
        }

        EFormsSurcharge surcharge;
        if (patch.getIdNotice() != null) {
            surcharge = eformsSurchargeRepository.findByNoticeIdAndCodeAndIdPlatformIsNull(patch.getIdNotice(), code).orElse(null);
        } else {
            eformsSurchargeRepository.disableByNoticeIdAndCodeAndIdPlatformIsNull(code);
            surcharge = eformsSurchargeRepository.findByCodeAndIdPlatformIsNullAndNoticeIdIsNull(code).orElse(null);
        }
        if (surcharge == null) {
            surcharge = EFormsSurcharge.builder().code(code).noticeId(patch.getIdNotice()).actif(true).defaultValue(patch.getDefaultValue()).hidden(patch.getHidden()).dynamicListFrom(patch.getDynamicListFrom()).staticList(patch.getStaticList()).readonly(patch.getReadonly()).obligatoire(patch.getObligatoire()).build();
        } else {
            surcharge.setActif(true);
            surcharge.setDefaultValue(patch.getDefaultValue());
            surcharge.setHidden(patch.getHidden());
            surcharge.setDynamicListFrom(patch.getDynamicListFrom());
            surcharge.setStaticList(patch.getStaticList());
            surcharge.setReadonly(patch.getReadonly());
            surcharge.setObligatoire(patch.getObligatoire());
        }

        return eformsSurchargeMapper.mapToModel(eformsSurchargeRepository.save(surcharge));
    }


    private String gestionEtat(ReponseSimap reponseSimap) {
        StringBuilder etat = new StringBuilder();
        if (reponseSimap.getStatus() != null && reponseSimap.getStatus().equals("NOT_PUBLISHED")) {

            switch (reponseSimap.getReasonCode().toString()) {
                case "BV":
                    etat = new StringBuilder("Business validation");
                    break;
                case "SV":
                    etat = new StringBuilder("Securité Validation");
                    break;
                case "XMLV":
                    etat = new StringBuilder("Xml validation");
                    break;
                case "XMLc":
                    etat = new StringBuilder("Xml conversion");
                    break;
                default:
                    break;
            }

        } else if (reponseSimap.getStatus() != null && reponseSimap.getStatus().equals("QUALIFICATION_ERROR")) {

            etat = new StringBuilder("la publication ne peut être validé pour la qualification ");

        } else if (reponseSimap.getStatus() != null && reponseSimap.getStatus().equals("NOT_PUBLISHED")) {

            switch (reponseSimap.getReasonCode().toString()) {
                case "CP":
                    etat = new StringBuilder("Cancel Publication");
                    break;
                case "CPV":
                    etat = new StringBuilder("Mauvais CPV");
                    break;
                case "DU":
                    etat = new StringBuilder("Duplication");
                    break;
                case "HR":
                    etat = new StringBuilder("Autorisation du header refusé");
                    break;
                case "ILD":
                    etat = new StringBuilder("Illegible Demfax");
                    break;
                case "IN":
                    etat = new StringBuilder("Document incomplet");
                    break;
                case "MD":
                    etat = new StringBuilder("Modification");
                    break;
                case "NP":
                    etat = new StringBuilder("Pas pour publication");
                    break;
                case "NA":
                    etat = new StringBuilder("No Answer to Demfax");
                    break;
                case "OD":
                    etat = new StringBuilder("Autre Département");
                    break;
                case "OT":
                    etat = new StringBuilder("Autre raison");
                    break;
                case "PNP":
                    etat = new StringBuilder("Préparé Non publié");
                    break;
                case "WFN":
                    etat = new StringBuilder("Mauvaise forme");
                    break;
                case "WFI":
                    etat = new StringBuilder("Mauvaise information de l'attibution de l'autorité");
                    break;
                case "WL":
                    etat = new StringBuilder("Mauvais language");
                    break;
                case "NDX":
                    etat = new StringBuilder("NoDocExt existe déjà");
                    break;
                default:
                    break;
            }
        }
        if (reponseSimap.getValidationRulesReport() != null)
            for (Item item : reponseSimap.getValidationRulesReport().getItems()) {
                if (!Boolean.TRUE.equals(item.getValid()) && item.getSeverity().equals("ERROR")) {
                    etat.append("\n").append(item.getMessage());
                }
            }
        if (reponseSimap.getTechnicalValidationReport() != null)
            for (Item item : reponseSimap.getTechnicalValidationReport().getItems()) {
                if (!Boolean.TRUE.equals(item.getValid()) && item.getSeverity().equals("ERROR")) {
                    etat.append("\n").append(item.getMessage());
                }
            }

        return etat.toString();
    }

    @Override
    public void updateEForms(EFormsFormulaireEntity eFormsFormulaireEntity) {
        Integer noticeVersion = eFormsFormulaireEntity.getNoticeVersion();
        var plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(eFormsFormulaireEntity.getPlateforme());
        String eformsToken = plateformeConfiguration == null ? null : plateformeConfiguration.getEformsToken();
        EsentoolPageDTO pageDTO = eFormsWS.search(0, 1, EformsSearch.builder().versionId((noticeVersion > 9 ? "" : "0") + noticeVersion).id(eFormsFormulaireEntity.getUuid()).build(), eformsToken);
        if (pageDTO != null && !CollectionUtils.isEmpty(pageDTO.getContent())) {
            EsentoolNoticeDTO esentoolNoticeDTO = pageDTO.getContent().get(0);
            StatutENoticeEnum status = esentoolNoticeDTO.getStatus();
            if (status.equals(eFormsFormulaireEntity.getStatut())) {
                return;
            }
            log.info("Mise à jour de EFORMS {}", eFormsFormulaireEntity.getUuid());
            eFormsFormulaireEntity.setStatut(status);
            Date publicationDate = esentoolNoticeDTO.getPublicationDate();
            eFormsFormulaireEntity.setPublicationDate(publicationDate);
            String publicationId = esentoolNoticeDTO.getPublicationId();
            eFormsFormulaireEntity.setPublicationId(publicationId);
            ZonedDateTime submittedAt = esentoolNoticeDTO.getSubmittedAt();
            eFormsFormulaireEntity.setDateSoumission(submittedAt);
            eFormsFormulaireEntity = eFormsFormulaireRepository.save(eFormsFormulaireEntity);
            SuiviAnnonce suiviAnnonce = eFormsFormulaireEntity.getSuiviAnnonce();

            updateEformsAnnonce(esentoolNoticeDTO, status, publicationDate, submittedAt, suiviAnnonce);
        }

    }

    private void updateEformsAnnonce(EsentoolNoticeDTO esentoolNoticeDTO, StatutENoticeEnum status, Date publicationDate, ZonedDateTime submittedAt, SuiviAnnonce suiviAnnonce) {
        if (suiviAnnonce != null) {
            suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.getStatutSuiviAnnonceFromENotice(status));
            if (publicationDate != null) suiviAnnonce.setDatePublication(publicationDate.toInstant());
            String publicationId = esentoolNoticeDTO.getPublicationId();
            if (publicationId != null) {
                suiviAnnonce.setNumeroAvis(publicationId);
                int length = publicationId.length();
                String last11Characters = length > 11 ? publicationId.substring(length - 11) : publicationId;
                suiviAnnonce.setLienPublication("https://ted.europa.eu/udl?uri=TED:NOTICE:" + last11Characters + ":TEXT:EN:HTML&src=0");
                suiviAnnonce.setNumeroJoue(last11Characters);
            }
            if (submittedAt != null) {
                suiviAnnonce.setDateSoumission(submittedAt.toInstant());
                suiviAnnonce.setDateSoumissionTed(submittedAt.toInstant());
            }
            StatutSuiviAnnonceEnum statut = suiviAnnonce.getStatut();
            suiviAnnonce.setSupportPublication(SupportPublicationEnum.TED);
            if (StatutSuiviAnnonceEnum.REJETER_SUPPORT.equals(statut)) {
                ValidationResult rapport = this.getValidationReport(suiviAnnonce.getFormulaire().getId(), suiviAnnonce.getIdPlatform());
                if (rapport != null && !CollectionUtils.isEmpty(rapport.getErrors())) {
                    Map<String, Object> i18n = getSdkI18n("fr", schematronConfiguration.getLibelleTag());
                    String message = rapport.getErrors().stream().map(FailedAssertRepresentation::getText).map(s -> i18n.getOrDefault(s, s)).map(String.class::cast).collect(Collectors.joining(", "));
                    suiviAnnonce.setMessageStatut(message);
                }

            }
            SuiviTypeAvisPub typeAvisPub = suiviAnnonce.getTypeAvisPub();
            int id = suiviAnnonce.getId();
            updateTypeAvis(status, id, statut, typeAvisPub);

            suiviAnnonceRepository.save(suiviAnnonce);
        }
    }

    private void updateTypeAvis(StatutENoticeEnum status, int id, StatutSuiviAnnonceEnum statut, SuiviTypeAvisPub typeAvisPub) {
        if (typeAvisPub != null) {
            List<SuiviAnnonce> annonces = typeAvisPub.getAnnonces();
            if (StatutENoticeEnum.SUBMITTED.equals(status)) {
                List<SuiviAnnonce> annoncesNationauxToSend = annonces.stream().filter(suiviAnnonce1 -> {
                    Support support = suiviAnnonce1.getSupport();
                    return support.isNational() && !support.isExternal() && !StatutSuiviAnnonceEnum.EN_ATTENTE.equals(suiviAnnonce1.getStatut());
                }).collect(Collectors.toList());
                annoncesNationauxToSend.forEach(suiviAnnonce1 -> {
                    suiviAnnonce1.setStatut(StatutSuiviAnnonceEnum.EN_ATTENTE);
                    suiviAnnonce1.setDateDemandeEnvoi(ZonedDateTime.now().toInstant());
                    suiviAnnonce1 = suiviAnnonceRepository.save(suiviAnnonce1);
                });

            }
            List<StatutSuiviAnnonceEnum> statuts = annonces.stream().map(SuiviAnnonce::getStatut).distinct().toList();
            StatutTypeAvisAnnonceEnum statutFromAnnonceEnum = StatutTypeAvisAnnonceEnum.getStatutFromAnnonceEnum(statuts);
            if (statutFromAnnonceEnum.equals(typeAvisPub.getStatut())) {
                return;
            }
            typeAvisPub.setStatut(statutFromAnnonceEnum);
            typeAvisPub.setDateModificationStatut(ZonedDateTime.now());
            typeAvisPub.setRaisonRefus(StatutSuiviAnnonceEnum.REJETER_SUPPORT.equals(statut) ? "Rejeté par le support TED" : null);
            if (StatutTypeAvisAnnonceEnum.REJETE.equals(statutFromAnnonceEnum)) {
                annonces.stream().filter(suiviAnnonce -> StatutSuiviAnnonceEnum.ENVOI_PLANIFIER.equals(suiviAnnonce.getStatut())).forEach(suiviAnnonce1 -> {
                    if (suiviAnnonce1.getId() != id) {
                        suiviAnnonce1.setStatut(StatutSuiviAnnonceEnum.ANNULER);
                        suiviAnnonce1.setMessageStatut("Publication Annulé suite au rejet par le support TED");
                        suiviAnnonceRepository.save(suiviAnnonce1);
                    }
                });
            }
        }
    }
}
