package fr.atexo.annonces.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

/**
 * DTO - Représente un support de publication
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class OffreResumeDTO {

    private Integer id;
    private String libelle;
    private String code;
    private String codeGroupe;
    private Boolean europeen;

}
