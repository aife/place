package fr.atexo.annonces.boot.ws.tncp;

import fr.atexo.annonces.commun.tncp.Avis;
import fr.atexo.annonces.commun.tncp.Client;
import fr.atexo.annonces.commun.tncp.Redirection;
import fr.atexo.annonces.commun.tncp.Token;
import fr.atexo.annonces.config.EFormsHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@Component
@Slf4j
public class TncpWS {
    private final URI authentification;
    private final URI publication;
    private final RestTemplate atexoRestTemplate;

    public TncpWS(@Value("${tncp.piste.auth.url}") URI authentification, @Value("${tncp.piste.url}${tncp.piste.creation}") URI publication, RestTemplate atexoRestTemplate) {
        this.authentification = authentification;
        this.publication = publication;
        this.atexoRestTemplate = atexoRestTemplate;
    }

    public Token authentifier(final Client client) {
        log.info("Authentification auprès de PLACE avec le client = {} de URL = {}", client.clientId(), authentification);
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));
        // Request body
        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("client_id", client.clientId());
        requestBody.add("client_secret", client.clientSecret());
        requestBody.add("grant_type", "client_credentials");
        requestBody.add("scope", "openid");

        return atexoRestTemplate.exchange(authentification, HttpMethod.POST, new HttpEntity<>(requestBody, headers), Token.class).getBody();
    }


    public Redirection publier(final Token token, final Avis avis) {
        log.info("Publication auprès de TNCP avec le token = {} de URL = {}", token, publication);
        String body = EFormsHelper.getString(avis);
        log.info("Avec le json = {}", body);
        var headers = new HttpHeaders();
        headers.setBearerAuth(token.getAccessToken());
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));

        return atexoRestTemplate.exchange(publication, HttpMethod.POST, new HttpEntity<>(body, headers), Redirection.class).getBody();
    }



}
