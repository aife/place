package fr.atexo.annonces.dto;

import lombok.*;

import java.io.Serializable;


@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SuiviTypeAvisPubPieceJointeAssoDTO implements Serializable {

    private static final long serialVersionUID = -5416829527656795263L;
    private Long id;

    private PieceJointeDTO pieceJointe;
}
