//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.06.02 à 04:50:57 PM CEST 
//


package fr.atexo.annonces.dto.mol;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java pour anonymous complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="avis_concentrateur"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="id_avis" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *                   &lt;element name="uid_consultation" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="date_envoi" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="nom_fichier_annonce" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="organisme" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/&gt;
 *                   &lt;element name="suivi"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="AVIS"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="SUPPORT" maxOccurs="unbounded"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;choice maxOccurs="unbounded"&gt;
 *                                                   &lt;element name="STATUT"&gt;
 *                                                     &lt;complexType&gt;
 *                                                       &lt;complexContent&gt;
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                           &lt;attribute name="DATE" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *                                                           &lt;attribute name="VALUE" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *                                                         &lt;/restriction&gt;
 *                                                       &lt;/complexContent&gt;
 *                                                     &lt;/complexType&gt;
 *                                                   &lt;/element&gt;
 *                                                   &lt;element name="ANNONCE"&gt;
 *                                                     &lt;complexType&gt;
 *                                                       &lt;complexContent&gt;
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                           &lt;sequence minOccurs="0"&gt;
 *                                                             &lt;element name="INFOS_PUB"&gt;
 *                                                               &lt;complexType&gt;
 *                                                                 &lt;complexContent&gt;
 *                                                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                                     &lt;attribute name="URL_PDF" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                                                     &lt;attribute name="URL" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                                                     &lt;attribute name="PARUTION" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                                                     &lt;attribute name="PAR_TYPE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                                                     &lt;attribute name="NUM_ANN_PAR" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                                                     &lt;attribute name="DATE_PUB" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *                                                                   &lt;/restriction&gt;
 *                                                                 &lt;/complexContent&gt;
 *                                                               &lt;/complexType&gt;
 *                                                             &lt;/element&gt;
 *                                                           &lt;/sequence&gt;
 *                                                           &lt;attribute name="ID_JO" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                                           &lt;attribute name="ID_ANN" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                                           &lt;attribute name="FICHIER" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                                           &lt;attribute name="ETAT" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                                           &lt;attribute name="DATE_ENVOI" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *                                                         &lt;/restriction&gt;
 *                                                       &lt;/complexContent&gt;
 *                                                     &lt;/complexType&gt;
 *                                                   &lt;/element&gt;
 *                                                 &lt;/choice&gt;
 *                                               &lt;/sequence&gt;
 *                                               &lt;attribute name="CODE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                     &lt;attribute name="TYPE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                     &lt;attribute name="FORMAT" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                     &lt;attribute name="NATURE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                     &lt;attribute name="DMT" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" /&gt;
 *                                     &lt;attribute name="REF_CONSULTATION" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                     &lt;attribute name="TITLE_CONSULTATION" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                     &lt;attribute name="ID_CONSULTATION" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" /&gt;
 *                                     &lt;attribute name="DATE_SENT" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *                                     &lt;attribute name="ID" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" /&gt;
 *                                     &lt;attribute name="TITLE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="reference_utilisateur" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "response", propOrder = {"avisConcentrateur"})
@XmlRootElement(name = "response")
@ToString
@EqualsAndHashCode
@JacksonXmlRootElement(localName = "reponse")
public class Response {

    @XmlElement(name = "avis_concentrateur", required = true)
    @JsonProperty("avis_concentrateur")
    protected List<AvisConcentrateur> avisConcentrateur;

    /**
     * Obtient la valeur de la propriété avisConcentrateur.
     *
     * @return possible object is
     * {@link AvisConcentrateur }
     */
    public List<AvisConcentrateur> getAvisConcentrateur() {
        if (avisConcentrateur == null)
            avisConcentrateur = new ArrayList<>();
        return avisConcentrateur;
    }

    /**
     * Définit la valeur de la propriété avisConcentrateur.
     *
     * @param value allowed object is
     *              {@link AvisConcentrateur }
     */
    public void setAvisConcentrateur(List<AvisConcentrateur> value) {
        this.avisConcentrateur = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="id_avis" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
     *         &lt;element name="uid_consultation" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="date_envoi" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="nom_fichier_annonce" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="organisme" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/&gt;
     *         &lt;element name="suivi"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="AVIS"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="SUPPORT" maxOccurs="unbounded"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;choice maxOccurs="unbounded"&gt;
     *                                         &lt;element name="STATUT"&gt;
     *                                           &lt;complexType&gt;
     *                                             &lt;complexContent&gt;
     *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                                 &lt;attribute name="DATE" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
     *                                                 &lt;attribute name="VALUE" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
     *                                               &lt;/restriction&gt;
     *                                             &lt;/complexContent&gt;
     *                                           &lt;/complexType&gt;
     *                                         &lt;/element&gt;
     *                                         &lt;element name="ANNONCE"&gt;
     *                                           &lt;complexType&gt;
     *                                             &lt;complexContent&gt;
     *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                                 &lt;sequence minOccurs="0"&gt;
     *                                                   &lt;element name="INFOS_PUB"&gt;
     *                                                     &lt;complexType&gt;
     *                                                       &lt;complexContent&gt;
     *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                                           &lt;attribute name="URL_PDF" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                                                           &lt;attribute name="URL" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                                                           &lt;attribute name="PARUTION" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                                                           &lt;attribute name="PAR_TYPE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                                                           &lt;attribute name="NUM_ANN_PAR" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                                                           &lt;attribute name="DATE_PUB" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
     *                                                         &lt;/restriction&gt;
     *                                                       &lt;/complexContent&gt;
     *                                                     &lt;/complexType&gt;
     *                                                   &lt;/element&gt;
     *                                                 &lt;/sequence&gt;
     *                                                 &lt;attribute name="ID_JO" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                                                 &lt;attribute name="ID_ANN" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                                                 &lt;attribute name="FICHIER" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                                                 &lt;attribute name="ETAT" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                                                 &lt;attribute name="DATE_ENVOI" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
     *                                               &lt;/restriction&gt;
     *                                             &lt;/complexContent&gt;
     *                                           &lt;/complexType&gt;
     *                                         &lt;/element&gt;
     *                                       &lt;/choice&gt;
     *                                     &lt;/sequence&gt;
     *                                     &lt;attribute name="CODE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                           &lt;attribute name="TYPE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                           &lt;attribute name="FORMAT" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                           &lt;attribute name="NATURE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                           &lt;attribute name="DMT" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" /&gt;
     *                           &lt;attribute name="REF_CONSULTATION" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                           &lt;attribute name="TITLE_CONSULTATION" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                           &lt;attribute name="ID_CONSULTATION" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" /&gt;
     *                           &lt;attribute name="DATE_SENT" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
     *                           &lt;attribute name="ID" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" /&gt;
     *                           &lt;attribute name="TITLE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="reference_utilisateur" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"idAvis", "uidConsultation", "dateEnvoi", "nomFichierAnnonce", "organisme", "suivi", "referenceUtilisateur"})
    @ToString
    @EqualsAndHashCode
    @Builder
    public static class AvisConcentrateur {

        @XmlElement(name = "id_avis")
        @JsonProperty("id_avis")
        protected String idAvis;
        @XmlElement(name = "uid_consultation", required = true)
        @JsonProperty("uid_consultation")
        protected String uidConsultation;
        @JsonProperty("date_envoi")
        @XmlElement(name = "date_envoi", required = true)
        protected String dateEnvoi;
        @XmlElement(name = "nom_fichier_annonce", required = true)
        @JsonProperty("nom_fichier_annonce")
        protected String nomFichierAnnonce;
        protected String organisme;
        @XmlElement(required = true)
        protected String suivi;
        @XmlElement(name = "reference_utilisateur", required = true)
        @JsonProperty("reference_utilisateur")
        protected String referenceUtilisateur;

        /**
         * Obtient la valeur de la propriété idAvis.
         */
        public String getIdAvis() {
            return idAvis;
        }

        /**
         * Définit la valeur de la propriété idAvis.
         */
        public void setIdAvis(String value) {
            this.idAvis = value;
        }

        /**
         * Obtient la valeur de la propriété uidConsultation.
         *
         * @return possible object is
         * {@link String }
         */
        public String getUidConsultation() {
            return uidConsultation;
        }

        /**
         * Définit la valeur de la propriété uidConsultation.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setUidConsultation(String value) {
            this.uidConsultation = value;
        }

        /**
         * Obtient la valeur de la propriété dateEnvoi.
         *
         * @return possible object is
         * {@link String }
         */
        public String getDateEnvoi() {
            return dateEnvoi;
        }

        /**
         * Définit la valeur de la propriété dateEnvoi.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setDateEnvoi(String value) {
            this.dateEnvoi = value;
        }

        /**
         * Obtient la valeur de la propriété nomFichierAnnonce.
         *
         * @return possible object is
         * {@link String }
         */
        public String getNomFichierAnnonce() {
            return nomFichierAnnonce;
        }

        /**
         * Définit la valeur de la propriété nomFichierAnnonce.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setNomFichierAnnonce(String value) {
            this.nomFichierAnnonce = value;
        }

        /**
         * Obtient la valeur de la propriété organisme.
         */
        public String getOrganisme() {
            return organisme;
        }

        /**
         * Définit la valeur de la propriété organisme.
         */
        public void setOrganisme(String value) {
            this.organisme = value;
        }

        /**
         * Obtient la valeur de la propriété suivi.
         *
         * @return possible object is
         * {@link Suivi }
         */
        public String getSuivi() {
            return suivi;
        }

        /**
         * Définit la valeur de la propriété suivi.
         *
         * @param value allowed object is
         *              {@link Suivi }
         */
        public void setSuivi(String value) {
            this.suivi = value;
        }

        /**
         * Obtient la valeur de la propriété referenceUtilisateur.
         *
         * @return possible object is
         * {@link String }
         */
        public String getReferenceUtilisateur() {
            return referenceUtilisateur;
        }

        /**
         * Définit la valeur de la propriété referenceUtilisateur.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setReferenceUtilisateur(String value) {
            this.referenceUtilisateur = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="AVIS"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="SUPPORT" maxOccurs="unbounded"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;choice maxOccurs="unbounded"&gt;
         *                               &lt;element name="STATUT"&gt;
         *                                 &lt;complexType&gt;
         *                                   &lt;complexContent&gt;
         *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                       &lt;attribute name="DATE" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
         *                                       &lt;attribute name="VALUE" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
         *                                     &lt;/restriction&gt;
         *                                   &lt;/complexContent&gt;
         *                                 &lt;/complexType&gt;
         *                               &lt;/element&gt;
         *                               &lt;element name="ANNONCE"&gt;
         *                                 &lt;complexType&gt;
         *                                   &lt;complexContent&gt;
         *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                       &lt;sequence minOccurs="0"&gt;
         *                                         &lt;element name="INFOS_PUB"&gt;
         *                                           &lt;complexType&gt;
         *                                             &lt;complexContent&gt;
         *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                                 &lt;attribute name="URL_PDF" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                                                 &lt;attribute name="URL" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                                                 &lt;attribute name="PARUTION" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                                                 &lt;attribute name="PAR_TYPE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                                                 &lt;attribute name="NUM_ANN_PAR" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                                                 &lt;attribute name="DATE_PUB" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
         *                                               &lt;/restriction&gt;
         *                                             &lt;/complexContent&gt;
         *                                           &lt;/complexType&gt;
         *                                         &lt;/element&gt;
         *                                       &lt;/sequence&gt;
         *                                       &lt;attribute name="ID_JO" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                                       &lt;attribute name="ID_ANN" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                                       &lt;attribute name="FICHIER" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                                       &lt;attribute name="ETAT" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                                       &lt;attribute name="DATE_ENVOI" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
         *                                     &lt;/restriction&gt;
         *                                   &lt;/complexContent&gt;
         *                                 &lt;/complexType&gt;
         *                               &lt;/element&gt;
         *                             &lt;/choice&gt;
         *                           &lt;/sequence&gt;
         *                           &lt;attribute name="CODE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *                 &lt;attribute name="TYPE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                 &lt;attribute name="FORMAT" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                 &lt;attribute name="NATURE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                 &lt;attribute name="DMT" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" /&gt;
         *                 &lt;attribute name="REF_CONSULTATION" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                 &lt;attribute name="TITLE_CONSULTATION" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                 &lt;attribute name="ID_CONSULTATION" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" /&gt;
         *                 &lt;attribute name="DATE_SENT" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
         *                 &lt;attribute name="ID" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" /&gt;
         *                 &lt;attribute name="TITLE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {"avis"})
        @ToString
        @EqualsAndHashCode
        public static class Suivi {

            @XmlElement(name = "AVIS", required = true)
            @JsonProperty("AVIS")
            protected AVIS avis;

            /**
             * Obtient la valeur de la propriété avis.
             *
             * @return possible object is
             * {@link AVIS }
             */
            public AVIS getAVIS() {
                return avis;
            }

            /**
             * Définit la valeur de la propriété avis.
             *
             * @param value allowed object is
             *              {@link AVIS }
             */
            public void setAVIS(AVIS value) {
                this.avis = value;
            }


            /**
             * <p>Classe Java pour anonymous complex type.
             *
             * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="SUPPORT" maxOccurs="unbounded"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;choice maxOccurs="unbounded"&gt;
             *                     &lt;element name="STATUT"&gt;
             *                       &lt;complexType&gt;
             *                         &lt;complexContent&gt;
             *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                             &lt;attribute name="DATE" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
             *                             &lt;attribute name="VALUE" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
             *                           &lt;/restriction&gt;
             *                         &lt;/complexContent&gt;
             *                       &lt;/complexType&gt;
             *                     &lt;/element&gt;
             *                     &lt;element name="ANNONCE"&gt;
             *                       &lt;complexType&gt;
             *                         &lt;complexContent&gt;
             *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                             &lt;sequence minOccurs="0"&gt;
             *                               &lt;element name="INFOS_PUB"&gt;
             *                                 &lt;complexType&gt;
             *                                   &lt;complexContent&gt;
             *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                                       &lt;attribute name="URL_PDF" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *                                       &lt;attribute name="URL" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *                                       &lt;attribute name="PARUTION" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *                                       &lt;attribute name="PAR_TYPE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *                                       &lt;attribute name="NUM_ANN_PAR" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *                                       &lt;attribute name="DATE_PUB" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
             *                                     &lt;/restriction&gt;
             *                                   &lt;/complexContent&gt;
             *                                 &lt;/complexType&gt;
             *                               &lt;/element&gt;
             *                             &lt;/sequence&gt;
             *                             &lt;attribute name="ID_JO" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *                             &lt;attribute name="ID_ANN" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *                             &lt;attribute name="FICHIER" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *                             &lt;attribute name="ETAT" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *                             &lt;attribute name="DATE_ENVOI" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
             *                           &lt;/restriction&gt;
             *                         &lt;/complexContent&gt;
             *                       &lt;/complexType&gt;
             *                     &lt;/element&gt;
             *                   &lt;/choice&gt;
             *                 &lt;/sequence&gt;
             *                 &lt;attribute name="CODE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *       &lt;attribute name="TYPE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *       &lt;attribute name="FORMAT" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *       &lt;attribute name="NATURE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *       &lt;attribute name="DMT" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" /&gt;
             *       &lt;attribute name="REF_CONSULTATION" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *       &lt;attribute name="TITLE_CONSULTATION" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *       &lt;attribute name="ID_CONSULTATION" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" /&gt;
             *       &lt;attribute name="DATE_SENT" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
             *       &lt;attribute name="ID" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" /&gt;
             *       &lt;attribute name="TITLE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {"support"})
            @ToString
            @EqualsAndHashCode
            public static class AVIS {

                @XmlElement(name = "SUPPORT", required = true)
                @JsonProperty("SUPPORT")
                protected List<SUPPORT> support;
                @JsonProperty("TYPE")
                @XmlAttribute(name = "TYPE", required = true)
                protected String type;
                @JsonProperty("FORMAT")
                @XmlAttribute(name = "FORMAT", required = true)
                protected String format;
                @JsonProperty("NATURE")
                @XmlAttribute(name = "NATURE", required = true)
                protected String nature;
                @XmlAttribute(name = "DMT", required = true)
                @JsonProperty("DMT")
                @XmlSchemaType(name = "unsignedByte")
                protected short dmt;
                @XmlAttribute(name = "REF_CONSULTATION", required = true)
                @JsonProperty("REF_CONSULTATION")
                protected String refconsultation;
                @XmlAttribute(name = "TITLE_CONSULTATION", required = true)
                @JsonProperty("TITLE_CONSULTATION")
                protected String titleconsultation;
                @XmlAttribute(name = "ID_CONSULTATION", required = true)
                @XmlSchemaType(name = "unsignedInt")
                @JsonProperty("ID_CONSULTATION")
                protected long idconsultation;
                @XmlAttribute(name = "DATE_SENT", required = true)
                @JsonProperty("DATE_SENT")
                @XmlSchemaType(name = "dateTime")
                protected XMLGregorianCalendar datesent;
                @XmlAttribute(name = "ID", required = true)
                @JsonProperty("ID")
                @XmlSchemaType(name = "unsignedInt")
                protected long id;
                @JsonProperty("TITLE")
                @XmlAttribute(name = "TITLE", required = true)
                protected String title;

                /**
                 * Gets the value of the support property.
                 *
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the support property.
                 *
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getSUPPORT().add(newItem);
                 * </pre>
                 *
                 *
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link SUPPORT }
                 */
                public List<SUPPORT> getSUPPORT() {
                    if (support == null) {
                        support = new ArrayList<SUPPORT>();
                    }
                    return this.support;
                }

                /**
                 * Obtient la valeur de la propriété type.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getTYPE() {
                    return type;
                }

                /**
                 * Définit la valeur de la propriété type.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setTYPE(String value) {
                    this.type = value;
                }

                /**
                 * Obtient la valeur de la propriété format.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getFORMAT() {
                    return format;
                }

                /**
                 * Définit la valeur de la propriété format.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setFORMAT(String value) {
                    this.format = value;
                }

                /**
                 * Obtient la valeur de la propriété nature.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getNATURE() {
                    return nature;
                }

                /**
                 * Définit la valeur de la propriété nature.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setNATURE(String value) {
                    this.nature = value;
                }

                /**
                 * Obtient la valeur de la propriété dmt.
                 */
                public short getDMT() {
                    return dmt;
                }

                /**
                 * Définit la valeur de la propriété dmt.
                 */
                public void setDMT(short value) {
                    this.dmt = value;
                }

                /**
                 * Obtient la valeur de la propriété refconsultation.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getREFCONSULTATION() {
                    return refconsultation;
                }

                /**
                 * Définit la valeur de la propriété refconsultation.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setREFCONSULTATION(String value) {
                    this.refconsultation = value;
                }

                /**
                 * Obtient la valeur de la propriété titleconsultation.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getTITLECONSULTATION() {
                    return titleconsultation;
                }

                /**
                 * Définit la valeur de la propriété titleconsultation.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setTITLECONSULTATION(String value) {
                    this.titleconsultation = value;
                }

                /**
                 * Obtient la valeur de la propriété idconsultation.
                 */
                public long getIDCONSULTATION() {
                    return idconsultation;
                }

                /**
                 * Définit la valeur de la propriété idconsultation.
                 */
                public void setIDCONSULTATION(long value) {
                    this.idconsultation = value;
                }

                /**
                 * Obtient la valeur de la propriété datesent.
                 *
                 * @return possible object is
                 * {@link XMLGregorianCalendar }
                 */
                public XMLGregorianCalendar getDATESENT() {
                    return datesent;
                }

                /**
                 * Définit la valeur de la propriété datesent.
                 *
                 * @param value allowed object is
                 *              {@link XMLGregorianCalendar }
                 */
                public void setDATESENT(XMLGregorianCalendar value) {
                    this.datesent = value;
                }

                /**
                 * Obtient la valeur de la propriété id.
                 */
                public long getID() {
                    return id;
                }

                /**
                 * Définit la valeur de la propriété id.
                 */
                public void setID(long value) {
                    this.id = value;
                }

                /**
                 * Obtient la valeur de la propriété title.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getTITLE() {
                    return title;
                }

                /**
                 * Définit la valeur de la propriété title.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setTITLE(String value) {
                    this.title = value;
                }


                /**
                 * <p>Classe Java pour anonymous complex type.
                 *
                 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
                 *
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;choice maxOccurs="unbounded"&gt;
                 *           &lt;element name="STATUT"&gt;
                 *             &lt;complexType&gt;
                 *               &lt;complexContent&gt;
                 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                   &lt;attribute name="DATE" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
                 *                   &lt;attribute name="VALUE" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
                 *                 &lt;/restriction&gt;
                 *               &lt;/complexContent&gt;
                 *             &lt;/complexType&gt;
                 *           &lt;/element&gt;
                 *           &lt;element name="ANNONCE"&gt;
                 *             &lt;complexType&gt;
                 *               &lt;complexContent&gt;
                 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                   &lt;sequence minOccurs="0"&gt;
                 *                     &lt;element name="INFOS_PUB"&gt;
                 *                       &lt;complexType&gt;
                 *                         &lt;complexContent&gt;
                 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                             &lt;attribute name="URL_PDF" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                 *                             &lt;attribute name="URL" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                 *                             &lt;attribute name="PARUTION" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                 *                             &lt;attribute name="PAR_TYPE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                 *                             &lt;attribute name="NUM_ANN_PAR" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                 *                             &lt;attribute name="DATE_PUB" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
                 *                           &lt;/restriction&gt;
                 *                         &lt;/complexContent&gt;
                 *                       &lt;/complexType&gt;
                 *                     &lt;/element&gt;
                 *                   &lt;/sequence&gt;
                 *                   &lt;attribute name="ID_JO" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                 *                   &lt;attribute name="ID_ANN" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                 *                   &lt;attribute name="FICHIER" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                 *                   &lt;attribute name="ETAT" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                 *                   &lt;attribute name="DATE_ENVOI" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
                 *                 &lt;/restriction&gt;
                 *               &lt;/complexContent&gt;
                 *             &lt;/complexType&gt;
                 *           &lt;/element&gt;
                 *         &lt;/choice&gt;
                 *       &lt;/sequence&gt;
                 *       &lt;attribute name="CODE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {"statut", "annonce"})
                @ToString
                @EqualsAndHashCode
                public static class SUPPORT {

                    @XmlElement(name = "STATUT")
                    @JsonProperty("STATUT")
                    protected List<STATUT> statut;
                    @XmlElement(name = "ANNONCE")
                    @JsonProperty("ANNONCE")
                    protected List<STATUT> annonce;
                    @JsonProperty("CODE")
                    @XmlAttribute(name = "CODE", required = true)
                    protected String code;

                    public List<STATUT> getStatut() {
                        return statut;
                    }

                    public void setStatut(List<STATUT> statut) {
                        this.statut = statut;
                    }

                    public List<STATUT> getAnnonce() {
                        return annonce;
                    }

                    public void setAnnonce(List<STATUT> annonce) {
                        this.annonce = annonce;
                    }

                    /**
                     * Obtient la valeur de la propriété code.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getCODE() {
                        return code;
                    }

                    /**
                     * Définit la valeur de la propriété code.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setCODE(String value) {
                        this.code = value;
                    }


                    /**
                     * <p>Classe Java pour anonymous complex type.
                     *
                     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
                     *
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;sequence minOccurs="0"&gt;
                     *         &lt;element name="INFOS_PUB"&gt;
                     *           &lt;complexType&gt;
                     *             &lt;complexContent&gt;
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *                 &lt;attribute name="URL_PDF" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                     *                 &lt;attribute name="URL" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                     *                 &lt;attribute name="PARUTION" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                     *                 &lt;attribute name="PAR_TYPE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                     *                 &lt;attribute name="NUM_ANN_PAR" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                     *                 &lt;attribute name="DATE_PUB" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
                     *               &lt;/restriction&gt;
                     *             &lt;/complexContent&gt;
                     *           &lt;/complexType&gt;
                     *         &lt;/element&gt;
                     *       &lt;/sequence&gt;
                     *       &lt;attribute name="ID_JO" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                     *       &lt;attribute name="ID_ANN" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                     *       &lt;attribute name="FICHIER" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                     *       &lt;attribute name="ETAT" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                     *       &lt;attribute name="DATE_ENVOI" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {"infospub"})
                    @ToString
                    @EqualsAndHashCode
                    public static class ANNONCE {

                        @XmlElement(name = "INFOS_PUB")
                        @JsonProperty("INFOS_PUB")
                        protected INFOSPUB infospub;
                        @JsonProperty("ID_JO")
                        @XmlAttribute(name = "ID_JO", required = true)
                        protected String idjo;
                        @XmlAttribute(name = "ID_ANN", required = true)
                        @JsonProperty("ID_ANN")
                        protected String idann;
                        @XmlAttribute(name = "FICHIER", required = true)
                        @JsonProperty("FICHIER")
                        protected String fichier;
                        @XmlAttribute(name = "ETAT", required = true)
                        protected String etat;
                        @XmlAttribute(name = "DATE_ENVOI", required = true)
                        @JsonProperty("DATE_ENVOI")
                        @XmlSchemaType(name = "date")
                        protected XMLGregorianCalendar dateenvoi;

                        /**
                         * Obtient la valeur de la propriété infospub.
                         *
                         * @return possible object is
                         * {@link INFOSPUB }
                         */
                        public INFOSPUB getINFOSPUB() {
                            return infospub;
                        }

                        /**
                         * Définit la valeur de la propriété infospub.
                         *
                         * @param value allowed object is
                         *              {@link INFOSPUB }
                         */
                        public void setINFOSPUB(INFOSPUB value) {
                            this.infospub = value;
                        }

                        /**
                         * Obtient la valeur de la propriété idjo.
                         *
                         * @return possible object is
                         * {@link String }
                         */
                        public String getIDJO() {
                            return idjo;
                        }

                        /**
                         * Définit la valeur de la propriété idjo.
                         *
                         * @param value allowed object is
                         *              {@link String }
                         */
                        public void setIDJO(String value) {
                            this.idjo = value;
                        }

                        /**
                         * Obtient la valeur de la propriété idann.
                         *
                         * @return possible object is
                         * {@link String }
                         */
                        public String getIDANN() {
                            return idann;
                        }

                        /**
                         * Définit la valeur de la propriété idann.
                         *
                         * @param value allowed object is
                         *              {@link String }
                         */
                        public void setIDANN(String value) {
                            this.idann = value;
                        }

                        /**
                         * Obtient la valeur de la propriété fichier.
                         *
                         * @return possible object is
                         * {@link String }
                         */
                        public String getFICHIER() {
                            return fichier;
                        }

                        /**
                         * Définit la valeur de la propriété fichier.
                         *
                         * @param value allowed object is
                         *              {@link String }
                         */
                        public void setFICHIER(String value) {
                            this.fichier = value;
                        }

                        /**
                         * Obtient la valeur de la propriété etat.
                         *
                         * @return possible object is
                         * {@link String }
                         */
                        public String getETAT() {
                            return etat;
                        }

                        /**
                         * Définit la valeur de la propriété etat.
                         *
                         * @param value allowed object is
                         *              {@link String }
                         */
                        public void setETAT(String value) {
                            this.etat = value;
                        }

                        /**
                         * Obtient la valeur de la propriété dateenvoi.
                         *
                         * @return possible object is
                         * {@link XMLGregorianCalendar }
                         */
                        public XMLGregorianCalendar getDATEENVOI() {
                            return dateenvoi;
                        }

                        /**
                         * Définit la valeur de la propriété dateenvoi.
                         *
                         * @param value allowed object is
                         *              {@link XMLGregorianCalendar }
                         */
                        public void setDATEENVOI(XMLGregorianCalendar value) {
                            this.dateenvoi = value;
                        }


                        /**
                         * <p>Classe Java pour anonymous complex type.
                         *
                         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
                         *
                         * <pre>
                         * &lt;complexType&gt;
                         *   &lt;complexContent&gt;
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                         *       &lt;attribute name="URL_PDF" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                         *       &lt;attribute name="URL" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                         *       &lt;attribute name="PARUTION" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                         *       &lt;attribute name="PAR_TYPE" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                         *       &lt;attribute name="NUM_ANN_PAR" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                         *       &lt;attribute name="DATE_PUB" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
                         *     &lt;/restriction&gt;
                         *   &lt;/complexContent&gt;
                         * &lt;/complexType&gt;
                         * </pre>
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        @ToString
                        @EqualsAndHashCode
                        public static class INFOSPUB {

                            @XmlAttribute(name = "URL_PDF", required = true)
                            @JsonProperty("URL_PDF")
                            protected String urlpdf;
                            @XmlAttribute(name = "URL", required = true)
                            @JsonProperty("URL")
                            protected String url;
                            @XmlAttribute(name = "PARUTION", required = true)
                            @JsonProperty("PARUTION")
                            protected String parution;
                            @XmlAttribute(name = "PAR_TYPE", required = true)
                            @JsonProperty("PAR_TYPE")
                            protected String partype;
                            @XmlAttribute(name = "NUM_ANN_PAR", required = true)
                            @JsonProperty("NUM_ANN_PAR")
                            protected String numannpar;
                            @XmlAttribute(name = "DATE_PUB", required = true)
                            @JsonProperty("DATE_PUB")
                            @XmlSchemaType(name = "date")
                            protected XMLGregorianCalendar datepub;

                            /**
                             * Obtient la valeur de la propriété urlpdf.
                             *
                             * @return possible object is
                             * {@link String }
                             */
                            public String getURLPDF() {
                                return urlpdf;
                            }

                            /**
                             * Définit la valeur de la propriété urlpdf.
                             *
                             * @param value allowed object is
                             *              {@link String }
                             */
                            public void setURLPDF(String value) {
                                this.urlpdf = value;
                            }

                            /**
                             * Obtient la valeur de la propriété url.
                             *
                             * @return possible object is
                             * {@link String }
                             */
                            public String getURL() {
                                return url;
                            }

                            /**
                             * Définit la valeur de la propriété url.
                             *
                             * @param value allowed object is
                             *              {@link String }
                             */
                            public void setURL(String value) {
                                this.url = value;
                            }

                            /**
                             * Obtient la valeur de la propriété parution.
                             *
                             * @return possible object is
                             * {@link String }
                             */
                            public String getPARUTION() {
                                return parution;
                            }

                            /**
                             * Définit la valeur de la propriété parution.
                             *
                             * @param value allowed object is
                             *              {@link String }
                             */
                            public void setPARUTION(String value) {
                                this.parution = value;
                            }

                            /**
                             * Obtient la valeur de la propriété partype.
                             *
                             * @return possible object is
                             * {@link String }
                             */
                            public String getPARTYPE() {
                                return partype;
                            }

                            /**
                             * Définit la valeur de la propriété partype.
                             *
                             * @param value allowed object is
                             *              {@link String }
                             */
                            public void setPARTYPE(String value) {
                                this.partype = value;
                            }

                            /**
                             * Obtient la valeur de la propriété numannpar.
                             *
                             * @return possible object is
                             * {@link String }
                             */
                            public String getNUMANNPAR() {
                                return numannpar;
                            }

                            /**
                             * Définit la valeur de la propriété numannpar.
                             *
                             * @param value allowed object is
                             *              {@link String }
                             */
                            public void setNUMANNPAR(String value) {
                                this.numannpar = value;
                            }

                            /**
                             * Obtient la valeur de la propriété datepub.
                             *
                             * @return possible object is
                             * {@link XMLGregorianCalendar }
                             */
                            public XMLGregorianCalendar getDATEPUB() {
                                return datepub;
                            }

                            /**
                             * Définit la valeur de la propriété datepub.
                             *
                             * @param value allowed object is
                             *              {@link XMLGregorianCalendar }
                             */
                            public void setDATEPUB(XMLGregorianCalendar value) {
                                this.datepub = value;
                            }

                        }

                    }


                    /**
                     * <p>Classe Java pour anonymous complex type.
                     *
                     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
                     *
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;attribute name="DATE" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
                     *       &lt;attribute name="VALUE" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    @ToString
                    @EqualsAndHashCode
                    public static class STATUT {

                        @XmlAttribute(name = "DATE", required = true)
                        @XmlSchemaType(name = "dateTime")
                        @JsonProperty("DATE")
                        protected XMLGregorianCalendar date;
                        @XmlAttribute(name = "VALUE", required = true)
                        @JsonProperty("VALUE")
                        protected BigDecimal value;

                        /**
                         * Obtient la valeur de la propriété date.
                         *
                         * @return possible object is
                         * {@link XMLGregorianCalendar }
                         */
                        public XMLGregorianCalendar getDATE() {
                            return date;
                        }

                        /**
                         * Définit la valeur de la propriété date.
                         *
                         * @param value allowed object is
                         *              {@link XMLGregorianCalendar }
                         */
                        public void setDATE(XMLGregorianCalendar value) {
                            this.date = value;
                        }

                        /**
                         * Obtient la valeur de la propriété value.
                         *
                         * @return possible object is
                         * {@link BigDecimal }
                         */
                        public BigDecimal getVALUE() {
                            return value;
                        }

                        /**
                         * Définit la valeur de la propriété value.
                         *
                         * @param value allowed object is
                         *              {@link BigDecimal }
                         */
                        public void setVALUE(BigDecimal value) {
                            this.value = value;
                        }

                    }

                }

            }

        }

    }

}
