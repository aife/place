package fr.atexo.annonces.mapper;

import fr.atexo.annonces.boot.entity.EFormsSurcharge;
import fr.atexo.annonces.config.DaoMapper;
import fr.atexo.annonces.dto.EformsSurchargeDTO;
import org.mapstruct.Mapper;

@Mapper
public interface EformsSurchargeMapper extends DaoMapper<EFormsSurcharge, EformsSurchargeDTO> {


}
