package fr.atexo.annonces.dto;

import lombok.*;

import java.time.Instant;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContexteDTO {

    private Integer id;
    private UUID uuid;
    private Instant creation;
    private Instant modification;
    private Set<AcheteurDTO> acheteurs;

}
