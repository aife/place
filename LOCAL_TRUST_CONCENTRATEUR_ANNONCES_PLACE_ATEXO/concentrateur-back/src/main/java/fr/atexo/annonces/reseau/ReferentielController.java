package fr.atexo.annonces.reseau;

import fr.atexo.annonces.dto.AgentDTO;
import fr.atexo.annonces.dto.OrganismeDTO;
import fr.atexo.annonces.dto.ReferentielDTO;
import fr.atexo.annonces.service.ReferentielMpeService;
import fr.atexo.annonces.service.ReferentielService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Point d'entrée web pour manipuler les supports
 */
@RestController
@RequestMapping("/rest/v2/referentiels")
@Tag(name = "Referentiels")
public class ReferentielController {
    private final ReferentielMpeService referentielMpeService;
    private final ReferentielService referentielService;

    public ReferentielController(ReferentielMpeService referentielMpeService, ReferentielService referentielService) {
        this.referentielMpeService = referentielMpeService;
        this.referentielService = referentielService;
    }

    @GetMapping("procedures")
    public List<ReferentielDTO> getProcedure(@RequestParam String idPlatform) {
        return referentielService.getProcedure(idPlatform);
    }

    @GetMapping("organismes")
    public List<OrganismeDTO> getOrganismes(@RequestParam String idPlatform) {
        return referentielMpeService.getOrganismes(idPlatform);
    }

    @GetMapping("agents")
    public List<AgentDTO> getAgents(@RequestParam String idPlatform) {
        return referentielMpeService.getAgents(idPlatform);
    }
}
