package fr.atexo.annonces.service.shared.kafka.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;

@ConditionalOnProperty(value = "kafka.enabled")
@Configuration
@EnableKafka
@RequiredArgsConstructor
@Slf4j
public class KafkaConfig {

    @Value(value = "${kafka.broker}")
    private String bootstrapAddress;

    @Value(value = "${kafka.topic.contrat.reception.groupe:exec}")
    private String contratGroupeReception;

    private final KafkaSecurityConf kafkaSecurityConf;
    @Value(value = "${kafka.security.enabled}")
    private boolean securityEnabled;

    @Bean
    public ConsumerFactory<String, String> tncpConsumerFactory() {
        var config = new HashMap<String, Object>();
        if (securityEnabled) {
            log.info("Kafka Security is enabled");
            config.putAll(kafkaSecurityConf.getKafkaSecurityProperties());
            log.info("Kafka Security Properties pour le consommateur: {}", config);
        }

        // Adding the Configuration
        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        config.put(ConsumerConfig.GROUP_ID_CONFIG, contratGroupeReception);
        config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        config.put(JsonDeserializer.TRUSTED_PACKAGES, "fr.atexo.execution");
        config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);

        return new DefaultKafkaConsumerFactory<>(config);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory tncpKafkaListenerContainerFactory(ConsumerFactory<String, String> tncpConsumerFactory) {
        var factory = new ConcurrentKafkaListenerContainerFactory<String, String>();
        factory.setConsumerFactory(tncpConsumerFactory);
        return factory;
    }
}
