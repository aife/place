package fr.atexo.annonces.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AcheteurChoixDTO {

    private String login;
    private String email;

}
