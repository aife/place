package fr.atexo.annonces.dto;

import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;


@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PieceJointeDTO implements Serializable {

    private static final long serialVersionUID = -5416829527656795263L;
    private Long id;

    private String name;

    private String path;

    private String contentType;

    private ZonedDateTime creationDate;

    private Long size;

}
