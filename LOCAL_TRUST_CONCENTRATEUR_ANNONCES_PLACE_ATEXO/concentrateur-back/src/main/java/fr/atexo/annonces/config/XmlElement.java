package fr.atexo.annonces.config;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class XmlElement {
    private String elementName;
    private List<String> properties;
}
