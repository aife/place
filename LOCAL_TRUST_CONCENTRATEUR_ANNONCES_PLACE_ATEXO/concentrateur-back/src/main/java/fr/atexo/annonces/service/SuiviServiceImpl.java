package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.entity.tncp.SuiviAvisPublie;
import fr.atexo.annonces.boot.repository.tncp.SuiviRepository;
import fr.atexo.annonces.dto.SuiviDTO;
import fr.atexo.annonces.mapper.SuiviAvisPublieMapper;
import fr.atexo.annonces.service.exception.SuiviIntrouvableException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class SuiviServiceImpl implements SuiviService {

    @NonNull
    private SuiviRepository repository;
    @NonNull
    private SuiviAvisPublieMapper suiviMapper;


    @Override
    public SuiviDTO sauvegarder(SuiviAvisPublie suivi) {
        return suiviMapper.mapToModel(repository.save(suivi));
    }

    @Override
    public SuiviAvisPublie recuperer(String uuid) {
        return this.rechercher(uuid).orElseThrow(() -> new SuiviIntrouvableException(uuid));
    }

    @Override
    public Optional<SuiviAvisPublie> rechercher(String uuid) {
        return repository.findByUuid(uuid);
    }
}
