package fr.atexo.annonces.boot.controller;


import fr.atexo.annonces.boot.entity.OauthClientDetails;
import fr.atexo.annonces.boot.repository.OauthClientDetailsRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class AuthService {

    private final OauthClientDetailsRepository oauthClientDetailsRepository;
    @Value("${annonces.admin.password}")
    private String password;
    @Value("${annonces.admin.login}")
    private String login;
    @Value("${annonces.admin.plateforme}")
    private String plateforme;

    public AuthService(OauthClientDetailsRepository oauthClientDetailsRepository) {
        this.oauthClientDetailsRepository = oauthClientDetailsRepository;
    }

    @PostConstruct
    public void initialize() {
        OauthClientDetails oauthClientDetails = oauthClientDetailsRepository.findById(login).orElse(null);
        if (oauthClientDetails == null) {
            oauthClientDetails = new OauthClientDetails();
        }
        oauthClientDetails.setClientId(login);
        oauthClientDetails.setAccessTokenValidity(18000);
        oauthClientDetails.setRefreshTokenValidity(72000);
        oauthClientDetails.setScope("read,write,platform:" + plateforme + ",consultation,validation:eco,validation:sip");
        oauthClientDetails.setAuthorizedGrantTypes("client_credentials");
        String salt = BCrypt.gensalt();
        String loginHash = BCrypt.hashpw(password, salt);
        oauthClientDetails.setClientSecret("{bcrypt}" + loginHash);
        oauthClientDetailsRepository.save(oauthClientDetails);
    }


}
