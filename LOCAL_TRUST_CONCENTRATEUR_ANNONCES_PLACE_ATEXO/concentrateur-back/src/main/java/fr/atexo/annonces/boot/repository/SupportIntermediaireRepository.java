package fr.atexo.annonces.boot.repository;

import fr.atexo.annonces.boot.entity.SupportIntermediaire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * Repository pour accéder aux objets Support
 */
@Repository
public interface SupportIntermediaireRepository extends JpaRepository<SupportIntermediaire, Integer>, JpaSpecificationExecutor<SupportIntermediaire> {

    SupportIntermediaire findByIdPlateformeAndSupportCode(String plateforme, String codeSupport);
}
