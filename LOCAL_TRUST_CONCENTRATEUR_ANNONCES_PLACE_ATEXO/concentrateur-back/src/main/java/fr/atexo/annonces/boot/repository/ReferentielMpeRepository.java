package fr.atexo.annonces.boot.repository;


import fr.atexo.annonces.boot.entity.ReferentielServiceMpe;
import fr.atexo.annonces.config.DaoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ReferentielMpeRepository extends DaoRepository<ReferentielServiceMpe, Integer> {

    Optional<ReferentielServiceMpe> findByCodeAndOrganismeIdAndIdMpe(String code, Integer idOrganisme, Long idMpe);

    Optional<ReferentielServiceMpe> findByCodeAndOrganismeIdAndIdMpeIsNull(String code, Integer idOrganisme);
}
