package fr.atexo.annonces.dto;

import fr.atexo.annonces.modeles.StatutTypeAvisAnnonceEnum;
import lombok.*;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SuiviTypeAvisPubDTO implements Serializable {

    private Long id;

    private Boolean validerSip;

    private Boolean validerEco;

    private Boolean invaliderEco;

    private ZonedDateTime dateInvalidationEco;

    private ZonedDateTime dateValidationEco;

    private ZonedDateTime dateValidationSip;

    private ZonedDateTime dateEnvoiNational;

    private ZonedDateTime dateEnvoiEuropeen;

    private Integer idConsultation;

    private String reference;

    private String titre;

    private Long delaiSoumission;

    private String urlEforms;

    private String objet;

    private String raisonRefus;

    private boolean europeen;

    @Enumerated(EnumType.STRING)
    private StatutTypeAvisAnnonceEnum statut;

    private ZonedDateTime dateDemandeEnvoi;

    private ZonedDateTime dlro;

    private ZonedDateTime dateMiseEnLigne;

    private ZonedDateTime dateMiseEnLigneCalcule;

    private ZonedDateTime dateCreation;

    private ZonedDateTime dateModification;

    private OrganismeResumeDTO organisme;


    private ReferentielDTO service;

    private ReferentielDTO procedure;

    private OffreDTO offreRacine;

    private ConsultationFacturationDTO facturation;

    private ZonedDateTime dateValidationNational;

    private ZonedDateTime dateValidationEuropeen;

    private AgentDTO creerPar;

    private AgentDTO modifierPar;

    private ZonedDateTime dateModificationStatut;

    private List<SuiviAnnonceDTO> annonces;

    private List<ValidationHistoriqueDTO> historiques;

    private FileStatusEnum statutEdition;

    private String statutRedaction;

    private Integer versionEdition;

    private String mailObjet;

    private String mailContenu;

    private AgentDTO validateurSip;

    private AgentDTO validateurEco;
    private ZonedDateTime dateEnvoiEuropeenPrevisionnel;
    private ZonedDateTime dateEnvoiNationalPrevisionnel;
}
