package fr.atexo.annonces.boot.repository;

import fr.atexo.annonces.boot.entity.Offre;
import fr.atexo.annonces.boot.entity.OffreTypeProcedure;
import fr.atexo.annonces.boot.entity.PlateformeConfiguration;
import fr.atexo.annonces.boot.entity.TypeProcedure;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OffreTypeProcedureRepository extends JpaRepository<OffreTypeProcedure, Integer>, JpaSpecificationExecutor<OffreTypeProcedure> {

    List<OffreTypeProcedure> findAllByAcronymeOrganismeAndProcedureAbbreviation(String acronymeOrganisme, String abbreviationProcedure);

    @Query("SELECT DISTINCT c.procedure FROM OffreTypeProcedure c WHERE c.acronymeOrganisme in :acronymeOrganisme")
    List<TypeProcedure> findAllProcedureByAcronymeOrganismeIn(@Param("acronymeOrganisme") List<String> acronymeOrganisme);

    @Query("SELECT DISTINCT c.procedure.id FROM OffreTypeProcedure c WHERE c.procedure.abbreviation in :typeProcedureAbbreviation")
    List<Integer> findAllIdByProcedureAbbreviation(@Param("typeProcedureAbbreviation") List<String> typeProcedureAbbreviation);

    OffreTypeProcedure findByOffreAndProcedureAndAcronymeOrganismeAndPlateformeConfiguration(Offre offre, TypeProcedure procedure, String organisme, PlateformeConfiguration plateformeConfiguration);

    List<OffreTypeProcedure> findAllByPlateformeConfiguration(PlateformeConfiguration plateformeConfiguration);

    void deleteAllByPlateformeConfiguration(PlateformeConfiguration plateformeConfiguration);

    void deleteAllByPlateformeConfigurationIsNull();

    List<OffreTypeProcedure> findAllAllByPlateformeConfigurationIsNull();
}
