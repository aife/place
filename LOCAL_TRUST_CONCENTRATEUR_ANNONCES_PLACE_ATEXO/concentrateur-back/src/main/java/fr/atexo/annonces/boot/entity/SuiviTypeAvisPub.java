package fr.atexo.annonces.boot.entity;

import fr.atexo.annonces.dto.FileStatusEnum;
import fr.atexo.annonces.modeles.StatutTypeAvisAnnonceEnum;
import lombok.*;
import org.hibernate.envers.Audited;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;


@Table(name = "suivi_type_avis_pub")
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter
@Getter
@Entity
@EqualsAndHashCode
@EntityListeners(AuditingEntityListener.class)
public class SuiviTypeAvisPub implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "VALIDER_SIP")
    private Boolean validerSip;

    @Column(name = "VALIDER_ECO")
    private Boolean validerEco;

    @Column(name = "INVALIDER_ECO")
    private Boolean invaliderEco;

    @Column(name = "DATE_VALIDATION")
    private ZonedDateTime dateValidation;

    @Column(name = "DATE_INVALIDATION_ECO")
    private ZonedDateTime dateInvalidationEco;

    @Column(name = "DATE_VALIDATION_ECO")
    private ZonedDateTime dateValidationEco;

    @Column(name = "DATE_VALIDATION_SIP")
    private ZonedDateTime dateValidationSip;

    @Column(name = "DATE_ENVOI_NATIONAL")
    @Audited(withModifiedFlag = true)
    private ZonedDateTime dateEnvoiNational;

    @Column(name = "DATE_ENVOI_EUROPEEN")
    @Audited(withModifiedFlag = true)
    private ZonedDateTime dateEnvoiEuropeen;

    @Column(name = "ID_CONSULTATION")
    private Integer idConsultation;

    @Column(name = "REFERENCE")
    private String reference;
    @Column(name = "TITRE")
    @Lob
    private String titre;
    @Column(name = "OBJET")
    @Lob
    private String objet;

    @Column(name = "RAISON_REFUS")
    @Lob
    @Audited(withModifiedFlag = true)
    private String raisonRefus;

    @Column(name = "EU", nullable = false)
    private boolean europeen;

    @Column(name = "STATUT", nullable = false)
    @Enumerated(EnumType.STRING)
    @Audited(withModifiedFlag = true)
    private StatutTypeAvisAnnonceEnum statut;

    @Column(name = "STATUT_EDITION")
    @Enumerated(EnumType.STRING)
    @Audited(withModifiedFlag = true)
    private FileStatusEnum statutEdition;

    @Column(name = "STATUT_REDACTION")
    @Audited(withModifiedFlag = true)
    private String statutRedaction;

    @Column(name = "TOKEN_EDITION")
    @Lob
    private String tokenEdition;

    @Column(name = "VERSION_EDITION")
    private Integer versionEdition;

    @Column(name = "DATE_DEMANDE_ENVOI")
    private ZonedDateTime dateDemandeEnvoi;

    @Column(name = "DATE_MODIFICATION_STATUT")
    @Audited(withModifiedFlag = true)
    private ZonedDateTime dateModificationStatut;

    @Column(name = "DLRO")
    private ZonedDateTime dlro;
    @Column(name = "DATE_MISE_EN_LIGNE")
    @Audited(withModifiedFlag = true)
    private ZonedDateTime dateMiseEnLigne;

    @Column(name = "DATE_MISE_EN_LIGNE_CALCULE")
    private ZonedDateTime dateMiseEnLigneCalcule;

    @Column(name = "DATE_VALIDATION_NATIONAL")
    @Audited(withModifiedFlag = true)
    private ZonedDateTime dateValidationNational;

    @Column(name = "DATE_VALIDATION_EUROPEEN")
    @Audited(withModifiedFlag = true)
    private ZonedDateTime dateValidationEuropeen;

    @Column(name = "DATE_CREATION", nullable = false)
    @CreatedDate
    private ZonedDateTime dateCreation;

    @Column(name = "DATE_MODIFICATION", nullable = false)
    @Audited
    @LastModifiedDate
    private ZonedDateTime dateModification;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_CREER_PAR")
    @CreatedBy
    private Agent creerPar;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_MODIFIER_PAR")
    @Audited(targetAuditMode = NOT_AUDITED)
    @LastModifiedBy
    private Agent modifierPar;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_VALIDATEUR")
    private Agent validateur;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_VALIDATEUR_SIP")
    private Agent validateurSip;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_VALIDATEUR_ECO")
    private Agent validateurEco;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_INVALIDATEUR_ECO")
    private Agent invalidateurEco;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_ORGANISME")
    private Organisme organisme;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PROCEDURE")
    private TypeProcedure procedure;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_SERVICE")
    private ReferentielServiceMpe service;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_FACTURATION", referencedColumnName = "ID")
    private ConsultationFacturation facturation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_OFFRE_RACINE", referencedColumnName = "ID")
    private Offre offreRacine;

    @OneToMany(mappedBy = "typeAvisPub", fetch = FetchType.LAZY)
    private List<SuiviAnnonce> annonces;

    @Column(name = "MAIL_OBJET")
    @Audited(withModifiedFlag = true)
    private String mailObjet;

    @Column(name = "MAIL_CONTENU")
    @Lob
    @Audited(withModifiedFlag = true)
    private String mailContenu;

    @OneToMany(mappedBy = "suiviTypeAvisPub", fetch = FetchType.LAZY)
    private List<SuiviTypeAvisPubPieceJointeAssoEntity> mailPiecejointeList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PARENT", referencedColumnName = "ID")
    private SuiviTypeAvisPub parent;

    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
    private List<SuiviTypeAvisPub> children;

    @Column(name = "ID_MPE_OLD")
    private String idMpeOld;

    @Column(name = "DATE_ENVOI_NATIONAL_PREVISIONNEL")
    private ZonedDateTime dateEnvoiNationalPrevisionnel;

    @Column(name = "DATE_ENVOI_EUROPEEN_PREVISIONNEL")
    private ZonedDateTime dateEnvoiEuropeenPrevisionnel;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_AVIS_PORTAIL", referencedColumnName = "ID")
    private PieceJointe avisPortail;

    public List<SuiviAnnonce> getNationalAnnonces() {
        if (CollectionUtils.isEmpty(annonces)) return new ArrayList<>();
        return annonces.stream().filter(annonce -> annonce.getSupport() != null && !annonce.getSupport().isEuropeen()).toList();
    }

    public SuiviAnnonce getEuropeenAnnonce() {
        if (CollectionUtils.isEmpty(annonces)) return null;
        return annonces.stream().filter(annonce -> annonce.getSupport() != null && annonce.getSupport().isEuropeen()).findFirst().orElse(null);
    }

    public PieceJointe getAvisNational() {
        return getNationalAnnonces().stream().map(SuiviAnnonce::getAvisFichier).filter(Objects::nonNull).findFirst().orElse(null);
    }


}
