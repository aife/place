package fr.atexo.annonces.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public final class PageDTO<T> {
    private boolean first;
    private boolean last;
    private int totalPages;
    private long totalElements;
    private int numberOfElements;
    private int size;
    private List<T> content;

}
