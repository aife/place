package fr.atexo.annonces.boot.ws.validator;


import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@ConfigurationProperties("external-apis.validator")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ValidatorConfiguration {
    private String basePath;
    private String apiKey;
    private Map<String, String> ws;

}
