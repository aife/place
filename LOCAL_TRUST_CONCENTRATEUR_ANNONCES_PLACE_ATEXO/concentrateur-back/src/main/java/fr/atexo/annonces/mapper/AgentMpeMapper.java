package fr.atexo.annonces.mapper;

import com.atexo.annonces.commun.mpe.EnvoiMpe;
import fr.atexo.annonces.boot.entity.Agent;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = {StringAndListStringMapper.class})
public interface AgentMpeMapper {

    @Mapping(target = "identifiant", source = "agent.login")
    @Mapping(target = "nom", source = "agent.nom")
    @Mapping(target = "prenom", source = "agent.prenom")
    @Mapping(target = "idMpe", source = "agent.id")
    @Mapping(target = "acheteurPublic", source = "agent.nomCourantAcheteurPublic")
    @Mapping(target = "mpeToken", source = "agent.api.token")
    @Mapping(target = "mpeRefreshToken", source = "agent.api.refreshToken")
    @Mapping(target = "email", source = "agent.email")
    @Mapping(target = "fax", source = "agent.fax")
    @Mapping(target = "telephone", source = "agent.telephone")
    Agent mapToEntity(EnvoiMpe agentMpe);

    @Mapping(target = "identifiant", source = "agent.login")
    @Mapping(target = "nom", source = "agent.nom")
    @Mapping(target = "prenom", source = "agent.prenom")
    @Mapping(target = "idMpe", source = "agent.id")
    @Mapping(target = "acheteurPublic", source = "agent.nomCourantAcheteurPublic")
    @Mapping(target = "mpeToken", source = "agent.api.token")
    @Mapping(target = "mpeRefreshToken", source = "agent.api.refreshToken")
    @Mapping(target = "email", source = "agent.email")
    @Mapping(target = "fax", source = "agent.fax")
    @Mapping(target = "telephone", source = "agent.telephone")
    void mapToEntity(EnvoiMpe agentMpe, @MappingTarget Agent agent);
}
