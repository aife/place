package fr.atexo.annonces.service;


import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.StringWriter;
import java.security.SignatureException;

@Service
@Slf4j
@RequiredArgsConstructor
public class FreemarkerLocalService implements TemplateService {

    private Configuration configuration;


    @Autowired
    public FreemarkerLocalService(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public <T> String merge(String name, T data) throws SignatureException {
        var out = new StringWriter();

        try {
            var template = configuration.getTemplate(name);

            template.process(data, out);
        } catch (IOException | TemplateException e) {
            throw new SignatureException(name, e);
        }

        return out.toString();
    }
}
