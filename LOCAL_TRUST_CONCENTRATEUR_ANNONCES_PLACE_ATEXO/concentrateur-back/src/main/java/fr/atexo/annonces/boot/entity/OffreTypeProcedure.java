package fr.atexo.annonces.boot.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "offre_type_procedure")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OffreTypeProcedure implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_TYPE_PROCEDURE")
    private TypeProcedure procedure;

    @Column(name = "ACRONYME_ORGANISME", length = 50)
    private String acronymeOrganisme;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_OFFRE")
    private Offre offre;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PLATEFORME")
    private PlateformeConfiguration plateformeConfiguration;


}
