package fr.atexo.annonces.dto;

/**
 * DTO - Représente un département du référentiel en base de données
 */
public class DepartementDTO {

    private String code;
    private String libelle;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
