package fr.atexo.annonces.boot.entity;

import fr.atexo.annonces.boot.entity.tncp.SuiviAvisPublie;
import fr.atexo.annonces.dto.FileStatusEnum;
import fr.atexo.annonces.modeles.StatutSuiviAnnonceEnum;
import fr.atexo.annonces.modeles.SupportPublicationEnum;
import lombok.*;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.List;


@Table(name = "suivi_annonces")
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter
@Getter
@Entity
@EqualsAndHashCode(of = {"idAnnonce", "idConsultation", "idPlatform"})
@EntityListeners(AuditingEntityListener.class)
public class SuiviAnnonce implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private int id;

    @Column(name = "NUMERO_AVIS")
    private String numeroAvis;

    @Column(name = "ID_ANNONCE")
    @Type(type = "text")
    private String idAnnonce;

    @Column(name = "SIMPLIFIE")
    private boolean simplifie;


    @Column(name = "SUPPORT_PUBLICATION")
    @Enumerated(EnumType.STRING)
    private SupportPublicationEnum supportPublication;

    @Column(name = "STATUT", nullable = false)
    @Enumerated(EnumType.STRING)
    private StatutSuiviAnnonceEnum statut;

    @Column(name = "STATUT_JOUE")
    private String statutJoue;

    @Column(name = "STATUT_BOAMP")
    private String statutBoamp;

    @Column(name = "MESSAGE_STATUT")
    @Type(type = "text")
    private String messageStatut;

    @Column(name = "XML")
    @Type(type = "text")
    @Basic(fetch = FetchType.LAZY)
    private String xml;

    @Column(name = "XML_TRANSFORM")
    @Type(type = "text")
    @Basic(fetch = FetchType.LAZY)
    private String xmlTransform;

    @Column(name = "XML_TRANSACTION")
    @Type(type = "text")
    @Basic(fetch = FetchType.LAZY)
    private String xmlTransaction;

    @Column(name = "NUMERO_JOUE")
    private String numeroJoue;

    @Column(name = "LIEN_PUBLICATION")
    private String lienPublication;

    @Column(name = "LIEN_PUBLICATION_TED")
    private String lienPublicationTed;

    @Column(name = "LANGUE")
    private String langue;

    @Column(name = "OFFRE")
    private String offrePublication;

    @Column(name = "ID_CONSULTATION")
    private Integer idConsultation;

    @Column(name = "DATE_DEMANDE_ENVOI")
    private Instant dateDemandeEnvoi;

    @Column(name = "DATE_DEBUT_ENVOI")
    private Instant dateDebutEnvoi;

    @Column(name = "DATE_SOUMISSION")
    private Instant dateSoumission;

    @Column(name = "DATE_SOUMISSION_TED")
    private Instant dateSoumissionTed;

    @Column(name = "DATE_ENVOI")
    private Instant dateEnvoi;

    @Column(name = "DATE_PUBLICATION")
    private Instant datePublication;

    @Column(name = "DATE_PUBLICATION_TED")
    private Instant datePublicationTed;
    @Column(name = "ID_PLATFORM")
    private String idPlatform;

    @Column(name = "ORGANISME")
    @Type(type = "text")
    private String organisme;


    @Column(name = "XML_CONCENTRATEUR")
    @Type(type = "text")
    @Basic(fetch = FetchType.LAZY)
    private String xmlConcentrateur;

    @Column(name = "PRIX_SIMPLIFIE")
    private Double prixSimplifie;

    @Column(name = "PRIX_STANDARD")
    private Double prixStandard;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_OFFRE", referencedColumnName = "ID")
    private Offre offre;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "ID_SUPPORT", referencedColumnName = "ID", nullable = false)
    private Support support;

    @Column(name = "DATE_CREATION", nullable = false)
    @CreatedDate
    private ZonedDateTime dateCreation;

    @Column(name = "DATE_MODIFICATION", nullable = false)
    @LastModifiedDate
    private ZonedDateTime dateModification;
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "suiviAnnonce")
    private EFormsFormulaireEntity formulaire;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_AGENT")
    private Agent agent;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_ORGANISME")
    private Organisme organismeMpe;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PROCEDURE")
    private TypeProcedure procedure;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_SERVICE")
    private ReferentielServiceMpe service;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_SUIVI_TYPE_AVIS_PUB")
    private SuiviTypeAvisPub typeAvisPub;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PARENT", referencedColumnName = "ID")
    private SuiviAnnonce parent;

    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
    private List<SuiviAnnonce> children;

    @Column(name = "ID_MPE_OLD")
    private String idMpeOld;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_AVIS_FICHIER", referencedColumnName = "ID")
    private PieceJointe avisFichier;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_AVIS_FICHIER_SIMPLIFIE", referencedColumnName = "ID")
    private PieceJointe avisFichierSimplifie;


    @Column(name = "STATUT_EDITION")
    @Enumerated(EnumType.STRING)
    private FileStatusEnum statutEdition;

    @Column(name = "STATUT_REDACTION")
    private String statutRedaction;

    @Column(name = "TOKEN_EDITION")
    @Lob
    private String tokenEdition;

    @Column(name = "VERSION_EDITION")
    private Integer versionEdition;

    @Column(name = "DATE_VALIDATION")
    private ZonedDateTime dateValidation;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "suiviAnnonce")
    private List<SuiviAvisPublie> suiviList;

    @Column(name = "TENTATIVE")
    private Integer tentative;//Nombre de tentatives de publication qui ont échoué

    @Column(name = "MESSAGE_ERREUR")
    @Lob
    private String messageErreur;
}
