package fr.atexo.annonces.boot.ws.docgen;

import fr.atexo.annonces.dto.DocumentEditorRequest;
import fr.atexo.annonces.dto.ValidationDocumentEditorRequest;
import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class DocGenWS {
    private final RestTemplate atexoRestTemplate;


    private final String docgenBaseUrl;

    private final String generationWs;

    private final String editionRequestWs;
    private final String validationRequestWs;
    private final String documentStatusWs;
    private final String conversionWs;
    private static final String MESSAGE = "Le fichier ne peut pas être null";

    public DocGenWS(RestTemplate atexoRestTemplate, @Value("${external-apis.doc-gen.ws.generation-doc}") String generationWs, @Value("${external-apis.doc-gen.base-path}") String docgenBaseUrl, @Value("${external-apis.doc-gen.ws.edition-request}") String editionRequestWs, @Value("${external-apis.doc-gen.ws.validation-request}") String validationRequestWs, @Value("${external-apis.doc-gen.ws.document-status}") String documentStatusWs, @Value("${external-apis.doc-gen.ws.conversion}") String conversionWs) {
        this.atexoRestTemplate = atexoRestTemplate;
        this.docgenBaseUrl = docgenBaseUrl;
        this.generationWs = generationWs;
        this.editionRequestWs = editionRequestWs;
        this.validationRequestWs = validationRequestWs;
        this.documentStatusWs = documentStatusWs;
        this.conversionWs = conversionWs;
    }

    public Resource generateDocx(List<KeyValueRequest> map, File ressource, boolean convertToPdf, String defaultOnNull) {
        if (ressource == null || defaultOnNull == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, MESSAGE);
        }
        if (map == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "La liste des parametres ne doit pas être null");
        }
        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("template", new FileSystemResource(ressource));
        final HttpHeaders theJsonHeader = new HttpHeaders();
        theJsonHeader.setContentType(MediaType.APPLICATION_JSON);
        bodyMap.add("keyValues", new HttpEntity<>(map, theJsonHeader));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);

        Map<String, String> urlParameters = new HashMap<>();
        urlParameters.put("convertToPdf", String.valueOf(convertToPdf));
        urlParameters.put("defaultOnNull", defaultOnNull);
        urlParameters.put("dateFormat", "dd/MM/yyyy HH:mm");
        return atexoRestTemplate.postForObject(docgenBaseUrl + generationWs, requestEntity, Resource.class, urlParameters);
    }


    public String getEditionToken(File document, DocumentEditorRequest editorRequest, String extension) throws IOException {
        if (document == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, MESSAGE);
        }
        if (editorRequest == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Les parametres d'édition en ligne ne doivent pas être null");
        }
        if (!"docx".equals(extension) && !"xlsx".equals(extension) && !"pptx".equals(extension)) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Les extensions éligibles pour l'édition sont pptx, xlsx, docx");
        }
        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        File tmp = File.createTempFile("edit-", "." + extension);
        Files.deleteIfExists(tmp.toPath());
        Files.copy(document.toPath(), tmp.toPath());
        bodyMap.add("file", new FileSystemResource(tmp));
        final HttpHeaders theJsonHeader = new HttpHeaders();
        theJsonHeader.setContentType(MediaType.APPLICATION_JSON);
        bodyMap.add("editorRequest", new HttpEntity<>(editorRequest, theJsonHeader));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> body = new HttpEntity<>(bodyMap, headers);
        String result = atexoRestTemplate.postForObject(docgenBaseUrl + editionRequestWs, body, String.class);
        Files.deleteIfExists(tmp.toPath());
        return result;
    }

    public String getValidationToken(File document, ValidationDocumentEditorRequest editorRequest, String extension) throws IOException {
        if (document == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, MESSAGE);
        }
        if (editorRequest == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Les parametres d'édition en ligne ne doivent pas être null");
        }
        if (!"docx".equals(extension) && !"xlsx".equals(extension) && !"pptx".equals(extension)) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Les extensions éligibles pour l'édition sont pptx, xlsx, docx");
        }
        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        File tmp = File.createTempFile("edit-", "." + extension);
        Files.deleteIfExists(tmp.toPath());
        Files.copy(document.toPath(), tmp.toPath());
        bodyMap.add("document", new FileSystemResource(tmp));
        final HttpHeaders theJsonHeader = new HttpHeaders();
        theJsonHeader.setContentType(MediaType.APPLICATION_JSON);
        bodyMap.add("editorRequest", new HttpEntity<>(editorRequest, theJsonHeader));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> body = new HttpEntity<>(bodyMap, headers);
        String result = atexoRestTemplate.postForObject(docgenBaseUrl + validationRequestWs, body, String.class);
        Files.deleteIfExists(tmp.toPath());
        return result;
    }

    public ResponseEntity<Resource> getDocument(String token) {
        if (token == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Le token ne peut pas être null");
        }
        Map<String, String> urlParameters = new HashMap<>();
        urlParameters.put("token", token);
        return atexoRestTemplate.getForEntity(docgenBaseUrl + documentStatusWs, Resource.class, urlParameters);
    }


    public Resource convertToPDF(File generatedFile, String extension) {
        if (generatedFile == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, MESSAGE);
        }
        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        try {

            File tmp = File.createTempFile("edit-", "." + extension);
            Files.deleteIfExists(tmp.toPath());
            Files.copy(generatedFile.toPath(), tmp.toPath());
            bodyMap.add("document", new FileSystemResource(tmp));
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);
            Map<String, String> urlParameters = new HashMap<>();
            urlParameters.put("outputFormat", "pdf");
            Resource result = atexoRestTemplate.postForObject(docgenBaseUrl + conversionWs, requestEntity, Resource.class, urlParameters);
            Files.deleteIfExists(tmp.toPath());
            return result;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage(), e);
        }
    }
}
