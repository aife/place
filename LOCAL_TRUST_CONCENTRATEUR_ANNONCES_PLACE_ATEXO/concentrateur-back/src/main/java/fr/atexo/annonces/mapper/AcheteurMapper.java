package fr.atexo.annonces.mapper;

import com.atexo.annonces.commun.mpe.AcheteurMpe;
import eu.europa.publications.resource.schema.ted.r2_0_9.reception.TedEsenders;
import fr.atexo.annonces.boot.entity.AcheteurEntity;
import fr.atexo.annonces.dto.AcheteurDTO;
import org.mapstruct.Mapper;

@Mapper
public interface AcheteurMapper extends Mappable<AcheteurDTO, AcheteurMpe> {

    @Override
    AcheteurMpe map(AcheteurDTO acheteur, TedEsenders tedEsenders, boolean rie);


    AcheteurEntity mapToEntity(AcheteurMpe acheteur);
    AcheteurMpe mapToModel(AcheteurEntity acheteur);


}
