package fr.atexo.annonces.mapper.tncp;

import com.atexo.annonces.commun.mpe.ConsultationMpe;
import eu.europa.publications.resource.schema.ted.r2_0_9.reception.TedEsenders;
import fr.atexo.annonces.commun.tncp.Profil;
import fr.atexo.annonces.mapper.Mappable;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProfilMapper implements Mappable<ConsultationMpe, Profil> {

    @Override
    public Profil map(ConsultationMpe consultationMpe, TedEsenders tedEsenders, boolean rie) {
        return Profil.AVIS_PUBLICITE_GESTION;
    }
}
