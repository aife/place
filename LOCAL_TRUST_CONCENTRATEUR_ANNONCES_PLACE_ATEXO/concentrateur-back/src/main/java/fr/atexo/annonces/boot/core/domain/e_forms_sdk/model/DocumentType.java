package fr.atexo.annonces.boot.core.domain.e_forms_sdk.model;


import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DocumentType {
    private String id;
    private String namespace;
    private String rootElement;
    private String schemaLocation;
    private List<Namespaces> additionalNamespaces;
}
