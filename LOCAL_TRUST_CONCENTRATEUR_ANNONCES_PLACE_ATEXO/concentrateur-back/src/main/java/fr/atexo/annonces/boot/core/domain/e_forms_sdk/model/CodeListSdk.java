package fr.atexo.annonces.boot.core.domain.e_forms_sdk.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class CodeListSdk extends SdkVersion {
    private List<Code> codelists;
}
