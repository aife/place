package fr.atexo.annonces.boot.ws.mpe.model;

import com.atexo.annonces.commun.mpe.AcheteurMpe;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class HydraAcheteurResponse {

    @JsonProperty("hydra:member")
    private List<AcheteurMpe> list;
}
