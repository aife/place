package fr.atexo.annonces.dto;

import com.atexo.annonces.commun.mpe.EnvoiMpe;
import fr.atexo.annonces.boot.ws.mpe.model.ConsultationContexte;
import fr.atexo.annonces.modeles.StatutSuiviAnnonceEnum;
import lombok.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * DTO - Représente une demande de publication
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SuiviAnnonceDTO implements Serializable {

    private Integer id;
    private Integer idParent;
    private Integer idConsultation;
    private Long idEForms;
    private Boolean eFormsValid;
    private StatutSuiviAnnonceEnum statut;
    private FileStatusEnum statutEdition;
    private String statutJoue;

    private String statutBoamp;
    private String tedVersion;
    private EnvoiMpe envoi;
    private Instant dateEnvoi;
    private Instant dateSoumission;
    private Instant dateSoumissionTed;
    private Instant dateDemandeEnvoi;
    private Instant dateDebutEnvoi;
    private Instant datePublication;
    private OffreDTO offre;
    private SupportDTO support;
    private String idPlatform;
    private String messageStatut;
    private boolean simplifie;
    private String numeroAvis;
    private String numeroDossier;
    private String numeroJoue;
    private String numeroTed;
    private String numeroAvisParent;
    private String numeroJoueParent;
    private String lienPublicationParent;
    private String lienPublication;
    private String organisme;
    private OrganismeResumeDTO organismeMpe;
    private Double prixSimplifie;
    private Double prixStandard;
    private String codeDestination;
    private ZonedDateTime dateCreation;

    private List<HistoriqueDTO> historique = new ArrayList<>();
    private ContexteDTO contexte;
    private AcheteurChoixDTO acheteur;
    private ConsultationContexte consultationContexte;

    private Boolean validerSip;
    private Boolean validerEco;
    private ZonedDateTime dateValidationEco;

    private ZonedDateTime dateValidationSip;
    private ZonedDateTime dateModification;

    private ZonedDateTime dateMiseEnLigneCalcule;

    private Long idTypeAvisPub;

    private ReferentielDTO service;

    private ReferentielDTO procedure;

    private PieceJointeDTO avisFichier;
    private PieceJointeDTO avisFichierSimplifie;

    private List<SuiviDTO> suiviList;


}
