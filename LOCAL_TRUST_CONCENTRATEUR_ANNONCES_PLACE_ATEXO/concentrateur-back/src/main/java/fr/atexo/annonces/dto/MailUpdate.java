package fr.atexo.annonces.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class MailUpdate {

    @NotBlank(message = "L'objet est obligatoire")
    private String objet;

    @NotBlank(message = "Le contenu est obligatoire")
    private String contenu;
}
