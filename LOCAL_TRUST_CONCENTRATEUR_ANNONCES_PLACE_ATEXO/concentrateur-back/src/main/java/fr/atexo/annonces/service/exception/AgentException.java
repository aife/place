package fr.atexo.annonces.service.exception;

import lombok.Getter;

@Getter
public class AgentException extends AtexoException {

    public AgentException(AgentExceptionEnum agentExceptionEnum, String errorMessage, Throwable err) {
        super(agentExceptionEnum.getCode(), errorMessage, err);

    }

    public AgentException(AgentExceptionEnum agentExceptionEnum, String errorMessage) {
        super(agentExceptionEnum.getCode(), errorMessage);

    }
}
