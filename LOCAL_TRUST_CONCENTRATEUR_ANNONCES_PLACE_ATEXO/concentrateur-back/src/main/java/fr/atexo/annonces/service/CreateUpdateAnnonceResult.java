package fr.atexo.annonces.service;

import fr.atexo.annonces.dto.SuiviAnnonceDTO;

/**
 * Classe permettant d'encapsuler le résultat d'une opération création/mise à jour d'une annonce.
 */
public class CreateUpdateAnnonceResult extends CreateUpdateResult<SuiviAnnonceDTO> {

    public CreateUpdateAnnonceResult(boolean created, SuiviAnnonceDTO dtoObject) {
        super(created, dtoObject);
    }
}
