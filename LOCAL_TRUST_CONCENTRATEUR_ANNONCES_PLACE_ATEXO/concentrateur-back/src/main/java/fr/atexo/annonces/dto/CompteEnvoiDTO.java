package fr.atexo.annonces.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CompteEnvoiDTO {

    private String idPlatform;
    private ConfigurationConsultationDTO configuration;

}
