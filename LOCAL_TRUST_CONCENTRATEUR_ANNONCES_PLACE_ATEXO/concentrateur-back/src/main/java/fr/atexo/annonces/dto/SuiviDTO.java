package fr.atexo.annonces.dto;

import fr.atexo.annonces.boot.entity.tncp.SuiviStatut;
import lombok.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SuiviDTO implements Serializable {

    private Integer id;
    private String uuid;
    private SuiviStatut statut;
    private Instant creation;
    private Instant envoi;
    private Instant verification;
    private String refPublication;
    private int annonce;
    private AcheteurChoixDTO acheteur;
    private String redirection;

}
