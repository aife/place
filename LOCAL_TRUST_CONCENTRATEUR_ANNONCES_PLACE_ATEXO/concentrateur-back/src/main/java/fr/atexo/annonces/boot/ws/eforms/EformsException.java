package fr.atexo.annonces.boot.ws.eforms;

import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import lombok.Getter;
import org.springframework.web.client.RestClientResponseException;

@Getter
public class EformsException extends AtexoException {
    public EformsException(ExceptionEnum exceptionEnum, String responseBodyAsString, RestClientResponseException e) {
        super(exceptionEnum, responseBodyAsString, e);
    }

    public EformsException(ExceptionEnum exceptionEnum, String message, Exception e) {
        super(exceptionEnum, message, e);
    }

    public EformsException(ExceptionEnum exceptionEnum, String s) {
        super(exceptionEnum, s);
    }
}
