package fr.atexo.annonces.reseau;

import org.apache.commons.net.util.Base64;

public final class AuthentificationUtils {

    private AuthentificationUtils() {
    }

    /**
     * Génère l'authentification
     *
     * @param compte
     * @param motDePasse
     * @return String
     */
    static String genererAuthentification(final String compte, final String motDePasse) {
        String authStringEnc = "Basic "; // méthode authentification Basic
        String base64Auth = Base64.encodeBase64StringUnChunked((compte + ":" + motDePasse // méthode authentification Basic
        ).getBytes()); // encoding Base64

        return authStringEnc + base64Auth;
    }
}
