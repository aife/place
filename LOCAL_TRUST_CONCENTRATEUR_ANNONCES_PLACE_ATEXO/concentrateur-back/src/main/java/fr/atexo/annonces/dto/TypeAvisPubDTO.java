package fr.atexo.annonces.dto;

import lombok.*;

import java.io.Serializable;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class TypeAvisPubDTO implements Serializable {

    private static final long serialVersionUID = -1410232521216613768L;
    private String libelle;
    private String code;
    private Boolean europeen;


}
