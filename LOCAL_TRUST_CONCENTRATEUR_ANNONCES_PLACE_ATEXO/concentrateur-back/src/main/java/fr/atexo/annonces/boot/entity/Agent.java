package fr.atexo.annonces.boot.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Set;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "agent", uniqueConstraints = {@UniqueConstraint(columnNames = {"identifiant", "id_organisme"})},
        indexes = {@Index(name = "agent_identifiant_organisme_plateforme", columnList = "identifiant,id_organisme"),
                @Index(name = "agent_id_index", columnList = "id")})
public class Agent implements Serializable {


    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid",
            strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    private String id;

    @Column(name = "identifiant")
    private String identifiant;

    @Lob
    @Column(name = "acheteur_public")
    private String acheteurPublic;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "nom")
    private String nom;

    @Lob
    @Column(name = "roles", nullable = false)
    private String roles;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_organisme")
    private Organisme organisme;

    @Column(name = "premier_acces")
    private ZonedDateTime datePremierAcces;

    @Column(name = "dernier_acces")
    private ZonedDateTime dateDernierAcces;

    @Column(name = "email", length = 100)
    private String email;

    @Lob
    @Column(name = "envoi_mpe")
    private String envoiMpe;

    @Lob
    @Column(name = "mpe_refresh_token")
    private String mpeRefreshToken;

    @Lob
    @Column(name = "mpe_token", length = 100)
    private String mpeToken;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_service")
    private ReferentielServiceMpe service;

    @Column(name = "id_mpe")
    private String idMpe;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "fax")
    private String fax;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "agent")
    private Set<AgentSessionEntity> sessions;
}
