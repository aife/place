package fr.atexo.annonces.boot.ws.ressources;

public class RessourcesException extends RuntimeException {
    private static final long serialVersionUID = 5669054343003133752L;

    public RessourcesException(String message) {
        super(message);
    }

    public RessourcesException(String message, Throwable cause) {
        super(message, cause);
    }
}
