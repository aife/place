package fr.atexo.annonces.dto.mol;

import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class AvisPublieCriteriaVo {

    protected Date dateDebut;
    protected Date dateFin;
    private String uidPf;

}
