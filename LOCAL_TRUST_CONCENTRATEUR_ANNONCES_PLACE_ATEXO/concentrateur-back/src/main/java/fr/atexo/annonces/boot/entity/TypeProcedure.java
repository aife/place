package fr.atexo.annonces.boot.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;


@Entity
@Table(name = "type_procedure")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TypeProcedure implements Serializable {

    private static final long serialVersionUID = -1410232521216613768L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_type_procedure", unique = true, nullable = false)
    private Integer id;

    @Column(name = "abbreviation", length = 50)
    private String abbreviation;

    @Column(name = "libelle_type_procedure_fr", length = 100)
    private String libelle;

    @Column(name = "correspondance_ted", length = 100)
    private String correspondanceTed;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "procedure")
    private Set<OffreTypeProcedure> offresOrganisme;

    @Column(name = "avis_externe")
    private Boolean avisExterne;



}
