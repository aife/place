package fr.atexo.annonces.mapper.tncp;

import eu.europa.publications.resource.schema.ted.r2_0_9.reception.TedEsenders;
import fr.atexo.annonces.commun.tncp.Cpv;
import fr.atexo.annonces.mapper.Mappable;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueMappingStrategy;

@Mapper(componentModel = "spring", nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface CpvMapper extends Mappable<String, Cpv> {

    @Override
    @Mapping(source = "cpv", target = "value")
    Cpv map(String cpv, TedEsenders tedEsenders, boolean rie);

}
