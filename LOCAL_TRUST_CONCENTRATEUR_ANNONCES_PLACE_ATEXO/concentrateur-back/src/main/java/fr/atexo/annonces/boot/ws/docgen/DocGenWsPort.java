package fr.atexo.annonces.boot.ws.docgen;


import fr.atexo.annonces.dto.DocumentEditorRequest;
import fr.atexo.annonces.dto.FileStatus;
import fr.atexo.annonces.dto.ValidationDocumentEditorRequest;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface DocGenWsPort {

    InputStream generatePdf(List<KeyValueRequest> map, File ressource) throws IOException;

    InputStream generateDocument(List<KeyValueRequest> map, File ressource, String defaultOnNull) throws IOException;

    String getEditionToken(File ressource, DocumentEditorRequest request, String extension);

    String getValidationToken(File ressource, ValidationDocumentEditorRequest request, String extension);

    InputStream getDocument(String token);

    FileStatus getDocumentStatus(String token);

    Resource convertToPDF(File ressource, String extension);
}
