package fr.atexo.annonces.boot.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "SUPPORT_INTERMEDIAIRE")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SupportIntermediaire implements Serializable {

    private static final long serialVersionUID = -1410232521211913768L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "ID_PLATEFORME")
    private String idPlateforme;

    @Column(name = "CODE_DESTINATION", length = 150)
    private String codeDestination;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_SUPPORT", referencedColumnName = "ID")
    private Support support;


}
