package fr.atexo.annonces.mapper;

import fr.atexo.annonces.boot.entity.EFormsFormulaireEntity;
import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.boot.entity.SupportIntermediaire;
import fr.atexo.annonces.config.DaoMapper;
import fr.atexo.annonces.dto.AcheteurChoixDTO;
import fr.atexo.annonces.dto.HistoriqueDTO;
import fr.atexo.annonces.dto.SuiviAnnonceDTO;
import fr.atexo.annonces.dto.SuiviOffreDTO;
import fr.atexo.annonces.modeles.StatutSuiviAnnonceEnum;
import org.mapstruct.*;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.util.CollectionUtils;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Mapper entre l'entité SuiviAnnonce et son DTO
 */
@Mapper(uses = {OffreMapper.class, SuiviAvisPublieMapper.class})
@ComponentScan("fr.atexo.annonces.mapper")
public interface SuiviAnnonceMapper extends DaoMapper<SuiviAnnonce, SuiviAnnonceDTO> {

    @Mapping(target = "offre", qualifiedByName = "ignoreSupport")
    @Mapping(target = "idEForms", source = "formulaire.id")
    @Mapping(target = "eFormsValid", source = "formulaire.valid")
    @Mapping(target = "numeroTed", source = "formulaire.uuid")
    @Mapping(target = "tedVersion", source = "formulaire.tedVersion")
    @Mapping(target = "idParent", source = "parent.id")
    @Mapping(target = "numeroAvisParent", source = "parent.numeroAvis")
    @Mapping(target = "numeroJoueParent", source = "parent.numeroJoue")
    @Mapping(target = "lienPublicationParent", source = "parent.lienPublication")
    @Mapping(target = "acheteur", qualifiedByName = "mapAcheteur", source = "suiviAnnonce")
    @Named("ignoreSupport")
    @Mapping(target = "dateMiseEnLigneCalcule", source = "typeAvisPub.dateMiseEnLigneCalcule")
    SuiviAnnonceDTO suiviAnnonceToSuiviAnnonceDTOIgnoreSupport(SuiviAnnonce suiviAnnonce);

    @Mapping(target = "codeDestination", qualifiedByName = "mapCodeDestination", source = "suiviAnnonce")
    @Mapping(target = "historique", qualifiedByName = "mapActionsAnnonces", source = "suiviAnnonce")
    @Mapping(target = "idEForms", source = "formulaire.id")
    @Mapping(target = "eFormsValid", source = "formulaire.valid")
    @Mapping(target = "numeroTed", source = "formulaire.uuid")
    @Mapping(target = "tedVersion", source = "formulaire.tedVersion")
    @Mapping(target = "idParent", source = "parent.id")
    @Mapping(target = "numeroAvisParent", source = "parent.numeroAvis")
    @Mapping(target = "numeroJoueParent", source = "parent.numeroJoue")
    @Mapping(target = "lienPublicationParent", source = "parent.lienPublication")
    @Mapping(target = "acheteur", qualifiedByName = "mapAcheteur", source = "suiviAnnonce")
    @Named("withOffres")
    @Mapping(target = "dateMiseEnLigneCalcule", source = "typeAvisPub.dateMiseEnLigneCalcule")
    SuiviAnnonceDTO suiviAnnonceToSuiviAnnonceDTO(SuiviAnnonce suiviAnnonce);

    @Mapping(target = "numeroTed", source = "formulaire.uuid")
    SuiviOffreDTO suiviAnnonceToSuiviOffreDTO(SuiviAnnonce suiviAnnonce);

    @Mapping(target = "idEForms", source = "formulaire.id")
    @Mapping(target = "numeroTed", source = "formulaire.uuid")
    @Mapping(target = "tedVersion", source = "formulaire.tedVersion")
    @Mapping(target = "eFormsValid", source = "formulaire.valid")
    @Mapping(target = "idParent", source = "parent.id")
    @Mapping(target = "numeroAvisParent", source = "parent.numeroAvis")
    @Mapping(target = "numeroJoueParent", source = "parent.numeroJoue")
    @Mapping(target = "lienPublicationParent", source = "parent.lienPublication")
    @Mapping(target = "acheteur", qualifiedByName = "mapAcheteur", source = "suiviAnnonce")
    @Mapping(target = "dateMiseEnLigneCalcule", source = "typeAvisPub.dateMiseEnLigneCalcule")
    SuiviAnnonceDTO suiviAnnonceToSuiviAnnonceDTOIgnoreOffres(SuiviAnnonce suiviAnnonce);

    default SuiviAnnonceDTO suiviAnnonceToSuiviAnnonceDTO(SuiviAnnonce suiviAnnonce, boolean includeSupport) {
        if (includeSupport) {
            return suiviAnnonceToSuiviAnnonceDTO(suiviAnnonce);
        } else {
            return suiviAnnonceToSuiviAnnonceDTOIgnoreSupport(suiviAnnonce);
        }
    }

    @IterableMapping(qualifiedByName = "ignoreSupport")
    @Named("ignoreSupports")
    List<SuiviAnnonceDTO> suiviAnnoncesToSuiviAnnonceDTOsIgnoreSupport(List<SuiviAnnonce> suiviAnnonces);

    @IterableMapping(qualifiedByName = "withOffres")
    List<SuiviAnnonceDTO> suiviAnnoncesToSuiviAnnonceDTOs(List<SuiviAnnonce> suiviAnnonces);

    default List<SuiviAnnonceDTO> suiviAnnoncesToSuiviAnnonceDTOs(List<SuiviAnnonce> suiviAnnonces, boolean includeSupport) {
        if (includeSupport) {
            return suiviAnnoncesToSuiviAnnonceDTOs(suiviAnnonces);
        } else {
            return suiviAnnoncesToSuiviAnnonceDTOsIgnoreSupport(suiviAnnonces);
        }
    }

    @Mapping(target = "offre", ignore = true)
    @Mapping(target = "idConsultation", ignore = true)
    @Mapping(target = "dateDebutEnvoi", ignore = true)
    @Mapping(target = "dateDemandeEnvoi", ignore = true)
    @Mapping(target = "dateEnvoi", ignore = true)
    @Mapping(target = "dateSoumission", ignore = true)
    @Mapping(target = "dateSoumissionTed", ignore = true)
    @Mapping(target = "datePublication", ignore = true)
    @Mapping(target = "datePublicationTed", ignore = true)
    @Mapping(target = "idPlatform", ignore = true)
    @Mapping(target = "messageStatut", ignore = true)
    @Mapping(target = "statutJoue", ignore = true)
    @Mapping(target = "statutBoamp", ignore = true)
    @Mapping(target = "dateCreation", ignore = true)
    @Mapping(target = "dateModification", ignore = true)
    @Mapping(target = "prixStandard", ignore = true)
    @Mapping(target = "prixSimplifie", ignore = true)
    @Mapping(target = "organisme", ignore = true)
    @Mapping(target = "agent", ignore = true)
    @Mapping(target = "xmlConcentrateur", ignore = true)
    @Mapping(target = "xmlTransaction", ignore = true)
    @Mapping(target = "xml", ignore = true)
    @Mapping(target = "service", ignore = true)
    @Mapping(target = "procedure", ignore = true)
    @Mapping(target = "suiviList", ignore = true)
    @Mapping(target = "organismeMpe", ignore = true)
    @Mapping(target = "formulaire", ignore = true)
    @Mapping(target = "typeAvisPub", ignore = true)
    void updateSuiviAnnonceFromSuiviAnnonceDTO(SuiviAnnonceDTO suiviAnnonceDTO, @MappingTarget SuiviAnnonce suiviAnnonce);

    @Named("mapCodeDestination")
    default String mapCodeDestination(SuiviAnnonce entity) {
        if (entity.getIdPlatform() == null || entity.getOffre() == null || entity.getSupport() == null) {
            return null;
        }
        List<SupportIntermediaire> supportIntermediaires = entity.getSupport().getSupportIntermediaires();
        if (CollectionUtils.isEmpty(supportIntermediaires) || supportIntermediaires.stream().noneMatch(supportIntermediaire -> entity.getIdPlatform().equals(supportIntermediaire.getIdPlateforme()))) {
            return entity.getSupport().getCodeDestination();
        }

        SupportIntermediaire found = supportIntermediaires.stream().filter(supportIntermediaire -> entity.getIdPlatform().equals(supportIntermediaire.getIdPlateforme())).findFirst().orElse(null);
        return found == null ? null : found.getCodeDestination();
    }


    @Named("mapActionsAnnonces")
    default List<HistoriqueDTO> mapActionsAnnonces(SuiviAnnonce entity) {
        List<HistoriqueDTO> map = new ArrayList<>();
        EFormsFormulaireEntity formulaire = entity.getFormulaire();
        if (formulaire != null) {
            do {
                ZonedDateTime dateSoumission = formulaire.getDateSoumission();
                ZonedDateTime dateModification = formulaire.getDateModification();
                ZonedDateTime dateEnvoiAnnulation = formulaire.getDateEnvoiAnnulation();
                ZonedDateTime dateEnvoiSoumission = formulaire.getDateEnvoiSoumission();
                if (formulaire.getStatut() != null) {
                    if (dateSoumission != null) {
                        map.add(0, HistoriqueDTO.builder().date(dateSoumission).statut(StatutSuiviAnnonceEnum.EN_COURS_DE_PUBLICATION).build());
                    }
                    if (dateEnvoiSoumission != null) {
                        map.add(0, HistoriqueDTO.builder().date(dateEnvoiSoumission).statut(StatutSuiviAnnonceEnum.EN_ATTENTE).build());
                    }
                    if (dateEnvoiAnnulation != null)
                        map.add(0, HistoriqueDTO.builder().date(dateEnvoiAnnulation).statut(StatutSuiviAnnonceEnum.EN_COURS_D_ARRET).build());

                    switch (formulaire.getStatut()) {
                        case STOPPED:
                            if (dateModification != null)
                                map.add(0, HistoriqueDTO.builder().date(dateModification).statut(StatutSuiviAnnonceEnum.ARRETER).build());


                            break;

                        case VALIDATION_FAILED:
                        case NOT_PUBLISHED:
                            if (dateModification != null) {
                                map.add(0, HistoriqueDTO.builder().date(dateModification).statut(StatutSuiviAnnonceEnum.REJETER_SUPPORT).build());
                            }
                            break;
                        default:

                    }
                }
                formulaire = formulaire.getPrevious();

            } while (formulaire != null);
        }


        while (entity != null) {
            ZonedDateTime dateCreation = entity.getDateCreation();
            if (dateCreation != null) {
                SuiviAnnonce parent = entity.getParent();
                if (parent == null) {
                    StatutSuiviAnnonceEnum statut = StatutSuiviAnnonceEnum.BROUILLON;
                    map.add(HistoriqueDTO.builder().date(dateCreation).statut(statut).build());
                } else {
                    map.add(HistoriqueDTO.builder().date(dateCreation).statut(parent.getNumeroAvis() != null ? StatutSuiviAnnonceEnum.MODIFIER : StatutSuiviAnnonceEnum.REPRENDRE).build());
                }
            }
            if (entity.getDateDemandeEnvoi() != null) {
                map.add(HistoriqueDTO.builder().date(ZonedDateTime.ofInstant(entity.getDateDemandeEnvoi(), ZoneId.systemDefault())).statut(StatutSuiviAnnonceEnum.EN_ATTENTE).build());
            }
            if (entity.getDateEnvoi() != null) {
                map.add(HistoriqueDTO.builder().date(entity.getDateEnvoi().atZone(ZoneId.systemDefault())).statut(StatutSuiviAnnonceEnum.PREPARATION_PUBLICATION).build());
            }
            if (entity.getDateSoumission() != null) {
                map.add(HistoriqueDTO.builder().date(entity.getDateSoumission().atZone(ZoneId.systemDefault())).statut(StatutSuiviAnnonceEnum.EN_COURS_DE_PUBLICATION).build());
            }
            if (entity.getDatePublication() != null) {
                map.add(HistoriqueDTO.builder().date(entity.getDatePublication().atZone(ZoneId.systemDefault())).statut(StatutSuiviAnnonceEnum.PUBLIER).build());
            }
            if (entity.getDateValidation() != null) {
                map.add(HistoriqueDTO.builder().date(entity.getDateValidation()).statut(StatutSuiviAnnonceEnum.COMPLET).build());
            }

            entity = entity.getParent();
        }
        return map.stream().distinct().sorted(Comparator.comparing(HistoriqueDTO::getDate)).collect(Collectors.toList());
    }

    @Named("mapAcheteur")
    default AcheteurChoixDTO mapAcheteur(SuiviAnnonce entity) {
        if (entity == null || CollectionUtils.isEmpty(entity.getSuiviList())) {
            return null;
        }
        return entity.getSuiviList().stream().filter(suiviAvisPublie -> suiviAvisPublie.getAcheteur() != null).map(suiviAvisPublie -> AcheteurChoixDTO.builder().email(suiviAvisPublie.getAcheteur().getEmail()).login(suiviAvisPublie.getAcheteur().getLogin()).build()).findFirst().orElse(null);
    }
}
