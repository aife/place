package fr.atexo.annonces.modeles;

public enum SupportPublicationEnum {

    BOAMP("BOAMP"), LESECHOS("LESECHOS"), LESECHOSBIMEDIA("LESECHOSBIMEDIA"),
    TED("TED"),
    TNCP("TNCP"),
    SUDOUEST("SUDOUEST"), SO("SO"), CL("CL"), CL_SO("CL_SO"), SO_DL("SO_DL"), SO_RP_EP("SO_RP_EP"), LM("LM"), LNR("LNR"), LPC("LPC"),
    EMARCHEPUBLIC("EMARCHEPUBLIC"), LEPARISIEN("LEPARISIEN"), LEPARISIENBIMEDIA("LEPARISIENBIMEDIA"), JBTP("JBTP"), LAL("LAL"), JAL("JAL");

    private String support;

    /**
     * Constructeur
     */
    SupportPublicationEnum(String support) {
        this.support = support;
    }


}
