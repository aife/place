package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.reseau.HttpFileParameter;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;

/**
 * Classe regroupant les constructeurs des objets à envoyer dans le corps d'une requête HTTP
 */
@Service
public class HttpTransformerServiceImpl implements HttpTransformerService {

    public static final String LES_ECHOS_XML_FILE_NAME = "fichierPublication";
    public static final String LES_ECHOS_PDF_FILE_NAME = "fichierAnnonce";
    private final SuiviAnnonceService suiviAnnonceService;

    public HttpTransformerServiceImpl(SuiviAnnonceService suiviAnnonceService) {
        this.suiviAnnonceService = suiviAnnonceService;
    }

    @Override
    public MultiValueMap<String, Object> buildLesEchosFileParameters(List<HttpFileParameter> fileParameters) {
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        for (HttpFileParameter fileParameter : fileParameters) {
            body.add(fileParameter.getName(), fileParameter.getFile());
        }
        return body;
    }

    @Override
    public HttpFileParameter buildLesEchosXmlFileParameter(String xml) {
        HttpHeaders xmlPartHeaders = new HttpHeaders();
        ContentDisposition xmlPartContentDisposition = ContentDisposition
                .builder("form-data")
                .name(LES_ECHOS_XML_FILE_NAME)
                .filename("depot_lesechos.xml")
                .build();
        xmlPartHeaders.setContentDisposition(xmlPartContentDisposition);
        xmlPartHeaders.setContentType(MediaType.TEXT_XML);

        return new HttpFileParameter(LES_ECHOS_XML_FILE_NAME, new HttpEntity<>(xml.getBytes(), xmlPartHeaders));
    }

    @Override
    public HttpFileParameter buildLesEchosPdfFileParameter(Message<?> message) {
        Map<String, Object> headers = message.getHeaders();
        int idAnnonce = (Integer) headers.get("idAnnonce");
        SuiviAnnonce suiviAnnonce = suiviAnnonceService.getSuiviAnnonceEntity(idAnnonce);

        HttpHeaders pdfPartHeaders = new HttpHeaders();

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        suiviAnnonceService.exportToDocument(suiviAnnonce, outputStream,  null, suiviAnnonce.getOffre().isConvertToPdf());
        ContentDisposition pdfPartContentDisposition = ContentDisposition
                .builder("form-data")
                .name(LES_ECHOS_PDF_FILE_NAME)
                .filename("export.pdf")
                .build();
        pdfPartHeaders.setContentDisposition(pdfPartContentDisposition);
        pdfPartHeaders.setContentType(MediaType.APPLICATION_PDF);

        return new HttpFileParameter(LES_ECHOS_PDF_FILE_NAME, new HttpEntity<>(outputStream.toByteArray(), pdfPartHeaders));
    }
}
