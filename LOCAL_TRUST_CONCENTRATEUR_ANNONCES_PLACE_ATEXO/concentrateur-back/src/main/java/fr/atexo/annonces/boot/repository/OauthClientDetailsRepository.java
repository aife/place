package fr.atexo.annonces.boot.repository;

import fr.atexo.annonces.boot.entity.OauthClientDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository pour accéder aux objets Departement
 */
@Repository
public interface OauthClientDetailsRepository extends JpaRepository<OauthClientDetails, String> {

}
