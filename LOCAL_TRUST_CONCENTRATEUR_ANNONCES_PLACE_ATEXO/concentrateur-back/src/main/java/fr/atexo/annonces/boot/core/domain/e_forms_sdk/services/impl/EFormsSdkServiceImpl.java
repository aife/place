package fr.atexo.annonces.boot.core.domain.e_forms_sdk.services.impl;

import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.impl.GenericSdkService;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.FieldDetails;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNotice;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.XmlStructureDetails;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.services.IEFormsSdkService;
import fr.atexo.annonces.boot.entity.Offre;
import fr.atexo.annonces.boot.entity.Support;
import fr.atexo.annonces.boot.repository.EformsSurchargeRepository;
import fr.atexo.annonces.boot.repository.OffreRepository;
import fr.atexo.annonces.boot.repository.ReferentielSurchargeLibelleRepository;
import fr.atexo.annonces.boot.repository.SupportRepository;
import fr.atexo.annonces.boot.ws.eforms.SchematronConfiguration;
import fr.atexo.annonces.config.EFormsHelper;
import fr.atexo.annonces.service.exception.AtexoException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static fr.atexo.annonces.config.EFormsHelper.getObjectFromFile;

@Slf4j
@Service
public class EFormsSdkServiceImpl extends GenericSdkService implements IEFormsSdkService {

    private final OffreRepository offreRepository;
    private final SupportRepository supportRepository;
    private final ReferentielSurchargeLibelleRepository referentielSurchargeLibelleRepository;

    public EFormsSdkServiceImpl(SchematronConfiguration schematronConfiguration, OffreRepository offreRepository, SupportRepository supportRepository, ReferentielSurchargeLibelleRepository referentielSurchargeLibelleRepository, EformsSurchargeRepository eformsSurchargeRepository) {
        super(eformsSurchargeRepository, schematronConfiguration);
        this.offreRepository = offreRepository;
        this.supportRepository = supportRepository;
        this.referentielSurchargeLibelleRepository = referentielSurchargeLibelleRepository;
        this.initOffre(schematronConfiguration.getResourcesTag());
    }

    public void initOffre(String resourcesTag) {
        String code = "ENOTICES";
        Support saved = supportRepository.findByCode(code);
        Support support = Support.builder()
                .id(saved == null ? null : saved.getId())
                .code(code)
                .actif(true)
                .libelle("TED eNotices")
                .description("TED eNotices")
                .url("https://enotices.ted.europa.eu")
                .poids(0)
                .europeen(true)
                .external(false)
                .national(false)
                .choixUnique(false)
                .codeDestination(code)
                .codeGroupe("TED")
                .afficherLogo(true)
                .build();

        support = supportRepository.save(support);
        Support finalSupport = support;

        Map<String, Object> noticeList = this.getSdkI18n("fr", resourcesTag)
                .entrySet()
                .stream()
                .filter(stringObjectEntry -> stringObjectEntry.getKey() != null)
                .filter(stringObjectEntry -> stringObjectEntry.getKey().startsWith("notice|name|"))
                .collect(Collectors.toMap(stringObjectEntry -> stringObjectEntry.getKey().replace("notice|name|", "") + "_ENOTICES", Map.Entry::getValue));
        Set<Offre> offres = support.getOffres();
        if (!CollectionUtils.isEmpty(offres)) {
            offres.forEach(offre -> {
                offre.setSupports(new ArrayList<>());
                log.info("Dissociation de l'offre {} du support {}", offre.getCode(), finalSupport.getCode());
                offreRepository.save(offre);

            });

        }
        noticeList.forEach((key, value) -> {
            String label = value.toString();
            Offre offre = offreRepository.findByCode(key).orElse(null);
            if (offre == null) {
                offre = Offre.builder()
                        .supports(List.of(finalSupport))
                        .code(key)
                        .libelle(label)
                        .build();
                log.info("Ajout de {} : {}", key, label);
            } else {
                offre.setLibelle(label);
                offre.setSupports(List.of(finalSupport));
                log.info("Mise à jour de {} : {}", key, label);
            }
            offreRepository.save(offre);

        });

    }

    @Override
    public FieldDetails getFieldById(String id, String version) {
        return getFields(version).getFields().stream().filter(fieldDetails -> fieldDetails.getId().equals(id))
                .findFirst()
                .map(fieldDetails -> {
                    String nodeId = fieldDetails.getParentNodeId();
                    List<XmlStructureDetails> nodeIds = getNodeIds(nodeId, version);
                    fieldDetails.setNodeIds(nodeIds);
                    return fieldDetails;
                })
                .orElse(null);
    }


    @Override
    @Cacheable(cacheNames = "getNodeIdsById")
    public List<XmlStructureDetails> getNodeIdsById(String id, String version) {
        return getNodeIds(id, version);
    }


    @Override
    @Cacheable(cacheNames = "getCodeById")
    public Map<String, Set<String>> getCodeById(String id, String lang, String search, String plateforme, String version) {
        return this.getCodeListById(id, lang != null ? lang : "en", search, plateforme, version);
    }

    @Override
    @Cacheable(cacheNames = "getFieldsOptions")
    public Map<String, Set<String>> getFieldsOptions(String id, String plateforme, String version, String search, String lang) {
        FieldDetails fieldDetail = this.getFieldById(id, version);
        if (fieldDetail.getCodeList() != null) {
            return this.getCodeListById(fieldDetail.getCodeList().getValue().getId(), lang, search, plateforme, version);
        }
        List<String> attributes = fieldDetail.getAttributes();
        if (CollectionUtils.isEmpty(attributes)) {
            return Map.of(id, Set.of());
        }

        return attributes.stream()
                .map(field -> getFieldById(field, version))
                .filter(fieldDetails -> fieldDetails.getCodeList() != null)
                .map(fieldDetails -> fieldDetails.getCodeList().getValue().getId())
                .map(idCodeList -> this.getCodeListById(idCodeList, "en", search, plateforme, version))
                .findFirst().orElse(Map.of(id, Set.of()));
    }


    @Override
    public String getParentCode(String id, String plateforme, String version) {
        Map<String, Set<String>> list = this.getCodeById(id, null, null, plateforme, version);
        return list.keySet().stream().findFirst().orElse(null);
    }

    @Override
    public Object getI18n(String lang) {
        try {
            File file = ResourceUtils.getFile("classpath:i18n/eforms-" + lang + ".json");
            return EFormsHelper.getObjectFromFile(file, Object.class);
        } catch (FileNotFoundException e) {
            log.error("Erreur lors du chargement du fichier i18n : {}", e.getMessage());
        }
        return new HashMap<>();
    }

    @Override
    public Map<String, Object> getSurchargeI18n(Integer idOrganisme, String lang) {
        Map<String, Object> map = new HashMap<>();

        referentielSurchargeLibelleRepository.findAllByOrganismeIdAndLang(idOrganisme, lang)
                .forEach(referentielSurchargeLibelle -> map.put(referentielSurchargeLibelle.getCode(), referentielSurchargeLibelle.getLibelle()));
        return map;
    }

    @Override
    public SubNotice getNotice(String idNotice, String version) {
        SubNotice subNotice;
        subNotice = getSubNoticeList(version).getNoticeSubTypes().stream().filter(subNotice1 -> subNotice1.getSubTypeId().equals(idNotice)).findFirst().orElse(new SubNotice());
        try {
            SubNotice subNoticeMetadata = getObjectFromFile(getEformsFolderSdk(version) + "/notice-types/" + idNotice + ".json", SubNotice.class);
            subNotice.setMetadata(subNoticeMetadata.getMetadata());
            subNotice.setContent(subNoticeMetadata.getContent());
        } catch (IOException e) {
            throw new AtexoException(400, e);
        }
        return subNotice;
    }

}
