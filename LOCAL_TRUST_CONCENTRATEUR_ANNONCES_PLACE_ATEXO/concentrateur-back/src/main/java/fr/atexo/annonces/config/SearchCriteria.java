package fr.atexo.annonces.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class SearchCriteria {
    private String key;
    private SearchOperatorEnum operation;
    private Object value;
    private boolean join = false;
    private boolean not = false;

    public SearchCriteria(String key, SearchOperatorEnum operation, Object value) {
        this.key = key;
        this.operation = operation;
        this.value = value;
    }

}
