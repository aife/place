package fr.atexo.annonces.boot.entity;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "agent_session")
@EntityListeners(AuditingEntityListener.class)
public class AgentSessionEntity implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_agent", referencedColumnName = "id")
    private Agent agent;

    @Lob
    @Column(name = "token", nullable = false)
    private String token;


    @Column(name = "DATE_CREATION", nullable = false)
    @CreatedDate
    private ZonedDateTime dateCreation;


}
