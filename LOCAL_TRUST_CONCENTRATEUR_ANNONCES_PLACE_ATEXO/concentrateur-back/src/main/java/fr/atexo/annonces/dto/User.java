package fr.atexo.annonces.dto;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @NotEmpty
    private String id;
    @NotEmpty
    private String name;

    private List<String> roles = new ArrayList<>();
}
