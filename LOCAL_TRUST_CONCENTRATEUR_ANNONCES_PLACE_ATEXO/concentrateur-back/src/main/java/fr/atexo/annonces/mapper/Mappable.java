package fr.atexo.annonces.mapper;


import eu.europa.publications.resource.schema.ted.r2_0_9.reception.TedEsenders;

public interface Mappable<T, U> {

    U map(T t, TedEsenders tedEsenders, boolean rie);
}
