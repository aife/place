package fr.atexo.annonces.reseau;

import fr.atexo.annonces.boot.configuration.http_logging.LogEntryExit;
import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.dto.*;
import fr.atexo.annonces.service.AgentService;
import fr.atexo.annonces.service.CreateUpdateAnnonceResult;
import fr.atexo.annonces.service.SuiviAnnonceService;
import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import fr.atexo.annonces.service.exception.ExportConfigurationException;
import fr.atexo.annonces.service.exception.MissingArgumentException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.net.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.logging.LogLevel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

/**
 * Point d'entrée web pour manipuler les demandes de publication
 */
@RestController
@RequestMapping("/rest/v2/annonces")
@Tag(name = "Annonces")
public class AnnonceController {

    private final Logger logger = LoggerFactory.getLogger(AnnonceController.class);

    private final SuiviAnnonceService suiviAnnonceService;
    private final AgentService agentService;

    public AnnonceController(SuiviAnnonceService suiviAnnonceService, AgentService agentService) {
        this.suiviAnnonceService = suiviAnnonceService;
        this.agentService = agentService;
    }


    @GetMapping
    @Operation(description = "swagger.annonces.get.operation.summary", summary = "swagger.annonces.get.operation.notes", responses = {@ApiResponse(responseCode = HttpServletResponse.SC_UNAUTHORIZED + "", description = "swagger.response.unauthorized"), @ApiResponse(responseCode = HttpServletResponse.SC_NOT_FOUND + "", description = "swagger.annonces.response.notFound")})
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    @LogEntryExit(skip = true)
    public ResponseEntity<Object> getSuiviAnnonce(@RequestParam @Parameter(required = true, name = "swagger.annonces.param.idPlatform") String idPlatform, @RequestParam @Parameter(required = true, name = "swagger.annonces.param.idConsultation", example = "1") Integer idConsultation, @RequestParam(required = false) @Parameter(name = "swagger.annonces.param.codeSupport") String codeSupport, @RequestParam(required = false) @Parameter(name = "swagger.annonces.param.codeOffre") String codeOffre, @RequestParam(required = false) @Parameter(example = "support", name = "swagger.annonces.get.param.include") AnnonceIncludeParameterEnum include) {
        boolean includeSupport = AnnonceIncludeParameterEnum.SUPPORT.equals(include);
        if (codeSupport == null) {
            return new ResponseEntity<>(suiviAnnonceService.getSuiviAnnonces(idPlatform, idConsultation, includeSupport), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(suiviAnnonceService.getSuiviAnnonce(idPlatform, idConsultation, codeSupport, codeOffre, includeSupport), HttpStatus.OK);
        }
    }

    @PutMapping
    @Operation(description = "swagger.annonces.put.operation.summary", summary = "swagger.annonces.put.operation.notes", responses = {@ApiResponse(responseCode = HttpServletResponse.SC_UNAUTHORIZED + "", description = "swagger.response.unauthorized"), @ApiResponse(responseCode = HttpServletResponse.SC_NOT_FOUND + "", description = "swagger.annonces.put.response.notFound"), @ApiResponse(responseCode = HttpServletResponse.SC_FORBIDDEN + "", description = "swagger.annonces.put.response.forbidden"), @ApiResponse(responseCode = HttpServletResponse.SC_BAD_REQUEST + "", description = "swagger.response.badRequest"), @ApiResponse(responseCode = HttpServletResponse.SC_CONFLICT + "", description = "swagger.annonces.put.response.conflict")})
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public ResponseEntity<Object> putAnnonce(@RequestParam @Parameter(required = true, name = "swagger.annonces.param.idPlatform") String idPlatform, @RequestParam @Parameter(required = true, name = "swagger.annonces.param.idConsultation", example = "1") Integer idConsultation, @RequestParam(required = false) @Parameter(name = "swagger.annonces.param.codeSupport") String codeSupport, @RequestParam(required = false) @Parameter(name = "swagger.annonces.param.codeOffre") String codeOffre, @RequestBody @Parameter(required = true, name = "swagger.annonces.put.param.suiviAnnonceDTO") SuiviAnnonceDTO suiviAnnonceDTO, @RequestHeader("Authorization") String token) {

        if (codeSupport == null) {
            return new ResponseEntity<>(suiviAnnonceService.updateSuiviAnnonces(idPlatform, idConsultation, suiviAnnonceDTO), HttpStatus.OK);
        } else {
            CreateUpdateAnnonceResult result = suiviAnnonceService.createOrUpdateSuiviAnnonce(idPlatform, idConsultation, codeSupport, codeOffre, suiviAnnonceDTO, token.replace("Bearer ", ""));
            return new ResponseEntity<>(result.getDtoObject(), result.isCreated() ? HttpStatus.CREATED : HttpStatus.OK);
        }
    }


    @PatchMapping("{id}/simplifie")
    @Operation(description = "swagger.annonces.patch.operation.summary", summary = "swagger.annonces.patch.operation.notes", responses = {@ApiResponse(responseCode = HttpServletResponse.SC_UNAUTHORIZED + "", description = "swagger.response.unauthorized"), @ApiResponse(responseCode = HttpServletResponse.SC_NOT_FOUND + "", description = "swagger.annonces.put.response.notFound"), @ApiResponse(responseCode = HttpServletResponse.SC_FORBIDDEN + "", description = "swagger.annonces.put.response.forbidden"), @ApiResponse(responseCode = HttpServletResponse.SC_BAD_REQUEST + "", description = "swagger.response.badRequest"), @ApiResponse(responseCode = HttpServletResponse.SC_CONFLICT + "", description = "swagger.annonces.put.response.conflict")})
    @PreAuthorize("#oauth2.hasScope('platform:' + #simplifieDTO.idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #simplifieDTO.idConsultation)")
    public ResponseEntity<Boolean> updateSimplifie(@PathVariable Integer id, @RequestBody(required = false) @Parameter(name = "swagger.annonces.put.param.simplifieDTO") ConsultationSimplifieDTO simplifieDTO) {
        return new ResponseEntity<>(suiviAnnonceService.updateSimplifie(id, simplifieDTO), HttpStatus.OK);

    }

    @PatchMapping("{id}")
    public ResponseEntity<SuiviAnnonceDTO> modifyStatut(@PathVariable Integer id, @RequestHeader("Authorization") String token, @RequestBody(required = false) @Parameter(name = "swagger.annonces.put.param.SuiviAnnonceUpdate") SuiviAnnonceUpdate update) {
        return new ResponseEntity<>(suiviAnnonceService.modifyStatut(id, update, token.replace("Bearer ", "")), HttpStatus.OK);

    }


    @DeleteMapping("{id}")
    @Operation(description = "swagger.annonces.delete.operation.summary", summary = "swagger.annonces.delete.operation.notes", responses = {@ApiResponse(responseCode = HttpServletResponse.SC_UNAUTHORIZED + "", description = "swagger.response.unauthorized"), @ApiResponse(responseCode = HttpServletResponse.SC_NOT_FOUND + "", description = "swagger.annonces.response.notFound"), @ApiResponse(responseCode = HttpServletResponse.SC_CONFLICT + "", description = "swagger.annonces.delete.response.conflict")})
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public Boolean deleteAnnonce(@PathVariable Integer id, @RequestParam @Parameter(required = true, name = "swagger.annonces.param.idPlatform") String idPlatform, @RequestParam @Parameter(required = true, name = "swagger.annonces.param.idConsultation", example = "1") Integer idConsultation) {
        return suiviAnnonceService.deleteSuiviAnnonce(id, idPlatform, idConsultation);
    }

    @GetMapping("/pdf")
    @Operation(description = "swagger.annonces.pdf.get.operation.summary", summary = "swagger.annonces.pdf.get.operation.notes", responses = {@ApiResponse(responseCode = HttpServletResponse.SC_NO_CONTENT + "", description = "swagger.annonces.pdf.get.response.noContent"), @ApiResponse(responseCode = HttpServletResponse.SC_UNAUTHORIZED + "", description = "swagger.response.unauthorized"), @ApiResponse(responseCode = HttpServletResponse.SC_NOT_FOUND + "", description = "swagger.annonces.response.notFound")})
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public void getAnnoncePdf(@RequestParam @Parameter(required = true, name = "swagger.annonces.param.idPlatform") String idPlatform, @RequestParam @Parameter(required = true, name = "swagger.annonces.param.idConsultation", example = "1") Integer idConsultation, @RequestParam @Parameter(required = true, name = "swagger.annonces.param.codeSupport") String codeSupport, @RequestParam @Parameter(required = true, name = "swagger.annonces.param.codeOffre") String codeOffre, @RequestParam(required = false) Boolean simplifier, HttpServletResponse response) throws IOException {
        try {
            SuiviAnnonce suiviAnnonce = suiviAnnonceService.getSuiviAnnonceEntity(idPlatform, idConsultation, codeSupport, codeOffre);
            if (suiviAnnonce == null) {
                throw new ExportConfigurationException("suiviAnnonce ne peut pas être null");
            }

            String filename = suiviAnnonceService.geDocumentName(suiviAnnonce, false);
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            response.setHeader("Content-Disposition", "attachment; filename=" + filename);
            suiviAnnonceService.exportToDocument(suiviAnnonce, response.getOutputStream(), simplifier, suiviAnnonce.getOffre().isConvertToPdf());

        } catch (MissingArgumentException e) {
            resetServletResponse(response, e);
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        } catch (Exception e) {
            resetServletResponse(response, e);
            throw e;
        }
    }


    @PostMapping("/pdf")
    @Operation(description = "swagger.annonces.pdf.post.operation.summary", summary = "swagger.annonces.pdf.post.operation.notes", responses = {@ApiResponse(responseCode = HttpServletResponse.SC_UNAUTHORIZED + "", description = "swagger.response.unauthorized"), @ApiResponse(responseCode = HttpServletResponse.SC_BAD_REQUEST + "", description = "swagger.response.badRequest")})
    public void postAnnoncePdf(@RequestBody @Parameter(required = true, name = "swagger.annonces.pdf.post.param.suiviAnnonceDTO") SuiviAnnonceDTO suiviAnnonceDTO, @RequestParam(required = false) Boolean simplifier, HttpServletResponse response) throws IOException {
        try {
            SuiviAnnonce suiviAnnonce = suiviAnnonceService.getSuiviAnnonceEntity(suiviAnnonceDTO.getIdPlatform(), suiviAnnonceDTO.getIdConsultation(), suiviAnnonceDTO.getSupport().getCode(), suiviAnnonceDTO.getOffre().getCode());
            if (suiviAnnonce == null) {
                throw new ExportConfigurationException("suiviAnnonce ne peut pas être null");
            }

            boolean convertToPdf = suiviAnnonce.getOffre().isConvertToPdf();
            String filename = suiviAnnonceService.geDocumentName(suiviAnnonce, convertToPdf);
            response.setHeader("Content-Disposition", "attachment; filename=" + filename);
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            suiviAnnonceService.exportToDocument(suiviAnnonce, response.getOutputStream(), simplifier, convertToPdf);
        } catch (Exception e) {
            resetServletResponse(response, e);
            throw e;
        }
    }

    @GetMapping("{id}/pdf")
    @Operation(description = "swagger.annonces.pdf.post.operation.summary", summary = "swagger.annonces.pdf.post.operation.notes", responses = {@ApiResponse(responseCode = HttpServletResponse.SC_UNAUTHORIZED + "", description = "swagger.response.unauthorized"), @ApiResponse(responseCode = HttpServletResponse.SC_BAD_REQUEST + "", description = "swagger.response.badRequest")})
    public void getAnnoncePdf(@PathVariable Integer id, @RequestParam(required = false) Boolean simplifier, HttpServletResponse response) throws IOException {
        try {
            SuiviAnnonce suiviAnnonce = suiviAnnonceService.getSuiviAnnonceEntity(id);
            if (suiviAnnonce == null) {
                throw new ExportConfigurationException("suiviAnnonce ne peut pas être null");
            }
            boolean convertToPdf = suiviAnnonce.getOffre().isConvertToPdf();
            String filename = suiviAnnonceService.geDocumentName(suiviAnnonce, convertToPdf);
            response.setHeader("Content-Disposition", "attachment; filename=" + filename);
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            suiviAnnonceService.exportToDocument(suiviAnnonce, response.getOutputStream(), simplifier, convertToPdf);
        } catch (Exception e) {
            resetServletResponse(response, e);
            throw e;
        }
    }

    @PostMapping("/configuration")
    @PreAuthorize("#oauth2.hasScope('platform:' + #config.idPlatform)")
    @LogEntryExit(value = LogLevel.INFO, showArgs = true)
    public Boolean updateConfiguration(@RequestHeader("Authorization") String token, @RequestBody @Parameter(required = true, name = "swagger.annonces.pdf.post.param.ConfigurationUpdateDTO") ConfigurationUpdateDTO config) {
        String xml = config.getXml();
        if (xml != null) {
            config.setXml(new String(Base64.decodeBase64(xml)));
        }
        return suiviAnnonceService.updateConfiguration(config, Set.of(), token.replace("Bearer ", ""));
    }

    @GetMapping("/configuration/{idConsultation}")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public ConfigurationConsultationDTO getConfiguration(@RequestHeader("Authorization") String token, @RequestParam String idPlatform, @PathVariable Integer idConsultation) {
        ConfigurationConsultationDTO configuration = agentService.getConfiguration(token.replace("Bearer ", ""), idConsultation, idPlatform);
        if (configuration == null) {
            throw new AtexoException(ExceptionEnum.NOT_FOUND, "Aucune session de consultation trouvée");
        }
        return configuration;
    }

    @GetMapping("/{id}/ouvrir")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public SuiviDTO ouvrirCompteAcheteur(@PathVariable Integer id, @RequestHeader("Authorization") String token, @RequestParam String idPlatform, @RequestParam Integer idConsultation, @RequestParam(required = false, defaultValue = "false") boolean rie) {
        return suiviAnnonceService.ouvrirCompteAcheteur(id, token.replace("Bearer ", ""), idPlatform, idConsultation, rie);
    }

    @PatchMapping("{id}/reprendre")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public SuiviAnnonceDTO reprendre(@PathVariable Integer id, @RequestParam String idPlatform, @RequestParam Integer idConsultation, @RequestHeader("Authorization") String token) {

        return suiviAnnonceService.reprendreById(id, idPlatform, idConsultation, token.replace("Bearer ", ""));
    }

    @PatchMapping("{id}/change")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) and #oauth2.hasAnyScope('consultation', 'consultation:' + #idConsultation)")
    public SuiviAnnonceDTO change(@PathVariable Integer id, @RequestParam String idPlatform, @RequestParam Integer idConsultation, @RequestHeader("Authorization") String token) {
        return suiviAnnonceService.changeById(id, idPlatform, idConsultation, token.replace("Bearer ", ""));
    }

    private void resetServletResponse(HttpServletResponse response, Exception e) {
        try {
            response.reset();
        } catch (IllegalStateException f) {
            logger.error("Erreur survenue lors de la génération du PDF ; impossible de transmettre cette erreur" + " au client car des données du PDF ont déjà été envoyées", e);
            throw f;
        }
    }

    @GetMapping("/{id}/redirection")
    public SuiviDTO getRedirection(@PathVariable("id") Integer id) {
        return suiviAnnonceService.getRedirection(id);
    }


    @GetMapping("{id}/editeur-en-ligne")
    public EditionRequest modifyAvisDocument(@PathVariable int id, @RequestHeader("Authorization") String token, @RequestParam boolean simplifie, @RequestParam String idPlatform, @RequestParam Integer idConsultation) {
        return suiviAnnonceService.modifyAvisDocument(id, idPlatform, idConsultation, token.replace("Bearer ", ""), simplifie);

    }

    @PatchMapping("{id}/reinit-document")
    public SuiviAnnonceDTO regenererAvisDocument(@PathVariable int id, @RequestParam boolean simplifie, @RequestHeader("Authorization") String token, @RequestParam String idPlatform, @RequestParam Integer idConsultation) {
        return suiviAnnonceService.reinitierAvisDocument(id, idPlatform, idConsultation, token.replace("Bearer ", ""), simplifie);

    }

    @PostMapping("{id}/editeur-en-ligne/callback")
    public TrackDocumentResponse trackDocument(@PathVariable int id, @RequestParam boolean simplifie, @RequestBody FileStatus status) {
        return suiviAnnonceService.getDocumentCallback(id, simplifie, status);
    }

}
