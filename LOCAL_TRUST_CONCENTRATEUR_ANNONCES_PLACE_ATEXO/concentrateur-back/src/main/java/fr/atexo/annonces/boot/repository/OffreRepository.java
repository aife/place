package fr.atexo.annonces.boot.repository;

import fr.atexo.annonces.boot.entity.Offre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Repository pour accéder aux objets Offre
 */
@Repository
public interface OffreRepository extends JpaRepository<Offre, Integer> {

    Optional<Offre> findByCode(String code);

    @Query(value = "UPDATE offre SET ID_OFFRE_ASSOCIEE = null", nativeQuery = true)
    void resetOffreAssociee();
}
