package fr.atexo.annonces.boot.ws.validator;

import fr.atexo.annonces.dto.validator.ValidatorDTO;
import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
public class ValidatorWS {
    private final RestTemplate atexoRestTemplate;
    private final ValidatorConfiguration validatorConfiguration;

    public ValidatorWS(RestTemplate atexoRestTemplate, ValidatorConfiguration validatorConfiguration) {
        this.atexoRestTemplate = atexoRestTemplate;
        this.validatorConfiguration = validatorConfiguration;
    }


    public Resource validateXml(String notice, ValidatorDTO metadata, String eformsToken) {
        try {
            metadata.setNotice(notice);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("X-API-Key", StringUtils.hasText(eformsToken) ? eformsToken : validatorConfiguration.getApiKey());

            String url = validatorConfiguration.getBasePath() + validatorConfiguration.getWs().get("validate");
            return atexoRestTemplate.postForObject(url, new HttpEntity<>(metadata, headers), Resource.class);
        } catch (RestClientResponseException e) {
            throw new AtexoException(ExceptionEnum.EFORMS_WS, e.getResponseBodyAsString(), e);
        } catch (Exception e) {
            throw new AtexoException(ExceptionEnum.EFORMS_WS, e.getMessage(), e);
        }
    }


}
