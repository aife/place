package fr.atexo.annonces.dto;

import lombok.*;

import java.time.Instant;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class TokenBody {
    private Instant exp;
}
