package fr.atexo.annonces.service.exception;

/**
 * Exception levée si l'opération demandée n'est pas réalisable (par exemple, le service n'a pas l'autorité nécessaire
 * pour effectuer l'opération)
 */
public class IllegalOperationException extends RuntimeException {

    private static final long serialVersionUID = -6905816077612939848L;

    public IllegalOperationException(String message) {
        super(message);
    }
}
