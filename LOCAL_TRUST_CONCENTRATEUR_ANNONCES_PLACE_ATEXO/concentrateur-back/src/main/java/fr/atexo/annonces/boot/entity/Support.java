package fr.atexo.annonces.boot.entity;

import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "support")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"code"})
public class Support implements Serializable {

    private static final long serialVersionUID = -1410232521216613768L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;

    @Column(name = "CODE")
    private String code;

    @Column(name = "LIBELLE")
    private String libelle;

    @Column(name = "CODE_GROUPE")
    private String codeGroupe;

    @Column(name = "EXTERNAL")
    private boolean external;

    @Column(name = "ACTIF")
    private boolean actif;

    @Column(name = "LOGO_URL")
    private String logoUrl;

    @Column(name = "MAIL")
    private String mail;

    @Column(name = "CODE_DESTINATION")
    private String codeDestination;

    @Column(name = "DESCRIPTION")
    @Type(type = "text")
    private String description;

    @Column(name = "URL")
    private String url;

    @Column(name = "PAYS", length = 3)
    private String pays;

    @Column(name = "POIDS")
    private int poids;

    @Column(name = "AFFICHER_LOGO")
    private boolean afficherLogo;

    @Column(name = "NATIONAL")
    private boolean national;

    @Column(name = "EUROPEEN")
    private boolean europeen;

    @Column(name = "CHOIX_UNIQUE")
    private boolean choixUnique;

    @Column(name = "COMPTE_OBLIGATOIRE")
    private boolean compteObligatoire;


    @Column(name = "SELECTIONNE_PAR_DEFAUT")
    private boolean selectionneParDefaut;

    @Column(name = "IFRAME")
    private boolean iframe;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "support_offre_asso",
            joinColumns = @JoinColumn(name = "ID_SUPPORT"),
            inverseJoinColumns = @JoinColumn(name = "ID_OFFRE"))
    private Set<Offre> offres;

    @OneToMany(mappedBy = "support", fetch = FetchType.LAZY)
    private List<SupportIntermediaire> supportIntermediaires;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "support_jal_departement",
            inverseJoinColumns = @JoinColumn(name = "ID_DEPARTEMENT"),
            joinColumns = @JoinColumn(name = "ID_SUPPORT"))
    private Set<Departement> jal;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "support_pqr_departement",
            inverseJoinColumns = @JoinColumn(name = "ID_DEPARTEMENT"),
            joinColumns = @JoinColumn(name = "ID_SUPPORT"))
    private Set<Departement> pqr;

}
