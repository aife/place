package fr.atexo.annonces.service;

import eu.europa.publications.resource.schema.ted.r2_0_9.reception.TedEsenders;
import fr.atexo.annonces.boot.entity.Offre;
import fr.atexo.annonces.boot.entity.PieceJointe;
import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.boot.entity.SuiviTypeAvisPub;
import fr.atexo.annonces.dto.*;
import fr.atexo.annonces.modeles.StatutSuiviAnnonceEnum;
import org.springframework.messaging.Message;

import java.io.OutputStream;
import java.util.List;
import java.util.Set;

/**
 * Couche service pour manipuler les demandes de publications et leur suivi
 */
public interface SuiviAnnonceService extends FopPdfExportable<SuiviAnnonce> {

    SuiviAnnonceDTO getSuiviAnnonce(String idPlatform, Integer idConsultation, String codeSupport, String codeOffre, boolean includeSupport);

    SuiviAnnonce getSuiviAnnonceEntity(String idPlatform, Integer idConsultation, String codeSupport, String codeOffre);

    SuiviAnnonce getSuiviAnnonceEntity(int id);

    List<SuiviAnnonceDTO> getSuiviAnnonces(String idPlatform, Integer idConsultation, boolean includeSupport);

    CreateUpdateAnnonceResult createOrUpdateSuiviAnnonce(String idPlatform, Integer idConsulation,
                                                         String codeSupport, String codeOffre, SuiviAnnonceDTO suiviAnnonceDTO, String token);

    Boolean deleteSuiviAnnonce(Integer id, String idPlatform, Integer idConsulation);

    List<SuiviAnnonceDTO> updateSuiviAnnonces(String idPlatform, Integer idConsultation, SuiviAnnonceDTO suiviAnnonceDTO);

    String toXmlIntegration(Message<?> message);

    void exportToPdfV2(SuiviTypeAvisPub avisPub, SuiviAnnonce suiviAnnonce, Offre offre, OutputStream exportPdf, TedEsenders tedEsenders, Boolean simplifier, boolean convertToPdf);

    String updateXmlTransform(Integer idAnnonce, String xmlTransform);

    void updateErrorMessage(Integer idAnnonce, String errorMessage, boolean rejetConcentrateur);

    boolean updateConfiguration(ConfigurationUpdateDTO config, Set<String> roles, String token);

    void sendTed(Message<?> message);

    SuiviAnnonce sauvegarder(SuiviAnnonce annonce);

    SuiviAnnonceDTO modifyStatut(Integer id, SuiviAnnonceUpdate update, String token);

    SuiviAnnonce modifyAnnonceStatut(SuiviAnnonce suiviAnnonce, SuiviAnnonceUpdate build, String token);

    SuiviDTO ouvrirCompteAcheteur(Integer id, String token, String idPlatform, Integer idConsultation, boolean rie);

    Boolean updateSimplifie(Integer id, ConsultationSimplifieDTO simplifieDTO);

    SuiviDTO getRedirection(Integer id);

    SuiviAnnonce change(SuiviAnnonce suiviAnnonce, SuiviTypeAvisPub avisPub, String token);

    SuiviAnnonce reprendre(SuiviAnnonce suiviAnnonce, SuiviTypeAvisPub avisPub, String token);

    void fixStatut(SuiviAnnonce suiviAnnonce, StatutSuiviAnnonceEnum statut);

    SuiviAnnonce addPieceJointe(Integer id, PieceJointe pieceJointe);

    void synchronizeLesEchos();

    EditionRequest modifyAvisDocument(int id, String idPlatform, Integer idConsultation, String bearer, boolean simplifie);

    void updateDocumentEdition(FileStatus status, SuiviAnnonce suiviAnnonce, boolean isSimplifie);

    TrackDocumentResponse getDocumentCallback(int id, boolean simplifie, FileStatus status);

    SuiviAnnonceDTO reinitierAvisDocument(int id, String idPlatform, Integer idConsultation, String bearer, boolean simplifie);

    SuiviAnnonceDTO reprendreById(Integer id, String idPlatform, Integer idConsultation, String bearer);
    SuiviAnnonceDTO changeById(Integer id, String idPlatform, Integer idConsultation, String bearer);

    void suiviDocument(SuiviAnnonce annonce);
}
