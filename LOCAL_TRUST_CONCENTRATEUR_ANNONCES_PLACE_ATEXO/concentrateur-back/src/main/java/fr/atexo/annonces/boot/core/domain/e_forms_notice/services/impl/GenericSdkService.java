package fr.atexo.annonces.boot.core.domain.e_forms_notice.services.impl;

import com.atexo.annonces.commun.mpe.DonneesComplementaires;
import com.atexo.annonces.commun.mpe.ProfilJoueMpe;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.*;
import fr.atexo.annonces.boot.repository.EformsSurchargeRepository;
import fr.atexo.annonces.boot.ws.eforms.SchematronConfiguration;
import fr.atexo.annonces.config.EFormsHelper;
import fr.atexo.annonces.config.XMLResourceBundleControl;
import fr.atexo.annonces.dto.EformsSurchargeDTO;
import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import liquibase.repackaged.org.apache.commons.text.StringEscapeUtils;
import lombok.extern.slf4j.Slf4j;
import org.cornutum.regexpgen.RandomGen;
import org.cornutum.regexpgen.RegExpGen;
import org.cornutum.regexpgen.js.Parser;
import org.cornutum.regexpgen.random.RandomBoundsGen;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static fr.atexo.annonces.config.EFormsHelper.*;

@Slf4j
public abstract class GenericSdkService {

    protected final SchematronConfiguration schematronConfiguration;
    protected final EformsSurchargeRepository eformsSurchargeRepository;

    public GenericSdkService(EformsSurchargeRepository eformsSurchargeRepository, SchematronConfiguration schematronConfiguration) {
        this.eformsSurchargeRepository = eformsSurchargeRepository;
        this.schematronConfiguration = schematronConfiguration;
    }

    protected boolean getMandatoryForbiddenDetailsValue(String idNotice, MandatoryForbiddenDetails mandatory) {

        if (mandatory == null) return false;
        List<Constraint> constraints = mandatory.getConstraints();
        if (CollectionUtils.isEmpty(constraints)) return mandatory.isValue();
        if (constraints.stream().anyMatch(constraint -> constraint.getNoticeTypes().contains(idNotice) && !StringUtils.hasText(constraint.getCondition()))) {
            return constraints.stream().filter(constraint -> constraint.getNoticeTypes().contains(idNotice)).anyMatch(Constraint::isValue);
        }
        return false;
    }

    protected XmlStructureDetails getXmlStructure(String id, String version) {
        return getFields(version).getXmlStructure().stream().filter(item -> item.getId().equals(id)).findFirst().orElse(null);
    }

    protected List<Object> getXmlStaticValue(String id, String idNotice, String versionSdk) {
        return getFields(versionSdk).getFields().stream().filter(item -> item.getParentNodeId().equals(id) && !getMandatoryForbiddenDetailsValue(idNotice, item.getForbidden()) && item.getPresetValue() != null && item.getAttributeName() == null && CollectionUtils.isEmpty(item.getAttributes()) && !item.getPresetValue().equals("{NOW}") && item.getType().equals("id")).map(fieldDetails -> {
            String presetValue = fieldDetails.getPresetValue();
            return addToObject(new Object(), fieldDetails.getId(), presetValue, id, id, versionSdk);

        }).filter(Objects::nonNull).collect(Collectors.toList());
    }

    protected FieldsSdk getFields(String version) {
        try {
            return getObjectFromFile(getEformsFolderSdk(version) + "/fields/fields.json", FieldsSdk.class);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return null;
    }


    public SubNoticeListSdk getSubNoticeList(String version) {
        try {
            return getObjectFromFile(getEformsFolderSdk(version) + "/notice-types/notice-types.json", SubNoticeListSdk.class);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return null;
    }

    public String removeLeadingZeros(String input) {
        if (input == null) {
            return null;
        }
        // Remove leading zeros using a loop
        int i = 0;
        while (i < input.length() && input.charAt(i) == '0') {
            i++;
        }

        return input.substring(i);

    }

    protected CodeListSdk getCodelists(String version) {
        try {
            return getObjectFromFile(getEformsFolderSdk(version) + "/codelists/codelists.json", CodeListSdk.class);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return null;
    }

    @Cacheable(cacheNames = "getNodeIds")
    public List<XmlStructureDetails> getNodeIds(String nodeId, String version) {
        List<XmlStructureDetails> nodeIds = new ArrayList<>();
        while (nodeId != null) {
            XmlStructureDetails xmlStructureDetails = getXmlStructure(nodeId, version);
            if (xmlStructureDetails != null) {
                nodeIds.add(xmlStructureDetails);
                nodeId = xmlStructureDetails.getParentId();

            }
        }
        return nodeIds;
    }

    protected String getId(FieldDetails fieldDetails, Integer i) {
        String uuid = getExemple(fieldDetails, i);
        if (uuid == null) {
            uuid = UUID.randomUUID().toString();
        }
        return uuid;
    }

    protected String getExemple(FieldDetails fieldDetails, Integer i) {
        if (fieldDetails == null) return null;
        PatternField pattern = fieldDetails.getPattern();
        if (pattern != null && pattern.getValue() != null) {
            String idScheme = fieldDetails.getIdScheme();
            if (idScheme == null) {
                RandomGen random = new RandomBoundsGen();
                RegExpGen generator = Parser.parseRegExp(pattern.getValue());
                return generator.generate(random);
            } else {
                if (i == null) i = 1;
                int length = String.valueOf(i).length();
                return idScheme + "-" + "0".repeat(Math.max(0, 4 - length)) + i;
            }
        }
        return null;
    }


    protected Object addToObjectV2(Object formulaireObjet, String fieldId, Object value) {

        if (value != null && value instanceof String && !((String) value).isEmpty()) {
            value = StringEscapeUtils.unescapeHtml4((String) value);
        }
        if (fieldId == null) {
            return value;
        }

        String jsonString = "{\"%s\": %s }";
        try {
            Object o = getObjectFromString(String.format(jsonString, fieldId, getString(value)), Object.class);
            formulaireObjet = merge(formulaireObjet, o);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return formulaireObjet;
    }

    protected Object setSurchargeGroup(Object formulaireObjet, SubNoticeMetadata group) {
        EformsSurchargeDTO surcharge = group.getSurcharge();
        if (surcharge != null && surcharge.getDefaultValue() != null) {
            String defaultValue = surcharge.getDefaultValue();
            log.info("Surcharge {} avec {}", group.getId(), defaultValue);
            Object objectFromString = EFormsHelper.getObjectFromString(defaultValue, Object.class);
            if (group.is_repeatable()) {
                List<Object> objects = new ArrayList<>();
                JsonNode node = valueToTree(formulaireObjet);
                if (!node.isEmpty())
                    objects.add(formulaireObjet);
                node = valueToTree(objectFromString);
                if (node.isArray())
                    objects.addAll((Collection<?>) objectFromString);
                else
                    objects.add(objectFromString);
                return objects;
            } else {
                return EFormsHelper.merge(formulaireObjet, objectFromString);
            }
        } else {
            return formulaireObjet;
        }
    }

    protected Object addToObject(Object formulaireObjet, String fieldId, Object value, String nodeStop, String nodeId, String version) {
        if (value == null) {
            return formulaireObjet;
        }
        JsonNode node = valueToTree(value);
        if (node.isObject() && node.isEmpty()) {
            return formulaireObjet;
        }
        if (value instanceof String && !((String) value).isEmpty()) {
            value = StringEscapeUtils.unescapeHtml4((String) value);
        }
        Object o;
        String jsonString = "{\"%s\": %s }";
        String listString = "{\"%s\":  [%s] }";

        try {
            if (fieldId != null)
                o = getObjectFromString(String.format(jsonString, fieldId, getString(value)), Object.class);
            else o = value;

            List<XmlStructureDetails> list = this.getNodeIds(nodeId, version);
            /*String finalNodeStop = nodeStop;
            if (nodeStop != null && nodeId != null && !CollectionUtils.isEmpty(list) && list.stream().noneMatch(item -> item.getId().equals(finalNodeStop))) {
                List<XmlStructureDetails> listNodeStop = this.getNodeIds(nodeStop);
                for (XmlStructureDetails xmlStructureDetails : listNodeStop) {
                    if (list.stream().anyMatch(item -> item.getId().equals(xmlStructureDetails.getId()))) {
                        nodeStop = xmlStructureDetails.getId();
                        break;
                    }

                }

            }*/
            for (XmlStructureDetails xmlStructureDetails : list) {
                if (xmlStructureDetails.getId().equals(nodeStop)) {
                    break;
                }
                String valeur;
                if (xmlStructureDetails.isRepeatable()) {
                    if (EFormsHelper.valueToTree(o).isArray())
                        valeur = String.format(jsonString, xmlStructureDetails.getId(), getString(o));
                    else valeur = String.format(listString, xmlStructureDetails.getId(), getString(o));
                } else {
                    valeur = String.format(jsonString, xmlStructureDetails.getId(), getString(o));
                }
                o = getObjectFromString(valeur, Object.class);

            }
            formulaireObjet = merge(formulaireObjet, o);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return formulaireObjet;
    }

    protected Object deleteObject(Object formulaireObjet) {
        List<String> fieldsToDelete = Arrays.asList("BT-738-notice", "BT-127-notice");
        try {
            for (String fieldId : fieldsToDelete) {
                JsonNode node = valueToTree(formulaireObjet);
                formulaireObjet = deleteFromNode(fieldId, node);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return formulaireObjet;
    }

    private static Object deleteFromNode(String fieldId, JsonNode node) {
        if (node == null) return null;
        if (!node.isObject() && !node.isArray()) {
            return node;
        }
        if (node.isArray()) {
            List<Object> objects = new ArrayList<>();
            for (JsonNode jsonNode : node) {
                objects.add(deleteFromNode(fieldId, jsonNode));
            }
            return objects;
        } else if (node.has(fieldId)) {
            ObjectNode objectNode = (ObjectNode) node;
            objectNode.remove(fieldId);
            return objectNode;
        } else {
            node.fields().forEachRemaining(entry -> {
                JsonNode value = entry.getValue();
                deleteFromNode(fieldId, value);
            });
            return node;
        }
    }

    public Object getPresetValue(String presetValue, String type, String versionSdk) {
        try {

        switch (type) {
            case "date", "date-time" -> {
                ZonedDateTime dateTime;
                if (presetValue.equals("{NOW}")) {
                    dateTime = ZonedDateTime.now();
                } else {
                    dateTime = LocalDate.parse(presetValue, DateTimeFormatter.ofPattern("yyyy-MM-ddXXXXX")).atStartOfDay(ZoneId.of("Europe/Paris"));
                }
                dateTime = dateTime.withZoneSameInstant(ZoneId.of("Europe/Paris"));
                Object date = addToObject(new Object(), "type", "Europe/Paris", null, null, versionSdk);
                DateTimeFormatter formatterDate = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                date = addToObject(date, "value", dateTime.format(formatterDate), null, null, versionSdk);
                return date;
            }
            case "time", "zoned-time" -> {
                ZonedDateTime dateTime;
                if (presetValue.equals("{NOW}")) {
                    dateTime = ZonedDateTime.now();
                } else {
                    dateTime = ZonedDateTime.parse(presetValue, DateTimeFormatter.ofPattern("HH:mm:ssXXXXX"));
                }
                dateTime = dateTime.withZoneSameInstant(ZoneId.of("Europe/Paris"));
                Object date = addToObject(new Object(), "type", "Europe/Paris", null, null, versionSdk);
                DateTimeFormatter formatterDate = DateTimeFormatter.ofPattern("HH:mm:ss");
                date = addToObject(date, "value", dateTime.format(formatterDate), null, null, versionSdk);
                return date;
            }
            default -> {
                log.warn("Aucune description pour le champs {} de type {}", presetValue, type);
                return null;
            }
        }
        } catch (Exception e) {
            log.error("Erreur lors de la récupération de la valeur par défaut : {}", e.getMessage());
            return null;
        }

    }

    protected Object setGroupId(Object formulaireObjet, String nodeStop, SubNoticeMetadata item, String nodeId, String version, Integer index) {
        if (EFormsHelper.valueToTree(formulaireObjet).isEmpty()) return formulaireObjet;
        String identifierFieldId = item.get_identifierFieldId();
        if (StringUtils.hasText(identifierFieldId)) {
            FieldDetails fieldDetails = getFields(version).getFields().stream().filter(fieldDetails1 -> fieldDetails1.getId().equals(identifierFieldId)).findFirst().orElse(null);
            String id = getId(fieldDetails, index);
            formulaireObjet = addToObject(formulaireObjet, identifierFieldId, id, nodeId == null ? nodeStop : nodeId, fieldDetails.getParentNodeId(), version);
        }
        return formulaireObjet;
    }

    protected Object setGroupIdV2(Object formulaireObjet, SubNoticeMetadata item, String version) {
        if (EFormsHelper.valueToTree(formulaireObjet).isEmpty()) return formulaireObjet;
        String identifierFieldId = item.get_identifierFieldId();
        if (StringUtils.hasText(identifierFieldId)) {
            FieldDetails fieldDetails = getFields(version).getFields().stream().filter(fieldDetails1 -> fieldDetails1.getId().equals(identifierFieldId)).findFirst().orElse(null);
            String id = getId(fieldDetails, null);
            addToObjectV2(formulaireObjet, identifierFieldId, id);
        }
        return formulaireObjet;
    }

    public Map<String, Set<String>> getCodeListById(String idCodeList, String lang, String search, String plateforme, String version) {
        String id = idCodeList;
        Map<String, Set<String>> surchargedCodes = getCode(lang, id, plateforme);
        if (!CollectionUtils.isEmpty(surchargedCodes)) {
            return surchargedCodes;
        }
        do {
            String finalId = id;
            Map<String, Object> o = new HashMap<>();
            String key = "code|name|" + id;
            getBundle(o, "code", key, version, lang);
            Object codeList = o.get(key);

            if (codeList == null && !lang.equals("en")) {
                o = new HashMap<>();
                getBundle(o, "code", key, version, "en");
                codeList = o.get(key);
            }

            if (codeList == null) {
                Code code = getCodelists(version).getCodelists().stream().filter(item -> item.getId().equals(finalId)).findFirst().orElse(null);
                if (code == null) return new HashMap<>();
                id = code.getParentId();
            } else {
                if (CollectionUtils.isEmpty(o)) {
                    return Map.of(id, Set.of());
                }
                Map<String, String> objectFromString = getObjectFromString(getString(codeList), Map.class);
                if (search == null) {
                    Set<String> set = objectFromString.entrySet().stream().sorted(Map.Entry.comparingByValue()).map(Map.Entry::getKey).filter(Objects::nonNull).collect(Collectors.toCollection(LinkedHashSet::new));
                    return Map.of(id, set);
                }
                Set<String> set = objectFromString.entrySet().stream().filter(stringStringEntry -> {
                    String value = org.apache.commons.lang3.StringUtils.stripAccents(stringStringEntry.getValue().toLowerCase());
                    String codeKey = org.apache.commons.lang3.StringUtils.stripAccents(stringStringEntry.getKey().toLowerCase());
                    String prefix = org.apache.commons.lang3.StringUtils.stripAccents(search.toLowerCase());
                    return value.contains(prefix) || codeKey.startsWith(prefix);
                }).sorted(Map.Entry.comparingByValue()).map(Map.Entry::getKey).filter(Objects::nonNull).collect(Collectors.toCollection(LinkedHashSet::new));
                return Map.of(id, set);

            }
        } while (id != null);
        return new HashMap<>();
    }


    public Map<String, Set<String>> getCode(String lang, String id, String plateforme) {
        try {
            File file;
            file = new File(schematronConfiguration.getInternationalizationDir(), plateforme + "/eforms-" + lang + ".json");
            if (!file.exists()) file = ResourceUtils.getFile("classpath:i18n/eforms-" + lang + ".json");
            Map<String, Object> map = EFormsHelper.getObjectFromFile(file, Map.class);
            String key = "code|name|" + id + ".";
            Set<String> set = map.keySet().stream().filter(value -> value.startsWith(key)).map(s -> s.replace(key, "")).collect(Collectors.toSet());
            return CollectionUtils.isEmpty(set) ? Collections.emptyMap() : Map.of(id, set);
        } catch (FileNotFoundException e) {
            log.error("Erreur lors du chargement du fichier i18n : {}", e.getMessage());
        }
        return Collections.emptyMap();
    }

    protected void getBundle(Map<String, Object> o, String bundleName, String startWith, String version, String lang) {
        Locale.setDefault(new Locale(lang, ""));
        ResourceBundle bundle = ResourceBundle.getBundle(bundleName, new XMLResourceBundleControl(getEformsFolderSdk(version)));
        bundle.getKeys().asIterator().forEachRemaining(key -> {
            boolean skip = false;
            if (startWith != null && !key.startsWith(startWith)) {
                skip = true;
            }
            if (!skip) {
                List<String> chemin = Arrays.stream(key.split("[.]")).collect(Collectors.toList());
                String traductionString = bundle.getString(key);
                getI18nMap(o, traductionString, chemin, 0);
            }
        });
    }

    private Map<String, Object> getI18nMap(Map<String, Object> o, String traductionString, List<String> chemin, int i) {
        String k1 = chemin.get(i);
        if (i == (chemin.size() - 1)) {
            o.put(k1, traductionString);
        } else {
            Object o1 = o.computeIfAbsent(k1, k -> new HashMap<String, Object>());
            return getI18nMap((Map<String, Object>) o1, traductionString, chemin, i + 1);
        }
        return o;
    }

    protected Object buildCpvSecondaires(Object formulaireObjet, String id, String idType, String nodeStop, FieldDetails fieldDetails, String parentNodeId, List<String> cpvSecondaires, String version) {
        if (!CollectionUtils.isEmpty(cpvSecondaires)) {
            List<Object> objects = new ArrayList<>();
            cpvSecondaires.forEach(s -> objects.add(buildObject(formulaireObjet, id, idType, nodeStop, fieldDetails, parentNodeId, s, "cpv", version)));
            return addToObject(formulaireObjet, null, objects, nodeStop, parentNodeId, version);
        } else return formulaireObjet;
    }

    protected Object buildObject(Object formulaireObjet, String id, String idType, String nodeStop, FieldDetails fieldDetails, String parentNodeId, Object value, Object typeValue, String version) {
        var cpv = addToObject(new Object(), id, value, null, null, version);
        FieldDetails cpvDetails = getFields(version).getFields().stream().filter(fieldDetails1 -> fieldDetails1.getId().equals(idType)).findFirst().orElse(null);
        cpv = addToObject(cpv, idType, typeValue, fieldDetails.getParentNodeId(), cpvDetails.getParentNodeId(), version);
        return addToObject(formulaireObjet, null, cpv, nodeStop, parentNodeId, version);
    }

    protected String getFromCodeList(String key, String value, String plateforme, String version) {
        if (value == null || key == null) {
            return null;
        }

        Map<String, Set<String>> codeListById = this.getCodeListById(key, "en", value.toUpperCase(), plateforme, version);
        if (CollectionUtils.isEmpty(codeListById)) {
            return null;
        }
        Set<String> set = codeListById.get(key);
        return set.stream().filter(Objects::nonNull).filter(s -> s.toUpperCase().startsWith(value.toUpperCase())).findFirst().orElse(null);

    }

    protected Object getNaturePrestation(String nature) {
        if (nature == null) return null;
        switch (nature) {
            case "/api/v2/referentiels/nature-prestations/1":
                return "works";
            case "/api/v2/referentiels/nature-prestations/2":
                return "supplies";
            case "/api/v2/referentiels/nature-prestations/3":
                return "services";
            default:
                return null;

        }
    }

    protected String getReponseElectronique(String reponseElectronique) {
        if (reponseElectronique == null) return null;
        switch (reponseElectronique.toUpperCase()) {
            case "OBLIGATOIRE":
                return "required";
            case "AUTORISEE":
                return "allowed";
            case "REFUSEE":
                return "not-allowed";
            default:
                return "not-allowed";
        }
    }

    protected String getSignatureElectronique(String signatureElectronique) {
        if (signatureElectronique == null) return null;
        switch (signatureElectronique.toUpperCase()) {
            case "NON_REQUISE":
                return "false";
            default:
                return "true";
        }
    }

    protected String getFormeJuridique(ProfilJoueMpe profilJoueMpe) {
        if (profilJoueMpe == null) return null;
        if (profilJoueMpe.isAutoriteNationale()) {
            return "body-pl";
        }
        if (profilJoueMpe.isOfficeNationale()) {
            return "body-pl";
        }
        if (profilJoueMpe.isCollectiviteTerritoriale()) {
            return "body-pl";
        }
        if (profilJoueMpe.isOfficeRegionale()) {
            return "body-pl";
        }
        if (profilJoueMpe.isOrganismePublic()) {
            return "body-pl";
        }
        if (profilJoueMpe.isOrganisationEuropenne()) {
            return "int-org";
        }

        return null;
    }

    protected String getPouvoirJuridique(ProfilJoueMpe profilJoueMpe) {
        if (profilJoueMpe == null) return null;
        if (profilJoueMpe.isDefense()) {
            return "defence";
        }
        if (profilJoueMpe.isEduction()) {
            return "education";
        }
        if (profilJoueMpe.isSante()) {
            return "health";
        }
        if (profilJoueMpe.isEnvironnement()) {
            return "env-pro";
        }
        if (profilJoueMpe.isServicesGeneraux()) {
            return "gen-pub";
        }
        if (profilJoueMpe.isSecuritePublic()) {
            return "pub-os";
        }
        if (profilJoueMpe.isAffairesEconomiques()) {
            return "econ-aff";
        }
        if (profilJoueMpe.isDeveloppementCollectif()) {
            return "hc-am";
        }
        if (profilJoueMpe.isProtectionSociale()) {
            return "soc-pro";
        }
        if (profilJoueMpe.isLoisirs()) {
            return "rcr";
        }

        return null;

    }

    public Map<String, String> getNamespame(String version) {
        Map<String, String> namespaces = new HashMap<>();
        namespaces = getNamespame(getEformsFolderSdk(version) + "/schemas", namespaces);
        Map<String, String> unique = new HashMap<>();
        Map<String, String> finalNamespaces = namespaces;
        namespaces.keySet().forEach(s -> {
            if (!unique.containsValue(finalNamespaces.get(s))) {
                unique.put(s, finalNamespaces.get(s));
            }
        });
        return unique;

    }

    public Map<String, String> getNamespame(String path, Map<String, String> namespaces) {
        File file = new File(path);
        for (File xsd : file.listFiles()) {
            if (xsd.isDirectory()) {
                getNamespame(xsd.getAbsolutePath(), namespaces);
            } else {
                final Document doc = loadXsdDocument(xsd.getAbsolutePath());
                final Element rootElem = doc.getDocumentElement();
                if (rootElem != null && rootElem.getNodeName().equals("xsd:schema")) {
                    for (int i = 0; i < rootElem.getAttributes().getLength(); i++) {
                        Node item = rootElem.getAttributes().item(i);
                        String nodeName = item.getNodeName();
                        String namespaceURI = item.getNodeValue();
                        if (namespaceURI != null && nodeName.startsWith("xmlns:"))
                            namespaces.put(nodeName.replace("xmlns:", ""), namespaceURI);

                    }
                }
            }
        }
        return namespaces;
    }


    public Document correctSequence(Document xmlDocument, String jaxbPackage) {
        Element documentElement = xmlDocument.getDocumentElement();
        removeEmptyElements(documentElement);
        StringWriter stringWriter = new StringWriter();
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(jaxbPackage);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            Object txLifeType = unmarshaller.unmarshal(new InputSource(new StringReader(getXmlString(xmlDocument))));

            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.marshal(txLifeType, stringWriter);

            return convertStringToXMLDocument(stringWriter.toString());
        } catch (Exception e) {
            log.error("Erreur lors de la correction de la séquence : {}", e.getMessage());
            log.error("XML : {}", getXmlString(xmlDocument));
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage(), e);
        }
    }

    private static void removeEmptyElements(Element documentElement) {
        NodeList childNodes = documentElement.getChildNodes();
        for (int i = childNodes.getLength() - 1; i >= 0; i--) {
            Node item = childNodes.item(i);
            NodeList itemChildNodes = item.getChildNodes();
            if (itemChildNodes.getLength() > 0) {
                removeEmptyElements((Element) item);
            }
            short nodeType = item.getNodeType();
            if ((nodeType == Node.ELEMENT_NODE && itemChildNodes.getLength() == 0) || (nodeType == Node.TEXT_NODE && item.getTextContent().trim().isEmpty())) {
                documentElement.removeChild(item);
            }
        }
    }

    private Document convertStringToXMLDocument(String xmlString) {
        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        //API to obtain DOM Document instance
        DocumentBuilder builder = null;
        try {
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();

            //Parse the content to Document object
            return builder.parse(new InputSource(new StringReader(xmlString)));
        } catch (Exception e) {
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage(), e);
        }
    }


    public Map<String, Object> getSdkI18n(String lang, String version) {
        Locale.setDefault(new Locale(lang, ""));
        Map<String, Object> o = new HashMap<>();
        List<String> bundlesName = new ArrayList<>();
        File file = new File(getEformsFolderSdk(version) + "/translations/");
        if (!file.exists()) {
            return new HashMap<>();
        }
        for (File bundle : file.listFiles()) {
            String name = bundle.getName();
            String suffix = "_" + lang + ".xml";
            if (name.endsWith(suffix)) {
                bundlesName.add(name.replace(suffix, ""));
            }
        }
        log.info("Getting translation from {}", String.join(", ", bundlesName));

        bundlesName.forEach(bundleName -> {
            try {
                getBundle(o, bundleName, null, version, lang);
            } catch (Exception e) {
                log.error("Erreur lors de l'extraction de {} : {}", bundleName, e.getMessage());
            }


        });
        return o;
    }

    public String getEformsFolderSdk(String tag) {
        if (tag == null) tag = schematronConfiguration.getResourcesTag();
        else tag = tag.replace(schematronConfiguration.getPrefixZipFile().toLowerCase(), "");
        getSdk(schematronConfiguration.getDownloadDir(), schematronConfiguration.getResourcesUrl(), schematronConfiguration.getPrefixZipFile(), tag);
        return schematronConfiguration.getDownloadDir() + schematronConfiguration.getPrefixZipFile() + tag;
    }

    protected String getVariantes(DonneesComplementaires donneesComplementaires) {
        if (donneesComplementaires == null) return null;
        if (Boolean.TRUE.equals(donneesComplementaires.getVarianteExigee())) {
            return "required";
        } else {
            if (Boolean.TRUE.equals(donneesComplementaires.getVariantesAutorisees())) {
                return "allowed";
            } else {
                return "not-allowed";
            }
        }
    }
}
