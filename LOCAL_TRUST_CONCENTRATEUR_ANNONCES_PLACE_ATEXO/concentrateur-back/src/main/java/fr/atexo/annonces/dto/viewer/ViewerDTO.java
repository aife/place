package fr.atexo.annonces.dto.viewer;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ViewerDTO {
    private String file;
    private String language;
    private String format;
    private boolean summary;
}
