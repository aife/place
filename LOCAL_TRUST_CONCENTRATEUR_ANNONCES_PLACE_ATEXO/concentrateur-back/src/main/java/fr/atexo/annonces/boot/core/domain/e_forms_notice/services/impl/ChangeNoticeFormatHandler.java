package fr.atexo.annonces.boot.core.domain.e_forms_notice.services.impl;

import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.PreviousNoticeFormatHandler;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.FieldDetails;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNotice;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNoticeMetadata;
import fr.atexo.annonces.boot.entity.EFormsFormulaireEntity;
import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.boot.repository.EformsSurchargeRepository;
import fr.atexo.annonces.boot.ws.eforms.SchematronConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

@Slf4j
public class ChangeNoticeFormatHandler extends GenericSdkService implements PreviousNoticeFormatHandler {


    public ChangeNoticeFormatHandler(EformsSurchargeRepository eformsSurchargeRepository, SchematronConfiguration schematronConfiguration) {
        super(eformsSurchargeRepository, schematronConfiguration);
    }


    @Override
    public Object processGroup(SubNotice notice, Object formulaireObjet, SubNoticeMetadata group, EFormsFormulaireEntity previousFormulaire, EFormsFormulaireEntity newFormulaire, String nodeStop, Map<String, List<PreviousNoticeFormatHandler>> kownHandlers, boolean first) {
        String versionSdk = newFormulaire.getVersionSdk();
        for (SubNoticeMetadata item : group.getContent()) {
            String nodeId = (item.is_repeatable() || item.get_identifierFieldId() != null) ? item.getNodeId() : null;
            List<PreviousNoticeFormatHandler> handler = kownHandlers.get(nodeId);
            if (handler != null) {

                for (PreviousNoticeFormatHandler noticeFormatHandler : handler) {
                    Object value = noticeFormatHandler.processGroup(notice, new Object(), item, previousFormulaire, newFormulaire, nodeId, kownHandlers, false);
                    formulaireObjet = addToObject(formulaireObjet, null, value, nodeStop, nodeId, versionSdk);
                }
            } else if (nodeId != null) {
                if (("field".equals(item.getContentType()))) {
                    formulaireObjet = processElement(item, notice, formulaireObjet, item.getId(), previousFormulaire, newFormulaire, nodeStop);
                }
                if (!CollectionUtils.isEmpty(item.getContent())) {
                    Object o = processGroup(notice, new Object(), item, previousFormulaire, newFormulaire, nodeId, kownHandlers, false);
                    formulaireObjet = addToObject(formulaireObjet, null, o, nodeStop, nodeId, versionSdk);
                }
            } else {
                if (("field".equals(item.getContentType()))) {
                    formulaireObjet = processElement(item, notice, formulaireObjet, item.getId(), previousFormulaire, newFormulaire, nodeStop);
                }
                if (!CollectionUtils.isEmpty(item.getContent())) {
                    formulaireObjet = processGroup(notice, formulaireObjet, item, previousFormulaire, newFormulaire, nodeStop, kownHandlers, false);
                }
            }
        }
        formulaireObjet = setGroupId(formulaireObjet, nodeStop, group, group.getNodeId(), versionSdk, null);

        return formulaireObjet;
    }

    @Override
    public Object processElement(SubNoticeMetadata item, SubNotice notice, Object formulaireObjet, String id, EFormsFormulaireEntity previous, EFormsFormulaireEntity newSaved, String nodeStop) {

        String versionSdk = newSaved.getVersionSdk();
        FieldDetails fieldDetails = getFields(versionSdk).getFields().stream().filter(fieldDetails1 -> fieldDetails1.getId().equals(id)).findFirst().orElse(null);
        if (fieldDetails == null || getMandatoryForbiddenDetailsValue(newSaved.getIdNotice(), fieldDetails.getForbidden())) {
            return formulaireObjet;
        }
        String parentNodeId = fieldDetails.getParentNodeId();
        boolean repeatable = item.is_repeatable();
        if (id.equals("BT-758-notice")) {
            String previousUuid;
            SuiviAnnonce suiviAnnonce = previous.getSuiviAnnonce();
            if (suiviAnnonce != null && StringUtils.hasText(suiviAnnonce.getLienPublication())) {
                String lienPublication = suiviAnnonce.getLienPublication();
                previousUuid = lienPublication.substring(lienPublication.indexOf("TED:NOTICE:") + 11, lienPublication.indexOf(":TEXT:"));
            } else if (StringUtils.hasText(previous.getPublicationId())) {
                previousUuid = removeLeadingZeros(previous.getPublicationId());
            } else if (StringUtils.hasText(previous.getNoDocExt())) {
                previousUuid = previous.getNoDocExt();
            } else {
                previousUuid = previous.getUuid();
            }

            if (!StringUtils.hasText(previousUuid)) {
                return formulaireObjet;
            }
            return addToObject(formulaireObjet, id, repeatable ? List.of(previousUuid) : previousUuid, nodeStop, parentNodeId, versionSdk);
        } else {

            log.info("Pas de correspondance change pour {} le group ND-Change", id);
        }
        return formulaireObjet;
    }

    public String removeLeadingZeros(String input) {
        if (!StringUtils.hasText(input)) {
            return null;
        }
        // Remove leading zeros using a loop
        int i = 0;
        while (i < input.length() && input.charAt(i) == '0') {
            i++;
        }

        return input.substring(i);

    }
}
