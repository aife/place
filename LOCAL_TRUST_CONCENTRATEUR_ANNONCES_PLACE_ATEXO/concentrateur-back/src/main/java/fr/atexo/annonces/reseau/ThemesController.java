package fr.atexo.annonces.reseau;

import fr.atexo.annonces.boot.entity.Organisme;
import fr.atexo.annonces.service.AgentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/rest/v2/themes")
@Slf4j
public class ThemesController {

    private final RestTemplate atexoRestTemplate;
    private final AgentService agentService;

    public ThemesController(RestTemplate atexoRestTemplate, AgentService agentService) {
        this.atexoRestTemplate = atexoRestTemplate;
        this.agentService = agentService;
    }


    @GetMapping("/css/mpe-new-client.css")
    public String getMpeCss(@RequestHeader("Authorization") String token) {
        var agent = agentService.findByToken(token.replace("Bearer ", ""));
        if (agent == null) {
            return null;
        }
        Organisme organisme = agent.getOrganisme();
        if (organisme == null) {
            return null;
        }
        try {
            return atexoRestTemplate.getForObject(organisme.getPlateformeUrl() + "/themes/css/mpe-new-client.css", String.class);
        } catch (Exception e) {
            log.error("Erreur lors de la récupération du css", e);
            return null;
        }
    }


}
