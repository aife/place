package fr.atexo.annonces.reseau;

import fr.atexo.annonces.boot.configuration.http_logging.LogEntryExit;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.IEFormsNoticeServices;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.EformsSurchargePatch;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.FieldDetails;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNotice;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.XmlStructureDetails;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.services.IEFormsSdkService;
import fr.atexo.annonces.boot.ws.eforms.SchematronConfiguration;
import fr.atexo.annonces.dto.EformsSurchargeDTO;
import fr.atexo.annonces.service.AgentService;
import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/rest/v2/eforms-sdk")
@Slf4j
public class EFormsSdkResource {

    private final IEFormsSdkService ieFormsSdkService;
    private final IEFormsNoticeServices ieFormsNoticeServices;
    private final AgentService agentService;
    private final SchematronConfiguration schematronConfiguration;

    @Value("${annonces.admin.plateforme}")
    private String plateformeAdministrateur;

    public EFormsSdkResource(IEFormsSdkService ieFormsSdkService, IEFormsNoticeServices ieFormsNoticeServices, AgentService agentService, SchematronConfiguration schematronConfiguration) {
        this.ieFormsSdkService = ieFormsSdkService;
        this.ieFormsNoticeServices = ieFormsNoticeServices;
        this.agentService = agentService;
        this.schematronConfiguration = schematronConfiguration;
    }

    @GetMapping("/sdk-i18n/{lang}")
    @LogEntryExit(skip = true)
    public Map<String, Object> getSdkI18n(@PathVariable String lang) {
        Map<String, Object> sdkI18n = ieFormsSdkService.getSdkI18n(lang, schematronConfiguration.getLibelleTag());
        Map<String, Object> sdkI18nEn = ieFormsSdkService.getSdkI18n("en", schematronConfiguration.getLibelleTag());
        sdkI18nEn.forEach((s, o) -> {
            if (!sdkI18n.containsKey(s)) {
                sdkI18n.put(s, o);
            }
        });
        return sdkI18n;
    }

    @GetMapping("/surcharge-i18n/{lang}")
    @LogEntryExit(skip = true)
    public Map<String, Object> getSurchargeI18n(@PathVariable String lang, @RequestHeader(value = "Authorization", required = false) String token) {
        var agent = agentService.findByToken(token.replace("Bearer ", ""));
        if (agent == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucun agent est rattaché à ce token");
        }
        return ieFormsSdkService.getSurchargeI18n(agent.getOrganisme().getId(), lang);
    }

    @GetMapping("/i18n/{lang}")
    @LogEntryExit(skip = true)
    public Object getI18n(@PathVariable String lang) {
        return ieFormsSdkService.getI18n(lang);
    }

    @GetMapping("{version}/fields/{id}")
    @LogEntryExit(skip = true)
    public FieldDetails getFieldById(@PathVariable String id, @PathVariable String version) {
        return ieFormsSdkService.getFieldById(id, version);
    }

    @GetMapping("{version}/nodes/{id}")
    @LogEntryExit(skip = true)
    public List<XmlStructureDetails> getNodeIdsById(@PathVariable String id, @PathVariable String version) {
        return ieFormsSdkService.getNodeIdsById(id, version);
    }

    @GetMapping("{version}/codes/{id}")
    @LogEntryExit(skip = true)
    public Map<String, Set<String>> getCodeById(@RequestParam String idPlatform, @PathVariable String id, @PathVariable String version, @RequestParam(required = false) String lang, @RequestParam(required = false) String search) {
        return ieFormsSdkService.getCodeById(id, lang, search, idPlatform, version);
    }

    @GetMapping("{version}/{id}/options")
    public Map<String, Set<String>> getFieldsOptions(@RequestParam String idPlatform, @PathVariable String id, @PathVariable String version, @RequestParam(required = false) String search, @RequestParam(required = false) String lang) {
        return ieFormsSdkService.getFieldsOptions(id, idPlatform, version, search, lang);
    }


    @GetMapping("{plateforme}/{noticeId}/structure")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform)")
    public SubNotice getSubNoticeStructure(@PathVariable String noticeId, @PathVariable String plateforme, @RequestParam String idPlatform) {
        if (!plateformeAdministrateur.equals(idPlatform)) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Vous n'êtes pas autorisé à consulter cette plateforme");
        }
        return ieFormsNoticeServices.getSubNotice(noticeId, schematronConfiguration.getResourcesTag(), plateforme);
    }

    @GetMapping("{noticeId}/structure")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform)")
    public SubNotice getSubNoticeStructureAtexo(@PathVariable String noticeId, @RequestParam String idPlatform) {
        if (!plateformeAdministrateur.equals(idPlatform)) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Vous n'êtes pas autorisé à consulter cette plateforme");
        }
        return ieFormsNoticeServices.getSubNotice(noticeId, schematronConfiguration.getResourcesTag(), null);
    }

    @PatchMapping("/fields/{plateforme}/surcharge/{id}")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform)")
    public EformsSurchargeDTO surchargerPlateforme(@PathVariable String id, @PathVariable String plateforme, @RequestParam String idPlatform, @RequestBody EformsSurchargePatch patch) {
        if (!plateformeAdministrateur.equals(idPlatform)) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Vous n'êtes pas autorisé à consulter cette plateforme");
        }
        return ieFormsNoticeServices.surchargerPlateforme(plateforme, id, schematronConfiguration.getResourcesTag(), patch);
    }

    @PatchMapping("/fields/surcharge/{id}")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform)")
    public EformsSurchargeDTO surchargerAtexo(@PathVariable String id, @RequestParam String idPlatform, @RequestBody EformsSurchargePatch patch) {
        if (!plateformeAdministrateur.equals(idPlatform)) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Vous n'êtes pas autorisé à consulter cette plateforme");
        }
        return ieFormsNoticeServices.surchargerAtexo(id, schematronConfiguration.getResourcesTag(), patch);
    }


}
