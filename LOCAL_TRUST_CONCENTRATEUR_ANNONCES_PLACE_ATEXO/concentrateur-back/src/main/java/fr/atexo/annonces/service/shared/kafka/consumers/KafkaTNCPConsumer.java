package fr.atexo.annonces.service.shared.kafka.consumers;

import fr.atexo.annonces.config.EFormsHelper;
import fr.atexo.annonces.dto.tncp.StatutEnum;
import fr.atexo.annonces.dto.tncp.TncpSuiviNotification;
import fr.atexo.annonces.service.TNCPService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

@ConditionalOnProperty(value = "kafka.enabled")
@Component
@RequiredArgsConstructor
@Slf4j
public class KafkaTNCPConsumer {

    private final TNCPService tncpService;

    @KafkaListener(topics = "${kafka.topic.notification.reception.nom}", groupId = "${kafka.topic.notification.reception.groupe:concpub}", containerFactory = "tncpKafkaListenerContainerFactory")
    public void consume(String reponseString) {
        log.info("Message reçu du TNCP {}", reponseString);
        TncpSuiviNotification reponseTNCP = EFormsHelper.getObjectFromString(reponseString, TncpSuiviNotification.class);
        if (reponseTNCP == null) {
            log.error("Erreur de désérialisation de la réponse TNCP");
            return;
        }
        log.info("Traitement de la réponse TNCP : {}", reponseTNCP.getUuid());
        if (StatutEnum.FINI.equals(reponseTNCP.getStatut())) {
            String base64 = reponseTNCP.getObjetDestination();
            String response = new String(Base64Utils.decodeFromString(base64));
            tncpService.synchronize(response);
        } else {
            log.error("Erreur de publication TNCP : {}", reponseTNCP.getMessage());
        }
    }
}
