package fr.atexo.annonces.mapper;

import fr.atexo.annonces.boot.entity.ConsultationFiltre;
import fr.atexo.annonces.dto.ConsultationFiltreDTO;
import org.mapstruct.Mapper;

/**
 * Mapper entre l'entité Support et son DTO
 */
@Mapper(uses = {StringAndListStringMapper.class})
public interface ConsultationFiltreMapper {

    ConsultationFiltreDTO mapToDto(ConsultationFiltre entity);

    ConsultationFiltre mapToEntity(ConsultationFiltreDTO dto);


}
