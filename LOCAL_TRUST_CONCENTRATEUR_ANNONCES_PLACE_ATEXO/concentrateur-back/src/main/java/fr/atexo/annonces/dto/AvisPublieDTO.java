package fr.atexo.annonces.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;


@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AvisPublieDTO implements Serializable {

    private int id;

    private String idAvis;

    private String type;

    private String uidConsultation;

    private String loginBoamp;

    private String passwordBoamp;

    private String uidAgentmetteur;

    private String uidPlateforme;

    private String service;

    private String organisme;

    private LocalDateTime dateEnvoi;

    private LocalDateTime dateMaj;

    private String xmlContexte;

    private PieceJointeDTO annonce;

    private String xmlFichierComplementaire;

    private String nomFichierAnnonce;

    private String nomFichierComplementaire;

    private String suivi;

    private String referenceUtilisateur;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdAvis() {
        return idAvis;
    }

    public void setIdAvis(String idAvis) {
        this.idAvis = idAvis;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUidConsultation() {
        return uidConsultation;
    }

    public void setUidConsultation(String uidConsultation) {
        this.uidConsultation = uidConsultation;
    }

    public String getLoginBoamp() {
        return loginBoamp;
    }

    public void setLoginBoamp(String loginBoamp) {
        this.loginBoamp = loginBoamp;
    }

    public String getPasswordBoamp() {
        return passwordBoamp;
    }

    public void setPasswordBoamp(String passwordBoamp) {
        this.passwordBoamp = passwordBoamp;
    }

    public String getUidAgentmetteur() {
        return uidAgentmetteur;
    }

    public void setUidAgentmetteur(String uidAgentmetteur) {
        this.uidAgentmetteur = uidAgentmetteur;
    }

    public String getUidPlateforme() {
        return uidPlateforme;
    }

    public void setUidPlateforme(String uidPlateforme) {
        this.uidPlateforme = uidPlateforme;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getOrganisme() {
        return organisme;
    }

    public void setOrganisme(String organisme) {
        this.organisme = organisme;
    }

    public LocalDateTime getDateEnvoi() {
        return dateEnvoi;
    }

    public void setDateEnvoi(LocalDateTime dateEnvoi) {
        this.dateEnvoi = dateEnvoi;
    }

    public LocalDateTime getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(LocalDateTime dateMaj) {
        this.dateMaj = dateMaj;
    }

    public String getXmlContexte() {
        return xmlContexte;
    }

    public void setXmlContexte(String xmlContexte) {
        this.xmlContexte = xmlContexte;
    }

    public PieceJointeDTO getAnnonce() {
        return annonce;
    }

    public void setAnnonce(PieceJointeDTO annonce) {
        this.annonce = annonce;
    }

    public String getXmlFichierComplementaire() {
        return xmlFichierComplementaire;
    }

    public void setXmlFichierComplementaire(String xmlFichierComplementaire) {
        this.xmlFichierComplementaire = xmlFichierComplementaire;
    }

    public String getNomFichierAnnonce() {
        return nomFichierAnnonce;
    }

    public void setNomFichierAnnonce(String nomFichierAnnonce) {
        this.nomFichierAnnonce = nomFichierAnnonce;
    }

    public String getNomFichierComplementaire() {
        return nomFichierComplementaire;
    }

    public void setNomFichierComplementaire(String nomFichierComplementaire) {
        this.nomFichierComplementaire = nomFichierComplementaire;
    }

    public String getSuivi() {
        return suivi;
    }

    public void setSuivi(String suivi) {
        this.suivi = suivi;
    }

    public String getReferenceUtilisateur() {
        return referenceUtilisateur;
    }

    public void setReferenceUtilisateur(String referenceUtilisateur) {
        this.referenceUtilisateur = referenceUtilisateur;
    }
}
