package fr.atexo.annonces.service;

import com.atexo.annonces.commun.mpe.ConsultationMpe;
import com.atexo.annonces.commun.mpe.OrganismeMpe;
import com.atexo.annonces.commun.mpe.ServiceMpe;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import eu.europa.publications.resource.schema.ted.r2_0_9.reception.TedEsenders;
import fr.atexo.annonces.boot.entity.*;
import fr.atexo.annonces.boot.entity.tncp.SuiviAvisPublie;
import fr.atexo.annonces.boot.entity.tncp.SuiviStatut;
import fr.atexo.annonces.boot.repository.*;
import fr.atexo.annonces.boot.ws.mol.MolWS;
import fr.atexo.annonces.boot.ws.mpe.model.ConsultationContexte;
import fr.atexo.annonces.config.EFormsHelper;
import fr.atexo.annonces.dto.AvisPublieDTO;
import fr.atexo.annonces.dto.ConfigurationConsultationDTO;
import fr.atexo.annonces.dto.ListeAnnonces.ListeAnnonces;
import fr.atexo.annonces.dto.SuiviDTO;
import fr.atexo.annonces.dto.cmplt.CMPLT;
import fr.atexo.annonces.dto.iactx.IACTX;
import fr.atexo.annonces.dto.mol.*;
import fr.atexo.annonces.dto.publication.AVISEMIS;
import fr.atexo.annonces.mapper.AvisPublieMapper;
import fr.atexo.annonces.modeles.StatutSuiviAnnonceEnum;
import fr.atexo.annonces.modeles.SupportPublicationEnum;
import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ConcentraturException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import liquibase.repackaged.org.apache.commons.text.StringEscapeUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.net.util.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.isNumeric;

@Component
@Slf4j
public class MolServiceImpl implements MolService {

    private final MolWS molWS;
    private final AvisPublieRepository avisPublieRepository;
    private final SuiviService suiviService;
    private final PlateformeConfigurationRepository configurationRepository;
    private final SuiviAnnonceRepository suiviAnnonceRepository;
    private final PieceJointeService pieceJointeService;
    private final AgentService agentService;
    private final OrganismeService organismeService;
    private final SupportRepository supportRepository;
    private final OffreRepository offreRepository;
    private final AvisPublieMapper avisPublieMapper;

    @Value("${external-apis.mol.key}")
    private String molSecretKey;

    public MolServiceImpl(MolWS molWS, AvisPublieRepository avisPublieRepository, SuiviService suiviService, PlateformeConfigurationRepository configurationRepository, SuiviAnnonceRepository suiviAnnonceRepository, PieceJointeService pieceJointeService, AgentService agentService, OrganismeService organismeService, SupportRepository supportRepository, OffreRepository offreRepository, AvisPublieMapper avisPublieMapper) {
        this.molWS = molWS;
        this.avisPublieRepository = avisPublieRepository;
        this.suiviService = suiviService;
        this.configurationRepository = configurationRepository;
        this.suiviAnnonceRepository = suiviAnnonceRepository;
        this.pieceJointeService = pieceJointeService;
        this.agentService = agentService;
        this.organismeService = organismeService;
        this.supportRepository = supportRepository;
        this.offreRepository = offreRepository;
        this.avisPublieMapper = avisPublieMapper;
    }

    @Override
    public String generateXmlResponse(String request, String soap) {
        try {

            String xml = request.substring(request.indexOf("<xmlRequest"));
            String endXml = "</xmlRequest>";
            xml = xml.substring(0, xml.indexOf(endXml)).split(">")[1];
            String decode = new String(Base64.decodeBase64(xml.getBytes()));
            JacksonXmlModule module = new JacksonXmlModule();
            module.setDefaultUseWrapper(false);
            XmlMapper xmlMapper = new XmlMapper(module);
            xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            log.info("Suivi de {} : {}", soap, decode);
            Request result = xmlMapper.readValue(decode, Request.class);
            List<AvisPublie> publies = getAvisPubliesFromRequest(result);
            Envelope envelope = new Envelope();
            Body body = new Body();
            GenerateXmlResponseResponse response = new GenerateXmlResponseResponse();
            Response value1 = new Response();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            publies.forEach(avisPublie -> {
                value1.getAvisConcentrateur().add(Response.AvisConcentrateur.builder().idAvis(avisPublie.getIdAvis()).dateEnvoi(avisPublie.getDateEnvoi().format(formatter)).nomFichierAnnonce(Base64.encodeBase64StringUnChunked(avisPublie.getNomFichierAnnonce().getBytes())).organisme(avisPublie.getOrganisme()).referenceUtilisateur(avisPublie.getReferenceUtilisateur()).suivi(avisPublie.getSuivi()).uidConsultation(avisPublie.getUidConsultation()).build());
            });
            response.setReturn(xmlMapper.writeValueAsString(value1));
            body.setGenerateXmlResponseRespone(response);
            envelope.setBody(body);
            return getString(envelope);

        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    @Override
    public List<Response.AvisConcentrateur> getPublicationSynchro(Request request) {
        List<AvisPublie> publies = getAvisPubliesFromRequest(request);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return publies.stream().map(avisPublie -> Response.AvisConcentrateur.builder().idAvis(avisPublie.getIdAvis()).dateEnvoi(avisPublie.getDateEnvoi().format(formatter)).nomFichierAnnonce(Base64.encodeBase64StringUnChunked(avisPublie.getNomFichierAnnonce().getBytes())).organisme(avisPublie.getOrganisme()).referenceUtilisateur(avisPublie.getReferenceUtilisateur()).suivi(avisPublie.getSuivi()).uidConsultation(avisPublie.getUidConsultation()).build()).collect(Collectors.toList());
    }

    @Override
    public List<AvisPublieDTO> getAvisPublies(Request request) {
        return avisPublieMapper.avisPubliesToAvisPublieDTOs(this.getAvisPubliesFromRequest(request));

    }

    @Override
    public SuiviDTO envoyer(ConfigurationConsultationDTO config, SuiviAvisPublie suivi, TedEsenders tedEsenders, String token, boolean rie) {
        var agent = agentService.findByToken(token);
        if (config.getContexte() != null && config.getContexte().getOrganismeConsultation() != null) {
            AcheteurEntity acheteur = suivi.getAcheteur();
            String xmlSignature = this.prepareIACTX(acheteur, agent, config.getContexte());
            String xmlToken;
            suivi.setEnvoi(Instant.now());
            suivi.setContexteConsultation(EFormsHelper.getString(config.getContexte()));
            try {

                log.info("xmltoken envoyé au MOL" + xmlSignature);
                xmlToken = org.apache.commons.net.util.Base64.encodeBase64StringUnChunked(xmlSignature.getBytes(StandardCharsets.ISO_8859_1));
                suivi.setToken(xmlToken);
                String textToHash = xmlToken + this.molSecretKey;
                byte[] cleBytes = textToHash.getBytes(StandardCharsets.ISO_8859_1);
                byte[] hashed = DigestUtils.getSha1Digest().digest(cleBytes);
                xmlSignature = Base64.encodeBase64StringUnChunked(hashed);
                suivi.setSignatureToken(xmlSignature);


                ListeAnnonces listeAnnonces = this.prepareListeAnnonces(acheteur, config, agent);
                JAXBContext jaxbContextIactx = JAXBContext.newInstance(IACTX.class);
                Marshaller jaxbMarshallerIactx = jaxbContextIactx.createMarshaller();
                jaxbMarshallerIactx.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                jaxbContextIactx = JAXBContext.newInstance(ListeAnnonces.class);
                jaxbMarshallerIactx = jaxbContextIactx.createMarshaller();
                jaxbMarshallerIactx.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                StringWriter swIannonce = new StringWriter();
                jaxbMarshallerIactx.marshal(listeAnnonces, swIannonce);
                log.info("xmlboamp envoyé au MOL" + swIannonce);
                String xmlBoamp = org.apache.commons.net.util.Base64.encodeBase64StringUnChunked(swIannonce.toString().getBytes(StandardCharsets.ISO_8859_1));
                suivi.setAvis(xmlBoamp);

                ResponseEntity<String> response = molWS.openMOL(xmlToken, xmlSignature, xmlBoamp);
                URI redirection = response.getHeaders().getLocation();
                log.info("URL récupérée depuis MOL = " + redirection);
                suivi.setRedirection(redirection.toString());
                suivi.setStatut(SuiviStatut.ENVOYE);
            } catch (Exception e) {
                log.error("Erreur pendant génération xmlboamp pour MOL", e);
                suivi.setStatut(SuiviStatut.ERREUR);
            }

            return suiviService.sauvegarder(suivi);
        } else {
            throw new AtexoException(ExceptionEnum.MANDATORY, "Données de l'organisme dans ConfigurationConsultationDTO manquantes.");
        }

    }

    private ListeAnnonces prepareListeAnnonces(AcheteurEntity acheteur, ConfigurationConsultationDTO config, Agent agentConnecte) {
        ListeAnnonces listeAnnonces = new ListeAnnonces();
        ListeAnnonces.Emetteur emetteur = new ListeAnnonces.Emetteur();
        ListeAnnonces.Generateur generateur = new ListeAnnonces.Generateur();
        generateur.setIdAppli("MPE3.000");
        generateur.setVersionAppli("MPE 3.0");
        emetteur.setLogin(acheteur.getLogin());
        emetteur.setMel(acheteur.getEmail());
        emetteur.setPassword(acheteur.getPassword());
        listeAnnonces.setEmetteur(emetteur);
        listeAnnonces.setGenerateur(generateur);
        ListeAnnonces.Ann ann = new ListeAnnonces.Ann();
        ListeAnnonces.Ann.Organisme annOrganisme = new ListeAnnonces.Ann.Organisme();
        ListeAnnonces.Ann.Organisme.CorrespondantPRM correspondant = new ListeAnnonces.Ann.Organisme.CorrespondantPRM();
        if (config.getContexte().getAgent() != null) {
            ann.setIdAnnEmetteur(agentConnecte.getIdMpe());
            correspondant.setNom(agentConnecte.getNom());
            correspondant.setPren(agentConnecte.getPrenom());
            correspondant.setFonc("");
        }
        annOrganisme.setCorrespondantPRM(correspondant);
        ann.setOrganisme(annOrganisme);
        ListeAnnonces.Ann.Description description = new ListeAnnonces.Ann.Description();
        ListeAnnonces.Ann.Description.CPV cpv = new ListeAnnonces.Ann.Description.CPV();
        ListeAnnonces.Ann.Description.CPV.ObjetPrincipal principal = new ListeAnnonces.Ann.Description.CPV.ObjetPrincipal();
        ListeAnnonces.Ann.Caracteristiques caracteristiques = new ListeAnnonces.Ann.Caracteristiques();
        ListeAnnonces.Ann.Delais delais = new ListeAnnonces.Ann.Delais();
        ListeAnnonces.Ann.Renseignements renseignements = new ListeAnnonces.Ann.Renseignements();
        ListeAnnonces.Ann.NatureMarche natureMarche = new ListeAnnonces.Ann.NatureMarche();
        if (config.getContexte().getConsultation() != null) {
            description.setObjet(config.getContexte().getConsultation().getObjet());
            principal.setClassPrincipale(config.getContexte().getConsultation().getCodeCpvPrincipal());
            caracteristiques.setPrincipales(config.getContexte().getConsultation().getIntitule());
            delais.setReceptionOffres(String.valueOf(config.getContexte().getConsultation().getDateLimiteRemiseOffres()));
            renseignements.setIdMarche(config.getContexte().getConsultation().getReference());
            if (config.getContexte().getConsultation().getNaturePrestation() != null) {
                String nature = config.getContexte().getConsultation().getNaturePrestation();
                if (nature != null && !nature.isEmpty()) {
                    char lastCharat = nature.charAt(nature.length() - 1);
                    switch (lastCharat) {
                        case '1' -> natureMarche.setTravaux("");
                        case '2' -> natureMarche.setFournitures("");
                        case '3' -> natureMarche.setServices("");
                        default -> {
                            natureMarche.setServices(null);
                            natureMarche.setTravaux(null);
                            natureMarche.setFournitures(null);
                        }
                    }
                }
            }
        }
        cpv.setObjetPrincipal(principal);
        description.setCPV(cpv);
        ann.setDescription(description);
        ann.setCaracteristiques(caracteristiques);
        ann.setDelais(delais);
        ann.setRenseignements(renseignements);
        ann.setNatureMarche(natureMarche);
        listeAnnonces.setAnn(ann);
        return listeAnnonces;
    }

    private String prepareIACTX(AcheteurEntity acheteur, Agent agentConnecte, ConsultationContexte contexte) {
        ConsultationMpe consultation = contexte.getConsultation();
        var consultationId = StringUtils.hasText(consultation.getReference()) ? consultation.getReference() : consultation.getId();
        OrganismeMpe organisme = contexte.getOrganismeAgent();
        ServiceMpe service = contexte.getServiceAgent();
        String plateformeUuid = agentConnecte.getOrganisme().getPlateformeUuid();
        var prefixConsultation = plateformeUuid + "#" + organisme.getAcronyme() + "#" + acheteur.getLogin();
        String xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n" + "<IACTX><TSTP>CONST_DATE</TSTP><ACT>0</ACT><IDTF><LGN>CONST_LOGIN</LGN><PWD>CONST_PWD</PWD><PRF>0</PRF><WKF>CONST_HABILITATION</WKF>CONST_MPS</IDTF><CTX><IDC>CONST_UID_CONSULATATION</IDC><IDCHS><IDCH><![CDATA[CONST_UID_CONSULATATION]]></IDCH></IDCHS><TIT>CONST_TITRE_CONS</TIT><REF_PP>CONST_REF_USER</REF_PP><DMT>0</DMT><URL_CSL><![CDATA[CONST_URL_ACCES_DIRECT_CONS]]></URL_CSL><LOGO_CSL><![CDATA[marches-publics.gouv.fr.gif]]></LOGO_CSL><PROV>CONST_PROV</PROV><THEME_GRAPHIQUE>CONST_THEME_GRAPHIQUE</THEME_GRAPHIQUE><IDCL></IDCL><PERS><CIV>M.</CIV><NOM>CONST_NOM_AGENT</NOM><PRENOM>CONST_PRENOM_AGENT</PRENOM><IDPM>CONST_IDSERVICE</IDPM><IDPMM>CONST_ID_ORGANISME</IDPMM><AG><![CDATA[0]]></AG><AGSC><![CDATA[0]]></AGSC><DEL><![CDATA[0]]></DEL><RS>CONST_LIBELLE_SERVICE</RS><ADR>CONST_ADRESSE_SERVICE</ADR><ADR2>CONST_ADRESSE2_SERVICE</ADR2><CP>CONST_CP_SERVICE</CP><VIL>CONST_VILLE_SERVICE</VIL><EMAIL>CONST_EMAIL_AGENT</EMAIL><TPH>CONST_TEL_AGENT</TPH><SRT>CONST_SIRET_SERVICE</SRT><OKEMAILGM>0</OKEMAILGM><OKEMAILPART>0</OKEMAILPART></PERS></CTX><CMPLT><URLPF><![CDATA[CONST_URL_PF]]></URLPF><UID_PF><![CDATA[CONST_UID_PF]]></UID_PF><ORGANISME><![CDATA[CONST_ORGANISME]]></ORGANISME></CMPLT></IACTX>";


        xml = xml.replace("CONST_DATE", LocalDateTime.now().plusYears(1L).format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
        xml = xml.replace("CONST_LOGIN", prefixConsultation + "##" + agentConnecte.getIdMpe());
        xml = xml.replace("CONST_PWD", prefixConsultation + "##" + agentConnecte.getIdMpe());
        xml = xml.replace("CONST_UID_CONSULATATION", prefixConsultation + "##" + consultationId);
        xml = xml.replace("CONST_NOM_AGENT", StringEscapeUtils.escapeXml11(agentConnecte.getNom()));
        xml = xml.replace("CONST_PRENOM_AGENT", StringEscapeUtils.escapeXml11(agentConnecte.getPrenom()));
        xml = xml.replace("CONST_EMAIL_AGENT", agentConnecte.getEmail());
        if (agentConnecte.getTelephone() != null) {
            xml = xml.replace("CONST_TEL_AGENT", agentConnecte.getTelephone());
        } else {
            xml = xml.replace("CONST_TEL_AGENT", "0000000000");
        }
        if (agentConnecte.getService() != null) {
            xml = xml.replace("CONST_IDSERVICE", String.valueOf(agentConnecte.getService().getId()));
        } else {
            xml = xml.replace("CONST_IDSERVICE", "0");
        }
        xml = xml.replace("CONST_ID_ORGANISME", organisme.getAcronyme());
        String libelleService = null;
        String adresseService = null;
        String adresseSuiteService = null;
        String cpService = null;
        String serviceVille = null;
        String serviceSiren = null;
        if (service != null) {
            libelleService = service.getLibelle();
            adresseService = service.getAdresse();
            adresseSuiteService = service.getAdresseSuite();
            cpService = service.getCodePostal();
            serviceVille = service.getVille();
            serviceSiren = service.getSiren();
        } else {
            libelleService = organisme.getDenomination();
            adresseService = organisme.getAdresse();
            adresseSuiteService = organisme.getAdresse2();
            cpService = organisme.getCodePostal();
            serviceVille = organisme.getVille();
            serviceSiren = organisme.getSiren();
        }

        if (libelleService != null) {
            xml = xml.replace("CONST_LIBELLE_SERVICE", StringEscapeUtils.escapeXml11(libelleService));

        } else {
            xml = xml.replace("CONST_LIBELLE_SERVICE", "A RENSEIGNER");
        }
        if (adresseService != null) {
            xml = xml.replace("CONST_ADRESSE_SERVICE", StringEscapeUtils.escapeXml11(adresseService));

        } else {
            xml = xml.replace("CONST_ADRESSE_SERVICE", "A RENSEIGNER");
        }
        if (adresseSuiteService != null) {
            xml = xml.replace("CONST_ADRESSE2_SERVICE", StringEscapeUtils.escapeXml11(adresseSuiteService));

        } else {
            xml = xml.replace("CONST_ADRESSE2_SERVICE", "A RENSEIGNER");
        }
        if (cpService != null) {
            xml = xml.replace("CONST_CP_SERVICE", cpService);

        } else {
            xml = xml.replace("CONST_CP_SERVICE", "00000");
        }

        if (serviceVille != null) {
            xml = xml.replace("CONST_VILLE_SERVICE", StringEscapeUtils.escapeXml11(serviceVille));

        } else {
            xml = xml.replace("CONST_VILLE_SERVICE", "A RENSEIGNER");
        }

        if (serviceSiren != null) {
            xml = xml.replace("CONST_SIRET_SERVICE", serviceSiren);

        } else {
            xml = xml.replace("CONST_SIRET_SERVICE", "00000000000000");
        }
        xml = xml.replace("CONST_REF_USER", consultation.getReference());
        xml = xml.replace("CONST_TITRE_CONS", StringEscapeUtils.escapeXml11(consultation.getIntitule()));
        xml = xml.replace("CONST_UID_PF", agentConnecte.getOrganisme().getPlateformeUuid());


        if (consultation.getUrlConsultation() != null)
            xml = xml.replace("CONST_URL_ACCES_DIRECT_CONS", consultation.getUrlConsultation());
        xml = xml.replace("CONST_HABILITATION", "1");
        xml = xml.replace("CONST_THEME_GRAPHIQUE", "0");

        if (acheteur.getMoniteurProvenance() != null) {
            xml = xml.replace("CONST_PROV", String.valueOf(acheteur.getMoniteurProvenance()));
        } else {
            PlateformeConfiguration conf = configurationRepository.findByIdPlateforme(plateformeUuid);
            if (conf != null && conf.getProv() != null) {
                xml = xml.replace("CONST_PROV", String.valueOf(conf.getProv()));
            } else {
                xml = xml.replace("CONST_PROV", "90");
            }
        }

        xml = xml.replace("CONST_ORGANISME", agentConnecte.getOrganisme().getAcronymeOrganisme());

        xml = xml.replace("CONST_MPS", "");
        return xml;
    }


    private List<AvisPublie> getAvisPubliesFromRequest(Request request) {
        if (request.getDateDebut() == null && request.getDateFin() == null) {
            throw new AtexoException(ExceptionEnum.MANDATORY, "Les dates ne doivent pas être null");
        }
        LocalDateTime begin = LocalDateTime.ofInstant(request.getDateDebut().toInstant(), ZoneId.systemDefault());
        LocalDateTime end = LocalDateTime.ofInstant(request.getDateFin().toInstant(), ZoneId.systemDefault());
        return avisPublieRepository.findByUidPlateformeAndDateEnvoiIsBetween(request.getUidPf(), begin, end);
    }

    @Override
    public List<AVISEMIS.AVIS> getAvisToSynchronize(Date dateDebut, Date dateFin) {
        if (dateDebut == null && dateFin == null) {
            throw new AtexoException(ExceptionEnum.MANDATORY, "Les dates ne doivent pas être null");
        }
        JacksonXmlModule module = new JacksonXmlModule();
        module.setDefaultUseWrapper(false);
        XmlMapper xmlMapper = new XmlMapper(module);
        xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        AVISEMIS suiviMol = molWS.getSuiviMol(dateDebut, dateFin);
        List<AVISEMIS.AVIS> list = suiviMol.getAVIS();
        return list.stream().filter(avis -> avis.getREFCONSULTATION() != null).filter(avis -> avis.getREFCONSULTATION().split("#").length > 0).filter(avis -> {
            String[] refs = avis.getREFCONSULTATION().split("#");
            String idConsultationMpe = refs[refs.length - 1];
            String idPlatform = refs[0];
            return isNumeric(idConsultationMpe) && configurationRepository.existsByIdPlateforme(idPlatform);
        }).collect(Collectors.toList());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void synchronize(AVISEMIS.AVIS avis, SuiviAnnonce suiviAnnonce, AVISEMIS.AVIS.SUPPORT support) {

        JacksonXmlModule module = new JacksonXmlModule();
        module.setDefaultUseWrapper(false);
        XmlMapper xmlMapper = new XmlMapper(module);
        xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        this.updateAnnonces(avis, suiviAnnonce);

        SuiviAvisPublie saved = suiviService.rechercher(avis.getID()).orElse(null);
            if (saved == null) {
                log.info("Aucun avis pour {}", avis.getID());
                saved = SuiviAvisPublie.with().statut(SuiviStatut.CREE).uuid(avis.getID()).creation(Instant.now()).suiviAnnonce(suiviAnnonce).build();
            }
            log.info("MAJ de l'avis pour {}", avis.getID());
            String suivi;
            try {
                suivi = xmlMapper.writeValueAsString(avis);
            } catch (JsonProcessingException e) {
                log.error(e.getMessage());
                throw new ConcentraturException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage());
            }
        if (suivi != null) saved.setLastNotification(suivi);
        suiviService.sauvegarder(saved);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public List<SuiviAnnonce> getAnnonces(AVISEMIS.AVIS avis, String idPlatform, String idConsultationMpe, String organisme, AVISEMIS.AVIS.SUPPORT support) {
        SuiviAnnonce annonce = suiviAnnonceRepository.findByNumeroAvisAndIdPlatformAndIdConsultationAndSupportCodeGroupeIn(avis.getID(), idPlatform, Integer.valueOf(idConsultationMpe), List.of("BOAMPJOUE", "MOL"));
        if (annonce != null) {
            return List.of(annonce);
        }
        List<SuiviAnnonce> suiviAnnonces = suiviAnnonceRepository.findByIdPlatformAndIdConsultationAndSupportCodeGroupeIn(idPlatform, Integer.valueOf(idConsultationMpe), List.of("BOAMPJOUE", "MOL"));
        if (!CollectionUtils.isEmpty(suiviAnnonces)) {
            log.info("Traitement de l'avis {} pour la consultation {} et l'organisme {} / plateforme {}", avis.getID(), idConsultationMpe, organisme, idPlatform);
            if (suiviAnnonces.size() > 1) {
                log.warn("Vérification pour {} annonces/ {}", suiviAnnonces.size(), EFormsHelper.getString(avis));
            }
            return suiviAnnonces;
        } else {
            log.info("Création de l'avis {} pour la consultation {} et l'organisme {} / plateforme {}", avis.getID(), idConsultationMpe, organisme, idPlatform);
            annonce = getSuiviAnnonces(avis, idPlatform, idConsultationMpe, organisme, support);
            if (annonce != null) {
                return List.of(annonce);
            }
        }
        return suiviAnnonces;
    }


    private SuiviAnnonce getSuiviAnnonces(AVISEMIS.AVIS avis, String idPlatform, String idConsultationMpe, String organisme, AVISEMIS.AVIS.SUPPORT support) {
        SuiviAnnonce suiviAnnonce = new SuiviAnnonce();
        suiviAnnonce.setIdPlatform(idPlatform);
        suiviAnnonce.setIdConsultation(Integer.valueOf(idConsultationMpe));
        suiviAnnonce.setNumeroAvis(avis.getID());
        suiviAnnonce.setSupportPublication(SupportPublicationEnum.BOAMP);
        boolean isJoue = false;
        boolean isBoamp = false;
        try {
                if ("BOA".equals(support.getCODE())) {
                    isBoamp = true;
                } else if ("JOU".equals(support.getCODE())) {
                    isJoue = true;
                }

            if (supportRepository.existsByCodeInAndActifIsTrue(List.of("BOAMPJOUE", "BOAMP", "JOUE"))) {
                String codeOffre = avis.getNATURE() + "_" + avis.getTYPE();
                String libelle = "";
                if (avis.getNATURE().equals("ORIGINAL")) {
                    libelle += "Avis " + avis.getTYPE();
                } else {
                    libelle += "Avis rectificatif " + avis.getTYPE();
                }
                Offre offre = offreRepository.findByCode(codeOffre).orElse(null);
                if (offre == null) {
                    offre = offreRepository.save(Offre.builder().convertToPdf(false).code(codeOffre).libelle(libelle).documentExtension(avis.getFORMAT().equals("X") ? "xml" : "pdf").build());
                }
                suiviAnnonce.setOffre(offre);
                if (isBoamp && isJoue) {
                    Support boampjoue = supportRepository.findByCode("BOAMPJOUE");
                    suiviAnnonce.setSupport(boampjoue);
                } else if (isBoamp) {
                    Support boamp = supportRepository.findByCode("BOAMP");
                    suiviAnnonce.setSupport(boamp);

                } else if (isJoue) {
                    Support joue = supportRepository.findByCode("JOUE");
                    suiviAnnonce.setSupport(joue);
                }
            }
            if (suiviAnnonce.getSupport() == null && supportRepository.existsByCodeInAndActifIsTrue(List.of("BOAMP_MOL"))) {
                Support joue = supportRepository.findByCode("BOAMP_MOL");
                suiviAnnonce.setSupport(joue);

            }
            if (suiviAnnonce.getSupport() == null) {
                log.warn("Aucun support trouvé pour l'annonce {}", EFormsHelper.getString(avis));
                return null;
            }
            Organisme organismeMpe = organismeService.savePlateformeOrganismeIfNotExist(OrganismeMpe.with().acronyme(organisme).denomination(organisme).sigle(organisme).build(), "BOAMP", idPlatform);
            suiviAnnonce.setOrganismeMpe(organismeMpe);
            suiviAnnonce.setOrganisme(organismeMpe.getLibelleOrganisme());
            suiviAnnonce.setOffrePublication(avis.getTYPE());
            suiviAnnonce.setDateCreation(avis.getDATESENT().toGregorianCalendar().toZonedDateTime());
            suiviAnnonce.setDateSoumission(avis.getDATESENT().toGregorianCalendar().toInstant());
            suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.EN_COURS_DE_PUBLICATION);
            return suiviAnnonceRepository.save(suiviAnnonce);
        } catch (Exception e) {
            log.error("Erreur lors de la mise à jour de l'annonce {}", e.getMessage());
        }
        return null;
    }

    private void updateAnnonces(AVISEMIS.AVIS avis, SuiviAnnonce suiviAnnonce) {
        suiviAnnonce.setNumeroAvis(avis.getID());
        suiviAnnonce.setSupportPublication(SupportPublicationEnum.BOAMP);
        try {
            avis.getSUPPORT().forEach(support -> {
                List<AVISEMIS.AVIS.SUPPORT.ANNONCE> annonces = support.getANNONCE();
                suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.EN_COURS_DE_PUBLICATION);

                if (!CollectionUtils.isEmpty(annonces)) {
                    String code = suiviAnnonce.getSupport().getCode();
                    AVISEMIS.AVIS.SUPPORT.ANNONCE annonce = annonces.get(annonces.size() - 1);
                    String idann = annonce.getIDANN();
                    String idjo = annonce.getIDJO();
                    if ("BOA".equals(support.getCODE()) && (code.contains("BOA") || code.equals("BOAMP_MOL"))) {
                        suiviAnnonce.setStatutBoamp(annonce.getETAT());
                        if (annonce.getDATEENVOI() != null) {
                            Timestamp timestamp = new Timestamp(annonce.getDATEENVOI().toGregorianCalendar().getTimeInMillis());
                            suiviAnnonce.setDateSoumission(timestamp.toInstant());
                        }
                        suiviAnnonce.setIdAnnonce(idann);
                        suiviAnnonce.setNumeroJoue(idjo);
                        AVISEMIS.AVIS.SUPPORT.ANNONCE.INFOSPUB infospub = annonce.getINFOSPUB();
                        if (infospub != null) {
                            suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.PUBLIER);
                            if (StringUtils.hasText(annonce.getINFOSPUB().getURL()))
                                suiviAnnonce.setLienPublication(infospub.getURL());
                            Timestamp timestamp = new Timestamp(infospub.getDATEPUB().toGregorianCalendar().getTimeInMillis());
                            suiviAnnonce.setDatePublication(timestamp.toInstant());
                        }
                    }
                    if ("JOU".equals(support.getCODE()) && (code.contains("JOU") || code.equals("BOAMP_MOL"))) {
                        suiviAnnonce.setStatutJoue(annonce.getETAT());
                        if (StringUtils.hasText(annonce.getINFOSPUB().getURLPDF())) {
                            suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.PUBLIER);
                            suiviAnnonce.setLienPublicationTed(annonce.getINFOSPUB().getURLPDF());
                        }
                    }
                }
            });
            suiviAnnonceRepository.save(suiviAnnonce);
        } catch (Exception e) {
            log.error("Erreur lors de la mise à jour de l'annonce {}", e.getMessage());
        }
    }


    @Override
    public AvisPublieDTO addPublicationSynchroneMOL(String synchrone, MultipartFile annonce, MultipartFile complement) {
        String contexteXml = new String(Base64.decodeBase64(synchrone.getBytes()));
        String complementString = getText(complement);
        PieceJointe pieceJointeAnnonce = null;
        if (annonce != null) {
            pieceJointeAnnonce = pieceJointeService.save(annonce);

        }
        AvisPublie avisPublie = this.addInfosAvisPublie(contexteXml, pieceJointeAnnonce, complementString, annonce == null ? null : annonce.getOriginalFilename(), complement == null ? null : complement.getOriginalFilename());
        return avisPublieMapper.avisPublieToAvisPublieDTO(avisPublie);
    }

    private AvisPublie addInfosAvisPublie(String contexteXml, PieceJointe pieceJointeAnnonce, String complementString, String annonceName, String complementName) {
        IACTX contexte = getXmlFromString(contexteXml, IACTX.class);
        if (contexte == null) {
            return null;
        }
        CMPLT complement = getXmlFromString(complementString, CMPLT.class);
        String idAvis = complement != null ? complement.getIDORIG() : null;
        if (idAvis == null) {
            return null;
        }
        AvisPublie saved = avisPublieRepository.findByIdAvis(idAvis).orElse(null);
        IACTX.CMPLT cmplt = contexte.getCMPLT();
        String organisme = cmplt != null ? cmplt.getORGANISME() : null;
        IACTX.CTX ctx = contexte.getCTX();

        IACTX.CTX.PERS pers = ctx == null ? null : ctx.getPERS();
        String idpm = pers == null ? null : pers.getIDPM();
        if (!StringUtils.hasText(organisme)) {
            organisme = idpm;
        }
        LocalDateTime now = LocalDateTime.now();
        IACTX.IDTF idtf = contexte.getIDTF();
        Integer id = saved == null ? null : saved.getId();

        AvisPublie build = AvisPublie.builder().id(id).uidConsultation(ctx == null ? null : ctx.getIDC()).uidAgentmetteur(idtf == null ? null : idtf.getLGN()).uidPlateforme(cmplt == null ? null : cmplt.getUIDPF()).service(idpm).organisme(organisme).referenceUtilisateur(ctx == null ? null : ctx.getREFPP()).xmlContexte(contexteXml).idAvis(idAvis).type(complement.getTYPE()).dateEnvoi(saved == null ? now : saved.getDateEnvoi()).dateMaj(now).annonce(pieceJointeAnnonce).nomFichierAnnonce(annonceName).nomFichierComplementaire(complementName).xmlFichierComplementaire(complementString).build();
        return avisPublieRepository.save(build);
    }

    private <T> T getXmlFromString(String text, Class<T> someClass) {
        if (text == null) {
            return null;
        }
        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(text.getBytes())) {
            JAXBContext jaxbContextTed = JAXBContext.newInstance(someClass);
            Unmarshaller jaxbUnmarshaller = jaxbContextTed.createUnmarshaller();
            return (T) jaxbUnmarshaller.unmarshal(inputStream);
        } catch (Exception e) {
            log.error("Erreur lors de l'extraction de {} : {}", someClass, e.getMessage());
            throw new ConcentraturException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage());
        }

    }

    private String getText(MultipartFile inputStream) {
        if (inputStream == null) return null;
        try {
            return new String(inputStream.getInputStream().readAllBytes(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            log.error(e.getMessage());
            throw new ConcentraturException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage());
        }
    }

    private String getString(Envelope value1) throws JAXBException {
        JAXBContext jaxbContextTed = JAXBContext.newInstance(Envelope.class);
        Marshaller jaxbMarshaller = jaxbContextTed.createMarshaller();
        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(value1, sw);
        return sw.toString();
    }


    public boolean sisNumeric(String strNum) {
        Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");
        if (strNum == null) {
            return false;
        }
        return pattern.matcher(strNum).matches();
    }
}
