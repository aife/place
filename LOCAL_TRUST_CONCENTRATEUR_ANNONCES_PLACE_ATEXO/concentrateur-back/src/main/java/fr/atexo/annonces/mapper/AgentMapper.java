package fr.atexo.annonces.mapper;

import fr.atexo.annonces.boot.entity.Agent;
import fr.atexo.annonces.config.DaoMapper;
import fr.atexo.annonces.dto.AgentDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {StringAndListStringMapper.class})
public interface AgentMapper extends DaoMapper<Agent, AgentDTO> {


}
