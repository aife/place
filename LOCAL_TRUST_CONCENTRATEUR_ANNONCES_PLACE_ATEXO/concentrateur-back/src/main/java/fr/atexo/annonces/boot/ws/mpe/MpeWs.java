package fr.atexo.annonces.boot.ws.mpe;


import com.atexo.annonces.commun.mpe.*;
import fr.atexo.annonces.boot.ws.mpe.model.HydraAcheteurResponse;
import fr.atexo.annonces.boot.ws.mpe.model.HydraLotResponse;
import fr.atexo.annonces.boot.ws.mpe.model.HydraProfilJoueResponse;
import fr.atexo.annonces.config.EFormsHelper;
import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Component
public class MpeWs {

    private static final String MESSAGE = "Token ne peut pas être null";
    private final RestTemplate restTemplate;


    public MpeWs(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ConsultationMpe getConsultation(Integer id, String url, String token) {
        if (token == null)
            throw new AtexoException(ExceptionEnum.MANDATORY, MESSAGE);
        log.info("Recuperation de la consultation {}", id);
        //Creating a HttpClient object
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.add("accept", "application/json");
            return EFormsHelper.getObjectFromString(restTemplate.exchange(url + "/api/v2/consultations/" + id, HttpMethod.GET,
                    new HttpEntity<>(headers), String.class).getBody(), ConsultationMpe.class);
        } catch (RestClientResponseException e) {
            log.error("Erreur lors de la recuperation de la consultation {} : {}", id, e.getResponseBodyAsString(), e);
        } catch (Exception e) {
            log.error("Erreur lors de la recuperation de la consultation {} : {}", id, e.getMessage(), e);
        }
        return null;
    }

    public DonneesComplementaires getDonneComplementaire(Integer id, String url, String token) {
        if (token == null)
            throw new AtexoException(ExceptionEnum.MANDATORY, MESSAGE);
        log.info("Recuperation Donne Complementaire de la consultation {}", id);
        //Creating a HttpClient object
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.add("accept", "application/json");
            return restTemplate.exchange(url + "/api/v2/consultations/" + id + "/donnee-complementaire", HttpMethod.GET,
                    new HttpEntity<>(headers), DonneesComplementaires.class).getBody();
        } catch (RestClientResponseException e) {
            log.error("Erreur lors de la recuperation de la Donne Complementaire de la consultation {} : {}", id, e.getResponseBodyAsString(), e);
        } catch (Exception e) {
            log.error("Erreur lors de la recuperation de la Donne Complementaire de la consultation {} : {}", id, e.getMessage(), e);
        }
        return null;
    }

    public CritereAttributionMpe getCritereAttribution(String hydraId, String url, String token) {
        if (token == null)
            throw new AtexoException(ExceptionEnum.MANDATORY, MESSAGE);
        log.info("Recuperation Critère attribution {}", hydraId);
        //Creating a HttpClient object
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.add("accept", "application/json");
            return restTemplate.exchange(url + hydraId, HttpMethod.GET,
                    new HttpEntity<>(headers), CritereAttributionMpe.class).getBody();
        } catch (RestClientResponseException e) {
            log.error("Erreur lors de la recuperation de Critère attribution {} : {}", hydraId, e.getResponseBodyAsString(), e);
        } catch (Exception e) {
            log.error("Erreur lors de la recuperation de Critère attribution {} : {}", hydraId, e.getMessage(), e);
        }
        return null;
    }

    public DonneesComplementaires getDonneComplementaireLot(Integer id, String url, String token) {
        if (token == null)
            throw new AtexoException(ExceptionEnum.MANDATORY, MESSAGE);
        log.info("Recuperation Donne Complementaire du lot {}", id);
        //Creating a HttpClient object
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.add("accept", "application/json");
            return restTemplate.exchange(url + "/api/v2/lots/" + id + "/donnee-complementaire", HttpMethod.GET,
                    new HttpEntity<>(headers), DonneesComplementaires.class).getBody();
        } catch (RestClientResponseException e) {
            log.error("Erreur lors de la recuperation de la Donne Complementaire du lot {} : {}", id, e.getResponseBodyAsString(), e);
        } catch (Exception e) {
            log.error("Erreur lors de la recuperation de la Donne Complementaire du lot {} : {}", id, e.getMessage(), e);
        }
        return null;
    }

    public AgentMpe getAgent(Integer id, String url, String token) {
        if (token == null)
            throw new AtexoException(ExceptionEnum.MANDATORY, MESSAGE);
        log.info("Recuperation de l'agent {}", id);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.add("accept", "application/json");
            return restTemplate.exchange(url + "/api/v2/agents/" + id, HttpMethod.GET,
                    new HttpEntity<>(headers), AgentMpe.class).getBody();
        } catch (RestClientResponseException e) {
            log.error("Erreur lors de la recuperation de l'agent {} : {}", id, e.getResponseBodyAsString(), e);
        } catch (Exception e) {
            log.error("Erreur lors de la recuperation de l'agent {} : {}", id, e.getMessage(), e);
        }
        return null;
    }


    public ServiceMpe getService(String hydraId, String url, String token) {
        if (!StringUtils.hasText(hydraId))
            return null;
        if (token == null)
            throw new AtexoException(ExceptionEnum.MANDATORY, MESSAGE);
        log.info("Recuperation du service {}", hydraId);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("accept", "application/json");
            headers.setBearerAuth(token);
            return restTemplate.exchange(url + hydraId, HttpMethod.GET,
                    new HttpEntity<>(headers), ServiceMpe.class).getBody();
        } catch (RestClientResponseException e) {
            log.error("Erreur lors de la recuperation du service {} : {}", hydraId, e.getResponseBodyAsString(), e);
        } catch (Exception e) {
            log.error("Erreur lors de la recuperation du service {} : {}", hydraId, e.getMessage(), e);
        }
        return null;
    }

    public NatureMpe getNaturePrestation(String hydraId, String url, String token) {
        if (!StringUtils.hasText(hydraId))
            return null;
        if (token == null)
            throw new AtexoException(ExceptionEnum.MANDATORY, MESSAGE);
        log.info("Recuperation du service {}", hydraId);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("accept", "application/json");
            headers.setBearerAuth(token);
            return restTemplate.exchange(url + hydraId, HttpMethod.GET,
                    new HttpEntity<>(headers), NatureMpe.class).getBody();
        } catch (RestClientResponseException e) {
            log.error("Erreur lors de la recuperation de la nature de prestation {} : {}", hydraId, e.getResponseBodyAsString(), e);
        } catch (Exception e) {
            log.error("Erreur lors de la recuperation de la nature de prestation {} : {}", hydraId, e.getMessage(), e);
        }
        return null;
    }

    public OrganismeMpe getOrganisme(String hydraId, String url, String token) {
        if (!StringUtils.hasText(hydraId))
            return null;
        if (token == null)
            throw new AtexoException(ExceptionEnum.MANDATORY, MESSAGE);
        log.info("Recuperation du organisme {}", hydraId);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.add("accept", "application/json");
            return restTemplate.exchange(url + hydraId, HttpMethod.GET,
                    new HttpEntity<>(headers), OrganismeMpe.class).getBody();
        } catch (RestClientResponseException e) {
            log.error("Erreur lors de la recuperation du organisme {} : {}", hydraId, e.getResponseBodyAsString(), e);
        } catch (Exception e) {
            log.error("Erreur lors de la recuperation du organisme {} : {}", hydraId, e.getMessage(), e);
        }
        return null;
    }

    public List<LotMpe> getListeLots(Integer id, String url, String token) {
        if (token == null)
            throw new AtexoException(ExceptionEnum.MANDATORY, MESSAGE);
        log.info("Recuperation de la liste de lots de consultation {}", id);
        //Creating a HttpClient object
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("accept", "*/*");
            headers.setBearerAuth(token);
            ResponseEntity<HydraLotResponse> responseEntity = restTemplate.exchange(url + "/api/v2/lots?pagination=false&consultation=" + id, HttpMethod.GET,
                    new HttpEntity<>(headers), HydraLotResponse.class);
            return responseEntity.getBody().getList().stream()
                    .filter(mpeLot -> mpeLot.getConsultation().endsWith("/" + id))
                    .peek(lotMpe -> lotMpe.setId(Integer.valueOf(lotMpe.getHydraId().substring(lotMpe.getHydraId().lastIndexOf("/") + 1))))
                    .collect(Collectors.toList());

        } catch (RestClientResponseException e) {
            log.error("Erreur lors de la recuperation de la liste de lots de consultation {} : {}", id, e.getResponseBodyAsString(), e);
        } catch (Exception e) {
            log.error("Erreur lors de la recuperation de la liste de lots de consultation {} : {}", id, e.getMessage(), e);
        }
        return Collections.emptyList();
    }


    public LotMpe getLot(String hydraId, String url, String token) {
        if (token == null)
            throw new AtexoException(ExceptionEnum.MANDATORY, MESSAGE);
        log.info("Recuperation du lot {}", hydraId);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.add("accept", "application/json");
            return restTemplate.exchange(url + hydraId, HttpMethod.GET,
                    new HttpEntity<>(headers), LotMpe.class).getBody();
        } catch (RestClientResponseException e) {
            log.error("Erreur lors de la recuperation du lot {} : {}", hydraId, e.getResponseBodyAsString(), e);
        } catch (Exception e) {
            log.error("Erreur lors de la recuperation du lot {} : {}", hydraId, e.getMessage(), e);
        }
        return null;
    }

    public ReferentielMpe getReferentiel(String hydraId, String url, String token) {
        if (!StringUtils.hasText(hydraId))
            return null;
        if (token == null)
            throw new AtexoException(ExceptionEnum.MANDATORY, MESSAGE);
        log.info("Recuperation du lot {}", hydraId);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.add("accept", "*/*");
            return restTemplate.exchange(url + hydraId, HttpMethod.GET,
                    new HttpEntity<>(headers), ReferentielMpe.class).getBody();
        } catch (RestClientResponseException e) {
            log.error("Erreur lors de la recuperation du lot {} : {}", hydraId, e.getResponseBodyAsString(), e);
        } catch (Exception e) {
            log.error("Erreur lors de la recuperation du lot {} : {}", hydraId, e.getMessage(), e);
        }
        return null;
    }


    public ContratMpe getContrat(String hydraId, String url, String token) {
        if (!StringUtils.hasText(hydraId))
            return null;
        if (token == null)
            throw new AtexoException(ExceptionEnum.MANDATORY, MESSAGE);
        log.info("Recuperation du lot {}", hydraId);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.add("accept", "*/*");
            return restTemplate.exchange(url + hydraId, HttpMethod.GET,
                    new HttpEntity<>(headers), ContratMpe.class).getBody();
        } catch (RestClientResponseException e) {
            log.error("Erreur lors de la recuperation du lot {} : {}", hydraId, e.getResponseBodyAsString(), e);
        } catch (Exception e) {
            log.error("Erreur lors de la recuperation du lot {} : {}", hydraId, e.getMessage(), e);
        }
        return null;
    }

    public List<AcheteurMpe> getAcheteurs(Long serviceId, String organisme, String url, String token) {
        if (token == null)
            throw new AtexoException(ExceptionEnum.MANDATORY, MESSAGE);
        log.info("Recuperation des acheteurs pour l'organisme {}", organisme);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.add("accept", "*/*");
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url + "/api/v2/acheteur-publics");
            builder.queryParam("pagination", Boolean.FALSE.toString());
            if (serviceId != null) {
                builder.queryParam("serviceId", serviceId);
            }
            if (organisme != null) {
                builder.queryParam("organisme", organisme);
            }
            String uriBuilder = builder.build().encode().toUriString();
            ResponseEntity<HydraAcheteurResponse> responseEntity = restTemplate.exchange(uriBuilder, HttpMethod.GET,
                    new HttpEntity<>(headers), HydraAcheteurResponse.class);
            return Objects.requireNonNull(responseEntity.getBody()).getList();
        } catch (RestClientResponseException e) {
            log.error("Erreur lors de la recuperation des acheteurs pour l'organisme {} : {}", organisme, e.getResponseBodyAsString(), e);
        } catch (Exception e) {
            log.error("Erreur lors de la recuperation des acheteurs pour l'organisme {} : {}", organisme, e.getMessage(), e);
        }
        return Collections.emptyList();
    }

    public List<ProfilJoueMpe> getComptesJoues(String agentId, String organisme, String url, String token) {
        if (token == null)
            throw new AtexoException(ExceptionEnum.MANDATORY, MESSAGE);
        log.info("Recuperation du compte joue pour l'organisme {}", organisme);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.add("accept", "*/*");
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url + "/api/v2/t-profil-joues");
            builder.queryParam("pagination", Boolean.FALSE.toString());
            if (agentId != null) {
                builder.queryParam("agent", agentId);
            }
            if (organisme != null) {
                builder.queryParam("organisme", organisme);
            }
            String uriBuilder = builder.build().encode().toUriString();
            ResponseEntity<HydraProfilJoueResponse> responseEntity = restTemplate.exchange(uriBuilder, HttpMethod.GET,
                    new HttpEntity<>(headers), HydraProfilJoueResponse.class);
            return Objects.requireNonNull(responseEntity.getBody()).getList();
        } catch (RestClientResponseException e) {
            log.error("Erreur lors de la recuperation des acheteurs pour l'organisme {} : {}", organisme, e.getResponseBodyAsString(), e);
        } catch (Exception e) {
            log.error("Erreur lors de la recuperation des acheteurs pour l'organisme {} : {}", organisme, e.getMessage(), e);
        }
        return Collections.emptyList();
    }


}
