package fr.atexo.annonces.modeles;

import fr.atexo.annonces.boot.entity.SuiviTypeAvisPub;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.ZonedDateTime;
import java.util.Set;

@AllArgsConstructor
@Getter
@Setter
@Builder
public class SuiviTypeAvisSpecification implements Specification<SuiviTypeAvisPub> {

    private static final long serialVersionUID = 1222004680882504439L;

    private Set<String> plateformes;
    private Set<StatutTypeAvisAnnonceEnum> statuts;
    private ZonedDateTime dateModificationMax;
    private ZonedDateTime dateModificationMin;


    @Override
    public Predicate toPredicate(Root<SuiviTypeAvisPub> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        criteriaQuery.distinct(true);
        Predicate predicate = criteriaBuilder.isNotEmpty(root.get("annonces"));
        if (!CollectionUtils.isEmpty(this.plateformes)) {
            predicate = criteriaBuilder.and(predicate, root.get("organisme").get("plateformeUuid").in(this.plateformes));
        }

        if (!CollectionUtils.isEmpty(statuts)) {
            predicate = addStatutFilter(root, criteriaBuilder, predicate, this.statuts);
        } else {
            predicate = addStatutFilter(root, criteriaBuilder, predicate, Set.of(
                    StatutTypeAvisAnnonceEnum.REJETE,
                    StatutTypeAvisAnnonceEnum.REJETE_ECO,
                    StatutTypeAvisAnnonceEnum.REJETE_SIP,
                    StatutTypeAvisAnnonceEnum.ENVOYE,
                    StatutTypeAvisAnnonceEnum.ENVOI_PLANIFIER));
        }

        if (dateModificationMin != null) {
            predicate = addDateModificationLessThenFilter(root, criteriaBuilder, predicate);
        }
        if (dateModificationMax != null) {
            predicate = addDateModificationGreaterThenFilter(root, criteriaBuilder, predicate);
        }
        return predicate;
    }


    private Predicate addInFilter(Root<SuiviTypeAvisPub> root, CriteriaBuilder criteriaBuilder,
                                  Predicate predicate, String key, Object value) {
        Predicate paysPredict = criteriaBuilder.disjunction();
        paysPredict.getExpressions().add(root.get(key).in(value));
        return criteriaBuilder.and(predicate, paysPredict);
    }

    private Predicate addStatutFilter(Root<SuiviTypeAvisPub> root, CriteriaBuilder criteriaBuilder,
                                      Predicate predicate, Set<StatutTypeAvisAnnonceEnum> statuts) {
        return criteriaBuilder.and(predicate, addInFilter(root, criteriaBuilder, predicate, "statut", statuts));
    }

    private Predicate addDateModificationLessThenFilter(Root<SuiviTypeAvisPub> root, CriteriaBuilder criteriaBuilder,
                                                        Predicate predicate) {

        Predicate datePredict = criteriaBuilder.disjunction();
        datePredict.getExpressions().add(criteriaBuilder.lessThanOrEqualTo(root.get("dateModification"), dateModificationMax));
        return criteriaBuilder.and(predicate, datePredict);
    }

    private Predicate addDateModificationGreaterThenFilter(Root<SuiviTypeAvisPub> root, CriteriaBuilder criteriaBuilder,
                                                           Predicate predicate) {

        Predicate datePredict = criteriaBuilder.disjunction();
        datePredict.getExpressions().add(criteriaBuilder.greaterThanOrEqualTo(root.get("dateModification"), dateModificationMin));
        return criteriaBuilder.and(predicate, datePredict);
    }


}
