package fr.atexo.annonces.mapper.tncp;

import eu.europa.publications.resource.schema.ted.r2_0_9.reception.TedEsenders;
import fr.atexo.annonces.config.EFormsHelper;
import fr.atexo.annonces.mapper.Mappable;
import fr.atexo.annonces.modeles.PaysInsee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class PaysInseeMapper implements Mappable<String, String> {

    @Value("classpath:mapping/mapping-mpe-tncp.yml")
    Resource mappingMpeTncp;


    @Override
    public String map(String pays, TedEsenders tedEsenders, boolean rie) {
        if (pays == null) return null;
        final List<Object> mpeTncpMappingValues = EFormsHelper.getObjectFromString(EFormsHelper.getString(EFormsHelper.getMappingObjet(mappingMpeTncp).get("pays-insee")), List.class);
        return mpeTncpMappingValues.stream()
                .map(objet -> EFormsHelper.getObjectFromString(EFormsHelper.getString(objet), PaysInsee.class))
                .filter(paysInsee -> paysInsee.getPays().equalsIgnoreCase(pays)).map(PaysInsee::getCode).findFirst().orElse(null);

    }

}
