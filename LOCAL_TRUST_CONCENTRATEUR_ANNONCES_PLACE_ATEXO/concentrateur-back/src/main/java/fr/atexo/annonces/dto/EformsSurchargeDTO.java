package fr.atexo.annonces.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class EformsSurchargeDTO implements Serializable {

    private String defaultValue;

    private String dynamicListFrom;

    private String staticList;

    private Boolean hidden;

    private Boolean readonly;

    private Boolean obligatoire;

    private Integer ordre;

    private boolean actif;

}
