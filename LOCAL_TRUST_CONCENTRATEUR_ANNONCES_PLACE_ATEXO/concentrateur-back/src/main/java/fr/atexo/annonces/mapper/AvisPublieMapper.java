package fr.atexo.annonces.mapper;

import fr.atexo.annonces.boot.entity.AvisPublie;
import fr.atexo.annonces.dto.AvisPublieDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(uses = PieceJointeMapper.class)
public interface AvisPublieMapper {


    AvisPublieDTO avisPublieToAvisPublieDTO(AvisPublie avisPublie);

    List<AvisPublieDTO> avisPubliesToAvisPublieDTOs(List<AvisPublie> avisPublies);

}
