package fr.atexo.annonces.reseau;

/**
 * Paramètre d'appel aux méthodes du controller d'Annonce. Indique quelles sont les sous-ressources de l'Annonce que
 * le client voudrait récupérer en même temps que l'Annonce.
 */
public enum AnnonceIncludeParameterEnum {
    SUPPORT
}
