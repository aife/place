package fr.atexo.annonces.service.exception;

import lombok.Getter;
import org.slf4j.helpers.MessageFormatter;

@Getter
public class EnvoiInvalideException extends RuntimeException {

	private final String json;

	public EnvoiInvalideException(String json) {
		super(MessageFormatter.format("L'envoi par MPE {} n'a pas le format attendu", json).getMessage());

		this.json = json;
	}
}
