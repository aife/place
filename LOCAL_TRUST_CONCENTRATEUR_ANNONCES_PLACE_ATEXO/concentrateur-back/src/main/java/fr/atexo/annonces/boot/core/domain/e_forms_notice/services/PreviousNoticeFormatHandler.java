package fr.atexo.annonces.boot.core.domain.e_forms_notice.services;

import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNotice;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNoticeMetadata;
import fr.atexo.annonces.boot.entity.EFormsFormulaireEntity;

import java.util.List;
import java.util.Map;

public interface PreviousNoticeFormatHandler {

    Object processGroup(SubNotice notice, Object formulaireObjet, SubNoticeMetadata group, EFormsFormulaireEntity previousFormulaire, EFormsFormulaireEntity newFormulaire, String nodeStop, Map<String, List<PreviousNoticeFormatHandler>> kownHandlers, boolean first);

    Object processElement(SubNoticeMetadata item, SubNotice notice, Object formulaireObjet, String id, EFormsFormulaireEntity previousFormulaire, EFormsFormulaireEntity newFormulaire, String nodeStop);


}
