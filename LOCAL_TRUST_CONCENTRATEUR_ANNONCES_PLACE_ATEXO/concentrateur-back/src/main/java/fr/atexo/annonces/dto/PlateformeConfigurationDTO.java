package fr.atexo.annonces.dto;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlateformeConfigurationDTO implements Serializable {

    private static final long serialVersionUID = -1410232521211913768L;

    private Long id;
    @NotEmpty
    private String idPlateforme;
    @NotEmpty
    private String pays;
    private List<String> supports;

    private boolean europeen;

    private boolean national;

    private boolean eformsModal;

    private boolean pqr;
    private boolean jal;
    private boolean groupement;

    private boolean validationObligatoire;

    private boolean associationProcedure;

    private boolean associationOrganisme;
    private String eformsToken;


}
