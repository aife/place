package fr.atexo.annonces.service.exception;

import lombok.Getter;

@Getter
public class AtexoException extends RuntimeException {
    protected String message;
    protected int code;
    protected String type;

    public AtexoException(int code, Throwable err) {
        super(err);
        this.code = code;
        this.message = err.getMessage();
    }

    public AtexoException(int code, String message, Throwable err) {
        super(err);
        this.code = code;
        this.message = message;
    }

    public AtexoException(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public AtexoException(Throwable err) {
        super(err);
    }

    public AtexoException() {
    }

    public AtexoException(ExceptionEnum technicalError, String message) {
        this.code = technicalError.getCode();
        this.message = message;
        this.type = technicalError.getType();
    }

    public AtexoException(ExceptionEnum technicalError, String message, Throwable err) {
        super(err);
        this.code = technicalError.getCode();
        this.message = message;
        this.type = technicalError.getType();
    }
}
