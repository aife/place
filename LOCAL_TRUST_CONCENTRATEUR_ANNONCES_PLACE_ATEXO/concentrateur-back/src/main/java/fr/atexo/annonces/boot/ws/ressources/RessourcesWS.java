package fr.atexo.annonces.boot.ws.ressources;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Component
@Slf4j
public class RessourcesWS {
    private final RestTemplate atexoRestTemplate;

    private final String ressourcesBaseUrl;

    private final String avisStandardTexte;
    private final String avisSimplifieTexte;

    public RessourcesWS(RestTemplate atexoRestTemplate,
                        @Value("${external-apis.ressources.base-path}") String ressourcesBaseUrl,
                        @Value("${external-apis.ressources.ws.avis-standard-texte}") String avisStandardTexte,
                        @Value("${external-apis.ressources.ws.avis-simplifie-texte}") String avisSimplifieTexte) {
        this.atexoRestTemplate = atexoRestTemplate;
        this.ressourcesBaseUrl = ressourcesBaseUrl;
        this.avisStandardTexte = avisStandardTexte;
        this.avisSimplifieTexte = avisSimplifieTexte;
    }

    public File downloadAvisTemplate(boolean simplifie) {
        String resource;
        if (simplifie)
            resource = ressourcesBaseUrl + avisSimplifieTexte;
        else {
            resource = ressourcesBaseUrl + avisStandardTexte;
        }
        try {
            File tempFile = File.createTempFile("ressources-", ".docx");
            return download(resource, tempFile);
        } catch (IOException e) {
            log.error("Erreur lors du téléchargement de la fiche {}", resource);
            throw new RessourcesException("Erreur lors du téléchargement de la fiche " + resource, e);
        }
    }

    public File downloadAvisTemplate(String resource) {
        try {
            File tempFile = File.createTempFile("ressources-", ".docx");
            return download(resource, tempFile);
        } catch (IOException e) {
            log.error("Erreur lors du téléchargement de la fiche {}", resource);
            throw new RessourcesException("Erreur lors du téléchargement de la fiche " + resource, e);
        }
    }


    public File download(String urlInputResource, File pathXmlInputResource) {
        return atexoRestTemplate.execute(urlInputResource,
                HttpMethod.GET,
                clientHttpRequest -> {
                },
                clientHttpResponse -> {
                    StreamUtils.copy(clientHttpResponse.getBody(), new FileOutputStream(pathXmlInputResource));
                    return pathXmlInputResource;
                });
    }
}
