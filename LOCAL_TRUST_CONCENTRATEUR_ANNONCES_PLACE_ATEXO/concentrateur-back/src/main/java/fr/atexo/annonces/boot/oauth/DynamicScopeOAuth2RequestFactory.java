package fr.atexo.annonces.boot.oauth;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Permet la création de token avec des scopes dynamiques, c'est-à-dire qui ne sont pas enregistrés dans la définition
 * des clients et qui dépendent d'autres paramètres lors de la demande de token. Doit être couplé avec un
 * RequestValidator custom pour pouvoir passer la validation des scopes.
 *
 * @see DynamicScopeOAuth2RequestValidator
 */
@Component
@Slf4j
public class DynamicScopeOAuth2RequestFactory extends DefaultOAuth2RequestFactory {

    public static final String PLATFORM_SCOPE = "platform:";
    public static final String CONSULTATION_SCOPE = "consultation:";
    public static final String VALIDATION_SCOPE = "validation:";
    public static final String ALL_CONSULTATION_SCOPE = "consultation";
    private static final String ID_CONSULTATION_TOKEN_REQUEST_PARAM = "idConsultation";
    private static final String TYPE_VALIDATION_TOKEN_REQUEST_PARAM = "typeValidation";

    public DynamicScopeOAuth2RequestFactory(ClientDetailsService clientDetailsService) {
        super(clientDetailsService);
    }

    /**
     * Crée un jeton d'accès via {@link DefaultOAuth2RequestFactory} et l'enrichit avec des scopes dynamiques :
     * <ul><li>Un scope "platform" selon l'id du client oauth ;</li>
     * <li>Un scope "consultation" selon l'id de la consultation passée en paramètre de la requête de demande de jeton
     * d'accès. Si aucun id n'est passé en paramètre, un scope autorisant un accès à toutes les consultations est
     * attribué.</li></ul>
     *
     * @param requestParameters   Paramètres Http de la requête
     * @param authenticatedClient Détails du client oauth
     * @return Jeton d'accès
     */
    @Override
    public TokenRequest createTokenRequest(Map<String, String> requestParameters, ClientDetails authenticatedClient) {
        TokenRequest tokenRequest = super.createTokenRequest(requestParameters, authenticatedClient);
        Set<String> scope = new HashSet<>(tokenRequest.getScope());
        scope.add(getPlatformScope(authenticatedClient));
        scope.add(getConsultationScope(requestParameters));
        String validationScope = getValidationScope(requestParameters);
        if (validationScope != null)
            scope.add(validationScope);
        tokenRequest.setScope(scope);
        return tokenRequest;
    }

    private String getPlatformScope(ClientDetails authenticatedClient) {
        return PLATFORM_SCOPE + authenticatedClient.getClientId();
    }

    private String getConsultationScope(Map<String, String> requestParameters) {
        if (requestParameters.containsKey(ID_CONSULTATION_TOKEN_REQUEST_PARAM)) {
            return CONSULTATION_SCOPE + requestParameters.get(ID_CONSULTATION_TOKEN_REQUEST_PARAM);
        } else {
            return ALL_CONSULTATION_SCOPE;
        }
    }

    private String getValidationScope(Map<String, String> requestParameters) {
        if (requestParameters.containsKey(TYPE_VALIDATION_TOKEN_REQUEST_PARAM)) {
            return VALIDATION_SCOPE + requestParameters.get(TYPE_VALIDATION_TOKEN_REQUEST_PARAM);
        }
        return null;
    }

}
