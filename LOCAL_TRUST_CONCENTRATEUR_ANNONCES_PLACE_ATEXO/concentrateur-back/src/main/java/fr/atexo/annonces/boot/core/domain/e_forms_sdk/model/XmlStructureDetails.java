package fr.atexo.annonces.boot.core.domain.e_forms_sdk.model;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class XmlStructureDetails {
    private String id;
    private String parentId;
    private String xpathAbsolute;
    private String xpathRelative;
    private String identifierFieldId;
    private boolean repeatable;
}
