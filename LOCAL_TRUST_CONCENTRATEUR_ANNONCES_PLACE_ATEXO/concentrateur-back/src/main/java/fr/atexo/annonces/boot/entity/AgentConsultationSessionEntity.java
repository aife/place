package fr.atexo.annonces.boot.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "AGENT_CONSULTATION_SESSION")
public class AgentConsultationSessionEntity implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_AGENT", referencedColumnName = "id")
    private Agent agent;

    @Column(name = "ID_CONSULTATION")
    private Integer idConsultation;

    @Lob
    @Column(name = "CONTEXTE", nullable = false)
    private String contexte;


}
