package fr.atexo.annonces.reseau;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;

/**
 * Représente un paramètre HTTP POST d'envoi de fichier
 */
public class HttpFileParameter {

    private final Logger logger = LoggerFactory.getLogger(HttpFileParameter.class);
    private final String name;
    private final HttpEntity<byte[]> file;

    public HttpFileParameter(String name, HttpEntity<byte[]> file) {
        this.name = name;
        this.file = file;
        if (logger.isDebugEnabled()) {
            byte[] body = file.getBody();
            logger.debug("Construction d'un paramètre HTTP type file du nom de {} et de taille {}",
                    name, body != null ? body.length : null);
        }
    }

    public String getName() {
        return name;
    }

    public HttpEntity<byte[]> getFile() {
        return file;
    }
}
