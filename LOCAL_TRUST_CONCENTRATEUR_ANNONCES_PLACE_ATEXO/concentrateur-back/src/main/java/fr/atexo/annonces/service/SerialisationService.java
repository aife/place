package fr.atexo.annonces.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service de génération de fichier
 */
@Service
public interface SerialisationService {

	/**
	 * Permet de sérialiser l'objet
	 *
	 * @param payload les données brutes
	 * @param features fonctionnalités de sérialisation
	 *
	 * @return l'objet désérialisé
	 */
	<T> String serialise(T payload, final SerializationFeature... features);

	/**
	 * Permet de désérialiser l'objet
	 *
	 * @param serialisation l'objet à désérialiser
	 * @param clazz la classe de l'objet
	 * @param features fonctionnalités de désérialisation
	 *
	 * @return l'objet
	 */
	<T> Optional<T> deserialise(String serialisation, Class<T> clazz, final DeserializationFeature... features);
}
