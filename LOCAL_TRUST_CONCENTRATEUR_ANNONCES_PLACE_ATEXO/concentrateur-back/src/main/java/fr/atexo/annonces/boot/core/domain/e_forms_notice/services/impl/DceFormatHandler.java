package fr.atexo.annonces.boot.core.domain.e_forms_notice.services.impl;

import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.NoticeFormatHandler;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.FieldDetails;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNotice;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNoticeMetadata;
import fr.atexo.annonces.boot.entity.EFormsFormulaireEntity;
import fr.atexo.annonces.boot.repository.EformsSurchargeRepository;
import fr.atexo.annonces.boot.ws.eforms.SchematronConfiguration;
import fr.atexo.annonces.boot.ws.mpe.model.ConsultationContexte;
import fr.atexo.annonces.config.EFormsHelper;
import fr.atexo.annonces.dto.ConfigurationConsultationDTO;
import fr.atexo.annonces.dto.EformsSurchargeDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
public class DceFormatHandler extends GenericSdkService implements NoticeFormatHandler {


    public DceFormatHandler(EformsSurchargeRepository eformsSurchargeRepository, SchematronConfiguration schematronConfiguration) {
        super(eformsSurchargeRepository, schematronConfiguration);
    }

    @Override
    public Object processGroup(SubNotice notice, ConfigurationConsultationDTO contexte, Object formulaireObjet, SubNoticeMetadata group, EFormsFormulaireEntity saved, String nodeStop, Map<String, List<NoticeFormatHandler>> kownHandlers) {
        for (SubNoticeMetadata item : group.getContent()) {
            String nodeId = (item.is_repeatable() || item.get_identifierFieldId() != null) ? item.getNodeId() : null;

            List<NoticeFormatHandler> handler = kownHandlers.get(nodeId);
            if (handler != null) {

                for (NoticeFormatHandler noticeFormatHandler : handler) {
                    Object value = noticeFormatHandler.processGroup(notice, contexte, new Object(), item, saved, nodeId, kownHandlers);
                    formulaireObjet = addToObject(formulaireObjet, null, value, nodeStop, nodeId, saved.getVersionSdk());
                }
            } else if (nodeId != null) {
                if (("field".equals(item.getContentType()))) {
                    formulaireObjet = processElement(item, notice, contexte, formulaireObjet, item.getId(), saved, nodeStop);
                }
                if (!CollectionUtils.isEmpty(item.getContent())) {
                    Object o = processGroup(notice, contexte, new Object(), item, saved, nodeId, kownHandlers);
                    formulaireObjet = addToObject(formulaireObjet, null, o, nodeStop, nodeId, saved.getVersionSdk());
                }
            } else {
                if (("field".equals(item.getContentType()))) {
                    formulaireObjet = processElement(item, notice, contexte, formulaireObjet, item.getId(), saved, nodeStop);
                }
                if (!CollectionUtils.isEmpty(item.getContent())) {
                    formulaireObjet = processGroup(notice, contexte, formulaireObjet, item, saved, nodeStop, kownHandlers);
                }
            }
        }
        return setSurchargeGroup(formulaireObjet, group);
    }


    @Override
    public Object processElement(SubNoticeMetadata item, SubNotice notice, ConfigurationConsultationDTO configurationConsultationDTO, Object formulaireObjet, String id, EFormsFormulaireEntity saved, String nodeStop) {
        if (configurationConsultationDTO == null || configurationConsultationDTO.getContexte() == null) {
            return formulaireObjet;
        }
        ConsultationContexte contexte = configurationConsultationDTO.getContexte();
        String versionSdk = saved.getVersionSdk();
        FieldDetails fieldDetails = getFields(versionSdk).getFields()
                .stream().filter(fieldDetails1 -> fieldDetails1.getId().equals(id))
                .findFirst().orElse(null);
        if (fieldDetails == null) {
            return formulaireObjet;
        }
        String parentNodeId = fieldDetails.getParentNodeId();
        EformsSurchargeDTO surcharge = item.getSurcharge();
        if (surcharge != null && surcharge.getDefaultValue() != null) {
            return addToObject(formulaireObjet, id, surcharge.getDefaultValue(), nodeStop, parentNodeId, versionSdk);

        }
        boolean repeatable = item.is_repeatable();
        String presetValue = item.get_presetValue();
        if (StringUtils.hasText(presetValue)) {
            Object object = getPresetValue(presetValue, fieldDetails.getType(), versionSdk);
            if (object != null)
                return addToObject(formulaireObjet, id, repeatable ? List.of(object) : object, nodeStop, parentNodeId, versionSdk);
        }

        switch (id) {

            case "OPT-140-Lot", "OPT-140-Part" -> {
                return addToObject(formulaireObjet, id, repeatable ? List.of("1") : "1", nodeStop, parentNodeId, versionSdk);
            }
            case "BT-14-Lot", "BT-14-Part" -> {
                return addToObject(formulaireObjet, id, repeatable ? List.of("non-restricted-document") : "non-restricted-document", nodeStop, parentNodeId, versionSdk);
            }
            case "BT-15-Lot", "BT-15-Part" -> {
                String adresse = contexte.getConsultation().getUrlConsultation();
                if (!StringUtils.hasText(adresse)) {
                    return null;
                }
                return addToObject(formulaireObjet, id, repeatable ? List.of(adresse) : adresse, nodeStop, parentNodeId, versionSdk);
            }


            default -> log.debug("Pas de correspondance pour {} dans le groupe lot", id);
        }
        return formulaireObjet;
    }


}
