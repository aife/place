package fr.atexo.annonces.boot;

import fr.atexo.annonces.reseau.AnnonceIncludeParameterEnum;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Permet de convertir le paramètre d'appel "include" du controller d'Annonce en un Enum de manière insensible à la
 * casse
 */
@Component
public class AnnonceIncludeParameterConverter implements Converter<String, AnnonceIncludeParameterEnum> {
    @Override
    public AnnonceIncludeParameterEnum convert(String source) {
        return AnnonceIncludeParameterEnum.valueOf(source.toUpperCase());
    }
}
