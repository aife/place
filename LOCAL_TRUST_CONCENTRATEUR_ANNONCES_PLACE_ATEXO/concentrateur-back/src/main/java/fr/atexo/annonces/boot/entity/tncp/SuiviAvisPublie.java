package fr.atexo.annonces.boot.entity.tncp;

import fr.atexo.annonces.boot.entity.AcheteurEntity;
import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.boot.entity.converters.UUIDConverter;
import lombok.*;
import org.checkerframework.common.aliasing.qual.Unique;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;


@Table(name = "suivi_avis_publie")
@Builder(toBuilder = true, builderMethodName = "with")
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter
@Getter
@Entity
@EqualsAndHashCode
public class SuiviAvisPublie implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "uuid", updatable = false)
    @Unique
    @Builder.Default
    private String uuid = UUID.randomUUID().toString();

    @Column(name = "statut", nullable = false)
    @Enumerated(EnumType.STRING)
    @Builder.Default
    private SuiviStatut statut = SuiviStatut.CREE;

    @Column(name = "date_creation", nullable = false)
    @Builder.Default
    private Instant creation = Instant.now();

    @Column(name = "date_envoi")
    private Instant envoi;

    @Lob
    @Column(name = "contexte_consultation")
    private String contexteConsultation;

    @Column(name = "date_verification")
    private Instant verification;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "avis", columnDefinition = "longtext")
    @Lob
    private String avis;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "token", columnDefinition = "longtext")
    @Lob
    private String token;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "signature_token", columnDefinition = "longtext")
    @Lob
    private String signatureToken;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "publication", columnDefinition = "longtext")
    @Lob
    private String publication;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "last_notification", columnDefinition = "longtext")
    @Lob
    private String lastNotification;

    @Getter
    @Column(name = "ref_publication")
    private String refPublication;

    @Getter
    @Column(name = "redirection")
    @Lob
    private String redirection;

    @Setter
    @Getter
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_ACHETEUR")
    private AcheteurEntity acheteur;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_SUIVI_ANNONCES")
    private SuiviAnnonce suiviAnnonce;

}
