package fr.atexo.annonces.boot.ws.ressources;


import java.io.File;

public interface RessourcesWsPort {

    //File downloadAvisTemplate();
    File downloadAvisTemplate(String ws, boolean simplifie);
}
