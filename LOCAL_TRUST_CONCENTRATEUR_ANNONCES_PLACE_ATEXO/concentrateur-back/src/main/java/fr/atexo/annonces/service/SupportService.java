package fr.atexo.annonces.service;

import fr.atexo.annonces.dto.ConsultationFiltreDTO;
import fr.atexo.annonces.dto.OffreDTO;
import fr.atexo.annonces.dto.SupportDTO;

import java.util.List;

/**
 * Couche service pour manipuler les supports de publication
 */
public interface SupportService {

    List<SupportDTO> getSupports(Double montant, Boolean national, Boolean europeen, List<String> jal, List<String> pqr,
                                 boolean includeOffres, String idPlateform, Integer idConsultation, List<String> typeProcedure, List<String> organismes, List<String> blocs, Boolean onlyJal, Boolean onlyPqr);

    List<OffreDTO> getPlateformeOffres(Double montant, Boolean national, Boolean europeen, List<String> jal, List<String> pqr,
                                       String idPlateform, Integer idConsultation, List<String> typeProcedure, List<String> organismes, List<String> blocs);

    ConsultationFiltreDTO getFiltreSupport(Integer idConsultation, String idPlateforme, Boolean europeen);

    List<SupportDTO> getAllSupports();

    List<String> getSupportCodeGroupe(String idPlatform, List<String> typeProcedures, List<String> organismes, List<String> blocs);

    List<SupportDTO> getSupportPlateforme(String idPlatform, List<String> typeProcedures, List<String> organismes, List<String> blocs);
}
