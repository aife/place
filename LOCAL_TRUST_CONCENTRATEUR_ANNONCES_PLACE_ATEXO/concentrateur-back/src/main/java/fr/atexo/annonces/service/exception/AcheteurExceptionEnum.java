package fr.atexo.annonces.service.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum AcheteurExceptionEnum {
    NOT_FOUND(404, "Acheteur inexistant");

    private final int code;
    private final String type;

}
