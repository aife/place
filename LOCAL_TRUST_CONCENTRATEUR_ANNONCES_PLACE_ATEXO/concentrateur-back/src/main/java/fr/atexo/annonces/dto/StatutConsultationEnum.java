package fr.atexo.annonces.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatutConsultationEnum {
    STATUS_ELABORATION("0"),

    STATUS_PREPARATION("1"),

    STATUS_CONSULTATION("2"),

    STATUS_OUVERTURE_ANALYSE("3"),

    STATUS_DECISION("4"),

    STATUS_A_ARCHIVER("5"),

    STATUS_ARCHIVE_REALISEE("6"),

    STATUS_APRES_DECISION("7"),

    STATUS_AVANT_ARCHIVE("8");

    private final String code;
}
