package fr.atexo.annonces.boot.core.domain.e_forms_sdk.model;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RepeatableDetails {
    private boolean value;
    private String severity;
}
