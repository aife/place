package fr.atexo.annonces.boot.entity;

import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter
@Getter
@Entity
@EqualsAndHashCode
@Table(name = "E_FORMS_PROCEDURE")
@EntityListeners(AuditingEntityListener.class)
public class EFormsProcedureEntity implements Serializable {

    private static final long serialVersionUID = 3294968555933851444L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "UUID", nullable = false)
    private String uuid;

    @Column(name = "CODE", unique = true, nullable = false)
    private String code;

    @Column(name = "LIBELLE", unique = true, nullable = false)
    private String libelle;

}
