package fr.atexo.annonces.modeles;

import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.boot.entity.SuiviTypeAvisPub;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;

@AllArgsConstructor
@Getter
@Setter
@Builder
public class DashboardSuiviTypeAvisSpecification implements Specification<SuiviTypeAvisPub> {

    private static final long serialVersionUID = 1222004680882504439L;

    private Set<String> plateformes;
    private Set<String> references;
    private Set<String> organismes;
    private Set<String> supports;
    private Set<String> procedures;
    private Set<String> offres;
    private Set<StatutTypeAvisAnnonceEnum> statuts;
    private Boolean europeen;
    private Boolean sip;
    private Set<Integer> idConsultation;
    private ZonedDateTime dateEnvoiEuropeenPrevisionnelMax;
    private ZonedDateTime dateEnvoiNationalPrevisionnelMax;
    private ZonedDateTime dateMiseEnligneCalculeMax;
    private ZonedDateTime dateMiseEnligneCalculeMin;
    private ZonedDateTime dateModificationMax;
    private ZonedDateTime dateModificationMin;
    private String dateValidationMax;
    private String dateValidationMin;
    private String dateDlroMax;
    private String dateDlroMin;
    private Set<String> identifiantValidateurs;
    private Set<String> services;


    @Override
    public Predicate toPredicate(Root<SuiviTypeAvisPub> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        criteriaQuery.distinct(true);
        Predicate predicate = criteriaBuilder.isEmpty(root.get("children"));
        predicate = criteriaBuilder.and(predicate, criteriaBuilder.isNotEmpty(root.get("annonces")));


        predicate = addNotNullFilter(root, criteriaBuilder, predicate, "dateMiseEnLigne");
        if (!CollectionUtils.isEmpty(this.plateformes)) {
            predicate = criteriaBuilder.and(predicate, root.get("organisme").get("plateformeUuid").in(this.plateformes));
        }
        if (!CollectionUtils.isEmpty(this.procedures)) {
            predicate = criteriaBuilder.and(predicate, root.get("procedure").get("abbreviation").in(this.procedures));
        }
        if (!CollectionUtils.isEmpty(this.supports)) {
            Join<SuiviTypeAvisPub, SuiviAnnonce> annonceJoin = root.join("annonces", JoinType.LEFT);
            predicate = addSupportFilter(annonceJoin, criteriaBuilder, predicate);
        }
        if (!CollectionUtils.isEmpty(this.organismes)) {
            predicate = criteriaBuilder.and(predicate, root.get("organisme").get("sigleOrganisme").in(this.organismes));
        }
        if (!CollectionUtils.isEmpty(this.services)) {
            predicate = criteriaBuilder.and(predicate, root.get("service").get("code").in(this.services));
        }
        if (!CollectionUtils.isEmpty(this.offres)) {
            predicate = criteriaBuilder.and(predicate, root.get("offreRacine").get("code").in(this.offres));

        }

        if (!CollectionUtils.isEmpty(this.idConsultation)) {
            predicate = addInFilter(root, criteriaBuilder, predicate, "idConsultation", idConsultation);
        }
        if (!CollectionUtils.isEmpty(statuts)) {
            predicate = addStatutFilter(root, criteriaBuilder, predicate, this.statuts);
        } else if (Boolean.TRUE.equals(sip)) {
            predicate = addStatutFilter(root, criteriaBuilder, predicate, Set.of(
                    StatutTypeAvisAnnonceEnum.REJETE,
                    StatutTypeAvisAnnonceEnum.REJETE_SIP,
                    StatutTypeAvisAnnonceEnum.ENVOYE,
                    StatutTypeAvisAnnonceEnum.ENVOI_PLANIFIER,
                    StatutTypeAvisAnnonceEnum.EN_ATTENTE_VALIDATION_SIP));

        } else {
            predicate = addStatutFilter(root, criteriaBuilder, predicate, Set.of(
                    StatutTypeAvisAnnonceEnum.REJETE,
                    StatutTypeAvisAnnonceEnum.REJETE_ECO,
                    StatutTypeAvisAnnonceEnum.ENVOYE,
                    StatutTypeAvisAnnonceEnum.ENVOI_PLANIFIER,
                    StatutTypeAvisAnnonceEnum.EN_ATTENTE_VALIDATION_SIP,
                    StatutTypeAvisAnnonceEnum.EN_ATTENTE_VALIDATION_ECO));
        }
        if (europeen != null) {
            predicate = addEqualsFilter(root, criteriaBuilder, predicate, "europeen", europeen);
        }
        if (!CollectionUtils.isEmpty(this.references)) {
            predicate = addInFilter(root, criteriaBuilder, predicate, "reference", references);
        }
        if (sip != null) {
            predicate = addFacturationFilter(root, criteriaBuilder, predicate);
        }
        if (dateMiseEnligneCalculeMax != null) {
            predicate = addDateLessThenFilter(root, criteriaBuilder, predicate, "dateMiseEnLigneCalcule", dateMiseEnligneCalculeMax);
        }
        if (dateEnvoiNationalPrevisionnelMax != null) {
            predicate = addDateLessThenFilter(root, criteriaBuilder, predicate, "dateEnvoiNationalPrevisionnel", dateEnvoiNationalPrevisionnelMax);
        }
        if (dateEnvoiEuropeenPrevisionnelMax != null) {
            predicate = addDateLessThenFilter(root, criteriaBuilder, predicate, "dateEnvoiEuropeenPrevisionnel", dateEnvoiEuropeenPrevisionnelMax);
        }
        if (dateMiseEnligneCalculeMin != null) {
            predicate = addDateGreaterThenFilter(root, criteriaBuilder, predicate, "dateMiseEnLigneCalcule", dateMiseEnligneCalculeMin);
        }
        if (dateModificationMin != null) {
            predicate = addDateGreaterThenFilter(root, criteriaBuilder, predicate, "dateModification", dateModificationMin);
        }
        if (dateModificationMax != null) {
            predicate = addDateLessThenFilter(root, criteriaBuilder, predicate, "dateModification", dateModificationMax);
        }
        if (dateValidationMin != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            ZonedDateTime date = LocalDate.parse(dateValidationMin, formatter).atStartOfDay().atZone(ZoneId.of("Europe/Paris"));
            predicate = addDateGreaterThenFilter(root, criteriaBuilder, predicate, "dateValidation", date);
        }
        if (dateValidationMax != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            ZonedDateTime date = LocalDate.parse(dateValidationMax, formatter).atStartOfDay().atZone(ZoneId.of("Europe/Paris"));
            predicate = addDateLessThenFilter(root, criteriaBuilder, predicate, "dateValidation", date);
        }
        if (dateDlroMin != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            ZonedDateTime date = LocalDate.parse(dateDlroMin, formatter).atStartOfDay().atZone(ZoneId.of("Europe/Paris"));
            predicate = addDateGreaterThenFilter(root, criteriaBuilder, predicate, "dlro", date);
        }
        if (dateDlroMax != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            ZonedDateTime date = LocalDate.parse(dateDlroMax, formatter).atStartOfDay().atZone(ZoneId.of("Europe/Paris"));
            predicate = addDateLessThenFilter(root, criteriaBuilder, predicate, "dlro", date);
        }
        if (!CollectionUtils.isEmpty(this.identifiantValidateurs)) {
            Predicate validateur = criteriaBuilder.disjunction();
            validateur.getExpressions().add(root.get("validateur").get("identifiant").in(identifiantValidateurs));
            predicate = criteriaBuilder.and(predicate, validateur);
        }
        return predicate;
    }


    private Predicate addEqualsFilter(Root<SuiviTypeAvisPub> root, CriteriaBuilder criteriaBuilder,
                                      Predicate predicate, String key, Object value) {
        Predicate paysPredict = criteriaBuilder.disjunction();
        paysPredict.getExpressions().add(criteriaBuilder.equal(root.get(key), value));
        return criteriaBuilder.and(predicate, paysPredict);
    }

    private Predicate addInFilter(Root<SuiviTypeAvisPub> root, CriteriaBuilder criteriaBuilder,
                                  Predicate predicate, String key, Object value) {
        Predicate paysPredict = criteriaBuilder.disjunction();
        paysPredict.getExpressions().add(root.get(key).in(value));
        return criteriaBuilder.and(predicate, paysPredict);
    }

    private Predicate addSupportFilter(Join<SuiviTypeAvisPub, SuiviAnnonce> annonceJoin, CriteriaBuilder criteriaBuilder,
                                       Predicate predicate) {
        return criteriaBuilder.and(predicate, annonceJoin.get("support").get("code").in(this.supports));
    }

    private Predicate addOffresFilter(Join<SuiviTypeAvisPub, SuiviAnnonce> annonceJoin, CriteriaBuilder criteriaBuilder,
                                      Predicate predicate) {
        return criteriaBuilder.and(predicate, annonceJoin.get("offre").get("code").in(this.offres));
    }

    private Predicate addFacturationFilter(Root<SuiviTypeAvisPub> root, CriteriaBuilder criteriaBuilder,
                                           Predicate predicate) {
        Predicate equal = criteriaBuilder.equal(root.get("facturation").get("sip"), sip);
        if (Boolean.TRUE.equals(sip)) {
            equal = criteriaBuilder.and(equal, criteriaBuilder.equal(root.get("validerEco"), sip));
        }
        return criteriaBuilder.and(predicate, equal);
    }


    private Predicate addStatutFilter(Root<SuiviTypeAvisPub> root, CriteriaBuilder criteriaBuilder,
                                      Predicate predicate, Set<StatutTypeAvisAnnonceEnum> statuts) {
        return criteriaBuilder.and(predicate, addInFilter(root, criteriaBuilder, predicate, "statut", statuts));
    }

    private Predicate addDateLessThenFilter(Root<SuiviTypeAvisPub> root, CriteriaBuilder criteriaBuilder,
                                            Predicate predicate, String key, ZonedDateTime date) {

        Predicate datePredict = criteriaBuilder.disjunction();
        datePredict.getExpressions().add(criteriaBuilder.lessThanOrEqualTo(root.get(key), date));
        return criteriaBuilder.and(predicate, datePredict);
    }

    private Predicate addDateGreaterThenFilter(Root<SuiviTypeAvisPub> root, CriteriaBuilder criteriaBuilder,
                                               Predicate predicate, String key, ZonedDateTime date) {
        Predicate datePredict = criteriaBuilder.disjunction();
        datePredict.getExpressions().add(criteriaBuilder.greaterThanOrEqualTo(root.get(key), date));
        return criteriaBuilder.and(predicate, datePredict);
    }


    private Predicate addNotNullFilter(Root<SuiviTypeAvisPub> root, CriteriaBuilder criteriaBuilder,
                                       Predicate predicate, String key) {

        Predicate datePredict = criteriaBuilder.disjunction();
        datePredict.getExpressions().add(criteriaBuilder.isNotNull(root.get(key)));
        return criteriaBuilder.and(predicate, datePredict);
    }

}
