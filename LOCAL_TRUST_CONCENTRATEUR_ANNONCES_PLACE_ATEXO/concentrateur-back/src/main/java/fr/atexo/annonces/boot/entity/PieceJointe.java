package fr.atexo.annonces.boot.entity;

import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "PIECE_JOINTE")
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class PieceJointe implements Serializable {

    private static final long serialVersionUID = -7165888978484811903L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "nom")
    private String name;

    @Column(name = "chemin")
    private String path;

    @Column(name = "content_type")
    private String contentType;

    @Column(name = "md5")
    private String md5;

    @Column(name = "date_creation")
    @CreatedDate
    private ZonedDateTime creationDate;

    @Column(name = "taille")
    private Long size;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PIECE_JOINTE_PARENTE", referencedColumnName = "ID")
    private PieceJointe pieceJointeParente;

    @Column(name = "ID_MPE_OLD")
    private String idMpeOld;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_AGENT")
    @CreatedBy
    private Agent agent;

}
