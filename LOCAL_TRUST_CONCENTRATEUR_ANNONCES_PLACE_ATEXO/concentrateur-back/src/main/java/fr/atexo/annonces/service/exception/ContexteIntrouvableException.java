package fr.atexo.annonces.service.exception;

import lombok.Getter;
import org.slf4j.helpers.MessageFormatter;

import java.util.UUID;

@Getter
public class ContexteIntrouvableException extends RuntimeException {

	private final UUID uuid;

	public ContexteIntrouvableException(final UUID uuid) {
		super(MessageFormatter.format("Le contexte {} est introuvable", uuid).getMessage());

		this.uuid = uuid;
	}
}
