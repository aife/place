package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.entity.*;
import fr.atexo.annonces.boot.ws.docgen.KeyValueRequest;
import fr.atexo.annonces.dto.EditionRequest;
import fr.atexo.annonces.dto.FileStatus;

import java.io.File;
import java.io.InputStream;
import java.util.List;

public interface EspaceDocumentaireService {

    boolean fileExists(PieceJointe document);

    String getEditionToken(Agent agent, String prefix, String callback, String id, String token, PieceJointe document);

    String getValidationToken(Agent agent, String prefix, String callback, String id, String token, PieceJointe document, String statutRedaction);

    InputStream loadFileAsResourcePDF(String chemin, String extension);

    InputStream loadFileAsResource(String chemin, String extension);


    PieceJointe getPieceJointeFromDocgenValidation(PieceJointe document, String tokenEdition, String id);


    PieceJointe getPieceJointeFromDocgenEdit(PieceJointe document, String tokenEdition, String id);

    EditionRequest getUrlEdition(String editionToken);

    File downloadAvisTemplate(Offre offre, boolean simplifie);

    PieceJointe getPieceJointe(String libelle, List<KeyValueRequest> map, File template, String extension);


    FileStatus getDocumentStatus(String tokenEdition);
}
