package fr.atexo.annonces.reseau;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Classe pour gérer l'envoi de mail
 */
public class MailEnvoi {

    /**
     * fichier journal
     */
    private static final Logger LOG = LoggerFactory.getLogger(MailEnvoi.class);

    /**
     * Permet d'activer ou désactiver l'alerte par mail
     */
    private static String alerteActive = null;

    /**
     * L'email source : celui de demat
     */
    private static String expediteur = null;

    /**
     * L'email source : celui de demat
     */
    private static String destinataire = null;

    /**
     * L'email source : celui de demat
     */
    private static String copieCachee = null;

    /**
     * La machine qui gére l'envoi d'email smtp
     */
    private static String smtpHost = null;

    /**
     * Port réseau
     */
    private static String smtpPort = null;

    /**
     * Mot de passe du compte expéditeur
     */
    private static String password = null;

    /**
     * Méthode qui gère l'envoi de mail
     *
     * @param contenu
     * @param objet
     */
    public static void envoyerMail(final String contenu, final String objet) {

        if (!"oui".equalsIgnoreCase(alerteActive)) {
            return; // l'alerte mail n'est pas active, on n'envoie pas de mail dans ce cas
        }

        try {
            // Récupère les propriétés du systeme
            Properties props = System.getProperties();

            // Spécification du serveur mail
            props.put("mail.smtp.host", smtpHost);
            props.put("mail.smtp.from", expediteur);
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.socketFactory.port", smtpPort);
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.socketFactory.fallback", "false");
            props.put("mail.smtp.starttls.enable", "true"); // tls actif pour gmail

            // Récupère la session
            Session session = Session.getDefaultInstance(props, new Authenticator() {

                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(expediteur, password);
                }
            });

            // Définition du message
            MimeMessage message = new MimeMessage(session);

            // Spécification de l'expéditeur
            message.setFrom(new InternetAddress(expediteur));

            // Spécification du destinataire
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinataire));

            // ajoute de la copie cachée si elle existe
            if (!"".equals(copieCachee)) {
                message.addRecipient(Message.RecipientType.CC, new InternetAddress(copieCachee));
            }

            // objet du mail
            message.setSubject(objet);

            // contenu du mail
            message.setText(contenu);
            LOG.debug("Contenu mail envoye: " + contenu);

            Transport.send(message);
        } catch (AddressException addressException) {
            LOG.error(addressException.getMessage());
        } catch (MessagingException messagingException) {
            LOG.error(messagingException.getMessage());
        }
    }

    /**
     * Définit la copie cachée de l'alerte mail
     *
     * @param copieCachee
     */
    public final void setCopieCachee(final String copieCachee) {
        MailEnvoi.copieCachee = copieCachee;
    }

    /**
     * Définit le destinataire de l'alerte mail
     *
     * @param destinataire
     */
    public final void setDestinataire(final String destinataire) {
        MailEnvoi.destinataire = destinataire;
    }

    /**
     * Définit l'expéditeur de l'alerte mail
     *
     * @param expediteur
     */
    public final void setExpediteur(final String expediteur) {
        MailEnvoi.expediteur = expediteur;
    }

    /**
     * Définit le host smtp
     *
     * @param smtpHost
     */
    public final void setSmtpHost(final String smtpHost) {
        MailEnvoi.smtpHost = smtpHost;
    }

    /**
     * Définit le port smtp
     *
     * @param smtpPort
     */
    public final void setSmtpPort(final String smtpPort) {
        MailEnvoi.smtpPort = smtpPort;
    }

    /**
     * Définit le mot de passe du compte mail
     *
     * @param password
     */
    public final void setPassword(final String password) {
        MailEnvoi.password = password;
    }

    /**
     * Définit si l'alerte mail est active ou non
     *
     * @param alerteActive
     */
    public final void setAlerteActive(final String alerteActive) {
        MailEnvoi.alerteActive = alerteActive;
    }
}
