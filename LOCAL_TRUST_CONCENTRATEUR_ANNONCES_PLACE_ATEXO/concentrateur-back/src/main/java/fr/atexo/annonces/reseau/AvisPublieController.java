package fr.atexo.annonces.reseau;

import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.dto.SuiviTypeAvisPubDTO;
import fr.atexo.annonces.modeles.StatutSuiviAnnonceEnum;
import fr.atexo.annonces.modeles.StatutTypeAvisAnnonceEnum;
import fr.atexo.annonces.service.SuiviAnnonceService;
import fr.atexo.annonces.service.SuiviTypeAvisPubService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/rest/v2/avis-publie")
@Tag(name = "AvisPublieController")
@Slf4j
public class AvisPublieController {


    private final SuiviTypeAvisPubService suiviTypeAvisPubService;
    private final SuiviAnnonceService suiviAnnonceService;

    public AvisPublieController(SuiviTypeAvisPubService suiviTypeAvisPubService, SuiviAnnonceService suiviAnnonceService) {
        this.suiviTypeAvisPubService = suiviTypeAvisPubService;
        this.suiviAnnonceService = suiviAnnonceService;
    }


    @GetMapping("{id}/download")
    public void getPortailPdf(@PathVariable Long id, @RequestParam String idPlatform, @RequestParam Integer idConsultation, HttpServletResponse response) throws IOException {
        SuiviTypeAvisPubDTO suiviTypeAvisPubDTO = suiviTypeAvisPubService.get(id, idPlatform, idConsultation);
        List<StatutTypeAvisAnnonceEnum> autorise = List.of(StatutTypeAvisAnnonceEnum.REJETE, StatutTypeAvisAnnonceEnum.REJETE_SIP, StatutTypeAvisAnnonceEnum.REJETE_ECO, StatutTypeAvisAnnonceEnum.ENVOYE, StatutTypeAvisAnnonceEnum.ENVOI_PLANIFIER);
        if (!autorise.contains(suiviTypeAvisPubDTO.getStatut())) {
            throw new IllegalArgumentException("L'avis n'est pas dans un état autorisé pour être exporté");
        }
        try {
            SuiviAnnonce annonce = suiviTypeAvisPubService.getPortailAnnonce(id, idPlatform, idConsultation);
            String filename = suiviAnnonceService.geDocumentName(annonce, true);
            response.setHeader("Content-Disposition", "attachment; filename=" + filename);
            response.setContentType(MediaType.APPLICATION_PDF_VALUE);
            suiviAnnonceService.exportToDocument(annonce, response.getOutputStream(), false, true);
        } catch (Exception e) {
            resetServletResponse(response, e);
            throw e;
        }
    }

    @GetMapping("annonces/{id}/download")
    public void getAnnoncePdf(@PathVariable Integer id, HttpServletResponse response) throws IOException {
        List<StatutSuiviAnnonceEnum> autorise = List.of(StatutSuiviAnnonceEnum.ENVOI_PLANIFIER, StatutSuiviAnnonceEnum.ARRETER, StatutSuiviAnnonceEnum.EN_COURS_DE_PUBLICATION, StatutSuiviAnnonceEnum.REJETER_CONCENTRATEUR, StatutSuiviAnnonceEnum.REJETER_SUPPORT, StatutSuiviAnnonceEnum.PUBLIER, StatutSuiviAnnonceEnum.REJETER_CONCENTRATEUR_VALIDATION_ECO, StatutSuiviAnnonceEnum.REJETER_CONCENTRATEUR_VALIDATION_SIP);
        SuiviAnnonce annonce = suiviAnnonceService.getSuiviAnnonceEntity(id);
        if (!autorise.contains(annonce.getStatut())) {
            throw new IllegalArgumentException("L'annonce n'est pas dans un état autorisé pour être exporté");
        }
        try {
            String filename = suiviAnnonceService.geDocumentName(annonce, true);
            response.setHeader("Content-Disposition", "attachment; filename=" + filename);
            response.setContentType(MediaType.APPLICATION_PDF_VALUE);
            suiviAnnonceService.exportToDocument(annonce, response.getOutputStream(), false, true);
        } catch (Exception e) {
            resetServletResponse(response, e);
            throw e;
        }
    }

    private void resetServletResponse(HttpServletResponse response, Exception e) {
        try {
            response.reset();
        } catch (IllegalStateException f) {
            log.error("Erreur survenue lors de la génération du PDF ; impossible de transmettre cette erreur" + " au client car des données du PDF ont déjà été envoyées", e);
            throw f;
        }
    }

}
