package fr.atexo.annonces.mapper;

import fr.atexo.annonces.boot.entity.PlateformeConfiguration;
import fr.atexo.annonces.dto.PlateformeConfigurationDTO;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * Mapper entre l'entité Support et son DTO
 */
@Mapper(uses = {StringAndListStringMapper.class})
public interface PlateformeConfigurationMapper {

    PlateformeConfigurationDTO mapToModel(PlateformeConfiguration entity);

    List<PlateformeConfigurationDTO> mapToModels(List<PlateformeConfiguration> entity);

    PlateformeConfiguration mapToEntity(PlateformeConfigurationDTO dto);
}
