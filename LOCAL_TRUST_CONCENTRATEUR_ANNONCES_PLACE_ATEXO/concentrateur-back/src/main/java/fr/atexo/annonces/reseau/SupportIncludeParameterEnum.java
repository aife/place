package fr.atexo.annonces.reseau;

/**
 * Paramètre d'appel aux méthodes du controller de Support. Indique quelles sont les sous-ressources du Support que
 * le client voudrait récupérer en même temps que le Support.
 */
public enum SupportIncludeParameterEnum {
    OFFRES
}
