package fr.atexo.annonces.boot.core.domain.e_forms_notice.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public class EFormsFormulaireAdd extends EFormsFormulaireId implements Serializable {

    private String organisme;
    private String tokenMpe;
    private String mpeUrl;
    private String auteurEmail;

}
