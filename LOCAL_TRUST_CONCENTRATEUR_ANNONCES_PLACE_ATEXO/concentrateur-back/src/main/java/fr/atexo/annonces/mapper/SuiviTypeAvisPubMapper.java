package fr.atexo.annonces.mapper;

import fr.atexo.annonces.boot.entity.SuiviTypeAvisPub;
import fr.atexo.annonces.config.DaoMapper;
import fr.atexo.annonces.dto.SuiviTypeAvisPubAdd;
import fr.atexo.annonces.dto.SuiviTypeAvisPubDTO;
import fr.atexo.annonces.dto.SuiviTypeAvisPubRevisionDTO;
import fr.atexo.annonces.dto.ValidationHistoriqueDTO;
import fr.atexo.annonces.modeles.StatutTypeAvisAnnonceEnum;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


@Mapper(uses = {ConsultationFacturationMapper.class, TypeProcedureMapper.class, OffreMapper.class, SupportMapper.class, SuiviAnnonceMapper.class})
public interface SuiviTypeAvisPubMapper extends DaoMapper<SuiviTypeAvisPub, SuiviTypeAvisPubDTO> {

    @Override
    @Mapping(target = "historiques", qualifiedByName = "mapActionsTypeAvis", source = "entity")
    @Mapping(target = "delaiSoumission", qualifiedByName = "toDelaiSoumission", source = "entity")
    SuiviTypeAvisPubDTO mapToModel(SuiviTypeAvisPub entity);

    SuiviTypeAvisPubRevisionDTO mapToRevision(SuiviTypeAvisPub entity);

    @Named("toDelaiSoumission")
    default Long toDelaiSoumission(SuiviTypeAvisPub entity) {
        if (entity == null) {
            return null;
        }
        ZonedDateTime dateLimite = ZonedDateTime.now();
        ZonedDateTime dateMiseEnLigne = entity.getDateMiseEnLigne();
        if (dateMiseEnLigne != null && dateMiseEnLigne.isAfter(dateLimite)) {
            dateLimite = dateMiseEnLigne;
        }

        ZonedDateTime dlro = entity.getDlro();
        if (dlro != null) {
            return Duration.between(dateLimite, dlro).toDays();

        } else {
            return null;
        }

    }

    @Named("mapActionsTypeAvis")
    default List<ValidationHistoriqueDTO> mapActionsTypeAvis(SuiviTypeAvisPub entity) {
        List<ValidationHistoriqueDTO> map = new ArrayList<>();
        ZonedDateTime dateValidationSip = entity.getDateValidationSip();
        if (dateValidationSip != null) {
            map.add(ValidationHistoriqueDTO.builder()
                    .date(dateValidationSip)
                    .statut(Boolean.TRUE.equals(entity.getValiderSip()) ? StatutTypeAvisAnnonceEnum.ENVOI_PLANIFIER : StatutTypeAvisAnnonceEnum.REJETE)
                    .build());
        }
        ZonedDateTime dateValidationEco = entity.getDateValidationEco();
        if (dateValidationSip != null) {
            map.add(ValidationHistoriqueDTO.builder()
                    .date(dateValidationEco)
                    .statut(Boolean.TRUE.equals(entity.getValiderEco()) ? StatutTypeAvisAnnonceEnum.EN_ATTENTE_VALIDATION_SIP : StatutTypeAvisAnnonceEnum.REJETE)
                    .build());
        }
        ZonedDateTime dateEnvoiNational = entity.getDateEnvoiNational();
        ZonedDateTime dateEnvoiEuropeen = entity.getDateEnvoiEuropeen();
        if (dateEnvoiEuropeen != null || dateEnvoiNational != null) {
            map.add(ValidationHistoriqueDTO.builder().date(dateEnvoiEuropeen != null ? dateEnvoiEuropeen : dateEnvoiNational)
                    .statut(StatutTypeAvisAnnonceEnum.ENVOYE)
                    .build());
        }

        return map.stream()
                .filter(validationHistoriqueDTO -> validationHistoriqueDTO.getDate() != null)
                .sorted(Comparator.comparing(ValidationHistoriqueDTO::getDate))
                .collect(Collectors.toList());
    }

    @Mapping(target = "organisme", ignore = true)
    @Mapping(target = "service", ignore = true)
    @Mapping(target = "procedure", ignore = true)
    SuiviTypeAvisPub mapToEntity(SuiviTypeAvisPubAdd avisPub);
}
