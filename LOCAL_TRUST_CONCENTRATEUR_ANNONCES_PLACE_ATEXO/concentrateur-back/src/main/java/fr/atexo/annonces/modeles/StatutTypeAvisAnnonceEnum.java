package fr.atexo.annonces.modeles;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

@AllArgsConstructor
@Getter
public enum StatutTypeAvisAnnonceEnum {

    BROUILLON(1), EN_ATTENTE_VALIDATION_ECO(3), EN_ATTENTE_VALIDATION_SIP(4), REJETE_ECO(5), REJETE_SIP(6), REJETE(7), ENVOI_PLANIFIER(8), ENVOYE(9);
    private final int code;

    public static StatutTypeAvisAnnonceEnum getStatutFromAnnonceEnum(List<StatutSuiviAnnonceEnum> statuts) {
        if (statuts == null || statuts.isEmpty()) {
            return BROUILLON;
        }
        return statuts.stream().filter(Objects::nonNull).map(statut -> switch (statut) {
            case REJETER_CONCENTRATEUR_VALIDATION_SIP -> REJETE_SIP;
            case REJETER_CONCENTRATEUR_VALIDATION_ECO -> REJETE_ECO;
            case EN_COURS_D_ARRET, ARRETER, REJETER_SUPPORT, REJETER_CONCENTRATEUR -> REJETE;
            case EN_ATTENTE_VALIDATION_SIP -> EN_ATTENTE_VALIDATION_SIP;
            case EN_ATTENTE_VALIDATION_ECO -> EN_ATTENTE_VALIDATION_ECO;
            case EN_COURS_DE_PUBLICATION, PUBLIER -> ENVOYE;
            case EN_ATTENTE, ENVOI_PLANIFIER -> ENVOI_PLANIFIER;
            case BROUILLON, COMPLET, EXTERNAL -> BROUILLON;
            default -> null;
        }).filter(Objects::nonNull).min(Comparator.comparingInt(StatutTypeAvisAnnonceEnum::getCode)).orElse(BROUILLON);


    }
}
