package fr.atexo.annonces.service;

import java.io.OutputStream;

/**
 * Définit une méthode qui exporte un objet vers un fichier PDF
 */
public interface PdfExportable<T> {
    void exportToDocument(T object, OutputStream exportPdf, Boolean simplifier, boolean convertPdf);

    String geDocumentName(T object, boolean convertPdf);
}
