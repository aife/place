package fr.atexo.annonces.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TrackDocumentResponse {
    private String error;
}
