package fr.atexo.annonces.boot.core.domain.e_forms_notice.exception;

import fr.atexo.annonces.service.exception.AtexoException;
import lombok.Getter;

@Getter
public class NoticeException extends AtexoException {

    public NoticeException(NoticeExceptionEnum noticeExceptionEnum, String errorMessage, Throwable err) {
        super(err);
        this.message = errorMessage;
        this.code = noticeExceptionEnum.getCode();
        this.type = noticeExceptionEnum.getType();
    }

    public NoticeException(NoticeExceptionEnum noticeExceptionEnum, String errorMessage) {
        super();
        this.message = errorMessage;
        this.code = noticeExceptionEnum.getCode();
        this.type = noticeExceptionEnum.getType();
    }
}
