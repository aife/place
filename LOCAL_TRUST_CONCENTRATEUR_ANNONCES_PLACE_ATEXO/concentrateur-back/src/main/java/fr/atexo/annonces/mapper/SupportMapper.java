package fr.atexo.annonces.mapper;

import fr.atexo.annonces.boot.entity.Support;
import fr.atexo.annonces.dto.SupportDTO;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

/**
 * Mapper entre l'entité Support et son DTO
 */
@Mapper(uses = {OffreMapper.class, DepartementMapper.class})
public interface SupportMapper {

    @Mapping(target = "offres", ignore = true)
    @Named("ignoreOffres")
    SupportDTO supportToSupportDTOIgnoreOffres(Support support);

    @Mapping(target = "offres", qualifiedByName = "listIgnoreSupport")
    SupportDTO supportToSupportDTO(Support support);

    @IterableMapping(qualifiedByName = "ignoreOffres")
    List<SupportDTO> supportsToSupportDTOsIgnoreOffres(List<Support> supports);

    List<SupportDTO> supportsToSupportDTOs(List<Support> supports);

    default List<SupportDTO> supportsToSupportDTOs(List<Support> supports, boolean includeOffres) {
        if (includeOffres) {
            return supportsToSupportDTOs(supports);
        } else {
            return supportsToSupportDTOsIgnoreOffres(supports);
        }
    }

}
