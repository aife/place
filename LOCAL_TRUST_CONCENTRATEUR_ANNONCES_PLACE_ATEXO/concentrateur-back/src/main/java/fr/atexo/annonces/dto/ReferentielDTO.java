package fr.atexo.annonces.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class ReferentielDTO implements Serializable {
    protected Integer id;

    protected String code;

    protected String libelle;

    protected String idExterne;

    protected Boolean avisExterne;

    protected Boolean racine;


}
