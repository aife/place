package fr.atexo.annonces.service;

import org.springframework.stereotype.Component;

import java.security.SignatureException;

@Component
public interface TemplateService {

    <T> String merge(String template, T data) throws SignatureException;
}
