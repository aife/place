package fr.atexo.annonces.reseau;

import com.atexo.annonces.commun.mpe.EnvoiMpe;
import fr.atexo.annonces.boot.configuration.http_logging.LogEntryExit;
import fr.atexo.annonces.service.AgentService;
import org.springframework.boot.logging.LogLevel;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/rest/v2/agents")
public class AgentController {


    private final AgentService agentService;

    public AgentController(AgentService agentService) {
        this.agentService = agentService;
    }

    @PostMapping
    @PreAuthorize("#oauth2.hasScope('platform:' + #envoi.getAgent().getPlateforme())")
    @LogEntryExit(LogLevel.INFO)
    public String addAgent(@RequestBody EnvoiMpe envoi,
                           @RequestHeader("Authorization") String token) {
        return agentService.addAgent(envoi, token.replace("Bearer ", ""));
    }

    @GetMapping("/exists")
    public Boolean getAgentHealth(@RequestHeader("Authorization") String token) {
        return agentService.existByToken(token.replace("Bearer ", ""));
    }
}
