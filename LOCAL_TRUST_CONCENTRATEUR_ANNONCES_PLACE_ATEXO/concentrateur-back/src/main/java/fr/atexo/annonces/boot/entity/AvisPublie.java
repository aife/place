package fr.atexo.annonces.boot.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;


@Entity
@Table(name = "BOAMP_AVIS_PUBLIE")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AvisPublie implements Serializable {
    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;

    @Column(name = "ID_AVIS", length = 60)
    private String idAvis;

    @Column(name = "TYPE", length = 20)
    private String type;


    @Column(name = "UID_CONSULTATION", length = 200)
    private String uidConsultation;


    @Column(name = "LOGIN_BOAMP", length = 200)
    private String loginBoamp;


    @Column(name = "PASSWORD_BOAMP", length = 200)
    private String passwordBoamp;

    @Column(name = "UID_AGENT_EMETTEUR", length = 200)
    private String uidAgentmetteur;

    @Column(name = "UID_PF", length = 200)
    private String uidPlateforme;

    @Column(name = "SERVICE", length = 200)
    private String service;

    @Column(name = "ORGANISME", length = 200)
    private String organisme;

    @Column(name = "DATE_ENVOI")
    private LocalDateTime dateEnvoi;

    @Column(name = "DATE_MAJ")
    private LocalDateTime dateMaj;

    @Lob
    @Column(name = "XML_CONTEXTE")
    private String xmlContexte;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ANNONCE", referencedColumnName = "ID")
    private PieceJointe annonce;

    @Lob
    @Column(name = "XML_FICHIER_COMPLEMENTAIRE")
    private String xmlFichierComplementaire;

    @Lob
    @Column(name = "NOM_FICHIER_ANNONCE")
    private String nomFichierAnnonce;

    @Lob
    @Column(name = "NOM_FICHIER_COMPLEMENTAIRE")
    private String nomFichierComplementaire;

    @Lob
    @Column(name = "SUIVI")
    private String suivi;

    @Column(name = "REFERENCE_UTILISATEUR")
    private String referenceUtilisateur;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdAvis() {
        return idAvis;
    }

    public void setIdAvis(String idAvis) {
        this.idAvis = idAvis;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUidConsultation() {
        return uidConsultation;
    }

    public void setUidConsultation(String uidConsultation) {
        this.uidConsultation = uidConsultation;
    }

    public String getLoginBoamp() {
        return loginBoamp;
    }

    public void setLoginBoamp(String loginBoamp) {
        this.loginBoamp = loginBoamp;
    }

    public String getPasswordBoamp() {
        return passwordBoamp;
    }

    public void setPasswordBoamp(String passwordBoamp) {
        this.passwordBoamp = passwordBoamp;
    }

    public String getUidAgentmetteur() {
        return uidAgentmetteur;
    }

    public void setUidAgentmetteur(String uidAgentmetteur) {
        this.uidAgentmetteur = uidAgentmetteur;
    }

    public String getUidPlateforme() {
        return uidPlateforme;
    }

    public void setUidPlateforme(String uidPlateforme) {
        this.uidPlateforme = uidPlateforme;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getOrganisme() {
        return organisme;
    }

    public void setOrganisme(String organisme) {
        this.organisme = organisme;
    }

    public LocalDateTime getDateEnvoi() {
        return dateEnvoi;
    }

    public void setDateEnvoi(LocalDateTime dateEnvoi) {
        this.dateEnvoi = dateEnvoi;
    }

    public LocalDateTime getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(LocalDateTime dateMaj) {
        this.dateMaj = dateMaj;
    }

    public String getXmlContexte() {
        return xmlContexte;
    }

    public void setXmlContexte(String xmlContexte) {
        this.xmlContexte = xmlContexte;
    }

    public PieceJointe getAnnonce() {
        return annonce;
    }

    public void setAnnonce(PieceJointe annonce) {
        this.annonce = annonce;
    }

    public String getXmlFichierComplementaire() {
        return xmlFichierComplementaire;
    }

    public void setXmlFichierComplementaire(String xmlFichierComplementaire) {
        this.xmlFichierComplementaire = xmlFichierComplementaire;
    }

    public String getNomFichierAnnonce() {
        return nomFichierAnnonce;
    }

    public void setNomFichierAnnonce(String nomFichierAnnonce) {
        this.nomFichierAnnonce = nomFichierAnnonce;
    }

    public String getNomFichierComplementaire() {
        return nomFichierComplementaire;
    }

    public void setNomFichierComplementaire(String nomFichierComplementaire) {
        this.nomFichierComplementaire = nomFichierComplementaire;
    }

    public String getSuivi() {
        return suivi;
    }

    public void setSuivi(String suivi) {
        this.suivi = suivi;
    }

    public String getReferenceUtilisateur() {
        return referenceUtilisateur;
    }

    public void setReferenceUtilisateur(String referenceUtilisateur) {
        this.referenceUtilisateur = referenceUtilisateur;
    }
}
