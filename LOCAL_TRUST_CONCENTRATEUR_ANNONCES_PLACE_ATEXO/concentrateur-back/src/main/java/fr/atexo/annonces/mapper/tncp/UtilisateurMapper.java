package fr.atexo.annonces.mapper.tncp;

import com.atexo.annonces.commun.mpe.AgentMpe;
import eu.europa.publications.resource.schema.ted.r2_0_9.reception.TedEsenders;
import fr.atexo.annonces.commun.tncp.Utilisateur;
import fr.atexo.annonces.mapper.Mappable;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UtilisateurMapper extends Mappable<AgentMpe, Utilisateur> {

    @Override
    Utilisateur map(AgentMpe agent, TedEsenders tedEsenders, boolean rie);

}
