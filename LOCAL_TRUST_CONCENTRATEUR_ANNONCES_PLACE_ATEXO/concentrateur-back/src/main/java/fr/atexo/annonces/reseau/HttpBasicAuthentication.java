package fr.atexo.annonces.reseau;

import org.apache.commons.net.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Génère la valeur de l'header Authorization pour une authentification basique Http
 */
public class HttpBasicAuthentication {

    private final Logger logger = LoggerFactory.getLogger(HttpBasicAuthentication.class);
    private final String headerValue;

    public HttpBasicAuthentication(String username, String password) {
        headerValue = "Basic " + Base64.encodeBase64StringUnChunked((username + ":" + password).getBytes());
        logger.debug("Construction d'un header HTTP Authorization avec comme valeur {}", headerValue);
    }

    public String getHeaderValue() {
        return headerValue;
    }
}
