package fr.atexo.annonces.dto;

import fr.atexo.annonces.modeles.StatutSuiviAnnonceEnum;
import lombok.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZonedDateTime;

/**
 * DTO - Représente une demande de publication
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SuiviOffreDTO implements Serializable {
    private Integer id;
    private Integer idConsultation;
    private StatutSuiviAnnonceEnum statut;
    private Instant dateEnvoi;
    private Instant dateSoumission;
    private Instant dateSoumissionTed;
    private Instant dateDemandeEnvoi;
    private Instant dateDebutEnvoi;
    private Instant datePublication;
    private Instant datePublicationTed;
    private OffreResumeDTO offre;
    private SupportResumeDTO support;
    private String idPlatform;
    private ZonedDateTime dateCreation;
    private ZonedDateTime dateModification;
    private String numeroTed;
    private String numeroAvis;
    private String numeroDossier;
    private String numeroJoue;
    private String statutJoue;
    private String statutBoamp;
    private String lienPublication;
    private String lienTelechargement;
    private String lienPublicationTed;

}
