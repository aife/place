package fr.atexo.annonces.jaxb.suivi.simap;

/**
 * Created by qba on 31/05/17.
 */

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ojs_number",
        "no_doc_ojs",
        "publication_date",
        "ted_links"
})
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PublicationInfo {

    @JsonProperty("ojs_number")
    private String ojsNumber;
    @JsonProperty("no_doc_ojs")
    private String noDocOjs;
    @JsonProperty("publication_date")
    private String publicationDate;
    @JsonProperty("ted_links")
    private TedLinks tedLinks;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ojs_number")
    public String getOjsNumber() {
        return ojsNumber;
    }

    @JsonProperty("ojs_number")
    public void setOjsNumber(String ojsNumber) {
        this.ojsNumber = ojsNumber;
    }

    @JsonProperty("no_doc_ojs")
    public String getNoDocOjs() {
        return noDocOjs;
    }

    @JsonProperty("no_doc_ojs")
    public void setNoDocOjs(String noDocOjs) {
        this.noDocOjs = noDocOjs;
    }

    @JsonProperty("publication_date")
    public String getPublicationDate() {
        return publicationDate;
    }

    @JsonProperty("publication_date")
    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    @JsonProperty("ted_links")
    public TedLinks getTedLinks() {
        return tedLinks;
    }

    @JsonProperty("ted_links")
    public void setTedLinks(TedLinks tedLinks) {
        this.tedLinks = tedLinks;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
