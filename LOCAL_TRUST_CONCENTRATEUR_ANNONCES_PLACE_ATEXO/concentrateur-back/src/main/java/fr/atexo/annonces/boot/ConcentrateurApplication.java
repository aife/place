package fr.atexo.annonces.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Profile;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@Profile({"local", "standalone", "annonces", "docker"})
@ComponentScan({"fr.atexo.annonces"})
@EnableScheduling
@EnableRetry
@ImportResource("classpath:new_springintegration.xml")
public class ConcentrateurApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(ConcentrateurApplication.class, args);
    }
}
