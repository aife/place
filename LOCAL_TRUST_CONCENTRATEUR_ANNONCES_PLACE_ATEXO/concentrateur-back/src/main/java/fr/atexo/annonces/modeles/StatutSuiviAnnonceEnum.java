package fr.atexo.annonces.modeles;

import fr.atexo.annonces.boot.entity.tncp.SuiviStatut;

public enum StatutSuiviAnnonceEnum {

    EXTERNAL,
    BROUILLON,
    ARCHIVER,
    SUPPRIMER,
    COMPLET,
    EN_ATTENTE,
    RETRY,
    EN_COURS_DE_TRAITEMENT,
    EN_ATTENTE_VALIDATION_ECO,
    EN_ATTENTE_VALIDATION_SIP,
    REPRENDRE,
    MODIFIER,
    EN_COURS_DE_PUBLICATION,
    PREPARATION_PUBLICATION,
    ENVOI_PLANIFIER,
    EN_COURS_D_ARRET,
    REJETER_SUPPORT,
    REJETER_CONCENTRATEUR,
    REJETER_CONCENTRATEUR_VALIDATION_ECO,
    REJETER_CONCENTRATEUR_VALIDATION_SIP,
    PUBLIER,
    ARRETER,
    ANNULER;

    public static StatutSuiviAnnonceEnum getStatutSuiviAnnonce(String statut) {

        switch (statut.toUpperCase()) {
            case "RECEIVED":
                return EN_COURS_DE_PUBLICATION;
            case "RECEPTION_ERROR":
                return REJETER_SUPPORT;
            case "QUALIFICATION_ERROR":
                return REJETER_SUPPORT;
            case "VALIDATION_ACCEPTED":
                return PUBLIER;
            case "QUALITY_ACCEPTED":
                return EN_COURS_DE_PUBLICATION;
            case "QUALITY_SKIPPED":
                return EN_COURS_DE_PUBLICATION;
            case "IN_PROGRESS":
                return EN_COURS_DE_PUBLICATION;
            case "PUBLISHED":
                return PUBLIER;
            case "NOT_PUBLISHED":
                return REJETER_SUPPORT;
            default:
                return null;
        }

    }

    public static StatutSuiviAnnonceEnum getStatutSuiviAnnonceFromENotice(StatutENoticeEnum statut) {

        switch (statut) {
            case DRAFT:
            case CONVERTED_FROM_PUBLISHED:
                return BROUILLON;
            case PUBLISHING:
            case SUBMITTING:
            case SUBMITTED:
                return EN_COURS_DE_PUBLICATION;
            case VALIDATION_FAILED:
            case NOT_PUBLISHED:
                return REJETER_SUPPORT;
            case PUBLISHED:
                return PUBLIER;
            case STOPPED:
                return ARRETER;
            case ARCHIVED:
                return ARCHIVER;
            case DELETED:
                return SUPPRIMER;
            case STOPPING:
                return EN_COURS_D_ARRET;
            case CONCENTRATEUR_VALIDATING_ECO:
                return EN_ATTENTE_VALIDATION_ECO;
            case CONCENTRATEUR_VALIDATING_SIP:
                return EN_ATTENTE_VALIDATION_SIP;
            case CONCENTRATEUR_REJET_ECO:
                return REJETER_CONCENTRATEUR_VALIDATION_ECO;
            case CONCENTRATEUR_REJET_SIP:
                return REJETER_CONCENTRATEUR_VALIDATION_SIP;
            default:
                return null;
        }

    }

    public static StatutSuiviAnnonceEnum getStatutSuiviAnnonceFromTypeAvis(StatutTypeAvisAnnonceEnum statut, boolean external, boolean valideNational) {

        if (external) {
            return EXTERNAL;
        }
        switch (statut) {
            case REJETE:
                return REJETER_CONCENTRATEUR;
            case EN_ATTENTE_VALIDATION_SIP:
                return EN_ATTENTE_VALIDATION_SIP;
            case EN_ATTENTE_VALIDATION_ECO:
                return EN_ATTENTE_VALIDATION_ECO;
            case ENVOYE:
                return EN_COURS_DE_PUBLICATION;
            case ENVOI_PLANIFIER:
                return ENVOI_PLANIFIER;
            case REJETE_ECO:
                return REJETER_CONCENTRATEUR_VALIDATION_ECO;
            case REJETE_SIP:
                return REJETER_CONCENTRATEUR_VALIDATION_SIP;
            default:
                return valideNational ? COMPLET : BROUILLON;
        }
    }

    public static StatutSuiviAnnonceEnum getStatutSuivisAnnonceFromTypeAvis(StatutTypeAvisAnnonceEnum statut) {
        switch (statut) {
            case BROUILLON:
                return BROUILLON;
            case REJETE:
                return REJETER_SUPPORT;
            case EN_ATTENTE_VALIDATION_SIP:
                return EN_ATTENTE_VALIDATION_SIP;
            case EN_ATTENTE_VALIDATION_ECO:
                return EN_ATTENTE_VALIDATION_ECO;
            case ENVOYE:
                return EN_COURS_DE_PUBLICATION;
            case ENVOI_PLANIFIER:
                return ENVOI_PLANIFIER;
            case REJETE_ECO:
                return REJETER_CONCENTRATEUR_VALIDATION_ECO;
            case REJETE_SIP:
                return REJETER_CONCENTRATEUR_VALIDATION_SIP;
            default:
                return BROUILLON;
        }
    }

    public static StatutSuiviAnnonceEnum getStatutSuiviAnnonceFromSuiviStatut(SuiviStatut statut) {
        switch (statut) {
            case CREE:
                return BROUILLON;
            case ENVOYE:
                return EN_COURS_DE_PUBLICATION;
            case REFUSE:
                return REJETER_SUPPORT;
            case PUBLIE:
                return PUBLIER;
            default:
                return null;
        }
    }

    public static StatutSuiviAnnonceEnum getStatutFromLesEchos(String etat) {
        switch (etat) {
            case "PUBLIE":
                return PUBLIER;
            case "REJET_TECHNIQUE":
            case "REJET_EDITORIAL":
                return REJETER_SUPPORT;
            default:
                return EN_COURS_DE_PUBLICATION;
        }
    }
}
