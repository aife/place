package fr.atexo.annonces.boot.core.domain.xlsx.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public enum XlsxAnnoncesStatistiqueHeaderEnum {

    NOM(0, "Avis"),
    EN_ATTENTE_DE_VALIDATION_ECO(1, "En attente de validation ECO"),
    EN_ATTENTE_DE_VALIDATION_SIP(2, "En attente de validation SIP"),
    ENVOI_PLANIFIE(3, "Envoi planifié"),
    ENVOYE(4, "Envoyé"),
    REJETE(5, "Rejeté"),
    ENVOI_REJETE(6, "Envoi rejeté");

    private final int index;
    private final String title;

    public static List<XlsxHeader> transform(Boolean sip) {
        return Arrays
                .stream(XlsxAnnoncesStatistiqueHeaderEnum.values())
                .filter(xlsxAnnoncesStatistiqueHeaderEnum -> !Boolean.TRUE.equals(sip) || !xlsxAnnoncesStatistiqueHeaderEnum.equals(EN_ATTENTE_DE_VALIDATION_ECO))
                .map(xlsxBuDaHeaderEnum -> XlsxHeader
                        .builder()
                        .index(Boolean.TRUE.equals(sip) && xlsxBuDaHeaderEnum.getIndex() > 0 ? xlsxBuDaHeaderEnum.getIndex() - 1 : xlsxBuDaHeaderEnum.getIndex())
                        .title(xlsxBuDaHeaderEnum.getTitle())
                        .build())
                .collect(Collectors.toList());
    }
}
