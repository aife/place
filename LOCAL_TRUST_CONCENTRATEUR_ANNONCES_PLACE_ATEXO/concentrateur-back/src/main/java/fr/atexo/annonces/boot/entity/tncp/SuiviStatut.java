package fr.atexo.annonces.boot.entity.tncp;

public enum SuiviStatut {
    CREE,
    ENVOYE,
    PUBLIE,
    REFUSE,
    ERREUR
}
