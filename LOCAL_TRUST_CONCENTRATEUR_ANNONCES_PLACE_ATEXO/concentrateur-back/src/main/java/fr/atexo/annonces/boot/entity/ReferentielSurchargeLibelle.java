package fr.atexo.annonces.boot.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Entity
@Table(name = "referential_surcharge_libelle", uniqueConstraints = {@UniqueConstraint(columnNames = {"code", "id_organisme"})},
        indexes = {@Index(name = "libelle_surcharge_code_index", columnList = "code,id_organisme")})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor

public class ReferentielSurchargeLibelle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_organisme")
    private Organisme organisme;

    @Column(name = "code", nullable = false)
    private String code;

    @Lob
    @Column(name = "libelle", nullable = false)
    private String libelle;

    @Column(name = "lang", nullable = false)
    private String lang;

    @Column(name = "ACTIF", nullable = false)
    private boolean actif;

}
