package fr.atexo.annonces.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.cfg.ConfigFeature;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class SerialisationServiceImpl implements SerialisationService {

	@AllArgsConstructor
	@Getter
	public static abstract class FeatureState<T extends ConfigFeature> {
		private T feature;
		private boolean enabled;

		public boolean isDisabled() {
			return !this.isEnabled();
		}
	}

	@Getter
	public static class DeserializationFeatureState extends FeatureState<DeserializationFeature>  {
		public DeserializationFeatureState(DeserializationFeature feature, boolean enabled) {
			super(feature, enabled);
		}
	}

	@Getter
	public static class SerializationFeatureState extends FeatureState<SerializationFeature>  {
		public SerializationFeatureState(SerializationFeature feature, boolean enabled) {
			super(feature, enabled);
		}
	}

	@NonNull
	private final ObjectMapper mapper;

	@Override
	public <T> String serialise(final T payload, final SerializationFeature... features) {
		String result = null;

		var states = Arrays.stream(features).map(feature->new SerializationFeatureState(feature, mapper.isEnabled(feature))).toList();

		try {
			states.stream().filter(FeatureState::isDisabled).forEach(state -> mapper.enable(state.getFeature()));

			result = mapper.writeValueAsString(payload);
		} catch (final JsonProcessingException e) {
			log.error("Impossible de sérialiser le contexte {}", payload);
		} finally {
			states.stream().filter(FeatureState::isDisabled).forEach(state -> mapper.disable(state.getFeature()));
		}

		return result;
	}

	@Override
	public <T> Optional<T> deserialise(final String contenu, final Class<T> clazz, final DeserializationFeature... features) {
		T result = null;

		var states = Arrays.stream(features).map(feature->new DeserializationFeatureState(feature, mapper.isEnabled(feature))).toList();

		try {
			states.stream().filter(FeatureState::isDisabled).forEach(state -> mapper.enable(state.getFeature()));

			result = mapper.readValue(contenu, clazz);
		} catch (final JsonProcessingException e) {
			log.error("Impossible de désérialiser le contexte {}", contenu);
		} finally {
			states.stream().filter(FeatureState::isDisabled).forEach(state -> mapper.disable(state.getFeature()));
		}

		return Optional.ofNullable(result);
	}
}
