package fr.atexo.annonces.boot.configuration;

import fr.atexo.annonces.boot.entity.Agent;
import fr.atexo.annonces.service.AgentService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.Optional;

@Configuration
public class AtexoAuditorAware implements AuditorAware<Agent> {

    private final AgentService agentService;

    public AtexoAuditorAware(AgentService agentService) {
        this.agentService = agentService;
    }

    @Bean
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Optional<Agent> getCurrentAuditor() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (attributes != null) {
            HttpServletRequest request = attributes.getRequest();
            Enumeration<String> authorization = request.getHeaders("Authorization");
            if (authorization.hasMoreElements()) {
                String token = authorization.nextElement().replace("Bearer ", "");
                Agent byToken = agentService.findByToken(token);
                return byToken != null ? Optional.of(byToken) : Optional.empty();
            }
        }

        return agentService.findByAnonyme();
    }
}
