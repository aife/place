package fr.atexo.annonces.service.exception;

import java.text.MessageFormat;

/**
 * Exception levée si un quelconque problème survient lors de la configuration du transformer XSL
 */
public class ExportConfigurationException extends RuntimeException {

    private static final long serialVersionUID = -5672949357366650263L;
    private static final String EXPORT_FAILURE_MESSAGE = "Erreur lors de la génération du PDF pour la ressource ayant pour {0} {1}";

    public ExportConfigurationException(Throwable cause, String identifierProperty, String identifierValue) {
        super(MessageFormat.format(EXPORT_FAILURE_MESSAGE, identifierProperty, identifierValue), cause);
    }

    public ExportConfigurationException(String identifierProperty, String identifierValue) {
        super(MessageFormat.format(EXPORT_FAILURE_MESSAGE, identifierProperty, identifierValue));
    }

    public ExportConfigurationException(String message) {
        super(message);
    }

}
