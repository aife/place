package fr.atexo.annonces.boot.repository;


import fr.atexo.annonces.boot.entity.AcheteurEntity;
import fr.atexo.annonces.config.DaoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AcheteurRepository extends DaoRepository<AcheteurEntity, Long> {

    Optional<AcheteurEntity> findByLoginAndEmailAndOrganismeId(String login, String email, Integer organismeId);

    List<AcheteurEntity> findAllByOrganismeId(Integer organismeId);

}
