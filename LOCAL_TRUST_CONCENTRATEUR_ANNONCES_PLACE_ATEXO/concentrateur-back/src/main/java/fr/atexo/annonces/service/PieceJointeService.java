package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.entity.PieceJointe;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

public interface PieceJointeService {
    PieceJointe save(PieceJointe pieceJointe, InputStream inputStream);

    PieceJointe save(MultipartFile file);

    PieceJointe save(InputStream file, String contentType, String name);

    String getExtension(String name);

    void deleteById(Long id);

    PieceJointe getById(Long id);

    Resource loadFileAsResource(String chemin);


    PieceJointe clone(PieceJointe avisExterne);
}

