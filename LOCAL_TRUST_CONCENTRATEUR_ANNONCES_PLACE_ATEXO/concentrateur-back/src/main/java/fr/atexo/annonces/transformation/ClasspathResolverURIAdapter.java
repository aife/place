package fr.atexo.annonces.transformation;

import org.apache.fop.apps.io.ResourceResolverFactory;
import org.apache.xmlgraphics.io.Resource;
import org.apache.xmlgraphics.io.ResourceResolver;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URL;

/**
 * Récupéré de https://stackoverflow.com/questions/41661997/set-fopfactorybuilder-baseuri-to-jar-classpath
 * Permet de mettre des URL relatives au classpath dans le XSL FO
 */
public class ClasspathResolverURIAdapter implements ResourceResolver {

    private final ResourceResolver wrapped;

    public ClasspathResolverURIAdapter() {
        this.wrapped = ResourceResolverFactory.createDefaultResourceResolver();
    }

    @Override
    public Resource getResource(URI uri) throws IOException {
        if ("classpath".equals(uri.getScheme())) {
            URL url = getClass().getClassLoader().getResource(uri.getSchemeSpecificPart());
            if (url == null) {
                throw new IOException("Impossible d'accéder la ressource " + uri.getSchemeSpecificPart());
            }
            return new Resource(url.openStream());
        } else {
            return wrapped.getResource(uri);
        }
    }

    @Override
    public OutputStream getOutputStream(URI uri) throws IOException {
        return wrapped.getOutputStream(uri);
    }
}
