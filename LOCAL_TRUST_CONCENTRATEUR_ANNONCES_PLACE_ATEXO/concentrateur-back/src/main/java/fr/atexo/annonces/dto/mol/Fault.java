//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.06.02 à 03:26:18 PM CEST 
//


package fr.atexo.annonces.dto.mol;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import javax.xml.namespace.QName;


/**
 * Fault reporting structure
 *
 * <p>Classe Java pour Fault complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="Fault"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="faultcode" type="{http://www.w3.org/2001/XMLSchema}QName"/&gt;
 *         &lt;element name="faultstring" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="faultactor" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0"/&gt;
 *         &lt;element name="detail" type="{http://schemas.xmlsoap.org/soap/envelope/}detail" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Fault", propOrder = {
        "faultcode",
        "faultstring",
        "faultactor",
        "detail"
})
@ToString
@EqualsAndHashCode
public class Fault {

    @XmlElement(required = true)
    protected QName faultcode;
    @XmlElement(required = true)
    protected String faultstring;
    @XmlSchemaType(name = "anyURI")
    protected String faultactor;
    protected Detail detail;

    /**
     * Obtient la valeur de la propriété faultcode.
     *
     * @return possible object is
     * {@link QName }
     */
    public QName getFaultcode() {
        return faultcode;
    }

    /**
     * Définit la valeur de la propriété faultcode.
     *
     * @param value allowed object is
     *              {@link QName }
     */
    public void setFaultcode(QName value) {
        this.faultcode = value;
    }

    /**
     * Obtient la valeur de la propriété faultstring.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFaultstring() {
        return faultstring;
    }

    /**
     * Définit la valeur de la propriété faultstring.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFaultstring(String value) {
        this.faultstring = value;
    }

    /**
     * Obtient la valeur de la propriété faultactor.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFaultactor() {
        return faultactor;
    }

    /**
     * Définit la valeur de la propriété faultactor.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFaultactor(String value) {
        this.faultactor = value;
    }

    /**
     * Obtient la valeur de la propriété detail.
     *
     * @return possible object is
     * {@link Detail }
     */
    public Detail getDetail() {
        return detail;
    }

    /**
     * Définit la valeur de la propriété detail.
     *
     * @param value allowed object is
     *              {@link Detail }
     */
    public void setDetail(Detail value) {
        this.detail = value;
    }

}
