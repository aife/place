package fr.atexo.annonces.boot.core.domain.e_forms_notice.services.impl;

import com.atexo.annonces.commun.mpe.DonneesComplementaires;
import com.atexo.annonces.commun.mpe.LotMpe;
import com.fasterxml.jackson.databind.JsonNode;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.NoticeFormatHandler;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.FieldDetails;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNotice;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNoticeMetadata;
import fr.atexo.annonces.boot.entity.EFormsFormulaireEntity;
import fr.atexo.annonces.boot.repository.EformsSurchargeRepository;
import fr.atexo.annonces.boot.ws.eforms.SchematronConfiguration;
import fr.atexo.annonces.boot.ws.mpe.model.ConsultationContexte;
import fr.atexo.annonces.dto.ConfigurationConsultationDTO;
import fr.atexo.annonces.dto.EformsSurchargeDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static fr.atexo.annonces.config.EFormsHelper.valueToTree;

@Slf4j
public class LotFormatHandler extends GenericSdkService implements NoticeFormatHandler {


    public LotFormatHandler(EformsSurchargeRepository eformsSurchargeRepository, SchematronConfiguration schematronConfiguration) {
        super(eformsSurchargeRepository, schematronConfiguration);
    }

    @Override
    public Object processGroup(SubNotice notice, ConfigurationConsultationDTO configurationConsultationDTO, Object formulaireObjet, SubNoticeMetadata group, EFormsFormulaireEntity saved, String nodeStop, Map<String, List<NoticeFormatHandler>> kownHandlers) {
        List<Object> o = new ArrayList<>();
        ConsultationContexte contexte = configurationConsultationDTO.getContexte();
        if (contexte == null) {
            return o;
        }
        List<LotMpe> mpeLots = getLotMpes(contexte);
        for (int i = 0; i < mpeLots.size(); i++) {
            LotMpe mpeLot = mpeLots.get(i);
            if (!CollectionUtils.isEmpty(group.getContent())) {
                contexte.setLot(mpeLot);
                String nodeId = group.getNodeId();
                int index = i + 1;
                Object processList = processLot(notice, configurationConsultationDTO, new Object(), group.getContent(), nodeId, saved, kownHandlers, index);
                processList = setGroupId(processList, nodeStop, group, nodeId, saved.getVersionSdk(), index);
                JsonNode node = valueToTree(processList);
                if (!node.isObject() || !node.isEmpty()) {
                    o.add(processList);
                }

            }
        }
        contexte.setLot(null);
        return o;
    }


    private List<LotMpe> getLotMpes(ConsultationContexte contexte) {
        if (contexte == null) {
            return List.of(LotMpe.with()
                    .numero("1")
                    .build());
        }
        List<LotMpe> mpeLots = contexte.getLots();
        if (CollectionUtils.isEmpty(mpeLots)) {
            return List.of(LotMpe.with()
                    .codeCpvPrincipal(contexte.getConsultation().getCodeCpvPrincipal())
                    .clauses(contexte.getConsultation().getClauses())
                    .codeCpvSecondaire1(contexte.getConsultation().getCodeCpvSecondaire1())
                    .codeCpvSecondaire2(contexte.getConsultation().getCodeCpvSecondaire2())
                    .codeCpvSecondaire3(contexte.getConsultation().getCodeCpvSecondaire3())
                    .intitule(contexte.getConsultation().getIntitule())
                    .descriptionSuccinte(contexte.getConsultation().getObjet())
                    .naturePrestation(contexte.getConsultation().getNaturePrestation())
                    .consultation(contexte.getConsultation().getReference())
                    .numero("1")
                    .typeDecision(contexte.getConsultation().getTypeDecision())
                    .typeDecisionAdmissionSad(contexte.getConsultation().getTypeDecisionAdmissionSad())
                    .typeDecisionARenseigner(contexte.getConsultation().getTypeDecisionARenseigner())
                    .typeDecisionAttributionMarche(contexte.getConsultation().getTypeDecisionAttributionMarche())
                    .typeDecisionAttributionAccordCadre(contexte.getConsultation().getTypeDecisionAttributionAccordCadre())
                    .typeDecisionDeclarationInfructueux(contexte.getConsultation().getTypeDecisionDeclarationInfructueux())
                    .typeDecisionDeclarationSansSuite(contexte.getConsultation().getTypeDecisionDeclarationSansSuite())
                    .build());

        }
        return mpeLots;
    }

    private Object processLot(SubNotice notice, ConfigurationConsultationDTO contexte, Object formulaireObjet, List<SubNoticeMetadata> content, String nodeStop, EFormsFormulaireEntity saved, Map<String, List<NoticeFormatHandler>> kownHandlers, Integer index) {
        for (SubNoticeMetadata item : content) {
            String nodeId = (item.is_repeatable() || item.get_identifierFieldId() != null) ? item.getNodeId() : null;
            List<NoticeFormatHandler> handler = kownHandlers.get(nodeId);
            if (handler != null) {

                for (NoticeFormatHandler noticeFormatHandler : handler) {
                    Object value = noticeFormatHandler.processGroup(notice, contexte, new Object(), item, saved, nodeId, kownHandlers);
                    formulaireObjet = addToObject(formulaireObjet, null, value, nodeStop, nodeId, saved.getVersionSdk());
                }
            } else if (nodeId != null) {
                if (("field".equals(item.getContentType()))) {
                    formulaireObjet = processElement(item, notice, contexte, formulaireObjet, item.getId(), saved, nodeStop);
                }

                if (!CollectionUtils.isEmpty(item.getContent())) {
                    Object o = processLot(notice, contexte, new Object(), item.getContent(), nodeId, saved, kownHandlers, index);
                    formulaireObjet = addToObject(formulaireObjet, null, o, nodeStop, nodeId, saved.getVersionSdk());
                }
            } else {
                if (("field".equals(item.getContentType()))) {
                    formulaireObjet = processElement(item, notice, contexte, formulaireObjet, item.getId(), saved, nodeStop);
                }

                if (!CollectionUtils.isEmpty(item.getContent())) {
                    formulaireObjet = processLot(notice, contexte, formulaireObjet, item.getContent(), nodeStop, saved, kownHandlers, index);
                }
            }


            formulaireObjet = setGroupId(formulaireObjet, nodeStop, item, nodeId, saved.getVersionSdk(), index);
        }

        return formulaireObjet;
    }

    @Override
    public Object processElement(SubNoticeMetadata item, SubNotice notice, ConfigurationConsultationDTO configurationConsultationDTO, Object formulaireObjet, String id, EFormsFormulaireEntity saved, String nodeStop) {
        if (configurationConsultationDTO == null || configurationConsultationDTO.getContexte() == null) {
            return formulaireObjet;
        }
        ConsultationContexte contexte = configurationConsultationDTO.getContexte();
        String versionSdk = saved.getVersionSdk();
        FieldDetails fieldDetails = getFields(versionSdk).getFields()
                .stream().filter(fieldDetails1 -> fieldDetails1.getId().equals(id))
                .findFirst().orElse(null);
        if (fieldDetails == null) {
            return formulaireObjet;
        }
        String parentNodeId = fieldDetails.getParentNodeId();
        EformsSurchargeDTO surcharge = item.getSurcharge();
        if (surcharge != null && surcharge.getDefaultValue() != null) {
            return addToObject(formulaireObjet, id, surcharge.getDefaultValue(), nodeStop, parentNodeId, versionSdk);
        }
        boolean repeatable = item.is_repeatable();
        String presetValue = item.get_presetValue();
        if (StringUtils.hasText(presetValue)) {
            Object object = getPresetValue(presetValue, fieldDetails.getType(), versionSdk);
            if (object != null)
                return addToObject(formulaireObjet, id, repeatable ? List.of(object) : object, nodeStop, parentNodeId, versionSdk);
        }
        LotMpe mpeLot = contexte.getLot();

        String codeCpvPrincipal = mpeLot.getCodeCpvPrincipal();
        String codeCpvSecondaire1 = mpeLot.getCodeCpvSecondaire1();
        String codeCpvSecondaire2 = mpeLot.getCodeCpvSecondaire2();
        String codeCpvSecondaire3 = mpeLot.getCodeCpvSecondaire3();
        Object time = addToObject(new Object(), "type", "Europe/Paris", null, null, versionSdk);
        Object date = addToObject(new Object(), "type", "Europe/Paris", null, null, versionSdk);
        DateTimeFormatter formatterDate = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        DateTimeFormatter formatterTime = DateTimeFormatter.ofPattern("HH:mm:ss");

        ZonedDateTime dateLimiteRemiseOffres = contexte.getConsultation().getDateLimiteRemiseOffres();
        if (dateLimiteRemiseOffres != null && dateLimiteRemiseOffres.isAfter(ZonedDateTime.of(1997, 1, 1, 0, 0, 0, 0, ZoneId.of("Europe/Paris")))) {
            dateLimiteRemiseOffres = dateLimiteRemiseOffres.withZoneSameInstant(ZoneId.of("Europe/Paris"));
            time = addToObject(time, "value", dateLimiteRemiseOffres.format(formatterTime), null, null, versionSdk);
            date = addToObject(date, "value", dateLimiteRemiseOffres.format(formatterDate), null, null, versionSdk);
        }


        DonneesComplementaires donneesComplementaires = contexte.getDonneesComplementaires();
        switch (id) {
            case "BT-131(d)-Lot" -> {
                if (Boolean.TRUE.equals(contexte.getConsultation().getEnveloppeOffre()) && dateLimiteRemiseOffres != null &&
                        dateLimiteRemiseOffres.isAfter(ZonedDateTime.of(1997, 1, 1, 0, 0, 0, 0, ZoneId.of("Europe/Paris"))))
                    return addToObject(formulaireObjet, id, repeatable ? List.of(date) : date, nodeStop, parentNodeId, versionSdk);
                return formulaireObjet;

            }
            case "BT-1311(d)-Lot" -> {
                if (Boolean.TRUE.equals(contexte.getConsultation().getEnveloppeCandidature()) && dateLimiteRemiseOffres != null &&
                        dateLimiteRemiseOffres.isAfter(ZonedDateTime.of(1997, 1, 1, 0, 0, 0, 0, ZoneId.of("Europe/Paris"))))
                    return addToObject(formulaireObjet, id, repeatable ? List.of(date) : date, nodeStop, parentNodeId, versionSdk);
                return formulaireObjet;
            }
            case "BT-131(t)-Lot" -> {
                if (Boolean.TRUE.equals(contexte.getConsultation().getEnveloppeOffre()) && dateLimiteRemiseOffres != null &&
                        dateLimiteRemiseOffres.isAfter(ZonedDateTime.of(1997, 1, 1, 0, 0, 0, 0, ZoneId.of("Europe/Paris"))))
                    return addToObject(formulaireObjet, id, repeatable ? List.of(time) : time, nodeStop, parentNodeId, versionSdk);
                return formulaireObjet;
            }
            case "BT-1311(t)-Lot" -> {
                if (Boolean.TRUE.equals(contexte.getConsultation().getEnveloppeCandidature()) && dateLimiteRemiseOffres != null &&
                        dateLimiteRemiseOffres.isAfter(ZonedDateTime.of(1997, 1, 1, 0, 0, 0, 0, ZoneId.of("Europe/Paris"))))
                    return addToObject(formulaireObjet, id, repeatable ? List.of(time) : time, nodeStop, parentNodeId, versionSdk);
                return formulaireObjet;
            }

            case "BT-133-Lot" -> {
                if (donneesComplementaires != null && donneesComplementaires.getLieuOuverturePlis() != null) {
                    Object lieuOuverturePlis = donneesComplementaires.getLieuOuverturePlis();
                    return addToObject(formulaireObjet, id, repeatable ? List.of(lieuOuverturePlis) : lieuOuverturePlis, nodeStop, parentNodeId, versionSdk);
                }
                return formulaireObjet;
            }
            case "BT-22-Lot", "BT-22-Part" -> {
                String numero = mpeLot.getNumero();
                return !StringUtils.hasText(numero) ? formulaireObjet : addToObject(formulaireObjet, id, repeatable ? List.of(numero) : numero, nodeStop, parentNodeId, versionSdk);
            }
            case "BT-764-Lot" -> {
                String value = "not-allowed";
                return addToObject(formulaireObjet, id, repeatable ? List.of(value) : value, nodeStop, parentNodeId, versionSdk);
            }
            case "BT-71-Lot", "BT-71-Part" -> {
                String value = "none";
                return addToObject(formulaireObjet, id, repeatable ? List.of(value) : value, nodeStop, parentNodeId, versionSdk);
            }
            case "BT-17-Lot" -> {
                String reponseElectronique = contexte.getConsultation().getReponseElectronique();
                String reponseElectronique1 = getReponseElectronique(reponseElectronique);
                return reponseElectronique == null ? formulaireObjet : addToObject(formulaireObjet, id, repeatable ? List.of(reponseElectronique1) : reponseElectronique1, nodeStop, parentNodeId, versionSdk);
            }
            case "BT-18-Lot" -> {
                String reponseElectronique = contexte.getConsultation().getReponseElectronique();
                String reponseElectronique1 = getReponseElectronique(reponseElectronique);
                if ("not-allowed".equals(reponseElectronique1)) {
                    return formulaireObjet;
                }
                String urlConsultation = contexte.getConsultation().getUrlConsultation();
                return !StringUtils.hasText(urlConsultation) ? formulaireObjet : addToObject(formulaireObjet, id, repeatable ? List.of(urlConsultation) : urlConsultation, nodeStop, parentNodeId, versionSdk);
            }
            case "BT-744-Lot" -> {
                String signatureElectronique = contexte.getConsultation().getSignatureElectronique();
                String signatureElectronique1 = getSignatureElectronique(signatureElectronique);
                return signatureElectronique == null ? formulaireObjet : addToObject(formulaireObjet, id, repeatable ? List.of(signatureElectronique1) : signatureElectronique1, nodeStop, parentNodeId, versionSdk);
            }

            case "BT-767-Lot", "BT-94-Lot", "BT-92-Lot", "BT-93-Lot" -> {
                return addToObject(formulaireObjet, id, repeatable ? List.of(false) : false, nodeStop, parentNodeId, versionSdk);
            }
            case "BT-726-Lot", "BT-726-Part", "BT-115-Lot", "BT-115-Part" -> {
                return addToObject(formulaireObjet, id, repeatable ? List.of(true) : true, nodeStop, parentNodeId, versionSdk);
            }
            case "OPT-301-Part-Mediator", "OPT-301-Lot-TenderEval", "OPT-301-Part-TenderEval", "OPT-301-Lot-TenderReceipt", "OPT-301-Part-TenderReceipt", "OPT-301-Lot-DocProvider", "OPT-301-Part-DocProvider", "OPT-301-Lot-AddInfo", "OPT-301-Part-AddInfo" -> {
                return addToObject(formulaireObjet, id, repeatable ? List.of("ORG-0001") : "ORG-0001", nodeStop, parentNodeId, versionSdk);
            }
            case "OPT-301-Part-ReviewOrg", "OPT-301-Lot-ReviewOrg" -> {
                return addToObject(formulaireObjet, id, repeatable ? List.of("ORG-0002") : "ORG-0002", nodeStop, parentNodeId, versionSdk);
            }
            case "BT-747-Lot" -> {
                List<Object> objects = new ArrayList<>();
                objects.add(addToObject(new Object(), id, "tp-abil", nodeStop, parentNodeId, versionSdk));
                objects.add(addToObject(new Object(), id, "sui-act", nodeStop, parentNodeId, versionSdk));
                objects.add(addToObject(new Object(), id, "ef-stand", nodeStop, parentNodeId, versionSdk));
                return addToObject(formulaireObjet, null, objects, nodeStop, parentNodeId, versionSdk);
            }
            case "BT-21-Lot", "BT-21-Part" -> {
                String title = mpeLot.getIntitule();
                return title == null ? formulaireObjet : addToObject(formulaireObjet, id, title, nodeStop, parentNodeId, versionSdk);
            }
            case "BT-24-Lot", "BT-24-Part" -> {
                String infoAdd = mpeLot.getDescriptionSuccinte();
                return !StringUtils.hasText(infoAdd) ? formulaireObjet : addToObject(formulaireObjet, id, repeatable ? List.of(infoAdd) : infoAdd, nodeStop, parentNodeId, versionSdk);

            }
            case "BT-300-Lot", "BT-300-Part" -> {
                if (mpeLot.getDonneesComplementaires() == null || !StringUtils.hasText(mpeLot.getDonneesComplementaires().getInformationComplementaire())) {
                    return formulaireObjet;
                }
                String infosComplementaire = mpeLot.getDonneesComplementaires().getInformationComplementaire();
                return StringUtils.hasText(infosComplementaire) ? addToObject(formulaireObjet, id, repeatable ? List.of(infosComplementaire) : infosComplementaire, nodeStop, parentNodeId, versionSdk) : formulaireObjet;
            }
            case "BT-23-Lot", "BT-23-Part" -> {
                String nature = mpeLot.getNaturePrestation();
                Object prestation = this.getNaturePrestation(nature);
                return prestation == null ? formulaireObjet : addToObject(formulaireObjet, id, repeatable ? List.of(prestation) : prestation, nodeStop, parentNodeId, versionSdk);
            }
            case "BT-60-Lot" -> {
                return addToObject(formulaireObjet, id, repeatable ? List.of("\"no-eu-funds\"") : "no-eu-funds", nodeStop, parentNodeId, versionSdk);
            }
            case "BT-765-Lot", "BT-765-Part", "BT-766-Lot", "BT-766-Part" -> {
                return addToObject(formulaireObjet, id, repeatable ? List.of("none") : "none", nodeStop, parentNodeId, versionSdk);
            }
            case "BT-262-Lot" -> {
                String idType = "BT-26(m)-Lot";
                if (StringUtils.hasText(codeCpvPrincipal)) {
                    return buildObject(formulaireObjet, id, idType, nodeStop, fieldDetails, parentNodeId, repeatable ? List.of(codeCpvPrincipal) : codeCpvPrincipal, "cpv", versionSdk);
                } else
                    return buildObject(formulaireObjet, id, idType, nodeStop, fieldDetails, parentNodeId, null, "cpv", versionSdk);
            }
            case "BT-262-Part" -> {
                String idType = "BT-26(m)-Part";
                if (StringUtils.hasText(codeCpvPrincipal)) {
                    return buildObject(formulaireObjet, id, idType, nodeStop, fieldDetails, parentNodeId, repeatable ? List.of(codeCpvPrincipal) : codeCpvPrincipal, "cpv", versionSdk);
                } else
                    return buildObject(formulaireObjet, id, idType, nodeStop, fieldDetails, parentNodeId, null, "cpv", versionSdk);
            }
            case "BT-63-Lot", "BT-63-Part" -> {
                donneesComplementaires = contexte.getDonneesComplementaires();
                String variantes = getVariantes(donneesComplementaires);
                if (StringUtils.hasText(variantes)) {
                    return addToObject(formulaireObjet, id, repeatable ? List.of(variantes) : variantes, nodeStop, parentNodeId, versionSdk);
                }
                return formulaireObjet;
            }
            case "BT-263-Lot", "BT-263-Part" -> {
                List<String> cpvSecondaires = new ArrayList<>();
                if (StringUtils.hasText(codeCpvSecondaire1)) {
                    cpvSecondaires.add(codeCpvSecondaire1);
                }
                if (StringUtils.hasText(codeCpvSecondaire2)) {
                    cpvSecondaires.add(codeCpvSecondaire2);
                }
                if (StringUtils.hasText(codeCpvSecondaire3)) {
                    cpvSecondaires.add(codeCpvSecondaire3);
                }
                if (!CollectionUtils.isEmpty(cpvSecondaires)) {
                    return buildCpvSecondaires(formulaireObjet, id, "BT-26(a)-Lot", nodeStop, fieldDetails, parentNodeId, cpvSecondaires, versionSdk);
                } else return formulaireObjet;
            }
            case "BT-736-Lot" -> {
                String value = "no";
                return addToObject(formulaireObjet, id, repeatable ? List.of(value) : value, nodeStop, parentNodeId, versionSdk);
            }
            case "BT-743-Lot" -> {
                String value = "required";
                return addToObject(formulaireObjet, id, repeatable ? List.of(value) : value, nodeStop, parentNodeId, versionSdk);
            }
            default -> log.debug("Pas de correspondance pour {} dans le groupe lot", id);
        }
        return formulaireObjet;
    }


}
