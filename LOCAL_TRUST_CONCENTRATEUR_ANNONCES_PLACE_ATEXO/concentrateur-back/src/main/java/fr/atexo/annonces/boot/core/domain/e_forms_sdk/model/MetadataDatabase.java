package fr.atexo.annonces.boot.core.domain.e_forms_sdk.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MetadataDatabase {
    private String version;
    private LocalDateTime createdOn;
}
