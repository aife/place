package fr.atexo.annonces.service;

/**
 * Classe permettant d'encapsuler le résultat d'une opération création/mise à jour.
 */
public class CreateUpdateResult<T> {
    private boolean created;
    private T dtoObject;

    public CreateUpdateResult(boolean created, T dtoObject) {
        this.created = created;
        this.dtoObject = dtoObject;
    }

    /**
     * Indique si l'opération a abouti à une création ou une mise à jour
     *
     * @return true si création, false si mise à jour
     */
    public boolean isCreated() {
        return created;
    }

    public void setCreated(boolean created) {
        this.created = created;
    }

    /**
     * Retourne une représentation de la ressource créée ou mise à jour
     *
     * @return Ressource concernée par l'opération
     */
    public T getDtoObject() {
        return dtoObject;
    }

    public void setDtoObject(T dtoObject) {
        this.dtoObject = dtoObject;
    }
}
