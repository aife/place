package fr.atexo.annonces.config;

import fr.atexo.annonces.boot.AnnonceIncludeParameterConverter;
import fr.atexo.annonces.boot.SupportIncludeParameterConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new AnnonceIncludeParameterConverter());
        registry.addConverter(new SupportIncludeParameterConverter());
    }
}
