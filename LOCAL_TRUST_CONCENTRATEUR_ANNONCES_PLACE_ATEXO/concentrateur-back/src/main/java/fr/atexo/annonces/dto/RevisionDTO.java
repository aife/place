package fr.atexo.annonces.dto;

import lombok.*;

import java.util.Set;


@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class RevisionDTO<T> {
    private String type;
    private T item;
    private Set<String> champs;

}
