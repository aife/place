package fr.atexo.annonces.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MOLEnvoiDTO {

    private AcheteurChoixDTO compte;
    private ConfigurationConsultationDTO configuration;


}
