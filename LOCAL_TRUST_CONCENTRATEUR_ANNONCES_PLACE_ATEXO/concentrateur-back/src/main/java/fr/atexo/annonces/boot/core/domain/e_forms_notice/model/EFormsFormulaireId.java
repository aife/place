package fr.atexo.annonces.boot.core.domain.e_forms_notice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class EFormsFormulaireId {

    private String lang;
    private String idNotice;
    private Integer versionNotice;
    @NotNull(message = "l'id de la consultation ne doit pas être null")
    private Integer idConsultation;

    private Integer idAnnonce;
}
