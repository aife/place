package fr.atexo.annonces.mapper;

import fr.atexo.annonces.boot.entity.SuiviTypeAvisPubPieceJointeAssoEntity;
import fr.atexo.annonces.config.DaoMapper;
import fr.atexo.annonces.dto.SuiviTypeAvisPubPieceJointeAssoDTO;
import org.mapstruct.Mapper;


@Mapper(uses = {PieceJointeMapper.class})
public interface SuiviTypeAvisPubPieceJointeAssoMapper extends DaoMapper<SuiviTypeAvisPubPieceJointeAssoEntity, SuiviTypeAvisPubPieceJointeAssoDTO> {



}
