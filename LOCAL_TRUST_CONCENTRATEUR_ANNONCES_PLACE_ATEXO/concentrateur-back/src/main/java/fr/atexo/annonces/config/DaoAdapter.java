package fr.atexo.annonces.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DaoAdapter<ID, ENTITY extends Serializable, MODEL extends Serializable, REPOSITORY extends DaoRepository<ENTITY, ID>, MAPPER extends DaoMapper<ENTITY, MODEL>> {

    protected MAPPER mapper;
    protected REPOSITORY repository;

    public DaoAdapter(MAPPER mapper, REPOSITORY repository) {
        this.mapper = mapper;
        this.repository = repository;
    }

    public MODEL findById(ID idAgent) {
        return mapper.mapToModel(repository.findById(idAgent).orElse(null));
    }

    public MODEL save(MODEL model) {
        return model == null ? null : mapper.mapToModel(repository.save(mapper.mapToEntity(model)));
    }

    public List<MODEL> saveAll(List<MODEL> list) {
        final List<MODEL> models = new ArrayList<>();
        repository.saveAll(mapper.mapToEntities(list)).forEach(refCpvEntity -> models.add(mapper.mapToModel(refCpvEntity)));
        return models;
    }

    public List<MODEL> findAllByCriteria(List<SearchCriteria> searchCriteria) {
        return mapper.
                mapToModels(repository.findAllByCriteria(searchCriteria));

    }

    public boolean deleteById(ID id) {
        this.repository.deleteById(id);
        return true;
    }
}
