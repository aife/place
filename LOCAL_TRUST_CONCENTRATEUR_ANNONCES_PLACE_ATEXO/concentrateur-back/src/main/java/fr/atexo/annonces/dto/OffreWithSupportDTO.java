package fr.atexo.annonces.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;


@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class OffreWithSupportDTO {

    private String libelle;
    private String code;
    private String codeGroupe;
    private Boolean europeen;

    private List<SupportResumeDTO> supports;

}
