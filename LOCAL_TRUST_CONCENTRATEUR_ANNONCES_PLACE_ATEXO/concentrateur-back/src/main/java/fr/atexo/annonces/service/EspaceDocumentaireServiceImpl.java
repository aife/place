package fr.atexo.annonces.service;


import fr.atexo.annonces.boot.entity.Agent;
import fr.atexo.annonces.boot.entity.Offre;
import fr.atexo.annonces.boot.entity.PieceJointe;
import fr.atexo.annonces.boot.ws.docgen.DocGenWsPort;
import fr.atexo.annonces.boot.ws.docgen.KeyValueRequest;
import fr.atexo.annonces.boot.ws.edition_en_ligne.EditionEnLigneWsPort;
import fr.atexo.annonces.boot.ws.ressources.RessourcesWsPort;
import fr.atexo.annonces.dto.*;
import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import fr.atexo.annonces.service.exception.PieceJointeException;
import fr.atexo.annonces.service.exception.PieceJointeExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


@Slf4j
@Component
public class EspaceDocumentaireServiceImpl implements EspaceDocumentaireService {

    private final DocGenWsPort docGenWsPort;
    private final RessourcesWsPort ressourcesWsPort;
    private final PieceJointeService pieceJointeService;
    private final EditionEnLigneWsPort editionEnLigneWsPort;
    private final String pieceJointePath;
    private static final String ERROR_MESSAGE = "la piece jointe n'existe pas";

    public EspaceDocumentaireServiceImpl(PieceJointeService pieceJointeService, DocGenWsPort docGenWsPort, RessourcesWsPort ressourcesWsPort, EditionEnLigneWsPort editionEnLigneWsPort, @Value("${upload.folder.path}") String pieceJointePath) {
        this.docGenWsPort = docGenWsPort;
        this.ressourcesWsPort = ressourcesWsPort;
        this.editionEnLigneWsPort = editionEnLigneWsPort;
        this.pieceJointeService = pieceJointeService;
        this.pieceJointePath = pieceJointePath;
    }


    @Override
    public boolean fileExists(PieceJointe document) {
        if (document == null) {
            return false;
        }
        File ressource = new File(this.pieceJointePath, document.getPath());
        return ressource.exists();
    }

    @Override
    public String getEditionToken(Agent agent, String prefix, String callback, String id, String token, PieceJointe document) {
        if (document == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Le document est null");
        }
        String name = document.getName();
        String plateformeId = prefix + agent.getOrganisme().getPlateformeUuid();
        ValidationDocumentEditorRequest documentEditorRequest = ValidationDocumentEditorRequest.builder().callback(callback).headersCallback(Map.of("Authorization", "Bearer " + token)).documentId(id + "-" + document.getId()).plateformeId(plateformeId).documentTitle(name).mode("edit").user(User.builder().id(agent.getId()).name(agent.getPrenom() + " " + agent.getNom()).roles(Arrays.asList("modification", "validation")).build()).build();
        if (!this.fileExists(document)) {
            throw new PieceJointeException(PieceJointeExceptionEnum.NOT_FOUND, ERROR_MESSAGE);
        }
        return docGenWsPort.getEditionToken(new File(this.pieceJointePath, document.getPath()), documentEditorRequest, pieceJointeService.getExtension(name));
    }

    @Override
    public String getValidationToken(Agent agent, String prefix, String callback, String id, String token, PieceJointe document, String statutRedaction) {
        if (document == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Le document est null");
        }
        String name = document.getName();
        String plateformeId = prefix + agent.getOrganisme().getPlateformeUuid();
        ValidationDocumentEditorRequest documentEditorRequest = ValidationDocumentEditorRequest.builder().callback(callback).headersCallback(Map.of("Authorization", "Bearer " + token)).documentId(id + "-" + document.getId()).plateformeId(plateformeId).documentTitle(name).mode("validation").status(statutRedaction != null ? FileRedacStatusEnum.valueOf(statutRedaction).getCode() : null).user(User.builder().id(agent.getId()).name(agent.getPrenom() + " " + agent.getNom()).roles(Arrays.asList("modification", "validation")).build()).build();
        if (!this.fileExists(document)) {
            throw new PieceJointeException(PieceJointeExceptionEnum.NOT_FOUND, ERROR_MESSAGE);
        }
        return docGenWsPort.getValidationToken(new File(this.pieceJointePath, document.getPath()), documentEditorRequest, pieceJointeService.getExtension(name));
    }

    @Override
    public InputStream loadFileAsResourcePDF(String chemin, String extension) {
        File file = new File(pieceJointePath + chemin);
        if (file.exists()) {
            try {
                return docGenWsPort.convertToPDF(file, extension).getInputStream();
            } catch (IOException e) {
                log.error("Erreur de conversion de PDF {}", e.getMessage());
                throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage());
            }
        } else {
            throw new PieceJointeException(PieceJointeExceptionEnum.NOT_FOUND, ERROR_MESSAGE);
        }

    }

    @Override
    public InputStream loadFileAsResource(String chemin, String extension) {
        File file = new File(pieceJointePath + chemin);
        if (file.exists()) {
            try {
                return new FileInputStream(file);
            } catch (IOException e) {
                log.error("Erreur de conversion de PDF {}", e.getMessage());
                throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage());
            }
        } else {
            throw new PieceJointeException(PieceJointeExceptionEnum.NOT_FOUND, ERROR_MESSAGE);
        }

    }


    @Override
    public PieceJointe getPieceJointeFromDocgenValidation(PieceJointe document, String tokenEdition, String id) {
        InputStream zip = docGenWsPort.getDocument(tokenEdition);
        if (zip == null) {
            log.info("Le document de l'avis {} semble être fermé", id);
            return null;
        }
        try (ZipInputStream zipInputStream = new ZipInputStream(zip)) {

            // Loop through each entry in the ZIP archive
            ZipEntry entry;
            while ((entry = zipInputStream.getNextEntry()) != null) {
                String entryName = entry.getName();
                if (entryName.endsWith(".docx")) {
                    File tmpFile = File.createTempFile("tmp", ".docx");
                    String md5;
                    Files.copy(zipInputStream, tmpFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    try (var stream = new FileInputStream(tmpFile); var toCopy = new FileInputStream(tmpFile)) {
                        md5 = org.springframework.util.DigestUtils.md5DigestAsHex(stream);


                        log.info("Récupération du document {} avec le hash {} pour l'avis {}", document.getName(), md5, id);
                        if (!md5.equals(document.getMd5())) {
                            PieceJointe pieceJointe = PieceJointe.builder().md5(md5).contentType(document.getContentType()).name(document.getName()).pieceJointeParente(document).build();
                            PieceJointe savedPieceJointe = pieceJointeService.save(pieceJointe, toCopy);
                            zipInputStream.closeEntry();
                            return savedPieceJointe;
                        }
                    } catch (Exception e) {
                        log.error("Erreur lors de la récupération du document : {}", e.getMessage());
                    } finally {
                        tmpFile.deleteOnExit();
                    }
                    break;
                }
            }

        } catch (IOException e) {
            log.error("Erreur lors de la récupération du document : {}", e.getMessage());
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, "Erreur lors de la récupération du document : " + e.getMessage());
        }
        return null;
    }

    @Override
    public PieceJointe getPieceJointeFromDocgenEdit(PieceJointe document, String tokenEdition, String id) {
        InputStream zip = docGenWsPort.getDocument(tokenEdition);
        if (zip == null) {
            log.info("Le document de l'avis {} semble être fermé", id);
            return null;
        }

        try {

            File tmpFile = File.createTempFile("tmp", ".docx");
            String md5;
            Files.copy(zip, tmpFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            try (var stream = new FileInputStream(tmpFile); var toCopy = new FileInputStream(tmpFile)) {
                md5 = org.springframework.util.DigestUtils.md5DigestAsHex(stream);
                log.info("Récupération du document {} avec le hash {} pour l'avis {}", document.getName(), md5, id);
                if (!md5.equals(document.getMd5())) {
                    PieceJointe pieceJointe = PieceJointe.builder().md5(md5).contentType(document.getContentType()).name(document.getName()).pieceJointeParente(document).build();
                    return pieceJointeService.save(pieceJointe, toCopy);
                }
            } catch (Exception e) {
                log.error("Erreur lors de la récupération du document : {}", e.getMessage());
            } finally {
                tmpFile.deleteOnExit();
            }


        } catch (IOException e) {
            log.error("Erreur lors de la récupération du document : {}", e.getMessage());
        }
        return null;
    }


    @Override
    public EditionRequest getUrlEdition(String editionToken) {
        return EditionRequest.builder().url(editionEnLigneWsPort.getEditionUrl(editionToken)).build();
    }

    @Override
    public File downloadAvisTemplate(Offre offre, boolean simplifie) {
        String resource = null;
        if (offre != null) {
            if (simplifie) resource = offre.getAvisSimplifieTexteWs();
            else {
                resource = offre.getAvisStandardTexteWs();
            }
        }
        log.info("Récupération du template {}", resource);
        return ressourcesWsPort.downloadAvisTemplate(resource, simplifie);
    }


    @Override
    public PieceJointe getPieceJointe(String libelle, List<KeyValueRequest> map, File template, String extension) {
        try (InputStream generatedFile = docGenWsPort.generateDocument(map, template, "")) {
            return pieceJointeService.save(generatedFile, "application/octet-stream", libelle + "." + extension);

        } catch (Exception e) {
            log.error("Erreur lors de la génération du formulaire : {}", e.getMessage());
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, "Erreur lors de la génération du formulaire : " + e.getMessage());
        } finally {
            if (template != null) {
                template.deleteOnExit();
            }
        }
    }


    @Override
    public FileStatus getDocumentStatus(String tokenEdition) {
        return docGenWsPort.getDocumentStatus(tokenEdition);
    }


}
