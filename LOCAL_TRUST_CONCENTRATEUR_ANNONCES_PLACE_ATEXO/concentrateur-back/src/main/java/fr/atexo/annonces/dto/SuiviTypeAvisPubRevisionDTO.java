package fr.atexo.annonces.dto;

import fr.atexo.annonces.modeles.StatutTypeAvisAnnonceEnum;
import lombok.*;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;
import java.time.ZonedDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SuiviTypeAvisPubRevisionDTO implements Serializable {

    private Long id;

    private ZonedDateTime dateEnvoiNational;

    private ZonedDateTime dateEnvoiEuropeen;

    private String raisonRefus;

    @Enumerated(EnumType.STRING)
    private StatutTypeAvisAnnonceEnum statut;

    private FileStatusEnum statutEdition;

    private String statutRedaction;

    private ZonedDateTime dateModificationStatut;

    private ZonedDateTime dateMiseEnLigne;

    private ZonedDateTime dateValidationNational;

    private ZonedDateTime dateValidationEuropeen;

    private ZonedDateTime dateModification;

    private AgentDTO modifierPar;

    private String mailObjet;

    private String mailContenu;

    private PieceJointeDTO avisExterne;

}
