package fr.atexo.annonces.service;

import com.atexo.annonces.commun.mpe.AcheteurMpe;
import com.atexo.annonces.commun.mpe.OrganismeMpe;
import com.atexo.annonces.commun.mpe.ReferentielMpe;
import com.atexo.annonces.commun.mpe.ServiceMpe;
import eu.europa.publications.resource.schema.ted._2021.nuts.Nuts;
import eu.europa.publications.resource.schema.ted.r2_0_9.reception.*;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.model.EFormsFormulaire;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.model.EFormsFormulaireAdd;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.IEFormsNoticeServices;
import fr.atexo.annonces.boot.entity.Offre;
import fr.atexo.annonces.boot.entity.*;
import fr.atexo.annonces.boot.entity.tncp.SuiviAvisPublie;
import fr.atexo.annonces.boot.repository.*;
import fr.atexo.annonces.boot.ws.docgen.DocGenWsPort;
import fr.atexo.annonces.boot.ws.docgen.KeyValueRequest;
import fr.atexo.annonces.boot.ws.jal.LesechosWS;
import fr.atexo.annonces.boot.ws.mpe.MpeWs;
import fr.atexo.annonces.boot.ws.mpe.model.ConsultationContexte;
import fr.atexo.annonces.boot.ws.viewer.ViewerWS;
import fr.atexo.annonces.config.EFormsHelper;
import fr.atexo.annonces.dto.*;
import fr.atexo.annonces.dto.lesecho.suivi.Suivi;
import fr.atexo.annonces.dto.transaction.TRANSACTION;
import fr.atexo.annonces.dto.viewer.ViewerDTO;
import fr.atexo.annonces.mapper.*;
import fr.atexo.annonces.modeles.StatutENoticeEnum;
import fr.atexo.annonces.modeles.StatutSuiviAnnonceEnum;
import fr.atexo.annonces.modeles.StatutTypeAvisAnnonceEnum;
import fr.atexo.annonces.service.exception.*;
import liquibase.repackaged.org.apache.commons.text.StringEscapeUtils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.util.Base64;
import org.apache.fop.apps.FOPException;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.xml.bind.*;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.TransformerConfigurationException;
import java.io.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static fr.atexo.annonces.config.EFormsHelper.nutsToDepartement;
import static java.util.stream.Collectors.toList;

/**
 * Gestion des demandes de publication et de leur suivi
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class SuiviAnnonceServiceImpl extends BaseFopPdfExportable<SuiviAnnonce> implements SuiviAnnonceService {


    public static final String IDENTIFIANT_CONSULTATION_SUPPORT_MESSAGE = "id plate-forme/id consultation/code du support";
    public static final String IDENTIFIANT_ANNONCE_MESSAGE = "id";
    public static final String RESOURCETYPE_ANNONCE_MESSAGE = "Annonce";

    @Value("${export.pdf.xslfo.path}")
    private String exportPdfXslPath;

    @Value("${annonces.url}")
    private String urlEforms;
    @Value("${annonces.mail.ted.cc}")
    private String mailTedCc;

    @NonNull
    private final ReferentielMpeService referentielMpeService;

    @NonNull
    private final EFormsFormulaireRepository eFormsFormulaireRepository;
    @NonNull
    private final AcheteurRepository acheteurRepository;
    @NonNull
    private final AcheteurMapper acheteurMapper;

    @NonNull
    private final PlateformeConfigurationRepository plateformeConfigurationRepository;

    @NonNull
    private final SuiviTypeAvisPubRepository suiviTypeAvisPubRepository;
    @NonNull
    private final SuiviTypeAvisPubMapper suiviTypeAvisPubMapper;


    @NonNull
    private final MolService molService;
    @NonNull
    private final TNCPService tncpService;

    @NonNull
    private final OffreRepository offreRepository;

    @NonNull
    private final SupportRepository supportRepository;
    @NonNull
    private final OffreTypeProcedureRepository offreTypeProcedureRepository;
    @NonNull
    private final TypeProcedureRepository typeProcedureRepository;
    @NonNull
    private final OrganismeService organismeService;
    @NonNull
    private final AgentService agentService;

    @NonNull
    private final MpeWs mpeWs;

    @NonNull
    private final SupportIntermediaireRepository supportIntermediaireRepository;

    @NonNull
    private final SuiviAnnonceMapper suiviAnnonceMapper;
    @NonNull
    private final OffreMapper offreMapper;

    @NonNull
    private final DocGenWsPort docGenWsPort;

    @NonNull
    private final IEFormsNoticeServices ieFormsNoticeServices;

    @NonNull
    private final ViewerWS viewerWS;

    @NonNull
    private final PieceJointeService pieceJointeService;
    @NonNull
    private final EspaceDocumentaireService espaceDocumentaireService;

    @NonNull
    private SuiviAnnonceRepository repository;
    @NonNull
    private SuiviAvisPublieMapper suiviAvisPublieMapper;
    @NonNull
    private LesechosWS lesechosWS;
    private final Object lock = new Object();

    @NonNull
    private AgentConsultationSessionRepository agentConsultationSessionRepository;

    @Value("${annonces.plateforme}")
    private String plateforme;

    public static String insertString(String originalString, String stringToBeInserted, int index) {

        StringBuilder newString = new StringBuilder();

        for (int i = 0; i < originalString.length(); i++) {


            newString.append(originalString.charAt(i));

            if (i == index) {
                newString.append(stringToBeInserted);
            }
        }

        return newString.toString();
    }

    @Override
    public SuiviAnnonceDTO getSuiviAnnonce(String idPlatform, Integer idConsultation, String codeSupport, String codeOffre, boolean includeSupport) {

        SuiviAnnonce annonce = this.getSuiviAnnonceEntity(idPlatform, idConsultation, codeSupport, codeOffre);
        return suiviAnnonceMapper.suiviAnnonceToSuiviAnnonceDTO(annonce, includeSupport);
    }

    @Override
    public SuiviAnnonce getSuiviAnnonceEntity(String idPlatform, Integer idConsultation, String codeSupport, String codeOffre) {
        SuiviAnnonce optionalSuiviAnnonce = repository.findByIdPlatformAndIdConsultationAndSupportCodeAndOffreCode(idPlatform, idConsultation, codeSupport, codeOffre).stream().max(Comparator.comparing(SuiviAnnonce::getDateCreation)).orElse(null);
        if (optionalSuiviAnnonce == null) {
            throw new ResourceNotFoundException(RESOURCETYPE_ANNONCE_MESSAGE, IDENTIFIANT_CONSULTATION_SUPPORT_MESSAGE, idPlatform + "/" + idConsultation + "/" + codeSupport);
        }
        return optionalSuiviAnnonce;

    }

    @Override
    public SuiviAnnonce getSuiviAnnonceEntity(int id) {
        return repository.findById(id).orElseThrow(() -> new ResourceNotFoundException(RESOURCETYPE_ANNONCE_MESSAGE, IDENTIFIANT_ANNONCE_MESSAGE, String.valueOf(id)));

    }

    @Override
    public List<SuiviAnnonceDTO> getSuiviAnnonces(String idPlatform, Integer idConsultation, boolean includeSupport) {
        List<SuiviAnnonce> suiviAnnonces = repository.findAllByIdPlatformAndIdConsultationAndChildrenIsEmpty(idPlatform, idConsultation);
        return suiviAnnonceMapper.suiviAnnoncesToSuiviAnnonceDTOs(suiviAnnonces, includeSupport);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public CreateUpdateAnnonceResult createOrUpdateSuiviAnnonce(String idPlatform, Integer idConsultation, String codeSupport, String codeOffre, SuiviAnnonceDTO suiviAnnonceDTO, String token) {
        validateStatutUpdate(suiviAnnonceDTO, StatutSuiviAnnonceEnum.COMPLET, StatutSuiviAnnonceEnum.EXTERNAL, StatutSuiviAnnonceEnum.EN_ATTENTE_VALIDATION_SIP, StatutSuiviAnnonceEnum.EN_COURS_D_ARRET, StatutSuiviAnnonceEnum.BROUILLON, StatutSuiviAnnonceEnum.EN_ATTENTE, StatutSuiviAnnonceEnum.EN_ATTENTE_VALIDATION_ECO);
        Support support = supportRepository.findByCode(codeSupport);
        SuiviAnnonce optionalSuiviAnnonce = null;
        if (suiviAnnonceDTO.getIdTypeAvisPub() == null) {
            if (support.isChoixUnique()) {
                optionalSuiviAnnonce = repository.findByIdPlatformAndIdConsultationAndSupportCode(idPlatform, idConsultation, codeSupport).stream().max(Comparator.comparing(SuiviAnnonce::getDateCreation)).orElse(null);

            } else {
                optionalSuiviAnnonce = repository.findByIdPlatformAndIdConsultationAndSupportCodeAndOffreCode(idPlatform, idConsultation, codeSupport, codeOffre).stream().max(Comparator.comparing(SuiviAnnonce::getDateCreation)).orElse(null);
            }
        }
        Offre offre = null;
        if (!support.isCompteObligatoire()) {
            offre = getAndValidateOffreUpdate(codeSupport, suiviAnnonceDTO);
        }

        if (optionalSuiviAnnonce != null) {
            log.debug("Annonce trouvée, mode mise à jour ; idPlatform {} / idConsultation {} / codeSupport {}", idPlatform, idConsultation, codeSupport);
            return updateSuiviAnnonce(suiviAnnonceDTO, optionalSuiviAnnonce, offre, support, token);
        } else {
            log.debug("Annonce non trouvée, mode création ; idPlatform {} / idConsultation {} / codeSupport {}", idPlatform, idConsultation, codeSupport);
            return createSuiviAnnonce(idPlatform, idConsultation, suiviAnnonceDTO, offre, support, token);
        }
    }

    private void validateStatutUpdate(SuiviAnnonceDTO suiviAnnonceDTO, StatutSuiviAnnonceEnum... allowedStatuts) {
        for (StatutSuiviAnnonceEnum allowedStatut : allowedStatuts) {
            if (allowedStatut.equals(suiviAnnonceDTO.getStatut())) {
                return;
            }
        }
        throw new IllegalOperationException("Impossible de créer ou mettre à jour une annonce avec le statut " + suiviAnnonceDTO.getStatut().name());
    }

    private Offre getAndValidateOffreUpdate(String codeSupport, SuiviAnnonceDTO suiviAnnonceDTO) {
        if (suiviAnnonceDTO.getOffre() == null) {
            throw new MissingArgumentException("Aucune offre sélectionnée pour cette annonce.");
        }
        Optional<Offre> optionalOffre = offreRepository.findByCode(suiviAnnonceDTO.getOffre().getCode());
        if (optionalOffre.isEmpty()) {
            throw new ResourceNotFoundException("Offre", "code", suiviAnnonceDTO.getOffre().getCode());
        }
        Offre offre = optionalOffre.get();
        if (offre.getSupports().stream().noneMatch(support -> support.getCode().equals(codeSupport))) {
            throw new InconsistentArgumentsException("L'offre " + offre.getCode() + " n'est pas rattachée au support " + codeSupport);
        }
        Support support = supportRepository.findByCode(codeSupport);
        if (support.isExternal() && suiviAnnonceDTO.getStatut() != StatutSuiviAnnonceEnum.EXTERNAL) {
            throw new InconsistentArgumentsException("Impossible de créer ou mettre à jour une annonce au statut " + suiviAnnonceDTO.getStatut().name() + " sur le support externe " + codeSupport);
        }
        if (!support.isExternal() && suiviAnnonceDTO.getStatut() == StatutSuiviAnnonceEnum.EXTERNAL) {
            throw new InconsistentArgumentsException("Impossible de créer ou mettre à jour une annonce au statut " + "EXTERNAL sur le support non-externe " + codeSupport);
        }
        return offre;
    }

    private CreateUpdateAnnonceResult createSuiviAnnonce(String idPlatform, Integer idConsulation, SuiviAnnonceDTO suiviAnnonceDTO, Offre offre, Support support, String token) {
        SuiviAnnonce suiviAnnonce = new SuiviAnnonce();
        suiviAnnonceMapper.updateSuiviAnnonceFromSuiviAnnonceDTO(suiviAnnonceDTO, suiviAnnonce);
        suiviAnnonce.setIdConsultation(idConsulation);
        suiviAnnonce.setIdPlatform(idPlatform);
        suiviAnnonce.setOffre(offre);
        suiviAnnonce.setTentative(0);
        suiviAnnonce.setSupport(support);
        updateDateDemande(suiviAnnonceDTO, suiviAnnonce);
        if (suiviAnnonceDTO.getStatut() == null) {
            suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.BROUILLON);
        } else {
            suiviAnnonce.setStatut(suiviAnnonceDTO.getStatut());
        }

        if (suiviAnnonceDTO.getIdTypeAvisPub() != null) {
            var suiviTypeAvis = suiviTypeAvisPubRepository.findById(suiviAnnonceDTO.getIdTypeAvisPub()).orElseThrow(() -> new ResourceNotFoundException("Type d'avis de publication", "id", "" + suiviAnnonceDTO.getIdTypeAvisPub()));
            suiviAnnonce.setTypeAvisPub(suiviTypeAvis);

        }
        Agent agent = agentService.findByToken(token);
        if (agent != null) {
            suiviAnnonce.setAgent(agent);
            suiviAnnonce.setOrganismeMpe(agent.getOrganisme());
        }
        var config = agentService.getConfiguration(token, idConsulation, idPlatform);
        if (config != null) {
            ConsultationContexte consultationContexte = config.getContexte();
            if (consultationContexte != null) {
                Organisme organisme = null;
                if (consultationContexte.getOrganismeConsultation() != null) {
                    organisme = this.organismeService.savePlateformeOrganismeIfNotExist(consultationContexte.getOrganismeConsultation(), consultationContexte.getServeurUrl(), idPlatform);
                    suiviAnnonce.setOrganismeMpe(organisme);
                }
                ServiceMpe directionService = consultationContexte.getServiceConsultation();
                if (directionService != null) {
                    ReferentielServiceMpe service = ReferentielServiceMpe.builder().libelle(directionService.getLibelle()).organisme(organisme).idMpe(directionService.getId()).code(directionService.getSigle()).actif(true).build();
                    service = referentielMpeService.getReferential(service);
                    suiviAnnonce.setService(service);
                }
                ReferentielMpe typeProcedure = consultationContexte.getTypeProcedureConsultation();
                if (typeProcedure != null && StringUtils.hasText(typeProcedure.getAbreviation()) && StringUtils.hasText(typeProcedure.getLibelle())) {
                    TypeProcedure procedure = typeProcedureRepository.findByAbbreviationAndLibelle(typeProcedure.getAbreviation(), typeProcedure.getLibelle());
                    if (procedure == null) {
                        procedure = new TypeProcedure();
                        procedure.setAbbreviation(typeProcedure.getAbreviation());
                        procedure.setLibelle(typeProcedure.getLibelle());
                        procedure.setAvisExterne(false);
                        procedure = typeProcedureRepository.save(procedure);

                    }
                    suiviAnnonce.setProcedure(procedure);

                }
                if (config.getXml() != null) {
                    suiviAnnonce.setXml(Base64.encodeBase64StringUnChunked(config.getXml().getBytes()));
                }

            }

        }
        this.updateOffreFromXml(suiviAnnonce);
        suiviAnnonce = repository.save(suiviAnnonce);
        if ("ENOTICES".equals(support.getCode())) {
            Integer organismeId = suiviAnnonce.getOrganismeMpe().getId();
            ieFormsNoticeServices.addFormulaire(token, EFormsFormulaireAdd.builder().idConsultation(suiviAnnonce.getIdConsultation()).idNotice(offre.getCode().replace("_ENOTICES", "")).idAnnonce(suiviAnnonce.getId()).lang("FRA").auteurEmail(agent == null ? null : agent.getEmail()).tokenMpe(agent == null ? null : agent.getMpeToken()).mpeUrl(agent == null ? null : agent.getOrganisme().getPlateformeUrl()).build(), suiviAnnonce.getIdPlatform(), organismeId);
        }
        return new CreateUpdateAnnonceResult(true, suiviAnnonceMapper.suiviAnnonceToSuiviAnnonceDTOIgnoreSupport(suiviAnnonce));
    }

    public void updateOffreFromXml(SuiviAnnonce suiviAnnonce) {
        if (suiviAnnonce == null || suiviAnnonce.getOffre() == null || suiviAnnonce.getXml() == null) {
            return;
        }
        String xmlBase64 = suiviAnnonce.getXml();
        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64.decodeBase64(xmlBase64.getBytes()))) {
            JAXBContext jaxbContextTed = JAXBContext.newInstance(TedEsenders.class);
            Unmarshaller jaxbUnmarshaller = jaxbContextTed.createUnmarshaller();
            JAXBElement<TedEsenders> unmarshal = (JAXBElement<TedEsenders>) jaxbUnmarshaller.unmarshal(inputStream);
            TedEsenders tedEsenders = unmarshal.getValue();
            F052014 f052014 = tedEsenders.getFORMSECTION().getF052014();
            BodyF05 contractingbody = f052014.getCONTRACTINGBODY();
            if (contractingbody == null) {
                return;
            }

            eu.europa.publications.resource.schema.ted.r2_0_9.reception.Offre offre = contractingbody.getOffre();
            if (offre == null) {
                offre = new eu.europa.publications.resource.schema.ted.r2_0_9.reception.Offre();
            }
            offre.setCODE(suiviAnnonce.getOffre().getCode());
            offre.setLIBELLE(suiviAnnonce.getOffre().getLibelle());
            contractingbody.setOffre(offre);
            Marshaller jaxbMarshaller = jaxbContextTed.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(tedEsenders, sw);
            String xmlString = sw.toString();
            suiviAnnonce.setXmlConcentrateur(Base64.encodeBase64StringUnChunked(xmlString.getBytes()));
            String xmlStringTransaction = getXmlTransaction(suiviAnnonce.getIdPlatform(), f052014, tedEsenders);
            suiviAnnonce.setXmlTransaction(Base64.encodeBase64StringUnChunked(xmlStringTransaction.getBytes()));
            suiviAnnonce.setOrganisme(tedEsenders.getSENDER().getCONTACT().getORGANISATION());
        } catch (Exception e) {
            log.error("Erreur lors de la mise à jour de l'offre : {}", e.getMessage());
        }
    }

    public String getXmlTransaction(String plateforme, F052014 f052014, TedEsenders tedEsenders) throws JAXBException {
        if (f052014 == null || tedEsenders == null) {
            return null;
        }
        //Set Xml Transaction
        TRANSACTION transaction = new TRANSACTION();
        transaction.setPLATEFORME(plateforme);

        TRANSACTION.ORGANISMEACHETEUR organismeacheteur = new TRANSACTION.ORGANISMEACHETEUR();
        Sender.CONTACT contact = tedEsenders.getSENDER().getCONTACT();
        if (contact != null) {
            organismeacheteur.setORGANISATION(contact.getORGANISATION());
            if (contact.getCOUNTRY() != null) {
                TRANSACTION.ORGANISMEACHETEUR.PAYS pays = new TRANSACTION.ORGANISMEACHETEUR.PAYS();
                pays.setVALUE(contact.getCOUNTRY().getVALUE());
                organismeacheteur.setPAYS(pays);
            }
            organismeacheteur.setEMAIL(contact.getEMAIL());
            transaction.setORGANISMEACHETEUR(organismeacheteur);
        }


        TRANSACTION.COORDONNEEACHETEUR coordonneeacheteur = new TRANSACTION.COORDONNEEACHETEUR();
        CiF05 complementaryinfo = f052014.getCOMPLEMENTARYINFO();
        if (complementaryinfo != null && complementaryinfo.getADDRESSREVIEWBODY() != null) {
            ContactReview addressreviewbody = complementaryinfo.getADDRESSREVIEWBODY();
            coordonneeacheteur.setNOMOFFICIEL(addressreviewbody.getOFFICIALNAME());
            coordonneeacheteur.setADDRESSE(addressreviewbody.getADDRESS());
            coordonneeacheteur.setVILLE(addressreviewbody.getTOWN());
            coordonneeacheteur.setPOSTALCODE(addressreviewbody.getPOSTALCODE());
            if (addressreviewbody.getCOUNTRY() != null) {
                TRANSACTION.COORDONNEEACHETEUR.PAYS pays2 = new TRANSACTION.COORDONNEEACHETEUR.PAYS();
                pays2.setVALUE(addressreviewbody.getCOUNTRY().getVALUE());
                coordonneeacheteur.setPAYS(pays2);
            }
            coordonneeacheteur.setURL(addressreviewbody.getURLBUYER());
            transaction.setCOORDONNEEACHETEUR(coordonneeacheteur);
        }

        TRANSACTION.OFFRE offre1 = new TRANSACTION.OFFRE();
        BodyF05 contractingbody = f052014.getCONTRACTINGBODY();
        if (contractingbody != null && contractingbody.getOffre() != null) {
            offre1.setCODE(contractingbody.getOffre().getCODE());
            offre1.setLIBELLE(contractingbody.getOffre().getLIBELLE());
            transaction.setOFFRE(offre1);
        }
        ProcedureF05 procedure = f052014.getProcedure();
        if (procedure != null) {
            transaction.setDLRO(procedure.getDATERECEIPTTENDERS() + " " + procedure.getTIMERECEIPTTENDERS());
        }
        ObjectContractF05 objectcontract = f052014.getOBJECTCONTRACT();
        if (objectcontract != null) {
            if (objectcontract.getShortDescr() != null) {
                transaction.setOBJETCONSULTATION(objectcontract.getShortDescr().getParagraphs());
            }
            transaction.setREFERENCECONSULTATION(objectcontract.getReferenceNumber());
        }

        JAXBContext jaxbContextTransaction = JAXBContext.newInstance(TRANSACTION.class);
        Marshaller jaxbMarshallerTransaction = jaxbContextTransaction.createMarshaller();
        jaxbMarshallerTransaction.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        StringWriter swTransaction = new StringWriter();
        jaxbMarshallerTransaction.marshal(transaction, swTransaction);
        return swTransaction.toString();
    }

    private CreateUpdateAnnonceResult updateSuiviAnnonce(SuiviAnnonceDTO suiviAnnonceDTO, SuiviAnnonce suiviAnnonce, Offre offre, Support support, String token) {
        if ((suiviAnnonce.getStatut() != StatutSuiviAnnonceEnum.COMPLET && suiviAnnonce.getStatut() != StatutSuiviAnnonceEnum.BROUILLON && suiviAnnonce.getStatut() != StatutSuiviAnnonceEnum.EN_ATTENTE_VALIDATION_ECO && suiviAnnonce.getStatut() != StatutSuiviAnnonceEnum.EXTERNAL && suiviAnnonce.getStatut() != StatutSuiviAnnonceEnum.REJETER_SUPPORT && suiviAnnonce.getStatut() != StatutSuiviAnnonceEnum.REJETER_CONCENTRATEUR) && ((suiviAnnonce.getStatut() != StatutSuiviAnnonceEnum.PUBLIER && suiviAnnonce.getStatut() != StatutSuiviAnnonceEnum.ARRETER && suiviAnnonce.getStatut() != StatutSuiviAnnonceEnum.EN_COURS_DE_PUBLICATION) || suiviAnnonce.getFormulaire() == null)) {
            throw new ConflictingResourceException("Impossible de mettre à jour l'annonce : elle est à l'état " + suiviAnnonce.getStatut().name());
        }
        EFormsFormulaireEntity formulaire = suiviAnnonce.getFormulaire();
        if (formulaire != null) {
            if (StatutSuiviAnnonceEnum.EN_COURS_D_ARRET.equals(suiviAnnonceDTO.getStatut()))
                ieFormsNoticeServices.stopPublication(formulaire.getId(), suiviAnnonce.getIdPlatform());

        }
        suiviAnnonceMapper.updateSuiviAnnonceFromSuiviAnnonceDTO(suiviAnnonceDTO, suiviAnnonce);
        suiviAnnonce.setOffre(offre);
        suiviAnnonce.setSupport(support);

        updateDateDemande(suiviAnnonceDTO, suiviAnnonce);
        this.updateOffreFromXml(suiviAnnonce);
        suiviAnnonce = repository.save(suiviAnnonce);
        return new CreateUpdateAnnonceResult(false, suiviAnnonceMapper.suiviAnnonceToSuiviAnnonceDTOIgnoreSupport(suiviAnnonce));
    }


    private void updateDateDemande(SuiviAnnonceDTO suiviAnnonceDTO, SuiviAnnonce suiviAnnonce) {
        if (suiviAnnonceDTO.getStatut() == StatutSuiviAnnonceEnum.EN_ATTENTE) {
            suiviAnnonce.setDateDemandeEnvoi(Instant.now());
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean deleteSuiviAnnonce(Integer id, String idPlatform, Integer idConsultation) {
        Optional<SuiviAnnonce> optionalSuiviAnnonce = repository.findById(id);
        if (optionalSuiviAnnonce.isEmpty()) {
            throw new ResourceNotFoundException(RESOURCETYPE_ANNONCE_MESSAGE, IDENTIFIANT_CONSULTATION_SUPPORT_MESSAGE, idPlatform + "/" + idConsultation + "/" + id);
        }
        SuiviAnnonce suiviAnnonce = optionalSuiviAnnonce.get();
        if (!idPlatform.equals(suiviAnnonce.getIdPlatform())) {
            throw new ResourceNotFoundException(RESOURCETYPE_ANNONCE_MESSAGE, IDENTIFIANT_CONSULTATION_SUPPORT_MESSAGE, idPlatform + "/" + idConsultation + "/" + id);
        }
        if (!idConsultation.equals(suiviAnnonce.getIdConsultation())) {
            throw new ResourceNotFoundException(RESOURCETYPE_ANNONCE_MESSAGE, IDENTIFIANT_CONSULTATION_SUPPORT_MESSAGE, idPlatform + "/" + idConsultation + "/" + id);
        }
        if (suiviAnnonce.getStatut() != StatutSuiviAnnonceEnum.EN_ATTENTE_VALIDATION_SIP && suiviAnnonce.getStatut() != StatutSuiviAnnonceEnum.EN_ATTENTE_VALIDATION_ECO && suiviAnnonce.getStatut() != StatutSuiviAnnonceEnum.BROUILLON && suiviAnnonce.getStatut() != StatutSuiviAnnonceEnum.COMPLET && suiviAnnonce.getStatut() != StatutSuiviAnnonceEnum.EXTERNAL) {
            throw new ConflictingResourceException("Impossible de supprimer l'annonce ; elle est à l'état " + suiviAnnonce.getStatut().name());
        }
        EFormsFormulaireEntity eFormsFormulaire = suiviAnnonce.getFormulaire();
        if (eFormsFormulaire != null) {
            eFormsFormulaireRepository.deleteById(eFormsFormulaire.getId());
        }
        PieceJointe avisFichier = suiviAnnonce.getAvisFichier();
        PieceJointe avisFichierSimplifie = suiviAnnonce.getAvisFichierSimplifie();
        repository.delete(suiviAnnonce);
        if (suiviAnnonce.getTypeAvisPub() == null) {
            if (avisFichier != null) {
                pieceJointeService.deleteById(avisFichier.getId());
            }
            if (avisFichierSimplifie != null) {
                pieceJointeService.deleteById(avisFichierSimplifie.getId());
            }
        }
        return true;
    }

    @Override
    public List<SuiviAnnonceDTO> updateSuiviAnnonces(String idPlatform, Integer idConsultation, SuiviAnnonceDTO suiviAnnonceDTO) {
        validateStatutUpdate(suiviAnnonceDTO, StatutSuiviAnnonceEnum.EN_ATTENTE_VALIDATION_ECO, StatutSuiviAnnonceEnum.BROUILLON, StatutSuiviAnnonceEnum.EN_ATTENTE);
        List<SuiviAnnonce> suiviAnnonces = repository.findAllByIdPlatformAndIdConsultation(idPlatform, idConsultation);
        if (suiviAnnonces.isEmpty()) {
            throw new ResourceNotFoundException("Annonces", "id plate-forme/id consultation", idPlatform + "/" + idConsultation);
        }
        return suiviAnnonces.stream().filter(suiviAnnonce -> suiviAnnonce.getStatut() == StatutSuiviAnnonceEnum.BROUILLON || suiviAnnonce.getStatut() == StatutSuiviAnnonceEnum.REJETER_CONCENTRATEUR).map(suiviAnnonce -> {
            suiviAnnonceMapper.updateSuiviAnnonceFromSuiviAnnonceDTO(suiviAnnonceDTO, suiviAnnonce);
            updateDateDemande(suiviAnnonceDTO, suiviAnnonce);
            this.updateOffreFromXml(suiviAnnonce);
            return repository.save(suiviAnnonce);
        }).map(suiviAnnonceMapper::suiviAnnonceToSuiviAnnonceDTOIgnoreSupport).toList();
    }

    @Override
    public String toXmlIntegration(Message<?> message) {
        Map<String, Object> headers = message.getHeaders();

        int idAnnonce = (Integer) headers.get("idAnnonce");
        SuiviAnnonce suiviAnnonce = repository.findById(idAnnonce).orElseThrow(() -> new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucun suivi d'annonce trouvé pour l'id " + idAnnonce));
        return toXml(suiviAnnonce);

    }

    @Override
    public String toXml(SuiviAnnonce suiviAnnonce) {
        String xmlBase64 = suiviAnnonce.getXml();
        Offre offre1 = suiviAnnonce.getOffre();
        if (!StringUtils.hasText(xmlBase64)) {
            String codeSupport = suiviAnnonce.getSupport() != null ? suiviAnnonce.getSupport().getCode() : null;
            throw new MissingArgumentException("Le XML de l'annonce ayant pour id plate-forme/id consultation/code du support " + suiviAnnonce.getIdPlatform() + "/" + suiviAnnonce.getIdConsultation() + "/" + codeSupport + " est vide.");
        }
        String xml = new String(Base64.decodeBase64(xmlBase64.getBytes()));
        String baliseBody = "<CONTRACTING_BODY>";
        int indexBody = xml.indexOf(baliseBody);
        if (indexBody != -1 && !xml.contains("<OFFRE>") && offre1 != null) {
            try {
                String baliseAddress = "</ADDRESS_CONTRACTING_BODY>";
                String offreXml = "<OFFRE><CODE><![CDATA[" + offre1.getCode() + "]]></CODE><LIBELLE><![CDATA[" + offre1.getLibelle() + "]]></LIBELLE></OFFRE>";
                int indexAddress = xml.indexOf(baliseAddress);
                if (indexAddress != -1) {
                    return insertString(xml, offreXml, indexAddress + baliseAddress.length());
                } else {
                    return insertString(xml, offreXml, indexBody + baliseBody.length());
                }
            } catch (Exception e) {
                log.error("Erreur lors de l'extraction XML : {}", e.getMessage());
            }
        }
        return xml;
    }

    @Override
    public String toXmlTransaction(SuiviAnnonce suiviAnnonce) {
        String xmlTransactionBase64 = suiviAnnonce.getXmlTransaction();
        String xmlBase64 = suiviAnnonce.getXml();
        String xmlTransaction = null;
        if (StringUtils.hasText(xmlTransactionBase64)) {
            xmlTransaction = new String(Base64.decodeBase64(xmlTransactionBase64));
        }
        if (!StringUtils.hasText(xmlTransaction) && StringUtils.hasText(xmlBase64)) {
            String xml = new String(Base64.decodeBase64(xmlBase64));
            try (ByteArrayInputStream inputStream = new ByteArrayInputStream(xml.getBytes())) {
                JAXBContext jaxbContextTed = JAXBContext.newInstance(TedEsenders.class);
                Unmarshaller jaxbUnmarshaller = jaxbContextTed.createUnmarshaller();
                JAXBElement<TedEsenders> unmarshal = (JAXBElement<TedEsenders>) jaxbUnmarshaller.unmarshal(inputStream);
                TedEsenders tedEsenders = unmarshal.getValue();
                if (tedEsenders == null || tedEsenders.getFORMSECTION() == null || tedEsenders.getFORMSECTION().getF052014() == null) {
                    String codeSupport = suiviAnnonce.getOffre() != null && suiviAnnonce.getSupport() != null ? suiviAnnonce.getSupport().getCode() : null;
                    throw new MissingArgumentException("La baslise <FORMS> du XML de l'annonce ayant pour id plate-forme/id consultation/code du support " + suiviAnnonce.getIdPlatform() + "/" + suiviAnnonce.getIdConsultation() + "/" + codeSupport + " est vide.");

                }
                F052014 f052014 = tedEsenders.getFORMSECTION().getF052014();
                xmlTransaction = this.getXmlTransaction(suiviAnnonce.getIdPlatform(), f052014, tedEsenders);
            } catch (Exception e) {
                log.error("Erreur lors de la récupération du xml transaction : {}", e.getMessage());
            }
        }
        if (!StringUtils.hasText(xmlTransaction)) {
            String codeSupport = suiviAnnonce.getOffre() != null && suiviAnnonce.getSupport() != null ? suiviAnnonce.getSupport().getCode() : null;
            throw new MissingArgumentException("Le XML Transaction de l'annonce ayant pour id plate-forme/id consultation/code du support " + suiviAnnonce.getIdPlatform() + "/" + suiviAnnonce.getIdConsultation() + "/" + codeSupport + " est vide.");
        }
        return xmlTransaction;
    }

    @Override
    public String geDocumentName(SuiviAnnonce suiviAnnonce, boolean convertPdf) {
        String idPlatform = suiviAnnonce.getIdPlatform();
        Support support = suiviAnnonce.getSupport();
        if (support == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Le support de l'annonce ayant pour id plate-forme/id consultation " + suiviAnnonce.getIdPlatform() + "/" + suiviAnnonce.getIdConsultation() + " est vide.");
        }
        Offre offre = suiviAnnonce.getOffre();
        if (offre == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "L'offre de l'annonce ayant pour id plate-forme/id consultation " + suiviAnnonce.getIdPlatform() + "/" + suiviAnnonce.getIdConsultation() + " est vide.");
        }
        String codeSupport = support.getCode();
        Integer idConsultation = suiviAnnonce.getIdConsultation();
        PieceJointe avisFichier;
        SuiviTypeAvisPub typeAvisPub = suiviAnnonce.getTypeAvisPub();
        if (typeAvisPub != null && typeAvisPub.getAvisPortail() != null && espaceDocumentaireService.fileExists(typeAvisPub.getAvisPortail())) {
            avisFichier = typeAvisPub.getAvisPortail();
        } else if (suiviAnnonce.isSimplifie()) {
            avisFichier = suiviAnnonce.getAvisFichierSimplifie();
        } else {
            avisFichier = suiviAnnonce.getAvisFichier();
        }
        if (avisFichier != null && espaceDocumentaireService.fileExists(avisFichier)) {
            if (!convertPdf) {
                return avisFichier.getName();
            } else {
                return FilenameUtils.getBaseName(avisFichier.getName()) + ".pdf";
            }
        }
        if (support.isEuropeen()) {
            EFormsFormulaireEntity formulaire = suiviAnnonce.getFormulaire();
            if (formulaire == null) {
                log.error("Aucun formulaire est attaché à ce suivi d'annonce {} / {} / {}", idPlatform, idConsultation, codeSupport);

                throw new PdfExportException(IDENTIFIANT_CONSULTATION_SUPPORT_MESSAGE, idPlatform + "/" + idConsultation + "/" + codeSupport);
            }
            if (!"esentool".equals(formulaire.getTedVersion())) {
                return "export.pdf";
            }

        }
        return offre.getLibelle() + (!convertPdf ? ".docx" : ".pdf");
    }

    @Override
    public void exportToDocument(SuiviAnnonce suiviAnnonce, OutputStream exportPdf, Boolean simplifier, boolean convertPdf) {
        String idPlatform = suiviAnnonce.getIdPlatform();
        Support support = suiviAnnonce.getSupport();
        if (support == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Le support de l'annonce ayant pour id plate-forme/id consultation " + suiviAnnonce.getIdPlatform() + "/" + suiviAnnonce.getIdConsultation() + " est vide.");
        }
        Offre offre = suiviAnnonce.getOffre();
        if (offre == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "L'offre de l'annonce ayant pour id plate-forme/id consultation " + suiviAnnonce.getIdPlatform() + "/" + suiviAnnonce.getIdConsultation() + " est vide.");
        }
        log.info("Export de l'annonce {} / {} / {}", idPlatform, suiviAnnonce.getIdConsultation(), support.getCode());
        SuiviTypeAvisPub typeAvisPub = suiviAnnonce.getTypeAvisPub();
        String codeSupport = support.getCode();
        Integer idConsultation = suiviAnnonce.getIdConsultation();

        PieceJointe avisFichier;
        if (typeAvisPub != null && typeAvisPub.getAvisPortail() != null && espaceDocumentaireService.fileExists(typeAvisPub.getAvisPortail())) {
            avisFichier = typeAvisPub.getAvisPortail();
        } else if (suiviAnnonce.isSimplifie() || Boolean.TRUE.equals(simplifier)) {
            avisFichier = suiviAnnonce.getAvisFichierSimplifie();
        } else {
            avisFichier = suiviAnnonce.getAvisFichier();
        }
        if (avisFichier != null && espaceDocumentaireService.fileExists(avisFichier)) {
            String extension = pieceJointeService.getExtension(avisFichier.getName());
            log.info("Récupération du fichier {} en {} pour l'annonce {}", avisFichier.getName(), convertPdf ? "PDF" : extension, suiviAnnonce.getId());
            if (!convertPdf || "pdf".equalsIgnoreCase(extension)) {
                try (InputStream resource = espaceDocumentaireService.loadFileAsResource(avisFichier.getPath(), extension)) {
                    resource.transferTo(exportPdf);
                } catch (IOException e) {
                    log.error("Erreur lors de la récupération du formulaire : {}", e.getMessage());
                    throw new PdfExportException(e, IDENTIFIANT_CONSULTATION_SUPPORT_MESSAGE, idPlatform + "/" + idConsultation + "/" + codeSupport);
                }
                return;
            } else {
                try (InputStream resource = espaceDocumentaireService.loadFileAsResourcePDF(avisFichier.getPath(), extension)) {
                    resource.transferTo(exportPdf);
                } catch (IOException e) {
                    log.error("Erreur lors de la récupération du formulaire : {}", e.getMessage());
                    throw new PdfExportException(e, IDENTIFIANT_CONSULTATION_SUPPORT_MESSAGE, idPlatform + "/" + idConsultation + "/" + codeSupport);
                }
                return;
            }
        }
        if (support.isEuropeen()) {
            EFormsFormulaireEntity formulaire = suiviAnnonce.getFormulaire();
            if (formulaire != null && !"esentool".equals(formulaire.getTedVersion())) {
                log.info("Récupération du pdf européen {} pour l'annonce {}", formulaire.getUuid(), suiviAnnonce.getId());
                try {
                    var plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(idPlatform);
                    String eformsToken = plateformeConfiguration == null ? null : plateformeConfiguration.getEformsToken();
                    byte[] binaryData = Base64.decodeBase64(formulaire.getXmlGenere().getBytes());
                    viewerWS.reportPDF(binaryData, ViewerDTO.builder().summary(suiviAnnonce.isSimplifie()).language(formulaire.getLang().substring(0, 2).toLowerCase()).build(), eformsToken).getInputStream().transferTo(exportPdf);
                    return;
                } catch (IOException e) {
                    throw new PdfExportException(e, IDENTIFIANT_CONSULTATION_SUPPORT_MESSAGE, idPlatform + "/" + idConsultation + "/" + codeSupport);
                }
            }

        }
        log.info("Récupération du formulaire à partir du XML pour l'annonce {}", suiviAnnonce.getId());
        String xmlBase64 = suiviAnnonce.getXml();
        if (StringUtils.hasText(xmlBase64)) {
            try (ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64.decodeBase64(xmlBase64.getBytes()))) {
                JAXBContext jaxbContext = JAXBContext.newInstance(TedEsenders.class);
                javax.xml.bind.Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                JAXBElement<TedEsenders> unmarshal = (JAXBElement<TedEsenders>) jaxbUnmarshaller.unmarshal(inputStream);
                TedEsenders tedEsenders = unmarshal.getValue();
                if (tedEsenders == null || tedEsenders.getFORMSECTION() == null || tedEsenders.getFORMSECTION().getF052014() == null || tedEsenders.getFORMSECTION().getF052014().getProcedure() == null || StringUtils.isEmpty(tedEsenders.getFORMSECTION().getF052014().getProcedure().getTECHNIQUEACHAT())) {
                    exportToPdfV1(suiviAnnonce, exportPdf);
                } else {
                    exportToPdfV2(typeAvisPub, suiviAnnonce, offre, exportPdf, tedEsenders, simplifier, convertPdf);
                }
            } catch (Exception e) {
                log.error("Erreur lors de la récupération du formulaire : {}", e.getMessage());
                throw new PdfExportException(e, IDENTIFIANT_CONSULTATION_SUPPORT_MESSAGE, idPlatform + "/" + idConsultation + "/" + codeSupport);
            }
        } else {
            TedEsenders tedEsenders = new TedEsenders();
            FormSection formsection = new FormSection();
            formsection.setNOTICEUUID(suiviAnnonce.getIdAnnonce());
            tedEsenders.setFORMSECTION(formsection);
            exportToPdfV2(typeAvisPub, suiviAnnonce, offre, exportPdf, tedEsenders, simplifier, convertPdf);
        }
    }


    @Override
    public void exportToPdfV2(SuiviTypeAvisPub avisPub, SuiviAnnonce suiviAnnonce, Offre offre, OutputStream exportPdf, TedEsenders tedEsenders, Boolean simplifier, boolean convertToPdf) {

        List<KeyValueRequest> map = new ArrayList<>();
        map.add(KeyValueRequest.builder().key("forms").value(tedEsenders.getFORMSECTION().getF052014()).build());
        map.add(KeyValueRequest.builder().key("sender").value(tedEsenders.getSENDER()).build());
        Integer idConsultation = suiviAnnonce.getIdConsultation();
        String idPlatform = suiviAnnonce.getIdPlatform();
        ConfigurationConsultationDTO configurationByPlateforme = agentService.getConfigurationByPlateforme(idConsultation, idPlatform);
        if (configurationByPlateforme != null) {
            ConsultationContexte contexte = configurationByPlateforme.getContexte();
            map.add(KeyValueRequest.builder().key("ctx").value(contexte).build());

        }
        SuiviAnnonceDTO suiviAnnonceDTO = suiviAnnonceMapper.suiviAnnonceToSuiviAnnonceDTO(suiviAnnonce);
        map.add(KeyValueRequest.builder().key("annonce").value(suiviAnnonceDTO).build());

        if (avisPub != null) {
            map.add(KeyValueRequest.builder().key("avisPub").value(suiviTypeAvisPubMapper.mapToModel(avisPub)).build());
            if (avisPub.getParent() != null) {
                map.add(KeyValueRequest.builder().key("avisPubParent").value(suiviTypeAvisPubMapper.mapToModel(avisPub.getParent())).build());
            }
        }
        map.add(KeyValueRequest.builder().key("offre").value(suiviAnnonceDTO.getOffre()).build());
        map.add(KeyValueRequest.builder().key("support").value(suiviAnnonceDTO.getSupport()).build());
        File template = espaceDocumentaireService.downloadAvisTemplate(offre, simplifier != null ? simplifier : suiviAnnonceDTO.isSimplifie());
        try (InputStream generatedFile = convertToPdf ? docGenWsPort.generatePdf(map, template) : docGenWsPort.generateDocument(map, template, "")) {
            if (generatedFile != null) {
                generatedFile.transferTo(exportPdf);
            }
        } catch (Exception e) {
            String codeSupport = suiviAnnonceDTO.getOffre() != null && suiviAnnonceDTO.getSupport() != null ? suiviAnnonceDTO.getSupport().getCode() : null;
            throw new ExportConfigurationException(e, IDENTIFIANT_CONSULTATION_SUPPORT_MESSAGE, suiviAnnonceDTO.getIdPlatform() + "/" + suiviAnnonceDTO.getIdConsultation() + "/" + codeSupport);
        } finally {
            if (template != null) {
                template.deleteOnExit();
            }
        }
    }

    public void exportToPdfV1(SuiviAnnonce suiviAnnonce, OutputStream exportPdf) {
        try {
            exportToPdf(suiviAnnonce, new ClassPathResource(exportPdfXslPath).getFile(), exportPdf, buildXslParameters(suiviAnnonce));
        } catch (IOException | FOPException | TransformerConfigurationException e) {
            String codeSupport = suiviAnnonce.getOffre() != null && suiviAnnonce.getSupport() != null ? suiviAnnonce.getSupport().getCode() : null;
            throw new ExportConfigurationException(e, IDENTIFIANT_CONSULTATION_SUPPORT_MESSAGE, suiviAnnonce.getIdPlatform() + "/" + suiviAnnonce.getIdConsultation() + "/" + codeSupport);
        } catch (XslTransformationException e) {
            String codeSupport = suiviAnnonce.getOffre() != null && suiviAnnonce.getSupport() != null ? suiviAnnonce.getSupport().getCode() : null;
            throw new PdfExportException(e, IDENTIFIANT_CONSULTATION_SUPPORT_MESSAGE, suiviAnnonce.getIdPlatform() + "/" + suiviAnnonce.getIdConsultation() + "/" + codeSupport);
        }
    }

    private Map<String, Object> buildXslParameters(SuiviAnnonce suiviAnnonce) {
        Map<String, Object> parameters = new HashMap<>();
        if (suiviAnnonce.getStatut() == StatutSuiviAnnonceEnum.BROUILLON) {
            parameters.put("brouillon", true);
        } else {
            parameters.put("statut", suiviAnnonce.getStatut().name());
            if (suiviAnnonce.getDateDemandeEnvoi() != null) {
                parameters.put("dateDemandeEnvoi", suiviAnnonce.getDateDemandeEnvoi().atZone(ZoneId.systemDefault()).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
            }
            if (suiviAnnonce.getDateDebutEnvoi() != null) {
                parameters.put("dateDebutEnvoi", suiviAnnonce.getDateDebutEnvoi().atZone(ZoneId.systemDefault()).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
            }
            if (suiviAnnonce.getDateEnvoi() != null) {
                parameters.put("dateEnvoi", suiviAnnonce.getDateEnvoi().atZone(ZoneId.systemDefault()).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
            }
            if (suiviAnnonce.getDatePublication() != null) {
                parameters.put("datePublication", suiviAnnonce.getDatePublication().atZone(ZoneId.systemDefault()).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
            }
        }
        if (suiviAnnonce.getOffre() != null && suiviAnnonce.getSupport() != null && suiviAnnonce.getSupport().getLibelle() != null) {
            parameters.put("support", suiviAnnonce.getSupport().getLibelle());
        }
        if (suiviAnnonce.getOffre() != null && suiviAnnonce.getOffre().getLibelle() != null) {
            parameters.put("offre", suiviAnnonce.getOffre().getLibelle());
        }
        return parameters;
    }

    @Override
    public String updateXmlTransform(Integer idAnnonce, String xmlTransform) {
        Optional<SuiviAnnonce> optionalSuiviAnnonce = repository.findById(idAnnonce);
        if (optionalSuiviAnnonce.isEmpty()) {
            throw new ResourceNotFoundException(RESOURCETYPE_ANNONCE_MESSAGE, (long) idAnnonce);
        }
        SuiviAnnonce suiviAnnonce = optionalSuiviAnnonce.get();
        suiviAnnonce.setXmlTransform(Base64.encodeBase64StringUnChunked(xmlTransform.getBytes()));
        this.updateOffreFromXml(suiviAnnonce);
        repository.save(suiviAnnonce);
        return xmlTransform;
    }

    @Override
    public void updateErrorMessage(Integer idAnnonce, String errorMessage, boolean rejetConcentrateur) {
        Optional<SuiviAnnonce> optionalSuiviAnnonce = repository.findById(idAnnonce);
        if (optionalSuiviAnnonce.isEmpty()) {
            log.error("Mise à jour de l'annonce en erreur : impossible de retrouver l'annonce ayant pour ID {}", idAnnonce);
            return;
        }

        log.info("Mise à jour de l'annonce en erreur : {} => {}", idAnnonce, errorMessage);
        SuiviAnnonce suiviAnnonce = optionalSuiviAnnonce.get();
        //todo check si c'est bien dans le cas d'un rejet support et pas concentrateur qu'on va prendre en compte les tentatives
        //si c'est un rejet concentrateur ou un rejet support avec un nb de tentatives supérieur à 5 on met l'annonce au statut REJETER
        if ((rejetConcentrateur || suiviAnnonce.getTentative() >= 5) && !errorMessage.contains("503")) {
            suiviAnnonce.setMessageStatut(errorMessage);
            suiviAnnonce.setDateEnvoi(Instant.now());
            if (rejetConcentrateur) {
                suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.REJETER_CONCENTRATEUR);
            } else {
                suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.REJETER_SUPPORT);
            }
            EFormsFormulaireEntity formulaire = suiviAnnonce.getFormulaire();
            if (formulaire != null) {
                formulaire.setStatut(StatutENoticeEnum.getStatutENoticeFromSuiviAnnonce(suiviAnnonce.getStatut()));
                formulaire.setDateModificationStatut(ZonedDateTime.now());
                eFormsFormulaireRepository.save(formulaire);
            }
            suiviAnnonce = repository.save(suiviAnnonce);
            SuiviTypeAvisPub typeAvisPub = suiviAnnonce.getTypeAvisPub();
            if (typeAvisPub != null) {
                List<StatutSuiviAnnonceEnum> statuts = typeAvisPub.getAnnonces().stream().map(SuiviAnnonce::getStatut).distinct().toList();
                StatutTypeAvisAnnonceEnum statutFromAnnonceEnum = StatutTypeAvisAnnonceEnum.getStatutFromAnnonceEnum(statuts);
                log.info("Mise à jour du statut de l'avis {} => {}", typeAvisPub.getId(), statutFromAnnonceEnum);
                typeAvisPub.setStatut(statutFromAnnonceEnum);
                typeAvisPub.setDateModificationStatut(ZonedDateTime.now());
                typeAvisPub.setRaisonRefus("Erreur lors du l'envoi. Veuillez contacter le support.");
                typeAvisPub = suiviTypeAvisPubRepository.save(typeAvisPub);
                if (suiviAnnonce.getSupport().isEuropeen() && !CollectionUtils.isEmpty(typeAvisPub.getNationalAnnonces())) {
                    repository.saveAll(typeAvisPub.getNationalAnnonces().stream().peek(nationalAnnonce -> {
                        nationalAnnonce.setStatut(StatutSuiviAnnonceEnum.ANNULER);
                        nationalAnnonce.setDateDebutEnvoi(Instant.now());
                        nationalAnnonce.setMessageStatut("Rejet de l'annonce européenne");
                    }).collect(toList()));
                }
            }
        } else {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
            suiviAnnonce.setTentative(suiviAnnonce.getTentative() + 1);
            String message = "<ul> Essai : " + suiviAnnonce.getTentative() + 1 + "</br>" +
                    "<li> Horaire : " + formatter.format(ZonedDateTime.now()) + "</li>" +
                    "<li> Détail message : " + errorMessage + "</li>" +
                    "</ul>";
            String messageErreur = suiviAnnonce.getMessageErreur() == null ? message : suiviAnnonce.getMessageErreur() + " " + message;
            suiviAnnonce.setMessageErreur(messageErreur);
            suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.RETRY);
            repository.save(suiviAnnonce);
        }

    }

    @Override
    public boolean updateConfiguration(ConfigurationUpdateDTO config, Set<String> roles, String token) {
        log.info("Mise à jour de la configuration de la consultation {} / {}", config.getIdPlatform(), config.getIdConsultation());
        String dmls = config.getDmls();
        List<JalDTO> jalList = CollectionUtils.isEmpty(config.getJalList()) ? new ArrayList<>() : config.getJalList().stream().peek(jalDTO -> jalDTO.setNom(StringEscapeUtils.unescapeHtml4(jalDTO.getNom()))).distinct().toList();

        List<ConsultationFacturationDTO> facturationList = config.getFacturationList().stream().peek(facturationDTO -> facturationDTO.setAdresse(StringEscapeUtils.unescapeHtml4(facturationDTO.getAdresse()))).distinct().toList();
        ConfigurationConsultationDTO configurationConsultationDTO = ConfigurationConsultationDTO.with().blocList(config.getBlocList()).jalList(jalList).facturationList(facturationList).europeen(config.getEuropeen() != null && config.getEuropeen()).build();
        String xmlDecoded = config.getXml();
        String idPlatform = config.getIdPlatform();
        Integer idConsultation = config.getIdConsultation();

        if (xmlDecoded != null) {
            configurationConsultationDTO.setXml(xmlDecoded);

            repository.updateXmlByIdPlatformAndIdConsultation(idPlatform, idConsultation, Base64.encodeBase64StringUnChunked(xmlDecoded.getBytes()));

            try (ByteArrayInputStream inputStream = new ByteArrayInputStream(xmlDecoded.getBytes())) {
                JAXBContext jaxbContext = JAXBContext.newInstance(TedEsenders.class);
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                JAXBElement<TedEsenders> unmarshal = (JAXBElement<TedEsenders>) jaxbUnmarshaller.unmarshal(inputStream);
                TedEsenders tedEsenders = unmarshal.getValue();

                ObjectContractF05 objectcontract = tedEsenders.getFORMSECTION().getF052014().getOBJECTCONTRACT();
                if (objectcontract != null) {
                    if (objectcontract.getValEstimatedTotal() != null) {
                        BigDecimal value = objectcontract.getValEstimatedTotal().getValue();
                        configurationConsultationDTO.setMontant(value);
                    }
                    List<ObjectF05> objectDescr = objectcontract.getObjectDescr();
                    if (!CollectionUtils.isEmpty(objectDescr)) {
                        List<String> departements2016 = new ArrayList<>(objectDescr.stream().map(ObjectF05::getNuts).filter(nuts1 -> !CollectionUtils.isEmpty(nuts1)).flatMap(List::stream).filter(Objects::nonNull).map(Nuts::getCODE).filter(Objects::nonNull).distinct().toList());
                        List<String> departements2021 = objectDescr.stream().map(ObjectF05::getNuts2021).filter(nuts1 -> !CollectionUtils.isEmpty(nuts1)).flatMap(List::stream).filter(Objects::nonNull).map(Nuts::getCODE).filter(Objects::nonNull).distinct().toList();
                        departements2016.addAll(departements2021);
                        configurationConsultationDTO.setCodeNuts(departements2016);
                        configurationConsultationDTO.setDepartements(departements2016.stream().map(nutsToDepartement::get).filter(Objects::nonNull).toList());
                    }
                }

            } catch (Exception e) {
                log.error("Erreur lors de la mise à jour de l'avis {}", e.getMessage());
            }
        }
        configurationConsultationDTO.setUrlEforms(urlEforms);
        PlateformeConfiguration plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(config.getIdPlatform());
        if (plateformeConfiguration != null) {
            configurationConsultationDTO.setJal(plateformeConfiguration.isJal());
            configurationConsultationDTO.setPqr(plateformeConfiguration.isPqr());
            if (config.getEuropeen() == null) {
                configurationConsultationDTO.setEuropeen(plateformeConfiguration.isEuropeen());
            }
            configurationConsultationDTO.setNational(plateformeConfiguration.isNational());
            String supports = plateformeConfiguration.getSupports();
            configurationConsultationDTO.setGroupement(plateformeConfiguration.isGroupement());
            configurationConsultationDTO.setValidationObligatoire(plateformeConfiguration.isValidationObligatoire());
            if (StringUtils.hasText(supports)) configurationConsultationDTO.setSupports(List.of(supports.split(",")));
            configurationConsultationDTO.setEformsModal(plateformeConfiguration.isEformsModal());
        } else {
            configurationConsultationDTO.setJal(true);
            configurationConsultationDTO.setPqr(true);
            if (config.getEuropeen() == null) {
                configurationConsultationDTO.setEuropeen(false);
            }
            configurationConsultationDTO.setNational(true);
        }

        configurationConsultationDTO.setDmls(dmls);
        Agent agent = agentService.findByToken(token);
        if (agent != null) {
            String serveurToken = agent.getMpeToken();
            String serveurUrl = agent.getOrganisme().getPlateformeUrl();
            if (StringUtils.hasText(serveurUrl) && StringUtils.hasText(serveurToken)) {
                try {
                    ConsultationContexte consultationContexte = ieFormsNoticeServices.getConsultationContexte(serveurUrl, serveurToken, idConsultation, config.getIdPlatform(), Boolean.TRUE.equals(config.getEuropeen()), agent.getIdMpe());
                    try {
                        List<SuiviTypeAvisPub> typeAvisPubs = suiviTypeAvisPubRepository.findByOrganismePlateformeUuidAndIdConsultationAndChildrenIsEmpty(config.getIdPlatform(), config.getIdConsultation());
                        if (!CollectionUtils.isEmpty(typeAvisPubs) && consultationContexte != null && consultationContexte.getConsultation() != null) {
                            ZonedDateTime dlro = consultationContexte.getConsultation().getDateLimiteRemiseOffres();
                            ZonedDateTime dateMiseEnLigne = null;
                            if (StringUtils.hasText(dmls)) {
                                Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(dmls);
                                dateMiseEnLigne = ZonedDateTime.ofInstant(date1.toInstant(), ZoneId.systemDefault());
                            }

                            for (SuiviTypeAvisPub suiviTypeAvisPub : typeAvisPubs) {
                                boolean toSave = false;
                                if (dateMiseEnLigne != null && !dateMiseEnLigne.equals(suiviTypeAvisPub.getDateMiseEnLigne()) && suiviTypeAvisPub.getDateMiseEnLigneCalcule() == null) {
                                    suiviTypeAvisPub.setDateMiseEnLigne(dateMiseEnLigne);
                                    toSave = true;
                                }
                                if (dlro != null && !dlro.equals(suiviTypeAvisPub.getDlro()) && suiviTypeAvisPub.getDateMiseEnLigneCalcule() == null) {
                                    suiviTypeAvisPub.setDlro(dlro);
                                    configurationConsultationDTO.setDlro(dlro);
                                    toSave = true;
                                }

                                if (toSave) {
                                    suiviTypeAvisPubRepository.save(suiviTypeAvisPub);
                                }
                            }
                        }
                    } catch (Exception e) {
                        log.error("Erreur lors de la mise à jour de DMLS / DLRO de l'avis {}", e.getMessage());
                    }
                    if (consultationContexte != null) {
                        Long idService = consultationContexte.getServiceConsultation() != null ? consultationContexte.getServiceConsultation().getId() : null;
                        OrganismeMpe organismeConsultation = consultationContexte.getOrganismeConsultation();
                        Organisme savedOrganismeConsultation = null;
                        if (organismeConsultation != null) {
                            savedOrganismeConsultation = this.organismeService.savePlateformeOrganismeIfNotExist(organismeConsultation, serveurUrl, idPlatform);
                            if (savedOrganismeConsultation != null) {
                                repository.updateOrganismeByIdPlatformAndIdConsultation(idPlatform, idConsultation, savedOrganismeConsultation);
                            }
                        }
                        String organisme = organismeConsultation != null ? organismeConsultation.getAcronyme() : null;
                        if (hasSupportsWithAcheteurs(plateformeConfiguration) && savedOrganismeConsultation != null) {
                            List<AcheteurMpe> acheteurs = mpeWs.getAcheteurs(idService, organisme, serveurUrl, serveurToken);
                            if (!CollectionUtils.isEmpty(acheteurs)) {
                                this.updateAcheteurs(acheteurs, savedOrganismeConsultation);
                                List<AcheteurChoixDTO> comptes = new ArrayList<>();
                                if (!CollectionUtils.isEmpty(acheteurs)) {
                                    acheteurs.forEach(acheteur -> comptes.add(AcheteurChoixDTO.builder().email(acheteur.getEmail()).login(acheteur.getLogin()).build()));
                                    consultationContexte.setComptes(comptes);
                                }
                            }
                        } else {
                            consultationContexte.setComptes(new ArrayList<>());
                        }
                        configurationConsultationDTO.setContexte(consultationContexte);
                        if (!CollectionUtils.isEmpty(jalList)) {
                            this.updateSupportJal(jalList, plateformeConfiguration, consultationContexte.getTypeProcedureConsultation(), organismeConsultation);
                        }
                    }

                } catch (Exception e) {
                    log.error("Erreur lors de la récupération des données depuis le serveur {}", e.getMessage());
                }
            }
        }


        String contexteString = EFormsHelper.getString(configurationConsultationDTO);
        if (agent != null && idConsultation != null && agent.getId() != null) {
            agentConsultationSessionRepository.findByAgentIdAndIdConsultation(agent.getId(), idConsultation).ifPresentOrElse(agentConsultationSession -> {
                agentConsultationSessionRepository.updateContexte(idConsultation, agent.getId(), contexteString);
            }, () -> {
                AgentConsultationSessionEntity session = AgentConsultationSessionEntity.builder().contexte(contexteString).agent(agent).idConsultation(idConsultation).build();
                agentConsultationSessionRepository.save(session);
            });
        }

        return true;
    }

    public boolean hasSupportsWithAcheteurs(PlateformeConfiguration plateformeConfiguration) {
        if (plateformeConfiguration == null) {
            plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(plateforme);
        }
        if (plateformeConfiguration == null) {
            return false;
        }

        String supports = plateformeConfiguration.getSupports();
        if (StringUtils.hasText(supports)) {
            return supportRepository.existsByCodeInAndCompteObligatoireIsTrueAndActifIsTrue(Arrays.asList(supports.split(",")));
        }
        return supportRepository.existsByCompteObligatoireIsTrueAndActifIsTrue();
    }

    private void updateAcheteurs(List<AcheteurMpe> acheteurs, Organisme organisme) {
        List<AcheteurEntity> alreadySaved = this.acheteurRepository.findAllByOrganismeId(Math.toIntExact(organisme.getId()));
        if (!alreadySaved.isEmpty()) {
            alreadySaved.forEach(acheteurEntity -> {
                acheteurEntity.setActif(false);
                this.acheteurRepository.save(acheteurEntity);
            });
        }
        acheteurs.forEach(acheteur -> {
            AcheteurEntity savedAcheteur = this.acheteurRepository.findByLoginAndEmailAndOrganismeId(acheteur.getLogin(), acheteur.getEmail(), organisme.getId()).orElse(null);
            if (savedAcheteur == null) {
                savedAcheteur = this.acheteurMapper.mapToEntity(acheteur);
                savedAcheteur.setOrganisme(organisme);
            } else {
                savedAcheteur.setEmail(acheteur.getEmail());
                savedAcheteur.setLogin(acheteur.getLogin());
                savedAcheteur.setPassword(acheteur.getPassword());
                savedAcheteur.setToken(acheteur.getToken());
                savedAcheteur.setMoniteurProvenance(acheteur.getMoniteurProvenance());
            }
            savedAcheteur.setActif(true);
            this.acheteurRepository.save(savedAcheteur);
        });
    }

    private void updateSupportJal(List<JalDTO> jalList, PlateformeConfiguration plateformeConfiguration, ReferentielMpe typeProcedure, OrganismeMpe mpeOrganisme) {
        if (plateformeConfiguration != null && (!plateformeConfiguration.isAssociationOrganisme() || !plateformeConfiguration.isAssociationProcedure())) {
            log.info("La plateforme {} n'est pas configurée pour l'association des supports JAL", plateformeConfiguration.getIdPlateforme());
            return;
        }
        if (typeProcedure == null) {
            return;
        }
        var proceduresOffres = offreTypeProcedureRepository.findAllByAcronymeOrganismeAndProcedureAbbreviation(mpeOrganisme.getAcronyme(), typeProcedure.getAbreviation());
        String codeGroupe = "JAL";
        try {
            String pays = plateformeConfiguration == null ? "FRA" : plateformeConfiguration.getPays();
            List<Support> supportJalList = supportRepository.findByCodeGroupeAndPays(codeGroupe, pays);
            supportJalList.stream().filter(support -> jalList.stream().noneMatch(jalDTO -> {
                String codeSupport = pays + "-" + jalDTO.getEmail() + "-" + Base64.encodeBase64StringUnChunked(jalDTO.getNom().getBytes());
                return codeSupport.equals(support.getCode());
            })).forEach(support -> {
                support.setActif(false);
                supportRepository.save(support);
            });
            jalList.forEach(jalDTO -> {
                String codeSupport = pays + "-" + jalDTO.getEmail() + "-" + Base64.encodeBase64StringUnChunked(jalDTO.getNom().getBytes());
                Support support = supportRepository.findByCode(codeSupport);
                String nom = jalDTO.getNom();
                if (support == null) {
                    support = Support.builder().codeGroupe(codeGroupe).actif(true).pays(pays).choixUnique(true).libelle(nom).code(codeSupport).codeDestination("MAIL").mail(jalDTO.getEmail()).poids(0).national(true).europeen(false).description("Organe de presse : " + nom).url(jalDTO.getEmail()).external(false).build();

                } else {
                    support.setActif(true);
                    support.setLibelle(nom);
                    support.setDescription("Organe de presse : " + nom);
                }
                if (support.getOffres() == null) support.setOffres(new HashSet<>());
                List<Offre> offres = proceduresOffres.stream().map(OffreTypeProcedure::getOffre).toList();
                for (Offre offre : offres) {
                    if (support.getOffres().stream().noneMatch(offre1 -> offre1.getCode().equals(offre.getCode()))) {
                        support.getOffres().add(offre);
                    }
                }

                supportRepository.save(support);


            });
        } catch (Exception e) {
            log.error("Erreur lors de la mise à jour des supports JAL {}", e.getMessage());
        }
    }


    public String getCodeDestination(SuiviAnnonce payload) {
        SupportIntermediaire supportIntermediaire = supportIntermediaireRepository.findByIdPlateformeAndSupportCode(payload.getIdPlatform(), payload.getSupport().getCode());

        return supportIntermediaire == null ? payload.getSupport().getCodeDestination() : supportIntermediaire.getCodeDestination();
    }

    public String getDestinataire(SuiviAnnonce payload) {
        return payload.getSupport().getMail();
    }

    public String getCc(SuiviAnnonce payload) {
        SuiviTypeAvisPub suiviTypeAvisPub = payload.getTypeAvisPub();
        if (suiviTypeAvisPub == null) {
            return null;
        }
        if (StringUtils.hasText(mailTedCc)) {
            return mailTedCc;
        }
        ConsultationFacturation facturation = suiviTypeAvisPub.getFacturation();
        return facturation == null ? null : facturation.getMail();
    }

    public double getPdfLength(File file) {

        PDFTextStripper pdfStripper;

        String parsedText;
        PDFParser parser = null;
        try {
            parser = new PDFParser(new RandomAccessFile(file, "r"));
        } catch (IOException e) {
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage(), e);
        }
        try (COSDocument cosDoc = parser.getDocument(); PDDocument pdDoc = new PDDocument(cosDoc);) {

            parser.parse();

            pdfStripper = new PDFTextStripper();

            parsedText = pdfStripper.getText(pdDoc);
            String text = parsedText.trim();
            return text.length() * 0.183;
        } catch (Exception e) {
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage(), e);

        }
    }

    @Override
    public void sendTed(Message<?> message) {
        Map<String, Object> headers = message.getHeaders();
        int idAnnonce = (Integer) headers.get("idAnnonce");
        String codeDestination = (String) headers.get("codeDestination");

        SuiviAnnonce suiviAnnonce = repository.findById(idAnnonce).orElseThrow(() -> new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucun suivi d'annonce trouvé pour l'id " + idAnnonce));
        SuiviTypeAvisPub typeAvisPub = suiviAnnonce.getTypeAvisPub();
        PieceJointe avisFichier = suiviAnnonce.getAvisFichier();
        if (typeAvisPub != null && avisFichier != null) {
            suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.PUBLIER);
            Instant datePublication = ZonedDateTime.now().toInstant();
            suiviAnnonce.setDatePublication(datePublication);
            suiviAnnonce.setDateDebutEnvoi(Instant.now());
            suiviAnnonce.setDateEnvoi(Instant.now());
            suiviAnnonce = repository.save(suiviAnnonce);
            List<SuiviAnnonce> annonces = typeAvisPub.getNationalAnnonces();
            List<SuiviAnnonce> annoncesNationauxToSend = annonces.stream().filter(suiviAnnonce1 -> !StatutSuiviAnnonceEnum.EN_ATTENTE.equals(suiviAnnonce1.getStatut()) && !StatutSuiviAnnonceEnum.PUBLIER.equals(suiviAnnonce1.getStatut()) && !StatutSuiviAnnonceEnum.EN_COURS_DE_PUBLICATION.equals(suiviAnnonce1.getStatut()) && !StatutSuiviAnnonceEnum.EN_COURS_DE_TRAITEMENT.equals(suiviAnnonce1.getStatut())).toList();
            annoncesNationauxToSend.forEach(national -> {
                national.setStatut(StatutSuiviAnnonceEnum.EN_ATTENTE);
                national.setDateDemandeEnvoi(ZonedDateTime.now().toInstant());
                repository.save(national);
            });

            return;
        }
        EFormsFormulaireEntity formulaire = suiviAnnonce.getFormulaire();
        if (formulaire == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Un avis européen est obligatoire");
        }
        EFormsFormulaire formsFormulaire = ieFormsNoticeServices.submit(formulaire.getId(), suiviAnnonce.getIdPlatform(), suiviAnnonce.getTypeAvisPub() != null);
        if (formsFormulaire == null) {
            log.warn("Erreur lors de l'envoi de l'avis européen");
            this.resetSuiviAnnonceEuropeen(suiviAnnonce);
        } else {
            this.updateSuiviAnnonceEuropeen(suiviAnnonce, codeDestination);
        }
    }
    @Override
    public SuiviAnnonce sauvegarder(SuiviAnnonce annonce) {
        return repository.save(annonce);
    }


    @Override
    public SuiviAnnonce modifyAnnonceStatut(SuiviAnnonce suiviAnnonce, SuiviAnnonceUpdate update, String token) {

        EFormsFormulaireEntity formulaire = suiviAnnonce.getFormulaire();
        StatutSuiviAnnonceEnum statutSuiviAnnonce = update.getStatut();
        if (statutSuiviAnnonce != null) {
            if (formulaire != null && StatutSuiviAnnonceEnum.EN_COURS_D_ARRET.equals(statutSuiviAnnonce))
                ieFormsNoticeServices.stopPublication(formulaire.getId(), suiviAnnonce.getIdPlatform());
            else {
                suiviAnnonce.setStatut(statutSuiviAnnonce);
                if (formulaire != null) {
                    formulaire.setStatut(StatutENoticeEnum.getStatutENoticeFromSuiviAnnonce(suiviAnnonce.getStatut()));
                    formulaire.setDateModificationStatut(ZonedDateTime.now());
                    eFormsFormulaireRepository.save(formulaire);
                }
            }
        }
        return suiviAnnonce;
    }

    @Override
    public SuiviAnnonceDTO modifyStatut(Integer id, SuiviAnnonceUpdate update, String token) {


        SuiviAnnonce suiviAnnonce = repository.findById(id).orElseThrow(() -> new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucun suivi d'annonce trouvé pour l'id " + id));
        suiviAnnonce = repository.save(modifyAnnonceStatut(suiviAnnonce, update, token));

        SuiviTypeAvisPub typeAvisPub = suiviAnnonce.getTypeAvisPub();
        if (typeAvisPub != null) {
            List<StatutSuiviAnnonceEnum> statuts = typeAvisPub.getAnnonces().stream().map(SuiviAnnonce::getStatut).distinct().collect(toList());
            typeAvisPub.setStatut(StatutTypeAvisAnnonceEnum.getStatutFromAnnonceEnum(statuts));
            typeAvisPub.setDateModificationStatut(ZonedDateTime.now());
            suiviTypeAvisPubRepository.save(typeAvisPub);
        }
        return suiviAnnonceMapper.suiviAnnonceToSuiviAnnonceDTO(suiviAnnonce);
    }

    @Override
    public SuiviDTO ouvrirCompteAcheteur(Integer id, String token, String idPlatform, Integer idConsultation, boolean rie) {
        var annonce = repository.findById(id).orElseThrow(() -> new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucun suivi d'annonce trouvé pour l'id " + id));
        if (!idPlatform.equals(annonce.getIdPlatform())) {
            throw new AtexoException(ExceptionEnum.NOT_AUTHORIZED, "Annonce hors périmètre pour l'id " + id);
        }
        if (!idConsultation.equals(annonce.getIdConsultation())) {
            throw new AtexoException(ExceptionEnum.NOT_AUTHORIZED, "Annonce hors périmètre pour l'id " + id);
        }
        var config = agentService.getConfiguration(token, idConsultation, idPlatform);
        SuiviAvisPublie suivi = annonce.getSuiviList().stream().filter(suiviAvisPublie -> suiviAvisPublie.getAcheteur() != null).findFirst().orElse(null);
        if (suivi == null || suivi.getAcheteur() == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucun acheteur trouvé pour l'id " + id);
        }
        SuiviDTO suiviDTO = null;
        String xmlBase64 = annonce.getXml();
        TedEsenders tedEsenders = null;
        if (StringUtils.hasText(xmlBase64)) {
            try (ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64.decodeBase64(xmlBase64.getBytes()))) {
                JAXBContext jaxbContextTed = JAXBContext.newInstance(TedEsenders.class);
                Unmarshaller jaxbUnmarshaller = jaxbContextTed.createUnmarshaller();
                JAXBElement<TedEsenders> unmarshal = (JAXBElement<TedEsenders>) jaxbUnmarshaller.unmarshal(inputStream);
                tedEsenders = unmarshal.getValue();

            } catch (Exception e) {
                log.error("Erreur lors de la récupération du xml transaction : {}", e.getMessage());
            }
        }
        if ("TNCP".equals(annonce.getSupport().getCodeGroupe())) {
            suiviDTO = tncpService.envoyer(config, suivi, tedEsenders, token, rie);
        } else if ("MOL".equals(annonce.getSupport().getCodeGroupe())) {
            suiviDTO = molService.envoyer(config, suivi, tedEsenders, token, rie);
        } else {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucun service d'envoi trouvé pour le support " + annonce.getSupport().getCodeGroupe());
        }
        if (!StatutSuiviAnnonceEnum.PUBLIER.equals(annonce.getStatut()) && !StatutSuiviAnnonceEnum.REJETER_SUPPORT.equals(annonce.getStatut())) {
            annonce.setStatut(StatutSuiviAnnonceEnum.PREPARATION_PUBLICATION);
            annonce.setDateEnvoi(Instant.now());
            repository.save(annonce);
        }
        return suiviDTO;
    }

    @Override
    public Boolean updateSimplifie(Integer id, ConsultationSimplifieDTO simplifieDTO) {
        var annonce = repository.findById(id).orElseThrow(() -> new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucun suivi d'annonce trouvé pour l'id " + id));
        if (!annonce.getIdPlatform().equals(simplifieDTO.getIdPlatform()))
            throw new AtexoException(ExceptionEnum.NOT_AUTHORIZED, "Annonce hors périmètre pour l'id " + id);
        annonce.setSimplifie(simplifieDTO.isSimplifie());
        repository.save(annonce);
        return true;
    }

    @Override
    public SuiviDTO getRedirection(Integer id) {
        var annonce = repository.findById(id).orElseThrow(() -> new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucun suivi d'annonce trouvé pour l'id " + id));
        SuiviAvisPublie suivi = annonce.getSuiviList().stream().filter(suiviAvisPublie -> suiviAvisPublie.getAcheteur() != null).findFirst().orElse(null);
        return suiviAvisPublieMapper.mapToModel(suivi);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SuiviAnnonce change(SuiviAnnonce suiviAnnonce, SuiviTypeAvisPub avisPub, String token) {
        EFormsFormulaireEntity formulaire = suiviAnnonce.getFormulaire();

        SuiviAnnonce cloneAnnonce = SuiviAnnonce.builder().avisFichier(pieceJointeService.clone(suiviAnnonce.getAvisFichier())).avisFichierSimplifie(pieceJointeService.clone(suiviAnnonce.getAvisFichierSimplifie())).service(suiviAnnonce.getService()).offre(suiviAnnonce.getOffre()).xml(suiviAnnonce.getXml()).support(suiviAnnonce.getSupport()).typeAvisPub(avisPub).idPlatform(suiviAnnonce.getIdPlatform()).idConsultation(suiviAnnonce.getIdConsultation()).statut(StatutSuiviAnnonceEnum.BROUILLON).simplifie(suiviAnnonce.isSimplifie()).organismeMpe(suiviAnnonce.getOrganismeMpe()).procedure(suiviAnnonce.getProcedure()).xmlTransform(suiviAnnonce.getXmlTransform()).xmlConcentrateur(suiviAnnonce.getXmlConcentrateur()).parent(suiviAnnonce).build();
        cloneAnnonce = repository.save(cloneAnnonce);
        if (formulaire != null) {
            ieFormsNoticeServices.changeEFormsFormulaire(formulaire, cloneAnnonce, token);
        }

        return cloneAnnonce;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SuiviAnnonce reprendre(SuiviAnnonce suiviAnnonce, SuiviTypeAvisPub avisPub, String token) {
        EFormsFormulaireEntity formulaire = suiviAnnonce.getFormulaire();

        SuiviAnnonce cloneAnnonce = SuiviAnnonce.builder().avisFichier(pieceJointeService.clone(suiviAnnonce.getAvisFichier())).avisFichierSimplifie(pieceJointeService.clone(suiviAnnonce.getAvisFichierSimplifie())).service(suiviAnnonce.getService()).offre(suiviAnnonce.getOffre()).xml(suiviAnnonce.getXml()).support(suiviAnnonce.getSupport()).typeAvisPub(avisPub).idPlatform(suiviAnnonce.getIdPlatform()).idConsultation(suiviAnnonce.getIdConsultation()).statut(StatutSuiviAnnonceEnum.BROUILLON).simplifie(suiviAnnonce.isSimplifie()).organisme(suiviAnnonce.getOrganisme()).langue(suiviAnnonce.getLangue()).agent(agentService.findByToken(token)).organismeMpe(suiviAnnonce.getOrganismeMpe()).procedure(suiviAnnonce.getProcedure()).xmlTransform(suiviAnnonce.getXmlTransform()).xmlConcentrateur(suiviAnnonce.getXmlConcentrateur()).parent(suiviAnnonce).build();
        cloneAnnonce = repository.save(cloneAnnonce);
        if (formulaire != null) {
            ieFormsNoticeServices.reprendreEFormsFormulaire(formulaire, cloneAnnonce, token);
        }

        return cloneAnnonce;
    }

    @Override
    public void fixStatut(SuiviAnnonce suiviAnnonce, StatutSuiviAnnonceEnum statut) {
        suiviAnnonce.setStatut(statut);
        EFormsFormulaireEntity formulaire = suiviAnnonce.getFormulaire();
        StatutENoticeEnum statutENoticeFromSuiviAnnonce = StatutENoticeEnum.getStatutENoticeFromSuiviAnnonce(suiviAnnonce.getStatut());
        if (formulaire != null && formulaire.getStatut() != null && !formulaire.getStatut().equals(statutENoticeFromSuiviAnnonce)) {
            formulaire.setStatut(statutENoticeFromSuiviAnnonce);
            eFormsFormulaireRepository.save(formulaire);
        }
        repository.save(suiviAnnonce);
    }


    @Override
    public SuiviAnnonce addPieceJointe(Integer id, PieceJointe pieceJointe) {
        var annonce = repository.findById(id).orElseThrow(() -> new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucun suivi d'annonce trouvé pour l'id " + id));
        annonce.setAvisFichier(pieceJointe);
        return repository.save(annonce);
    }

    @Override
    public void synchronizeLesEchos() {

        Suivi response = lesechosWS.getSuivi();
        if (response == null || response.getAnnonces() == null || CollectionUtils.isEmpty(response.getAnnonces().getAnnonce())) {
            return;
        }
        response.getAnnonces().getAnnonce().forEach(annonce -> {
            String idAnnonceExterne = String.valueOf(annonce.getIdAnnonce());
            List<Suivi.Annonces.Annonce.SupportsPublication.SupportPublication> supportPublication = annonce.getSupportsPublication().getSupportPublication();
            if (CollectionUtils.isEmpty(supportPublication)) {
                return;
            }
            Suivi.Annonces.Annonce.SupportsPublication.SupportPublication.Statuts statuts = supportPublication.get(supportPublication.size() - 1).getStatuts();
            if (statuts == null || CollectionUtils.isEmpty(statuts.getStatut())) {
                return;
            }
            List<Suivi.Annonces.Annonce.SupportsPublication.SupportPublication.Statuts.Statut> list = statuts.getStatut();
            String etat = list.get(list.size() - 1).getEtat();
            XMLGregorianCalendar dateStatut = list.get(list.size() - 1).getDate();
            StatutSuiviAnnonceEnum annonceEnum = StatutSuiviAnnonceEnum.getStatutFromLesEchos(etat);
            SuiviAnnonce suiviAnnonce = repository.findByNumeroAvis(idAnnonceExterne);
            suiviAnnonce.setStatut(annonceEnum);
            suiviAnnonce.setMessageStatut(etat);
            Instant from = dateStatut.toGregorianCalendar().toInstant();
            if (suiviAnnonce.getDateSoumission() == null) {
                suiviAnnonce.setDateSoumission(from);
            }
            if (annonceEnum.equals(StatutSuiviAnnonceEnum.PUBLIER)) {
                suiviAnnonce.setDatePublication(from);
            }
            repository.save(suiviAnnonce);
        });

    }

    @Override
    public EditionRequest modifyAvisDocument(int id, String idPlatform, Integer idConsultation, String token, boolean simplifie) {
        SuiviAnnonce suiviAnnonce = getSuiviAnnonceEntity(Math.toIntExact(id));
        var agent = agentService.findByToken(token);
        if (agent == null) {
            throw new AtexoException(ExceptionEnum.FORBIDDEN, "Agent non trouvé");
        }

        PieceJointe document;
        if (simplifie) {
            document = suiviAnnonce.getAvisFichierSimplifie();
        } else {
            document = suiviAnnonce.getAvisFichier();
        }
        if (document == null || !espaceDocumentaireService.fileExists(document)) {
            PieceJointe pieceJointe = initAvisDocument(suiviAnnonce, simplifie);
            if (simplifie) {
                suiviAnnonce.setAvisFichierSimplifie(pieceJointe);
            } else {
                suiviAnnonce.setAvisFichier(pieceJointe);
            }
            suiviAnnonce = repository.save(suiviAnnonce);

        }
        return this.editDocument(agent, suiviAnnonce, token, simplifie);
    }

    @Override
    public SuiviAnnonceDTO reinitierAvisDocument(int id, String idPlatform, Integer idConsultation, String token, boolean simplifie) {
        SuiviAnnonce suiviAnnonce = getSuiviAnnonceEntity(Math.toIntExact(id));
        var agent = agentService.findByToken(token);
        if (agent == null) {
            throw new AtexoException(ExceptionEnum.FORBIDDEN, "Agent non trouvé");
        }

        PieceJointe document;
        if (simplifie) {
            document = suiviAnnonce.getAvisFichierSimplifie();
            suiviAnnonce.setAvisFichierSimplifie(null);
        } else {
            document = suiviAnnonce.getAvisFichier();
            suiviAnnonce.setAvisFichier(null);
        }
        if (document != null) {
            suiviAnnonce = repository.save(suiviAnnonce);
            pieceJointeService.deleteById(document.getId());
        }
        return suiviAnnonceMapper.suiviAnnonceToSuiviAnnonceDTO(suiviAnnonce);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SuiviAnnonceDTO reprendreById(Integer id, String idPlatform, Integer idConsultation, String bearer) {
        SuiviAnnonce suiviAnnonce = this.getSuiviAnnonceEntity(id);
        if (suiviAnnonce == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucun suivi d'annonce trouvé pour l'id " + id);
        }
        if (!idConsultation.equals(suiviAnnonce.getIdConsultation())) {
            throw new AtexoException(ExceptionEnum.NOT_AUTHORIZED, "Annonce hors périmètre pour l'id " + id);
        }
        if (!idPlatform.equals(suiviAnnonce.getIdPlatform())) {
            throw new AtexoException(ExceptionEnum.NOT_AUTHORIZED, "Annonce hors périmètre pour l'id " + id);
        }
        SuiviTypeAvisPub typeAvisPub = suiviAnnonce.getTypeAvisPub();
        if (typeAvisPub != null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "L'annonce ne peut pas être reprise car elle est déjà associée à un avis");
        }

        return suiviAnnonceMapper.suiviAnnonceToSuiviAnnonceDTOIgnoreOffres(this.reprendre(suiviAnnonce, typeAvisPub, bearer));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SuiviAnnonceDTO changeById(Integer id, String idPlatform, Integer idConsultation, String token) {
        SuiviAnnonce suiviAnnonce = this.getSuiviAnnonceEntity(id);
        if (suiviAnnonce == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucun suivi d'annonce trouvé pour l'id " + id);
        }
        if (!idConsultation.equals(suiviAnnonce.getIdConsultation())) {
            throw new AtexoException(ExceptionEnum.NOT_AUTHORIZED, "Annonce hors périmètre pour l'id " + id);
        }
        if (!idPlatform.equals(suiviAnnonce.getIdPlatform())) {
            throw new AtexoException(ExceptionEnum.NOT_AUTHORIZED, "Annonce hors périmètre pour l'id " + id);
        }
        SuiviTypeAvisPub typeAvisPub = suiviAnnonce.getTypeAvisPub();
        if (typeAvisPub != null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "L'annonce ne peut pas être reprise car elle est déjà associée à un avis");
        }

        return suiviAnnonceMapper.suiviAnnonceToSuiviAnnonceDTOIgnoreOffres(this.change(suiviAnnonce, null, token));
    }

    @Override
    public void suiviDocument(SuiviAnnonce suiviAnnonce) {
        if (suiviAnnonce.getTokenEdition() == null) {
            suiviAnnonce.setStatutEdition(FileStatusEnum.CLOSED_WITHOUT_EDITING);
            repository.save(suiviAnnonce);
            return;
        }
        try {
            FileStatus status = docGenWsPort.getDocumentStatus(suiviAnnonce.getTokenEdition());
            if (status == null || status.getStatus() == null) {
                suiviAnnonce.setStatutEdition(FileStatusEnum.CLOSED_WITHOUT_EDITING);
                repository.save(suiviAnnonce);
                return;
            }
            log.info("Passage du statut du document {} du statut {} au statut {}", suiviAnnonce.getId(), suiviAnnonce.getStatutEdition(), status.getStatus());

            updateDocumentEdition(status, suiviAnnonce, suiviAnnonce.isSimplifie());
        } catch (Exception e) {
            log.error("Erreur lors de la récupération du statut du document : {}", e.getMessage());
            suiviAnnonce.setStatutEdition(FileStatusEnum.CLOSED_WITHOUT_EDITING);
            repository.save(suiviAnnonce);
        }
    }

    @Override
    public void updateDocumentEdition(FileStatus status, SuiviAnnonce suiviAnnonce, boolean isSimplifie) {

        suiviAnnonce.setStatutEdition(status.getStatus());
        suiviAnnonce.setVersionEdition(status.getVersion());
        FileRedacStatusEnum fileRedacStatusEnum = FileRedacStatusEnum.fromValue(status.getRedacStatus());
        suiviAnnonce.setStatutRedaction(fileRedacStatusEnum.name());

        int id = suiviAnnonce.getId();
        if (suiviAnnonce.getDateValidation() == null && fileRedacStatusEnum.equals(FileRedacStatusEnum.VALIDER)) {
            log.info("Date de validation du l'annonce {} : {}", id, status.getModificationDate());
            Timestamp modificationDate = status.getModificationDate();
            if (modificationDate != null) {
                Instant instant = Instant.ofEpochSecond(modificationDate.getTime());
                String timeZoneId = "Europe/Paris";
                ZonedDateTime zonedDateTime = instant.atZone(ZoneId.of(timeZoneId));
                suiviAnnonce.setDateValidation(zonedDateTime);
                suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.COMPLET);
            } else {
                suiviAnnonce.setDateValidation(ZonedDateTime.now());
                suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.COMPLET);
            }
        } else if (suiviAnnonce.getDateValidation() != null) {
            log.info("Date de invalidation du l'annonce {} ", id);
            suiviAnnonce.setDateValidation(null);
            suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.BROUILLON);
        }

        PieceJointe document = isSimplifie ? suiviAnnonce.getAvisFichierSimplifie() : suiviAnnonce.getAvisFichier();
        PieceJointe savedPieceJointe = espaceDocumentaireService.getPieceJointeFromDocgenEdit(document, suiviAnnonce.getTokenEdition(), "annonce-" + suiviAnnonce.getId());
        if (savedPieceJointe != null) {
            if (isSimplifie) {
                suiviAnnonce.setAvisFichierSimplifie(savedPieceJointe);
            } else {
                suiviAnnonce.setAvisFichier(savedPieceJointe);
            }
        }

        repository.save(suiviAnnonce);
    }

    @Override
    public TrackDocumentResponse getDocumentCallback(int id, boolean simplifie, FileStatus status) {
        log.info("Réception du statut {} de l'annonce {}", status.getStatus(), id);
        SuiviAnnonce suiviAnnonce = repository.findById(id).orElse(null);
        if (suiviAnnonce == null) {
            return TrackDocumentResponse.builder().error("1").build();
        }
        try {

            this.updateDocumentEdition(status, suiviAnnonce, simplifie);
        } catch (Exception e) {
            log.error("Erreur lors de la mise à jour du statut du document : {}", e.getMessage());
            return TrackDocumentResponse.builder().error("1").build();
        }
        return TrackDocumentResponse.builder().error("0").build();
    }

    private PieceJointe initAvisDocument(SuiviAnnonce annonce, boolean simplifie) {
        if (annonce == null) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucune annonce associée à cette avis");
        }

        String xmlBase64 = annonce.getXml();
        if (StringUtils.hasText(xmlBase64)) {
            String xml = new String(Base64.decodeBase64(xmlBase64));
            try (ByteArrayInputStream inputStream = new ByteArrayInputStream(xml.getBytes())) {
                JAXBContext jaxbContext = JAXBContext.newInstance(TedEsenders.class);
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                JAXBElement<TedEsenders> unmarshal = (JAXBElement<TedEsenders>) jaxbUnmarshaller.unmarshal(inputStream);
                TedEsenders tedEsenders = unmarshal.getValue();
                if (tedEsenders != null && tedEsenders.getFORMSECTION() != null && tedEsenders.getFORMSECTION().getF052014() != null && tedEsenders.getSENDER() != null) {

                    List<KeyValueRequest> map = new ArrayList<>();
                    map.add(KeyValueRequest.builder().key("forms").value(tedEsenders.getFORMSECTION().getF052014()).build());
                    map.add(KeyValueRequest.builder().key("sender").value(tedEsenders.getSENDER()).build());
                    ConfigurationConsultationDTO configurationByPlateforme = agentService.getConfigurationByPlateforme(annonce.getIdConsultation(), annonce.getOrganismeMpe().getPlateformeUuid());
                    if (configurationByPlateforme != null) {
                        ConsultationContexte contexte = configurationByPlateforme.getContexte();
                        map.add(KeyValueRequest.builder().key("ctx").value(contexte).build());
                    }

                    Offre offre = annonce.getOffre();
                    map.add(KeyValueRequest.builder().key("offre").value(offreMapper.offreToOffreDTO(offre)).build());

                    File template = espaceDocumentaireService.downloadAvisTemplate(offre, simplifie);
                    String libelle = offre.getLibelle();
                    return espaceDocumentaireService.getPieceJointe(libelle, map, template, offre.getDocumentExtension());
                }
            } catch (Exception e) {
                log.error("Erreur lors de la récupération du formulaire : {}", e.getMessage());
                throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, "Erreur lors de la récupération du formulaire : " + e.getMessage());
            }
        }

        return null;
    }

    public EditionRequest editDocument(Agent agent, SuiviAnnonce suiviAnnonce, String token, boolean simplifie) {

        String endpoint = "/concentrateur-annonces/rest/v2/annonces/" + suiviAnnonce.getId() + "/editeur-en-ligne/callback?simplifie=" + simplifie;
        String callback = agent.getOrganisme().getPlateformeUrl() + endpoint;
        this.getDocumentWithEditionToken(agent, callback, suiviAnnonce, token, simplifie);
        suiviAnnonce = repository.save(suiviAnnonce);
        return espaceDocumentaireService.getUrlEdition(suiviAnnonce.getTokenEdition());
    }

    private void getDocumentWithEditionToken(Agent agent, String callback, SuiviAnnonce annonce, String token, boolean simplifie) {
        PieceJointe document;
        if (simplifie) {
            document = annonce.getAvisFichierSimplifie();
        } else {
            document = annonce.getAvisFichier();
        }
        int id = annonce.getId();
        String editionToken = espaceDocumentaireService.getEditionToken(agent, "concentrateur@annonce-", callback, String.valueOf(id), token, document);
        annonce.setTokenEdition(editionToken);
        annonce.setStatutEdition(FileStatusEnum.REQUEST_TO_OPEN);
    }


    @Override
    protected Logger getLogger() {
        return log;
    }


    public void updateSuiviAnnonceNational(Message<?> message) {
        Map<String, Object> headers = message.getHeaders();
        synchronized (lock) {
            int idAnnonce = (Integer) headers.get("idAnnonce");
            SuiviAnnonce suiviAnnonce = repository.findById(idAnnonce).orElseThrow(() -> new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucun suivi d'annonce trouvé pour l'id " + idAnnonce));
            String codeDestination = (String) headers.get("codeDestination");
            SuiviTypeAvisPub typeAvisPub = suiviAnnonce.getTypeAvisPub();
            StatutSuiviAnnonceEnum statut;
            if (typeAvisPub == null) {
                statut = StatutSuiviAnnonceEnum.EN_COURS_DE_PUBLICATION;
            } else {
                statut = StatutSuiviAnnonceEnum.PUBLIER;
                suiviAnnonce.setDatePublication(ZonedDateTime.now().toInstant());
            }
            suiviAnnonce.setStatut(statut);
            suiviAnnonce.setMessageStatut("Les mails ont été envoyés aux contacts " + codeDestination);
            ZonedDateTime now = ZonedDateTime.now();
            suiviAnnonce.setDateEnvoi(now.toInstant());
            repository.save(suiviAnnonce);
            if (typeAvisPub != null) {
                List<StatutSuiviAnnonceEnum> statuts = new ArrayList<>(typeAvisPub.getAnnonces().stream().filter(annonce -> annonce.getId() != idAnnonce).map(SuiviAnnonce::getStatut).distinct().toList());
                statuts.add(statut);
                typeAvisPub.setStatut(StatutTypeAvisAnnonceEnum.getStatutFromAnnonceEnum(statuts));
                typeAvisPub.setDateModificationStatut(now);
                typeAvisPub.setDateEnvoiNational(now);
                typeAvisPub.setRaisonRefus(null);
                suiviTypeAvisPubRepository.save(typeAvisPub);
            }
        }
    }

    public void updateSuiviAnnonceEuropeen(SuiviAnnonce suiviAnnonce, String codeDestination) {
        synchronized (lock) {
            suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.EN_COURS_DE_PUBLICATION);
            suiviAnnonce.setMessageStatut("L'avis a été envoyé au " + codeDestination);
            ZonedDateTime now = ZonedDateTime.now();
            suiviAnnonce.setDateEnvoi(now.toInstant());
            suiviAnnonce = repository.save(suiviAnnonce);
            SuiviTypeAvisPub typeAvisPub = suiviAnnonce.getTypeAvisPub();
            if (typeAvisPub != null) {
                int id = suiviAnnonce.getId();
                List<StatutSuiviAnnonceEnum> statuts = new ArrayList<>(typeAvisPub.getAnnonces().stream().filter(annonce -> annonce.getId() != id).map(SuiviAnnonce::getStatut).distinct().toList());
                statuts.add(StatutSuiviAnnonceEnum.EN_COURS_DE_PUBLICATION);
                typeAvisPub.setStatut(StatutTypeAvisAnnonceEnum.getStatutFromAnnonceEnum(statuts));
                typeAvisPub.setDateModificationStatut(now);
                typeAvisPub.setDateEnvoiEuropeen(now);
                typeAvisPub.setRaisonRefus(null);
                suiviTypeAvisPubRepository.save(typeAvisPub);
            }
        }
    }

    public void resetSuiviAnnonceEuropeen(SuiviAnnonce suiviAnnonce) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.RETRY);
        String message = "<ul> Essai : " + suiviAnnonce.getTentative() + 1 + "</br>" + "<li> Horaire : " + formatter.format(ZonedDateTime.now()) + "</li>" + "<li> Détail message : Erreur lors de l'envoi au TED</li>" + "</ul>";
        String messageErreur = suiviAnnonce.getMessageErreur() == null ? message : suiviAnnonce.getMessageErreur() + " " + message;
        suiviAnnonce.setMessageErreur(messageErreur);
        repository.save(suiviAnnonce);

    }

}
