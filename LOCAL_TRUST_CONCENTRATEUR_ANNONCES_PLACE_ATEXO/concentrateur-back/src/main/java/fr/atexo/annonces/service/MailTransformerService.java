package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Header;

/**
 * Transformer permettant de construire un MailMessage à partir des informations d'un objet
 */
public interface MailTransformerService {

    MimeMailMessage sendMail(Message<?> message) throws Exception;

    String getSubject(SuiviAnnonce suiviAnnonce, @Header String libelleSupport) throws Exception;
}
