package fr.atexo.annonces.dto.eforms;

import com.fasterxml.jackson.annotation.JsonInclude;
import fr.atexo.annonces.modeles.StatutENoticeEnum;
import lombok.*;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EsentoolNoticeDTO {
    private String id;
    private String versionId;
    private String procedureId;
    private StatutENoticeEnum status;
    private ZonedDateTime updatedAt;
    private ZonedDateTime submittedAt;
    private String publicationId;
    private Date publicationDate;
    private String noticeType;
    private String legalBasis;
    private List<String> languages;
}
