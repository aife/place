package fr.atexo.annonces.boot.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter
@Getter
@Entity
@EqualsAndHashCode
@Table(name = "organisme")
public class Organisme {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "acronyme_organisme")
    private String acronymeOrganisme;

    @Column(name = "sigle_organisme")
    private String sigleOrganisme;

    @Column(name = "libelle_organisme")
    private String libelleOrganisme;

    @Column(name = "plateforme_uuid")
    private String plateformeUuid;

    @Column(name = "plateforme_url")
    private String plateformeUrl;

    @OneToMany(mappedBy = "organisme", fetch = FetchType.LAZY)
    private List<ReferentielServiceMpe> services;

    @OneToMany(mappedBy = "organisme", fetch = FetchType.LAZY)
    private List<AcheteurEntity> acheteurs;

}
