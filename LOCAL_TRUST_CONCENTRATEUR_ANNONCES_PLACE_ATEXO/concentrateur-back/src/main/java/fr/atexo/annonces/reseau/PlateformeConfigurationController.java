package fr.atexo.annonces.reseau;

import fr.atexo.annonces.boot.core.domain.plateforme.services.IPlateformeConfigurationService;
import fr.atexo.annonces.dto.PlateformeConfigurationDTO;
import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/rest/v2/plateformes-configuration")
@Tag(name = "PlateformeConfigurationController")
@Slf4j
public class PlateformeConfigurationController {

    @Value("${annonces.admin.plateforme}")
    private String plateformeAdministrateur;
    private final IPlateformeConfigurationService iPlateformeConfigurationService;

    public PlateformeConfigurationController(IPlateformeConfigurationService iPlateformeConfigurationService) {
        this.iPlateformeConfigurationService = iPlateformeConfigurationService;
    }


    @GetMapping
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform)")
    public List<PlateformeConfigurationDTO> get(@RequestParam String idPlatform) {
        if (!plateformeAdministrateur.equals(idPlatform)) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Vous n'êtes pas autorisé à consulter cette plateforme");
        }
        return iPlateformeConfigurationService.getPlateformes();
    }

    @GetMapping("{id}")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform)")
    public PlateformeConfigurationDTO get(@PathVariable Long id, @RequestParam String idPlatform) {
        if (!plateformeAdministrateur.equals(idPlatform)) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Vous n'êtes pas autorisé à consulter cette plateforme");
        }
        return iPlateformeConfigurationService.getPlateforme(id);
    }

    @PostMapping
    public PlateformeConfigurationDTO addOrUpdatePlateformConfiguration(@RequestBody PlateformeConfigurationDTO plateforme) {
        return iPlateformeConfigurationService.addOrUpdatePlateformConfiguration(plateforme);
    }

    @DeleteMapping("{id}")
    public boolean delete(@PathVariable Long id) {
        iPlateformeConfigurationService.delete(id);
        return true;
    }

    @PutMapping("{id}")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform)")
    public PlateformeConfigurationDTO put(@PathVariable Long id, @RequestParam String idPlatform, @RequestBody PlateformeConfigurationDTO configuration) {
        if (!plateformeAdministrateur.equals(idPlatform)) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Vous n'êtes pas autorisé à consulter cette plateforme");
        }
        return iPlateformeConfigurationService.put(id, configuration);
    }


    @PatchMapping("import-xlsx")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform)")
    public boolean importAtexoAssociationXls(
            @RequestPart @NotEmpty(message = "le fichier ne doit pas être vide") @Valid MultipartFile xlsx,
            @RequestParam String idPlatform) {
        if (!plateformeAdministrateur.equals(idPlatform)) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Vous n'êtes pas autorisé à consulter cette plateforme");
        }
        return iPlateformeConfigurationService.patchAssociationProcedureAvisOrganisme(null, xlsx);
    }

    @GetMapping("export-xlsx")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform)")
    public ResponseEntity<InputStreamResource> getAtexoAssociationXls(@RequestParam String idPlatform) throws IOException {
        if (!plateformeAdministrateur.equals(idPlatform)) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Vous n'êtes pas autorisé à consulter cette plateforme");
        }
        ByteArrayOutputStream file = iPlateformeConfigurationService.exportConfiguration(null);
        Resource resource = new ByteArrayResource(file.toByteArray());
        return ResponseEntity.ok()
                .contentType(MediaType.valueOf("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=ref-achat.xlsx")
                .body(new InputStreamResource(resource.getInputStream()));
    }

    @PatchMapping("/{id}/import-xlsx")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform)")
    public boolean importAssociationXls(@PathVariable Long id,
                                        @RequestPart @NotEmpty(message = "le fichier ne doit pas être vide") @Valid MultipartFile xlsx,
                                        @RequestParam String idPlatform) {
        if (!plateformeAdministrateur.equals(idPlatform)) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Vous n'êtes pas autorisé à consulter cette plateforme");
        }
        return iPlateformeConfigurationService.patchAssociationProcedureAvisOrganisme(id, xlsx);
    }

    @GetMapping("/{id}/export-xlsx")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform)")
    public ResponseEntity<InputStreamResource> getAssociationXls(@PathVariable Long id, @RequestParam String idPlatform) throws IOException {
        if (!plateformeAdministrateur.equals(idPlatform)) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Vous n'êtes pas autorisé à consulter cette plateforme");
        }
        ByteArrayOutputStream file = iPlateformeConfigurationService.exportConfiguration(id);
        Resource resource = new ByteArrayResource(file.toByteArray());
        return ResponseEntity.ok()
                .contentType(MediaType.valueOf("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=ref-achat.xlsx")
                .body(new InputStreamResource(resource.getInputStream()));
    }


}
