package fr.atexo.annonces.service.exception;

import java.text.MessageFormat;

/**
 * Exception levée si un quelconque problème survient lors de l'export d'un objet vers un fichier PDF
 */
public class PdfExportException extends RuntimeException {

    private static final long serialVersionUID = -4980999501990393657L;
    private static final String EXPORT_FAILURE_MESSAGE = "Erreur(s) lors de la génération du PDF pour la ressource ayant pour {0} {1} :";

    public PdfExportException(Throwable cause, String identifierProperty, String identifierValue) {
        super(MessageFormat.format(EXPORT_FAILURE_MESSAGE, identifierProperty, identifierValue), cause);
    }

    public PdfExportException(String identifierProperty, String identifierValue) {
        super(MessageFormat.format(EXPORT_FAILURE_MESSAGE, identifierProperty, identifierValue));
    }
}
