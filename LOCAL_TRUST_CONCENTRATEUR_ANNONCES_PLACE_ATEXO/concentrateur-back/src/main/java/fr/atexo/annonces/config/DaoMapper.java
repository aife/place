package fr.atexo.annonces.config;

import fr.atexo.annonces.dto.PageDTO;
import org.springframework.data.domain.Page;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import static java.util.Collections.emptyList;
import static org.springframework.util.CollectionUtils.isEmpty;

public interface DaoMapper<E extends Serializable, M extends Serializable> {

    E mapToEntity(M model);

    M mapToModel(E entity);

    default List<E> mapToEntities(Collection<M> models) {
        if (isEmpty(models)) {
            return emptyList();
        }
        return models.stream()
                .map(this::mapToEntity)
                .toList();
    }

    default List<M> mapToModels(Collection<E> entities) {
        if (isEmpty(entities)) {
            return emptyList();
        }
        return entities.stream()
                .map(this::mapToModel)
                .toList();
    }

    default PageDTO<M> mapToPageModel(Page<E> responses) {
        if (Objects.isNull(responses)) {
            return null;
        }
        return PageDTO.<M>builder()
                .content(this.mapToModels(responses.getContent()))
                .first(responses.isFirst())
                .last(responses.isLast())
                .numberOfElements(responses.getNumberOfElements())
                .size(responses.getSize())
                .totalElements(responses.getTotalElements())
                .totalPages(responses.getTotalPages())
                .build();
    }

    default PageDTO<M> mapToPageModelIgnoreContent(Page<E> responses) {
        if (Objects.isNull(responses)) {
            return null;
        }
        return PageDTO.<M>builder()
                .content(new ArrayList<>())
                .first(responses.isFirst())
                .last(responses.isLast())
                .numberOfElements(responses.getNumberOfElements())
                .size(responses.getSize())
                .totalElements(responses.getTotalElements())
                .totalPages(responses.getTotalPages())
                .build();
    }
}
