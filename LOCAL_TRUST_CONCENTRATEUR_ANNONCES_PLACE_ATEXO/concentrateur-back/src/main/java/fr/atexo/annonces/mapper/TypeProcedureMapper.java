package fr.atexo.annonces.mapper;

import fr.atexo.annonces.boot.entity.TypeProcedure;
import fr.atexo.annonces.config.DaoMapper;
import fr.atexo.annonces.dto.ReferentielDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


@Mapper
public interface TypeProcedureMapper extends DaoMapper<TypeProcedure, ReferentielDTO> {

    @Override
    @Mapping(target = "code", source = "abbreviation")
    ReferentielDTO mapToModel(TypeProcedure entity);


}
