package fr.atexo.annonces.boot.core.domain.e_forms_notice.services.impl;

import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.PreviousNoticeFormatHandler;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.FieldDetails;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNotice;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNoticeMetadata;
import fr.atexo.annonces.boot.entity.EFormsFormulaireEntity;
import fr.atexo.annonces.boot.repository.EformsSurchargeRepository;
import fr.atexo.annonces.boot.ws.eforms.SchematronConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

@Slf4j
public class ChangeRootFormatHandler extends GenericSdkService implements PreviousNoticeFormatHandler {


    public ChangeRootFormatHandler(EformsSurchargeRepository eformsSurchargeRepository, SchematronConfiguration schematronConfiguration) {
        super(eformsSurchargeRepository, schematronConfiguration);
    }


    @Override
    public Object processGroup(SubNotice notice, Object formulaireObjet, SubNoticeMetadata group, EFormsFormulaireEntity previousFormulaire, EFormsFormulaireEntity newFormulaire, String nodeStop, Map<String, List<PreviousNoticeFormatHandler>> kownHandlers, boolean first) {
        for (SubNoticeMetadata item : group.getContent()) {
            String nodeId = (item.is_repeatable() || item.get_identifierFieldId() != null) ? item.getNodeId() : null;
            List<PreviousNoticeFormatHandler> handler = kownHandlers.get(nodeId);
            String versionSdk = newFormulaire.getVersionSdk();
            if (handler != null) {
                for (PreviousNoticeFormatHandler noticeFormatHandler : handler) {
                    Object value = noticeFormatHandler.processGroup(notice, new Object(), item, previousFormulaire, newFormulaire, nodeId, kownHandlers, false);
                    formulaireObjet = addToObject(formulaireObjet, null, value, nodeStop, nodeId, versionSdk);
                }
            } else if (nodeId != null) {
                if (("field".equals(item.getContentType()))) {
                    formulaireObjet = processElement(item, notice, formulaireObjet, item.getId(), previousFormulaire, newFormulaire, nodeStop);
                }
                if (!CollectionUtils.isEmpty(item.getContent())) {
                    Object o = processGroup(notice, new Object(), item, previousFormulaire, newFormulaire, nodeId, kownHandlers, false);
                    formulaireObjet = addToObject(formulaireObjet, null, o, nodeStop, nodeId, versionSdk);
                }
            } else {
                if (("field".equals(item.getContentType()))) {
                    formulaireObjet = processElement(item, notice, formulaireObjet, item.getId(), previousFormulaire, newFormulaire, nodeStop);
                }
                if (!CollectionUtils.isEmpty(item.getContent())) {
                    formulaireObjet = processGroup(notice, formulaireObjet, item, previousFormulaire, newFormulaire, nodeStop, kownHandlers, false);
                }
            }
        }
        return first ? deleteObject(formulaireObjet) : formulaireObjet;
    }

    @Override
    public Object processElement(SubNoticeMetadata item, SubNotice notice, Object formulaireObjet, String id, EFormsFormulaireEntity previousFormulaire, EFormsFormulaireEntity newFormulaire, String nodeStop) {
        String versionSdk = newFormulaire.getVersionSdk();
        FieldDetails fieldDetails = getFields(versionSdk).getFields().stream().filter(fieldDetails1 -> fieldDetails1.getId().equals(id)).findFirst().orElse(null);
        if (fieldDetails == null || getMandatoryForbiddenDetailsValue(newFormulaire.getIdNotice(), fieldDetails.getForbidden())) {
            return formulaireObjet;
        }
        String parentNodeId = fieldDetails.getParentNodeId();
        boolean repeatable = item.is_repeatable();
        switch (id) {
            case "BT-757-notice" -> {
                Integer noticeVersion = newFormulaire.getNoticeVersion();
                String value = String.valueOf(noticeVersion);
                if (noticeVersion < 10) {
                    value = "0" + value;
                }
                return addToObject(formulaireObjet, id, repeatable ? List.of(value) : value, nodeStop, parentNodeId, versionSdk);
            }
            case "OPT-001-notice" -> {
                String ublVersion = getSubNoticeList(versionSdk).getUblVersion();
                return addToObject(formulaireObjet, id, repeatable ? List.of(ublVersion) : ublVersion, nodeStop, parentNodeId, versionSdk);
            }
            case "OPT-002-notice" -> {

                String[] sdkVersion = getFields(versionSdk).getSdkVersion().split("\\.");
                String value = sdkVersion[0] + "." + sdkVersion[1];
                return addToObject(formulaireObjet, id, repeatable ? List.of(value) : value, nodeStop, parentNodeId, versionSdk);
            }
            case "BT-701-notice" -> {

                String uuid = newFormulaire.getUuid();
                return addToObject(formulaireObjet, id, repeatable ? List.of(uuid) : uuid, nodeStop, parentNodeId, versionSdk);
            }
            case "BT-05(a)-notice" -> {
                ZonedDateTime dateCreation = ZonedDateTime.now();
                Object date = addToObject(new Object(), "type", "Europe/Paris", null, null, versionSdk);
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                String formatted = dateCreation.format(formatter);
                date = addToObject(date, "value", formatted, null, null, versionSdk);

                return addToObject(formulaireObjet, id, repeatable ? List.of(date) : date, nodeStop, parentNodeId, versionSdk);
            }
            case "BT-05(b)-notice" -> {
                ZonedDateTime dateCreation = ZonedDateTime.now();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
                Object date = addToObject(new Object(), "type", "Europe/Paris", null, null, versionSdk);
                String formatted = dateCreation.format(formatter);
                date = addToObject(date, "value", formatted, null, null, versionSdk);
                return addToObject(formulaireObjet, id, repeatable ? List.of(date) : date, nodeStop, parentNodeId, versionSdk);
            }

            default -> log.debug("Pas de correspondance pour {}", id);

        }

        return formulaireObjet;
    }
}
