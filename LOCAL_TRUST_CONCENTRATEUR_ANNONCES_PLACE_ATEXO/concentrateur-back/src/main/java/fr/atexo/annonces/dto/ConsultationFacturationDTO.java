package fr.atexo.annonces.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;


@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode(of = {"adresse", "mail", "sip"})
public class ConsultationFacturationDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    @JsonProperty("address")
    private String adresse;

    @JsonProperty("email")
    private String mail;
    private boolean sip;
}
