package fr.atexo.annonces.modeles;

import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.ZonedDateTime;
import java.util.Set;

@AllArgsConstructor
@Getter
@Setter
@Builder
public class SuiviAnnonceSpecification implements Specification<SuiviAnnonce> {

    private static final long serialVersionUID = 1222004680882504439L;

    private Set<String> plateformes;
    private Set<StatutSuiviAnnonceEnum> statuts;
    private ZonedDateTime dateModificationMax;
    private ZonedDateTime dateModificationMin;


    @Override
    public Predicate toPredicate(Root<SuiviAnnonce> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        criteriaQuery.distinct(true);
        Predicate predicate = criteriaBuilder.conjunction();

        if (!CollectionUtils.isEmpty(statuts)) {
            predicate = addStatutFilter(root, criteriaBuilder, predicate, this.statuts);
        }


        if (dateModificationMin != null) {
            predicate = addDateModificationLessThenFilter(root, criteriaBuilder, predicate);
        }
        if (dateModificationMax != null) {
            predicate = addDateModificationGreaterThenFilter(root, criteriaBuilder, predicate);
        }
        return predicate;
    }

    private Predicate addInFilter(Root<SuiviAnnonce> root, CriteriaBuilder criteriaBuilder,
                                  Predicate predicate, String key, Object value) {
        Predicate paysPredict = criteriaBuilder.disjunction();
        paysPredict.getExpressions().add(root.get(key).in(value));
        return criteriaBuilder.and(predicate, paysPredict);
    }


    private Predicate addStatutFilter(Root<SuiviAnnonce> root, CriteriaBuilder criteriaBuilder,
                                      Predicate predicate, Set<StatutSuiviAnnonceEnum> statuts) {
        return criteriaBuilder.and(predicate, addInFilter(root, criteriaBuilder, predicate, "statut", statuts));
    }

    private Predicate addDateModificationLessThenFilter(Root<SuiviAnnonce> root, CriteriaBuilder criteriaBuilder,
                                                        Predicate predicate) {

        Predicate datePredict = criteriaBuilder.disjunction();
        datePredict.getExpressions().add(criteriaBuilder.lessThanOrEqualTo(root.get("dateModification"), dateModificationMax));
        return criteriaBuilder.and(predicate, datePredict);
    }

    private Predicate addDateModificationGreaterThenFilter(Root<SuiviAnnonce> root, CriteriaBuilder criteriaBuilder,
                                                           Predicate predicate) {

        Predicate datePredict = criteriaBuilder.disjunction();
        datePredict.getExpressions().add(criteriaBuilder.greaterThanOrEqualTo(root.get("dateModification"), dateModificationMin));
        return criteriaBuilder.and(predicate, datePredict);
    }


}
