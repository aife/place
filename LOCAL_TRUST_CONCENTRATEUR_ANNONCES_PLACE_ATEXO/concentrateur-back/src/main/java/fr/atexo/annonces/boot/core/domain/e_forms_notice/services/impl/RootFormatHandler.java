package fr.atexo.annonces.boot.core.domain.e_forms_notice.services.impl;

import com.atexo.annonces.commun.mpe.*;
import com.fasterxml.jackson.databind.JsonNode;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.NoticeFormatHandler;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.FieldDetails;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNotice;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNoticeMetadata;
import fr.atexo.annonces.boot.entity.EFormsFormulaireEntity;
import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.boot.entity.TypeProcedure;
import fr.atexo.annonces.boot.repository.EformsSurchargeRepository;
import fr.atexo.annonces.boot.ws.eforms.SchematronConfiguration;
import fr.atexo.annonces.boot.ws.mpe.model.ConsultationContexte;
import fr.atexo.annonces.config.EFormsHelper;
import fr.atexo.annonces.dto.ConfigurationConsultationDTO;
import fr.atexo.annonces.dto.EformsSurchargeDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static fr.atexo.annonces.config.EFormsHelper.valueToTree;

@Slf4j
public class RootFormatHandler extends GenericSdkService implements NoticeFormatHandler {


    public RootFormatHandler(EformsSurchargeRepository eformsSurchargeRepository, SchematronConfiguration schematronConfiguration) {
        super(eformsSurchargeRepository, schematronConfiguration);
    }

    @Override
    public Object processGroup(SubNotice notice, ConfigurationConsultationDTO contexte, Object formulaireObjet, SubNoticeMetadata group, EFormsFormulaireEntity saved, String nodeStop, Map<String, List<NoticeFormatHandler>> kownHandlers) {
        for (SubNoticeMetadata item : group.getContent()) {
            String nodeId = (item.is_repeatable() || item.get_identifierFieldId() != null) ? item.getNodeId() : null;

            List<NoticeFormatHandler> handler = kownHandlers.get(nodeId);
            if (handler != null) {

                for (NoticeFormatHandler noticeFormatHandler : handler) {
                    Object value = noticeFormatHandler.processGroup(notice, contexte, new Object(), item, saved, nodeId, kownHandlers);
                    formulaireObjet = addToObject(formulaireObjet, null, value, nodeStop, nodeId, saved.getVersionSdk());
                }
            } else if (nodeId != null) {
                if (("field".equals(item.getContentType()))) {
                    formulaireObjet = processElement(item, notice, contexte, formulaireObjet, item.getId(), saved, nodeStop);
                }
                if (!CollectionUtils.isEmpty(item.getContent())) {
                    Object o = processGroup(notice, contexte, new Object(), item, saved, nodeId, kownHandlers);
                    formulaireObjet = addToObject(formulaireObjet, null, o, nodeStop, nodeId, saved.getVersionSdk());
                }
            } else {
                if (("field".equals(item.getContentType()))) {
                    formulaireObjet = processElement(item, notice, contexte, formulaireObjet, item.getId(), saved, nodeStop);
                }
                if (!CollectionUtils.isEmpty(item.getContent())) {
                    formulaireObjet = processGroup(notice, contexte, formulaireObjet, item, saved, nodeStop, kownHandlers);
                }
            }
        }
        formulaireObjet = setGroupId(formulaireObjet, nodeStop, group, group.getNodeId(), saved.getVersionSdk(), null);
        return setSurchargeGroup(formulaireObjet, group);
    }

    @Override
    public Object processElement(SubNoticeMetadata item, SubNotice notice, ConfigurationConsultationDTO configurationConsultationDTO, Object formulaireObjet, String id, EFormsFormulaireEntity saved, String nodeStop) {
        if (configurationConsultationDTO == null || configurationConsultationDTO.getContexte() == null) {
            return formulaireObjet;
        }
        String versionSdk = saved.getVersionSdk();
        ConsultationContexte contexte = configurationConsultationDTO.getContexte();
        FieldDetails fieldDetails = getFields(versionSdk).getFields().stream().filter(fieldDetails1 -> fieldDetails1.getId().equals(id)).findFirst().orElse(null);
        if (fieldDetails == null) {
            return formulaireObjet;
        }
        String parentNodeId = fieldDetails.getParentNodeId();
        EformsSurchargeDTO surcharge = item.getSurcharge();
        if (surcharge != null && surcharge.getDefaultValue() != null) {
            return addToObject(formulaireObjet, id, surcharge.getDefaultValue(), nodeStop, parentNodeId, versionSdk);

        }
        ConsultationMpe mpeConsultation = contexte.getConsultation();

        if (!CollectionUtils.isEmpty(contexte.getLots()) && StringUtils.isEmpty(mpeConsultation.getCodeCpvPrincipal())) {
            LotMpe firstLot = contexte.getLots().get(0);
            if (firstLot.getCodeCpvPrincipal() != null && !firstLot.getCodeCpvPrincipal().isEmpty()) {
                mpeConsultation.setCodeCpvPrincipal(firstLot.getCodeCpvPrincipal());
            }
        }

        ReferentielMpe typeProcedure = contexte.getTypeProcedureConsultation();
        String codeCpvPrincipal = mpeConsultation.getCodeCpvPrincipal();
        String codeCpvSecondaire1 = mpeConsultation.getCodeCpvSecondaire1();
        String codeCpvSecondaire2 = mpeConsultation.getCodeCpvSecondaire2();
        String codeCpvSecondaire3 = mpeConsultation.getCodeCpvSecondaire3();
        ProfilJoueMpe profilJoueMpe = contexte.getProfilJoue();
        boolean repeatable = item.is_repeatable();
        String presetValue = item.get_presetValue();
        if (StringUtils.hasText(presetValue)) {
            Object object = getPresetValue(presetValue, fieldDetails.getType(), versionSdk);
            if (object != null)
                return addToObject(formulaireObjet, id, repeatable ? List.of(object) : object, nodeStop, parentNodeId, versionSdk);
        }

        switch (id) {
            case "BT-106-Procedure":
                return addToObject(formulaireObjet, id, repeatable ? List.of(false) : false, nodeStop, parentNodeId, versionSdk);
            case "OPT-300-Procedure-Buyer":
                return addToObject(formulaireObjet, id, repeatable ? List.of("ORG-0001") : "ORG-0001", nodeStop, parentNodeId, versionSdk);
            case "BT-11-Procedure-Buyer":
                String formeJuridique = getFormeJuridique(profilJoueMpe);
                String value = formeJuridique == null ? "pub-undert-cga" : formeJuridique;
                return addToObject(formulaireObjet, id, repeatable ? List.of(value) : value, nodeStop, parentNodeId, versionSdk);
            case "BT-10-Procedure-Buyer":
                String pouvoirJuridique = getPouvoirJuridique(profilJoueMpe);
                value = pouvoirJuridique == null ? "pub-os" : pouvoirJuridique;
                return addToObject(formulaireObjet, id, repeatable ? List.of(value) : value, nodeStop, parentNodeId, versionSdk);
            case "BT-508-Procedure-Buyer":
                if (profilJoueMpe == null || !StringUtils.hasText(profilJoueMpe.getAdresseProfilAcheteur())) {
                    return formulaireObjet;
                } else {
                    String adresseProfilAcheteur = profilJoueMpe.getAdresseProfilAcheteur();
                    return addToObject(formulaireObjet, id, repeatable ? List.of(adresseProfilAcheteur) : adresseProfilAcheteur, nodeStop, parentNodeId, versionSdk);
                }
            case "BT-105-Procedure":
                SuiviAnnonce suiviAnnonce = saved.getSuiviAnnonce();
                if (suiviAnnonce == null) {
                    return formulaireObjet;
                }
                TypeProcedure procedure = suiviAnnonce.getProcedure();
                if (procedure == null) {
                    return formulaireObjet;
                }
                String typeProcedureLibelle = procedure.getCorrespondanceTed();
                if (!StringUtils.hasText(typeProcedureLibelle)) {
                    return formulaireObjet;
                }
                return addToObject(formulaireObjet, id, repeatable ? List.of(typeProcedureLibelle) : typeProcedureLibelle, nodeStop, parentNodeId, versionSdk);
            case "BT-04-notice":
                String uuid = saved.getProcedure().getUuid();
                return addToObject(formulaireObjet, id, repeatable ? List.of(uuid) : uuid, nodeStop, parentNodeId, versionSdk);
            case "BT-01-notice":
                String legalBasis = notice.getLegalBasis();
                return addToObject(formulaireObjet, id, repeatable ? List.of(legalBasis) : legalBasis, nodeStop, parentNodeId, versionSdk);
            case "BT-02-notice":
                String type = notice.getType();
                return addToObject(formulaireObjet, id, repeatable ? List.of(type) : type, nodeStop, parentNodeId, versionSdk);
            case "BT-03-notice":
                String formType = notice.getFormType();
                return addToObject(formulaireObjet, id, repeatable ? List.of(formType) : formType, nodeStop, parentNodeId, versionSdk);
            case "OPP-070-notice":
                String subTypeId = notice.getSubTypeId();
                return addToObject(formulaireObjet, id, repeatable ? List.of(subTypeId) : subTypeId, nodeStop, parentNodeId, versionSdk);
            case "BT-757-notice":
                Integer noticeVersion = saved.getNoticeVersion();
                if (noticeVersion > 9) {
                    return addToObject(formulaireObjet, id, repeatable ? List.of(noticeVersion) : noticeVersion, nodeStop, parentNodeId, versionSdk);
                }
                value = "0" + noticeVersion;
                return addToObject(formulaireObjet, id, repeatable ? List.of(value) : value, nodeStop, parentNodeId, versionSdk);
            case "OPT-001-notice":
                String ublVersion = getSubNoticeList(versionSdk).getUblVersion();
                return addToObject(formulaireObjet, id, repeatable ? List.of(ublVersion) : ublVersion, nodeStop, parentNodeId, versionSdk);
            case "OPT-002-notice":
                String[] sdkVersion = getFields(versionSdk).getSdkVersion().split("\\.");
                value = sdkVersion[0] + "." + sdkVersion[1];
                return addToObject(formulaireObjet, id, repeatable ? List.of(value) : value, nodeStop, parentNodeId, versionSdk);
            case "BT-300-Procedure":

                DonneesComplementaires donneesComplementaires = contexte.getDonneesComplementaires();

                StringBuilder infosComplementaire = new StringBuilder();
                if (donneesComplementaires != null && StringUtils.hasText(donneesComplementaires.getAutresInformations()))
                    infosComplementaire.append("Autres informations : ").append(donneesComplementaires.getAutresInformations()).append("\n");
                if (donneesComplementaires != null && StringUtils.hasText(donneesComplementaires.getModaliteVisiteLieuxReunion()))
                    infosComplementaire.append("Modalités de Visite des lieux / Réunion : ").append(donneesComplementaires.getModaliteVisiteLieuxReunion()).append("\n");
                if (StringUtils.hasText(mpeConsultation.getModaliteRetraitDossier()))
                    infosComplementaire.append("Modalités de retrait du dossier : ").append(mpeConsultation.getModaliteRetraitDossier()).append("\n");
                if (StringUtils.hasText(mpeConsultation.getReceptionOffresCandidatures()))
                    infosComplementaire.append("Réception des offres/candidatures : ").append(mpeConsultation.getReceptionOffresCandidatures()).append("\n");
                if (donneesComplementaires != null && StringUtils.hasText(donneesComplementaires.getConditionParticipation()))
                    infosComplementaire.append("Conditions de participation : ").append(donneesComplementaires.getConditionParticipation());
                return StringUtils.hasText(infosComplementaire) ? addToObject(formulaireObjet, id, repeatable ? List.of(infosComplementaire) : infosComplementaire, nodeStop, parentNodeId, versionSdk) : formulaireObjet;

            case "BT-701-notice":
                uuid = saved.getUuid();
                return addToObject(formulaireObjet, id, repeatable ? List.of(uuid) : uuid, nodeStop, parentNodeId, versionSdk);
            case "BT-262-Procedure":
                String idType = "BT-26(m)-Procedure";
                if (StringUtils.hasText(codeCpvPrincipal)) {
                    return buildObject(formulaireObjet, id, idType, nodeStop, fieldDetails, parentNodeId, repeatable ? List.of(codeCpvPrincipal) : codeCpvPrincipal, "cpv", versionSdk);
                } else {
                    return buildObject(formulaireObjet, id, idType, nodeStop, fieldDetails, parentNodeId, null, "cpv", versionSdk);
                }

            case "BT-263-Procedure":
                List<String> cpvSecondaires = new ArrayList<>();
                if (StringUtils.hasText(codeCpvSecondaire1)) {
                    cpvSecondaires.add(codeCpvSecondaire1);
                }
                if (StringUtils.hasText(codeCpvSecondaire2)) {
                    cpvSecondaires.add(codeCpvSecondaire2);
                }
                if (StringUtils.hasText(codeCpvSecondaire3)) {
                    cpvSecondaires.add(codeCpvSecondaire3);
                }
                if (!CollectionUtils.isEmpty(contexte.getLots())) {
                    cpvSecondaires.addAll(contexte.getLots().stream().map(LotMpe::getCodeCpvPrincipal).filter(cpv ->
                            Objects.nonNull(cpv) && !cpv.equals(mpeConsultation.getCodeCpvPrincipal())
                    ).toList());
                    cpvSecondaires.addAll(contexte.getLots().stream().map(LotMpe::getCodeCpvSecondaire1).filter(Objects::nonNull).toList());
                    cpvSecondaires.addAll(contexte.getLots().stream().map(LotMpe::getCodeCpvSecondaire2).filter(Objects::nonNull).toList());
                    cpvSecondaires.addAll(contexte.getLots().stream().map(LotMpe::getCodeCpvSecondaire3).filter(Objects::nonNull).toList());
                }
                return buildCpvSecondaires(formulaireObjet, id, "BT-26(a)-Procedure", nodeStop, fieldDetails, parentNodeId, cpvSecondaires, versionSdk);

            case "BT-05(a)-notice":
                ZonedDateTime dateCreation = saved.getDateCreation();
                Object date = addToObject(new Object(), "type", "Europe/Paris", null, null, versionSdk);
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                String formatted = dateCreation.format(formatter);
                date = addToObject(date, "value", repeatable ? List.of(formatted) : formatted, null, null, versionSdk);

                return addToObject(formulaireObjet, id, date, nodeStop, parentNodeId, versionSdk);
            case "BT-05(b)-notice":
                dateCreation = saved.getDateCreation();
                formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
                date = addToObject(new Object(), "type", "Europe/Paris", null, null, versionSdk);
                formatted = dateCreation.format(formatter);
                date = addToObject(date, "value", repeatable ? List.of(formatted) : formatted, null, null, versionSdk);
                return addToObject(formulaireObjet, id, date, nodeStop, parentNodeId, versionSdk);
            case "BT-21-Procedure":
                String intitule = contexte.getConsultation().getIntitule();
                return StringUtils.hasText(intitule) ? addToObject(formulaireObjet, id, repeatable ? List.of(intitule) : intitule, nodeStop, parentNodeId, versionSdk) : formulaireObjet;
            case "BT-22-Procedure":
                String reference = contexte.getConsultation().getReference();
                return StringUtils.hasText(reference) ? addToObject(formulaireObjet, id, repeatable ? List.of(reference) : reference, nodeStop, parentNodeId, versionSdk) : formulaireObjet;

            case "BT-24-Procedure":
                String objet = contexte.getConsultation().getObjet();
                return !StringUtils.hasText(objet) ? formulaireObjet : addToObject(formulaireObjet, id, repeatable ? List.of(objet) : objet, nodeStop, parentNodeId, versionSdk);
            case "BT-23-Procedure":
                String naturePrestation = mpeConsultation.getNaturePrestation();
                if (StringUtils.hasText(naturePrestation)) {
                    Object prestation = getNaturePrestation(naturePrestation);
                    return addToObject(formulaireObjet, id, repeatable ? List.of(prestation) : prestation, nodeStop, parentNodeId, versionSdk);
                }
                return formulaireObjet;
            default:
                log.debug("Pas de correspondance pour {}", id);

        }
        return formulaireObjet;
    }


}
