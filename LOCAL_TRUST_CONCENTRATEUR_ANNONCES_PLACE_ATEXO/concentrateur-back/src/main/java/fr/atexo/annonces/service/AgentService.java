package fr.atexo.annonces.service;


import com.atexo.annonces.commun.mpe.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.atexo.annonces.boot.entity.*;
import fr.atexo.annonces.boot.repository.AgentConsultationSessionRepository;
import fr.atexo.annonces.boot.repository.AgentRepository;
import fr.atexo.annonces.boot.repository.AgentSessionRepository;
import fr.atexo.annonces.config.EFormsHelper;
import fr.atexo.annonces.dto.ConfigurationConsultationDTO;
import fr.atexo.annonces.mapper.AgentMpeMapper;
import fr.atexo.annonces.service.exception.AgentException;
import fr.atexo.annonces.service.exception.AgentExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Optional;
import java.util.Set;


@Slf4j
@Component
public class AgentService {

    private final AgentSessionRepository agentSessionRepository;
    private final AgentRepository agentRepository;
    private final AgentConsultationSessionRepository agentConsultationSessionRepository;
    private final OrganismeService organismeService;
    private final ReferentielMpeService referentielMpeService;

    private final AgentMpeMapper agentMpeMapper;

    public AgentService(AgentSessionRepository agentSessionRepository, AgentRepository agentRepository, AgentConsultationSessionRepository agentConsultationSessionRepository, OrganismeService organismeService, ReferentielMpeService referentielMpeService, AgentMpeMapper agentMpeMapper) {
        this.agentSessionRepository = agentSessionRepository;
        this.agentRepository = agentRepository;
        this.agentConsultationSessionRepository = agentConsultationSessionRepository;
        this.organismeService = organismeService;
        this.referentielMpeService = referentielMpeService;
        this.agentMpeMapper = agentMpeMapper;
    }


    public Agent saveAgentIfNotExist(EnvoiMpe mpe, Set<String> roles, Organisme organisme) throws AgentException {
        try {

            if (mpe == null || mpe == null || mpe.getAgent() == null || mpe.getAgent().getAcronymeOrganisme() == null || mpe.getAgent().getOrganisme() == null || mpe.getAgent().getPlateforme() == null) {
                throw new AgentException(AgentExceptionEnum.SAVE_ERROR, "La plateforme/organisme ne doivent pas être null");
            }

            EnvoiAgent agentType = mpe.getAgent();


            var identifiant = agentType.getLogin();
            var idOrganisme = organisme.getId();
            log.info("Récupération agent : identifiant = {}, id organisme = {}", identifiant, idOrganisme);
            Agent savedAgent = agentRepository.findByIdentifiantAndOrganismeId(identifiant, idOrganisme).orElse(null);

            if (agentType.getOrganisme() == null) {
                agentType.setOrganisme(new OrganismeMpe());
            }
            agentType.getOrganisme().setId(Long.valueOf(idOrganisme));
            if (savedAgent == null) {
                savedAgent = agentMpeMapper.mapToEntity(mpe);
                savedAgent.setOrganisme(organisme);
            } else {
                agentMpeMapper.mapToEntity(mpe, savedAgent);
            }

            ReferentielServiceMpe service = null;
            ServiceMpe typeService = agentType.getService();
            if (typeService != null && typeService.getId() != null && typeService.getSigle() != null) {
                ReferentielServiceMpe service1 = ReferentielServiceMpe.builder().libelle(typeService.getLibelle()).organisme(organisme).code(typeService.getSigle()).actif(true).idMpe(typeService.getId()).build();
                service = referentielMpeService.getReferential(service1);
            }
            try {
                savedAgent.setService(service);
                savedAgent.setEnvoiMpe(new ObjectMapper().writeValueAsString(mpe));
            } catch (JsonProcessingException e) {
                throw new AgentException(AgentExceptionEnum.MPE_ENVOI, e.getMessage());
            }
            if (!CollectionUtils.isEmpty(roles)) savedAgent.setRoles(String.join(",", roles));
            else {
                savedAgent.setRoles("");
            }
            savedAgent = agentRepository.save(savedAgent);
            return savedAgent;

        } catch (Exception e) {
            throw new AgentException(AgentExceptionEnum.SAVE_ERROR, e.getMessage(), e);
        }
    }

    public ConfigurationConsultationDTO getConfiguration(String token, Integer idConsultation, String idPlateform) {
        AgentConsultationSessionEntity consultationSession = null;
        if (token != null) {
            Agent agent = this.findByToken(token);
            if (agent != null) {
                consultationSession = agentConsultationSessionRepository.findByAgentIdAndIdConsultation(agent.getId(), idConsultation).orElse(null);
            }
        }
        if (idPlateform != null && consultationSession == null) {
            consultationSession = agentConsultationSessionRepository.findByAgentOrganismePlateformeUuidAndIdConsultation(idPlateform, idConsultation).stream().findFirst().orElse(null);

        }

        if (consultationSession == null) {
            consultationSession = agentConsultationSessionRepository.findByIdConsultation(idConsultation).stream().findFirst().orElse(null);

        }
        if (consultationSession == null) {
            return null;
        }
        return EFormsHelper.getObjectFromString(consultationSession.getContexte(), ConfigurationConsultationDTO.class);
    }

    public String addAgent(EnvoiMpe envoi, String token) {

        if (envoi == null || envoi.getAgent() == null || envoi.getAgent().getApi() == null) {
            throw new AgentException(AgentExceptionEnum.MANDATORY, "Agent pas renseigné");
        }
        EnvoiAgent agent = envoi.getAgent();
        Api api = agent.getApi();
        String serveurUrl = api.getUrl();
        OrganismeMpe mpeOrganisme = agent.getOrganisme();
        Organisme organisme = this.organismeService.savePlateformeOrganismeIfNotExist(mpeOrganisme, serveurUrl, agent.getPlateforme());
        var savedAgent = this.saveAgentIfNotExist(envoi, Set.of(), organisme);
        if (CollectionUtils.isEmpty(savedAgent.getSessions()) || savedAgent.getSessions().stream().noneMatch(agentSessionEntity -> agentSessionEntity.getToken().equals(token))) {
            agentSessionRepository.save(AgentSessionEntity.builder().agent(savedAgent).token(token).build());
        }

        return savedAgent.getId();

    }

    public Agent findByToken(String token) {
        if (token == null) return agentRepository.findById("anonyme").orElse(null);
        AgentSessionEntity sessionAgent = agentSessionRepository.findByToken(token);
        if (sessionAgent == null) {
            return agentRepository.findById("anonyme").orElse(null);
        }
        return sessionAgent.getAgent();
    }

    public Optional<Agent> findByAnonyme() {
        return agentRepository.findById("anonyme");
    }

    public boolean existByToken(String token) {
        return agentSessionRepository.existsByToken(token);
    }

    public ConfigurationConsultationDTO getConfigurationByPlateforme(Integer idConsultation, String plateformeUuid) {
        AgentConsultationSessionEntity consultationSession;

        log.info("Récupération de la configuration de la consultation {} pour la plateforme {}", idConsultation, plateformeUuid);

        consultationSession = agentConsultationSessionRepository.findByAgentOrganismePlateformeUuidAndIdConsultation(plateformeUuid, idConsultation).stream().findFirst().orElse(null);

        return consultationSession == null ? null : EFormsHelper.getObjectFromString(consultationSession.getContexte(), ConfigurationConsultationDTO.class);
    }
}

