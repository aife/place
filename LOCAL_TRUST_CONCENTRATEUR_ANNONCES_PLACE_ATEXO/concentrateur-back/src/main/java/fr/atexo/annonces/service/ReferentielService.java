package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.entity.Organisme;
import fr.atexo.annonces.boot.repository.OffreTypeProcedureRepository;
import fr.atexo.annonces.boot.repository.OrganismeRepository;
import fr.atexo.annonces.dto.ReferentielDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ReferentielService {

    private final OffreTypeProcedureRepository offreTypeProcedureRepository;
    private final OrganismeRepository organismeRepository;

    public ReferentielService(OffreTypeProcedureRepository offreTypeProcedureRepository, OrganismeRepository organismeRepository) {
        this.offreTypeProcedureRepository = offreTypeProcedureRepository;
        this.organismeRepository = organismeRepository;
    }


    public List<ReferentielDTO> getProcedure(String plateforme) {
        List<Organisme> organismes = organismeRepository.findByPlateformeUuid(plateforme);

        return offreTypeProcedureRepository.findAllProcedureByAcronymeOrganismeIn(organismes.stream().map(Organisme::getAcronymeOrganisme).collect(Collectors.toList())).stream()
                .map(eFormsProcedureEntity -> ReferentielDTO.builder()
                        .id(eFormsProcedureEntity.getId())
                        .code(eFormsProcedureEntity.getAbbreviation())
                        .libelle(eFormsProcedureEntity.getLibelle())
                        .build())
                .collect(Collectors.toList());
    }
}
