package fr.atexo.annonces.boot.repository;

import fr.atexo.annonces.boot.entity.PlateformeConfiguration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * Repository pour accéder aux objets Support
 */
@Repository
public interface PlateformeConfigurationRepository extends JpaRepository<PlateformeConfiguration, Long>, JpaSpecificationExecutor<PlateformeConfiguration> {

    PlateformeConfiguration findByIdPlateforme(String plateforme);

    long countByCronMasterIsTrue();

    boolean existsByIdPlateforme(String idPlatform);
}
