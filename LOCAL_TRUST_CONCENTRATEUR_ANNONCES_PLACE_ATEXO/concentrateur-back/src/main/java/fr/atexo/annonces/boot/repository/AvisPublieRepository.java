package fr.atexo.annonces.boot.repository;

import fr.atexo.annonces.boot.entity.AvisPublie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Repository pour accéder aux objets AvisPublie
 */
@Repository
public interface AvisPublieRepository extends JpaRepository<AvisPublie, Integer> {

    Optional<AvisPublie> findByIdAvis(String id);

    List<AvisPublie> findByUidPlateformeAndDateEnvoiIsBetween(String plateforme, LocalDateTime begin, LocalDateTime end);
}
