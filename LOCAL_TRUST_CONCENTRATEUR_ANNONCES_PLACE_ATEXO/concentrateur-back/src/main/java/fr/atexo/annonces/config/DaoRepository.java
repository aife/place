package fr.atexo.annonces.config;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.persistence.LockModeType;
import java.util.List;

public interface DaoRepository<T, ID> extends PagingAndSortingRepository<T, ID>,
        JpaSpecificationExecutor<T> {

    default List<T> findAllByCriteria(List<SearchCriteria> searchCriteria) {
        Specification<T> specification = getSpecification(searchCriteria);
        return this.findAll(specification);
    }

    default T findOneByCriteria(List<SearchCriteria> searchCriteria) {
        Specification<T> specification = getSpecification(searchCriteria);
        return this.findOne(specification).orElse(null);
    }

    default Page<T> findAllByCriteria(List<SearchCriteria> searchCriteria, Pageable pageable) {
        Specification<T> specification = getSpecification(searchCriteria);

        return this.findAll(specification, pageable);
    }

    private Specification<T> getSpecification(List<SearchCriteria> searchCriteria) {
        Specification<T> specification = null;
        for (SearchCriteria buDaSearchCriteria : searchCriteria) {
            if (specification == null) {
                specification = Specification.<T>where(
                        new DatabaseSpecification<T>(buDaSearchCriteria));
            } else {
                specification = specification.and(new DatabaseSpecification<T>(buDaSearchCriteria));
            }
        }
        return specification;
    }
}
