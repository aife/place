package fr.atexo.annonces.dto.tncp;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.ZonedDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TncpSuiviNotification implements Serializable {
    private String uuid;
    private InterfaceEnum flux;
    private TypeEnum type;
    private EtapeEnum etape;
    private String objetDestination;
    private String uuidPlateforme;
    private String message;
    private String messageDetails;
    private ZonedDateTime dateEnvoi;
    private StatutEnum statut;
    private String body;
    private String traitementPlateforme;
    private ZonedDateTime dateReception;
}
