package fr.atexo.annonces.boot.core.domain.e_forms_notice.services;

import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNotice;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNoticeMetadata;
import fr.atexo.annonces.boot.entity.EFormsFormulaireEntity;
import fr.atexo.annonces.dto.ConfigurationConsultationDTO;

import java.util.List;
import java.util.Map;

public interface NoticeFormatHandler {

    Object processGroup(SubNotice notice, ConfigurationConsultationDTO configurationConsultationDTO, Object formulaireObjet, SubNoticeMetadata group, EFormsFormulaireEntity saved, String nodeStop, Map<String, List<NoticeFormatHandler>> kownHandlers);

    Object processElement(SubNoticeMetadata item, SubNotice notice, ConfigurationConsultationDTO contexte, Object formulaireObjet, String id, EFormsFormulaireEntity saved, String nodeStop);


}
