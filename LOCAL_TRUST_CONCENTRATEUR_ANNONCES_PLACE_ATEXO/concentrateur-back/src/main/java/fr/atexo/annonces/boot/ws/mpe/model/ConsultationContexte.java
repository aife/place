package fr.atexo.annonces.boot.ws.mpe.model;

import com.atexo.annonces.commun.mpe.*;
import fr.atexo.annonces.dto.AcheteurChoixDTO;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderMethodName = "with")
public class ConsultationContexte {
    private ConsultationMpe consultation;
    private LotMpe lot;
    private String lieuExecution;
    private OrganismeMpe organismeConsultation;
    private OrganismeMpe organismeAgent;
    private ContratMpe typeContrat;
    private ReferentielMpe typeProcedureConsultation;
    @Singular
    private List<AcheteurChoixDTO> comptes;
    private String serveurUrl;
    private AcheteurMpe acheteur;
    private DonneesComplementaires donneesComplementaires;
    private List<CritereAttributionMpe> critereAttribution;
    private ProfilJoueMpe profilJoue;
    private List<LotMpe> lots;
    private ServiceMpe serviceAgent;
    private ServiceMpe serviceConsultation;
    private AgentMpe agent;
    private NatureMpe naturePrestation;
    private List<ServiceMpe> services;

}
