package fr.atexo.annonces.boot.ws.eforms;


import fr.atexo.annonces.boot.core.domain.e_forms_notice.model.ESentoolSearch;
import fr.atexo.annonces.jaxb.suivi.simap.ReponseSimapPage;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.util.Base64;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;


@Component
@Slf4j
public class ESentoolWS {

    private final RestTemplate atexoRestTemplate;

    private final ESentoolConfiguration eSentoolConfiguration;

    public ESentoolWS(RestTemplate atexoRestTemplate, ESentoolConfiguration eSentoolConfiguration) {
        this.atexoRestTemplate = atexoRestTemplate;
        this.eSentoolConfiguration = eSentoolConfiguration;
    }

    public ReponseSimapPage searchNotice(int page, int pageSize, ESentoolSearch search) {

        org.springframework.http.HttpEntity<MultiValueMap<String, Object>> requestEntity = new org.springframework.http.HttpEntity<>(getHeaders());

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(eSentoolConfiguration.getBasePath() + eSentoolConfiguration.getWs().get("search"));
        builder.queryParam("page", page);
        builder.queryParam("pageSize", pageSize);
        if (search != null) {

            if (search.getStatus() != null) {
                builder.queryParam("status", search.getStatus());
            }
            if (search.getReceivedFrom() != null) {
                builder.queryParam("receivedFrom", search.getReceivedFrom());
            }
            if (search.getReceivedFrom() != null) {
                builder.queryParam("receivedTo", search.getReceivedTo());
            }
            if (search.getPublishedFrom() != null) {
                builder.queryParam("publishedFrom", search.getPublishedFrom());
            }
            if (search.getPublishedTo() != null) {
                builder.queryParam("publishedTo", search.getPublishedTo());
            }
            if (search.getNoDocExt() != null) {
                builder.queryParam("noDocExt", search.getNoDocExt());
            }
        }
        String url = builder.build().toUriString();
        log.debug("URL : {}", url);
        return atexoRestTemplate.exchange(url, HttpMethod.GET, requestEntity, ReponseSimapPage.class).getBody();
    }

    private HttpHeaders getHeaders() {
        String authEncoded = Base64.encodeBase64StringUnChunked(eSentoolConfiguration.getApiKey().getBytes());


        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        httpHeaders.set("Authorization", "Basic " + authEncoded);

        return httpHeaders;
    }


}
