package fr.atexo.annonces.boot.ws.eforms;

import fr.atexo.annonces.dto.eforms.*;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class EFormsWS {
    private final RestTemplate atexoRestTemplate;
    private final EformsConfiguration eformsConfiguration;

    public EFormsWS(RestTemplate atexoRestTemplate, EformsConfiguration eformsConfiguration) {
        this.atexoRestTemplate = atexoRestTemplate;
        this.eformsConfiguration = eformsConfiguration;

    }

    private final List<Integer> errorTed = List.of(400, 500, 409);
    public EformsSubmitResponseDTO submit(File notice, Metadata metadata, String eformsToken) {
        try {
            MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
            bodyMap.add("notice", new FileSystemResource(notice));
            final HttpHeaders theJsonHeader = new HttpHeaders();
            theJsonHeader.setContentType(MediaType.APPLICATION_JSON);
            bodyMap.add("metadata", new HttpEntity<>(metadata, theJsonHeader));
            HttpHeaders headers = new HttpHeaders();

            headers.setBearerAuth(StringUtils.hasText(eformsToken) ? eformsToken : eformsConfiguration.getApiKey());
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            headers.setAccept(List.of(MediaType.APPLICATION_JSON));
            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);
            Map<String, String> urlParameters = new HashMap<>();
            String url = eformsConfiguration.getBasePath() + eformsConfiguration.getWs().get("submit");
            return atexoRestTemplate.postForObject(url, requestEntity, EformsSubmitResponseDTO.class, urlParameters);
        } catch (RestClientResponseException e) {
            log.error("Erreur lors de la soumission de la notice : {}", e.getResponseBodyAsString());
            if (errorTed.contains(e.getRawStatusCode())) {
                throw new EformsException(ExceptionEnum.EFORMS_WS, e.getResponseBodyAsString(), e);
            }
            return null;
        } catch (Exception e) {
            log.error("Erreur lors de la soumission de la notice : {}", e.getMessage());
            throw new EformsException(ExceptionEnum.EFORMS_WS, e.getMessage(), e);
        }
    }


    public EsentoolNoticeDTO stopPublication(String noticeId, String versionId, String eformsToken) {
        if (noticeId == null || versionId == null) {
            throw new EformsException(ExceptionEnum.TECHNICAL_ERROR, "the notice business identifier (notice identifier - version) sont obligatoires");
        }
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(StringUtils.hasText(eformsToken) ? eformsToken : eformsConfiguration.getApiKey());
            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(null, headers);
            Map<String, String> urlParameters = new HashMap<>();
            urlParameters.put("businessId", noticeId + "-" + versionId);
            return atexoRestTemplate.postForObject(eformsConfiguration.getBasePath() + eformsConfiguration.getWs().get("stop-publication"), requestEntity, EsentoolNoticeDTO.class, urlParameters);
        } catch (RestClientResponseException e) {
            log.error("Erreur lors de l'arrêt de la publication : {}", e.getResponseBodyAsString());
            throw new EformsException(ExceptionEnum.EFORMS_WS, e.getResponseBodyAsString(), e);
        } catch (Exception e) {
            log.error("Erreur lors de l'arrêt de la publication : {}", e.getMessage());
            throw new EformsException(ExceptionEnum.EFORMS_WS, e.getMessage(), e);
        }
    }

    public File getValidationReport(String noticeId, String versionId, String eformsToken) {
        if (noticeId == null || versionId == null) {
            throw new EformsException(ExceptionEnum.TECHNICAL_ERROR, "the notice business identifier (notice identifier - version) sont obligatoires");
        }

        try {
            Map<String, String> urlParameters = new HashMap<>();
            String bussunessId = noticeId + "-" + versionId;
            urlParameters.put("businessId", bussunessId);
            File tempFile = File.createTempFile("rapport-" + bussunessId, ".svrl");
            return atexoRestTemplate.execute(eformsConfiguration.getBasePath() + eformsConfiguration.getWs().get("get-validation"),
                    HttpMethod.GET,
                    clientHttpRequest -> clientHttpRequest.getHeaders().setBearerAuth(StringUtils.hasText(eformsToken) ? eformsToken : eformsConfiguration.getApiKey()),
                    clientHttpResponse -> {
                        StreamUtils.copy(clientHttpResponse.getBody(), new FileOutputStream(tempFile));
                        return tempFile;
                    },
                    urlParameters);
        } catch (RestClientResponseException e) {
            log.error("Erreur lors de la récupération du rapport de validation : {}", e.getResponseBodyAsString());
            throw new EformsException(ExceptionEnum.EFORMS_WS, e.getResponseBodyAsString(), e);
        } catch (Exception e) {
            log.error("Erreur lors de la récupération du rapport de validation : {}", e.getMessage());
            throw new EformsException(ExceptionEnum.EFORMS_WS, e.getMessage(), e);
        }
    }

    public EsentoolPageDTO search(int page, int size, EformsSearch search, String eformsToken) {
        if (search == null) {
            log.error("Aucun critère de recherche renseigné");
            throw new EformsException(ExceptionEnum.TECHNICAL_ERROR, "Aucun critère de recherche renseigné");
        }
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(StringUtils.hasText(eformsToken) ? eformsToken : eformsConfiguration.getApiKey());
            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(headers);

            String url = eformsConfiguration.getBasePath() + eformsConfiguration.getWs().get("get-notices");
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);
            builder.queryParam("page", page)
                    .queryParam("size", size);
            if (search.getId() != null) {
                builder.queryParam("id", search.getId());
            }
            if (search.getVersionId() != null) {
                builder.queryParam("versionId", search.getVersionId());
            }
            if (search.getSubmittedAfter() != null) {
                builder.queryParam("submittedAfter", search.getSubmittedAfter());
            }
            if (search.getSubmittedBefore() != null) {
                builder.queryParam("submittedBefore", search.getSubmittedBefore());
            }
            if (search.getPublishedAfter() != null) {
                builder.queryParam("publishedAfter", search.getPublishedAfter());
            }
            if (search.getPublishedBefore() != null) {
                builder.queryParam("publishedBefore", search.getPublishedBefore());
            }

            if (search.getStatuses() != null) {
                builder.queryParam("statuses", search.getStatuses());
            }

            String uriBuilder = builder.build().encode().toUriString();


            return atexoRestTemplate.exchange(uriBuilder, HttpMethod.GET,
                    requestEntity, EsentoolPageDTO.class).getBody();
        } catch (RestClientResponseException e) {
            log.error("Erreur lors de la recherche des notices : {}", e.getResponseBodyAsString());
        } catch (Exception e) {
            log.error("Erreur lors de la recherche des notices : {}", e.getMessage());
        }
        return null;
    }


}
