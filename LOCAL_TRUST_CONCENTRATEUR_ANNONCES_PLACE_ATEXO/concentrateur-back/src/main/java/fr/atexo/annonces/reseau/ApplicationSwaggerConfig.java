package fr.atexo.annonces.reseau;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.tags.Tag;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Locale;

@Configuration
@OpenAPIDefinition(servers = {@Server(url = "/concentrateur-annonces", description = "Default Server URL")})
public class ApplicationSwaggerConfig {

    private final MessageSource messageSource;


    public ApplicationSwaggerConfig(MessageSource messageSource) {
        this.messageSource = messageSource;
    }


    @Bean
    public OpenAPI customOpenAPI(@Value("${annonces.version}") String version) {
        final String securitySchemeName = "bearerAuth";
        final String securitySchemeNameOauth2 = "OAUTH2";
        return new OpenAPI()
                .addSecurityItem(new SecurityRequirement().addList(securitySchemeName))
                .addSecurityItem(new SecurityRequirement().addList(securitySchemeNameOauth2))
                .components(
                        new Components()
                                .addSecuritySchemes(securitySchemeName,
                                        new SecurityScheme()
                                                .name(securitySchemeName)
                                                .type(SecurityScheme.Type.OAUTH2)
                                                .scheme("bearer")
                                                .bearerFormat("JWT")
                                ))
                .tags(Arrays.asList(getTags("Supports", messageSource.getMessage("swagger.tag.supports", null, "swagger.tag.supports", Locale.getDefault())),
                        getTags("Annonces", messageSource.getMessage("swagger.tag.annonces", null, "swagger.tag.annonces", Locale.getDefault())),
                        getTags("Départements", messageSource.getMessage("swagger.tag.departements", null, "swagger.tag.departements", Locale.getDefault()))))
                .info(new Info()
                        .title("Concentrateur")
                        .version(version)
                        .description("Documentation de l'API du Concentrateur. Tout web service dont le chemin d'accès" +
                                " contient 'v2' fait partie du cycle de vie 'publicité simplifiée' et nécessite d'être" +
                                " authentifié pour y accéder.")
                        .termsOfService("http://www.atexo.com/")
                        .license(new License().name("Atexo " + LocalDate.now().getYear()).url("http://www.atexo.com/")));
    }

    private Tag getTags(String name, String description) {
        Tag tag = new Tag();
        tag.setName(name);
        tag.description(description);
        return tag;
    }


}
