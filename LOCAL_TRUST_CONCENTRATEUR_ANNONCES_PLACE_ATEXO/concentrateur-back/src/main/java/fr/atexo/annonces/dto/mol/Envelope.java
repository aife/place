//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.06.02 à 03:26:18 PM CEST 
//


package fr.atexo.annonces.dto.mol;

import lombok.Data;

import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java pour Envelope complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="Envelope"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://schemas.xmlsoap.org/soap/envelope/}Header" minOccurs="0"/&gt;
 *         &lt;element ref="{http://schemas.xmlsoap.org/soap/envelope/}Body"/&gt;
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;anyAttribute processContents='lax' namespace='##other'/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Envelope", propOrder = {
        "header",
        "body"
})
@XmlRootElement(name = "Envelope")
@Data
public class Envelope {

    @XmlElement(name = "Header")
    protected Header header;
    @XmlElement(name = "Body", required = true)
    protected Body body;

}
