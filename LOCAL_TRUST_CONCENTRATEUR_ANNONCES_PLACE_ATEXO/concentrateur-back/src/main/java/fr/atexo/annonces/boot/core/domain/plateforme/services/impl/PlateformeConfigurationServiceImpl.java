package fr.atexo.annonces.boot.core.domain.plateforme.services.impl;

import fr.atexo.annonces.boot.core.domain.plateforme.services.IPlateformeConfigurationService;
import fr.atexo.annonces.boot.core.domain.xlsx.model.*;
import fr.atexo.annonces.boot.core.domain.xlsx.services.IXlsxService;
import fr.atexo.annonces.boot.entity.*;
import fr.atexo.annonces.boot.repository.*;
import fr.atexo.annonces.dto.PlateformeConfigurationDTO;
import fr.atexo.annonces.mapper.PlateformeConfigurationMapper;
import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class PlateformeConfigurationServiceImpl implements IPlateformeConfigurationService {

    private final PlateformeConfigurationRepository plateformeConfigurationRepository;
    private final ConsultationFiltreRepository consultationFiltreRepository;
    private final PlateformeConfigurationMapper plateformeConfigurationMapper;
    private final IXlsxService xlsxService;
    private final TypeProcedureRepository procedureRepository;
    private final EformsSurchargeRepository eformsSurchargeRepository;
    private final OffreRepository offreRepository;
    private final OffreTypeProcedureRepository offreTypeProcedureRepository;
    private final String sheetnameAssociationProcedureAvisEuropeens = "Procédures Européennes";
    private final String sheetnameProcedure = "Correspondance Proc Eforms";
    private final String sheetnameAssociationProcedureAvisNational = "Procédures Nationales";
    private final String sheetnameAssociationAvis = "Européens et Nationaux";
    private final String sheetnameConfigurationEforms = "Surcharge EFORMS";
    @Value("${annonces.plateforme}")
    private String plateforme;

    public PlateformeConfigurationServiceImpl(PlateformeConfigurationRepository plateformeConfigurationRepository, ConsultationFiltreRepository consultationFiltreRepository, PlateformeConfigurationMapper plateformeConfigurationMapper, IXlsxService xlsxService, TypeProcedureRepository procedureRepository, EformsSurchargeRepository eformsSurchargeRepository, OffreRepository offreRepository, OffreTypeProcedureRepository offreTypeProcedureRepository) {
        this.plateformeConfigurationRepository = plateformeConfigurationRepository;
        this.consultationFiltreRepository = consultationFiltreRepository;
        this.plateformeConfigurationMapper = plateformeConfigurationMapper;
        this.xlsxService = xlsxService;
        this.procedureRepository = procedureRepository;
        this.eformsSurchargeRepository = eformsSurchargeRepository;
        this.offreRepository = offreRepository;
        this.offreTypeProcedureRepository = offreTypeProcedureRepository;
    }


    @PostConstruct
    public void initialize() {
        PlateformeConfiguration plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(plateforme);
        if (plateformeConfiguration == null) {
            long count = plateformeConfigurationRepository.countByCronMasterIsTrue();
            plateformeConfiguration = PlateformeConfiguration.builder().idPlateforme(plateforme).validationObligatoire(false).associationOrganisme(false).associationProcedure(false).eformsModal(false).groupement(false).jal(true).pqr(true).national(true).europeen(true).supports("").pays("FRA").cronMaster(count == 0).build();
            plateformeConfigurationRepository.save(plateformeConfiguration);
        }
    }

    @Override
    public List<PlateformeConfigurationDTO> getPlateformes() {
        List<PlateformeConfiguration> plateformeConfiguration = plateformeConfigurationRepository.findAll();
        return plateformeConfigurationMapper.mapToModels(plateformeConfiguration);
    }

    @Override
    public PlateformeConfigurationDTO getPlateforme(Long id) {
        PlateformeConfiguration plateformeConfiguration = plateformeConfigurationRepository.findById(id).orElseThrow(() -> new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucune plateforme trouvée"));
        return plateformeConfigurationMapper.mapToModel(plateformeConfiguration);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public PlateformeConfigurationDTO put(Long id, PlateformeConfigurationDTO configuration) {
        PlateformeConfiguration saved = plateformeConfigurationRepository.findById(id).orElseThrow(() -> new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucune plateforme trouvée"));
        if (!saved.getIdPlateforme().equals(configuration.getIdPlateforme())) {
            new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "La plateforme est inchangeable");
        }
        PlateformeConfiguration plateformeConfiguration = plateformeConfigurationMapper.mapToEntity(configuration);
        plateformeConfiguration.setId(id);
        return plateformeConfigurationMapper.mapToModel(plateformeConfigurationRepository.save(plateformeConfiguration));
    }


    @Override
    @Transactional(rollbackOn = Exception.class)
    public PlateformeConfigurationDTO addOrUpdatePlateformConfiguration(@Valid PlateformeConfigurationDTO plateforme) {
        PlateformeConfiguration toSave = plateformeConfigurationMapper.mapToEntity(plateforme);
        String idPlatform = plateforme.getIdPlateforme();
        PlateformeConfiguration saved = plateformeConfigurationRepository.findByIdPlateforme(idPlatform);
        if (saved != null) {
            toSave.setId(saved.getId());
        }
        if (!toSave.isNational()) {
            consultationFiltreRepository.updateNationalToFalse(idPlatform);
        }
        if (!toSave.isEuropeen()) {
            consultationFiltreRepository.updateEuropeenToFalse(idPlatform);
        }
        if (!toSave.isJal()) {
            consultationFiltreRepository.updateJalToFalse(idPlatform);
        }
        if (!toSave.isPqr()) {
            consultationFiltreRepository.updatePqrToFalse(idPlatform);
        }
        return plateformeConfigurationMapper.mapToModel(plateformeConfigurationRepository.save(toSave));
    }


    @Override
    @Transactional(rollbackOn = Exception.class)
    public void delete(Long id) {
        plateformeConfigurationRepository.deleteById(id);
    }


    @Override
    @Transactional(rollbackOn = Exception.class)
    public boolean patchAssociationProcedureAvisOrganisme(Long id, MultipartFile document) {
        PlateformeConfiguration plateformeConfiguration = null;
        if (id != null)
            plateformeConfiguration = plateformeConfigurationRepository.findById(id).orElseThrow(() -> new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucune plateforme trouvée"));
        if (document == null || document.getOriginalFilename() == null || !document.getOriginalFilename().endsWith(".xlsx")) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Le fichier doit être d'extension XLSX");
        }
        InputStream inputStream;
        try {
            inputStream = document.getInputStream();
        } catch (IOException e) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Erreur lors de la lecture du fichier");
        }
        if (plateformeConfiguration == null) {
            offreTypeProcedureRepository.deleteAllByPlateformeConfigurationIsNull();
        } else {
            offreTypeProcedureRepository.deleteAllByPlateformeConfiguration(plateformeConfiguration);
        }

        int row = 1;
        try (XSSFWorkbook xlsx = xlsxService.openFile(inputStream)) {
            for (int sheeNum = 0; sheeNum < xlsx.getNumberOfSheets(); sheeNum++) {
                List<List<String>> lists = xlsxService.extractValues(xlsx, sheeNum, row);
                if (sheetnameProcedure.equals(xlsx.getSheetAt(sheeNum).getSheetName())) {
                    updateTypeProcedure(lists);
                } else if (sheetnameAssociationProcedureAvisEuropeens.equals(xlsx.getSheetAt(sheeNum).getSheetName())) {
                    updateAssociationProcedureAvis(plateformeConfiguration, lists, "_ENOTICES");
                } else if (sheetnameAssociationProcedureAvisNational.equals(xlsx.getSheetAt(sheeNum).getSheetName())) {
                    updateAssociationProcedureAvis(plateformeConfiguration, lists, null);
                } else if (sheetnameAssociationAvis.equals(xlsx.getSheetAt(sheeNum).getSheetName())) {
                    updateAssociationAvisEuropeensAvisNational(lists);
                } else if (sheetnameConfigurationEforms.equals(xlsx.getSheetAt(sheeNum).getSheetName())) {
                    List<EFormsSurcharge> eFormsSurcharges;
                    if (plateformeConfiguration == null) {
                        eFormsSurcharges = eformsSurchargeRepository.findByIdPlatformIsNull();
                    } else {
                        eFormsSurcharges = eformsSurchargeRepository.findByIdPlatform(plateformeConfiguration.getIdPlateforme());
                    }
                    updateEForms(lists, eFormsSurcharges);
                } else {
                    log.warn("Ignoring sheet {}", xlsx.getSheetAt(sheeNum).getSheetName());
                }
            }

        } catch (IOException e) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, e.getMessage(), e);
        }
        return false;
    }

    @Override
    public ByteArrayOutputStream exportConfiguration(Long id) {
        PlateformeConfiguration plateformeConfiguration = null;
        if (id != null)
            plateformeConfiguration = plateformeConfigurationRepository.findById(id).orElseThrow(() -> new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucune plateforme trouvée"));

        List<OffreTypeProcedure> offreTypeProcedures;
        List<OffreTypeProcedure> allByPlateformeConfigurationIsNull = offreTypeProcedureRepository.findAllAllByPlateformeConfigurationIsNull();
        if (plateformeConfiguration == null) {
            offreTypeProcedures = allByPlateformeConfigurationIsNull;
        } else {
            offreTypeProcedures = offreTypeProcedureRepository.findAllByPlateformeConfiguration(plateformeConfiguration);
            allByPlateformeConfigurationIsNull.forEach(offreTypeProcedure -> {
                if (offreTypeProcedures.stream().noneMatch(offreTypeProcedure1 -> offreTypeProcedure1.getProcedure().getId().equals(offreTypeProcedure.getProcedure().getId()) && offreTypeProcedure1.getOffre().getId().equals(offreTypeProcedure.getOffre().getId()) && offreTypeProcedure1.getAcronymeOrganisme().equals(offreTypeProcedure.getAcronymeOrganisme()))) {
                    offreTypeProcedures.add(offreTypeProcedure);
                }
            });
        }

        try (XSSFWorkbook workbook = new XSSFWorkbook()) {
            XSSFSheet xssfSheetProcedure = xlsxService.createXLSX(workbook, sheetnameProcedure, XlsxTypeProcedureHeaderEnum.transform());
            addXlsxProcedure(xssfSheetProcedure, procedureRepository.findAll());
            XSSFSheet xssfSheetEuropeen = xlsxService.createXLSX(workbook, sheetnameAssociationProcedureAvisEuropeens, XlsxAssociationAvisProcedureOrganismeHeaderEnum.transform());


            List<OffreTypeProcedure> enotice = offreTypeProcedures.stream().filter(offreTypeProcedure -> {
                Offre offre = offreTypeProcedure.getOffre();
                return offre != null && offre.getSupports().stream().anyMatch(Support::isEuropeen);
            }).toList();
            addXlsxOffreTypeProcedure(xssfSheetEuropeen, enotice);
            //Ajout des Ref par défaut
            XSSFSheet xssfSheetNational = xlsxService.createXLSX(workbook, sheetnameAssociationProcedureAvisNational, XlsxAssociationAvisProcedureOrganismeHeaderEnum.transform());
            List<OffreTypeProcedure> national = offreTypeProcedures.stream().filter(offreTypeProcedure -> {
                Offre offre = offreTypeProcedure.getOffre();
                return offre != null && offre.getSupports().stream().noneMatch(Support::isEuropeen);
            }).toList();
            addXlsxOffreTypeProcedure(xssfSheetNational, national);
            XSSFSheet xssfSheetAvisAssociation = xlsxService.createXLSX(workbook, sheetnameAssociationAvis, XlsxAssociationAvisHeaderEnum.transform());
            List<Offre> avisWithAssociation = offreTypeProcedures.stream().filter(offreTypeProcedure -> {
                Offre offre = offreTypeProcedure.getOffre();
                return offre != null && offre.getOffreAssociee() != null;
            }).map(OffreTypeProcedure::getOffre).distinct().toList();
            addXlsxOffre(xssfSheetAvisAssociation, avisWithAssociation);
            XSSFSheet xssfSheetEForms = xlsxService.createXLSX(workbook, sheetnameConfigurationEforms, XlsxEFormsSurchargeHeaderEnum.transform());
            List<EFormsSurcharge> eFormsSurcharges;
            List<EFormsSurcharge> byIdPlatformIsNull = eformsSurchargeRepository.findByIdPlatformIsNull();
            if (plateformeConfiguration == null) {
                eFormsSurcharges = byIdPlatformIsNull;
            } else {
                eFormsSurcharges = eformsSurchargeRepository.findByIdPlatform(plateformeConfiguration.getIdPlateforme());
                byIdPlatformIsNull.forEach(eFormsSurcharge -> {
                    if (eFormsSurcharges.stream().noneMatch(eFormsSurcharge1 -> eFormsSurcharge1.getCode().equals(eFormsSurcharge.getCode()))) {
                        eFormsSurcharges.add(eFormsSurcharge);
                    }
                });
            }
            addXlsxEforms(xssfSheetEForms, eFormsSurcharges);
            ByteArrayOutputStream outFile = new ByteArrayOutputStream();
            workbook.write(outFile);
            return outFile;
        } catch (IOException e) {
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage(), e);
        }

    }


    private void addXlsxOffreTypeProcedure(XSSFSheet xssfSheetMesRef, List<OffreTypeProcedure> offreTypeProcedures) {
        List<List<XlsxColumn>> xlsxData = getXlsxData(offreTypeProcedures);
        xlsxService.setData(xssfSheetMesRef, xlsxData, 1);
        xlsxService.autoSize(xssfSheetMesRef, XlsxAssociationAvisProcedureOrganismeHeaderEnum.values().length);
    }

    private void addXlsxOffre(XSSFSheet xssfSheetMesRef, List<Offre> offres) {
        List<List<XlsxColumn>> xlsxData = getXlsxOffreData(offres);
        xlsxService.setData(xssfSheetMesRef, xlsxData, 1);
        xlsxService.autoSize(xssfSheetMesRef, XlsxAssociationAvisHeaderEnum.values().length);
    }

    private void addXlsxProcedure(XSSFSheet xssfSheetMesRef, List<TypeProcedure> typeProcedures) {
        List<List<XlsxColumn>> xlsxData = getXlsxProcedureData(typeProcedures);
        xlsxService.setData(xssfSheetMesRef, xlsxData, 1);
        xlsxService.autoSize(xssfSheetMesRef, XlsxAssociationAvisHeaderEnum.values().length);
    }

    private void addXlsxEforms(XSSFSheet xssfSheetMesRef, List<EFormsSurcharge> eFormsSurcharges) {
        List<List<XlsxColumn>> xlsxData = getXlsxEFormsData(eFormsSurcharges);
        xlsxService.setData(xssfSheetMesRef, xlsxData, 1);
        xlsxService.autoSize(xssfSheetMesRef, XlsxEFormsSurchargeHeaderEnum.values().length);
    }


    private List<List<XlsxColumn>> getXlsxData(List<OffreTypeProcedure> content) {
        return content.stream().map(offreTypeProcedure -> {
            List<XlsxColumn> xlsxColumns = new ArrayList<>();

            xlsxColumns.add(XlsxColumn.builder().index(XlsxAssociationAvisProcedureOrganismeHeaderEnum.ACRONYME_ORGANISME.getIndex()).value(offreTypeProcedure.getAcronymeOrganisme()).type(CellType.STRING).build());
            TypeProcedure procedure = offreTypeProcedure.getProcedure();
            xlsxColumns.add(XlsxColumn.builder().index(XlsxAssociationAvisProcedureOrganismeHeaderEnum.ABREVIATION_PROCEDURE.getIndex()).value(procedure.getAbbreviation()).type(CellType.STRING).build());
            xlsxColumns.add(XlsxColumn.builder().index(XlsxAssociationAvisProcedureOrganismeHeaderEnum.LIBELLE_PROCEDURE.getIndex()).value(procedure.getLibelle()).type(CellType.STRING).build());
            Offre offre = offreTypeProcedure.getOffre();
            xlsxColumns.add(XlsxColumn.builder().index(XlsxAssociationAvisProcedureOrganismeHeaderEnum.CODE_AVIS.getIndex()).value(offre.getCode()).type(CellType.STRING).build());
            xlsxColumns.add(XlsxColumn.builder().index(XlsxAssociationAvisProcedureOrganismeHeaderEnum.LIBELLE_AVIS.getIndex()).value(offre.getLibelle()).type(CellType.STRING).build());
            return xlsxColumns;
        }).toList();
    }

    private List<List<XlsxColumn>> getXlsxProcedureData(List<TypeProcedure> content) {
        return content.stream().map(typeProcedure -> {
            List<XlsxColumn> xlsxColumns = new ArrayList<>();

            xlsxColumns.add(XlsxColumn.builder().index(XlsxTypeProcedureHeaderEnum.ID_PROCEDURE.getIndex()).value(String.valueOf(typeProcedure.getId())).type(CellType.STRING).build());
            xlsxColumns.add(XlsxColumn.builder().index(XlsxTypeProcedureHeaderEnum.ABREVIATION_PROCEDURE.getIndex()).value(typeProcedure.getAbbreviation()).type(CellType.STRING).build());
            xlsxColumns.add(XlsxColumn.builder().index(XlsxTypeProcedureHeaderEnum.LIBELLE_PROCEDURE.getIndex()).value(typeProcedure.getLibelle()).type(CellType.STRING).build());
            Boolean avisExterne = typeProcedure.getAvisExterne();

            String correspondanceTed = typeProcedure.getCorrespondanceTed();
            xlsxColumns.add(XlsxColumn.builder().index(XlsxTypeProcedureHeaderEnum.CORRESPONDANCE_EFORMS.getIndex())
                    .value(correspondanceTed).type(CellType.STRING).build());
            xlsxColumns.add(XlsxColumn.builder().index(XlsxTypeProcedureHeaderEnum.AVIS_EXTERNE.getIndex())
                    .value(avisExterne == null ? "" : String.valueOf(avisExterne)).type(CellType.STRING).build());
            return xlsxColumns;
        }).toList();
    }

    private List<List<XlsxColumn>> getXlsxOffreData(List<Offre> content) {
        return content.stream().map(offre -> {
            List<XlsxColumn> xlsxColumns = new ArrayList<>();

            xlsxColumns.add(XlsxColumn.builder().index(XlsxAssociationAvisHeaderEnum.CODE_AVIS_EUROPEEN.getIndex()).value(offre.getCode()).type(CellType.STRING).build());
            xlsxColumns.add(XlsxColumn.builder().index(XlsxAssociationAvisHeaderEnum.LIBELLE_AVIS_EUROPEEN.getIndex()).value(offre.getLibelle()).type(CellType.STRING).build());
            Offre offreAssocie = offre.getOffreAssociee();
            xlsxColumns.add(XlsxColumn.builder().index(XlsxAssociationAvisHeaderEnum.CODE_AVIS_NATIONAL.getIndex()).value(offreAssocie.getCode()).type(CellType.STRING).build());
            xlsxColumns.add(XlsxColumn.builder().index(XlsxAssociationAvisHeaderEnum.LIBELLE_AVIS_NATIONAL.getIndex()).value(offreAssocie.getLibelle()).type(CellType.STRING).build());
            return xlsxColumns;
        }).toList();
    }

    private List<List<XlsxColumn>> getXlsxEFormsData(List<EFormsSurcharge> content) {
        return content.stream().map(formsSurcharge -> {
            List<XlsxColumn> xlsxColumns = new ArrayList<>();

            xlsxColumns.add(XlsxColumn.builder().index(XlsxEFormsSurchargeHeaderEnum.CODE.getIndex()).value(formsSurcharge.getCode()).type(CellType.STRING).build());
            Boolean readonly = formsSurcharge.getReadonly();
            xlsxColumns.add(XlsxColumn.builder().index(XlsxEFormsSurchargeHeaderEnum.READONLY.getIndex()).value(readonly != null ? String.valueOf(readonly) : "").type(CellType.STRING).build());
            Boolean hidden = formsSurcharge.getHidden();
            xlsxColumns.add(XlsxColumn.builder().index(XlsxEFormsSurchargeHeaderEnum.HIDDEN.getIndex()).value(String.valueOf(hidden != null ? hidden : "")).type(CellType.STRING).build());
            String defaultValue = formsSurcharge.getDefaultValue();
            xlsxColumns.add(XlsxColumn.builder().index(XlsxEFormsSurchargeHeaderEnum.DEFAULT.getIndex()).value(defaultValue != null ? defaultValue : "").type(CellType.STRING).build());
            Boolean obligatoire = formsSurcharge.getObligatoire();
            xlsxColumns.add(XlsxColumn.builder().index(XlsxEFormsSurchargeHeaderEnum.OBLIGATOIRE.getIndex()).value(String.valueOf(obligatoire != null ? obligatoire : "")).type(CellType.STRING).build());
            xlsxColumns.add(XlsxColumn.builder().index(XlsxEFormsSurchargeHeaderEnum.ACTIF.getIndex()).value(String.valueOf(formsSurcharge.isActif())).type(CellType.STRING).build());
            String staticList = formsSurcharge.getStaticList();
            xlsxColumns.add(XlsxColumn.builder().index(XlsxEFormsSurchargeHeaderEnum.LISTE_STATIQUE.getIndex()).value(staticList != null ? staticList : "").type(CellType.STRING).build());
            String dynamicListFrom = formsSurcharge.getDynamicListFrom();
            xlsxColumns.add(XlsxColumn.builder().index(XlsxEFormsSurchargeHeaderEnum.LISTE_DYNAMIQUE.getIndex()).value(dynamicListFrom != null ? dynamicListFrom : "").type(CellType.STRING).build());
            String noticeId = formsSurcharge.getNoticeId();
            xlsxColumns.add(XlsxColumn.builder().index(XlsxEFormsSurchargeHeaderEnum.ID_NOTICE.getIndex()).value(noticeId != null ? noticeId : "").type(CellType.STRING).build());
            return xlsxColumns;
        }).toList();
    }

    private void updateTypeProcedure(List<List<String>> lists) {
        List<String> added = new ArrayList<>();
        for (List<String> strings : lists) {
            if (strings.size() < XlsxTypeProcedureHeaderEnum.values().length) {
                log.warn("Ignoring liste {}", StringUtils.join(strings, ","));
                continue;
            }
            String id = strings.get(XlsxTypeProcedureHeaderEnum.ID_PROCEDURE.getIndex());
            String procedureCode = strings.get(XlsxTypeProcedureHeaderEnum.ABREVIATION_PROCEDURE.getIndex());
            String procedureLibelle = strings.get(XlsxTypeProcedureHeaderEnum.LIBELLE_PROCEDURE.getIndex());
            String abbreviation = strings.get(XlsxTypeProcedureHeaderEnum.AVIS_EXTERNE.getIndex());
            String correspondanceTed = strings.get(XlsxTypeProcedureHeaderEnum.CORRESPONDANCE_EFORMS.getIndex());
            if (StringUtils.isBlank(id) || StringUtils.isBlank(procedureCode) || StringUtils.isBlank(id)) {
                log.warn("Ignoring liste {}", StringUtils.join(strings, ","));
                continue;
            }
            log.info("Saving liste {}", StringUtils.join(strings, ","));

            int parseInt = Integer.parseInt(id);
            TypeProcedure typeProcedure = procedureRepository.findById(parseInt).orElse(null);
            if (typeProcedure == null) {
                log.info("Procedure {} not found", procedureCode);
                typeProcedure = TypeProcedure.builder()
                        .correspondanceTed(StringUtils.isBlank(correspondanceTed) ? null : correspondanceTed)
                        .avisExterne(StringUtils.isBlank(abbreviation) ? null : Boolean.parseBoolean(abbreviation))
                        .libelle(procedureLibelle)
                        .abbreviation(procedureCode)
                        .id(parseInt)
                        .build();
                procedureRepository.save(typeProcedure);
            } else {
                typeProcedure.setCorrespondanceTed(StringUtils.isBlank(correspondanceTed) ? null : correspondanceTed);
                typeProcedure.setAvisExterne(StringUtils.isBlank(abbreviation) ? null : Boolean.parseBoolean(abbreviation));
                typeProcedure.setLibelle(procedureLibelle);
                typeProcedure.setAbbreviation(procedureCode);
                procedureRepository.save(typeProcedure);
            }

            added.add(id + "#" + procedureCode + "#" + procedureLibelle);
        }
        log.info("Fin de la mise à jour des procédures avec {} procédures ajoutées", added.size());
        log.info("Voici la liste des procédures ajoutées : {}", StringUtils.join(added, ","));

    }

    private void updateAssociationProcedureAvis(PlateformeConfiguration plateformeConfiguration, List<List<String>> lists, String suffix) {
        List<String> added = new ArrayList<>();
        for (List<String> strings : lists) {
            if (strings.size() < XlsxAssociationAvisProcedureOrganismeHeaderEnum.values().length) {
                log.warn("Ignoring liste {}", StringUtils.join(strings, ","));
                continue;
            }
            String avis = strings.get(XlsxAssociationAvisProcedureOrganismeHeaderEnum.CODE_AVIS.getIndex());
            String procedureCode = strings.get(XlsxAssociationAvisProcedureOrganismeHeaderEnum.ABREVIATION_PROCEDURE.getIndex());
            String procedureLibelle = strings.get(XlsxAssociationAvisProcedureOrganismeHeaderEnum.LIBELLE_PROCEDURE.getIndex());
            String organisme = strings.get(XlsxAssociationAvisProcedureOrganismeHeaderEnum.ACRONYME_ORGANISME.getIndex());
            if (StringUtils.isBlank(avis) || StringUtils.isBlank(procedureCode) || StringUtils.isBlank(organisme)) {
                log.warn("Ignoring liste {}", StringUtils.join(strings, ","));
                continue;
            }
            log.info("Saving liste {}", StringUtils.join(strings, ","));
            if (!StringUtils.isBlank(suffix) && !avis.endsWith(suffix)) {
                avis += suffix;
            }
            Offre offre = offreRepository.findByCode(avis).orElse(null);
            if (offre == null) {
                log.warn("Offre {} not found", avis);
                continue;
            }
            TypeProcedure typeProcedure = procedureRepository.findByAbbreviationAndLibelle(procedureCode, procedureLibelle);
            if (typeProcedure == null) {
                log.info("Procedure {} not found", procedureCode);
                continue;
            }

            OffreTypeProcedure offreTypeProcedure = offreTypeProcedureRepository.findByOffreAndProcedureAndAcronymeOrganismeAndPlateformeConfiguration(offre, typeProcedure, organisme, plateformeConfiguration);
            if (offreTypeProcedure == null) {
                offreTypeProcedure = OffreTypeProcedure.builder().acronymeOrganisme(organisme).offre(offre).procedure(typeProcedure).plateformeConfiguration(plateformeConfiguration).build();
                offreTypeProcedureRepository.save(offreTypeProcedure);
            }
            added.add(avis + "#" + procedureCode + "#" + organisme);
        }

        log.info("Fin de l'association des procédures avec {} procédures ajoutées", added.size());
        log.info("Voici la liste des associations ajoutées : {}", StringUtils.join(added, ","));

    }

    private void updateEForms(List<List<String>> lists, List<EFormsSurcharge> eFormsSurcharges) {
        List<String> added = new ArrayList<>();
        for (List<String> strings : lists) {
            if (strings.size() < XlsxEFormsSurchargeHeaderEnum.values().length) {
                log.warn("Ignoring liste {}", StringUtils.join(strings, ","));
                continue;
            }
            String code = strings.get(XlsxEFormsSurchargeHeaderEnum.CODE.getIndex());
            if (StringUtils.isBlank(code)) {
                log.warn("Ignoring liste {}", StringUtils.join(strings, ","));
                continue;
            }
            log.info("Saving liste {}", StringUtils.join(strings, ","));
            EFormsSurcharge eFormsSurcharge = eFormsSurcharges.stream().filter(eFormsSurcharge1 -> code.equals(eFormsSurcharge1.getCode())).findFirst().orElse(null);
            if (eFormsSurcharge == null) {
                eFormsSurcharge = EFormsSurcharge.builder().code(code).build();
            }
            eFormsSurcharge.setReadonly(Boolean.parseBoolean(strings.get(XlsxEFormsSurchargeHeaderEnum.READONLY.getIndex())));
            eFormsSurcharge.setHidden(Boolean.parseBoolean(strings.get(XlsxEFormsSurchargeHeaderEnum.HIDDEN.getIndex())));
            String defaultValue = strings.get(XlsxEFormsSurchargeHeaderEnum.DEFAULT.getIndex());
            eFormsSurcharge.setDefaultValue(StringUtils.isBlank(defaultValue) ? null : defaultValue);
            eFormsSurcharge.setActif(Boolean.parseBoolean(strings.get(XlsxEFormsSurchargeHeaderEnum.ACTIF.getIndex())));
            String staticList = strings.get(XlsxEFormsSurchargeHeaderEnum.LISTE_STATIQUE.getIndex());
            eFormsSurcharge.setStaticList(StringUtils.isBlank(staticList) ? null : staticList);
            String dynamicListFrom = strings.get(XlsxEFormsSurchargeHeaderEnum.LISTE_DYNAMIQUE.getIndex());
            eFormsSurcharge.setDynamicListFrom(StringUtils.isBlank(dynamicListFrom) ? null : dynamicListFrom);
            String noticeId = strings.get(XlsxEFormsSurchargeHeaderEnum.ID_NOTICE.getIndex());
            eFormsSurcharge.setNoticeId(StringUtils.isBlank(noticeId) ? null : noticeId);
            eFormsSurcharge = eformsSurchargeRepository.save(eFormsSurcharge);
            added.add(eFormsSurcharge.getCode());

        }
        log.info("Fin de la mise à jour des surcharges avec {} surcharges ajoutées", added.size());
        log.info("Voici la liste des surcharges ajoutées : {}", StringUtils.join(added, ","));
        eFormsSurcharges.stream().filter(eFormsSurcharge -> !added.contains(eFormsSurcharge.getCode())).forEach(eFormsSurcharge -> {
            log.info("Suppression de la surcharge avec {}", eFormsSurcharge.getCode());
            eformsSurchargeRepository.delete(eFormsSurcharge);
        });
    }

    private void updateAssociationAvisEuropeensAvisNational(List<List<String>> lists) {
        offreRepository.resetOffreAssociee();
        List<String> added = new ArrayList<>();
        for (List<String> strings : lists) {
            if (strings.size() < XlsxAssociationAvisHeaderEnum.values().length) {
                log.warn("Ignoring liste {}", StringUtils.join(strings, ","));
                continue;
            }
            String codeAvisEuropeen = strings.get(XlsxAssociationAvisHeaderEnum.CODE_AVIS_EUROPEEN.getIndex());
            String codeAvisNational = strings.get(XlsxAssociationAvisHeaderEnum.CODE_AVIS_NATIONAL.getIndex());
            if (StringUtils.isBlank(codeAvisEuropeen) || StringUtils.isBlank(codeAvisNational)) {
                log.warn("Ignoring liste {}", StringUtils.join(strings, ","));
                continue;
            }
            log.info("Saving liste {}", StringUtils.join(strings, ","));

            Offre offreEuropeen = offreRepository.findByCode(codeAvisEuropeen).orElse(null);
            if (offreEuropeen == null) {
                log.warn("Offre {} not found", codeAvisEuropeen);
                continue;
            }
            Offre offreNational = offreRepository.findByCode(codeAvisNational).orElse(null);
            if (offreNational == null) {
                log.warn("Offre {} not found", codeAvisNational);
                continue;
            }
            offreEuropeen.setOffreAssociee(offreNational);
            offreEuropeen = offreRepository.save(offreEuropeen);
            added.add(codeAvisEuropeen + "#" + codeAvisNational);
        }
        log.info("Fin de l'association des avis  européens / nationaux avec {} avis ajoutées", added.size());
        log.info("Voici la liste des associations des avis  européens / nationaux ajoutées : {}", StringUtils.join(added, ","));
    }
}
