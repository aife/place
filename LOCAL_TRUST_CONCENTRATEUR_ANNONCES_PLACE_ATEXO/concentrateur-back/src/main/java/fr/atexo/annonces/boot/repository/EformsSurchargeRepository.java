package fr.atexo.annonces.boot.repository;


import fr.atexo.annonces.boot.entity.EFormsSurcharge;
import fr.atexo.annonces.config.DaoRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface EformsSurchargeRepository extends DaoRepository<EFormsSurcharge, Long> {

    EFormsSurcharge findByCodeAndIdPlatformAndNoticeIdIsNullAndActifIsTrue(String code, String idPlatform);

    EFormsSurcharge findByCodeAndIdPlatformIsNullAndNoticeIdIsNullAndActifIsTrue(String code);

    EFormsSurcharge findByCodeAndIdPlatformAndNoticeIdAndActifIsTrue(String code, String idPlatform, String noticeId);


    List<EFormsSurcharge> findByIdPlatform(String idPlatform);

    List<EFormsSurcharge> findByIdPlatformIsNull();

    EFormsSurcharge findByCodeAndIdPlatformIsNullAndNoticeIdAndActifIsTrue(String id, String noticeId);

    Optional<EFormsSurcharge> findByNoticeIdAndCodeAndIdPlatform(String idNotice, String code, String idPlatform);

    Optional<EFormsSurcharge> findByNoticeIdAndCodeAndIdPlatformIsNull(String idNotice, String code);

    Optional<EFormsSurcharge> findByCodeAndIdPlatformAndNoticeIdIsNull(String code, String idPlatform);

    Optional<EFormsSurcharge> findByCodeAndIdPlatformIsNullAndNoticeIdIsNull(String code);

    @Modifying
    @Transactional
    @Query("update EFormsSurcharge c set c.actif=false where c.noticeId is not null and c.code = :code and c.idPlatform = :plateforme")
    void disableByNoticeIdAndCodeAndIdPlatform(String code, String plateforme);

    @Modifying
    @Transactional
    @Query("update EFormsSurcharge c set c.actif=false where c.noticeId is not null and c.code = :code and c.idPlatform is null")
    void disableByNoticeIdAndCodeAndIdPlatformIsNull(String code);
}
