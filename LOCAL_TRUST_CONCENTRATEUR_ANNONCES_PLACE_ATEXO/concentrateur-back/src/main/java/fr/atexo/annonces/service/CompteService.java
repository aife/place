package fr.atexo.annonces.service;

import eu.europa.publications.resource.schema.ted.r2_0_9.reception.TedEsenders;
import fr.atexo.annonces.boot.entity.tncp.SuiviAvisPublie;
import fr.atexo.annonces.dto.ConfigurationConsultationDTO;
import fr.atexo.annonces.dto.SuiviDTO;

public interface CompteService {

    SuiviDTO envoyer(ConfigurationConsultationDTO envoi, SuiviAvisPublie suivi, TedEsenders tedEsenders, String token, boolean rie);
}
