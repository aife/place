package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.entity.Agent;
import fr.atexo.annonces.boot.entity.Organisme;
import fr.atexo.annonces.boot.entity.ReferentielServiceMpe;
import fr.atexo.annonces.boot.repository.AgentRepository;
import fr.atexo.annonces.boot.repository.OrganismeRepository;
import fr.atexo.annonces.boot.repository.ReferentielMpeRepository;
import fr.atexo.annonces.dto.AgentDTO;
import fr.atexo.annonces.dto.OrganismeDTO;
import fr.atexo.annonces.dto.ReferentielDTO;
import fr.atexo.annonces.mapper.AgentMapper;
import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ReferentielMpeService {

    private final ReferentielMpeRepository referentielMpeRepository;
    private final OrganismeRepository organismeRepository;
    private final AgentRepository agentRepository;
    private final AgentMapper agentMapper;

    public ReferentielMpeService(ReferentielMpeRepository referentielMpeRepository, OrganismeRepository organismeRepository, AgentRepository agentRepository, AgentMapper agentMapper) {
        this.referentielMpeRepository = referentielMpeRepository;
        this.organismeRepository = organismeRepository;
        this.agentRepository = agentRepository;
        this.agentMapper = agentMapper;
    }

    private ReferentielServiceMpe setReferentialService(ReferentielServiceMpe referential) {
        if (referential == null) {
            return null;
        }
        if (referential.getOrganisme() == null) {
            throw new AtexoException(ExceptionEnum.MANDATORY, "L'organisme du référentiel est obligatoire");
        }
        String code = referential.getCode();
        log.info("Récupération référentiel : code = {}", code);
        Long idMpe = referential.getIdMpe();
        Integer id = referential.getOrganisme().getId();
        ReferentielServiceMpe serviceMpe = referentielMpeRepository.findByCodeAndOrganismeIdAndIdMpe(code, id, idMpe).orElse(null);
        if (serviceMpe == null) {
            serviceMpe = referentielMpeRepository.findByCodeAndOrganismeIdAndIdMpeIsNull(code, id).orElse(null);
            if (serviceMpe != null) {
                serviceMpe.setIdMpe(idMpe);
                return referentielMpeRepository.save(serviceMpe);
            }
        } else {
            return serviceMpe;
        }
        return referentielMpeRepository.save(referential);
    }

    public ReferentielServiceMpe getReferential(ReferentielServiceMpe referential) {
        if (referential == null) {
            return null;
        }
        return setReferentialService(referential);
    }


    public List<OrganismeDTO> getOrganismes(String plateforme) {
        return organismeRepository.findByPlateformeUuid(plateforme).stream()
                .map(organismeEntity -> OrganismeDTO.builder()
                        .id(organismeEntity.getId())
                        .acronymeOrganisme(organismeEntity.getAcronymeOrganisme())
                        .libelleOrganisme(organismeEntity.getLibelleOrganisme())
                        .plateformeUuid(organismeEntity.getPlateformeUuid())
                        .plateformeUrl(organismeEntity.getPlateformeUrl())
                        .sigleOrganisme(organismeEntity.getSigleOrganisme())
                        .services(getReferentials(organismeEntity))
                        .build())
                .collect(java.util.stream.Collectors.toList());
    }

    private List<ReferentielDTO> getReferentials(Organisme organismeEntity) {
        return organismeEntity.getServices().stream().map(referentielMpeEntity -> ReferentielDTO.builder()
                .id(referentielMpeEntity.getId())
                .code(referentielMpeEntity.getCode())
                .libelle(referentielMpeEntity.getLibelle())
                .build()).collect(java.util.stream.Collectors.toList());
    }

    public List<AgentDTO> getAgents(String idPlatform) {
        List<Agent> byOrganismePlateformeUuid = agentRepository.findByOrganismePlateformeUuid(idPlatform)
                .stream().collect(Collectors.groupingBy(Agent::getIdentifiant, Collectors.toList()))
                .values().stream().map(agents -> agents.get(0))
                .collect(Collectors.toList());
        return agentMapper.mapToModels(byOrganismePlateformeUuid);
    }
}
