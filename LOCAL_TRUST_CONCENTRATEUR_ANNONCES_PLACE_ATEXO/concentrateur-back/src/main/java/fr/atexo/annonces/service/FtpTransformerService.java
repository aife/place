package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.entity.SuiviAnnonce;

/**
 * Transformer permettant de retourner un byte[] prêt à être envoyé via FTP à partir des informations d'un objet
 */
public interface FtpTransformerService {

    byte[] exportPdfToByteArray(SuiviAnnonce suiviAnnonceDTO);

    byte[] exportXmlToByteArray(SuiviAnnonce suiviAnnonceDTO);
    byte[] exportXmlTransactionToByteArray(SuiviAnnonce suiviAnnonceDTO);
}
