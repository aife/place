package fr.atexo.annonces.dto.tncp;

public enum StatutEnum {
    FINI, ERREUR, EN_COURS, EN_ATTENTE
}
