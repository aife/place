package fr.atexo.annonces.boot.ws.edition_en_ligne;

import org.springframework.stereotype.Component;

@Component
public class EditionEnLignWsAdapter implements EditionEnLigneWsPort {

    private final EditionEnLigneWS editionEnLigneWS;

    public EditionEnLignWsAdapter(EditionEnLigneWS editionEnLigneWS) {
        this.editionEnLigneWS = editionEnLigneWS;
    }

    @Override
    public String getEditionUrl(String token) {
        return editionEnLigneWS.getEditionUrl(token);
    }
}
