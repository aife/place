package fr.atexo.annonces.reseau;

import fr.atexo.annonces.boot.configuration.http_logging.LogEntryExit;
import fr.atexo.annonces.dto.ConsultationFiltreDTO;
import fr.atexo.annonces.dto.OffreDTO;
import fr.atexo.annonces.dto.SupportDTO;
import fr.atexo.annonces.service.SupportService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Point d'entrée web pour manipuler les supports
 */
@RestController
@RequestMapping("/rest/v2/supports")
@Tag(name = "Supports")
public class SupportController {

    private final SupportService supportService;

    public SupportController(SupportService supportService) {
        this.supportService = supportService;
    }

    @GetMapping
    @Operation(
            description = "swagger.supports.get.operation.summary",
            summary = "swagger.supports.get.operation.notes",
            responses = {@ApiResponse(responseCode = HttpServletResponse.SC_UNAUTHORIZED + "", description = "swagger.response.unauthorized")})
    @LogEntryExit(skip = true)
    public List<SupportDTO> getSupports(
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.montant", example = "50000") Double montant,
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.national") Boolean national,
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.europeen") Boolean europeen,
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.jal") List<String> jal,
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.pqr") Boolean allJal,
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.pqr") List<String> pqr,
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.pqr") Boolean allPqr,
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.typeProcedures") List<String> typeProcedures,
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.organismes") List<String> organismes,
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.bloc") List<String> blocs,
            @RequestParam(required = false) @Parameter(example = "OFFRES", name = "swagger.supports.get.param.include") SupportIncludeParameterEnum include,
            @RequestParam String idPlatform, @RequestParam(required = false) Integer idConsultation) {
        boolean includeOffres = SupportIncludeParameterEnum.OFFRES.equals(include);
        return supportService.getSupports(montant, national, europeen, jal, pqr, includeOffres, idPlatform, idConsultation, typeProcedures, organismes, blocs, allJal, allPqr);
    }


    @GetMapping("filtre")
    public ConsultationFiltreDTO getFiltre(@RequestParam String idPlatform, @RequestParam Integer idConsultation, @RequestParam(required = false) Boolean europeen) {
        return supportService.getFiltreSupport(idConsultation, idPlatform, europeen);
    }

    @GetMapping("plateforme/offres")
    public List<OffreDTO> getPlateformeOffres(
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.montant", example = "50000") Double montant,
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.national") Boolean national,
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.europeen") Boolean europeen,
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.jal") List<String> jal,
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.pqr") List<String> pqr,
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.typeProcedures") List<String> typeProcedures,
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.organismes") List<String> organismes,
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.bloc") List<String> blocs,
            @RequestParam String idPlatform, @RequestParam(required = false) Integer idConsultation) {
        return supportService.getPlateformeOffres(montant, national, europeen, jal, pqr, idPlatform, idConsultation, typeProcedures, organismes, blocs);
    }

    @GetMapping("plateforme/codeGroupe")
    public List<String> getSupportCodeGroupe(
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.typeProcedures") List<String> typeProcedures,
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.organismes") List<String> organismes,
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.bloc") List<String> blocs,
            @RequestParam String idPlatform) {
        return supportService.getSupportCodeGroupe(idPlatform, typeProcedures, organismes, blocs);
    }

    @GetMapping("plateforme/supports")
    public List<SupportDTO> getSupportPlateforme(
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.typeProcedures") List<String> typeProcedures,
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.organismes") List<String> organismes,
            @RequestParam(required = false) @Parameter(name = "swagger.supports.get.param.bloc") List<String> blocs,
            @RequestParam String idPlatform) {
        return supportService.getSupportPlateforme(idPlatform, typeProcedures, organismes, blocs);
    }
}
