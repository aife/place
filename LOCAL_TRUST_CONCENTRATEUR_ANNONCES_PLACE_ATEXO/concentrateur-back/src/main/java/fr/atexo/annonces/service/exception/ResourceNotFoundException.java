package fr.atexo.annonces.service.exception;

import java.text.MessageFormat;

/**
 * Exception levée si la ressource demandée est introuvable en base de données
 */
public class ResourceNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 5009347726595562266L;
    private static final String NOT_FOUND_MESSAGE_WITH_ID = "L''objet {0} ayant pour identifiant {1} n''existe pas";
    private static final String NOT_FOUND_MESSAGE = "L''objet {0} ayant pour {1} {2} n''existe pas";

    public ResourceNotFoundException(String resourceType, Long idResource) {
        super(MessageFormat.format(NOT_FOUND_MESSAGE_WITH_ID, resourceType, idResource));
    }

    public ResourceNotFoundException(String resourceType, String identifierProperty, String identifierValue) {
        super(MessageFormat.format(NOT_FOUND_MESSAGE, resourceType, identifierProperty, identifierValue));
    }
}
