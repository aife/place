package fr.atexo.annonces.boot.core.domain.e_forms_sdk.model;

import fr.atexo.annonces.config.XmlElement;
import fr.atexo.annonces.dto.EformsSurchargeDTO;
import lombok.*;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SubNoticeMetadata {

    private String id;
    private String contentType;
    private String displayType;
    private String description;
    private String _label;
    private boolean readOnly;
    private boolean forceHidden = false;
    private boolean extended;
    private Boolean collapsed;
    private boolean _repeatable;
    private String _presetValue;
    private List<String> _idSchemes;
    private String _idScheme;
    private String _identifierFieldId;
    private String nodeId;
    private List<SubNoticeMetadata> content;
    private List<String> fieldIds;
    private boolean hidden;
    private boolean obligatoire;
    private EformsSurchargeDTO surcharge;

    private String xpathAbsolute;
    private List<XmlElement> elements;
    private Map<String, List<XmlElement>> subElements;

}
