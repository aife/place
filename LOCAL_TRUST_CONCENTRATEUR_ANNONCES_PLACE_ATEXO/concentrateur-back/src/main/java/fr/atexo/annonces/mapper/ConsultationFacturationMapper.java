package fr.atexo.annonces.mapper;

import fr.atexo.annonces.boot.entity.ConsultationFacturation;
import fr.atexo.annonces.config.DaoMapper;
import fr.atexo.annonces.dto.ConsultationFacturationDTO;
import org.mapstruct.Mapper;

@Mapper
public interface ConsultationFacturationMapper extends DaoMapper<ConsultationFacturation, ConsultationFacturationDTO> {


}
