package fr.atexo.annonces.boot.entity;

import fr.atexo.annonces.modeles.ModeEformsEnum;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "PLATEFORME_CONFIGURATION")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlateformeConfiguration implements Serializable {

    private static final long serialVersionUID = -1410232521211913768L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "ID_PLATEFORME")
    private String idPlateforme;
    @Column(name = "SUPPORTS")
    private String supports;

    @Column(name = "EUROPEEN")
    private boolean europeen;

    @Column(name = "NATIONAL")
    private boolean national;

    @Column(name = "EFORMS_MODAL")
    private boolean eformsModal;

    @Column(name = "PAYS", length = 3)
    private String pays;

    @Column(name = "PQR")
    private boolean pqr;

    @Column(name = "JAL")
    private boolean jal;

    @Column(name = "GROUPEMENT")
    private boolean groupement;

    @Column(name = "VALIDATION_OBLIGATOIRE")
    private boolean validationObligatoire;

    @Column(name = "ASSOCIATION_PROCEDURE")
    private boolean associationProcedure;

    @Column(name = "ASSOCIATION_ORGANISME")
    private boolean associationOrganisme;

    @Column(name = "PROV")
    private Integer prov;

    @Lob
    @Column(name = "EFORMS_TOKEN")
    private String eformsToken;

    @Column(name = "CRON_MASTER")
    private boolean cronMaster;

    @Column(name = "MODE_EFORMS")
    @Enumerated(EnumType.STRING)
    private ModeEformsEnum modeEforms;

}
