package fr.atexo.annonces.service.exception;

/**
 * Exception levée si les arguments fournis au service ne sont pas compatibles entre eux.
 */
public class InconsistentArgumentsException extends RuntimeException {

    private static final long serialVersionUID = 8337652247232685196L;

    public InconsistentArgumentsException(String message) {
        super(message);
    }
}
