package fr.atexo.annonces.boot.repository;

import fr.atexo.annonces.boot.entity.Departement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository pour accéder aux objets Departement
 */
@Repository
public interface DepartementRepository extends JpaRepository<Departement, Integer> {

}
