package fr.atexo.annonces.mapper.tncp;

import com.atexo.annonces.commun.mpe.AcheteurMpe;
import eu.europa.publications.resource.schema.ted.r2_0_9.reception.TedEsenders;
import fr.atexo.annonces.commun.tncp.Compte;
import fr.atexo.annonces.mapper.Mappable;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CompteMapper extends Mappable<AcheteurMpe, Compte> {

    @Override
    @Mapping(target = "id", source = "acheteur.login")
    Compte map(AcheteurMpe acheteur, TedEsenders tedEsenders, boolean rie);

}
