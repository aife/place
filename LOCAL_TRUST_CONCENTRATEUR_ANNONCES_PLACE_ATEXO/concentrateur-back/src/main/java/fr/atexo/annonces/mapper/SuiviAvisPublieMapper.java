package fr.atexo.annonces.mapper;

import fr.atexo.annonces.boot.entity.tncp.SuiviAvisPublie;
import fr.atexo.annonces.config.DaoMapper;
import fr.atexo.annonces.dto.SuiviDTO;
import org.mapstruct.Mapper;

@Mapper
public interface SuiviAvisPublieMapper extends DaoMapper<SuiviAvisPublie, SuiviDTO> {


}
