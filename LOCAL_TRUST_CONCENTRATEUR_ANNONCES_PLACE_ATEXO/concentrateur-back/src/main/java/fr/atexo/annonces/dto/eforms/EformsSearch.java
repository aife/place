package fr.atexo.annonces.dto.eforms;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.time.ZonedDateTime;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EformsSearch {
    private String id;
    private String versionId;
    private ZonedDateTime submittedAfter;
    private ZonedDateTime submittedBefore;
    private ZonedDateTime publishedAfter;
    private ZonedDateTime publishedBefore;
    private List<String> statuses;

}
