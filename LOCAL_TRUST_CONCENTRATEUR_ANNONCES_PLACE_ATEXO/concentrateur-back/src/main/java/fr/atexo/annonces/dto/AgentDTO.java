package fr.atexo.annonces.dto;

import lombok.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class AgentDTO implements Serializable {
    private static final long serialVersionUID = 2992867365312767348L;
    private String id;

    private String identifiant;

    private String acheteurPublic;

    private String prenom;

    private String nom;

    private OrganismeResumeDTO organisme;

    private ZonedDateTime datePremierAcces;

    private ZonedDateTime dateDernierAcces;

    private String email;

    private String telephone;

    private ReferentielDTO service;

    private String idMpe;

}
