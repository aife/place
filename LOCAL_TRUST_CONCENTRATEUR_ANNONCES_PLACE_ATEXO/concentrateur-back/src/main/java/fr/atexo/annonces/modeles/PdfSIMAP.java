package fr.atexo.annonces.modeles;

/**
 * Created by qba on 29/03/16.
 */
public class PdfSIMAP {

    private String pdfEncodedBase64;

    public String getPdfEncodedBase64() {
        return pdfEncodedBase64;
    }

    public void setPdfEncodedBase64(String value) {
        pdfEncodedBase64 = value;
    }


}
