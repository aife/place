package fr.atexo.annonces.boot.entity;

import lombok.*;
import org.springframework.context.annotation.Lazy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "offre")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"code"})
public class Offre implements Serializable {

    private static final long serialVersionUID = 6421193064827311152L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;

    @Column(name = "CODE")
    private String code;

    @Column(name = "LIBELLE", length = 5000)
    private String libelle;

    @Lazy
    @Column(name = "AVIS_SIMPLIFIE_TEXTE_WS")
    private String avisSimplifieTexteWs;

    @Lazy
    @Column(name = "AVIS_STANDARD_TEXTE_WS")
    private String avisStandardTexteWs;

    @Lazy
    @Column(name = "ASSOCIATION_JAL")
    private Boolean associationJal;

    @Lazy
    @Column(name = "DOCUMENT_EXTENSION")
    private String documentExtension;

    @Lazy
    @Column(name = "CONVERT_TO_PDF")
    private boolean convertToPdf;


    @Lazy
    @Column(name = "modification_date_mise_en_ligne")
    private boolean modificationDateMiseEnLigne;


    @Lazy
    @Column(name = "MONTANT_MIN")
    private Double montantMin;

    @Lazy
    @Column(name = "MONTANT_MAX")
    private Double montantMax;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "support_offre_asso", inverseJoinColumns = @JoinColumn(name = "ID_SUPPORT"), joinColumns = @JoinColumn(name = "ID_OFFRE"))
    private List<Support> supports;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "offre")
    private Set<OffreTypeProcedure> typeProceduresOrganisme;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_OFFRE_ASSOCIEE", referencedColumnName = "ID")
    private Offre offreAssociee;
}
