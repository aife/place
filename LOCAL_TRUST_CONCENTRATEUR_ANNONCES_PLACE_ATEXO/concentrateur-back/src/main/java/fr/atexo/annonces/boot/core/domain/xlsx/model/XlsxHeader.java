package fr.atexo.annonces.boot.core.domain.xlsx.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.poi.ss.usermodel.CellType;

import java.util.List;

@Getter
@AllArgsConstructor
@Builder
@Setter
public class XlsxHeader {
    private int index;
    private String title;
    private List<String> values;
    private CellType type;
}
