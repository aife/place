package fr.atexo.annonces.boot.core.domain.e_forms_sdk.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Namespaces {
    private String prefix;
    private String uri;
    private String schemaLocation;
}
