package fr.atexo.annonces.service;

import fr.atexo.annonces.reseau.HttpFileParameter;
import org.springframework.messaging.Message;
import org.springframework.util.MultiValueMap;

import java.util.List;

/**
 * Transformer permettant de retourner des objets à envoyer dans le corps d'une requête HTTP, voire de construire
 * l'ensemble des paramètres d'une requête HTTP
 */
public interface HttpTransformerService {

    MultiValueMap<String, Object> buildLesEchosFileParameters(List<HttpFileParameter> fileParameters);

    HttpFileParameter buildLesEchosXmlFileParameter(String xml);

    HttpFileParameter buildLesEchosPdfFileParameter(Message<?> message);
}
