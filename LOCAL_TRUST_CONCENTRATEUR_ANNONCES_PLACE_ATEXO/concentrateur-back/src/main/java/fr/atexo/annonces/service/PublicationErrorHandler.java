package fr.atexo.annonces.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

/**
 * Permet de définir un comportement spécifique pour toute exception levée durant la routine de demande de publication
 */
@Service
@Slf4j
public class PublicationErrorHandler {

    private final SuiviAnnonceService suiviAnnonceService;

    public PublicationErrorHandler(SuiviAnnonceService suiviAnnonceService) {
        this.suiviAnnonceService = suiviAnnonceService;
    }

    public void handleError(Integer idAnnonce, Exception e) {
        log.error("Erreur Publication {} => {}", e.getClass(), e.getMessage());
        suiviAnnonceService.updateErrorMessage(idAnnonce, e.getMessage(), e instanceof MessagingException);
    }
}
