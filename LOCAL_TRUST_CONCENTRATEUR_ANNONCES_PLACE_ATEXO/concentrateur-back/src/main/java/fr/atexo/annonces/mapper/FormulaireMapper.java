package fr.atexo.annonces.mapper;


import fr.atexo.annonces.boot.core.domain.e_forms_notice.model.EFormsFormulaire;
import fr.atexo.annonces.boot.entity.EFormsFormulaireEntity;
import fr.atexo.annonces.config.DaoMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface FormulaireMapper extends DaoMapper<EFormsFormulaireEntity, EFormsFormulaire> {

    EFormsFormulaireEntity mapToEntity(EFormsFormulaire model);

    @Mapping(target = "idAnnonce", source = "suiviAnnonce.id")
    @Mapping(target = "idTypeAvisPub", source = "suiviAnnonce.typeAvisPub.id")
    @Mapping(target = "previousId", source = "previous.id")
    @Mapping(target = "nextId", source = "next.id")
    EFormsFormulaire mapToModel(EFormsFormulaireEntity entity);
}
