package fr.atexo.annonces.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * DTO - Représente un support de publication
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ConsultationFiltreDTO {

    private int id;

    @NotNull
    private Integer idConsultation;


    @NotNull
    private String idPlatform;

    private List<String> codeDepartements;

    private boolean jal;

    private boolean pqr;

    private Double montant;

    private boolean national;
    private boolean europeen;
}
