package fr.atexo.annonces.mapper.tncp;

import com.atexo.annonces.commun.mpe.LotMpe;
import eu.europa.publications.resource.schema.ted.r2_0_9.reception.TedEsenders;
import fr.atexo.annonces.commun.tncp.LotItem;
import fr.atexo.annonces.commun.tncp.TncpTypeDecisionEnum;
import fr.atexo.annonces.mapper.Mappable;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class LotMapper implements Mappable<LotMpe, LotItem> {

    @NonNull
    private CpvMapper cpvMapper;

    @Override
    public LotItem map(final LotMpe lot, TedEsenders tedEsenders, boolean rie) {
        var result = LotItem.with();

        result.cpvPrincipal(cpvMapper.map(lot.getCodeCpvPrincipal(), tedEsenders,rie));
        result.decision(null); // TODO
        result.description(lot.getDescriptionSuccinte());
        result.lieuExecution(null); // TODO
        result.montant(null); // TODO
        result.numero(lot.getNumero());
        result.unite(null); // TODO
        result.valeur(null); // TODO
        result.decision(TncpTypeDecisionEnum.fromValue(lot.getTypeDecision()));
        return result.build();
    }
}
