package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.dto.AvisPublieDTO;
import fr.atexo.annonces.dto.mol.Request;
import fr.atexo.annonces.dto.mol.Response;
import fr.atexo.annonces.dto.publication.AVISEMIS;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;

public interface MolService extends CompteService {
    String generateXmlResponse(String request, String soap);

    List<Response.AvisConcentrateur> getPublicationSynchro(Request request);

    List<AVISEMIS.AVIS> getAvisToSynchronize(Date dateDebut, Date dateFin);

    void synchronize(AVISEMIS.AVIS avis, SuiviAnnonce suiviAnnonce, AVISEMIS.AVIS.SUPPORT support);

    List<SuiviAnnonce> getAnnonces(AVISEMIS.AVIS avis, String idPlatform, String idConsultationMpe, String organisme, AVISEMIS.AVIS.SUPPORT support);

    AvisPublieDTO addPublicationSynchroneMOL(String synchrone, MultipartFile annonceFile, MultipartFile complementFile);

    List<AvisPublieDTO> getAvisPublies(Request request);

}
