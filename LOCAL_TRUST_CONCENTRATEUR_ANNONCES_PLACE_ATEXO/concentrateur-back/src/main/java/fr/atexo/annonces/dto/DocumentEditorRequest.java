package fr.atexo.annonces.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Map;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class DocumentEditorRequest {
    @NotEmpty
    @NotNull
    private String documentId;
    @NotEmpty
    @NotNull
    private String plateformeId;
    private Integer version;
    @NotEmpty
    @NotNull
    private String documentTitle;
    @NotEmpty
    @NotNull
    private String callback;
    private Map<String, String> headersCallback;
    private String mode;
    @NotNull
    private User user;
}
