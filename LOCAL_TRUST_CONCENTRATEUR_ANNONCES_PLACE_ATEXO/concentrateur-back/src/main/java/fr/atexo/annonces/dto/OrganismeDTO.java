package fr.atexo.annonces.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class OrganismeDTO {
    private Integer id;

    private String sigleOrganisme;

    private String acronymeOrganisme;

    private String libelleOrganisme;

    private String plateformeUuid;

    private String plateformeUrl;

    private List<ReferentielDTO> services;


}
