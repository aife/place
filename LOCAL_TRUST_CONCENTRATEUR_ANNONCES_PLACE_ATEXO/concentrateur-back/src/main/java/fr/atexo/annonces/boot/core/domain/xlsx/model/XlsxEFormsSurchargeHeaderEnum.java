package fr.atexo.annonces.boot.core.domain.xlsx.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@Getter
@AllArgsConstructor
public enum XlsxEFormsSurchargeHeaderEnum {

    CODE(0, "Code"),
    READONLY(1, "Readonly"),
    HIDDEN(2, "Hidden"),
    DEFAULT(3, "Default"),
    OBLIGATOIRE(4, "Obligatoire"),
    ACTIF(5, "Actif"),
    LISTE_STATIQUE(6, "Liste statique"),
    LISTE_DYNAMIQUE(7, "Liste dynamique"),
    ID_NOTICE(8, "type d'avis");

    private final int index;
    private final String title;

    public static List<XlsxHeader> transform() {
        return Arrays
                .stream(XlsxEFormsSurchargeHeaderEnum.values())
                .map(xlsxProjetAchatHeaderEnum -> XlsxHeader
                        .builder()
                        .index(xlsxProjetAchatHeaderEnum.getIndex())
                        .title(xlsxProjetAchatHeaderEnum.getTitle())
                        .build())
                .toList();
    }
}
