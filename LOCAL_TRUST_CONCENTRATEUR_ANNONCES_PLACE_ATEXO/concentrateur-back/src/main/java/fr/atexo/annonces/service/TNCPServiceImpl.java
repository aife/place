package fr.atexo.annonces.service;

import com.atexo.annonces.commun.mpe.AcheteurMpe;
import eu.europa.publications.resource.schema.ted.r2_0_9.reception.TedEsenders;
import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.boot.entity.tncp.SuiviAvisPublie;
import fr.atexo.annonces.boot.entity.tncp.SuiviStatut;
import fr.atexo.annonces.boot.repository.SuiviAnnonceRepository;
import fr.atexo.annonces.boot.repository.tncp.SuiviRepository;
import fr.atexo.annonces.boot.ws.tncp.TncpWS;
import fr.atexo.annonces.commun.tncp.*;
import fr.atexo.annonces.config.EFormsHelper;
import fr.atexo.annonces.dto.ConfigurationConsultationDTO;
import fr.atexo.annonces.dto.SuiviDTO;
import fr.atexo.annonces.mapper.Mappable;
import fr.atexo.annonces.modeles.StatutSuiviAnnonceEnum;
import fr.atexo.annonces.modeles.SupportPublicationEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Slf4j

public class TNCPServiceImpl implements TNCPService {
    @Value("${tncp.piste.client-id}")
    private String clientId;

    @Value("${tncp.piste.client-secret}")
    private String clientSecret;

    private final SuiviService suiviService;
    private final TncpWS tncpWS;
    private final SuiviAnnonceRepository suiviAnnonceRepository;

    private final SuiviRepository suiviRepository;
    private final Mappable<ConfigurationConsultationDTO, Avis> avisMapper;


    public TNCPServiceImpl(SuiviService suiviService, Mappable<ConfigurationConsultationDTO, Avis> avisMapper, TncpWS tncpWS, SuiviAnnonceRepository suiviAnnonceRepository, SuiviRepository suiviRepository) {
        this.suiviService = suiviService;
        this.avisMapper = avisMapper;
        this.tncpWS = tncpWS;

        this.suiviAnnonceRepository = suiviAnnonceRepository;
        this.suiviRepository = suiviRepository;
    }


    @Override
    public SuiviDTO envoyer(ConfigurationConsultationDTO configuration, SuiviAvisPublie suivi, TedEsenders tedEsenders, String tokenAgent, boolean rie) {
        try {
            log.info("envoi PUB TNCP avec le compte {}", suivi.getAcheteur());
            suivi.setEnvoi(Instant.now());
            suivi.setContexteConsultation(EFormsHelper.getString(configuration.getContexte()));
            Token token = tncpWS.authentifier(Client.with().clientId(clientId).clientSecret(clientSecret).build());
            suivi.setToken(token.getAccessToken());
            log.info("Token récupéré depuis TNCP = " + token);
            var acheteur = suivi.getAcheteur();
            configuration.getContexte().setAcheteur(AcheteurMpe.with().email(acheteur.getEmail()).login(acheteur.getLogin()).password(acheteur.getPassword()).build());

            Avis avis = avisMapper.map(configuration, tedEsenders, Boolean.TRUE.equals(rie));
            suivi.setAvis(EFormsHelper.getString(avis));

            Redirection redirection = tncpWS.publier(token, avis);
            log.info("URL récupérée depuis TNCP = " + redirection);
            String string = redirection.uri().toString();
            suivi.setRedirection(string);
            Map<String, String> queryParams = parseQueryParameters(redirection.uri().getQuery());

            // Retrieve a specific query parameter
            String redirectionContextId = queryParams.get("redirection_context_id");
            suivi.setUuid(redirectionContextId);

            suivi.setStatut(SuiviStatut.ENVOYE);
        } catch (Exception e) {
            log.error("Erreur lors de la publication de l'avis", e);
            suivi.setStatut(SuiviStatut.ERREUR);
        }


        return suiviService.sauvegarder(suivi);

    }

    private static Map<String, String> parseQueryParameters(String query) throws UnsupportedEncodingException {
        Map<String, String> queryParams = new HashMap<>();
        if (query != null) {
            String[] pairs = query.split("&");
            for (String pair : pairs) {
                String[] keyValue = pair.split("=");
                if (keyValue.length == 2) {
                    String key = URLDecoder.decode(keyValue[0], StandardCharsets.UTF_8);
                    String value = URLDecoder.decode(keyValue[1], StandardCharsets.UTF_8);
                    queryParams.put(key, value);
                }
            }
        }
        return queryParams;
    }

    @Override
    public void synchronize(String response) {
        log.info("Dernière notification TNCP = " + response);
        updateTncp(response);

    }

    public List<SuiviAnnonce> updateTncp(String response) {
        TncpNotificationResponse notificationResponse = EFormsHelper.getObjectFromString(response, TncpNotificationResponse.class);
        if (notificationResponse == null || notificationResponse.response() == null || CollectionUtils.isEmpty(notificationResponse.response().notifications()))
            return null;
        return notificationResponse.response().notifications().stream().filter(notification -> StringUtils.hasText(notification.donneesNotif()) && notification.bulleMetierInfos() != null && "PUBLICITE".equals(notification.bulleMetierInfos().bulleMetier())).map(notification -> {
            DonneeNotification donneeNotification = EFormsHelper.getObjectFromString(notification.donneesNotif(), DonneeNotification.class);
            if (donneeNotification == null || donneeNotification.publicationDetails() == null || donneeNotification.publicationStatus() == null || donneeNotification.publicationStatus().annonceId() == null || donneeNotification.identifiers() == null || donneeNotification.identifiers().consultationId() == null) {
                log.error("Donnée de notification TNCP invalide : {}", notification.donneesNotif());
                return null;
            }
            return donneeNotification;
        }).filter(Objects::nonNull).map(donneeNotification -> {
            PublicationStatus publicationStatus = donneeNotification.publicationStatus();
            Identifiers identifiers = donneeNotification.identifiers();
            PublicationDetails publicationDetails = donneeNotification.publicationDetails();
            SuiviAvisPublie suivi = suiviService.rechercher(publicationStatus.annonceId()).orElse(null);
            SuiviAnnonce suiviAnnonce = null;
            if (suivi == null) {
                suiviAnnonce = suiviAnnonceRepository.findAllByIdConsultationAndSupportCodeGroupe(Integer.parseInt(donneeNotification.identifiers().consultationId()), "TNCP").stream().findFirst().orElseThrow(() -> new RuntimeException("Impossible de trouver le suivi de l'annonce " + publicationStatus.annonceId()));
                suivi = suiviRepository.save(SuiviAvisPublie.with().statut(SuiviStatut.CREE).uuid(publicationStatus.annonceId()).suiviAnnonce(suiviAnnonce).build());
            } else if (suivi.getSuiviAnnonce() == null) {
                suiviAnnonce = suiviAnnonceRepository.findAllByIdConsultationAndSupportCodeGroupe(Integer.parseInt(donneeNotification.identifiers().consultationId()), "TNCP").stream().findFirst().orElseThrow(() -> new RuntimeException("Impossible de trouver le suivi de l'annonce " + publicationStatus.annonceId()));
                suivi.setSuiviAnnonce(suiviAnnonce);
            }
            suivi.setLastNotification(EFormsHelper.getString(donneeNotification));
            if (publicationStatus.state().equals("PU_PU")) {
                suivi.setStatut(SuiviStatut.PUBLIE);
            } else if (publicationStatus.state().equals("RE")) {
                suivi.setStatut(SuiviStatut.REFUSE);
            }
            String idAnnonce = identifiers.noticeId();
            suiviAnnonce.setIdAnnonce(idAnnonce);
            suivi.setRefPublication(idAnnonce);
            String offrePublication = identifiers.noticeTitle();
            suiviAnnonce.setOffrePublication(offrePublication);
            suivi.setAvis(offrePublication);
            if (identifiers.boampDateSent() != null)
                suiviAnnonce.setDateSoumission(identifiers.boampDateSent().toInstant());
            if (identifiers.tedDateSent() != null)
                suiviAnnonce.setDateSoumissionTed(identifiers.tedDateSent().toInstant());
            suiviAnnonce.setSupportPublication(SupportPublicationEnum.TNCP);

            PublicationDetail boamp = publicationDetails.boampPublication();
            if (boamp != null) {
                String id = boamp.id();
                suiviAnnonce.setNumeroAvis(id);
                if (id != null && id.equals(publicationStatus.journalOfficielId())) {
                    suiviAnnonce.setStatutBoamp(publicationStatus.state());
                }
                if (boamp.date() != null) {
                    suiviAnnonce.setDatePublication(boamp.date().toInstant());
                    suivi.setStatut(SuiviStatut.PUBLIE);
                }
                if (boamp.url() != null) {
                    suiviAnnonce.setLienPublication(boamp.url());
                }
            }
            PublicationDetail ted = publicationDetails.tedPublication();
            if (ted != null) {
                String id = ted.id();
                suiviAnnonce.setNumeroJoue(id);
                if (id != null && id.equals(publicationStatus.journalOfficielId())) {
                    suiviAnnonce.setStatutJoue(publicationStatus.state());
                }
                if (ted.date() != null) {
                    suiviAnnonce.setDatePublicationTed(ted.date().toInstant());
                }
                if (ted.url() != null) {
                    suiviAnnonce.setLienPublicationTed(ted.url());
                }
            }
            StatutSuiviAnnonceEnum suiviStatut = StatutSuiviAnnonceEnum.getStatutSuiviAnnonceFromSuiviStatut(suivi.getStatut());
            if (suiviStatut != null) {
                suiviAnnonce.setStatut(suiviStatut);
            }
            suivi = suiviRepository.save(suivi);
            return suiviAnnonceRepository.save(suiviAnnonce);
        }).collect(Collectors.toList());

    }
}
