package fr.atexo.annonces.dto;

import fr.atexo.annonces.modeles.StatutSuiviAnnonceEnum;
import lombok.*;

import java.io.Serializable;

/**
 * DTO - Représente une demande de publication
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SuiviAnnonceUpdate implements Serializable {

    private StatutSuiviAnnonceEnum statut;

}
