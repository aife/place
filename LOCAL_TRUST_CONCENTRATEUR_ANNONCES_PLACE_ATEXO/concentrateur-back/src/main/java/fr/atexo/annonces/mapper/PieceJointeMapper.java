package fr.atexo.annonces.mapper;

import fr.atexo.annonces.boot.entity.PieceJointe;
import fr.atexo.annonces.config.DaoMapper;
import fr.atexo.annonces.dto.PieceJointeDTO;
import org.mapstruct.Mapper;

@Mapper
public interface PieceJointeMapper extends DaoMapper<PieceJointe, PieceJointeDTO> {


}
