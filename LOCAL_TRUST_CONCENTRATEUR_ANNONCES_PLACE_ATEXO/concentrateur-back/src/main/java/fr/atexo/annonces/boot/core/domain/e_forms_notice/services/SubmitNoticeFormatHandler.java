package fr.atexo.annonces.boot.core.domain.e_forms_notice.services;

import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNoticeMetadata;
import fr.atexo.annonces.boot.entity.EFormsFormulaireEntity;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;

public interface SubmitNoticeFormatHandler {

    Object processGroup(Object formulaireObjet, SubNoticeMetadata group, EFormsFormulaireEntity saved, String nodeStop, Map<String, List<SubmitNoticeFormatHandler>> kownHandlers, ZonedDateTime dateEnvoi);

    Object processElement(SubNoticeMetadata item, Object formulaireObjet, String id, EFormsFormulaireEntity saved, String nodeStop, ZonedDateTime dateEnvoi);


}
