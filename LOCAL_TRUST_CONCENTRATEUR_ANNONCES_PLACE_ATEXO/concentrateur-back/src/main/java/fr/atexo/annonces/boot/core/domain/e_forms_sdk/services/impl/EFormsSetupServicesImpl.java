package fr.atexo.annonces.boot.core.domain.e_forms_sdk.services.impl;

import fr.atexo.annonces.boot.core.domain.e_forms_sdk.services.IEFormsSetupServices;
import fr.atexo.annonces.boot.ws.eforms.SchematronConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

import static fr.atexo.annonces.config.EFormsHelper.getSdk;

@Service
@Slf4j
public class EFormsSetupServicesImpl implements IEFormsSetupServices {


    private final SchematronConfiguration schematronConfiguration;

    public EFormsSetupServicesImpl(SchematronConfiguration schematronConfiguration) {
        this.schematronConfiguration = schematronConfiguration;
        try {
            this.initialize();
        } catch (Exception e) {
            log.error("Erreur lors de l'initialisation de EFORMS : {}", e.getMessage());
        }
    }

    @Override
    public void initialize() {
        Set<String> tags = new HashSet<>();
        tags.add(schematronConfiguration.getResourcesTag());
        String resourcesDir = schematronConfiguration.getDownloadDir();
        String resourcesUrl = schematronConfiguration.getResourcesUrl();
        String prefixZipFile = schematronConfiguration.getPrefixZipFile();
        tags.forEach(tag -> getSdk(resourcesDir, resourcesUrl, prefixZipFile, tag));

    }


}
