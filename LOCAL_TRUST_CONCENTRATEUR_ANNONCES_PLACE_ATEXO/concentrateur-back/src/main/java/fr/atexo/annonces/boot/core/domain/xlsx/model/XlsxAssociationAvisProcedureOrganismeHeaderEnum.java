package fr.atexo.annonces.boot.core.domain.xlsx.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@Getter
@AllArgsConstructor
public enum XlsxAssociationAvisProcedureOrganismeHeaderEnum {

    ACRONYME_ORGANISME(0, "acronyme Organisme"),
    ABREVIATION_PROCEDURE(1, "abreviation Procedure"),
    LIBELLE_PROCEDURE(2, "libelle Procedure"),
    CODE_AVIS(3, "Code Avis"),
    LIBELLE_AVIS(4, "Libellé Avis");

    private final int index;
    private final String title;

    public static List<XlsxHeader> transform() {
        return Arrays
                .stream(XlsxAssociationAvisProcedureOrganismeHeaderEnum.values())
                .map(xlsxProjetAchatHeaderEnum -> XlsxHeader
                        .builder()
                        .index(xlsxProjetAchatHeaderEnum.getIndex())
                        .title(xlsxProjetAchatHeaderEnum.getTitle())
                        .build())
                .toList();
    }
}
