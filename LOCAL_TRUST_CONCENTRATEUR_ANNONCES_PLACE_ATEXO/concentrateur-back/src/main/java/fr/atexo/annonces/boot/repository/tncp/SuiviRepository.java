package fr.atexo.annonces.boot.repository.tncp;

import fr.atexo.annonces.boot.entity.tncp.SuiviAvisPublie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface SuiviRepository extends JpaRepository<SuiviAvisPublie, Long> {

	Optional<SuiviAvisPublie> findByUuid(String uuid);

}

