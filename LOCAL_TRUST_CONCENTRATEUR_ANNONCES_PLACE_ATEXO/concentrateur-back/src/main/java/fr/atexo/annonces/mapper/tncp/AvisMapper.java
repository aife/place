package fr.atexo.annonces.mapper.tncp;

import com.atexo.annonces.commun.mpe.OrganismeMpe;
import com.atexo.annonces.commun.mpe.ServiceMpe;
import eu.europa.publications.resource.schema.ted.r2_0_9.reception.TedEsenders;
import fr.atexo.annonces.boot.ws.mpe.model.ConsultationContexte;
import fr.atexo.annonces.commun.tncp.Avis;
import fr.atexo.annonces.commun.tncp.Payload;
import fr.atexo.annonces.config.EFormsHelper;
import fr.atexo.annonces.dto.ConfigurationConsultationDTO;
import fr.atexo.annonces.mapper.Mappable;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class AvisMapper implements Mappable<ConfigurationConsultationDTO, Avis> {

    @NonNull
    private UtilisateurMapper utilisateurMapper;

    @NonNull
    private ProfilMapper profilMapper;

    @NonNull
    private ConsultationMapper consultationMapper;

    @NonNull
    private ReferencesMapper referencesMapper;

    @NonNull
    private CompteMapper compteMapper;

    @Override
    public Avis map(final ConfigurationConsultationDTO config, TedEsenders tedEsenders, boolean rie) {
        ConsultationContexte contexte = config.getContexte();
        log.info("Construction du mapping à partir de : {}", EFormsHelper.getString(contexte));
        var consultation = contexte.getConsultation();
        OrganismeMpe organisme = contexte.getOrganismeConsultation();
        ServiceMpe service = Boolean.TRUE.equals(consultation.getOrganismeDecentralise()) ? contexte.getServiceConsultation() :
                ServiceMpe.with().organisme(organisme.getDenomination())
                        .codePostal(organisme.getCodePostal())
                        .adresse(organisme.getAdresse())
                        .sigle(organisme.getSigle())
                        .siren(organisme.getSiren())
                        .siret(organisme.getSiret())
                        .build();
        var agent = contexte.getAgent();
        var compte = contexte.getAcheteur();

        if (service == null) {
            log.warn("Service is null for consultation {}", consultation.getId());
            service = contexte.getServiceAgent();
        }
        String siret = service != null ? service.getSiret() : null;
        if (siret == null) {
            siret = organisme != null ? organisme.getSiret() : null;
        }
        String serviceAvis = service != null ? service.getSigle() + "-" + service.getId() : null;
        if (serviceAvis == null) {
            serviceAvis = organisme != null ? organisme.getAcronyme() + "-" + organisme.getId() : null;
        }
        return Avis.with()
                .utilisateur(utilisateurMapper.map(agent, tedEsenders, rie))
                .siret(siret)
                .targetApplication(rie ? "avis-de-publicite-rie" : "avis-de-publicite")
                .service(serviceAvis)
                .profil(profilMapper.map(consultation, tedEsenders, rie))
                .payload(Payload.with()
                        .consultation(consultationMapper.map(contexte, tedEsenders, rie))
                        .references(referencesMapper.map(consultation, tedEsenders, rie))
                        .compte(compteMapper.map(compte, tedEsenders, rie))
                        .build())

                .build();
    }
}
