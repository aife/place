package fr.atexo.annonces.service.exception;

import lombok.Getter;

@Getter
public class AcheteurException extends AtexoException {

    public AcheteurException(AcheteurExceptionEnum acheteurExceptionEnum, String errorMessage, Throwable err) {
        super(acheteurExceptionEnum.getCode(), errorMessage, err);

    }

    public AcheteurException(AcheteurExceptionEnum acheteurExceptionEnum, String errorMessage) {
        super(acheteurExceptionEnum.getCode(), errorMessage);

    }
}
