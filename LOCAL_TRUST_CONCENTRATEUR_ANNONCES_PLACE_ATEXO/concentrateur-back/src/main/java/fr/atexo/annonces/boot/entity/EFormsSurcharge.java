package fr.atexo.annonces.boot.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "e_forms_surcharge", uniqueConstraints = {@UniqueConstraint(columnNames = {"code", "id_platform", "NOTICE_ID"})}, indexes = {@Index(name = "surcharge_code_plateforme_avis_index", columnList = "code,id_platform,NOTICE_ID")})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor

public class EFormsSurcharge implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "id_platform")
    private String idPlatform;

    @Column(name = "code", nullable = false)
    private String code;

    @Lob
    @Column(name = "default_value")
    private String defaultValue;

    @Column(name = "hidden")
    private Boolean hidden;

    @Column(name = "readonly")
    private Boolean readonly;

    @Column(name = "obligatoire")
    private Boolean obligatoire;

    @Column(name = "ACTIF", nullable = false)
    private boolean actif;

    @Column(name = "dynamic_list_from")
    private String dynamicListFrom;

    @Column(name = "static_list")
    private String staticList;

    @Column(name = "ordre")
    private Integer ordre;

    @Column(name = "NOTICE_ID")
    private String noticeId;


}
