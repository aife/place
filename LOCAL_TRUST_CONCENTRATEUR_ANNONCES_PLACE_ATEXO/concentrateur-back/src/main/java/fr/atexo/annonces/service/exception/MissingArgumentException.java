package fr.atexo.annonces.service.exception;

/**
 * Exception levée si une donnée d'entrée n'est pas présente mais est nécessaire à l'exécution
 */
public class MissingArgumentException extends RuntimeException {

    private static final long serialVersionUID = 4550138445346468976L;

    public MissingArgumentException(String message) {
        super(message);
    }
}
