package fr.atexo.annonces.dto;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SuiviTypeAvisPubPatch implements Serializable {


    @NotEmpty
    private List<JalDTO> jalList;

    private ConsultationFacturationDTO facturation;

}
