package fr.atexo.annonces.config;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.NoticeFormatHandler;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.PreviousNoticeFormatHandler;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.SubmitNoticeFormatHandler;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.impl.*;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.XmlStructureDetails;
import fr.atexo.annonces.boot.repository.EformsSurchargeRepository;
import fr.atexo.annonces.boot.ws.eforms.SchematronConfiguration;
import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.Resource;
import org.springframework.util.CollectionUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.yaml.snakeyaml.Yaml;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@Slf4j
public final class EFormsHelper {

    private static final int BUFFER_SIZE = 4096;
    public static Map<String, String> nutsToDepartement = new HashMap<>();
    private static final ObjectMapper OBJECT_MAPPER;
    private static final String ERROR_MESSAGE = "Le xlsx ne doit pas être null";

    static {
        nutsToDepartement = new HashMap<>();
        nutsToDepartement.put("FR101", "75");
        nutsToDepartement.put("FR102", "77");
        nutsToDepartement.put("FR103", "78");
        nutsToDepartement.put("FR104", "91");
        nutsToDepartement.put("FR105", "92");
        nutsToDepartement.put("FR106", "93");
        nutsToDepartement.put("FR107", "94");
        nutsToDepartement.put("FR108", "95");
        nutsToDepartement.put("FRB01", "18");
        nutsToDepartement.put("FRB02", "28");
        nutsToDepartement.put("FRB03", "36");
        nutsToDepartement.put("FRB04", "37");
        nutsToDepartement.put("FRB05", "41");
        nutsToDepartement.put("FRB06", "45");
        nutsToDepartement.put("FRC11", "21");
        nutsToDepartement.put("FRC12", "58");
        nutsToDepartement.put("FRC13", "71");
        nutsToDepartement.put("FRC14", "89");
        nutsToDepartement.put("FRC21", "25");
        nutsToDepartement.put("FRC22", "39");
        nutsToDepartement.put("FRC23", "70");
        nutsToDepartement.put("FRC24", "90");
        nutsToDepartement.put("FRD11", "14");
        nutsToDepartement.put("FRD12", "50");
        nutsToDepartement.put("FRD13", "61");
        nutsToDepartement.put("FRD21", "27");
        nutsToDepartement.put("FRD22", "76");
        nutsToDepartement.put("FRE11", "59");
        nutsToDepartement.put("FRE12", "62");
        nutsToDepartement.put("FRE21", "02");
        nutsToDepartement.put("FRE22", "60");
        nutsToDepartement.put("FRE23", "80");
        nutsToDepartement.put("FRF11", "67");
        nutsToDepartement.put("FRF12", "68");
        nutsToDepartement.put("FRF21", "08");
        nutsToDepartement.put("FRF22", "10");
        nutsToDepartement.put("FRF23", "51");
        nutsToDepartement.put("FRF24", "52");
        nutsToDepartement.put("FRF31", "54");
        nutsToDepartement.put("FRF32", "55");
        nutsToDepartement.put("FRF33", "57");
        nutsToDepartement.put("FRF34", "88");
        nutsToDepartement.put("FRG01", "44");
        nutsToDepartement.put("FRG02", "49");
        nutsToDepartement.put("FRG03", "53");
        nutsToDepartement.put("FRG04", "72");
        nutsToDepartement.put("FRG05", "85");
        nutsToDepartement.put("FRH01", "22");
        nutsToDepartement.put("FRH02", "29");
        nutsToDepartement.put("FRH03", "35");
        nutsToDepartement.put("FRH04", "56");
        nutsToDepartement.put("FRI11", "24");
        nutsToDepartement.put("FRI12", "33");
        nutsToDepartement.put("FRI13", "40");
        nutsToDepartement.put("FRI14", "47");
        nutsToDepartement.put("FRI15", "64");
        nutsToDepartement.put("FRI21", "19");
        nutsToDepartement.put("FRI22", "23");
        nutsToDepartement.put("FRI23", "87");
        nutsToDepartement.put("FRI31", "16");
        nutsToDepartement.put("FRI32", "17");
        nutsToDepartement.put("FRI33", "79");
        nutsToDepartement.put("FRI34", "86");
        nutsToDepartement.put("FRJ11", "11");
        nutsToDepartement.put("FRJ12", "30");
        nutsToDepartement.put("FRJ13", "34");
        nutsToDepartement.put("FRJ14", "48");
        nutsToDepartement.put("FRJ15", "66");
        nutsToDepartement.put("FRJ21", "09");
        nutsToDepartement.put("FRJ22", "12");
        nutsToDepartement.put("FRJ23", "31");
        nutsToDepartement.put("FRJ24", "32");
        nutsToDepartement.put("FRJ25", "46");
        nutsToDepartement.put("FRJ26", "65");
        nutsToDepartement.put("FRJ27", "81");
        nutsToDepartement.put("FRJ28", "82");
        nutsToDepartement.put("FRK11", "03");
        nutsToDepartement.put("FRK12", "15");
        nutsToDepartement.put("FRK13", "43");
        nutsToDepartement.put("FRK14", "63");
        nutsToDepartement.put("FRK21", "01");
        nutsToDepartement.put("FRK22", "07");
        nutsToDepartement.put("FRK23", "26");
        nutsToDepartement.put("FRK24", "38");
        nutsToDepartement.put("FRK25", "42");
        nutsToDepartement.put("FRK26", "69");
        nutsToDepartement.put("FRK27", "73");
        nutsToDepartement.put("FRK28", "74");
        nutsToDepartement.put("FRL01", "04");
        nutsToDepartement.put("FRL02", "05");
        nutsToDepartement.put("FRL03", "06");
        nutsToDepartement.put("FRL04", "13");
        nutsToDepartement.put("FRL05", "83");
        nutsToDepartement.put("FRL06", "84");
        nutsToDepartement.put("FRM01", "2A");
        nutsToDepartement.put("FRM02", "2B");
        nutsToDepartement.put("FRY10", "971");
        nutsToDepartement.put("FRY20", "972");
        nutsToDepartement.put("FRY30", "973");
        nutsToDepartement.put("FRY40", "974");
        nutsToDepartement.put("FRY50", "976");
        OBJECT_MAPPER = new ObjectMapper();
        OBJECT_MAPPER.disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
        OBJECT_MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        OBJECT_MAPPER.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        OBJECT_MAPPER.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        //   OBJECT_MAPPER.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

        OBJECT_MAPPER.registerModule(new JavaTimeModule());
    }

    private EFormsHelper() {
        throw new IllegalStateException("Utility class");
    }

    public static boolean hasToDownloadDirectory(String file) {
        if (file == null) {
            return false;
        }
        File path = new File(file);
        if (path.exists()) {
            return false;
        }

        path.mkdirs();
        log.info("****************** {} Folder reinitialized successfully ******************", file);

        return true;
    }

    public static void cleanBatchFiles(String file) {
        if (file == null) {
            return;
        }
        try {
            File pathXmlInputResourceFile = new File(file);
            final File parent = pathXmlInputResourceFile.getParentFile();
            if (Files.exists(parent.toPath())) {
                Files.deleteIfExists(pathXmlInputResourceFile.toPath());
            } else {
                Files.createDirectory(parent.toPath());
            }
            log.info("****************** {} File deleted successfully ******************", file);

        } catch (IOException e) {
            log.error("****************** Failed to delete the file {} ******************", file, e);
        }
    }

    public static void decompressFile(String zipFilePath) {
        if (zipFilePath == null) {
            return;
        }
        final File file = new File(zipFilePath);

        try {
            switch (FilenameUtils.getExtension(zipFilePath)) {
                case "gz":
                    decompressGzip(file, zipFilePath);
                    break;
                case "zip":
                    unzip(file);
                    break;
                default:
                    throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, "Extension non reconnue");
            }

            Files.deleteIfExists(file.toPath());
        } catch (IOException e) {
            log.error("Erreur lors de la décompression => {}", e.getMessage());
        }
    }

    private static void unzip(File file) throws IOException {
        if (file == null) {
            return;
        }
        try (ZipInputStream zipIn = new ZipInputStream(new FileInputStream(file))) {
            String destDirectory = file.getParentFile().getAbsolutePath();
            ZipEntry entry = zipIn.getNextEntry();
            // iterates over entries in the zip file
            while (entry != null) {
                String filePath = destDirectory + File.separator + entry.getName();
                if (!entry.isDirectory()) {
                    // if the entry is a file, extracts it
                    extractFile(zipIn, filePath);
                } else {
                    // if the entry is a directory, make the directory
                    File dir = new File(filePath);
                    Files.createDirectories(dir.toPath());
                }
                zipIn.closeEntry();
                entry = zipIn.getNextEntry();
            }
        }
    }

    public static void decompressGzip(File source, String zipFilePath) throws IOException {
        if (source == null || zipFilePath == null) {
            return;
        }
        try (GZIPInputStream gis = new GZIPInputStream(new FileInputStream(source)); FileOutputStream fos = new FileOutputStream(zipFilePath.replace(".gz", ""))) {
            // copy GZIPInputStream to FileOutputStream
            byte[] buffer = new byte[BUFFER_SIZE];
            int len;
            while ((len = gis.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }

        }

    }

    private static void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        if (zipIn == null || filePath == null) {
            return;
        }
        final File file = new File(filePath);
        Files.deleteIfExists(file.toPath());
        try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file))) {
            byte[] bytesIn = new byte[BUFFER_SIZE];
            int read;
            while ((read = zipIn.read(bytesIn)) != -1) {
                bos.write(bytesIn, 0, read);
            }
        }
    }

    public static void download(String urlInputResource, String pathXmlInputResource) {
        if (urlInputResource == null || pathXmlInputResource == null) {
            return;
        }
        try (InputStream inputStream = new URL(urlInputResource).openStream()) {
            File file = new File(pathXmlInputResource);
            file.deleteOnExit();
            Files.copy(inputStream, file.toPath());
        } catch (Exception e) {
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage(), e);
        }
    }

    public static void getSdk(String downloadDir, String resourcesUrl, String prefixZipFile, String tag) {
        String folder = downloadDir + prefixZipFile + tag;
        boolean hasToDownload = EFormsHelper.hasToDownloadDirectory(folder);
        if (!hasToDownload) {
            return;
        }
        log.info("Le dossier du tag {} n'existe pas, on le télécharge", tag);
        String file = downloadDir + tag + ".zip";
        String url = resourcesUrl + tag + ".zip";
        EFormsHelper.download(url, file);

        EFormsHelper.decompressFile(file);
        EFormsHelper.cleanBatchFiles(file);
    }

    public static <T> T getObjectFromFile(String fileName, Class<T> tClass) throws IOException {
        Path pathSource = Paths.get(fileName).toAbsolutePath();
        ObjectMapper objectMapper = getObjectMapper();
        return objectMapper.readValue(pathSource.toFile(), tClass);
    }

    public static <T> T getObjectFromFile(File string, Class<T> tClass) {
        try {
            ObjectMapper objectMapper = getObjectMapper();
            return objectMapper.readValue(string, tClass);
        } catch (IOException e) {
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage(), e);
        }
    }

    public static <T> T getObjectFromString(String string, Class<T> tClass) {
        if (string == null) {
            return null;
        }
        try {
            ObjectMapper objectMapper = getObjectMapper();
            return objectMapper.readValue(string, tClass);
        } catch (JsonProcessingException e) {
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage(), e);
        }
    }

    public static <T> String getString(T o) {
        if (o == null) {
            return null;
        }
        ObjectMapper objectMapper = getObjectMapper();
        try {
            return objectMapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            log.error("Erreur converting object to string {}", e.getMessage());
            return o.toString();
        }
    }


    public static boolean containsNonEmptyValues(JsonNode node) {
        if (node == null) {
            return false;
        }
        if (node.isArray()) {
            for (JsonNode field : node) {
                if (!field.isNull() && isNotEmptyArray(field) && isNotEmptyArrayWithElements(field)) {
                    return true;
                }
            }
        } else {
            return !node.isNull() && isNotEmptyArray(node) && isNotEmptyArrayWithElements(node);
        }
        return false;
    }

    public static boolean isNotEmptyArray(JsonNode node) {
        return !node.isArray() || !node.isEmpty();
    }

    public static boolean isNotEmptyArrayWithElements(JsonNode node) {
        if (node.isArray()) {
            for (JsonNode element : node) {
                if (element.isTextual() && element.textValue().isEmpty()) {
                    return false;
                }
            }
        }
        return true;
    }

    public static Object merge(Object local, Object remote) throws AtexoException {
        try {

            JsonNode node = valueToTree(local);
            JsonNode node1 = valueToTree(remote);
            if (node.isEmpty()) {
                return remote;
            }
            if (node1.isEmpty()) {
                return local;
            }
            if (node.isArray() || node1.isArray()) {
                return local;
            }
            ObjectNode localNode = (ObjectNode) node;
            ObjectNode remoteNode = (ObjectNode) node1;

            ObjectReader updater = getObjectMapper().readerForUpdating(localNode);
            Object merged = updater.readValue(remoteNode);
            return getObjectFromString(getString(merged), Object.class);
        } catch (Exception e) {
            log.error("Erreur lors du merge des objets {} et {}", local, remote);
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage(), e);
        }
    }

    public static Map<String, Map<String, String>> getMapping(Resource mappingMpeTncp) {
        if (mappingMpeTncp == null) {
            return new HashMap<>();
        }
        try {
            Yaml yaml = new Yaml();
            Map<String, Object> map = yaml.load(mappingMpeTncp.getInputStream());
            return OBJECT_MAPPER.readValue(OBJECT_MAPPER.writeValueAsString(map), Map.class);
        } catch (Exception e) {
            log.error("Erreur lors du chargement du mapping {}", e.getMessage());
            return new HashMap<>();
        }
    }

    public static Map<String, Object> getMappingObjet(Resource mappingMpeTncp) {
        if (mappingMpeTncp == null) {
            return new HashMap<>();
        }
        try {
            Yaml yaml = new Yaml();
            Map<String, Object> map = yaml.load(mappingMpeTncp.getInputStream());
            return OBJECT_MAPPER.readValue(OBJECT_MAPPER.writeValueAsString(map), Map.class);
        } catch (Exception e) {
            log.error("Erreur lors du chargement du mapping {}", e.getMessage());
            return new HashMap<>();
        }
    }

    public static ObjectMapper getObjectMapper() {
        return OBJECT_MAPPER;
    }

    public static Document loadXsdDocument(String inputName) {

        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(false);
        factory.setIgnoringElementContentWhitespace(true);
        factory.setIgnoringComments(true);
        Document doc = null;

        try {
            final DocumentBuilder builder = factory.newDocumentBuilder();
            final File inputFile = new File(inputName);
            doc = builder.parse(inputFile);
        } catch (final Exception e) {
            log.error("Erreur lors du chargement de xsd : {}", e.getMessage());
        }

        return doc;
    }

    public static Element addXPathToXml(String xpath, boolean isArray, boolean isGroup, String nodeValue, Document document, Element root) {
        if ("/*".equals(xpath)) {
            return root;
        }
        if (nodeValue != null) {
            nodeValue = nodeValue.replaceAll("\n", " ");
        }
        Element parentNode;
        if (root == null) {
            parentNode = document.getDocumentElement();
        } else {
            parentNode = root;
        }
        log.debug("process xpath relative -> {} dans {} avec la valeur {}", xpath, parentNode.getNodeName(), nodeValue);
        Element child = null;

        try {
            if (!StringUtils.isBlank(xpath)) {
                List<XmlElement> pathValuesplit = getXpathElementList(xpath);
                int i = pathValuesplit.size();

                for (int j = 0; j < i; j++) {
                    child = null;
                    XmlElement xmlElement = pathValuesplit.get(j);
                    String field = xmlElement.getElementName();
                    List<String> properties = xmlElement.getProperties();
                    if (!CollectionUtils.isEmpty(properties) && properties.stream().anyMatch(s -> s.contains("text()"))) {
                        for (String property : properties) {
                            if (property.contains("text()")) {
                                List<XmlElement> conditions = getXpathElementList(property);
                                boolean verified = checkCondition(conditions, parentNode);
                                if (!verified) {
                                    return parentNode;
                                }
                            }
                        }

                    }

                    String parentNodeName = parentNode.getNodeName();
                    if (parentNodeName.equals(field)) {
                        continue;
                    }
                    NodeList childNodes = parentNode.getChildNodes();
                    if (!isArray || j == (i - 1)) for (int k = 0; k < childNodes.getLength(); k++) {
                        Node item = childNodes.item(k);
                        String name = item.getNodeName();
                        if (field.equals(name) && item instanceof Element) {
                            child = (Element) item;
                            if (!isArray) {
                                log.debug("    - récupération de l'élément {} dans {}", field, parentNode.getNodeName());
                                break;
                            }
                        }
                    }
                    log.debug("    - process xpath relative {} dans {}", field, parentNode.getNodeName());


                    if (child == null && !field.startsWith("@")) {
                        log.debug("    - ajout de l'élément {} dans {}", field, parentNode.getNodeName());

                        child = document.createElement(field);
                        if (j == (i - 1) && !isGroup && StringUtils.isBlank(child.getTextContent())) {
                            log.debug("    - ajout de la valeur [{}] de l'élément [{}]", nodeValue, child.getNodeName());
                            child.appendChild(document.createTextNode(nodeValue == null ? "" : nodeValue));
                        }
                        parentNode.appendChild(child);
                    } else if (child != null && !isGroup && !field.startsWith("@") && j == (i - 1) && StringUtils.isBlank(child.getTextContent())) {
                        log.debug("    - ajout de la valeur [{}] de l'élément [{}]", nodeValue, child.getNodeName());
                        child.appendChild(document.createTextNode(nodeValue == null ? "" : nodeValue));
                    }

                    if (!CollectionUtils.isEmpty(properties) && child != null) {
                        for (String property : properties) {
                            if (property.startsWith("@") && property.contains("=")) {
                                String[] split1 = property.split("=");
                                String name = split1[0];
                                String value = split1[1];
                                String propertyValue = value.replaceAll("'", "");
                                String propertyKey = name.replace("@", "");
                                log.debug("    - ajout de la propriété {} de l'élément {} avec la valeur {}", propertyKey, child.getNodeName(), propertyValue);
                                child.setAttribute(propertyKey, propertyValue);
                            } else {
                                log.debug("ignore property {} ", property);
                            }
                        }
                    }

                    if (field.startsWith("@")) {
                        String propertyKey = field.replace("@", "");
                        if (propertyKey.contains("=")) {
                            String[] split1 = propertyKey.split("=");
                            propertyKey = split1[0];
                            String propertyValue = split1[1].replaceAll("'", "");
                            log.debug("    - ajout de la propriété {} de l'élément {} avec la valeur {}", propertyKey, parentNode.getNodeName(), propertyValue);
                            parentNode.setAttribute(propertyKey, propertyValue);
                        } else if (nodeValue != null) {
                            log.debug("    - ajout de la propriété {} de l'élément {} avec la valeur {}", propertyKey, parentNode.getNodeName(), nodeValue);
                            parentNode.setAttribute(propertyKey, nodeValue);
                        }
                    }
                    if (child != null) {
                        parentNode = child;
                    }
                }

            }
        } catch (Exception e) {
            log.error("Erreur lors du process de {} / {}", xpath, nodeValue, e);
        }

        return child == null ? parentNode : child;
    }

    private static boolean checkCondition(List<XmlElement> conditions, Element parentNode) {
        return true;
    }

    public static Element addGroupXPathToXml(String xpath, boolean isArray, boolean isGroup, String nodeValue, Document document, Element root) {
        if ("/*".equals(xpath)) {
            return root;
        }
        if (nodeValue != null) {
            nodeValue = nodeValue.replaceAll("\n", " ");
        }
        Element parentNode;
        if (root == null) {
            parentNode = document.getDocumentElement();
        } else {
            parentNode = root;
        }
        log.debug("process xpath relative -> {} dans {} avec la valeur {}", xpath, parentNode.getNodeName(), nodeValue);
        Element child = null;

        try {
            if (!StringUtils.isBlank(xpath)) {
                List<XmlElement> pathValuesplit = getXpathElementList(xpath);
                int i = pathValuesplit.size();

                for (int j = 0; j < i; j++) {
                    child = null;
                    XmlElement xmlElement = pathValuesplit.get(j);
                    String field = xmlElement.getElementName();


                    String parentNodeName = parentNode.getNodeName();
                    if (parentNodeName.equals(field)) {
                        continue;
                    }
                    NodeList childNodes = parentNode.getChildNodes();
                    if (!isArray) {
                        for (int k = 0; k < childNodes.getLength(); k++) {
                            Node item = childNodes.item(k);
                            String name = item.getNodeName();
                            if (field.equals(name) && item instanceof Element) {
                                if (xmlElement.getProperties().stream().anyMatch(s -> s.contains("@listName="))) {
                                    List<XmlElement> property = getXpathElementList(xmlElement.getProperties().stream().filter(s -> s.contains("@listName=")).findFirst().get());
                                    if (hasListName(property, (Element) item)) {
                                        child = (Element) item;
                                        log.debug("    - récupération de l'élément {} dans {}", field, parentNode.getNodeName());
                                        break;
                                    }
                                } else {
                                    child = (Element) item;
                                    log.debug("    - récupération de l'élément {} dans {}", field, parentNode.getNodeName());
                                    break;
                                }
                            }
                        }
                    }
                    log.debug("    - process xpath relative {} dans {}", field, parentNode.getNodeName());


                    if (child == null && !field.startsWith("@")) {
                        log.debug("    - ajout de l'élément {} dans {}", field, parentNode.getNodeName());

                        child = document.createElement(field);
                        if (j == (i - 1) && !isGroup && StringUtils.isBlank(child.getTextContent())) {
                            log.debug("    - ajout de la valeur [{}] de l'élément [{}]", nodeValue, child.getNodeName());
                            child.appendChild(document.createTextNode(nodeValue == null ? "" : nodeValue));
                        }
                        parentNode.appendChild(child);
                    } else if (child != null && !isGroup && !field.startsWith("@") && j == (i - 1) && StringUtils.isBlank(child.getTextContent())) {
                        log.debug("    - ajout de la valeur [{}] de l'élément [{}]", nodeValue, child.getNodeName());
                        child.appendChild(document.createTextNode(nodeValue == null ? "" : nodeValue));
                    }

                    if (!CollectionUtils.isEmpty(xmlElement.getProperties())) {
                        for (String property : xmlElement.getProperties()) {
                            if (property.startsWith("@") && property.contains("=")) {
                                String[] split1 = property.split("=");
                                String name = split1[0];
                                String value = split1[1];
                                String propertyValue = value.replaceAll("'", "");
                                String propertyKey = name.replace("@", "");
                                log.debug("    - ajout de la propriété {} de l'élément {} avec la valeur {}", propertyKey, child.getNodeName(), propertyValue);
                                child.setAttribute(propertyKey, propertyValue);
                            } else {
                                log.debug("ignore property {} ", property);
                            }
                        }
                    }

                    if (field.startsWith("@")) {
                        String propertyKey = field.replace("@", "");
                        if (propertyKey.contains("=")) {
                            String[] split1 = propertyKey.split("=");
                            propertyKey = split1[0];
                            String propertyValue = split1[1].replaceAll("'", "");
                            log.debug("    - ajout de la propriété {} de l'élément {} avec la valeur {}", propertyKey, parentNode.getNodeName(), propertyValue);
                            parentNode.setAttribute(propertyKey, propertyValue);
                        } else if (nodeValue != null) {
                            log.debug("    - ajout de la propriété {} de l'élément {} avec la valeur {}", propertyKey, parentNode.getNodeName(), nodeValue);
                            parentNode.setAttribute(propertyKey, nodeValue);
                        }
                    }
                    if (child != null) {
                        parentNode = child;
                    }
                }

            }
        } catch (Exception e) {
            log.error("Erreur lors du process de {} / {}", xpath, nodeValue, e);
        }

        return child == null ? parentNode : child;
    }

    private static boolean hasListName(List<XmlElement> property, Element item) {
        Element child = item;
        for (XmlElement xmlElement : property) {
            String field = xmlElement.getElementName();
            child = getElement(child, field);
            if (child == null) {
                return false;
            }
        }
        return true;
    }

    private static Element getElement(Element root, String field) {
        if (root == null) {
            return null;
        }
        if (field == null) {
            return null;
        }
        String value = null;
        if (field.contains("=")) {
            String[] split = field.split("=");
            field = split[0];
            value = split[1];
        }
        if (field.startsWith("@")) {
            field = field.replace("@", "");
            if (root.hasAttribute(field) && (value == null || root.getAttribute(field).equals(value))) {
                return root;
            } else {
                return null;
            }
        }
        NodeList childNodes = root.getChildNodes();
        if (childNodes.getLength() == 0) return null;
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node node = childNodes.item(i);
            if (node instanceof Element) {
                Element element = (Element) node;
                if (element.getNodeName().equals(field) &&(value == null || element.getTextContent().equals(value))) {
                    return element;
                }
            }
        }
        return null;
    }

    public static Element buildXmlFromXPath(String xpath, Element parentNode, boolean mustCreate) {
        if ("/*".equals(xpath)) {
            return parentNode;
        }


        log.debug("process xpath relative -> {}", xpath);
        Element child = null;

        try {
            if (!StringUtils.isBlank(xpath)) {
                List<XmlElement> pathValuesplit = getXpathElementList(xpath);
                for (XmlElement xmlElement : pathValuesplit) {
                    child = null;
                    String field = xmlElement.getElementName();
                    String parentNodeName = parentNode.getNodeName();
                    NodeList childNodes = parentNode.getChildNodes();
                    if (childNodes.getLength() > 0) {
                        for (int k = 0; k < childNodes.getLength(); k++) {
                            Node item = childNodes.item(k);
                            String name = item.getNodeName();
                            if (field.equals(name) && item instanceof Element) {
                                child = (Element) item;
                                if (!mustCreate) {
                                    log.debug("    - récupération de l'élément {} dans {}", field, parentNode.getNodeName());
                                    break;
                                }
                            }
                        }
                    }
                    if (parentNodeName.equals(field) && !mustCreate) {
                        continue;
                    }


                    if (child == null && !field.startsWith("@")) {
                        log.debug("    - ajout de l'élément {} dans {}", field, parentNode.getNodeName());

                        child = parentNode.getOwnerDocument().createElement(field);

                        parentNode.appendChild(child);
                    }

                    if (!CollectionUtils.isEmpty(xmlElement.getProperties())) {
                        for (String property : xmlElement.getProperties()) {
                            if (property.startsWith("@") && property.contains("=")) {
                                String[] split1 = property.split("=");
                                String name = split1[0];
                                String value = split1[1];
                                String propertyValue = value.replaceAll("'", "");
                                String propertyKey = name.replace("@", "");
                                log.debug("    - ajout de la propriété {} de l'élément {} avec la valeur {}", propertyKey, child.getNodeName(), propertyValue);
                                child.setAttribute(propertyKey, propertyValue);
                            } else {
                                log.debug("ignore property {} ", property);
                            }
                        }
                    }

                    if (field.startsWith("@")) {
                        String propertyKey = field.replace("@", "");
                        if (propertyKey.contains("=")) {
                            String[] split1 = propertyKey.split("=");
                            propertyKey = split1[0];
                            String propertyValue = split1[1].replaceAll("'", "");
                            log.debug("    - ajout de la propriété {} de l'élément {} avec la valeur {}", propertyKey, parentNode.getNodeName(), propertyValue);
                            parentNode.setAttribute(propertyKey, propertyValue);
                        }
                    }
                    if (child != null) {
                        parentNode = child;
                    }
                }

            }
        } catch (Exception e) {
            log.error("Erreur lors du process de {}", xpath, e);
        }

        return child == null ? parentNode : child;
    }


    public static Element addRootXPathListToXml(String xpath, Document document, Element parentNode, String nodeValue) {
        if (StringUtils.isBlank(nodeValue)) {
            return parentNode;
        }
        if (parentNode == null) {
            parentNode = document.getDocumentElement();
        }
        log.debug("ajout d'un noeud list à partir de xpath {} dans l'élement {} avec la valeur {}", xpath, parentNode.getNodeName(), nodeValue);

        try {
            if (!StringUtils.isBlank(xpath)) {

                List<XmlElement> pathValuesplit = getXpathElementList(xpath);

                int i = pathValuesplit.size();
                for (int j = 0; j < i - 1; j++) {
                    XmlElement xmlElement = pathValuesplit.get(j);
                    String relativeField = xmlElement.getElementName();
                    String parentNodeName = parentNode.getNodeName();
                    if (parentNodeName.equals(relativeField)) {
                        continue;
                    }
                    Element found = null;
                    log.debug("    - ajout du nouveau élément {} dans {}", relativeField, parentNode.getNodeName());
                    found = document.createElement(relativeField);
                    parentNode.appendChild(found);

                    parentNode = found;
                }
                String field = pathValuesplit.get(i - 1).getElementName();
                if (!field.startsWith("@")) {
                    if (parentNode.getNodeName().equals(field)) {
                        log.debug("    - récupération de l'élément {} dans {}", field, parentNode.getNodeName());
                    }
                    log.debug("    - ajout du nouveau élément de la liste {} dans {}", field, parentNode.getNodeName());
                    Element child = document.createElement(field);
                    child.appendChild(document.createTextNode(nodeValue));

                    parentNode.appendChild(child);
                    return child;
                } else {
                    String attribute = field.replace("@", "");
                    if (!parentNode.hasAttribute(attribute)) {
                        log.debug("    - ajout de l'attribue {} dans {}", attribute, parentNode.getNodeName());
                        parentNode.setAttribute(attribute, nodeValue);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Erreur lors du process de {} / {}", xpath, nodeValue, e);
        }

        return parentNode;
    }

    public static Element addXPathListToXml(String xpath, Document document, Element parentNode, String nodeValue) {
        if (parentNode == null) {
            parentNode = document.getDocumentElement();
        }
        log.debug("ajout d'un noeud à partir de xpath {} dans l'élement {}", xpath, parentNode.getNodeName());

        try {
            if (!StringUtils.isBlank(xpath)) {

                List<XmlElement> pathValuesplit = getXpathElementList(xpath);

                int i = pathValuesplit.size();
                for (int j = 0; j < i - 1; j++) {
                    XmlElement xmlElement = pathValuesplit.get(j);
                    String relativeField = xmlElement.getElementName();
                    String parentNodeName = parentNode.getNodeName();
                    if (parentNodeName.equals(relativeField)) {
                        continue;
                    }
                    Element found = null;
                    NodeList childNodes = parentNode.getChildNodes();
                    for (int k = 0; k < childNodes.getLength(); k++) {
                        Node item = childNodes.item(k);
                        String name = item.getNodeName();
                        if (relativeField.equals(name) && item instanceof Element) {
                            found = (Element) item;
                            log.debug("    - récupération de l'élément {} dans {}", relativeField, parentNode.getNodeName());
                            break;
                        }
                    }
                    if (found == null) {
                        log.debug("    - ajout du nouveau élément {} dans {}", relativeField, parentNode.getNodeName());
                        found = document.createElement(relativeField);
                        parentNode.appendChild(found);
                    }
                    parentNode = found;
                }
                String field = pathValuesplit.get(i - 1).getElementName();
                if (parentNode.getNodeName().contains(field)) {
                    log.debug("    - récupération de l'élément {} dans {}", field, parentNode.getNodeName());
                }
                log.debug("    - ajout du nouveau élément de la liste {} dans {}", field, parentNode.getNodeName());
                Element child = document.createElement(field);
                if (nodeValue != null && StringUtils.isBlank(child.getTextContent()))
                    child.appendChild(document.createTextNode(nodeValue));

                parentNode.appendChild(child);
                return child;
            }
        } catch (Exception e) {
            log.error("Erreur lors du process de {} / {}", xpath, nodeValue, e);
        }

        return parentNode;
    }

    public static Element addXPathValueListToXml(String xpath, Document document, Element parentNode, String nodeValue) {
        if (parentNode == null) {
            parentNode = document.getDocumentElement();
        }
        log.debug("ajout d'un noeud à partir de xpath {} dans l'élement {}", xpath, parentNode.getNodeName());

        try {
            if (!StringUtils.isBlank(xpath)) {

                List<XmlElement> pathValuesplit = getXpathElementList(xpath);

                int i = pathValuesplit.size();
                for (int j = 0; j < i - 1; j++) {
                    XmlElement xmlElement = pathValuesplit.get(j);
                    String relativeField = xmlElement.getElementName();
                    log.debug("    - ajout du nouveau élément {} dans {}", relativeField, parentNode.getNodeName());
                    Element found = document.createElement(relativeField);
                    parentNode.appendChild(found);
                    parentNode = found;
                }
                String field = pathValuesplit.get(i - 1).getElementName();
                if (parentNode.getNodeName().contains(field)) {
                    log.debug("    - récupération de l'élément {} dans {}", field, parentNode.getNodeName());
                }
                log.debug("    - ajout du nouveau élément de la liste {} dans {}", field, parentNode.getNodeName());
                Element child = document.createElement(field);
                if (nodeValue != null && StringUtils.isBlank(child.getTextContent()))
                    child.appendChild(document.createTextNode(nodeValue));

                parentNode.appendChild(child);
                return child;
            }
        } catch (Exception e) {
            log.error("Erreur lors du process de {} / {}", xpath, nodeValue, e);
        }

        return parentNode;
    }

    public static List<XmlElement> getXpathElementList(String xpath) {
        if (StringUtils.isBlank(xpath)) {
            return Collections.emptyList();
        }
        List<XmlElement> segments = new ArrayList<>();
        int startIndex = 0;
        int endIndex = 0;
        int recursive = 0;
        for (char c : xpath.toCharArray()) {

            if (c == '[') {
                recursive++;
            }

            if (c == ']') {
                recursive--;
            }
            if (c == '/' && recursive == 0) {
                String s = xpath.substring(startIndex, endIndex);
                getXmlElement(segments, s);
                startIndex = endIndex + 1;
            }
            endIndex++;

        }
        String s = xpath.substring(startIndex, endIndex);
        getXmlElement(segments, s);
        log.debug("transform {} to", xpath);
        for (XmlElement segment : segments) {
            log.debug("- element : {} / properties : {}", segment.getElementName(), segment.getProperties());
        }
        return segments;
    }

    public static String extractXPathFromTest(String xpath) {
        if (StringUtils.isBlank(xpath)) {
            return "";
        }
        StringBuilder result = new StringBuilder();
        int recursive = 0;
        do {
            xpath = xpath.replace("((", "(").replace("))", ")");
        } while (xpath.contains("((") || xpath.contains("))"));
        for (char c : xpath.toCharArray()) {

            if (c == '(') {
                recursive++;
            }

            if (c == ')') {
                recursive--;
                if (recursive <= 1) {
                    return result.toString();
                }
            }
            if (recursive == 1 && c != '(') {
                result.append(c);
            }

        }
        return result.toString();
    }

    private static void getXmlElement(List<XmlElement> segments, String s) {
        if (!StringUtils.isBlank(s)) {
            String elementName = s.split("\\[")[0];
            if (!"*".equals(elementName)) {

                String attribute = s.replaceFirst(elementName, "");
                List<String> properties = getXpathProperties(attribute);
                segments.add(XmlElement.builder().elementName(elementName).properties(properties).build());
            }
        }
    }


    public static List<String> getXpathProperties(String xpath) {
        if (StringUtils.isBlank(xpath)) {
            return Collections.emptyList();
        }
        if (!xpath.contains("[")) return List.of(xpath);
        List<String> segments = new ArrayList<>();
        int startIndex = 0;
        int endIndex = 0;
        int recursive = 0;
        for (char c : xpath.toCharArray()) {

            if (c == '[') {
                recursive++;
            }

            if (c == ']') {
                recursive--;
            }
            if (c == ']' && recursive == 0) {
                String s = xpath.substring(startIndex + 1, endIndex);
                if (!StringUtils.isBlank(s)) {
                    segments.add(s);
                }
                startIndex = endIndex + 1;
            }
            endIndex++;

        }
        log.debug("transform {} to", xpath);
        for (String segment : segments) {
            log.debug("   - propriété : {}", segment);
        }
        return segments;
    }

    public static String getMatch(String search, String regex) {
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(search);
        if (m.find()) {
            return m.group(1);
        }
        return null;
    }

    public static File getXmlFile(Document document) {

        try {
            String xmlString = getXmlString(document);
            // to return a XMLstring in response to an API
            File file = Files.createTempFile("validate-", ".xml").toFile();
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(xmlString);
            writer.close();
            return file;
        } catch (IOException e) {
            log.error("Erreur lors de la transformation XML : {}", e.getMessage());
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage(), e);
        }
    }

    public static File getStringFile(String prefix, String xmlString) {

        try {
            // to return a XMLstring in response to an API
            File file = Files.createTempFile(prefix + "-", ".xml").toFile();
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(xmlString);
            writer.close();
            return file;
        } catch (IOException e) {
            log.error("Erreur lors de la transformation XML : {}", e.getMessage());
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage(), e);
        }
    }

    public static String getXmlString(Document document) {

        try (StringWriter writer = new StringWriter()) {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            DOMSource domSource = new DOMSource(document);
            // to return a XMLstring in response to an API
            StreamResult resultToFile = new StreamResult(writer);
            transformer.transform(domSource, resultToFile);
            return writer.toString().replaceAll(" xmlns=\"\"", "");
        } catch (TransformerException | IOException e) {
            log.error("Erreur lors de la transformation XML : {}", e.getMessage());
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage(), e);
        }
    }

    public static JsonNode valueToTree(Object objet) {
        ObjectMapper objectMapper = getObjectMapper();
        return objectMapper.valueToTree(objet);
    }

    public static Map<String, List<NoticeFormatHandler>> knownTags(EformsSurchargeRepository eformsSurchargeRepository, SchematronConfiguration schematronConfiguration) {
        Map<String, List<NoticeFormatHandler>> knownTags = new HashMap<>();
        knownTags.put("ND-Root", List.of(new RootFormatHandler(eformsSurchargeRepository, schematronConfiguration)));
        knownTags.put("ND-Lot", List.of(new LotFormatHandler(eformsSurchargeRepository, schematronConfiguration)));
        knownTags.put("ND-Part", List.of(new LotFormatHandler(eformsSurchargeRepository, schematronConfiguration)));
        knownTags.put("ND-Organization", List.of(new ServiceOrganistationFormatHandler(eformsSurchargeRepository, schematronConfiguration)));
        knownTags.put("ND-ProcedurePlacePerformanceAdditionalInformation", List.of(new LieuExecutionFormatHandler(eformsSurchargeRepository, schematronConfiguration)));
        knownTags.put("ND-LotProcurementDocument", List.of(new DceFormatHandler(eformsSurchargeRepository, schematronConfiguration)));

        knownTags.put("GR-Root", List.of(new RootFormatHandler(eformsSurchargeRepository, schematronConfiguration)));
        knownTags.put("GR-Lot", List.of(new LotFormatHandler(eformsSurchargeRepository, schematronConfiguration)));
        knownTags.put("GR-Part", List.of(new LotFormatHandler(eformsSurchargeRepository, schematronConfiguration)));
        knownTags.put("GR-Organisations", List.of(new ServiceOrganistationFormatHandler(eformsSurchargeRepository, schematronConfiguration)));
        knownTags.put("GR-ProcedurePlacePerformanceAdditionalInformation", List.of(new LieuExecutionFormatHandler(eformsSurchargeRepository, schematronConfiguration)));
        knownTags.put("GR-LotProcurementDocument", List.of(new DceFormatHandler(eformsSurchargeRepository, schematronConfiguration)));
        return knownTags;
    }

    public static Map<String, List<PreviousNoticeFormatHandler>> changeTags(EformsSurchargeRepository eformsSurchargeRepository, SchematronConfiguration schematronConfiguration) {
        Map<String, List<PreviousNoticeFormatHandler>> knownTags = new HashMap<>();
        knownTags.put("ND-Root", List.of(new ChangeRootFormatHandler(eformsSurchargeRepository, schematronConfiguration), new ChangeNoticeFormatHandler(eformsSurchargeRepository, schematronConfiguration)));
        return knownTags;
    }

    public static Map<String, List<PreviousNoticeFormatHandler>> reprendreTags(EformsSurchargeRepository eformsSurchargeRepository, SchematronConfiguration schematronConfiguration) {
        Map<String, List<PreviousNoticeFormatHandler>> knownTags = new HashMap<>();
        knownTags.put("ND-Root", List.of(new ChangeRootFormatHandler(eformsSurchargeRepository, schematronConfiguration)));
        return knownTags;
    }

    public static Map<String, List<SubmitNoticeFormatHandler>> submitTags(EformsSurchargeRepository eformsSurchargeRepository, SchematronConfiguration schematronConfiguration) {
        Map<String, List<SubmitNoticeFormatHandler>> knownTags = new HashMap<>();
        knownTags.put("ND-Root", List.of(new SubmitRootFormatHandler(eformsSurchargeRepository, schematronConfiguration)));
        return knownTags;
    }

    public static String validateXMLSchema(String xsdPath, String xmlPath) {
        String xmlString = null;
        try {
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new File(xsdPath));
            Validator validator = schema.newValidator();
            File file = new File(xmlPath);
            xmlString = Files.readString(file.toPath());
            validator.validate(new StreamSource(file));
        } catch (IOException | SAXException e) {
            log.error("Exception: {} => {}", xmlString, e.getMessage());
            return e.getMessage();
        }
        return null;
    }

    public static Element buildGroupXmlFromXPath(List<XmlStructureDetails> nodeIds, String stopNodeId, Element root) {
        if (nodeIds == null || nodeIds.isEmpty()) {
            return root;
        }
        Element child = root;
        boolean build = false;
        for (int i = nodeIds.size() - 1; i >= 0; i--) {
            XmlStructureDetails xmlStructureDetails = nodeIds.get(i);
            String nodeId = xmlStructureDetails.getId();

            if (nodeId == null || nodeId.equals(stopNodeId)) {
                build = true;
            }
            if (build) {
                String xpath = xmlStructureDetails.getXpathRelative();
                child = buildXmlFromXPath(xpath, child, false);
                log.debug(EFormsHelper.getString(child));
            }
        }
        return child;
    }

    public static XSSFWorkbook openFile(InputStream inputStream) {
        if (inputStream == null) {
            throw new AtexoException(ExceptionEnum.MANDATORY, ERROR_MESSAGE);
        }
        try {
            return new XSSFWorkbook(inputStream);
        } catch (Exception e) {
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage(), e);
        }
    }
}
