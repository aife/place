package fr.atexo.annonces.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

/**
 * DTO - Représente un support de publication
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"code"})
public class OffreDTO {

    private Integer id;
    private String libelle;
    private String code;
    private OffreDTO offreAssociee;
    private String documentExtension;
    private boolean modificationDateMiseEnLigne;
}
