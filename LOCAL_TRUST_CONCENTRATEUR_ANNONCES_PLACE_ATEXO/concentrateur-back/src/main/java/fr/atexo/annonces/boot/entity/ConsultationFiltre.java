package fr.atexo.annonces.boot.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Table(name = "CONSULTATION_FILTRE")
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter
@Getter
@Entity
@EqualsAndHashCode
public class ConsultationFiltre implements Serializable {
    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private int id;


    @Column(name = "ID_CONSULTATION")
    private Integer idConsultation;


    @Column(name = "ID_PLATFORM")
    private String idPlatform;

    @Lob
    @Column(name = "CODE_DEPARTEMENTS")
    private String codeDepartements;

    @Column(name = "JAL")
    private boolean jal;

    @Column(name = "PQR")
    private boolean pqr;

    @Column(name = "SUPPORT_NATIONAUX")
    private boolean national;
    @Column(name = "SUPPORT_EUROPEEN")
    private boolean europeen;
    @Column(name = "MONTANT")
    private Double montant;


}
