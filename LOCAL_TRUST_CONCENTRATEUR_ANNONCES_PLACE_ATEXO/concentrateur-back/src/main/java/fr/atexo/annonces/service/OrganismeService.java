package fr.atexo.annonces.service;


import com.atexo.annonces.commun.mpe.OrganismeMpe;
import fr.atexo.annonces.boot.entity.Organisme;
import fr.atexo.annonces.boot.entity.PlateformeConfiguration;
import fr.atexo.annonces.boot.repository.OrganismeRepository;
import fr.atexo.annonces.boot.repository.PlateformeConfigurationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class OrganismeService {

    private final OrganismeRepository organismeRepository;
    private final PlateformeConfigurationRepository plateformeConfigurationRepository;

    public OrganismeService(OrganismeRepository organismeRepository, PlateformeConfigurationRepository plateformeConfigurationRepository) {
        this.organismeRepository = organismeRepository;
        this.plateformeConfigurationRepository = plateformeConfigurationRepository;
    }

    public Organisme savePlateformeOrganismeIfNotExist(OrganismeMpe mpeOrganisme, String serveurUrl, String plateformeUuid) {
        if (mpeOrganisme == null) {
            return null;
        }
        Organisme organisme = Organisme.builder()
                .acronymeOrganisme(mpeOrganisme.getAcronyme())
                .sigleOrganisme(mpeOrganisme.getSigle())
                .libelleOrganisme(mpeOrganisme.getDenomination())
                .plateformeUuid(plateformeUuid)
                .plateformeUrl(serveurUrl)
                .build();
        var configuration = plateformeConfigurationRepository.findByIdPlateforme(plateformeUuid);
        if (configuration == null) {
            plateformeConfigurationRepository.save(PlateformeConfiguration.builder()
                    .idPlateforme(plateformeUuid)
                    .associationOrganisme(false)
                    .associationProcedure(false)
                    .groupement(false)
                    .eformsModal(false)
                    .pqr(true)
                    .jal(true)
                    .europeen(false)
                    .national(true)
                    .pays("FRA")
                    .validationObligatoire(false)
                    .build());
        }

        log.info("Récupération organisme : organisme = {} / plateforme = {}", organisme.getAcronymeOrganisme(), organisme.getPlateformeUuid());

        Organisme saved = organismeRepository.findByAcronymeOrganismeAndPlateformeUuid(organisme.getAcronymeOrganisme(), organisme.getPlateformeUuid()).orElse(null);
        if (saved == null) {
            return organismeRepository.save(organisme);
        }
        if (saved.getSigleOrganisme() == null) {
            saved.setSigleOrganisme(mpeOrganisme.getSigle());
            saved = organismeRepository.save(saved);
        }
        return saved;
    }

}
