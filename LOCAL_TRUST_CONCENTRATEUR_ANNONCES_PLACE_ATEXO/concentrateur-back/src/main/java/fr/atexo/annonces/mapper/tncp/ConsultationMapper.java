package fr.atexo.annonces.mapper.tncp;

import com.atexo.annonces.commun.mpe.AgentMpe;
import com.atexo.annonces.commun.mpe.CritereAttributionMpe;
import com.atexo.annonces.commun.mpe.ReferentielMpe;
import com.atexo.annonces.commun.mpe.ServiceMpe;
import eu.europa.publications.resource.schema.ted.r2_0_9.reception.TedEsenders;
import fr.atexo.annonces.boot.ws.mpe.model.ConsultationContexte;
import fr.atexo.annonces.commun.tncp.*;
import fr.atexo.annonces.mapper.Mappable;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Service
@Slf4j
@RequiredArgsConstructor
public class ConsultationMapper implements Mappable<ConsultationContexte, Consultation> {

    @NonNull
    private NatureMapper natureMapper;

    @NonNull
    private CpvMapper cpvMapper;

    @NonNull
    private LotMapper lotMapper;
    @NonNull
    private PaysInseeMapper paysInseeMapper;

    @NonNull
    private TypeProcedureMapper typeProcedureMapper;
    @NonNull
    private TechniqueAchatMapper techniqueAchatMapper;

    @NonNull
    private VarianteMapper varianteMapper;

    public static final String FRANCE = "FR";

    @Override
    public Consultation map(final ConsultationContexte config, TedEsenders tedEsenders, boolean rie) {
        var consultation = config.getConsultation();
        var donneesComplementaires = config.getDonneesComplementaires();
        var builder = Consultation.with();
        var lots = config.getLots();
        var contrat = config.getTypeContrat();

        // Nature
        builder.nature(natureMapper.map(consultation.getNaturePrestation(), tedEsenders, rie));
        // CPV
        ofNullable(consultation.getCodeCpvPrincipal()).map(t -> cpvMapper.map(t, tedEsenders, rie)).ifPresent(builder::cpvPrincipal);
        // Lieux d'exécution
        if (!CollectionUtils.isEmpty(consultation.getDepartmentsWithNames())) {
            String code = consultation.getDepartmentsWithNames().entrySet().stream().findFirst().get().getKey();
            String nom = consultation.getDepartmentsWithNames().entrySet().stream().findFirst().get().getValue();
            builder.lieuExecution(LieuExecution.with().code(code).codePays(paysInseeMapper.map(nom, tedEsenders, rie)).localite(nom).build());
        }
        if (contrat != null && contrat.getIdExterne() != null) {
            techniqueAchatMapper.map(contrat.getIdExterne(), tedEsenders, rie).ifPresent(builder::typeTechniqueAchat);
        }

        AgentMpe agentMpe = config.getAgent();
        List<CritereAttributionMpe> criteresAttributionMpe = config.getCritereAttribution();

        // Criteres attribution
        if (!CollectionUtils.isEmpty(criteresAttributionMpe)) {
            builder.criteresAttribution(CriteresAttribution.with().criteres(criteresAttributionMpe.stream().map(CritereAttributionMpe::getDonneeComplementaire).collect(Collectors.toSet())).criteresPonderes(criteresAttributionMpe.stream().map(critereAttributionMpe -> CriterePondereItem.with().critere(critereAttributionMpe.getDonneeComplementaire()).criterePoids(critereAttributionMpe.getPonderation()).criterePCT(critereAttributionMpe.getOrdre()).build()).collect(Collectors.toSet())).build());
        }

        if (donneesComplementaires != null) {
            if (donneesComplementaires.getAvecTranche() != null) {
                builder.tranche(donneesComplementaires.getAvecTranche());
            }
            if (donneesComplementaires.getNombreCandidatsFixe() != null) {
                builder.nombreMaxCandidat(donneesComplementaires.getNombreCandidatsFixe());
            } else if (donneesComplementaires.getNombreCandidatsMax() != null) {
                builder.nombreMaxCandidat(donneesComplementaires.getNombreCandidatsMax());
            }
            builder.reductionSuccessiveCandidats(donneesComplementaires.getPhaseSuccessive());
// Dates
            LocalDate debut = null;
            if (null != donneesComplementaires.getDureeDateDebut()) {
                debut = donneesComplementaires.getDureeDateDebut().toLocalDate();
                builder.dateDebutContratEstimee(debut);
            }
            LocalDate fin = null;
            if (null != donneesComplementaires.getDureeDateFin()) {
                fin = donneesComplementaires.getDureeDateFin().toLocalDate();
                builder.dateFinContratEstimee(fin);
            }

            if (null != donneesComplementaires.getDelaiValiditeOffres()) {
                builder.delaiValiditeOffreAcheteur(donneesComplementaires.getDelaiValiditeOffres());
            }

            builder.variantesRequises(varianteMapper.map(donneesComplementaires, tedEsenders, rie));
        }
        String dateLimiteRemisePlis = consultation.getDateLimiteRemisePlis();
        if (null != dateLimiteRemisePlis) {
            builder.dateLimiteRemiseCandidature(ZonedDateTime.parse(dateLimiteRemisePlis).toLocalDate());
        }
        ZonedDateTime dateLimiteRemiseOffres = consultation.getDateLimiteRemiseOffres();
        if (null != dateLimiteRemiseOffres) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
            builder.dateLimiteRemiseOffre(formatter.format(dateLimiteRemiseOffres));
        }
        // Lots
        if (!CollectionUtils.isEmpty(lots)) {
            builder.lots(lots.stream().map(t -> lotMapper.map(t, tedEsenders, rie)).collect(Collectors.toSet()));
        } else {
            builder.lots(Set.of());
        }
        builder.objet(consultation.getObjet());

        ReferentielMpe typeProcedure = config.getTypeProcedureConsultation();

        if (typeProcedure != null && typeProcedure.getIdExterne() != null) {
            typeProcedureMapper.map(typeProcedure.getIdExterne(), tedEsenders, rie).ifPresent(builder::typeProcedure);
        }
        try {
            URI urlConsultation = new URI(consultation.getUrlConsultation());
            builder.urlConsultation(urlConsultation);
            URI urlProfilAcheteur = new URI(urlConsultation.getScheme() + "://" + urlConsultation.getAuthority() + "/entreprise");
            builder.urlProfilAcheteur(urlProfilAcheteur);
        } catch (URISyntaxException e) {
            log.error("Impossible de convertir l'url de la consultation {} en URL valide", consultation.getUrlConsultation());
        }

        // Acheteur
        if (agentMpe != null) {
            ServiceMpe agentService = config.getServiceAgent();
            String adresse = agentService != null ? agentService.getAdresse() : null;
            String numeroVoie = null;
            StringBuilder nomVoie = new StringBuilder();
            String typeVoie = null;
            if (adresse != null) {
                String[] split = adresse.split(" ");
                if (split.length > 0) {
                    numeroVoie = split[0];
                }
                if (split.length > 1) {
                    typeVoie = split[1];
                }
                for (int i = 1; i < split.length; i++) {
                    if (i == 1) {
                        nomVoie = new StringBuilder(split[i]);
                    } else {
                        nomVoie.append(" ").append(split[i]);
                    }
                }
            }
            builder.acheteur(Contact.with().email(agentMpe.getEmail()).nom(agentMpe.getNom()).prenom(agentMpe.getPrenom()).telephone(agentMpe.getTelephone()).raisonSociale(agentService != null ? agentService.getSigle() : null).siret(agentService != null ? agentService.getSiret() : null).adresse(Adresse.with().code(agentService != null ? agentService.getCodePostal() : null)
                    .localite(agentService != null ? agentService.getVille() : null)
                    .codePays(paysInseeMapper.map(agentService == null ? null : agentService.getPays(), tedEsenders, rie)).nomVoie(nomVoie.toString()).typeVoie(typeVoie).numeroVoie(numeroVoie).build()).build());
        }
        if (tedEsenders != null) {
            var procedure = tedEsenders.getFORMSECTION().getF052014().getProcedure();
            builder.usageNegociation(procedure.getUsageNegociation());
        }

        return builder.build();
    }


}
