package fr.atexo.annonces.service.exception;

import fr.atexo.annonces.service.XslTransformationErrorSeverity;
import org.springframework.util.MultiValueMap;

/**
 * Exception levée si un problème est survenu lors d'une transformation XSL. Le message affiche l'ensemble des
 * problèmes détectés ainsi que leur sévérité.
 */
public class XslTransformationException extends RuntimeException {

    private static final long serialVersionUID = 3207833672675566778L;
    private static final String XSL_TRANSFORMATION_ERROR_MESSAGE = "Une ou plusieurs erreurs sont survenues lors de la transformation du XML :";

    private final MultiValueMap<XslTransformationErrorSeverity, String> collector;

    public XslTransformationException(MultiValueMap<XslTransformationErrorSeverity, String> collector) {
        super();
        this.collector = collector;
    }

    public XslTransformationException(Throwable cause, MultiValueMap<XslTransformationErrorSeverity, String> collector) {
        super(cause);
        this.collector = collector;
    }

    @Override
    public String getMessage() {
        StringBuilder sb = new StringBuilder(XSL_TRANSFORMATION_ERROR_MESSAGE);
        collector.forEach((severity, messageList) -> messageList.forEach(message -> {
            sb.append(System.lineSeparator());
            sb.append(severity.name());
            sb.append(" - ");
            sb.append(message);
        }));
        return sb.toString();
    }

}
