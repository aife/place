package fr.atexo.annonces.boot.entity;

import fr.atexo.annonces.modeles.StatutENoticeEnum;
import lombok.*;
import org.hibernate.envers.Audited;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.history.RevisionMetadata;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter
@Getter
@Entity
@EqualsAndHashCode(of = {"id", "uuid", "idNotice", "lang", "idConsultation", "versionSdk", "noticeVersion"}, callSuper = false)
@Table(name = "E_FORMS_FORMULAIRE")
@EntityListeners(AuditingEntityListener.class)
public class EFormsFormulaireEntity implements Serializable {

    private static final long serialVersionUID = 3294968555933851444L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "ID_MPE_OLD")
    private String idMpeOld;

    @Column(name = "UUID", nullable = false)
    private String uuid;

    @Column(name = "NO_DOC_EXT")
    private String noDocExt;

    @Column(name = "ID_NOTICE", length = 100)
    private String idNotice;

    @Column(name = "LANG", length = 3)
    private String lang;

    @Column(name = "ID_CONSULTATION")
    private Integer idConsultation;
    @Column(name = "INTITULE_CONSULTATION")
    @Lob
    private String intituleConsultation;
    @Column(name = "VERSION_SDK")
    private String versionSdk;
    @Column(name = "REFERENCE_CONSULTATION")
    private String referenceConsultation;

    @Column(name = "NOTICE_VERSION")
    private Integer noticeVersion;

    @Column(name = "STATUT")
    @Enumerated(EnumType.STRING)
    @Audited(withModifiedFlag = true)
    private StatutENoticeEnum statut;

    @Column(name = "XML_GENERE")
    @Lob
    private String xmlGenere;

    @Column(name = "FORMULAIRE")
    @Lob
    private String formulaire;

    @Column(name = "valid")
    @Audited(withModifiedFlag = true)
    private boolean valid;

    @Column(name = "TED_VERSION")
    private String tedVersion;

    @Column(name = "DATE_CREATION", nullable = false)
    @CreatedDate
    private ZonedDateTime dateCreation;

    @Column(name = "DATE_MODIFICATION", nullable = false)
    @Audited
    @LastModifiedDate
    private ZonedDateTime dateModification;

    @Column(name = "DATE_ENVOI_SOUMISSION")
    private ZonedDateTime dateEnvoiSoumission;

    @Column(name = "DATE_MODIFICATION_STATUT")
    @Audited
    private ZonedDateTime dateModificationStatut;

    @Column(name = "DATE_SOUMISSION")
    private ZonedDateTime dateSoumission;

    @Column(name = "DATE_ENVOI_ANNULATION")
    private ZonedDateTime dateEnvoiAnnulation;

    @Transient
    private RevisionMetadata<Integer> editVersion;

    @Column(name = "PLATEFORME")
    private String plateforme;

    @Column(name = "AUTEUR_EMAIL")
    private String auteurEmail;

    @Column(name = "PUBLICATION_ID")
    private String publicationId;

    @Column(name = "PUBLICATION_DATE")
    private Date publicationDate;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn(name = "ID_SUIVI_ANNONCE", referencedColumnName = "ID")
    private SuiviAnnonce suiviAnnonce;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PREVIOUS_FORMULAIRE", referencedColumnName = "ID")
    private EFormsFormulaireEntity previous;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "previous")
    private EFormsFormulaireEntity next;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ID_PROCEDURE", referencedColumnName = "ID")
    private EFormsProcedureEntity procedure;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_ORGANISME")
    private Organisme organisme;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_CREER_PAR")
    @CreatedBy
    private Agent creerPar;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_MODIFIER_PAR")
    @Audited(targetAuditMode = NOT_AUDITED)
    @LastModifiedBy
    private Agent modifierPar;

    @Column(name = "SVRL")
    @Lob
    private String svrlString;

}
