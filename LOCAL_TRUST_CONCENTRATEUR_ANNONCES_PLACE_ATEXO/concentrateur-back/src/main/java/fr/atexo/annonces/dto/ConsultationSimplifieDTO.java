package fr.atexo.annonces.dto;

import lombok.*;


@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
public class ConsultationSimplifieDTO {

    private boolean simplifie;

    public Integer idConsultation;

    public String idPlatform;

}
