package fr.atexo.annonces.service.exception;

import lombok.Getter;

import java.text.MessageFormat;

@Getter
public class ContexteInvalideException extends RuntimeException {

	private final String contexte;

	public ContexteInvalideException(final String contexte) {
		super(MessageFormat.format("Le contexte {} est invalide", contexte));

		this.contexte = contexte;
	}
}
