package fr.atexo.annonces.mapper;

import fr.atexo.annonces.boot.entity.Departement;
import fr.atexo.annonces.dto.DepartementDTO;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * Mapper entre l'entité Departement et son DTO
 */
@Mapper
public interface DepartementMapper {

    DepartementDTO departementToDepartementDTO(Departement departement);

    List<DepartementDTO> departementsToDepartementDTOs(List<Departement> departements);

}
