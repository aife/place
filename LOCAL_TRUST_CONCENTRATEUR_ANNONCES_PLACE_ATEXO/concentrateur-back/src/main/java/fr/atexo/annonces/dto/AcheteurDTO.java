package fr.atexo.annonces.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AcheteurDTO {

    private String login;
    private String email;
    private String password;
    private String token;

}
