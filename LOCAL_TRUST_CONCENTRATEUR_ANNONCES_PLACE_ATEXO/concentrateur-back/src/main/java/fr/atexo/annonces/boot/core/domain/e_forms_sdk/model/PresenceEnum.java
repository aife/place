package fr.atexo.annonces.boot.core.domain.e_forms_sdk.model;

public enum PresenceEnum {
    MANDATORY, FORBIDDEN
}
