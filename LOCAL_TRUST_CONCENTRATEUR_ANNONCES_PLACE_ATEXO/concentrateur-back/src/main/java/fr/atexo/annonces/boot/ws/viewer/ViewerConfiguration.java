package fr.atexo.annonces.boot.ws.viewer;


import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@ConfigurationProperties("external-apis.viewer")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ViewerConfiguration {
    private String basePath;
    private String apiKey;
    private Map<String, String> ws;

}
