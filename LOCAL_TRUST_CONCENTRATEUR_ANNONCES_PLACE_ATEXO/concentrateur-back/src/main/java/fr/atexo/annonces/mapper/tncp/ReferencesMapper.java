package fr.atexo.annonces.mapper.tncp;

import com.atexo.annonces.commun.mpe.ConsultationMpe;
import eu.europa.publications.resource.schema.ted.r2_0_9.reception.TedEsenders;
import fr.atexo.annonces.commun.tncp.References;
import fr.atexo.annonces.mapper.Mappable;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ReferencesMapper extends Mappable<ConsultationMpe, References> {

    @Override
    @Mapping(target = "technicalId", source = "consultation.id")
    @Mapping(target = "externalReference", ignore = true)
    References map(ConsultationMpe consultation, TedEsenders tedEsenders, boolean rie);

}
