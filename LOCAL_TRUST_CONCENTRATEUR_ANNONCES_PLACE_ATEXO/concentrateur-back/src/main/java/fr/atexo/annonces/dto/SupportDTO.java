package fr.atexo.annonces.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.Set;

/**
 * DTO - Représente un support de publication
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"code"})
public class SupportDTO {

    private String libelle;
    private String code;
    private String mail;
    private String codeGroupe;
    private boolean external;
    private Set<OffreDTO> offres;
    private String description;
    private String url;
    private boolean national;
    private boolean europeen;
    private boolean choixUnique;
    private boolean afficherLogo;
    private boolean selectionneParDefaut;
    private String pays;
    private boolean compteObligatoire;
    private String logoUrl;
    private boolean iframe;
    private Set<DepartementDTO> jal;
    private Set<DepartementDTO> pqr;

}
