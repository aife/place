package fr.atexo.annonces.boot;

import fr.atexo.annonces.service.exception.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Exception Handler permettant de faire correspondre les exceptions des services avec des codes retours HTTP explicites
 */
@ControllerAdvice
public class ControllerExceptionHandler {

    public static final String EXCEPTION_HANDLING_LOG = "Rest API - Exception handling: ";
    private final Logger logger = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @ExceptionHandler
    public ResponseEntity<String> handleResourceNotFoundException(ResourceNotFoundException e) {
        logger.error(EXCEPTION_HANDLING_LOG, e);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<String> handleIllegalOperationException(IllegalOperationException e) {
        logger.error(EXCEPTION_HANDLING_LOG, e);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler
    public ResponseEntity<String> handleConflictingResourceException(ConflictingResourceException e) {
        logger.error(EXCEPTION_HANDLING_LOG, e);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
    }

    @ExceptionHandler
    public ResponseEntity<String> handleInconsistentArgumentsException(InconsistentArgumentsException e) {
        logger.error(EXCEPTION_HANDLING_LOG, e);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<String> handleMissingArgumentException(MissingArgumentException e) {
        logger.error(EXCEPTION_HANDLING_LOG, e);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<String> handleExportConfigurationException(ExportConfigurationException e) {
        logger.error(EXCEPTION_HANDLING_LOG, e);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler
    public ResponseEntity<String> handlePdfExportException(PdfExportException e) {
        logger.error(EXCEPTION_HANDLING_LOG, e);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
