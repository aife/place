package fr.atexo.annonces.dto.validator;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ValidatorDTO {
    private String notice;
    private String language;
    private String validationMode = "static";
    private String eFormsSdkVersion;
}
