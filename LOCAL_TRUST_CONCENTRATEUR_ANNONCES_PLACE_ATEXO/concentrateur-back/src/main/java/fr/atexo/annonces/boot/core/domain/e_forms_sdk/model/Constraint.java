package fr.atexo.annonces.boot.core.domain.e_forms_sdk.model;


import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Constraint {
    private String severity;
    private String condition;
    private String message;
    private boolean value;
    private List<String> noticeTypes;
}
