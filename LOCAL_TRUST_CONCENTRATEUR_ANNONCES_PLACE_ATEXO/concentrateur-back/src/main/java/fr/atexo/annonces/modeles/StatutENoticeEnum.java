package fr.atexo.annonces.modeles;

public enum StatutENoticeEnum {
    COMPLETED, CONCENTRATEUR_VALIDATING_ECO, CONCENTRATEUR_VALIDATING_SIP, CONCENTRATEUR_REJET_ECO, CONCENTRATEUR_REJET_SIP, DRAFT, CONCENTRATEUR_VALIDATED, SUBMITTING, SUBMITTED, STOPPED, STOPPING, PUBLISHING, PUBLISHED, DELETED, NOT_PUBLISHED, CONVERTED_FROM_PUBLISHED, ARCHIVED, VALIDATION_FAILED;


    public static StatutENoticeEnum getStatut(String statut) {
        return switch (statut.toUpperCase()) {
            case "RECEIVED" -> SUBMITTED;
            case "RECEPTION_ERROR" -> VALIDATION_FAILED;
            case "QUALIFICATION_ERROR" -> VALIDATION_FAILED;
            case "VALIDATION_ACCEPTED" -> PUBLISHED;
            case "QUALITY_ACCEPTED" -> PUBLISHING;
            case "QUALITY_SKIPPED" -> PUBLISHING;
            case "IN_PROGRESS" -> PUBLISHING;
            case "PUBLISHED" -> PUBLISHED;
            case "NOT_PUBLISHED" -> NOT_PUBLISHED;
            default -> null;
        };

    }


    public static StatutENoticeEnum getStatutENoticeFromSuiviAnnonce(StatutSuiviAnnonceEnum statut) {

        switch (statut) {
            case BROUILLON:
                return DRAFT;
            case EN_COURS_DE_PUBLICATION:
                return PUBLISHING;
            case REJETER_SUPPORT:
            case REJETER_CONCENTRATEUR:
                return VALIDATION_FAILED;
            case PUBLIER:
                return PUBLISHED;
            case ARRETER:
                return STOPPED;
            case ARCHIVER:
                return ARCHIVED;
            case SUPPRIMER:
                return DELETED;
            case EN_COURS_D_ARRET:
                return STOPPING;
            case EN_ATTENTE_VALIDATION_ECO:
                return CONCENTRATEUR_VALIDATING_ECO;
            case EN_ATTENTE_VALIDATION_SIP:
                return CONCENTRATEUR_VALIDATING_SIP;
            case REJETER_CONCENTRATEUR_VALIDATION_ECO:
                return CONCENTRATEUR_REJET_ECO;
            case REJETER_CONCENTRATEUR_VALIDATION_SIP:
                return CONCENTRATEUR_REJET_SIP;
            case ENVOI_PLANIFIER:
                return CONCENTRATEUR_VALIDATED;
            case EN_ATTENTE:
                return CONCENTRATEUR_VALIDATED;
            case PREPARATION_PUBLICATION:
                return COMPLETED;
            case EXTERNAL:
            default:
                return null;
        }

    }
}
