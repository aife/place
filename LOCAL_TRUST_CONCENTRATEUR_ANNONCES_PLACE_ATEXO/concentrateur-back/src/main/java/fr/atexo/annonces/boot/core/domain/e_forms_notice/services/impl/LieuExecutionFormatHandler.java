package fr.atexo.annonces.boot.core.domain.e_forms_notice.services.impl;

import com.atexo.annonces.commun.mpe.LotMpe;
import com.fasterxml.jackson.databind.JsonNode;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.NoticeFormatHandler;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.FieldDetails;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNotice;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNoticeMetadata;
import fr.atexo.annonces.boot.entity.EFormsFormulaireEntity;
import fr.atexo.annonces.boot.repository.EformsSurchargeRepository;
import fr.atexo.annonces.boot.ws.eforms.SchematronConfiguration;
import fr.atexo.annonces.boot.ws.mpe.model.ConsultationContexte;
import fr.atexo.annonces.dto.ConfigurationConsultationDTO;
import fr.atexo.annonces.dto.EformsSurchargeDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

import static fr.atexo.annonces.config.EFormsHelper.valueToTree;

@Slf4j
public class LieuExecutionFormatHandler extends GenericSdkService implements NoticeFormatHandler {


    public LieuExecutionFormatHandler(EformsSurchargeRepository eformsSurchargeRepository, SchematronConfiguration schematronConfiguration) {
        super(eformsSurchargeRepository, schematronConfiguration);
    }

    @Override
    public Object processGroup(SubNotice notice, ConfigurationConsultationDTO configurationConsultationDTO, Object formulaireObjet, SubNoticeMetadata group, EFormsFormulaireEntity saved, String nodeStop, Map<String, List<NoticeFormatHandler>> kownHandlers) {
        ConsultationContexte contexte = configurationConsultationDTO.getContexte();
        if (contexte == null || contexte.getConsultation() == null || contexte.getConsultation().getCodesNuts() == null) {
            return formulaireObjet;
        }
        List<String> list = contexte.getConsultation().getCodesNuts();
        List<String> codesNuts = list
                .stream().map(nuts -> {
                    Map<String, Set<String>> result = this.getCodeListById("nuts-lvl3", "en", nuts, saved.getPlateforme(), saved.getVersionSdk());
                    if (result.containsKey("nuts-lvl3")) {
                        return result.get("nuts-lvl3").stream().filter(s -> s.equalsIgnoreCase(nuts)).collect(Collectors.toList());
                    }
                    return null;
                }).filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .distinct()
                .toList();
        if (CollectionUtils.isEmpty(codesNuts)) {
            return formulaireObjet;
        }
        List<Object> o = new ArrayList<>();
        for (int i = 0; i < codesNuts.size(); i++) {
            String codeNuts = codesNuts.get(i);
            if (!CollectionUtils.isEmpty(group.getContent())) {
                contexte.setLieuExecution(codeNuts);
                String nodeId = group.getNodeId();
                int index = i + 1;
                Object processList = processcodeNuts(configurationConsultationDTO, new Object(), group.getContent(), nodeId, saved, index);
                processList = setGroupId(processList, nodeStop, group, nodeId, saved.getVersionSdk(), index);
                JsonNode node = valueToTree(processList);
                if (!node.isObject() || !node.isEmpty()) {
                    o.add(processList);
                }

            }
        }
        contexte.setLot(null);
        return o;
    }

    private List<LotMpe> getLotMpes(ConsultationContexte contexte) {
        List<LotMpe> mpeLots = contexte.getLots();
        if (CollectionUtils.isEmpty(mpeLots)) {
            mpeLots = List.of(LotMpe.with()
                    .codeCpvPrincipal(contexte.getConsultation().getCodeCpvPrincipal())
                    .clauses(contexte.getConsultation().getClauses())
                    .codeCpvSecondaire1(contexte.getConsultation().getCodeCpvSecondaire1())
                    .codeCpvSecondaire2(contexte.getConsultation().getCodeCpvSecondaire2())
                    .codeCpvSecondaire3(contexte.getConsultation().getCodeCpvSecondaire3())
                    .intitule(contexte.getConsultation().getIntitule())
                    .descriptionSuccinte(contexte.getConsultation().getObjet())
                    .naturePrestation(contexte.getConsultation().getNaturePrestation())
                    .consultation(contexte.getConsultation().getReference())
                    .numero("1")
                    .typeDecision(contexte.getConsultation().getTypeDecision())
                    .typeDecisionAdmissionSad(contexte.getConsultation().getTypeDecisionAdmissionSad())
                    .typeDecisionARenseigner(contexte.getConsultation().getTypeDecisionARenseigner())
                    .typeDecisionAttributionMarche(contexte.getConsultation().getTypeDecisionAttributionMarche())
                    .typeDecisionAttributionAccordCadre(contexte.getConsultation().getTypeDecisionAttributionAccordCadre())
                    .typeDecisionDeclarationInfructueux(contexte.getConsultation().getTypeDecisionDeclarationInfructueux())
                    .typeDecisionDeclarationSansSuite(contexte.getConsultation().getTypeDecisionDeclarationSansSuite())
                    .build());

        }
        return mpeLots;
    }

    private Object processcodeNuts(ConfigurationConsultationDTO contexte, Object formulaireObjet, List<SubNoticeMetadata> content, String nodeStop, EFormsFormulaireEntity saved, Integer index) {
        for (SubNoticeMetadata item : content) {
            String nodeId = (item.is_repeatable() || item.get_identifierFieldId() != null) ? item.getNodeId() : null;
            if (("field".equals(item.getContentType()))) {
                formulaireObjet = processElement(item, null, contexte, formulaireObjet, item.getId(), saved, nodeStop);
            }
            if (!CollectionUtils.isEmpty(item.getContent())) {
                if (nodeId == null) {
                    formulaireObjet = processcodeNuts(contexte, formulaireObjet, item.getContent(), nodeStop, saved, index);
                } else {
                    Object o = processcodeNuts(contexte, new Object(), item.getContent(), nodeId, saved, index);
                    formulaireObjet = addToObject(formulaireObjet, null, o, nodeStop, nodeId, saved.getVersionSdk());

                }
            }
            formulaireObjet = setGroupId(formulaireObjet, nodeStop, item, nodeId, saved.getVersionSdk(), index);
        }

        return formulaireObjet;
    }

    @Override
    public Object processElement(SubNoticeMetadata item, SubNotice notice, ConfigurationConsultationDTO configurationConsultationDTO, Object formulaireObjet, String id, EFormsFormulaireEntity saved, String nodeStop) {
        if (configurationConsultationDTO == null || configurationConsultationDTO.getContexte() == null) {
            return formulaireObjet;
        }

        ConsultationContexte contexte = configurationConsultationDTO.getContexte();
        String versionSdk = saved.getVersionSdk();
        FieldDetails fieldDetails = getFields(versionSdk).getFields()
                .stream().filter(fieldDetails1 -> fieldDetails1.getId().equals(id))
                .findFirst().orElse(null);
        if (fieldDetails == null) {
            return formulaireObjet;
        }
        String parentNodeId = fieldDetails.getParentNodeId();
        EformsSurchargeDTO surcharge = item.getSurcharge();
        if (surcharge != null && surcharge.getDefaultValue() != null) {
            return addToObject(formulaireObjet, id, surcharge.getDefaultValue(), nodeStop, parentNodeId, versionSdk);

        }
        boolean repeatable = item.is_repeatable();
        String presetValue = item.get_presetValue();
        if (StringUtils.hasText(presetValue)) {
            Object object = getPresetValue(presetValue, fieldDetails.getType(), versionSdk);
            if (object != null)
                return addToObject(formulaireObjet, id, repeatable ? List.of(object) : object, nodeStop, parentNodeId, versionSdk);
        }

        switch (id) {
            case "BT-5141-Procedure" -> {
                return addToObject(formulaireObjet, id, repeatable ? List.of("LUX") : "LUX", nodeStop, parentNodeId, versionSdk);
            }
            case "BT-5071-Procedure" -> {
                String lieuExecution = contexte.getLieuExecution();
                return addToObject(formulaireObjet, id, repeatable ? List.of(lieuExecution) : lieuExecution, nodeStop, parentNodeId, versionSdk);
            }

            default -> log.debug("Pas de correspondance pour {} dans le groupe lieu exécution", id);
        }
        return formulaireObjet;
    }


}
