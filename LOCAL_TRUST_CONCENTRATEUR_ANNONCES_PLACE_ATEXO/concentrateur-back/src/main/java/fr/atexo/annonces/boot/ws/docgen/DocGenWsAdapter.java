package fr.atexo.annonces.boot.ws.docgen;

import fr.atexo.annonces.dto.DocumentEditorRequest;
import fr.atexo.annonces.dto.FileStatus;
import fr.atexo.annonces.dto.FileStatusEnum;
import fr.atexo.annonces.dto.ValidationDocumentEditorRequest;
import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Component
@Slf4j
public class DocGenWsAdapter implements DocGenWsPort {

    private final DocGenWS docGenWS;

    public DocGenWsAdapter(DocGenWS docGenWS) {
        this.docGenWS = docGenWS;
    }

    @Override
    public InputStream generatePdf(List<KeyValueRequest> map, File ressource) throws IOException {
        Resource resource = docGenWS.generateDocx(map, ressource, true, "");
        return resource == null ? null : resource.getInputStream();
    }

    @Override
    public InputStream generateDocument(List<KeyValueRequest> map, File ressource, String defaultOnNull) throws IOException {
        Resource resource = docGenWS.generateDocx(map, ressource, false, defaultOnNull);
        return resource == null ? null : resource.getInputStream();
    }

    @Override
    public String getEditionToken(File ressource, DocumentEditorRequest request, String extension) {
        try {
            return docGenWS.getEditionToken(ressource, request, extension);
        } catch (IOException e) {
            log.error("Erreur lors de demande de token {}", e.getMessage());
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, "Erreur lors de demande de token", e);
        }
    }

    @Override
    public String getValidationToken(File ressource, ValidationDocumentEditorRequest request, String extension) {
        try {
            return docGenWS.getValidationToken(ressource, request, extension);
        } catch (IOException e) {
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, "Erreur lors de demande de token", e);
        }
    }

    @Override
    public InputStream getDocument(String token) {
        try {
            ResponseEntity<Resource> resource = docGenWS.getDocument(token);
            if (resource == null) {
                return null;
            }
            Resource body = resource.getBody();
            return body == null ? null : body.getInputStream();
        } catch (IOException e) {
            log.error("Erreur lors de récpération du document {}", e.getMessage());
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, "Erreur lors de la récpération du document", e);
        }
    }

    @Override
    public FileStatus getDocumentStatus(String token) {
        ResponseEntity<Resource> resource = docGenWS.getDocument(token);
        if (resource == null) {
            return null;
        }
        FileStatus status = new FileStatus();

        HttpHeaders headers = resource.getHeaders();
        if (headers.containsKey("document-status") && !CollectionUtils.isEmpty(headers.get("document-status"))) {
            status.setStatus(FileStatusEnum.valueOf(headers.get("document-status").get(0)));
        }

        if (headers.containsKey("document-version") && !CollectionUtils.isEmpty(headers.get("document-version"))) {
            status.setVersion(Integer.valueOf(headers.get("document-version").get(0)));
        }

        if (headers.containsKey("redac-status") && !CollectionUtils.isEmpty(headers.get("redac-status"))) {
            status.setRedacStatus(Integer.valueOf(headers.get("redac-status").get(0)));
        }
        return status;
    }


    @Override
    public Resource convertToPDF(File ressource, String extension) {
        return docGenWS.convertToPDF(ressource, extension);
    }

}
