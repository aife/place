package fr.atexo.annonces.reseau;

import fr.atexo.annonces.boot.entity.Organisme;
import fr.atexo.annonces.boot.entity.tncp.SuiviAvisPublie;
import fr.atexo.annonces.boot.entity.tncp.SuiviStatut;
import fr.atexo.annonces.boot.repository.AcheteurRepository;
import fr.atexo.annonces.dto.AcheteurChoixDTO;
import fr.atexo.annonces.dto.SuiviDTO;
import fr.atexo.annonces.mapper.SuiviAvisPublieMapper;
import fr.atexo.annonces.service.SuiviAnnonceService;
import fr.atexo.annonces.service.SuiviService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

/**
 * Point d'entrée web pour manipuler les demandes de publication
 */
@RestController
@RequestMapping("/rest/v2/suivis")
@RequiredArgsConstructor
public class SuiviController {

    @NonNull
    private SuiviService suiviService;
    @NonNull
    private SuiviAvisPublieMapper suiviMapper;
    @NonNull
    private SuiviAnnonceService suiviAnnonceService;

    @NonNull
    private AcheteurRepository acheteurRepository;

    @GetMapping("/{uuid}")
    public SuiviDTO sauvegarder(@PathVariable("uuid") String uuid) {
        return suiviMapper.mapToModel(suiviService.recuperer(uuid));
    }

    @PutMapping
    public SuiviDTO creer(@RequestBody SuiviDTO suivi) {
        var annonce = suiviAnnonceService.getSuiviAnnonceEntity(suivi.getAnnonce());
        if (annonce == null) {
            throw new IllegalArgumentException("Annonce non trouvée");
        }
        if (annonce.getSuiviList().stream().anyMatch(s -> s.getAcheteur() != null)) {
            throw new IllegalArgumentException("Un suivi existe déjà pour cet acheteur");
        }
        AcheteurChoixDTO suiviAcheteur = suivi.getAcheteur();
        Organisme organismeMpe = annonce.getOrganismeMpe();
        var acheteur = acheteurRepository.findByLoginAndEmailAndOrganismeId(suiviAcheteur.getLogin(), suiviAcheteur.getEmail(), organismeMpe.getId()).orElseThrow();
        SuiviAvisPublie build = SuiviAvisPublie.with().acheteur(acheteur)
                .statut(SuiviStatut.CREE).suiviAnnonce(annonce)
                .build();
        return suiviService.sauvegarder(build);
    }
}
