package fr.atexo.annonces.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class OrganismeResumeDTO {
    private Integer id;

    private String acronymeOrganisme;

    private String sigleOrganisme;

    private String libelleOrganisme;

    private String plateformeUuid;

    private String plateformeUrl;



}
