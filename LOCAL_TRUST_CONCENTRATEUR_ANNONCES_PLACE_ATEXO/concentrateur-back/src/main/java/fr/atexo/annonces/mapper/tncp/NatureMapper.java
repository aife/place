package fr.atexo.annonces.mapper.tncp;

import eu.europa.publications.resource.schema.ted.r2_0_9.reception.TedEsenders;
import fr.atexo.annonces.commun.tncp.Nature;
import fr.atexo.annonces.mapper.Mappable;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class NatureMapper implements Mappable<String, Nature> {

    @Override
    public Nature map(final String nature, TedEsenders tedEsenders, boolean rie) {

        return Nature.from(nature).orElse(null);
    }
}
