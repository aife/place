package fr.atexo.annonces.dto;

import com.atexo.annonces.commun.mpe.OrganismeMpe;
import com.atexo.annonces.commun.mpe.ReferentielMpe;
import com.atexo.annonces.commun.mpe.ServiceMpe;
import lombok.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SuiviTypeAvisPubAdd implements Serializable {

    @NonNull
    private Integer idConsultation;

    @NonNull
    private String reference;

    @NonNull
    private String titre;

    @NonNull
    private String objet;

    private boolean europeen;

    private ZonedDateTime dlro;

    private ZonedDateTime dateMiseEnLigne;

    private ZonedDateTime dateMiseEnLigneCalcule;

    private OrganismeMpe organisme;

    private ServiceMpe service;

    private ReferentielMpe procedure;

    private List<JalDTO> jalList;

    private ConsultationFacturationDTO facturation;

    @NonNull
    private OffreDTO offreRacine;
}
