package fr.atexo.annonces.boot.core.domain.e_forms_notice.services.impl;

import com.atexo.annonces.commun.mpe.ProfilJoueMpe;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.NoticeFormatHandler;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.FieldDetails;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNotice;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNoticeMetadata;
import fr.atexo.annonces.boot.entity.EFormsFormulaireEntity;
import fr.atexo.annonces.boot.repository.EformsSurchargeRepository;
import fr.atexo.annonces.boot.ws.eforms.SchematronConfiguration;
import fr.atexo.annonces.boot.ws.mpe.model.ConsultationContexte;
import fr.atexo.annonces.config.EFormsHelper;
import fr.atexo.annonces.dto.ConfigurationConsultationDTO;
import fr.atexo.annonces.dto.EformsSurchargeDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
public class ServiceOrganistationFormatHandler extends GenericSdkService implements NoticeFormatHandler {


    public ServiceOrganistationFormatHandler(EformsSurchargeRepository eformsSurchargeRepository, SchematronConfiguration schematronConfiguration) {
        super(eformsSurchargeRepository, schematronConfiguration);
    }

    @Override
    public Object processGroup(SubNotice notice, ConfigurationConsultationDTO contexte, Object formulaireObjet, SubNoticeMetadata group, EFormsFormulaireEntity saved, String nodeStop, Map<String, List<NoticeFormatHandler>> kownHandlers) {
        for (SubNoticeMetadata item : group.getContent()) {
            String nodeId = (item.is_repeatable() || item.get_identifierFieldId() != null) ? item.getNodeId() : null;
            List<NoticeFormatHandler> handler = kownHandlers.get(nodeId);
            if (handler != null) {

                for (NoticeFormatHandler noticeFormatHandler : handler) {
                    Object value = noticeFormatHandler.processGroup(notice, contexte, new Object(), item, saved, nodeId, kownHandlers);
                    formulaireObjet = addToObject(formulaireObjet, null, value, nodeStop, nodeId, saved.getVersionSdk());
                }
            } else if (nodeId != null) {
                if (("field".equals(item.getContentType()))) {
                    formulaireObjet = processElement(item, notice, contexte, formulaireObjet, item.getId(), saved, nodeStop);
                }
                if (!CollectionUtils.isEmpty(item.getContent())) {
                    Object o = processGroup(notice, contexte, new Object(), item, saved, nodeId, kownHandlers);
                    formulaireObjet = addToObject(formulaireObjet, null, o, nodeStop, nodeId, saved.getVersionSdk());
                }
            } else {
                if (("field".equals(item.getContentType()))) {
                    formulaireObjet = processElement(item, notice, contexte, formulaireObjet, item.getId(), saved, nodeStop);
                }
                if (!CollectionUtils.isEmpty(item.getContent())) {
                    formulaireObjet = processGroup(notice, contexte, formulaireObjet, item, saved, nodeStop, kownHandlers);
                }
            }
        }
        formulaireObjet = setGroupId(formulaireObjet, nodeStop, group, group.getNodeId(), saved.getVersionSdk(), null);
        return setSurchargeGroup(formulaireObjet, group);

    }

    @Override
    public Object processElement(SubNoticeMetadata item, SubNotice notice, ConfigurationConsultationDTO configurationConsultationDTO, Object formulaireObjet, String id, EFormsFormulaireEntity saved, String nodeStop) {
        if (configurationConsultationDTO == null || configurationConsultationDTO.getContexte() == null) {
            return formulaireObjet;
        }

        ConsultationContexte contexte = configurationConsultationDTO.getContexte();
        String versionSdk = saved.getVersionSdk();
        FieldDetails fieldDetails = getFields(versionSdk).getFields().stream().filter(fieldDetails1 -> fieldDetails1.getId().equals(id)).findFirst().orElse(null);
        if (fieldDetails == null) {
            return formulaireObjet;
        }
        String parentNodeId = fieldDetails.getParentNodeId();
        EformsSurchargeDTO surcharge = item.getSurcharge();
        boolean repeatable = item.is_repeatable();
        if (surcharge != null && surcharge.getDefaultValue() != null) {
            return addToObject(formulaireObjet, id, surcharge.getDefaultValue(), nodeStop, parentNodeId, versionSdk);

        }
        String presetValue = item.get_presetValue();
        if (StringUtils.hasText(presetValue)) {
            Object object = getPresetValue(presetValue, fieldDetails.getType(), versionSdk);
            if (object != null)
                return addToObject(formulaireObjet, id, repeatable ? List.of(object) : object, nodeStop, parentNodeId, versionSdk);
        }

        ProfilJoueMpe profilJoue = contexte.getProfilJoue();
        switch (id) {
            case "OPT-200-Organization-Company":
                return addToObject(formulaireObjet, id, repeatable ? List.of("ORG-0001") : "ORG-0001", nodeStop, parentNodeId, versionSdk);
            case "BT-514-Organization-Company":
                if (profilJoue != null) {
                    return addToObject(formulaireObjet, id, repeatable ? List.of("LUX") : "LUX", nodeStop, parentNodeId, versionSdk);
                } else {
                    return formulaireObjet;
                }
            case "BT-500-Organization-Company":
                if (profilJoue != null && StringUtils.hasText(profilJoue.getNomOfficiel())) {
                    return addToObject(formulaireObjet, id, repeatable ? List.of(profilJoue.getNomOfficiel()) : profilJoue.getNomOfficiel(), nodeStop, parentNodeId, versionSdk);
                }
                break;
            case "BT-501-Organization-Company":
                if (profilJoue != null && StringUtils.hasText(profilJoue.getNumeroNationalIdentification())) {
                    String numeroNationalIdentification = profilJoue.getNumeroNationalIdentification();
                    return addToObject(formulaireObjet, id, repeatable ? List.of(numeroNationalIdentification) : numeroNationalIdentification, nodeStop, parentNodeId, versionSdk);
                }
                break;
            case "BT-505-Organization-Company":
                if (profilJoue != null && StringUtils.hasText(profilJoue.getAdresseProfilAcheteur())) {
                    return addToObject(formulaireObjet, id, repeatable ? List.of(profilJoue.getAdresseProfilAcheteur()) : profilJoue.getAdresseProfilAcheteur(), nodeStop, parentNodeId, versionSdk);
                }
                break;
            case "BT-506-Organization-Company":
                if (profilJoue != null && StringUtils.hasText(profilJoue.getEmail())) {
                    return addToObject(formulaireObjet, id, repeatable ? List.of(profilJoue.getEmail()) : profilJoue.getEmail(), nodeStop, parentNodeId, versionSdk);
                }
                break;
            case "BT-739-Organization-Company":
                if (profilJoue != null && StringUtils.hasText(profilJoue.getFax())) {
                    return addToObject(formulaireObjet, id, repeatable ? List.of(profilJoue.getFax()) : profilJoue.getFax(), nodeStop, parentNodeId, versionSdk);
                }
                break;
            case "BT-502-Organization-Company":
                if (profilJoue != null && StringUtils.hasText(profilJoue.getPointContact())) {
                    return addToObject(formulaireObjet, id, repeatable ? List.of(profilJoue.getPointContact()) : profilJoue.getPointContact(), nodeStop, parentNodeId, versionSdk);
                }
                break;
            case "BT-503-Organization-Company":
                if (profilJoue != null && StringUtils.hasText(profilJoue.getTelephone())) {
                    return addToObject(formulaireObjet, id, repeatable ? List.of(profilJoue.getTelephone()) : profilJoue.getTelephone(), nodeStop, parentNodeId, versionSdk);
                }
                break;
            case "BT-513-Organization-Company":
                if (profilJoue != null && StringUtils.hasText(profilJoue.getVille())) {
                    return addToObject(formulaireObjet, id, repeatable ? List.of(profilJoue.getVille()) : profilJoue.getVille(), nodeStop, parentNodeId, versionSdk);
                }
                break;
            case "BT-510(a)-Organization-Company":
                if (profilJoue != null && StringUtils.hasText(profilJoue.getAdresse())) {
                    return addToObject(formulaireObjet, id, repeatable ? List.of(profilJoue.getAdresse()) : profilJoue.getAdresse(), nodeStop, parentNodeId, versionSdk);
                }
                break;
            case "BT-512-Organization-Company":
                if (profilJoue != null && StringUtils.hasText(profilJoue.getCodePostal())) {
                    return addToObject(formulaireObjet, id, repeatable ? List.of(profilJoue.getCodePostal()) : profilJoue.getCodePostal(), nodeStop, parentNodeId, versionSdk);
                }
                break;

            default:
                log.debug("Pas de correspondance pour {}", id);

        }
        return formulaireObjet;
    }

}
