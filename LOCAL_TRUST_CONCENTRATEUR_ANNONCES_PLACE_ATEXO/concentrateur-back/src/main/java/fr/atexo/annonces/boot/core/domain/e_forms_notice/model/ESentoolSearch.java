package fr.atexo.annonces.boot.core.domain.e_forms_notice.model;

import fr.atexo.annonces.modeles.StatutENoticeEnum;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Setter
@Getter
@Builder
public class ESentoolSearch {

    private StatutENoticeEnum status;
    @NotEmpty(message = "La date de début de réception est obligatoire")
    private String receivedFrom;
    private String receivedTo;
    private String publishedFrom;
    private String publishedTo;
    private String noDocExt;
}
