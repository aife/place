package fr.atexo.annonces.boot.core.domain.e_forms_notice.model;

import fr.atexo.annonces.config.XmlElement;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
public class FailedAssertRepresentation {
    private String id;
    private String test;
    private String location;
    private String text;
    private List<XmlElement> elements;
}
