package fr.atexo.annonces.boot.repository;


import fr.atexo.annonces.boot.entity.EFormsProcedureEntity;
import fr.atexo.annonces.config.DaoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EFormsProcedureRepository extends DaoRepository<EFormsProcedureEntity, Long> {

    EFormsProcedureEntity findByCode(String code);
}
