package fr.atexo.annonces.boot.core.domain.e_forms_sdk.model;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Privacy {
    private String code;
    private String unpublishedFieldId;
    private String reasonCodeFieldId;
    private String reasonDescriptionFieldId;
    private String publicationDateFieldId;


}
