package fr.atexo.annonces.dto;

import fr.atexo.annonces.modeles.StatutTypeAvisAnnonceEnum;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StatutStatistique {

    private StatutTypeAvisAnnonceEnum statut;
    private Long total;
}
