package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.entity.ConsultationFiltre;
import fr.atexo.annonces.boot.entity.Offre;
import fr.atexo.annonces.boot.entity.PlateformeConfiguration;
import fr.atexo.annonces.boot.entity.Support;
import fr.atexo.annonces.boot.repository.ConsultationFiltreRepository;
import fr.atexo.annonces.boot.repository.OffreTypeProcedureRepository;
import fr.atexo.annonces.boot.repository.PlateformeConfigurationRepository;
import fr.atexo.annonces.boot.repository.SupportRepository;
import fr.atexo.annonces.dto.ConsultationFiltreDTO;
import fr.atexo.annonces.dto.OffreDTO;
import fr.atexo.annonces.dto.SupportDTO;
import fr.atexo.annonces.mapper.ConsultationFiltreMapper;
import fr.atexo.annonces.mapper.OffreMapper;
import fr.atexo.annonces.mapper.SupportMapper;
import fr.atexo.annonces.modeles.SupportSpecification;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Gestion des supports de publication
 */
@Service
public class SupportServiceImpl implements SupportService {

    private final SupportRepository supportRepository;
    private final PlateformeConfigurationRepository plateformeConfigurationRepository;
    private final ConsultationFiltreRepository consultationFiltreRepository;
    private final SupportMapper supportMapper;
    private final ConsultationFiltreMapper consultationFiltreMapper;
    private final OffreTypeProcedureRepository offreTypeProcedureRepository;

    public SupportServiceImpl(SupportRepository supportRepository, PlateformeConfigurationRepository plateformeConfigurationRepository, ConsultationFiltreRepository consultationFiltreRepository, SupportMapper supportMapper, OffreMapper offreMapper, ConsultationFiltreMapper consultationFiltreMapper, OffreTypeProcedureRepository offreTypeProcedureRepository) {
        this.supportRepository = supportRepository;
        this.plateformeConfigurationRepository = plateformeConfigurationRepository;
        this.consultationFiltreRepository = consultationFiltreRepository;
        this.supportMapper = supportMapper;
        this.consultationFiltreMapper = consultationFiltreMapper;
        this.offreTypeProcedureRepository = offreTypeProcedureRepository;
    }

    @Override
    public List<SupportDTO> getSupports(Double montant, Boolean national, Boolean europeen, List<String> jal, List<String> pqr,
                                        boolean includeOffres, String idPlateform, Integer idConsultation, List<String> typeProcedure, List<String> organismes, List<String> blocs, Boolean allJal, Boolean allPqr) {
        List<Support> supports = getSupportsEntity(montant, national, europeen, jal, pqr, idPlateform, idConsultation, typeProcedure, organismes, blocs, allJal, allPqr);
        return supportMapper.supportsToSupportDTOs(supports, includeOffres);
    }

    private List<Support> getSupportsEntity(Double montant, Boolean national, Boolean europeen, List<String> jal, List<String> pqr, String idPlateform, Integer idConsultation, List<String> typeProcedure, List<String> organismes, List<String> blocs, Boolean allJal, Boolean allPqr) {
        if (idConsultation != null) {
            List<String> departements = new ArrayList<>();
            if (!CollectionUtils.isEmpty(jal)) {
                departements = jal;
            } else if (!CollectionUtils.isEmpty(pqr)) {
                departements = pqr;
            }
            ConsultationFiltreDTO dto = ConsultationFiltreDTO.builder()
                    .codeDepartements(departements)
                    .national(Boolean.TRUE.equals(national))
                    .europeen(Boolean.TRUE.equals(europeen))
                    .idPlatform(idPlateform)
                    .idConsultation(idConsultation)
                    .jal(!CollectionUtils.isEmpty(jal) || Boolean.TRUE.equals(allJal))
                    .pqr(!CollectionUtils.isEmpty(pqr) || Boolean.TRUE.equals(allPqr))
                    .montant(montant)
                    .build();
            this.updateFiltreSupport(dto);
        }
        PlateformeConfiguration plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(idPlateform);
        List<String> supportsCode = new ArrayList<>();
        String pays = "FRA";
        List<Integer> idsProcedure = new ArrayList<>();
        List<String> acronymeOrganismes = new ArrayList<>();
        if (plateformeConfiguration != null) {
            if (StringUtils.hasText(plateformeConfiguration.getSupports()))
                supportsCode = List.of(plateformeConfiguration.getSupports().split(","));
            pays = plateformeConfiguration.getPays();
            if (Boolean.TRUE.equals(plateformeConfiguration.isAssociationProcedure()) && !CollectionUtils.isEmpty(typeProcedure)) {
                idsProcedure = offreTypeProcedureRepository.findAllIdByProcedureAbbreviation(typeProcedure);
            }
            if (Boolean.TRUE.equals(plateformeConfiguration.isAssociationOrganisme()) && !CollectionUtils.isEmpty(organismes)) {
                acronymeOrganismes = organismes;
            }
        }


        SupportSpecification supportSpecification = new SupportSpecification(montant, national, europeen, jal, pqr, pays, supportsCode, idsProcedure, acronymeOrganismes, blocs, allJal, allPqr);
        return supportRepository.findAll(supportSpecification);
    }

    @Override
    public List<OffreDTO> getPlateformeOffres(Double montant, Boolean national, Boolean europeen, List<String> jal, List<String> pqr,
                                              String idPlateform, Integer idConsultation, List<String> typeProcedure, List<String> organismes, List<String> blocs) {
        return this.getSupportsEntity(montant, national, europeen, jal, pqr, idPlateform, idConsultation, typeProcedure, organismes, blocs, true, true)
                .stream()
                .map(Support::getOffres)
                .flatMap(Set::stream)
                .map(offre -> {
                    Offre offreAssociee = offre.getOffreAssociee();
                    return OffreDTO.builder()
                            .code(offre.getCode())
                            .libelle(offre.getLibelle())
                            .offreAssociee(offreAssociee == null ? null : OffreDTO.builder()
                                    .code(offreAssociee.getCode())
                                    .libelle(offreAssociee.getLibelle())
                                    .build())
                            .build();
                })
                .distinct()
                .sorted(Comparator.comparing(OffreDTO::getCode))
                .collect(Collectors.toList());
    }

    @Override
    public ConsultationFiltreDTO getFiltreSupport(Integer idConsultation, String idPlateforme, Boolean europeen) {
        ConsultationFiltre consultation = consultationFiltreRepository.findByIdPlatformAndIdConsultation(idPlateforme, idConsultation);
        if (consultation != null) {
            PlateformeConfiguration plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(idPlateforme);
            if (plateformeConfiguration != null) {
                if (consultation.isJal() && !plateformeConfiguration.isJal()) {
                    consultation.setJal(false);
                }
                if (consultation.isPqr() && !plateformeConfiguration.isPqr()) {
                    consultation.setPqr(false);
                }
                if (consultation.isEuropeen() && !plateformeConfiguration.isEuropeen()) {
                    consultation.setEuropeen(false);
                }
                if (consultation.isNational() && !plateformeConfiguration.isNational()) {
                    consultation.setNational(false);
                }
            } else {
                consultation.setEuropeen(false);
            }

            consultation = consultationFiltreRepository.save(consultation);
        }

        return consultationFiltreMapper.mapToDto(consultation);
    }

    @Override
    public List<SupportDTO> getAllSupports() {
        return supportMapper.supportsToSupportDTOsIgnoreOffres(supportRepository.findAll());
    }

    @Override
    public List<String> getSupportCodeGroupe(String idPlatform, List<String> typeProcedures, List<String> organismes, List<String> blocs) {

        PlateformeConfiguration plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(idPlatform);

        Boolean europeen = null;
        Boolean national = null;
        if (plateformeConfiguration != null) {
            if (plateformeConfiguration.isEuropeen()) {
                europeen = true;
            }
            if (plateformeConfiguration.isNational()) {
                national = true;
            }
        }

        return this.getSupportsEntity(null, national, europeen, null, null, idPlatform, null, typeProcedures, organismes, blocs, true, true)
                .stream()
                .peek(support -> {
                    if (support.getCodeGroupe() == null && !support.isEuropeen()) {
                        support.setCodeGroupe("NATIONAL");
                    }
                    if (support.getCodeGroupe() == null && support.isEuropeen()) {
                        support.setCodeGroupe("EUROPEEN");
                    }
                })
                .map(Support::getCodeGroupe)
                .distinct()
                .sorted()
                .toList();
    }

    @Override
    public List<SupportDTO> getSupportPlateforme(String idPlatform, List<String> typeProcedures, List<String> organismes, List<String> blocs) {
        PlateformeConfiguration plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(idPlatform);

        Boolean europeen = null;
        Boolean national = null;
        if (plateformeConfiguration != null) {
            if (plateformeConfiguration.isEuropeen()) {
                europeen = true;
            }
            if (plateformeConfiguration.isNational()) {
                national = true;
            }
        }

        var supports = this.getSupportsEntity(null, national, europeen, null, null, idPlatform, null, typeProcedures, organismes, blocs, true, true);
        return supportMapper.supportsToSupportDTOs(supports, true);
    }

    private ConsultationFiltreDTO updateFiltreSupport(@NotNull @Valid ConsultationFiltreDTO dto) {
        ConsultationFiltre saved = consultationFiltreRepository.findByIdPlatformAndIdConsultation(dto.getIdPlatform(), dto.getIdConsultation());
        ConsultationFiltre toSave = consultationFiltreMapper.mapToEntity(dto);
        if (saved != null) {
            toSave.setId(saved.getId());
            if ((!toSave.isJal() || !toSave.isPqr()) && StringUtils.isEmpty(toSave.getCodeDepartements())) {
                toSave.setCodeDepartements(saved.getCodeDepartements());
            }
        }
        return consultationFiltreMapper.mapToDto(consultationFiltreRepository.save(toSave));
    }
}
