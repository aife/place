package fr.atexo.annonces.boot.oauth;


import org.springframework.security.oauth2.common.exceptions.InvalidScopeException;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.OAuth2RequestValidator;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * Permet de valider les scopes dynamiques provenant de {@link DynamicScopeOAuth2RequestFactory} tout en supportant la
 * validation traditionnelle des scopes définis dans les informations du client.
 */
@Component
public class DynamicScopeOAuth2RequestValidator implements OAuth2RequestValidator {

    @Override
    public void validateScope(AuthorizationRequest authorizationRequest, ClientDetails client) {
        validateScope(authorizationRequest.getScope(), client.getScope());
    }

    @Override
    public void validateScope(TokenRequest tokenRequest, ClientDetails client) {
        validateScope(tokenRequest.getScope(), client.getScope());
    }

    private void validateScope(Set<String> requestScopes, Set<String> clientScopes) {

        if (clientScopes != null && !clientScopes.isEmpty()) {
            for (String scope : requestScopes) {
                if (!clientScopes.contains(scope) && !isDynamicScope(scope)) {
                    throw new InvalidScopeException("Invalid scope: " + scope, clientScopes);
                }
            }
        }

        if (requestScopes.isEmpty()) {
            throw new InvalidScopeException("Empty scope (either the client or the user is not allowed the requested scopes)");
        }
    }

    private boolean isDynamicScope(String scope) {
        return scope.startsWith(DynamicScopeOAuth2RequestFactory.PLATFORM_SCOPE)
                || scope.startsWith(DynamicScopeOAuth2RequestFactory.CONSULTATION_SCOPE)
                || scope.startsWith(DynamicScopeOAuth2RequestFactory.VALIDATION_SCOPE)
                || scope.equals(DynamicScopeOAuth2RequestFactory.ALL_CONSULTATION_SCOPE);
    }
}
