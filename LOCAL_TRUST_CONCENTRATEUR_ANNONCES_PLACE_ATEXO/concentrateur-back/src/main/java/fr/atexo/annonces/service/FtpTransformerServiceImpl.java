package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.util.Base64;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;

/**
 * Génère un export au format byte[] à partir d'objets métiers.
 */
@Service
@Slf4j
public class FtpTransformerServiceImpl implements FtpTransformerService {

    private final SuiviAnnonceService suiviAnnonceService;

    public FtpTransformerServiceImpl(SuiviAnnonceService suiviAnnonceService) {
        this.suiviAnnonceService = suiviAnnonceService;
    }

    @Override
    public byte[] exportPdfToByteArray(SuiviAnnonce suiviAnnonce) {
        log.info("exportPdfToByteArray : {}", suiviAnnonce.getIdConsultation());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        suiviAnnonceService.exportToDocument(suiviAnnonce, outputStream, suiviAnnonce.isSimplifie(), suiviAnnonce.getOffre().isConvertToPdf());
        return outputStream.toByteArray();
    }

    @Override
    public byte[] exportXmlTransactionToByteArray(SuiviAnnonce suiviAnnonce) {
        log.info("exportXmlTransactionToByteArray : {} / {}", suiviAnnonce.getIdConsultation(), suiviAnnonce.getIdPlatform());
        return suiviAnnonceService.toXmlTransaction(suiviAnnonce).getBytes();
    }

    @Override
    public byte[] exportXmlToByteArray(SuiviAnnonce suiviAnnonce) {
        log.info("exportXmlToByteArray : {} / {}", suiviAnnonce.getIdConsultation(), suiviAnnonce.getIdPlatform());
        String xmlBase64 = suiviAnnonce.getXml();
        return new String(Base64.decodeBase64(xmlBase64)).getBytes();

    }

}
