package fr.atexo.annonces.reseau;

import fr.atexo.annonces.service.MolService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("index.php5")
@Tag(name = "Marchés OnLine")
@Slf4j
public class MolPhpController {
    private final MolService molService;

    public MolPhpController(MolService molService) {
        this.molService = molService;
    }

    @PostMapping(produces = {MediaType.APPLICATION_XML_VALUE})
    public String generateXmlResponse(@RequestBody String request, @RequestParam(required = false) String soap,
                                      @RequestParam(required = false) String page) {
        if ("interfaceSuiviSychroneMol".equals(soap))
            return molService.generateXmlResponse(request, soap);
        return "Aucun service trouvé";
    }

}
