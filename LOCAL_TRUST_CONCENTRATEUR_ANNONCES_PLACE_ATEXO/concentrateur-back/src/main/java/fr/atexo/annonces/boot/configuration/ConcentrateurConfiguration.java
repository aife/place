package fr.atexo.annonces.boot.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.atexo.annonces.reseau.MailEnvoi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

import static fr.atexo.annonces.config.EFormsHelper.getObjectMapper;

@Configuration
public class ConcentrateurConfiguration {
    @Value("${annonces.mail.smtp.port}")
    private String portSmtp;
    @Value("${annonces.mail.smtp.host}")
    private String hostSmtp;
    @Value("${annonces.mail.smtp.username}")
    private String usernameSmtp;
    @Value("${annonces.mail.smtp.password}")
    private String passwordSmtp;

    @Value("${annonces.mail.smtp.auth}")
    private String authSmtp;
    @Value("${annonces.mail.smtp.starttls.enable}")
    private String starttlsEnableSmtp;
    @Value("${annonces.mail.transport.protocol}")
    private String transportProtocolSmtp;
    @Value("${annonces.mail.debug}")
    private String debugSmtp;

    @Value("${annonces.mail.port}")
    private String port;
    @Value("${annonces.mail.host}")
    private String host;

    @Value("${annonces.mail.password}")
    private String password;
    @Value("${annonces.mail.from}")
    private String from;
    @Value("${annonces.mail.to}")
    private String to;
    @Value("${annonces.mail.cc}")
    private String cc;
    @Value("${annonces.mail.actif}")
    private String actif;

    @Bean
    public MailEnvoi connecteurService() {
        MailEnvoi mailEnvoi = new MailEnvoi();
        mailEnvoi.setSmtpHost(host);
        mailEnvoi.setSmtpPort(port);
        mailEnvoi.setExpediteur(from);
        mailEnvoi.setPassword(password);
        mailEnvoi.setDestinataire(to);
        mailEnvoi.setCopieCachee(cc);
        mailEnvoi.setAlerteActive(actif);
        return mailEnvoi;
    }

    @Bean
    public JavaMailSenderImpl mailSender() {
        JavaMailSenderImpl sender = new JavaMailSenderImpl();
        sender.setHost(hostSmtp);
        sender.setPort(Integer.parseInt(portSmtp));
        sender.setUsername(usernameSmtp);
        sender.setPassword(passwordSmtp);
        Properties javaMailProperties = new Properties();
        javaMailProperties.put("mail.smtp.auth", authSmtp);
        javaMailProperties.put("mail.smtp.starttls.enable", starttlsEnableSmtp);
        javaMailProperties.put("mail.transport.protocol", transportProtocolSmtp);
        javaMailProperties.put("mail.debug", debugSmtp);
        sender.setJavaMailProperties(javaMailProperties);
        return sender;
    }

    @Bean
    public ObjectMapper objectMapper() {
        return getObjectMapper();
    }
}
