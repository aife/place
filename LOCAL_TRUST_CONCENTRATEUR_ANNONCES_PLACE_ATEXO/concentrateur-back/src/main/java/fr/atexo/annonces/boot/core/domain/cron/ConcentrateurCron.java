package fr.atexo.annonces.boot.core.domain.cron;

import fr.atexo.annonces.boot.core.domain.e_forms_notice.model.ESentoolSearch;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.IEFormsNoticeServices;
import fr.atexo.annonces.boot.entity.*;
import fr.atexo.annonces.boot.repository.*;
import fr.atexo.annonces.boot.ws.eforms.ESentoolWS;
import fr.atexo.annonces.config.EFormsHelper;
import fr.atexo.annonces.config.SearchCriteria;
import fr.atexo.annonces.config.SearchOperatorEnum;
import fr.atexo.annonces.dto.FileStatusEnum;
import fr.atexo.annonces.dto.Token;
import fr.atexo.annonces.dto.publication.AVISEMIS;
import fr.atexo.annonces.jaxb.suivi.simap.PublicationInfo;
import fr.atexo.annonces.jaxb.suivi.simap.ReponseSimap;
import fr.atexo.annonces.jaxb.suivi.simap.ReponseSimapPage;
import fr.atexo.annonces.jaxb.suivi.simap.TedLinks;
import fr.atexo.annonces.modeles.DashboardSuiviTypeAvisSpecification;
import fr.atexo.annonces.modeles.StatutENoticeEnum;
import fr.atexo.annonces.modeles.StatutSuiviAnnonceEnum;
import fr.atexo.annonces.modeles.StatutTypeAvisAnnonceEnum;
import fr.atexo.annonces.service.MolService;
import fr.atexo.annonces.service.SuiviAnnonceService;
import fr.atexo.annonces.service.SuiviTypeAvisPubService;
import fr.atexo.annonces.service.TNCPService;
import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class ConcentrateurCron {

    private final SupportRepository supportRepository;
    private final EFormsFormulaireRepository eFormsFormulaireRepository;
    private final SuiviTypeAvisPubRepository suiviTypeAvisPubRepository;
    private final IEFormsNoticeServices ieFormsNoticeServices;
    private final ESentoolWS esentoolWS;
    private final SuiviAnnonceService suiviAnnonceService;
    private final SuiviTypeAvisPubService suiviTypeAvisPubService;
    private final SuiviAnnonceRepository suiviAnnonceRepository;
    private final AgentSessionRepository agentSessionRepository;
    private final PlateformeConfigurationRepository plateformeConfigurationRepository;
    private final MolService molService;
    private final TNCPService tncpService;
    private static final long DELAI_SUPPRESION_FICHIER_TMP = 1000L * 60L * 60L * 24L;

    @Value("${annonces.plateforme}")
    private String plateforme;
    private final Object lockTed = new Object();
    private final Object lockAgent = new Object();
    private final Object lockAvis = new Object();
    private final Object lockDocuments = new Object();
    private final Object lockMol = new Object();
    private final Object lockTncp = new Object();
    private final Object lockLesEchos = new Object();

    public ConcentrateurCron(SupportRepository supportRepository, EFormsFormulaireRepository eFormsFormulaireRepository, SuiviTypeAvisPubRepository suiviTypeAvisPubRepository, IEFormsNoticeServices ieFormsNoticeServices, ESentoolWS esentoolWS, SuiviAnnonceService suiviAnnonceService, SuiviTypeAvisPubService suiviTypeAvisPubService, SuiviAnnonceRepository suiviAnnonceRepository, AgentSessionRepository agentSessionRepository, PlateformeConfigurationRepository plateformeConfigurationRepository, MolService molService, TNCPService tncpService) {
        this.supportRepository = supportRepository;
        this.eFormsFormulaireRepository = eFormsFormulaireRepository;
        this.suiviTypeAvisPubRepository = suiviTypeAvisPubRepository;
        this.ieFormsNoticeServices = ieFormsNoticeServices;
        this.esentoolWS = esentoolWS;
        this.suiviAnnonceService = suiviAnnonceService;
        this.suiviTypeAvisPubService = suiviTypeAvisPubService;
        this.suiviAnnonceRepository = suiviAnnonceRepository;
        this.agentSessionRepository = agentSessionRepository;
        this.plateformeConfigurationRepository = plateformeConfigurationRepository;
        this.molService = molService;
        this.tncpService = tncpService;
    }


    @Scheduled(cron = "${annonces.cron.clean.agent}")
    public void refreshToken() {
        synchronized (lockAgent) {
            if (!ifPlateformsIsCronMaster()) {
                return;
            }
            Pageable page = Pageable.ofSize(100).withPage(0);
            Page<AgentSessionEntity> all;
            int totalPage;
            do {
                all = agentSessionRepository.findAll(page);
                totalPage = all.getTotalPages();
                all.forEach(agentSessionEntity -> {
                    Token token = Token.builder().accessToken(agentSessionEntity.getToken()).build();
                    Instant now = Instant.now();
                    if (now.isAfter(token.getExpiresTime())) {
                        agentSessionRepository.delete(agentSessionEntity);
                    }

                });
                page = page.next();
            } while (page.getPageNumber() < totalPage);
        }
    }

    @Scheduled(cron = "${annonces.cron.suivi.eforms}")
    public void suiviTedNoticeSubmitted() {
        synchronized (lockTed) {
            if (!ifPlateformsIsCronMaster()) {
                return;
            }
            if (!ifTedActive()) {
                return;
            }
            // if time between 08:00 and 19:00
            ZonedDateTime now = ZonedDateTime.now();
            if (now.getHour() < 8 || now.getHour() > 19) {
                return;
            }
            log.debug("Synchronisation des avis TED");
            List<SearchCriteria> searchCriteria = new ArrayList<>();
            searchCriteria.add(SearchCriteria.builder().key("statut").operation(SearchOperatorEnum.IN).value(List.of(StatutENoticeEnum.STOPPING, StatutENoticeEnum.SUBMITTED, StatutENoticeEnum.PUBLISHING)).build());
            suiviTedNotice(searchCriteria);
        }
    }

    @Scheduled(cron = "${annonces.cron.suivi.eforms-submitting}")
    public void suiviTedNoticeSubmitting() {
        synchronized (lockTed) {
            if (!ifPlateformsIsCronMaster()) {
                return;
            }
            if (!ifTedActive()) {
                return;
            }
            // if time between 08:00 and 19:00 and not weekend
            ZonedDateTime now = ZonedDateTime.now();
            if (now.getHour() < 8 || now.getHour() > 19 || now.getDayOfWeek().equals(DayOfWeek.SATURDAY) || now.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
                return;
            }
            log.debug("Synchronisation des avis TED");
            List<SearchCriteria> searchCriteria = new ArrayList<>();
            searchCriteria.add(SearchCriteria.builder().key("statut").operation(SearchOperatorEnum.IN).value(List.of(StatutENoticeEnum.SUBMITTING)).build());
            suiviTedNotice(searchCriteria);
        }
    }

    private void suiviTedNotice(List<SearchCriteria> searchCriteria) {
        PageRequest pageable = PageRequest.of(0, 10);
        int totalPage;
        Page<EFormsFormulaireEntity> allByCriteria;
        do {
            allByCriteria = eFormsFormulaireRepository.findAllByCriteria(searchCriteria, pageable);
            totalPage = allByCriteria.getTotalPages();
            allByCriteria.forEach(eFormsFormulaireEntity -> {
                try {
                    ieFormsNoticeServices.updateEForms(eFormsFormulaireEntity);
                } catch (Exception e) {
                    log.error("Erreur lors de l'envoi de la requete de suivi {}", e.getMessage());
                }
            });
            pageable = pageable.next();
        } while (pageable.getPageNumber() < totalPage);
        log.debug("Fin de la synchronisation des avis TED");
    }


    @Scheduled(cron = "${annonces.cron.suivi.mol}")
    public void suiviMol() {
        synchronized (lockMol) {
            if (!ifPlateformsIsCronMaster()) {
                return;
            }
            if (!ifMolActive()) {
                return;
            }
            log.debug("Synchronisation des avis MOL");
            Date dateFin = new Date();
            LocalDateTime ldt = LocalDateTime.ofInstant(dateFin.toInstant(), ZoneId.systemDefault());
            ldt = ldt.minusDays(3);
            Date date = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
            var list = molService.getAvisToSynchronize(date, dateFin);
            list.forEach(avis -> {
                String[] refs = avis.getREFCONSULTATION().split("#");
                String idConsultationMpe = refs[refs.length - 1];
                String idPlatform = refs[0];
                String organisme = refs[1];
                try {
                    for (AVISEMIS.AVIS.SUPPORT support : avis.getSUPPORT()) {
                        String code = support.getCODE();
                        if (!"BOA".equals(code) && !"JOU".equals(code)) {
                            continue;
                        }
                        List<SuiviAnnonce> suiviAnnonces = molService.getAnnonces(avis, idPlatform, idConsultationMpe, organisme, support);
                        suiviAnnonces.forEach(suiviAnnonce -> {
                            try {
                                molService.synchronize(avis, suiviAnnonce, support);
                            } catch (Exception e) {
                                log.error("Erreur lors de la synchronisation de l'avis {}", avis.getREFCONSULTATION());
                            }
                        });
                    }
                } catch (Exception e) {
                    log.error("Erreur lors de la synchronisation de l'avis {}", avis.getREFCONSULTATION());
                }
            });
            log.debug("Fin de la synchronisation des avis MOL");
        }
    }

    @Scheduled(cron = "${annonces.suivi.lesechos.scheduling}")
    public void suiviLesEchos() {
        synchronized (lockLesEchos) {

            if (!ifPlateformsIsCronMaster()) {
                return;
            }
            if (!isLesEchoActive()) {
                return;
            }
            log.debug("Synchronisation des avis Les Echos");
            suiviAnnonceService.synchronizeLesEchos();
            log.debug("Fin de la synchronisation des avis Les Echos");
        }
    }


    @Scheduled(cron = "${annonces.cron.suivi.docgen}")
    public void checkDocumentStatus() {
        synchronized (lockDocuments) {
            if (!ifPlateformsIsCronMaster()) {
                return;
            }
            log.debug("Synchronisation des documents des avis");
            List<FileStatusEnum> status = List.of(FileStatusEnum.REQUEST_TO_OPEN, FileStatusEnum.OPENED, FileStatusEnum.EDITED_AND_SAVED);
            Page<SuiviTypeAvisPub> typeAvisPubs;
            PageRequest pageable = PageRequest.of(0, 10);
            int totalPage;
            do {
                typeAvisPubs = suiviTypeAvisPubRepository.findByStatutEditionInAndStatut(status, StatutTypeAvisAnnonceEnum.BROUILLON, pageable);
                totalPage = typeAvisPubs.getTotalPages();
                typeAvisPubs.getContent().forEach(suiviTypeAvisPubService::suiviDocument);
                pageable = pageable.next();
            } while (pageable.getPageNumber() < totalPage);
            log.debug("Fin de la synchronisation des documents des avis");

            log.debug("Synchronisation des documents des annonces");
            Page<SuiviAnnonce> suiviAnnonces;
            pageable = PageRequest.of(0, 10);
            do {
                suiviAnnonces = suiviAnnonceRepository.findByStatutEditionInAndStatutIn(status, List.of(StatutSuiviAnnonceEnum.BROUILLON, StatutSuiviAnnonceEnum.COMPLET), pageable);
                totalPage = suiviAnnonces.getTotalPages();
                suiviAnnonces.getContent().forEach(suiviAnnonceService::suiviDocument);
                pageable = pageable.next();
            } while (pageable.getPageNumber() < totalPage);
            log.debug("Fin de la synchronisation des documents des avis");
        }
    }

    @Scheduled(cron = "${annonces.cron.publication.retry}")
    public void checkRetryStatus() {
        Instant date = Instant.now().minusSeconds(1 * 60);
        suiviAnnonceRepository.updateRetryAnnonces(date);

    }

    @Scheduled(cron = "${annonces.cron.publication.avis}")
    public void sendPublication() {
        synchronized (lockAvis) {
            if (!ifPlateformsIsCronMaster()) {
                return;
            }
            log.debug("Publication des avis européens en attente");
            ZonedDateTime now = ZonedDateTime.now();
            DashboardSuiviTypeAvisSpecification specification = DashboardSuiviTypeAvisSpecification.builder().statuts(Set.of(StatutTypeAvisAnnonceEnum.ENVOI_PLANIFIER)).dateEnvoiEuropeenPrevisionnelMax(now).build();
            Pageable pageable = PageRequest.of(0, 10);
            int totalPage;
            Page<SuiviTypeAvisPub> avisPubs;
            do {
                avisPubs = suiviTypeAvisPubRepository.findAll(specification, pageable);
                totalPage = avisPubs.getTotalPages();
                avisPubs.getContent().forEach(avisPub -> {
                    List<SuiviAnnonce> suiviAnnonces = avisPub.getAnnonces().stream().filter(suiviAnnonce -> {
                        Support support = suiviAnnonce.getSupport();
                        Offre offre = suiviAnnonce.getOffre();
                        Offre offreRacine = avisPub.getOffreRacine();
                        return offre != null && support != null && offreRacine != null && offreRacine.getId().equals(offre.getId()) && !StatutSuiviAnnonceEnum.EN_COURS_DE_TRAITEMENT.equals(suiviAnnonce.getStatut()) && !StatutSuiviAnnonceEnum.PUBLIER.equals(suiviAnnonce.getStatut()) && !StatutSuiviAnnonceEnum.EN_COURS_DE_PUBLICATION.equals(suiviAnnonce.getStatut()) && !StatutSuiviAnnonceEnum.EN_ATTENTE.equals(suiviAnnonce.getStatut());
                    }).peek(suiviAnnonce -> {
                        suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.EN_ATTENTE);
                        suiviAnnonce.setDateDemandeEnvoi(now.toInstant());
                    }).toList();
                    if (!CollectionUtils.isEmpty(suiviAnnonces)) {
                        suiviAnnonceRepository.saveAll(avisPub.getAnnonces());
                        avisPub.setDateDemandeEnvoi(now);
                    } else {
                        List<StatutSuiviAnnonceEnum> statuts = new ArrayList<>(avisPub.getAnnonces().stream().map(SuiviAnnonce::getStatut).distinct().toList());
                        avisPub.setStatut(StatutTypeAvisAnnonceEnum.getStatutFromAnnonceEnum(statuts));
                    }
                });
                suiviTypeAvisPubRepository.saveAll(avisPubs);
                pageable = pageable.next();
            } while (pageable.getPageNumber() < totalPage);
            log.debug("Fin de la publication des avis européens en attente");
            log.debug("Publication des avis nationaux après l'envoi des avis européens");
            specification = DashboardSuiviTypeAvisSpecification.builder().statuts(Set.of(StatutTypeAvisAnnonceEnum.ENVOI_PLANIFIER)).dateEnvoiNationalPrevisionnelMax(now.plusMinutes(5)).build();
            pageable = PageRequest.of(0, 10);
            do {
                avisPubs = suiviTypeAvisPubRepository.findAll(specification, pageable);
                totalPage = avisPubs.getTotalPages();
                avisPubs.getContent().forEach(avisPub -> {
                    List<SuiviAnnonce> suiviAnnonces = avisPub.getAnnonces().stream().peek(suiviAnnonce -> {
                        suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.EN_ATTENTE);
                        suiviAnnonce.setDateDemandeEnvoi(now.toInstant());
                    }).toList();
                    if (!CollectionUtils.isEmpty(suiviAnnonces)) {
                        suiviAnnonceRepository.saveAll(avisPub.getAnnonces());
                        avisPub.setDateDemandeEnvoi(now);
                    } else {
                        List<StatutSuiviAnnonceEnum> statuts = new ArrayList<>(avisPub.getAnnonces().stream().map(SuiviAnnonce::getStatut).distinct().toList());
                        avisPub.setStatut(StatutTypeAvisAnnonceEnum.getStatutFromAnnonceEnum(statuts));
                    }
                });
                suiviTypeAvisPubRepository.saveAll(avisPubs);
                pageable = pageable.next();
            } while (pageable.getPageNumber() < totalPage);
            log.debug("Fin de la publication des avis nationaux après l'envoi des avis européens");
        }
    }

    public boolean ifPlateformsIsCronMaster() {
        PlateformeConfiguration plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(plateforme);
        if (plateformeConfiguration == null) {
            return false;
        }

        return plateformeConfiguration.isCronMaster();
    }

    public boolean ifTedActive() {
        PlateformeConfiguration plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(plateforme);
        if (plateformeConfiguration == null) {
            return false;
        }

        boolean europeen = plateformeConfiguration.isEuropeen();
        if (!europeen) {
            return false;
        }
        String supports = plateformeConfiguration.getSupports();
        if (StringUtils.hasText(supports)) {
            return Arrays.asList(supports.split(",")).contains("ENOTICES");
        }
        return supportRepository.existsByCodeInAndActifIsTrue(List.of("ENOTICES"));
    }

    public boolean isTncpActive() {
        PlateformeConfiguration plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(plateforme);
        if (plateformeConfiguration == null) {
            return false;
        }

        boolean national = plateformeConfiguration.isNational();
        if (!national) {
            return false;
        }
        String supports = plateformeConfiguration.getSupports();
        if (StringUtils.hasText(supports)) {
            return Arrays.asList(supports.split(",")).contains("BOAMP_TNCP");
        }
        return supportRepository.existsByCodeInAndActifIsTrue(List.of("BOAMP_TNCP"));
    }

    public boolean isLesEchoActive() {
        PlateformeConfiguration plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(plateforme);
        if (plateformeConfiguration == null) {
            return false;
        }

        boolean jal = plateformeConfiguration.isJal();
        boolean pqr = plateformeConfiguration.isPqr();
        if (!jal && !pqr) {
            return false;
        }
        String codeDestination = "DEMATIS";
        String supports = plateformeConfiguration.getSupports();
        if (StringUtils.hasText(supports)) {
            return supportRepository.existsByCodeInAndCodeDestinationAndActifIsTrue(Arrays.asList(supports.split(",")), codeDestination);
        }
        return supportRepository.existsByCodeDestinationInAndActifIsTrue(List.of("DEMATIS"));
    }

    public boolean ifMolActive() {
        PlateformeConfiguration plateformeConfiguration = plateformeConfigurationRepository.findByIdPlateforme(plateforme);
        if (plateformeConfiguration == null) {
            return false;
        }

        boolean national = plateformeConfiguration.isNational();
        if (!national) {
            return false;
        }
        String supports = plateformeConfiguration.getSupports();
        List<String> codes = List.of("BOAMP_MOL", "MOL", "JOUE", "BOAMP", "BOAMPJOUE", "MONITEUR");
        if (StringUtils.hasText(supports)) {
            return Arrays.stream(supports.split(",")).anyMatch(codes::contains);
        }
        return supportRepository.existsByCodeInAndActifIsTrue(codes);
    }

    //méthode appelée dans spring integration
    public List<SuiviAnnonce> getDemandesPublicationEnAttente() {
        if (!this.ifPlateformsIsCronMaster()) {
            return new ArrayList<>();
        }

        List<SuiviAnnonce> demandesPublicationEnAttente = suiviAnnonceRepository.getDemandesPublicationEnAttente();
        if (!CollectionUtils.isEmpty(demandesPublicationEnAttente)) {
            log.info("Nombre d'annonce en attente de publication {}", demandesPublicationEnAttente.size());
            return demandesPublicationEnAttente.stream().map(suiviAnnonce -> {
                suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.EN_COURS_DE_TRAITEMENT);
                suiviAnnonce.setDateDebutEnvoi(Instant.now());
                log.info("En cours de traitement de l'annonce {}", suiviAnnonce.getId());
                return suiviAnnonceRepository.save(suiviAnnonce);
            }).collect(Collectors.toList());
        }

        return new ArrayList<>();
    }

    @Scheduled(cron = "0 0 1 * * *")
    public void cleanTmpFiles() {
        File[] files = FileUtils.getTempDirectory().listFiles();
        long date = System.currentTimeMillis() - DELAI_SUPPRESION_FICHIER_TMP;
        for (File file : files) {
            if (FileUtils.isFileOlder(file, date)) {
                if (file.isFile()) {
                    log.info("fichier {} supprime", file.getAbsolutePath());
                    FileUtils.deleteQuietly(file);
                } else {
                    try (Stream<Path> walk = Files.walk(file.toPath())) {
                        walk.sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::deleteOnExit);
                    } catch (IOException e) {
                        log.error("Erreur lors de la suppression des fichiers temporaires {}", file.getAbsolutePath(), e);
                    }
                }
            }
        }

    }

    public void synchroniseAll(ESentoolSearch search) {
        int page = 0;
        int size = 100;
        ReponseSimapPage simapPage;
        int total = 0;
        do {
            simapPage = esentoolWS.searchNotice(page, size, search);
            List<ReponseSimap> content = simapPage.getContent();
            String pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'";
            ieFormsNoticeServices.updateTedFromList(content, pattern);
            total += simapPage.getNumberOfElements();
            log.info("Synchronisation de {} notices / {}", total, simapPage.getTotalElements());
            page++;
        } while (!simapPage.isLast());


    }

    public void synchroniseFile(MultipartFile template) {
        try (XSSFWorkbook xlsx = EFormsHelper.openFile(template.getInputStream())) {
            XSSFSheet sheet = xlsx.getSheetAt(0);
            FormulaEvaluator evaluator = xlsx.getCreationHelper().createFormulaEvaluator();
            List<ReponseSimap> reponseSimaps = new ArrayList<>();
            String pattern = "dd/MM/yyyy HH:mm:ss";
            int total = 0;
            int totalRows = sheet.getLastRowNum();
            for (int rownum = 1; rownum <= totalRows + 1; rownum++) {
                Row row = sheet.getRow(rownum);
                if (row == null) continue;
                CellValue cellValue = evaluator.evaluate(row.getCell(0));
                String sumissionId = cellValue == null ? null : cellValue.getStringValue();
                if (!StringUtils.hasText(sumissionId)) {
                    continue;
                }
                cellValue = evaluator.evaluate(row.getCell(1));
                String receivedOn = cellValue == null ? null : cellValue.getStringValue();
                cellValue = evaluator.evaluate(row.getCell(2));
                String statut = cellValue == null ? null : cellValue.getStringValue();
                cellValue = evaluator.evaluate(row.getCell(3));
                String statutDate = cellValue == null ? null : cellValue.getStringValue() + " 00:00:00";
                cellValue = evaluator.evaluate(row.getCell(4));
                String noDocExt = cellValue == null ? null : cellValue.getStringValue();
                cellValue = evaluator.evaluate(row.getCell(5));
                String noDocOjs = cellValue == null ? null : cellValue.getStringValue();
                cellValue = evaluator.evaluate(row.getCell(6));
                String form = cellValue == null ? null : cellValue.getStringValue();
                String lienTed = row.getCell(9).getStringCellValue();
                PublicationInfo.PublicationInfoBuilder publicationInfoBuilder = PublicationInfo.builder().noDocOjs(noDocOjs).publicationDate("PUBLISHED".equalsIgnoreCase(statut) ? statutDate : null);
                if (StringUtils.hasText(lienTed)) {
                    publicationInfoBuilder.tedLinks(TedLinks.builder()
                            .additionalProperties(Map.of("FR", lienTed))
                            .build());
                }
                reponseSimaps.add(ReponseSimap.builder().form(form).noDocExt(noDocExt).receivedAt(receivedOn).status(statut).statusUpdatedAt(statutDate).submissionId(sumissionId).publicationInfo(publicationInfoBuilder.build()).build());

                if (reponseSimaps.size() % 100 == 0) {
                    ieFormsNoticeServices.updateTedFromList(reponseSimaps, pattern);
                    total += reponseSimaps.size();
                    log.info("Synchronisation de {} notices / {}", total, totalRows);
                    reponseSimaps = new ArrayList<>();
                }

            }
            ieFormsNoticeServices.updateTedFromList(reponseSimaps, pattern);
            total += reponseSimaps.size();
            log.info("Synchronisation de {} notices / {}", total, totalRows);


        } catch (IOException e) {
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage(), e);
        }
    }
}
