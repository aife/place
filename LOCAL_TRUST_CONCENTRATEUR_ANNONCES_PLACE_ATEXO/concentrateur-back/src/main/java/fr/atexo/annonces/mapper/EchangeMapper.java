package fr.atexo.annonces.mapper;

import com.atexo.annonces.commun.mpe.ConsultationMpe;
import com.atexo.annonces.commun.mpe.Echange;
import com.atexo.annonces.commun.mpe.EnvoiMpe;
import eu.europa.publications.resource.schema.ted.r2_0_9.reception.TedEsenders;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.util.Pair;

@Mapper
public interface EchangeMapper extends Mappable<Pair<EnvoiMpe, ConsultationMpe>, Echange> {

    @Override
    @Mapping(target = "agent", source = "couple.first.agent")
    @Mapping(target = "organisme", source = "couple.first.agent.organisme")
    @Mapping(target = "service", source = "couple.first.agent.service")
    @Mapping(target = "consultation", source = "couple.second")
    Echange map(Pair<EnvoiMpe, ConsultationMpe> couple, TedEsenders tedEsenders, boolean rie);

}
