package fr.atexo.annonces.boot.ws.mpe.model;

import com.atexo.annonces.commun.mpe.ProfilJoueMpe;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class HydraProfilJoueResponse {

    @JsonProperty("hydra:member")
    private List<ProfilJoueMpe> list;
}
