package fr.atexo.annonces.boot.core.domain.xlsx.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.poi.ss.usermodel.CellType;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class XlsxColumn {
    private int index;
    private String value;
    private CellType type;
}
