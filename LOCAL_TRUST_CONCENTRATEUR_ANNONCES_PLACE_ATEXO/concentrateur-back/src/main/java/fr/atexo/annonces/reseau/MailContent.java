package fr.atexo.annonces.reseau;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;

/**
 * Représente un contenu de mail : soit le corps du mail, soit une PJ
 */
@Getter
@Setter

public class MailContent {

    private final Logger logger = LoggerFactory.getLogger(MailContent.class);
    private final String attachmentFilename;
    private final String body;
    private final ByteArrayResource attachmentFile;

    public MailContent(String body) {
        this.body = body;
        this.attachmentFilename = null;
        this.attachmentFile = null;
        logger.debug("Construction d'un paramètre de mail de type body avec le texte suivant : {}", body);
    }

    public MailContent(ByteArrayResource attachmentFile, String attachmentFilename) {
        this.attachmentFilename = attachmentFilename;
        this.attachmentFile = attachmentFile;
        this.body = null;
        logger.debug("Construction d'un paramètre de mail de type pièce jointe de nom {}",
                attachmentFilename);
    }

    public String getBody() {
        return body;
    }

    public ByteArrayResource getAttachmentFile() {
        return attachmentFile;
    }
}
