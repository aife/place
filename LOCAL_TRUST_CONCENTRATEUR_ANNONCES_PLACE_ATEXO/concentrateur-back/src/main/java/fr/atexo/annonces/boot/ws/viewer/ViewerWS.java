package fr.atexo.annonces.boot.ws.viewer;

import fr.atexo.annonces.dto.viewer.ViewerDTO;
import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.util.Base64;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
public class ViewerWS {
    private final RestTemplate atexoRestTemplate;
    private final ViewerConfiguration viewerConfiguration;

    public ViewerWS(RestTemplate atexoRestTemplate, ViewerConfiguration viewerConfiguration) {
        this.atexoRestTemplate = atexoRestTemplate;
        this.viewerConfiguration = viewerConfiguration;
    }

    public Resource reportPDF(byte[] binaryData, ViewerDTO metadata, String eformsToken) {
        try {
            metadata.setFormat("PDF");
            metadata.setFile(Base64.encodeBase64StringUnChunked(binaryData));
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("X-API-Key", StringUtils.hasText(eformsToken) ? eformsToken : viewerConfiguration.getApiKey());

            String url = viewerConfiguration.getBasePath() + viewerConfiguration.getWs().get("render");
            return atexoRestTemplate.postForObject(url, new HttpEntity<>(metadata, headers), Resource.class);
        } catch (RestClientResponseException e) {
            throw new AtexoException(ExceptionEnum.EFORMS_WS, e.getResponseBodyAsString(), e);
        } catch (Exception e) {
            throw new AtexoException(ExceptionEnum.EFORMS_WS, e.getMessage(), e);
        }
    }

    public String reportHtml(byte[] binaryData, ViewerDTO metadata) {
        try {
            metadata.setFormat("HTML");
            metadata.setFile(Base64.encodeBase64StringUnChunked(binaryData));
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("X-API-Key", viewerConfiguration.getApiKey());

            String url = viewerConfiguration.getBasePath() + viewerConfiguration.getWs().get("render");
            return atexoRestTemplate.postForObject(url, new HttpEntity<>(metadata, headers), String.class);
        } catch (RestClientResponseException e) {
            throw new AtexoException(ExceptionEnum.EFORMS_WS, e.getResponseBodyAsString(), e);
        } catch (Exception e) {
            throw new AtexoException(ExceptionEnum.EFORMS_WS, e.getMessage(), e);
        }
    }

}
