package fr.atexo.annonces.dto;

import lombok.*;

import java.io.Serializable;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IntermediaireDTO implements Serializable {

    private static final long serialVersionUID = -1410232521211913768L;

    private String idPlateforme;

    private String codeDestination;

    IntermediaireDTO intermediaire;

}
