package fr.atexo.annonces.boot.core.domain.e_forms_sdk.services;

import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.FieldDetails;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNotice;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.XmlStructureDetails;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface IEFormsSdkService {

    FieldDetails getFieldById(String id, String version);

    List<XmlStructureDetails> getNodeIdsById(String id, String version);

    Map<String, Object> getSdkI18n(String lang, String version);

    Map<String, Set<String>> getCodeById(String id, String lang, String search, String plateforme, String version);
    Map<String, Set<String>> getFieldsOptions(String id, String plateforme, String version, String search, String lang);

    String getParentCode(String id, String plateforme, String version);

    Object getI18n(String lang);

    Map<String, Object> getSurchargeI18n(Integer idOrganisme, String lang);

    SubNotice getNotice(String idNotice, String version);

}
