package fr.atexo.annonces.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

@Configuration
@Slf4j
public class RestTemplateConfig {
    private static final String SSL_PROTOCOL = "TLS";

    @Bean
    @Primary
    public RestTemplate restTemplate() {
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setConnectTimeout(5 * 60 * 1000);
        requestFactory.setReadTimeout(5 * 60 * 1000);
        requestFactory.setHttpClient(createHttpClient());
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter(StandardCharsets.UTF_8);
        restTemplate.getMessageConverters().add(0, stringHttpMessageConverter);
        restTemplate.getMessageConverters().forEach(httpMessageConverter -> {
            if (httpMessageConverter instanceof StringHttpMessageConverter) {
                ((StringHttpMessageConverter) httpMessageConverter).setDefaultCharset(StandardCharsets.UTF_8);
            }
            if (httpMessageConverter instanceof AllEncompassingFormHttpMessageConverter) {
                ((AllEncompassingFormHttpMessageConverter) httpMessageConverter).setCharset(StandardCharsets.UTF_8);
                ((AllEncompassingFormHttpMessageConverter) httpMessageConverter).setMultipartCharset(StandardCharsets.UTF_8);
            }

        });
        return restTemplate;
    }

    private HttpClient createHttpClient() {
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create().useSystemProperties();
        httpClientBuilder.setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE);
        httpClientBuilder.setSSLContext(createSSLContext());
        return httpClientBuilder.build();
    }

    private SSLContext createSSLContext() {
        SSLContext sslContext = null;
        try {
            sslContext = SSLContext.getInstance(SSL_PROTOCOL);
            sslContext.init(null, new TrustManager[]{TrustAllManager.THE_INSTANCE}, new SecureRandom());
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            log.error(e.getMessage());
        }
        return sslContext;
    }

}
