package fr.atexo.annonces.boot.ws.ressources;

import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class RessourcesWsAdapter implements RessourcesWsPort {

    private final RessourcesWS ressourcesWS;

    public RessourcesWsAdapter(RessourcesWS ressourcesWS) {
        this.ressourcesWS = ressourcesWS;
    }

    @Override
    public File downloadAvisTemplate(String ws, boolean simplifie) {
        return ws != null ? ressourcesWS.downloadAvisTemplate(ws) : ressourcesWS.downloadAvisTemplate(simplifie);
    }

}
