package fr.atexo.annonces.boot.core.domain.e_forms_notice.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ValidationResult {
    private boolean valid;
    private boolean validXml;
    private String xml;
    private String svrlString;
    private List<FailedAssertRepresentation> errors;
}
