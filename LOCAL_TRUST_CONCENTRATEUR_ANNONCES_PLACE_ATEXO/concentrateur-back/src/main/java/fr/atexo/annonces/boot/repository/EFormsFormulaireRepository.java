package fr.atexo.annonces.boot.repository;


import fr.atexo.annonces.boot.entity.EFormsFormulaireEntity;
import fr.atexo.annonces.config.DaoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EFormsFormulaireRepository extends DaoRepository<EFormsFormulaireEntity, Long> {
    boolean existsByUuid(String uuid);

    EFormsFormulaireEntity findByUuidAndSuiviAnnonceIsNotNull(String uuid);

    EFormsFormulaireEntity findByNoDocExtAndSuiviAnnonceIsNotNull(String noDocExt);
}
