package fr.atexo.annonces.boot.core.domain.xlsx.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@Getter
@AllArgsConstructor
public enum XlsxTypeProcedureHeaderEnum {

    ID_PROCEDURE(0, "id_type_procedure"),
    ABREVIATION_PROCEDURE(1, "abreviation Procedure"),
    LIBELLE_PROCEDURE(2, "libelle Procedure"),
    CORRESPONDANCE_EFORMS(3, "correspondance EForms"),
    AVIS_EXTERNE(4, "avis Externe");

    private final int index;
    private final String title;

    public static List<XlsxHeader> transform() {
        return Arrays
                .stream(XlsxTypeProcedureHeaderEnum.values())
                .map(xlsxProjetAchatHeaderEnum -> XlsxHeader
                        .builder()
                        .index(xlsxProjetAchatHeaderEnum.getIndex())
                        .title(xlsxProjetAchatHeaderEnum.getTitle())
                        .build())
                .toList();
    }
}
