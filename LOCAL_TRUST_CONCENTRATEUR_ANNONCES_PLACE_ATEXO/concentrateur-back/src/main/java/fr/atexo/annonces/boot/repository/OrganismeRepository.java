package fr.atexo.annonces.boot.repository;

import fr.atexo.annonces.boot.entity.Organisme;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Repository pour accéder aux objets Support
 */
@Repository
public interface OrganismeRepository extends JpaRepository<Organisme, Integer>, JpaSpecificationExecutor<Organisme> {

    Optional<Organisme> findByAcronymeOrganismeAndPlateformeUuid(String acronyme, String plateformeUuid);

    List<Organisme> findByPlateformeUuid(String plateforme);
}
