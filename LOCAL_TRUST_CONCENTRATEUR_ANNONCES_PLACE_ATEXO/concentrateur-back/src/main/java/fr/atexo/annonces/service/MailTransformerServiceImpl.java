package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.entity.*;
import fr.atexo.annonces.boot.repository.SuiviAnnonceRepository;
import fr.atexo.annonces.reseau.MailContent;
import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StreamUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Construit des MailMessage à partir d'objets métiers. Utilise les informations des objets fournis pour compléter le
 * mail : sujet, corps, pièces jointes, etc.
 */
@Service
@Slf4j
public class MailTransformerServiceImpl implements MailTransformerService {

    private final EspaceDocumentaireService espaceDocumentaireService;
    private final SuiviAnnonceService suiviAnnonceService;
    private final TemplateService templateService;
    private final SuiviAnnonceRepository suiviAnnonceRepository;
    private final PieceJointeService pieceJointeService;

    private final JavaMailSender mailSender;

    @Value("classpath:/xsl/mail/mailBody.xsl")
    private Resource mailBodyXslResource;


    public MailTransformerServiceImpl(EspaceDocumentaireService espaceDocumentaireService, SuiviAnnonceService suiviAnnonceService, TemplateService templateService, SuiviAnnonceRepository suiviAnnonceRepository, PieceJointeService pieceJointeService, JavaMailSender mailSender) {
        this.espaceDocumentaireService = espaceDocumentaireService;
        this.suiviAnnonceService = suiviAnnonceService;
        this.templateService = templateService;
        this.suiviAnnonceRepository = suiviAnnonceRepository;
        this.pieceJointeService = pieceJointeService;
        this.mailSender = mailSender;
    }

    public MimeMessageHelper buildMail(List<MailContent> mailContentList) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
        for (MailContent mailContent : mailContentList) {
            if (mailContent.getBody() != null) {
                helper.setText(mailContent.getBody(), true);
            } else {
                helper.addAttachment(mailContent.getAttachmentFilename(), mailContent.getAttachmentFile());
            }
        }
        return helper;
    }


    public MailContent buildAnnonceAttachment(SuiviAnnonce suiviAnnonce, String referenceNumber) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        String filename = suiviAnnonceService.geDocumentName(suiviAnnonce, suiviAnnonce.getOffre().isConvertToPdf());
        suiviAnnonceService.exportToDocument(suiviAnnonce, outputStream, null, suiviAnnonce.getOffre().isConvertToPdf());
        String extension = pieceJointeService.getExtension(filename);
        ByteArrayResource byteArrayResource = new ByteArrayResource(outputStream.toByteArray());
        return new MailContent(byteArrayResource, "Annonce_" + referenceNumber + "." + extension);
    }

    public MailContent buildXmlAttachment(String xml, String referenceNumber) {
        ByteArrayResource byteArrayResource = new ByteArrayResource(xml.getBytes());
        return new MailContent(byteArrayResource, "Annonce_" + referenceNumber + ".xml");
    }

    private MailContent buildXmlTransactionAttachment(String xml, String referenceNumber) {
        ByteArrayResource byteArrayResource = new ByteArrayResource(xml.getBytes());
        return new MailContent(byteArrayResource, "Transaction_" + referenceNumber + ".xml");
    }

    @Override
    public MimeMailMessage sendMail(Message<?> message) throws Exception {
        Map<String, Object> headers = message.getHeaders();
        int idAnnonce = (Integer) headers.get("idAnnonce");

        SuiviAnnonce suiviAnnonce = suiviAnnonceRepository.findById(idAnnonce).orElseThrow(() -> new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, "Aucun suivi d'annonce trouvé pour l'id " + idAnnonce));
        List<MailContent> mailContentList = new ArrayList<>();
        SuiviTypeAvisPub typeAvisPub = suiviAnnonce.getTypeAvisPub();
        String referenceNumber = extractValueFromXmlString(suiviAnnonceService.toXml(suiviAnnonce), "/TED_ESENDERS/FORM_SECTION/F05_2014/OBJECT_CONTRACT/REFERENCE_NUMBER");
        mailContentList.add(this.buildAnnonceAttachment(suiviAnnonce, referenceNumber));

        if (typeAvisPub != null) {
            List<SuiviTypeAvisPubPieceJointeAssoEntity> mailPiecejointeList = typeAvisPub.getMailPiecejointeList();
            if (!CollectionUtils.isEmpty(mailPiecejointeList)) {

                for (SuiviTypeAvisPubPieceJointeAssoEntity mailPieceJointe : mailPiecejointeList) {
                    if (espaceDocumentaireService.fileExists(mailPieceJointe.getPieceJointe())) {
                        mailContentList.add(this.buildAttachment(mailPieceJointe));
                    } else {
                        log.warn("Piece jointe {} non trouvée", mailPieceJointe.getPieceJointe().getPath());
                    }
                }
            }
            String mailContenu = typeAvisPub.getMailContenu();
            if (StringUtils.isBlank(mailContenu)) {
                Offre offre = typeAvisPub.getOffreRacine();
                Offre offreAssociee = offre.getOffreAssociee();
                Offre offreNational = typeAvisPub.isEuropeen() ? offreAssociee : offre;
                mailContenu = "Mesdames, Messieurs,\n" +
                        "Veuillez trouver ci-joint un " + offreNational.getLibelle() + " à faire publier dans votre prochaine édition.";

            }
            mailContenu = mailContenu.replaceAll("\n", "<br/>");
            Map<String, Object> parameters = Map.of("typeAvis", typeAvisPub, "mailContenu", mailContenu, "libelleOffre", suiviAnnonce.getOffre().getLibelle(), "libelleSupport", suiviAnnonce.getSupport().getLibelle());
            String contenu = templateService.merge("mailFacturation.ftl", parameters);
            mailContentList.add(new MailContent(contenu));
        } else {
            Map<String, Object> parameters = Map.of("libelleOffre", suiviAnnonce.getOffre().getLibelle(), "libelleSupport", suiviAnnonce.getSupport().getLibelle());
            String contenu = transformXmlWithXslt(suiviAnnonceService.toXml(suiviAnnonce), parameters, mailBodyXslResource);
            mailContentList.add(new MailContent(contenu));
            mailContentList.add(this.buildXmlAttachment(suiviAnnonceService.toXml(suiviAnnonce), referenceNumber));
            mailContentList.add(this.buildXmlTransactionAttachment(suiviAnnonceService.toXmlTransaction(suiviAnnonce), referenceNumber));
        }
        return new MimeMailMessage(this.buildMail(mailContentList));


    }

    public static String transformXmlWithXslt(String xmlInput, Map<String, Object> parameters, Resource xsltFilePath) throws Exception {
        // Create the source for XML and XSLT
        StreamSource xmlSource = new StreamSource(new StringReader(xmlInput));
        StreamSource xsltSource = new StreamSource(xsltFilePath.getInputStream());

        // Create a StringWriter to store the transformation result
        StringWriter resultWriter = new StringWriter();
        StreamResult resultStream = new StreamResult(resultWriter);

        // Create the XSLT transformer
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer(xsltSource);
        parameters.forEach(transformer::setParameter);
        // Perform the transformation
        transformer.transform(xmlSource, resultStream);

        // Get the transformed result as a string
        return resultWriter.toString();
    }

    private MailContent buildAttachment(SuiviTypeAvisPubPieceJointeAssoEntity mailPieceJointe) throws MessagingException {
        PieceJointe pieceJointe = mailPieceJointe.getPieceJointe();
        Resource byteArrayResource = pieceJointeService.loadFileAsResource(pieceJointe.getPath());
        return new MailContent(resourceToByteArrayResource(byteArrayResource), pieceJointe.getName());

    }

    public static ByteArrayResource resourceToByteArrayResource(Resource resource) throws MessagingException {
        try (InputStream inputStream = resource.getInputStream()) {
            byte[] byteArray = StreamUtils.copyToByteArray(inputStream);
            return new ByteArrayResource(byteArray);
        } catch (IOException e) {
            throw new MessagingException("Error while converting resource to byte array resource", e);
        }
    }

    @Override
    public String getSubject(SuiviAnnonce suiviAnnonce, @Header String libelleSupport) throws Exception {
        Support support = suiviAnnonce.getSupport();
        if (!"MAIL".equals(support.getCodeDestination())) {
            return null;
        }
        SuiviTypeAvisPub typeAvisPub = suiviAnnonce.getTypeAvisPub();
        if (typeAvisPub != null) {
            return (StringUtils.isBlank(typeAvisPub.getMailObjet()) ? "Avis d’adjudication " + typeAvisPub.getReference() + " à publier dans [organe de presse]" : typeAvisPub.getMailObjet()).replace("[organe de presse]", libelleSupport)
                    .replace("[organes_de_presse_selectionnees]", libelleSupport);
        }
        String organisation = extractValueFromXmlString(suiviAnnonceService.toXml(suiviAnnonce), "/TED_ESENDERS/SENDER/CONTACT/ORGANISATION");
        String referenceNumber = extractValueFromXmlString(suiviAnnonceService.toXml(suiviAnnonce), "/TED_ESENDERS/FORM_SECTION/F05_2014/OBJECT_CONTRACT/REFERENCE_NUMBER");
        return "Annonce - " + organisation + " - " + libelleSupport + " - " + referenceNumber;
    }

    public static String extractValueFromXmlString(String xmlString, String expression) throws Exception {
        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();

        // Parse the input source into a Document
        Document document = convertXmlStringToDocument(xmlString);

        // Evaluate the XPath expression
        return (String) xpath.evaluate(expression, document, XPathConstants.STRING);
    }

    public static Document convertXmlStringToDocument(String xmlString) throws MessagingException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
            InputSource inputSource = new InputSource(new StringReader(xmlString));
            return builder.parse(inputSource);
        } catch (ParserConfigurationException | IOException | SAXException e) {
            throw new MessagingException("Error while converting xml string to document", e);
        }
    }
}
