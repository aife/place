//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2022.06.02 à 03:26:18 PM CEST 
//


package fr.atexo.annonces.dto.mol;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.util.Date;


/**
 * <p>Classe Java pour anonymous complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="uid_pf" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="date_debut" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="date_fin" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "request", propOrder = {"uidPf", "dateDebut", "dateFin"})
@XmlRootElement(name = "request")
@ToString
@Getter
@Setter
@EqualsAndHashCode
public class Request {

    @XmlElement(name = "uid_pf", required = true)
    @JsonProperty("uid_pf")
    protected String uidPf;
    @XmlElement(name = "date_debut", required = true)
    @JsonProperty("date_debut")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    protected Date dateDebut;
    @JsonProperty("date_fin")
    @XmlElement(name = "date_fin", required = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    protected Date dateFin;

}
