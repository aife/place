package fr.atexo.annonces.dto;

import lombok.*;

import java.io.Serializable;

/**
 * DTO - Représente une demande de publication
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(of = {"nom", "email"})
@ToString(of = {"nom", "email"})
public class JalDTO implements Serializable {

    private String nom;
    private String email;
}
