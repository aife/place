package fr.atexo.annonces.service.exception;


public class PieceJointeException extends AtexoException {

    public PieceJointeException(PieceJointeExceptionEnum projetExceptionEnum, String errorMessage) {
        this.message = errorMessage;
        this.code = projetExceptionEnum.getCode();
        this.type = projetExceptionEnum.getType();
    }

    public PieceJointeException(PieceJointeExceptionEnum projetExceptionEnum, String errorMessage, Throwable err) {
        super(err);
        this.message = errorMessage;
        this.code = projetExceptionEnum.getCode();
        this.type = projetExceptionEnum.getType();
    }
}
