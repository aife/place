package fr.atexo.annonces.boot.core.domain.xlsx.services.impl;


import fr.atexo.annonces.boot.core.domain.xlsx.model.XlsxColumn;
import fr.atexo.annonces.boot.core.domain.xlsx.model.XlsxHeader;
import fr.atexo.annonces.boot.core.domain.xlsx.services.IXlsxService;
import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Slf4j
@Service
public class XlsxServiceImpl implements IXlsxService {
    private static final String ARIAL = "Arial";
    private static final String ERROR_MESSAGE = "Le xlsx ne doit pas être null";


    @Override
    public XSSFSheet createXLSX(XSSFWorkbook workbook, XSSFSheet sheet) {
        if (workbook == null) {
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, "XLSX non initié");
        }
        setPrint(sheet);
        XSSFCellStyle cellStyle = this.createStyleForCells(workbook);
        int rownum = 6;
        Cell cell;
        Row row;
        ZonedDateTime date = ZonedDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM", Locale.FRENCH);
        String monthName = date.format(formatter);

        row = sheet.getRow(rownum);
        cell = row.createCell(2, CellType.STRING);
        cell.setCellStyle(cellStyle);
        cell.setCellValue(date.getDayOfMonth() + " " +
                monthName.toLowerCase() + " " + date.getYear());
        return sheet;
    }

    private static void setPrint(XSSFSheet sheet) {
        XSSFPrintSetup printSetup = sheet.getPrintSetup();
        printSetup.setFitHeight((short) 1);
        printSetup.setFitWidth((short) 1);
        printSetup.setOrientation(PrintOrientation.LANDSCAPE);
        sheet.setFitToPage(true);
    }

    @Override
    public XSSFSheet createXLSX(XSSFWorkbook workbook, String sheetname, List<XlsxHeader> headers) {
        if (workbook == null) {
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, "XLSX non initié");
        }
        XSSFSheet sheet = workbook.createSheet(sheetname);
        setPrint(sheet);
        //ligne des titres colonnes
        int rownum = 0;
        Cell cell;
        Row row;
        XSSFCellStyle titleStyle = this.createStyleForTitle(workbook);
        row = sheet.createRow(rownum);
        for (XlsxHeader value : headers) {
            cell = row.createCell(value.getIndex(), CellType.STRING);
            cell.setCellValue(value.getTitle());
            cell.setCellStyle(titleStyle);
        }
        return sheet;
    }

    @Override
    public void setData(XSSFSheet sheet, int rownum, int column, String value, CellType type) {
        if (sheet == null) {
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, "Onglet non initié");
        }
        Row row = sheet.getRow(rownum);
        if (row == null) {
            row = sheet.createRow(rownum);
        }
        XSSFCellStyle cellStyle = this.createStyleForCells(sheet.getWorkbook());
        addCell(row, column, value, type, cellStyle);


    }

    @Override
    public void setData(XSSFSheet sheet, List<List<XlsxColumn>> rowList, int start) {
        if (sheet == null) {
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, "Onglet non initié");
        }
        int rownum = start;
        XSSFCellStyle cellStyle = this.createStyleForCells(sheet.getWorkbook());
        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        for (List<XlsxColumn> columnList : rowList) {
            Row row = sheet.getRow(rownum);
            if (row == null) {
                row = sheet.createRow(rownum);
            }
            for (XlsxColumn column : columnList) {
                addCell(row, column.getIndex(), column.getValue(), column.getType(), cellStyle);

            }
            rownum++;
        }

    }

    @Override
    public void autoSize(XSSFSheet sheet, int end) {
        for (int i = 0; i < end; i++) {
            sheet.autoSizeColumn(i);
        }
    }

    private XSSFCellStyle createStyleForTitle(XSSFWorkbook workbook) {
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeightInPoints((short) 9);
        font.setFontName(ARIAL);
        font.setColor(IndexedColors.BLUE.getIndex());
        XSSFCellStyle style = workbook.createCellStyle();
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.CENTER);
        return style;
    }


    private XSSFCellStyle createStyleForCells(XSSFWorkbook workbook) {
        XSSFFont font = workbook.createFont();
        font.setFontHeightInPoints((short) 9);
        font.setFontName(ARIAL);
        XSSFCellStyle style = workbook.createCellStyle();
        style.setFont(font);

        return style;
    }


    private void addCell(Row row, int i, String value, CellType type, XSSFCellStyle cellStyle) {
        Cell cell = row.getCell(i);
        if (cell == null) {
            cell = row.createCell(i, type);
        }
        cell.setCellValue(value);
        cell.setCellStyle(cellStyle);
    }

    @Override
    public XSSFWorkbook openFile(InputStream inputStream) {
        if (inputStream == null) {
            throw new AtexoException(ExceptionEnum.MANDATORY, ERROR_MESSAGE);
        }
        try {
            return new XSSFWorkbook(inputStream);
        } catch (Exception e) {
            throw new AtexoException(ExceptionEnum.FUNCTIONAL_ERROR, e.getMessage(), e);
        }
    }

    @Override
    public List<List<String>> extractValues(XSSFWorkbook xlsx, int sheetIndex, int rownumStart) {
        XSSFSheet sheet = xlsx.getSheetAt(sheetIndex);
        List<List<String>> rows = new ArrayList<>();
        FormulaEvaluator evaluator = xlsx.getCreationHelper().createFormulaEvaluator();

        for (int rownum = rownumStart; rownum <= sheet.getLastRowNum(); rownum++) {
            List<String> values = new ArrayList<>();

            Row row = sheet.getRow(rownum);
            if (row == null)
                continue;
            short lastCellNum = row.getLastCellNum();
            if (lastCellNum < 0)
                continue;
            for (int i = row.getFirstCellNum(); i <= lastCellNum; i++) {
                Cell cell = row.getCell(i);
                CellValue cellValue = evaluator.evaluate(cell);
                if (cellValue == null)
                    values.add("");
                else
                    values.add(cell.toString());
            }
            rows.add(values);
        }

        return rows;
    }


}

