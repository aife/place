package fr.atexo.annonces.boot.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;



@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter
@Getter
@Entity
@EqualsAndHashCode
@Table(name = "ACHETEUR")
public class AcheteurEntity implements Serializable {
    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "MAIL")
    private String email;

    @Column(name = "LOGIN")
    private String login;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "TOKEN")
    private String token;


    @Column(name = "PROV")
    private Integer moniteurProvenance;

    @Column(name = "ACTIF")
    private boolean actif;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_organisme")
    private Organisme organisme;


}
