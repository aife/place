package fr.atexo.annonces.boot.core.domain.e_forms_sdk.model;


import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FieldFormulairesDetails {
    private String id;
    private String name;
    private String btId;
    private String xPath;
    private String type;
    private String legalType;
    private boolean repeatable;
    private CodeList codeList;
    private List<Constraint> constraints;
}
