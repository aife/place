package fr.atexo.annonces.service.exception;

import lombok.Getter;
import org.slf4j.helpers.MessageFormatter;

import java.util.UUID;

@Getter
public class SuiviIntrouvableException extends RuntimeException {

	private final String uuid;

	public SuiviIntrouvableException(String uuid) {
		super(MessageFormatter.format("Suivi {} introuvable", uuid).getMessage());

		this.uuid = uuid;
	}
}
