package fr.atexo.annonces.mapper.tncp;

import eu.europa.publications.resource.schema.ted.r2_0_9.reception.TedEsenders;
import fr.atexo.annonces.commun.tncp.TncpTypeProcedureEnum;
import fr.atexo.annonces.config.EFormsHelper;
import fr.atexo.annonces.mapper.Mappable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
public class TypeProcedureMapper implements Mappable<String, Optional<TncpTypeProcedureEnum>> {

    @Value("classpath:mapping/mapping-mpe-tncp.yml")
    Resource mappingMpeTncp;


    @Override
    public Optional<TncpTypeProcedureEnum> map(String idExterne, TedEsenders tedEsenders, boolean rie) {
        if (idExterne == null)
            return Optional.empty();
        final Map<String, String> mpeTncpMappingValues = EFormsHelper.getMapping(mappingMpeTncp).get("type-procedure");
        String correspondance = mpeTncpMappingValues.get(idExterne);
        return Optional.ofNullable(TncpTypeProcedureEnum.fromValue(correspondance));

    }

}
