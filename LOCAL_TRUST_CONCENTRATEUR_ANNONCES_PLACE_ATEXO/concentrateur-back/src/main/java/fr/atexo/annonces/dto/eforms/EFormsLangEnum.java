package fr.atexo.annonces.dto.eforms;

public enum EFormsLangEnum {
    FRA, ENG, ARG, AFG, LUX, DEU, BEL
}
