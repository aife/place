package fr.atexo.annonces.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.MultiValueMap;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.TransformerException;

/**
 * Gestion des erreurs de transformation XSL. Chaque erreur détectée est enregistrée dans le collecteur fourni dans le
 * constructeur pour pouvoir y accéder une fois le traitement fini.
 */
public class XslTransformationErrorListener implements ErrorListener {

    private final Logger logger = LoggerFactory.getLogger(XslTransformationErrorListener.class);
    private final MultiValueMap<XslTransformationErrorSeverity, String> collector;

    public XslTransformationErrorListener(MultiValueMap<XslTransformationErrorSeverity, String> collector) {
        this.collector = collector;
    }

    @Override
    public void warning(TransformerException e) {
        logger.debug("Transformation XSL - WARN", e);
        collector.add(XslTransformationErrorSeverity.WARN, e.getMessageAndLocation());
    }

    @Override
    public void error(TransformerException e) {
        logger.debug("Transformation XSL - ERROR", e);
        collector.add(XslTransformationErrorSeverity.ERROR, e.getMessageAndLocation());
    }

    @Override
    public void fatalError(TransformerException e) {
        logger.debug("Transformation XSL - FATAL", e);
        collector.add(XslTransformationErrorSeverity.FATAL, e.getMessageAndLocation());
    }
}
