package fr.atexo.annonces.mapper;

import fr.atexo.annonces.boot.entity.Offre;
import fr.atexo.annonces.dto.OffreDTO;
import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Named;

import java.util.List;
import java.util.Set;

/**
 * Mapper entre l'entité Offre et son DTO
 */
@Mapper(collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED, uses = SupportMapper.class)
public interface OffreMapper {

    @Named("ignoreSupport")
    OffreDTO offreToOffreDTOIgnoreSupport(Offre offre);

    OffreDTO offreToOffreDTO(Offre offre);

    @IterableMapping(qualifiedByName = "ignoreSupport")
    @Named("listIgnoreSupport")
    Set<OffreDTO> offresToOffreDTOsIgnoreSupport(Set<Offre> offres);

    Set<OffreDTO> offresToOffreDTOs(Set<Offre> offres);
}
