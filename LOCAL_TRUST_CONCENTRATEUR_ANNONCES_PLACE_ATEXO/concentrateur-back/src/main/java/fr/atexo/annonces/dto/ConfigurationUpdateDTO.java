package fr.atexo.annonces.dto;

import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * DTO - Représente une demande de publication
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ConfigurationUpdateDTO implements Serializable {

    private String xml;
    private String idPlatform;
    private Integer idConsultation;
    private Boolean europeen;
    private String dmls;
    private List<String> blocList;
    private List<JalDTO> jalList;
    private List<ConsultationFacturationDTO> facturationList;
}
