package fr.atexo.annonces.service.exception;

/**
 * Exception levée si l'état de la ressource existante n'est pas compatible avec l'opération demandée
 */
public class ConflictingResourceException extends RuntimeException {

    private static final long serialVersionUID = -6416139697911121571L;

    public ConflictingResourceException(String message) {
        super(message);
    }
}
