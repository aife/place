package fr.atexo.annonces.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.*;
import org.apache.commons.codec.binary.Base64;

import java.io.IOException;
import java.time.Instant;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Token {
    @JsonProperty("access_token")
    private String accessToken;

    @JsonProperty("refresh_token")
    private String refreshToken;

    public Instant getExpiresTime() {
        String[] splitString = this.accessToken.split("\\.");
        return getInstant(splitString);
    }

    public Instant getRefreshExpiresTime() {
        String[] splitString = this.refreshToken.split("\\.");
        return getInstant(splitString);
    }

    public Instant getInstant(String... splitString) {
        if (splitString == null || splitString.length < 2)
            return Instant.now();
        try {
            String base64EncodedBody = splitString[1];
            Base64 base64Url = new Base64(true);
            String body = new String(base64Url.decode(base64EncodedBody));
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            objectMapper.registerModule(new JavaTimeModule());

            return objectMapper.readValue(body, TokenBody.class).getExp();
        } catch (IOException e) {
            return Instant.now();
        }
    }
}
