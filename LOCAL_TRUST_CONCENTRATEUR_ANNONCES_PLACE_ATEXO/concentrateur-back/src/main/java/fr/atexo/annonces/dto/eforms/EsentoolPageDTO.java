package fr.atexo.annonces.dto.eforms;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EsentoolPageDTO {
    private int totalSize;
    private List<EsentoolNoticeDTO> content;
}
