package fr.atexo.annonces.boot.ws.jal;

import fr.atexo.annonces.dto.lesecho.suivi.Suivi;
import fr.atexo.annonces.dto.publication.AVISEMIS;
import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.util.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.net.URI;

@Component
@Slf4j
public class LesechosWS {

    private final RestTemplate atexoRestTemplate;
    private final URI suivi;

    private final String username;
    private final String password;

    public LesechosWS(@Value("${annonces.lesechos.http.url.suivi}") URI suivi, RestTemplate atexoRestTemplate, @Value("${annonces.lesechos.http.username}") String username, @Value("${annonces.lesechos.http.password}") String password) {
        this.suivi = suivi;

        this.atexoRestTemplate = atexoRestTemplate;
        this.username = username;
        this.password = password;
    }

    public Suivi getSuivi() {
        var headers = new HttpHeaders();
        headers.set("User-Agent", "Atexo");
        headers.set("Cache-Control", "no-cache");
        headers.set("Pragma", "no-cache");
        headers.setBasicAuth(username, password);
        String body = atexoRestTemplate.exchange(suivi, HttpMethod.GET, new HttpEntity<>(headers), String.class).getBody();
        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(body.getBytes())) {
            JAXBContext jaxbContextTed = JAXBContext.newInstance(Suivi.class);
            Unmarshaller jaxbUnmarshaller = jaxbContextTed.createUnmarshaller();

            return (Suivi) jaxbUnmarshaller.unmarshal(inputStream);
        } catch (Exception e) {
            log.error("Erreur lors de la récupération du suivi", e);
            throw new AtexoException(ExceptionEnum.TECHNICAL_ERROR, e.getMessage(), e);
        }
    }


}
