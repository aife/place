package fr.atexo.annonces.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AnnonceStatistique {

    private TypeAvisPubDTO offre;
    private List<StatutStatistique> statuts;
}
