package fr.atexo.annonces.service.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ExceptionEnum {
    NOT_FOUND(404, "Non existant"), SAVE_ERROR(500, "Erreur lors de l'enregistrement"),
    DELETE_ERROR(500, "Erreur lors de la suppression"),
    NOT_AUTHORIZED(401, "Non autorisé"),
    FORBIDDEN(403, "Accès refusé"),
    MANDATORY(400, "Champs obligatoires"),
    NO_RESULT(500, "Aucun résultat"),
    TECHNICAL_ERROR(500, "Erreur technique"),
    FUNCTIONAL_ERROR(500, "Erreur fonctionnel"),
    EFORMS_WS(500, "Erreur WS E-Forms"),
    TED_WS(500, "Erreur WS E-SendTool");

    private final int code;
    private final String type;

}
