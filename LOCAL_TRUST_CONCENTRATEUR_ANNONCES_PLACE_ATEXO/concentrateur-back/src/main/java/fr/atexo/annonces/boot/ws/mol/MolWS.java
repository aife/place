package fr.atexo.annonces.boot.ws.mol;

import fr.atexo.annonces.config.RestTemplateConfig;
import fr.atexo.annonces.dto.publication.AVISEMIS;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.util.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class MolWS {
    private final RestTemplate atexoRestTemplate;


    private final String molBaseUrl;

    private final String suiviWs;
    private final String openWs;
    private final String login;
    private final String password;

    public MolWS(RestTemplate atexoRestTemplate, @Value("${external-apis.mol.ws.suivi}") String suiviWs, @Value("${external-apis.mol.base-path}") String molBaseUrl, @Value("${external-apis.mol.login}") String login, @Value("${external-apis.mol.password}") String password, @Value("${external-apis.mol.ws.open}") String openWs) {
        this.atexoRestTemplate = atexoRestTemplate;
        this.molBaseUrl = molBaseUrl;
        this.suiviWs = suiviWs;
        this.login = login;
        this.password = password;
        this.openWs = openWs;
    }

    public AVISEMIS getSuiviMol(Date date, Date datefin) {
        DateFormat formatter = new SimpleDateFormat("ddMMyyyy");
        Map<String, String> urlParameters = new HashMap<>();
        String debutString = formatter.format(date);
        urlParameters.put("date", debutString);
        String finString = formatter.format(datefin);
        urlParameters.put("datefin", finString);
        String auth = login + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64Chunked(auth.getBytes(StandardCharsets.US_ASCII));
        String authHeader = "Basic " + new String(encodedAuth);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", authHeader);

        log.info("Appel du WS MOL : {} entre {} et {}", molBaseUrl + suiviWs, debutString, finString);
        ResponseEntity<String> exchange = atexoRestTemplate.exchange(molBaseUrl + suiviWs, HttpMethod.GET, new HttpEntity<>(null, headers), String.class, urlParameters);
        String body = exchange.getBody();
        if (exchange.getStatusCode().is2xxSuccessful()) {
            try (ByteArrayInputStream inputStream = new ByteArrayInputStream(body.getBytes())) {
                JAXBContext jaxbContextTed = JAXBContext.newInstance(AVISEMIS.class);
                Unmarshaller jaxbUnmarshaller = jaxbContextTed.createUnmarshaller();

                return (AVISEMIS) jaxbUnmarshaller.unmarshal(inputStream);

            } catch (Exception e) {
                log.error(e.getMessage());
                return null;
            }
        }
        log.error("Erreur {}", exchange.getStatusCodeValue());
        return null;
    }

    public ResponseEntity<String> openMOL(String xmlToken, String xmlSignature, String xmlBoamp) {
        log.info("Appel du WS MOL : {} ", molBaseUrl + openWs);
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl("no-cache, no-store, max-age=0, must-revalidate");
        headers.setPragma("no-cache");
        MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("xmltoken", xmlToken);
        bodyMap.add("xmlsignature", xmlSignature);
        bodyMap.add("xmlboamp", xmlBoamp);
        return new RestTemplateConfig().restTemplate().exchange(molBaseUrl + openWs, HttpMethod.POST, new HttpEntity<>(bodyMap, headers), String.class);
    }
}
