package fr.atexo.annonces.config;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

public class XMLResourceBundleControl extends ResourceBundle.Control {
    private static String XML = "xml";
    private final String resourcesDir;

    public XMLResourceBundleControl(String resourcesDir) {
        this.resourcesDir = resourcesDir;
    }

    public List<String> getFormats(String baseName) {
        return Collections.singletonList(XML);
    }

    public ResourceBundle newBundle(String baseName, Locale locale, String format,
                                    ClassLoader loader, boolean reload) throws
            IOException {

        if ((baseName == null) || (locale == null) || (format == null) || (loader == null)) {
            throw new NullPointerException();
        }
        ResourceBundle bundle = null;
        if (!format.equals(XML)) {
            return null;
        }

        String bundleName = toBundleName(baseName, locale);
        String resourceName = toResourceName(bundleName, format);
        File file = new File(resourcesDir + "/translations/" + resourceName);

        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
        bundle = new XMLResourceBundle(bis);
        bis.close();

        return bundle;
    }
}

