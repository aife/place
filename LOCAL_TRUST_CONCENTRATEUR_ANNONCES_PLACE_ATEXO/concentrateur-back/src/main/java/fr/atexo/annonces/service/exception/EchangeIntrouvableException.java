package fr.atexo.annonces.service.exception;

import lombok.Getter;
import org.slf4j.helpers.MessageFormatter;

@Getter
public class EchangeIntrouvableException extends RuntimeException {

	private final String json;

	public EchangeIntrouvableException(String json) {
		super(MessageFormatter.format("Echange {} incorrect", json).getMessage());

		this.json = json;
	}
}
