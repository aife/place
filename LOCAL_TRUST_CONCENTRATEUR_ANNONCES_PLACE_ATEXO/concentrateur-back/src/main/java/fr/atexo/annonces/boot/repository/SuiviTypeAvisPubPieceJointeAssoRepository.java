package fr.atexo.annonces.boot.repository;

import fr.atexo.annonces.boot.entity.SuiviTypeAvisPubPieceJointeAssoEntity;
import fr.atexo.annonces.config.DaoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SuiviTypeAvisPubPieceJointeAssoRepository extends DaoRepository<SuiviTypeAvisPubPieceJointeAssoEntity, Long> {
    List<SuiviTypeAvisPubPieceJointeAssoEntity> findAllBySuiviTypeAvisPubId(Long id);
}
