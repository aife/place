package fr.atexo.annonces.reseau;

import fr.atexo.annonces.boot.core.domain.cron.ConcentrateurCron;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.model.EFormsFormulaire;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.model.EFormsFormulaireSearch;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.model.ESentoolSearch;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.model.ValidationResult;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.IEFormsNoticeServices;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNotice;
import fr.atexo.annonces.dto.LangPatch;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

@RestController
@MultipartConfig
@RequestMapping("/rest/v2/eforms-formulaires")
@Slf4j
public class EFormsNoticesResource {

    private final IEFormsNoticeServices iNoticeServices;
    private final ConcentrateurCron concentrateurCron;

    public EFormsNoticesResource(IEFormsNoticeServices iNoticeServices, ConcentrateurCron concentrateurCron) {
        this.iNoticeServices = iNoticeServices;
        this.concentrateurCron = concentrateurCron;
    }

    @GetMapping
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform)")
    public List<EFormsFormulaire> getFormulaires(EFormsFormulaireSearch formulaire, @RequestParam String idPlatform, @RequestHeader String organisme) {
        return iNoticeServices.getFormulaires(formulaire, idPlatform, organisme);
    }

    @PutMapping("{id}")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) ")
    public EFormsFormulaire save(@RequestParam String idPlatform,
                                 @PathVariable Long id,
                                 @RequestBody Object formulaire) {
        return iNoticeServices.save(id, formulaire, idPlatform);
    }


    @GetMapping("/{id}")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform)")
    public EFormsFormulaire getSubNotice(@PathVariable Long id, @RequestParam String idPlatform) {
        return iNoticeServices.getFormulaireEForms(id, idPlatform);
    }

    @GetMapping("{id}/structure")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform)")
    public SubNotice getSubNoticeStructure(@PathVariable Long id, @RequestParam String idPlatform) {
        return iNoticeServices.getSubNoticeStructure(id, idPlatform);
    }


    @PatchMapping("{id}/lang")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) ")
    public EFormsFormulaire changeOfficialLang(@RequestParam String idPlatform,
                                               @PathVariable Long id,
                                               @RequestBody LangPatch request) {
        return iNoticeServices.changeOfficialLang(id, request, idPlatform);
    }

    @DeleteMapping("{id}/stop-publication")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform)")
    public EFormsFormulaire stopPublication(@RequestParam String idPlatform,
                                            @PathVariable Long id) {
        return iNoticeServices.stopPublication(id, idPlatform);
    }

    @GetMapping("{id}/rapport-validation")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform)")
    public ValidationResult getValidationReport(@RequestParam String idPlatform,
                                                @PathVariable Long id) {
        return iNoticeServices.getValidationReport(id, idPlatform);
    }

    @PatchMapping("{id}/change")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) ")
    public EFormsFormulaire change(@RequestParam String idPlatform,
                                   @RequestHeader(name = "Authorization") String token,
                                   @PathVariable Long id) {
        return iNoticeServices.change(id, idPlatform, token.replace("Bearer ", ""));
    }

    @PatchMapping("{id}/reprendre")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) ")
    public EFormsFormulaire reprendre(@RequestParam String idPlatform,
                                      @RequestHeader(name = "Authorization") String token,
                                      @PathVariable Long id) {
        return iNoticeServices.reprendre(id, idPlatform, token.replace("Bearer ", ""));
    }

    @PatchMapping("{id}/verify")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) ")
    public ValidationResult validate(@PathVariable Long id,
                                     @RequestParam String idPlatform,
                                     @RequestParam(required = false) String mode,
                                     @RequestBody Object formulaire) {
        return iNoticeServices.verify(id, formulaire, idPlatform, mode != null ? mode : "static");
    }

    @PatchMapping("{id}/validate")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) ")
    public EFormsFormulaire verify(@PathVariable Long id,
                                   @RequestParam String idPlatform) {
        return iNoticeServices.validate(id, idPlatform, false);
    }

    @GetMapping("synchronise")
    public Boolean synchronise() {
        concentrateurCron.suiviTedNoticeSubmitting();
        concentrateurCron.suiviTedNoticeSubmitted();
        return true;
    }

    @PatchMapping("synchronise/all")
    public Boolean synchroniseAll(@Valid @NotNull(message = "Les critères de recherche sont obligatoires") ESentoolSearch search) {
        concentrateurCron.synchroniseAll(search);
        return true;
    }
    @PatchMapping(value = "synchronise/file", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Boolean synchroniseAll(@RequestPart MultipartFile template) {
        concentrateurCron.synchroniseFile(template);
        return true;
    }

    @GetMapping("{id}/pdf")
    @PreAuthorize("#oauth2.hasScope('platform:' + #idPlatform) ")
    public void getAnnoncePdf(
            @PathVariable Long id,
            @RequestParam String idPlatform,
            HttpServletResponse response) throws IOException {
        response.setHeader("Content-Disposition", "attachment; filename=export.pdf");
        response.setContentType(MediaType.APPLICATION_PDF_VALUE);
        try {
            iNoticeServices.exportToPdf(id, response.getOutputStream(), idPlatform);
        } catch (Exception e) {
            resetServletResponse(response, e);
            throw e;
        }
    }

    private void resetServletResponse(HttpServletResponse response, Exception e) {
        try {
            response.reset();
        } catch (IllegalStateException f) {
            log.error("Erreur survenue lors de la génération du PDF ; impossible de transmettre cette erreur" +
                    " au client car des données du PDF ont déjà été envoyées", e);
            throw f;
        }
    }

}
