package fr.atexo.annonces.boot.core.domain.e_forms_sdk.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class Code {
    private String id;
    private String parentId;
    private String filename;
    private String description;
    private String _label;

}
