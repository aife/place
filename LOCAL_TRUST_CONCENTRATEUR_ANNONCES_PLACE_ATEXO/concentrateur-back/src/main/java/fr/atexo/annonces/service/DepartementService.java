package fr.atexo.annonces.service;

import fr.atexo.annonces.dto.DepartementDTO;

import java.util.List;

/**
 * Couche service pour manipuler les départements
 */
public interface DepartementService {
    List<DepartementDTO> getDepartements();
}
