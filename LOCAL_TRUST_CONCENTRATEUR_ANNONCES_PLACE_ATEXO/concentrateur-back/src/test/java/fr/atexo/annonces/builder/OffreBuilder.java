package fr.atexo.annonces.builder;

import fr.atexo.annonces.boot.entity.Offre;
import fr.atexo.annonces.boot.entity.Support;

import java.util.List;

/**
 * Builder pour Offre
 */
public final class OffreBuilder {
    private Integer id;
    private String libelle;
    private String code;
    private Double montantMin;
    private Double montantMax;
    private List<Support> supports;

    private OffreBuilder() {
    }

    public static OffreBuilder anOffre() {
        return new OffreBuilder();
    }

    public OffreBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public OffreBuilder withLibelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public OffreBuilder withCode(String code) {
        this.code = code;
        return this;
    }

    public OffreBuilder withMontantMin(Double montantMin) {
        this.montantMin = montantMin;
        return this;
    }

    public OffreBuilder withMontantMax(Double montantMax) {
        this.montantMax = montantMax;
        return this;
    }

    public OffreBuilder withSupport(Support support) {
        this.supports = List.of(support);
        return this;
    }

    public Offre build() {
        Offre offre = new Offre();
        offre.setId(id);
        offre.setLibelle(libelle);
        offre.setCode(code);
        offre.setMontantMin(montantMin);
        offre.setMontantMax(montantMax);
        offre.setSupports(supports);
        return offre;
    }

    public OffreBuilder withDefaults() {
        this.id = 1;
        this.libelle = "Offre de test";
        this.code = "OFFRE_TEST";
        this.montantMin = 0D;
        this.montantMax = 90000D;
        this.supports = List.of(SupportBuilder.aSupport().withCode("SUPPORT_TEST").build());
        return this;
    }
}
