package fr.atexo.annonces.service;


import fr.atexo.annonces.config.EFormsHelper;
import fr.atexo.annonces.config.XmlElement;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.Assert.assertNotNull;

public class EformsHelperTest {

    @Test
    void mustReturnOnlyRootWithIndex() {
        //Given
        String xpath = "/root[1]/element1[2]/element2[3]";

        //When
        List<XmlElement> result = EFormsHelper.getXpathElementList(xpath);

        //Then
        assertNotNull(result);
        assertThat(result).hasSize(3);
        assertThat(result.get(0).getElementName()).isEqualTo("root");
        assertThat(result.get(0).getProperties()).containsExactly("1");
        assertThat(result.get(1).getElementName()).isEqualTo("element1");
        assertThat(result.get(1).getProperties()).containsExactly("2");
        assertThat(result.get(2).getElementName()).isEqualTo("element2");
        assertThat(result.get(2).getProperties()).containsExactly("3");
    }

    @Test
    void mustReturnOnlyRootWithProperties() {
        //Given
        String xpath = "/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:SubcontractingTerm[efbc:TermCode/@listName='applicability']/efbc:TermAmount/@currencyID";

        //When
        List<XmlElement> result = EFormsHelper.getXpathElementList(xpath);

        //Then
        assertNotNull(result);
        assertThat(result).hasSize(9);
        assertThat(result.get(0).getElementName()).isEqualTo("ext:UBLExtensions");
        assertThat(result.get(0).getProperties()).isEmpty();
        assertThat(result.get(1).getElementName()).isEqualTo("ext:UBLExtension");
        assertThat(result.get(1).getProperties()).isEmpty();
        assertThat(result.get(2).getElementName()).isEqualTo("ext:ExtensionContent");
        assertThat(result.get(2).getProperties()).isEmpty();
        assertThat(result.get(3).getElementName()).isEqualTo("efext:EformsExtension");
        assertThat(result.get(3).getProperties()).isEmpty();
        assertThat(result.get(4).getElementName()).isEqualTo("efac:NoticeResult");
        assertThat(result.get(4).getProperties()).isEmpty();
        assertThat(result.get(5).getElementName()).isEqualTo("efac:LotTender");
        assertThat(result.get(5).getProperties()).isEmpty();
        assertThat(result.get(6).getElementName()).isEqualTo("efac:SubcontractingTerm");
        assertThat(result.get(6).getProperties()).containsExactly("efbc:TermCode/@listName='applicability'");
        assertThat(result.get(7).getElementName()).isEqualTo("efbc:TermAmount");
        assertThat(result.get(4).getProperties()).isEmpty();
        assertThat(result.get(8).getElementName()).isEqualTo("@currencyID");
        assertThat(result.get(8).getProperties()).isEmpty();
    }

    @Test
    void mustReturnXPathFromTest() {
        String test = "(cac:TenderSubmissionDeadlinePeriod/cbc:EndDate/xs:date(text()) > ../../cbc:IssueDate/xs:date(text())) or not((cac:TenderSubmissionDeadlinePeriod/cbc:EndDate) and (../../cbc:IssueDate))";
        String xpath = EFormsHelper.extractXPathFromTest(test);
        assertThat(xpath).isEqualTo("cac:TenderSubmissionDeadlinePeriod/cbc:EndDate/xs:date");
    }

    @Test
    void mustReturnXPathFromTest2() {
        String test = "((cac:AppealReceiverParty/cac:PartyIdentification/cbc:ID/normalize-space(text()) = /*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Organizations/efac:Organization/efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text())) or (cac:AppealReceiverParty/cac:PartyIdentification/cbc:ID/normalize-space(text()) = /*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Organizations/efac:Organization/efac:TouchPoint/cac:PartyIdentification/cbc:ID/normalize-space(text()))) or not(cac:AppealReceiverParty/cac:PartyIdentification/cbc:ID)";
        String xpath = EFormsHelper.extractXPathFromTest(test);
        assertThat(xpath).isEqualTo("cac:AppealReceiverParty/cac:PartyIdentification/cbc:ID/normalize-space");
    }

    @Test
    void mustReturnXPathFromTest3() {
        String test = "https://ted.europa.eu/udl?uri=TED:NOTICE:501512-2022:TEXT:FR:HTML";
        //copy until TED:NOTICE:
        String previousUuid = test.substring(test.indexOf("TED:NOTICE:") + 11).split(":")[0];
        assertThat(previousUuid).isEqualTo("501512-2022");
    }

}
