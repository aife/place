package fr.atexo.annonces.boot.repository;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

/**
 * TODO : à faire marcher une fois qu'on aura tout passé en spring boot
 */
@DataJpaTest
@Disabled
class SupportRepositoryTest {

    @Autowired
    private SupportRepository supportRepository;

    @Test
    void findAllSupportsFetchOffres_shouldReturnAllSupportsWithOffres() {
        Assertions.assertThat(supportRepository).isNotNull();
    }

    @Test
    void findAllSupportsByOffresMontantFetchOffres_shouldReturnSomeSupportsWithOffres() {
        Assertions.assertThat(supportRepository).isNotNull();
    }
}