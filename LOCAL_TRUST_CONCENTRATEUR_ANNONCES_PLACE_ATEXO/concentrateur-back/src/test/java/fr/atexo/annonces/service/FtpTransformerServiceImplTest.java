package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.entity.Offre;
import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayOutputStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doAnswer;

/**
 * Test du bon export des objets métiers vers un byte[]
 */
@ExtendWith(MockitoExtension.class)
class FtpTransformerServiceImplTest {

    @Mock
    private SuiviAnnonceService suiviAnnonceService;

    @InjectMocks
    private FtpTransformerServiceImpl ftpTransformerService;

    @Test
    void exportPdfToByteArray_shouldReturnSuiviAnnoncePdfExport() {
        SuiviAnnonce annonce = new SuiviAnnonce();
        annonce.setOffre(Offre.builder().convertToPdf(true).build());
        String export = "TEST EXPORT";
        doAnswer(invocation -> {
            ((ByteArrayOutputStream) invocation.getArguments()[1]).writeBytes(export.getBytes());
            ((ByteArrayOutputStream) invocation.getArguments()[1]).close();
            return null;
        }).when(suiviAnnonceService).exportToDocument(any(SuiviAnnonce.class), any(ByteArrayOutputStream.class), anyBoolean(), anyBoolean());

        byte[] exportedBytes = ftpTransformerService.exportPdfToByteArray(annonce);

        assertThat(exportedBytes).isEqualTo(export.getBytes());
    }
}
