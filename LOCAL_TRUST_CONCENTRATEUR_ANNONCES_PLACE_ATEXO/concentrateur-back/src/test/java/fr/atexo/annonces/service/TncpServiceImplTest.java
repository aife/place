package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.boot.entity.tncp.SuiviAvisPublie;
import fr.atexo.annonces.boot.repository.SuiviAnnonceRepository;
import fr.atexo.annonces.modeles.StatutSuiviAnnonceEnum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Tests pour la couche service des départements
 */
@ExtendWith(MockitoExtension.class)
@Disabled
class TncpServiceImplTest {

    @Mock
    private SuiviService suiviService;
    @Mock
    private SuiviAnnonceRepository suiviAnnonceRepository;


    @InjectMocks
    private TNCPServiceImpl tncpService;

    @Test
    void whenUpdateTncpThenReturnListUpdated() throws IOException {
        String json = Files.readString(new File("src/test/resources/data/FluxRetourTNCPC2.json").toPath());
        when(suiviAnnonceRepository.findAllByIdConsultationAndSupportCodeGroupe(any(), any())).thenReturn(List.of(SuiviAnnonce.builder()
                .suiviList(List.of(SuiviAvisPublie.with()
                        .suiviAnnonce(SuiviAnnonce.builder()
                                .idAnnonce("2021-0000000000")
                                .build())
                        .build())).build()));
        when(suiviAnnonceRepository.save(any())).thenAnswer(invocationOnMock -> invocationOnMock.getArgument(0));
        List<SuiviAnnonce> result = tncpService.updateTncp(json);
        Assertions.assertNotNull(result);
        assertThat(result).hasSize(12);
        assertThat(result.stream().filter(annonce -> annonce.getStatut().equals(StatutSuiviAnnonceEnum.PUBLIER))).hasSize(12);
        assertThat(result.stream().filter(annonce -> annonce.getStatut().equals(StatutSuiviAnnonceEnum.REJETER_SUPPORT))).hasSize(0);
    }
}
