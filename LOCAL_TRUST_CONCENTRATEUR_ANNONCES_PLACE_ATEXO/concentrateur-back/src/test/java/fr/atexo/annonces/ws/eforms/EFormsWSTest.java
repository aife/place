package fr.atexo.annonces.ws.eforms;


import fr.atexo.annonces.boot.ws.eforms.EFormsWS;
import fr.atexo.annonces.boot.ws.eforms.EformsConfiguration;
import fr.atexo.annonces.config.RestTemplateConfig;
import fr.atexo.annonces.dto.eforms.EformsSearch;
import fr.atexo.annonces.dto.eforms.EsentoolPageDTO;
import fr.atexo.annonces.dto.eforms.Metadata;
import fr.atexo.annonces.service.exception.AtexoException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Disabled
class EFormsWSTest {
    private EformsConfiguration eformsConfiguration = EformsConfiguration.builder()
            .basePath("https://enotices2.preview.ted.europa.eu/esenders/api")
            .apiKey("f6c56be1ab314ba2bfa7597855d8e526")
            .ws(Map.of("submit", "/v2/notice/notices/submit", "get-notices", "/v2/notice/notices/search-esentool",
                    "get-validation", "/v2/notice/notices/{businessId}/validation-report", "stop-publication", "/v2/notice/notices/{businessId}/stop-publication-esentool"))
            .build();
    private EFormsWS eFormsWS = new EFormsWS(new RestTemplateConfig().restTemplate(), eformsConfiguration);

    @Test
    void Given_Xml_When_SubmitNotice_Then_Ok() {
        File notice = new File("src/test/resources/data/MPE_avisPreInfo.xml");
        assertThatExceptionOfType(AtexoException.class)
                .isThrownBy(() -> eFormsWS.submit(notice, Metadata.builder().noticeAuthorEmail("regis.menet@atexo.com")
                        .noticeAuthorLocale("fr")
                        .build(), null))
                .withMessage("{\"code\":\"notice.NOTICE_IMPORT_VALUE_EXCEPTION\",\"message\":\"A notice with the same id and version id already exists.\",\"showDetails\":true}");

    }

    @Test
    void Given_Page_WhenSearchNotice_Then_ReturnList() {

        EformsSearch search = EformsSearch.builder()
                .publishedAfter(ZonedDateTime.parse("2023-12-07T15:28:10Z"))
                .statuses(List.of("PUBLISHED"))
                .submittedAfter(ZonedDateTime.parse("2023-12-05T15:28:10Z"))
                .build();
        EsentoolPageDTO avisemis = eFormsWS.search(0, 20, search, null);
        assertNotNull(avisemis);
        assertThat(avisemis.getContent()).isNotEmpty();

    }

    @Test
    void getValidationReport() {

        File rapport = eFormsWS.getValidationReport("40a7f2aa-f751-492a-9f1c-f1f049a8c775", "01", null);
        assertTrue(rapport.exists());
        rapport.delete();
    }

}
