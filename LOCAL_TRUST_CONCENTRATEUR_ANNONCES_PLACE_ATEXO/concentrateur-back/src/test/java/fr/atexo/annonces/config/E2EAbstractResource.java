package fr.atexo.annonces.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import fr.atexo.annonces.boot.configuration.AtexoAuditorAware;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.model.EFormsFormulaire;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.model.EFormsFormulaireAdd;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.util.JsonExpectationsHelper;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


@SpringBootTest(classes = {E2EScanConfig.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("e2e")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Disabled
public abstract class E2EAbstractResource {

    @RegisterExtension
    public static final MariaDbExtension MARIA_DB_EXTENSION = new MariaDbExtension();
    protected final static String PATH_SUPPORT = "/concentrateur-annonces/rest/v2/supports";
    protected final static String PATH_MOL = "/concentrateur-annonces/marches-online";
    protected final static String PATH_ANNONCES = "/concentrateur-annonces/rest/v2/annonces";
    protected final static String PATH_EFORMS_NOTICES = "/concentrateur-annonces/rest/v2/eforms-formulaires/";
    protected final static String PATH_EFORMS_SDK = "/concentrateur-annonces/rest/v2/eforms-sdk";
    protected static final String DATA_PATH = "src/test/resources/data/";
    protected static final String JSON_PATH = "src/test/resources/json/";
    private static final String STUB_PATH = "src/test/resources/json/expected/";
    private static final String STUB_EXTENSION = ".json";
    protected final ObjectMapper objectMapper = new ObjectMapper();
    protected final JsonExpectationsHelper jsonHelper = new JsonExpectationsHelper();
    String mpeUrl = "https://mpe-release.local-trust.com/";

    protected MpeToken authentificationV2 = getAuthentificationV2(mpeUrl);
    protected EFormsFormulaireAdd build;
    @Value("${annonces.admin.password}")
    protected String password;
    @Value("${annonces.admin.login}")
    protected String login;
    @Value("${annonces.admin.plateforme}")
    protected String plateforme;
    protected int idConsultation = 508127;
    protected static EFormsFormulaire savedNotice;
    @Autowired
    protected TestRestTemplate testRestTemplate;
    protected String token;
    @LocalServerPort
    protected int port;
    protected String BASE_URL = "http://localhost:";

    protected static String readStub(String stubId) throws IOException {
        Path pathSource = Paths.get(STUB_PATH + stubId + STUB_EXTENSION).toAbsolutePath();
        List<String> lines = Files.readAllLines(pathSource, Charset.defaultCharset());
        return lines.stream().reduce((result, concatWith) -> (result + concatWith.trim()).replace("\": ", "\":")).orElse(null);
    }

    @BeforeEach
    public void setUp() throws Exception {
        this.initToken();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.registerModule(new JavaTimeModule());
        build = EFormsFormulaireAdd.builder()
                .idNotice("16")
                .lang("FRA")
                .idConsultation(idConsultation)
                .tokenMpe(authentificationV2.getToken())
                .mpeUrl(mpeUrl)
                .auteurEmail("ismail.atitallah@atexo.com")
                .build();
    }

    protected HttpHeaders getBearerHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + token);
        headers.set("organisme", "pim");
        headers.set("plateforme", plateforme);
        headers.setContentType(MediaType.APPLICATION_JSON);

        return headers;
    }

    protected <T> void assertJsonEqual(String expectedStubId, T actualObject) throws Exception {

        String actual = objectMapper.writeValueAsString(actualObject);
        jsonHelper.assertJsonEqual(readStub(expectedStubId + "_expected"), actual, false);
    }


    protected <T> T getObject(String jsonId, Class<T> tClass) throws IOException {
        Path pathSource = Paths.get(JSON_PATH + jsonId + STUB_EXTENSION).toAbsolutePath();
        return objectMapper.readValue(pathSource.toFile(), tClass);
    }


    public void initToken() throws Exception {
        if (this.token != null) {
            return;
        }
        var restTemplate = new RestTemplate();
        var uri = new URI("http://localhost:" + port + "/concentrateur-annonces/rest/v2/oauth/token");

        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(login, password);


        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(null, headers);

        ResponseEntity<ITToken> result = restTemplate.exchange(uri + "?grant_type=client_credentials&idPlatform=" + plateforme + "&idConsultation=" + idConsultation, HttpMethod.GET, request, ITToken.class);
        final ITToken body = result.getBody();
        if (body == null) {
            throw new Exception();
        }
        this.token = body.getAccessToken();

    }

    protected MpeToken getAuthentificationV2(String mpeUrl) {
        String url = mpeUrl + "api/v2/token";
        AuthenitificationRequest request = AuthenitificationRequest.builder().login("mpe_tncp_int").password("mpe_tncp_int").build();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");

        return new RestTemplateConfig().restTemplate().postForEntity(url,
                new HttpEntity<>(request, headers), MpeToken.class).getBody();

    }

}
