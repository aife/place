package fr.atexo.annonces.config;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AuthenitificationRequest {
    private String login;
    private String password;
}
