package fr.atexo.annonces.mapper;

import fr.atexo.annonces.boot.entity.Offre;
import fr.atexo.annonces.builder.OffreBuilder;
import fr.atexo.annonces.dto.OffreDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test du mapper entre Offre et OffreDTO
 */
@SpringBootTest(classes = {OffreMapperImpl.class, SupportMapperImpl.class, DepartementMapperImpl.class})
class OffreMapperIntegrationTest {

    @Autowired
    private OffreMapper offreMapper;

    @Test
    void mapOffre_shouldReturnMappedOffreDTO() {
        Offre offre = OffreBuilder.anOffre().withDefaults().build();

        OffreDTO offreDTO = offreMapper.offreToOffreDTOIgnoreSupport(offre);

        assertThat(offreDTO).isEqualToIgnoringGivenFields(offre, "support");
    }

    @Test
    void mapOffreIncludeSupport_shouldReturnMappedOffreDTOWithSupportDTO() {
        Offre offre = OffreBuilder.anOffre().withDefaults().build();

        OffreDTO offreDTO = offreMapper.offreToOffreDTO(offre);

        assertThat(offreDTO).usingRecursiveComparison().isEqualTo(offre);
    }

    @Test
    void mapSetOffre_shouldReturnSetOffreDTO() {
        Set<Offre> offreSet = new HashSet<>();
        offreSet.add(OffreBuilder.anOffre().withDefaults().build());
        offreSet.add(OffreBuilder.anOffre().withDefaults().withId(2).build());

        Set<OffreDTO> offreDTOSet = offreMapper.offresToOffreDTOsIgnoreSupport(offreSet);

        assertThat(offreDTOSet).hasSameSizeAs(offreSet);
    }

    @Test
    void mapSetOffreIncludeSupport_shouldReturnSetOffreDTOWithSupportDTO() {
        Set<Offre> offreSet = new HashSet<>();
        offreSet.add(OffreBuilder.anOffre().withDefaults().build());
        offreSet.add(OffreBuilder.anOffre().withDefaults().withId(2).build());

        Set<OffreDTO> offreDTOSet = offreMapper.offresToOffreDTOs(offreSet);

        assertThat(offreDTOSet).hasSameSizeAs(offreSet);
    }

}
