package fr.atexo.annonces.reseau;

import fr.atexo.annonces.boot.core.domain.e_forms_notice.model.EFormsFormulaire;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.model.EFormsFormulaireAdd;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.model.ValidationResult;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.SubNotice;
import fr.atexo.annonces.config.E2EAbstractResource;
import fr.atexo.annonces.config.EFormsHelper;
import fr.atexo.annonces.config.MpeToken;
import fr.atexo.annonces.config.WsMessage;
import org.junit.jupiter.api.*;
import org.springframework.http.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Disabled
class EFormsNoticeControllerTest extends E2EAbstractResource {

    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test
    @Order(1)
    @DisplayName("1- Vérifier que le ws d'ajout de avis est sécurisé")
    void Given_NoToken_When_PutAvis_Then_ReturnUnauthorized() {
        String mpeUrl = "https://mpe-release.local-trust.com/";

        MpeToken authentificationV2 = getAuthentificationV2(mpeUrl);
        EFormsFormulaireAdd build = EFormsFormulaireAdd.builder()
                .idNotice("16")
                .lang("FRA")
                .idConsultation(idConsultation)
                .tokenMpe(authentificationV2.getToken())
                .mpeUrl(mpeUrl)
                .auteurEmail("ismail.atitallah@atexo.com")
                .build();
        ResponseEntity<WsMessage> result = testRestTemplate.exchange(BASE_URL + port + PATH_EFORMS_NOTICES,
                HttpMethod.POST,
                new HttpEntity<>(build), WsMessage.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @Order(2)
    @DisplayName("2- Vérifier que le ws d'ajout d'avis est OK avec une requête valide")
    void Given_ValidRequest_When_PostAvis_Then_ReturnOk() throws Exception {
        //Given

        String stubId = "POST_NOTICE";

        //When
        HttpHeaders httpHeaders = getBearerHeader();
        ResponseEntity<EFormsFormulaire> result = testRestTemplate.exchange(BASE_URL + port + PATH_EFORMS_NOTICES,
                HttpMethod.POST,
                new HttpEntity<>(build, httpHeaders), EFormsFormulaire.class);
        //Then
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
        savedNotice = result.getBody();
        assertNotNull(savedNotice);
        assertJsonEqual(stubId, savedNotice);
    }

    @Test
    @Order(3)
    @DisplayName("3- Vérifier que le ws de récupération de avis est sécurisé")
    void Given_NoToken_When_GetAvis_Then_ReturnUnauthorized() {
        ResponseEntity<WsMessage> result = testRestTemplate.getForEntity(BASE_URL + port + PATH_EFORMS_NOTICES
                        + "?idConsultation=" + build.getIdConsultation()
                , WsMessage.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @Order(4)
    @DisplayName("4- Vérifier que le ws de liste d'avis est OK avec une requête valide")
    void Given_Token_When_GetListeAvis_Then_ReturnList() throws Exception {
        //When
        HttpHeaders httpHeaders = getBearerHeader();
        ResponseEntity<EFormsFormulaire[]> result = testRestTemplate.exchange(BASE_URL + port + PATH_EFORMS_NOTICES
                        + "?idConsultation=" + build.getIdConsultation(),
                HttpMethod.GET,
                new HttpEntity<>(null, httpHeaders), EFormsFormulaire[].class);
        //Then
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
        assertNotNull(result.getBody());
        assertJsonEqual("GET_LIST_NOTICE", result.getBody());
    }

    @Test
    @Order(5)
    @DisplayName("5- Vérifier que le ws de avis est OK avec une requête valide")
    void Given_Params_When_GetAvis_Then_ReturnNotice() throws Exception {
        //When
        HttpHeaders httpHeaders = getBearerHeader();
        ResponseEntity<EFormsFormulaire> result = testRestTemplate.exchange(BASE_URL + port + PATH_EFORMS_NOTICES
                        + savedNotice.getId(),
                HttpMethod.GET,
                new HttpEntity<>(null, httpHeaders), EFormsFormulaire.class);
        //Then
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
        assertNotNull(result.getBody());
        assertJsonEqual("GET_NOTICE", result.getBody());
    }

    @Test
    @Order(6)
    @DisplayName("6- Vérifier que le ws de structure d'avis est OK avec une requête valide")
    void Given_Params_When_GetAvisStructure_Then_ReturnNoticeStructure() throws Exception {
        //When
        HttpHeaders httpHeaders = getBearerHeader();
        ResponseEntity<SubNotice> result = testRestTemplate.exchange(BASE_URL + port + PATH_EFORMS_NOTICES
                        + savedNotice.getId() + "/structure",
                HttpMethod.GET,
                new HttpEntity<>(null, httpHeaders), SubNotice.class);
        //Then
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
        assertNotNull(result.getBody());
        assertJsonEqual("GET_NOTICE_STRUCTURE", result.getBody());
    }

    @Test
    @Order(7)
    @DisplayName("7- Vérifier que le ws de modification d'avis est OK avec une requête valide")
    void Given_ValidRequest_When_PutAvis_Then_ReturnOk() throws Exception {
        //Given

        String stubId = "PUT_NOTICE";

        //When
        HttpHeaders httpHeaders = getBearerHeader();
        ResponseEntity<EFormsFormulaire> result = testRestTemplate.exchange(BASE_URL + port + PATH_EFORMS_NOTICES
                        + savedNotice.getId(),
                HttpMethod.PUT,
                new HttpEntity<>(EFormsHelper.getObjectFromString(savedNotice.getFormulaire(), Object.class), httpHeaders), EFormsFormulaire.class);
        //Then
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
        savedNotice = result.getBody();
        assertNotNull(savedNotice);
        assertJsonEqual(stubId, savedNotice);
    }

    @Test
    @Order(8)
    @DisplayName("8- Vérifier que le ws de validation d'avis est OK avec une requête valide")
    void Given_ValidRequest_When_ValidateAvis_Then_ReturnOk() throws Exception {
        if (savedNotice == null) {
            this.Given_ValidRequest_When_PostAvis_Then_ReturnOk();
        }
        //Given

        String stubId = "VALIDATE_NOTICE";

        //When
        HttpHeaders httpHeaders = getBearerHeader();
        ResponseEntity<ValidationResult> result = testRestTemplate.exchange(BASE_URL + port + PATH_EFORMS_NOTICES
                        + savedNotice.getId() + "/validate",
                HttpMethod.POST,
                new HttpEntity<>(EFormsHelper.getObjectFromString(savedNotice.getFormulaire(), Object.class), httpHeaders),
                ValidationResult.class);
        //Then
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
        assertNotNull(result.getBody());
        assertJsonEqual(stubId, result.getBody());
    }

    @Test
    @Order(9)
    @DisplayName("9- Vérifier que le ws de soumission d'avis est OK avec une requête valide")
    void Given_ValidRequest_When_SubmitAvis_Then_ReturnOk() throws Exception {
        //Given

        String stubId = "SUBMIT_NOTICE";

        //When
        HttpHeaders httpHeaders = getBearerHeader();
        ResponseEntity<EFormsFormulaire> result = testRestTemplate.exchange(BASE_URL + port + PATH_EFORMS_NOTICES
                        + savedNotice.getId() + "/submit",
                HttpMethod.POST,
                new HttpEntity<>(EFormsHelper.getObjectFromString(savedNotice.getFormulaire(), Object.class), httpHeaders),
                EFormsFormulaire.class);
        //Then
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
        assertNotNull(result.getBody());
        assertJsonEqual(stubId, result.getBody());
    }

    @Test
    @Order(10)
    @DisplayName("10- Vérifier que le ws de stop de publication d'avis est OK avec une requête valide")
    void Given_ValidRequest_When_StopAvis_Then_ReturnOk() throws Exception {
        //Given


        //When
        HttpHeaders httpHeaders = getBearerHeader();
        ResponseEntity<EFormsFormulaire> result = testRestTemplate.exchange(BASE_URL + port + PATH_EFORMS_NOTICES
                        + savedNotice.getId() + "/stop-publication",
                HttpMethod.DELETE,
                new HttpEntity<>(httpHeaders), EFormsFormulaire.class);
        //Then
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
        assertNotNull(result.getBody());
        assertThat(result.getBody()).isEqualTo("OK");

    }
}
