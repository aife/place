package fr.atexo.annonces.reseau;

import fr.atexo.annonces.config.E2EAbstractResource;
import fr.atexo.annonces.config.WsMessage;
import fr.atexo.annonces.dto.SuiviAnnonceDTO;
import fr.atexo.annonces.dto.SupportDTO;
import org.junit.jupiter.api.*;
import org.springframework.http.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Disabled
class AnnonceControllerTest extends E2EAbstractResource {

    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test
    @Order(1)
    @DisplayName("1- Vérifier que le ws d'ajout de annonces est sécurisé")
    public void Given_NoToken_When_PutAnnonces_Then_ReturnUnauthorized() {
        ResponseEntity<WsMessage> result = testRestTemplate.exchange(BASE_URL + port + PATH_ANNONCES,
                HttpMethod.PUT,
                null, WsMessage.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @Order(2)
    @DisplayName("2- Vérifier que le ws d'ajout d'annonces est OK avec une requête valide")
    public void Given_ValidRequest_When_PutAnnonces_Then_ReturnOk() throws Exception {
        //Given
        String stubId = "PUT_ANNONCE";
        SuiviAnnonceDTO suiviAnnonceDTO = getObject(stubId, SuiviAnnonceDTO.class);
        suiviAnnonceDTO.setIdConsultation(idConsultation);
        suiviAnnonceDTO.setIdPlatform(plateforme);
        //When
        HttpHeaders httpHeaders = getBearerHeader();
        ResponseEntity<Object> result = testRestTemplate.exchange(BASE_URL + port + PATH_ANNONCES +
                        "?idPlatform=" + plateforme + "&idConsultation=" + idConsultation + "&codeSupport=" + suiviAnnonceDTO.getSupport().getCode(),
                HttpMethod.PUT,
                new HttpEntity<>(suiviAnnonceDTO, httpHeaders), Object.class);
        //Then
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.CREATED.value());
        assertNotNull(result.getBody());
        assertJsonEqual(stubId, result.getBody());
    }

    @Test
    @Order(3)
    @DisplayName("3- Vérifier que le ws de récupération de annonces est sécurisé")
    public void Given_NoToken_When_GetAnnonces_Then_ReturnUnauthorized() {
        ResponseEntity<WsMessage> result = testRestTemplate.postForEntity(BASE_URL + port + PATH_ANNONCES, null, WsMessage.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @Order(4)
    @DisplayName("4- Vérifier que le ws de annonces est OK avec une requête valide")
    public void Given_Token_When_GetAnnonces_Then_ReturnList() throws Exception {
        //When
        HttpHeaders httpHeaders = getBearerHeader();
        ResponseEntity<SupportDTO[]> result = testRestTemplate.exchange(BASE_URL + port + PATH_ANNONCES +
                        "?idPlatform=" + plateforme + "&idConsultation=" + idConsultation,
                HttpMethod.GET,
                new HttpEntity<>(null, httpHeaders), SupportDTO[].class);
        //Then
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
        assertNotNull(result.getBody());
        assertJsonEqual("GET_LIST_ANNONCES", result.getBody());
    }

    @Test
    @Order(5)
    @DisplayName("5- Vérifier que le ws de annonces est OK avec une requête valide")
    public void Given_IcludeOffres_When_GetAnnonces_Then_ReturnList() throws Exception {
        //When
        HttpHeaders httpHeaders = getBearerHeader();
        ResponseEntity<SupportDTO[]> result = testRestTemplate.exchange(BASE_URL + port + PATH_ANNONCES +
                        "?include=support&idPlatform=" + plateforme + "&idConsultation=" + idConsultation,
                HttpMethod.GET,
                new HttpEntity<>(null, httpHeaders), SupportDTO[].class);
        //Then
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
        assertNotNull(result.getBody());
        assertJsonEqual("GET_LIST_ANNONCES_WITH_SUPPORT", result.getBody());
    }
}
