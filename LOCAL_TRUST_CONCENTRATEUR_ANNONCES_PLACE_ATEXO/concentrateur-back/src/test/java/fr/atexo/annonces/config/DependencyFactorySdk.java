/*package fr.atexo.annonces.config;

import eu.europa.ted.eforms.sdk.ComponentFactory;
import eu.europa.ted.eforms.sdk.SdkSymbolResolver;
import eu.europa.ted.efx.EfxTranslatorOptions;
import eu.europa.ted.efx.exceptions.ThrowingErrorListener;
import eu.europa.ted.efx.interfaces.*;
import eu.europa.ted.efx.xpath.XPathScriptGenerator;
import lombok.extern.slf4j.Slf4j;
import org.antlr.v4.runtime.BaseErrorListener;

import java.nio.file.Path;

@Slf4j
public class DependencyFactorySdk implements TranslatorDependencyFactory {

    private final String sdkVersion;
    private final Path sdkRootPath;


    ScriptGenerator scriptGenerator;
    MarkupGenerator markupGenerator;


    public DependencyFactorySdk(String sdkVersion, Path sdkRootPath) throws InstantiationException {
        this.sdkVersion = sdkVersion;
        this.sdkRootPath = sdkRootPath;

    }

    @Override
    public SymbolResolver createSymbolResolver(String sdkVersion) {
        try {
            return new SdkSymbolResolver(this.sdkVersion, this.sdkRootPath);
        } catch (InstantiationException e) {
            log.error("Failed to instantiate SDK Symbol Resolver for SDK version [{}]", sdkVersion, e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public ScriptGenerator createScriptGenerator(String sdkVersion) {
        try {
            return ComponentFactory.getScriptGenerator(this.sdkVersion, EfxTranslatorOptions.DEFAULT);
        } catch (InstantiationException e) {
            log.error("Failed to instantiate SDK Script Generator for SDK version [{}]", sdkVersion, e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public ScriptGenerator createScriptGenerator(String sdkVersion, TranslatorOptions options) {
        if (scriptGenerator == null) {
            this.scriptGenerator = new XPathScriptGenerator(options);
        }
        return this.scriptGenerator;
    }

    @Override
    public MarkupGenerator createMarkupGenerator(String sdkVersion) {
        return this.createMarkupGenerator(sdkVersion, EfxTranslatorOptions.DEFAULT);
    }

    @Override
    public MarkupGenerator createMarkupGenerator(String sdkVersion, TranslatorOptions options) {
        if (this.markupGenerator == null) {
            try {
                this.markupGenerator = ComponentFactory.getMarkupGenerator(this.sdkVersion, options);
            } catch (InstantiationException e) {
                log.error("Failed to instantiate SDK Markup Generator for SDK version [{}]", sdkVersion, e);
                throw new RuntimeException(e);
            }
        }
        return this.markupGenerator;
    }

    @Override
    public BaseErrorListener createErrorListener() {
        return ThrowingErrorListener.INSTANCE;
    }
}
*/
