package fr.atexo.annonces.mapper;

import fr.atexo.annonces.boot.entity.Support;
import fr.atexo.annonces.builder.SupportBuilder;
import fr.atexo.annonces.dto.SupportDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test du mapper entre Support et SupportDTO
 */
@SpringBootTest(classes = {SupportMapperImpl.class, OffreMapperImpl.class, DepartementMapperImpl.class})
class SupportMapperIntegrationTest {

    @Autowired
    private SupportMapper supportMapper;

    @Test
    void mapSupport_shouldReturnMappedSupportDTO() {
        Support support = SupportBuilder.aSupport().withDefaults().build();

        SupportDTO supportDTO = supportMapper.supportToSupportDTOIgnoreOffres(support);

        assertThat(supportDTO)
                .isEqualToIgnoringGivenFields(support, "offres")
                .extracting(SupportDTO::getOffres)
                .isNull();
    }

    @Test
    void mapSupportIncludeOffre_shouldReturnMappedSupportDTOWithOffreDTO() {
        Support support = SupportBuilder.aSupport().withDefaults().build();

        SupportDTO supportDTO = supportMapper.supportToSupportDTO(support);

        assertThat(supportDTO).isEqualToIgnoringGivenFields(support, "offres");
        assertThat(supportDTO.getOffres()).hasSameSizeAs(support.getOffres());
    }

    @Test
    void mapListSupport_shouldReturnListSupportDTO() {
        List<Support> supportList = Arrays.asList(SupportBuilder.aSupport().withDefaults().build(),
                SupportBuilder.aSupport().withDefaults().withId(2).build());

        List<SupportDTO> supportDTOList = supportMapper.supportsToSupportDTOs(supportList, false);

        assertThat(supportDTOList)
                .hasSameSizeAs(supportList)
                .extracting(SupportDTO::getOffres)
                .containsOnlyNulls();
    }

    @Test
    void mapListSupportIncludeOffre_shouldReturnListSupportDTOWithOffreDTO() {
        List<Support> supportList = Arrays.asList(SupportBuilder.aSupport().withDefaults().build(),
                SupportBuilder.aSupport().withDefaults().withId(2).build());

        List<SupportDTO> supportDTOList = supportMapper.supportsToSupportDTOs(supportList, true);

        assertThat(supportDTOList)
                .hasSameSizeAs(supportList)
                .extracting(SupportDTO::getOffres)
                .doesNotContainNull();
    }

}
