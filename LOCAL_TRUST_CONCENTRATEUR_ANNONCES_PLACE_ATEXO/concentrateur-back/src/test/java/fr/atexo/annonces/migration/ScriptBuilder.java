package fr.atexo.annonces.migration;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

public class ScriptBuilder {

    @Test
    void buildLocal() throws IOException {
        var plateforme_uuid_to_update = "MPE_atexo_develop";
        var plateforme_url_to_update = "https://simap-mpe-int.local-trust.com";
        var plateforme_lang_to_update = "LUX";
        var nas_concentrateur = "/data/annonces/fichiers/";
        var nas_mpe = "/data/apache2/simap-mpe-int.local-trust.com/data/";
        var serveur_concentrateur = "atexo-concpub-int-01";
        var user_concentrateur = "iat-atx";
        var serveur_mpe = "simap-mpe-int-01";
        var user_mpe = "iat-atx";
        var bddConcPub = "concentrateur.";
        var bddMpe = "mpe.";

        //read files from folder src/main/resources/migration/step
        //for each file, replace the following strings:
        createFile(plateforme_uuid_to_update, plateforme_url_to_update, plateforme_lang_to_update, nas_concentrateur, nas_mpe, serveur_concentrateur, user_concentrateur, serveur_mpe, user_mpe, bddConcPub, bddMpe);

    }

    @Test
    void buildInt() throws IOException {
        var plateforme_uuid_to_update = "MPE_simap_int";
        String host = "simap-mpe-int.local-trust.com";
        var plateforme_url_to_update = "https://" + host;
        var plateforme_lang_to_update = "LUX";
        var nas_concentrateur = "/data/annonces/fichiers/";
        var nas_mpe = "/data/apache2/" + host + "/data/";
        var serveur_concentrateur = "atexo-concpub-int-01";
        var user_concentrateur = "iat-atx";
        var serveur_mpe = "simap-mpe-int-01";
        var user_mpe = "iat-atx";
        var bddConcPub = "atexo_int_concentrateur_annonces_db.";
        var bddMpe = "mpe.";

        //read files from folder src/main/resources/migration/step
        //for each file, replace the following strings:
        createFile(plateforme_uuid_to_update, plateforme_url_to_update, plateforme_lang_to_update, nas_concentrateur, nas_mpe, serveur_concentrateur, user_concentrateur, serveur_mpe, user_mpe, bddConcPub, bddMpe);
    }

    @Test
    void buildREC() throws IOException {
        var plateforme_uuid_to_update = "MPE_simap_rec";
        String host = "pmp-mpe-r1.tp.etat.lu";
        var plateforme_url_to_update = "https://" + host;
        var plateforme_lang_to_update = "LUX";
        var nas_concentrateur = "/data/annonces/fichiers/";
        var nas_mpe = "/data/apache2/" + host + "/data/";
        var serveur_concentrateur = "simap-concpub-rec-01";
        var user_concentrateur = "abo-atx";
        var serveur_mpe = "simap-mpe-rec-01";
        var user_mpe = "abo-atx";
        var bddConcPub = "simap_rec_concentrateur_annonces_db.";
        var bddMpe = "mpe.";

        createFile(plateforme_uuid_to_update, plateforme_url_to_update, plateforme_lang_to_update, nas_concentrateur, nas_mpe, serveur_concentrateur, user_concentrateur, serveur_mpe, user_mpe, bddConcPub, bddMpe);
    }

    @Test
    void buildPreprod() throws IOException {
        var plateforme_uuid_to_update = "MPE_simap_preprod";
        String host = "pmp-prep.tp.etat.lu";
        var plateforme_url_to_update = "https://" + host;
        var plateforme_lang_to_update = "LUX";
        var nas_concentrateur = "/data/annonces/fichiers/";
        var nas_mpe = "/data/apache2/pmp.tp.etat.lu/data/";
        var serveur_concentrateur = "simap-concpub-prep-01";
        var user_concentrateur = "tomcat";
        var serveur_mpe = "simap-mpe-prod-01";
        var user_mpe = "abo-atx";
        var bddConcPub = "simap_prep_concentrateur_annonces_db.";
        var bddMpe = "mpe.";

        createFile(plateforme_uuid_to_update, plateforme_url_to_update, plateforme_lang_to_update, nas_concentrateur, nas_mpe, serveur_concentrateur, user_concentrateur, serveur_mpe, user_mpe, bddConcPub, bddMpe);
    }

    @Test
    void buildProd() throws IOException {
        var plateforme_uuid_to_update = "MPE_simap_prod";
        String host = "pmp.tp.etat.lu";
        var plateforme_url_to_update = "https://" + host;
        var plateforme_lang_to_update = "LUX";
        var nas_concentrateur = "/data/annonces/fichiers/";
        var nas_mpe = "/data/apache2/" + host + "/data/";
        var serveur_concentrateur = "simap-concpub-prod-01";
        var user_concentrateur = "tomcat";
        var serveur_mpe = "simap-mpe-prod-01";
        var user_mpe = "abo-atx";
        var bddConcPub = "simap_prod_concentrateur_annonces_db.";
        var bddMpe = "mpe.";

        createFile(plateforme_uuid_to_update, plateforme_url_to_update, plateforme_lang_to_update, nas_concentrateur, nas_mpe, serveur_concentrateur, user_concentrateur, serveur_mpe, user_mpe, bddConcPub, bddMpe);
    }

    private static void createFile(String plateforme_uuid_to_update, String plateforme_url_to_update, String plateforme_lang_to_update, String nas_concentrateur, String nas_mpe, String serveur_concentrateur, String user_concentrateur, String serveur_mpe, String user_mpe, String bddConcPub, String bddMpe) throws IOException {
        //read files from folder src/main/resources/migration/step
        //for each file, replace the following strings:
        File file = new File("src/test/resources/migration/scripts/" + plateforme_uuid_to_update + ".sql");
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        File folder = new File("src/test/resources/migration/step");
        List<File> listOfFiles = Arrays.stream(folder.listFiles()).sorted().toList();
        final StringBuilder content = new StringBuilder();
        for (File f : listOfFiles) {
            if (f.isFile()) {
                //Read file
                Files.readAllLines(f.toPath()).forEach(s -> content.append(s).append("\n"));
                //Replace strings
            }
        }
        String result = content.toString().replace("MPE_atexo_develop", plateforme_uuid_to_update)
                .replace("https://simap-mpe-int.local-trust.com", plateforme_url_to_update)
                .replace("LUX", plateforme_lang_to_update)
                .replace("/data/annonces/fichiers/", nas_concentrateur)
                .replace("/data/apache2/simap-mpe-int.local-trust.com/data/", nas_mpe)
                .replace("atexo-concpub-int-01", serveur_concentrateur)
                .replace("iat-atx", user_concentrateur)
                .replace("simap-mpe-int-01", serveur_mpe)
                .replace("iat-atx", user_mpe)
                .replace("concentrateur.", bddConcPub)
                .replace("mpe.", bddMpe);
        //Write file
        Files.writeString(file.toPath(), result);
    }
}
