package fr.atexo.annonces.config;


import org.testcontainers.containers.MariaDBContainer;

public class ITCustomMariaDBContainer extends MariaDBContainer<ITCustomMariaDBContainer> {
    private static ITCustomMariaDBContainer ITCustomMariaDBContainer;

    private ITCustomMariaDBContainer() {
        super();
    }

    public static ITCustomMariaDBContainer getInstance() {
        if (ITCustomMariaDBContainer == null) {
            ITCustomMariaDBContainer = new ITCustomMariaDBContainer().withDatabaseName("concentrateurs")
                    .withCommand("mysqld", "--lower_case_table_names=1");
        }
        return ITCustomMariaDBContainer;
    }

    @Override
    public void start() {
        super.start();
        System.setProperty("DB_URL", ITCustomMariaDBContainer.getJdbcUrl());
        System.setProperty("DB_USERNAME", ITCustomMariaDBContainer.getUsername());
        System.setProperty("DB_PASSWORD", ITCustomMariaDBContainer.getPassword());
    }

    @Override
    public void stop() {
        // do nothing, JVM handles shut down
    }
}
