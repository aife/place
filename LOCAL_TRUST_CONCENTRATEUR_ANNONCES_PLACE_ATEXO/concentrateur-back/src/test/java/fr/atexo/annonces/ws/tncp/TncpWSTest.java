package fr.atexo.annonces.ws.tncp;


import fr.atexo.annonces.boot.ws.tncp.TncpWS;
import fr.atexo.annonces.commun.tncp.Client;
import fr.atexo.annonces.commun.tncp.Token;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@Disabled
class TncpWSTest {

    private TncpWS tncpWS = new TncpWS(new URI("https://oauth.rec.piste.gouv.fr/api/oauth/token"), new URI("https://api.rec.piste.gouv.fr/avis/redirections-contexts"), new RestTemplate());

    TncpWSTest() throws URISyntaxException {
    }

    @Test
    void WhenAuthentificationThenReturnToken() {
        Token token = tncpWS.authentifier(Client.with().clientId("6f63a252-eb6c-4e62-ae77-c8fe2be79661").clientSecret("50626b85-a893-4167-a9ab-5554ba97da19").build());
        assertNotNull(token);
        assertNotNull(token.getAccessToken());

    }



}
