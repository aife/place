package fr.atexo.annonces.reseau;

import fr.atexo.annonces.config.E2EAbstractResource;
import fr.atexo.annonces.config.WsMessage;
import fr.atexo.annonces.dto.SupportDTO;
import org.junit.jupiter.api.*;
import org.springframework.http.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Disabled
class SupportControllerTest extends E2EAbstractResource {
    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test
    @Order(1)
    @DisplayName("1- Vérifier que le ws de récupération de support est sécurisé")
    public void Given_NoToken_When_GetSupport_Then_ReturnUnauthorized() {
        ResponseEntity<WsMessage> result = testRestTemplate.postForEntity(BASE_URL + port + PATH_SUPPORT, null, WsMessage.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @Order(2)
    @DisplayName("2- Vérifier que le ws de support est OK avec une requête valide")
    public void Given_Token_When_GetSupport_Then_ReturnList() throws Exception {
        //When
        HttpHeaders httpHeaders = getBearerHeader();
        String url = BASE_URL + port + PATH_SUPPORT + "?idPlatform=" + plateforme + "&idConsultation=" + this.idConsultation;
        ResponseEntity<SupportDTO[]> result = testRestTemplate.exchange(url,
                HttpMethod.GET,
                new HttpEntity<>(null, httpHeaders), SupportDTO[].class);
        //Then
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
        assertNotNull(result.getBody());
        assertJsonEqual("GET_LIST_SUPPORT", result.getBody());
    }

    @Test
    @Order(3)
    @DisplayName("3- Vérifier que le ws de support est OK avec une requête valide")
    public void Given_IcludeOffres_When_GetSupport_Then_ReturnList() throws Exception {
        //When
        HttpHeaders httpHeaders = getBearerHeader();
        String url = BASE_URL + port + PATH_SUPPORT +
                "?include=offres" + "&idPlatform=" + plateforme + "&idConsultation=" + this.idConsultation;
        ResponseEntity<SupportDTO[]> result = testRestTemplate.exchange(url,
                HttpMethod.GET,
                new HttpEntity<>(null, httpHeaders), SupportDTO[].class);
        //Then
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
        assertNotNull(result.getBody());
        assertJsonEqual("GET_LIST_SUPPORT_WITH_OFFRES", result.getBody());
    }
}
