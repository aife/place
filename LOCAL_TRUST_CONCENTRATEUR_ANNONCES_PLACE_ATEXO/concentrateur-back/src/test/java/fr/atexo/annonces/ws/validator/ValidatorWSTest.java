package fr.atexo.annonces.ws.validator;


import com.helger.schematron.svrl.SVRLHelper;
import com.helger.schematron.svrl.SVRLMarshaller;
import com.helger.schematron.svrl.jaxb.FailedAssert;
import com.helger.schematron.svrl.jaxb.SchematronOutputType;
import fr.atexo.annonces.boot.ws.validator.ValidatorConfiguration;
import fr.atexo.annonces.boot.ws.validator.ValidatorWS;
import fr.atexo.annonces.config.RestTemplateConfig;
import fr.atexo.annonces.dto.validator.ValidatorDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
@Disabled
class ValidatorWSTest {
    private ValidatorConfiguration validatorConfiguration = ValidatorConfiguration.builder()
            .basePath("https://cvs.preview.ted.europa.eu")
            .apiKey("d7b10c46f4f94b7b891007e639ed62a8")
            .ws(Map.of("validate", "/v1/notices/validation"))
            .build();
    private ValidatorWS validatorWS = new ValidatorWS(new RestTemplateConfig().restTemplate(), validatorConfiguration);

    @Test
    void validateXml() throws IOException {

        //  File notice = new File("src/test/resources/data/cn_24_minimal.xml");
        File notice = new File("src/test/resources/data/formulaire.xml");
        Resource avisemis = validatorWS.validateXml(Files.readString(notice.toPath()), ValidatorDTO.builder()
                .language("fr")
                .validationMode("static")
                .eFormsSdkVersion("eforms-sdk-1.5")
                .build(), null);
        assertNotNull(avisemis);
        File destination = new File("src/test/resources/data/report.xml");
        FileUtils.copyInputStreamToFile(avisemis.getInputStream(), destination);
        SchematronOutputType reportExpected = new SVRLMarshaller().read(destination);
        List<String> errors = new ArrayList<>();
        List<Object> failedAsserts = reportExpected.getActivePatternAndFiredRuleAndFailedAssert();
        for (Object object : failedAsserts) {
            if (object instanceof FailedAssert) {
                FailedAssert failedAssert = (FailedAssert) object;
                String text = SVRLHelper.getAsString(failedAssert.getText());
                log.error("text {} test {}", text, failedAssert.getTest());
                errors.add(text);
            }
        }
        assertThat(errors).isEmpty();
    }


}
