package fr.atexo.annonces.boot.ws.docgen;

import fr.atexo.annonces.config.EFormsHelper;
import fr.atexo.annonces.config.RestTemplateConfig;
import fr.atexo.annonces.config.WsScanConfig;
import fr.atexo.annonces.dto.DocumentEditorRequest;
import fr.atexo.annonces.dto.User;
import fr.atexo.annonces.service.exception.AtexoException;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ActiveProfiles;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = {WsScanConfig.class, RestTemplateConfig.class})
@ActiveProfiles("ws")
class DocgenAdapterTest {

    @Autowired
    private DocGenWsAdapter docGenWsAdapter;


    @Test
    void Given_TemplateNull_When_GenerateDocument_Then_ThrowException() {
        List<KeyValueRequest> map = new ArrayList<>();
        assertThatExceptionOfType(AtexoException.class)
                .isThrownBy(() -> docGenWsAdapter.generateDocument(map, null,"à compléter"))
                .withMessage("Le fichier ne peut pas être null");

    }

    @Test
    void Given_ListeVariableNull_When_GenerateDocument_ThrowException() {
        File template = new File("src/test/resources/data/avis-standard.docx");
        assertThatExceptionOfType(AtexoException.class)
                .isThrownBy(() -> docGenWsAdapter.generateDocument(null, template,"à compléter"))
                .withMessage("La liste des parametres ne doit pas être null");

    }


    @Test
    void Given_Template_When_GenerateDocument_Then_ReturnDocx() throws IOException {
        List<KeyValueRequest> keyValueRequests = Arrays.asList(EFormsHelper.getObjectFromFile("src/test/resources/data/keyValueRequests.json", KeyValueRequest[].class));
        File template = new File("src/test/resources/data/avis-standard.docx");
        InputStream result = docGenWsAdapter.generateDocument(keyValueRequests, template,"à compléter");
        assertNotNull(result);
        XWPFDocument wordDoc = new XWPFDocument(result);
        XWPFWordExtractor ex = new XWPFWordExtractor(wordDoc);
        assertEquals("JOURNAL  OFFICIEL  DE  LA  RÉPUBLIQUE  FRANÇAISE\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "Avis national standard \n" +
                "\n" +
                "à compléter \n" +
                "\n" +
                "Date limite de réponse : 30 août 2023 à 10:00\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "Section 1\n" +
                "Identification de l’acheteur\n" +
                "\n" +
                "Nom complet de l’acheteur (*) : à compléter\n" +
                "Type de Numéro national d’identification (*) : SIRET\tNo national d’identification (*) : à compléter\n" +
                "Ville : à compléter\tCode Postal : à compléter\n" +
                "Groupement d’acheteurs (*) : Non\t\n" +
                "\n" +
                "\t\n" +
                "\n" +
                "\n" +
                "\n" +
                "Section 2\n" +
                "Communication\n" +
                "\n" +
                "Moyen d’accès aux documents de la consultation (*) :\tLien URL vers le profil d’acheteur\tà compléter\n" +
                "Identifiant interne de la consultation : 2300145\n" +
                "L’intégralité des documents de la consultation se trouve sur le profil d’acheteur (*) :Oui\t\n" +
                "Utilisation de moyens de communication non communément disponibles (*) : Non\t\n" +
                "\n" +
                "\n" +
                "Nom du contact (*) : à compléter\tAdresse mail du contact : à compléter\n" +
                "\tNo téléphone du contact :à compléter\n" +
                "\n" +
                "\n" +
                "Section 3\n" +
                "Procédure\n" +
                "\n" +
                "Type de procédure (*) : à compléter\n" +
                "Condition de participations (*) :\n" +
                "Aptitude à exercer l’activité professionnelle\tConditions / moyens de preuve : à compléter\n" +
                "Capacité économique et financière\tConditions / moyens de preuve : à compléter\n" +
                "Capacités techniques et professionnelles\tConditions / moyens de preuve : à compléter\n" +
                "Technique d’achat (*) : Sans objet\n" +
                "Date et heure limites de réception des plis (*) : 30 août 2023 à 10:00\n" +
                "Présentation des offres par catalogue électronique (*) :\tà compléter\n" +
                "Réduction du nombre de candidats (*) : Non\tNombre maximum de candidats : à compléter\n" +
                "\t\n" +
                "Possibilité d’attribution sans négociation  (Attribution sur la base de l’offre initiale) (*) : Non\n" +
                "L’acheteur exige la présentation de variantes (*) :Non\n" +
                "Identification des catégories d’acheteurs intervenant (Si accord-cadre) :\n" +
                "\n" +
                "\n" +
                "\n" +
                "Critères d’attribution (obligatoire si SAD) :\n" +
                "\n" +
                "\n" +
                "Section 4\n" +
                "Identification du marché\n" +
                "\n" +
                "Intitulé du marché (*) : Test kad 23/08/23  é à è ü ö ä $ £ € ß ç % &amp; [ ] @ \"L'Hôtel\"\tCode CPV Principal : à compléter\n" +
                "Type de marché (*) : travaux\t\t\n" +
                "Description succincte du marché : Test kad 23/08/23  ?? ?? ?? ?? ?? ?? $ ?? ??? ?? ?? % & [ ] @ \"L'H??tel\"\n" +
                "Lieu principal d’exécution du marché (*) : à compléter\tDurée du marché (en mois) : à compléter\n" +
                "Valeur estimée du besoin (en euros) : à compléter à compléter\t(Si accord-cadre ou SAD, indiquer la valeur maximale)\n" +
                "\n" +
                "\n" +
                "La consultation comporte des tranches (*) : Non\n" +
                "La consultation pévoit une réservation de tout ou partie du marché (si marché alloti, préciser pour chaque lot dans la description)(*) : Non\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "Section 5\n" +
                "Lots\n" +
                "\n" +
                "Marché alloti (*) : Non\n" +
                "\n" +
                "           \n" +
                "Section 6\n" +
                "Informations complémentaires\n" +
                "\n" +
                "Visite obligatoire (*) :\tNon\tDétails sur la visite (si oui) : à compléter\n" +
                "Autres informations complémentaires :\n" +
                "\n" +
                "(*) Les champs signalés par un astérisque doivent obligatoirement être renseignés\n" +
                "1/3\n", ex.getText());

    }

    @Test
    void Given_TemplateNull_When_ConvertToPDF_Then_ThrowException() {
        assertThatExceptionOfType(AtexoException.class)
                .isThrownBy(() -> docGenWsAdapter.convertToPDF(null, null))
                .withMessage("Le fichier ne peut pas être null");

    }

    @Test
    @Disabled
    void Given_ExtensionAndTemplateValid_When_ConvertToPDF_Then_ReturnPDF() throws IOException {
        File template = new File("src/test/resources/data/avis-standard.docx");
        Resource result = docGenWsAdapter.convertToPDF(template, "docx");
        assertNotNull(result);
        assertTrue(isPDF(result.getInputStream()));
    }

    @Test
    void Given_TemplateNull_When_GetEditionToken_Then_ThrowException() {
        assertThatExceptionOfType(AtexoException.class)
                .isThrownBy(() -> docGenWsAdapter.getEditionToken(null, null, null))
                .withMessage("Le fichier ne peut pas être null");

    }

    @Test
    void Given_ConfigurationNull_When_GetEditionToken_Then_ThrowException() {
        File template = new File("src/test/resources/data/avis-standard.docx");
        assertThatExceptionOfType(AtexoException.class)
                .isThrownBy(() -> docGenWsAdapter.getEditionToken(template, null, "docx"))
                .withMessage("Les parametres d'édition en ligne ne doivent pas être null");

    }

    @Test
    void Given_ExtensionNull_When_GetEditionToken_Then_ThrowException() {
        File template = new File("src/test/resources/data/avis-standard.docx");
        DocumentEditorRequest request = DocumentEditorRequest.builder()
                .documentId("Test")
                .documentTitle("Test.docx")
                .plateformeId("reccensment-ws")
                .mode("edit")
                .user(User.builder()
                        .id("1")
                        .name("Test")
                        .build())
                .build();
        assertThatExceptionOfType(AtexoException.class)
                .isThrownBy(() -> {
                    docGenWsAdapter.getEditionToken(template, request, null);
                })
                .withMessage("Les extensions éligibles pour l'édition sont pptx, xlsx, docx");

    }


    @Test
    void Given_ExtensionAndConfigurationAndTemplateValid_When_GetEditionToken_Then_ReturnToken() {
        File template = new File("src/test/resources/data/avis-standard.docx");
        DocumentEditorRequest request = DocumentEditorRequest.builder()
                .documentId("Test")
                .documentTitle("Test.docx")
                .plateformeId("reccensment-ws")
                .mode("edit")
                .user(User.builder()
                        .id("1")
                        .name("Test")
                        .build())
                .build();
        String result = docGenWsAdapter.getEditionToken(template, request, "docx");
        assertNotNull(result);

    }


    @Test
    void Given_TokenNull_When_GetDocument_Then_ThrowException() {
        assertThatExceptionOfType(AtexoException.class)
                .isThrownBy(() -> docGenWsAdapter.getDocument(null))
                .withMessage("Le token ne peut pas être null");

    }

    @Test
    void Given_ExtensionAndConfigurationAndTemplateValid_When_GetDocument_Then_ReturnDocument() throws IOException {
        File template = new File("src/test/resources/data/avis-standard.docx");
        DocumentEditorRequest request = DocumentEditorRequest.builder()
                .documentId("Test")
                .documentTitle("Test.docx")
                .plateformeId("annonces-ws")
                .mode("edit")
                .user(User.builder()
                        .id("1")
                        .name("Test")
                        .build())
                .build();
        String token = docGenWsAdapter.getEditionToken(template, request, "docx");
        // WHEN
        InputStream result = docGenWsAdapter.getDocument(token);
        assertNotNull(result);
        XWPFDocument wordDoc = new XWPFDocument(result);
        XWPFWordExtractor ex = new XWPFWordExtractor(wordDoc);
        assertEquals("JOURNAL  OFFICIEL  DE  LA  RÉPUBLIQUE  FRANÇAISE\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "Avis national standard \n" +
                "\n" +
                "[forms.contractingbody.addressContractingBody.officialname] \n" +
                "\n" +
                "Date limite de réponse : [forms.procedure.dateLimiteReponse]\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "Section 1\n" +
                "Identification de l’acheteur\n" +
                "\n" +
                "Nom complet de l’acheteur (*) : [forms.contractingbody.addressContractingBody.officialname]\n" +
                "Type de Numéro national d’identification (*) : SIRET\tNo national d’identification (*) : [forms.contractingbody.addressContractingBody.siret]\n" +
                "Ville : [forms.contractingbody.addressContractingBody.town]\tCode Postal : [forms.contractingbody.addressContractingBody.postalcode]\n" +
                "Groupement d’acheteurs (*) : [forms.objectcontract.isGroupement]\t\n" +
                "\n" +
                "\t\n" +
                "\n" +
                "\n" +
                "\n" +
                "Section 2\n" +
                "Communication\n" +
                "\n" +
                "Moyen d’accès aux documents de la consultation (*) :\tLien URL vers le profil d’acheteur\t[forms.contractingbody.urlDocument]\n" +
                "Identifiant interne de la consultation : [forms.objectcontract.referenceNumber]\n" +
                "L’intégralité des documents de la consultation se trouve sur le profil d’acheteur (*) :Oui\t\n" +
                "Utilisation de moyens de communication non communément disponibles (*) : Non\t\n" +
                "\n" +
                "\n" +
                "Nom du contact (*) : [forms.contractingbody.addressContractingBody.contactpoint]\tAdresse mail du contact : [forms.contractingbody.addressContractingBody.email]\n" +
                "\tNo téléphone du contact :[forms.contractingbody.addressContractingBody.phone]\n" +
                "\n" +
                "\n" +
                "Section 3\n" +
                "Procédure\n" +
                "\n" +
                "Type de procédure (*) : [forms.procedure.typeProcedure]\n" +
                "Condition de participations (*) :\n" +
                "Aptitude à exercer l’activité professionnelle\tConditions / moyens de preuve : [forms.procedure.conditions.activiteProfessionel]\n" +
                "Capacité économique et financière\tConditions / moyens de preuve : [forms.procedure.conditions.economiqueFinanciere]\n" +
                "Capacités techniques et professionnelles\tConditions / moyens de preuve : [forms.procedure.conditions.techniquesProfessionels]\n" +
                "Technique d’achat (*) : [forms.procedure.techniqueachat]\n" +
                "Date et heure limites de réception des plis (*) : [forms.procedure.dateLimiteReponse]\n" +
                "Présentation des offres par catalogue électronique (*) :\t[forms.procedure.catalogueelectronique]\n" +
                "Réduction du nombre de candidats (*) : [forms.procedure.isReduit]\tNombre maximum de candidats : [forms.procedure.nbcandidatsmax]\n" +
                "\t\n" +
                "Possibilité d’attribution sans négociation  (Attribution sur la base de l’offre initiale) (*) : [forms.procedure.isAttribSansnEgo]\n" +
                "L’acheteur exige la présentation de variantes (*) :Non\n" +
                "Identification des catégories d’acheteurs intervenant (Si accord-cadre) :\n" +
                "\n" +
                "\n" +
                "\n" +
                "Critères d’attribution (obligatoire si SAD) :\n" +
                "[accriterion] : [acweighting]\n" +
                "\n" +
                "\n" +
                "Section 4\n" +
                "Identification du marché\n" +
                "\n" +
                "Intitulé du marché (*) : [forms.objectcontract.title.p.value]\tCode CPV Principal : [forms.objectcontract.cpvMain.cpvcode.code]\n" +
                "Type de marché (*) : [forms.objectcontract.typeContratLibelle]\t\t\n" +
                "Description succincte du marché : [forms.objectcontract.shortDescr.paragraphs]\n" +
                "Lieu principal d’exécution du marché (*) : [forms.objectcontract.lieuExecution]\tDurée du marché (en mois) : [forms.objectcontract.duration.value]\n" +
                "Valeur estimée du besoin (en euros) : [forms.objectcontract.valObject.formatedValue] [forms.objectcontract.valObject.currency]\t(Si accord-cadre ou SAD, indiquer la valeur maximale)\n" +
                "\n" +
                "\n" +
                "La consultation comporte des tranches (*) : [forms.objectcontract.isTranche]\n" +
                "La consultation pévoit une réservation de tout ou partie du marché (si marché alloti, préciser pour chaque lot dans la description)(*) : [forms.objectcontract.isMarcheReserve]\n" +
                "\n" +
                "\n" +
                "\n" +
                "Type de marché réservé :\n" +
                "[value]\n" +
                "\n" +
                "\n" +
                "Section 5\n" +
                "Lots\n" +
                "\n" +
                "Marché alloti (*) : [forms.objectcontract.isLotDivision]\n" +
                "\n" +
                "           \n" +
                "Description  du lot no [item] : [title.p.value]\n" +
                "- CPV du lot n°[item] : [cpvCodes]\n" +
                "- Estimation de la valeur hors taxes du lot  no [item] : [valObject.formatedValue] [valObject.currency]\n" +
                "- Critères d’attributions :\n" +
                "[accriterion] : [acweighting]\n" +
                "- Type de marché réservé :\n" +
                "[value]\n" +
                "\n" +
                "\n" +
                "Section 6\n" +
                "Informations complémentaires\n" +
                "\n" +
                "Visite obligatoire (*) :\t[forms.procedure.isVisiteobligatoire]\tDétails sur la visite (si oui) : [forms.procedure.visiteobligatoire]\n" +
                "Autres informations complémentaires :\n" +
                "\n" +
                "(*) Les champs signalés par un astérisque doivent obligatoirement être renseignés\n" +
                "1/3\n", ex.getText());

    }

    public boolean isPDF(InputStream file) {
        Scanner input = new Scanner(file);
        while (input.hasNextLine()) {
            final String checkline = input.nextLine();
            if (checkline.contains("%PDF-")) {
                // a match!
                return true;
            }
        }
        return false;
    }
}
