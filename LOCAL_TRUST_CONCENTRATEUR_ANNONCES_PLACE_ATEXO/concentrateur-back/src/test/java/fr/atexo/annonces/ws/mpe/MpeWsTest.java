package fr.atexo.annonces.ws.mpe;


import com.atexo.annonces.commun.mpe.*;
import fr.atexo.annonces.boot.ws.mpe.MpeWs;
import fr.atexo.annonces.config.AuthenitificationRequest;
import fr.atexo.annonces.config.MpeToken;
import fr.atexo.annonces.config.RestTemplateConfig;
import fr.atexo.annonces.config.WsResource;
import fr.atexo.annonces.service.exception.AtexoException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;

import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@Disabled
class MpeWsTest extends WsResource {

    private final Integer idConsultationAllotie = 500762;
    private final Integer idConsultationNonAllotie = 508128;
    private final MpeWs mpeWs = new MpeWs(new RestTemplateConfig().restTemplate());

    @Test
    void Given_IdConsultation_Then_ReturnConsultation() throws Exception {
        String urlMpe = "https://pmp-mpe-r1.tp.etat.lu";
        String login = "admin_t5y";
        String password = "admin_t5y";
        MpeToken token = this.getAuthentificationV2(urlMpe, login, password);
        ConsultationMpe result = mpeWs.getConsultation(510586, urlMpe, token.getToken());
        //510655 => à valider
        assertJsonEqual("getConsultation", result);
    }


    @Test
    void Given_IdConsultation_Then_ReturnDonneesComplementaires() throws Exception {
        String urlMpe = "https://pmp-mpe-r1.tp.etat.lu";
        String login = "admin_t5y";
        String password = "admin_t5y";
        MpeToken token = this.getAuthentificationV2(urlMpe, login, password);
        DonneesComplementaires result = mpeWs.getDonneComplementaire(510586, urlMpe, token.getToken());
        assertJsonEqual("getDonneComplementaire", result);
    }

    @Test
    void Given_IdConsultationInvalid_Then_ThrowEception() {
        String urlMpe = "https://mpe-release.local-trust.com";
        String login = "mpe_tncp_int";
        String password = "mpe_tncp_int";
        MpeToken token = this.getAuthentificationV2(urlMpe, login, password);
        AtexoException exception = assertThrows(AtexoException.class, () -> mpeWs.getConsultation(0, urlMpe, token.getToken()));
        assertEquals("{\"type\":\"https:\\/\\/tools.ietf.org\\/html\\/rfc2616#section-10\",\"title\":\"An error occurred\",\"detail\":\"Not Found\"}", exception.getMessage());
    }

    @Test
    void Given_IdAgent_When_getAgentCreateur_Then_ReturnAgent() throws Exception {
        String urlMpe = "https://mpe-release.local-trust.com";
        String login = "mpe_tncp_int";
        String password = "mpe_tncp_int";
        MpeToken token = this.getAuthentificationV2(urlMpe, login, password);
        AgentMpe result = mpeWs.getAgent(24, urlMpe, token.getToken());
        assertJsonEqual("getAgent", result);
    }


    @Test
    void Given_IdAgentInvalid_When_getAgentCreateur_Then_ThrowEception() {
        String urlMpe = "https://mpe-release.local-trust.com";
        String login = "mpe_tncp_int";
        String password = "mpe_tncp_int";
        MpeToken token = this.getAuthentificationV2(urlMpe, login, password);

        AtexoException exception = assertThrows(AtexoException.class, () -> {
            mpeWs.getAgent(999999999, "http://mpe-release.local-trust.com", token.getToken());
        });
        assertEquals("{\"type\":\"https:\\/\\/tools.ietf.org\\/html\\/rfc2616#section-10\",\"title\":\"An error occurred\",\"detail\":\"Not Found\"}", exception.getMessage());
    }

    @Test
    void Given_IdService_When_getService_Then_ReturnService() throws Exception {
        String urlMpe = "https://mpe-release.local-trust.com";
        String login = "mpe_tncp_int";
        String password = "mpe_tncp_int";
        MpeToken token = this.getAuthentificationV2(urlMpe, login, password);
        ServiceMpe result = mpeWs.getService("/api/v2/referentiels/services/2921", urlMpe, token.getToken());
        assertNotNull(result);
        assertJsonEqual("getService", result);
    }

    @Test
    void Given_IdServiceInvalid_When_getService_Then_ReturnNull() {
        String urlMpe = "https://mpe-release.local-trust.com";
        String login = "mpe_tncp_int";
        String password = "mpe_tncp_int";
        MpeToken token = this.getAuthentificationV2(urlMpe, login, password);
        ServiceMpe result = mpeWs.getService("/api/v2/referentiels/services/9999999999999999999999999", urlMpe, token.getToken());
        assertNull(result);
    }

    @Test
    void Given_IdConsultation_When_getListeLots_Then_ReturnListe() throws Exception {
        String urlMpe = "https://mpe-release.local-trust.com";
        String login = "mpe_tncp_int";
        String password = "mpe_tncp_int";
        MpeToken token = this.getAuthentificationV2(urlMpe, login, password);
        //passer id d'une consultation allotie
        List<LotMpe> result = mpeWs.getListeLots(idConsultationAllotie, urlMpe, token.getToken());
        assertJsonEqual("getLots", result);
    }

    @Test
    void Given_IdConsultationInvalid_When_getLots_Then_ReturnEmptyList() {
        String urlMpe = "https://mpe-release.local-trust.com";
        String login = "mpe_tncp_int";
        String password = "mpe_tncp_int";
        MpeToken token = this.getAuthentificationV2(urlMpe, login, password);
        List<LotMpe> result = mpeWs.getListeLots(0, urlMpe, token.getToken());
        assertThat(result).isEmpty();
    }

    @Test
    void Given_IdLot_When_getLot_Then_ReturnLot() throws Exception {
        String urlMpe = "https://mpe-release.local-trust.com";
        String login = "mpe_tncp_int";
        String password = "mpe_tncp_int";
        MpeToken token = this.getAuthentificationV2(urlMpe, login, password);
        List<LotMpe> lots = mpeWs.getListeLots(idConsultationAllotie, urlMpe, token.getToken());
        LotMpe result = mpeWs.getLot(lots.get(0).getHydraId(), urlMpe, token.getToken());
        assertNotNull(result);
        assertJsonEqual("getLot", result);
    }

    @Test
    void Given_IdLotInvalid_When_getLot_Then_ReturnNull() {
        String urlMpe = "https://mpe-release.local-trust.com";
        String login = "mpe_tncp_int";
        String password = "mpe_tncp_int";
        MpeToken token = this.getAuthentificationV2(urlMpe, login, password);
        LotMpe result = mpeWs.getLot("/api/v2/lots/9999999999999999999999999", urlMpe, token.getToken());
        assertNull(result);
    }

    @Test
    void Given_Organisme_When_getAcheteurs_Then_ReturnAcheteurs() throws Exception {
        String urlMpe = "https://mpe-release.local-trust.com";
        String login = "mpe_tncp_int";
        String password = "mpe_tncp_int";
        MpeToken token = this.getAuthentificationV2(urlMpe, login, password);

        List<AcheteurMpe> result = mpeWs.getAcheteurs(2921L, "pmi-min-1", urlMpe, token.getToken());
        assertNotNull(result);
        assertFalse(result.isEmpty());
    }

    @Test
    void Given_Organisme_When_getComptesJoues_Then_ReturnComptesJoues() throws Exception {
        String urlMpe = "https://simap-mpe-int.local-trust.com";
        String login = "admin_t5y";
        String password = "admin_t5y";
        MpeToken token = this.getAuthentificationV2(urlMpe, login, password);
        List<ProfilJoueMpe> result = mpeWs.getComptesJoues("6", "t5y", urlMpe, token.getToken());
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertJsonEqual("getComptesJoues", result);
    }


    private MpeToken getAuthentificationV2(String urlMpe, String login, String password) {
        String url = urlMpe + "/api/v2/token";

        AuthenitificationRequest request = AuthenitificationRequest.builder().login(login).password(password).build();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");

        return new RestTemplateConfig().restTemplate().postForEntity(url,
                new HttpEntity<>(request, headers), MpeToken.class).getBody();

    }


}
