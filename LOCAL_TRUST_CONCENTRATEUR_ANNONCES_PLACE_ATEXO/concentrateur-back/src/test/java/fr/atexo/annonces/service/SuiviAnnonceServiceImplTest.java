package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.IEFormsNoticeServices;
import fr.atexo.annonces.boot.entity.Offre;
import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.boot.entity.Support;
import fr.atexo.annonces.boot.repository.*;
import fr.atexo.annonces.boot.ws.docgen.DocGenWsPort;
import fr.atexo.annonces.boot.ws.jal.LesechosWS;
import fr.atexo.annonces.boot.ws.mpe.MpeWs;
import fr.atexo.annonces.boot.ws.ressources.RessourcesWsPort;
import fr.atexo.annonces.boot.ws.viewer.ViewerWS;
import fr.atexo.annonces.builder.OffreBuilder;
import fr.atexo.annonces.builder.SuiviAnnonceBuilder;
import fr.atexo.annonces.builder.SupportBuilder;
import fr.atexo.annonces.dto.OffreDTO;
import fr.atexo.annonces.dto.SuiviAnnonceDTO;
import fr.atexo.annonces.mapper.*;
import fr.atexo.annonces.modeles.StatutSuiviAnnonceEnum;
import fr.atexo.annonces.service.exception.*;
import org.apache.commons.net.util.Base64;
import org.junit.Ignore;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * Tests pour la couche service des demandes de publication et de leur suivi
 */
@ExtendWith(MockitoExtension.class)
@Disabled
class SuiviAnnonceServiceImplTest {

    @Mock
    private SuiviAnnonceRepository repository;
    @Mock
    private EFormsFormulaireRepository eFormsFormulaireRepository;
    @Mock
    private SuiviTypeAvisPubRepository consultationFacturationRepository;
    @Mock
    private SuiviTypeAvisPubMapper consultationFacturationMapper;
    @Mock
    private SuiviTypeAvisPubService suiviTypeAvisPubService;

    @Mock
    private RessourcesWsPort ressourcesWsPortMock;
    @Mock
    private DocGenWsPort docGenWsPortMock;
    @Mock
    private ViewerWS viewerWSMock;
    @Mock
    private SupportIntermediaireRepository supportIntermediaireRepositoryMock;
    @Mock
    private IEFormsNoticeServices ieFormsNoticeServices;
    @Mock
    private OffreRepository offreRepository;
    @Mock
    private SupportRepository supportRepository;
    @Mock
    private PlateformeConfigurationRepository plateformeConfigurationRepository;

    @Mock
    private SuiviAnnonceMapper suiviAnnonceMapper;

    @Mock
    private EchangeMapper echangeMapper;

    @Mock
    private SerialisationService serialisationService;
    @Mock
    private SuiviAnnonceRepository suiviAnnonceRepository;
    @Mock
    private ReferentielMpeService referentielMpeService;
    @Mock
    private AcheteurRepository acheteurRepository;
    @Mock
    private AcheteurMapper acheteurMapper;
    @Mock
    private MolService molService;
    @Mock
    private TNCPService tncpService;
    @Mock
    private OffreTypeProcedureRepository offreTypeProcedureRepository;
    @Mock
    private OrganismeService organismeService;
    @Mock
    private TypeProcedureRepository typeProcedureRepository;
    @Mock
    private AgentService agentService;
    @Mock
    private MpeWs mpeWs;
    @Mock
    private PieceJointeService pieceJointeService;
    @Mock
    private EspaceDocumentaireService espaceDocumentaireService;
    @Mock
    private OffreMapper offreMapper;
    @Mock
    private LesechosWS lesechosWS;
    @Mock
    private AgentConsultationSessionRepository agentConsultationSessionRepository;
    @Mock
    private SuiviAvisPublieMapper suiviAvisPublieMapper;

    @Captor
    private ArgumentCaptor<SuiviAnnonce> suiviAnnonceCaptor;

    @InjectMocks
    private SuiviAnnonceServiceImpl suiviAnnonceService;

    @Test
    void getSuiviAnnonce_givenExistingIdConsultationCodeOffre_shouldReturnSuiviAnnonceDTO() {
        String idPlatform = "PLATEFORME_TEST";
        SuiviAnnonce suiviAnnonce = new SuiviAnnonce();
        SuiviAnnonceDTO suiviAnnonceDTO = new SuiviAnnonceDTO();
        List<SuiviAnnonce> optionalSuiviAnnonce = List.of(suiviAnnonce);
        Integer idConsultation = 1;
        String codeSupport = "TEST";
        String codeOffre = "TEST";
        boolean includeSupport = false;
        lenient().when(repository.findByIdPlatformAndIdConsultationAndSupportCodeAndOffreCode(anyString(), anyInt(), anyString(), anyString())).thenReturn(optionalSuiviAnnonce);
        when(suiviAnnonceMapper.suiviAnnonceToSuiviAnnonceDTO(any(), anyBoolean())).thenReturn(suiviAnnonceDTO);

        SuiviAnnonceDTO result = suiviAnnonceService.getSuiviAnnonce(idPlatform, idConsultation, codeSupport, codeOffre, includeSupport);

        assertThat(result).isEqualTo(suiviAnnonceDTO);
    }

    @Test
    void getSuiviAnnonce_givenNonExistingIdConsultationCodeOffre_shouldThrowException() {
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        String codeSupport = "TEST";
        String codeOffre = "TEST";
        boolean includeSupport = false;
        lenient().when(repository.findByIdPlatformAndIdConsultationAndSupportCodeAndOffreCode(anyString(), anyInt(), anyString(), anyString())).thenReturn(List.of());

        assertThatExceptionOfType(ResourceNotFoundException.class).isThrownBy(() -> suiviAnnonceService.getSuiviAnnonce(idPlatform, idConsultation, codeSupport, codeOffre, includeSupport)).withMessageContainingAll("PLATEFORME_TEST");
    }

    @Test
    void getSuiviAnnonces_shouldReturnSuiviAnnonceDTOs() {
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        boolean includeSupport = false;
        SuiviAnnonce suiviAnnonce1 = new SuiviAnnonce();
        SuiviAnnonce suiviAnnonce2 = new SuiviAnnonce();
        List<SuiviAnnonce> suiviAnnonces1 = Arrays.asList(suiviAnnonce1, suiviAnnonce2);
        SuiviAnnonceDTO suiviAnnonceDTO1 = new SuiviAnnonceDTO();
        SuiviAnnonceDTO suiviAnnonceDTO2 = new SuiviAnnonceDTO();
        List<SuiviAnnonceDTO> suiviAnnonceDTOS = Arrays.asList(suiviAnnonceDTO1, suiviAnnonceDTO2);
        lenient().when(suiviAnnonceMapper.suiviAnnoncesToSuiviAnnonceDTOs(any(), anyBoolean())).thenReturn(suiviAnnonceDTOS);
        List<SuiviAnnonceDTO> result = suiviAnnonceService.getSuiviAnnonces(idPlatform, idConsultation, includeSupport);
        assertThat(result).hasSize(2);
    }




    @Test
    void getSuiviAnnonces_givenEmptySearchResult_shouldReturnEmptyList() {
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        boolean includeSupport = false;
        List<SuiviAnnonce> suiviAnnonces = Collections.emptyList();
        List<SuiviAnnonceDTO> suiviAnnonceDTOS = Collections.emptyList();
        List<SuiviAnnonceDTO> result = suiviAnnonceService.getSuiviAnnonces(idPlatform, idConsultation, includeSupport);
        assertThat(result).isEqualTo(suiviAnnonceDTOS);
    }

    @Test
    void createOrUpdateAnnonce_givenIllegalStatut_shouldThrowException() {
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        String codeSupport = "TEST";
        String codeOffre = "TEST";
        SuiviAnnonceDTO suiviAnnonceDTO = new SuiviAnnonceDTO();
        suiviAnnonceDTO.setStatut(StatutSuiviAnnonceEnum.PUBLIER);

        assertThatExceptionOfType(IllegalOperationException.class).isThrownBy(() -> suiviAnnonceService.createOrUpdateSuiviAnnonce(idPlatform, idConsultation, codeSupport, codeOffre, suiviAnnonceDTO, "token")).withMessageContaining(StatutSuiviAnnonceEnum.PUBLIER.name());
    }

    @Test
    void createOrUpdateAnnonce_givenNullOffreArgument_shouldThrowException() {
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        String codeSupport = "TEST";
        String codeOffre = "TEST";
        SuiviAnnonceDTO suiviAnnonceDTO = new SuiviAnnonceDTO();
        suiviAnnonceDTO.setStatut(StatutSuiviAnnonceEnum.BROUILLON);
        when(supportRepository.findByCode(Mockito.anyString())).thenReturn(new Support());

        assertThatExceptionOfType(MissingArgumentException.class).isThrownBy(() -> suiviAnnonceService.createOrUpdateSuiviAnnonce(idPlatform, idConsultation, codeSupport, codeOffre, suiviAnnonceDTO, "token")).withMessageContaining("offre");
    }

    @Test
    @Ignore
    void createOrUpdateAnnonce_givenNonExistingOffre_shouldThrowException() {
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        String codeSupport = "TEST";
        String codeOffre = "TEST";
        SuiviAnnonceDTO suiviAnnonceDTO = new SuiviAnnonceDTO();
        suiviAnnonceDTO.setStatut(StatutSuiviAnnonceEnum.BROUILLON);
        suiviAnnonceDTO.setOffre(new OffreDTO());
        suiviAnnonceDTO.getOffre().setCode("TEST_OFFRE");
        when(offreRepository.findByCode(suiviAnnonceDTO.getOffre().getCode())).thenReturn(Optional.empty());
        when(supportRepository.findByCode(Mockito.anyString())).thenReturn(new Support());

        assertThatExceptionOfType(ResourceNotFoundException.class).isThrownBy(() -> suiviAnnonceService.createOrUpdateSuiviAnnonce(idPlatform, idConsultation, codeSupport, codeOffre, suiviAnnonceDTO, "token")).withMessageContaining(suiviAnnonceDTO.getOffre().getCode());
    }

    @Test
    @Ignore
    void createOrUpdateAnnonce_givenNonLinkedOffreSupport_shouldThrowException() {
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        String codeSupport = "TEST_SUPPORT";
        String codeOffre = "TEST";
        SuiviAnnonceDTO suiviAnnonceDTO = new SuiviAnnonceDTO();
        suiviAnnonceDTO.setStatut(StatutSuiviAnnonceEnum.BROUILLON);
        suiviAnnonceDTO.setOffre(new OffreDTO());
        suiviAnnonceDTO.getOffre().setCode("OFFRE_TEST");
        Optional<Offre> optionalOffre = Optional.of(OffreBuilder.anOffre().withDefaults().withCode("OFFRE_TEST").withSupport(SupportBuilder.aSupport().withDefaults().withCode("CODE SUPPORT DIFFERENT").build()).build());
        when(offreRepository.findByCode(suiviAnnonceDTO.getOffre().getCode())).thenReturn(optionalOffre);
        when(supportRepository.findByCode(Mockito.anyString())).thenReturn(new Support());

        assertThatExceptionOfType(InconsistentArgumentsException.class).isThrownBy(() -> suiviAnnonceService.createOrUpdateSuiviAnnonce(idPlatform, idConsultation, codeSupport, codeOffre, suiviAnnonceDTO, "token")).withMessageContainingAll(codeSupport, suiviAnnonceDTO.getOffre().getCode());
    }

    @Test
    void createOrUpdateAnnonce_givenInvalidStatut_shouldThrowException() {
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        String codeSupport = "TEST_SUPPORT";
        String codeOffre = "TEST";
        SuiviAnnonceDTO suiviAnnonceDTO = new SuiviAnnonceDTO();
        suiviAnnonceDTO.setStatut(StatutSuiviAnnonceEnum.ENVOI_PLANIFIER);
        suiviAnnonceDTO.setOffre(new OffreDTO());
        suiviAnnonceDTO.getOffre().setCode("OFFRE_TEST");
        Optional<Offre> optionalOffre = Optional.of(OffreBuilder.anOffre().withDefaults().withCode("OFFRE_TEST").withSupport(SupportBuilder.aSupport().withDefaults().withCode("TEST_SUPPORT").withExternal(true).build()).build());
        assertThatExceptionOfType(IllegalOperationException.class).isThrownBy(() -> {
            CreateUpdateAnnonceResult token = suiviAnnonceService.createOrUpdateSuiviAnnonce(idPlatform, idConsultation, codeSupport, codeOffre, suiviAnnonceDTO, "token");

        }).withMessageContainingAll(StatutSuiviAnnonceEnum.ENVOI_PLANIFIER.name());
    }

    @Test
    void createOrUpdateAnnonce_givenExternalStatutOnNonExternalSupport_shouldThrowException() {
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        String codeSupport = "TEST_SUPPORT";
        String codeOffre = "TEST";
        SuiviAnnonceDTO suiviAnnonceDTO = new SuiviAnnonceDTO();
        suiviAnnonceDTO.setStatut(StatutSuiviAnnonceEnum.EXTERNAL);
        suiviAnnonceDTO.setOffre(new OffreDTO());
        suiviAnnonceDTO.getOffre().setCode("OFFRE_TEST");
        Optional<Offre> optionalOffre = Optional.of(OffreBuilder.anOffre().withDefaults().withCode("OFFRE_TEST").withSupport(SupportBuilder.aSupport().withDefaults().withCode("TEST_SUPPORT").build()).build());
        when(offreRepository.findByCode(suiviAnnonceDTO.getOffre().getCode())).thenReturn(optionalOffre);
        when(supportRepository.findByCode(Mockito.anyString())).thenReturn(new Support());

        assertThatExceptionOfType(InconsistentArgumentsException.class).isThrownBy(() -> suiviAnnonceService.createOrUpdateSuiviAnnonce(idPlatform, idConsultation, codeSupport, codeOffre, suiviAnnonceDTO, "token")).withMessageContainingAll(codeSupport, StatutSuiviAnnonceEnum.EXTERNAL.name());
    }

    @Test
    void createOrUpdateAnnonce_givenNonExistingSuiviAnnonce_shouldReturnCreatedSuiviAnnonceDTO() {
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        String codeSupport = "TEST_SUPPORT";
        String codeOffre = "TEST";
        SuiviAnnonceDTO suiviAnnonceDTO = new SuiviAnnonceDTO();
        suiviAnnonceDTO.setStatut(StatutSuiviAnnonceEnum.BROUILLON);
        suiviAnnonceDTO.setOffre(new OffreDTO());
        suiviAnnonceDTO.getOffre().setCode("OFFRE_TEST");
        SuiviAnnonceDTO suiviAnnonceDTO2 = new SuiviAnnonceDTO();
        suiviAnnonceDTO2.setStatut(StatutSuiviAnnonceEnum.BROUILLON);
        suiviAnnonceDTO2.setOffre(new OffreDTO());
        suiviAnnonceDTO2.getOffre().setCode("OFFRE_TEST");
        Optional<Offre> optionalOffre = Optional.of(OffreBuilder.anOffre().withDefaults().withCode("OFFRE_TEST").withSupport(SupportBuilder.aSupport().withDefaults().withCode("TEST_SUPPORT").build()).build());
        lenient().when(offreRepository.findByCode(suiviAnnonceDTO.getOffre().getCode())).thenReturn(optionalOffre);
        lenient().when(repository.findByIdPlatformAndIdConsultationAndSupportCodeAndOffreCode(anyString(), anyInt(), anyString(), anyString())).thenReturn(List.of());
        lenient().when(repository.save(any(SuiviAnnonce.class))).then(returnsFirstArg());
        lenient().when(suiviAnnonceMapper.suiviAnnonceToSuiviAnnonceDTOIgnoreSupport(any())).thenReturn(suiviAnnonceDTO2);
        lenient().when(supportRepository.findByCode(Mockito.anyString())).thenReturn(new Support());
        CreateUpdateResult<SuiviAnnonceDTO> result = suiviAnnonceService.createOrUpdateSuiviAnnonce(idPlatform, idConsultation, codeSupport, codeOffre, suiviAnnonceDTO, "token");
        assertThat(result.isCreated()).isTrue();
        assertThat(result.getDtoObject().getStatut()).isEqualTo(StatutSuiviAnnonceEnum.BROUILLON);

    }

    @Test
    void createOrUpdateAnnonce_givenExistingSuiviAnnonceEnAttenteStatut_shouldThrowException() {
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        String codeSupport = "TEST_SUPPORT";
        String codeOffre = "TEST";
        SuiviAnnonceDTO suiviAnnonceDTO = new SuiviAnnonceDTO();
        suiviAnnonceDTO.setStatut(StatutSuiviAnnonceEnum.BROUILLON);
        suiviAnnonceDTO.setOffre(new OffreDTO());
        suiviAnnonceDTO.getOffre().setCode("OFFRE_TEST");
        Optional<Offre> optionalOffre = Optional.of(OffreBuilder.anOffre().withDefaults().withCode("OFFRE_TEST").withSupport(SupportBuilder.aSupport().withDefaults().withCode("TEST_SUPPORT").build()).build());
        when(offreRepository.findByCode(anyString())).thenReturn(optionalOffre);
        SuiviAnnonce suiviAnnonce = SuiviAnnonceBuilder.aSuiviAnnonce().withDefaults().withStatut(StatutSuiviAnnonceEnum.EN_ATTENTE).build();
        when(repository.findByIdPlatformAndIdConsultationAndSupportCodeAndOffreCode(anyString(), anyInt(), anyString(), anyString())).thenReturn(List.of(suiviAnnonce));
        when(supportRepository.findByCode(Mockito.anyString())).thenReturn(new Support());

        assertThatExceptionOfType(ConflictingResourceException.class).isThrownBy(() -> suiviAnnonceService.createOrUpdateSuiviAnnonce(idPlatform, idConsultation, codeSupport, codeOffre, suiviAnnonceDTO, "token")).withMessageContainingAll(StatutSuiviAnnonceEnum.EN_ATTENTE.name());
    }

    @Test
    void createOrUpdateAnnonce_givenExistingSuiviAnnonceBrouillonStatut_shouldReturnUpdatedSuiviAnnonceDTO() {
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        String codeSupport = "TEST_SUPPORT";
        String codeOffre = "TEST";
        SuiviAnnonceDTO suiviAnnonceDTO = new SuiviAnnonceDTO();
        suiviAnnonceDTO.setStatut(StatutSuiviAnnonceEnum.EN_ATTENTE);
        suiviAnnonceDTO.setOffre(new OffreDTO());
        suiviAnnonceDTO.getOffre().setCode("OFFRE_TEST");
        Optional<Offre> optionalOffre = Optional.of(OffreBuilder.anOffre().withDefaults().withCode("OFFRE_TEST").withSupport(SupportBuilder.aSupport().withDefaults().withCode("TEST_SUPPORT").build()).build());
        when(offreRepository.findByCode(anyString())).thenReturn(optionalOffre);
        SuiviAnnonce suiviAnnonce = SuiviAnnonceBuilder.aSuiviAnnonce().withDefaults().withStatut(StatutSuiviAnnonceEnum.BROUILLON).build();
        when(repository.findByIdPlatformAndIdConsultationAndSupportCodeAndOffreCode(anyString(), anyInt(), anyString(), anyString())).thenReturn(List.of(suiviAnnonce));
        SuiviAnnonceDTO createdSuiviAnnonceDTO = new SuiviAnnonceDTO();
        when(repository.save(any())).then(returnsFirstArg());
        when(suiviAnnonceMapper.suiviAnnonceToSuiviAnnonceDTOIgnoreSupport(any())).thenReturn(createdSuiviAnnonceDTO);
        when(supportRepository.findByCode(Mockito.anyString())).thenReturn(new Support());

        CreateUpdateResult<SuiviAnnonceDTO> result = suiviAnnonceService.createOrUpdateSuiviAnnonce(idPlatform, idConsultation, codeSupport, codeOffre, suiviAnnonceDTO, "token");

        assertThat(result.isCreated()).isFalse();
        assertThat(result.getDtoObject()).isEqualTo(createdSuiviAnnonceDTO);
        verify(suiviAnnonceMapper).suiviAnnonceToSuiviAnnonceDTOIgnoreSupport(suiviAnnonceCaptor.capture());
        assertThat(suiviAnnonceCaptor.getValue().getDateDemandeEnvoi()).isNotNull();
    }


    @Test
    void deleteSuiviAnnonce_givenExistingIdConsultationCodeOffre_shouldReturnSuiviAnnonceDTO() {
        String idPlatform = "PLATEFORME_TEST";
        SuiviAnnonce suiviAnnonce = SuiviAnnonceBuilder.aSuiviAnnonce().withDefaults().build();
        SuiviAnnonceDTO suiviAnnonceDTO = new SuiviAnnonceDTO();
        Optional<SuiviAnnonce> optionalSuiviAnnonce = Optional.of(suiviAnnonce);
        Integer idConsultation = 1;
        Integer id = 1;
        lenient().when(repository.findById(anyInt())).thenReturn(optionalSuiviAnnonce);
        boolean result = suiviAnnonceService.deleteSuiviAnnonce(id, idPlatform, idConsultation);
        assertThat(result).isEqualTo(true);
    }

    @Test
    void deleteSuiviAnnonce_givenNonBrouillonAnnonce_shouldThrowException() {
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        Integer id = 1;
        SuiviAnnonce suiviAnnonce = SuiviAnnonceBuilder.aSuiviAnnonce().withDefaults().withStatut(StatutSuiviAnnonceEnum.EN_ATTENTE).build();
        Optional<SuiviAnnonce> optionalSuiviAnnonce = Optional.of(suiviAnnonce);
        lenient().when(repository.findById(id)).thenReturn(optionalSuiviAnnonce);

        assertThatExceptionOfType(ConflictingResourceException.class).isThrownBy(() -> suiviAnnonceService.deleteSuiviAnnonce(id, idPlatform, idConsultation)).withMessageContaining("Impossible de supprimer l'annonce ; elle est à l'état " + StatutSuiviAnnonceEnum.EN_ATTENTE.name());
    }

    @Test
    void deleteSuiviAnnonce_givenNonExistingIdConsultationCodeOffre_shouldThrowException() {
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        Integer id = 1;

        lenient().when(repository.findById(id)).thenReturn(Optional.empty());

        assertThatExceptionOfType(ResourceNotFoundException.class).isThrownBy(() -> suiviAnnonceService.deleteSuiviAnnonce(id, idPlatform, idConsultation)).withMessageContainingAll(idConsultation.toString(), id + "");
    }

    @Test
    void updateSuiviAnnonces_givenIllegalStatut_shouldThrowException() {
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        SuiviAnnonceDTO suiviAnnonceDTO = new SuiviAnnonceDTO();
        suiviAnnonceDTO.setStatut(StatutSuiviAnnonceEnum.PUBLIER);

        assertThatExceptionOfType(IllegalOperationException.class).isThrownBy(() -> suiviAnnonceService.updateSuiviAnnonces(idPlatform, idConsultation, suiviAnnonceDTO)).withMessageContaining(StatutSuiviAnnonceEnum.PUBLIER.name());
    }

    @Test
    void updateSuiviAnnonces_givenEmptySearchResult_shouldThrowException() {
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        SuiviAnnonceDTO suiviAnnonceDTO = new SuiviAnnonceDTO();
        suiviAnnonceDTO.setStatut(StatutSuiviAnnonceEnum.BROUILLON);
        lenient().when(repository.findAllByIdPlatformAndIdConsultation(anyString(), anyInt())).thenReturn(Collections.emptyList());

        assertThatExceptionOfType(ResourceNotFoundException.class).isThrownBy(() -> suiviAnnonceService.updateSuiviAnnonces(idPlatform, idConsultation, suiviAnnonceDTO)).withMessageContaining(idConsultation.toString());
    }

    @Test
    void updateSuiviAnnonces_shouldReturnUpdatedSuiviAnnonceDTOs() {
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        SuiviAnnonceDTO suiviAnnonceDTO = new SuiviAnnonceDTO();
        suiviAnnonceDTO.setStatut(StatutSuiviAnnonceEnum.EN_ATTENTE);
        List<SuiviAnnonce> suiviAnnonces = Arrays.asList(SuiviAnnonceBuilder.aSuiviAnnonce().withDefaults().build(), SuiviAnnonceBuilder.aSuiviAnnonce().withDefaults().withStatut(StatutSuiviAnnonceEnum.PUBLIER).build());
        lenient().when(repository.findAllByIdPlatformAndIdConsultation(idPlatform, idConsultation)).thenReturn(suiviAnnonces);
        lenient().when(repository.save(any(SuiviAnnonce.class))).then(returnsFirstArg());
        lenient().when(suiviAnnonceMapper.suiviAnnonceToSuiviAnnonceDTOIgnoreSupport(any(SuiviAnnonce.class))).thenReturn(new SuiviAnnonceDTO());

        List<SuiviAnnonceDTO> result = suiviAnnonceService.updateSuiviAnnonces(idPlatform, idConsultation, suiviAnnonceDTO);

        assertThat(result).hasSize(1);
    }

    @Test
    void toXml_shouldReturnXmlAttribute() {
        String xml = "TEST XML";
        SuiviAnnonce suiviAnnonceDTO = new SuiviAnnonce();
        suiviAnnonceDTO.setXml(Base64.encodeBase64StringUnChunked(xml.getBytes()));

        String result = suiviAnnonceService.toXml(suiviAnnonceDTO);

        assertThat(result).isEqualTo(xml);
    }

    @Test
    void toXml_shouldReturnXmlAttributeWithOffre() {
        String xml = "TEST XML";
        SuiviAnnonce suiviAnnonceDTO = new SuiviAnnonce();
        suiviAnnonceDTO.setXml(Base64.encodeBase64StringUnChunked(xml.getBytes()));
        Offre offre = new Offre();
        offre.setCode("Code");
        offre.setLibelle("Libellé");
        suiviAnnonceDTO.setOffre(offre);

        String result = suiviAnnonceService.toXml(suiviAnnonceDTO);

        assertThat(result).isEqualTo(xml);
    }

    @Test
    void toXml_givenEmptyXmlInDto_shouldThrowException() {
        Integer idConsultation = 1;
        String codeSupport = "TEST";
        String xml = "";
        SuiviAnnonce suiviAnnonceDTO = new SuiviAnnonce();
        Support supportDTO = new Support();
        supportDTO.setCode(codeSupport);
        Offre offreDTO = new Offre();
        suiviAnnonceDTO.setSupport(supportDTO);
        suiviAnnonceDTO.setOffre(offreDTO);
        suiviAnnonceDTO.setXml(xml);
        suiviAnnonceDTO.setIdConsultation(idConsultation);

        assertThatExceptionOfType(MissingArgumentException.class).isThrownBy(() -> suiviAnnonceService.toXml(suiviAnnonceDTO)).withMessageContainingAll(idConsultation.toString(), codeSupport);
    }


    @Test
    void updateXmlTransform_givenNonExistingIdAnnonce_shouldThrowException() {
        Integer idAnnonce = 1;
        when(repository.findById(idAnnonce)).thenReturn(Optional.empty());

        assertThatExceptionOfType(ResourceNotFoundException.class).isThrownBy(() -> suiviAnnonceService.updateXmlTransform(idAnnonce, null)).withMessageContaining(idAnnonce.toString());
    }

    @Test
    void updateXmlTransform_givenExistingIdAnnonce_shouldUpdateXmlTransformAndReturnXml() {
        Integer idAnnonce = 1;
        String xml = "TEST XML UPDATE";
        SuiviAnnonce suiviAnnonce = SuiviAnnonceBuilder.aSuiviAnnonce().withDefaults().build();
        when(repository.findById(anyInt())).thenReturn(Optional.of(suiviAnnonce));

        String returnedXml = suiviAnnonceService.updateXmlTransform(idAnnonce, xml);

        assertThat(returnedXml).isEqualTo(xml);
        verify(repository).save(suiviAnnonceCaptor.capture());
        assertThat(suiviAnnonceCaptor.getValue().getXmlTransform()).isEqualTo(Base64.encodeBase64StringUnChunked(xml.getBytes()));
    }

    @Test
    void updateErrorMessage_givenNonExistingIdAnnonce_shouldReturn() {
        Integer idAnnonce = 1;
        when(repository.findById(anyInt())).thenReturn(Optional.empty());

        suiviAnnonceService.updateErrorMessage(idAnnonce, null, false);

        verify(repository, never()).save(any());
    }

    @Test
    void updateErrorMessage_givenNotRejetConcentrateur_shouldUpdateMessageStatut() {
        Integer idAnnonce = 1;
        String message = "TEST ERROR MESSAGE";
        SuiviAnnonce suiviAnnonce = new SuiviAnnonce();
        suiviAnnonce.setStatut(StatutSuiviAnnonceEnum.EN_ATTENTE);
        lenient().when(repository.findById(anyInt())).thenReturn(Optional.of(suiviAnnonce));

        suiviAnnonceService.updateErrorMessage(idAnnonce, message, false);

        verify(repository).save(suiviAnnonceCaptor.capture());
        assertThat(suiviAnnonceCaptor.getValue()).extracting(fr.atexo.annonces.boot.entity.SuiviAnnonce::getMessageStatut, fr.atexo.annonces.boot.entity.SuiviAnnonce::getStatut).containsExactly(message, StatutSuiviAnnonceEnum.REJETER_SUPPORT);
        assertThat(suiviAnnonceCaptor.getValue().getDateDebutEnvoi()).isNotEqualTo(Instant.EPOCH);
    }

    @Test
    void updateErrorMessage_givenRejetConcentrateur_shouldUpdateMessageStatutAndStatut() {
        Integer idAnnonce = 1;
        String message = "TEST ERROR MESSAGE";
        SuiviAnnonce suiviAnnonce = SuiviAnnonceBuilder.aSuiviAnnonce().withDefaults().withStatut(StatutSuiviAnnonceEnum.EN_ATTENTE).build();
        lenient().when(repository.findById(anyInt())).thenReturn(Optional.of(suiviAnnonce));

        suiviAnnonceService.updateErrorMessage(idAnnonce, message, true);

        verify(repository).save(suiviAnnonceCaptor.capture());
        assertThat(suiviAnnonceCaptor.getValue()).extracting(fr.atexo.annonces.boot.entity.SuiviAnnonce::getMessageStatut, fr.atexo.annonces.boot.entity.SuiviAnnonce::getStatut).containsExactly(message, StatutSuiviAnnonceEnum.REJETER_CONCENTRATEUR);
        assertThat(suiviAnnonceCaptor.getValue().getDateDebutEnvoi()).isNotEqualTo(Instant.EPOCH);
    }


    @Test
    void getPdfLength() {

        String fileName = "src/test/resources/data/ted.pdf";
        File file = new File(fileName);
        double length = suiviAnnonceService.getPdfLength(file);
        assertThat(length).isNotZero();
    }

}
