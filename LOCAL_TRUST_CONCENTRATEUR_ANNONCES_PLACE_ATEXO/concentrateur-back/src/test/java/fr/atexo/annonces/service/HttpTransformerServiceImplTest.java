package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.reseau.HttpFileParameter;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.util.MultiValueMap;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

/**
 * Test des constructions des paramètres Http
 */
@ExtendWith(MockitoExtension.class)
@Disabled
class HttpTransformerServiceImplTest {

    @Mock
    private SuiviAnnonceService suiviAnnonceService;

    @InjectMocks
    private HttpTransformerServiceImpl httpTransformerService;

    @Test
    void buildLesEchosFileParameters_shouldReturnMultiValueMap() {
        String filename1 = "test1";
        String filename2 = "test2";
        HttpEntity<byte[]> file1 = new HttpEntity<>("test1file".getBytes());
        HttpEntity<byte[]> file2 = new HttpEntity<>("test2file".getBytes());
        List<HttpFileParameter> fileParameters = new ArrayList<>();
        fileParameters.add(new HttpFileParameter(filename1, file1));
        fileParameters.add(new HttpFileParameter(filename2, file2));

        MultiValueMap<String, Object> multiValueMap = httpTransformerService.buildLesEchosFileParameters(fileParameters);

        assertThat(multiValueMap).containsOnlyKeys(filename1, filename2).flatExtracting(filename1, filename2).containsExactly(file1, file2);
    }

    @Test
    void buildLesEchosXmlFileParameter_shouldReturnHttpFileParameter() {
        String xml = "XML TEST";

        HttpFileParameter xmlFileParameter = httpTransformerService.buildLesEchosXmlFileParameter(xml);

        assertThat(xmlFileParameter).extracting(HttpFileParameter::getName).isEqualTo(HttpTransformerServiceImpl.LES_ECHOS_XML_FILE_NAME);
        assertThat(xmlFileParameter).extracting(HttpFileParameter::getFile).extracting(HttpEntity::getBody).isEqualTo(xml.getBytes());
        assertThat(xmlFileParameter.getFile().getHeaders()).containsOnlyKeys(HttpHeaders.CONTENT_DISPOSITION, HttpHeaders.CONTENT_TYPE);
        assertThat(xmlFileParameter.getFile().getHeaders().get(HttpHeaders.CONTENT_DISPOSITION)).anyMatch(value -> value.contains(HttpTransformerServiceImpl.LES_ECHOS_XML_FILE_NAME));
        assertThat(xmlFileParameter.getFile().getHeaders().get(HttpHeaders.CONTENT_TYPE)).containsOnly(MediaType.TEXT_XML_VALUE);
    }

    @Test
    void buildLesEchosPdfFileParameter_shouldReturnHttpFileParameter() {
        SuiviAnnonce suiviAnnonceDTO = new SuiviAnnonce();
        String export = "TEST EXPORT";
        doAnswer(invocation -> {
            ((ByteArrayOutputStream) invocation.getArguments()[1]).writeBytes(export.getBytes());
            ((ByteArrayOutputStream) invocation.getArguments()[1]).close();
            return null;
        }).when(suiviAnnonceService).exportToDocument(eq(suiviAnnonceDTO), any(ByteArrayOutputStream.class), anyBoolean(), anyBoolean());
        when(suiviAnnonceService.getSuiviAnnonceEntity(anyInt())).thenReturn(suiviAnnonceDTO);
        Message<?> message = new Message<>() {
            @Override
            public SuiviAnnonce getPayload() {
                return suiviAnnonceDTO;
            }

            @Override
            public MessageHeaders getHeaders() {
                Map<String, Object> map = new HashMap<>();
                map.put("idAnnonce", 1);
                return new MessageHeaders(map);
            }
        };
        HttpFileParameter pdfFileParameter = httpTransformerService.buildLesEchosPdfFileParameter(message);

        assertThat(pdfFileParameter).extracting(HttpFileParameter::getName).isEqualTo(HttpTransformerServiceImpl.LES_ECHOS_PDF_FILE_NAME);
        assertThat(pdfFileParameter).extracting(HttpFileParameter::getFile).extracting(HttpEntity::getBody).isEqualTo(export.getBytes());
        assertThat(pdfFileParameter.getFile().getHeaders()).containsOnlyKeys(HttpHeaders.CONTENT_DISPOSITION, HttpHeaders.CONTENT_TYPE);
        assertThat(pdfFileParameter.getFile().getHeaders().get(HttpHeaders.CONTENT_DISPOSITION)).anyMatch(value -> value.contains(HttpTransformerServiceImpl.LES_ECHOS_PDF_FILE_NAME));
        assertThat(pdfFileParameter.getFile().getHeaders().get(HttpHeaders.CONTENT_TYPE)).containsOnly(MediaType.APPLICATION_PDF_VALUE);
    }
}
