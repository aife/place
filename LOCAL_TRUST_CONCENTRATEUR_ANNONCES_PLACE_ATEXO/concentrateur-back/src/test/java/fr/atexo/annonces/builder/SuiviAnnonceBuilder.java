package fr.atexo.annonces.builder;

import fr.atexo.annonces.boot.entity.Offre;
import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.modeles.StatutSuiviAnnonceEnum;

import java.sql.Timestamp;
import java.time.Instant;

/**
 * Builder pour SuiviAnnonce
 */
public final class SuiviAnnonceBuilder {
    private int id;
    private StatutSuiviAnnonceEnum statut;
    private Integer idConsultation;
    private String xml;
    private Offre offre;
    private Timestamp dateEnvoi;
    private Timestamp datePublication;
    private Instant dateDebutEnvoi;
    private Instant dateDemandeEnvoi;
    private String idPlatform;
    private String messageStatut;
    private String organisme;

    private SuiviAnnonceBuilder() {
    }

    public static SuiviAnnonceBuilder aSuiviAnnonce() {
        return new SuiviAnnonceBuilder();
    }

    public SuiviAnnonceBuilder withId(int id) {
        this.id = id;
        return this;
    }

    public SuiviAnnonceBuilder withStatut(StatutSuiviAnnonceEnum statut) {
        this.statut = statut;
        return this;
    }

    public SuiviAnnonceBuilder withIdConsultation(Integer idConsultation) {
        this.idConsultation = idConsultation;
        return this;
    }

    public SuiviAnnonceBuilder withXml(String xml) {
        this.xml = xml;
        return this;
    }

    public SuiviAnnonceBuilder withOffre(Offre offre) {
        this.offre = offre;
        return this;
    }

    public SuiviAnnonceBuilder withDateTraitement(Timestamp dateEnvoi) {
        this.dateEnvoi = dateEnvoi;
        return this;
    }

    public SuiviAnnonceBuilder withDatePublication(Timestamp datePublication) {
        this.datePublication = datePublication;
        return this;
    }

    public SuiviAnnonceBuilder withDateTraitementInterne(Instant dateDebutEnvoi) {
        this.dateDebutEnvoi = dateDebutEnvoi;
        return this;
    }

    public SuiviAnnonceBuilder withDateDemande(Instant dateDemandeEnvoi) {
        this.dateDemandeEnvoi = dateDemandeEnvoi;
        return this;
    }

    public SuiviAnnonceBuilder withIdPlatform(String idPlatform) {
        this.idPlatform = idPlatform;
        return this;
    }

    public SuiviAnnonceBuilder withMessageStatut(String messageStatut) {
        this.messageStatut = messageStatut;
        return this;
    }

    public SuiviAnnonceBuilder withOrganisme(String organisme) {
        this.organisme = organisme;
        return this;
    }

    public SuiviAnnonce build() {
        SuiviAnnonce suiviAnnonce = new SuiviAnnonce();
        suiviAnnonce.setId(id);
        suiviAnnonce.setStatut(statut);
        suiviAnnonce.setIdConsultation(idConsultation);
        suiviAnnonce.setXml(xml);
        suiviAnnonce.setOffre(offre);
        suiviAnnonce.setDateEnvoi(dateEnvoi.toInstant());
        suiviAnnonce.setDatePublication(datePublication.toInstant());
        suiviAnnonce.setDateDebutEnvoi(dateDebutEnvoi);
        suiviAnnonce.setDateDemandeEnvoi(dateDemandeEnvoi);
        suiviAnnonce.setIdPlatform(idPlatform);
        suiviAnnonce.setMessageStatut(messageStatut);
        suiviAnnonce.setOrganisme(organisme);
        return suiviAnnonce;
    }

    public SuiviAnnonceBuilder withDefaults() {
        this.id = 1;
        this.idConsultation = 1;
        this.statut = StatutSuiviAnnonceEnum.BROUILLON;
        this.xml = "";
        this.offre = OffreBuilder.anOffre().withDefaults().build();
        this.dateEnvoi = Timestamp.from(Instant.EPOCH);
        this.datePublication = Timestamp.from(Instant.EPOCH);
        this.dateDebutEnvoi = Instant.EPOCH;
        this.dateDemandeEnvoi = Instant.EPOCH;
        this.idPlatform = "PLATEFORME_TEST";
        this.messageStatut = "Annonce en cours";
        this.organisme = "Organisme de test";
        return this;
    }
}
