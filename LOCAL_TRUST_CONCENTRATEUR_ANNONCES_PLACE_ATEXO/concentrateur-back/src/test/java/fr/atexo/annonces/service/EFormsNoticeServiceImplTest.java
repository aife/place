package fr.atexo.annonces.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.model.EFormsFormulaire;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.model.EFormsFormulaireAdd;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.impl.EFormsNoticeServicesImpl;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.Constraint;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.DocumentType;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.MandatoryForbiddenDetails;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.services.IEFormsSetupServices;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.services.impl.EFormsSdkServiceImpl;
import fr.atexo.annonces.boot.core.domain.e_forms_sdk.services.impl.EFormsSetupServicesImpl;
import fr.atexo.annonces.boot.entity.*;
import fr.atexo.annonces.boot.repository.*;
import fr.atexo.annonces.boot.ws.eforms.EFormsWS;
import fr.atexo.annonces.boot.ws.eforms.SchematronConfiguration;
import fr.atexo.annonces.boot.ws.mpe.MpeWs;
import fr.atexo.annonces.config.AuthenitificationRequest;
import fr.atexo.annonces.config.EFormsHelper;
import fr.atexo.annonces.config.MpeToken;
import fr.atexo.annonces.config.RestTemplateConfig;
import fr.atexo.annonces.dto.ConfigurationConsultationDTO;
import fr.atexo.annonces.dto.eforms.EFormsLangEnum;
import fr.atexo.annonces.dto.eforms.EformsSubmitResponseDTO;
import fr.atexo.annonces.mapper.EformsSurchargeMapperImpl;
import fr.atexo.annonces.mapper.FormulaireMapperImpl;
import fr.atexo.annonces.modeles.StatutENoticeEnum;
import org.apache.commons.io.FileUtils;
import org.apache.commons.net.util.Base64;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.test.util.ReflectionTestUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Tests pour la couche service des départements
 */
@ExtendWith(MockitoExtension.class)
class EFormsNoticeServiceImplTest {

    String resourcesTag = "1.10.0";
    String libelleTag = "1.10.0";
    @Mock
    SchematronConfiguration schematronConfigurationMock;
    SchematronConfiguration schematronConfiguration = SchematronConfiguration.builder().downloadDir("target/data/eForms/").resourcesUrl("https://github.com/OP-TED/eForms-SDK/archive/refs/tags/").prefixZipFile("eForms-SDK-").resourcesTag(resourcesTag).libelleTag(libelleTag).build();
    private IEFormsSetupServices eFormsSetupServices = new EFormsSetupServicesImpl(schematronConfiguration);
    @Mock
    private EFormsFormulaireRepository eFormsFormulaireRepository;
    @Mock
    private AgentService agentService;
    @Mock
    private PlateformeConfigurationRepository plateformeConfigurationRepository;
    @Mock
    private EFormsProcedureRepository eFormsProcedureRepository;
    @Mock
    private SuiviAnnonceRepository suiviAnnonceRepository;
    @Mock
    private OrganismeRepository organismeRepository;
    @Mock
    private IEFormsSetupServices ieFormsSetupServices;
    @Mock
    private FormulaireMapperImpl formulaireMapper;
    @Mock
    private EFormsSdkServiceImpl ieFormsSdkService;
    @Mock
    private EformsSurchargeRepository eformsSurchargeRepository;
    @Mock
    private EFormsWS eFormsWS;
    @InjectMocks
    private EFormsNoticeServicesImpl eFormsNoticeServices;
    @Mock
    private EformsSurchargeMapperImpl eformsSurchargeMapper;
    // private DependencyFactorySdk dependencyFactorySdk;
    // final private TranslatorOptions TRANSLATOR_OPTIONS = new EfxTranslatorOptions(DecimalFormat.EFX_DEFAULT);

    @BeforeEach
    void setUp() throws InstantiationException {

        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(eFormsNoticeServices, "schematronConfiguration", schematronConfiguration);
        ReflectionTestUtils.setField(eFormsNoticeServices, "eFormsFormulaireRepository", eFormsFormulaireRepository);
        ReflectionTestUtils.setField(eFormsNoticeServices, "eFormsProcedureRepository", eFormsProcedureRepository);
        ReflectionTestUtils.setField(eFormsNoticeServices, "suiviAnnonceRepository", suiviAnnonceRepository);
        ReflectionTestUtils.setField(eFormsNoticeServices, "organismeRepository", organismeRepository);
        ReflectionTestUtils.setField(eFormsNoticeServices, "formulaireMapper", formulaireMapper);
        ReflectionTestUtils.setField(eFormsNoticeServices, "eformsSurchargeMapper", eformsSurchargeMapper);
        ReflectionTestUtils.setField(eFormsNoticeServices, "ieFormsSdkService", ieFormsSdkService);
        ReflectionTestUtils.setField(eFormsNoticeServices, "eFormsWS", eFormsWS);
        ReflectionTestUtils.setField(eFormsNoticeServices, "eformsSurchargeRepository", eformsSurchargeRepository);
        ReflectionTestUtils.setField(eFormsNoticeServices, "agentService", agentService);
        ReflectionTestUtils.setField(eFormsNoticeServices, "mpeWs", new MpeWs(new RestTemplateConfig().restTemplate()));
        String pathname = "target/data/eForms/";
        //   dependencyFactorySdk = new DependencyFactorySdk(resourcesTag, new File(pathname).getAbsoluteFile().toPath());
    }

    /*
        private String translateExpression(final String expression, final String... params) {
            try {
                return EfxTranslator.translateExpression(dependencyFactorySdk, resourcesTag,
                        expression, TRANSLATOR_OPTIONS, params);
            } catch (InstantiationException e) {
                throw new RuntimeException(e);
            }
        }


        @Test
        void testParenthesizedBooleanExpression() {
            assertEquals("(true() or true()) and false()",
                    translateExpression("{ND-LotTenderingProcess} ${BT-131(d)-Lot is present}"));
        }
    */
    @Test
    void addXmlNoticeFromConsultation() throws IOException {

        String mpeUrl = "https://simap-mpe-int.local-trust.com/";

        MpeToken authentificationV2 = getAuthentificationV2(mpeUrl);
        EFormsFormulaireAdd build = EFormsFormulaireAdd.builder().idNotice("16").idConsultation(510389).tokenMpe(authentificationV2.getToken()).mpeUrl(mpeUrl).lang("FRA").idAnnonce(1).auteurEmail("ismail.atitallah@atexo.com").build();
        Long id = 1L;
        EFormsFormulaireEntity entity = EFormsFormulaireEntity.builder().id(id).procedure(EFormsProcedureEntity.builder().uuid("986b9b26-fbce-4c9b-87e3-f3b4bf8b8c61").build()).statut(StatutENoticeEnum.PUBLISHED).valid(true).suiviAnnonce(SuiviAnnonce.builder().idConsultation(1).offre(Offre.builder().code("16_ENOTICES").build()).build()).lang("FRA").auteurEmail("ismail.atitallah@atexo.com").idNotice("16").noticeVersion(1).statut(StatutENoticeEnum.STOPPED).uuid("42345f77-a491-40e4-ad13-2f4dc6766a31").idConsultation(1).plateforme("MPE_atexo_develop").build();

        String xml = Files.readString(new File("src/test/resources/data/pub.xml").toPath());

        when(eformsSurchargeRepository.findByCodeAndIdPlatformIsNullAndNoticeIdIsNullAndActifIsTrue(any())).thenAnswer(invocation -> {
            Object argument = invocation.getArgument(0);
            if (argument.equals("GR-Organisations")) {
                return EFormsSurcharge.builder().defaultValue("{\n" + "            \"BT-633-Organization\": false,\n" + "            \"ND-Company\": {\n" + "              \"BT-500-Organization-Company\": \"Juridictions administratives\",\n" + "              \"BT-501-Organization-Company\": \"JURAD\",\n" + "              \"BT-505-Organization-Company\": \"https://justice.public.lu/fr/organisation-justice/juridictions-administratives.html\",\n" + "              \"ND-CompanyAddress\": {\n" + "                \"BT-510(a)-Organization-Company\": \"1, rue du Fort Thüngen\",\n" + "                \"BT-512-Organization-Company\": \"1499\",\n" + "                \"BT-513-Organization-Company\": \"Luxembourg\",\n" + "                \"BT-514-Organization-Company\": \"LUX\"\n" + "              },\n" + "              \"ND-CompanyContact\": {\n" + "                \"BT-502-Organization-Company\": \"Tribunal administratif - greffe\",\n" + "                \"BT-506-Organization-Company\": \"greffe.tribunal@ja.etat.lu\",\n" + "                \"BT-503-Organization-Company\": \"42105-1\"\n" + "              },\n" + "              \"OPT-200-Organization-Company\": \"ORG-0002\"\n" + "            }\n" + "          }").build();
            }
            if (argument.equals("GR-Procedure-ExclusionGrounds")) {
                return EFormsSurcharge.builder().defaultValue("[\n" +
                        "          {\n" +
                        "            \"BT-67(a)-Procedure\": \"crime-org\"\n" +
                        "          },\n" +
                        "          {\n" +
                        "            \"BT-67(a)-Procedure\": \"corruption\"\n" +
                        "          },\n" +
                        "          {\n" +
                        "            \"BT-67(a)-Procedure\": \"terr-offence\"\n" +
                        "          },\n" +
                        "          {\n" +
                        "            \"BT-67(a)-Procedure\": \"finan-laund\"\n" +
                        "          },\n" +
                        "          {\n" +
                        "            \"BT-67(a)-Procedure\": \"human-traffic\"\n" +
                        "          },\n" +
                        "          {\n" +
                        "            \"BT-67(a)-Procedure\": \"socsec-pay\"\n" +
                        "          },\n" +
                        "          {\n" +
                        "            \"BT-67(a)-Procedure\": \"tax-pay\"\n" +
                        "          }\n" +
                        "        ]").build();
            }

            return null;
        });

        when(formulaireMapper.mapToModel(any())).thenCallRealMethod();
        when(eformsSurchargeMapper.mapToModel(any())).thenCallRealMethod();
        when(ieFormsSdkService.getNotice(any(), any())).thenCallRealMethod();
        when(ieFormsSdkService.getSubNoticeList(any())).thenCallRealMethod();
        when(ieFormsSdkService.getEformsFolderSdk(any())).thenReturn(schematronConfiguration.getDownloadDir() + schematronConfiguration.getPrefixZipFile() + schematronConfiguration.getResourcesTag());
        when(agentService.getConfiguration(any(), any(), any())).thenReturn(EFormsHelper.getObjectFromFile("src/test/resources/json/consultation.json", ConfigurationConsultationDTO.class));
        when(suiviAnnonceRepository.findById(any())).thenReturn(Optional.ofNullable(SuiviAnnonce.builder().xml(xml).offre(Offre.builder().code("4").build()).build()));

        when(eFormsFormulaireRepository.save(any())).thenAnswer(invocationOnMock -> invocationOnMock.getArgument(0));
        when(eFormsProcedureRepository.findByCode(any())).thenReturn(null);
        when(eFormsProcedureRepository.save(any())).thenAnswer(invocationOnMock -> invocationOnMock.getArgument(0));
        when(eFormsFormulaireRepository.findById(any())).thenReturn(Optional.of(entity));
        EformsSubmitResponseDTO t = new EformsSubmitResponseDTO();
        when(eFormsWS.submit(any(), any(), any())).thenReturn(t);

        EFormsFormulaire result = eFormsNoticeServices.addFormulaire("token", build, "plateforme", 1);
        assertNotNull(result);
        assertNotNull(result.getFormulaire());
        BufferedWriter writer = new BufferedWriter(new FileWriter("src/test/resources/data/exemple.json"));
        String str = new String(Base64.decodeBase64(result.getFormulaire().getBytes()));
        writer.write(str);
        writer.close();
        entity.setFormulaire(result.getFormulaire());
        entity.setStatut(StatutENoticeEnum.DRAFT);
        var resultXml = eFormsNoticeServices.submit(id, "MPE_atexo_develop", true);
        writer = new BufferedWriter(new FileWriter("src/test/resources/data/exemple.xml"));
        writer.write(new String(Base64.decodeBase64(resultXml.getXmlGenere().getBytes())));
        writer.close();
    }

    @Test
    void buildXmlDocument() throws IOException {

        Path pathSource = Paths.get("src/test/resources/data/formulaire.json").toAbsolutePath();
        Object input = new ObjectMapper().readValue(pathSource.toFile(), Object.class);

        Document result = eFormsNoticeServices.buildXmlDocument(input, DocumentType.builder().id("CN").namespace("urn:oasis:names:specification:ubl:schema:xsd:ContractNotice-2").rootElement("ContractNotice").build(), "16", EFormsLangEnum.FRA.name(), "MPE_atexo_develop", schematronConfiguration.getResourcesTag());
        assertNotNull(result);
        String xmlString = EFormsHelper.getXmlString(result);
        BufferedWriter writer = new BufferedWriter(new FileWriter("src/test/resources/data/formulaire.xml"));
        writer.write(xmlString);

        writer.close();
    }

    @Test
    void buildXmlDocumentV2() throws IOException {

        Path pathSource = Paths.get("src/test/resources/data/new-formulaire.json").toAbsolutePath();
        Object input = new ObjectMapper().readValue(pathSource.toFile(), Object.class);

        Document result = eFormsNoticeServices.buildXmlDocumentV2(input, DocumentType.builder().id("CN").namespace("urn:oasis:names:specification:ubl:schema:xsd:ContractNotice-2").rootElement("ContractNotice").build(), EFormsLangEnum.FRA.name(), "MPE_atexo_develop", schematronConfiguration.getResourcesTag());
        assertNotNull(result);
        String xmlString = EFormsHelper.getXmlString(result);
        BufferedWriter writer = new BufferedWriter(new FileWriter("src/test/resources/data/new-formulaire.xml"));
        writer.write(xmlString);

        writer.close();
    }

    @Test
    void changeFormulaire() throws IOException {

        Path pathSource = Paths.get("src/test/resources/data/formulaire.json").toAbsolutePath();
        Object input = new ObjectMapper().readValue(pathSource.toFile(), Object.class);
        Long id = 1L;
        EFormsFormulaireEntity entity = EFormsFormulaireEntity.builder().id(id).procedure(EFormsProcedureEntity.builder().uuid("986b9b26-fbce-4c9b-87e3-f3b4bf8b8c61").build()).statut(StatutENoticeEnum.PUBLISHED).valid(true).suiviAnnonce(SuiviAnnonce.builder().idConsultation(1).offre(Offre.builder().code("16_ENOTICES").build()).build()).lang("FRA").publicationId("99550-2023").auteurEmail("ismail.atitallah@atexo.com").idNotice("16").noticeVersion(1).formulaire(Base64.encodeBase64StringUnChunked(EFormsHelper.getString(input).getBytes())).uuid("42345f77-a491-40e4-ad13-2f4dc6766a31").idConsultation(1).plateforme("MPE_atexo_develop").build();
        when(eformsSurchargeRepository.findByCodeAndIdPlatformIsNullAndNoticeIdIsNullAndActifIsTrue(any())).thenAnswer(invocation -> !invocation.getArgument(0).equals("GR-Organisations") ? null : EFormsSurcharge.builder().defaultValue("{\n" + "            \"BT-633-Organization\": false,\n" + "            \"ND-Company\": {\n" + "              \"BT-500-Organization-Company\": \"Juridictions administratives\",\n" + "              \"BT-501-Organization-Company\": \"JURAD\",\n" + "              \"BT-505-Organization-Company\": \"https://justice.public.lu/fr/organisation-justice/juridictions-administratives.html\",\n" + "              \"ND-CompanyAddress\": {\n" + "                \"BT-510(a)-Organization-Company\": \"1, rue du Fort Thüngen\",\n" + "                \"BT-512-Organization-Company\": \"1499\",\n" + "                \"BT-513-Organization-Company\": \"Luxembourg\",\n" + "                \"BT-514-Organization-Company\": \"LUX\"\n" + "              },\n" + "              \"ND-CompanyContact\": {\n" + "                \"BT-502-Organization-Company\": \"Tribunal administratif - greffe\",\n" + "                \"BT-506-Organization-Company\": \"greffe.tribunal@ja.etat.lu\",\n" + "                \"BT-503-Organization-Company\": \"42105-1\"\n" + "              },\n" + "              \"OPT-200-Organization-Company\": \"ORG-0002\"\n" + "            }\n" + "          }").build());
        when(formulaireMapper.mapToModel(any())).thenCallRealMethod();
        when(ieFormsSdkService.getNotice(any(), any())).thenCallRealMethod();
        when(ieFormsSdkService.getSubNoticeList(any())).thenCallRealMethod();
        when(ieFormsSdkService.getEformsFolderSdk(any())).thenReturn(schematronConfiguration.getDownloadDir() + schematronConfiguration.getPrefixZipFile() + schematronConfiguration.getResourcesTag());
        when(eFormsFormulaireRepository.findById(any())).thenReturn(Optional.of(entity));
        when(eFormsFormulaireRepository.save(any())).thenAnswer(invocationOnMock -> invocationOnMock.getArgument(0));
        when(formulaireMapper.mapToModel(any())).thenCallRealMethod();
        when(agentService.findByToken(any())).thenReturn(Agent.builder().id("1").email("test@test.com").build());
        EformsSubmitResponseDTO t = new EformsSubmitResponseDTO();
        when(eFormsWS.submit(any(), any(), any())).thenReturn(t);

        EFormsFormulaire result = eFormsNoticeServices.change(id, "MPE_atexo_develop", "token");
        assertNotNull(result);
        assertNotNull(result.getPreviousId());
        assertThat(result.getUuid()).isNotEqualTo(entity.getUuid());
        assertThat(result.getNoticeVersion()).isEqualTo(1);
        assertThat(result.getPreviousId()).isEqualTo(entity.getId());
        BufferedWriter writer = new BufferedWriter(new FileWriter("src/test/resources/data/formulaire-change.json"));
        String str = new String(Base64.decodeBase64(result.getFormulaire().getBytes()));
        writer.write(str);
        writer.close();
        entity.setFormulaire(result.getFormulaire());
        entity.setStatut(StatutENoticeEnum.DRAFT);
        var resultXml = eFormsNoticeServices.submit(id, "MPE_atexo_develop", true);
        writer = new BufferedWriter(new FileWriter("src/test/resources/data/formulaire-change.xml"));
        writer.write(new String(Base64.decodeBase64(resultXml.getXmlGenere().getBytes())));
        writer.close();
    }

    @Test
    void submitFormulaire() throws IOException {

        Path pathSource = Paths.get("src/test/resources/data/formulaire.json").toAbsolutePath();
        Path pathXml = Paths.get("src/test/resources/data/formulaire-chane.xml").toAbsolutePath();
        Object input = new ObjectMapper().readValue(pathSource.toFile(), Object.class);
        Long id = 1L;
        EFormsFormulaireEntity entity = EFormsFormulaireEntity.builder().id(id).procedure(EFormsProcedureEntity.builder().uuid("986b9b26-fbce-4c9b-87e3-f3b4bf8b8c61").build()).statut(StatutENoticeEnum.PUBLISHED).valid(true).suiviAnnonce(SuiviAnnonce.builder().idConsultation(1).offre(Offre.builder().code("16_ENOTICES").build()).build()).lang("FRA").publicationId("99550-2023").auteurEmail("ismail.atitallah@atexo.com").idNotice("16").noticeVersion(1)
                .formulaire(Base64.encodeBase64StringUnChunked(EFormsHelper.getString(input).getBytes()))
                .xmlGenere(Base64.encodeBase64StringUnChunked(FileUtils.readFileToString(pathXml.toFile(), "UTF-8").getBytes()))
                .uuid("42345f77-a491-40e4-ad13-2f4dc6766a31").idConsultation(1).plateforme("MPE_atexo_develop").build();
        when(ieFormsSdkService.getNotice(any(), any())).thenCallRealMethod();
        when(ieFormsSdkService.getSubNoticeList(any())).thenCallRealMethod();
        when(ieFormsSdkService.getEformsFolderSdk(any())).thenReturn(schematronConfiguration.getDownloadDir() + schematronConfiguration.getPrefixZipFile() + schematronConfiguration.getResourcesTag());
        when(eFormsFormulaireRepository.save(any())).thenAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        ZonedDateTime dateSoumission = ZonedDateTime.of(2021, 9, 1, 0, 0, 0, 0, ZoneId.of("Europe/Paris"));
        EFormsFormulaireEntity result = eFormsNoticeServices.updateNoticeDatePublication(dateSoumission, entity);
        assertNotNull(result);

        BufferedWriter writer = new BufferedWriter(new FileWriter("src/test/resources/data/formulaire-submit-updated.json"));
        writer.write(new String(Base64.decodeBase64(result.getFormulaire())));
        writer.close();
        writer = new BufferedWriter(new FileWriter("src/test/resources/data/formulaire-submit-updated.xml"));
        writer.write(new String(Base64.decodeBase64(result.getXmlGenere())));
        writer.close();
    }

    @Test
    void reprendreFormulaire() throws IOException {

        Path pathSource = Paths.get("src/test/resources/data/formulaire.json").toAbsolutePath();
        Object input = new ObjectMapper().readValue(pathSource.toFile(), Object.class);
        Long id = 1L;
        EFormsFormulaireEntity entity = EFormsFormulaireEntity.builder().id(id).procedure(EFormsProcedureEntity.builder().uuid("986b9b26-fbce-4c9b-87e3-f3b4bf8b8c61").build()).statut(StatutENoticeEnum.PUBLISHED).valid(true).suiviAnnonce(SuiviAnnonce.builder().idConsultation(1).offre(Offre.builder().code("16_ENOTICES").build()).build()).lang("FRA").auteurEmail("ismail.atitallah@atexo.com").idNotice("16").noticeVersion(1).statut(StatutENoticeEnum.STOPPED).formulaire(Base64.encodeBase64StringUnChunked(EFormsHelper.getString(input).getBytes())).uuid("42345f77-a491-40e4-ad13-2f4dc6766a31").idConsultation(1).plateforme("MPE_atexo_develop").build();

        when(formulaireMapper.mapToModel(any())).thenCallRealMethod();
        when(ieFormsSdkService.getNotice(any(), any())).thenCallRealMethod();
        when(ieFormsSdkService.getSubNoticeList(any())).thenCallRealMethod();
        when(ieFormsSdkService.getEformsFolderSdk(any())).thenReturn(schematronConfiguration.getDownloadDir() + schematronConfiguration.getPrefixZipFile() + schematronConfiguration.getResourcesTag());
        when(eFormsFormulaireRepository.findById(any())).thenReturn(Optional.of(entity));
        when(eFormsFormulaireRepository.save(any())).thenAnswer(invocationOnMock -> invocationOnMock.getArgument(0));
        when(formulaireMapper.mapToModel(any())).thenCallRealMethod();
        when(agentService.findByToken(any())).thenReturn(Agent.builder().id("1").email("test@test.com").build());
        EformsSubmitResponseDTO t = new EformsSubmitResponseDTO();
        when(eFormsWS.submit(any(), any(), any())).thenReturn(t);

        EFormsFormulaire result = eFormsNoticeServices.reprendre(id, "MPE_atexo_develop", null);
        assertNotNull(result);
        assertNotNull(result.getPreviousId());
        assertThat(result.getUuid()).isEqualTo(entity.getUuid());
        assertThat(result.getNoticeVersion()).isEqualTo(entity.getNoticeVersion() + 1);
        assertThat(result.getPreviousId()).isEqualTo(entity.getId());
        BufferedWriter writer = new BufferedWriter(new FileWriter("src/test/resources/data/formulaire-reprendre.json"));
        String str = new String(Base64.decodeBase64(result.getFormulaire().getBytes()));
        writer.write(str);
        writer.close();
        entity.setFormulaire(result.getFormulaire());
        entity.setStatut(StatutENoticeEnum.DRAFT);
        var resultXml = eFormsNoticeServices.submit(id, "MPE_atexo_develop", true);
        writer = new BufferedWriter(new FileWriter("src/test/resources/data/formulaire-reprendre.xml"));
        writer.write(new String(Base64.decodeBase64(resultXml.getXmlGenere().getBytes())));
        writer.close();
    }


    private MpeToken getAuthentificationV2(String mpeUrl) {
        String url = mpeUrl + "api/v2/token";
        AuthenitificationRequest request = AuthenitificationRequest.builder().login("admin_t5x").password("admin_t5x").build();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");

        return new RestTemplateConfig().restTemplate().postForEntity(url, new HttpEntity<>(request, headers), MpeToken.class).getBody();

    }


    @Test
    void validateXmlFromXsd() throws IOException, ParserConfigurationException, SAXException {

        String xsdPath = "src/main/resources/xsd/enotices/schemas/maindoc/UBL-ContractNotice-2.3.xsd";
        String xmlPath = "src/test/resources/data/notice-valide.xml";
        // String xmlPath = "src/test/resources/data/cn_24_minimal.xml";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new File(xmlPath));

        Document result = eFormsNoticeServices.correctSequence(doc, "oasis.names.specification.ubl.schema.xsd.contractnotice_2");
        String fileName = xmlPath + "-corrected.xml";
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
        writer.write(EFormsHelper.getXmlString(result));

        writer.close();
        String valid = EFormsHelper.validateXMLSchema(xsdPath, fileName);
        assertNull(valid);
    }

    @Test
    void mustReturnNotForbiddenWithoutConditionWhenConditionAndTrue() {
        String noticeId = "29";
        boolean result = eFormsNoticeServices.estForbidden(noticeId, null, MandatoryForbiddenDetails.builder().value(false).severity("ERROR").constraints(List.of(Constraint.builder().noticeTypes(List.of("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "CEI", "T01", "X01", "X02")).value(true).severity("ERROR").build(), Constraint.builder().noticeTypes(List.of("29", "30", "31", "32", "33", "34", "35", "36", "37", "T02")).condition("{ND-SettledContract} ${not(BT-142-LotResult == 'selec-w')}").value(true).severity("ERROR").build())).build(), true);
        assertFalse(result);
    }

    @Test
    void mustReturnForbiddenWithoutConditionWhenConditionAndFalse() {
        String noticeId = "29";
        boolean result = eFormsNoticeServices.estForbidden(noticeId, null, MandatoryForbiddenDetails.builder().value(true).severity("ERROR").constraints(List.of(Constraint.builder().noticeTypes(List.of("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "CEI", "T01", "X01", "X02")).value(true).severity("ERROR").build(), Constraint.builder().noticeTypes(List.of("29", "30", "31", "32", "33", "34", "35", "36", "37", "T02")).condition("{ND-SettledContract} ${not(BT-142-LotResult == 'selec-w')}").value(true).severity("ERROR").build())).build(), true);
        assertTrue(result);
    }

    @Test
    void mustReturnForbiddenWithCondition() {
        String noticeId = "29";
        boolean result = eFormsNoticeServices.estForbidden(noticeId, null, MandatoryForbiddenDetails.builder().value(false).severity("ERROR").constraints(List.of(Constraint.builder().noticeTypes(List.of("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "CEI", "T01", "X01", "X02")).value(true).severity("ERROR").build(), Constraint.builder().noticeTypes(List.of("29", "30", "31", "32", "33", "34", "35", "36", "37", "T02")).condition("{ND-SettledContract} ${not(BT-142-LotResult == 'selec-w')}").value(true).severity("ERROR").build())).build(), false);
        assertTrue(result);
    }
}
