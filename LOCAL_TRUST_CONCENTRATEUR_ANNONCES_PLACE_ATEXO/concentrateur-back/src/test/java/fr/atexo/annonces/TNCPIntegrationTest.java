package fr.atexo.annonces;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import fr.atexo.annonces.boot.ws.mpe.model.ConsultationContexte;
import fr.atexo.annonces.config.EFormsHelper;
import fr.atexo.annonces.dto.ConfigurationConsultationDTO;
import fr.atexo.annonces.mapper.tncp.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest(classes = {AvisMapper.class,
        CompteMapperImpl.class, ConsultationMapper.class, PaysInseeMapper.class, TechniqueAchatMapper.class,
        CpvMapperImpl.class,
        TypeProcedureMapper.class,
        LotMapper.class,
        NatureMapper.class,
        ProfilMapper.class,
        ReferencesMapperImpl.class,
        UtilisateurMapperImpl.class,
        VarianteMapper.class})
@DisplayName("Test d'intégration TNCP")
@Slf4j
public class TNCPIntegrationTest {

    @Autowired
    private AvisMapper target;

    @Test
    @DisplayName("Test passant du mapper d'avis TNCP")
    void GIVEN_un_contexte_valide_WHEN_transformation_en_avis_THEN_le_resultat_est_conforme() throws JsonProcessingException {
        var contexte = EFormsHelper.getObjectFromFile(new File("src/test/resources/data/consultation.json"), ConsultationContexte.class);
        var result = target.map(ConfigurationConsultationDTO.with().contexte(contexte).build(), null, true);

        assertThat(result).isNotNull();

        var mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());

        var output = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(result);

        log.debug(output);

        assertThat(output).isEqualTo("{\n" +
                "  \"user\" : {\n" +
                "    \"email\" : \"mathieu.michaud@atexo.com\",\n" +
                "    \"firstName\" : \"Patrick\",\n" +
                "    \"lastName\" : \"Dupond_02\"\n" +
                "  },\n" +
                "  \"siret\" : \"44090956200033\",\n" +
                "  \"targetApplication\" : \"avis-de-publicite-rie\",\n" +
                "  \"service\" : \"Service interne de dématérialisation-10602\",\n" +
                "  \"profiles\" : [ \"AVIS_PUBLICITE_GESTION\" ],\n" +
                "  \"payload\" : {\n" +
                "    \"consultation\" : {\n" +
                "      \"categoriePrincipale\" : \"travaux\",\n" +
                "      \"codeCpvPrincipal\" : \"45212413\",\n" +
                "      \"codesCpvSecondaires\" : [ ],\n" +
                "      \"contactAcheteur\" : {\n" +
                "        \"adresse\" : {\n" +
                "          \"code\" : \"75002\",\n" +
                "          \"codePays\" : \"99327\",\n" +
                "          \"localite\" : \"PARIS 2\",\n" +
                "          \"nomVoie\" : \"BD DES CAPUCINES\",\n" +
                "          \"numeroVoie\" : \"17\",\n" +
                "          \"typeVoie\" : \"BD\"\n" +
                "        },\n" +
                "        \"adresseElectronique\" : \"mathieu.michaud@atexo.com\",\n" +
                "        \"nom\" : \"Dupond_02\",\n" +
                "        \"prenom\" : \"Patrick\",\n" +
                "        \"raisonSociale\" : \"Service interne de dématérialisation\",\n" +
                "        \"siret\" : \"44090956200033\",\n" +
                "        \"telephone\" : \"+33 102030400\"\n" +
                "      },\n" +
                "      \"criteresAttribution\" : null,\n" +
                "      \"dateDebutContratEstimee\" : null,\n" +
                "      \"dateFinContratEstimee\" : null,\n" +
                "      \"dateLimiteRemiseCandidature\" : [ -1, 11, 30 ],\n" +
                "      \"dateLimiteRemiseOffre\" : \"2024-02-13T23:51:00\",\n" +
                "      \"delaiValiditeOffreAcheteur\" : 0,\n" +
                "      \"dureeContratEstimee\" : null,\n" +
                "      \"lieuExecution\" : {\n" +
                "        \"code\" : \"ZA\",\n" +
                "        \"codePays\" : \"99303\",\n" +
                "        \"localite\" : \"AFRIQUE DU SUD\",\n" +
                "        \"nomVoie\" : null,\n" +
                "        \"numeroVoie\" : null,\n" +
                "        \"typeVoie\" : null\n" +
                "      },\n" +
                "      \"lots\" : [ ],\n" +
                "      \"marcheReserve\" : null,\n" +
                "      \"objet\" : \"ABE_13022024\",\n" +
                "      \"offreTousLots\" : null,\n" +
                "      \"nombreMaxCandidat\" : null,\n" +
                "      \"reductionSuccessiveCandidats\" : false,\n" +
                "      \"tranche\" : false,\n" +
                "      \"typeProcedure\" : \"appel d'offres ouvert\",\n" +
                "      \"typeTechniqueAchat\" : null,\n" +
                "      \"urlConsultation\" : \"https://recette.marches-publics.gouv.fr/entreprise/consultation/504736?orgAcronyme=pmi-min-1\",\n" +
                "      \"urlProfilAcheteur\" : \"https://recette.marches-publics.gouv.fr/entreprise\",\n" +
                "      \"usageNegociation\" : null,\n" +
                "      \"variantesRequises\" : \"non autorisé\"\n" +
                "    },\n" +
                "    \"consultationReferences\" : {\n" +
                "      \"technicalId\" : 504736,\n" +
                "      \"reference\" : \"ABE_13022024\",\n" +
                "      \"externalReference\" : null\n" +
                "    },\n" +
                "    \"piampCredentials\" : null\n" +
                "  }\n" +
                "}");

    }
}
