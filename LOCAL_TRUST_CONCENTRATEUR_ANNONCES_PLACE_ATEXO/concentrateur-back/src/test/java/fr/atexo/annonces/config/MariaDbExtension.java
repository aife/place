package fr.atexo.annonces.config;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class MariaDbExtension implements BeforeAllCallback, AfterAllCallback {
    public static final ITCustomMariaDBContainer MARIA_DB_CONTAINER = ITCustomMariaDBContainer.getInstance();

    @Override
    public void beforeAll(ExtensionContext context) {
        MARIA_DB_CONTAINER.start();
    }

    @Override
    public void afterAll(ExtensionContext context) {
        MARIA_DB_CONTAINER.stop();
    }
}
