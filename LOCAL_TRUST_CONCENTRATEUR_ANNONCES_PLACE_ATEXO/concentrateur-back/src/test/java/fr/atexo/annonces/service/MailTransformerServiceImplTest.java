package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.entity.Offre;
import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.reseau.MailContent;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Test de la construction d'un mail selon les informations métiers
 */
@ExtendWith(MockitoExtension.class)
class MailTransformerServiceImplTest {

    @Mock
    private SuiviAnnonceService suiviAnnonceService;

    @Mock
    private JavaMailSender mailSender;
    @Mock
    private PieceJointeService pieceJointeService;

    @Mock
    private MimeMessage mimeMessage;

    @InjectMocks
    private MailTransformerServiceImpl mailTransformerService;

    @Test
    void buildMailMessage_shouldReturnMimeMailMessage() throws MessagingException, IOException {
        MailContent mailBodyContent = new MailContent("test body");
        MailContent attachedFile = new MailContent(new ByteArrayResource("test attachment".getBytes()), "Annonce_test.xml");
        List<MailContent> mailContentList = List.of(mailBodyContent, attachedFile);
        when(mailSender.createMimeMessage()).thenReturn(mimeMessage);

        MimeMessageHelper mimeMessageHelper = mailTransformerService.buildMail(mailContentList);

        assertThat(mimeMessageHelper).isNotNull();
        BodyPart mainBody = mimeMessageHelper.getMimeMultipart().getBodyPart(0);
        assertThat(mainBody.getContent()).isEqualTo("test body");
        BodyPart attachment = mimeMessageHelper.getRootMimeMultipart().getBodyPart(1);
        assertThat(attachment.getFileName()).isEqualTo("Annonce_test.xml");
        assertThat((ByteArrayInputStream) attachment.getContent()).hasContent("test attachment");
    }

    @Test
    void buildPdfAttachment_shouldReturnMailContent() {
        SuiviAnnonce suiviAnnonceDTO = new SuiviAnnonce();
        suiviAnnonceDTO.setOffre(Offre.builder().convertToPdf(true).build());
        when(pieceJointeService.getExtension(any())).thenReturn("pdf");

        MailContent mailContent = mailTransformerService.buildAnnonceAttachment(suiviAnnonceDTO, "test");

        assertThat(mailContent)
                .isNotNull()
                .extracting(MailContent::getAttachmentFilename)
                .isNotNull()
                .isEqualTo("Annonce_test.pdf");
    }

    @Test
    void buildXmlAttachment_shouldReturnMailContent() {
        MailContent mailContent = mailTransformerService.buildXmlAttachment("xml test", "test");

        assertThat(mailContent)
                .isNotNull()
                .extracting(MailContent::getAttachmentFilename)
                .isNotNull()
                .isEqualTo("Annonce_test.xml");
    }

}
