package fr.atexo.annonces.service;

import eu.europa.publications.resource.schema.ted.r2_0_9.reception.*;
import fr.atexo.annonces.boot.core.domain.e_forms_notice.services.IEFormsNoticeServices;
import fr.atexo.annonces.boot.entity.Offre;
import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.boot.entity.Support;
import fr.atexo.annonces.boot.repository.*;
import fr.atexo.annonces.boot.ws.docgen.DocGenWS;
import fr.atexo.annonces.boot.ws.docgen.DocGenWsAdapter;
import fr.atexo.annonces.boot.ws.docgen.DocGenWsPort;
import fr.atexo.annonces.boot.ws.ressources.RessourcesWS;
import fr.atexo.annonces.boot.ws.ressources.RessourcesWsAdapter;
import fr.atexo.annonces.boot.ws.ressources.RessourcesWsPort;
import fr.atexo.annonces.boot.ws.viewer.ViewerWS;
import fr.atexo.annonces.dto.transaction.TRANSACTION;
import fr.atexo.annonces.mapper.EchangeMapper;
import fr.atexo.annonces.mapper.SuiviAnnonceMapper;
import fr.atexo.annonces.mapper.SuiviTypeAvisPubMapper;
import fr.atexo.annonces.modeles.StatutSuiviAnnonceEnum;
import fr.atexo.annonces.service.exception.PdfExportException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.*;
import java.io.*;
import java.nio.file.Files;
import java.sql.Timestamp;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

/**
 * Tests d'intégration pour générer un PDF. Les tests avec le tag "dev" génèrent un fichier qu'il est possible d'ouvrir
 * par la suite pour examiner le résultat.
 */
@ExtendWith(MockitoExtension.class)
@Disabled
class ExportPdfIntegrationTest {
    @Mock
    private PlateformeConfigurationRepository plateformeConfigurationRepository;

    @Mock
    private SuiviTypeAvisPubRepository consultationFacturationRepository;

    @Mock
    private SuiviTypeAvisPubMapper consultationFacturationMapper;

    @Mock
    private SuiviAnnonceRepository repository;

    @Mock
    private EFormsFormulaireRepository eFormsFormulaireRepository;

    @Mock
    private SupportRepository supportRepository;

    @Mock
    private OffreRepository offreRepository;

    @Mock
    private SupportIntermediaireRepository supportIntermediaireRepositoryMock;

    @Mock
    private IEFormsNoticeServices ieFormsNoticeServices;

    @Mock
    private SuiviAnnonceMapper suiviAnnonceMapper;

    @Mock
    private ViewerWS viewerWS;

    @Mock
    private SuiviTypeAvisPubService suiviTypeAvisPubService;

    @Mock
    private EchangeMapper echangeMapper;

    @Mock
    private SerialisationService serialisationService;

    @Mock
    private SuiviAnnonceRepository suiviAnnonceRepository;

    @Mock
    private DocGenWsPort docGenWsPortMock = new DocGenWsAdapter(new DocGenWS(new RestTemplate(), "/docgen/api/v1/document-generator/generate?convertToPdf={convertToPdf}&defaultOnNull={defaultOnNull}", "https://gendoc-release.local-trust.com/", "/docgen/api/v2/document-editor/request", "/docgen/api/v2/document-editor/request-validation", "/docgen/api/v1/document-suivi/status?token={token}", "/docgen/api/v1/document-convertor/{outputFormat}/finalize"));

    @Mock
    private RessourcesWsPort ressourcesWsPortMock = new RessourcesWsAdapter(new RessourcesWS(new RestTemplate(), "https://ressources-release.local-trust.com/",
            "/docs/modeles/atexo/concentrateur_mpe/avis-standard-text.docx",
            "/docs/modeles/atexo/concentrateur_mpe/avis-simplifie-text.docx"));

    @InjectMocks
    private SuiviAnnonceServiceImpl suiviAnnonceService;

    @BeforeEach
    public void setup() {
        ReflectionTestUtils.setField(suiviAnnonceService, "exportPdfXslPath", "/xsl/fo/TED_ESENDERS_fo.xsl");
        ReflectionTestUtils.setField(suiviAnnonceService, "urlEforms", "url");
    }

    @Test
    void exportPdf_givenMalformedXml_shouldWrapAndThrowPdfExportException() throws IOException {
        Integer idConsultation = 1;
        String codeSupport = "TEST";
        SuiviAnnonce suiviAnnonceDTO = new SuiviAnnonce();
        Support supportDTO = new Support();
        supportDTO.setCode(codeSupport);
        Offre offreDTO = new Offre();
        suiviAnnonceDTO.setSupport(supportDTO);
        suiviAnnonceDTO.setOffre(offreDTO);
        suiviAnnonceDTO.setIdConsultation(idConsultation);
        suiviAnnonceDTO.setStatut(StatutSuiviAnnonceEnum.BROUILLON);
        String xml = Files.readString(new File("src/test/resources/xsl/fo/TED_ESENDERS_malformed.xml").toPath());
        suiviAnnonceDTO.setXml(xml);
        suiviAnnonceDTO.setXmlConcentrateur(xml);
        ByteArrayOutputStream result = new ByteArrayOutputStream();

        assertThatExceptionOfType(PdfExportException.class).isThrownBy(() -> suiviAnnonceService.exportToDocument(suiviAnnonceDTO, result, null, true)).withMessageContainingAll(idConsultation.toString(), codeSupport).withCauseExactlyInstanceOf(UnmarshalException.class).withStackTraceContaining("Erreur(s) lors de la génération du PDF pour la ressource ayant pour id plate-forme/id consultation/code du support null/1/TEST :");
    }

    @Test
    void exportPdf_givenEmptyXml_shouldWrapAndThrowPdfExportException() throws IOException {
        Integer idConsultation = 1;
        String codeSupport = "TEST";
        SuiviAnnonce suiviAnnonceDTO = new SuiviAnnonce();
        Support supportDTO = new Support();
        supportDTO.setCode(codeSupport);
        supportDTO.setLibelle("Support de Test");
        Offre offreDTO = new Offre();
        suiviAnnonceDTO.setSupport(supportDTO);
        suiviAnnonceDTO.setOffre(offreDTO);
        suiviAnnonceDTO.setIdConsultation(idConsultation);
        suiviAnnonceDTO.setStatut(StatutSuiviAnnonceEnum.EN_ATTENTE);
        suiviAnnonceDTO.setDateDemandeEnvoi(new Date().toInstant());
        String xml = Files.readString(new File("src/test/resources/xsl/fo/TED_ESENDERS_empty.xml").toPath());
        suiviAnnonceDTO.setXml(xml);
        suiviAnnonceDTO.setXmlConcentrateur(xml);
        ByteArrayOutputStream result = new ByteArrayOutputStream();

        assertThatExceptionOfType(PdfExportException.class).isThrownBy(() -> suiviAnnonceService.exportToDocument(suiviAnnonceDTO, result, null, true)).withMessageContainingAll(idConsultation.toString(), codeSupport).withCauseExactlyInstanceOf(PdfExportException.class).withStackTraceContaining("Document is empty");
    }

    @Test
    @Disabled
    void exportPdfV2_givenXml_shouldProduceContent() throws IOException {
        SuiviAnnonce suiviAnnonceDTO = new SuiviAnnonce();
        suiviAnnonceDTO.setStatut(StatutSuiviAnnonceEnum.EN_COURS_DE_PUBLICATION);
        suiviAnnonceDTO.setDateDebutEnvoi(new Date().toInstant());
        String xml = Files.readString(new File("src/test/resources/xsl/fo/TED_ESENDERS_correct-v2.xml").toPath());
        suiviAnnonceDTO.setXml(xml);
        suiviAnnonceDTO.setXmlConcentrateur(xml);
        ByteArrayOutputStream result = new ByteArrayOutputStream();

        suiviAnnonceService.exportToDocument(suiviAnnonceDTO, result, null, true);

        assertThat(result.size()).isNotZero();
    }

    @Test
    void exportPdfV1_givenXml_shouldProduceContent() throws IOException {
        SuiviAnnonce suiviAnnonceDTO = new SuiviAnnonce();
        suiviAnnonceDTO.setStatut(StatutSuiviAnnonceEnum.EN_COURS_DE_PUBLICATION);
        suiviAnnonceDTO.setDateDebutEnvoi(new Date().toInstant());
        String xml = Files.readString(new File("src/test/resources/xsl/fo/TED_ESENDERS_correct-v1.xml").toPath());
        suiviAnnonceDTO.setXml(xml);
        suiviAnnonceDTO.setXmlConcentrateur(xml);
        ByteArrayOutputStream result = new ByteArrayOutputStream();

        suiviAnnonceService.exportToDocument(suiviAnnonceDTO, result, null, true);

        assertThat(result.size()).isNotZero();
    }

    @Test
    @Tag("dev")
    void exportPdfV1_givenXmlAndFileOutputStream_shouldWritePdfFile() throws IOException {
        SuiviAnnonce suiviAnnonceDTO = new SuiviAnnonce();
        suiviAnnonceDTO.setStatut(StatutSuiviAnnonceEnum.PUBLIER);
        suiviAnnonceDTO.setDateEnvoi(new Date().toInstant());
        suiviAnnonceDTO.setDatePublication(new Date().toInstant());
        Support supportDTO = new Support();
        supportDTO.setLibelle("Support de test");
        Offre offreDTO = new Offre();
        offreDTO.setLibelle("Offre de test");
        suiviAnnonceDTO.setSupport(supportDTO);
        suiviAnnonceDTO.setOffre(offreDTO);
        String xml = Files.readString(new File("src/test/resources/xsl/fo/TED_ESENDERS_correct-v1.xml").toPath());
        suiviAnnonceDTO.setXml(xml);
        suiviAnnonceDTO.setXmlConcentrateur(xml);
        File pdfFile = new File("src/test/resources/xsl/fo/TED_ESENDERS_correct-v1.pdf");
        FileOutputStream result = new FileOutputStream(pdfFile);

        suiviAnnonceService.exportToDocument(suiviAnnonceDTO, result, null, true);

        assertThat(pdfFile).exists();
        pdfFile.deleteOnExit();
    }

    @Test
    @Disabled
    void exportPdfV2_givenXmlAndFileOutputStream_shouldWritePdfFile() throws IOException {
        SuiviAnnonce suiviAnnonceDTO = new SuiviAnnonce();
        suiviAnnonceDTO.setStatut(StatutSuiviAnnonceEnum.PUBLIER);
        suiviAnnonceDTO.setDateEnvoi(new Date().toInstant());
        suiviAnnonceDTO.setDatePublication(new Date().toInstant());
        Support supportDTO = new Support();
        supportDTO.setLibelle("Support de test");
        Offre offreDTO = new Offre();
        offreDTO.setLibelle("Offre de test");
        suiviAnnonceDTO.setSupport(supportDTO);
        suiviAnnonceDTO.setOffre(offreDTO);
        String xml = Files.readString(new File("src/test/resources/xsl/fo/TED_ESENDERS_correct-v2.xml").toPath());
        suiviAnnonceDTO.setXml(xml);
        suiviAnnonceDTO.setXmlConcentrateur(xml);
        File pdfFile = new File("src/test/resources/xsl/fo/TED_ESENDERS_correct-v2.pdf");
        FileOutputStream result = new FileOutputStream(pdfFile);

        suiviAnnonceService.exportToDocument(suiviAnnonceDTO, result, null, true);

        assertThat(pdfFile).exists();
        pdfFile.deleteOnExit();
    }

    @Test
    void marshal_givenXml_shouldProduceContent() throws IOException {
        SuiviAnnonce suiviAnnonce = getSuiviAnnonce();

        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(suiviAnnonce.getXml().getBytes())) {
            JAXBContext jaxbContextTed = JAXBContext.newInstance(TedEsenders.class);
            Unmarshaller jaxbUnmarshaller = jaxbContextTed.createUnmarshaller();
            JAXBElement<TedEsenders> unmarshal = (JAXBElement<TedEsenders>) jaxbUnmarshaller.unmarshal(inputStream);
            TedEsenders tedEsenders = unmarshal.getValue();
            F052014 f052014 = tedEsenders.getFORMSECTION().getF052014();
            BodyF05 contractingbody = f052014.getCONTRACTINGBODY();
            if (contractingbody == null) {
                return;
            }

            var offre = contractingbody.getOffre();
            offre.setCODE(suiviAnnonce.getOffre().getCode());
            offre.setLIBELLE(suiviAnnonce.getOffre().getLibelle());
            Marshaller jaxbMarshaller = jaxbContextTed.createMarshaller();
            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(tedEsenders, sw);
            String xmlString = sw.toString();
            suiviAnnonce.setXml(xmlString);
            suiviAnnonce.setXmlConcentrateur(xmlString);
            //Set Xml Transaction
            TRANSACTION transaction = new TRANSACTION();
            transaction.setPLATEFORME(suiviAnnonce.getIdPlatform());

            TRANSACTION.ORGANISMEACHETEUR organismeacheteur = new TRANSACTION.ORGANISMEACHETEUR();
            Sender.CONTACT contact = tedEsenders.getSENDER().getCONTACT();
            organismeacheteur.setORGANISATION(contact.getORGANISATION());
            TRANSACTION.ORGANISMEACHETEUR.PAYS pays = new TRANSACTION.ORGANISMEACHETEUR.PAYS();
            pays.setVALUE(contact.getCOUNTRY().getVALUE());
            organismeacheteur.setPAYS(pays);
            organismeacheteur.setEMAIL(contact.getEMAIL());
            transaction.setORGANISMEACHETEUR(organismeacheteur);

            TRANSACTION.COORDONNEEACHETEUR coordonneeacheteur = new TRANSACTION.COORDONNEEACHETEUR();
            ContactReview addressreviewbody = f052014.getCOMPLEMENTARYINFO().getADDRESSREVIEWBODY();
            coordonneeacheteur.setNOMOFFICIEL(addressreviewbody.getNATIONALID());
            coordonneeacheteur.setADDRESSE(addressreviewbody.getADDRESS());
            coordonneeacheteur.setVILLE(addressreviewbody.getTOWN());
            coordonneeacheteur.setPOSTALCODE(addressreviewbody.getPOSTALCODE());
            TRANSACTION.COORDONNEEACHETEUR.PAYS pays2 = new TRANSACTION.COORDONNEEACHETEUR.PAYS();
            pays2.setVALUE(addressreviewbody.getCOUNTRY().getVALUE());
            coordonneeacheteur.setPAYS(pays2);
            coordonneeacheteur.setURL(addressreviewbody.getURLBUYER());
            transaction.setCOORDONNEEACHETEUR(coordonneeacheteur);

            TRANSACTION.OFFRE offre1 = new TRANSACTION.OFFRE();
            offre1.setCODE(offre.getCODE());
            offre1.setLIBELLE(offre.getLIBELLE());
            transaction.setOFFRE(offre1);

            transaction.setOBJETCONSULTATION(f052014.getOBJECTCONTRACT().getShortDescr().getParagraphs());
            transaction.setREFERENCECONSULTATION(f052014.getOBJECTCONTRACT().getReferenceNumber());
            JAXBContext jaxbContextTransaction = JAXBContext.newInstance(TRANSACTION.class);
            Marshaller jaxbMarshallerTransaction = jaxbContextTransaction.createMarshaller();
            StringWriter swTransaction = new StringWriter();
            jaxbMarshallerTransaction.marshal(transaction, swTransaction);
            String xmlStringTransaction = swTransaction.toString();
            assertThat(xmlStringTransaction.replaceAll("\n", "")).isEqualTo("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><TRANSACTION><ORGANISME_ACHETEUR><ORGANISATION>string</ORGANISATION><PAYS VALUE=\"EU\"/><E_MAIL>example@example.com</E_MAIL></ORGANISME_ACHETEUR><COORDONNEE_ACHETEUR><ADDRESSE>1 rue Saint-Test</ADDRESSE><VILLE>Paris</VILLE><POSTAL_CODE>75001</POSTAL_CODE><PAYS VALUE=\"FR\"/></COORDONNEE_ACHETEUR><OFFRE><CODE>Code</CODE><LIBELLE>Libellé</LIBELLE></OFFRE><OBJET_CONSULTATION>AMO Concernant la recherche de clients pour les locaux commerciaux et de bureaux à la vente et/ou                        a la location                    Accord-cadre à marchés subséquents multi attributaires.</OBJET_CONSULTATION><REFERENCE_CONSULTATION>test_num</REFERENCE_CONSULTATION></TRANSACTION>");
        } catch (Exception e) {
        }
    }

    private SuiviAnnonce getSuiviAnnonce() throws IOException {
        SuiviAnnonce suiviAnnonceDTO = new SuiviAnnonce();
        suiviAnnonceDTO.setStatut(StatutSuiviAnnonceEnum.EN_COURS_DE_PUBLICATION);
        suiviAnnonceDTO.setDateDebutEnvoi(new Date().toInstant());
        Offre offre1 = new Offre();
        offre1.setCode("Code");
        offre1.setLibelle("Libellé");
        suiviAnnonceDTO.setOffre(offre1);
        String xml = Files.readString(new File("src/test/resources/xsl/fo/TED_ESENDERS_correct-v2.xml").toPath());
        suiviAnnonceDTO.setXml(xml);
        suiviAnnonceDTO.setXmlConcentrateur(xml);
        return suiviAnnonceDTO;
    }


}
