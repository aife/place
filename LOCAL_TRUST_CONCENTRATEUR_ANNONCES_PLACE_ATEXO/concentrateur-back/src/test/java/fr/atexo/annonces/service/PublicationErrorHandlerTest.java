package fr.atexo.annonces.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.integration.MessageRejectedException;
import org.springframework.messaging.Message;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Test pour l'handler d'erreur de spring integration
 */
@ExtendWith(MockitoExtension.class)
class PublicationErrorHandlerTest {

    @Mock
    private SuiviAnnonceService suiviAnnonceService;

    @Mock
    private Message<?> message;

    @InjectMocks
    private PublicationErrorHandler publicationErrorHandler;

    @Test
    void handleError_givenMessageRejectedException_shouldCallServiceWithRejetConcentrateur() {
        Integer idAnnonce = 1;
        String exceptionMessage = "Test Exception";

        publicationErrorHandler.handleError(idAnnonce, new MessageRejectedException(message, exceptionMessage));

        verify(suiviAnnonceService, times(1)).updateErrorMessage(eq(idAnnonce), eq(exceptionMessage), eq(true));
    }

    @Test
    void handleError_givenNonMessageRejectedException_shouldCallServiceWithNotRejetConcentrateur() {
        Integer idAnnonce = 1;
        String exceptionMessage = "Test Exception";

        publicationErrorHandler.handleError(idAnnonce, new RuntimeException(exceptionMessage));

        verify(suiviAnnonceService, times(1)).updateErrorMessage(eq(idAnnonce), eq(exceptionMessage), eq(false));
    }
}