package fr.atexo.annonces.mapper;

import fr.atexo.annonces.boot.entity.Departement;
import fr.atexo.annonces.builder.DepartementBuilder;
import fr.atexo.annonces.dto.DepartementDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test du mapper entre Departement et DepartementDTO
 */
@SpringBootTest(classes = {DepartementMapperImpl.class})
class DepartementMapperIntegrationTest {

    @Autowired
    private DepartementMapper departementMapper;

    @Test
    void mapListDepartement_shouldReturnMappedListDepartementDTO() {
        List<Departement> departementList = List.of(DepartementBuilder.aDepartement().withDefaults().build(),
                DepartementBuilder.aDepartement().withId(2).withCode("02").withLibelle("Autre").build());

        List<DepartementDTO> departementDTOList = departementMapper.departementsToDepartementDTOs(departementList);

        assertThat(departementDTOList)
                .hasSameSizeAs(departementList)
                .zipSatisfy(departementList, (departementDTO, departement) ->
                        assertThat(departementDTO).isEqualToComparingFieldByField(departement));
    }

}
