package fr.atexo.annonces.builder;

import fr.atexo.annonces.boot.entity.Departement;

/**
 * Builder pour Departement
 */
public final class DepartementBuilder {
    private Integer id;
    private String code;
    private String libelle;

    private DepartementBuilder() {
    }

    public static DepartementBuilder aDepartement() {
        return new DepartementBuilder();
    }

    public DepartementBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public DepartementBuilder withCode(String code) {
        this.code = code;
        return this;
    }

    public DepartementBuilder withLibelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public Departement build() {
        Departement departement = new Departement();
        departement.setId(id);
        departement.setCode(code);
        departement.setLibelle(libelle);
        return departement;
    }

    public DepartementBuilder withDefaults() {
        this.id = 1;
        this.code = "01";
        this.libelle = "Département de test";
        return this;
    }
}
