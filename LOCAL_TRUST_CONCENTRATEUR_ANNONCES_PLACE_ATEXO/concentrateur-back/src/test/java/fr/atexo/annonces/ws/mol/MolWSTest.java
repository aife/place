package fr.atexo.annonces.ws.mol;


import fr.atexo.annonces.boot.ws.mol.MolWS;
import fr.atexo.annonces.config.RestTemplateConfig;
import fr.atexo.annonces.dto.publication.AVISEMIS;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.net.util.Base64;
import org.assertj.core.api.AssertionsForInterfaceTypes;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class MolWSTest {

    private MolWS molWS = new MolWS(new RestTemplateConfig().restTemplate(), "/publication/interapp/atexoStatsPublication.do?date={date}&datefin={datefin}",
            "https://pub.preprod.marchesonline.com", "ATEXOUser", "re5uoxeta", "/publication/interapp/entermolpublication.do");

    @Test
    void Given_Begin_End_When_getUpdateContactsFromDate_Then_ReturnNotEmptyList() {

        Date dateFin = new Date();
        LocalDateTime ldt = LocalDateTime.ofInstant(dateFin.toInstant(), ZoneId.systemDefault());
        ldt = ldt.minusDays(2);
        Date date = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
        AVISEMIS avisemis = molWS.getSuiviMol(date, dateFin);
        assertNotNull(avisemis);

    }

    @Test
    void Given_xml_Then_OpenMol() {

        String actuel = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<IACTX>\n" +
                "    <TSTP>04/09/2024 15:38:39</TSTP>\n" +
                "    <ACT>0</ACT>\n" +
                "    <IDTF>\n" +
                "        <LGN>MPE_placeorme_recette#pmi-min-1#ABE_21062023##39317</LGN>\n" +
                "        <PWD>MPE_placeorme_recette#pmi-min-1#ABE_21062023##39317</PWD>\n" +
                "        <PRF>0</PRF>\n" +
                "        <WKF>1</WKF>\n" +
                "    </IDTF>\n" +
                "    <CTX>\n" +
                "        <IDC>MPE_placeorme_recette#pmi-min-1#ABE_21062023##501568</IDC>\n" +
                "        <TIT>ABE_29122022_test_tncp_allotie_dce</TIT>\n" +
                "        <REF_PP>ABE_29122022_tncp_allotie_dce</REF_PP>\n" +
                "        <DMT>0</DMT>\n" +
                "        <URL_CSL>![CDATA[https://recette.marches-publics.gouv.fr/entreprise/consultation/501568?orgAcronyme=pmi-min-1]]</URL_CSL>\n" +
                "        <PROV>90</PROV>\n" +
                "        <THEME_GRAPHIQUE>0</THEME_GRAPHIQUE>\n" +
                "        <PERS>\n" +
                "            <CIV>M.</CIV>\n" +
                "            <NOM>Dupond_02</NOM>\n" +
                "            <PRENOM>Patrick</PRENOM>\n" +
                "            <IDPM>0</IDPM>\n" +
                "            <IDPMM>pmi-min-1</IDPMM>\n" +
                "            <AG>![CDATA[0]]</AG>\n" +
                "            <AGSC>![CDATA[0]]</AGSC>\n" +
                "            <DEL>![CDATA[0]]</DEL>\n" +
                "            <RS>Organisme de formation</RS>\n" +
                "            <ADR>70, rue de Varenne</ADR>\n" +
                "            <CP>75007</CP>\n" +
                "            <VIL>Paris</VIL>\n" +
                "            <EMAIL>mathieu.michaud@atexo.com</EMAIL>\n" +
                "            <SRT>44090956200025</SRT>\n" +
                "            <OKEMAILGM>0</OKEMAILGM>\n" +
                "            <OKEMAILPART>0</OKEMAILPART>\n" +
                "        </PERS>\n" +
                "    </CTX>\n" +
                "    <CMPLT>\n" +
                "        <UID_PF>MPE_placeorme_recette</UID_PF>\n" +
                "        <ORGANISME>pmi-min-1</ORGANISME>\n" +
                "    </CMPLT>\n" +
                "</IACTX>";
        String xmlToken = org.apache.commons.net.util.Base64.encodeBase64String(actuel.getBytes());
        String textToHash = xmlToken + "ATXIDFSecretKey";
        byte[] cleBytes = textToHash.getBytes(StandardCharsets.ISO_8859_1);
        byte[] hashed = DigestUtils.getSha1Digest().digest(cleBytes);
        String xmlSignature = Base64.encodeBase64StringUnChunked(hashed);
        String xmlBoamp = Base64.encodeBase64StringUnChunked(("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<ListeAnnonces>\n" +
                "    <emetteur>\n" +
                "        <login>ABE_21062023</login>\n" +
                "        <mel>ABE_21062023@atexo.com</mel>\n" +
                "        <password>ABE_21062023_mdp</password>\n" +
                "    </emetteur>\n" +
                "    <generateur>\n" +
                "        <idAppli>MPE3.000</idAppli>\n" +
                "        <versionAppli>MPE 3.0</versionAppli>\n" +
                "    </generateur>\n" +
                "    <ann>\n" +
                "        <id_AnnEmetteur>39317</id_AnnEmetteur>\n" +
                "        <organisme>\n" +
                "            <correspondantPRM>\n" +
                "                <nom>Dupond_02</nom>\n" +
                "                <pren>Patrick</pren>\n" +
                "                <fonc></fonc>\n" +
                "            </correspondantPRM>\n" +
                "        </organisme>\n" +
                "        <natureMarche>\n" +
                "            <Travaux></Travaux>\n" +
                "        </natureMarche>\n" +
                "        <description>\n" +
                "            <objet>ABE_29122022_test_tncp_allotie_dce</objet>\n" +
                "            <CPV>\n" +
                "                <ObjetPrincipal/>\n" +
                "            </CPV>\n" +
                "        </description>\n" +
                "        <caracteristiques>\n" +
                "            <principales>ABE_29122022_test_tncp_allotie_dce</principales>\n" +
                "        </caracteristiques>\n" +
                "        <delais>\n" +
                "            <ReceptionOffres>2030-08-29T15:30Z[UTC]</ReceptionOffres>\n" +
                "        </delais>\n" +
                "        <renseignements>\n" +
                "            <idMarche>ABE_29122022_tncp_allotie_dce</idMarche>\n" +
                "        </renseignements>\n" +
                "    </ann>\n" +
                "</ListeAnnonces>\n").getBytes());
        ResponseEntity<String> result = molWS.openMOL(xmlToken, xmlSignature, xmlBoamp);
        assertNotNull(result);


    }

}
