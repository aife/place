package fr.atexo.annonces.mapper;

import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.boot.entity.SuiviTypeAvisPub;
import fr.atexo.annonces.builder.SuiviAnnonceBuilder;
import fr.atexo.annonces.dto.SuiviAnnonceDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.time.ZoneId;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class SuiviAnnonceMapperMockitoTest {

    @Mock
    private SuiviAnnonce suiviAnnonce;

    @Mock
    private OffreMapper offreMapper;

    @Mock
    private SuiviAvisPublieMapper suiviAvisPublieMapper;

    @InjectMocks
    private SuiviAnnonceMapperImpl suiviAnnonceMapper;

    @Test
    void when_SuiviAnnonceto_thenReturnSuiviAnnonceDTO() {

        SuiviAnnonce suiviAnnonce = SuiviAnnonceBuilder.aSuiviAnnonce().withDefaults().build();
        SuiviTypeAvisPub suiviTypeAvisPub = new SuiviTypeAvisPub();
        suiviTypeAvisPub.setDateMiseEnLigneCalcule(LocalDateTime.now().atZone(ZoneId.systemDefault()));
        suiviAnnonce.setTypeAvisPub(suiviTypeAvisPub);

        lenient().when(offreMapper.offreToOffreDTO(any())).thenReturn(null);
        lenient().when(suiviAvisPublieMapper.mapToModels(any())).thenReturn(null);

        SuiviAnnonceDTO suiviAnnonceDTO = suiviAnnonceMapper.suiviAnnonceToSuiviAnnonceDTO(suiviAnnonce, false);

        assertThat(suiviAnnonceDTO.getDateMiseEnLigneCalcule()).isEqualTo(suiviAnnonce.getTypeAvisPub().getDateMiseEnLigneCalcule());
    }
}
