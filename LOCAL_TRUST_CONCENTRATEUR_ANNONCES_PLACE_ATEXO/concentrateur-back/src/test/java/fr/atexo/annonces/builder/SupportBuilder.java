package fr.atexo.annonces.builder;

import fr.atexo.annonces.boot.entity.Departement;
import fr.atexo.annonces.boot.entity.Offre;
import fr.atexo.annonces.boot.entity.Support;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Builder pour Support
 */
public final class SupportBuilder {
    private Integer id;
    private String libelle;
    private String code;
    private String codeGroupe;
    private boolean external;
    private boolean europeen;
    private boolean actif;
    private boolean choixUnique;
    private Set<Offre> offres;
    private String description;
    private String url;
    private int poids;
    private boolean national;
    private Set<Departement> jal;
    private Set<Departement> pqr;
    private String codeDestination;

    private SupportBuilder() {
    }

    public static SupportBuilder aSupport() {
        return new SupportBuilder();
    }

    public SupportBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public SupportBuilder withLibelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public SupportBuilder withCode(String code) {
        this.code = code;
        return this;
    }

    public SupportBuilder withCodeGroupe(String codeGroupe) {
        this.codeGroupe = codeGroupe;
        return this;
    }

    public SupportBuilder withExternal(boolean external) {
        this.external = external;
        return this;
    }

    public SupportBuilder withActif(boolean actif) {
        this.actif = actif;
        return this;
    }

    public SupportBuilder withOffres(Set<Offre> offres) {
        this.offres = offres;
        return this;
    }

    public SupportBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public SupportBuilder withUrl(String url) {
        this.url = url;
        return this;
    }

    public SupportBuilder withPoids(int poids) {
        this.poids = poids;
        return this;
    }

    public SupportBuilder withNational(boolean national) {
        this.national = national;
        return this;
    }

    public SupportBuilder withJal(Set<Departement> jal) {
        this.jal = jal;
        return this;
    }

    public SupportBuilder withPqr(Set<Departement> pqr) {
        this.pqr = pqr;
        return this;
    }

    public SupportBuilder withChoixUnique(boolean choixUnique) {
        this.choixUnique = choixUnique;
        return this;
    }

    public SupportBuilder withEuropeen(boolean europeen) {
        this.europeen = europeen;
        return this;
    }

    public SupportBuilder withCodeDestination(String codeDestination) {
        this.codeDestination = codeDestination;
        return this;
    }

    public Support build() {
        Support support = new Support();
        support.setId(id);
        support.setLibelle(libelle);
        support.setCode(code);
        support.setCodeGroupe(codeGroupe);
        support.setExternal(external);
        support.setActif(actif);
        support.setChoixUnique(actif);
        support.setEuropeen(europeen);
        support.setOffres(offres);
        support.setDescription(description);
        support.setUrl(url);
        support.setPoids(poids);
        support.setNational(national);
        support.setJal(jal);
        support.setPqr(pqr);
        support.setCodeDestination(codeDestination);
        return support;
    }

    public SupportBuilder withDefaults() {
        this.id = 1;
        this.libelle = "Support de test";
        this.code = "SUPPORT_TEST";
        this.codeGroupe = "SUPPORT_TEST_GROUP";
        this.external = false;
        this.europeen = false;
        this.choixUnique = true;
        this.actif = true;
        Set<Offre> offreSet = new HashSet<>();
        offreSet.add(OffreBuilder.anOffre().withDefaults().build());
        offreSet.add(OffreBuilder.anOffre().withDefaults().withId(2).build());
        this.offres = offreSet;
        this.description = "Description de test";
        this.url = "test.com";
        this.poids = 1;
        this.choixUnique = true;
        this.national = false;
        this.jal = new HashSet<>();
        this.pqr = new HashSet<>();
        this.codeDestination = "TEST_DESTINATION";
        return this;
    }
}
