package fr.atexo.annonces.ws.viewer;


import fr.atexo.annonces.boot.ws.viewer.ViewerConfiguration;
import fr.atexo.annonces.boot.ws.viewer.ViewerWS;
import fr.atexo.annonces.config.RestTemplateConfig;
import fr.atexo.annonces.dto.viewer.ViewerDTO;
import org.apache.commons.io.FileUtils;
import org.apache.commons.net.util.Base64;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@Disabled
class ViewerWSTest {
    private ViewerConfiguration viewerConfiguration = ViewerConfiguration.builder()
            .basePath("https://viewer.preview.ted.europa.eu")
            .apiKey("d7b10c46f4f94b7b891007e639ed62a8")
            .ws(Map.of("render", "/v1/render"))
            .build();
    private ViewerWS viewerWS = new ViewerWS(new RestTemplateConfig().restTemplate(), viewerConfiguration);

    @Test
    void getReportPdf() throws IOException {

        File notice = new File("src/test/resources/data/cn_24_minimal.xml");
        byte[] binaryData = Files.readString(notice.toPath()).getBytes();
        Resource avisemis = viewerWS.reportPDF(binaryData, ViewerDTO.builder()
                .summary(false)
                .format("PDF")
                .language("fr")
                .build(), null);
        assertNotNull(avisemis);
        FileUtils.copyInputStreamToFile(avisemis.getInputStream(), new File("src/test/resources/data/ted.pdf"));

    }

    @Test
    void getReportHtml() throws IOException {

        File notice = new File("src/test/resources/data/cn_24_minimal.xml");
        String avisemis = viewerWS.reportHtml(Files.readString(notice.toPath()).getBytes(), ViewerDTO.builder()
                .summary(false)
                .format("HTML")
                .language("fr")
                .build());
        assertNotNull(avisemis);
        FileUtils.writeStringToFile(new File("src/test/resources/data/ted.html"), avisemis, StandardCharsets.UTF_8);

    }

}
