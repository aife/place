package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.entity.Support;
import fr.atexo.annonces.boot.repository.ConsultationFiltreRepository;
import fr.atexo.annonces.boot.repository.PlateformeConfigurationRepository;
import fr.atexo.annonces.boot.repository.SupportRepository;
import fr.atexo.annonces.dto.SupportDTO;
import fr.atexo.annonces.mapper.ConsultationFiltreMapper;
import fr.atexo.annonces.mapper.SupportMapper;
import fr.atexo.annonces.modeles.SupportSpecification;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Tests pour la couche service des Support
 */
@ExtendWith(MockitoExtension.class)
class SupportServiceImplTest {

    @Mock
    private SupportRepository supportRepository;
    @Mock
    private ConsultationFiltreRepository consultationFiltreRepository;

    @Mock
    private SupportMapper supportMapper;
    @Mock
    private ConsultationFiltreMapper consultationFiltreMapper;
    @Mock
    private PlateformeConfigurationRepository plateformeConfigurationRepository;

    @InjectMocks
    private SupportServiceImpl supportService;

    @Test
    void getSupports_shouldReturnSupportDTO() {
        List<Support> supports = Arrays.asList(new Support(), new Support());
        List<SupportDTO> supportDTOS = Arrays.asList(new SupportDTO(), new SupportDTO());
        boolean includeOffres = true;
        when(supportRepository.findAll(any(SupportSpecification.class))).thenReturn(supports);
        when(supportMapper.supportsToSupportDTOs(supports, includeOffres)).thenReturn(supportDTOS);

        List<SupportDTO> allSupports = supportService.getSupports(0D, null, null, null, null, includeOffres, "mpe_release", 1, null, null, null, null, null);

        assertThat(allSupports).isEqualTo(supportDTOS);
    }

    @Test
    void getSupports_givenEmptySearchResult_shouldReturnEmptyList() {
        List<Support> supports = Collections.emptyList();
        List<SupportDTO> supportDTOS = Collections.emptyList();
        boolean includeOffres = true;
        when(supportRepository.findAll(any(SupportSpecification.class))).thenReturn(supports);
        when(supportMapper.supportsToSupportDTOs(supports, includeOffres)).thenReturn(supportDTOS);

        List<SupportDTO> supportFiltres = supportService.getSupports(0D, null, null, null, null, includeOffres, "mpe_release", 1, null, null, null, null, null);

        assertThat(supportFiltres).isEqualTo(supportDTOS);
    }

}
