package fr.atexo.annonces.reseau;

import fr.atexo.annonces.boot.core.domain.e_forms_sdk.model.FieldDetails;
import fr.atexo.annonces.config.E2EAbstractResource;
import org.junit.jupiter.api.*;
import org.springframework.http.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Disabled
class EFormsSdkControllerTest extends E2EAbstractResource {

    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();
    }


    @Test
    @Order(1)
    @DisplayName("1- Vérifier que le ws de sdk i18n est OK avec une requête valide")
    void Given_ValidRequest_When_getI18nSdk_Then_ReturnOk() throws Exception {
        //Given

        String stubId = "GET_SDK_I18N";

        //When
        HttpHeaders httpHeaders = getBearerHeader();
        ResponseEntity<Object> result = testRestTemplate.exchange(BASE_URL + port + PATH_EFORMS_SDK + "/sdk-i18n/fr",
                HttpMethod.GET,
                new HttpEntity<>(httpHeaders), Object.class);
        //Then
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
        assertJsonEqual(stubId, result.getBody());
    }

    @Test
    @Order(2)
    @DisplayName("2- Vérifier que le ws de champs détails est OK avec une requête valide")
    void Given_ValidRequest_When_getFieldById_Then_ReturnOk() throws Exception {
        //Given

        String stubId = "GET_FIELD";

        //When
        HttpHeaders httpHeaders = getBearerHeader();
        ResponseEntity<FieldDetails> result = testRestTemplate.exchange(BASE_URL + port + PATH_EFORMS_SDK + "/fields/BT-739-Organization-TouchPoint",
                HttpMethod.GET,
                new HttpEntity<>(httpHeaders), FieldDetails.class);
        //Then
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
        assertJsonEqual(stubId, result.getBody());
    }

    @Test
    @Order(3)
    @DisplayName("3- Vérifier que le ws de champs détails est OK avec une requête valide")
    void Given_ValidRequest_When_getCodeListById_Then_ReturnOk() throws Exception {
        //Given

        String stubId = "GET_CODES";
        String search = "Avis de";
        //When
        HttpHeaders httpHeaders = getBearerHeader();
        ResponseEntity<Object> result = testRestTemplate.exchange(BASE_URL + port + PATH_EFORMS_SDK + "/codes/notice-type?lang=fr&search=" + search,
                HttpMethod.GET,
                new HttpEntity<>(httpHeaders), Object.class);
        //Then
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
        assertJsonEqual(stubId, result.getBody());
    }
    @Test
    @Order(4)
    @DisplayName("4- Vérifier que le ws de i18n est OK avec une requête valide")
    void Given_ValidRequest_When_getI18n_Then_ReturnOk() throws Exception {
        //Given

        String stubId = "GET_I18N";

        //When
        HttpHeaders httpHeaders = getBearerHeader();
        ResponseEntity<Object> result = testRestTemplate.exchange(BASE_URL + port + PATH_EFORMS_SDK + "/i18n/fr",
                HttpMethod.GET,
                new HttpEntity<>(httpHeaders), Object.class);
        //Then
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
        assertJsonEqual(stubId, result.getBody());
    }


}
