package fr.atexo.annonces.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MpeToken {
    private String token;
    @JsonProperty("refresh_token")
    private String refreshToken;
}
