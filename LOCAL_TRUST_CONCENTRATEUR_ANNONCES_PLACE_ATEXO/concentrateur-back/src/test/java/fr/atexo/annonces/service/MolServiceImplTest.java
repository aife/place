package fr.atexo.annonces.service;

import com.atexo.annonces.commun.mpe.ConsultationMpe;
import com.atexo.annonces.commun.mpe.OrganismeMpe;
import fr.atexo.annonces.boot.entity.AcheteurEntity;
import fr.atexo.annonces.boot.entity.Agent;
import fr.atexo.annonces.boot.entity.Organisme;
import fr.atexo.annonces.boot.entity.tncp.SuiviAvisPublie;
import fr.atexo.annonces.boot.repository.AcheteurRepository;
import fr.atexo.annonces.boot.ws.mol.MolWS;
import fr.atexo.annonces.dto.ConfigurationConsultationDTO;
import fr.atexo.annonces.service.exception.AtexoException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class MolServiceImplTest {

    @Mock
    private MolWS molWS;
    @Mock
    private AcheteurRepository acheteurRepository;
    @Mock
    private AgentService agentService;

    @InjectMocks
    private MolServiceImpl molService;


    @Test
    void Given_compteAndConfigAndOrganismeId_Then_OpenMol() throws URISyntaxException {

        String compte = "grid.aca@finances.gouv.fr";
        Organisme org = Organisme.builder()
                .plateformeUuid("MPE_atexo_develop")
                .id(1)
                .build();
        Agent agent = Agent.builder()
                .organisme(org)
                .build();
        AcheteurEntity acheteur = AcheteurEntity.builder()
                .organisme(org)
                .email("grid.aca@finances.gouv.fr")
                .actif(true)
                .id(1L)
                .password("id")
                .login("1")
                .build();
        OrganismeMpe orgMPE = new OrganismeMpe();
        orgMPE.setAcronyme("pmi-min-1");
        orgMPE.setDenomination("denomination");
        ConsultationMpe consultation = new ConsultationMpe();
        consultation.setId(501606);
        consultation.setIntitule("test");
        consultation.setReference("ref");
        consultation.setUrlConsultation("url");
        HttpHeaders headers = new HttpHeaders();
        URI location = new URI("test");
        headers.setLocation(location);
        String res = "reponseOK";
        // when(molWS.openMOL(anyString(), anyString(), anyString())).thenReturn(res);
        //String result = molService.openMOL(compte, config);
        //assertEquals("reponseOK", result);
    }

    @Test
    void Given_ContexteWithoutOrganisme_Then_ThrowException() {
        ConfigurationConsultationDTO config = ConfigurationConsultationDTO.with().build();
        assertThatExceptionOfType(AtexoException.class)
                .isThrownBy(() -> molService.envoyer(config, SuiviAvisPublie.with()
                        .acheteur(AcheteurEntity.builder().build())
                        .build(), null, null, true))
                .withMessageContainingAll("Données de l'organisme dans ConfigurationConsultationDTO manquantes.");
    }


}
