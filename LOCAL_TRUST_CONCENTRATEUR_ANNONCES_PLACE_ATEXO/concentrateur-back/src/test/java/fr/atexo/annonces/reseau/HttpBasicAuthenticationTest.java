package fr.atexo.annonces.reseau;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test de la bonne génération de la valeur de l'header Authorization en HTTP en mode Basic
 */
class HttpBasicAuthenticationTest {

    @Test
    void getHeaderValue_givenUserPassword_shouldReturnEncodedHttpAuthorizationHeaderValue() {
        HttpBasicAuthentication basicAuthentication = new HttpBasicAuthentication("user", "motdepasse");

        assertThat(basicAuthentication.getHeaderValue()).isEqualTo("Basic dXNlcjptb3RkZXBhc3Nl");
    }
}