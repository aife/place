package fr.atexo.annonces.service;

import fr.atexo.annonces.boot.entity.Departement;
import fr.atexo.annonces.boot.repository.DepartementRepository;
import fr.atexo.annonces.dto.DepartementDTO;
import fr.atexo.annonces.mapper.DepartementMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * Tests pour la couche service des départements
 */
@ExtendWith(MockitoExtension.class)
class DepartementServiceImplTest {

    @Mock
    private DepartementRepository departementRepository;

    @Mock
    private DepartementMapper departementMapper;

    @InjectMocks
    private DepartementServiceImpl departementService;

    @Test
    void getDepartements_shouldReturnDepartementDTOs() {
        List<Departement> departements = Arrays.asList(new Departement(), new Departement());
        List<DepartementDTO> departementDTOS = Arrays.asList(new DepartementDTO(), new DepartementDTO());
        when(departementRepository.findAll()).thenReturn(departements);
        when(departementMapper.departementsToDepartementDTOs(departements)).thenReturn(departementDTOS);

        List<DepartementDTO> allDepartements = departementService.getDepartements();

        assertThat(allDepartements).isEqualTo(departementDTOS);
    }

}