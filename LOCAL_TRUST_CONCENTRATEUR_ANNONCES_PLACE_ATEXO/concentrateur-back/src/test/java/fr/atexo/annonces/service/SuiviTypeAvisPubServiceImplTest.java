package fr.atexo.annonces.service;

import com.atexo.annonces.commun.mpe.ConsultationMpe;
import com.atexo.annonces.commun.mpe.OrganismeMpe;
import fr.atexo.annonces.boot.entity.*;
import fr.atexo.annonces.boot.repository.*;
import fr.atexo.annonces.boot.ws.mpe.model.ConsultationContexte;
import fr.atexo.annonces.dto.*;
import fr.atexo.annonces.mapper.SuiviTypeAvisPubMapper;
import fr.atexo.annonces.mapper.SuiviTypeAvisPubPieceJointeAssoMapper;
import fr.atexo.annonces.modeles.StatutSuiviAnnonceEnum;
import fr.atexo.annonces.modeles.StatutTypeAvisAnnonceEnum;
import fr.atexo.annonces.service.exception.AtexoException;
import fr.atexo.annonces.service.exception.ExceptionEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class SuiviTypeAvisPubServiceImplTest {

    @Mock
    private AgentService agentService;
    @Mock
    private OrganismeService organismeService;
    @Mock
    private SuiviAnnonceService suiviAnnonceService;
    @Mock
    private PieceJointeService pieceJointeService;
    @Mock
    private SuiviTypeAvisPubRepository suiviTypeAvisPubRepository;
    @Mock
    private SuiviAnnonceRepository suiviAnnonceRepository;
    @Mock
    private PlateformeConfigurationRepository plateformeConfigurationRepository;
    @Mock
    private EFormsFormulaireRepository eFormsFormulaireRepository;
    @Mock
    private OffreRepository offreRepository;
    @Mock
    private SuiviTypeAvisPubPieceJointeAssoRepository suiviTypeAvisPubPieceJointeAssoRepository;
    @Mock
    private SuiviTypeAvisPubPieceJointeAssoMapper suiviTypeAvisPubPieceJointeAssoMapper;
    @Mock
    private SuiviTypeAvisPubMapper suiviTypeAvisPubMapper;
    @InjectMocks
    private SuiviTypeAvisPubServiceImpl suiviTypeAvisPubService;

    private SuiviTypeAvisPub avisPubEntity;

    @BeforeEach
    void setUp() {
        Organisme organisme = Organisme.builder().plateformeUuid("plateforme").build();


        OrganismeMpe organismeMpe = new OrganismeMpe();
        organismeMpe.setAcronyme("acronyme");


        Offre offreEntity = Offre.builder().libelle("libelleOffre").code("codeOffre").build();


        avisPubEntity = SuiviTypeAvisPub.builder().id(1L).objet("objet").idConsultation(1).reference("ref").europeen(true).titre("titre").offreRacine(offreEntity).organisme(organisme).build();
    }

    @Test
    void Given_AgentHorsPerimetre_When_GetDocuments_Then_ThrowError() {
        Organisme organisme = Organisme.builder().plateformeUuid("plateforme").build();
        Agent agent = Agent.builder().organisme(organisme).build();
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        String token = "tokentest";
        when(suiviTypeAvisPubRepository.findById(anyLong())).thenReturn(java.util.Optional.ofNullable(avisPubEntity));
        AtexoException exception = assertThrows(AtexoException.class, () -> {
            suiviTypeAvisPubService.getDocuments(1L, idPlatform, idConsultation, token);
        });
        assertEquals("Hors périmetre", exception.getMessage());
    }

    @Test
    void When_GetDocuments_ThenGetDocuments() {
        String token = "tokentest";
        List<SuiviTypeAvisPubPieceJointeAssoDTO> assoList = new ArrayList<>();
        SuiviTypeAvisPubPieceJointeAssoDTO asso = SuiviTypeAvisPubPieceJointeAssoDTO.builder().id(1L).build();
        assoList.add(asso);
        List<SuiviTypeAvisPubPieceJointeAssoEntity> assoEntityList = new ArrayList<>();
        when(suiviTypeAvisPubRepository.findById(anyLong())).thenReturn(java.util.Optional.ofNullable(avisPubEntity));
        when(suiviTypeAvisPubPieceJointeAssoRepository.findAllBySuiviTypeAvisPubId(anyLong())).thenReturn(assoEntityList);
        when(suiviTypeAvisPubPieceJointeAssoMapper.mapToModels(anyCollection())).thenReturn(assoList);
        List<SuiviTypeAvisPubPieceJointeAssoDTO> result = suiviTypeAvisPubService.getDocuments(1L, avisPubEntity.getOrganisme().getPlateformeUuid(),
                avisPubEntity.getIdConsultation(), token);
        assertEquals(1, result.size());
        assertEquals(1L, result.get(0).getId());
    }

    @Test
    void Given_AgentHorsPerimetre_When_DeleteById_Then_ThrowError() {
        Organisme organisme = Organisme.builder().plateformeUuid("plateforme").build();
        Agent agent = Agent.builder().organisme(organisme).build();
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        when(suiviTypeAvisPubRepository.findById(anyLong())).thenReturn(Optional.of(avisPubEntity));
        AtexoException exception = assertThrows(AtexoException.class, () -> {
            suiviTypeAvisPubService.deletePieceJointeByIdTypeAvisAndIdAsso(1L, 1L, idPlatform, idConsultation);
        });
        assertEquals("Hors périmetre", exception.getMessage());
    }

    @Test
    void Given_AgentHorsPerimetre_When_ImporterDocument_Then_ThrowError() throws FileNotFoundException {


        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        PieceJointe pieceJointe = PieceJointe.builder().build();
        InputStream inputStream = new FileInputStream("src/test/resources/data/pub.xml");
        when(suiviTypeAvisPubRepository.findById(anyLong())).thenReturn(Optional.ofNullable(avisPubEntity));
        AtexoException exception = assertThrows(AtexoException.class, () -> {
            suiviTypeAvisPubService.importerDocument(1L, idPlatform, idConsultation, pieceJointe, inputStream);
        });
        assertEquals("Hors périmetre", exception.getMessage());
    }

    @Test
    void Given_InvalidIdTypeAvis_When_ImporterDocument_Then_ThrowError() throws FileNotFoundException {
        Organisme organisme = Organisme.builder().plateformeUuid("PLATEFORME_TEST").build();
        Agent agent = Agent.builder().organisme(organisme).build();
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        PieceJointe pieceJointe = PieceJointe.builder().build();
        InputStream inputStream = new FileInputStream("src/test/resources/data/pub.xml");
        AtexoException exception = new AtexoException(ExceptionEnum.NOT_FOUND, "Facturation non trouvée");
        when(suiviTypeAvisPubRepository.findById(anyLong())).thenThrow(exception);
        AtexoException exception2 = assertThrows(AtexoException.class, () -> {
            suiviTypeAvisPubService.importerDocument(1L, idPlatform, idConsultation, pieceJointe, inputStream);
        });
        assertEquals("Facturation non trouvée", exception2.getMessage());
    }


    @Test
    void Given_AgentHorsPerimetre_When_UpdateMail_Then_ThrowError() {
        Organisme organisme = Organisme.builder().plateformeUuid("plateforme").build();
        Agent agent = Agent.builder().organisme(organisme).build();
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        MailUpdate update = MailUpdate.builder().contenu("contenu").objet("objet").build();
        when(suiviTypeAvisPubRepository.findById(anyLong())).thenReturn(java.util.Optional.ofNullable(avisPubEntity));
        AtexoException exception = assertThrows(AtexoException.class, () -> {
            suiviTypeAvisPubService.updateMail(1L, update, idPlatform, idConsultation);
        });
        assertEquals("Hors périmetre", exception.getMessage());
    }

    @Test
    void Given_AgentHorsPerimetre_When_ImporterAvisExterne_Then_ThrowError() throws FileNotFoundException {
        Organisme organisme = Organisme.builder().plateformeUuid("plateforme").build();
        Agent agent = Agent.builder().organisme(organisme).build();
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        String token = "tokentest";
        PieceJointe pieceJointe = PieceJointe.builder().build();
        InputStream inputStream = new FileInputStream("src/test/resources/data/pub.xml");
        when(suiviTypeAvisPubRepository.findById(anyLong())).thenReturn(java.util.Optional.ofNullable(avisPubEntity));
        AtexoException exception = assertThrows(AtexoException.class, () -> {
            suiviTypeAvisPubService.importerAvisExterne(1L, idPlatform, idConsultation, token, pieceJointe, inputStream);
        });
        assertEquals("Hors périmetre", exception.getMessage());
    }

    @Test
    void Given_InvalidIdTypeAvis_When_ImporterAvisExterne_Then_ThrowError() throws FileNotFoundException {
        Organisme organisme = Organisme.builder().plateformeUuid("PLATEFORME_TEST").build();
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        String token = "tokentest";
        PieceJointe pieceJointe = PieceJointe.builder().build();
        InputStream inputStream = new FileInputStream("src/test/resources/data/pub.xml");
        AtexoException exception = new AtexoException(ExceptionEnum.NOT_FOUND, "Facturation non trouvée");
        when(suiviTypeAvisPubRepository.findById(anyLong())).thenThrow(exception);
        AtexoException exception2 = assertThrows(AtexoException.class, () -> {
            suiviTypeAvisPubService.importerAvisExterne(1L, idPlatform, idConsultation, token, pieceJointe, inputStream);
        });
        assertEquals("Facturation non trouvée", exception2.getMessage());
    }

    @Test
    void When_ImporterAvisExterne_Then_AddAvisExterne() throws FileNotFoundException {
        Organisme organisme = Organisme.builder().plateformeUuid("PLATEFORME_TEST").build();
        Agent agent = Agent.builder().organisme(organisme).build();
        String idPlatform = "PLATEFORME_TEST";
        Integer idConsultation = 1;
        String token = "tokentest";
        PieceJointe pieceJointe = PieceJointe.builder().build();
        InputStream inputStream = new FileInputStream("src/test/resources/data/pub.xml");
        SuiviTypeAvisPub avisPub = SuiviTypeAvisPub.builder().creerPar(agent).organisme(organisme).modifierPar(agent).annonces(List.of(SuiviAnnonce.builder().support(Support.builder().europeen(true).build()).build())).idConsultation(idConsultation).build();
        SuiviTypeAvisPubDTO avis = SuiviTypeAvisPubDTO.builder().id(1L).creerPar(AgentDTO.builder().organisme(OrganismeResumeDTO.builder().plateformeUuid(idPlatform).build()).build()).build();

        when(pieceJointeService.save(any(), any())).thenReturn(pieceJointe);
        when(suiviTypeAvisPubRepository.findById(anyLong())).thenReturn(Optional.ofNullable(avisPub));
        when(suiviTypeAvisPubMapper.mapToModel(any())).thenReturn(avis);
        SuiviTypeAvisPubDTO result = suiviTypeAvisPubService.importerAvisExterne(1L, idPlatform, idConsultation, token, pieceJointe, inputStream);
        assertEquals(1L, result.getId());
        assertEquals("PLATEFORME_TEST", result.getCreerPar().getOrganisme().getPlateformeUuid());
    }

    @Test
    void Given_InvalidDataForGetConfiguration_When_DemandeValidation_Then_ThrowError() {
        when(agentService.getConfiguration(anyString(), anyInt(), any())).thenReturn(null);
        AtexoException exception = assertThrows(AtexoException.class, () -> {
            suiviTypeAvisPubService.demandeValidation(1L, 2, "platformId", "token");
        });
        assertEquals("Aucun contexte est associé à cette cette consultation", exception.getMessage());
    }

    @Test
    public void Given_ConsultationWithoutSuiviTypeAvisPub_When_DemandeValidation_Then_ThrowError() {
        ConfigurationConsultationDTO config = new ConfigurationConsultationDTO();
        ConsultationContexte contexte = new ConsultationContexte();
        ConsultationMpe consultation = new ConsultationMpe();
        contexte.setConsultation(consultation);
        config.setContexte(contexte);
        when(agentService.getConfiguration(anyString(), anyInt(), any())).thenReturn(config);
        when(suiviTypeAvisPubRepository.findById(anyLong())).thenReturn(java.util.Optional.empty());
        AtexoException exception = assertThrows(AtexoException.class, () -> {
            suiviTypeAvisPubService.demandeValidation(1L, 2, "platformId", "token");
        });
        assertEquals("Avis non trouvée " + 1, exception.getMessage());
    }

    @Test
    public void Given_AvisHorsPerimetre_When_DemandeValidation_Then_ThrowError() {
        ConfigurationConsultationDTO config = new ConfigurationConsultationDTO();
        ConsultationContexte contexte = new ConsultationContexte();
        ConsultationMpe consultation = new ConsultationMpe();
        contexte.setConsultation(consultation);
        config.setContexte(contexte);
        Organisme organisme = Organisme.builder().plateformeUuid("PLATEFORME_TEST2").build();
        Agent agent = Agent.builder().organisme(organisme).build();
        SuiviTypeAvisPub typeAvisPub = SuiviTypeAvisPub.builder().organisme(organisme).build();
        when(agentService.getConfiguration(anyString(), anyInt(), any())).thenReturn(config);
        when(suiviTypeAvisPubRepository.findById(anyLong())).thenReturn(java.util.Optional.ofNullable(typeAvisPub));
        AtexoException exception = assertThrows(AtexoException.class, () -> {
            suiviTypeAvisPubService.demandeValidation(1L, 2, "PLATEFORME_TEST", "tokentest");
        });
        assertEquals("Hors périmetre", exception.getMessage());
    }

    @Test
    public void Given_InvalidStatut_When_DemandeValidation_Then_ThrowError() {
        ConfigurationConsultationDTO config = new ConfigurationConsultationDTO();
        ConsultationContexte contexte = new ConsultationContexte();
        ConsultationMpe consultation = new ConsultationMpe();
        contexte.setConsultation(consultation);
        config.setContexte(contexte);
        Organisme organisme = Organisme.builder().plateformeUuid("PLATEFORME_TEST").build();
        SuiviTypeAvisPub typeAvisPub = SuiviTypeAvisPub.builder().idConsultation(2).organisme(organisme).statut(StatutTypeAvisAnnonceEnum.EN_ATTENTE_VALIDATION_SIP).build();
        when(agentService.getConfiguration(anyString(), anyInt(), any())).thenReturn(config);
        when(suiviTypeAvisPubRepository.findById(anyLong())).thenReturn(java.util.Optional.ofNullable(typeAvisPub));
        AtexoException exception = assertThrows(AtexoException.class, () -> {
            suiviTypeAvisPubService.demandeValidation(1L, 2, "PLATEFORME_TEST", "tokentest");
        });
        assertEquals("Avis en cours de validation", exception.getMessage());
    }

    @Test
    public void Given_InvalidDataForConsultation_When_DemandeValidation_Then_ThrowError() {
        ConfigurationConsultationDTO config = new ConfigurationConsultationDTO();
        ConsultationContexte contexte = new ConsultationContexte();
        ConsultationMpe consultation = new ConsultationMpe();
        contexte.setConsultation(consultation);
        config.setContexte(contexte);
        Organisme organisme = Organisme.builder().plateformeUuid("PLATEFORME_TEST").build();
        Offre offre = Offre.builder().libelle("libelleOffre").build();
        SuiviTypeAvisPub typeAvisPub = SuiviTypeAvisPub.builder().organisme(organisme).idConsultation(2).offreRacine(offre).statut(StatutTypeAvisAnnonceEnum.BROUILLON).build();
        when(agentService.getConfiguration(anyString(), anyInt(), any())).thenReturn(config);
        when(suiviTypeAvisPubRepository.findById(anyLong())).thenReturn(java.util.Optional.ofNullable(typeAvisPub));
        AtexoException exception = assertThrows(AtexoException.class, () -> {
            suiviTypeAvisPubService.demandeValidation(1L, 2, "PLATEFORME_TEST", "tokentest");
        });
        assertEquals("Vous ne pouvez pas transmettre l'avis de publicité \"libelleOffre\" car la consultation n'a pas été validée.", exception.getMessage());
    }


    @Test
    public void When_DemandeValidationForSuiviWithAvisExterne_Then_AddAvisExterne() {
        ConfigurationConsultationDTO config = new ConfigurationConsultationDTO();
        ConsultationContexte contexte = new ConsultationContexte();
        ConsultationMpe consultation = new ConsultationMpe();
        consultation.setEtatValidation(true);
        consultation.setStatutCalcule("test");
        contexte.setConsultation(consultation);
        config.setContexte(contexte);
        Organisme organisme = Organisme.builder().plateformeUuid("PLATEFORME_TEST").build();
        Offre offre = Offre.builder().libelle("libelleOffre").build();
        PieceJointe pieceJointe = PieceJointe.builder().name("name").build();
        List<SuiviAnnonce> annonces = new ArrayList<>();
        Support support = Support.builder().europeen(true).external(true).build();
        EFormsFormulaireEntity formulaire = EFormsFormulaireEntity.builder().build();
        SuiviAnnonce suiviAnnonce = SuiviAnnonce.builder().avisFichier(pieceJointe).support(support).formulaire(formulaire).build();
        annonces.add(suiviAnnonce);
        SuiviTypeAvisPub typeAvisPub = SuiviTypeAvisPub.builder().idConsultation(2).organisme(organisme).annonces(annonces).offreRacine(offre).statut(StatutTypeAvisAnnonceEnum.BROUILLON).build();
        SuiviTypeAvisPubDTO avis = SuiviTypeAvisPubDTO.builder().id(1L).creerPar(AgentDTO.builder().organisme(OrganismeResumeDTO.builder().plateformeUuid("PLATEFORME_TEST").build()).build()).build();
        when(agentService.getConfiguration(anyString(), anyInt(), any())).thenReturn(config);
        when(suiviTypeAvisPubRepository.findById(anyLong())).thenReturn(java.util.Optional.ofNullable(typeAvisPub));
        when(suiviTypeAvisPubRepository.save(any())).thenReturn(typeAvisPub);

        when(suiviTypeAvisPubMapper.mapToModel(any())).thenReturn(avis);
        // Appelez la méthode que vous testez
        SuiviTypeAvisPubDTO result = suiviTypeAvisPubService.demandeValidation(1L, 2, "PLATEFORME_TEST", "token");
        verify(suiviAnnonceRepository).saveAll(annonces);
        assertEquals(1L, result.getId());
        assertEquals("PLATEFORME_TEST", result.getCreerPar().getOrganisme().getPlateformeUuid());
    }

    @Test
    void When_GetConsultationAnnonces_ThenGetConsultationAnnonces() {
        Organisme organisme = Organisme.builder().plateformeUuid("PLATEFORME_TEST").build();
        Agent agent = Agent.builder().organisme(organisme).build();
        List<SuiviTypeAvisPub> suivis = new ArrayList<>();
        SuiviTypeAvisPub typeAvisPub = SuiviTypeAvisPub.builder().organisme(organisme).statut(StatutTypeAvisAnnonceEnum.BROUILLON).build();
        SuiviTypeAvisPubDTO avis = SuiviTypeAvisPubDTO.builder().id(1L).creerPar(AgentDTO.builder().organisme(OrganismeResumeDTO.builder().plateformeUuid("PLATEFORME_TEST").build()).build()).build();
        suivis.add(typeAvisPub);
        List<SuiviTypeAvisPubDTO> suivisDtos = new ArrayList<>();
        suivisDtos.add(avis);
        when(suiviTypeAvisPubRepository.findByOrganismePlateformeUuidAndIdConsultationAndChildrenIsEmpty(anyString(), anyInt())).thenReturn(suivis);
        when(suiviTypeAvisPubMapper.mapToModels(anyCollection())).thenReturn(suivisDtos);
        List<SuiviTypeAvisPubDTO> result = suiviTypeAvisPubService.getConsultationAnnonces("PLATEFORME_TEST", 1);
        assertEquals(1, result.size());
        assertEquals(1L, result.get(0).getId());
    }

    @Test
    void Given_AnnonceHorsPerimetre_When_AddSuiviTypeAvisPub_Then_ThrowError() {
        OffreDTO offre = OffreDTO.builder().libelle("libelleOffre").build();
        SuiviTypeAvisPubAdd avisPub = SuiviTypeAvisPubAdd.builder().objet("objet").idConsultation(1).reference("ref").titre("titre").offreRacine(offre).build();
        Organisme organisme = Organisme.builder().plateformeUuid("PLATEFORME_TEST2").build();
        Agent agent = Agent.builder().organisme(organisme).build();
        when(agentService.findByToken(anyString())).thenReturn(agent);
        AtexoException exception = assertThrows(AtexoException.class, () -> {
            suiviTypeAvisPubService.addSuiviTypeAvisPub(avisPub, "tokentest", "PLATEFORME_TEST");
        });
        assertEquals("Hors périmetre", exception.getMessage());
    }

    @Test
    void Given_InvalideCodeOffre_When_AddSuiviTypeAvisPub_Then_ThrowError() {
        OffreDTO offre2 = OffreDTO.builder().libelle("libelleOffre2").code("codeOffre2").build();
        OffreDTO offre = OffreDTO.builder().libelle("libelleOffre").offreAssociee(offre2).code("codeOffre").build();
        Offre offreEntity2 = Offre.builder().libelle("libelleOffre2").code("codeOffre2").build();
        Offre offreEntity = Offre.builder().libelle("libelleOffre").code("codeOffre").offreAssociee(offreEntity2).build();
        SuiviTypeAvisPubAdd avisPub = SuiviTypeAvisPubAdd.builder().objet("objet").idConsultation(1).reference("ref").titre("titre").offreRacine(offre).build();
        Organisme organisme = Organisme.builder().plateformeUuid("PLATEFORME_TEST").build();
        Agent agent = Agent.builder().organisme(organisme).build();
        when(agentService.findByToken(anyString())).thenReturn(agent);
        when(offreRepository.findByCode(anyString())).thenReturn(Optional.ofNullable(offreEntity));
        AtexoException exception = assertThrows(AtexoException.class, () -> {
            suiviTypeAvisPubService.addSuiviTypeAvisPub(avisPub, "tokentest", "PLATEFORME_TEST");
        });
        assertEquals("Facturation obligatoire", exception.getMessage());
    }

    @Test
    void Given_InvalidData_When_AddSuiviTypeAvisPub_Then_ThrowError() {
        OffreDTO offre2 = OffreDTO.builder().libelle("libelleOffre2").code("codeOffre2").build();
        OffreDTO offre = OffreDTO.builder().libelle("libelleOffre").offreAssociee(offre2).code("codeOffre").build();
        Offre offreEntity2 = Offre.builder().libelle("libelleOffre2").code("codeOffre2").build();
        Offre offreEntity = Offre.builder().libelle("libelleOffre").code("codeOffre").offreAssociee(offreEntity2).build();
        ConsultationFacturationDTO facturation = ConsultationFacturationDTO.builder().id(1L).build();
        SuiviTypeAvisPubAdd avisPub = SuiviTypeAvisPubAdd.builder().objet("objet").idConsultation(1).facturation(facturation).reference("ref").titre("titre").offreRacine(offre).build();
        Organisme organisme = Organisme.builder().plateformeUuid("PLATEFORME_TEST").build();
        Agent agent = Agent.builder().organisme(organisme).build();
        when(agentService.findByToken(anyString())).thenReturn(agent);
        when(offreRepository.findByCode(anyString())).thenReturn(Optional.ofNullable(offreEntity));
        AtexoException exception = assertThrows(AtexoException.class, () -> {
            suiviTypeAvisPubService.addSuiviTypeAvisPub(avisPub, "tokentest", "PLATEFORME_TEST");
        });
        assertEquals("Support obligatoire", exception.getMessage());
    }

    @Test
    void When_AddSuiviTypeAvisPubEuropeen_Then_AddSuiviTypeAvisPub() {

        Organisme organisme = Organisme.builder().plateformeUuid("PLATEFORME_TEST").plateformeUrl("url").build();
        OrganismeMpe organismeMpe = new OrganismeMpe();
        organismeMpe.setAcronyme("acronyme");
        Agent agent = Agent.builder().organisme(organisme).build();
        OffreDTO offre = OffreDTO.builder().libelle("libelleOffre").code("codeOffre").build();
        Offre offreEntity = Offre.builder().libelle("libelleOffre").code("codeOffre").build();
        ConsultationFacturationDTO facturation = ConsultationFacturationDTO.builder().id(1L).build();

        SuiviTypeAvisPubAdd avisPub = SuiviTypeAvisPubAdd.builder().objet("objet").europeen(true).idConsultation(1).facturation(facturation).organisme(organismeMpe).reference("ref").titre("titre").offreRacine(offre).build();
        SuiviTypeAvisPub avisPubEntity = SuiviTypeAvisPub.builder().objet("objet").idConsultation(1).reference("ref").europeen(true).titre("titre").offreRacine(offreEntity).build();
        SuiviTypeAvisPubDTO avis = SuiviTypeAvisPubDTO.builder().id(1L).europeen(true).creerPar(AgentDTO.builder().organisme(OrganismeResumeDTO.builder().plateformeUuid("PLATEFORME_TEST").build()).build()).build();
        SuiviAnnonceDTO suiviAnnonceDTO = SuiviAnnonceDTO.builder().idPlatform("PLATEFORME_TEST").statut(StatutSuiviAnnonceEnum.BROUILLON).idConsultation(1).offre(OffreDTO.builder().code("codeOffre").build()).build();
        when(agentService.findByToken(anyString())).thenReturn(agent);
        when(offreRepository.findByCode(anyString())).thenReturn(Optional.ofNullable(offreEntity));
        when(suiviTypeAvisPubMapper.mapToEntity(any(SuiviTypeAvisPubAdd.class))).thenReturn(avisPubEntity);
        when(suiviTypeAvisPubMapper.mapToModel(any())).thenReturn(avis);
        when(suiviTypeAvisPubRepository.save(any())).thenReturn(avisPubEntity);

        SuiviTypeAvisPubDTO result = suiviTypeAvisPubService.addSuiviTypeAvisPub(avisPub, "tokentest", "PLATEFORME_TEST");

        assertEquals(true, result.isEuropeen());
    }
}
