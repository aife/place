package fr.atexo.annonces.mapper;

import fr.atexo.annonces.boot.entity.Offre;
import fr.atexo.annonces.boot.entity.SuiviAnnonce;
import fr.atexo.annonces.builder.OffreBuilder;
import fr.atexo.annonces.builder.SuiviAnnonceBuilder;
import fr.atexo.annonces.dto.OffreDTO;
import fr.atexo.annonces.dto.SuiviAnnonceDTO;
import fr.atexo.annonces.modeles.StatutSuiviAnnonceEnum;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test du mapper entre Offre et OffreDTO
 */
@SpringBootTest(classes = {SuiviAnnonceMapperImpl.class, OffreMapperImpl.class, SupportMapperImpl.class, DepartementMapperImpl.class})
@Disabled
class SuiviAnnonceMapperIntegrationTest {

    @Autowired
    private SuiviAnnonceMapper suiviAnnonceMapper;

    @Test
    void mapSuiviAnnonce_shouldReturnMappedSuiviAnnonceDTO() {
        SuiviAnnonce suiviAnnonce = SuiviAnnonceBuilder.aSuiviAnnonce().withDefaults().build();
        SuiviAnnonceDTO suiviAnnonceDTO = suiviAnnonceMapper.suiviAnnonceToSuiviAnnonceDTO(suiviAnnonce, false);
        assertThat(suiviAnnonceDTO.getIdConsultation()).isEqualTo(suiviAnnonce.getIdConsultation());
        assertThat(suiviAnnonceDTO.getIdPlatform()).isEqualTo(suiviAnnonce.getIdPlatform());
        assertThat(suiviAnnonceDTO.getMessageStatut()).isEqualTo(suiviAnnonce.getMessageStatut());
        assertThat(suiviAnnonceDTO.getOrganisme()).isEqualTo(suiviAnnonce.getOrganisme());
        assertThat(suiviAnnonceDTO.getStatut()).isEqualTo(StatutSuiviAnnonceEnum.BROUILLON);
        assertThat(suiviAnnonceDTO.getOffre().getCode()).isEqualTo(suiviAnnonce.getOffre().getCode());
        assertThat(suiviAnnonceDTO.getSupport()).isNull();
        assertThat(suiviAnnonceDTO.getDateDebutEnvoi()).isEqualTo(Instant.EPOCH);
        assertThat(suiviAnnonceDTO.getDateDemandeEnvoi()).isEqualTo(Instant.EPOCH);
        assertThat(suiviAnnonceDTO).extracting(SuiviAnnonceDTO::getStatut).isEqualTo(StatutSuiviAnnonceEnum.BROUILLON);
    }

    @Test
    void mapSuiviAnnonceIncludeSupport_shouldReturnMappedSuiviAnnonceDTOWithSupportDTO() {
        SuiviAnnonce suiviAnnonce = SuiviAnnonceBuilder.aSuiviAnnonce().withDefaults().build();
        SuiviAnnonceDTO suiviAnnonceDTO = suiviAnnonceMapper.suiviAnnonceToSuiviAnnonceDTO(suiviAnnonce, true);
        assertThat(suiviAnnonceDTO.getIdConsultation()).isEqualTo(suiviAnnonce.getIdConsultation());
        assertThat(suiviAnnonceDTO.getIdPlatform()).isEqualTo(suiviAnnonce.getIdPlatform());
        assertThat(suiviAnnonceDTO.getMessageStatut()).isEqualTo(suiviAnnonce.getMessageStatut());
        assertThat(suiviAnnonceDTO.getOrganisme()).isEqualTo(suiviAnnonce.getOrganisme());
        assertThat(suiviAnnonceDTO.getStatut()).isEqualTo(StatutSuiviAnnonceEnum.BROUILLON);
        assertThat(suiviAnnonceDTO.getOffre().getCode()).isEqualTo(suiviAnnonce.getOffre().getCode());
        assertThat(suiviAnnonceDTO.getSupport().getCode()).isEqualTo("SUPPORT_TEST");
        assertThat(suiviAnnonceDTO.getDateDebutEnvoi()).isEqualTo(Instant.EPOCH);
        assertThat(suiviAnnonceDTO.getDateDemandeEnvoi()).isEqualTo(Instant.EPOCH);
        assertThat(suiviAnnonceDTO).extracting(SuiviAnnonceDTO::getStatut).isEqualTo(StatutSuiviAnnonceEnum.BROUILLON);
    }

    @Test
    void mapListSuiviAnnonce_shouldReturnListSuiviAnnonceDTO() {
        List<SuiviAnnonce> suiviAnnonceList = Arrays.asList(SuiviAnnonceBuilder.aSuiviAnnonce().withDefaults().build(),
                SuiviAnnonceBuilder.aSuiviAnnonce().withDefaults().withId(2).build());

        List<SuiviAnnonceDTO> suiviAnnonceDTOList = suiviAnnonceMapper.suiviAnnoncesToSuiviAnnonceDTOs(suiviAnnonceList, false);

        assertThat(suiviAnnonceDTOList)
                .hasSameSizeAs(suiviAnnonceList);
    }

    @Test
    void mapListSuiviAnnonceIncludeSupport_shouldReturnListSuiviAnnonceDTOWithSupportDTO() {
        List<SuiviAnnonce> suiviAnnonceList = Arrays.asList(SuiviAnnonceBuilder.aSuiviAnnonce().withDefaults().build(),
                SuiviAnnonceBuilder.aSuiviAnnonce().withDefaults().withId(2).build());

        List<SuiviAnnonceDTO> suiviAnnonceDTOList = suiviAnnonceMapper.suiviAnnoncesToSuiviAnnonceDTOs(suiviAnnonceList, true);

        assertThat(suiviAnnonceDTOList)
                .hasSameSizeAs(suiviAnnonceList);
    }

    @Test
    void updateSuiviAnnonce_shouldOnlyChangeSuiviAnnonceStatutAndXmlAndOrganisme() {
        Offre offre = OffreBuilder.anOffre().build();
        SuiviAnnonce copySuiviAnnonce = SuiviAnnonceBuilder.aSuiviAnnonce().withDefaults()
                .withStatut(StatutSuiviAnnonceEnum.BROUILLON)
                .withOffre(offre)
                .build();
        SuiviAnnonce suiviAnnonce = SuiviAnnonceBuilder.aSuiviAnnonce().withDefaults()
                .withStatut(StatutSuiviAnnonceEnum.BROUILLON)
                .withOffre(offre)
                .build();
        SuiviAnnonceDTO suiviAnnonceDTO = new SuiviAnnonceDTO();
        suiviAnnonceDTO.setOrganisme("Autre organisme de test");
        suiviAnnonceDTO.setIdConsultation(8);
        suiviAnnonceDTO.setOffre(new OffreDTO());
        suiviAnnonceDTO.setStatut(StatutSuiviAnnonceEnum.EN_ATTENTE);

        suiviAnnonceMapper.updateSuiviAnnonceFromSuiviAnnonceDTO(suiviAnnonceDTO, suiviAnnonce);

        assertThat(suiviAnnonce)
                .extracting(fr.atexo.annonces.boot.entity.SuiviAnnonce::getStatut, fr.atexo.annonces.boot.entity.SuiviAnnonce::getXml, SuiviAnnonce::getOrganisme)
                .containsExactly(StatutSuiviAnnonceEnum.EN_ATTENTE, "XML Test", "Autre organisme de test");
        assertThat(suiviAnnonce)
                .isEqualToIgnoringGivenFields(copySuiviAnnonce, "xml", "statut", "organisme");
    }

}
