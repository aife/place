package fr.atexo.annonces.ws.lesechos;


import fr.atexo.annonces.boot.ws.jal.LesechosWS;
import fr.atexo.annonces.dto.lesecho.suivi.Suivi;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

import static org.hibernate.validator.internal.util.Contracts.assertNotEmpty;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class LesEchosWSTest {

    private LesechosWS lesechosWS = new LesechosWS(new URI("https://preprod.hubal.fr/suivi"), new RestTemplate(), "concentrateurAtexo", "AtexoPreprod75");

    LesEchosWSTest() throws URISyntaxException {
    }

    @Test
    void WhenAuthentificationThenReturnToken() {
        Suivi suivi = lesechosWS.getSuivi();
        assertNotNull(suivi);

    }

}
