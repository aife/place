package fr.atexo.annonces.service;

import org.junit.jupiter.api.Test;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.xml.transform.TransformerException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test du handler des erreurs de transformation XSL
 */
class XslTransformationErrorListenerTest {

    @Test
    void warning_shouldCollectWarnMessage() {
        MultiValueMap<XslTransformationErrorSeverity, String> collector = new LinkedMultiValueMap<>();
        XslTransformationErrorListener listener = new XslTransformationErrorListener(collector);
        TransformerException e = new TransformerException("WARN - test");

        listener.warning(e);

        assertThat(collector).containsKey(XslTransformationErrorSeverity.WARN);
        assertThat(collector.get(XslTransformationErrorSeverity.WARN))
                .anyMatch(message -> message.contains("WARN - test"));
    }

    @Test
    void error_shouldCollectErrorMessage() {
        MultiValueMap<XslTransformationErrorSeverity, String> collector = new LinkedMultiValueMap<>();
        XslTransformationErrorListener listener = new XslTransformationErrorListener(collector);
        TransformerException e = new TransformerException("ERROR - test");

        listener.error(e);

        assertThat(collector).containsKey(XslTransformationErrorSeverity.ERROR);
        assertThat(collector.get(XslTransformationErrorSeverity.ERROR))
                .anyMatch(message -> message.contains("ERROR - test"));
    }

    @Test
    void fatalError_shouldCollectFatalMessage() {
        MultiValueMap<XslTransformationErrorSeverity, String> collector = new LinkedMultiValueMap<>();
        XslTransformationErrorListener listener = new XslTransformationErrorListener(collector);
        TransformerException e = new TransformerException("FATAL - test");

        listener.fatalError(e);

        assertThat(collector).containsKey(XslTransformationErrorSeverity.FATAL);
        assertThat(collector.get(XslTransformationErrorSeverity.FATAL))
                .anyMatch(message -> message.contains("FATAL - test"));
    }
}