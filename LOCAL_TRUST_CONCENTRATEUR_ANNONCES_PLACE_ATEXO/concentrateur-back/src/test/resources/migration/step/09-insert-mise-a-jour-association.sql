# mise à jour de la table association
INSERT INTO concentrateur.support_offre_asso (ID_SUPPORT, ID_OFFRE)
select distinct annonce.id_support, annonce.id_offre
from concentrateur.suivi_annonces annonce
WHERE annonce.ID_OFFRE is not null
  and annonce.ID_MPE_OLD is not null
  and (annonce.id_support, annonce.id_offre) not in
      (SELECT asso.id_support, asso.id_offre from concentrateur.support_offre_asso asso);

