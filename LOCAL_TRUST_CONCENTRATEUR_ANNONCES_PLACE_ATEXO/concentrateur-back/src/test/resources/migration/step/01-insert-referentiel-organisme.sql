#Création d'organisme

INSERT INTO concentrateur.organisme (acronyme_organisme, libelle_organisme, plateforme_uuid, plateforme_url,
                                     sigle_organisme)
SELECT distinct ap.acronyme,
                ap.denomination_org,
                @plateforme_uuid_to_update,
                @plateforme_url_to_update,
                ap.sigle
from mpe.Organisme ap
where ap.acronyme not in
      (select distinct acronyme_organisme
       from concentrateur.organisme
       where plateforme_uuid = @plateforme_uuid_to_update);