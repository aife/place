#facturation PAS SIP

INSERT INTO concentrateur.consultation_facturation (ADRESSE, MAIL, SIP)
SELECT distinct j.information_facturation, j.email_ar, false
from mpe.AdresseFacturationJal j
         inner join mpe.Annonce_Press p on j.id = p.id_adresse_facturation
where (j.information_facturation, j.email_ar, false) not in (select ADRESSE, MAIL, SIP
                                                             from concentrateur.consultation_facturation)
  and j.facturation_sip = '0'
UNION
SELECT distinct j.information_facturation, j.email_ar, true
from mpe.AdresseFacturationJal j
         inner join mpe.Annonce_Press p on j.id = p.id_adresse_facturation
where (j.information_facturation, j.email_ar, true) not in (select ADRESSE, MAIL, SIP
                                                            from concentrateur.consultation_facturation)
  and j.facturation_sip = '1';
