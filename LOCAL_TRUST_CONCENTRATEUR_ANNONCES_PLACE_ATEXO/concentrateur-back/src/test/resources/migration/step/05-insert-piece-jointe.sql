#  ajout des pieces jointes PRESS
INSERT concentrateur.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                app.nom_fichier,
                CONCAT(b.chemin, b.id, '-0'),
                app.taille,
                CONCAT('Annonce_Press_PieceJointe-', app.id),
                b.hash
from mpe.Annonce_Press_PieceJointe app
         left join mpe.Annonce_Press ap on ap.id = app.id_annonce_press
         inner join mpe.Destinataire_Pub dp
                    on dp.id = ap.id_Dest_Press
         inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
         inner join mpe.blobOrganisme_file b on b.id = app.piece
where b.chemin is not null
  and b.old_id is null
  and not exists (select distinct ID_MPE_OLD
                  from concentrateur.piece_jointe
                  where ID_MPE_OLD = CONCAT('Annonce_Press_PieceJointe-', app.id));

INSERT concentrateur.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                app.nom_fichier,
                CONCAT(b.organisme, '/files/', b.id, '-0'),
                app.taille,
                CONCAT('Annonce_Press_PieceJointe-', app.id),
                b.hash
from mpe.Annonce_Press_PieceJointe app
         left join mpe.Annonce_Press ap on ap.id = app.id_annonce_press
         inner join mpe.Destinataire_Pub dp
                    on dp.id = ap.id_Dest_Press
         inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
         inner join mpe.blobOrganisme_file b on b.id = app.piece
where b.chemin is null
  and b.old_id is null
  and not exists (select distinct ID_MPE_OLD
                  from concentrateur.piece_jointe
                  where ID_MPE_OLD = CONCAT('Annonce_Press_PieceJointe-', app.id));

# ajout des pieces jointes avec old id

INSERT concentrateur.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                app.nom_fichier,
                CONCAT(b.chemin, b.old_id, '-0'),
                app.taille,
                CONCAT('blobOrganisme_file-old-id-', b.old_id),
                b.hash
from mpe.Annonce_Press_PieceJointe app
         left join mpe.Annonce_Press ap on ap.id = app.id_annonce_press
         inner join mpe.Destinataire_Pub dp
                    on dp.id = ap.id_Dest_Press
         inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
         inner join mpe.blobOrganisme_file b on b.id = app.piece
where b.chemin is not null
  and b.old_id is not null
  and not exists (select distinct ID_MPE_OLD
                  from concentrateur.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id));

INSERT concentrateur.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                app.nom_fichier,
                CONCAT(b.organisme, '/files/', b.old_id, '-0'),
                app.taille,
                CONCAT('blobOrganisme_file-old-id-', b.old_id),
                b.hash
from mpe.Annonce_Press_PieceJointe app
         left join mpe.Annonce_Press ap on ap.id = app.id_annonce_press
         inner join mpe.Destinataire_Pub dp
                    on dp.id = ap.id_Dest_Press
         inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
         inner join mpe.blobOrganisme_file b on b.id = app.piece
where b.chemin is null
  and b.old_id is not null
  and not exists (select distinct ID_MPE_OLD
                  from concentrateur.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id));
# début AN-627

# ajout des pieces jointes OPOCE
INSERT concentrateur.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.chemin, b.id, '-0'),
                1,
                CONCAT('blobOrganisme_file-', b.id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_pdf_opoce
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is not null
  and b.old_id is null
  and not exists (select distinct ID_MPE_OLD
                  from concentrateur.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-', b.id));

INSERT concentrateur.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.organisme, '/files/', b.id, '-0'),
                1,
                CONCAT('blobOrganisme_file-', b.id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_pdf_opoce
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is null
  and b.old_id is null
  and not exists (select distinct ID_MPE_OLD
                  from concentrateur.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-', b.id));


# ajout des pieces jointes OPOCE OLD
INSERT concentrateur.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.chemin, b.old_id, '-0'),
                1,
                CONCAT('blobOrganisme_file-old-id-', b.old_id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_pdf_opoce
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is not null
  and b.old_id is not null
  and not exists (select distinct ID_MPE_OLD
                  from concentrateur.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id));

INSERT concentrateur.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.organisme, '/files/', b.old_id, '-0'),
                1,
                CONCAT('blobOrganisme_file-old-id-', b.old_id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_pdf_opoce
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is null
  and b.old_id is not null
  and not exists (select distinct ID_MPE_OLD
                  from concentrateur.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id));
# ajout des pieces jointes portail OLD
INSERT concentrateur.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.chemin, b.old_id, '-0'),
                1,
                CONCAT('blobOrganisme_file-old-id-', b.old_id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_portail
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is not null
  and b.old_id is not null
  and not exists (select distinct ID_MPE_OLD
                  from concentrateur.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id));

INSERT concentrateur.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.organisme, '/files/', b.old_id, '-0'),
                1,
                CONCAT('blobOrganisme_file-old-id-', b.old_id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_portail
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is null
  and b.old_id is not null
  and not exists (select distinct ID_MPE_OLD
                  from concentrateur.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id));

INSERT concentrateur.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.chemin, b.id, '-0'),
                1,
                CONCAT('blobOrganisme_file-', b.id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_portail
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is not null
  and b.old_id is null
  and not exists (select distinct ID_MPE_OLD
                  from concentrateur.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-', b.id));

INSERT concentrateur.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.organisme, '/files/', b.id, '-0'),
                1,
                CONCAT('blobOrganisme_file-', b.id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_portail
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is null
  and b.old_id is null
  and not exists (select distinct ID_MPE_OLD
                  from concentrateur.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-', b.id));

#fin AN-627
