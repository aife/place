# update existing service
update concentrateur.referential_service_mpe toUpdate inner join concentrateur.organisme o
    on o.id = toUpdate.id_organisme
set toUpdate.ID_MPE= (select ms.id as id_service_mpe
                      from concentrateur.referential_service_mpe cs
                               inner join mpe.Service ms
                               inner join concentrateur.organisme o
                                          on o.acronyme_organisme = ms.organisme and
                                             o.plateforme_uuid = @plateforme_uuid_to_update
                                              and ms.libelle = cs.libelle and ms.sigle = cs.code
                      where toUpdate.id = cs.id)
where ID_MPE is null
  and o.plateforme_uuid = @plateforme_uuid_to_update;

update concentrateur.referential_service_mpe toUpdate inner join concentrateur.organisme o
    on o.id = toUpdate.id_organisme
set toUpdate.code= (select ms.sigle
                    from mpe.Service ms
                    where toUpdate.ID_MPE = ms.id)
  , toUpdate.libelle= (select ms.libelle
                       from mpe.Service ms
                       where toUpdate.ID_MPE = ms.id)
where ID_MPE is not null
  and o.plateforme_uuid = @plateforme_uuid_to_update;

#Création de service
INSERT INTO concentrateur.referential_service_mpe (code, libelle, id_organisme, ID_MPE)
SELECT distinct ap.sigle,
                ap.libelle,
                o2.id,
                ap.id
from mpe.Service ap
         inner join mpe.Organisme o on ap.organisme = o.acronyme
         inner join concentrateur.organisme o2 on o2.acronyme_organisme = o.acronyme
where o2.plateforme_uuid = @plateforme_uuid_to_update
  and not exists (select distinct s.code, s.libelle, s.ID_MPE
                  from concentrateur.referential_service_mpe s
                  where s.id_organisme = o2.id
                    and ap.id = s.ID_MPE
                    and ap.sigle = s.code
                    and ap.libelle = s.libelle);

INSERT INTO concentrateur.referential_service_mpe (code, libelle, id_organisme, ID_MPE)
SELECT distinct service.sigle,
                service.libelle,
                organismeConsultation.id,
                service.id
from mpe.Avis_Pub avisPub
         left join mpe.Consultation consultation on consultation.id = avisPub.consultation_id
         left join mpe.Service service on service.id = consultation.service_id
         left join concentrateur.organisme organismeConsultation
                   on organismeConsultation.acronyme_organisme = avisPub.organisme
         left join concentrateur.referential_service_mpe refService
                   on refService.ID_MPE = service.id
where consultation.service_id is not null
  and not exists (select distinct s.code, s.libelle, s.ID_MPE
                  from concentrateur.referential_service_mpe s
                  where s.id_organisme = organismeConsultation.id
                    and service.id = s.ID_MPE
                    and service.sigle = s.code
                    and service.libelle = s.libelle);
