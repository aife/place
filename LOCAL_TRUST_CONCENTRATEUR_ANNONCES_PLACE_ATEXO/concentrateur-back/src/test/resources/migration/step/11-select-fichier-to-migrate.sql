# déplacer cette liste de fichiers du nas de MPE vers le nas de concentrateur PUB

SET @serveur_concentrateur = 'atexo-concpub-int-01';
SET @user_concentrateur = 'iat-atx';
SET @serveur_mpe = 'simap-mpe-int-01';
SET @user_mpe = 'iat-atx';
SET @nas_concentrateur = '/data/annonces/fichiers/';
SET @nas_mpe = '/data/apache2/simap-mpe-int.local-trust.com/data/';

SELECT rCrC.*
from (SELECT distinct CONCAT(
                              'cpt=$((cpt+1)); echo "$cpt" ; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                              ' "test ! -d ''', @nas_concentrateur, result.folder, ''' && mkdir -p ''',
                              @nas_concentrateur, result.folder,
                              '''"; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                              ' [ ! -f "', @nas_concentrateur, result.chemin, '" ] && scp -3q ', @user_mpe, '@',
                              @serveur_mpe, ':', @nas_mpe, result.chemin,
                              ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur,
                              result.chemin,
                              ' || echo "file exists"')
      FROM (SELECT distinct CONCAT(b.chemin, b.id, '-0') as chemin, b.chemin as folder
            from mpe.Avis app
                     inner join mpe.Avis_Pub apu on apu.id_avis_pdf_opoce = app.id
                     inner join mpe.blobOrganisme_file b on b.id = app.avis
            where b.chemin is not null
              and b.old_id is null
            UNION

            SELECT distinct CONCAT(b.chemin, b.id, '-0') as chemin, b.chemin as folder
            from mpe.Annonce_Press_PieceJointe app
                     left join mpe.Annonce_Press ap on ap.id = app.id_annonce_press
                     inner join mpe.Destinataire_Pub dp
                                on dp.id = ap.id_Dest_Press
                     inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
                     inner join mpe.blobOrganisme_file b on b.id = app.piece
            where b.chemin is not null
              and b.old_id is null) as result
# déplacer cette liste de fichiers du nas de MPE vers le nas de concentrateur PUB
      UNION
      SELECT distinct CONCAT(' cpt=$((cpt+1)); echo "$cpt" ; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                             ' [ ! -f "', @nas_concentrateur, result.chemin,
                             '" ] && scp -3q ', @user_mpe, '@',
                             @serveur_mpe, ':', @nas_mpe, result.chemin,
                             ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur,
                             result.chemin,
                             ' || echo "file exists"')
      FROM (SELECT distinct CONCAT(b.organisme, '/files/', b.id, '-0') as chemin, b.organisme as organisme

            from mpe.Avis app

                     inner join mpe.Avis_Pub apu on apu.id_avis_pdf_opoce = app.id
                     inner join mpe.blobOrganisme_file b on b.id = app.avis
            where b.chemin is null
              and b.old_id is null
            UNION

            SELECT distinct CONCAT(b.organisme, '/files/', b.id, '-0') as chemin, b.organisme as organisme
            from mpe.Annonce_Press_PieceJointe app
                     left join mpe.Annonce_Press ap on ap.id = app.id_annonce_press
                     inner join mpe.Destinataire_Pub dp
                                on dp.id = ap.id_Dest_Press
                     inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
                     inner join mpe.blobOrganisme_file b on b.id = app.piece
            where b.chemin is null
              and b.old_id is null) as result


      UNION
      SELECT distinct CONCAT(
                              'cpt=$((cpt+1)); echo "$cpt" ; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                              ' "test ! -d ''', @nas_concentrateur, result.folder, ''' && mkdir -p ''',
                              @nas_concentrateur, result.folder,
                              '''"; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                              ' [ ! -f "', @nas_concentrateur, result.chemin, '" ] && scp -3q ', @user_mpe, '@',
                              @serveur_mpe, ':', @nas_mpe, result.chemin,
                              ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur,
                              result.chemin,
                              ' || echo "file exists"')
      FROM (SELECT distinct CONCAT(b.chemin, b.old_id, '-0') as chemin, b.chemin as folder
            from mpe.Avis app
                     inner join mpe.Avis_Pub apu on apu.id_avis_pdf_opoce = app.id
                     inner join mpe.blobOrganisme_file b on b.id = app.avis
            where b.chemin is not null
              and b.old_id is not null
            UNION

            SELECT distinct CONCAT(b.chemin, b.old_id, '-0') as chemin, b.chemin as folder
            from mpe.Annonce_Press_PieceJointe app
                     left join mpe.Annonce_Press ap on ap.id = app.id_annonce_press
                     inner join mpe.Destinataire_Pub dp
                                on dp.id = ap.id_Dest_Press
                     inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
                     inner join mpe.blobOrganisme_file b on b.id = app.piece
            where b.chemin is not null
              and b.old_id is not null) as result

# déplacer cette liste de fichiers du nas de MPE vers le nas de concentrateur PUB
      UNION
      SELECT distinct CONCAT(' cpt=$((cpt+1)); echo "$cpt" ; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                             ' [ ! -f "', @nas_concentrateur, result.chemin,
                             '" ] && scp -3q ', @user_mpe, '@',
                             @serveur_mpe, ':', @nas_mpe, result.chemin,
                             ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur,
                             result.chemin,
                             ' || echo "file exists"')
      FROM (SELECT distinct CONCAT(b.organisme, '/files/', b.old_id, '-0') as chemin, b.organisme as organisme

            from mpe.Avis app

                     inner join mpe.Avis_Pub apu on apu.id_avis_pdf_opoce = app.id
                     inner join mpe.blobOrganisme_file b on b.id = app.avis
            where b.chemin is null
              and b.old_id is not null
            UNION

            SELECT distinct CONCAT(b.organisme, '/files/', b.old_id, '-0') as chemin, b.organisme as organisme
            from mpe.Annonce_Press_PieceJointe app
                     left join mpe.Annonce_Press ap on ap.id = app.id_annonce_press
                     inner join mpe.Destinataire_Pub dp
                                on dp.id = ap.id_Dest_Press
                     inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
                     inner join mpe.blobOrganisme_file b on b.id = app.piece
            where b.chemin is null
              and b.old_id is not null) as result

      UNION
      SELECT distinct CONCAT(
                              'cpt=$((cpt+1)); echo "$cpt" ; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                              ' "test ! -d ''', @nas_concentrateur, result.folder, ''' && mkdir -p ''',
                              @nas_concentrateur, result.folder,
                              '''"; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                              ' [ ! -f "', @nas_concentrateur, result.chemin, '" ] && scp -3q ', @user_mpe, '@',
                              @serveur_mpe, ':', @nas_mpe, result.chemin,
                              ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur,
                              result.chemin,
                              ' || echo "file exists"')
      FROM (SELECT distinct CONCAT(b.chemin, b.old_id, '-0') as chemin, b.chemin as folder
            from mpe.Avis app
                     inner join mpe.Avis_Pub apu on apu.id_avis_pdf_opoce = app.id
                     inner join mpe.blobOrganisme_file b on b.id = app.avis
            where b.chemin is not null
              and b.old_id is not null
            UNION

            SELECT distinct CONCAT(b.chemin, b.old_id, '-0') as chemin, b.chemin as folder
            from mpe.Annonce_Press_PieceJointe app
                     left join mpe.Annonce_Press ap on ap.id = app.id_annonce_press
                     inner join mpe.Destinataire_Pub dp
                                on dp.id = ap.id_Dest_Press
                     inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
                     inner join mpe.blobOrganisme_file b on b.id = app.piece
            where b.chemin is not null
              and b.old_id is not null) as result
      # début AN-627

# scp pièce portail

      UNION
      SELECT distinct CONCAT(
                              'cpt=$((cpt+1)); echo "$cpt" ; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                              ' "test ! -d ''', @nas_concentrateur, result.folder, ''' && mkdir -p ''',
                              @nas_concentrateur, result.folder,
                              '''"; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                              ' [ ! -f "', @nas_concentrateur, result.chemin, '" ] && scp -3q ', @user_mpe, '@',
                              @serveur_mpe, ':', @nas_mpe, result.chemin,
                              ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur,
                              result.chemin,
                              ' || echo "file exists"')
      FROM (SELECT distinct CONCAT(b.chemin, b.old_id, '-0') as chemin, b.chemin as folder
            from mpe.Avis_Pub apu
                     inner join mpe.Avis a on a.id = apu.id_avis_portail
                     inner join mpe.blobOrganisme_file b on b.id = a.avis
            where b.chemin is not null
              and b.old_id is not null) as result

      UNION
      SELECT distinct CONCAT(' cpt=$((cpt+1)); echo "$cpt" ; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                             ' [ ! -f "', @nas_concentrateur, result.chemin,
                             '" ] && scp -3q ', @user_mpe, '@',
                             @serveur_mpe, ':', @nas_mpe, result.chemin,
                             ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur,
                             result.chemin,
                             ' || echo "file exists"')
      FROM (SELECT distinct CONCAT(b.organisme, '/files/', b.old_id, '-0') as chemin, b.organisme as organisme

            from mpe.Avis_Pub apu
                     inner join mpe.Avis a on a.id = apu.id_avis_portail
                     inner join mpe.blobOrganisme_file b on b.id = a.avis
            where b.chemin is null
              and b.old_id is not null) as result


      UNION
      SELECT distinct CONCAT(
                              'cpt=$((cpt+1)); echo "$cpt" ; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                              ' "test ! -d ''', @nas_concentrateur, result.folder, ''' && mkdir -p ''',
                              @nas_concentrateur, result.folder,
                              '''"; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                              ' [ ! -f "', @nas_concentrateur, result.chemin, '" ] && scp -3q ', @user_mpe, '@',
                              @serveur_mpe, ':', @nas_mpe, result.chemin,
                              ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur,
                              result.chemin,
                              ' || echo "file exists"')
      FROM (SELECT distinct CONCAT(b.chemin, b.id, '-0') as chemin, b.chemin as folder
            from mpe.Avis_Pub apu
                     inner join mpe.Avis a on a.id = apu.id_avis_portail
                     inner join mpe.blobOrganisme_file b on b.id = a.avis
            where b.chemin is not null
              and b.old_id is null) as result
# déplacer cette liste de fichiers du nas de MPE vers le nas de concentrateur PUB
      UNION
      SELECT distinct CONCAT(' cpt=$((cpt+1)); echo "$cpt" ; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                             ' [ ! -f "', @nas_concentrateur, result.chemin,
                             '" ] && scp -3q ', @user_mpe, '@',
                             @serveur_mpe, ':', @nas_mpe, result.chemin,
                             ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur,
                             result.chemin,
                             ' || echo "file exists"')
      FROM (SELECT distinct CONCAT(b.organisme, '/files/', b.id, '-0') as chemin, b.organisme as organisme
            from mpe.Avis_Pub apu
                     inner join mpe.Avis a on a.id = apu.id_avis_portail
                     inner join mpe.blobOrganisme_file b on b.id = a.avis
            where b.chemin is null
              and b.old_id is null) as result) as rCrC;

# fin AN-627
