INSERT INTO concentrateur.suivi_annonces(DATE_TRAITEMENT, NUMERO_AVIS, MESSAGE_STATUT, NUMERO_DOSSIER, NUMERO_JOUE,
                                         LIEN_PUBLICATION, ID_CONSULTATION, ID_OFFRE,
                                         DATE_TRAITEMENT_INTERNE, DATE_DEMANDE, ID_PLATFORM, ORGANISME, DATE_CREATION,
                                         DATE_MODIFICATION, ID_AGENT, ID_ORGANISME,
                                         ID_SUPPORT, ID_SERVICE, ID_PROCEDURE,
                                         ID_SUIVI_TYPE_AVIS_PUB, ID_MPE_OLD, DATE_PUBLICATION, STATUT, XML)
SELECT distinct suivi.DATE_ENVOI_EUROPEEN,
                form.no_doc_ext,
                form.message_retour,
                dest.id_dossier,
                form.id_joue,
                form.lien_publication,
                suivi.ID_CONSULTATION,
                suivi.ID_OFFRE_RACINE,
                suivi.DATE_TRAITEMENT,
                suivi.DATE_ENVOI_EUROPEEN,
                o.plateforme_uuid,
                o.acronyme_organisme,
                suivi.DATE_CREATION,
                CASE
                    WHEN dest.DATE_MODIFICATION = '' THEN suivi.DATE_CREATION
                    ELSE dest.DATE_MODIFICATION
                    END,
                suivi.ID_CREER_PAR,
                suivi.ID_ORGANISME,
                s.ID,
                suivi.ID_SERVICE,
                suivi.ID_PROCEDURE,
                suivi.ID,
                CONCAT('Destinataire_Pub-', dest.id),
                dest.date_publication,
                CASE
                    WHEN dest.date_publication is not null THEN 'PUBLIER'
                    WHEN ap.statut = '1' THEN 'EN_ATTENTE_VALIDATION_ECO'
                    WHEN ap.statut = '2' THEN 'REJETER_CONCENTRATEUR_VALIDATION_ECO'
                    WHEN ap.statut = '3' THEN 'EN_ATTENTE_VALIDATION_SIP'
                    WHEN ap.statut = '4' THEN 'ENVOI_PLANIFIER'
                    WHEN ap.statut = '5' THEN 'EN_COURS_DE_PUBLICATION'
                    WHEN ap.statut = '6' THEN 'REJETER_CONCENTRATEUR_VALIDATION_SIP'
                    WHEN dest.etat = '5' and ap.id_avis_pdf_opoce is null THEN 'EN_COURS_DE_PUBLICATION'
                    WHEN dest.etat = '6' and ap.id_avis_pdf_opoce is null THEN 'EN_COURS_DE_PUBLICATION'
                    WHEN dest.etat = '5' and ap.id_avis_pdf_opoce is not null THEN 'PUBLIER'
                    WHEN dest.etat = '6' and ap.id_avis_pdf_opoce is not null THEN 'PUBLIER'
                    WHEN dest.etat = '7' THEN 'PUBLIER'
                    WHEN dest.etat = '8' and ap.id_avis_pdf_opoce is null THEN 'EN_COURS_DE_PUBLICATION'
                    WHEN dest.etat = '8' and ap.id_avis_pdf_opoce is not null THEN 'PUBLIER'
                    WHEN dest.etat = '9' THEN 'PUBLIER'
                    WHEN dest.etat = '10' THEN 'REJETER_SUPPORT'
                    END,
                form.xml
from mpe.Destinataire_Pub dest
         inner join (SELECT form1.*
                     from (select id_destinataire_opoce, max(opoce.id) as id
                           from mpe.FormXmlDestinataireOpoce opoce
                                    inner join mpe.Destinataire_Pub pub on pub.id = id_destinataire_opoce
                                    inner join mpe.Avis_Pub ap
                           where ap.id = pub.id_avis
                             and ap.statut <> '0'
                           group by id_destinataire_opoce) as xml
                              inner join mpe.FormXmlDestinataireOpoce form1 on xml.id = form1.id
                     group by xml.id_destinataire_opoce) form on dest.id = form.id_destinataire_opoce
         inner join mpe.Avis_Pub ap on ap.id = dest.id_avis
         inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
         inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme,
     concentrateur.support s
where s.CODE = 'ENOTICES'
  and dest.id_support = 1
  and ap.statut <> '0'
  and suivi.EU is true
  and o.plateforme_uuid = @plateforme_uuid_to_update
  and not exists(select distinct ss.ID_MPE_OLD
                 from concentrateur.suivi_annonces ss
                          inner join concentrateur.organisme o on o.id = ss.ID_ORGANISME
                 where o.plateforme_uuid = @plateforme_uuid_to_update
                   and ss.ID_MPE_OLD = CONCAT('Destinataire_Pub-', dest.id));


INSERT INTO concentrateur.suivi_annonces(DATE_TRAITEMENT, NUMERO_AVIS, MESSAGE_STATUT, NUMERO_DOSSIER, NUMERO_JOUE,
                                         LIEN_PUBLICATION, ID_CONSULTATION, ID_OFFRE,
                                         DATE_TRAITEMENT_INTERNE, DATE_DEMANDE, ID_PLATFORM, ORGANISME, DATE_CREATION,
                                         DATE_MODIFICATION, ID_AGENT, ID_ORGANISME,
                                         ID_SUPPORT, ID_SERVICE, ID_PROCEDURE,
                                         ID_SUIVI_TYPE_AVIS_PUB, ID_MPE_OLD, DATE_PUBLICATION, STATUT, XML)
SELECT suivi.DATE_ENVOI_EUROPEEN,
       null,
       null,
       dest.id_dossier,
       null,
       null,
       suivi.ID_CONSULTATION,
       suivi.ID_OFFRE_RACINE,
       suivi.DATE_TRAITEMENT,
       suivi.DATE_ENVOI_EUROPEEN,
       o.plateforme_uuid,
       o.acronyme_organisme,
       suivi.DATE_CREATION,
       CASE
           WHEN dest.DATE_MODIFICATION = '' THEN suivi.DATE_CREATION
           ELSE dest.DATE_MODIFICATION
           END
        ,
       suivi.ID_CREER_PAR,
       suivi.ID_ORGANISME,
       s.ID,
       suivi.ID_SERVICE,
       suivi.ID_PROCEDURE,
       suivi.ID,
       CONCAT('Destinataire_Pub-', dest.id),
       dest.date_publication,
       CASE
           WHEN dest.date_publication is not null THEN 'PUBLIER'
           WHEN ap.statut = '1' THEN 'EN_ATTENTE_VALIDATION_ECO'
           WHEN ap.statut = '2' THEN 'REJETER_CONCENTRATEUR_VALIDATION_ECO'
           WHEN ap.statut = '3' THEN 'EN_ATTENTE_VALIDATION_SIP'
           WHEN ap.statut = '4' THEN 'ENVOI_PLANIFIER'
           WHEN ap.statut = '5' THEN 'PUBLIER'
           WHEN ap.statut = '6' THEN 'REJETER_CONCENTRATEUR_VALIDATION_SIP'
           WHEN dest.etat = '5' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '6' THEN 'PUBLIER'
           WHEN dest.etat = '7' THEN 'PUBLIER'
           WHEN dest.etat = '8' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '9' THEN 'PUBLIER'
           WHEN dest.etat = '10' THEN 'REJETER_SUPPORT'
           END,
       null
from mpe.Destinataire_Pub dest
         inner join mpe.Avis_Pub ap on ap.id = dest.id_avis
         inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
         inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme,
     concentrateur.support s
where s.CODE = 'ENOTICES'
  and dest.id_support = 1
  and o.plateforme_uuid = @plateforme_uuid_to_update
  and ap.statut <> '0'
  and suivi.EU is true
  and not exists(select distinct ss.ID_MPE_OLD
                 from concentrateur.suivi_annonces ss
                          inner join concentrateur.organisme o on o.id = ss.ID_ORGANISME
                 where o.plateforme_uuid = @plateforme_uuid_to_update
                   and ss.ID_MPE_OLD = CONCAT('Destinataire_Pub-', dest.id));


# add annonce JAL (support 2)

INSERT INTO concentrateur.suivi_annonces(DATE_TRAITEMENT, NUMERO_DOSSIER, DATE_PUBLICATION, ID_CONSULTATION, ID_OFFRE,
                                         DATE_TRAITEMENT_INTERNE, DATE_DEMANDE, ID_PLATFORM, ORGANISME, DATE_CREATION,
                                         DATE_MODIFICATION, ID_AGENT, ID_ORGANISME,
                                         ID_SUPPORT, ID_SERVICE, ID_PROCEDURE,
                                         ID_SUIVI_TYPE_AVIS_PUB, ID_MPE_OLD, STATUT)
SELECT suivi.DATE_ENVOI_NATIONAL,
       dest.id_dossier,
       dest.date_publication,
       suivi.ID_CONSULTATION,
       off.ID_OFFRE_ASSOCIEE,
       suivi.DATE_TRAITEMENT,
       suivi.DATE_ENVOI_NATIONAL,
       o.plateforme_uuid,
       o.acronyme_organisme,
       suivi.DATE_CREATION,
       CASE
           WHEN dest.DATE_MODIFICATION = '' THEN suivi.DATE_CREATION
           ELSE dest.DATE_MODIFICATION
           END
        ,
       suivi.ID_CREER_PAR,
       suivi.ID_ORGANISME,
       s.ID as supportId,
       suivi.ID_SERVICE,
       suivi.ID_PROCEDURE,
       suivi.ID,
       CONCAT('Destinataire_Annonce_Press-', desJal.id),
       CASE
           WHEN dest.date_publication is not null THEN 'PUBLIER'
           WHEN ap.statut = '1' THEN 'EN_ATTENTE_VALIDATION_ECO'
           WHEN ap.statut = '2' THEN 'REJETER_CONCENTRATEUR_VALIDATION_ECO'
           WHEN ap.statut = '3' THEN 'EN_ATTENTE_VALIDATION_SIP'
           WHEN ap.statut = '4' THEN 'ENVOI_PLANIFIER'
           WHEN ap.statut = '5' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN ap.statut = '6' THEN 'REJETE_SIP'
           WHEN dest.etat = '5' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '6' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '7' THEN 'PUBLIER'
           WHEN dest.etat = '8' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '9' THEN 'PUBLIER'
           WHEN dest.etat = '10' THEN 'REJETER_SUPPORT'
           END
from mpe.Destinataire_Annonce_Press desJal
         left join mpe.JAL jal on desJal.id_jal = jal.id
         left join mpe.Annonce_Press form on desJal.id_annonce_press = form.id
         inner join mpe.Destinataire_Pub dest on dest.id = form.id_Dest_Press
         inner join mpe.Avis_Pub ap on ap.id = dest.id_avis
         inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
         inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
         inner join concentrateur.offre off on off.id = suivi.ID_OFFRE_RACINE,
     concentrateur.support s
where s.CODE = CONCAT(@plateforme_lang_to_update, '-', jal.email, '-', TO_BASE64(jal.nom))
  and o.plateforme_uuid = @plateforme_uuid_to_update
  and dest.id_support = 2
  and ap.statut <> '0'
  and suivi.EU is true
  and not exists(select distinct ss.ID_MPE_OLD
                 from concentrateur.suivi_annonces ss
                          inner join concentrateur.organisme o on o.id = ss.ID_ORGANISME
                 where o.plateforme_uuid = @plateforme_uuid_to_update
                   and ss.ID_MPE_OLD = CONCAT('Destinataire_Annonce_Press-', desJal.id));


# add annonce JAL National (support 2)

INSERT INTO concentrateur.suivi_annonces(DATE_TRAITEMENT, NUMERO_DOSSIER, DATE_PUBLICATION, ID_CONSULTATION, ID_OFFRE,
                                         DATE_TRAITEMENT_INTERNE, DATE_DEMANDE, ID_PLATFORM, ORGANISME, DATE_CREATION,
                                         DATE_MODIFICATION, ID_AGENT, ID_ORGANISME,
                                         ID_SUPPORT, ID_SERVICE, ID_PROCEDURE,
                                         ID_SUIVI_TYPE_AVIS_PUB, ID_MPE_OLD, STATUT)
SELECT suivi.DATE_ENVOI_NATIONAL,
       dest.id_dossier,
       dest.date_publication,
       suivi.ID_CONSULTATION,
       off.ID,
       suivi.DATE_TRAITEMENT,
       suivi.DATE_ENVOI_NATIONAL,
       o.plateforme_uuid,
       o.acronyme_organisme,
       suivi.DATE_CREATION,
       CASE
           WHEN dest.DATE_MODIFICATION = '' THEN suivi.DATE_CREATION
           ELSE dest.DATE_MODIFICATION
           END
        ,
       suivi.ID_CREER_PAR,
       suivi.ID_ORGANISME,
       s.ID as supportId,
       suivi.ID_SERVICE,
       suivi.ID_PROCEDURE,
       suivi.ID,
       CONCAT('Destinataire_Annonce_Press-', desJal.id),
       CASE
           WHEN dest.date_publication is not null THEN 'PUBLIER'
           WHEN ap.statut = '1' THEN 'EN_ATTENTE_VALIDATION_ECO'
           WHEN ap.statut = '2' THEN 'REJETER_CONCENTRATEUR_VALIDATION_ECO'
           WHEN ap.statut = '3' THEN 'EN_ATTENTE_VALIDATION_SIP'
           WHEN ap.statut = '4' THEN 'ENVOI_PLANIFIER'
           WHEN ap.statut = '5' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN ap.statut = '6' THEN 'REJETE_SIP'
           WHEN dest.etat = '5' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '6' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '7' THEN 'PUBLIER'
           WHEN dest.etat = '8' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '9' THEN 'PUBLIER'
           WHEN dest.etat = '10' THEN 'REJETER_SUPPORT'
           END
from mpe.Destinataire_Annonce_Press desJal
         left join mpe.JAL jal on desJal.id_jal = jal.id
         left join mpe.Annonce_Press form on desJal.id_annonce_press = form.id
         inner join mpe.Destinataire_Pub dest on dest.id = form.id_Dest_Press
         inner join mpe.Avis_Pub ap on ap.id = dest.id_avis
         inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
         inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
         inner join concentrateur.offre off on off.id = suivi.ID_OFFRE_RACINE,
     concentrateur.support s
where s.CODE = CONCAT(@plateforme_lang_to_update, '-', jal.email, '-', TO_BASE64(jal.nom))
  and o.plateforme_uuid = @plateforme_uuid_to_update
  and dest.id_support = 2
  and ap.statut <> '0'
  and suivi.EU is false
  and not exists(select distinct ss.ID_MPE_OLD
                 from concentrateur.suivi_annonces ss
                          inner join concentrateur.organisme o on o.id = ss.ID_ORGANISME
                 where o.plateforme_uuid = @plateforme_uuid_to_update
                   and ss.ID_MPE_OLD = CONCAT('Destinataire_Annonce_Press-', desJal.id));


# add annonce JAL without jal in mpe.destinataire_annonce_press

INSERT INTO concentrateur.suivi_annonces(DATE_TRAITEMENT, NUMERO_DOSSIER, DATE_PUBLICATION, ID_CONSULTATION, ID_OFFRE,
                                         DATE_TRAITEMENT_INTERNE, DATE_DEMANDE, ID_PLATFORM, ORGANISME, DATE_CREATION,
                                         DATE_MODIFICATION, ID_AGENT, ID_ORGANISME,
                                         ID_SUPPORT, ID_SERVICE, ID_PROCEDURE,
                                         ID_SUIVI_TYPE_AVIS_PUB, ID_MPE_OLD, STATUT)
SELECT suivi.DATE_ENVOI_NATIONAL,
       dest.id_dossier,
       dest.date_publication,
       suivi.ID_CONSULTATION,
       suivi.ID_OFFRE_RACINE,
       suivi.DATE_TRAITEMENT,
       suivi.DATE_ENVOI_NATIONAL,
       o.plateforme_uuid,
       o.acronyme_organisme,
       suivi.DATE_CREATION,
       CASE
           WHEN dest.DATE_MODIFICATION = '' THEN suivi.DATE_CREATION
           ELSE dest.DATE_MODIFICATION
           END
        ,
       suivi.ID_CREER_PAR,
       suivi.ID_ORGANISME,
       s.ID as supportId,
       suivi.ID_SERVICE,
       suivi.ID_PROCEDURE,
       suivi.ID,
       CONCAT('Destinataire_Pub-', dest.id),
       CASE
           WHEN dest.date_publication is not null THEN 'PUBLIER'
           WHEN ap.statut = '1' THEN 'EN_ATTENTE_VALIDATION_ECO'
           WHEN ap.statut = '2' THEN 'REJETER_CONCENTRATEUR_VALIDATION_ECO'
           WHEN ap.statut = '3' THEN 'EN_ATTENTE_VALIDATION_SIP'
           WHEN ap.statut = '4' THEN 'ENVOI_PLANIFIER'
           WHEN ap.statut = '5' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN ap.statut = '6' THEN 'REJETER_CONCENTRATEUR_VALIDATION_SIP'
           WHEN dest.etat = '5' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '6' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '7' THEN 'PUBLIER'
           WHEN dest.etat = '8' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '9' THEN 'PUBLIER'
           WHEN dest.etat = '10' THEN 'REJETER_SUPPORT'
           END
from mpe.Annonce_Press form
         inner join mpe.Destinataire_Pub dest on dest.id = form.id_Dest_Press
         inner join mpe.Avis_Pub ap on ap.id = dest.id_avis
         inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
         inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme,
     (select min(ID) as ID from concentrateur.support where CODE like CONCAT(@plateforme_lang_to_update, '-%')) as s
where not (exists (select children1_.ID
                   from mpe.destinataire_annonce_press children1_
                   where form.ID = children1_.id_annonce_press))
  and dest.id_support = 2
  and o.plateforme_uuid = @plateforme_uuid_to_update
  and ap.statut <> '0'
  and not exists(select distinct ss.ID_MPE_OLD
                 from concentrateur.suivi_annonces ss
                          inner join concentrateur.organisme o on o.id = ss.ID_ORGANISME
                 where o.plateforme_uuid = @plateforme_uuid_to_update
                   and ss.ID_MPE_OLD = CONCAT('Destinataire_Pub-', dest.id));

# add annonce JAL without jal in mpe.destinataire_annonce_press

INSERT INTO concentrateur.suivi_annonces(DATE_TRAITEMENT, NUMERO_DOSSIER, DATE_PUBLICATION, ID_CONSULTATION, ID_OFFRE,
                                         DATE_TRAITEMENT_INTERNE, DATE_DEMANDE, ID_PLATFORM, ORGANISME, DATE_CREATION,
                                         DATE_MODIFICATION, ID_AGENT, ID_ORGANISME,
                                         ID_SUPPORT, ID_SERVICE, ID_PROCEDURE,
                                         ID_SUIVI_TYPE_AVIS_PUB, ID_MPE_OLD, STATUT)
SELECT suivi.DATE_ENVOI_NATIONAL,
       null,
       null,
       suivi.ID_CONSULTATION,
       suivi.ID_OFFRE_RACINE,
       suivi.DATE_TRAITEMENT,
       suivi.DATE_ENVOI_NATIONAL,
       o.plateforme_uuid,
       o.acronyme_organisme,
       suivi.DATE_CREATION,
       ap.date_creation,

       suivi.ID_CREER_PAR,
       suivi.ID_ORGANISME,
       s.ID as supportId,
       suivi.ID_SERVICE,
       suivi.ID_PROCEDURE,
       suivi.ID,
       CONCAT('Avis_Publie_', ap.id),
       CASE
           WHEN ap.date_publication is not null THEN 'PUBLIER'
           WHEN ap.statut = '1' THEN 'EN_ATTENTE_VALIDATION_ECO'
           WHEN ap.statut = '2' THEN 'REJETER_CONCENTRATEUR_VALIDATION_ECO'
           WHEN ap.statut = '3' THEN 'EN_ATTENTE_VALIDATION_SIP'
           WHEN ap.statut = '4' THEN 'ENVOI_PLANIFIER'
           WHEN ap.statut = '5' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN ap.statut = '6' THEN 'REJETER_CONCENTRATEUR_VALIDATION_SIP'
           END
from mpe.Avis_Pub ap
         inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
         inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme,
     (select min(ID) as ID from concentrateur.support where CODE like CONCAT(@plateforme_lang_to_update, '-%')) as s
where not (exists (select children1_.ID
                   from mpe.Destinataire_Pub children1_
                   where ap.ID = children1_.id_avis
                     and children1_.id_support = 2))
  and o.plateforme_uuid = @plateforme_uuid_to_update
  and ap.statut <> '0'
  and suivi.ID_FACTURATION is not null
  and not exists(select distinct ss.ID_MPE_OLD
                 from concentrateur.suivi_annonces ss
                          inner join concentrateur.organisme o on o.id = ss.ID_ORGANISME
                 where o.plateforme_uuid = @plateforme_uuid_to_update
                   and ss.ID_MPE_OLD = CONCAT('Avis_Publie_', ap.id));

#update suivi_annonces EU false piece jointe
update concentrateur.suivi_annonces sa inner join concentrateur.support s on s.id = sa.ID_SUPPORT
    inner join concentrateur.suivi_type_avis_pub st on st.ID = sa.ID_SUIVI_TYPE_AVIS_PUB
    inner join mpe.Avis_Pub ap on ap.id = st.ID_MPE_OLD
    inner join mpe.annonce_press_piecejointe piece on piece.id = ap.id_avis_presse
    inner join mpe.bloborganisme_file b on b.id = piece.piece
    inner join concentrateur.piece_jointe pj on pj.ID_MPE_OLD =
                                                CONCAT('Annonce_Press_PieceJointe-', ap.id_avis_presse)
SET sa.ID_AVIS_FICHIER= pj.id
where s.EUROPEEN is false
  and (sa.ID_AVIS_FICHIER is null or sa.ID_AVIS_FICHIER <> pj.id)
  and b.old_id is null;

#update suivi_annonces EU false piece jointe
update concentrateur.suivi_annonces sa inner join concentrateur.support s on s.id = sa.ID_SUPPORT
    inner join concentrateur.suivi_type_avis_pub st on st.ID = sa.ID_SUIVI_TYPE_AVIS_PUB
    inner join mpe.Avis_Pub ap on ap.id = st.ID_MPE_OLD
    inner join mpe.annonce_press_piecejointe piece on piece.id = ap.id_avis_presse
    inner join mpe.bloborganisme_file b on b.id = piece.piece
    inner join concentrateur.piece_jointe pj on pj.ID_MPE_OLD =
                                                CONCAT('blobOrganisme_file-old-id-', b.old_id)
SET sa.ID_AVIS_FICHIER= pj.id
where s.EUROPEEN is false
  and (sa.ID_AVIS_FICHIER is null or sa.ID_AVIS_FICHIER <> pj.id)
  and b.old_id is not null;
# début AN-627

#update suivi_annonces EU true piece jointe
update concentrateur.suivi_annonces sa inner join concentrateur.support s on s.id = sa.ID_SUPPORT
    inner join concentrateur.suivi_type_avis_pub st on st.ID = sa.ID_SUIVI_TYPE_AVIS_PUB
    inner join mpe.Avis_Pub ap on ap.id = st.ID_MPE_OLD
    inner join mpe.Avis a on a.id = ap.id_avis_pdf_opoce
    inner join mpe.bloborganisme_file b on b.id = a.avis
    inner join concentrateur.piece_jointe pj on pj.ID_MPE_OLD = CONCAT('blobOrganisme_file-', b.id)
SET sa.ID_AVIS_FICHIER= pj.id
where s.EUROPEEN is true
  and (sa.ID_AVIS_FICHIER is null or sa.ID_AVIS_FICHIER <> pj.id)
  and sa.LIEN_PUBLICATION is null
  and b.old_id is null;

#update suivi_annonces EU true piece jointe
update concentrateur.suivi_annonces sa inner join concentrateur.support s on s.id = sa.ID_SUPPORT
    inner join concentrateur.suivi_type_avis_pub st on st.ID = sa.ID_SUIVI_TYPE_AVIS_PUB
    inner join mpe.Avis_Pub ap on ap.id = st.ID_MPE_OLD
    inner join mpe.Avis a on a.id = ap.id_avis_pdf_opoce
    inner join mpe.bloborganisme_file b on b.id = a.avis
    inner join concentrateur.piece_jointe pj on pj.ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id)
SET sa.ID_AVIS_FICHIER= pj.id
where s.EUROPEEN is true
  and b.old_id is not null
  and (sa.ID_AVIS_FICHIER is null or sa.ID_AVIS_FICHIER <> pj.id)
  and sa.LIEN_PUBLICATION is null;
# fin AN-627




