# Ajout Type Avis PUB
INSERT INTO concentrateur.suivi_type_avis_pub(ID_MPE_OLD,
                                              ID_ORGANISME,
                                              ID_PROCEDURE,
                                              ID_SERVICE,
                                              ID_CONSULTATION,
                                              REFERENCE,
                                              TITRE,
                                              OBJET,
                                              DATE_MISE_EN_LIGNE,
                                              DATE_MISE_EN_LIGNE_CALCULE, DLRO, DATE_VALIDATION,
                                              DATE_CREATION, DATE_MODIFICATION)
SELECT avisPub.id,
       organismeConsultation.id,
       procedureConsult.id_type_procedure,
       refService.id,
       avisPub.consultation_id,
       consultation.reference_utilisateur,
       consultation.intitule,
       consultation.objet,
       CASE
           WHEN consultation.date_mise_en_ligne_souhaitee = '2018-02-29' THEN '2018-02-28'
           WHEN consultation.date_mise_en_ligne_souhaitee not in ('--null ::00')
               THEN consultation.date_mise_en_ligne_souhaitee
           ELSE consultation.date_mise_en_ligne_calcule END,
       consultation.date_mise_en_ligne_calcule,
       consultation.date_fin_affichage,
       avisPub.date_validation,
       avisPub.date_creation,
       avisPub.date_creation

from mpe.Avis_Pub avisPub
         left join concentrateur.organisme organismeConsultation
                   on organismeConsultation.acronyme_organisme = avisPub.organisme
         left join mpe.Consultation consultation on consultation.id = avisPub.consultation_id
         left join mpe.Service service on service.id = consultation.service_id
         left join concentrateur.referential_service_mpe refService
                   on refService.ID_MPE = service.id and refService.id_organisme = organismeConsultation.id
         left join concentrateur.type_procedure procedureConsult
                   on procedureConsult.id_type_procedure = consultation.id_type_procedure
         left join mpe.Type_Avis_Pub tap on tap.id = avisPub.type_avis

WHERE organismeConsultation.plateforme_uuid = @plateforme_uuid_to_update
  and avisPub.statut <> '0'
  and not exists(select distinct ss.ID_MPE_OLD
                 from concentrateur.suivi_type_avis_pub ss
                          inner join concentrateur.organisme o on o.id = ss.ID_ORGANISME
                 where o.plateforme_uuid = @plateforme_uuid_to_update
                   and ss.ID_MPE_OLD = avisPub.id);

#UPDATE AGENT CREATEUR

UPDATE concentrateur.suivi_type_avis_pub toUpdate
SET toUpdate.ID_CREER_PAR = (SELECT a.id
                             from concentrateur.agent a
                                      inner join mpe.avis_pub ap on a.id_mpe = ap.id_agent
                                      inner join mpe.Agent agent on ap.id_agent = agent.id
                                      inner join concentrateur.organisme o
                                                 on agent.organisme = o.acronyme_organisme and a.id_organisme = o.id
                             where ap.id = toUpdate.ID_MPE_OLD
                               and o.plateforme_uuid = @plateforme_uuid_to_update)
where toUpdate.ID_MPE_OLD is not null;

#UPDATE AGENT MODIFIE

UPDATE concentrateur.suivi_type_avis_pub toUpdate
SET toUpdate.ID_MODIFIER_PAR = (SELECT a.id
                                from concentrateur.agent a
                                         inner join mpe.avis_pub ap on a.id_mpe = ap.id_agent
                                         inner join mpe.Agent agent on ap.id_agent = agent.id
                                         inner join concentrateur.organisme o
                                                    on agent.organisme = o.acronyme_organisme and a.id_organisme = o.id
                                where ap.id = toUpdate.ID_MPE_OLD
                                  and o.plateforme_uuid = @plateforme_uuid_to_update)
where toUpdate.ID_MPE_OLD is not null;

#UPDATE AGENT VALIDATEUR

UPDATE concentrateur.suivi_type_avis_pub toUpdate
SET toUpdate.ID_VALIDATEUR = (SELECT a.id
                              from concentrateur.agent a
                                       inner join mpe.avis_pub ap on a.id_mpe = ap.id_agent_validateur
                                       inner join mpe.Agent agent on ap.id_agent = agent.id
                                       inner join concentrateur.organisme o
                                                  on agent.organisme = o.acronyme_organisme and a.id_organisme = o.id
                              where ap.id = toUpdate.ID_MPE_OLD
                                and o.plateforme_uuid = @plateforme_uuid_to_update)
where toUpdate.ID_MPE_OLD is not null;

#UPDATE AGENT VALIDATEUR ECO

UPDATE concentrateur.suivi_type_avis_pub toUpdate
SET toUpdate.ID_VALIDATEUR_ECO = (SELECT a.id
                                  from concentrateur.agent a
                                           inner join mpe.avis_pub ap on a.id_mpe = ap.id_agent_validateur_eco
                                           inner join mpe.Agent agent on ap.id_agent = agent.id
                                           inner join concentrateur.organisme o
                                                      on agent.organisme = o.acronyme_organisme and a.id_organisme = o.id
                                  where ap.id = toUpdate.ID_MPE_OLD
                                    and o.plateforme_uuid = @plateforme_uuid_to_update)
where toUpdate.ID_MPE_OLD is not null;
#UPDATE AGENT VALIDATEUR SIP

UPDATE concentrateur.suivi_type_avis_pub toUpdate
SET toUpdate.ID_VALIDATEUR_SIP = (SELECT a.id
                                  from concentrateur.agent a
                                           inner join mpe.avis_pub ap on a.id_mpe = ap.id_agent_validateur_sip
                                           inner join mpe.Agent agent on ap.id_agent = agent.id
                                           inner join concentrateur.organisme o
                                                      on agent.organisme = o.acronyme_organisme and a.id_organisme = o.id
                                  where ap.id = toUpdate.ID_MPE_OLD
                                    and o.plateforme_uuid = @plateforme_uuid_to_update)
where toUpdate.ID_MPE_OLD is not null;


#Mise � jour EU

UPDATE concentrateur.suivi_type_avis_pub
SET EU = true
where EU in not true and ID in (SELECT distinct suivi.ID
             from mpe.FormXmlDestinataireOpoce opoce
                      inner join mpe.Destinataire_Pub dp on dp.id = opoce.id_destinataire_opoce
                      inner join mpe.Avis_Pub ap on ap.id = dp.id_avis
                      inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                      inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
             WHERE o.plateforme_uuid = @plateforme_uuid_to_update
               and ap.id = suivi.ID_MPE_OLD
               and suivi.ID_ORGANISME = o.id
             UNION
             SELECT distinct suivi.ID
             from mpe.Avis_Pub ap
                      inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                      inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
             WHERE o.plateforme_uuid = @plateforme_uuid_to_update
               and ap.id = suivi.ID_MPE_OLD
               and ap.id_avis_pdf_opoce is not null
               and suivi.ID_ORGANISME = o.id);

UPDATE concentrateur.suivi_type_avis_pub
SET EU = false
where EU is null;


UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            concentrateur.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'LU01%'
         and offre.CODE = 'LUANMARCHE') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            concentrateur.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'LU03%'
         and offre.CODE = 'LUANRECTIFICATIF') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;



UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            concentrateur.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU02a%'
         and offre.CODE = '16_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;

UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            concentrateur.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU05b%'
         and offre.CODE = '16_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            concentrateur.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'LU02%'
         and offre.CODE = 'LUANCANDIDATURE') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;

UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            concentrateur.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU01%'
         and offre.CODE = '4_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            concentrateur.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU03%'
         and offre.CODE = '29_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;

UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            concentrateur.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU14%'
         and offre.CODE = '16_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            concentrateur.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU24%'
         and offre.CODE = '19_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            concentrateur.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU07%'
         and offre.CODE = '16_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;

UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            concentrateur.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU12%'
         and offre.CODE = '23_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            concentrateur.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU13%'
         and offre.CODE = '36_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            concentrateur.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU15%'
         and offre.CODE = '25_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            concentrateur.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU02b%'
         and offre.CODE = '16_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            concentrateur.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU05a%'
         and offre.CODE = '16_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            concentrateur.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU04%'
         and offre.CODE = '5_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;

UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            concentrateur.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU06%'
         and offre.CODE = '29_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            concentrateur.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'LU09%'
         and offre.CODE = '19_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            concentrateur.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'LU05%'
         and offre.CODE = 'LUANDEMANDEOFFRE') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


#type d'avis n'existe pas
UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId
       from concentrateur.offre offre
       WHERE offre.CODE = '16_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID_OFFRE_RACINE is null
  and suivi.EU is true;
UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId
       from concentrateur.offre offre
       WHERE offre.CODE = 'LUANMARCHE') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID_OFFRE_RACINE is null
  and suivi.EU is false
  and suivi.ID_OFFRE_RACINE <> A.offreId;


#Date d envoi eu / date traitement
UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct ap.date_envoi as DATE_TRAITEMENT, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.DATE_TRAITEMENT = A.DATE_TRAITEMENT
where suivi.ID = A.suiviId;

UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct ap.date_envoi as date_envoi, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.DATE_ENVOI_EUROPEEN = A.date_envoi
where suivi.ID = A.suiviId;

# update contenu mail
UPDATE concentrateur.suivi_type_avis_pub toUpdate
    , (SELECT ap.objet as objet, ap.texte as contenu, suivi.ID as suiviId
       from mpe.Annonce_Press ap
                inner join mpe.Destinataire_Pub dp on dp.id = ap.id_Dest_Press
                inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = apu.id
                inner join concentrateur.organisme o on o.acronyme_organisme = apu.organisme
       where o.plateforme_uuid = @plateforme_uuid_to_update) as A
SET toUpdate.MAIL_CONTENU = A.contenu,
    toUpdate.MAIL_OBJET   = A.objet
where toUpdate.ID = A.suiviId;

#Update facturation
UPDATE concentrateur.suivi_type_avis_pub toUpdate
    , (SELECT cf.ID as facturationId, suivi.ID as suiviId
       from mpe.Annonce_Press ap
                inner join mpe.Destinataire_Pub dp on dp.id = ap.id_Dest_Press
                inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = apu.id
                inner join concentrateur.organisme o on o.acronyme_organisme = apu.organisme
                inner join mpe.AdresseFacturationJal facturation on facturation.id = ap.id_adresse_facturation
                inner join concentrateur.consultation_facturation cf
       where cf.MAIL = facturation.email_ar
         and cf.ADRESSE = facturation.information_facturation
         and cf.SIP is false
         and facturation.facturation_sip = '0'
         and o.plateforme_uuid = @plateforme_uuid_to_update
         and not exists (select ID_FACTURATION, ID
                         from concentrateur.suivi_type_avis_pub
                         where (ID_FACTURATION, ID) = (cf.ID, suivi.ID))) as A
SET toUpdate.ID_FACTURATION = A.facturationId
where toUpdate.ID = A.suiviId
  and toUpdate.ID_FACTURATION <> A.facturationId;


UPDATE concentrateur.suivi_type_avis_pub toUpdate
    , (SELECT cf.ID as facturationId, suivi.ID as suiviId
       from mpe.Annonce_Press ap
                inner join mpe.Destinataire_Pub dp on dp.id = ap.id_Dest_Press
                inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = apu.id
                inner join concentrateur.organisme o on o.acronyme_organisme = apu.organisme
                inner join mpe.AdresseFacturationJal facturation on facturation.id = ap.id_adresse_facturation
                inner join concentrateur.consultation_facturation cf
       where cf.MAIL = facturation.email_ar
         and cf.ADRESSE = facturation.information_facturation
         and cf.SIP is true
         and facturation.facturation_sip = '1'
         and o.plateforme_uuid = @plateforme_uuid_to_update
         and not exists (select ID_FACTURATION, ID
                         from concentrateur.suivi_type_avis_pub
                         where (ID_FACTURATION, ID) = (cf.ID, suivi.ID))) as A
SET toUpdate.ID_FACTURATION = A.facturationId
where toUpdate.ID = A.suiviId
  and toUpdate.ID_FACTURATION <> A.facturationId;


UPDATE concentrateur.suivi_type_avis_pub toUpdate
    , (SELECT min(cf.ID) as facturationId, suivi.ID as suiviId
       from mpe.Annonce_Press ap
                inner join mpe.Destinataire_Pub dp on dp.id = ap.id_Dest_Press
                inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = apu.id
                inner join concentrateur.organisme o on o.acronyme_organisme = apu.organisme,
            mpe.AdresseFacturationJal facturation
                inner join concentrateur.consultation_facturation cf
       where cf.MAIL = facturation.email_ar
         and cf.ADRESSE = facturation.information_facturation
         and cf.SIP is false
         and ap.id_adresse_facturation is null
         and facturation.facturation_sip = '0'
         and o.plateforme_uuid = @plateforme_uuid_to_update
       group by suiviId) as A
SET toUpdate.ID_FACTURATION = A.facturationId
where toUpdate.ID = A.suiviId
  and toUpdate.ID_FACTURATION <> A.facturationId;

UPDATE concentrateur.suivi_type_avis_pub toUpdate
    , (SELECT min(cf.ID) as facturationId, suivi.ID as suiviId
       from mpe.Annonce_Press ap
                inner join mpe.Destinataire_Pub dp on dp.id = ap.id_Dest_Press
                inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = apu.id
                inner join concentrateur.organisme o on o.acronyme_organisme = apu.organisme,
            mpe.AdresseFacturationJal facturation
                inner join concentrateur.consultation_facturation cf
       where cf.MAIL = facturation.email_ar
         and cf.ADRESSE = facturation.information_facturation
         and cf.SIP is true
         and ap.id_adresse_facturation is null
         and facturation.facturation_sip = '1'
         and o.plateforme_uuid = @plateforme_uuid_to_update
       group by suiviId) as A
SET toUpdate.ID_FACTURATION = A.facturationId
where toUpdate.ID = A.suiviId
  and toUpdate.ID_FACTURATION <> A.facturationId;

UPDATE concentrateur.suivi_type_avis_pub toUpdate
    , (select ap.ID as id
       from concentrateur.suivi_type_avis_pub ap
                inner join mpe.avis_pub ap2 on ap.ID_MPE_OLD = ap2.id
       where ap.ID_FACTURATION is null
         and ap2.Sip = '1') as A
SET toUpdate.ID_FACTURATION = (SELECT min(cf.ID)
                               from mpe.AdresseFacturationJal facturation
                                        inner join concentrateur.consultation_facturation cf
                               where cf.MAIL = facturation.email_ar
                                 and cf.ADRESSE = facturation.information_facturation
                                 and cf.SIP is true
                                 and facturation.facturation_sip = '1')
where toUpdate.ID = A.id
  and toUpdate.EU is false;

UPDATE concentrateur.suivi_type_avis_pub toUpdate
    , (select ap.ID as id
       from concentrateur.suivi_type_avis_pub ap
                inner join mpe.avis_pub ap2 on ap.ID_MPE_OLD = ap2.id
       where ap.ID_FACTURATION is null
         and ap2.Sip = '0') as A
SET toUpdate.ID_FACTURATION = (SELECT min(cf.ID)
                               from mpe.AdresseFacturationJal facturation
                                        inner join concentrateur.consultation_facturation cf
                               where cf.MAIL = facturation.email_ar
                                 and cf.ADRESSE = facturation.information_facturation
                                 and cf.SIP is false
                                 and facturation.facturation_sip = '0')
where toUpdate.ID = A.id
  and toUpdate.EU is false;

UPDATE concentrateur.suivi_type_avis_pub toUpdate inner join concentrateur.offre o
    on toUpdate.ID_OFFRE_RACINE = o.ID
    , (select ap.ID as id
       from concentrateur.suivi_type_avis_pub ap
                inner join mpe.avis_pub ap2 on ap.ID_MPE_OLD = ap2.id
       where ap.ID_FACTURATION is null
         and ap2.Sip = '1') as A
SET toUpdate.ID_FACTURATION = (SELECT min(cf.ID)
                               from mpe.AdresseFacturationJal facturation
                                        inner join concentrateur.consultation_facturation cf
                               where cf.MAIL = facturation.email_ar
                                 and cf.ADRESSE = facturation.information_facturation
                                 and cf.SIP is true
                                 and facturation.facturation_sip = '1')
where toUpdate.ID = A.id
  and toUpdate.EU is true
  and o.ID_OFFRE_ASSOCIEE is not null;

UPDATE concentrateur.suivi_type_avis_pub toUpdate inner join concentrateur.offre o
    on toUpdate.ID_OFFRE_RACINE = o.ID
    , (select ap.ID as id
       from concentrateur.suivi_type_avis_pub ap
                inner join mpe.avis_pub ap2 on ap.ID_MPE_OLD = ap2.id
       where ap.ID_FACTURATION is null
         and ap2.Sip = '0') as A
SET toUpdate.ID_FACTURATION = (SELECT min(cf.ID)
                               from mpe.AdresseFacturationJal facturation
                                        inner join concentrateur.consultation_facturation cf
                               where cf.MAIL = facturation.email_ar
                                 and cf.ADRESSE = facturation.information_facturation
                                 and cf.SIP is false
                                 and facturation.facturation_sip = '0')
where toUpdate.ID = A.id
  and toUpdate.EU is true
  and o.ID_OFFRE_ASSOCIEE is not null;

# update when SIP is null

UPDATE concentrateur.suivi_type_avis_pub toUpdate
    , (select ap.ID as id
       from concentrateur.suivi_type_avis_pub ap
                inner join mpe.avis_pub ap2 on ap.ID_MPE_OLD = ap2.id
       where ap.ID_FACTURATION is null
         and ap2.Sip is null) as A
SET toUpdate.ID_FACTURATION = (SELECT min(cf.ID)
                               from mpe.AdresseFacturationJal facturation
                                        inner join concentrateur.consultation_facturation cf
                               where cf.MAIL = facturation.email_ar
                                 and cf.ADRESSE = facturation.information_facturation
                                 and cf.SIP is false
                                 and facturation.facturation_sip = '0')
where toUpdate.ID = A.id
  and toUpdate.EU is false;

UPDATE concentrateur.suivi_type_avis_pub toUpdate inner join concentrateur.offre o
    on toUpdate.ID_OFFRE_RACINE = o.ID
    , (select ap.ID as id
       from concentrateur.suivi_type_avis_pub ap
                inner join mpe.avis_pub ap2 on ap.ID_MPE_OLD = ap2.id
       where ap.ID_FACTURATION is null
         and ap2.Sip is null) as A
SET toUpdate.ID_FACTURATION = (SELECT min(cf.ID)
                               from mpe.AdresseFacturationJal facturation
                                        inner join concentrateur.consultation_facturation cf
                               where cf.MAIL = facturation.email_ar
                                 and cf.ADRESSE = facturation.information_facturation
                                 and cf.SIP is false
                                 and facturation.facturation_sip = '0')
where toUpdate.ID = A.id
  and toUpdate.EU is true
  and o.ID_OFFRE_ASSOCIEE is not null;


#Update Statut Suivi Avis Pub depuis l 'historique
UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId, h.motif_rejet as motifRejet, SUBSTR(h.date_modification, 1, 16) as dateModif
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Historique_Avis_Pub h on h.id_avis = ap.id
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and h.detail_statut = '1'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT                   = 'EN_ATTENTE_VALIDATION_ECO',
    suivi.DATE_MODIFICATION        = A.dateModif,
    suivi.DATE_MODIFICATION_STATUT = A.dateModif,
    suivi.DATE_VALIDATION_NATIONAL = A.dateModif,
    suivi.DATE_VALIDATION_EUROPEEN = A.dateModif,
    suivi.DATE_VALIDATION          = A.dateModif
where suivi.ID = A.suiviId;


UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId, h.motif_rejet as motifRejet, SUBSTR(h.date_modification, 1, 16) as dateModif
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Historique_Avis_Pub h on h.id_avis = ap.id
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and h.detail_statut = '2'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT                   = 'REJETE_ECO',
    suivi.DATE_MODIFICATION        = A.dateModif,
    suivi.DATE_MODIFICATION_STATUT = A.dateModif,
    suivi.DATE_VALIDATION_ECO      = A.dateModif,
    suivi.VALIDER_ECO              = false,
    suivi.RAISON_REFUS             = A.motifRejet
where suivi.ID = A.suiviId;

UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId, h.motif_rejet as motifRejet, SUBSTR(h.date_modification, 1, 16) as dateModif
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Historique_Avis_Pub h on h.id_avis = ap.id
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and h.detail_statut = '3'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT                   = 'EN_ATTENTE_VALIDATION_SIP',
    suivi.DATE_MODIFICATION        = A.dateModif,
    suivi.DATE_MODIFICATION_STATUT = A.dateModif,
    suivi.DATE_VALIDATION_ECO      = A.dateModif,
    suivi.VALIDER_ECO              = true,
    suivi.RAISON_REFUS             = A.motifRejet
where suivi.ID = A.suiviId;

UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId, h.motif_rejet as motifRejet, SUBSTR(h.date_modification, 1, 16) as dateModif
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Historique_Avis_Pub h on h.id_avis = ap.id
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.Sip = '0'
         and h.detail_statut = '4'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT                   = 'ENVOI_PLANIFIER',
    suivi.DATE_MODIFICATION        = A.dateModif,
    suivi.DATE_MODIFICATION_STATUT = A.dateModif,
    suivi.DATE_VALIDATION_ECO      = A.dateModif,
    suivi.VALIDER_ECO              = true,
    suivi.RAISON_REFUS             = A.motifRejet
where suivi.ID = A.suiviId;

UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId, h.motif_rejet as motifRejet, SUBSTR(h.date_modification, 1, 16) as dateModif
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Historique_Avis_Pub h on h.id_avis = ap.id
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.Sip = ' 1 '
         and h.detail_statut = ' 4 '
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT                   = ' ENVOI_PLANIFIER ',
    suivi.DATE_MODIFICATION        = A.dateModif,
    suivi.DATE_MODIFICATION_STATUT = A.dateModif,
    suivi.DATE_VALIDATION_SIP      = A.dateModif,
    suivi.VALIDER_SIP              = true,
    suivi.RAISON_REFUS             = A.motifRejet
where suivi.ID = A.suiviId;


UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId, h.motif_rejet as motifRejet, SUBSTR(h.date_modification, 1, 16) as dateModif
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Historique_Avis_Pub h on h.id_avis = ap.id
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and h.detail_statut = '5'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT                   = 'ENVOYE',
    suivi.VALIDER_ECO= true,
    suivi.DATE_MODIFICATION        = A.dateModif,
    suivi.DATE_MODIFICATION_STATUT = A.dateModif,
    suivi.DATE_ENVOI_EUROPEEN      = A.dateModif
where suivi.ID = A.suiviId
  and suivi.EU is true;

UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId, h.motif_rejet as motifRejet, SUBSTR(h.date_modification, 1, 16) as dateModif
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Historique_Avis_Pub h on h.id_avis = ap.id
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and h.detail_statut = '5'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT                   = 'ENVOYE',
    suivi.VALIDER_ECO= true,
    suivi.DATE_MODIFICATION        = A.dateModif,
    suivi.DATE_MODIFICATION_STATUT = A.dateModif,
    suivi.DATE_ENVOI_NATIONAL      = A.dateModif
where suivi.ID = A.suiviId
  and suivi.EU is true
  and suivi.ID_FACTURATION is not null;


UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId, h.motif_rejet as motifRejet, SUBSTR(h.date_modification, 1, 16) as dateModif
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Historique_Avis_Pub h on h.id_avis = ap.id
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and h.detail_statut = '5'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT                   = 'ENVOYE',
    suivi.VALIDER_ECO= true,
    suivi.DATE_MODIFICATION        = A.dateModif,
    suivi.DATE_MODIFICATION_STATUT = A.dateModif,
    suivi.DATE_ENVOI_NATIONAL      = A.dateModif
where suivi.ID = A.suiviId
  and suivi.EU is false;

UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId, h.motif_rejet as motifRejet, SUBSTR(h.date_modification, 1, 16) as dateModif
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Historique_Avis_Pub h on h.id_avis = ap.id
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and h.detail_statut = '6'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT                   = 'REJETE_SIP',
    suivi.DATE_MODIFICATION        = A.dateModif,
    suivi.DATE_MODIFICATION_STATUT = A.dateModif,
    suivi.DATE_VALIDATION_SIP      = A.dateModif,
    suivi.VALIDER_SIP              = false,
    suivi.RAISON_REFUS             = A.motifRejet
where suivi.ID = A.suiviId;
# -----------------------------------------

# Update Statut Suivi Avis Pub depuis l avis publié
UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.statut = '1'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT = 'EN_ATTENTE_VALIDATION_ECO'
where suivi.ID = A.suiviId;


UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.statut = '2'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT      = 'REJETE_ECO',
    suivi.VALIDER_ECO = false
where suivi.ID = A.suiviId;

UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.statut = '3'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT      = 'EN_ATTENTE_VALIDATION_SIP',
    suivi.VALIDER_ECO = true
where suivi.ID = A.suiviId;

UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.Sip = '0'
         and ap.statut = '4'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT      = 'ENVOI_PLANIFIER',
    suivi.VALIDER_ECO = true
where suivi.ID = A.suiviId;

UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Historique_Avis_Pub h on h.id_avis = ap.id
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.Sip = '1'
         and h.detail_statut = '4'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT      = 'ENVOI_PLANIFIER',
    suivi.VALIDER_SIP = true
where suivi.ID = A.suiviId;


UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.statut = '5'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT     = 'ENVOYE'
  , suivi.VALIDER_ECO= true
where suivi.ID = A.suiviId
  and suivi.EU is true;

UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.statut = '5'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT     = 'ENVOYE'
  , suivi.VALIDER_ECO= true
where suivi.ID = A.suiviId
  and suivi.EU is true
  and suivi.ID_FACTURATION is not null;


UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.statut = '5'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT     = 'ENVOYE',
    suivi.VALIDER_ECO= true
where suivi.ID = A.suiviId
  and suivi.EU is false;

UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.statut = '6'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT      = 'REJETE_SIP',
    suivi.VALIDER_SIP = false
where suivi.ID = A.suiviId;

update concentrateur.suivi_type_avis_pub
set DATE_MISE_EN_LIGNE_CALCULE= null,
    DATE_ENVOI_NATIONAL       = null,
    DATE_ENVOI_EUROPEEN       = null
where STATUT in ('REJETE_SIP', 'REJETE_ECO', 'COMPLET', 'BROUILLON');


UPDATE concentrateur.suivi_type_avis_pub suivi
set suivi.VALIDER_ECO= true
where suivi.STATUT = 'ENVOYE';

UPDATE concentrateur.suivi_type_avis_pub suivi
set suivi.VALIDER_SIP   = null,
    DATE_VALIDATION_SIP = null,
    STATUT              = 'REJETE_ECO'
where suivi.VALIDER_ECO is false;

UPDATE concentrateur.suivi_type_avis_pub suivi
set STATUT = 'REJETE_SIP'
where suivi.VALIDER_SIP is false;

# update envoie national
UPDATE concentrateur.suivi_type_avis_pub suivi
    , (SELECT distinct ap.date_envoi as date_envoi, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.DATE_ENVOI_NATIONAL = A.date_envoi
where suivi.ID = A.suiviId
  and suivi.ID_FACTURATION is not null;

# -----------------------------------------
update concentrateur.suivi_type_avis_pub inner join concentrateur.offre annonce
    on annonce.id = concentrateur.suivi_type_avis_pub.ID_OFFRE_RACINE
SET EU = true
where annonce.CODE like '%_ENOTICES';

delete
from concentrateur.suivi_type_avis_pub
where STATUT = 'BROUILLON'
  and ID_MPE_OLD is not null;

# début AN-627

# ajout des PDF Portail
update concentrateur.suivi_type_avis_pub st
    inner join mpe.Avis_Pub ap on ap.id = st.ID_MPE_OLD
    inner join mpe.Avis a on a.id = ap.id_avis_portail
    inner join mpe.blobOrganisme_file b on b.id = a.avis
    inner join concentrateur.piece_jointe pj on pj.ID_MPE_OLD = CONCAT('blobOrganisme_file-', b.id)
SET st.ID_AVIS_PORTAIL= pj.id
where (st.ID_AVIS_PORTAIL is null or st.ID_AVIS_PORTAIL <> pj.id)
  and b.old_id is null;


update concentrateur.suivi_type_avis_pub st
    inner join mpe.Avis_Pub ap on ap.id = st.ID_MPE_OLD
    inner join mpe.Avis a on a.id = ap.id_avis_portail
    inner join mpe.blobOrganisme_file b on b.id = a.avis
    inner join concentrateur.piece_jointe pj on pj.ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id)
SET st.ID_AVIS_PORTAIL= pj.id
where b.old_id is not null
  and (st.ID_AVIS_PORTAIL is null or st.ID_AVIS_PORTAIL <> pj.id);
# fin AN-627
