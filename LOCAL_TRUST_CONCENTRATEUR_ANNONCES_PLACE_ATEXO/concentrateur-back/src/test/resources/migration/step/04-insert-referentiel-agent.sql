#Ajout des agents avis PUB sans service
INSERT INTO concentrateur.agent (id, acheteur_public, identifiant, nom, prenom, id_organisme,
                                 id_service, email, id_mpe,
                                 telephone, fax, roles)
SELECT distinct UUID(),
                o.libelle_organisme,
                ap.login,
                ap.nom,
                ap.prenom,
                o.id,
                null,
                ap.email,
                ap.id,
                ap.num_tel,
                ap.num_fax,
                ''
from mpe.Agent ap
         inner join concentrateur.organisme o
                    on ap.organisme = o.acronyme_organisme and o.plateforme_uuid = @plateforme_uuid_to_update

where ap.id in (select distinct id_agent_validateur
                from mpe.Avis_Pub
                where id_agent_validateur is not null
                UNION
                select distinct id_agent_validateur_eco
                from mpe.Avis_Pub
                where id_agent_validateur_eco is not null
                UNION
                select distinct id_agent_validateur_sip
                from mpe.Avis_Pub
                where id_agent_validateur_sip is not null
                UNION
                select distinct id_agent
                from mpe.Avis_Pub)
  and not exists (select distinct id_mpe
                  from concentrateur.agent allAgent
                  where allAgent.id_mpe = ap.id
                    and o.id = allAgent.id_organisme)
  and ap.service_id is null;
#Ajout des agents avis PUB avec service

INSERT INTO concentrateur.agent (id, acheteur_public, identifiant, nom, prenom, id_organisme, id_mpe,
                                 id_service, email,
                                 telephone, fax, roles)
SELECT distinct UUID(),
                o.libelle_organisme,
                agent.login,
                agent.nom,
                agent.prenom,
                o.id     as id_organisme,
                agent.id as id_mpe,
                sc.id,
                agent.email,
                agent.num_tel,
                agent.num_fax,
                ''
from mpe.Agent agent
         inner join concentrateur.organisme o
                    on agent.organisme = o.acronyme_organisme and o.plateforme_uuid = @plateforme_uuid_to_update
         inner join mpe.Service s on agent.service_id = s.id
         inner join concentrateur.referential_service_mpe sc on sc.ID_MPE = s.id and sc.id_organisme = o.id
where agent.id in (select distinct id_agent_validateur
                   from mpe.Avis_Pub
                   where id_agent_validateur is not null
                   UNION
                   select distinct id_agent_validateur_eco
                   from mpe.Avis_Pub
                   where id_agent_validateur_eco is not null
                   UNION
                   select distinct id_agent_validateur_sip
                   from mpe.Avis_Pub
                   where id_agent_validateur_sip is not null
                   UNION
                   select distinct id_agent
                   from mpe.Avis_Pub)
  and not exists (select distinct id_mpe
                  from concentrateur.agent allAgent
                  where allAgent.id_mpe = agent.id
                    and o.id = allAgent.id_organisme)
  and agent.service_id is not null;

