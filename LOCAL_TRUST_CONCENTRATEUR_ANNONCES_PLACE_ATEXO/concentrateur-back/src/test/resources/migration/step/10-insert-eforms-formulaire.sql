# Ajout des eforms instance


INSERT INTO concentrateur.e_forms_formulaire(UUID, NO_DOC_EXT, STATUT, DATE_CREATION, LANG, ID_CONSULTATION,
                                             NOTICE_VERSION,
                                             DATE_MODIFICATION, ID_NOTICE, valid, XML_GENERE,
                                             PLATEFORME, PUBLICATION_ID, PUBLICATION_DATE,
                                             REFERENCE_CONSULTATION, INTITULE_CONSULTATION, VERSION_SDK,
                                             ID_SUIVI_ANNONCE,
                                             DATE_ENVOI_SOUMISSION, DATE_SOUMISSION,
                                             ID_ORGANISME, ID_CREER_PAR, ID_MODIFIER_PAR, DATE_MODIFICATION_STATUT,
                                             ID_MPE_OLD, TED_VERSION)
SELECT form.code_retour,
       form.no_doc_ext,
       CASE
           WHEN annonce.STATUT = 'EN_COURS_DE_PUBLICATION' THEN 'PUBLISHING'
           WHEN annonce.STATUT = 'PUBLIER' THEN 'PUBLISHED'
           WHEN annonce.STATUT = 'EN_ATTENTE_VALIDATION_ECO' THEN 'CONCENTRATEUR_VALIDATING_ECO'
           WHEN annonce.STATUT = 'EN_ATTENTE_VALIDATION_SIP' THEN 'CONCENTRATEUR_VALIDATING_SIP'
           WHEN annonce.STATUT = 'REJETER_CONCENTRATEUR_VALIDATION_ECO' THEN 'CONCENTRATEUR_REJET_ECO'
           WHEN annonce.STATUT = 'REJETER_CONCENTRATEUR_VALIDATION_SIP' THEN 'CONCENTRATEUR_REJET_SIP'
           WHEN annonce.STATUT = 'EN_ATTENTE' THEN 'CONCENTRATEUR_VALIDATED'
           WHEN annonce.STATUT = 'ENVOI_PLANIFIER' THEN 'SUBMITTING'
           WHEN annonce.STATUT = 'REJETER_SUPPORT' THEN 'VALIDATION_FAILED'
           END,
       annonce.DATE_MODIFICATION,
       'FRA',
       annonce.ID_CONSULTATION,
       1,
       annonce.DATE_MODIFICATION,
       REPLACE(offre.CODE, '_ENOTICES', ''),
       true,
       annonce.XML,
       annonce.ID_PLATFORM,
       annonce.NUMERO_JOUE,
       annonce.DATE_PUBLICATION,
       suivi.REFERENCE,
       suivi.TITRE,
       'esentool',
       annonce.ID,
       suivi.DATE_ENVOI_EUROPEEN,
       suivi.DATE_ENVOI_EUROPEEN,
       annonce.ID_ORGANISME,
       suivi.ID_CREER_PAR,
       suivi.ID_MODIFIER_PAR,
       suivi.DATE_MODIFICATION_STATUT,
       CONCAT('FormXmlDestinataireOpoce-', form.id),
       'esentool'


from mpe.Destinataire_Pub dest
         inner join concentrateur.suivi_annonces annonce on annonce.ID_MPE_OLD = CONCAT('Destinataire_Pub-', dest.id)
         inner join concentrateur.support s on annonce.ID_SUPPORT = s.ID
         inner join concentrateur.offre offre on annonce.ID_OFFRE = offre.ID
         inner join mpe.FormXmlDestinataireOpoce form on dest.id = form.id_destinataire_opoce
         inner join mpe.Avis_Pub ap on ap.id = dest.id_avis
         inner join concentrateur.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
         inner join concentrateur.organisme o on o.acronyme_organisme = ap.organisme
where s.CODE = 'ENOTICES'
  and dest.id_support = 1
  and annonce.ID_AVIS_FICHIER is null
  and annonce.ID_CONSULTATION is not null
  and o.plateforme_uuid = @plateforme_uuid_to_update
  and not exists(select distinct ss.ID_MPE_OLD
                 from concentrateur.e_forms_formulaire ss
                          inner join concentrateur.organisme o on o.id = ss.ID_ORGANISME
                 where o.plateforme_uuid = @plateforme_uuid_to_update
                   and ss.ID_MPE_OLD = CONCAT('FormXmlDestinataireOpoce-', form.id));
