SET @plateforme_uuid_to_update = 'MPE_atexo_develop';
SET @plateforme_url_to_update = 'https://simap-mpe-int.local-trust.com';
SET @plateforme_lang_to_update = 'LUX';

SET @serveur_concentrateur = 'atexo-concpub-int-01';
SET @user_concentrateur = 'iat-atx';
SET @serveur_mpe = 'simap-mpe-int-01';
SET @user_mpe = 'iat-atx';
SET @nas_concentrateur = '/data/annonces/fichiers/';
SET @nas_mpe = '/data/apache2/simap-mpe-int.local-trust.com/data/';

# clean e_forms_formulaire
delete
from concentrateur.e_forms_formulaire_aud
where ID in (SELECT distinct ID
             from concentrateur.e_forms_formulaire
             where ID_MPE_OLD is not null);


delete
from concentrateur.e_forms_formulaire
where ID_MPE_OLD is not null
  and ID not in (select ID_PREVIOUS_FORMULAIRE from concentrateur.e_forms_formulaire);
# clean annonce

delete
from concentrateur.suivi_annonces
where ID_MPE_OLD is not null
  and ID not in (select ID_PARENT from concentrateur.suivi_annonces);

# clean suivi_type_avis_pub
delete
from concentrateur.suivi_type_avis_pub_aud
where ID in (SELECT distinct ID
             from concentrateur.suivi_type_avis_pub
             where ID_MPE_OLD is not null);


delete
from concentrateur.suivi_type_avis_pub
where ID_MPE_OLD is not null
  and ID not in (select ID_PARENT from concentrateur.suivi_type_avis_pub);

#Supression des agents

DELETE
FROM concentrateur.agent
where agent.id_mpe in (select distinct id_agent_validateur
                       from mpe.Avis_Pub
                       where id_agent_validateur is not null
                       UNION
                       select distinct id_agent_validateur_eco
                       from mpe.Avis_Pub
                       where id_agent_validateur_eco is not null
                       UNION
                       select distinct id_agent_validateur_sip
                       from mpe.Avis_Pub
                       where id_agent_validateur_sip is not null
                       UNION
                       select distinct id_agent
                       from mpe.Avis_Pub)
  and id_organisme in (select id
                       from concentrateur.organisme
                       where plateforme_uuid = @plateforme_uuid_to_update)
  and envoi_mpe is null;
#Supression des service
delete
from concentrateur.referential_service_mpe
where ID_MPE is not null
  and ID not in (select distinct r.id
                 from concentrateur.agent a
                          inner join concentrateur.referential_service_mpe r on r.id = a.id_service);

#suppression des pieces jointes
delete
from concentrateur.piece_jointe
where ID_MPE_OLD is not null;

delete
from e_forms_formulaire
where ID_PREVIOUS_FORMULAIRE not in (select ID from e_forms_formulaire);
delete
from suivi_annonces
where ID_PARENT not in (select ID from suivi_annonces);
delete
from suivi_type_avis_pub
where ID_PARENT not in (select ID from suivi_type_avis_pub);
