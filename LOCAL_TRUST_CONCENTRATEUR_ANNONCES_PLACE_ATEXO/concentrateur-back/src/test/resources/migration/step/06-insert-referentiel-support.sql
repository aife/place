#Ajout des supports JALs


INSERT INTO concentrateur.support(URL, LIBELLE, CODE, CODE_GROUPE, ACTIF, EXTERNAL, DESCRIPTION, NATIONAL,
                                  CODE_DESTINATION, EUROPEEN, CHOIX_UNIQUE, PAYS, MAIL, compte_obligatoire,
                                  SELECTIONNE_PAR_DEFAUT, IFRAME, POIDS)
select distinct jal.email,
                jal.nom,
                CONCAT(@plateforme_lang_to_update, '-', jal.email, '-', TO_BASE64(jal.nom)),
                'JAL',
                true,
                false,
                jal.information_facturation,
                true,
                'MAIL',
                false,
                true,
                @plateforme_lang_to_update,
                jal.email,
                false,
                false,
                false,
                0
from mpe.Destinataire_Annonce_Press desJal
         inner join mpe.JAL jal on jal.id = desJal.id_jal
where not exists (select CODE
                  from concentrateur.support
                  where CODE = CONCAT(@plateforme_lang_to_update, '-', jal.email, '-', TO_BASE64(jal.nom)));
