SET @plateforme_uuid_to_update = 'MPE_simap_preprod';
SET @plateforme_url_to_update = 'https://pmp-prep.tp.etat.lu';
SET @plateforme_lang_to_update = 'LUX';

SET @serveur_concentrateur = 'simap-concpub-prep-01';
SET @user_concentrateur = 'abo-atx';
SET @serveur_mpe = 'simap-mpe-prod-01';
SET @user_mpe = 'abo-atx';
SET @nas_concentrateur = '/data/annonces/fichiers/';
SET @nas_mpe = '/data/apache2/pmp.tp.etat.lu/data/';

# début AN-627

# ajout des pieces jointes OPOCE
INSERT simap_prep_concentrateur_annonces_db.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.chemin, b.id, '-0'),
                1,
                CONCAT('blobOrganisme_file-', b.id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_pdf_opoce
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is not null
  and b.old_id is null
  and not exists (select distinct ID_MPE_OLD
                  from simap_prep_concentrateur_annonces_db.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-', b.id));

INSERT simap_prep_concentrateur_annonces_db.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.organisme, '/files/', b.id, '-0'),
                1,
                CONCAT('blobOrganisme_file-', b.id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_pdf_opoce
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is null
  and b.old_id is null
  and not exists (select distinct ID_MPE_OLD
                  from simap_prep_concentrateur_annonces_db.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-', b.id));


# ajout des pieces jointes OPOCE OLD
INSERT simap_prep_concentrateur_annonces_db.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.chemin, b.old_id, '-0'),
                1,
                CONCAT('blobOrganisme_file-old-id-', b.old_id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_pdf_opoce
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is not null
  and b.old_id is not null
  and not exists (select distinct ID_MPE_OLD
                  from simap_prep_concentrateur_annonces_db.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id));

INSERT simap_prep_concentrateur_annonces_db.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.organisme, '/files/', b.old_id, '-0'),
                1,
                CONCAT('blobOrganisme_file-old-id-', b.old_id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_pdf_opoce
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is null
  and b.old_id is not null
  and not exists (select distinct ID_MPE_OLD
                  from simap_prep_concentrateur_annonces_db.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id));
# ajout des pieces jointes portail OLD
INSERT simap_prep_concentrateur_annonces_db.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.chemin, b.old_id, '-0'),
                1,
                CONCAT('blobOrganisme_file-old-id-', b.old_id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_portail
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is not null
  and b.old_id is not null
  and not exists (select distinct ID_MPE_OLD
                  from simap_prep_concentrateur_annonces_db.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id));

INSERT simap_prep_concentrateur_annonces_db.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.organisme, '/files/', b.old_id, '-0'),
                1,
                CONCAT('blobOrganisme_file-old-id-', b.old_id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_portail
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is null
  and b.old_id is not null
  and not exists (select distinct ID_MPE_OLD
                  from simap_prep_concentrateur_annonces_db.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id));

INSERT simap_prep_concentrateur_annonces_db.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.chemin, b.id, '-0'),
                1,
                CONCAT('blobOrganisme_file-', b.id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_portail
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is not null
  and b.old_id is null
  and not exists (select distinct ID_MPE_OLD
                  from simap_prep_concentrateur_annonces_db.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-', b.id));

INSERT simap_prep_concentrateur_annonces_db.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.organisme, '/files/', b.id, '-0'),
                1,
                CONCAT('blobOrganisme_file-', b.id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_portail
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is null
  and b.old_id is null
  and not exists (select distinct ID_MPE_OLD
                  from simap_prep_concentrateur_annonces_db.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-', b.id));

# ajout des PDF Portail
update simap_prep_concentrateur_annonces_db.suivi_type_avis_pub st
    inner join mpe.Avis_Pub ap on ap.id = st.ID_MPE_OLD
    inner join mpe.Avis a on a.id = ap.id_avis_portail
    inner join mpe.blobOrganisme_file b on b.id = a.avis
    inner join simap_prep_concentrateur_annonces_db.piece_jointe pj on pj.ID_MPE_OLD = CONCAT('blobOrganisme_file-', b.id)
SET st.ID_AVIS_PORTAIL= pj.id
where st.ID_AVIS_PORTAIL <> pj.id
  and b.old_id is null;


update simap_prep_concentrateur_annonces_db.suivi_type_avis_pub st
    inner join mpe.Avis_Pub ap on ap.id = st.ID_MPE_OLD
    inner join mpe.Avis a on a.id = ap.id_avis_portail
    inner join mpe.blobOrganisme_file b on b.id = a.avis
    inner join simap_prep_concentrateur_annonces_db.piece_jointe pj on pj.ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id)
SET st.ID_AVIS_PORTAIL= pj.id
where b.old_id is not null
  and st.ID_AVIS_PORTAIL <> pj.id;

#update suivi_annonces EU true piece jointe
update simap_prep_concentrateur_annonces_db.suivi_annonces sa inner join simap_prep_concentrateur_annonces_db.support s on s.id = sa.ID_SUPPORT
    inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub st on st.ID = sa.ID_SUIVI_TYPE_AVIS_PUB
    inner join mpe.Avis_Pub ap on ap.id = st.ID_MPE_OLD
    inner join mpe.Avis a on a.id = ap.id_avis_pdf_opoce
    inner join mpe.bloborganisme_file b on b.id = a.avis
    inner join simap_prep_concentrateur_annonces_db.piece_jointe pj on pj.ID_MPE_OLD = CONCAT('blobOrganisme_file-', b.id)
SET sa.ID_AVIS_FICHIER= pj.id
where s.EUROPEEN is true
  and sa.ID_AVIS_FICHIER <> pj.id
  and sa.LIEN_PUBLICATION is null
  and b.old_id is null;

#update suivi_annonces EU true piece jointe
update simap_prep_concentrateur_annonces_db.suivi_annonces sa inner join simap_prep_concentrateur_annonces_db.support s on s.id = sa.ID_SUPPORT
    inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub st on st.ID = sa.ID_SUIVI_TYPE_AVIS_PUB
    inner join mpe.Avis_Pub ap on ap.id = st.ID_MPE_OLD
    inner join mpe.Avis a on a.id = ap.id_avis_pdf_opoce
    inner join mpe.bloborganisme_file b on b.id = a.avis
    inner join simap_prep_concentrateur_annonces_db.piece_jointe pj on pj.ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id)
SET sa.ID_AVIS_FICHIER= pj.id
where s.EUROPEEN is true
  and b.old_id is not null
  and sa.ID_AVIS_FICHIER <> pj.id
  and sa.LIEN_PUBLICATION is null;


# fin AN-627
