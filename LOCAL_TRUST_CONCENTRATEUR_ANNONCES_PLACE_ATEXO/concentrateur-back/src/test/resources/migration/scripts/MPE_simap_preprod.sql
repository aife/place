SET @plateforme_uuid_to_update = 'MPE_simap_preprod';
SET @plateforme_url_to_update = 'https://pmp-prep.tp.etat.lu';
SET @plateforme_lang_to_update = 'LUX';

SET @serveur_concentrateur = 'simap-concpub-prep-01';
SET @user_concentrateur = 'tomcat';
SET @serveur_mpe = 'simap-mpe-prod-01';
SET @user_mpe = 'tomcat';
SET @nas_concentrateur = '/data/annonces/fichiers/';
SET @nas_mpe = '/data/apache2/pmp.tp.etat.lu/data/';

# clean e_forms_formulaire
delete
from simap_prep_concentrateur_annonces_db.e_forms_formulaire_aud
where ID in (SELECT distinct ID
             from simap_prep_concentrateur_annonces_db.e_forms_formulaire
             where ID_MPE_OLD is not null);


delete
from simap_prep_concentrateur_annonces_db.e_forms_formulaire
where ID_MPE_OLD is not null
  and ID not in (select ID_PREVIOUS_FORMULAIRE from simap_prep_concentrateur_annonces_db.e_forms_formulaire);
# clean annonce

delete
from simap_prep_concentrateur_annonces_db.suivi_annonces
where ID_MPE_OLD is not null
  and ID not in (select ID_PARENT from simap_prep_concentrateur_annonces_db.suivi_annonces);

# clean suivi_type_avis_pub
delete
from simap_prep_concentrateur_annonces_db.suivi_type_avis_pub_aud
where ID in (SELECT distinct ID
             from simap_prep_concentrateur_annonces_db.suivi_type_avis_pub
             where ID_MPE_OLD is not null);


delete
from simap_prep_concentrateur_annonces_db.suivi_type_avis_pub
where ID_MPE_OLD is not null
  and ID not in (select ID_PARENT from simap_prep_concentrateur_annonces_db.suivi_type_avis_pub);

#Supression des agents

DELETE
FROM simap_prep_concentrateur_annonces_db.agent
where agent.id_mpe in (select distinct id_agent_validateur
                       from mpe.Avis_Pub
                       where id_agent_validateur is not null
                       UNION
                       select distinct id_agent_validateur_eco
                       from mpe.Avis_Pub
                       where id_agent_validateur_eco is not null
                       UNION
                       select distinct id_agent_validateur_sip
                       from mpe.Avis_Pub
                       where id_agent_validateur_sip is not null
                       UNION
                       select distinct id_agent
                       from mpe.Avis_Pub)
  and id_organisme in (select id
                       from simap_prep_concentrateur_annonces_db.organisme
                       where plateforme_uuid = @plateforme_uuid_to_update)
  and envoi_mpe is null;
#Supression des service
delete
from simap_prep_concentrateur_annonces_db.referential_service_mpe
where ID_MPE is not null
  and ID not in (select distinct r.id
                 from simap_prep_concentrateur_annonces_db.agent a
                          inner join simap_prep_concentrateur_annonces_db.referential_service_mpe r on r.id = a.id_service);

#suppression des pieces jointes
delete
from simap_prep_concentrateur_annonces_db.piece_jointe
where ID_MPE_OLD is not null;

delete
from e_forms_formulaire
where ID_PREVIOUS_FORMULAIRE not in (select ID from e_forms_formulaire);
delete
from suivi_annonces
where ID_PARENT not in (select ID from suivi_annonces);
delete
from suivi_type_avis_pub
where ID_PARENT not in (select ID from suivi_type_avis_pub);
#Création d'organisme

INSERT INTO simap_prep_concentrateur_annonces_db.organisme (acronyme_organisme, libelle_organisme, plateforme_uuid, plateforme_url,
                                     sigle_organisme)
SELECT distinct ap.acronyme,
                ap.denomination_org,
                @plateforme_uuid_to_update,
                @plateforme_url_to_update,
                ap.sigle
from mpe.Organisme ap
where ap.acronyme not in
      (select distinct acronyme_organisme
       from simap_prep_concentrateur_annonces_db.organisme
       where plateforme_uuid = @plateforme_uuid_to_update);
# update existing service
update simap_prep_concentrateur_annonces_db.referential_service_mpe toUpdate inner join simap_prep_concentrateur_annonces_db.organisme o
    on o.id = toUpdate.id_organisme
set toUpdate.ID_MPE= (select ms.id as id_service_mpe
                      from simap_prep_concentrateur_annonces_db.referential_service_mpe cs
                               inner join mpe.Service ms
                               inner join simap_prep_concentrateur_annonces_db.organisme o
                                          on o.acronyme_organisme = ms.organisme and
                                             o.plateforme_uuid = @plateforme_uuid_to_update
                                              and ms.libelle = cs.libelle and ms.sigle = cs.code
                      where toUpdate.id = cs.id)
where ID_MPE is null
  and o.plateforme_uuid = @plateforme_uuid_to_update;

update simap_prep_concentrateur_annonces_db.referential_service_mpe toUpdate inner join simap_prep_concentrateur_annonces_db.organisme o
    on o.id = toUpdate.id_organisme
set toUpdate.code= (select ms.sigle
                    from mpe.Service ms
                    where toUpdate.ID_MPE = ms.id)
  , toUpdate.libelle= (select ms.libelle
                       from mpe.Service ms
                       where toUpdate.ID_MPE = ms.id)
where ID_MPE is not null
  and o.plateforme_uuid = @plateforme_uuid_to_update;

#Création de service
INSERT INTO simap_prep_concentrateur_annonces_db.referential_service_mpe (code, libelle, id_organisme, ID_MPE)
SELECT distinct ap.sigle,
                ap.libelle,
                o2.id,
                ap.id
from mpe.Service ap
         inner join mpe.Organisme o on ap.organisme = o.acronyme
         inner join simap_prep_concentrateur_annonces_db.organisme o2 on o2.acronyme_organisme = o.acronyme
where o2.plateforme_uuid = @plateforme_uuid_to_update
  and not exists (select distinct s.code, s.libelle, s.ID_MPE
                  from simap_prep_concentrateur_annonces_db.referential_service_mpe s
                  where s.id_organisme = o2.id
                    and ap.id = s.ID_MPE
                    and ap.sigle = s.code
                    and ap.libelle = s.libelle);

INSERT INTO simap_prep_concentrateur_annonces_db.referential_service_mpe (code, libelle, id_organisme, ID_MPE)
SELECT distinct service.sigle,
                service.libelle,
                organismeConsultation.id,
                service.id
from mpe.Avis_Pub avisPub
         left join mpe.Consultation consultation on consultation.id = avisPub.consultation_id
         left join mpe.Service service on service.id = consultation.service_id
         left join simap_prep_concentrateur_annonces_db.organisme organismeConsultation
                   on organismeConsultation.acronyme_organisme = avisPub.organisme
         left join simap_prep_concentrateur_annonces_db.referential_service_mpe refService
                   on refService.ID_MPE = service.id
where consultation.service_id is not null
  and not exists (select distinct s.code, s.libelle, s.ID_MPE
                  from simap_prep_concentrateur_annonces_db.referential_service_mpe s
                  where s.id_organisme = organismeConsultation.id
                    and service.id = s.ID_MPE
                    and service.sigle = s.code
                    and service.libelle = s.libelle);
#facturation PAS SIP

INSERT INTO simap_prep_concentrateur_annonces_db.consultation_facturation (ADRESSE, MAIL, SIP)
SELECT distinct j.information_facturation, j.email_ar, false
from mpe.AdresseFacturationJal j
         inner join mpe.Annonce_Press p on j.id = p.id_adresse_facturation
where (j.information_facturation, j.email_ar, false) not in (select ADRESSE, MAIL, SIP
                                                             from simap_prep_concentrateur_annonces_db.consultation_facturation)
  and j.facturation_sip = '0'
UNION
SELECT distinct j.information_facturation, j.email_ar, true
from mpe.AdresseFacturationJal j
         inner join mpe.Annonce_Press p on j.id = p.id_adresse_facturation
where (j.information_facturation, j.email_ar, true) not in (select ADRESSE, MAIL, SIP
                                                            from simap_prep_concentrateur_annonces_db.consultation_facturation)
  and j.facturation_sip = '1';
#Ajout des agents avis PUB sans service
INSERT INTO simap_prep_concentrateur_annonces_db.agent (id, acheteur_public, identifiant, nom, prenom, id_organisme,
                                 id_service, email, id_mpe,
                                 telephone, fax, roles)
SELECT distinct UUID(),
                o.libelle_organisme,
                ap.login,
                ap.nom,
                ap.prenom,
                o.id,
                null,
                ap.email,
                ap.id,
                ap.num_tel,
                ap.num_fax,
                ''
from mpe.Agent ap
         inner join simap_prep_concentrateur_annonces_db.organisme o
                    on ap.organisme = o.acronyme_organisme and o.plateforme_uuid = @plateforme_uuid_to_update

where ap.id in (select distinct id_agent_validateur
                from mpe.Avis_Pub
                where id_agent_validateur is not null
                UNION
                select distinct id_agent_validateur_eco
                from mpe.Avis_Pub
                where id_agent_validateur_eco is not null
                UNION
                select distinct id_agent_validateur_sip
                from mpe.Avis_Pub
                where id_agent_validateur_sip is not null
                UNION
                select distinct id_agent
                from mpe.Avis_Pub)
  and not exists (select distinct id_mpe
                  from simap_prep_concentrateur_annonces_db.agent allAgent
                  where allAgent.id_mpe = ap.id
                    and o.id = allAgent.id_organisme)
  and ap.service_id is null;
#Ajout des agents avis PUB avec service

INSERT INTO simap_prep_concentrateur_annonces_db.agent (id, acheteur_public, identifiant, nom, prenom, id_organisme, id_mpe,
                                 id_service, email,
                                 telephone, fax, roles)
SELECT distinct UUID(),
                o.libelle_organisme,
                agent.login,
                agent.nom,
                agent.prenom,
                o.id     as id_organisme,
                agent.id as id_mpe,
                sc.id,
                agent.email,
                agent.num_tel,
                agent.num_fax,
                ''
from mpe.Agent agent
         inner join simap_prep_concentrateur_annonces_db.organisme o
                    on agent.organisme = o.acronyme_organisme and o.plateforme_uuid = @plateforme_uuid_to_update
         inner join mpe.Service s on agent.service_id = s.id
         inner join simap_prep_concentrateur_annonces_db.referential_service_mpe sc on sc.ID_MPE = s.id and sc.id_organisme = o.id
where agent.id in (select distinct id_agent_validateur
                   from mpe.Avis_Pub
                   where id_agent_validateur is not null
                   UNION
                   select distinct id_agent_validateur_eco
                   from mpe.Avis_Pub
                   where id_agent_validateur_eco is not null
                   UNION
                   select distinct id_agent_validateur_sip
                   from mpe.Avis_Pub
                   where id_agent_validateur_sip is not null
                   UNION
                   select distinct id_agent
                   from mpe.Avis_Pub)
  and not exists (select distinct id_mpe
                  from simap_prep_concentrateur_annonces_db.agent allAgent
                  where allAgent.id_mpe = agent.id
                    and o.id = allAgent.id_organisme)
  and agent.service_id is not null;

#  ajout des pieces jointes PRESS
INSERT simap_prep_concentrateur_annonces_db.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                app.nom_fichier,
                CONCAT(b.chemin, b.id, '-0'),
                app.taille,
                CONCAT('Annonce_Press_PieceJointe-', app.id),
                b.hash
from mpe.Annonce_Press_PieceJointe app
         left join mpe.Annonce_Press ap on ap.id = app.id_annonce_press
         inner join mpe.Destinataire_Pub dp
                    on dp.id = ap.id_Dest_Press
         inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
         inner join mpe.blobOrganisme_file b on b.id = app.piece
where b.chemin is not null
  and b.old_id is null
  and not exists (select distinct ID_MPE_OLD
                  from simap_prep_concentrateur_annonces_db.piece_jointe
                  where ID_MPE_OLD = CONCAT('Annonce_Press_PieceJointe-', app.id));

INSERT simap_prep_concentrateur_annonces_db.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                app.nom_fichier,
                CONCAT(b.organisme, '/files/', b.id, '-0'),
                app.taille,
                CONCAT('Annonce_Press_PieceJointe-', app.id),
                b.hash
from mpe.Annonce_Press_PieceJointe app
         left join mpe.Annonce_Press ap on ap.id = app.id_annonce_press
         inner join mpe.Destinataire_Pub dp
                    on dp.id = ap.id_Dest_Press
         inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
         inner join mpe.blobOrganisme_file b on b.id = app.piece
where b.chemin is null
  and b.old_id is null
  and not exists (select distinct ID_MPE_OLD
                  from simap_prep_concentrateur_annonces_db.piece_jointe
                  where ID_MPE_OLD = CONCAT('Annonce_Press_PieceJointe-', app.id));

# ajout des pieces jointes avec old id

INSERT simap_prep_concentrateur_annonces_db.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                app.nom_fichier,
                CONCAT(b.chemin, b.old_id, '-0'),
                app.taille,
                CONCAT('blobOrganisme_file-old-id-', b.old_id),
                b.hash
from mpe.Annonce_Press_PieceJointe app
         left join mpe.Annonce_Press ap on ap.id = app.id_annonce_press
         inner join mpe.Destinataire_Pub dp
                    on dp.id = ap.id_Dest_Press
         inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
         inner join mpe.blobOrganisme_file b on b.id = app.piece
where b.chemin is not null
  and b.old_id is not null
  and not exists (select distinct ID_MPE_OLD
                  from simap_prep_concentrateur_annonces_db.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id));

INSERT simap_prep_concentrateur_annonces_db.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                app.nom_fichier,
                CONCAT(b.organisme, '/files/', b.old_id, '-0'),
                app.taille,
                CONCAT('blobOrganisme_file-old-id-', b.old_id),
                b.hash
from mpe.Annonce_Press_PieceJointe app
         left join mpe.Annonce_Press ap on ap.id = app.id_annonce_press
         inner join mpe.Destinataire_Pub dp
                    on dp.id = ap.id_Dest_Press
         inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
         inner join mpe.blobOrganisme_file b on b.id = app.piece
where b.chemin is null
  and b.old_id is not null
  and not exists (select distinct ID_MPE_OLD
                  from simap_prep_concentrateur_annonces_db.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id));
# début AN-627

# ajout des pieces jointes OPOCE
INSERT simap_prep_concentrateur_annonces_db.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.chemin, b.id, '-0'),
                1,
                CONCAT('blobOrganisme_file-', b.id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_pdf_opoce
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is not null
  and b.old_id is null
  and not exists (select distinct ID_MPE_OLD
                  from simap_prep_concentrateur_annonces_db.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-', b.id));

INSERT simap_prep_concentrateur_annonces_db.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.organisme, '/files/', b.id, '-0'),
                1,
                CONCAT('blobOrganisme_file-', b.id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_pdf_opoce
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is null
  and b.old_id is null
  and not exists (select distinct ID_MPE_OLD
                  from simap_prep_concentrateur_annonces_db.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-', b.id));


# ajout des pieces jointes OPOCE OLD
INSERT simap_prep_concentrateur_annonces_db.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.chemin, b.old_id, '-0'),
                1,
                CONCAT('blobOrganisme_file-old-id-', b.old_id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_pdf_opoce
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is not null
  and b.old_id is not null
  and not exists (select distinct ID_MPE_OLD
                  from simap_prep_concentrateur_annonces_db.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id));

INSERT simap_prep_concentrateur_annonces_db.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.organisme, '/files/', b.old_id, '-0'),
                1,
                CONCAT('blobOrganisme_file-old-id-', b.old_id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_pdf_opoce
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is null
  and b.old_id is not null
  and not exists (select distinct ID_MPE_OLD
                  from simap_prep_concentrateur_annonces_db.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id));
# ajout des pieces jointes portail OLD
INSERT simap_prep_concentrateur_annonces_db.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.chemin, b.old_id, '-0'),
                1,
                CONCAT('blobOrganisme_file-old-id-', b.old_id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_portail
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is not null
  and b.old_id is not null
  and not exists (select distinct ID_MPE_OLD
                  from simap_prep_concentrateur_annonces_db.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id));

INSERT simap_prep_concentrateur_annonces_db.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.organisme, '/files/', b.old_id, '-0'),
                1,
                CONCAT('blobOrganisme_file-old-id-', b.old_id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_portail
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is null
  and b.old_id is not null
  and not exists (select distinct ID_MPE_OLD
                  from simap_prep_concentrateur_annonces_db.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id));

INSERT simap_prep_concentrateur_annonces_db.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.chemin, b.id, '-0'),
                1,
                CONCAT('blobOrganisme_file-', b.id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_portail
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is not null
  and b.old_id is null
  and not exists (select distinct ID_MPE_OLD
                  from simap_prep_concentrateur_annonces_db.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-', b.id));

INSERT simap_prep_concentrateur_annonces_db.piece_jointe(content_type, date_creation, nom, chemin, taille, ID_MPE_OLD, md5)
SELECT distinct 'application/octet-stream',
                now(),
                b.name,
                CONCAT(b.organisme, '/files/', b.id, '-0'),
                1,
                CONCAT('blobOrganisme_file-', b.id),
                b.hash
from mpe.Avis_Pub apu
         inner join mpe.Avis a on a.id = apu.id_avis_portail
         inner join mpe.blobOrganisme_file b on b.id = a.avis
where b.chemin is null
  and b.old_id is null
  and not exists (select distinct ID_MPE_OLD
                  from simap_prep_concentrateur_annonces_db.piece_jointe
                  where ID_MPE_OLD = CONCAT('blobOrganisme_file-', b.id));

#fin AN-627
#Ajout des supports JALs


INSERT INTO simap_prep_concentrateur_annonces_db.support(URL, LIBELLE, CODE, CODE_GROUPE, ACTIF, EXTERNAL, DESCRIPTION, NATIONAL,
                                  CODE_DESTINATION, EUROPEEN, CHOIX_UNIQUE, PAYS, MAIL, compte_obligatoire,
                                  SELECTIONNE_PAR_DEFAUT, IFRAME, POIDS)
select distinct jal.email,
                jal.nom,
                CONCAT(@plateforme_lang_to_update, '-', jal.email, '-', TO_BASE64(jal.nom)),
                'JAL',
                true,
                false,
                jal.information_facturation,
                true,
                'MAIL',
                false,
                true,
                @plateforme_lang_to_update,
                jal.email,
                false,
                false,
                false,
                0
from mpe.Destinataire_Annonce_Press desJal
         inner join mpe.JAL jal on jal.id = desJal.id_jal
where not exists (select CODE
                  from simap_prep_concentrateur_annonces_db.support
                  where CODE = CONCAT(@plateforme_lang_to_update, '-', jal.email, '-', TO_BASE64(jal.nom)));
# Ajout Type Avis PUB
INSERT INTO simap_prep_concentrateur_annonces_db.suivi_type_avis_pub(ID_MPE_OLD,
                                              ID_ORGANISME,
                                              ID_PROCEDURE,
                                              ID_SERVICE,
                                              ID_CONSULTATION,
                                              REFERENCE,
                                              TITRE,
                                              OBJET,
                                              DATE_MISE_EN_LIGNE,
                                              DATE_MISE_EN_LIGNE_CALCULE, DLRO, DATE_VALIDATION,
                                              DATE_CREATION, DATE_MODIFICATION)
SELECT avisPub.id,
       organismeConsultation.id,
       procedureConsult.id_type_procedure,
       refService.id,
       avisPub.consultation_id,
       consultation.reference_utilisateur,
       consultation.intitule,
       consultation.objet,
       CASE
           WHEN consultation.date_mise_en_ligne_souhaitee = '2018-02-29' THEN '2018-02-28'
           WHEN consultation.date_mise_en_ligne_souhaitee not in ('--null ::00')
               THEN consultation.date_mise_en_ligne_souhaitee
           ELSE consultation.date_mise_en_ligne_calcule END,
       consultation.date_mise_en_ligne_calcule,
       consultation.date_fin_affichage,
       avisPub.date_validation,
       avisPub.date_creation,
       avisPub.date_creation

from mpe.Avis_Pub avisPub
         left join simap_prep_concentrateur_annonces_db.organisme organismeConsultation
                   on organismeConsultation.acronyme_organisme = avisPub.organisme
         left join mpe.Consultation consultation on consultation.id = avisPub.consultation_id
         left join mpe.Service service on service.id = consultation.service_id
         left join simap_prep_concentrateur_annonces_db.referential_service_mpe refService
                   on refService.ID_MPE = service.id and refService.id_organisme = organismeConsultation.id
         left join simap_prep_concentrateur_annonces_db.type_procedure procedureConsult
                   on procedureConsult.id_type_procedure = consultation.id_type_procedure
         left join mpe.Type_Avis_Pub tap on tap.id = avisPub.type_avis

WHERE organismeConsultation.plateforme_uuid = @plateforme_uuid_to_update
  and avisPub.statut <> '0'
  and not exists(select distinct ss.ID_MPE_OLD
                 from simap_prep_concentrateur_annonces_db.suivi_type_avis_pub ss
                          inner join simap_prep_concentrateur_annonces_db.organisme o on o.id = ss.ID_ORGANISME
                 where o.plateforme_uuid = @plateforme_uuid_to_update
                   and ss.ID_MPE_OLD = avisPub.id);

#UPDATE AGENT CREATEUR

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub toUpdate
SET toUpdate.ID_CREER_PAR = (SELECT a.id
                             from simap_prep_concentrateur_annonces_db.agent a
                                      inner join mpe.avis_pub ap on a.id_mpe = ap.id_agent
                                      inner join mpe.Agent agent on ap.id_agent = agent.id
                                      inner join simap_prep_concentrateur_annonces_db.organisme o
                                                 on agent.organisme = o.acronyme_organisme and a.id_organisme = o.id
                             where ap.id = toUpdate.ID_MPE_OLD
                               and o.plateforme_uuid = @plateforme_uuid_to_update)
where toUpdate.ID_MPE_OLD is not null;

#UPDATE AGENT MODIFIE

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub toUpdate
SET toUpdate.ID_MODIFIER_PAR = (SELECT a.id
                                from simap_prep_concentrateur_annonces_db.agent a
                                         inner join mpe.avis_pub ap on a.id_mpe = ap.id_agent
                                         inner join mpe.Agent agent on ap.id_agent = agent.id
                                         inner join simap_prep_concentrateur_annonces_db.organisme o
                                                    on agent.organisme = o.acronyme_organisme and a.id_organisme = o.id
                                where ap.id = toUpdate.ID_MPE_OLD
                                  and o.plateforme_uuid = @plateforme_uuid_to_update)
where toUpdate.ID_MPE_OLD is not null;

#UPDATE AGENT VALIDATEUR

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub toUpdate
SET toUpdate.ID_VALIDATEUR = (SELECT a.id
                              from simap_prep_concentrateur_annonces_db.agent a
                                       inner join mpe.avis_pub ap on a.id_mpe = ap.id_agent_validateur
                                       inner join mpe.Agent agent on ap.id_agent = agent.id
                                       inner join simap_prep_concentrateur_annonces_db.organisme o
                                                  on agent.organisme = o.acronyme_organisme and a.id_organisme = o.id
                              where ap.id = toUpdate.ID_MPE_OLD
                                and o.plateforme_uuid = @plateforme_uuid_to_update)
where toUpdate.ID_MPE_OLD is not null;

#UPDATE AGENT VALIDATEUR ECO

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub toUpdate
SET toUpdate.ID_VALIDATEUR_ECO = (SELECT a.id
                                  from simap_prep_concentrateur_annonces_db.agent a
                                           inner join mpe.avis_pub ap on a.id_mpe = ap.id_agent_validateur_eco
                                           inner join mpe.Agent agent on ap.id_agent = agent.id
                                           inner join simap_prep_concentrateur_annonces_db.organisme o
                                                      on agent.organisme = o.acronyme_organisme and a.id_organisme = o.id
                                  where ap.id = toUpdate.ID_MPE_OLD
                                    and o.plateforme_uuid = @plateforme_uuid_to_update)
where toUpdate.ID_MPE_OLD is not null;
#UPDATE AGENT VALIDATEUR SIP

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub toUpdate
SET toUpdate.ID_VALIDATEUR_SIP = (SELECT a.id
                                  from simap_prep_concentrateur_annonces_db.agent a
                                           inner join mpe.avis_pub ap on a.id_mpe = ap.id_agent_validateur_sip
                                           inner join mpe.Agent agent on ap.id_agent = agent.id
                                           inner join simap_prep_concentrateur_annonces_db.organisme o
                                                      on agent.organisme = o.acronyme_organisme and a.id_organisme = o.id
                                  where ap.id = toUpdate.ID_MPE_OLD
                                    and o.plateforme_uuid = @plateforme_uuid_to_update)
where toUpdate.ID_MPE_OLD is not null;


#Mise � jour EU

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub
SET EU = true
where EU in not true and ID in (SELECT distinct suivi.ID
             from mpe.FormXmlDestinataireOpoce opoce
                      inner join mpe.Destinataire_Pub dp on dp.id = opoce.id_destinataire_opoce
                      inner join mpe.Avis_Pub ap on ap.id = dp.id_avis
                      inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                      inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
             WHERE o.plateforme_uuid = @plateforme_uuid_to_update
               and ap.id = suivi.ID_MPE_OLD
               and suivi.ID_ORGANISME = o.id
             UNION
             SELECT distinct suivi.ID
             from mpe.Avis_Pub ap
                      inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                      inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
             WHERE o.plateforme_uuid = @plateforme_uuid_to_update
               and ap.id = suivi.ID_MPE_OLD
               and ap.id_avis_pdf_opoce is not null
               and suivi.ID_ORGANISME = o.id);

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub
SET EU = false
where EU is null;


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            simap_prep_concentrateur_annonces_db.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'LU01%'
         and offre.CODE = 'LUANMARCHE') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            simap_prep_concentrateur_annonces_db.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'LU03%'
         and offre.CODE = 'LUANRECTIFICATIF') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;



UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            simap_prep_concentrateur_annonces_db.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU02a%'
         and offre.CODE = '16_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            simap_prep_concentrateur_annonces_db.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU05b%'
         and offre.CODE = '16_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            simap_prep_concentrateur_annonces_db.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'LU02%'
         and offre.CODE = 'LUANCANDIDATURE') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            simap_prep_concentrateur_annonces_db.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU01%'
         and offre.CODE = '4_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            simap_prep_concentrateur_annonces_db.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU03%'
         and offre.CODE = '29_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            simap_prep_concentrateur_annonces_db.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU14%'
         and offre.CODE = '16_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            simap_prep_concentrateur_annonces_db.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU24%'
         and offre.CODE = '19_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            simap_prep_concentrateur_annonces_db.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU07%'
         and offre.CODE = '16_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            simap_prep_concentrateur_annonces_db.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU12%'
         and offre.CODE = '23_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            simap_prep_concentrateur_annonces_db.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU13%'
         and offre.CODE = '36_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            simap_prep_concentrateur_annonces_db.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU15%'
         and offre.CODE = '25_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            simap_prep_concentrateur_annonces_db.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU02b%'
         and offre.CODE = '16_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            simap_prep_concentrateur_annonces_db.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU05a%'
         and offre.CODE = '16_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            simap_prep_concentrateur_annonces_db.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU04%'
         and offre.CODE = '5_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            simap_prep_concentrateur_annonces_db.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'EU06%'
         and offre.CODE = '29_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            simap_prep_concentrateur_annonces_db.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'LU09%'
         and offre.CODE = '19_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis,
            simap_prep_concentrateur_annonces_db.offre offre
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id
         and tap.libelle like 'LU05%'
         and offre.CODE = 'LUANDEMANDEOFFRE') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID = A.suiviId
  and suivi.ID_OFFRE_RACINE <> A.offreId;


#type d'avis n'existe pas
UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId
       from simap_prep_concentrateur_annonces_db.offre offre
       WHERE offre.CODE = '16_ENOTICES') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID_OFFRE_RACINE is null
  and suivi.EU is true;
UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct offre.id as offreId
       from simap_prep_concentrateur_annonces_db.offre offre
       WHERE offre.CODE = 'LUANMARCHE') as A
SET suivi.ID_OFFRE_RACINE = A.offreId
where suivi.ID_OFFRE_RACINE is null
  and suivi.EU is false
  and suivi.ID_OFFRE_RACINE <> A.offreId;


#Date d envoi eu / date traitement
UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct ap.date_envoi as DATE_TRAITEMENT, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.DATE_TRAITEMENT = A.DATE_TRAITEMENT
where suivi.ID = A.suiviId;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct ap.date_envoi as date_envoi, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.DATE_ENVOI_EUROPEEN = A.date_envoi
where suivi.ID = A.suiviId;

# update contenu mail
UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub toUpdate
    , (SELECT ap.objet as objet, ap.texte as contenu, suivi.ID as suiviId
       from mpe.Annonce_Press ap
                inner join mpe.Destinataire_Pub dp on dp.id = ap.id_Dest_Press
                inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = apu.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = apu.organisme
       where o.plateforme_uuid = @plateforme_uuid_to_update) as A
SET toUpdate.MAIL_CONTENU = A.contenu,
    toUpdate.MAIL_OBJET   = A.objet
where toUpdate.ID = A.suiviId;

#Update facturation
UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub toUpdate
    , (SELECT cf.ID as facturationId, suivi.ID as suiviId
       from mpe.Annonce_Press ap
                inner join mpe.Destinataire_Pub dp on dp.id = ap.id_Dest_Press
                inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = apu.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = apu.organisme
                inner join mpe.AdresseFacturationJal facturation on facturation.id = ap.id_adresse_facturation
                inner join simap_prep_concentrateur_annonces_db.consultation_facturation cf
       where cf.MAIL = facturation.email_ar
         and cf.ADRESSE = facturation.information_facturation
         and cf.SIP is false
         and facturation.facturation_sip = '0'
         and o.plateforme_uuid = @plateforme_uuid_to_update
         and not exists (select ID_FACTURATION, ID
                         from simap_prep_concentrateur_annonces_db.suivi_type_avis_pub
                         where (ID_FACTURATION, ID) = (cf.ID, suivi.ID))) as A
SET toUpdate.ID_FACTURATION = A.facturationId
where toUpdate.ID = A.suiviId
  and toUpdate.ID_FACTURATION <> A.facturationId;


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub toUpdate
    , (SELECT cf.ID as facturationId, suivi.ID as suiviId
       from mpe.Annonce_Press ap
                inner join mpe.Destinataire_Pub dp on dp.id = ap.id_Dest_Press
                inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = apu.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = apu.organisme
                inner join mpe.AdresseFacturationJal facturation on facturation.id = ap.id_adresse_facturation
                inner join simap_prep_concentrateur_annonces_db.consultation_facturation cf
       where cf.MAIL = facturation.email_ar
         and cf.ADRESSE = facturation.information_facturation
         and cf.SIP is true
         and facturation.facturation_sip = '1'
         and o.plateforme_uuid = @plateforme_uuid_to_update
         and not exists (select ID_FACTURATION, ID
                         from simap_prep_concentrateur_annonces_db.suivi_type_avis_pub
                         where (ID_FACTURATION, ID) = (cf.ID, suivi.ID))) as A
SET toUpdate.ID_FACTURATION = A.facturationId
where toUpdate.ID = A.suiviId
  and toUpdate.ID_FACTURATION <> A.facturationId;


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub toUpdate
    , (SELECT min(cf.ID) as facturationId, suivi.ID as suiviId
       from mpe.Annonce_Press ap
                inner join mpe.Destinataire_Pub dp on dp.id = ap.id_Dest_Press
                inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = apu.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = apu.organisme,
            mpe.AdresseFacturationJal facturation
                inner join simap_prep_concentrateur_annonces_db.consultation_facturation cf
       where cf.MAIL = facturation.email_ar
         and cf.ADRESSE = facturation.information_facturation
         and cf.SIP is false
         and ap.id_adresse_facturation is null
         and facturation.facturation_sip = '0'
         and o.plateforme_uuid = @plateforme_uuid_to_update
       group by suiviId) as A
SET toUpdate.ID_FACTURATION = A.facturationId
where toUpdate.ID = A.suiviId
  and toUpdate.ID_FACTURATION <> A.facturationId;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub toUpdate
    , (SELECT min(cf.ID) as facturationId, suivi.ID as suiviId
       from mpe.Annonce_Press ap
                inner join mpe.Destinataire_Pub dp on dp.id = ap.id_Dest_Press
                inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = apu.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = apu.organisme,
            mpe.AdresseFacturationJal facturation
                inner join simap_prep_concentrateur_annonces_db.consultation_facturation cf
       where cf.MAIL = facturation.email_ar
         and cf.ADRESSE = facturation.information_facturation
         and cf.SIP is true
         and ap.id_adresse_facturation is null
         and facturation.facturation_sip = '1'
         and o.plateforme_uuid = @plateforme_uuid_to_update
       group by suiviId) as A
SET toUpdate.ID_FACTURATION = A.facturationId
where toUpdate.ID = A.suiviId
  and toUpdate.ID_FACTURATION <> A.facturationId;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub toUpdate
    , (select ap.ID as id
       from simap_prep_concentrateur_annonces_db.suivi_type_avis_pub ap
                inner join mpe.avis_pub ap2 on ap.ID_MPE_OLD = ap2.id
       where ap.ID_FACTURATION is null
         and ap2.Sip = '1') as A
SET toUpdate.ID_FACTURATION = (SELECT min(cf.ID)
                               from mpe.AdresseFacturationJal facturation
                                        inner join simap_prep_concentrateur_annonces_db.consultation_facturation cf
                               where cf.MAIL = facturation.email_ar
                                 and cf.ADRESSE = facturation.information_facturation
                                 and cf.SIP is true
                                 and facturation.facturation_sip = '1')
where toUpdate.ID = A.id
  and toUpdate.EU is false;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub toUpdate
    , (select ap.ID as id
       from simap_prep_concentrateur_annonces_db.suivi_type_avis_pub ap
                inner join mpe.avis_pub ap2 on ap.ID_MPE_OLD = ap2.id
       where ap.ID_FACTURATION is null
         and ap2.Sip = '0') as A
SET toUpdate.ID_FACTURATION = (SELECT min(cf.ID)
                               from mpe.AdresseFacturationJal facturation
                                        inner join simap_prep_concentrateur_annonces_db.consultation_facturation cf
                               where cf.MAIL = facturation.email_ar
                                 and cf.ADRESSE = facturation.information_facturation
                                 and cf.SIP is false
                                 and facturation.facturation_sip = '0')
where toUpdate.ID = A.id
  and toUpdate.EU is false;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub toUpdate inner join simap_prep_concentrateur_annonces_db.offre o
    on toUpdate.ID_OFFRE_RACINE = o.ID
    , (select ap.ID as id
       from simap_prep_concentrateur_annonces_db.suivi_type_avis_pub ap
                inner join mpe.avis_pub ap2 on ap.ID_MPE_OLD = ap2.id
       where ap.ID_FACTURATION is null
         and ap2.Sip = '1') as A
SET toUpdate.ID_FACTURATION = (SELECT min(cf.ID)
                               from mpe.AdresseFacturationJal facturation
                                        inner join simap_prep_concentrateur_annonces_db.consultation_facturation cf
                               where cf.MAIL = facturation.email_ar
                                 and cf.ADRESSE = facturation.information_facturation
                                 and cf.SIP is true
                                 and facturation.facturation_sip = '1')
where toUpdate.ID = A.id
  and toUpdate.EU is true
  and o.ID_OFFRE_ASSOCIEE is not null;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub toUpdate inner join simap_prep_concentrateur_annonces_db.offre o
    on toUpdate.ID_OFFRE_RACINE = o.ID
    , (select ap.ID as id
       from simap_prep_concentrateur_annonces_db.suivi_type_avis_pub ap
                inner join mpe.avis_pub ap2 on ap.ID_MPE_OLD = ap2.id
       where ap.ID_FACTURATION is null
         and ap2.Sip = '0') as A
SET toUpdate.ID_FACTURATION = (SELECT min(cf.ID)
                               from mpe.AdresseFacturationJal facturation
                                        inner join simap_prep_concentrateur_annonces_db.consultation_facturation cf
                               where cf.MAIL = facturation.email_ar
                                 and cf.ADRESSE = facturation.information_facturation
                                 and cf.SIP is false
                                 and facturation.facturation_sip = '0')
where toUpdate.ID = A.id
  and toUpdate.EU is true
  and o.ID_OFFRE_ASSOCIEE is not null;

# update when SIP is null

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub toUpdate
    , (select ap.ID as id
       from simap_prep_concentrateur_annonces_db.suivi_type_avis_pub ap
                inner join mpe.avis_pub ap2 on ap.ID_MPE_OLD = ap2.id
       where ap.ID_FACTURATION is null
         and ap2.Sip is null) as A
SET toUpdate.ID_FACTURATION = (SELECT min(cf.ID)
                               from mpe.AdresseFacturationJal facturation
                                        inner join simap_prep_concentrateur_annonces_db.consultation_facturation cf
                               where cf.MAIL = facturation.email_ar
                                 and cf.ADRESSE = facturation.information_facturation
                                 and cf.SIP is false
                                 and facturation.facturation_sip = '0')
where toUpdate.ID = A.id
  and toUpdate.EU is false;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub toUpdate inner join simap_prep_concentrateur_annonces_db.offre o
    on toUpdate.ID_OFFRE_RACINE = o.ID
    , (select ap.ID as id
       from simap_prep_concentrateur_annonces_db.suivi_type_avis_pub ap
                inner join mpe.avis_pub ap2 on ap.ID_MPE_OLD = ap2.id
       where ap.ID_FACTURATION is null
         and ap2.Sip is null) as A
SET toUpdate.ID_FACTURATION = (SELECT min(cf.ID)
                               from mpe.AdresseFacturationJal facturation
                                        inner join simap_prep_concentrateur_annonces_db.consultation_facturation cf
                               where cf.MAIL = facturation.email_ar
                                 and cf.ADRESSE = facturation.information_facturation
                                 and cf.SIP is false
                                 and facturation.facturation_sip = '0')
where toUpdate.ID = A.id
  and toUpdate.EU is true
  and o.ID_OFFRE_ASSOCIEE is not null;


#Update Statut Suivi Avis Pub depuis l 'historique
UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId, h.motif_rejet as motifRejet, SUBSTR(h.date_modification, 1, 16) as dateModif
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Historique_Avis_Pub h on h.id_avis = ap.id
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and h.detail_statut = '1'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT                   = 'EN_ATTENTE_VALIDATION_ECO',
    suivi.DATE_MODIFICATION        = A.dateModif,
    suivi.DATE_MODIFICATION_STATUT = A.dateModif,
    suivi.DATE_VALIDATION_NATIONAL = A.dateModif,
    suivi.DATE_VALIDATION_EUROPEEN = A.dateModif,
    suivi.DATE_VALIDATION          = A.dateModif
where suivi.ID = A.suiviId;


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId, h.motif_rejet as motifRejet, SUBSTR(h.date_modification, 1, 16) as dateModif
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Historique_Avis_Pub h on h.id_avis = ap.id
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and h.detail_statut = '2'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT                   = 'REJETE_ECO',
    suivi.DATE_MODIFICATION        = A.dateModif,
    suivi.DATE_MODIFICATION_STATUT = A.dateModif,
    suivi.DATE_VALIDATION_ECO      = A.dateModif,
    suivi.VALIDER_ECO              = false,
    suivi.RAISON_REFUS             = A.motifRejet
where suivi.ID = A.suiviId;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId, h.motif_rejet as motifRejet, SUBSTR(h.date_modification, 1, 16) as dateModif
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Historique_Avis_Pub h on h.id_avis = ap.id
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and h.detail_statut = '3'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT                   = 'EN_ATTENTE_VALIDATION_SIP',
    suivi.DATE_MODIFICATION        = A.dateModif,
    suivi.DATE_MODIFICATION_STATUT = A.dateModif,
    suivi.DATE_VALIDATION_ECO      = A.dateModif,
    suivi.VALIDER_ECO              = true,
    suivi.RAISON_REFUS             = A.motifRejet
where suivi.ID = A.suiviId;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId, h.motif_rejet as motifRejet, SUBSTR(h.date_modification, 1, 16) as dateModif
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Historique_Avis_Pub h on h.id_avis = ap.id
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.Sip = '0'
         and h.detail_statut = '4'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT                   = 'ENVOI_PLANIFIER',
    suivi.DATE_MODIFICATION        = A.dateModif,
    suivi.DATE_MODIFICATION_STATUT = A.dateModif,
    suivi.DATE_VALIDATION_ECO      = A.dateModif,
    suivi.VALIDER_ECO              = true,
    suivi.RAISON_REFUS             = A.motifRejet
where suivi.ID = A.suiviId;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId, h.motif_rejet as motifRejet, SUBSTR(h.date_modification, 1, 16) as dateModif
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Historique_Avis_Pub h on h.id_avis = ap.id
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.Sip = ' 1 '
         and h.detail_statut = ' 4 '
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT                   = ' ENVOI_PLANIFIER ',
    suivi.DATE_MODIFICATION        = A.dateModif,
    suivi.DATE_MODIFICATION_STATUT = A.dateModif,
    suivi.DATE_VALIDATION_SIP      = A.dateModif,
    suivi.VALIDER_SIP              = true,
    suivi.RAISON_REFUS             = A.motifRejet
where suivi.ID = A.suiviId;


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId, h.motif_rejet as motifRejet, SUBSTR(h.date_modification, 1, 16) as dateModif
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Historique_Avis_Pub h on h.id_avis = ap.id
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and h.detail_statut = '5'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT                   = 'ENVOYE',
    suivi.VALIDER_ECO= true,
    suivi.DATE_MODIFICATION        = A.dateModif,
    suivi.DATE_MODIFICATION_STATUT = A.dateModif,
    suivi.DATE_ENVOI_EUROPEEN      = A.dateModif
where suivi.ID = A.suiviId
  and suivi.EU is true;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId, h.motif_rejet as motifRejet, SUBSTR(h.date_modification, 1, 16) as dateModif
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Historique_Avis_Pub h on h.id_avis = ap.id
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and h.detail_statut = '5'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT                   = 'ENVOYE',
    suivi.VALIDER_ECO= true,
    suivi.DATE_MODIFICATION        = A.dateModif,
    suivi.DATE_MODIFICATION_STATUT = A.dateModif,
    suivi.DATE_ENVOI_NATIONAL      = A.dateModif
where suivi.ID = A.suiviId
  and suivi.EU is true
  and suivi.ID_FACTURATION is not null;


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId, h.motif_rejet as motifRejet, SUBSTR(h.date_modification, 1, 16) as dateModif
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Historique_Avis_Pub h on h.id_avis = ap.id
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and h.detail_statut = '5'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT                   = 'ENVOYE',
    suivi.VALIDER_ECO= true,
    suivi.DATE_MODIFICATION        = A.dateModif,
    suivi.DATE_MODIFICATION_STATUT = A.dateModif,
    suivi.DATE_ENVOI_NATIONAL      = A.dateModif
where suivi.ID = A.suiviId
  and suivi.EU is false;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId, h.motif_rejet as motifRejet, SUBSTR(h.date_modification, 1, 16) as dateModif
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Historique_Avis_Pub h on h.id_avis = ap.id
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and h.detail_statut = '6'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT                   = 'REJETE_SIP',
    suivi.DATE_MODIFICATION        = A.dateModif,
    suivi.DATE_MODIFICATION_STATUT = A.dateModif,
    suivi.DATE_VALIDATION_SIP      = A.dateModif,
    suivi.VALIDER_SIP              = false,
    suivi.RAISON_REFUS             = A.motifRejet
where suivi.ID = A.suiviId;
# -----------------------------------------

# Update Statut Suivi Avis Pub depuis l avis publié
UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.statut = '1'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT = 'EN_ATTENTE_VALIDATION_ECO'
where suivi.ID = A.suiviId;


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.statut = '2'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT      = 'REJETE_ECO',
    suivi.VALIDER_ECO = false
where suivi.ID = A.suiviId;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.statut = '3'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT      = 'EN_ATTENTE_VALIDATION_SIP',
    suivi.VALIDER_ECO = true
where suivi.ID = A.suiviId;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.Sip = '0'
         and ap.statut = '4'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT      = 'ENVOI_PLANIFIER',
    suivi.VALIDER_ECO = true
where suivi.ID = A.suiviId;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Historique_Avis_Pub h on h.id_avis = ap.id
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.Sip = '1'
         and h.detail_statut = '4'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT      = 'ENVOI_PLANIFIER',
    suivi.VALIDER_SIP = true
where suivi.ID = A.suiviId;


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.statut = '5'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT     = 'ENVOYE'
  , suivi.VALIDER_ECO= true
where suivi.ID = A.suiviId
  and suivi.EU is true;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.statut = '5'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT     = 'ENVOYE'
  , suivi.VALIDER_ECO= true
where suivi.ID = A.suiviId
  and suivi.EU is true
  and suivi.ID_FACTURATION is not null;


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.statut = '5'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT     = 'ENVOYE',
    suivi.VALIDER_ECO= true
where suivi.ID = A.suiviId
  and suivi.EU is false;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and ap.statut = '6'
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.STATUT      = 'REJETE_SIP',
    suivi.VALIDER_SIP = false
where suivi.ID = A.suiviId;

update simap_prep_concentrateur_annonces_db.suivi_type_avis_pub
set DATE_MISE_EN_LIGNE_CALCULE= null,
    DATE_ENVOI_NATIONAL       = null,
    DATE_ENVOI_EUROPEEN       = null
where STATUT in ('REJETE_SIP', 'REJETE_ECO', 'COMPLET', 'BROUILLON');


UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
set suivi.VALIDER_ECO= true
where suivi.STATUT = 'ENVOYE';

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
set suivi.VALIDER_SIP   = null,
    DATE_VALIDATION_SIP = null,
    STATUT              = 'REJETE_ECO'
where suivi.VALIDER_ECO is false;

UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
set STATUT = 'REJETE_SIP'
where suivi.VALIDER_SIP is false;

# update envoie national
UPDATE simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi
    , (SELECT distinct ap.date_envoi as date_envoi, suivi.ID as suiviId
       from mpe.Avis_Pub ap
                inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
                inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
                inner join mpe.Type_Avis_Pub tap on tap.id = ap.type_avis
       WHERE o.plateforme_uuid = @plateforme_uuid_to_update
         and ap.id = suivi.ID_MPE_OLD
         and suivi.ID_ORGANISME = o.id) as A
SET suivi.DATE_ENVOI_NATIONAL = A.date_envoi
where suivi.ID = A.suiviId
  and suivi.ID_FACTURATION is not null;

# -----------------------------------------
update simap_prep_concentrateur_annonces_db.suivi_type_avis_pub inner join simap_prep_concentrateur_annonces_db.offre annonce
    on annonce.id = simap_prep_concentrateur_annonces_db.suivi_type_avis_pub.ID_OFFRE_RACINE
SET EU = true
where annonce.CODE like '%_ENOTICES';

delete
from simap_prep_concentrateur_annonces_db.suivi_type_avis_pub
where STATUT = 'BROUILLON'
  and ID_MPE_OLD is not null;

# début AN-627

# ajout des PDF Portail
update simap_prep_concentrateur_annonces_db.suivi_type_avis_pub st
    inner join mpe.Avis_Pub ap on ap.id = st.ID_MPE_OLD
    inner join mpe.Avis a on a.id = ap.id_avis_portail
    inner join mpe.blobOrganisme_file b on b.id = a.avis
    inner join simap_prep_concentrateur_annonces_db.piece_jointe pj on pj.ID_MPE_OLD = CONCAT('blobOrganisme_file-', b.id)
SET st.ID_AVIS_PORTAIL= pj.id
where (st.ID_AVIS_PORTAIL is null or st.ID_AVIS_PORTAIL <> pj.id)
  and b.old_id is null;


update simap_prep_concentrateur_annonces_db.suivi_type_avis_pub st
    inner join mpe.Avis_Pub ap on ap.id = st.ID_MPE_OLD
    inner join mpe.Avis a on a.id = ap.id_avis_portail
    inner join mpe.blobOrganisme_file b on b.id = a.avis
    inner join simap_prep_concentrateur_annonces_db.piece_jointe pj on pj.ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id)
SET st.ID_AVIS_PORTAIL= pj.id
where b.old_id is not null
  and (st.ID_AVIS_PORTAIL is null or st.ID_AVIS_PORTAIL <> pj.id);
# fin AN-627
INSERT INTO simap_prep_concentrateur_annonces_db.suivi_annonces(DATE_TRAITEMENT, NUMERO_AVIS, MESSAGE_STATUT, NUMERO_DOSSIER, NUMERO_JOUE,
                                         LIEN_PUBLICATION, ID_CONSULTATION, ID_OFFRE,
                                         DATE_TRAITEMENT_INTERNE, DATE_DEMANDE, ID_PLATFORM, ORGANISME, DATE_CREATION,
                                         DATE_MODIFICATION, ID_AGENT, ID_ORGANISME,
                                         ID_SUPPORT, ID_SERVICE, ID_PROCEDURE,
                                         ID_SUIVI_TYPE_AVIS_PUB, ID_MPE_OLD, DATE_PUBLICATION, STATUT, XML)
SELECT distinct suivi.DATE_ENVOI_EUROPEEN,
                form.no_doc_ext,
                form.message_retour,
                dest.id_dossier,
                form.id_joue,
                form.lien_publication,
                suivi.ID_CONSULTATION,
                suivi.ID_OFFRE_RACINE,
                suivi.DATE_TRAITEMENT,
                suivi.DATE_ENVOI_EUROPEEN,
                o.plateforme_uuid,
                o.acronyme_organisme,
                suivi.DATE_CREATION,
                CASE
                    WHEN dest.DATE_MODIFICATION = '' THEN suivi.DATE_CREATION
                    ELSE dest.DATE_MODIFICATION
                    END,
                suivi.ID_CREER_PAR,
                suivi.ID_ORGANISME,
                s.ID,
                suivi.ID_SERVICE,
                suivi.ID_PROCEDURE,
                suivi.ID,
                CONCAT('Destinataire_Pub-', dest.id),
                dest.date_publication,
                CASE
                    WHEN dest.date_publication is not null THEN 'PUBLIER'
                    WHEN ap.statut = '1' THEN 'EN_ATTENTE_VALIDATION_ECO'
                    WHEN ap.statut = '2' THEN 'REJETER_CONCENTRATEUR_VALIDATION_ECO'
                    WHEN ap.statut = '3' THEN 'EN_ATTENTE_VALIDATION_SIP'
                    WHEN ap.statut = '4' THEN 'ENVOI_PLANIFIER'
                    WHEN ap.statut = '5' THEN 'EN_COURS_DE_PUBLICATION'
                    WHEN ap.statut = '6' THEN 'REJETER_CONCENTRATEUR_VALIDATION_SIP'
                    WHEN dest.etat = '5' and ap.id_avis_pdf_opoce is null THEN 'EN_COURS_DE_PUBLICATION'
                    WHEN dest.etat = '6' and ap.id_avis_pdf_opoce is null THEN 'EN_COURS_DE_PUBLICATION'
                    WHEN dest.etat = '5' and ap.id_avis_pdf_opoce is not null THEN 'PUBLIER'
                    WHEN dest.etat = '6' and ap.id_avis_pdf_opoce is not null THEN 'PUBLIER'
                    WHEN dest.etat = '7' THEN 'PUBLIER'
                    WHEN dest.etat = '8' and ap.id_avis_pdf_opoce is null THEN 'EN_COURS_DE_PUBLICATION'
                    WHEN dest.etat = '8' and ap.id_avis_pdf_opoce is not null THEN 'PUBLIER'
                    WHEN dest.etat = '9' THEN 'PUBLIER'
                    WHEN dest.etat = '10' THEN 'REJETER_SUPPORT'
                    END,
                form.xml
from mpe.Destinataire_Pub dest
         inner join (SELECT form1.*
                     from (select id_destinataire_opoce, max(opoce.id) as id
                           from mpe.FormXmlDestinataireOpoce opoce
                                    inner join mpe.Destinataire_Pub pub on pub.id = id_destinataire_opoce
                                    inner join mpe.Avis_Pub ap
                           where ap.id = pub.id_avis
                             and ap.statut <> '0'
                           group by id_destinataire_opoce) as xml
                              inner join mpe.FormXmlDestinataireOpoce form1 on xml.id = form1.id
                     group by xml.id_destinataire_opoce) form on dest.id = form.id_destinataire_opoce
         inner join mpe.Avis_Pub ap on ap.id = dest.id_avis
         inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
         inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme,
     simap_prep_concentrateur_annonces_db.support s
where s.CODE = 'ENOTICES'
  and dest.id_support = 1
  and ap.statut <> '0'
  and suivi.EU is true
  and o.plateforme_uuid = @plateforme_uuid_to_update
  and not exists(select distinct ss.ID_MPE_OLD
                 from simap_prep_concentrateur_annonces_db.suivi_annonces ss
                          inner join simap_prep_concentrateur_annonces_db.organisme o on o.id = ss.ID_ORGANISME
                 where o.plateforme_uuid = @plateforme_uuid_to_update
                   and ss.ID_MPE_OLD = CONCAT('Destinataire_Pub-', dest.id));


INSERT INTO simap_prep_concentrateur_annonces_db.suivi_annonces(DATE_TRAITEMENT, NUMERO_AVIS, MESSAGE_STATUT, NUMERO_DOSSIER, NUMERO_JOUE,
                                         LIEN_PUBLICATION, ID_CONSULTATION, ID_OFFRE,
                                         DATE_TRAITEMENT_INTERNE, DATE_DEMANDE, ID_PLATFORM, ORGANISME, DATE_CREATION,
                                         DATE_MODIFICATION, ID_AGENT, ID_ORGANISME,
                                         ID_SUPPORT, ID_SERVICE, ID_PROCEDURE,
                                         ID_SUIVI_TYPE_AVIS_PUB, ID_MPE_OLD, DATE_PUBLICATION, STATUT, XML)
SELECT suivi.DATE_ENVOI_EUROPEEN,
       null,
       null,
       dest.id_dossier,
       null,
       null,
       suivi.ID_CONSULTATION,
       suivi.ID_OFFRE_RACINE,
       suivi.DATE_TRAITEMENT,
       suivi.DATE_ENVOI_EUROPEEN,
       o.plateforme_uuid,
       o.acronyme_organisme,
       suivi.DATE_CREATION,
       CASE
           WHEN dest.DATE_MODIFICATION = '' THEN suivi.DATE_CREATION
           ELSE dest.DATE_MODIFICATION
           END
        ,
       suivi.ID_CREER_PAR,
       suivi.ID_ORGANISME,
       s.ID,
       suivi.ID_SERVICE,
       suivi.ID_PROCEDURE,
       suivi.ID,
       CONCAT('Destinataire_Pub-', dest.id),
       dest.date_publication,
       CASE
           WHEN dest.date_publication is not null THEN 'PUBLIER'
           WHEN ap.statut = '1' THEN 'EN_ATTENTE_VALIDATION_ECO'
           WHEN ap.statut = '2' THEN 'REJETER_CONCENTRATEUR_VALIDATION_ECO'
           WHEN ap.statut = '3' THEN 'EN_ATTENTE_VALIDATION_SIP'
           WHEN ap.statut = '4' THEN 'ENVOI_PLANIFIER'
           WHEN ap.statut = '5' THEN 'PUBLIER'
           WHEN ap.statut = '6' THEN 'REJETER_CONCENTRATEUR_VALIDATION_SIP'
           WHEN dest.etat = '5' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '6' THEN 'PUBLIER'
           WHEN dest.etat = '7' THEN 'PUBLIER'
           WHEN dest.etat = '8' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '9' THEN 'PUBLIER'
           WHEN dest.etat = '10' THEN 'REJETER_SUPPORT'
           END,
       null
from mpe.Destinataire_Pub dest
         inner join mpe.Avis_Pub ap on ap.id = dest.id_avis
         inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
         inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme,
     simap_prep_concentrateur_annonces_db.support s
where s.CODE = 'ENOTICES'
  and dest.id_support = 1
  and o.plateforme_uuid = @plateforme_uuid_to_update
  and ap.statut <> '0'
  and suivi.EU is true
  and not exists(select distinct ss.ID_MPE_OLD
                 from simap_prep_concentrateur_annonces_db.suivi_annonces ss
                          inner join simap_prep_concentrateur_annonces_db.organisme o on o.id = ss.ID_ORGANISME
                 where o.plateforme_uuid = @plateforme_uuid_to_update
                   and ss.ID_MPE_OLD = CONCAT('Destinataire_Pub-', dest.id));


# add annonce JAL (support 2)

INSERT INTO simap_prep_concentrateur_annonces_db.suivi_annonces(DATE_TRAITEMENT, NUMERO_DOSSIER, DATE_PUBLICATION, ID_CONSULTATION, ID_OFFRE,
                                         DATE_TRAITEMENT_INTERNE, DATE_DEMANDE, ID_PLATFORM, ORGANISME, DATE_CREATION,
                                         DATE_MODIFICATION, ID_AGENT, ID_ORGANISME,
                                         ID_SUPPORT, ID_SERVICE, ID_PROCEDURE,
                                         ID_SUIVI_TYPE_AVIS_PUB, ID_MPE_OLD, STATUT)
SELECT suivi.DATE_ENVOI_NATIONAL,
       dest.id_dossier,
       dest.date_publication,
       suivi.ID_CONSULTATION,
       off.ID_OFFRE_ASSOCIEE,
       suivi.DATE_TRAITEMENT,
       suivi.DATE_ENVOI_NATIONAL,
       o.plateforme_uuid,
       o.acronyme_organisme,
       suivi.DATE_CREATION,
       CASE
           WHEN dest.DATE_MODIFICATION = '' THEN suivi.DATE_CREATION
           ELSE dest.DATE_MODIFICATION
           END
        ,
       suivi.ID_CREER_PAR,
       suivi.ID_ORGANISME,
       s.ID as supportId,
       suivi.ID_SERVICE,
       suivi.ID_PROCEDURE,
       suivi.ID,
       CONCAT('Destinataire_Annonce_Press-', desJal.id),
       CASE
           WHEN dest.date_publication is not null THEN 'PUBLIER'
           WHEN ap.statut = '1' THEN 'EN_ATTENTE_VALIDATION_ECO'
           WHEN ap.statut = '2' THEN 'REJETER_CONCENTRATEUR_VALIDATION_ECO'
           WHEN ap.statut = '3' THEN 'EN_ATTENTE_VALIDATION_SIP'
           WHEN ap.statut = '4' THEN 'ENVOI_PLANIFIER'
           WHEN ap.statut = '5' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN ap.statut = '6' THEN 'REJETE_SIP'
           WHEN dest.etat = '5' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '6' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '7' THEN 'PUBLIER'
           WHEN dest.etat = '8' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '9' THEN 'PUBLIER'
           WHEN dest.etat = '10' THEN 'REJETER_SUPPORT'
           END
from mpe.Destinataire_Annonce_Press desJal
         left join mpe.JAL jal on desJal.id_jal = jal.id
         left join mpe.Annonce_Press form on desJal.id_annonce_press = form.id
         inner join mpe.Destinataire_Pub dest on dest.id = form.id_Dest_Press
         inner join mpe.Avis_Pub ap on ap.id = dest.id_avis
         inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
         inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
         inner join simap_prep_concentrateur_annonces_db.offre off on off.id = suivi.ID_OFFRE_RACINE,
     simap_prep_concentrateur_annonces_db.support s
where s.CODE = CONCAT(@plateforme_lang_to_update, '-', jal.email, '-', TO_BASE64(jal.nom))
  and o.plateforme_uuid = @plateforme_uuid_to_update
  and dest.id_support = 2
  and ap.statut <> '0'
  and suivi.EU is true
  and not exists(select distinct ss.ID_MPE_OLD
                 from simap_prep_concentrateur_annonces_db.suivi_annonces ss
                          inner join simap_prep_concentrateur_annonces_db.organisme o on o.id = ss.ID_ORGANISME
                 where o.plateforme_uuid = @plateforme_uuid_to_update
                   and ss.ID_MPE_OLD = CONCAT('Destinataire_Annonce_Press-', desJal.id));


# add annonce JAL National (support 2)

INSERT INTO simap_prep_concentrateur_annonces_db.suivi_annonces(DATE_TRAITEMENT, NUMERO_DOSSIER, DATE_PUBLICATION, ID_CONSULTATION, ID_OFFRE,
                                         DATE_TRAITEMENT_INTERNE, DATE_DEMANDE, ID_PLATFORM, ORGANISME, DATE_CREATION,
                                         DATE_MODIFICATION, ID_AGENT, ID_ORGANISME,
                                         ID_SUPPORT, ID_SERVICE, ID_PROCEDURE,
                                         ID_SUIVI_TYPE_AVIS_PUB, ID_MPE_OLD, STATUT)
SELECT suivi.DATE_ENVOI_NATIONAL,
       dest.id_dossier,
       dest.date_publication,
       suivi.ID_CONSULTATION,
       off.ID,
       suivi.DATE_TRAITEMENT,
       suivi.DATE_ENVOI_NATIONAL,
       o.plateforme_uuid,
       o.acronyme_organisme,
       suivi.DATE_CREATION,
       CASE
           WHEN dest.DATE_MODIFICATION = '' THEN suivi.DATE_CREATION
           ELSE dest.DATE_MODIFICATION
           END
        ,
       suivi.ID_CREER_PAR,
       suivi.ID_ORGANISME,
       s.ID as supportId,
       suivi.ID_SERVICE,
       suivi.ID_PROCEDURE,
       suivi.ID,
       CONCAT('Destinataire_Annonce_Press-', desJal.id),
       CASE
           WHEN dest.date_publication is not null THEN 'PUBLIER'
           WHEN ap.statut = '1' THEN 'EN_ATTENTE_VALIDATION_ECO'
           WHEN ap.statut = '2' THEN 'REJETER_CONCENTRATEUR_VALIDATION_ECO'
           WHEN ap.statut = '3' THEN 'EN_ATTENTE_VALIDATION_SIP'
           WHEN ap.statut = '4' THEN 'ENVOI_PLANIFIER'
           WHEN ap.statut = '5' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN ap.statut = '6' THEN 'REJETE_SIP'
           WHEN dest.etat = '5' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '6' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '7' THEN 'PUBLIER'
           WHEN dest.etat = '8' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '9' THEN 'PUBLIER'
           WHEN dest.etat = '10' THEN 'REJETER_SUPPORT'
           END
from mpe.Destinataire_Annonce_Press desJal
         left join mpe.JAL jal on desJal.id_jal = jal.id
         left join mpe.Annonce_Press form on desJal.id_annonce_press = form.id
         inner join mpe.Destinataire_Pub dest on dest.id = form.id_Dest_Press
         inner join mpe.Avis_Pub ap on ap.id = dest.id_avis
         inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
         inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
         inner join simap_prep_concentrateur_annonces_db.offre off on off.id = suivi.ID_OFFRE_RACINE,
     simap_prep_concentrateur_annonces_db.support s
where s.CODE = CONCAT(@plateforme_lang_to_update, '-', jal.email, '-', TO_BASE64(jal.nom))
  and o.plateforme_uuid = @plateforme_uuid_to_update
  and dest.id_support = 2
  and ap.statut <> '0'
  and suivi.EU is false
  and not exists(select distinct ss.ID_MPE_OLD
                 from simap_prep_concentrateur_annonces_db.suivi_annonces ss
                          inner join simap_prep_concentrateur_annonces_db.organisme o on o.id = ss.ID_ORGANISME
                 where o.plateforme_uuid = @plateforme_uuid_to_update
                   and ss.ID_MPE_OLD = CONCAT('Destinataire_Annonce_Press-', desJal.id));


# add annonce JAL without jal in mpe.destinataire_annonce_press

INSERT INTO simap_prep_concentrateur_annonces_db.suivi_annonces(DATE_TRAITEMENT, NUMERO_DOSSIER, DATE_PUBLICATION, ID_CONSULTATION, ID_OFFRE,
                                         DATE_TRAITEMENT_INTERNE, DATE_DEMANDE, ID_PLATFORM, ORGANISME, DATE_CREATION,
                                         DATE_MODIFICATION, ID_AGENT, ID_ORGANISME,
                                         ID_SUPPORT, ID_SERVICE, ID_PROCEDURE,
                                         ID_SUIVI_TYPE_AVIS_PUB, ID_MPE_OLD, STATUT)
SELECT suivi.DATE_ENVOI_NATIONAL,
       dest.id_dossier,
       dest.date_publication,
       suivi.ID_CONSULTATION,
       suivi.ID_OFFRE_RACINE,
       suivi.DATE_TRAITEMENT,
       suivi.DATE_ENVOI_NATIONAL,
       o.plateforme_uuid,
       o.acronyme_organisme,
       suivi.DATE_CREATION,
       CASE
           WHEN dest.DATE_MODIFICATION = '' THEN suivi.DATE_CREATION
           ELSE dest.DATE_MODIFICATION
           END
        ,
       suivi.ID_CREER_PAR,
       suivi.ID_ORGANISME,
       s.ID as supportId,
       suivi.ID_SERVICE,
       suivi.ID_PROCEDURE,
       suivi.ID,
       CONCAT('Destinataire_Pub-', dest.id),
       CASE
           WHEN dest.date_publication is not null THEN 'PUBLIER'
           WHEN ap.statut = '1' THEN 'EN_ATTENTE_VALIDATION_ECO'
           WHEN ap.statut = '2' THEN 'REJETER_CONCENTRATEUR_VALIDATION_ECO'
           WHEN ap.statut = '3' THEN 'EN_ATTENTE_VALIDATION_SIP'
           WHEN ap.statut = '4' THEN 'ENVOI_PLANIFIER'
           WHEN ap.statut = '5' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN ap.statut = '6' THEN 'REJETER_CONCENTRATEUR_VALIDATION_SIP'
           WHEN dest.etat = '5' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '6' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '7' THEN 'PUBLIER'
           WHEN dest.etat = '8' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN dest.etat = '9' THEN 'PUBLIER'
           WHEN dest.etat = '10' THEN 'REJETER_SUPPORT'
           END
from mpe.Annonce_Press form
         inner join mpe.Destinataire_Pub dest on dest.id = form.id_Dest_Press
         inner join mpe.Avis_Pub ap on ap.id = dest.id_avis
         inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
         inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme,
     (select min(ID) as ID from simap_prep_concentrateur_annonces_db.support where CODE like CONCAT(@plateforme_lang_to_update, '-%')) as s
where not (exists (select children1_.ID
                   from mpe.destinataire_annonce_press children1_
                   where form.ID = children1_.id_annonce_press))
  and dest.id_support = 2
  and o.plateforme_uuid = @plateforme_uuid_to_update
  and ap.statut <> '0'
  and not exists(select distinct ss.ID_MPE_OLD
                 from simap_prep_concentrateur_annonces_db.suivi_annonces ss
                          inner join simap_prep_concentrateur_annonces_db.organisme o on o.id = ss.ID_ORGANISME
                 where o.plateforme_uuid = @plateforme_uuid_to_update
                   and ss.ID_MPE_OLD = CONCAT('Destinataire_Pub-', dest.id));

# add annonce JAL without jal in mpe.destinataire_annonce_press

INSERT INTO simap_prep_concentrateur_annonces_db.suivi_annonces(DATE_TRAITEMENT, NUMERO_DOSSIER, DATE_PUBLICATION, ID_CONSULTATION, ID_OFFRE,
                                         DATE_TRAITEMENT_INTERNE, DATE_DEMANDE, ID_PLATFORM, ORGANISME, DATE_CREATION,
                                         DATE_MODIFICATION, ID_AGENT, ID_ORGANISME,
                                         ID_SUPPORT, ID_SERVICE, ID_PROCEDURE,
                                         ID_SUIVI_TYPE_AVIS_PUB, ID_MPE_OLD, STATUT)
SELECT suivi.DATE_ENVOI_NATIONAL,
       null,
       null,
       suivi.ID_CONSULTATION,
       suivi.ID_OFFRE_RACINE,
       suivi.DATE_TRAITEMENT,
       suivi.DATE_ENVOI_NATIONAL,
       o.plateforme_uuid,
       o.acronyme_organisme,
       suivi.DATE_CREATION,
       ap.date_creation,

       suivi.ID_CREER_PAR,
       suivi.ID_ORGANISME,
       s.ID as supportId,
       suivi.ID_SERVICE,
       suivi.ID_PROCEDURE,
       suivi.ID,
       CONCAT('Avis_Publie_', ap.id),
       CASE
           WHEN ap.date_publication is not null THEN 'PUBLIER'
           WHEN ap.statut = '1' THEN 'EN_ATTENTE_VALIDATION_ECO'
           WHEN ap.statut = '2' THEN 'REJETER_CONCENTRATEUR_VALIDATION_ECO'
           WHEN ap.statut = '3' THEN 'EN_ATTENTE_VALIDATION_SIP'
           WHEN ap.statut = '4' THEN 'ENVOI_PLANIFIER'
           WHEN ap.statut = '5' THEN 'EN_COURS_DE_PUBLICATION'
           WHEN ap.statut = '6' THEN 'REJETER_CONCENTRATEUR_VALIDATION_SIP'
           END
from mpe.Avis_Pub ap
         inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
         inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme,
     (select min(ID) as ID from simap_prep_concentrateur_annonces_db.support where CODE like CONCAT(@plateforme_lang_to_update, '-%')) as s
where not (exists (select children1_.ID
                   from mpe.Destinataire_Pub children1_
                   where ap.ID = children1_.id_avis
                     and children1_.id_support = 2))
  and o.plateforme_uuid = @plateforme_uuid_to_update
  and ap.statut <> '0'
  and suivi.ID_FACTURATION is not null
  and not exists(select distinct ss.ID_MPE_OLD
                 from simap_prep_concentrateur_annonces_db.suivi_annonces ss
                          inner join simap_prep_concentrateur_annonces_db.organisme o on o.id = ss.ID_ORGANISME
                 where o.plateforme_uuid = @plateforme_uuid_to_update
                   and ss.ID_MPE_OLD = CONCAT('Avis_Publie_', ap.id));

#update suivi_annonces EU false piece jointe
update simap_prep_concentrateur_annonces_db.suivi_annonces sa inner join simap_prep_concentrateur_annonces_db.support s on s.id = sa.ID_SUPPORT
    inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub st on st.ID = sa.ID_SUIVI_TYPE_AVIS_PUB
    inner join mpe.Avis_Pub ap on ap.id = st.ID_MPE_OLD
    inner join mpe.annonce_press_piecejointe piece on piece.id = ap.id_avis_presse
    inner join mpe.bloborganisme_file b on b.id = piece.piece
    inner join simap_prep_concentrateur_annonces_db.piece_jointe pj on pj.ID_MPE_OLD =
                                                CONCAT('Annonce_Press_PieceJointe-', ap.id_avis_presse)
SET sa.ID_AVIS_FICHIER= pj.id
where s.EUROPEEN is false
  and (sa.ID_AVIS_FICHIER is null or sa.ID_AVIS_FICHIER <> pj.id)
  and b.old_id is null;

#update suivi_annonces EU false piece jointe
update simap_prep_concentrateur_annonces_db.suivi_annonces sa inner join simap_prep_concentrateur_annonces_db.support s on s.id = sa.ID_SUPPORT
    inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub st on st.ID = sa.ID_SUIVI_TYPE_AVIS_PUB
    inner join mpe.Avis_Pub ap on ap.id = st.ID_MPE_OLD
    inner join mpe.annonce_press_piecejointe piece on piece.id = ap.id_avis_presse
    inner join mpe.bloborganisme_file b on b.id = piece.piece
    inner join simap_prep_concentrateur_annonces_db.piece_jointe pj on pj.ID_MPE_OLD =
                                                CONCAT('blobOrganisme_file-old-id-', b.old_id)
SET sa.ID_AVIS_FICHIER= pj.id
where s.EUROPEEN is false
  and (sa.ID_AVIS_FICHIER is null or sa.ID_AVIS_FICHIER <> pj.id)
  and b.old_id is not null;
# début AN-627

#update suivi_annonces EU true piece jointe
update simap_prep_concentrateur_annonces_db.suivi_annonces sa inner join simap_prep_concentrateur_annonces_db.support s on s.id = sa.ID_SUPPORT
    inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub st on st.ID = sa.ID_SUIVI_TYPE_AVIS_PUB
    inner join mpe.Avis_Pub ap on ap.id = st.ID_MPE_OLD
    inner join mpe.Avis a on a.id = ap.id_avis_pdf_opoce
    inner join mpe.bloborganisme_file b on b.id = a.avis
    inner join simap_prep_concentrateur_annonces_db.piece_jointe pj on pj.ID_MPE_OLD = CONCAT('blobOrganisme_file-', b.id)
SET sa.ID_AVIS_FICHIER= pj.id
where s.EUROPEEN is true
  and (sa.ID_AVIS_FICHIER is null or sa.ID_AVIS_FICHIER <> pj.id)
  and sa.LIEN_PUBLICATION is null
  and b.old_id is null;

#update suivi_annonces EU true piece jointe
update simap_prep_concentrateur_annonces_db.suivi_annonces sa inner join simap_prep_concentrateur_annonces_db.support s on s.id = sa.ID_SUPPORT
    inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub st on st.ID = sa.ID_SUIVI_TYPE_AVIS_PUB
    inner join mpe.Avis_Pub ap on ap.id = st.ID_MPE_OLD
    inner join mpe.Avis a on a.id = ap.id_avis_pdf_opoce
    inner join mpe.bloborganisme_file b on b.id = a.avis
    inner join simap_prep_concentrateur_annonces_db.piece_jointe pj on pj.ID_MPE_OLD = CONCAT('blobOrganisme_file-old-id-', b.old_id)
SET sa.ID_AVIS_FICHIER= pj.id
where s.EUROPEEN is true
  and b.old_id is not null
  and (sa.ID_AVIS_FICHIER is null or sa.ID_AVIS_FICHIER <> pj.id)
  and sa.LIEN_PUBLICATION is null;
# fin AN-627




# mise à jour de la table association
INSERT INTO simap_prep_concentrateur_annonces_db.support_offre_asso (ID_SUPPORT, ID_OFFRE)
select distinct annonce.id_support, annonce.id_offre
from simap_prep_concentrateur_annonces_db.suivi_annonces annonce
WHERE annonce.ID_OFFRE is not null
  and annonce.ID_MPE_OLD is not null
  and (annonce.id_support, annonce.id_offre) not in
      (SELECT asso.id_support, asso.id_offre from simap_prep_concentrateur_annonces_db.support_offre_asso asso);

# Ajout des eforms instance


INSERT INTO simap_prep_concentrateur_annonces_db.e_forms_formulaire(UUID, NO_DOC_EXT, STATUT, DATE_CREATION, LANG, ID_CONSULTATION,
                                             NOTICE_VERSION,
                                             DATE_MODIFICATION, ID_NOTICE, valid, XML_GENERE,
                                             PLATEFORME, PUBLICATION_ID, PUBLICATION_DATE,
                                             REFERENCE_CONSULTATION, INTITULE_CONSULTATION, VERSION_SDK,
                                             ID_SUIVI_ANNONCE,
                                             DATE_ENVOI_SOUMISSION, DATE_SOUMISSION,
                                             ID_ORGANISME, ID_CREER_PAR, ID_MODIFIER_PAR, DATE_MODIFICATION_STATUT,
                                             ID_MPE_OLD, TED_VERSION)
SELECT form.code_retour,
       form.no_doc_ext,
       CASE
           WHEN annonce.STATUT = 'EN_COURS_DE_PUBLICATION' THEN 'PUBLISHING'
           WHEN annonce.STATUT = 'PUBLIER' THEN 'PUBLISHED'
           WHEN annonce.STATUT = 'EN_ATTENTE_VALIDATION_ECO' THEN 'CONCENTRATEUR_VALIDATING_ECO'
           WHEN annonce.STATUT = 'EN_ATTENTE_VALIDATION_SIP' THEN 'CONCENTRATEUR_VALIDATING_SIP'
           WHEN annonce.STATUT = 'REJETER_CONCENTRATEUR_VALIDATION_ECO' THEN 'CONCENTRATEUR_REJET_ECO'
           WHEN annonce.STATUT = 'REJETER_CONCENTRATEUR_VALIDATION_SIP' THEN 'CONCENTRATEUR_REJET_SIP'
           WHEN annonce.STATUT = 'EN_ATTENTE' THEN 'CONCENTRATEUR_VALIDATED'
           WHEN annonce.STATUT = 'ENVOI_PLANIFIER' THEN 'SUBMITTING'
           WHEN annonce.STATUT = 'REJETER_SUPPORT' THEN 'VALIDATION_FAILED'
           END,
       annonce.DATE_MODIFICATION,
       'FRA',
       annonce.ID_CONSULTATION,
       1,
       annonce.DATE_MODIFICATION,
       REPLACE(offre.CODE, '_ENOTICES', ''),
       true,
       annonce.XML,
       annonce.ID_PLATFORM,
       annonce.NUMERO_JOUE,
       annonce.DATE_PUBLICATION,
       suivi.REFERENCE,
       suivi.TITRE,
       'esentool',
       annonce.ID,
       suivi.DATE_ENVOI_EUROPEEN,
       suivi.DATE_ENVOI_EUROPEEN,
       annonce.ID_ORGANISME,
       suivi.ID_CREER_PAR,
       suivi.ID_MODIFIER_PAR,
       suivi.DATE_MODIFICATION_STATUT,
       CONCAT('FormXmlDestinataireOpoce-', form.id),
       'esentool'


from mpe.Destinataire_Pub dest
         inner join simap_prep_concentrateur_annonces_db.suivi_annonces annonce on annonce.ID_MPE_OLD = CONCAT('Destinataire_Pub-', dest.id)
         inner join simap_prep_concentrateur_annonces_db.support s on annonce.ID_SUPPORT = s.ID
         inner join simap_prep_concentrateur_annonces_db.offre offre on annonce.ID_OFFRE = offre.ID
         inner join mpe.FormXmlDestinataireOpoce form on dest.id = form.id_destinataire_opoce
         inner join mpe.Avis_Pub ap on ap.id = dest.id_avis
         inner join simap_prep_concentrateur_annonces_db.suivi_type_avis_pub suivi on suivi.id_mpe_old = ap.id
         inner join simap_prep_concentrateur_annonces_db.organisme o on o.acronyme_organisme = ap.organisme
where s.CODE = 'ENOTICES'
  and dest.id_support = 1
  and annonce.ID_AVIS_FICHIER is null
  and annonce.ID_CONSULTATION is not null
  and o.plateforme_uuid = @plateforme_uuid_to_update
  and not exists(select distinct ss.ID_MPE_OLD
                 from simap_prep_concentrateur_annonces_db.e_forms_formulaire ss
                          inner join simap_prep_concentrateur_annonces_db.organisme o on o.id = ss.ID_ORGANISME
                 where o.plateforme_uuid = @plateforme_uuid_to_update
                   and ss.ID_MPE_OLD = CONCAT('FormXmlDestinataireOpoce-', form.id));
# déplacer cette liste de fichiers du nas de MPE vers le nas de concentrateur PUB

SET @serveur_concentrateur = 'simap-concpub-prep-01';
SET @user_concentrateur = 'tomcat';
SET @serveur_mpe = 'simap-mpe-prod-01';
SET @user_mpe = 'tomcat';
SET @nas_concentrateur = '/data/annonces/fichiers/';
SET @nas_mpe = '/data/apache2/pmp.tp.etat.lu/data/';

SELECT rCrC.*
from (SELECT distinct CONCAT(
                              'cpt=$((cpt+1)); echo "$cpt" ; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                              ' "test ! -d ''', @nas_concentrateur, result.folder, ''' && mkdir -p ''',
                              @nas_concentrateur, result.folder,
                              '''"; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                              ' [ ! -f "', @nas_concentrateur, result.chemin, '" ] && scp -3q ', @user_mpe, '@',
                              @serveur_mpe, ':', @nas_mpe, result.chemin,
                              ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur,
                              result.chemin,
                              ' || echo "file exists"')
      FROM (SELECT distinct CONCAT(b.chemin, b.id, '-0') as chemin, b.chemin as folder
            from mpe.Avis app
                     inner join mpe.Avis_Pub apu on apu.id_avis_pdf_opoce = app.id
                     inner join mpe.blobOrganisme_file b on b.id = app.avis
            where b.chemin is not null
              and b.old_id is null
            UNION

            SELECT distinct CONCAT(b.chemin, b.id, '-0') as chemin, b.chemin as folder
            from mpe.Annonce_Press_PieceJointe app
                     left join mpe.Annonce_Press ap on ap.id = app.id_annonce_press
                     inner join mpe.Destinataire_Pub dp
                                on dp.id = ap.id_Dest_Press
                     inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
                     inner join mpe.blobOrganisme_file b on b.id = app.piece
            where b.chemin is not null
              and b.old_id is null) as result
# déplacer cette liste de fichiers du nas de MPE vers le nas de concentrateur PUB
      UNION
      SELECT distinct CONCAT(' cpt=$((cpt+1)); echo "$cpt" ; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                             ' [ ! -f "', @nas_concentrateur, result.chemin,
                             '" ] && scp -3q ', @user_mpe, '@',
                             @serveur_mpe, ':', @nas_mpe, result.chemin,
                             ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur,
                             result.chemin,
                             ' || echo "file exists"')
      FROM (SELECT distinct CONCAT(b.organisme, '/files/', b.id, '-0') as chemin, b.organisme as organisme

            from mpe.Avis app

                     inner join mpe.Avis_Pub apu on apu.id_avis_pdf_opoce = app.id
                     inner join mpe.blobOrganisme_file b on b.id = app.avis
            where b.chemin is null
              and b.old_id is null
            UNION

            SELECT distinct CONCAT(b.organisme, '/files/', b.id, '-0') as chemin, b.organisme as organisme
            from mpe.Annonce_Press_PieceJointe app
                     left join mpe.Annonce_Press ap on ap.id = app.id_annonce_press
                     inner join mpe.Destinataire_Pub dp
                                on dp.id = ap.id_Dest_Press
                     inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
                     inner join mpe.blobOrganisme_file b on b.id = app.piece
            where b.chemin is null
              and b.old_id is null) as result


      UNION
      SELECT distinct CONCAT(
                              'cpt=$((cpt+1)); echo "$cpt" ; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                              ' "test ! -d ''', @nas_concentrateur, result.folder, ''' && mkdir -p ''',
                              @nas_concentrateur, result.folder,
                              '''"; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                              ' [ ! -f "', @nas_concentrateur, result.chemin, '" ] && scp -3q ', @user_mpe, '@',
                              @serveur_mpe, ':', @nas_mpe, result.chemin,
                              ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur,
                              result.chemin,
                              ' || echo "file exists"')
      FROM (SELECT distinct CONCAT(b.chemin, b.old_id, '-0') as chemin, b.chemin as folder
            from mpe.Avis app
                     inner join mpe.Avis_Pub apu on apu.id_avis_pdf_opoce = app.id
                     inner join mpe.blobOrganisme_file b on b.id = app.avis
            where b.chemin is not null
              and b.old_id is not null
            UNION

            SELECT distinct CONCAT(b.chemin, b.old_id, '-0') as chemin, b.chemin as folder
            from mpe.Annonce_Press_PieceJointe app
                     left join mpe.Annonce_Press ap on ap.id = app.id_annonce_press
                     inner join mpe.Destinataire_Pub dp
                                on dp.id = ap.id_Dest_Press
                     inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
                     inner join mpe.blobOrganisme_file b on b.id = app.piece
            where b.chemin is not null
              and b.old_id is not null) as result

# déplacer cette liste de fichiers du nas de MPE vers le nas de concentrateur PUB
      UNION
      SELECT distinct CONCAT(' cpt=$((cpt+1)); echo "$cpt" ; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                             ' [ ! -f "', @nas_concentrateur, result.chemin,
                             '" ] && scp -3q ', @user_mpe, '@',
                             @serveur_mpe, ':', @nas_mpe, result.chemin,
                             ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur,
                             result.chemin,
                             ' || echo "file exists"')
      FROM (SELECT distinct CONCAT(b.organisme, '/files/', b.old_id, '-0') as chemin, b.organisme as organisme

            from mpe.Avis app

                     inner join mpe.Avis_Pub apu on apu.id_avis_pdf_opoce = app.id
                     inner join mpe.blobOrganisme_file b on b.id = app.avis
            where b.chemin is null
              and b.old_id is not null
            UNION

            SELECT distinct CONCAT(b.organisme, '/files/', b.old_id, '-0') as chemin, b.organisme as organisme
            from mpe.Annonce_Press_PieceJointe app
                     left join mpe.Annonce_Press ap on ap.id = app.id_annonce_press
                     inner join mpe.Destinataire_Pub dp
                                on dp.id = ap.id_Dest_Press
                     inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
                     inner join mpe.blobOrganisme_file b on b.id = app.piece
            where b.chemin is null
              and b.old_id is not null) as result

      UNION
      SELECT distinct CONCAT(
                              'cpt=$((cpt+1)); echo "$cpt" ; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                              ' "test ! -d ''', @nas_concentrateur, result.folder, ''' && mkdir -p ''',
                              @nas_concentrateur, result.folder,
                              '''"; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                              ' [ ! -f "', @nas_concentrateur, result.chemin, '" ] && scp -3q ', @user_mpe, '@',
                              @serveur_mpe, ':', @nas_mpe, result.chemin,
                              ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur,
                              result.chemin,
                              ' || echo "file exists"')
      FROM (SELECT distinct CONCAT(b.chemin, b.old_id, '-0') as chemin, b.chemin as folder
            from mpe.Avis app
                     inner join mpe.Avis_Pub apu on apu.id_avis_pdf_opoce = app.id
                     inner join mpe.blobOrganisme_file b on b.id = app.avis
            where b.chemin is not null
              and b.old_id is not null
            UNION

            SELECT distinct CONCAT(b.chemin, b.old_id, '-0') as chemin, b.chemin as folder
            from mpe.Annonce_Press_PieceJointe app
                     left join mpe.Annonce_Press ap on ap.id = app.id_annonce_press
                     inner join mpe.Destinataire_Pub dp
                                on dp.id = ap.id_Dest_Press
                     inner join mpe.Avis_Pub apu on apu.id = dp.id_avis
                     inner join mpe.blobOrganisme_file b on b.id = app.piece
            where b.chemin is not null
              and b.old_id is not null) as result
      # début AN-627

# scp pièce portail

      UNION
      SELECT distinct CONCAT(
                              'cpt=$((cpt+1)); echo "$cpt" ; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                              ' "test ! -d ''', @nas_concentrateur, result.folder, ''' && mkdir -p ''',
                              @nas_concentrateur, result.folder,
                              '''"; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                              ' [ ! -f "', @nas_concentrateur, result.chemin, '" ] && scp -3q ', @user_mpe, '@',
                              @serveur_mpe, ':', @nas_mpe, result.chemin,
                              ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur,
                              result.chemin,
                              ' || echo "file exists"')
      FROM (SELECT distinct CONCAT(b.chemin, b.old_id, '-0') as chemin, b.chemin as folder
            from mpe.Avis_Pub apu
                     inner join mpe.Avis a on a.id = apu.id_avis_portail
                     inner join mpe.blobOrganisme_file b on b.id = a.avis
            where b.chemin is not null
              and b.old_id is not null) as result

      UNION
      SELECT distinct CONCAT(' cpt=$((cpt+1)); echo "$cpt" ; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                             ' [ ! -f "', @nas_concentrateur, result.chemin,
                             '" ] && scp -3q ', @user_mpe, '@',
                             @serveur_mpe, ':', @nas_mpe, result.chemin,
                             ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur,
                             result.chemin,
                             ' || echo "file exists"')
      FROM (SELECT distinct CONCAT(b.organisme, '/files/', b.old_id, '-0') as chemin, b.organisme as organisme

            from mpe.Avis_Pub apu
                     inner join mpe.Avis a on a.id = apu.id_avis_portail
                     inner join mpe.blobOrganisme_file b on b.id = a.avis
            where b.chemin is null
              and b.old_id is not null) as result


      UNION
      SELECT distinct CONCAT(
                              'cpt=$((cpt+1)); echo "$cpt" ; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                              ' "test ! -d ''', @nas_concentrateur, result.folder, ''' && mkdir -p ''',
                              @nas_concentrateur, result.folder,
                              '''"; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                              ' [ ! -f "', @nas_concentrateur, result.chemin, '" ] && scp -3q ', @user_mpe, '@',
                              @serveur_mpe, ':', @nas_mpe, result.chemin,
                              ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur,
                              result.chemin,
                              ' || echo "file exists"')
      FROM (SELECT distinct CONCAT(b.chemin, b.id, '-0') as chemin, b.chemin as folder
            from mpe.Avis_Pub apu
                     inner join mpe.Avis a on a.id = apu.id_avis_portail
                     inner join mpe.blobOrganisme_file b on b.id = a.avis
            where b.chemin is not null
              and b.old_id is null) as result
# déplacer cette liste de fichiers du nas de MPE vers le nas de concentrateur PUB
      UNION
      SELECT distinct CONCAT(' cpt=$((cpt+1)); echo "$cpt" ; ssh -q ', @user_concentrateur, '@', @serveur_concentrateur,
                             ' [ ! -f "', @nas_concentrateur, result.chemin,
                             '" ] && scp -3q ', @user_mpe, '@',
                             @serveur_mpe, ':', @nas_mpe, result.chemin,
                             ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur,
                             result.chemin,
                             ' || echo "file exists"')
      FROM (SELECT distinct CONCAT(b.organisme, '/files/', b.id, '-0') as chemin, b.organisme as organisme
            from mpe.Avis_Pub apu
                     inner join mpe.Avis a on a.id = apu.id_avis_portail
                     inner join mpe.blobOrganisme_file b on b.id = a.avis
            where b.chemin is null
              and b.old_id is null) as result) as rCrC;

# fin AN-627
