SET @plateforme_uuid_to_update = 'MPE_simap_prod';
SET @plateforme_url_to_update = 'https://pmp.tp.etat.lu';
SET @plateforme_lang_to_update = 'LUX';

SET @serveur_concentrateur = 'simap-concpub-prod-01';
SET @user_concentrateur = 'olp-atx';
SET @serveur_mpe = 'simap-mpe-prod-01';
SET @user_mpe = 'olp-atx';
SET @nas_concentrateur = '/data/annonces/fichiers/';
SET @nas_mpe = '/data/apache2/pmp.tp.etat.lu/data/';

# scp pièce portail

SELECT distinct CONCAT('ssh ', @user_concentrateur, '@', @serveur_concentrateur, ' "mkdir -p ', @nas_concentrateur,
                       result.folder,
                       '"; scp -3q ', @user_mpe, '@',
                       @serveur_mpe, ':', @nas_mpe, result.chemin,
                       ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur, result.chemin)
FROM (SELECT distinct CONCAT(b.chemin, b.old_id, '-0') as chemin, b.chemin as folder
      from mpe.Avis_Pub apu
               inner join mpe.Avis a on a.id = apu.id_avis_portail
               inner join mpe.blobOrganisme_file b on b.id = a.avis
      where b.chemin is not null
        and b.old_id is not null) as result;

SELECT distinct CONCAT('scp -3q ', @user_mpe, '@',
                       @serveur_mpe, ':', @nas_mpe, result.chemin,
                       ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur, result.chemin)
FROM (SELECT distinct CONCAT(b.organisme, '/files/', b.old_id, '-0') as chemin, b.organisme as organisme

      from mpe.Avis_Pub apu
               inner join mpe.Avis a on a.id = apu.id_avis_portail
               inner join mpe.blobOrganisme_file b on b.id = a.avis
      where b.chemin is null
        and b.old_id is not null) as result;



SELECT distinct CONCAT('ssh ', @user_concentrateur, '@', @serveur_concentrateur, ' "sudo mkdir -p ', @nas_concentrateur,
                       result.folder,
                       '";', ' sudo scp ', @user_mpe, '@',
                       @serveur_mpe, ':', @nas_mpe, result.chemin,
                       ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur, result.chemin)
FROM (SELECT distinct CONCAT(b.chemin, b.id, '-0') as chemin, b.chemin as folder
      from mpe.Avis_Pub apu
               inner join mpe.Avis a on a.id = apu.id_avis_portail
               inner join mpe.blobOrganisme_file b on b.id = a.avis
      where b.chemin is not null
        and b.old_id is null) as result;
# déplacer cette liste de fichiers du nas de MPE vers le nas de concentrateur PUB
SELECT distinct CONCAT('ssh ', @user_concentrateur, '@', @serveur_concentrateur, ' "sudo mkdir -p ', @nas_concentrateur,
                       result.organisme,
                       '/files "; sudo scp ', @user_mpe, '@',
                       @serveur_mpe, ':', @nas_mpe, result.chemin,
                       ' ', @user_concentrateur, '@', @serveur_concentrateur, ':', @nas_concentrateur, result.chemin)
FROM (SELECT distinct CONCAT(b.organisme, '/files/', b.id, '-0') as chemin, b.organisme as organisme
      from mpe.Avis_Pub apu
               inner join mpe.Avis a on a.id = apu.id_avis_portail
               inner join mpe.blobOrganisme_file b on b.id = a.avis
      where b.chemin is null
        and b.old_id is null) as result;

# fin AN-627
