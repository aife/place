-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : pmp-bdd-master
-- Généré le : lun. 27 nov. 2023 à 14:56
-- Version du serveur : 10.3.37-MariaDB-0ubuntu0.20.04.1-log
-- Version de PHP : 8.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `simap_prod_MPE_DB`
--

-- --------------------------------------------------------

--
-- Structure de la table `AnnonceJALPieceJointe`
--

CREATE TABLE `AnnonceJALPieceJointe` (
  `id` int(11) NOT NULL,
  `organisme` varchar(30) NOT NULL,
  `id_annonce_jal` int(11) NOT NULL DEFAULT 0,
  `nom_fichier` varchar(100) NOT NULL DEFAULT '',
  `piece` int(11) NOT NULL DEFAULT 0,
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(25) NOT NULL DEFAULT '',
  `taille` varchar(25) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `AnnonceJALPieceJointe`
--
ALTER TABLE `AnnonceJALPieceJointe`
  ADD PRIMARY KEY (`id`,`organisme`),
  ADD KEY `id_annonce_jal` (`id_annonce_jal`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `AnnonceJALPieceJointe`
--
ALTER TABLE `AnnonceJALPieceJointe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `AnnonceJALPieceJointe`
--
ALTER TABLE `AnnonceJALPieceJointe`
  ADD CONSTRAINT `AnnonceJALPieceJointe_ibfk_1` FOREIGN KEY (`id_annonce_jal`) REFERENCES `AnnonceJAL` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
