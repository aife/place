-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : pmp-bdd-master
-- Généré le : lun. 27 nov. 2023 à 15:00
-- Version du serveur : 10.3.37-MariaDB-0ubuntu0.20.04.1-log
-- Version de PHP : 8.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `simap_prod_MPE_DB`
--

-- --------------------------------------------------------

--
-- Structure de la table `DestinataireAnnonceJAL`
--

CREATE TABLE `DestinataireAnnonceJAL` (
  `id` int(11) NOT NULL,
  `organisme` varchar(30) NOT NULL,
  `old_jal_id` int(11) DEFAULT NULL,
  `idAnnonceJAL` int(11) NOT NULL DEFAULT 0,
  `date_envoi` varchar(14) NOT NULL DEFAULT '',
  `date_pub` varchar(14) NOT NULL DEFAULT '',
  `statut` char(1) NOT NULL DEFAULT '',
  `accuse` char(1) NOT NULL DEFAULT '',
  `old_id_echange` int(11) DEFAULT NULL,
  `date_ar` varchar(25) DEFAULT NULL,
  `idJAL` bigint(20) UNSIGNED DEFAULT NULL,
  `id_echange` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `DestinataireAnnonceJAL`
--
ALTER TABLE `DestinataireAnnonceJAL`
  ADD PRIMARY KEY (`id`,`organisme`),
  ADD KEY `idJAL` (`old_jal_id`),
  ADD KEY `idAnnonceJAL` (`idAnnonceJAL`),
  ADD KEY `FK_destinataireAnnonceJAL_jal_id` (`idJAL`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `DestinataireAnnonceJAL`
--
ALTER TABLE `DestinataireAnnonceJAL`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `DestinataireAnnonceJAL`
--
ALTER TABLE `DestinataireAnnonceJAL`
  ADD CONSTRAINT `FK_destinataireAnnonceJAL_jal_id` FOREIGN KEY (`idJAL`) REFERENCES `JAL` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `destinataireAnnonceJAL_annonceJal_id` FOREIGN KEY (`idAnnonceJAL`) REFERENCES `AnnonceJAL` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
