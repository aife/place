-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : pmp-bdd-master
-- Généré le : lun. 27 nov. 2023 à 15:03
-- Version du serveur : 10.3.37-MariaDB-0ubuntu0.20.04.1-log
-- Version de PHP : 8.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `simap_prod_MPE_DB`
--

-- --------------------------------------------------------

--
-- Structure de la table `Type_Avis_Pub`
--

CREATE TABLE `Type_Avis_Pub` (
  `id` int(11) NOT NULL,
  `libelle` varchar(100) NOT NULL,
  `libelle_ar` varchar(100) DEFAULT NULL,
  `libelle_cz` varchar(100) DEFAULT NULL,
  `libelle_du` varchar(100) DEFAULT NULL,
  `libelle_en` varchar(100) DEFAULT NULL,
  `libelle_es` varchar(100) DEFAULT NULL,
  `libelle_fr` varchar(100) DEFAULT NULL,
  `libelle_it` varchar(100) DEFAULT NULL,
  `libelle_su` varchar(100) DEFAULT NULL,
  `region` int(11) NOT NULL,
  `resource_formulaire` varchar(100) NOT NULL,
  `ressource_doc_presse` varchar(255) DEFAULT NULL COMMENT 'Permet de stocker les noms des ressources des documens generés et envoyés à la presse',
  `nature_avis` int(2) DEFAULT NULL COMMENT 'Permet de préciser la nature de l''avis de publicité, 1 pour les avis pré information, 2 pour les avis initiaux, 3 pour les autres avis',
  `id_dispositif` int(11) NOT NULL DEFAULT 0,
  `type_pub` int(11) NOT NULL DEFAULT 0 COMMENT '0 : Esender, 1: SUB, 2: TED '
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `Type_Avis_Pub`
--

INSERT INTO `Type_Avis_Pub` (`id`, `libelle`, `libelle_ar`, `libelle_cz`, `libelle_du`, `libelle_en`, `libelle_es`, `libelle_fr`, `libelle_it`, `libelle_su`, `region`, `resource_formulaire`, `ressource_doc_presse`, `nature_avis`, `id_dispositif`, `type_pub`) VALUES
(1, 'EU01 AVIS DE PRE-INFORMATION', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_01', 'ressources/templatesAvis/TemplateANMarche_', 1, 1, 1),
(2, 'EU02a AVIS DE MARCHE (OUVERTE)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_02', 'ressources/templatesAvis/TemplateANMarches_EU2a_', 2, 2, 1),
(3, 'EU02b AVIS DE MARCHE (RESTREINTE-NEGOCIEE)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_02', 'ressources/templatesAvis/TemplateANCandidature_EU2b_', 2, 2, 1),
(4, 'EU03 AVIS D\'ATTRIBUTION', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_03', 'ressources/templatesAvis/TemplateANMarche_', 3, 3, 1),
(5, 'EU04 AVIS PERIODIQUE INDICATIF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_04', 'ressources/templatesAvis/TemplateANMarche_', 1, 4, 1),
(6, 'EU05a AVIS DE MARCHE OUVERTE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_05', 'ressources/templatesAvis/TemplateANMarches_EU5a_', 2, 5, 1),
(7, 'EU05b AVIS DE MARCHE (RESTREINTE-NEGOCIEE)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_05', 'ressources/templatesAvis/TemplateANCandidature_EU5b_', 2, 5, 1),
(8, 'EU06 AVIS D\'ATTRIBUTION', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_06', 'ressources/templatesAvis/TemplateANMarche_', 3, 6, 1),
(9, 'EU07 SYSTEME DE QUALIFICATION', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_07', 'ressources/templatesAvis/TemplateANMarche_', 2, 7, 1),
(12, 'EU12 AVIS DE CONCOURS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_12', 'ressources/templatesAvis/TemplateANConcours_EU12_', 2, 8, 1),
(13, 'EU13 RESULTATS DE CONCOURS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_13', 'ressources/templatesAvis/TemplateANMarche_', 3, 9, 1),
(14, 'EU14 RECTIFICATIF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_14', 'ressources/templatesAvis/TemplateANRectificatifEU14_', 4, 10, 1),
(15, 'EU15 AVIS TRANSPARENCE EX-ANTE VOLONTAIRE    ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_15', 'ressources/templatesAvis/TemplateANMarche_', 2, 11, 1),
(16, 'LU01 OUVERTE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 'Form_11', 'ressources/templatesAvis/TemplateANMarche_', NULL, 0, 0),
(17, 'LU02 RESTREINTE-NEGOCIEE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 'Form_12', 'ressources/templatesAvis/TemplateANCandidature_', NULL, 0, 0),
(18, 'LU03 RECTIFICATIF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 'Form_13', 'ressources/templatesAvis/TemplateANRectificatif_', NULL, 0, 0),
(19, 'LU05 DEMANDE_OFFRES', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 'Form_14', 'ressources/templatesAvis/TemplateANDemandeOffre_', NULL, 0, 0),
(21, 'EU21 SERVICES SOCIAUX', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_21', 'ressources/templatesAvis/TemplateANMarche_', 3, 0, 2),
(22, 'EU22b SERVICES SOCIAUX - SECTEURS SPECIAUX', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_22', ' ressources/templatesAvis/TemplateANCandidature_EU5b_', 3, 0, 2),
(23, 'EU23 SERVICES SOCIAUX - CONCESSIONS - PREINFORMATION', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 'Form_23', 'ressources/templatesAvis/TemplateANMarche_', 3, 0, 2),
(24, 'EU24 AVIS DE CONCESSION       ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_24', 'ressources/templatesAvis/TemplateANMarche_', 2, 0, 2),
(25, 'EU25 AVIS D\'ATTRIBUTION DE CONCESSION', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_25', 'ressources/templatesAvis/TemplateANMarche_', 3, 0, 2),
(26, 'EU14 RECTIFICATIF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_14', 'ressources/templatesAvis/TemplateANRectificatifEU14_', 4, 0, 2),
(27, 'EU15 AVIS TRANSPARENCE EX-ANTE VOLONTAIRE    ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_15', 'ressources/templatesAvis/TemplateANMarche_', 2, 24, 2),
(28, 'EU16 DEFENSE - AVIS DE PRE-INFORMATION', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_16', 'ressources/templatesAvis/TemplateANMarche_ ', 2, 1, 2),
(29, 'EU17 - DEFENSE - AVIS DE MARCHE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_17', ' ressources/templatesAvis/TemplateANCandidature_EU5b_', 2, 2, 2),
(30, 'EU18 DEFENSE - AVIS D\'ATTRIBUTION', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_18', 'ressources/templatesAvis/TemplateANMarche_ ', 2, 3, 2),
(31, 'EU19 DEFENSE - AVIS DE SOUS-TRAITANCE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_19', 'ressources/templatesAvis/TemplateANMarche_ ', 0, 0, 2),
(32, 'EU21 SERVICES SOCIAUX-ATTRIBUTION', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_18', NULL, 2, 0, 2),
(33, 'EU21 SERVICES SOCIAUX-PREINFORMATION', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_18', NULL, 1, 0, 2),
(34, 'EU22a SERVICES SOCIAUX - SECTEURS SPECIAUX - INDICATIF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_18', 'ressources/templatesAvis/TemplateANMarche_ ', 1, 0, 2),
(35, 'EU22c SERVICES SOCIAUX - SECTEURS SPECIAUX - ATTRIBUTION', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_18', 'ressources/templatesAvis/TemplateANMarche_ ', 3, 0, 2),
(36, 'EU23 SERVICES SOCIAUX - CONCESSIONS - ATTRIBUTION', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_18', 'ressources/templatesAvis/TemplateANMarche_ ', 3, 0, 2),
(37, 'EUT1 CONTRAT SERVICE PUBLIC - PRE-INFORMATION', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_37', 'ressources/templatesAvis/TemplateANMarche_', 1, 0, 2),
(38, 'EUT2 CONTRAT SERVICE PUBLIC - ATTRIBUTION', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_18', 'ressources/templatesAvis/TemplateANMarche_', 3, 0, 2),
(39, 'LU09 AVIS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Form_18', NULL, 2, 0, 2);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Type_Avis_Pub`
--
ALTER TABLE `Type_Avis_Pub`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `Type_Avis_Pub`
--
ALTER TABLE `Type_Avis_Pub`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
