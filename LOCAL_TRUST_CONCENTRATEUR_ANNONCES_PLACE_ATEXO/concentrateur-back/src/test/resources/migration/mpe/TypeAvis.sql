-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : pmp-bdd-master
-- Généré le : lun. 27 nov. 2023 à 15:04
-- Version du serveur : 10.3.37-MariaDB-0ubuntu0.20.04.1-log
-- Version de PHP : 8.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `simap_prod_MPE_DB`
--

-- --------------------------------------------------------

--
-- Structure de la table `TypeAvis`
--

CREATE TABLE `TypeAvis` (
  `id` int(11) NOT NULL,
  `intitule_avis` varchar(100) NOT NULL DEFAULT '',
  `intitule_avis_fr` varchar(100) DEFAULT NULL,
  `intitule_avis_en` varchar(100) DEFAULT NULL,
  `intitule_avis_es` varchar(100) DEFAULT NULL,
  `intitule_avis_su` varchar(100) DEFAULT NULL,
  `intitule_avis_du` varchar(100) DEFAULT NULL,
  `intitule_avis_cz` varchar(100) DEFAULT NULL,
  `intitule_avis_ar` varchar(100) DEFAULT NULL,
  `abbreviation` varchar(50) NOT NULL DEFAULT '',
  `id_type_avis_ANM` int(11) NOT NULL DEFAULT 0,
  `intitule_avis_it` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `TypeAvis`
--

INSERT INTO `TypeAvis` (`id`, `intitule_avis`, `intitule_avis_fr`, `intitule_avis_en`, `intitule_avis_es`, `intitule_avis_su`, `intitule_avis_du`, `intitule_avis_cz`, `intitule_avis_ar`, `abbreviation`, `id_type_avis_ANM`, `intitule_avis_it`) VALUES
(2, 'Avis de pré-information', 'Avis de pré-information', 'Preinformation notice', 'Anuncio previo', 'Annons om fÃ¶rhandsinformation', NULL, NULL, NULL, 'API', 1, 'Avviso di preinformazione'),
(3, 'Avis de marché', 'Avis de marché', 'Contract notice', 'Anuncio de licitaci', 'Annons om upphandling', NULL, NULL, NULL, 'AAPC', 2, 'Bando di gara'),
(4, 'Avis d\'attribution', 'Avis d\'attribution', 'Contract award notice', 'Anuncio de adjudicaci', 'Annons om tilldelat kontrakt', NULL, NULL, NULL, 'AA', 3, 'Avviso di aggiudicazione'),
(8, 'Annonce de décision de résiliation', 'Annonce de décision de résiliation', NULL, NULL, NULL, NULL, NULL, '??????? ???? ???????', 'ADR', 0, NULL),
(9, 'Projet d\'achat', 'Projet d\'achat', 'en Projet d\'achat ', 'es Projet d\'achat ', NULL, NULL, NULL, NULL, 'APA', 9, NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `TypeAvis`
--
ALTER TABLE `TypeAvis`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `TypeAvis`
--
ALTER TABLE `TypeAvis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
