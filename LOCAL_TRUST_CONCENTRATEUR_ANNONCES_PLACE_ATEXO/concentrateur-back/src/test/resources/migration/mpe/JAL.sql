-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : pmp-bdd-master
-- Généré le : lun. 27 nov. 2023 à 15:02
-- Version du serveur : 10.3.37-MariaDB-0ubuntu0.20.04.1-log
-- Version de PHP : 8.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `simap_prod_MPE_DB`
--

-- --------------------------------------------------------

--
-- Structure de la table `JAL`
--

CREATE TABLE `JAL` (
  `old_id` int(11) DEFAULT NULL,
  `organisme` varchar(30) NOT NULL,
  `old_service_id` int(11) DEFAULT NULL,
  `nom` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `email_ar` varchar(100) NOT NULL,
  `telecopie` varchar(20) NOT NULL,
  `information_facturation` mediumtext NOT NULL,
  `service_id` bigint(20) UNSIGNED DEFAULT NULL,
  `id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `JAL`
--

INSERT INTO `JAL` (`old_id`, `organisme`, `old_service_id`, `nom`, `email`, `email_ar`, `telecopie`, `information_facturation`, `service_id`, `id`) VALUES
(2, 't5y', 0, 'Le Quotidien', 'annonces@tageblatt.lu', '', '', '', NULL, 29),
(3, 't5y', 0, 'Luxemburger Wort', 'publicite@saint-paul.lu', '', '', '', NULL, 30),
(4, 't5y', 0, 'Tageblatt', 'annonces@tageblatt.lu', '', '', '', NULL, 31),
(5, 't5y', 0, ' Zeitung vum Lëtzebuerger Vollek', 'zeitungpub@zlv.lu', '', '', '', NULL, 32),
(8, 't5y', 0, 'Lëtzebuerger Land', 'land@land.lu', '', '', '', NULL, 33),
(9, 't5y', 0, 'Woxx', 'admin@woxx.lu', '', '', '', NULL, 34),
(10, 't5y', 0, 'ZZZ - Test - PAS DE PUBLICATION PRESSE', 'info@marches.public.lu', '', '', '', NULL, 35),
(11, 't5y', 0, 'De Letzeburger Bauer', 'annonces@centralepaysanne.lu', '', '', '', NULL, 36),
(13, 't5y', 0, 'ZZZ adresse_de_Test', 'sarah.benyachou@atexo.com', '', '', '', NULL, 37),
(14, 't5y', 0, 'ZZY 101 Studios S.A. (agence de communication)', '101@101.lu', '', '', '', NULL, 38);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `JAL`
--
ALTER TABLE `JAL`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `old_primary_key` (`old_id`,`organisme`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `JAL`
--
ALTER TABLE `JAL`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
