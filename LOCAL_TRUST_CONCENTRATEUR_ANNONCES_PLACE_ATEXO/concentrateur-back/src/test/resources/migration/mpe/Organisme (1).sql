-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : pmp-bdd-master
-- Généré le : lun. 27 nov. 2023 à 15:02
-- Version du serveur : 10.3.37-MariaDB-0ubuntu0.20.04.1-log
-- Version de PHP : 8.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `simap_prod_MPE_DB`
--

-- --------------------------------------------------------

--
-- Structure de la table `Organisme`
--

CREATE TABLE `Organisme` (
  `id` int(11) NOT NULL,
  `acronyme` varchar(30) NOT NULL DEFAULT '',
  `type_article_org` int(1) NOT NULL DEFAULT 0,
  `denomination_org` mediumtext DEFAULT NULL,
  `categorie_insee` varchar(20) DEFAULT NULL,
  `description_org` longtext DEFAULT NULL,
  `adresse` varchar(100) NOT NULL DEFAULT '',
  `cp` varchar(5) NOT NULL DEFAULT '',
  `ville` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL DEFAULT '',
  `id_attrib_file` varchar(11) DEFAULT NULL,
  `attrib_file` varchar(150) NOT NULL DEFAULT '',
  `date_creation` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` char(1) NOT NULL DEFAULT '1',
  `id_client_ANM` varchar(32) NOT NULL DEFAULT '0',
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `signataire_cao` mediumtext DEFAULT NULL,
  `offset` char(3) NOT NULL DEFAULT '0',
  `sigle` varchar(100) NOT NULL DEFAULT '',
  `adresse2` varchar(100) NOT NULL DEFAULT '',
  `tel` varchar(50) NOT NULL DEFAULT '',
  `telecopie` varchar(50) NOT NULL DEFAULT '',
  `pays` varchar(150) DEFAULT NULL,
  `affichage_entite` char(1) NOT NULL DEFAULT '',
  `id_initial` int(11) NOT NULL DEFAULT 0,
  `denomination_org_ar` varchar(100) NOT NULL DEFAULT '',
  `adresse_ar` varchar(100) NOT NULL DEFAULT '',
  `adresse2_ar` varchar(100) NOT NULL DEFAULT '',
  `denomination_org_fr` varchar(100) NOT NULL DEFAULT '',
  `adresse_fr` varchar(100) NOT NULL DEFAULT '',
  `adresse2_fr` varchar(100) NOT NULL DEFAULT '',
  `denomination_org_es` varchar(100) NOT NULL DEFAULT '',
  `adresse_es` varchar(100) NOT NULL DEFAULT '',
  `adresse2_es` varchar(100) NOT NULL DEFAULT '',
  `denomination_org_en` varchar(100) NOT NULL DEFAULT '',
  `adresse_en` varchar(100) NOT NULL DEFAULT '',
  `adresse2_en` varchar(100) NOT NULL DEFAULT '',
  `denomination_org_su` varchar(100) NOT NULL DEFAULT '',
  `adresse_su` varchar(100) NOT NULL DEFAULT '',
  `adresse2_su` varchar(100) NOT NULL DEFAULT '',
  `denomination_org_du` varchar(100) NOT NULL DEFAULT '',
  `adresse_du` varchar(100) NOT NULL DEFAULT '',
  `adresse2_du` varchar(100) NOT NULL DEFAULT '',
  `denomination_org_cz` varchar(100) NOT NULL DEFAULT '',
  `adresse_cz` varchar(100) NOT NULL DEFAULT '',
  `adresse2_cz` varchar(100) NOT NULL DEFAULT '',
  `denomination_org_it` varchar(100) NOT NULL DEFAULT '',
  `adresse_it` varchar(100) NOT NULL DEFAULT '',
  `adresse2_it` varchar(100) NOT NULL DEFAULT '',
  `description_org_ar` longtext NOT NULL,
  `ville_ar` varchar(100) NOT NULL DEFAULT '',
  `pays_ar` varchar(150) NOT NULL DEFAULT '',
  `description_org_fr` longtext NOT NULL,
  `ville_fr` varchar(100) NOT NULL DEFAULT '',
  `pays_fr` varchar(150) NOT NULL DEFAULT '',
  `description_org_es` longtext NOT NULL,
  `ville_es` varchar(100) NOT NULL DEFAULT '',
  `pays_es` varchar(150) NOT NULL DEFAULT '',
  `description_org_en` longtext NOT NULL,
  `ville_en` varchar(100) NOT NULL DEFAULT '',
  `pays_en` varchar(150) NOT NULL DEFAULT '',
  `description_org_su` longtext NOT NULL,
  `ville_su` varchar(100) NOT NULL DEFAULT '',
  `pays_su` varchar(150) NOT NULL DEFAULT '',
  `description_org_du` longtext NOT NULL,
  `ville_du` varchar(100) NOT NULL DEFAULT '',
  `pays_du` varchar(150) NOT NULL DEFAULT '',
  `description_org_cz` longtext NOT NULL,
  `ville_cz` varchar(100) NOT NULL DEFAULT '',
  `pays_cz` varchar(150) NOT NULL DEFAULT '',
  `description_org_it` longtext NOT NULL,
  `ville_it` varchar(100) NOT NULL DEFAULT '',
  `pays_it` varchar(150) NOT NULL DEFAULT '',
  `siren` varchar(9) NOT NULL,
  `complement` varchar(5) NOT NULL,
  `moniteur_provenance` int(11) DEFAULT NULL COMMENT 'contient la valeur de la balise PROV dans le fichier xml ''context'' du MOL',
  `code_acces_logiciel` varchar(30) DEFAULT NULL COMMENT 'contient le code d''acces du logiciel',
  `decalage_horaire` varchar(5) DEFAULT NULL COMMENT 'Permet de renseigner le decalage horaire',
  `lieu_residence` varchar(255) DEFAULT NULL COMMENT 'Permet de renseigner le lieu de residence du serice',
  `activation_fuseau_horaire` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d''activer l''utilisation du fuseau horaire pour le service. Activéà 1 et desactivé à 0',
  `alerte` enum('0','1') NOT NULL DEFAULT '0',
  `ordre` int(11) NOT NULL DEFAULT 0 COMMENT 'permet d''afficher les consultation selon un ordre par organisme',
  `URL_INTERFACE_ANM` varchar(100) DEFAULT NULL COMMENT 'url utilisé par le Cli l''interface annonces marches ',
  `sous_type_organisme` int(11) NOT NULL DEFAULT 2 COMMENT '1: Etat et ses établissements publics - Autres que ceux ayant un caractère industriel et commercial 2 :Collectivités territoriales / EPL / EPS',
  `pf_url` varchar(256) DEFAULT NULL,
  `tag_purge` tinyint(1) DEFAULT 0 COMMENT 'Champs qui permet de définir si cette organisme doit être purger (0: on ne faite rien ,1: on purge)',
  `id_externe` varchar(50) NOT NULL DEFAULT '0',
  `id_entite` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `Organisme`
--

INSERT INTO `Organisme` (`id`, `acronyme`, `type_article_org`, `denomination_org`, `categorie_insee`, `description_org`, `adresse`, `cp`, `ville`, `email`, `url`, `id_attrib_file`, `attrib_file`, `date_creation`, `active`, `id_client_ANM`, `status`, `signataire_cao`, `offset`, `sigle`, `adresse2`, `tel`, `telecopie`, `pays`, `affichage_entite`, `id_initial`, `denomination_org_ar`, `adresse_ar`, `adresse2_ar`, `denomination_org_fr`, `adresse_fr`, `adresse2_fr`, `denomination_org_es`, `adresse_es`, `adresse2_es`, `denomination_org_en`, `adresse_en`, `adresse2_en`, `denomination_org_su`, `adresse_su`, `adresse2_su`, `denomination_org_du`, `adresse_du`, `adresse2_du`, `denomination_org_cz`, `adresse_cz`, `adresse2_cz`, `denomination_org_it`, `adresse_it`, `adresse2_it`, `description_org_ar`, `ville_ar`, `pays_ar`, `description_org_fr`, `ville_fr`, `pays_fr`, `description_org_es`, `ville_es`, `pays_es`, `description_org_en`, `ville_en`, `pays_en`, `description_org_su`, `ville_su`, `pays_su`, `description_org_du`, `ville_du`, `pays_du`, `description_org_cz`, `ville_cz`, `pays_cz`, `description_org_it`, `ville_it`, `pays_it`, `siren`, `complement`, `moniteur_provenance`, `code_acces_logiciel`, `decalage_horaire`, `lieu_residence`, `activation_fuseau_horaire`, `alerte`, `ordre`, `URL_INTERFACE_ANM`, `sous_type_organisme`, `pf_url`, `tag_purge`, `id_externe`, `id_entite`) VALUES
(1, 't5y', 1, 'Portail des marchés publics', '4', 'Direction des marchés publics', '', '', '', '', '', NULL, '', '2011-10-25 13:43:49', '1', '0', '0', NULL, '0', 'PMP.LU', '', '', '', 'LUXEMBOURG', '1', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '440909562', '00033', NULL, NULL, NULL, NULL, '0', '0', 0, NULL, 2, NULL, 0, '0', NULL),
(2, 'a1t', 3, 'Organisme de test pour les entreprises', '7.1', 'Organisme de test pour les entreprises', '', '', '', '', '', NULL, '', '2013-11-21 16:53:20', '1', '0', '0', NULL, '0', 'A1T', '', '', '', 'Luxembourg', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '440909562', '00033', NULL, NULL, NULL, NULL, '0', '0', 0, NULL, 2, NULL, 0, '0', NULL),
(3, 'p8x', 3, 'Organisme de test', '7.4.90', 'Organisme de test', '', '', '', '', '', NULL, '', '2016-01-13 14:18:39', '0', '0', '0', NULL, '0', 'P8X', '', '', '', 'France', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '732829320', '00074', NULL, NULL, NULL, NULL, '0', '0', 0, NULL, 2, NULL, 0, '0', NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Organisme`
--
ALTER TABLE `Organisme`
  ADD PRIMARY KEY (`id`),
  ADD KEY `acronyme` (`acronyme`),
  ADD KEY `categorie_insee` (`categorie_insee`),
  ADD KEY `Idx_Organisme_active` (`active`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `Organisme`
--
ALTER TABLE `Organisme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `Organisme`
--
ALTER TABLE `Organisme`
  ADD CONSTRAINT `Organisme_ibfk_1` FOREIGN KEY (`categorie_insee`) REFERENCES `CategorieINSEE` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
