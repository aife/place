-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : pmp-bdd-master
-- Généré le : lun. 27 nov. 2023 à 14:56
-- Version du serveur : 10.3.37-MariaDB-0ubuntu0.20.04.1-log
-- Version de PHP : 8.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `simap_prod_MPE_DB`
--

-- --------------------------------------------------------

--
-- Structure de la table `AnnonceJAL`
--

CREATE TABLE `AnnonceJAL` (
  `id` int(11) NOT NULL,
  `organisme` varchar(30) NOT NULL,
  `libelle_type` varchar(250) NOT NULL DEFAULT '',
  `date_creation` varchar(20) NOT NULL,
  `objet` varchar(255) DEFAULT NULL,
  `texte` mediumtext DEFAULT NULL,
  `consultation_ref` int(11) NOT NULL DEFAULT 0,
  `option_envoi` int(11) DEFAULT NULL,
  `consultation_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `AnnonceJAL`
--
ALTER TABLE `AnnonceJAL`
  ADD PRIMARY KEY (`id`,`organisme`),
  ADD KEY `consultation_idx` (`consultation_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `AnnonceJAL`
--
ALTER TABLE `AnnonceJAL`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
