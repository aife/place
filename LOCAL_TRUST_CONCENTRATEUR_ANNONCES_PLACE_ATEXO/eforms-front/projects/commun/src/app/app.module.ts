import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ComponentsModule} from "@shared-global/components/components.module";
import {PipesModule} from "@shared-global/pipes/pipes.module";
import {CoreModule} from "@shared-global/core/core.module";
import {EformsStoreModule} from "@shared-global/store/eforms-store.module";
import {ToastrModule} from "ngx-toastr";
import {AtexoToastComponent} from "@shared-global/components/atexo-toast/atexo-toast.component";
import {DirectivesModule} from "@shared-global/directives/directives.module";
import {NgbCarouselConfig, NgbModalConfig, NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {BsModalService} from "ngx-bootstrap/modal";

@NgModule({
  declarations: [],
  imports: [
    NgbModule,
    CommonModule, ComponentsModule, PipesModule, CoreModule, EformsStoreModule, DirectivesModule,
    ToastrModule.forRoot({
      preventDuplicates: true,
      toastComponent: AtexoToastComponent
    }),
  ], providers: [NgbCarouselConfig,
    NgbModalConfig,
    BsModalService]
})
export class BibliotequeCommuneModule {
}
