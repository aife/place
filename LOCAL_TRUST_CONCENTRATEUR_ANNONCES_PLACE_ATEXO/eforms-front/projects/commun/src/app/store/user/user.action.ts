import {createAction, props} from '@ngrx/store';

export const setAccessToken = createAction('[USER] Set Access Token', props<{ accessToken: string, organisme: string, plateforme: string , redirect: string }>());
export const setDefaultParams = createAction('[USER] Set DefaultPage', props<{
  defaultPage: string,
  isWebComponent: boolean
}>());
export const logoutUser = createAction('[USER] Logout User');
export const successLogout = createAction('[USER] Logout');
