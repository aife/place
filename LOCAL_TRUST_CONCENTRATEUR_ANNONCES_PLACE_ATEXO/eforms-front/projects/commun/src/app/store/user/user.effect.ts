import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {logoutUser, setAccessToken, successLogout} from "@shared-global/store/user/user.action";
import {switchMap} from "rxjs/operators";
import {AuthenticationService} from "@shared-global/core/services/agent/authentification.service";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";

@Injectable()
export class UserEffect {
    constructor(
        private router: RoutingInterneService,
        private readonly actions$: Actions,
        private readonly authenticationService: AuthenticationService,
    ) {
    }

    logoutUser$ = createEffect(() => this.actions$.pipe(
        ofType(logoutUser),
        switchMap(() => {
            if (sessionStorage.getItem('eformsAccessToken')) {
                this.authenticationService.logout().then(res => {
                    this.router.navigateFromContexte("/login");
                }, err => {

                });
            }
            return [successLogout()]
        }))
    )

}
