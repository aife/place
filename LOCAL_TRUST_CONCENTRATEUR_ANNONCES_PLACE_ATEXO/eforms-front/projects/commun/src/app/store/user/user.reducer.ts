import {createReducer, on} from "@ngrx/store";
import {setAccessToken, setDefaultParams, successLogout} from "./user.action";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";

export interface UserState {
  connecting: boolean,
  connected: boolean,
  accessToken: string,
  organisme: string,
  plateforme: string,
  defaultPage: string,
  isWebComponent: boolean,
  error: string
}


function getInitialState(): UserState {
  let accessToken = sessionStorage.getItem('eformsAccessToken');
  if (isTokenExpired(accessToken)) {
    sessionStorage.removeItem("eformsAccessToken")
    sessionStorage.removeItem("eformsPlateforme")
    sessionStorage.removeItem("eformsOrganisme")
    sessionStorage.removeItem("isWebComponent")
    return {
      accessToken: null,
      connecting: false,
      organisme: null,
      plateforme: null,
      connected: false,
      defaultPage: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.ACCUEIL,
      isWebComponent: false,
      error: null
    };
  }
  return {
    accessToken: accessToken,
    connecting: false,
    organisme: sessionStorage.getItem('eformsOrganisme'),
    plateforme: sessionStorage.getItem('eformsPlateforme'),
    connected: !!accessToken,
    defaultPage: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.ACCUEIL,
    isWebComponent: JSON.parse(sessionStorage.getItem("isWebComponent")),
    error: null
  };
}

function isTokenExpired(token) {
  if (!token)
    return true;
  const base64Url = token.split(".")[1];
  const base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
  const jsonPayload = decodeURIComponent(
    atob(base64)
      .split("")
      .map(function (c) {
        return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join("")
  );

  const {exp} = JSON.parse(jsonPayload);
  return Date.now() >= exp * 1000
}


const initialState: UserState = getInitialState();

const _userReducer = createReducer(
  initialState,
  on(setAccessToken, (state, props) => {
    sessionStorage.setItem('eformsAccessToken', props.accessToken);
    sessionStorage.setItem('eformsPlateforme', props.plateforme);
    sessionStorage.setItem('eformsOrganisme', props.organisme);
    return {
      ...state,
      accessToken: props.accessToken,
      plateforme: props.plateforme,
      organisme: props.organisme,
      connecting: false,
      connected: true,
      error: null
    }
  }), on(setDefaultParams, (state, props) => {
    return {
      ...state,
      ...props,
    }
  }),
  on(successLogout, (state, props) => {
    return initialState;

  })
);

export function userReducer(state, action) {
  return _userReducer(state, action);
}
