import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {switchMap} from "rxjs/operators";
import {ToastrService} from 'ngx-toastr';
import {showErrorToast, showSuccessToast, showWarningToast} from "@shared-global/store/toast/toast.action";

@Injectable()
export class ToastEffect {
  constructor(
    private readonly actions$: Actions,
    private toastService: ToastrService
  ) {
  }


  showSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(showSuccessToast),
    switchMap((action) => {
        this.toastService.success(action.message, action.header);
        return [];

      }
    )))

  showWarning$ = createEffect(() => this.actions$.pipe(
    ofType(showWarningToast),
    switchMap((action) => {
        this.toastService.warning(action.message, action.header, {timeOut: 60 * 1000,});
        return [];

      }
    )))

  showError$ = createEffect(() => this.actions$.pipe(
    ofType(showErrorToast),
    switchMap((action) => {
        this.toastService.error(action.message, action.header);
        return [];

      }
    )))


}
