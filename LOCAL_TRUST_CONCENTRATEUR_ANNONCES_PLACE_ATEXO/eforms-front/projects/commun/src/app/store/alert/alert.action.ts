import {createAction, props} from '@ngrx/store';

export const showSuccessAlert = createAction('[ALERT] Show Success Alert',
  props<{ messages: Array<string>, icon: string, keepAfterNavigationChange: boolean, clickable: boolean }>());
export const showWarningAlert = createAction('[ALERT] Show Warning Alert',
  props<{ messages: Array<string>, icon: string, keepAfterNavigationChange: boolean, clickable: boolean }>());

export const showErrorAlert = createAction('[ALERT] Show Error Alert',
  props<{ messages: Array<string>, icon: string, keepAfterNavigationChange: boolean, clickable: boolean }>());
export const showInfoAlert = createAction('[ALERT] Show Info Alert',
  props<{ messages: Array<string>, icon: string, keepAfterNavigationChange: boolean, clickable: boolean }>());
export const selectAlert = createAction('[ALERT] Select Alert',
  props<{ index:number }>());
export const clearAlert = createAction('[ALERT] Clear Alert');
