import {createReducer, on} from "@ngrx/store";
import {
  clearAlert, selectAlert,
  showErrorAlert,
  showInfoAlert,
  showSuccessAlert,
  showWarningAlert
} from "@shared-global/store/alert/alert.action";

export interface AlertState {
  alert: {
    messages: Array<string>,
    type: string,
    icon: string,
    keepAfterNavigationChange: boolean,
    clickable: boolean
  },
  selectedMessage: number
}

const initialState: AlertState = {
  alert: null,
  selectedMessage: null
};


const _alertReducer = createReducer(
  initialState,
  on(selectAlert, (state, props) => {
    return {
      ...state,
      selectedMessage: props.index
    }
  }),
  on(showSuccessAlert, (state, props) => {
    return {
      ...state,
      alert: {
        ...props,
        type: "success"
      }
    }
  }), on(showWarningAlert, (state, props) => {
    return {
      ...state,
      alert: {
        ...props,
        type: "warning"
      }
    }
  }),
  on(showErrorAlert, (state, props) => {
    return {
      ...state,
      alert: {
        ...props,
        type: "danger"
      }
    }
  }),
  on(showInfoAlert, (state, props) => {
    return {
      ...state,
      alert: {
        ...props,
        type: "info"
      }
    }
  }), on(clearAlert, (state) => {
    return {
      ...state,
      alert: null
    }
  })
);

export function alertReducer(state, action) {
  return _alertReducer(state, action);
}
