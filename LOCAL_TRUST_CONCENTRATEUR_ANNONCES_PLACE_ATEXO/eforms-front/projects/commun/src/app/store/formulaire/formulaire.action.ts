import {createAction, props} from '@ngrx/store';

export const setFormulaire = createAction('[FORMULAIRE] Set Formulaire', props<{
  formulaire: any
}>());
export const switchExtended = createAction('[FORMULAIRE] Switch Extended Node', props<{
  id: string
}>());

export const setExtended = createAction('[FORMULAIRE] Set Extended Node', props<{
  id: string, extended: boolean
}>());
export const deleteExtended = createAction('[FORMULAIRE] Delete Extended Node', props<{
  id: string
}>());
