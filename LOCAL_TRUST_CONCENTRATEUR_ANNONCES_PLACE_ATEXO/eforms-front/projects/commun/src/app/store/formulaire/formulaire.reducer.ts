import {createReducer, on} from '@ngrx/store';
import {
  deleteExtended,
  setExtended,
  setFormulaire,
  switchExtended
} from '@shared-global/store/formulaire/formulaire.action';

export interface FormulaireState {

    formulaire: any
    extended: Map<string, boolean>
}


function getInitialState(): FormulaireState {
    return {
        formulaire: null,
        extended: new Map<string, boolean>()
    }
}


const initialState: FormulaireState = getInitialState();

const _formulaireReducer = createReducer(
    initialState
    , on(setFormulaire, (state, props) => {
        return {
            ...state,
            ...{formulaire: props.formulaire}
        }
    }), on(switchExtended, (state, props) => {
        let key = props.id;
        let extended = state.extended.get(key);
        if(extended === undefined){
            extended = true;
        }
        state.extended.set(key, !extended);
        return {
            ...state,
        }
    }), on(setExtended, (state, props) => {
        let key = props.id;
        let extended = state.extended.get(key);
        if (extended === undefined) {
            state.extended.set(key, props.extended);
        }
        return {
            ...state,
        }
  }), on(deleteExtended, (state, props) => {
    let key = props.id;
    let extended = state.extended.get(key);
    if (!!extended) {
      state.extended.delete(key);
    }
    return {
      ...state,
    };
    }),
);

export function formulaireReducer(state, action) {
    return _formulaireReducer(state, action);
}
