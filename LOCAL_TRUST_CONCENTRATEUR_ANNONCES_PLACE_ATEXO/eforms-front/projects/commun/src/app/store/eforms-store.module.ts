import {NgModule} from '@angular/core';
import {reducers} from './index';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {StoreRouterConnectingModule} from '@ngrx/router-store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {UserEffect} from '@shared-global/store/user/user.effect';
import {ToastEffect} from "@shared-global/store/toast/toast.effect";


@NgModule({
  imports: [
    StoreModule.forRoot(reducers),

    StoreRouterConnectingModule.forRoot({
      stateKey: 'router'
    }),

    EffectsModule.forRoot([
      UserEffect,
      ToastEffect

    ]),
    StoreDevtoolsModule.instrument({
      name: '[EFORMS-APP]',
      maxAge: 100
    })
  ]
})
export class EformsStoreModule {
}
