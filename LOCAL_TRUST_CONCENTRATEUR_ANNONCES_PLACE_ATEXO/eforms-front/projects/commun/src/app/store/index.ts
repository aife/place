import {ActionReducerMap} from '@ngrx/store';
import * as fromUserReducer from '@shared-global/store/user/user.reducer';
import * as fromAlertReducer from '@shared-global/store/alert/alert.reducer';
import * as fromFormaulaireReducer from '@shared-global/store/formulaire/formulaire.reducer';

export interface State {
  userReducer: fromUserReducer.UserState;
  alertReducer: fromAlertReducer.AlertState;
  formaulaireReducer: fromFormaulaireReducer.FormulaireState;
}

export const reducers: ActionReducerMap<State> = {
  userReducer: fromUserReducer.userReducer,
  alertReducer: fromAlertReducer.alertReducer,
  formaulaireReducer: fromFormaulaireReducer.formulaireReducer
};
