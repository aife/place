import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {State} from "@shared-global/store"
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    accessToken!: string
    plateforme!: string
    organisme!: string

    constructor(public store: Store<State>) {
        this.store.select(state => state.userReducer.accessToken).subscribe(value => {
            this.accessToken = value;
        });
        this.store.select(state => state.userReducer.plateforme).subscribe(value => {
            this.plateforme = value;
        });
        this.store.select(state => state.userReducer.organisme).subscribe(value => {
            this.organisme = value;
        });


    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!!this.accessToken) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${this.accessToken}`
                },

            });
        }

        if (!!this.organisme) {
            request = request.clone({
                setHeaders: {
                    organisme: `${this.organisme}`
                },
                setParams: {
                    organisme: `${this.organisme}`
                }
            });
        }
        if (!!this.plateforme) {
            request = request.clone({
                setHeaders: {
                    plateforme: `${this.plateforme}`
                },
                setParams: {
                    idPlatform: `${this.plateforme}`

                }
            });
        }
        return next.handle(request);
    }
}
