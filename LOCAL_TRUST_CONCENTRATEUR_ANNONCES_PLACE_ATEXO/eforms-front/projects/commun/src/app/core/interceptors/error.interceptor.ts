import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {Store} from "@ngrx/store";
import {catchError} from "rxjs/operators";
import {Injectable} from "@angular/core";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {State} from "@shared-global/store"
import {logoutUser} from "@shared-global/store/user/user.action";
import {showErrorToast} from "@shared-global/store/toast/toast.action";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    bsModalRef!: BsModalRef;

    constructor(private modalService: BsModalService, private readonly store: Store<State>, private readonly routingInterne: RoutingInterneService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            const error = err.error.message || err.statusText;
            if ([401].indexOf(err.status) !== -1) {
                this.store.dispatch(logoutUser());
                this.routingInterne.navigateFromContexte(AppInternalPathEnum.LOGIN + "/" + AppInternalPathEnum.SESSION_EXPIRED);
            } else {
                this.store.dispatch(showErrorToast({
                    header: "error.header",
                    message: error
                }))
            }
            return throwError(error);
        }))
    }
}
