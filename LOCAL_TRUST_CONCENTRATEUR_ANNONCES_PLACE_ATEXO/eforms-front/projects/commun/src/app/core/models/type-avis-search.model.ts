import {
  DashboardSuiviTypeAvisSpecification,
  StatutTypeAvisAnnonceEnum
} from "@shared-global/core/models/api/eforms.api";

export class TypeAvisSearchModel implements DashboardSuiviTypeAvisSpecification {
  europeen: boolean;
  idConsultation: number[] = [];
  statuts: StatutTypeAvisAnnonceEnum[] = [];
  supports: string[] = [];
  sip: boolean;
  plateformes: string[] = [];
  dateMiseEnligneCalculeMax: Date;
  dateMiseEnligneCalculeMin: Date;
  dateEnvoiEuropeenPrevisionnelMax: Date;
  dateEnvoiNationalPrevisionnelMax: Date;
  dateModificationMax: Date;
  dateModificationMin: Date;
  organismes: string[] = [];
  references: string[] = [];
  offres: string[] = [];
  procedures: string[] = [];
  dateDlroMax: string;
  dateDlroMin: string;
  dateValidationMax: string;
  dateValidationMin: string;
  identifiantValidateurs: string[];
  services: string[];


  constructor(sip?: boolean, statut?: StatutTypeAvisAnnonceEnum[], offre?: string[]) {
    this.sip = sip;
    this.statuts = statut?.length ? statut : []
    this.offres = offre?.length ? offre : [];
  }



}
