class BreadcrumbLinkModel {
  name: string;
  isLink: boolean;
  link?: string;

}

export class BreadcrumbModel {

  mainlabel: string;
  legende?: string;
  links?: BreadcrumbLinkModel[];

}
