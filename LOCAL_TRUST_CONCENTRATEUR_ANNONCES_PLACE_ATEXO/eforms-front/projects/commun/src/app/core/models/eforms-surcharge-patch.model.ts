import {EformsSurchargeDTO, EformsSurchargePatch} from '@shared-global/core/models/api/eforms.api';

export class EformsSurchargePatchModel implements EformsSurchargePatch {
  constructor(surcharge?: EformsSurchargeDTO) {
    if (!!surcharge) {
      this.defaultValue = surcharge.defaultValue;
      this.dynamicListFrom = surcharge.dynamicListFrom;
      this.staticList = surcharge.staticList;
      this.hidden = surcharge.hidden;
      this.readonly = surcharge.readonly;
      this.obligatoire = surcharge.obligatoire;
      this.ordre = surcharge.ordre;
    }
  }

  defaultValue: string;
  dynamicListFrom: string;
  staticList: string;
  idNotice: string;
  hidden: boolean;
  readonly: boolean;
  obligatoire: boolean;
  ordre: number;


}
