export class PageableModel {
  public size: number = 25;
  public page: number = 0;
  public sortField = "dateModification";
  public asc = false;
}
