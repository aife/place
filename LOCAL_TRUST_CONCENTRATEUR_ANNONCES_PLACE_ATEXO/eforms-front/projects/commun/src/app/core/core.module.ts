import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NotConnectedGuard} from "@shared-global/core/guards/not-connected.guard";
import {AuthGuard} from "@shared-global/core/guards/auth.guard";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {ModalModule} from "ngx-bootstrap/modal";
import {TokenInterceptor} from "@shared-global/core/interceptors/token.interceptor";
import {ErrorInterceptor} from "@shared-global/core/interceptors/error.interceptor";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {AuthenticationService} from "@shared-global/core/services/agent/authentification.service";
import {EformsSdkService} from "@shared-global/core/services/eforms-sdk.service";
import {EformsNoticesService} from "@shared-global/core/services/eforms-notices.service";
import {AnnoncesTypeAvisService} from "@shared-global/core/services/annonces-type-avis.service";
import {SupportsService} from "@shared-global/core/services/supports.service";
import {AnnoncesService} from "@shared-global/core/services/annonces.service";
import {ReferentielsService} from "@shared-global/core/services/referentiels.service";
import {ThemesService} from "@shared-global/core/services/themes.service";
import {PlateformeConfigurationService} from "@shared-global/core/services/plateforme-configuration.service";


@NgModule({
  declarations: [],
  imports: [
    CommonModule, HttpClientModule, ModalModule.forRoot(),

  ],
  providers: [NotConnectedGuard, AuthGuard, RoutingInterneService, AnnoncesTypeAvisService, SupportsService, AnnoncesService,
    EformsSdkService, EformsNoticesService, ReferentielsService, ThemesService,
    AuthenticationService, PlateformeConfigurationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }, {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    }]
})
export class CoreModule {
}
