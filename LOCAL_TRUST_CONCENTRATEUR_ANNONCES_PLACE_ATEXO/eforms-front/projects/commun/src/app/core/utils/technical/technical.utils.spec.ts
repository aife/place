import {
    dotPath,
    isEmptyDotPath,
    isEmptyObject,
    isEmptyOrNil,
    isEmptyOrNilDotPath,
    isEmptyOrNilProp,
    isEmptyProp,
    isNilDotPath,
    isNilProp,
    isTruthyObject
} from '@core-eforms/utils/technical/technical.utils';

describe('TechnicalUtils', () => {
    describe('dotPath behavior', () => {
        it('should return value at path', () => {
            expect(dotPath('a.b', {a: {b: {}}})).toEqual({});
            expect(dotPath('a.b', {a: {b: 0}})).toBe(0);
            expect(dotPath('a.b.c', {a: {b: {c: 'test'}}})).toBe('test');
            expect(dotPath('a.b', {a: {b: undefined}})).toBe(undefined);
            expect(dotPath('a.b', {a: {b: null}})).toBe(null);
            expect(dotPath('a.b', {a: {b: [1]}})).toEqual([1]);
            expect(dotPath('a.b', {a: {b: false}})).toBe(false);
        });

        it('should undefined true if property does not exist', () => {
            expect(dotPath('a', {})).toBe(undefined);
        });

        it('should return undefined if path is wrong', () => {
            expect(dotPath('a.b', {a: {c: {}}})).toBe(undefined);
        });

        it('should return undefined if path is empty', () => {
            expect(dotPath('', {a: 5})).toBe(undefined);
        });

        it('should return undefined if object is null or undefined', () => {
            expect(dotPath('a', null)).toBe(undefined);
            expect(dotPath('a', undefined)).toBe(undefined);
        });
    });

    describe('isEmptyObject behavior', () => {
        it('should return true if object is null', () => {
            expect(isEmptyObject(null)).toBeTruthy();
        });

        it('should return true if object is undefined', () => {
            expect(isEmptyObject(undefined)).toBeTruthy();
        });

        it('should return true if object is attribute is null', () => {
            expect(isEmptyObject({key: null})).toBeTruthy();
        });

        it('should return true if object is empty', () => {
            expect(isEmptyObject({})).toBeTruthy();
        });
        it('should return true if object attribute is undefined', () => {
            expect(isEmptyObject({key: undefined})).toBeTruthy();
        });

        it('should return false if all attributes are true', () => {
            expect(isEmptyObject({key: true, key2: true})).toBeFalsy();
        });

        it('should return false if all attributes are false', () => {
            expect(isEmptyObject({key: false, key2: false})).toBeFalsy();
        });

        it('should return false if one attribute is false', () => {
            expect(isEmptyObject({key: true, key2: false})).toBeFalsy();
        });

        it('should return false if object is valid with a boolean and a string', () => {
            expect(isEmptyObject({key: true, key2: 'false'})).toBeFalsy();
        });

        it('should return false if object is not valid with a boolean and an undefined', () => {
            expect(isEmptyObject({key: false, key2: undefined})).toBeFalsy();
        });

        it('should return true if object is falsy', () => {
            expect(isEmptyObject(null)).toBeTruthy();
        });

        it('should return true if object is undefined', () => {
            expect(isEmptyObject(undefined)).toBeTruthy();
        });

        it('should return true if object is empty', () => {
            expect(isEmptyObject({})).toBeTruthy();
        });

        it('should return true if array is empty', () => {
            expect(isEmptyObject([])).toBeTruthy();
        });

        it('should return true if all properties are null', () => {
            expect(isEmptyObject({string: null, number: null})).toBeTruthy();
        });

        it('should return true if all properties are empty', () => {
            expect(isEmptyObject({string: '', array: []})).toBeTruthy();
        });

        it('should return true if all properties are empty or null', () => {
            expect(isEmptyObject({string: '', number: null, object: {}, array: []})).toBeTruthy();
        });

        it('should return false if one of the property is not empty', () => {
            expect(isEmptyObject({string: '', number: null, object: {}, array: [1]})).toBeFalsy();
        });

        it('should return false if one of the property is not null', () => {
            expect(isEmptyObject({string: '', number: 123, object: {}, array: []})).toBeFalsy();
        });
    });

    describe('isTruthyObject behavior', () => {
        it('should return false if object is null', () => {
            expect(isTruthyObject(null)).toBeFalsy();
        });

        it('should return false if object is undefined', () => {
            expect(isTruthyObject(undefined)).toBeFalsy();
        });

        it('should return false if object is attribute is null', () => {
            expect(isTruthyObject({key: null})).toBeFalsy();
        });

        it('should return true if object is empty', () => {
            expect(isTruthyObject({})).toBeTruthy();
        });
        it('should return false if object is attribute is undefined', () => {
            expect(isTruthyObject({key: undefined})).toBeFalsy();
        });

        it('should return true if  all attributes are true', () => {
            expect(isTruthyObject({key: true, key2: true})).toBeTruthy();
        });

        it('should return false if object if all attributes are false', () => {
            expect(isTruthyObject({key: false, key2: false})).toBeFalsy();
        });

        it('should return false if one attribute is false', () => {
            expect(isTruthyObject({key: true, key2: false})).toBeFalsy();
        });

        it('should return true if object is valid with a boolean and a string', () => {
            expect(isTruthyObject({key: true, key2: 'false'})).toBeTruthy();
        });

        it('should return false if object is not valid with a boolean and an undefined', () => {
            expect(isTruthyObject({key: false, key2: undefined})).toBeFalsy();
        });

        it('should return true if object has empty array', () => {
            expect(isTruthyObject({array: []})).toBeTruthy();
        });
    });

    describe('isNilProp behavior', () => {
        it('should return true if object is null', () => {
            expect(isNilProp('a', null)).toBeTruthy();
        });

        it('should return true if object is undefined', () => {
            expect(isNilProp('a', undefined)).toBeTruthy();
        });

        it('should return true if property does not exist', () => {
            expect(isNilProp('a', {})).toBeTruthy();
        });

        it('should return true if property is null', () => {
            expect(isNilProp('a', {a: null})).toBeTruthy();
        });

        it('should return true if property is undefined', () => {
            expect(isNilProp('a', {a: undefined})).toBeTruthy();
        });

        it('should return false if object is defined', () => {
            expect(isNilProp('a', {a: ''})).toBeFalsy();
            expect(isNilProp('a', {a: []})).toBeFalsy();
            expect(isNilProp('a', {a: 1})).toBeFalsy();
            expect(isNilProp('a', {a: 'test'})).toBeFalsy();
            expect(isNilProp('a', {a: {}})).toBeFalsy();
            expect(isNilProp('a', {a: false})).toBeFalsy();
            expect(isNilProp('a', {a: new Date()})).toBeFalsy();
        });
    });

    describe('isNilDotPath behavior', () => {
        it('should return true if object is null', () => {
            expect(isNilDotPath('a', null)).toBeTruthy();
        });

        it('should return true if object is undefined', () => {
            expect(isNilDotPath('a', undefined)).toBeTruthy();
        });

        it('should return true if property does not exist', () => {
            expect(isNilDotPath('a', {})).toBeTruthy();
        });

        it('should return true if path is wrong', () => {
            expect(isNilDotPath('a.b', {a: {c: {}}})).toBeTruthy();
        });

        it('should return false if path is correct and property is not null or undefined', () => {
            expect(isNilDotPath('a.b', {a: {b: {}}})).toBeFalsy();
        });

        it('should return true if property is null', () => {
            expect(isNilDotPath('a.b', {a: {b: null}})).toBeTruthy();
        });

        it('should return true if property is undefined', () => {
            expect(isNilDotPath('a.b', {a: {b: undefined}})).toBeTruthy();
        });

        it('should return false if object is defined', () => {
            expect(isNilDotPath('a.b', {a: {b: ''}})).toBeFalsy();
            expect(isNilDotPath('a.b', {a: {b: []}})).toBeFalsy();
            expect(isNilDotPath('a.b', {a: {b: 1}})).toBeFalsy();
            expect(isNilDotPath('a.b', {a: {b: 'test'}})).toBeFalsy();
            expect(isNilDotPath('a.b', {a: {b: {}}})).toBeFalsy();
            expect(isNilDotPath('a.b', {a: {b: false}})).toBeFalsy();
            expect(isNilDotPath('a.b', {a: {b: new Date()}})).toBeFalsy();
        });
    });

    describe('isEmptyProp behavior', () => {
        it('should return false if object is null', () => {
            expect(isEmptyProp('a', null)).toBeFalsy();
        });

        it('should return false if object is undefined', () => {
            expect(isEmptyProp('a', undefined)).toBeFalsy();
        });

        it('should return false if property does not exist', () => {
            expect(isEmptyProp('a', {})).toBeFalsy();
        });

        it('should return false if property is not empty', () => {
            expect(isEmptyProp('a', {a: [1]})).toBeFalsy();
            expect(isEmptyProp('a', {a: 1})).toBeFalsy();
            expect(isEmptyProp('a', {a: 'test'})).toBeFalsy();
            expect(isEmptyProp('a', {a: {b: 1}})).toBeFalsy();
            expect(isEmptyProp('a', {a: false})).toBeFalsy();
            expect(isEmptyProp('a', {a: new Date()})).toBeFalsy();
            expect(isEmptyProp('a', {a: null})).toBeFalsy();
            expect(isEmptyProp('a', {a: undefined})).toBeFalsy();
        });
    });

    describe('isEmptyDotPath behavior', () => {
        it('should return false if object is null', () => {
            expect(isEmptyDotPath('a', null)).toBeFalsy();
        });

        it('should return false if object is undefined', () => {
            expect(isEmptyDotPath('a', undefined)).toBeFalsy();
        });

        it('should return false if property does not exist', () => {
            expect(isEmptyDotPath('a', {})).toBeFalsy();
        });

        it('should return false if path is wrong', () => {
            expect(isEmptyDotPath('a.b', {a: {c: 1}})).toBeFalsy();
        });

        it('should return true if path is correct and property is empty', () => {
            expect(isEmptyDotPath('a.b', {a: {b: {}}})).toBeTruthy();
            expect(isEmptyDotPath('a.b', {a: {b: ''}})).toBeTruthy();
            expect(isEmptyDotPath('a.b', {a: {b: []}})).toBeTruthy();
        });

        it('should return false if property is not empty', () => {
            expect(isEmptyDotPath('a.b', {a: {b: [1]}})).toBeFalsy();
            expect(isEmptyDotPath('a.b', {a: {b: 0}})).toBeFalsy();
            expect(isEmptyDotPath('a.b', {a: {b: 'test'}})).toBeFalsy();
            expect(isEmptyDotPath('a.b', {a: {b: false}})).toBeFalsy();
            expect(isEmptyDotPath('a.b', {a: {b: new Date()}})).toBeFalsy();
            expect(isEmptyDotPath('a.b', {a: {b: null}})).toBeFalsy();
            expect(isEmptyDotPath('a.b', {a: {b: undefined}})).toBeFalsy();
        });
    });

    describe('isEmptyOrNil behavior', () => {
        it('should return true if object is null', () => {
            expect(isEmptyOrNil(null)).toBeTruthy();
        });

        it('should return true if object is undefined', () => {
            expect(isEmptyOrNil(undefined)).toBeTruthy();
        });

        it('should return true if object is empty', () => {
            expect(isEmptyOrNil({})).toBeTruthy();
            expect(isEmptyOrNil('')).toBeTruthy();
            expect(isEmptyOrNil([])).toBeTruthy();
        });

        it('should return false if object is defined', () => {
            expect(isEmptyOrNil([1])).toBeFalsy();
            expect(isEmptyOrNil(0)).toBeFalsy();
            expect(isEmptyOrNil('test')).toBeFalsy();
            expect(isEmptyOrNil(false)).toBeFalsy();
            expect(isEmptyOrNil(new Date())).toBeFalsy();
        });
    });

    describe('isEmptyOrNilProp behavior', () => {
        it('should return true if object is null', () => {
            expect(isEmptyOrNilProp('a', null)).toBeTruthy();
        });

        it('should return true if object is undefined', () => {
            expect(isEmptyOrNilProp('a', undefined)).toBeTruthy();
        });

        it('should return true if property does not exist', () => {
            expect(isEmptyOrNilProp('a', {})).toBeTruthy();
        });

        it('should return true if property is null', () => {
            expect(isEmptyOrNilProp('a', {a: null})).toBeTruthy();
        });

        it('should return true if property is undefined', () => {
            expect(isEmptyOrNilProp('a', {a: undefined})).toBeTruthy();
        });

        it('should return true if property is empty', () => {
            expect(isEmptyOrNilProp('a', {a: []})).toBeTruthy();
            expect(isEmptyOrNilProp('a', {a: {}})).toBeTruthy();
            expect(isEmptyOrNilProp('a', {a: ''})).toBeTruthy();
        });

        it('should return false if object is defined', () => {
            expect(isEmptyOrNilProp('a', {a: 0})).toBeFalsy();
            expect(isEmptyOrNilProp('a', {a: 'test'})).toBeFalsy();
            expect(isEmptyOrNilProp('a', {a: {test: 1}})).toBeFalsy();
            expect(isEmptyOrNilProp('a', {a: false})).toBeFalsy();
            expect(isEmptyOrNilProp('a', {a: new Date()})).toBeFalsy();
        });
    });

    describe('isEmptyOrNilDotPath behavior', () => {
        it('should return true if object is null', () => {
            expect(isEmptyOrNilDotPath('a', null)).toBeTruthy();
        });

        it('should return true if object is undefined', () => {
            expect(isEmptyOrNilDotPath('a', undefined)).toBeTruthy();
        });

        it('should return true if property does not exist', () => {
            expect(isEmptyOrNilDotPath('a', {})).toBeTruthy();
        });

        it('should return true if path is wrong', () => {
            expect(isEmptyOrNilDotPath('a.b', {a: {c: 1}})).toBeTruthy();
        });

        it('should return true if path is correct and property is empty', () => {
            expect(isEmptyOrNilDotPath('a.b', {a: {b: {}}})).toBeTruthy();
            expect(isEmptyOrNilDotPath('a.b', {a: {b: ''}})).toBeTruthy();
            expect(isEmptyOrNilDotPath('a.b', {a: {b: []}})).toBeTruthy();
            expect(isEmptyOrNilDotPath('a.b', {a: {b: undefined}})).toBeTruthy();
            expect(isEmptyOrNilDotPath('a.b', {a: {b: null}})).toBeTruthy();
        });

        it('should return false if object is defined', () => {
            expect(isEmptyOrNilDotPath('a.b', {a: {b: 0}})).toBeFalsy();
            expect(isEmptyOrNilDotPath('a.b', {a: {b: 'test'}})).toBeFalsy();
            expect(isEmptyOrNilDotPath('a.b', {a: {b: false}})).toBeFalsy();
            expect(isEmptyOrNilDotPath('a.b', {a: {b: new Date()}})).toBeFalsy();
        });
    });
});
