function downloadFileFromResponse(response: any) {
    if (response.data) {
        const filename = getFileName(response);
        if (!filename)
            return;
        const blob = new Blob([response.data],
            {type: response.headers['content-type']});
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", filename);
        document.body.appendChild(link);
        link.click();
        /* eslint-disable */
        if (!!link.parentNode)
            link.parentNode.removeChild(link)
    }
}
function getFileName(response: any) {
    let filename = null;
    const disposition = response.headers['content-disposition'];
    if (disposition && disposition.indexOf('attachment') !== -1) {
        const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        const matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1]) {
            filename = matches[1].replace(/['"]/g, '');
        }
    }
    return filename;
}
export default downloadFileFromResponse;