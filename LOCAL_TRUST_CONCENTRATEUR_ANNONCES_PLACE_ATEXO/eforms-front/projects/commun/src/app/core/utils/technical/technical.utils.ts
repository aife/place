import {all, either, identity, isEmpty, isNil, path, prop, split, useWith, values} from 'ramda';
import {
  FailedAssertRepresentation,
  SubNoticeMetadata,
  XmlStructureDetails
} from '@shared-global/core/models/api/eforms.api';
import {isArray} from 'lodash';
import {Base64} from 'js-base64';

/**
 * get the property of an object according to the path supplied
 * @returns undefined if the path is wrong or the object is null or undefined
 * Usage :  dotPath('a.b', {a:{b:'hello'}}) -> 'hello'
 *          dotPath('a.b', {a:{b:null}}) -> null
 *          dotPath('a.b', {a:{b:undefined}}) -> undefined
 *          dotPath('a.b', {a:{b:0}}) -> 0
 *          dotPath('a.b', {a:{b:[]}}) -> []
 *          dotPath('a.b', null) -> undefined
 * @returns undefined if the path doesn't exist or if the object is null or undefined
 * @param dottedPath the path of the object separated with dots
 * @param obj the object to check the path from
 */
export function dotPath(dottedPath: string, obj: object): any {
  return useWith(path, [split('.')])(dottedPath, obj);
}

/**
 * Check if an object is falsy or if all of its properties are empty  null or undefined
 * @returns true if the object is falsy or if all of its properties are empty  null or undefined
 * @param obj the object to be checked
 */
export function isEmptyObject(obj: object): boolean {
  return !obj || all(either(isNil, isEmpty), values(obj));
}

/**
 * Check if an object is truthy and if all of its properties are truthy
 * @returns false if the object is falsy or if one of its properties is falsy
 * @param obj the object to be checked
 */
export function isTruthyObject(obj: object): boolean {
  return !!obj && all(identity, values(obj));
}

/**
 * Check if a given property of an object is null or undefined
 * @returns true if the property doesn't exist or if the object is null or undefined
 * @param property the property to be checked
 * @param obj the object to check the property from
 */
export function isNilProp(property: string, obj: object): boolean {
  return isNil(prop(property, obj));
}

/**
 * Check if a property of an object is null or undefined according to the path supplied
 * Usage :  isNilDotPath('a.b', {a:{b:null}}) -> true
 *          isNilDotPath('a.b', {a:{b:undefined}}) -> true
 *          isNilDotPath('a.b', {a:{b:0}}) -> false
 *          isNilDotPath('a.b', {a:{b:[]}}) -> true
 *          isNilDotPath('a.b', null) -> true
 * @returns true if the path doesn't exist or if the object is null or undefined
 * @param dottedPath the path of the object separated with dots
 * @param obj the object to check the path from
 */
export function isNilDotPath(dottedPath: string, obj: object): boolean {
  return isNil(dotPath(dottedPath, obj));
}

/**
 * Check if a given property of an object is empty
 * @returns true if the given label is its type's empty label
 * @returns false if the property does not exist
 * @param property the property to be checked
 * @param obj the object to check the property from
 */
export function isEmptyProp(property: string, obj: object | Array<any>): boolean {
  return isEmpty(prop(property, obj));
}

/**
 * Check if a property of an object is empty according to the path supplied
 * Usage :  isEmptyDotPath('a.b', {a:{b:''}}) -> true
 *          isEmptyDotPath('a.b', {a:{b:[]}}) -> true
 *          isEmptyDotPath('a.b', {a:{b:{}}}) -> true
 *          isEmptyDotPath('a.b', {a:{b:null}}) -> false
 *          isEmptyDotPath('a.b', []) -> false
 * @returns true if the given label is its type's empty label
 * @returns false if the property does not exist
 * @param dottedPath the path of the object separated with dots
 * @param obj the object to check the path from
 */
export function isEmptyDotPath(dottedPath: string, obj: any): boolean {
  return isEmpty(dotPath(dottedPath, obj));
}

/**
 * Check if an object is empty, null or undefined
 * @returns true if the object is empty, null or undefined
 * @param obj the object to be checked
 */
export function isEmptyOrNil(obj: any): boolean {
  return either(isNil, isEmpty)(obj);
}

/**
 * Check if a given property of an object is empty, null or undefined
 * @returns true if the property doesn't exist or if the object is null or undefined
 * @returns true if the object is empty, null or undefined
 * @param property the property to be checked
 * @param obj the object to check the property from

 */
export function isEmptyOrNilProp(property: string, obj: object): boolean {
  return either(isNil, isEmpty)(prop(property, obj));
}

/**
 * Check if a property of an object is empty, null or undefined according to the path supplied
 * Usage :  isEmptyOrNilDotPath('a.b', {a:{b:''}}) -> true
 *          isEmptyOrNilDotPath('a.b', {a:{b:[]}}) -> true
 *          isEmptyOrNilDotPath('a.b', {a:{b:{}}}) -> true
 *          isEmptyOrNilDotPath('a.b', {a:{b:null}}) -> true
 *          isEmptyOrNilDotPath('a.b', {a:{b:1}}) -> true
 * @returns true if the path doesn't exist or if the object is null or undefined
 * @returns true if the object is empty, null or undefined
 * @param dottedPath the path of the object separated with dots
 * @param obj the object to check the path from
 */
export function isEmptyOrNilDotPath(dottedPath: string, obj: object): boolean {
  return either(isNil, isEmpty)(dotPath(dottedPath, obj));
}

export function getIdObject(itemGroup: any, total: number): object {
  let object = {};
  if (itemGroup._identifierFieldId) {
    object[itemGroup._identifierFieldId] = itemGroup._idScheme + "-" + getIndex(total);
  }
  return object;
}

function getIndex(number: number) {
  let string = String(number);
  while (string.length < 4) {
    string = '0' + string;
  }
  return string;
}

export function getNodeId(item: SubNoticeMetadata) {
  return !!item._identifierFieldId || item._repeatable === true ? item.nodeId : null;
}

export function isHidden(item: SubNoticeMetadata) {
  if (!item) return true;
  return (!item.surcharge && item.hidden) || (item.surcharge && item.surcharge.hidden === true) || item.forceHidden

}

export function verifyCondition(item: SubNoticeMetadata) {
  if (!item) return true;
  return (!item.surcharge && item.hidden) || (item.surcharge && item.surcharge.hidden === true) || item.forceHidden

}

export function isReadonly(item: SubNoticeMetadata) {
  if (!item) return true;
  return (!item.surcharge && item.readOnly) || (item.surcharge && item.surcharge.readonly === true)

}

export function buildObjectFromXmlStructure(event: { object: any; nodeIds: XmlStructureDetails[] }, index: number) {
  const nodeIds = event.nodeIds;
  let value = event.object;
  let i = 0;
  while (i <= index && index !== -1) {
    var obj = {};
    let xmlStructureDetail = nodeIds[i];
    let s = xmlStructureDetail.id;
    if (!value[s]) {
      obj[s] = xmlStructureDetail.repeatable && !isArray(value) ? [value] : value;
      value = obj;
    }
    i++;
  }
  let filter = nodeIds.filter((k, index1) => index1 > index || index === -1);
  return {object: value, nodeIds: filter};
}

export function getNode(formulaire: any, nodeId: string, type: string, index?: number) {
  if (!formulaire || typeof formulaire !== 'object' || (Array.isArray(formulaire) && index === undefined))
    return null;
  if (Array.isArray(formulaire) && index !== undefined) {
    return getNode(formulaire[index], nodeId, type);
  }
  let nodeObject = null;
  let keys = Object.keys(formulaire);
  if (keys.includes(nodeId)) {
    return formulaire[nodeId]
  }
  let i = 0;
  while (i < keys.length && !nodeObject) {
    nodeObject = getNode(formulaire[keys[i]], nodeId, type);
    i++;
  }
  return nodeObject;
}

export function mergeJsonRecuresevily(json1: any, json2: any) {
  if (!json1) return json2;
  if (!json2) return json1;
  for (let key in json2) {
    if (json1.hasOwnProperty(key)) {
      if (Array.isArray(json1[key]) && Array.isArray(json2[key])) {
        if (json1[key].length === json2[key].length) {
          //add all properties of json2[key] to json1[key]
          for (let i = 0; i < json1[key].length; i++) {
            if (typeof json1[key][i] === 'object' && typeof json2[key][i] === 'object') {
              mergeJsonRecuresevily(json1[key][i], json2[key][i]);
            } else {
              json1[key][i] = json2[key][i];
            }
          }
        } else {
          json1[key] = json2[key];
        }
      } else if (typeof json1[key] === 'object' && typeof json2[key] === 'object') {
        mergeJsonRecuresevily(json1[key], json2[key]);
      } else {
        json1[key] = json2[key];
      }
    } else {
      json1[key] = json2[key];
    }
  }
  return json1;
}

export function findAllValuesOfKeysInObject(value: any, fields: string[], list) {
  if (!value || !fields || fields.length === 0) {
    return;
  }
  Object.keys(value).forEach(key => {
    if (fields.includes(key)) {
      list.push(value[key]);
    }

    if (Array.isArray(value[key])) {
      value[key].forEach(value1 => {
        findAllValuesOfKeysInObject(value1, fields, list);
      })
    } else if (typeof value[key] === 'object') {
      findAllValuesOfKeysInObject(value[key], fields, list);
    }
  });
}

// Function to find the next ID
export function findNextID(list: string[], suffix): string {
  let nextID = suffix + "-0001";
  if (!list || list.length === 0) return nextID;

  list.forEach((item) => {
    const currentNumber = parseInt(item.split("-")[1], 10);
    const nextNumber = parseInt(nextID.split("-")[1], 10);

    if (currentNumber >= nextNumber) {
      nextID = `${suffix}-${(currentNumber + 1).toString().padStart(4, "0")}`;
    }
  });

  return nextID;
}

export function equalsJson(a: any, b: any): boolean {
  const base64a = a !== null && a !== undefined ? Base64.toBase64(JSON.stringify(a)) : null;
  const base64b = b !== null && b !== undefined ? Base64.toBase64(JSON.stringify(b)) : null;
  return base64a === base64b;
}
export function getFailedAssertFromElements(errorList: FailedAssertRepresentation[], field: SubNoticeMetadata, index?: number): FailedAssertRepresentation[] {
  if (!errorList) {
    return [];
  }

  let elements = field.elements;
  let searchElement = index + '';
  if (elements && elements.length > 0) {

    return errorList.filter(assertion => assertion.elements && elements && assertion.elements.length >= elements.length)
      .map(assertion => {
        let match = true;
        elements.forEach((value1, index1) => {
          if (value1.elementName !== assertion.elements[index1].elementName) {
            match = false;
          } else if (value1.properties?.length > 0) {
            value1.properties.filter(prop => !prop.startsWith('cbc:ID')).forEach(property => {
              if (!assertion.elements[index1].properties.includes(property)) {
                match = false;
              }
            })
          }
        });
        let xmlElement = assertion.elements[elements.length - 1];
        if (match && index !== null && index !== undefined && index >= 0 && xmlElement?.properties) {
          match = xmlElement.properties.includes(searchElement);
        }

        return match ? assertion : null;
      }).filter(assertion => !!assertion);
  }
  let subElementsMap = field.subElements;
  if (subElementsMap) {
    const result = [];
    Object.keys(subElementsMap).forEach((key) => {
      let subElements = subElementsMap[key];
      if (subElements.length === 0) return;

      errorList.filter(assertion => assertion.elements && subElements && assertion.elements.length >= subElements.length)
        .forEach(assertion => {
          let match = true;
          subElements.forEach((value1, index1) => {
            if (value1.elementName !== assertion.elements[index1].elementName) {
              match = false;
            } else if (value1.properties) {
              value1.properties.filter(prop => !prop.startsWith('cbc:ID')).forEach(property => {
                if (!assertion.elements[index1].properties.includes(property)) {
                  match = false;
                }
              })
            }
          });
          let xmlElement = assertion.elements[subElements.length - 1];
          if (match && index !== null && index !== undefined && index >= 0 && xmlElement?.properties) {
            match = xmlElement.properties.includes(searchElement);
          }

          if (match && !result.includes(assertion)) {
            result.push(assertion);
          }
        });
    });
    return result;

  }
  return [];

}
