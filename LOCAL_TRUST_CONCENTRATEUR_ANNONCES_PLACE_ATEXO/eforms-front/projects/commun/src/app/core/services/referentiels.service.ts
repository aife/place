import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {AgentDTO, OrganismeDTO, ReferentielDTO} from "@shared-global/core/models/api/eforms.api";

@Injectable()
export class ReferentielsService {
  private prefix = '/concentrateur-annonces/rest/v2/referentiels';

  constructor(protected http: HttpClient) {
  }


  getProcedures(): Observable<ReferentielDTO[]> {
    const endpoint = `${this.prefix}/procedures`;
    return this.http.get<ReferentielDTO[]>(endpoint)
  }

  getOrganismes(): Observable<OrganismeDTO[]> {
    const endpoint = `${this.prefix}/organismes`;
    return this.http.get<OrganismeDTO[]>(endpoint)
  }
  getAgents(): Observable<AgentDTO[]> {
    const endpoint = `${this.prefix}/agents`;
    return this.http.get<AgentDTO[]>(endpoint)
  }


}
