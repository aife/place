import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {PlateformeConfigurationDTO, SuiviTypeAvisPubDTO} from "@shared-global/core/models/api/eforms.api";

@Injectable()
export class PlateformeConfigurationService {
  private prefix = '/concentrateur-annonces/rest/v2/plateformes-configuration';

  constructor(protected http: HttpClient) {
  }


  getAll(): Observable<PlateformeConfigurationDTO[]> {
    const endpoint = `${this.prefix}`;
    return this.http.get<PlateformeConfigurationDTO[]>(endpoint)
  }

  get(id: string): Observable<PlateformeConfigurationDTO> {
    const endpoint = `${this.prefix}/${id}`;
    return this.http.get<PlateformeConfigurationDTO>(endpoint)
  }

  modify(plateformeConfiguration: PlateformeConfigurationDTO) {
    const endpoint = `${this.prefix}/${plateformeConfiguration.id}`;
    return this.http.put<PlateformeConfigurationDTO>(endpoint, plateformeConfiguration)
  }

  importer(id: number, file: File): Observable<SuiviTypeAvisPubDTO> {
    const formData = new FormData();
    formData.append('xlsx', file);
    const endpoint = `${this.prefix}/${id}/import-xlsx`;
    return this.http.patch<SuiviTypeAvisPubDTO>(endpoint, formData);
  }

  importerAtexo(file: File): Observable<SuiviTypeAvisPubDTO> {
    const formData = new FormData();
    formData.append('xlsx', file);
    const endpoint = `${this.prefix}/import-xlsx`;
    return this.http.patch<SuiviTypeAvisPubDTO>(endpoint, formData);
  }

  exporter(id: number): Observable<SuiviTypeAvisPubDTO> {
    const endpoint = `${this.prefix}/${id}/export-xlsx`;
    // @ts-ignore
    return this.http.get<any>(endpoint, {responseType: 'blob'});
  }

  exporterAtexo(): Observable<any> {
    const endpoint = `${this.prefix}/export-xlsx`;
    // @ts-ignore
    return this.http.get<any>(endpoint, {responseType: 'blob'});
  }
}
