import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {OffreDTO, SupportDTO} from '@shared-global/core/models/api/eforms.api';

@Injectable()
export class SupportsService {
  private prefix = '/concentrateur-annonces/rest/v2/supports';

  constructor(protected http: HttpClient) {
  }


  getSupports(procedures?: string, organismes?: string, europeen?: boolean, national?: boolean): Observable<SupportDTO[]> {
    let params = {};
    let blocs = [];
    if (europeen !== null && europeen !== undefined) {
      params['europeen'] = europeen;
    }
    if (national !== null && national !== undefined) {
      params['national'] = national;
      if (national) {
        blocs.push('JAL');
      }
    }
    if (procedures !== null && procedures !== undefined) {
      params['typeProcedures'] = procedures;
    }
    if (organismes !== null && organismes !== undefined) {
      params['organismes'] = organismes;
    }
    if (blocs.length > 0) {
      params['blocs'] = blocs;
    }
    const endpoint = `${this.prefix}`;
    return this.http.get<SupportDTO[]>(endpoint, {params});
  }

  getOffres(procedures?: string, organismes?: string, europeen?: boolean, national?: boolean, blocs?: string[]): Observable<OffreDTO[]> {
    let params = {};
    if (!blocs) {
      blocs = [];
    }
    if (europeen !== null && europeen !== undefined) {
      params['europeen'] = europeen;
    }
    if (national !== null && national !== undefined) {
      params['national'] = national;
      if (national) {
        blocs.push('JAL');
      }
    }

    if (procedures !== null && procedures !== undefined) {
      params['typeProcedures'] = procedures;
    }
    if (organismes !== null && organismes !== undefined) {
      params['organismes'] = organismes;
    }
    if (blocs.length > 0) {
      params['blocs'] = blocs;
    }
    const endpoint = `${this.prefix}/plateforme/offres`;
    return this.http.get<OffreDTO[]>(endpoint, {params});
  }


}
