import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {
  ConfigurationConsultationDTO,
  StatutSuiviAnnonceEnum,
  SuiviAnnonceDTO,
  SuiviDTO
} from "@shared-global/core/models/api/eforms.api";

@Injectable()
export class AnnoncesService {
    private prefix = '/concentrateur-annonces/rest/v2/annonces';

    constructor(protected http: HttpClient) {
    }


    pdf(annonce: SuiviAnnonceDTO): Observable<any> {
        const endpoint = `${this.prefix}/${annonce.id}/pdf?forAgent=true`;
        // @ts-ignore
        return this.http.get<any>(endpoint, {responseType: "blob", observe: 'response'})
    }


    getRedirection(id: number): Observable<SuiviDTO> {
        const endpoint = `${this.prefix}/${id}/redirection`;

        // @ts-ignore
        return this.http.get<SuiviDTO>(endpoint)
    }

    modifyStatut(id: number, statut: StatutSuiviAnnonceEnum): Observable<SuiviAnnonceDTO> {
        return this.http.patch<SuiviAnnonceDTO>(`${this.prefix}/${id}`, {statut});
    }

    getConfiguration(id: number): Observable<ConfigurationConsultationDTO> {
        let params = {};
        return this.http.get<ConfigurationConsultationDTO>(`${this.prefix}/configuration/${id}`, {params});
    }
}
