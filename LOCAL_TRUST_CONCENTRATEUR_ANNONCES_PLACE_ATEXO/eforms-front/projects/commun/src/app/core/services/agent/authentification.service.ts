import {Injectable} from '@angular/core';

@Injectable()
export class AuthenticationService {


    async logout() {
        // remove user from local storage to log user out
        sessionStorage.removeItem('eformsAccessToken');
        sessionStorage.removeItem("eformsPlateforme");
        sessionStorage.removeItem("eformsOrganisme");
        return true;
    }
}
