import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ThemesService {


  private readonly BASE_PATH = '/concentrateur-annonces/rest/v2/themes';

  constructor(public readonly http: HttpClient) {
  }

  getRemoteCss(): Promise<string> {
    const apiUrl = this.BASE_PATH + '/css/mpe-new-client.css';
    return this.http.get(apiUrl, {responseType: 'text'}).toPromise();
  }
}
