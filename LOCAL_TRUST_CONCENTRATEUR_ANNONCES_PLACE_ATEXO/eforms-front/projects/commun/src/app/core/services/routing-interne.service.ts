import {Router} from "@angular/router";
import {Inject, Injectable} from "@angular/core";
import {APP_BASE_HREF, Location} from "@angular/common";


@Injectable()
export class RoutingInterneService {
    constructor(@Inject(APP_BASE_HREF) public baseHref: string, public readonly router: Router, private location: Location) {

    }

    navigateFromContexte(path: string, queryParams?: any, state?: any,) {
        console.log(path, queryParams, this.baseHref)
        switch (this.baseHref) {
            case '/web-component-eforms':
                this.router.navigate([{outlets: {eforms: path}}], {
                    queryParams,
                    state,
                    skipLocationChange: true
                });
                window.dispatchEvent(new CustomEvent("change-route-interne", {
                    detail: {path},
                    composed: true // Like this
                }))

                return;
            default:
                this.router.navigate([path], {queryParams, state});
        }
    }

    back() {
        this.location.back();
    }
}
