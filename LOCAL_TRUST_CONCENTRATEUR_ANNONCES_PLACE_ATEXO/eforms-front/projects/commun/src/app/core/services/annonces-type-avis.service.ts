import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {
  AnnonceStatistique,
  AvisValidationPatch,
  DashboardSuiviTypeAvisSpecification,
  EditionRequest,
  PageDTO,
  RevisionDTO,
  SuiviTypeAvisPubAdd,
  SuiviTypeAvisPubDTO,
  SuiviTypeAvisPubPatch,
  SuiviTypeAvisPubPieceJointeAssoDTO,
  SuiviTypeAvisPubRevisionDTO
} from '@shared-global/core/models/api/eforms.api';
import {PageableModel} from '@shared-global/core/models/pageable.model';
import {MailUpdateModel} from '../models/mail-update';

@Injectable()
export class AnnoncesTypeAvisService {
  private prefix = '/concentrateur-annonces/rest/v2/type-avis-annonces';

  constructor(protected http: HttpClient) {
  }


  getNotices(filtre: DashboardSuiviTypeAvisSpecification, pageable: PageableModel): Observable<PageDTO<SuiviTypeAvisPubDTO>> {
    const endpoint = `${this.prefix}`;
    let order: string;
    if (pageable.asc == true) {
      order = "ASC"
    } else if (pageable.asc == false) {
      order = "DESC"
    }
    let sort = pageable.sortField + "," + order
    let params: any = {page: pageable.page, size: pageable.size, sort, ...this.getParams(filtre)};

    return this.http.get<PageDTO<SuiviTypeAvisPubDTO>>(endpoint, {
      params: params
    })
  }

  getConsultationNotices(idConsultation: number): Observable<Array<SuiviTypeAvisPubDTO>> {
    const endpoint = `${this.prefix}/consultation/${idConsultation}`;
    return this.http.get<Array<SuiviTypeAvisPubDTO>>(endpoint)
  }


  validate(id: number, validation: string, valid: AvisValidationPatch) {
    const endpoint = `${this.prefix}/${id}/validation-${validation}`;
    return this.http.patch<SuiviTypeAvisPubDTO>(endpoint, valid)
  }

  invalidateEco(id: number, valid: AvisValidationPatch) {
    const endpoint = `${this.prefix}/${id}/invalidation-eco`;
    return this.http.patch<SuiviTypeAvisPubDTO>(endpoint, valid);
  }

  getParams(filter: any) {
    let params: any = {};
    if (!filter)
      return params;
    console.log(filter)

    Object.keys(filter).filter(key => filter[key] !== null && filter[key] !== undefined)
      .forEach(key => {
        params[key] = filter[key]
      })
    return params;
  }

  exportNotices(filtre: DashboardSuiviTypeAvisSpecification): Observable<any> {
    const endpoint = `${this.prefix}/export`;
    let params: any = {...this.getParams(filtre)};

    // @ts-ignore
    return this.http.get<any>(endpoint, {responseType: "blob", params})
  }

  statistiques(sip?: boolean): Observable<AnnonceStatistique[]> {
    const endpoint = `${this.prefix}/statistiques`;
    // @ts-ignore
    return sip !== null && sip !== undefined ? this.http.get<AnnonceStatistique[]>(endpoint, {params: {sip: sip}}) : this.http.get<AnnonceStatistique[]>(endpoint);
  }


  exportStatistiques(sip?: boolean) {
    const endpoint = `${this.prefix}/export-statistiques`;
    return sip !== null && sip !== undefined ? this.http.get<AnnonceStatistique[]>(endpoint, {
      params: {sip: sip},
      // @ts-ignore
      responseType: 'blob'
      // @ts-ignore
    }) : this.http.get<AnnonceStatistique[]>(endpoint, {responseType: 'blob'});
  }

  addAnnonce(avis: SuiviTypeAvisPubAdd): Observable<SuiviTypeAvisPubDTO> {
    const endpoint = `${this.prefix}`;
    return this.http.post<SuiviTypeAvisPubDTO>(endpoint, avis)
  }

  patchAnnonce(id: number, avis: SuiviTypeAvisPubPatch, idConsultation: number): Observable<SuiviTypeAvisPubDTO> {
    const endpoint = `${this.prefix}/${id}?idConsultation=${idConsultation}`;
    return this.http.patch<SuiviTypeAvisPubDTO>(endpoint, avis)
  }

  reprendre(id: number, idConsultation: number): Observable<SuiviTypeAvisPubDTO> {
    const endpoint = `${this.prefix}/${id}/reprendre?idConsultation=${idConsultation}`;
    return this.http.patch<SuiviTypeAvisPubDTO>(endpoint, null)
  }

  change(id: number, idConsultation: number): Observable<SuiviTypeAvisPubDTO> {
    const endpoint = `${this.prefix}/${id}/change?idConsultation=${idConsultation}`;
    return this.http.patch<SuiviTypeAvisPubDTO>(endpoint, null)
  }

  getAnnonce(id: number, idConsultation: number): Observable<SuiviTypeAvisPubDTO> {
    const endpoint = `${this.prefix}/${id}?idConsultation=${idConsultation}`;
    return this.http.get<SuiviTypeAvisPubDTO>(endpoint)

  }

  getAnnonceHistorique(id: number, idConsultation: number): Observable<RevisionDTO<SuiviTypeAvisPubRevisionDTO>[]> {
    const endpoint = `${this.prefix}/${id}/historiques?idConsultation=${idConsultation}`;
    return this.http.get<RevisionDTO<SuiviTypeAvisPubRevisionDTO>[]>(endpoint)

  }

  deleteAnnonce(id: number, idConsultation: number): Observable<boolean> {
    const endpoint = `${this.prefix}/${id}?idConsultation=${idConsultation}`;
    return this.http.delete<boolean>(endpoint)
  }

  editAnnonce(id: number, idConsultation: number): Observable<EditionRequest> {
    const endpoint = `${this.prefix}/${id}/editeur-en-ligne?idConsultation=${idConsultation}`;
    return this.http.get<EditionRequest>(endpoint)
  }

  demanderValidation(id: number, idConsultation: number) {
    const endpoint = `${this.prefix}/${id}/demande-validation?idConsultation=${idConsultation}`;
    return this.http.patch<SuiviTypeAvisPubDTO>(endpoint, null)
  }

  getDocuments(id: number, idConsultation): Observable<Array<SuiviTypeAvisPubPieceJointeAssoDTO>> {
    const endpoint = `${this.prefix}/${id}/documents?idConsultation=${idConsultation}`;
    return this.http.get<Array<SuiviTypeAvisPubPieceJointeAssoDTO>>(endpoint);
  }

  importDocument(id: number, file: File, idConsultation): Observable<SuiviTypeAvisPubPieceJointeAssoDTO> {
    const formData = new FormData();
    formData.append('document', file);
    const endpoint = `${this.prefix}/${id}/document-import?idConsultation=${idConsultation}`;
    return this.http.post<SuiviTypeAvisPubPieceJointeAssoDTO>(endpoint, formData);
  }

  importAvisExterne(id: number, file: File, idConsultation): Observable<SuiviTypeAvisPubDTO> {
    const formData = new FormData();
    formData.append('document', file);
    const endpoint = `${this.prefix}/${id}/avis-externe-import?idConsultation=${idConsultation}`;
    return this.http.post<SuiviTypeAvisPubDTO>(endpoint, formData);
  }

  downloadFile(id, idPieceJointe, idConsultation): Observable<any> {
    const endpoint = `${this.prefix}/${id}/download/${idPieceJointe}?idConsultation=${idConsultation}`;
    return this.http.get(endpoint, {responseType: 'blob'});
  }

  deletePieceJointe(id: number, idAsso: number, idConsultation): Observable<SuiviTypeAvisPubDTO> {
    const endpoint = `${this.prefix}/${id}/piece-jointe/${idAsso}?idConsultation=${idConsultation}`;
    return this.http.delete<SuiviTypeAvisPubDTO>(endpoint);
  }

  updateMail(id: number, update: MailUpdateModel, idConsultation): Observable<SuiviTypeAvisPubDTO> {
    const endpoint = `${this.prefix}/${id}/mail?idConsultation=${idConsultation}`;
    return this.http.patch<SuiviTypeAvisPubDTO>(endpoint, update)
  }

  deleteAvisExterne(id: number, idConsultation): Observable<SuiviTypeAvisPubDTO> {
    const endpoint = `${this.prefix}/${id}/avis-externe?idConsultation=${idConsultation}`;
    return this.http.delete<SuiviTypeAvisPubDTO>(endpoint);
  }

  pdfPortail(id: number, idConsultation): Observable<any> {
    const endpoint = `${this.prefix}/${id}/pdf-portail?idConsultation=${idConsultation}`;

    // @ts-ignore
    return this.http.get<any>(endpoint, {responseType: "blob"})
  }

}
