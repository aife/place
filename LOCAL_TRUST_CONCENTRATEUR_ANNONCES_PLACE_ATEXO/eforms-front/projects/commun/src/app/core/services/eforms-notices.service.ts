import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {EFormsFormulaire, SubNotice, ValidationResult} from "@shared-global/core/models/api/eforms.api";

@Injectable()
export class EformsNoticesService {
  private prefix = '/concentrateur-annonces/rest/v2/eforms-formulaires';

  constructor(protected http: HttpClient) {
  }


  getNoticeSchema(id: number): Observable<SubNotice> {
    const endpoint = `${this.prefix}/${id}/structure`;
    return this.http.get<SubNotice>(endpoint)
  }


  saveNotice(id: number, body: any): Observable<EFormsFormulaire> {
    const endpoint = `${this.prefix}/${id}`;
    return this.http.put<EFormsFormulaire>(endpoint, body)
  }

  validateNotice(id: number): Observable<EFormsFormulaire> {
    const endpoint = `${this.prefix}/${id}/validate`;
    return this.http.patch<EFormsFormulaire>(endpoint, null)
  }

  changeOfficialLang(id: number, lang: string): Observable<EFormsFormulaire> {
    const endpoint = `${this.prefix}/${id}/lang`;
    return this.http.patch<EFormsFormulaire>(endpoint, {lang})
  }

  verifyNotice(id: number, body: any): Observable<ValidationResult> {
    const endpoint = `${this.prefix}/${id}/verify`;
    return this.http.patch<ValidationResult>(endpoint, body)
  }

  submitNotice(id: number, body: any): Observable<any> {
    const endpoint = `${this.prefix}/${id}/submit`;
    return this.http.patch<any>(endpoint, body)
  }

  modifierNotice(id: number): Observable<any> {
    const endpoint = `${this.prefix}/${id}/change`;
    return this.http.patch<any>(endpoint, null)
  }

  reprendre(id: number): Observable<any> {
    const endpoint = `${this.prefix}/${id}/reprendre`;
    return this.http.patch<any>(endpoint, null)
  }

  stopPublication(id: number): Observable<any> {
    const endpoint = `${this.prefix}/${id}/stop-publication`;
    return this.http.delete<any>(endpoint)
  }


  getNotice(id: number): Observable<EFormsFormulaire> {
    const endpoint = `${this.prefix}/${id}`;
    return this.http.get<EFormsFormulaire>(endpoint)
  }

  getNotices(onlyOrganisme: boolean): Observable<EFormsFormulaire[]> {
    const endpoint = `${this.prefix}`;
    return this.http.get<EFormsFormulaire[]>(endpoint, {
      params: {onlyOrganisme}
    })
  }


  pdf(id: number): Observable<any> {
    const endpoint = `${this.prefix}/${id}/pdf`;

    // @ts-ignore
    return this.http.get<any>(endpoint, {responseType: "blob"})
  }

}
