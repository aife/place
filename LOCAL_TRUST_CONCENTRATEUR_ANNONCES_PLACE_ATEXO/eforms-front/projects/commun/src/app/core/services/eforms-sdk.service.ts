import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {
  EformsSurchargeDTO,
  EformsSurchargePatch,
  FieldDetails,
  XmlStructureDetails
} from '@shared-global/core/models/api/eforms.api';

@Injectable()
export class EformsSdkService {
  private prefix = '/concentrateur-annonces/rest/v2/eforms-sdk';

  constructor(protected http: HttpClient) {
  }


  getField(id: string, version: string): Observable<FieldDetails> {
    const endpoint = `${this.prefix}/${version}/fields/${id}`;
    return this.http.get<FieldDetails>(endpoint);
  }

  getNodesId(id: string, version: string): Observable<XmlStructureDetails[]> {
    const endpoint = `${this.prefix}/${version}/nodes/${id}`;
    return this.http.get<XmlStructureDetails[]>(endpoint);
  }

  getCodeById(id: string, version: string, lang: string, search: string): Observable<Array<string>> {
    const endpoint = `${this.prefix}/${version}/codes/${id}`;
    let params: any = {lang};
    if (search) {
      params = {...params, search};
    }
    return this.http.get<Array<string>>(endpoint, {params});
  }

  getFieldsOptions(id: string, version: string, search: string, lang: string): Observable<Array<string>> {
    const endpoint = `${this.prefix}/${version}/${id}/options`;
    let params: any = {lang};
    if (search) {
      params = {...params, search};
    }
    return this.http.get<Array<string>>(endpoint, {params});
  }

  getSdkI18n(selectedLang: string): Observable<any> {
    const endpoint = `${this.prefix}/sdk-i18n/${selectedLang}`;
    return this.http.get<any>(endpoint);
  }

  getSurchageI18n(selectedLang: string): Observable<any> {
    const endpoint = `${this.prefix}/surcharge-i18n/${selectedLang}`;
    return this.http.get<any>(endpoint);
  }

  getI18n(selectedLang: string): Observable<any> {
    const endpoint = `${this.prefix}/i18n/${selectedLang}`;
    return this.http.get<any>(endpoint);
  }

  getStructure(idPlateforme: string, idNotice: string) {
    let endpoint = '';
    if (idPlateforme)
      endpoint = `${this.prefix}/${idPlateforme}/${idNotice}/structure`;
    else {
      endpoint = `${this.prefix}/${idNotice}/structure`;
    }
    return this.http.get<any>(endpoint);
  }

  surcharger(idPlateforme: string, id: string, patch: EformsSurchargePatch) {
    let endpoint = '';
    if (idPlateforme)
      endpoint = `${this.prefix}/fields/${idPlateforme}/surcharge/${id}`;
    else
      endpoint = `${this.prefix}/fields/surcharge/${id}`;
    return this.http.patch<EformsSurchargeDTO>(endpoint, patch);
  }
}
