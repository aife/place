import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Store} from '@ngrx/store';
import {State} from "@shared-global/store"
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";

@Injectable()
export class AuthGuard implements CanActivate {
  accessToken!: string;

  constructor(private router: RoutingInterneService, private readonly store: Store<State>,) {
    this.store.select(state => state.userReducer.accessToken).subscribe(value => this.accessToken = value);
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.accessToken) {
      return true;
    }
    this.router.navigateFromContexte(AppInternalPathEnum.LOGIN);
    return false;
  }
}
