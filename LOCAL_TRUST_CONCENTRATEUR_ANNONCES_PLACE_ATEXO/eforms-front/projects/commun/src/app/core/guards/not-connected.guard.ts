import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Store} from '@ngrx/store';
import {State} from "@shared-global/store"
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";

@Injectable()
export class NotConnectedGuard implements CanActivate {
  connected = false;
  defaultPage = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.ACCUEIL;

  constructor(private router: RoutingInterneService, private readonly store: Store<State>) {
    this.store.select(state => state.userReducer.connected).subscribe(value => this.connected = value);
    this.store.select(state => state.userReducer.defaultPage).subscribe(value => this.defaultPage = value);
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.connected) {
      this.router.navigateFromContexte(this.defaultPage);
    }
    return !this.connected;
  }
}
