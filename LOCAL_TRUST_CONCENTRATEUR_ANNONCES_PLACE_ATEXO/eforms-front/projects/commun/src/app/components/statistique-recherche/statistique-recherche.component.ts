import {Component, EventEmitter, Input, Output} from '@angular/core';
import {PageableModel} from "@shared-global/core/models/pageable.model";


@Component({
    selector: 'atx-statistique-recherche',
    templateUrl: './statistique-recherche.component.html',
    styleUrls: ['./statistique-recherche.component.css']
})
export class StatistiqueRechercheComponent {

    @Input() total;
    @Input() cardView = true;
    @Input() showDetails;
    @Output() onChangeExpanded = new EventEmitter<boolean>();


    constructor() {

    }

}
