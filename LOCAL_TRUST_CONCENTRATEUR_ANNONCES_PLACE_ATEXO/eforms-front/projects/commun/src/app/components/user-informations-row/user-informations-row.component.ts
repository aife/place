import {Component, EventEmitter, Input, Output} from '@angular/core';
import {AgentDTO} from "@shared-global/core/models/api/eforms.api";

@Component({
    selector: 'atx-user-informations-row',
    templateUrl: './user-informations-row.component.html',
    styleUrls: ['./user-informations-row.component.css']
})
export class UserInformationsRowComponent {

    @Input()
    agent: AgentDTO;
    @Input() delete: boolean;
    @Output() onDeleteItem = new EventEmitter<any>();


}
