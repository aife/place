import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {
    PerfectScrollbarComponent,
    PerfectScrollbarConfigInterface,
    PerfectScrollbarDirective
} from 'ngx-perfect-scrollbar';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MenuItem} from "@core-eforms/models/menu-settings.model";
import {Router} from "@angular/router";

@Component({

    selector: 'app-verticalnav',
    templateUrl: './verticalnav.component.html',
    styleUrls: ['./verticalnav.component.css'],
    animations: [
        trigger('popOverState', [
            state('show', style({
                opacity: '1',
            })),
            state('hide', style({
                opacity: '0',
                height: '*',
            })),
            transition('show => hide', animate('200ms ease-in-out')),
            transition('hide => show', animate('200ms ease-in-out'))
        ])
    ]
})
export class VerticalnavComponent {
    child: any;
    public title;

    private _items = []

    showOnlyRequired = false;
    @Input() sdkVersion: string;
    @Input() readOnly = false;

    @Input() set items(items: MenuItem[]) {
        if (items)
            this._items = items;
    };


    get items() {
        return this._items;
    }

    public config: PerfectScrollbarConfigInterface = {wheelPropagation: false};

    @ViewChild(PerfectScrollbarComponent) componentRef?: PerfectScrollbarComponent;
    @ViewChild(PerfectScrollbarDirective, {static: true}) directiveRef?: PerfectScrollbarDirective;
    @Output() onChangeSection = new EventEmitter<number>()
    @Output() onChangeRequired = new EventEmitter<boolean>()

    constructor(private _router: Router) {
    }

    selectSection(index: number) {
        this._router.navigate([], {
            queryParams: {
                sectionId: index,
                subSectionId: null
            },
            queryParamsHandling: 'merge',
        });
    }


    onPrecedentSection() {
        this.onChangeSection.emit(-1)
    }

    onNextSection() {
        this.onChangeSection.emit(1)

    }

}
