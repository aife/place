import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AlertComponent} from './alert/alert.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import {NgSelectModule} from '@ng-select/ng-select';
import {CalloutsComponent} from './callouts/callouts.component';
import {PipesModule} from '../pipes/pipes.module';
import {DirectivesModule} from '../directives/directives.module';
import {TranslateModule, TranslatePipe} from '@ngx-translate/core';
import {
  NgbCarouselConfig,
  NgbDatepickerModule,
  NgbModalConfig,
  NgbModule,
  NgbPaginationModule,
  NgbTabsetModule,
  NgbTimepickerModule
} from '@ng-bootstrap/ng-bootstrap';
import {AtexoToastComponent} from './atexo-toast/atexo-toast.component';
import {RouterModule} from '@angular/router';
import {BreadcrumbComponent} from '@shared-global/components/breadcrumb/breadcrumb.component';
import {TimepickerModule} from 'ngx-bootstrap/timepicker';
import {LoginComponent} from '@shared-global/components/login/login.component';
import {MoreInformationComponent} from '@shared-global/components/more-information/more-information.component';
import {AccueilComponent} from '@shared-global/components/accueil/accueil.component';
import {EformsStoreModule} from '@shared-global/store/eforms-store.module';
import {FieldFormComponent} from '@shared-global/components/field-form/field-form.component';
import {GroupFieldFormComponent} from '@shared-global/components/group-field-form/group-field-form.component';
import {SectionFieldFormComponent} from '@shared-global/components/section-field-form/section-field-form.component';
import {CardFieldFormComponent} from '@shared-global/components/card-field-form/card-field-form.component';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {VerticalnavComponent} from '@shared-global/components/verticalnav/verticalnav.component';
import {NoticeInformationsComponent} from '@shared-global/components/notice-informations/notice-informations.component';
import {BsModalService} from 'ngx-bootstrap/modal';
import {ListViewComponent} from './list-view/list-view.component';
import {ItemViewComponent} from './item-view/item-view.component';
import {NoticeDetailsComponent} from './notice-details/notice-details.component';
import {NoticeModalWcComponent} from '@shared-global/components/notice-modal-wc/notice-modal-wc.component';
import {TypeAvisDetailsComponent} from '@shared-global/components/type-avis-details/type-avis-details.component';
import {FiltreFacturationComponent} from './filtre-facturation/filtre-facturation.component';
import {PaginationComponent} from '@shared-global/components/pagination/pagination.component';
import {
  StatistiqueRechercheComponent
} from '@shared-global/components/statistique-recherche/statistique-recherche.component';
import {HorizontalTimelineComponent} from '@shared-global/components/horizontal-timeline/horizontal-timeline.component';
import {
  ReferentielSelectItemComponent
} from '@shared-global/components/referentiel-select-item/referentiel-select-item.component';
import {
  TypeAvisChoixModalComponent
} from '@shared-global/components/type-avis-choix-modal/type-avis-choix-modal.component';
import {
  TypeAvisFormulaireDetailsComponent
} from '@shared-global/components/type-avis-formulaire-details/type-avis-formulaire-details.component';
import {UiSwitchModule} from 'ngx-ui-switch';
import {RejetAvisModalComponent} from '@shared-global/components/rejet-avis-modal/rejet-avis-modal.component';
import {EditMessageModalComponent} from '@shared-global/components/edit-message-modal/edit-message-modal.component';
import {
  TypeAvisRevisionModalComponent
} from '@shared-global/components/type-avis-revision-modal/type-avis-revision-modal.component';
import {NgxLoadingModule} from 'ngx-loading';
import {
  UserInformationsRowComponent
} from '@shared-global/components/user-informations-row/user-informations-row.component';
import {ContentTreeviewComponent} from '@shared-global/components/content-treeview/content-treeview.component';
import {
  ModalSurchargeFieldComponent
} from '@shared-global/components/modal-surcharge-field/modal-surcharge-field.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PipesModule,
    DirectivesModule,
    TranslateModule,
    NgbPaginationModule,
    ReactiveFormsModule,
    NgSelectModule,
    BsDatepickerModule.forRoot(),
    NgbDatepickerModule,
    NgbTimepickerModule,
    RouterModule,
    TimepickerModule.forRoot(),
    CommonModule,
    TranslateModule,
    EformsStoreModule,
    NgbTabsetModule,
    PerfectScrollbarModule,
    NgbModule,
    UiSwitchModule,
    NgxLoadingModule
  ],
  providers: [NgbCarouselConfig,
    NgbModalConfig,
    TranslatePipe,
    BsModalService,],
  declarations:
    [ModalSurchargeFieldComponent, ContentTreeviewComponent, UserInformationsRowComponent, TypeAvisRevisionModalComponent, RejetAvisModalComponent, TypeAvisChoixModalComponent, TypeAvisFormulaireDetailsComponent, ReferentielSelectItemComponent, HorizontalTimelineComponent, StatistiqueRechercheComponent, PaginationComponent, TypeAvisDetailsComponent, NoticeModalWcComponent, NoticeInformationsComponent, VerticalnavComponent, CardFieldFormComponent, SectionFieldFormComponent, GroupFieldFormComponent, FieldFormComponent, AccueilComponent, MoreInformationComponent, LoginComponent, BreadcrumbComponent, AlertComponent, CalloutsComponent, AtexoToastComponent, ListViewComponent, ItemViewComponent, NoticeDetailsComponent, FiltreFacturationComponent, EditMessageModalComponent],
  exports:
    [ModalSurchargeFieldComponent, ContentTreeviewComponent, UserInformationsRowComponent, TypeAvisRevisionModalComponent, RejetAvisModalComponent, TypeAvisChoixModalComponent, TypeAvisFormulaireDetailsComponent, ReferentielSelectItemComponent, StatistiqueRechercheComponent, PaginationComponent, TypeAvisDetailsComponent, NoticeModalWcComponent, NoticeInformationsComponent, VerticalnavComponent, CardFieldFormComponent, SectionFieldFormComponent, GroupFieldFormComponent, FieldFormComponent, AccueilComponent, MoreInformationComponent, LoginComponent, BreadcrumbComponent, AlertComponent, CalloutsComponent, AtexoToastComponent, ListViewComponent, NoticeDetailsComponent, FiltreFacturationComponent, EditMessageModalComponent]

})
export class ComponentsModule {
}
