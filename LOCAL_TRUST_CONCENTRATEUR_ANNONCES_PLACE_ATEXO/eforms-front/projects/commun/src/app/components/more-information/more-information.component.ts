import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'atx-more-information',
  templateUrl: './more-information.component.html',
  styleUrls: ['./more-information.component.css']
})
export class MoreInformationComponent implements OnInit {
  @Output() onClickInformation = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

}
