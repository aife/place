import {Component, EventEmitter, Input, OnDestroy, Output} from '@angular/core';
import {
  FailedAssertRepresentation,
  SubNoticeMetadata,
  XmlStructureDetails
} from '@shared-global/core/models/api/eforms.api';
import {
  buildObjectFromXmlStructure,
  findAllValuesOfKeysInObject,
  findNextID,
  getFailedAssertFromElements,
  getNode,
  getNodeId,
  isEmptyObject,
  mergeJsonRecuresevily
} from '@shared-global/core/utils/technical/technical.utils';
import {EformsSdkService} from '@shared-global/core/services/eforms-sdk.service';
import {State} from '@shared-global/store';
import {Store} from '@ngrx/store';
import {deleteExtended, setExtended, switchExtended} from '@shared-global/store/formulaire/formulaire.action';
import {Subscription} from 'rxjs';

@Component({
  selector: 'atx-card-field-form',
  templateUrl: './card-field-form.component.html',
  styleUrls: ['./card-field-form.component.scss'],
})
export class CardFieldFormComponent implements OnDestroy {


  protected readonly getNodeId = getNodeId;
  messageError: string;
  private nodeIds: { [index: string]: XmlStructureDetails[] } = {};

  creation = false;

  private _selectedError: string;
  private _chemin: string;
  @Input() set chemin(chemin: string) {
    this._chemin = chemin;
  }

  get chemin() {
    return this._chemin;
  }
  @Input() set selectedError(error: string) {
    this._selectedError = error;
  }
  get selectedError() {
    return this._selectedError;
  }
  private _formulaire: any;
  @Input() set formulaire(formulaire: any) {
    this._formulaire = formulaire;
    this.setObject();
  };

  get formulaire() {
    return this._formulaire;
  }

  private _showOnlyRequired = false;
  @Input() readOnly = false;
  private _nextIndex = 1;
  @Input() set nextIndex(nextIndex: number) {
    if (!!nextIndex) {
      this._nextIndex = nextIndex;
      this.creation = true;
    } else {
      this.creation = false;
      this._nextIndex = null;
    }

  }

  get nextIndex() {
    return this._nextIndex;
  }

  @Input() set showOnlyRequired(showOnlyRequired: boolean) {
    this._showOnlyRequired = showOnlyRequired;
    this.setObject();
  };

  get showOnlyRequired() {
    return this._showOnlyRequired;
  }

  @Input() set itemGroup(group: SubNoticeMetadata) {
    if (!group) {
      return;
    }
    this._itemGroup = group;

    this.subscription$ = this.store.select(state => state.formaulaireReducer.extended.get(this._chemin)).subscribe(value => {
      if (value !== undefined)
        this.extended = value;
      else {
        this.store.dispatch(setExtended({
          id: this.chemin,
          extended: !this._itemGroup.collapsed
        }));
        this.extended = !this._itemGroup.collapsed;
      }
    });
  }

  private subscription$: Subscription;

  @Input() sdkVersion: string;

  @Input() set reset(field: Date) {
    if (!field) {
      return;
    }
    this.object = {};
  }

  get itemGroup() {
    return this._itemGroup;
  }

  @Output() onChangeCardObject = new EventEmitter<{ object: any, nodeIds: XmlStructureDetails[] }>()
  @Output() onDeleteElement = new EventEmitter<number>()
  @Output() onShowAddContent = new EventEmitter<boolean>();

  hasError = false;
  private _itemGroup: SubNoticeMetadata;
  object = {};
  refresh: Date;
  @Input() noticeType: string;
  @Input() noticeId: number;
  @Input() index: number;

  @Input() parentNode: string;
  private _errors: FailedAssertRepresentation[];
  @Input() set errors(errors: FailedAssertRepresentation[]) {
    this._errors = errors;
  }

  get errors() {
    return this._errors;
  }

  active: any;
  extended = true;
  @Output() onChangeFieldVisibility = new EventEmitter<boolean>()


  constructor(private readonly eformsSdkService: EformsSdkService, private readonly store: Store<State>) {

  }


  setObject(): void {

    if (!this.formulaire) {
      this.creation = true;
      return;
    }
    this.creation = false;
    let nodeId = this._itemGroup.nodeId;
    if (!!nodeId) {
      const nodeObject = getNode(this.formulaire, nodeId, this._itemGroup.contentType, this.index);
      if (nodeObject) {
        this.object[this._itemGroup.id] = nodeObject
      }
    } else {
      this.object[this._itemGroup.id] = this.formulaire
    }
  }

  async ajouterList() {
    if (!isEmptyObject(this.object[this._itemGroup.id])) {
      this.refresh = new Date();
      let nodeIds = this.nodeIds[this._itemGroup.id];
      this.onChangeCardObject.emit({object: this.object[this._itemGroup.id], nodeIds: nodeIds});
      this.messageError = null
    } else {
      this.messageError = "Veuillez renseigner votre élément "
    }
  }

  private async setNoticeInternalId() {
    if (!this._itemGroup)
      return;
    let identifierFieldId = this._itemGroup._identifierFieldId;
    if (identifierFieldId) {
      this.store.select(state => state.formaulaireReducer.formulaire).subscribe(async formualire => {
        let ids = []
        findAllValuesOfKeysInObject(formualire, [identifierFieldId], ids);
        let field = await this.eformsSdkService.getField(identifierFieldId, this.sdkVersion).toPromise();
        let idObjet = {};
        idObjet[identifierFieldId] = findNextID(ids, this._itemGroup._idScheme);
        let nodeId = getNodeId(this._itemGroup);
        let index = field.nodeIds.map(value1 => value1.id).indexOf(nodeId);
        let {object, nodeIds} = buildObjectFromXmlStructure({object: idObjet, nodeIds: field.nodeIds}, index);
        this.nodeIds[this._itemGroup.id] = nodeIds;
        if (nodeId)
          this.object[this._itemGroup.id] = mergeJsonRecuresevily(this.object[this._itemGroup.id], object);
        this.ajouterList();
      })
    }else {
      this.ajouterList();
    }

  }

  deleteList() {
    this.store.dispatch(deleteExtended({
      id: this.chemin
    }));
    if (this.creation) {
      this.onShowAddContent.emit(false);
    } else {
      this.onDeleteElement.emit(this.index);
    }
  }
  changeGroupObject(event: { object: any, nodeIds: XmlStructureDetails[] }, addToList?: boolean) {
    let nodeId = getNodeId(this._itemGroup);
    let index = event.nodeIds.map(value1 => value1.id).indexOf(nodeId);
    let {object, nodeIds} = buildObjectFromXmlStructure(event, index);
    if (!this._itemGroup._repeatable || !nodeId) {
      this.onChangeCardObject.emit({
        object: object,
        nodeIds: nodeIds
      })
    } else {
      this.nodeIds[this._itemGroup.id] = nodeIds;
      if (nodeId)
        this.object[this._itemGroup.id] = mergeJsonRecuresevily(this.object[this._itemGroup.id], object);
      if (this.index >= 0) {
        this.onChangeCardObject.emit({
          object: this.object[this._itemGroup.id],
          nodeIds: nodeIds
        })
      } else if (addToList === true) {
        this.setNoticeInternalId();
      }
    }

  }

  setNewElement(event: { object: any, nodeIds: XmlStructureDetails[] }) {

  }

  get erreurMessages() {
    if (!this.errors)
      return []
    const messages = this.errors.map(value => value.text)
    let nodeId = getNodeId(this._itemGroup);
    if (!messages || messages.length === 0 || !nodeId) {
      return []
    }
    return messages.filter(value => {
      return value.includes(nodeId)
    })
  }

  ngOnDestroy(): void {
    if (!!this.subscription$)
      this.subscription$.unsubscribe()
  }

  changeExpanded() {
    this.store.dispatch(switchExtended({id: this._chemin}));
  }

  protected readonly getFailedAssertFromElements = getFailedAssertFromElements;

}
