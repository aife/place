import {Component, Input} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";
import {
    ConfigurationConsultationDTO,
    OffreDTO,
    SuiviTypeAvisPubAdd,
    SuiviTypeAvisPubDTO,
    SuiviTypeAvisPubPatch
} from "@shared-global/core/models/api/eforms.api";
import {AnnoncesTypeAvisService} from "@shared-global/core/services/annonces-type-avis.service";
import {SupportsService} from "@shared-global/core/services/supports.service";
import {AnnoncesService} from "@shared-global/core/services/annonces.service";

@Component({
    selector: 'atx-type-avis-choix-modal',
    templateUrl: './type-avis-choix-modal.component.html',
    styleUrls: ['./type-avis-choix-modal.component.css']
})
export class TypeAvisChoixModalComponent {
    private _typeAvis: SuiviTypeAvisPubDTO;
    @Input() onlyJals = false;

    @Input() set typeAvis(typeAvis: SuiviTypeAvisPubDTO) {
        this._typeAvis = typeAvis;
        if (this._typeAvis) {
            this.selectedOffre = this._typeAvis.offreRacine;
        }
    }

    get typeAvis(): SuiviTypeAvisPubDTO {
        return this._typeAvis;
    }

    offres: OffreDTO[];
    validation = false;
    showFacturation = false;
    showJal = false;

    selectedOffre: OffreDTO;
    selectedFacturationIndex: number;
    jalList = new Map<number, boolean>();


    configuration: ConfigurationConsultationDTO;

    private _idConsultation: number;

    get idConsultation(): number {
        return this._idConsultation;
    }

    @Input() set idConsultation(idConsultation: number) {
        this._idConsultation = idConsultation;
        if (this._idConsultation) {
            this.annoncesService.getConfiguration(this.idConsultation).subscribe(configuration => {

                this.configuration = configuration;
                this.jalList = new Map<number, boolean>();
                if (this.configuration) {
                    if (!this.typeAvis) {
                        this.supportsService.getOffres(this.configuration?.contexte?.typeProcedureConsultation?.abreviation,
                            this.configuration?.contexte?.organismeConsultation?.acronyme, this.configuration.europeen, !this.configuration.europeen).subscribe(value => {
                            this.offres = value;
                        });
                    }
                    if (this.configuration.facturationList) {
                        if (!this._typeAvis || !this._typeAvis.facturation)
                            this.selectedFacturationIndex = -1;
                        else {
                            this.configuration.facturationList.forEach((value, i) => {
                                let selected = value.address === this._typeAvis.facturation.address &&
                                    value.email === this._typeAvis.facturation.email && value.sip === this._typeAvis.facturation.sip;
                                if (selected) {
                                    this.selectedFacturationIndex = i;
                                }
                            })
                        }
                    }
                    if (this.configuration.jalList) {
                        this.configuration.jalList.forEach((value, i) => {
                            if (!this._typeAvis || !this._typeAvis.annonces || this._typeAvis.annonces.length === 0)
                                this.jalList.set(i, false);
                            else {
                                const list = this._typeAvis.annonces.map(annonce => annonce.support);
                                this.jalList.set(i, list.findIndex(value1 => value1.code.indexOf(value.email) > -1 && value1.libelle === value.nom) > -1);

                            }
                        })
                    }
                }
                this.setVue();
            })
        }
    }

    constructor(public bsModalRef: BsModalRef, private readonly annoncesTypeAvisService: AnnoncesTypeAvisService,
                private readonly supportsService: SupportsService, private readonly annoncesService: AnnoncesService) {

    }

    addSelectedAvis() {

        this.validation = true;
        const avis: SuiviTypeAvisPubAdd = {
            idConsultation: this.idConsultation,
            reference: this.configuration.contexte.consultation.reference,
            titre: this.configuration.contexte.consultation.intitule,
            objet: this.configuration.contexte.consultation.objet,
            europeen: this.configuration.europeen,
            dlro: this.configuration.contexte.consultation.dateLimiteRemiseOffres,
            dateMiseEnLigne: this.configuration.dmls ? new Date(this.configuration.dmls) : null,
            dateMiseEnLigneCalcule: this.configuration.contexte.consultation.dateMiseEnLigneCalcule ? new Date(this.configuration.contexte.consultation.dateMiseEnLigneCalcule) : null,
            organisme: this.configuration.contexte.organismeConsultation,
            service: this.configuration.contexte.serviceConsultation,
            procedure: this.configuration.contexte.typeProcedureConsultation,
            jalList: this.configuration.jalList.filter((value, index) => this.jalList.get(index)),
            facturation: this.selectedFacturationIndex > -1 ? this.configuration.facturationList[this.selectedFacturationIndex] : null,
            offreRacine: this.selectedOffre,
        }
        this.annoncesTypeAvisService.addAnnonce(avis).subscribe(value => {

            this.validation = false;
            this.closeModal();
        }, (error) => {
            this.validation = false;
        });
    }

    modifySelectedAvis() {

        this.validation = true;
        const avis: SuiviTypeAvisPubPatch = {
            jalList: this.configuration.jalList.filter((value, index) => this.jalList.get(index)),
            facturation: this.selectedFacturationIndex > -1 ? this.configuration.facturationList[this.selectedFacturationIndex] : null
        }
        this.annoncesTypeAvisService.patchAnnonce(this.typeAvis.id, avis, this.typeAvis.idConsultation).subscribe(value => {

            this.validation = false;
            this.closeModal();
        }, (error) => {
            this.validation = false;
        });
    }

    closeModal() {
        this.bsModalRef.hide();
    }


    changePrecochee(index: number, precoche: any) {

        if (!precoche) {
            this.selectedFacturationIndex = -1;
            return;
        }
        this.selectedFacturationIndex = index;
    }

    changeOffre(offre: OffreDTO) {
        this.selectedOffre = offre;
        this.setVue();
    }

    private setVue() {
        if (!this.typeAvis) {
            this.selectedFacturationIndex = -1;
            this.configuration.jalList.forEach((value, index) => {
                this.jalList.set(index, false);
            })
        }
        if (this.configuration && this.configuration.europeen && this.selectedOffre && this.selectedOffre.offreAssociee) {
            this.showFacturation = true;
            this.showJal = true
            return;
        }
        if (this.configuration && !this.configuration.europeen) {
            this.showFacturation = true;
            this.showJal = true
            return;

        }
        this.showFacturation = false;
        this.showJal = false

    }

    changeJal(index: number, $event: boolean) {
        this.jalList.set(index, $event);
    }

    isFormulaireValid() {
        if (!this.selectedOffre) {
            return false;
        }
        if (this.selectedFacturationIndex === -1 && this.showFacturation) {
            return false;
        }
        if (this.showJal) {
            let valid = false;
            this.jalList.forEach((value, key) => {
                if (value) {
                    valid = value;
                }
            });
            return valid;
        } else {
            return true;
        }
    }
}
