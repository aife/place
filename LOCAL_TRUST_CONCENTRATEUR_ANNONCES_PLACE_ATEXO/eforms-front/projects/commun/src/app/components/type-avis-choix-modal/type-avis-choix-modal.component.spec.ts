import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeAvisChoixModalComponent } from './type-avis-choix-modal.component';

describe('FormsPageComponent', () => {
  let component: TypeAvisChoixModalComponent;
  let fixture: ComponentFixture<TypeAvisChoixModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypeAvisChoixModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeAvisChoixModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
