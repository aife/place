import {Component, EventEmitter, Input, Output} from '@angular/core';
import {
  FailedAssertRepresentation,
  SubNoticeMetadata,
  XmlStructureDetails
} from '@shared-global/core/models/api/eforms.api';
import {
  buildObjectFromXmlStructure,
  getFailedAssertFromElements,
  getNode,
  getNodeId,
  mergeJsonRecuresevily
} from '@shared-global/core/utils/technical/technical.utils';
import {NgbTabChangeEvent} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';

@Component({
  selector: 'atx-section-field-form',
  templateUrl: './section-field-form.component.html',
  styleUrls: ['./section-field-form.component.scss'],
})
export class SectionFieldFormComponent {

  private _showOnlyRequired = false;
  @Input() set showOnlyRequired(showOnlyRequired: boolean) {
    this._showOnlyRequired = showOnlyRequired;
    this.setObjet();
  };

  @Input() readOnly = false;

  get showOnlyRequired() {
    return this._showOnlyRequired;
  }

  @Input() set group(group: Array<SubNoticeMetadata>) {
    if (!group) {
      return;
    }
    this._group = group;
  }

  @Input() set reset(field: Date) {
    if (!field) {
      return;
    }
    this.object = {};
  }

  get group() {
    return this._group;
  }

  @Input() set formulaire(formulaire: any) {
    this._formulaire = formulaire;
    this.setObjet();
  }

  get formulaire() {
    return this._formulaire;
  }

  private _formulaire: any;

  @Output() onChangeSectionObject = new EventEmitter<{ object: any, nodeIds: XmlStructureDetails[] }>()

  hasError = new Map<string, boolean>();
  private _group: Array<SubNoticeMetadata> = [];
  object = {};
  objectList: { [index: string]: any[] } = {};
  refresh: Date;
  @Input() noticeType: string;
  @Input() noticeId: number;
  @Input() activeId: string;
  @Input() sdkVersion: string;
  @Input() parentNode: string;
  @Input() errors: FailedAssertRepresentation[];
  @Input() chemin: string;
  active: any;
  private _selectedError: string;
  @Input() set selectedError(error: string) {
    this._selectedError = error;
    if (error) {
      this.group.forEach((value, index) => {
        let errors = getFailedAssertFromElements(this.errors, value, null)
          .filter(value1 => value1.id === error);
        if (errors.length > 0) {
          this.activeId = index + "";
        }

      })

    }
  }

  get selectedError() {
    return this._selectedError;
  }

  constructor(private _router: Router,) {
  }


  private setObjet() {
    if (!this.formulaire)
      return;
    this._group.forEach(itemGroup => {
      let nodeId = getNodeId(itemGroup);
      if (!!nodeId) {
        const nodeObject = getNode(this.formulaire, nodeId, itemGroup.contentType);
        if (itemGroup._repeatable && nodeObject) {
          this.objectList[itemGroup.id] = nodeObject
        } else if (nodeObject) {
          this.object[itemGroup.id] = nodeObject
        }
      }
    })
  }

  changeGroupObject(event: { object: any, nodeIds: XmlStructureDetails[] }, item: SubNoticeMetadata) {
    let nodeId = getNodeId(item);
    let index = event.nodeIds.map(value1 => value1.id).indexOf(nodeId);
    let {object, nodeIds} = buildObjectFromXmlStructure(event, index);
    if (nodeId)
      this.object[item.id] = mergeJsonRecuresevily(this.object[item.id], object);
    this.onChangeSectionObject.emit({
      object: nodeId ? this.object[item.id] : object,
      nodeIds: nodeIds
    })
  }

  modifyGroupList(item: any, event: { itemGroup: any, index: number, object: any, nodeIds: XmlStructureDetails[] }) {
    let nodeId = getNodeId(item);
    let index = event.nodeIds.map(value1 => value1.id).indexOf(nodeId);
    let {object, nodeIds} = buildObjectFromXmlStructure(event, index);
    if (nodeId)
      this.object[item.id] = mergeJsonRecuresevily(this.object[item.id], object);
    this.onChangeSectionObject.emit({
      object: nodeId ? this.object[item.id] : object,
      nodeIds: nodeIds
    })
  }

  get sectionsDisplay(): SubNoticeMetadata[] {
    return this._group ? this._group.filter(value => value.displayType === 'SECTION' && !value.forceHidden) : [];
  }

  getFormulaire(itemGroup: SubNoticeMetadata) {
    if (itemGroup._repeatable) {
      if (this.objectList[itemGroup.id]?.length > 0) {
        return this.objectList[itemGroup.id][0];
      } else return null;
    } else {
      let nodeId = getNodeId(itemGroup);
      if (!nodeId)
        return this.formulaire;
      else
        return this.formulaire[nodeId]
    }
  }

  onchangeTab($event: NgbTabChangeEvent) {
    this._router.navigate([], {
      queryParams: {
        subSectionId: $event.nextId
      },
      queryParamsHandling: 'merge',
    });
  }


  protected readonly getNodeId = getNodeId;
  protected readonly getFailedAssertFromElements = getFailedAssertFromElements;
}
