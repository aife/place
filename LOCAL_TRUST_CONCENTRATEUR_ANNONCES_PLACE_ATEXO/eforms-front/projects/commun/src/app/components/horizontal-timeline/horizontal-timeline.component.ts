import {Component, Input} from '@angular/core';

@Component({
  selector: 'atx-horizontal-timeline',
  templateUrl: './horizontal-timeline.component.html',
  styleUrls: ['./horizontal-timeline.component.css']
})
export class HorizontalTimelineComponent {

  @Input() events: any[]
}
