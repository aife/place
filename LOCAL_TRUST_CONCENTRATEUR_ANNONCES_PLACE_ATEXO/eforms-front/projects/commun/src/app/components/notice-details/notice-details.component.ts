import {Component, Input, OnInit} from '@angular/core';
import {EFormsFormulaire} from "@shared-global/core/models/api/eforms.api";
import {showErrorToast, showSuccessToast} from "@shared-global/store/toast/toast.action";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";
import {EformsNoticesService} from "@shared-global/core/services/eforms-notices.service";
import {TranslatePipe} from "@ngx-translate/core";
import {NoticeModalWcComponent} from "@shared-global/components/notice-modal-wc/notice-modal-wc.component";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";

@Component({
    selector: 'atx-notice-details',
    templateUrl: './notice-details.component.html',
    styleUrls: ['./notice-details.component.css']
})
export class NoticeDetailsComponent implements OnInit {

    @Input() notice: EFormsFormulaire;
    bsModalRef: BsModalRef;


    constructor(private modalService: BsModalService,
                private store: Store<State>,
                private routingInterneService: RoutingInterneService,
                private translatePipe: TranslatePipe,
                private readonly eformsNoticesService: EformsNoticesService) {
    }


    ngOnInit(): void {
    }

    exportNotice() {
        const blob = new Blob([this.notice.xmlGenere],
            {type: "application/xml"});
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", "notice-" + this.notice.idConsultation + "-" + this.notice.idNotice + "-" + this.notice.uuid + ".xml");
        document.body.appendChild(link);
        link.click();
        if (!!link.parentNode)
            link.parentNode.removeChild(link)
        this.store.dispatch(showSuccessToast({
            message: "Votre formulaire est exporté sous XML",
            header: 'Export XML'
        }))

    }

    validate() {
        this.eformsNoticesService.verifyNotice(this.notice.id, JSON.parse(this.notice.formulaire))
            .subscribe(value => {
                if (value.valid) {
                    this.store.dispatch(showSuccessToast({
                        message: "Votre formulaire est valide",
                        header: 'Validation formulaire'
                    }))
                } else {
                    let messages = value.errors.map(value1 => value1.text)
                        .map(value => this.translatePipe.transform(value))
                        .join(", ");
                    this.store.dispatch(showErrorToast({
                        message: messages,
                        header: 'Validation formulaire'

                    }))
                }
            }, (error) => {
                this.store.dispatch(showErrorToast({
                    message: error,
                    header: 'Validation formulaire'

                }))
            })

    }

    openModal(readOnly: boolean) {
        this.bsModalRef = this.modalService.show(NoticeModalWcComponent, {
            class: 'modal-full-width',
            backdrop: true,
            ignoreBackdropClick: false
        });
        this.bsModalRef.content.readOnly = readOnly;
        this.bsModalRef.content.params = {
            idNotice: this.notice.idNotice,
            idConsultation: this.notice.idConsultation,
            lang: this.notice.lang
        }
    }
}
