import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {SubNoticeMetadata} from "@shared-global/core/models/api/eforms.api";

@Component({
  selector: 'atx-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.css']
})
export class ListViewComponent implements OnChanges {

  @Input() items: any
  @Input() itemGroup: SubNoticeMetadata;
  @Output() onDeleteItem = new EventEmitter<number>()
  extended = [];

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.extended = [];
    if (!!this.items && this.isArray) {
      this.extended = this.items.map(()=> {
        return false;
      })
    }
  }


  fieldHeaders(item) {
    return item ? Object.keys(item).filter(value => !this.isObject(item[value])) : []
  }

  objectHeaders(item) {
    return item ? Object.keys(item).filter(value => this.isObject(item[value])) : []
  }

  isObject(itemElement: any) {
    return typeof itemElement === 'object';
  }

  get isArray() {
    return Array.isArray(this.items);
  }

  getItem(itemElement: any): Array<any> {
    return itemElement;
  }

  delete(i: number) {
    this.onDeleteItem.emit(i)
  }
}
