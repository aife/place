import {Component} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {State} from "@shared-global/store";
import {Store} from "@ngrx/store";
import {setAccessToken, setDefaultParams} from "@shared-global/store/user/user.action";
import {of} from "rxjs";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";

@Component({
  selector: 'atx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  message = "Vous n'êtes pas connectés";
  connecting$ = of(false)
  firstConnected = true;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private routerInterne: RoutingInterneService,
    private readonly store: Store<State>,
  ) {

    let query = new URLSearchParams(window.location.search);
    console.log('query', query)
    this.route.params.subscribe(params => {
      console.log('params', params)
      if (params.eformsAccessToken != null) {
        const defaults = {
          defaultPage: null,
          isWebComponent: false
        };
        let redirect = query?.get("redirect");
        console.log('redirect', redirect)
        if (!!redirect) {
          defaults.defaultPage = redirect;
        }
        let isWebComponent = query?.get("isWebComponent")
        if (isWebComponent === 'true') {
          defaults.isWebComponent = true;
          sessionStorage.setItem("isWebComponent", "true")
        } else {
          sessionStorage.setItem("isWebComponent", "false")
        }
        this.store.dispatch(setDefaultParams(defaults));
        this.login(params.eformsAccessToken, params.plateforme, params.organisme, redirect);

      }
    });
    this.connecting$ = this.store.select(state => state.userReducer.connecting);
    this.store.select(state => state.userReducer).subscribe(value => {
      if (value.connected && this.firstConnected) {
        this.firstConnected = false;
        this.routerInterne.navigateFromContexte(value.defaultPage);
      }
    });
  }


  login(accessToken: string, plateforme: string, organisme: string, redirect) {

    this.store.dispatch(setAccessToken({accessToken, organisme, redirect, plateforme}))

  }


}
