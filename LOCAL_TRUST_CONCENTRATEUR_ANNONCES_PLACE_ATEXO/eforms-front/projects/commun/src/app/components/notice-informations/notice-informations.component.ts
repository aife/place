import {Component, Input} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";
import {SubNotice} from "@shared-global/core/models/api/eforms.api";


@Component({
  selector: 'app-notice-informations',
  templateUrl: './notice-informations.component.html',
  styleUrls: ['./notice-informations.component.css']
})
export class NoticeInformationsComponent {
  @Input() subNotice: SubNotice;
  @Input() messages: any;
  @Input() formulaire: any;


  constructor(public bsModalRef: BsModalRef) {

  }

  closeModal() {
    this.bsModalRef.hide();
  }



}
