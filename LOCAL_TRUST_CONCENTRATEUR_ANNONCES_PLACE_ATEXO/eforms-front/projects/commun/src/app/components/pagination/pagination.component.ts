import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PageableModel} from "@shared-global/core/models/pageable.model";


@Component({
  selector: 'atx-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {

  @Input() total;
  @Input() showTriList = true;
  @Input() pageable: PageableModel;
  @Input() sizes = [10, 20];
  @Input() sortFieldsList = ['dateModification'];
  @Output() onChangePagination = new EventEmitter<PageableModel>();


  constructor() {

  }

  ngOnInit(): void {
  }

  changePage(page: number) {
    this.pageable.page = page - 1;
    this.onChangePagination.emit(this.pageable);
  }

  changeSize(page: number) {
    this.pageable.size = page
    this.pageable.page = 0;
    this.onChangePagination.emit(this.pageable);
  }

  changeAsc() {
    this.pageable.asc = !this.pageable.asc;
    this.pageable.page = 0;
    this.onChangePagination.emit(this.pageable);
  }

  changeSortField(sortField: string) {
    this.pageable.sortField = sortField
    this.pageable.page = 0;
    this.onChangePagination.emit(this.pageable);
  }

}
