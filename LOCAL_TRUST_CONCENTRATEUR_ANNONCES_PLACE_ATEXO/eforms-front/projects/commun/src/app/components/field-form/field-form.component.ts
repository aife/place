import {Component, EventEmitter, Input, OnDestroy, Output} from '@angular/core';
import {
  FailedAssertRepresentation,
  FieldDetails,
  SubNoticeMetadata,
  XmlStructureDetails
} from '@shared-global/core/models/api/eforms.api';
import {EformsSdkService} from '@shared-global/core/services/eforms-sdk.service';
import {TranslateService} from '@ngx-translate/core';
import {zoneTime} from '@shared-global/core/enums/app-internal-path.enum';
import {isArray} from 'lodash';
import {debounceTime} from 'rxjs/operators';
import {Subject, Subscription} from 'rxjs';
import {
  findAllValuesOfKeysInObject,
  getFailedAssertFromElements,
  isHidden,
  isReadonly
} from '@shared-global/core/utils/technical/technical.utils';
import {State} from '@shared-global/store';
import {Store} from '@ngrx/store';
import {EformsNoticesService} from '@shared-global/core/services/eforms-notices.service';
import moment from 'moment';

@Component({
  selector: 'atx-field-form',
  templateUrl: './field-form.component.html',
  styleUrls: ['./field-form.component.scss'],
})
export class FieldFormComponent implements OnDestroy {


  private _selectedError: string;
  @Input() set selectedError(error: string) {
    this._selectedError = error;
    // scroll to this component
    setTimeout(() => {
      const element = document.getElementById("BT-" + error);
      if (!!element) {
        element.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
      }
    }, 1000)
  }

  get selectedError() {
    return this._selectedError;
  }

  private _readOnly = false;
  @Input() set readOnly(readOnly: boolean) {
    this._readOnly = readOnly;
    if (this._readOnly) {
      this.optionList = [];
    }
  }

  get readOnly() {
    return this._readOnly;
  }

  @Input() set reset(field: Date) {
    if (!field) {
      return;
    }
    this.setInitialObject(this.configuration);
  }


  @Output() onChangeFieldObject = new EventEmitter<{ object: any, nodeIds: XmlStructureDetails[] }>()
  @Output() onChangeFieldVisibility = new EventEmitter<boolean>()


  @Input() field: SubNoticeMetadata;
  @Input() chemin: string;
  id = this.generateUuidV4();
  configuration: FieldDetails;
  refList: Array<string> = [];
  refListLoading = false;
  optionList: Array<string> = [];
  optionListLoading = false;
  object = {};
  refresh: Date;
  private _showOnlyRequired = false;
  @Input() set showOnlyRequired(showOnlyRequired: boolean) {
    this._showOnlyRequired = showOnlyRequired;
    this.setObjet();
    if (this._showOnlyRequired && this.field) {
      this.field.forceHidden = !this.isRequired;
      this.onChangeFieldVisibility.emit(this.field.forceHidden)
    } else if (!this._showOnlyRequired && this.field) {
      this.field.forceHidden = false;
      this.onChangeFieldVisibility.emit(this.field.forceHidden)
    }
  };

  isHiddenField() {
    return isHidden(this.field) && !this.isRequired;
  }

  get showOnlyRequired() {
    return this._showOnlyRequired;
  }

  @Input() sdkVersion: string;
  @Input() noticeType: string;
  @Input() noticeId: number;
  @Input() parentNode: string;
  private _errors: FailedAssertRepresentation[];
  @Input() set errors(errors: FailedAssertRepresentation[]) {
    this._errors = errors;
    if (this._errors) {
      this.erreurMessages = getFailedAssertFromElements(this._errors, this.field, null)
        .filter(value => value.elements.length === this.field.elements.length);
    }
  }

  get errors() {
    return this._errors;
  }

  @Input() set formulaire(formulaire: any) {
    this._formulaire = formulaire;
    this.init();
  }

  get formulaire() {
    return this._formulaire;
  }

  private _formulaire: any;
  active: any;
  searchDelay = 500;
  private subscription$: Subscription;
  erreurMessages: FailedAssertRepresentation[] = [];

  inputChanged: Subject<Date> = new Subject<Date>();

  search(term: string, item: any) {
    return true;
  }

  constructor(private readonly store: Store<State>, private readonly eformsSdkService: EformsSdkService, private readonly eformsNoticesService: EformsNoticesService, private translateService: TranslateService) {
    this.subscribeChange();
  }

  ngOnDestroy(): void {
    if (!!this.subscription$)
      this.subscription$.unsubscribe()
  }


  private subscribeChange(): void {
    this.subscription$ = this.inputChanged.pipe(debounceTime(this.searchDelay)).subscribe(() => {
      if (this.configuration.id === 'BT-702(a)-notice') {
        this.eformsNoticesService.changeOfficialLang(this.noticeId, this.object['BT-702(a)-notice']).subscribe(value => {
          this.onChangeFieldObject.emit({object: this.object, nodeIds: [...this.configuration.nodeIds]});
        });
      } else {
        this.onChangeFieldObject.emit({object: this.object, nodeIds: [...this.configuration.nodeIds]});
      }
    })
  }

  init(): void {
    if (this.configuration) {
      this.setInitialObject(this.configuration);
      if (this._showOnlyRequired) {
        this.field.forceHidden = !this.isRequired;
        this.onChangeFieldVisibility.emit(this.field.forceHidden);
      }
      this.setObjet();
      return;
    }

    if (!isHidden(this.field) && this.field.displayType === 'SECTION') {
      this.active = this.field.content[0];
    }
    if (this.field.contentType === 'field' && !isHidden(this.field)) {
      this.eformsSdkService.getField(this.field.id, this.sdkVersion,)
        .subscribe(value => {

          this.configuration = value;
          this.setInitialObject(this.configuration);
          if (this._showOnlyRequired) {
            this.field.forceHidden = !this.isRequired;
            this.onChangeFieldVisibility.emit(this.field.forceHidden);
          }
          this.setObjet();

        });
    }

  }

  private setObjet() {
    if (this.field?.surcharge?.dynamicListFrom || this.field?.surcharge?.staticList || this.field?.fieldIds?.length > 0) {
      this.refList = [];
      if (this.field?.surcharge?.staticList) {
        this.refList.push(...this.field.surcharge.staticList.split(","));
      }
      if (this.field?.surcharge?.dynamicListFrom) {
        let fields = this.field.surcharge.dynamicListFrom.split(",");
        this.store.select(state => state.formaulaireReducer.formulaire).subscribe(value => {
          let list = [];
          findAllValuesOfKeysInObject(value, fields, list);
          this.refList.push(...list)
        });
      }
      if (this.field.fieldIds?.length > 0) {
        this.store.select(state => state.formaulaireReducer.formulaire).subscribe(value => {
          this.refList = [];
          findAllValuesOfKeysInObject(value, this.field.fieldIds, this.refList)
        });
      }
    }
    if (!this.configuration || !this.formulaire)
      return;
    let obj = this.formulaire;
    let start = false;
    let i = this.configuration.nodeIds.length - 1;

    while (i > -1) {
      const value1 = this.configuration.nodeIds[i];
      if (start) {
        obj = isArray(obj) ? obj[0][value1.id] : obj[value1.id];
        if (!obj) {
          return;
        }
      }
      if (!start) {
        start = (value1.id === this.parentNode);
      }
      i--;
    }

    if (obj) {
      if (obj[this.configuration.parentNodeId]) {
        obj = obj[this.configuration.parentNodeId];
      }

      //this.readOnly = !!obj[this.field.id];
      if (isArray(obj) && obj[0][this.configuration.id] !== undefined && obj[0][this.configuration.id] !== null)
        this.object[this.configuration.id] = obj[0][this.configuration.id];
      else if (!isArray(obj) && obj[this.configuration.id] !== undefined && obj[this.configuration.id] !== null) {
        this.object[this.configuration.id] = obj[this.configuration.id];
      }

      if ((this.configuration.type === 'date-time' || this.configuration.type === 'date' || this.configuration.type === 'time'
          || this.configuration.type === 'zoned-time') && this.object[this.configuration.id] && this.object[this.configuration.id].value
        && this.object[this.configuration.id].value.includes('-')) {
        this.object[this.configuration.id].value = this.object[this.configuration.id].value.split('-').reverse().join('/');
      }
    }
  }


  get isRequired() {
    if (!this.configuration || !this.field)
      return false;
    if (this.field.surcharge && (this.field.surcharge.obligatoire !== null && this.field.surcharge.obligatoire !== undefined)) {
      return this.field.surcharge.obligatoire;
    }
    let isRequired = this.estObligatoireNonConditonne(this.configuration);
    let isForbidden = this.estForbiddenNonConditonne(this.configuration);
    return isRequired && !isForbidden;
  }

  estForbiddenNonConditonne(configuration) {
    let forbidden = configuration?.forbidden;
    let isForbidden = forbidden?.value;
    if (forbidden?.constraints && forbidden?.constraints?.length > 0) {
      forbidden.constraints
        .forEach(value => {
          if (value.noticeTypes.includes(this.noticeType)) {
            isForbidden = value.value && !!value.condition;
          }
        })
    }
    return isForbidden;
  }

  estForbiddenConditonne(configuration) {
    let forbidden = configuration?.forbidden;
    let isForbidden = false;
    if (forbidden?.constraints && forbidden?.constraints?.length > 0) {
      forbidden.constraints
        .forEach(value => {
          if (value.noticeTypes.includes(this.noticeType)) {
            isForbidden = value.value && !!value.condition;
          }
        })
    }
    return isForbidden;
  }

  private estObligatoireNonConditonne(configuration) {
    let mandatory = configuration?.mandatory;
    let isRequired = mandatory?.value;
    if (mandatory?.constraints && mandatory?.constraints?.length > 0) {
      mandatory.constraints
        .forEach(value => {
          if (value.noticeTypes.includes(this.noticeType)) {
            isRequired = value.value && !value.condition;
          }
        })
    }
    return isRequired;
  }

  estObligatoireConditonne(configuration) {
    let mandatory = configuration?.mandatory;
    let isRequired = false;
    if (mandatory?.constraints && mandatory?.constraints?.length > 0) {
      mandatory.constraints
        .forEach(value => {
          if (value.noticeTypes.includes(this.noticeType)) {
            isRequired = value.value && !!value.condition;
          }
        })
    }
    return isRequired;
  }

  onAddFreeText(term) {
    return term;
  }

  setInitialObject(configuration: FieldDetails) {
    switch (configuration.type) {
      case'date-time':
      case'date':
      case'time':
      case'zoned-time':
        this.object[configuration.id] = {type: "Europe/Paris", value: null}
        break;

      case'amount':
        this.object[configuration.id] = {type: "EUR", value: null}
        break;
      case 'measure':
        this.object[configuration.id] = {type: "DAY", value: null}
        break;
      default:
        this.object = {}
    }
  }

  deleteElement(toDelete, list) {
    if (!toDelete || !list) {
      return list;
    }
    list = list
      .filter(item => {
        return item !== toDelete;
      });
    this.changeField();
    return list;
  }

  isNumber(item) {
    let number = Number(item.value);
    return !isNaN(number)
  }

  changeField() {
    this.inputChanged.next(new Date());
  }

  getPlaceholder() {
    let manuellement = "Saisir";
    if (this.field._idSchemes && this.field._idSchemes.length > 0) {
      manuellement += ' avec le prefix (' + this.field._idSchemes.map(value => value + '-').join(", ") + ')'
    }
    if (this.field._idScheme) {
      manuellement += ' avec le prefix (' + this.field._idScheme + ')'
    }
    if (this.configuration.pattern?.value) {
      manuellement += ' avec le pattern (' + this.configuration.pattern.value + ')'
    }
    return manuellement;

  }

  searchByCodeAndLabel(term: string, item: any) {
    return true;
  }

  getList(list: string, search?: { term: string }) {
    this[list + 'Loading'] = true;
    if (this._readOnly) {
      this[list] = [];
    } else if (this.field?.surcharge?.dynamicListFrom || this.field?.surcharge?.staticList || this.field?.fieldIds?.length > 0) {
      this[list] = [];
      if (this.field?.surcharge?.staticList) {
        this[list].push(...this.field.surcharge.staticList.split(","));
      }
      if (this.field?.surcharge?.dynamicListFrom) {
        let fields = this.field.surcharge.dynamicListFrom.split(",");
        this.store.select(state => state.formaulaireReducer.formulaire).subscribe(value => {
          let listItem = [];
          findAllValuesOfKeysInObject(value, fields, listItem);
          this[list].push(...listItem)
        });
      }
      if (this.field.fieldIds?.length > 0) {
        this.store.select(state => state.formaulaireReducer.formulaire).subscribe(value => {
          findAllValuesOfKeysInObject(value, this.field.fieldIds, this[list]);
        });
      }
    } else if (this.configuration?.codeList?.value?.id && !this.configuration.attributes) {
      let key = this.configuration.codeList.value?.id;
      this.eformsSdkService.getCodeById(key, this.sdkVersion, this.translateService.currentLang, search?.term)
        .subscribe(ref => {
          if (ref) {
            let keys = Object.keys(ref);
            this[list] = ref[keys[0]];
          }
        })
    } else if (this.configuration.type === 'date-time' || this.configuration.type === 'date'
      || this.configuration.type === 'zoned-time' || this.configuration.type === 'time') {
      this[list] = zoneTime;
    } else {
      this.eformsSdkService.getFieldsOptions(this.field.id, this.sdkVersion, search?.term, this.translateService.currentLang)
        .subscribe(ref => {
          if (ref) {
            let keys = Object.keys(ref);
            this[list] = ref[keys[0]];
          }
        })
    }

    this[list + 'Loading'] = false;
  }


  getLength() {
    let element = this.object[this.field.id];
    if (!element) {
      return this.configuration.maxLength;
    }
    return this.configuration.maxLength - element.length
  }

  generateUuidV4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      // tslint:disable-next-line:no-bitwise
      const r = Math.random() * 16 | 0;
      // tslint:disable-next-line:no-bitwise
      const v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16).toUpperCase();
    })

  }

  changeDateField(date: any) {
    this.object[this.field.id]['value'] = date ? moment(date).utc(true).format("DD/MM/YYYY") : null;
    this.changeField()
  }

  isReadOnlyField() {
    return this.readOnly || isReadonly(this.field);
  }


  getTitleCondition() {
    let conditions = [];
    let forbidden = this.configuration?.forbidden;
    if (forbidden?.constraints && forbidden?.constraints?.length > 0) {
      forbidden.constraints
        .forEach(value => {
          if (value.noticeTypes.includes(this.noticeType) && !!value.condition && value.value) {
            conditions.push("Interdit : " + value.condition);
          }
        })
    }
    let mandatory = this.configuration?.mandatory;
    if (mandatory?.constraints && mandatory?.constraints?.length > 0) {
      mandatory.constraints
        .forEach(value => {
          if (value.noticeTypes.includes(this.noticeType) && !!value.condition && value.value) {
            conditions.push("Obligatoire : " + value.condition);
          }
        })
    }
    return conditions.join(" | ")
  }
}
