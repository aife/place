import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoticeModalWcComponent } from './notice-modal-wc.component';

describe('FormsPageComponent', () => {
  let component: NoticeModalWcComponent;
  let fixture: ComponentFixture<NoticeModalWcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoticeModalWcComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoticeModalWcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
