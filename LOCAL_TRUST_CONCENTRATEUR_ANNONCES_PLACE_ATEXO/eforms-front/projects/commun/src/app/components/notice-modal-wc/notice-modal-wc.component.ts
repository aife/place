import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {
  EFormsFormulaire,
  FailedAssertRepresentation,
  SubNotice,
  XmlStructureDetails
} from "@shared-global/core/models/api/eforms.api";
import {EformsNoticesService} from "@shared-global/core/services/eforms-notices.service";
import {State} from "@shared-global/store";
import {Store} from "@ngrx/store";
import {clearAlert, showErrorAlert, showInfoAlert, showSuccessAlert} from "@shared-global/store/alert/alert.action";
import {ActivatedRoute, Router} from "@angular/router";
import {isArray} from "lodash";
import {
  buildObjectFromXmlStructure,
  getNodeId,
  mergeJsonRecuresevily
} from "@shared-global/core/utils/technical/technical.utils";
import {NoticeInformationsComponent} from "@shared-global/components/notice-informations/notice-informations.component";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {showErrorToast, showSuccessToast} from "@shared-global/store/toast/toast.action";
import {TranslateService} from "@ngx-translate/core";
import {BsLocaleService} from "ngx-bootstrap/datepicker";

@Component({
  selector: 'atx-notice-modal',
  templateUrl: './notice-modal-wc.component.html',
  styleUrls: ['./notice-modal-wc.component.css']
})
export class NoticeModalWcComponent implements OnInit, OnDestroy {
  errors: FailedAssertRepresentation[];

  subNotice: SubNotice;
  showFormulaire = true;
  active: any;
  formulaireObject = {}
  object = {}
  savedNotice: EFormsFormulaire;
  readOnly = false;


  @Input() set id(id: number) {
    if (!!id) {
      this.eformsNoticesService.getNotice(id).subscribe(value => {
        this.savedNotice = value;
        this.readOnly = (value.statut !== 'DRAFT' && value.statut !== 'CONCENTRATEUR_VALIDATING_SIP'
          && value.statut !== 'CONCENTRATEUR_VALIDATING_ECO');
        this.object = JSON.parse(value.formulaire);
        this.formulaireObject = JSON.parse(value.formulaire);
        if (this.formulaireObject === null) {
          this.formulaireObject = {'ND-ROOT': {}}
        }
        this.getSubNoticeSchema()
      })
    }
  }

  selectedLang = 'en';
  langList = ["fr", "de", "en"]
  private bsModalRefInfo: any

  constructor(
    public bsModalRef: BsModalRef,
    private _router: Router,
    private route: ActivatedRoute, private modalService: BsModalService,
    public routingInterneService: RoutingInterneService,
    private readonly store: Store<State>,
    private bsLocaleService: BsLocaleService,
    private readonly translateService: TranslateService,
    private readonly eformsNoticesService: EformsNoticesService,
  ) {
    this.selectedLang = this.translateService.currentLang;
  }


  ngOnInit() {

  }

  getSubNoticeSchema() {
    this.eformsNoticesService.getNoticeSchema(this.savedNotice.id).subscribe(value => {
      this.subNotice = value;
      this.active = this.subNotice.content[0].id
    });

  }

  changeLang(lang: string) {
    this.selectedLang = lang;
    localStorage.setItem("eforms-language", lang)
    this.translateService.use(lang);
    this.bsLocaleService.use(lang);

  }

  closeModal() {
    this.bsModalRef.hide();
  }

  scroll(el: string, block: ScrollLogicalPosition) {
    const element = document.getElementById(el)
    if (element) element.scrollIntoView({behavior: 'smooth', block})
  }

  stop() {

    this.eformsNoticesService.stopPublication(this.savedNotice.id)
      .subscribe(value => {

        this.store.dispatch(showSuccessToast({
          message: "Votre avis est en cours d'arrêt",
          header: 'Annulation d\'avis'
        }));
        this.readOnly = true;
      }, (error) => {
        this.store.dispatch(showErrorToast({
          message: error,
          header: 'Annulation d\'avis'
        }))
      })

  }

  changer() {

    this.eformsNoticesService.modifierNotice(this.savedNotice.id)
      .subscribe(value => {
        this.savedNotice = value;
        this.store.dispatch(showSuccessToast({
          message: "Votre formulaire est créé",
          header: 'Changement de formulaire'
        }));
        this.routingInterneService.navigateFromContexte('agent/avis/' + value.id)
      }, (error) => {
        this.store.dispatch(showErrorToast({
          message: error,
          header: 'Changement de formulaire'
        }))
      })

  }

  reprendre() {
    this.eformsNoticesService.reprendre(this.savedNotice.id)
      .subscribe(value => {
        this.savedNotice = value;
        this.routingInterneService.navigateFromContexte('agent/avis/' + value.id)
      }, (error) => {
        this.store.dispatch(showErrorToast({
          message: error,
          header: 'Changement de formulaire'
        }))
      })

  }

  submit() {
    this.eformsNoticesService.submitNotice(this.savedNotice.id, this.object)
      .subscribe(value => {
        this.savedNotice = value;
        this.store.dispatch(showSuccessToast({
          message: "Votre formulaire est soumis",
          header: 'Soumission de formulaire'
        }))
        this.readOnly = true;
      }, (error) => {
        this.store.dispatch(showErrorToast({
          message: error,
          header: 'Soumission de formulaire'
        }))
      })

  }

  exportNotice() {
    this.eformsNoticesService.saveNotice(this.savedNotice.id, this.object)
      .subscribe(value => {
        this.savedNotice = value;
        const blob = new Blob([value.xmlGenere],
          {type: "application/xml"});
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", "notice-" + this.savedNotice.idConsultation + "-" + this.savedNotice.idNotice + "-" + this.savedNotice.uuid + ".xml");
        document.body.appendChild(link);
        link.click();
        /* eslint-disable */
        if (!!link.parentNode)
          link.parentNode.removeChild(link)
        this.store.dispatch(showSuccessToast({
          message: "Votre formulaire est exporté sous XML",
          header: 'Export XML'
        }))
      }, (error) => {
        this.store.dispatch(showErrorToast({
          message: "Votre formulaire n'est pas exporté",
          header: 'Export XML'
        }))
      })

  }

  validate() {
    this.store.dispatch(showInfoAlert({
      messages: ["Votre formulaire est en cours de validation..."],
      icon: 'la-info',
      keepAfterNavigationChange: false,
      clickable: false
    }))
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    this.eformsNoticesService.verifyNotice(this.savedNotice.id, this.object)
      .subscribe(value => {
        if (value.valid && value.validXml) {
          this.store.dispatch(showSuccessAlert({
            messages: ["Votre formulaire est valide"],
            icon: 'la-thumbs-up',
            keepAfterNavigationChange: false,
            clickable: false
          }))
        } else {

          this.errors = value.errors.map(value => {
            if (value.location) {
              const locations = value.location.split("/")
              locations[1] = '*';
              value.location = locations.join("/")
            }
            return value;
          })
          console.log(this.errors)
          this.store.dispatch(showErrorAlert({
            messages: this.errors.map(value1 => value1.text),
            icon: 'la-thumbs-down',
            keepAfterNavigationChange: false,
            clickable: true
          }))
        }
      }, (error) => {
        this.store.dispatch(showErrorAlert({
          messages: [error],
          icon: 'la-thumbs-down',
          keepAfterNavigationChange: false,
          clickable: false
        }))
      })

  }


  save() {
    this.eformsNoticesService.saveNotice(this.savedNotice.id, this.object)
      .subscribe(value => {
        this.savedNotice = value;
        this.store.dispatch(showSuccessToast({
          message: "Votre formulaire est sauvegardé",
          header: 'Sauvegarde Avis'
        }))

      }, (error) => {
        this.store.dispatch(showErrorToast({
          message: error,
          header: 'Sauvegarde Avis'
        }))
      })

  }


  changeGroupObject(event: { object: any, nodeIds: XmlStructureDetails[] }) {
    let {object} = buildObjectFromXmlStructure(event, event.nodeIds.length - 1)
    this.object = mergeJsonRecuresevily(this.object, object);
  }

  openMetaData() {
    this.bsModalRefInfo = this.modalService.show(NoticeInformationsComponent, {
      class: 'modal-full-width',
      backdrop: true,
      ignoreBackdropClick: false
    });
    this.bsModalRefInfo.content.subNotice = this.subNotice;
    this.bsModalRefInfo.content.formulaire = this.object['ND-Root'];
  }

  modifyList(event: { itemGroup: any; index: number; object: any }) {
    if (isArray(event.object[getNodeId(event.itemGroup)]) && event.index > -1) {
      let object;
      object = mergeJsonRecuresevily(this.object['ND-Root'][getNodeId(event.itemGroup)][event.index],
        event.object[getNodeId(event.itemGroup)][0]);
      this.object['ND-Root'][getNodeId(event.itemGroup)][event.index] = object;
    } else if (isArray(event.object[getNodeId(event.itemGroup)])) {
      this.object['ND-Root'][getNodeId(event.itemGroup)] = event.object[getNodeId(event.itemGroup)];
    } else {
      this.object['ND-Root'][getNodeId(event.itemGroup)] = [event.object[getNodeId(event.itemGroup)]];
    }
    console.log('modifyList', this.object)

  }

  ngOnDestroy(): void {
    this.store.dispatch(clearAlert());
    this._router.navigate([], {
      queryParams: {
        sectionId: null,
        subSectionId: null
      },
      queryParamsHandling: 'merge',
    });
  }
}
