import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SupportsService} from "@shared-global/core/services/supports.service";
import {
  AgentDTO,
  DashboardSuiviTypeAvisSpecification,
  OffreDTO,
  ReferentielDTO,
  StatutTypeAvisAnnonceEnum,
  SupportDTO
} from "@shared-global/core/models/api/eforms.api";
import {TypeAvisSearchModel} from "@shared-global/core/models/type-avis-search.model";
import {ReferentielsService} from "@shared-global/core/services/referentiels.service";
import {zoneTime} from "@shared-global/core/enums/app-internal-path.enum";
import moment from 'moment';

@Component({
  selector: 'atx-filtre-facturation',
  templateUrl: './filtre-facturation.component.html',
  styleUrls: ['./filtre-facturation.component.css']
})
export class FiltreFacturationComponent implements OnInit {

  @Output() onChangeFilter = new EventEmitter<DashboardSuiviTypeAvisSpecification>();
  supports: SupportDTO[];
  offres: OffreDTO[];
  procedures: ReferentielDTO[];
  @Input() showMore = false;

  @Input() statuts: Array<{ key: string, label: string, icon: string, value: StatutTypeAvisAnnonceEnum }> = []
  europeens: Array<{ label: string, icon: string, value: boolean }> =
    [{value: false, icon: 'la la-flag', label: 'national'}, {
      value: true,
      icon: 'la la-globe',
      label: 'europeen'
    }]

  @Input() filtre: DashboardSuiviTypeAvisSpecification = new TypeAvisSearchModel()
  servicesOrganismes: ReferentielDTO[];
  selectedServicesOrganismes: ReferentielDTO[];
  agents: AgentDTO[];

  constructor(private supportsService: SupportsService, private referentielsService: ReferentielsService) {

    this.supportsService.getSupports().subscribe(value => {
      this.supports = value
    }, () => this.supports = []);
    this.supportsService.getOffres().subscribe(value => {
      this.offres = value
    }, () => this.supports = []);
    this.referentielsService.getProcedures().subscribe(value => {
      this.procedures = value
    }, () => this.procedures = []);
    this.referentielsService.getOrganismes().subscribe(value => {
      this.servicesOrganismes = []
      value.forEach(organisme => {
        this.servicesOrganismes.push({
          libelle: organisme.sigleOrganisme + ' : ' + organisme.libelleOrganisme,
          code: organisme.sigleOrganisme,
          avisExterne: false,
          racine: true,
          id: null,
          idExterne: null
        })
        if (organisme.services) {
          organisme.services.forEach(service => {
            this.servicesOrganismes.push({
              libelle: organisme.sigleOrganisme + ' / ' + service.code + ' : ' + service.libelle,
              code: service.code,
              avisExterne: false,
              racine: false,
              id: null,
              idExterne: null
            })
          })
        }

      })
    }, () => this.servicesOrganismes = []);
    this.referentielsService.getAgents().subscribe(value => {
      this.agents = value
    }, () => this.agents = []);
  }

  ngOnInit(): void {
  }

  changeFilter(key: string, value: any) {
    if (this.filtre[key] === value) {
      this.filtre[key] = null;
    } else {
      this.filtre[key] = value;
    }
    this.rechercher()

  }

  changeStatus(value: any) {
    if (this.filtre.statuts.includes(value)) {
      this.filtre.statuts.splice(this.filtre.statuts.indexOf(value), 1);
    } else {
      this.filtre.statuts.push(value);
    }
    this.rechercher()

  }


  rechercher() {
    if (!!this.selectedServicesOrganismes) {
      this.filtre.organismes = this.selectedServicesOrganismes
        .filter(value => value.racine)
        .map(value => value.code)
      this.filtre.services = this.selectedServicesOrganismes
        .filter(value => !value.racine)
        .map(value => value.code)
    }
    this.onChangeFilter.emit(this.filtre);
  }

  reInitFilter() {
    this.filtre = new TypeAvisSearchModel(this.filtre.sip);
    this.rechercher();
  }

  deleteValidateur(item: any) {
    this.filtre.identifiantValidateurs = this.filtre.identifiantValidateurs.filter(value => value !== item.identifiant);
    this.rechercher();
  }

  deleteItem(item: any, field: string) {
    this.filtre[field] = this.filtre[field].filter(value => value !== item.code);
    this.rechercher();
  }

  deleteItemServices(item: any) {
    this.selectedServicesOrganismes = this.selectedServicesOrganismes.filter(value => value.code !== item.code);
    this.rechercher();
  }

  searchByNom(term: string, item: any) {
    term = term.toLocaleLowerCase();
    return item?.nom?.toLocaleLowerCase().indexOf(term) > -1
      || item?.prenom?.toLocaleLowerCase().indexOf(term) > -1;
  }

  searchByCode(term: string, item: any) {
    term = term.toLocaleLowerCase();
    return item?.code?.toLocaleLowerCase().indexOf(term) > -1
      || item?.libelle?.toLocaleLowerCase().indexOf(term) > -1;
  }

  protected readonly zoneTime = zoneTime;
  protected readonly moment = moment;
}
