import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltreFacturationComponent } from './filtre-facturation.component';

describe('FiltreFacturationComponent', () => {
  let component: FiltreFacturationComponent;
  let fixture: ComponentFixture<FiltreFacturationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FiltreFacturationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltreFacturationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
