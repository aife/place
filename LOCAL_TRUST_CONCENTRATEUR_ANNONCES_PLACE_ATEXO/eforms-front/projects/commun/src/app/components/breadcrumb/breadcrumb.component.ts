import {Component, EventEmitter, Input, Output} from '@angular/core';
import {BreadcrumbModel} from "@shared-global/core/models/breadcrumb.model";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {State} from "@shared-global/store";
import {Store} from "@ngrx/store";

@Component({
  selector: 'atx-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent {

  @Input() breadcrumb: BreadcrumbModel;
  @Input() information = false;
  @Input() loadingExport = false;
  @Input() loadingImport = false;
  @Input() openUrl: string;
  @Input() export = false;
  @Input() import = false;
  @Input() isForOpenUrl = false;
  @Output() onClickInformation = new EventEmitter<boolean>();
  @Output() onClickExport = new EventEmitter<Date>();
  @Output() onClickImport = new EventEmitter<File>();
  isWebComponent = true;

  constructor(private readonly routingInterneService: RoutingInterneService,
              private store: Store<State>) {
    this.store.select(state => state.userReducer.isWebComponent)
      .subscribe(value => {
        this.isWebComponent = value;
      })
  }

  navigateTo(link: string) {
    this.routingInterneService.navigateFromContexte(link);

  }

  exporter() {
    this.onClickExport.emit(new Date());
  }

  openLink() {
    if (!!this.openUrl) {
      window.open(this.openUrl, '_blank')
    }
  }

  openDialog() {
    document.getElementById('fileInput').click();
  }

  importer(files: any) {
    const file: File = files.target.files[0];
    this.onClickImport.emit(file);

  }

}
