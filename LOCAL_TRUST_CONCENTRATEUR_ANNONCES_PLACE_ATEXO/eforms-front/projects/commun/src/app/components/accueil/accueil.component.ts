import {Component, OnInit} from '@angular/core';
import {State} from "@shared-global/store";
import {Store} from "@ngrx/store";
import {Subject} from "rxjs";

@Component({
  selector: 'atx-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {
  private _unsubscribeAllMenu: Subject<any>;

  constructor(private readonly store: Store<State>) {
    this._unsubscribeAllMenu = new Subject();
  }

  ngOnInit(): void {

  }

}
