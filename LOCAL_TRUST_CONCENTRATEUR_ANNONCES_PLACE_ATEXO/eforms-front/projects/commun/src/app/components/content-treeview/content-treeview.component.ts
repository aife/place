import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {SubNotice, SubNoticeMetadata} from '@shared-global/core/models/api/eforms.api';
import {
  ModalSurchargeFieldComponent
} from '@shared-global/components/modal-surcharge-field/modal-surcharge-field.component';

@Component({
  selector: 'atx-content-treeview',
  templateUrl: './content-treeview.component.html',
  styleUrls: ['./content-treeview.component.scss']
})
export class ContentTreeviewComponent implements OnInit {

  @Input() metadata: SubNoticeMetadata[];
  @Input() niveau = 0;
  @Input() noticeId:string;
  @Input() noticelibelle:string;
  @Input() plateforme: string;


  bsModalRef!: BsModalRef;
  shouldShowChildren: boolean[] = [];


  constructor(private modalService: BsModalService) {

  }

  ngOnInit(): void {
    if (!!this.metadata) {
      this.metadata.forEach(chapitre => {
        this.shouldShowChildren.push(true);
      });
    }

  }

  hasChildren(chapitre: SubNoticeMetadata) {
    return chapitre.content?.length > 0 || chapitre.surcharge;
  }

  get clauseMarginLeft() {
    return 20 * (this.niveau + 1);
  }

  get chapitreMarginLeft() {
    return 20 * this.niveau;
  }

  showChildren(index: number) {
    this.shouldShowChildren[index] = !this.shouldShowChildren[index];
  }

  surcharger(field: SubNoticeMetadata) {

    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(ModalSurchargeFieldComponent, config);
    this.bsModalRef.content.plateforme = this.plateforme;
    this.bsModalRef.content.noticeId = this.noticeId;
    this.bsModalRef.content.noticelibelle = this.noticelibelle;
    this.bsModalRef.content.field = field;
    this.bsModalRef.content.onClose.subscribe(() => {
      this.bsModalRef = null;
    });
  }
}
