import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'atx-referentiel-select-item',
  templateUrl: './referentiel-select-item.component.html',
  styleUrls: ['./referentiel-select-item.component.css']
})
export class ReferentielSelectItemComponent implements OnInit {
  @Input() item: any;
  @Input() delete: boolean;
  @Input() withCode: boolean = false;
  @Output() onDeleteItem = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit(): void {
  }

}
