import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeAvisFormulaireDetailsComponent } from './type-avis-formulaire-details.component';

describe('NoticeDetailsComponent', () => {
  let component: TypeAvisFormulaireDetailsComponent;
  let fixture: ComponentFixture<TypeAvisFormulaireDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypeAvisFormulaireDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeAvisFormulaireDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
