import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {
  ConfigurationConsultationDTO,
  StatutSuiviAnnonceEnum,
  StatutTypeAvisAnnonceEnum,
  SuiviAnnonceDTO,
  SuiviTypeAvisPubDTO
} from '@shared-global/core/models/api/eforms.api';
import {AnnoncesTypeAvisService} from '@shared-global/core/services/annonces-type-avis.service';
import {AnnoncesService} from '@shared-global/core/services/annonces.service';
import {State} from '@shared-global/store';
import {Store} from '@ngrx/store';
import {saveFile} from '@shared-global/core/utils/download/doawnload.utils';
import {RoutingInterneService} from '@shared-global/core/services/routing-interne.service';
import {AppInternalPathEnum} from '@shared-global/core/enums/app-internal-path.enum';
import {showErrorToast, showSuccessToast, showWarningToast} from '@shared-global/store/toast/toast.action';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {EditMessageModalComponent} from '@shared-global/components/edit-message-modal/edit-message-modal.component';

@Component({
  selector: 'atx-type-avis-formulaire-details',
  templateUrl: './type-avis-formulaire-details.component.html',
  styleUrls: ['./type-avis-formulaire-details.component.css']
})
export class TypeAvisFormulaireDetailsComponent implements OnInit, OnDestroy {

  @Input() model: SuiviTypeAvisPubDTO;
  loadingImport = false;
  loadingSupprimer = false;
  loadingValidation = false;
  loadingRenew = false;
  private _showDetails = false;
  get showDetails() {
    return this._showDetails;
  }

  @Input() set showDetails(showDetails: boolean) {
    this._showDetails = showDetails;
    if (showDetails) {
      this.annoncesTypeAvisService.getAnnonce(this.model.id, this.model.idConsultation).subscribe(value => {
        this.model = value;
      });
      this.interval = setInterval(() => {
        this.annoncesTypeAvisService.getAnnonce(this.model.id, this.model.idConsultation).subscribe(value => {
          this.model = value;
        });
      }, 10000);
    } else {
      if (this.interval) {
        clearInterval(this.interval);
        this.interval = null;
      }
    }
  }

  @Input() typeValidation: StatutTypeAvisAnnonceEnum = null;
  @Output() updateTypeAvisAnnonce = new EventEmitter<SuiviTypeAvisPubDTO>();
  @Output() onExpandTypeAvisAnnonce = new EventEmitter<Date>();
  @Output() onRefreshTypeAvisAnnonce = new EventEmitter<Date>();
  @Output() onEditAnnonce = new EventEmitter<Date>();
  @Input() statuts: Array<{ key: string, label: string, icon: string, value: StatutTypeAvisAnnonceEnum }> = [];
  @Input() configuration: ConfigurationConsultationDTO;
  private interval: number;

  get annonceEuropeenne() {
    if (this.model && this.model.annonces)
      return this.model.annonces.find(value => value.support.europeen);
    return null;
  }

  get annoncesNationales() {
    if (this.model && this.model.annonces)
      return this.model.annonces.filter(value => !value.support.europeen);
    return [];
  }

  constructor(private routingInterne: RoutingInterneService,
              private modalService: BsModalService,
              private readonly annoncesTypeAvisService: AnnoncesTypeAvisService,
              private readonly annoncesService: AnnoncesService,
              private readonly store: Store<State>) {
  }

  downloadingPdfEuropeen = false;
  downloadingPdfNational = false;
  downloadingPdfPortail = false;
  loadingChangement = false;

  bsModalRef: BsModalRef;


  ngOnInit(): void {

  }


  isEditionOpen() {
    if (!this.model || !this.model.statutEdition) {
      return false;
    }
    return this.model.statutEdition !== 'CLOSED_WITHOUT_EDITING' && this.model.statutEdition !== 'SAVED_AND_CLOSED';
  }

  openAnnonce(annonce: SuiviAnnonceDTO) {
    if (annonce.support.europeen) {
      let page = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.NOTICES + '/' + annonce.idEForms;
      this.store.select(state => state.userReducer.accessToken).subscribe(value => {
        let prefix = location.host.indexOf('localhost') === -1 ? location.origin + '/publicite-annonces' : location.origin;
        let url = prefix + '/login/' + annonce.organismeMpe.plateformeUuid + '/'
          + annonce.organismeMpe.acronymeOrganisme + '/' + value + '?redirect=' + page;
        console.log(url);
        window.open(url, '_blank');

      });
    }
  }

  editAnnonce() {
    this.annoncesTypeAvisService.editAnnonce(this.model.id, this.model.idConsultation).subscribe(value => {
      window.open(value.url, '_blank');
      this.onRefreshTypeAvisAnnonce.emit(new Date());
    });

  }

  downloadPdf(annonce: SuiviAnnonceDTO, champ: string) {

    this[champ] = true;
    this.annoncesService.pdf(annonce).subscribe(value => {
        this[champ] = false;
        const contentDisposition = value.headers.get('Content-Disposition');
        console.log(value.headers, contentDisposition);

        if (contentDisposition) {
          // Extract the filename from the header
          const fileNameMatch = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/.exec(contentDisposition);

          if (fileNameMatch && fileNameMatch[1]) {
            const fileName = fileNameMatch[1].replace(/['"]/g, '');

            // Save the Blob as a file with the extracted filename
            saveFile(value.body, fileName);
          }
        } else if (annonce.avisFichier) {
          const filename = annonce.avisFichier.name;
          saveFile(value.body, filename);
        } else {
          const filename = 'avis_publicite_' + this.model.reference + (annonce.support.europeen ? '.pdf' : '.docx');
          saveFile(value.body, filename);
        }

      }, () => this[champ] = false
    );

  }

  changeExpanded() {
    this.onExpandTypeAvisAnnonce.emit(new Date());
  }

  supprimer() {
    this.loadingSupprimer = true;
    this.annoncesTypeAvisService.deleteAnnonce(this.model.id, this.model.idConsultation).subscribe(value => {
      this.onRefreshTypeAvisAnnonce.emit(new Date());
      this.loadingSupprimer = false;
    }, () => this.loadingSupprimer = false);


  }

  demanderValidation() {
    const valid = this.isConsultationValide();
    if (!valid) {
      this.store.dispatch(showWarningToast({
        message: 'Vous ne pouvez pas transmettre l\'avis de publicité "' + this.model.offreRacine.libelle + '" car la consultation n\'a pas été validée.',
        header: 'Erreur'
      }));
    } else {
      this.loadingValidation = true;
      this.annoncesTypeAvisService.demanderValidation(this.model.id, this.model.idConsultation).subscribe(value => {
        this.onRefreshTypeAvisAnnonce.emit(new Date());
        this.loadingValidation = false;
        this.store.dispatch(showSuccessToast({
          message: 'L\'avis de publicité "' + this.model.offreRacine.libelle + '" a été transmis avec succès',
          header: 'Avis de publicité transmis'
        }));
      }, () => {
        this.store.dispatch(showErrorToast({
          message: 'Une erreur est survenue lors de la transmission de l\'avis de publicité "' + this.model.offreRacine.libelle + '"',
          header: 'Avis de publicité transmis'
        }));
        this.loadingValidation = false;
      });
    }
  }

  reprendre() {
    this.loadingRenew = true;
    this.annoncesTypeAvisService.reprendre(this.model.id, this.model.idConsultation).subscribe(value => {
      this.onRefreshTypeAvisAnnonce.emit(new Date());
      this.loadingRenew = false;
      this.store.dispatch(showSuccessToast({
        message: 'L\'avis de publicité "' + this.model.offreRacine.libelle + '" a été repris avec succès',
        header: 'Avis de publicité repris'
      }));
    }, () => {
      this.store.dispatch(showErrorToast({
        message: 'Une erreur est survenue lors de la reprise de l\'avis de publicité "' + this.model.offreRacine.libelle + '"',
        header: 'Avis de publicité repris'
      }));
      this.loadingRenew = false;
    });

  }

  change() {
    this.loadingChangement = true;
    this.annoncesTypeAvisService.change(this.model.id, this.model.idConsultation).subscribe(value => {
      this.onRefreshTypeAvisAnnonce.emit(new Date());
      this.loadingChangement = false;
      this.store.dispatch(showSuccessToast({
        message: 'L\'avis de changement de publicité "' + this.model.offreRacine.libelle + '" a été créé avec succès',
        header: 'Avis de changement créé'
      }));
    }, () => {
      this.store.dispatch(showErrorToast({
        message: 'Une erreur est survenue lors de la modification de l\'avis de publicité "' + this.model.offreRacine.libelle + '"',
        header: 'Avis de changement créé'
      }));
      this.loadingChangement = false;
    });

  }


  getBadgeColor(statut: 'REJETE' | 'REJETE_ECO' | 'REJETE_SIP' | 'ENVOYE' | 'EXTERNAL' | 'BROUILLON' | 'ARCHIVER' | 'SUPPRIMER' | 'EN_ATTENTE' | 'EN_ATTENTE_VALIDATION_ECO' | 'EN_ATTENTE_VALIDATION_SIP' | 'VALIDER_ECO' | 'VALIDER_SIP' | 'REPRENDRE' | 'MODIFIER' | 'EN_COURS_DE_PUBLICATION' | 'PREPARATION_PUBLICATION' | 'ENVOI_PLANIFIER' | 'EN_COURS_D_ARRET' | 'REJETER_SUPPORT' | 'REJETER_CONCENTRATEUR' | 'REJETER_CONCENTRATEUR_VALIDATION_ECO' | 'REJETER_CONCENTRATEUR_VALIDATION_SIP' | 'PUBLIER' | 'ARRETER') {
    switch (statut) {
      case 'BROUILLON':
        return 'badge-warning';
      case 'ENVOI_PLANIFIER':
      case 'ENVOYE':
        return 'badge-success';
      case 'REJETE':
      case 'REJETE_ECO':
      case 'REJETE_SIP':
      case 'REJETER_CONCENTRATEUR':
      case 'REJETER_CONCENTRATEUR_VALIDATION_SIP':
      case 'REJETER_CONCENTRATEUR_VALIDATION_ECO':
      case 'REJETER_SUPPORT':
        return 'badge-danger';
      default:
        return 'badge-primary primary-client-bg-border';
    }
  }

  edit() {
    this.onEditAnnonce.emit(new Date());
  }

  isConsultationValide() {
    if (!this.configuration || !this.configuration.contexte || !this.configuration.contexte.consultation) {
      return false;
    }
    return !!this.configuration.contexte.consultation.statutCalcule && !this.configuration.contexte.consultation.statutCalcule.includes('0');

  }

  isCompleted() {
    return this.model.statut === 'BROUILLON' && this.model.annonces.every(value => value.statut !== 'BROUILLON');

  }

  ngOnDestroy(): void {
    if (this.interval) {
      clearInterval(this.interval);
      this.interval = null;
    }
  }

  openModal(readOnly: boolean) {
    this.bsModalRef = this.modalService.show(EditMessageModalComponent, {
      class: 'modal-full-width',
      backdrop: true,
      ignoreBackdropClick: false
    });
    this.bsModalRef.content.readOnly = readOnly;
    this.bsModalRef.content.avis = this.model;
    this.bsModalRef.content.mailList = this.annoncesNationales.map(value => value.support);
  }

  addAvisExterne(files: any) {
    this.loadingImport = true;
    const file: File = files.target.files[0];
    this.annoncesTypeAvisService.importAvisExterne(this.model.id, file, this.model.idConsultation).subscribe(value => {
      this.loadingImport = false;
      this.model = value;
    }, () => {
      this.loadingImport = false;
    });
  }

  openDialog() {
    document.getElementById('fileInput').click();
  }

  deleteAvisExterne() {
    if (!!this.annonceEuropeenne.avisFichier && !!this.annonceEuropeenne.avisFichier.id) {
      this.annoncesTypeAvisService.deleteAvisExterne(this.model.id, this.model.idConsultation).subscribe(value => {
        this.model = value;
      });
    }
  }

  canResent(annonce: SuiviAnnonceDTO) {
    if (!this.model.validerEco)
      return false;
    if (this.model.facturation && this.model.facturation.sip && !this.model.validerSip) {
      return false;
    }
    return ((annonce.statut === 'REJETER_SUPPORT' || (annonce.statut === 'ENVOI_PLANIFIER' && this.model.statut !== 'ENVOI_PLANIFIER')) && !annonce.support.europeen) || annonce.statut === 'REJETER_CONCENTRATEUR';
  }

  canResentNationales() {
    return this.annoncesNationales.some(value => this.canResent(value));
  }

  modifyAnnonce(annonce: SuiviAnnonceDTO, status: StatutSuiviAnnonceEnum) {
    this.annoncesService.modifyStatut(annonce.id, status).subscribe(value => {
      annonce = value;
      this.store.dispatch(showSuccessToast({
        message: 'type-avis.details.annonce.modification.success',
        header: 'type-avis.details.annonce.modification.header'
      }));
      this.onRefreshTypeAvisAnnonce.emit(new Date());
    });
  }

  modifyAnnonceNationales(status: StatutSuiviAnnonceEnum) {
    this.annoncesNationales.forEach(value => {
      if (this.canResent(value)) {
        this.modifyAnnonce(value, status);
      }
    });

  }

  getLibelleFromStatut(annonceDTO: SuiviAnnonceDTO) {
    switch (annonceDTO.statut) {
      case 'BROUILLON':
      case 'REPRENDRE':
        return 'A compléter';
      case 'EN_ATTENTE':
        return 'En attente d\'envoi';
      case 'ENVOI_PLANIFIER':
        return 'Envoi planifié';
      case 'REJETER_SUPPORT':
        return annonceDTO.support.europeen ? 'Rejeté par TED' : 'Rejeté';
      case 'REJETER_CONCENTRATEUR':
        return 'Rejeté';
      case 'PUBLIER':
        return 'Publié';
      case 'ARCHIVER':
        return 'Archivé';
      case 'SUPPRIMER':
        return 'Supprimé';
      case 'PREPARATION_PUBLICATION':
        return 'Préparation publication';
      case 'EN_COURS_DE_PUBLICATION':
        return 'Envoyé';
      case 'EN_COURS_D_ARRET':
        return 'En cours d\'arrêt';
      case 'ARRETER':
        return 'Arrêté';
      case 'REJETER_CONCENTRATEUR_VALIDATION_SIP':
        return 'Rejeté par SIP';
      case 'REJETER_CONCENTRATEUR_VALIDATION_ECO':
        return 'Rejeté par ECO';
      case 'ANNULER':
        return 'Annulé';
      case 'EN_ATTENTE_VALIDATION_ECO':
        return 'En attente de validation ECO';
      case 'EN_ATTENTE_VALIDATION_SIP':
        return 'En attente de validation SIP';
      default:
        return 'Complet';

    }
  }

  getbadgeFromStatut(annoncesNationale: SuiviAnnonceDTO) {
    switch (annoncesNationale.statut) {
      case 'BROUILLON':
      case 'REPRENDRE':
        return 'badge-warning';
      case 'EN_ATTENTE':
      case 'EN_ATTENTE_VALIDATION_SIP':
      case 'EN_ATTENTE_VALIDATION_ECO':
        return 'badge-primary primary-client-bg-border';
      case 'ENVOI_PLANIFIER':
      case 'PUBLIER':
      case 'COMPLET':
        return 'badge-success';
      case 'REJETER_SUPPORT':
      case 'REJETER_CONCENTRATEUR':
      case 'SUPPRIMER':
      case 'ARRETER':
      case 'EN_COURS_D_ARRET':
      case 'REJETER_CONCENTRATEUR_VALIDATION_SIP':
      case 'REJETER_CONCENTRATEUR_VALIDATION_ECO':
      case 'ANNULER':
        return 'badge-danger';
      default:
        return 'badge-success';

    }
  }

  downloadPdfPortail() {
    this.downloadingPdfPortail = true;
    this.annoncesTypeAvisService.pdfPortail(this.model.id, this.model.idConsultation).subscribe(value => {
        this.downloadingPdfPortail = false;
        const filename = 'avis_publicite_' + this.model.reference + '.pdf';
        saveFile(value, filename);
      }, () => this.downloadingPdfPortail = false
    );
  }

  hasAction() {
    return this.showDetails && (this.model.statut === 'BROUILLON' || this.model.statut === 'REJETE' || this.model.statut === 'REJETE_ECO' || this.model.statut === 'REJETE_SIP' ||
      (this.annonceEuropeenne && this.annonceEuropeenne.datePublication) ||
      (!!this.annoncesNationales && this.annoncesNationales.length > 0) ||
      this.previousTedUrl);
  }

  getTedIdentifiant() {

    let title = [];
    if (this.annonceEuropeenne) {
      if (this.annonceEuropeenne.numeroAvis) {
        title.push('Identifiant unique de l\'avis : ' + this.annonceEuropeenne.numeroAvis);
      } else if (this.annonceEuropeenne.numeroTed) {
        title.push('Identifiant unique de l\'avis : ' + this.annonceEuropeenne.numeroTed);

      }
      if (this.annonceEuropeenne.numeroTed) {
        title.push('Identifiant TED : ' + this.annonceEuropeenne.numeroTed);
      }

      if (this.annonceEuropeenne.numeroJoue) {
        title.push('Identifiant JOUE : ' + this.annonceEuropeenne.numeroJoue);
      }
      if (this.annonceEuropeenne.numeroAvisParent) {
        title.push('Identifiant unique de l\'avis précédant : ' + this.annonceEuropeenne.numeroAvisParent);
      }
    }
    return title.join(' | ');
  }

  get previousTedIdentifiant() {

    let identifiant = null;
    if (this.annonceEuropeenne) {
      if (this.annonceEuropeenne.numeroJoueParent) {
        identifiant = this.annonceEuropeenne.numeroJoueParent;
      } else if (this.annonceEuropeenne.numeroAvisParent) {
        identifiant = this.annonceEuropeenne.numeroAvisParent;
      }

    }
    return identifiant;
  }

  get tedIdentifiant() {

    let identifiant = null;
    if (this.annonceEuropeenne) {
      if (this.annonceEuropeenne.numeroJoue) {
        identifiant = this.annonceEuropeenne.numeroJoue;
      } else if (this.annonceEuropeenne.numeroAvis) {
        identifiant = this.annonceEuropeenne.numeroAvis;
      }

    }
    return identifiant;
  }

  get previousTedUrl(): string {
    if (this.annonceEuropeenne && this.annonceEuropeenne.lienPublicationParent) {
      return this.annonceEuropeenne.lienPublicationParent;
    }
    return null;
  }

  getStatut(model: SuiviTypeAvisPubDTO) {
    if (!!this.annonceEuropeenne) {
      if (!!this.annonceEuropeenne.datePublication && this.annonceEuropeenne.statut === 'PUBLIER')
        return 'PUBLIER';
      return model.statut;
    }
    if (!!this.annoncesNationales && this.annoncesNationales.length > 0 && this.annoncesNationales[0].datePublication && this.annoncesNationales[0].statut === 'PUBLIER') {
      return 'PUBLIER';
    }
    return model.statut;
  }

  canReprendre() {
    let avisRejete = this.model.statut === 'REJETE' || this.model.statut === 'REJETE_ECO' || this.model.statut === 'REJETE_SIP';
    if (!!this.annonceEuropeenne) {
      if (this.annonceEuropeenne.tedVersion === 'esentool')
        return false;
      if (this.annonceEuropeenne.statut === 'PUBLIER' || this.annonceEuropeenne.statut === 'EN_COURS_DE_PUBLICATION') {
        return false;
      }

    }
    return avisRejete;
  }

  openJoue() {
    if (!this.annonceEuropeenne) {
      return;
    }
    if (this.annonceEuropeenne.lienPublication) {
      window.open(this.annonceEuropeenne.lienPublication, '_blank');
    } else {
      this.downloadPdf(this.annonceEuropeenne, 'downloadingPdfEuropeen');
    }
  }

  protected readonly window = window;
}
