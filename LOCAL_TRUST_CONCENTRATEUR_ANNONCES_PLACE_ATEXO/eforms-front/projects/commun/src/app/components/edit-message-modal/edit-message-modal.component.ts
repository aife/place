import {Component, Input} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";
import {
  PieceJointeDTO,
  SuiviAnnonceDTO,
  SuiviTypeAvisPubDTO,
  SuiviTypeAvisPubPieceJointeAssoDTO,
  SupportDTO
} from "@shared-global/core/models/api/eforms.api";
import {AnnoncesTypeAvisService} from "@shared-global/core/services/annonces-type-avis.service";
import {saveFile} from "@shared-global/core/utils/download/doawnload.utils";
import {MailUpdateModel} from "@shared-global/core/models/mail-update";
import {AnnoncesService} from "@shared-global/core/services/annonces.service";

@Component({
  selector: 'atx-edit-message-modal',
  templateUrl: './edit-message-modal.component.html',
  styleUrls: ['./edit-message-modal.component.css']
})
export class EditMessageModalComponent {

  downloadingPdfParDefaut = false;
  mails: string;
  loadingImport = false;

  @Input() readOnly = false;

  get annoncesNationales(): Array<SuiviAnnonceDTO> {
    if (this.avis && this.avis.annonces)
      return this.avis.annonces.filter(value => !value.support.europeen);
    return [];
  }

  private _mailList: Array<SupportDTO>;

  @Input() set mailList(mailList: Array<SupportDTO>) {
    this._mailList = mailList;
    if (this._mailList) {
      this.mails = this._mailList.map(value => value.libelle + ' (' + value.mail + ')').join(", ");
    }
  }

  get mailList(): Array<SupportDTO> {
    return this._mailList;
  }

  private _avis: SuiviTypeAvisPubDTO;

  @Input() set avis(avis: SuiviTypeAvisPubDTO) {
    this._avis = avis;
    if (this._avis) {
      if (!!this._avis.annonces && this._avis.annonces.length > 0) {
        let nationales = this._avis.annonces.filter(value => !value.support.europeen);
        if (!this._avis.mailObjet) {
          this._avis.mailObjet = 'Avis d’adjudication ' + this.avis.reference + ' à publier dans [organe de presse]';
        }
        if (!this._avis.mailContenu) {
          this._avis.mailContenu = "Mesdames, Messieurs,\n" +
            "Veuillez trouver ci-joint un " + nationales[0].offre.libelle + " à faire publier dans votre prochaine édition."
        }

      }

      this.annoncesTypeAvisService.getDocuments(this.avis.id, this.avis.idConsultation).subscribe(value => {
        value.forEach(asso => {
          this.documents.push(asso);
        })

      })
    }
  }

  get avis(): SuiviTypeAvisPubDTO {
    return this._avis;
  }

  documents: Array<SuiviTypeAvisPubPieceJointeAssoDTO> = [];
  submitted = false;
  downloadingPdfPortail = false;


  constructor(public bsModalRef: BsModalRef, public annoncesService: AnnoncesService, public annoncesTypeAvisService: AnnoncesTypeAvisService) {
  }


  closeModal() {
    this.bsModalRef.hide();
  }

  enregistrer() {
    this.submitted = true;
    if (!!this.avis.mailObjet && !!this.avis.mailContenu) {
      let update: MailUpdateModel = new MailUpdateModel();
      update.objet = this.avis.mailObjet;
      update.contenu = this.avis.mailContenu;
      this.annoncesTypeAvisService.updateMail(this.avis.id, update, this.avis.idConsultation).subscribe(value => {
        this.avis = value;
        this.bsModalRef.hide();
      })
    }
  }

  delete(item: SuiviTypeAvisPubPieceJointeAssoDTO) {
    this.annoncesTypeAvisService.deletePieceJointe(this.avis.id, item.id, this.avis.idConsultation).subscribe(res => {
      this.annoncesTypeAvisService.getDocuments(this.avis.id, this.avis.idConsultation).subscribe(value => {
        this.documents = [];
        value.forEach(asso => {
          this.documents.push(asso);
        })

      })
    })
  }

  downlaodFile(item: PieceJointeDTO) {
    this.annoncesTypeAvisService.downloadFile(this.avis.id, item.id, this.avis.idConsultation).subscribe(blob => {
        let filename = item.name;
        saveFile(blob, filename);

      },
      (error: any) => {
        console.log('erreur', error);
      });
  }

  addFile(e: any) {
    this.loadingImport = true;
    const file: File = e.target.files[0];
    if (!!file) {
      this.annoncesTypeAvisService.importDocument(this.avis.id, file, this.avis.idConsultation).subscribe(value => {
        this.loadingImport = false;
        this.annoncesTypeAvisService.getDocuments(this.avis.id, this.avis.idConsultation).subscribe(value => {
          this.documents = [];
          value.forEach(asso => {
            this.documents.push(asso);
          })
        })
      }, error => this.loadingImport = false);
    }
  }


  downloadPdfParDefaut(annonce) {
    this.downloadingPdfParDefaut = true;
    this.annoncesService.pdf(annonce).subscribe(value => {
        this.downloadingPdfParDefaut = false;
        const contentDisposition = value.headers.get('Content-Disposition');

        if (contentDisposition) {
          // Extract the filename from the header
          const fileNameMatch = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/.exec(contentDisposition);

          if (fileNameMatch && fileNameMatch[1]) {
            const fileName = fileNameMatch[1].replace(/['"]/g, '');

            // Save the Blob as a file with the extracted filename
            saveFile(value.body, fileName);
          }
        } else {
          const filename = 'avis_publicite_' + this.avis.reference + '.docx';
          saveFile(value.body, filename);
        }

      }, () => this.downloadingPdfParDefaut = false
    )
  }

  openDialog() {
    document.getElementById('pieceJointe').click();
  }
}
