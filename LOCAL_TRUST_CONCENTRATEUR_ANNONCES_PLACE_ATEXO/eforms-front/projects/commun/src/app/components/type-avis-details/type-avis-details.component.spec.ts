import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeAvisDetailsComponent } from './type-avis-details.component';

describe('NoticeDetailsComponent', () => {
  let component: TypeAvisDetailsComponent;
  let fixture: ComponentFixture<TypeAvisDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypeAvisDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeAvisDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
