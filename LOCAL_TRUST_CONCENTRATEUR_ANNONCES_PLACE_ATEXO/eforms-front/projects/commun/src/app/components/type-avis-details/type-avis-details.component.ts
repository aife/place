import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {
  StatutSuiviAnnonceEnum,
  StatutTypeAvisAnnonceEnum,
  SuiviAnnonceDTO,
  SuiviTypeAvisPubDTO
} from '@shared-global/core/models/api/eforms.api';
import {AnnoncesTypeAvisService} from '@shared-global/core/services/annonces-type-avis.service';
import {AnnoncesService} from '@shared-global/core/services/annonces.service';
import {State} from '@shared-global/store';
import {Store} from '@ngrx/store';
import {showSuccessToast} from '@shared-global/store/toast/toast.action';
import {saveFile} from '@shared-global/core/utils/download/doawnload.utils';
import {RoutingInterneService} from '@shared-global/core/services/routing-interne.service';
import {RejetAvisModalComponent} from '@shared-global/components/rejet-avis-modal/rejet-avis-modal.component';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {
  TypeAvisRevisionModalComponent
} from '@shared-global/components/type-avis-revision-modal/type-avis-revision-modal.component';
import {EditMessageModalComponent} from '@shared-global/components/edit-message-modal/edit-message-modal.component';
import {
  TypeAvisChoixModalComponent
} from '@shared-global/components/type-avis-choix-modal/type-avis-choix-modal.component';

@Component({
  selector: 'atx-type-avis-details',
  templateUrl: './type-avis-details.component.html',
  styleUrls: ['./type-avis-details.component.css']
})
export class TypeAvisDetailsComponent implements OnInit {

  @Input() model: SuiviTypeAvisPubDTO;
  @Input() showDetails = false;
  @Input() typeValidation: StatutTypeAvisAnnonceEnum = null;
  @Output() updateTypeAvisAnnonce = new EventEmitter<SuiviTypeAvisPubDTO>();
  @Output() onExpandTypeAvisAnnonce = new EventEmitter<Date>();
  @Output() onRefreshTypeAvisAnnonce = new EventEmitter<Date>();
  @Input() statuts: Array<{ key: string, label: string, icon: string, value: StatutTypeAvisAnnonceEnum }> = [];

  downloadingAnnonce = new Map();
  bsModalRef!: BsModalRef;

  get annonceEuropeenne() {
    if (this.model && this.model.annonces)
      return this.model.annonces.find(value => value.support.europeen);
    return null;
  }

  get annoncesNationales() {
    if (this.model && this.model.annonces)
      return this.model.annonces.filter(value => !value.support.europeen);
    return [];
  }

  public get events(): any[] {
    if (!this.model) {
      return [];
    }
    let items: any[] = [];
    if (this.model.dlro) {
      items.push({
        title: 'type-avis.details.dlro',
        icon: 'la la-clock-o',
        badge: 'badge-primary primary-client-bg',
        date: new Date(this.model.dlro)
      });
    }
    if (this.model.dateMiseEnLigne) {
      items.push({
        title: 'type-avis.details.dateMiseEnLigne',
        icon: 'la la-calendar',
        badge: 'badge-primary primary-client-bg',
        date: new Date(this.model.dateMiseEnLigne)
      });
    }
    if (this.model.dateValidationEco) {
      items.push({
        title: 'type-avis.details.dateValidationEco',
        icon: this.model.validerEco ? 'la la-check-circle' : 'ft-alert-triangle',
        badge: 'badge-' + (this.model.validerEco ? 'success' : 'danger'),
        date: new Date(this.model.dateValidationEco)
      });
    }
    if (this.model.dateValidationSip) {
      items.push({
        title: 'type-avis.details.dateValidationSip',
        icon: this.model.validerSip ? 'la la-check-circle' : 'ft-alert-triangle',
        badge: 'badge-' + (this.model.validerSip ? 'success' : 'danger'),
        date: new Date(this.model.dateValidationSip)
      });
    }
    if (this.model.dateMiseEnLigneCalcule) {
      let date = new Date(this.model.dateMiseEnLigneCalcule);
      let past = (new Date().getTime() - date.getTime()) > 0;
      items.push({
        title: 'type-avis.details.dateMiseEnLigneCalcule',
        icon: past ? 'la la-calendar' : 'la la-rocket',
        badge: 'badge-primary primary-client-bg',
        date: date
      });
    }
    if (this.model.dateEnvoiNational) {
      items.push({
        title: 'type-avis.details.dateEnvoiNational',
        icon: 'la la-rocket',
        badge: 'badge-success',
        date: new Date(this.model.dateEnvoiNational)
      });
    }
    if (this.model.dateEnvoiEuropeen) {
      items.push({
        title: 'type-avis.details.dateEnvoiEuropeen',
        icon: 'la la-rocket',
        badge: 'badge-success',
        date: new Date(this.model.dateEnvoiEuropeen)
      });
    }
    if (this.model.dateEnvoiNationalPrevisionnel && !this.model.dateEnvoiNational ) {
      items.push({
        title: 'type-avis.details.dateEnvoiNationalPrevisionnel',
        icon: 'la la-rocket',
        badge: 'badge-success',
        date: new Date(this.model.dateEnvoiNationalPrevisionnel)
      });
    }

    if (this.model.dateEnvoiEuropeenPrevisionnel && !this.model.dateEnvoiEuropeen ) {
      items.push({
        title: 'type-avis.details.dateEnvoiEuropeenPrevisionnel',
        icon: 'la la-rocket',
        badge: 'badge-success',
        date: new Date(this.model.dateEnvoiEuropeenPrevisionnel)
      });
    }

    return items.sort((a, b) => a.date.getTime() - b.date.getTime());
  }

  constructor(private modalService: BsModalService,
              private routingInterne: RoutingInterneService, private readonly facturationService: AnnoncesTypeAvisService, private readonly annoncesService: AnnoncesService, private readonly store: Store<State>) {
  }


  ngOnInit(): void {
  }

  validate(valid) {
    let validation = this.typeValidation === 'EN_ATTENTE_VALIDATION_ECO' ? 'eco' : 'sip';
    if (valid) {
      this.facturationService.validate(this.model.id, validation, {
        valid: valid,
        raisonRefus: null
      }).subscribe(value => {
        this.model = value;
        this.updateTypeAvisAnnonce.emit(value);

      });
    } else {
      this.bsModalRef = this.modalService.show(RejetAvisModalComponent, {
        class: 'modal-full-width',
        backdrop: true,
        ignoreBackdropClick: false
      });
      this.bsModalRef.content.avisPub = this.model;
      this.bsModalRef.content.typeValidation = this.typeValidation;
      this.bsModalRef.onHide.subscribe(() => {
        this.updateTypeAvisAnnonce.emit(this.model);
        this.bsModalRef = null;
      });
    }
  }

  openModal(annonce: SuiviAnnonceDTO) {
    this.store.select(state => state.userReducer.accessToken).subscribe(value => {
      let page = '/agent/avis/' + annonce.idEForms;
      let prefix = location.host.indexOf('localhost') === -1 ? location.origin + '/publicite-annonces' : location.origin;
      let url = prefix + '/login/' + annonce.organismeMpe.plateformeUuid + '/'
        + annonce.organismeMpe.acronymeOrganisme + '/' + value + '?redirect=' + page;
      console.log(url);
      window.open(url, '_blank');
    });
  }

  openEditDocument(readOnly: boolean) {
    this.bsModalRef = this.modalService.show(EditMessageModalComponent, {
      class: 'modal-full-width',
      backdrop: true,
      ignoreBackdropClick: false
    });
    this.bsModalRef.content.readOnly = readOnly;
    this.bsModalRef.content.avis = this.model;
    this.bsModalRef.content.mailList = this.annoncesNationales.map(value => value.support);
  }

  downloadPdf(annonce: SuiviAnnonceDTO) {
    this.downloadingAnnonce.set(annonce.id, true);
    this.annoncesService.pdf(annonce).subscribe(value => {
      this.downloadingAnnonce.set(annonce.id, false);
      const contentDisposition = value.headers.get('Content-Disposition');

      if (contentDisposition) {
        // Extract the filename from the header
        const fileNameMatch = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/.exec(contentDisposition);

        if (fileNameMatch && fileNameMatch[1]) {
          const fileName = fileNameMatch[1].replace(/['"]/g, '');

          // Save the Blob as a file with the extracted filename
          saveFile(value.body, fileName);
        }
      } else if (annonce) {
        let filename = 'avis_publicite_' + this.model.reference + (annonce.support.europeen ? '.pdf' : '.docx');
        if (annonce.avisFichier) {
          filename = annonce.avisFichier.name;
        }
        saveFile(value.body, filename);
      }

    }, () => this.downloadingAnnonce.set(annonce.id, false));
  }

  changeExpanded() {
    this.onExpandTypeAvisAnnonce.emit(new Date());
  }

  modifyAnnonce(annonce: SuiviAnnonceDTO, status: StatutSuiviAnnonceEnum) {
    this.annoncesService.modifyStatut(annonce.id, status).subscribe(value => {
      annonce = value;
      this.store.dispatch(showSuccessToast({
        message: 'type-avis.details.annonce.modification.success',
        header: 'type-avis.details.annonce.modification.header'
      }));
      this.onRefreshTypeAvisAnnonce.emit(new Date());
    });
  }

  isActive(model: SuiviTypeAvisPubDTO, statut: {
    key: string;
    label: string;
    icon: string;
    value: StatutTypeAvisAnnonceEnum
  }) {
    //let map = model?.annonces.map(value => this.getStatutTypeAvisAnnonceEnum(value.statut));
    //   return model.statut === statut.value || map?.includes(statut.value);
    return model.statut === statut.value;
  }

  openTypeAvis(typeAvis) {
    this.bsModalRef = this.modalService.show(TypeAvisChoixModalComponent, {
      class: 'modal-full-width',
      backdrop: true,
      ignoreBackdropClick: false
    });
    this.bsModalRef.content.typeAvis = typeAvis;
    this.bsModalRef.content.idConsultation = this.model.idConsultation;
    this.bsModalRef.content.onlyJals = true;
    this.bsModalRef.onHide.subscribe(() => {
      this.onRefreshTypeAvisAnnonce.emit(new Date());
      this.bsModalRef = null;
    });
  }


  getStatutTypeAvisAnnonceEnum(statut: StatutSuiviAnnonceEnum): StatutTypeAvisAnnonceEnum {
    switch (statut) {
      case 'REJETER_CONCENTRATEUR_VALIDATION_SIP':
        return 'REJETE_SIP';
      case 'REJETER_CONCENTRATEUR_VALIDATION_ECO':
        return 'REJETE_ECO';
      case 'EN_COURS_D_ARRET':
      case 'ARRETER':
      case 'REJETER_SUPPORT':
      case 'REJETER_CONCENTRATEUR':
        return 'REJETE';
      case 'EN_ATTENTE_VALIDATION_SIP':
        return 'EN_ATTENTE_VALIDATION_SIP';
      case 'EN_ATTENTE_VALIDATION_ECO':
        return 'EN_ATTENTE_VALIDATION_ECO';
      case 'EN_COURS_DE_PUBLICATION':
      case 'PUBLIER':
        return 'ENVOYE';
      case 'EN_ATTENTE':
      case 'ENVOI_PLANIFIER':
        return 'ENVOI_PLANIFIER';
      case 'REPRENDRE':
      case 'BROUILLON':
      default:
        return 'BROUILLON';
    }
  }

  getStatuts(model: SuiviTypeAvisPubDTO) {
    if (!model?.facturation?.sip)
      return this.statuts.filter(value => value.value !== 'EN_ATTENTE_VALIDATION_SIP');
    return this.statuts;
  }

  canReject(model: SuiviTypeAvisPubDTO) {
    if (model?.facturation && model.facturation.sip && this.typeValidation == 'EN_ATTENTE_VALIDATION_SIP') {
      return model.statut === 'EN_ATTENTE_VALIDATION_SIP';
    }
    if (this.typeValidation === model.statut) {
      return true;
    }


    let annonce = model.annonces.find(value => value.tedVersion === 'esentool');
    return (annonce != null && annonce.statut === 'EN_COURS_DE_PUBLICATION') || model.statut === 'ENVOI_PLANIFIER';

  }

  canResent(annonce: SuiviAnnonceDTO) {
    if (!this.model.validerEco)
      return false;
    if (this.model.facturation && this.model.facturation.sip && !this.model.validerSip) {
      return false;
    }
    return (annonce.statut === 'REJETER_SUPPORT' && !annonce.support.europeen) || annonce.statut === 'REJETER_CONCENTRATEUR';
  }

  getHistorique() {
    this.bsModalRef = this.modalService.show(TypeAvisRevisionModalComponent, {
      class: 'modal-full-width',
      backdrop: true,
      ignoreBackdropClick: false
    });
    this.bsModalRef.content.avis = this.model;
  }

  openUrl(annonce: SuiviAnnonceDTO) {
    window.open(annonce.lienPublication, '_blank');
  }
}
