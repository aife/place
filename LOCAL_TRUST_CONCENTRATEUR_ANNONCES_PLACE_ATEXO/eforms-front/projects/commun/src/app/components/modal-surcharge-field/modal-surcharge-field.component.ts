import {Component, Input} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {Subject} from 'rxjs';
import {SubNoticeMetadata} from '@shared-global/core/models/api/eforms.api';
import {EformsSdkService} from '@shared-global/core/services/eforms-sdk.service';
import {EformsSurchargePatchModel} from '@shared-global/core/models/eforms-surcharge-patch.model';

@Component({
  selector: 'atx-surcharge-field',
  templateUrl: './modal-surcharge-field.component.html',
  styleUrls: ['./modal-surcharge-field.component.scss']
})
export class ModalSurchargeFieldComponent {
  private _field: SubNoticeMetadata;
  modification = false;
  patch = new EformsSurchargePatchModel();
  loading = false;

  @Input() set field(metadata: SubNoticeMetadata) {
    this._field = metadata;
    this.modification = metadata && !!metadata.surcharge;
    if (this.modification) {
      this.patch = new EformsSurchargePatchModel(metadata.surcharge);
    }
  }

  @Input() noticeId: string;
  @Input() plateforme: string;
  @Input() noticelibelle: string;

  get field() {
    return this._field;
  }

  onClose = new Subject<SubNoticeMetadata>();

  constructor(public bsModalRef: BsModalRef, private readonly eformsSdkService: EformsSdkService) {

  }

  closeModal() {
    this.bsModalRef.hide();
  }

  submit(toAllNotice: boolean) {
    if (toAllNotice === false) {
      this.patch.idNotice = this.noticeId;
    }
    this.loading = true;
    this.eformsSdkService.surcharger(this.plateforme, this.field.id, this.patch).subscribe((value) => {
      this.field.surcharge = value;
      this.onClose.next(this.field);
      this.bsModalRef.hide();

    }, () => {
      this.loading = false;
    });

  }

}
