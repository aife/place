import {Component, Input} from '@angular/core';
import {Store} from '@ngrx/store';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {
  AvisValidationPatch,
  StatutTypeAvisAnnonceEnum,
  SuiviTypeAvisPubDTO
} from '@shared-global/core/models/api/eforms.api';
import {AnnoncesTypeAvisService} from '@shared-global/core/services/annonces-type-avis.service';
import {State} from '@shared-global/store';
import {showErrorToast} from '@shared-global/store/toast/toast.action';

@Component({
    selector: 'atx-rejet-avis-modal',
    templateUrl: './rejet-avis-modal.component.html',
    styleUrls: ['./rejet-avis-modal.component.css']
})
export class RejetAvisModalComponent {
    @Input() avisPub: SuiviTypeAvisPubDTO;
    @Input() typeValidation: StatutTypeAvisAnnonceEnum = null;

    commentaire: string;
    refresh: boolean = false;

    constructor(private readonly store: Store<State>, public bsModalRef: BsModalRef,
                private readonly annoncesTypeAvisService: AnnoncesTypeAvisService) {

    }

    closeModal() {
        this.bsModalRef.hide();
    }

    invalidate() {


        let validation = this.typeValidation === 'EN_ATTENTE_VALIDATION_ECO' ? "eco" : 'sip';
        if (!this.commentaire || this.commentaire === '') {
            this.refresh = false;
            this.store.dispatch(showErrorToast({
                header: 'Rejet d\'avis',
                message: 'Le commentaire de rejet ne peut être vide'
            }));
            return;
        }
        const patch: AvisValidationPatch = {
            valid: false,
            raisonRefus: this.commentaire
        }
      const dateMiseEnLigneCalcule = this.avisPub.dateMiseEnLigneCalcule;
      if (this.avisPub.statut === 'EN_ATTENTE_VALIDATION_ECO' || this.avisPub.statut === 'EN_ATTENTE_VALIDATION_SIP') {
        this.annoncesTypeAvisService.validate(this.avisPub.id, validation, patch).subscribe(value => {
          this.closeModal();
        }, (error) => {
          this.refresh = false;
          this.store.dispatch(showErrorToast({
            header: 'Rejet d\'avis',
            message: error
          }));
        });
      } else {
        this.annoncesTypeAvisService.invalidateEco(this.avisPub.id, patch).subscribe(value => {
          this.sendNotif(value, dateMiseEnLigneCalcule);
          this.closeModal();
        }, (error) => {
            this.refresh = false;
            this.store.dispatch(showErrorToast({
              header: 'Rejet d\'avis',
              message: error
            }));
        });
      }
    }

  sendNotif(facturation: SuiviTypeAvisPubDTO, dateMiseEnLigneCalcule) {
    if (facturation.offreRacine.modificationDateMiseEnLigne === true && !!dateMiseEnLigneCalcule) {
      //check if date is after now
      const date = new Date(dateMiseEnLigneCalcule);
      const now = new Date(facturation.dateValidationEco);
      if (date >= now) {
        window.dispatchEvent(new CustomEvent('update-facturation-validation', {
          detail: {facturation},
          composed: true // Like this
        }));
        console.log('sendNotif', facturation);
        return;
      } else {
        console.log('date is not not after validation date');
      }
    } else {
      console.log('modificationDateMiseEnLigne is false or dateMiseEnLigneCalcule is null');
    }
  }
}
