import {
  AfterContentChecked,
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output
} from '@angular/core';
import {
  FailedAssertRepresentation,
  SubNoticeMetadata,
  XmlStructureDetails
} from '@shared-global/core/models/api/eforms.api';
import {isArray} from 'lodash';
import {
  buildObjectFromXmlStructure,
  equalsJson,
  getFailedAssertFromElements,
  getNode,
  getNodeId,
  isHidden,
  mergeJsonRecuresevily
} from '@shared-global/core/utils/technical/technical.utils';
import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';
import {MenuItem} from '@core-eforms/models/menu-settings.model';
import {EformsSdkService} from '@shared-global/core/services/eforms-sdk.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'atx-group-field-form',
  templateUrl: './group-field-form.component.html',
  styleUrls: ['./group-field-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GroupFieldFormComponent implements AfterViewInit, OnDestroy, AfterContentChecked {
  selectedIndex = 0;
  selectedSubIndex = 0;
  @Output() onChangeFieldVisibility = new EventEmitter<boolean>()
  items: MenuItem[] = [];


  @Input() set group(group: Array<SubNoticeMetadata>) {
    if (!group) {
      return;
    }
    this._group = group;
  }

  @Input() set reset(field: Date) {
    if (!field) {
      return;
    }
    this.object = {};
    this.objectList = [];
  }

  get group() {
    return this._group;
  }

  @Output() onChangeGroupObject = new EventEmitter<{ object: any, nodeIds: XmlStructureDetails[] }>()
  @Output() onSave = new EventEmitter<Date>()
  @Output() onModifyGroupList = new EventEmitter<{
    itemGroup: any,
    index: number,
    object: any,
    nodeIds: XmlStructureDetails[]
  }>()

  hasError = false;
  private _group: Array<SubNoticeMetadata> = [];
  public config: PerfectScrollbarConfigInterface = {wheelPropagation: false};
  object = {};
  objectList = [];
  refresh: Date;
  @Input() noticeType: string;

  @Input() noticeId: number;
  @Input() parentNode: string;
  private _showOnlyRequired = false;
  showAddBloc = false;
  @Input() readOnly = false;
  @Input() sdkVersion: string;
  @Input() chemin: string;

  @Input() set showOnlyRequired(showOnlyRequired: boolean) {
    this._showOnlyRequired = showOnlyRequired;
    this.setObjet();
  };

  get showOnlyRequired() {
    return this._showOnlyRequired;
  }

  private _errors: FailedAssertRepresentation[];
  @Input() set errors(errors: FailedAssertRepresentation[]) {
    this._errors = errors;
    this.setItems();
  }

  get errors() {
    return this._errors;
  }

  private _selectedError: string;
  @Input() set selectedError(error: string) {
    this._selectedError = error;
    if (this.niveau === 0) {
      this.items.forEach((value, index) => {
        value.errors.forEach(value1 => {
          if (value1.id === error) {
            this.selectItem(index, 0);
          }
        });
      })
    } else {
      // scroll to this component
      setTimeout(() => {
        const elementBt = document.getElementById('BT-' + error);
        if (!elementBt) {
          const element = document.getElementById('GR-' + error);
          if (!!element) {
            element.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
          }
        }
      }, 1000)
    }
  }

  get selectedError() {
    return this._selectedError;
  }

  active: any;

  @Input() set formulaire(formulaire: any) {
    this._formulaire = formulaire;
    this.setObjet();

  }

  get formulaire() {
    return this._formulaire;
  }

  private _formulaire: any;
  @Input() activeId: any;
  @Input() niveau: any;

  constructor(private sdkService: EformsSdkService, private _router: Router,
              private route: ActivatedRoute, private readonly cdf: ChangeDetectorRef
  ) {

  }

  ngOnDestroy(): void {
  }

  ngAfterContentChecked() {
    this.cdf.detectChanges();
  }

  ngAfterViewInit(): void {
    if (this.niveau === 0) {
      this.route.queryParams.subscribe(value => {
        if (value.sectionId) {
          if (value.subSectionId) {
            this.selectItem(Number(value.sectionId), Number(value.subSectionId))
          } else {
            this.selectItem(Number(value.sectionId), 0);
          }
        } else if (value.sectionId) {
          this.selectItem(0, 0)
        }
      })

    }
    this.setObjet();
  }


  private setObjet() {
    if (!this._formulaire)
      return;

    this._group.forEach(itemGroup => {
      let nodeId = getNodeId(itemGroup);
      if (!!nodeId) {
        const nodeObject = getNode(this.formulaire, nodeId, itemGroup.contentType);
        if (itemGroup._repeatable && nodeObject) {
          if (!equalsJson(nodeObject, this.objectList[this.chemin + itemGroup.id])) {
            this.objectList[this.chemin + itemGroup.id] = nodeObject;
          }
        } else if (nodeObject) {
          if (!equalsJson(nodeObject, this.object[this.chemin + itemGroup.id])) {
            this.object[this.chemin + itemGroup.id] = nodeObject;
          }
        }
      }
    })

    this.setItems();

  }

  async delete(index: any, itemGroup: SubNoticeMetadata) {
    let value = await this.sdkService.getNodesId(getNodeId(itemGroup), this.sdkVersion).toPromise();
    this.objectList[this.chemin + itemGroup.id].splice(index, 1);
    this.onModifyGroupList.emit({
      object: this.objectList[this.chemin + itemGroup.id],
      itemGroup: itemGroup,
      index: index,
      nodeIds: value
    })

  }


  changeFieldObject(event: { object: any, nodeIds: XmlStructureDetails[] }, item: SubNoticeMetadata) {
    let nodeId = getNodeId(item);
    let index = event.nodeIds.map(value1 => value1.id).indexOf(nodeId);
    let {object, nodeIds} = buildObjectFromXmlStructure(event, index);
    if (nodeId) {
      this.object[item.id] = mergeJsonRecuresevily(this.object[item.id], object);
    }
    this.onChangeGroupObject.emit({
      object: nodeId ? this.object[item.id] : object,
      nodeIds: nodeIds
    })

  }

  get groupDisplay(): SubNoticeMetadata[] {
    return this._group ? this._group.filter(value => value.displayType === 'GROUP' && !isHidden(value)) : [];
  }

  get sectionsDisplay(): SubNoticeMetadata[] {
    return this._group && this.niveau === 1 ? this._group.filter(value => value.displayType === 'SECTION' && !isHidden(value)) : [];
  }

  get sectionsVerticalDisplay(): SubNoticeMetadata[] {
    return this._group && this.niveau === 0 ? this._group.filter(value => value.displayType === 'SECTION' && !isHidden(value)) : [];
  }

  get fieldsDisplay(): SubNoticeMetadata[] {
    return this._group ? this._group.filter(value => value.contentType === 'field' && !isHidden(value)) : [];
  }

  changeSectionObject(event: { object: any, nodeIds: XmlStructureDetails[] }) {
    this.onChangeGroupObject.emit({
      object: event.object,
      nodeIds: event.nodeIds
    })

  }

  ajouterList(itemGroup, event: { object: any, nodeIds: XmlStructureDetails[] }) {
    let nodeId = getNodeId(itemGroup);
    let objectElementElement = isArray(event.object[nodeId]) ? event.object[nodeId][0] : event.object[nodeId];
    if (!this.objectList[this.chemin + itemGroup.id]) {
      this.objectList[this.chemin + itemGroup.id] = [];
    }
    this.objectList[this.chemin + itemGroup.id].push(JSON.parse(JSON.stringify(objectElementElement)));
    const object = {};
    object[nodeId] = this.objectList[this.chemin + itemGroup.id];
    this.onChangeGroupObject.emit({object, nodeIds: event.nodeIds});
    this.showAddBloc = false;
    setTimeout(() => this.showAddBloc = false, 500)

  }

  changeCardObject(event: { object: any, nodeIds: XmlStructureDetails[] }, item: SubNoticeMetadata) {
    let nodeId = getNodeId(item);
    let index = event.nodeIds.map(value1 => value1.id).indexOf(nodeId);

    let {object, nodeIds} = buildObjectFromXmlStructure(event, index);
    if (nodeId) {
      this.object[item.id] = mergeJsonRecuresevily(this.object[item.id], object);
    }
    this.onChangeGroupObject.emit({
      object: nodeId ? this.object[item.id] : object,
      nodeIds: nodeIds
    })
  }

  changeElementObject(event: { object: any, nodeIds: XmlStructureDetails[] }, item: SubNoticeMetadata, indexList) {
    let nodeId = getNodeId(item);
    let index = event.nodeIds.map(value1 => value1.id).indexOf(nodeId);

    let {object, nodeIds} = buildObjectFromXmlStructure(event, index);
    if (indexList > -1) {
      this.objectList[this.chemin + item.id][indexList] = mergeJsonRecuresevily(this.objectList[this.chemin + item.id][indexList], isArray(object[nodeId]) ? object[nodeId][0] : object[nodeId]);
    } else {
      this.objectList[this.chemin + item.id] = isArray(object[nodeId]) ? object[nodeId][0] : object[nodeId];
    }


    let obj = {};
    obj[nodeId] = this.objectList[this.chemin + item.id];
    this.onModifyGroupList.emit({
      object: obj,
      itemGroup: item,
      index: indexList,
      nodeIds: nodeIds
    })


  }


  getFormulaire(itemGroup: SubNoticeMetadata) {
    if (!this.objectList[this.chemin + itemGroup.id]) {
      this.setObjet();
    }

    if (itemGroup._repeatable && itemGroup.contentType !== 'field') {

      if (this.objectList[this.chemin + itemGroup.id]?.length > 0) {
        return this.objectList[this.chemin + itemGroup.id];
      } else return null;
    } else {
      let nodeId = getNodeId(itemGroup);
      return !nodeId || !this.formulaire ? this.formulaire : this.formulaire[nodeId]
    }
  }

  setItems() {
    let items = [];
    this.sectionsVerticalDisplay.forEach((value) => {
        if (value._repeatable) {
          let list = this.objectList[this.chemin + value.id];
          if (!list || list.length === 0) {
            // const idObject: object = getIdObject(value, 1);
            list = []
          }

          list.forEach((element, index) => {
            //   let errors = getFailedAssertFromElements(this.errors, value, list.length === 1 ? null : index);
            let errors = [];
            items.push({
              index: index,
              itemGroup: value,
              title: element[value._identifierFieldId],
              object: element,
              icon: 'la-plus',
              page: null,
              isOpen: true,
              hasError: errors?.length > 0,
              errors: errors,
              isSelected: false,
              badge: null,
              submenu: null,
              roles: [],
              section: null,
              deletable: list.length > 1
            })
          })
        } else {
//          let errors = getFailedAssertFromElements(this.errors, value, null);
          let errors = [];
          items.push({
            itemGroup: value,
            title: value._label,
            icon: null,
            page: null,
            isOpen: true,
            hasError: errors.length > 0,
            errors: errors,
            isSelected: false,
            badge: null,
            submenu: null,
            roles: [],
            section: null
          })
        }
      }
    );
    if (items.length > this.selectedIndex && this.selectedIndex > -1) {
      items[this.selectedIndex].isSelected = true;
    }
    this.items = items;
  }


  changeVerticalGroupObject(event: { object: any, nodeIds: XmlStructureDetails[] }) {
    let itemGroup = this.selectedGroup.itemGroup;
    let nodeId = getNodeId(itemGroup);
    let index = event.nodeIds.map(value1 => value1.id).indexOf(nodeId);
    let {object, nodeIds} = buildObjectFromXmlStructure(event, index);
    if (!itemGroup._repeatable || !nodeId) {
      this.onChangeGroupObject.emit({
        object: object,
        nodeIds: nodeIds
      })
    } else {
      if (nodeId && !object[nodeId]) {
        let obj = {};
        obj[nodeId] = object;
        this.onModifyGroupList.emit({
          object: obj,
          index: this.selectedGroup.index, itemGroup: this.selectedGroup.itemGroup,
          nodeIds: nodeIds
        })
      } else
        this.onModifyGroupList.emit({
          object: object,
          index: this.selectedGroup.index, itemGroup: this.selectedGroup.itemGroup,
          nodeIds: nodeIds
        })
    }

  }

  selectItem(sectionId, subSectionId) {
    if (sectionId !== this.selectedIndex) {
      this.selectedIndex = sectionId
      let subSectionList = this.selectedIndex > -1 ? this.selectedGroup?.itemGroup?.content.filter(value => value.displayType === 'SECTION' && !isHidden(value)) : [];
      if (subSectionList?.length > 0) {
        subSectionId = subSectionId ? subSectionId : 0
      } else {
        subSectionId = null;
      }
      this.selectedSubIndex = subSectionId;

      this._router.navigate([], {
        queryParams: {
          sectionId: this.selectedIndex,
          subSectionId: this.selectedSubIndex
        },
        queryParamsHandling: 'merge',
      });
    } else {
      let subSectionList = this.selectedIndex > -1 ? this.selectedGroup?.itemGroup?.content.filter(value => value.displayType === 'SECTION' && !isHidden(value)) : [];
      if (subSectionList?.length > 0) {
        subSectionId = subSectionId ? subSectionId : 0
      } else {
        subSectionId = null;
      }
      if (this.selectedSubIndex !== subSectionId) {
        this.selectedSubIndex = subSectionId;
        this._router.navigate([], {
          queryParams: {
            sectionId: this.selectedIndex,
            subSectionId: this.selectedSubIndex
          },
          queryParamsHandling: 'merge',
        });
      }
    }
    this.setItems()
  }

  get selectedFormulaire() {
    let result = null;
    if (this.selectedIndex < 0 || !this.selectedGroup) {
      result = null;
    } else {
      const item = this.selectedGroup.itemGroup;

      if (item._repeatable) {
        result = this.items[this.selectedIndex].object
      } else {
        let nodeId = getNodeId(item);
        if (!item._repeatable && !!nodeId) {
          const nodeObject = getNode(this.formulaire, nodeId, item.contentType);
          result = nodeObject ? nodeObject : this.formulaire;
        } else if (!item._repeatable && !nodeId) {
          result = this.formulaire;
        }
      }
    }
    return result;

  }

  get selectedGroup() {
    let menuItems = this.items;
    if (!this.selectedIndex)
      return menuItems?.length > 0 ? menuItems[0] : null;
    return menuItems[this.selectedIndex];
  }

  hasContent(itemGroup) {
    let filter = itemGroup?.content?.filter(value => {
      return !isHidden(value);
    });
    itemGroup.forceHidden = filter.length === 0;
    return filter?.length > 0;
  }

  modifyGroupList(event: { itemGroup: any, index: number, object: any, nodeIds: XmlStructureDetails[] }) {
    if (!!event.object[getNodeId(event.itemGroup)]) {
      this.onModifyGroupList.emit({
        object: event.object,
        nodeIds: event.nodeIds,
        itemGroup: event.itemGroup,
        index: event.index
      })
    } else {
      const object = {};
      object[getNodeId(event.itemGroup)] = event.object;
      this.onModifyGroupList.emit({
        object, nodeIds: event.nodeIds,
        itemGroup: event.itemGroup,
        index: event.index
      })
    }
  }


  onNextSection() {
    let subSectionList = this.selectedIndex > -1 ? this.selectedGroup?.itemGroup?.content.filter(value => value.displayType === 'SECTION' && !isHidden(value)) : [];
    let subIndex;
    if (subSectionList?.length > 0) {
      subIndex = !!this.selectedSubIndex ? this.selectedSubIndex + 1 : 1;
      if (subIndex < subSectionList.length && subIndex > 0) {
        this.selectItem(this.selectedIndex, subIndex);
        return;
      }

    }
    let index = this.selectedIndex + 1;
    if (index >= this.items.length) {
      index = 0;
    }
    this.selectItem(index, 0)
  }

  onPrecedentSection() {
    let subIndex;
    let subSectionList = this.selectedIndex > -1 ? this.selectedGroup?.itemGroup?.content.filter(value => value.displayType === 'SECTION' && !isHidden(value)) : [];
    if (subSectionList?.length > 0) {
      subIndex = this.selectedSubIndex - 1;
      if (subIndex > -1) {
        this.selectItem(this.selectedIndex, subIndex);
        return;
      }

    }
    let index = this.selectedIndex - 1;
    if (index < 0) {
      index = 0;
    }
    this.selectItem(index, 0)
  }

  changeSection(index: number) {
    this.onSave.emit(new Date());
    if (index < 0) {
      this.onPrecedentSection()
    } else {
      this.onNextSection();
    }
    this.setItems();
    this.cdf.detectChanges()
  }

  getNextIndex(itemGroup) {
    let item = this.getFormulaire(itemGroup);
    return !!item ? item.length + 1 : 1
  }

  setHidden(_group: Array<SubNoticeMetadata>) {
    if (!this._showOnlyRequired && _group) {
      _group.forEach(value => {
        value.forceHidden = false;
        if (value?.content?.length > 0) {
          this.setHidden(value.content)
        }
      });
      this.cdf.detectChanges();
    }
  }

  protected readonly getNodeId = getNodeId;
  protected readonly getFailedAssertFromElements = getFailedAssertFromElements;

  changeShowAddBloc($event: boolean, itemGroup: SubNoticeMetadata) {
    this.showAddBloc = $event;
    setTimeout(() => {
      if (this.showAddBloc) {
        const element = document.getElementById(this.chemin + itemGroup.id);
        element.scrollIntoView({
          behavior: 'smooth',
          block: 'start'
        });
      }
    }, 500);
  }

  getChemin() {
    if (this.selectedGroup.index >= 0)
      return this.chemin + this.selectedGroup.itemGroup.id + '[' + this.selectedGroup.index + ']/';
    return this.chemin + this.selectedGroup.itemGroup.id + '/';
  }
}
