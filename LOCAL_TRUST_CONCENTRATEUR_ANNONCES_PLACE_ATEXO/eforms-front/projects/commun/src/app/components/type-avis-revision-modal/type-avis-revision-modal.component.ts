import {Component, Input, OnInit} from '@angular/core';
import {
  EFormsFormulaire,
  RevisionDTO,
  SuiviTypeAvisPubDTO,
  SuiviTypeAvisPubRevisionDTO,
} from '@shared-global/core/models/api/eforms.api';
import {AnnoncesTypeAvisService} from '@shared-global/core/services/annonces-type-avis.service';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {EformsNoticesService} from '@shared-global/core/services/eforms-notices.service';
import {zip} from 'rxjs';

export class TypeAvisHistoriqueDTO {
    date: Date;
    statut: string;
    motifRejet: string;
    agent: string;
    support?: string;
}

@Component({
    selector: 'atx-type-avis-revision-modal',
    templateUrl: './type-avis-revision-modal.component.html',
    styleUrls: ['./type-avis-revision-modal.component.scss']
})
export class TypeAvisRevisionModalComponent implements OnInit {
  revisions: Array<TypeAvisHistoriqueDTO> = [];
    champs = ['statut', 'dateValidationNational', 'dateValidationEuropeen', 'avisExterne', 'mailObject', 'mailContenu',
        'dateEnvoiEuropeen', 'dateEnvoiNational', 'dateModificationStatut']

    private _avis: SuiviTypeAvisPubDTO;
    loading = false;

    @Input() set avis(avis: SuiviTypeAvisPubDTO) {
        this._avis = avis;
        if (avis) {
            this.loading = true;
          let annonceDTOS = this._avis.annonces.filter(value => {
            return !!value.idEForms;
          });
          if (annonceDTOS.length > 0) {

            zip(this.annoncesTypeAvisService.getAnnonceHistorique(avis.id, avis.idConsultation),
              this.eformsNoticesService.getNotice(annonceDTOS[0].idEForms)
            ).subscribe(([avis, eforms]) => {
              this.revisions = this.getRevisions(avis);
              this.getEformsRevision(eforms);
              this.revisions = this.revisions.sort((a, b) => {
                return new Date(a.date).getTime() - new Date(b.date).getTime();
              });
              this.loading = false;
            }, () => this.loading = false);
          } else {
            this.annoncesTypeAvisService.getAnnonceHistorique(avis.id, avis.idConsultation).subscribe(avis => {
              this.revisions = this.getRevisions(avis);
              this.revisions = this.revisions.sort((a, b) => {
                return new Date(a.date).getTime() - new Date(b.date).getTime();
              });
              this.loading = false;
            }, () => this.loading = false);
          }
        }
    }

  private getEformsRevision(eforms: EFormsFormulaire) {
    if (!!eforms.dateEnvoiSoumission) {
      this.revisions.push({
        date: eforms.dateEnvoiSoumission,
        statut: 'EFORMS_SUBMITTING',
        motifRejet: null,
        agent: null,
      });
    }
    if (!!eforms.dateSoumission) {
      this.revisions.push({
        date: eforms.dateSoumission,
        statut: 'EFORMS_SUBMITTED',
        motifRejet: null,
        agent: null,
      });
    }

    if (eforms.statut === 'STOPPED'
      || eforms.statut === 'PUBLISHING' || eforms.statut === 'DELETED'
      || eforms.statut === 'NOT_PUBLISHED' || eforms.statut === 'CONVERTED_FROM_PUBLISHED' || eforms.statut === 'ARCHIVED'
      || eforms.statut === 'VALIDATION_FAILED')
      this.revisions.push({
        date: eforms.dateModificationStatut,
        statut: 'EFORMS_' + eforms.statut,
        motifRejet: null,
        agent: null,
      });
    if (!!eforms.dateEnvoiAnnulation) {
      this.revisions.push({
        date: eforms.dateEnvoiAnnulation,
        statut: 'EFORMS_STOPPING',
        motifRejet: null,
        agent: null,
      });
    }
    if (!!eforms.publicationDate) {
      this.revisions.push({
        date: eforms.publicationDate,
        statut: 'EFORMS_PUBLISHED',
        motifRejet: null,
        agent: null,
      });
    }
  }

  get avis(): SuiviTypeAvisPubDTO {
        return this._avis;
    }

  constructor(public bsModalRef: BsModalRef, private readonly annoncesTypeAvisService: AnnoncesTypeAvisService,
              private readonly eformsNoticesService: EformsNoticesService) {
    }

    ngOnInit(): void {
    }

    getRevisionIcon(revision: TypeAvisHistoriqueDTO) {
        switch (revision.statut) {
            case "COMPLET_EUROPEEN":
            case "COMPLET_NATIONAL":
          case 'EFORMS_COMPLETED':
          case 'EFORMS_CONCENTRATEUR_VALIDATED':
          case 'EFORMS_SUBMITTED':
          case 'EFORMS_PUBLISHED':
                return "la-check"
            case "PUBLIER_EUROPEEN":
                return 'la-globe';
            case "CREATION":
                return 'la-save';
            case "ENVOI_PLANIFIER_NATIONAL":
                return "la-calendar"
            case "MODIFICATION":
            case "MODIFICATION_EUROPEEN":
            case "REPRENDRE":
            case "MODIFICATION_EUROPEEN_EXTERNE":
            case "MODIFICATION_NATIONAL":
            case "MODIFICATION_NATIONAL_MAIL_CONTENU":
            case "MODIFICATION_NATIONAL_MAIL_OBJET":
                return "la-edit"
            case "ENVOYE_EUROPEEN":
            case "ENVOYE_NATIONAL":
                return "la la-rocket"
            case "REJETE_SUPPORT_EUROPEEN":
            case "REJETE_SUPPORT_NATIONALREJETE_SUPPORT_NATIONAL":
            case "REJETE_SIP":
            case "REJETE_ECO":
          case 'EFORMS_CONCENTRATEUR_REJET_ECO':
          case 'EFORMS_CONCENTRATEUR_REJET_SIP':
          case 'EFORMS_STOPPED':
          case 'EFORMS_NOT_PUBLISHED':
          case 'EFORMS_VALIDATION_FAILED':
                return "la la-ban"
            case "TRANSMIS_ECO":
            case "TRANSMIS_SIP":
          case 'EFORMS_SUBMITTING':
          case 'EFORMS_PUBLISHING':
          case 'EFORMS_STOPPING':
                return "la la-hourglass-1"
            case "SUPPRESSION_EUROPEEN_EXTERNE":
          case 'EFORMS_DELETED':
                return "la la-trash"
            default:
                return "la-info";
        }

    }

  private getRevisions(avis: RevisionDTO<SuiviTypeAvisPubRevisionDTO>[]) {
    const revisions: Array<TypeAvisHistoriqueDTO> = new Array<TypeAvisHistoriqueDTO>();
    avis.filter(value => value.type === 'ADD' ||
      value.champs.some(item => this.champs.includes(item)))
      .forEach((value, index) => {
        this.getAction(value, index).forEach(value1 => revisions.push(value1));

      });

    if (revisions.length === 0) {
      revisions.push({
        date: this._avis.dateCreation,
        statut: 'CREATION',
        motifRejet: null,
        agent: this._avis.creerPar ? this._avis.creerPar.prenom + ' ' + this._avis.creerPar.nom.toUpperCase() : 'ANONYME ANONYME',
      });
      if (this._avis.statut === 'EN_ATTENTE_VALIDATION_ECO') {
        revisions.push({
          date: this._avis.dateModification,
          statut: 'TRANSMIS_ECO',
          motifRejet: null,
          agent: this._avis.modifierPar ? this._avis.modifierPar.prenom + ' ' + this._avis.modifierPar.nom.toUpperCase() : 'ANONYME ANONYME',
        });
      }
      if (this._avis.statut === 'EN_ATTENTE_VALIDATION_SIP' ||
        (this._avis.dateValidationNational && this._avis.validerEco && this._avis.facturation?.sip)) {
        revisions.push({
          date: this._avis.dateModification,
          statut: 'TRANSMIS_SIP',
          motifRejet: null,
          agent: this._avis.modifierPar ? this._avis.modifierPar.prenom + ' ' + this._avis.modifierPar.nom.toUpperCase() : 'ANONYME ANONYME',
        });
      }
      if (this._avis.dateEnvoiNationalPrevisionnel) {
        revisions.push({
          date: this._avis.dateEnvoiNationalPrevisionnel,
          statut: 'ENVOI_PLANIFIER_NATIONAL',
          motifRejet: null,
          agent: this._avis.modifierPar ? this._avis.modifierPar.prenom + ' ' + this._avis.modifierPar.nom.toUpperCase() : 'ANONYME ANONYME',
        });
      }
      if (this._avis.dateEnvoiEuropeenPrevisionnel) {
        revisions.push({
          date: this._avis.dateEnvoiEuropeenPrevisionnel,
          statut: 'ENVOI_PLANIFIER_EUROPEEN',
          motifRejet: null,
          agent: this._avis.modifierPar ? this._avis.modifierPar.prenom + ' ' + this._avis.modifierPar.nom.toUpperCase() : 'ANONYME ANONYME',
        });
      }

      if (this._avis.statut === 'REJETE_ECO') {
        revisions.push({
          date: this._avis.dateValidationEco,
          statut: 'REJETE_ECO',
          motifRejet: this._avis.raisonRefus,
          agent: this._avis.validateurEco ? this._avis.validateurEco.prenom + ' ' + this._avis.validateurEco.nom.toUpperCase() : 'ANONYME ANONYME',
        });
      }
      if (this._avis.statut === 'REJETE_SIP') {
        revisions.push({
          date: this._avis.dateValidationSip,
          statut: 'REJETE_SIP',
          motifRejet: this._avis.raisonRefus,
          agent: this._avis.validateurSip ? this._avis.validateurSip.prenom + ' ' + this._avis.validateurSip.nom.toUpperCase() : 'ANONYME ANONYME',
        });
      }
    }
    let modifierPar = this._avis.modifierPar;
    let agent = modifierPar ? modifierPar.prenom + ' ' + modifierPar.nom.toUpperCase() : 'ANONYME ANONYME';
    this._avis.annonces.filter(item => item.statut === 'REJETER_SUPPORT' || item.statut === 'REJETER_CONCENTRATEUR')
      .forEach(value => {
        revisions.push({
          date: value.dateDebutEnvoi ? value.dateDebutEnvoi : value.dateModification,
          statut: value.support.europeen ? 'REJETE_SUPPORT_EUROPEEN' : 'REJETE_SUPPORT_NATIONAL',
          motifRejet: value.messageStatut,
          support: value.support.libelle + (value.support.europeen ? '' : ' (' + value.support.mail + ' )'),
          agent: agent,
        });
      });

    this._avis.annonces
      .filter(value => value.statut === 'EN_COURS_DE_PUBLICATION' || value.statut === 'PUBLIER')
      .forEach(value => {
        revisions.push({
          date: value.dateDebutEnvoi ? value.dateDebutEnvoi : value.dateModification,
          statut: value.support.europeen ? 'ENVOYE_EUROPEEN' : 'ENVOYE_NATIONAL',
          support: value.support.libelle + (value.support.europeen ? '' : ' (' + value.support.mail + ' )'),
          motifRejet: null,
          agent: agent,
        });
      });
    this._avis.annonces
      .filter(value => value.statut === 'PUBLIER')
      .forEach(value => {
        revisions.push({
          date: value.datePublication ? value.datePublication : value.dateModification,
          statut: value.support.europeen ? 'ENVOYE_EUROPEEN' : 'ENVOYE_NATIONAL',
          support: value.support.libelle + (value.support.europeen ? '' : ' (' + value.support.mail + ' )'),
          motifRejet: null,
          agent: agent,
        });
      });
    return revisions;
  }

    getAction(revision: RevisionDTO<SuiviTypeAvisPubRevisionDTO>, index: number): TypeAvisHistoriqueDTO[] {
        let modifierPar = revision.item.modifierPar;
        let agent = modifierPar ? modifierPar.prenom + ' ' + modifierPar.nom.toUpperCase() : 'ANONYME ANONYME'
        let dateModification = revision.item.dateModification ? revision.item.dateModification : this._avis.dateModification
        if (revision.type === 'ADD') {
            return [{
                date: dateModification,
                statut: index === 0 ? 'CREATION' : 'REPRENDRE',
                motifRejet: null,
                agent: agent,
            }];
        }
        if (revision.champs.includes('dateEnvoiEuropeen')) {
            return [{
                date: this._avis.dateEnvoiEuropeen,
                statut: 'ENVOYE_EUROPEEN',
                motifRejet: null,
                agent: null,
            }]

        }


        if (revision.champs.includes('statut')) {
            switch (revision.item.statut) {
                case "REJETE":
                    let list = []
                    this._avis.annonces.filter(item => item.statut === 'REJETER_SUPPORT' || item.statut === 'REJETER_CONCENTRATEUR')
                        .forEach(value => {
                            list.push({
                                date: revision.item.dateModification,
                                statut: value.support.europeen ? 'REJETE_SUPPORT_EUROPEEN' : 'REJETE_SUPPORT_NATIONAL',
                                motifRejet: this._avis.raisonRefus,
                                support: value.support.libelle + (value.support.europeen ? '' : ' (' + value.support.mail + ' )'),
                                agent: agent,
                            });
                        })
                    return list;
                case "REJETE_ECO":
                    return [{
                        date: dateModification,
                        statut: 'REJETE_ECO',
                        motifRejet: revision.item.raisonRefus,
                        agent: agent,
                    }];
                case "REJETE_SIP":
                    return [{
                        date: dateModification,
                        statut: 'REJETE_SIP',
                        motifRejet: revision.item.raisonRefus,
                        agent: agent,
                    }];

                case "EN_ATTENTE_VALIDATION_ECO":
                    return [{
                        date: dateModification,
                        statut: 'TRANSMIS_ECO',
                        motifRejet: null,
                        agent: agent,
                    }];
                case "EN_ATTENTE_VALIDATION_SIP":
                    return [{
                        date: dateModification,
                        statut: 'TRANSMIS_SIP',
                        motifRejet: null,
                        agent: agent,
                    }];
                case "ENVOI_PLANIFIER":
                    return [{
                        date: dateModification,
                        statut: 'ENVOI_PLANIFIER_NATIONAL',
                        motifRejet: null,
                        agent: agent,
                    }];
                case "BROUILLON":
                    return [{
                        date: dateModification,
                        statut: 'REPRENDRE',
                        motifRejet: null,
                        agent: agent,
                    }];
                default:
                    return [];

            }
        }

        if (revision.champs.includes('dateValidationNational') && revision.item.dateValidationNational) {
            return [{
                date: dateModification,
                statut: 'COMPLET_NATIONAL',
                motifRejet: null,
                agent: agent,
            }];
        }
        if (revision.champs.includes('avisExterne') && revision.item.avisExterne) {
            return [{
                date: dateModification,
                statut: 'MODIFICATION_EUROPEEN_EXTERNE',
                motifRejet: null,
                agent: agent,
            }];
        }
        if (revision.champs.includes('avisExterne') && !revision.item.avisExterne) {
            return [{
                date: dateModification,
                statut: 'SUPPRESSION_EUROPEEN_EXTERNE',
                motifRejet: null,
                agent: agent,
            }];
        }
        if (revision.champs.includes('mailContenu')) {
            return [{
                date: dateModification,
                statut: 'MODIFICATION_NATIONAL_MAIL_CONTENU',
                motifRejet: null,
                agent: agent,
            }];
        }
        if (revision.champs.includes('mailObject')) {
            return [{
                date: dateModification,
                statut: 'MODIFICATION_NATIONAL_MAIL_OBJET',
                motifRejet: null,
                agent: agent,
            }];
        }
        if (revision.champs.includes('dateValidationEuropeen') && revision.item.dateValidationEuropeen) {
            return [{
                date: dateModification,
                statut: 'COMPLET_EUROPEEN',
                motifRejet: null,
                agent: agent,
            }];
        }

        return [];
    }

    getClass(revision: TypeAvisHistoriqueDTO) {

        switch (revision.statut) {

          case 'COMPLET_EUROPEEN':
          case 'COMPLET_NATIONAL':
          case 'PUBLIER_EUROPEEN':
          case 'ENVOYE_EUROPEEN':
          case 'ENVOYE_NATIONAL':
          case 'EFORMS_COMPLETED':
          case 'EFORMS_CONCENTRATEUR_VALIDATED':
          case 'EFORMS_SUBMITTED':
          case 'EFORMS_PUBLISHED':
            return 'timeline__event--type2';

          case 'REJETE_SUPPORT_EUROPEEN':
          case 'REJETE_SUPPORT_NATIONAL':
          case 'REJETE_SIP':
          case 'EFORMS_CONCENTRATEUR_REJET_ECO':
          case 'EFORMS_CONCENTRATEUR_REJET_SIP':
          case 'SUPPRESSION_EUROPEEN_EXTERNE':
          case 'EFORMS_STOPPED':
          case 'EFORMS_DELETED':
          case 'EFORMS_NOT_PUBLISHED':
          case 'EFORMS_VALIDATION_FAILED':
            return 'timeline__event--type3';
          case 'TRANSMIS_ECO':
          case 'EFORMS_CONCENTRATEUR_VALIDATING_ECO':
          case 'EFORMS_CONCENTRATEUR_VALIDATING_SIP':
          case 'TRANSMIS_SIP':
          case 'ENVOI_PLANIFIER_NATIONAL':
          case 'EFORMS_SUBMITTING':
          case 'EFORMS_STOPPING':
          case 'EFORMS_PUBLISHING':
                return 'timeline__event--type4';
            default:
                return 'timeline__event--type1';
        }

    }

    closeModal() {
        this.bsModalRef.hide();
    }

    getPrefix(revision: TypeAvisHistoriqueDTO) {
        switch (revision.statut) {
            case "COMPLET_EUROPEEN":
            case "ENVOYE_EUROPEEN":
            case "MODIFICATION_EUROPEEN":
            case "PUBLIER_EUROPEEN":
            case "REJETE_SUPPORT_EUROPEEN":
            case "MODIFICATION_EUROPEEN_EXTERNE":
            case "SUPPRESSION_EUROPEEN_EXTERNE":
          case  'EFORMS_COMPLETED':
          case 'EFORMS_CONCENTRATEUR_VALIDATING_ECO':
          case 'EFORMS_CONCENTRATEUR_VALIDATING_SIP':
          case 'EFORMS_CONCENTRATEUR_REJET_ECO':
          case 'EFORMS_CONCENTRATEUR_REJET_SIP':
          case 'EFORMS_DRAFT':
          case 'EFORMS_CONCENTRATEUR_VALIDATED':
          case 'EFORMS_SUBMITTING':
          case 'EFORMS_SUBMITTED':
          case 'EFORMS_STOPPED':
          case 'EFORMS_STOPPING':
          case 'EFORMS_PUBLISHING':
          case 'EFORMS_PUBLISHED':
          case 'EFORMS_DELETED':
          case 'EFORMS_NOT_PUBLISHED':
          case 'EFORMS_CONVERTED_FROM_PUBLISHED':
          case 'EFORMS_ARCHIVED':
          case 'EFORMS_VALIDATION_FAILED':
                return "Support OPOCE : ";
            case "COMPLET_NATIONAL":
            case "ENVOYE_NATIONAL":
            case "MODIFICATION_NATIONAL":
            case "MODIFICATION_NATIONAL_MAIL_CONTENU":
            case "MODIFICATION_NATIONAL_MAIL_OBJET":
                return "Support Presse : ";
            default:
                console.log(revision.statut)
                return this._avis.offreRacine.libelle + " : ";
        }
    }

}
