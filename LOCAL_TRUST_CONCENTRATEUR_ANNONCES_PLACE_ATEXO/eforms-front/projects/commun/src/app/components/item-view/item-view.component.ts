import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'atx-item-view',
  templateUrl: './item-view.component.html',
  styleUrls: ['./item-view.component.css']
})
export class ItemViewComponent {

  @Input() item: any

  fieldHeaders(item) {
    return item ? Object.keys(item).filter(value => !this.isObject(item[value])) : []
  }

  objectHeaders(item) {
    return item ? Object.keys(item).filter(value => this.isObject(item[value])) : []
  }

  isObject(itemElement: any) {
    return typeof itemElement === 'object';
  }

  get isArray() {
    return Array.isArray(this.item);
  }

  getItem(itemElement: any): Array<any> {
    return itemElement;
  }


  delete($event: number, value) {

  }
}
