import {Inject, Pipe, PipeTransform} from '@angular/core';
import {APP_BASE_HREF} from "@angular/common";

@Pipe({
  name: 'imageUrl'
})
export class ImageUrlPipe implements PipeTransform {


  constructor(@Inject(APP_BASE_HREF) private baseHref: string) {
  }

  transform(value: string): string {
    return location.origin + this.baseHref + '/' + value;
  }

}
