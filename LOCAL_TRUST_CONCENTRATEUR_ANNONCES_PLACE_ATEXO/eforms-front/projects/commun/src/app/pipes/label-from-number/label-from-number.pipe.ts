import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'labelFromNumber',
  pure: false
})
export class LabelFromNumberPipe implements PipeTransform {

  transform(label: any, number?: number): string {
    if (!label) return label;
    return !(number) || number < 2 ? label : label + 's';
  }
}
