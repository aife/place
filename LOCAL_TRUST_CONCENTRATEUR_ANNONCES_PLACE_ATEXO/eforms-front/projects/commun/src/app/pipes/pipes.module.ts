import {NgModule} from '@angular/core';
import {CommonModule, CurrencyPipe} from '@angular/common';
import {NumberFormatPipe} from "@shared-global/pipes/number-format/number-format.pipe";
import {ImageUrlPipe} from "@shared-global/pipes/image-url/image-url.pipe";
import {LabelFromNumberPipe} from "@shared-global/pipes/label-from-number/label-from-number.pipe";
import {SafeHtmlPipe} from "@shared-global/pipes/safe-html/safe-html.pipe";


@NgModule({
  declarations: [
    ImageUrlPipe, NumberFormatPipe, LabelFromNumberPipe, SafeHtmlPipe
  ],
  imports: [
    CommonModule
  ], providers: [ImageUrlPipe, NumberFormatPipe, LabelFromNumberPipe, CurrencyPipe, SafeHtmlPipe],
  exports: [ImageUrlPipe, NumberFormatPipe, LabelFromNumberPipe, SafeHtmlPipe]
})
export class PipesModule {
}
