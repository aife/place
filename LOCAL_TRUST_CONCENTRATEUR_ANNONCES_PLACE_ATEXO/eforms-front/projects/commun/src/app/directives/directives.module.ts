import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatchHeightDirective} from './match-height/match-height.directive';
import {DigitOnlyDirective} from './digit-only/digit-only.directive';
import {ReferentielPipe} from "@shared-global/directives/referential/referentiel.pipe";

@NgModule({
  declarations: [ReferentielPipe, MatchHeightDirective, DigitOnlyDirective],
  imports: [
    CommonModule
  ], exports: [ReferentielPipe, MatchHeightDirective, DigitOnlyDirective]
})
export class DirectivesModule {
}
