import {ReferentielPipe} from "@shared/pipes/referential/referentiel.pipe";


describe('CpvListPipe', () => {
  it('create an instance', () => {
    const pipe = new ReferentielPipe();
    expect(pipe).toBeTruthy();
  });
  it('should return code and libelle when codeCPV given', () => {
    const pipe = new ReferentielPipe();
    let code1 = {'id': 2, 'code': "code1", 'libelle': "libelle1",'libelleNiv2': "",'libelleNiv1': "", 'refAchat': null};
    let result = pipe.transform(code1);
    expect(result).toBe('libelle1');
    let result2 = pipe.transform(code1,true);
    expect(result2).toBe('libelle1 (code1)');
    let result3 = pipe.transform(code1,true,true);
    expect(result3).toBe('<b>code1</b> - libelle1');
  });

  it('should return code and libelle when ReferentielModel given', () => {
    const pipe = new ReferentielPipe();
    let ref1 = {'fils': null, selected: false, 'id': 2, 'code': "code1", 'libelle': "libelle1",'famille': "", 'legende': null};
    let result = pipe.transform(ref1);
    expect(result).toBe('libelle1');
    let result2 = pipe.transform(ref1,true);
    expect(result2).toBe('libelle1 (code1)');
    let result3 = pipe.transform(ref1,true,true);
    expect(result3).toBe('<b>code1</b> - libelle1');
  });

  it('should return code and libelle when Array given', () => {
    const pipe = new ReferentielPipe();
    let code1 = {'id': 2, 'code': "codeCPV1", 'libelle': "libelleCode1",'libelleNiv2': "",'libelleNiv1': "", 'refAchat': null};
    let ref1 = {'fils': null, selected: false, 'id': 2, 'code': "code1", 'libelle': "libelle1",'famille': "", 'legende': null};
    let tab = [code1,ref1];
    let result = pipe.transform(tab);
    expect(result).toBe('libelleCode1, libelle1');
    let result2 = pipe.transform(tab,true);
    expect(result2).toBe('libelleCode1 (codeCPV1), libelle1 (code1)');
    let result3 = pipe.transform(tab,true,true);
    expect(result3).toBe('<b>codeCPV1</b> - libelleCode1, <b>code1</b> - libelle1');
  });
  it('should return null when null given', () => {
    const pipe = new ReferentielPipe();
    let result = pipe.transform(null);
    expect(result).toBe(null);
  });

});
