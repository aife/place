import {Pipe, PipeTransform} from '@angular/core';
import {OffreDTO, ReferentielDTO, SupportDTO} from "@shared-global/core/models/api/eforms.api";

@Pipe({
  name: 'referentiel'
})
export class ReferentielPipe implements PipeTransform {

  transform(value: Array<OffreDTO | SupportDTO | ReferentielDTO> | OffreDTO | SupportDTO | ReferentielDTO, withCode?: boolean, inline?: boolean): unknown {
    if (!value) {
      return null;
    }
    if (Array.isArray(value)) {
      return value.filter(value1 => value1).map(value1 => {
        return this.getLabel(value1, withCode, inline);
      }).join(', ');
    } else {
      return this.getLabel(value, withCode, inline);
    }

  }


  private getLabel(value: OffreDTO | SupportDTO | ReferentielDTO, withCode: boolean, inline: boolean) {
    if (!withCode) {
      return value.libelle
    }
    if (inline)
      return '<b>' + value.code + '</b> - ' + value.libelle;
    return value.libelle + ' (' + value.code + ')'
  }
}
