import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateLoader, TranslateModule, TranslateStore} from "@ngx-translate/core";
import {HttpClient} from "@angular/common/http";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {LoginPageWCComponent} from './login-page-wc/login-page-wc.component';
import {ComponentsModule} from "@shared-global/components/components.module";
import {
  SessionExpiredPageWcComponent
} from "@pages-wc/public/session-expired-page-wc/session-expired-page-wc.component";
import {PipesModule} from "@shared-global/pipes/pipes.module";

export function HttpPublicLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/public-', '.json');
}


@NgModule({
    declarations: [LoginPageWCComponent, SessionExpiredPageWcComponent],
    imports: [
        CommonModule, TranslateModule, TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (HttpPublicLoaderFactory),
                deps: [HttpClient]
            }, isolate: true
        }), CommonModule, CommonModule, ComponentsModule, PipesModule
    ],
    providers: [TranslateStore],
    exports: [LoginPageWCComponent, SessionExpiredPageWcComponent]

})
export class PublicWcModule {
}
