import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginPageWCComponent } from './login-page-wc.component';

describe('LoginPageWCComponent', () => {
  let component: LoginPageWCComponent;
  let fixture: ComponentFixture<LoginPageWCComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginPageWCComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPageWCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
