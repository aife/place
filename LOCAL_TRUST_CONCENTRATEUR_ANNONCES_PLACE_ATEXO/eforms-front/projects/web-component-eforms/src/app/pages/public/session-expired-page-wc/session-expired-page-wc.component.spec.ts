import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionExpiredPageWcComponent } from './session-expired-page-wc.component';

describe('LoginPageWCComponent', () => {
  let component: SessionExpiredPageWcComponent;
  let fixture: ComponentFixture<SessionExpiredPageWcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SessionExpiredPageWcComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionExpiredPageWcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
