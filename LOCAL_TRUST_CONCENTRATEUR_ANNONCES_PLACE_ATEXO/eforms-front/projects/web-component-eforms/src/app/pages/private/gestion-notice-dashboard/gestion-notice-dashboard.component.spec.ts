import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionNoticeDashboardComponent } from './gestion-notice-dashboard.component';

describe('ValidationNoticeDashboardComponent', () => {
  let component: GestionNoticeDashboardComponent;
  let fixture: ComponentFixture<GestionNoticeDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionNoticeDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionNoticeDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
