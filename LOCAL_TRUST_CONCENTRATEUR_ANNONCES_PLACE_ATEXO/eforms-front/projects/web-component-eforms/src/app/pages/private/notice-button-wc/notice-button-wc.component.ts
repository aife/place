import {Component} from '@angular/core';
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {NoticeModalWcComponent} from "@shared-global/components/notice-modal-wc/notice-modal-wc.component";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-notice-button-wc',
    templateUrl: './notice-button-wc.component.html',
    styleUrls: ['./notice-button-wc.component.css']
})
export class NoticeButtonWcComponent {

    bsModalRef: BsModalRef;
    private params: { idConsultation: any; idNotice: any; lang: any };

    constructor(private modalService: BsModalService, private route: ActivatedRoute) {
        this.route.params.subscribe(params => {
            this.params = {
                idNotice: params.idNotice, idConsultation: params.idConsultation,
                lang: params.lang
            };


        });
    }


    openModal() {
       console.log(this.params)
        this.bsModalRef = this.modalService.show(NoticeModalWcComponent, {
            class: 'modal-full-width',
            backdrop: true,
            ignoreBackdropClick: false
        });
        this.bsModalRef.content.params = this.params;

    }
}
