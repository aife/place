import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AnnoncesService} from "@shared-global/core/services/annonces.service";
import {BreadcrumbModel} from "@shared-global/core/models/breadcrumb.model";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";

@Component({
  selector: 'app-notice-page',
  templateUrl: './notice-iframe-wc.component.html',
  styleUrls: ['./notice-iframe-wc.component.css']
})
export class NoticeIframeWcComponent implements OnInit {

  openUrl: string;
  compte: string = "";
  pageDescription: BreadcrumbModel = {
    mainlabel: 'breadcrumb.open',
    links: [{
      name: this.compte,
      isLink: false
    }]
  };
  loading: boolean = false;

  constructor(private route: ActivatedRoute, private annonceService: AnnoncesService) {

  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.annonceService.getRedirection(params.id).subscribe(value => {
        this.openUrl = value.redirection;
        this.compte = value.acheteur.email;
        this.pageDescription = {
          mainlabel: 'breadcrumb.open',
          links: [{
            name: this.compte,
            isLink: false
          }]
        };
      })

    });
  }


}
