import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AccueilPageWCComponent} from './accueil-page-wc/accueil-page-wc.component';
import {BibliotequeCommuneModule} from "@shared-global/app.module";
import {ComponentsModule} from "@shared-global/components/components.module";
import {BsLocaleService} from "ngx-bootstrap/datepicker";
import {TranslateModule} from "@ngx-translate/core";
import {NoticePageWcComponent} from "@pages-wc/private/notice-page-wc/notice-page-wc.component";
import {NgbTabsetModule} from "@ng-bootstrap/ng-bootstrap";
import {ModalModule} from 'ngx-bootstrap/modal';
import {NoticeDashboardComponent} from "@pages-wc/private/notice-dashboard/notice-dashboard.component";
import {MyNoticeDashboardComponent} from './my-notice-dashboard/my-notice-dashboard.component';
import {NoticeButtonWcComponent} from './notice-button-wc/notice-button-wc.component';
import {
  ValidationSipNoticeDashboardComponent
} from "@pages-wc/private/validation-sip-notice-dashboard/validation-sip-notice-dashboard.component";
import {
  ValidationNoticeDashboardComponent
} from "@pages-wc/private/validation-notice-dashboard/validation-notice-dashboard.component";
import {
  ValidationEcoNoticeDashboardComponent
} from "@pages-wc/private/validation-eco-notice-dashboard/validation-eco-notice-dashboard.component";
import {NgxLoadingModule} from "ngx-loading";
import {
  StatistiqueEcoNoticeDashboardComponent
} from "@pages-wc/private/statistique-eco-notice-dashboard/statistique-eco-notice-dashboard.component";
import {
  StatistiqueSipNoticeDashboardComponent
} from "@pages-wc/private/statistique-sip-notice-dashboard/statistique-sip-notice-dashboard.component";
import {NoticeIframeWcComponent} from "@pages-wc/private/notice-iframe-wc/notice-iframe-wc.component";
import {PipesModule} from "@shared-global/pipes/pipes.module";
import {
  GestionNoticeDashboardComponent
} from "@pages-wc/private/gestion-notice-dashboard/gestion-notice-dashboard.component";
import {
  PlateformeConfigurationPageWcComponent
} from "@pages-wc/private/plateforme-configuration-page-wc/plateforme-configuration-page-wc.component";
import {DirectivesModule} from "@shared-global/directives/directives.module";
import {NgSelectModule} from "@ng-select/ng-select";
import {
  AtexoConfigurationPageWcComponent
} from "@pages-wc/private/atexo-configuration-page-wc/atexo-configuration-page-wc.component";


@NgModule({
  declarations: [AtexoConfigurationPageWcComponent, PlateformeConfigurationPageWcComponent, GestionNoticeDashboardComponent, StatistiqueSipNoticeDashboardComponent, StatistiqueEcoNoticeDashboardComponent, AccueilPageWCComponent, ValidationSipNoticeDashboardComponent, ValidationEcoNoticeDashboardComponent, ValidationNoticeDashboardComponent, NoticePageWcComponent, NoticeDashboardComponent, MyNoticeDashboardComponent, NoticeButtonWcComponent, NoticeIframeWcComponent],
  exports: [
    AccueilPageWCComponent
  ],
  imports: [
    CommonModule,
    BibliotequeCommuneModule,
    FormsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
    TranslateModule,
    NgbTabsetModule,
    ModalModule.forRoot(),
    NgxLoadingModule,
    PipesModule,
    DirectivesModule,
    NgSelectModule
  ],
  providers: [BsLocaleService]
})
export class PrivateWcModule {

}
