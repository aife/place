import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatistiqueEcoNoticeDashboardComponent } from './statistique-eco-notice-dashboard.component';

describe('MyNoticeDashboardComponent', () => {
  let component: StatistiqueEcoNoticeDashboardComponent;
  let fixture: ComponentFixture<StatistiqueEcoNoticeDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatistiqueEcoNoticeDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatistiqueEcoNoticeDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
