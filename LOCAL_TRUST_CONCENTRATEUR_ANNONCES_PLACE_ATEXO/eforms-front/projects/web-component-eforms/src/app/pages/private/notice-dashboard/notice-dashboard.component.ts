import {Component, Input, OnInit} from '@angular/core';
import {EformsNoticesService} from "@shared-global/core/services/eforms-notices.service";
import {EFormsFormulaire} from "@shared-global/core/models/api/eforms.api";

@Component({
  selector: 'atx-notice-dashboard',
  templateUrl: './notice-dashboard.component.html',
  styleUrls: ['./notice-dashboard.component.css']
})
export class NoticeDashboardComponent implements OnInit {
  notices: EFormsFormulaire[];
  @Input() titre = 'Liste des formulaires européens';
  @Input() onlyOrganisme = false;

  constructor(
    private readonly eformsNoticesService: EformsNoticesService) {
  }

  ngOnInit(): void {
    this.eformsNoticesService.getNotices(this.onlyOrganisme).subscribe(value => {
      this.notices = value;
    })
  }


}
