import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidationNoticeDashboardComponent } from './validation-notice-dashboard.component';

describe('ValidationNoticeDashboardComponent', () => {
  let component: ValidationNoticeDashboardComponent;
  let fixture: ComponentFixture<ValidationNoticeDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValidationNoticeDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidationNoticeDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
