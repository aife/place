import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {
    DashboardSuiviTypeAvisSpecification,
    PageDTO,
    StatutTypeAvisAnnonceEnum,
    SuiviTypeAvisPubDTO
} from "@shared-global/core/models/api/eforms.api";
import {AnnoncesTypeAvisService} from "@shared-global/core/services/annonces-type-avis.service";
import {PageableModel} from "@shared-global/core/models/pageable.model";
import {BreadcrumbModel} from "@shared-global/core/models/breadcrumb.model";
import {TypeAvisSearchModel} from "@shared-global/core/models/type-avis-search.model";
import {saveFile} from "@shared-global/core/utils/download/doawnload.utils";

@Component({
    selector: 'atx-validation-notice-dashboard',
    templateUrl: './validation-notice-dashboard.component.html',
    styleUrls: ['./validation-notice-dashboard.component.css']
})
export class ValidationNoticeDashboardComponent implements OnInit {
    page: PageDTO<SuiviTypeAvisPubDTO>;
    total = 0;
    typeAvisPubs: SuiviTypeAvisPubDTO[];

    @Input() sortFieldsList: string[] = []
    @Input() titre = 'Validation des annonces';
    @Input() typeValidation: StatutTypeAvisAnnonceEnum = null;
    expandedMap: Map<number, boolean> = new Map<number, boolean>();
    private _showDetails = true;

    @Input() set showDetails(value: boolean) {
        this._showDetails = value;
        this.typeAvisPubs?.forEach(value1 => {
            this.expandedMap.set(value1.id, value);
        })
    }

    get showDetails(): boolean {
        return this._showDetails;
    }

    @Input() filtre: DashboardSuiviTypeAvisSpecification = new TypeAvisSearchModel();
    @Input() pageable: PageableModel = new PageableModel();
    @Input() statuts: Array<{ key: string, label: string, icon: string, value: StatutTypeAvisAnnonceEnum }> =
        [{key: 'V-ECO', value: 'EN_ATTENTE_VALIDATION_ECO', icon: 'la la-gavel', label: 'A_TRAITER'}, {
            key: 'AT',
            value: 'EN_ATTENTE_VALIDATION_SIP',
            icon: 'la la-hourglass-1',
            label: 'EN_ATTENTE_VALIDATION_SIP'
        },
            {key: 'EP', value: 'ENVOI_PLANIFIER', icon: 'la la-calendar', label: 'ENVOI_PLANIFIER'}, {
            key: 'E',
            value: 'ENVOYE',
            icon: 'la la-rocket',
            label: 'ENVOYE'
        },
            {key: 'ER', value: 'REJETE', icon: 'la la-ban', label: 'REJETE'},
            {key: 'R', value: 'REJETE_ECO', icon: 'la la-ban', label: 'REJETE_ECO'},
            {key: 'R', value: 'REJETE_SIP', icon: 'la la-ban', label: 'REJETE_SIP'}
        ]

    @Output() updateTypeAvisAnnonce = new EventEmitter<SuiviTypeAvisPubDTO>();
    loading = false;
    @Input() pageDescription: BreadcrumbModel = {
        mainlabel: 'validation-avis.titre',
        links: [{
            name: 'validation-avis.dashboard',
            isLink: false,
        }]
    };
    loadingExport = false;

    constructor(
        private readonly validationNoticesService: AnnoncesTypeAvisService) {
    }

    ngOnInit(): void {
        this.rechercher(this.filtre, this.pageable);
    }


    rechercher(filtre: DashboardSuiviTypeAvisSpecification, pageable) {
        this.filtre = filtre;
        this.pageable = pageable;
        this.loading = true;
        this.validationNoticesService.getNotices(filtre, pageable).subscribe(value => {
            this.loading = false;
            this.page = value;
            this.typeAvisPubs = value.content;
            this.typeAvisPubs?.forEach(value1 => {
                if (!this.expandedMap.has(value1.id)) {
                    this.expandedMap.set(value1.id, this.showDetails);
                }
            });
            this.total = value.totalElements;
        }, () => {
            this.loading = false;
            this.typeAvisPubs = []
        })
    }

    export() {
        this.loadingExport = true;
        let suffix = 'Tableau de bord des avis';
        let date = new Date();
        if (this.filtre.sip === true) {
            suffix += '_' + date.getUTCFullYear() + '_' + date.getMonth() + '_' + date.getDay() + '_SIP.xlsx'
        } else {
            suffix += '_' + date.getUTCFullYear() + '_' + date.getMonth() + '_' + date.getDay() + '_ECO.xlsx'
        }

        this.validationNoticesService.exportNotices(this.filtre).subscribe(value => {
            this.loadingExport = false;
            if (this.filtre.sip === true) {
                saveFile(value, suffix)
            } else {
                saveFile(value, suffix)
            }
        }, () => this.loadingExport = false)
    }
}
