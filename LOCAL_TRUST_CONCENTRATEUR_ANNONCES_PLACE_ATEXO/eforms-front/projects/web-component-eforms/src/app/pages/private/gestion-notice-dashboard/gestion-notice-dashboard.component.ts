import {Component, OnInit} from '@angular/core';
import {BreadcrumbModel} from "@shared-global/core/models/breadcrumb.model";
import {State} from "@shared-global/store";
import {Store} from "@ngrx/store";
import {ActivatedRoute} from "@angular/router";
import {AnnoncesService} from "@shared-global/core/services/annonces.service";
import {ConfigurationConsultationDTO, SuiviTypeAvisPubDTO} from "@shared-global/core/models/api/eforms.api";
import {AnnoncesTypeAvisService} from "@shared-global/core/services/annonces-type-avis.service";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {
    TypeAvisChoixModalComponent
} from "@shared-global/components/type-avis-choix-modal/type-avis-choix-modal.component";

@Component({
    selector: 'atx-gestion-notice-dashboard',
    templateUrl: './gestion-notice-dashboard.component.html',
    styleUrls: ['./gestion-notice-dashboard.component.css']
})
export class GestionNoticeDashboardComponent implements OnInit {
    loading = false;
    pageDescription: BreadcrumbModel = {
        mainlabel: 'breadcrumb.open',
        links: [{
            name: 'this.compte',
            isLink: false

        }]
    };
    bsModalRef!: BsModalRef;
    idConsultation: any;
    configuration: ConfigurationConsultationDTO;
    expandedMap: Map<number, boolean> = new Map<number, boolean>();
    avisPubDTOS: Array<SuiviTypeAvisPubDTO> = [];

    constructor(private readonly route: ActivatedRoute, private readonly store: Store<State>,
                private modalService: BsModalService,
                private readonly annoncesTypeAvisService: AnnoncesTypeAvisService,
                private readonly annoncesService: AnnoncesService) {
        this.route.params.subscribe(params => {
            this.idConsultation = params.idConsultation;

            this.annoncesService.getConfiguration(this.idConsultation).subscribe(res => {
                this.configuration = res;
                this.getAnnonces();
            })

        });
    }

    ngOnInit(): void {
    }


    getAnnonces() {
        this.loading = true;
        this.annoncesTypeAvisService.getConsultationNotices(this.idConsultation).subscribe(res => {
            res?.forEach(value1 => {
                if (!this.expandedMap.has(value1.id)) {
                    this.expandedMap.set(value1.id, false);
                }
            });
            this.avisPubDTOS = res;

            this.loading = false;
        }, () => {
            this.loading = false;
        })
    }

    openTypeAvis(typeAvis) {
        this.bsModalRef = this.modalService.show(TypeAvisChoixModalComponent, {
            class: "modal-full-width",
            backdrop: true,
            ignoreBackdropClick: false
        });
        this.bsModalRef.content.typeAvis = typeAvis;
        this.bsModalRef.content.idConsultation = this.idConsultation;
        this.bsModalRef.content.configuration = this.configuration;
        this.bsModalRef.onHide.subscribe(() => {
            this.getAnnonces();
            this.bsModalRef = null;
        })
    }



}
