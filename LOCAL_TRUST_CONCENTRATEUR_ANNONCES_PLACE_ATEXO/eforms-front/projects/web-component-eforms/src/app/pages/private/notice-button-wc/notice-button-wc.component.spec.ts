import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoticeButtonWcComponent } from './notice-button-wc.component';

describe('NoticeButtonWcComponent', () => {
  let component: NoticeButtonWcComponent;
  let fixture: ComponentFixture<NoticeButtonWcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoticeButtonWcComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoticeButtonWcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
