import {Component, OnInit} from '@angular/core';
import {
  DashboardSuiviTypeAvisSpecification,
  StatutTypeAvisAnnonceEnum,
  SuiviTypeAvisPubDTO
} from '@shared-global/core/models/api/eforms.api';
import {BreadcrumbModel} from '@shared-global/core/models/breadcrumb.model';
import {TypeAvisSearchModel} from '@shared-global/core/models/type-avis-search.model';
import {ActivatedRoute} from '@angular/router';
import {AppInternalPathEnum} from '@shared-global/core/enums/app-internal-path.enum';

@Component({
  selector: 'app-sip-notice-dashboard',
  templateUrl: './validation-sip-notice-dashboard.component.html',
  styleUrls: ['./validation-sip-notice-dashboard.component.css']
})
export class ValidationSipNoticeDashboardComponent implements OnInit {


  statuts: Array<{ key: string, label: string, icon: string, value: StatutTypeAvisAnnonceEnum }> =
    [{
      key: 'V-ECO',
      value: 'EN_ATTENTE_VALIDATION_ECO',
      icon: 'la la-hourglass-1',
      label: 'EN_ATTENTE_VALIDATION_ECO'
    }, {
      key: 'AT',
      value: 'EN_ATTENTE_VALIDATION_SIP',
      icon: 'la la-gavel',
      label: 'A_TRAITER'
    },
      {key: 'EP', value: 'ENVOI_PLANIFIER', icon: 'la la-calendar', label: 'ENVOI_PLANIFIER'}, {
      key: 'E',
      value: 'ENVOYE',
      icon: 'la la-rocket',
      label: 'ENVOYE'
    }, {key: 'R', value: 'REJETE_SIP', icon: 'la la-ban', label: 'REJETE_SIP'}, {
      key: 'ER',
      value: 'REJETE',
      icon: 'la la-ban',
      label: 'REJETE'
    }];

  filtre: DashboardSuiviTypeAvisSpecification;
  pageDescription: BreadcrumbModel = {
    mainlabel: 'validation-avis-sip.titre',
    links: [{
      name: 'validation-avis-sip.dashboard',
      isLink: true,
      link: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.VALIDATION_SIP_NOTICES
    }, {
      name: 'validation-avis-sip.search',
      isLink: false, link: ''
    }]
  };
  sortFieldsList: string[] = ['dateModification', 'dateCreation', 'statut', 'dateValidationSip', 'dateMiseEnLigne', 'dateMiseEnLigneCalcule', 'dateEnvoi'];

  constructor(private route: ActivatedRoute) {
    const statut: StatutTypeAvisAnnonceEnum = <'BROUILLON' | 'EN_ATTENTE_VALIDATION_ECO' | 'EN_ATTENTE_VALIDATION_SIP' | 'ENVOI_PLANIFIER' | 'ENVOYE' | 'REJETE'>this.route.snapshot.queryParamMap.get('statut');
    const offre: string = this.route.snapshot.queryParamMap.get('offre');
    this.filtre = new TypeAvisSearchModel(true, statut ? [statut] : [], offre ? [offre] : []);

  }

  ngOnInit(): void {
  }

  sendNotif(facturation: SuiviTypeAvisPubDTO) {
    if (facturation.offreRacine.modificationDateMiseEnLigne === true && !!facturation.dateMiseEnLigneCalcule) {
      //check if date is after now
      const date = new Date(facturation.dateMiseEnLigneCalcule);
      const now = new Date(facturation.dateValidationSip);
      if (date >= now) {
        window.dispatchEvent(new CustomEvent('update-facturation-validation', {
          detail: {facturation},
          composed: true // Like this
        }));
        console.log('sendNotif', facturation);
        return;
      } else {
        console.log('date is not not after validation date');
      }
    } else {
      console.log('modificationDateMiseEnLigne is false or dateMiseEnLigneCalcule is null');
    }
  }
}
