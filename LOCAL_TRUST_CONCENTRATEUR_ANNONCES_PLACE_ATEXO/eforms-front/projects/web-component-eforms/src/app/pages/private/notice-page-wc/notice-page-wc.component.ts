import {Component, OnDestroy, OnInit} from '@angular/core';
import {BreadcrumbParamsConst} from '@core-eforms/constantes/breadcrumb-params.const';
import {
  EFormsFormulaire,
  FailedAssertRepresentation,
  SubNotice,
  XmlStructureDetails
} from '@shared-global/core/models/api/eforms.api';
import {EformsNoticesService} from '@shared-global/core/services/eforms-notices.service';
import {State} from '@shared-global/store';
import {Store} from '@ngrx/store';
import {clearAlert, showErrorAlert, showInfoAlert, showSuccessAlert} from '@shared-global/store/alert/alert.action';
import {ActivatedRoute, Router} from '@angular/router';
import {isArray} from 'lodash';
import {
  buildObjectFromXmlStructure,
  getNodeId,
  mergeJsonRecuresevily
} from '@shared-global/core/utils/technical/technical.utils';
import {RoutingInterneService} from '@shared-global/core/services/routing-interne.service';
import {showErrorToast, showSuccessToast} from '@shared-global/store/toast/toast.action';
import {saveFile} from '@shared-global/core/utils/download/doawnload.utils';
import {setFormulaire} from '@shared-global/store/formulaire/formulaire.action';
import {Base64} from 'js-base64';

@Component({
  selector: 'app-notice-page',
  templateUrl: './notice-page-wc.component.html',
  styleUrls: ['./notice-page-wc.component.css']
})
export class NoticePageWcComponent implements OnInit, OnDestroy {
  param = BreadcrumbParamsConst.FORMULAIRE;
  errors: FailedAssertRepresentation[];
  subNotice: SubNotice;
  showOnlyRequired = false;
  back = false;
  readOnly = false;
  loading = false;
  showFormulaire = true;
  active: any;
  formulaireObject = {}
  object = {}
  savedNotice: EFormsFormulaire;
  extended = false;
  downloadingAnnonce = false;
  loadingSave = false;
  loadingExport = false;
  loadingExportSVRL = false;
  loadingVerification = false;
  loadingValidation = false;
  loadingStopping = false;
  loadingRenouvellement = false;
  loadingReprendre = false;
  selectedError: any;
  svrlString: string;


  constructor(
    private _router: Router, private route: ActivatedRoute,
    public routingInterneService: RoutingInterneService,
    private readonly store: Store<State>,
    private readonly eformsNoticesService: EformsNoticesService) {

  }

  ngOnInit() {
    this.back = this._router.navigated;
    this.route.queryParams.subscribe(value => {
      //   this.showOnlyRequired = value.showOnlyRequired === 'true';
      if (value.subSectionId || value.sectionId) {
        this.extended = false;
      }
    })

    this.route.params.subscribe(params => {

      this.eformsNoticesService.getNotice(params.id).subscribe(value => {
        this.savedNotice = value;
        this.readOnly = (value.statut !== 'DRAFT')
        //decode base 64 the formulaire
        let formulaire = Base64.fromBase64(value.formulaire);
        this.object = JSON.parse(formulaire);
        this.formulaireObject = JSON.parse(formulaire);
        if (value.svrlString)
          this.svrlString = Base64.fromBase64(value.svrlString);
        this.store.dispatch(setFormulaire({formulaire: JSON.parse(JSON.stringify(this.formulaireObject))}))
        console.log('start this.formulaireObject', this.formulaireObject)
        if (this.formulaireObject === null) {
          this.formulaireObject = {'ND-ROOT': {}}
        }
        this.getSubNoticeSchema()
      })

    });
  }

  getSubNoticeSchema() {
    this.loading = true;
    this.eformsNoticesService.getNoticeSchema(this.savedNotice.id).subscribe(value => {
      this.subNotice = value;
      this.active = this.subNotice.content[0].id
      this.loading = false;
      this.store.select(state => state.alertReducer.selectedMessage).subscribe(alert => {
        if (alert >= 0 && alert != null) {
          this.selectedError = this.errors[alert].id;
          console.log('selectedError', this.selectedError)
        }
      });
    }, error => {
      this.loading = false;
    });

  }

  submit() {
    this.eformsNoticesService.submitNotice(this.savedNotice.id, this.object)
      .subscribe(value => {
        this.savedNotice = value;
        this.store.dispatch(showSuccessToast({
          message: "Votre formulaire est soumis",
          header: 'Soumission de formulaire'
        }));
        this.readOnly = true;
      }, (error) => {
        this.store.dispatch(showErrorToast({
          message: error,
          header: 'Soumission de formulaire'
        }))
      })

  }

  stop() {
    this.loadingStopping = true;
    this.eformsNoticesService.stopPublication(this.savedNotice.id)
      .subscribe(value => {
        this.loadingStopping = false;
        this.store.dispatch(showSuccessToast({
          message: "Votre avis est en cours d'arrêt",
          header: 'Annulation d\'avis'
        }));
        this.readOnly = true;
      }, (error) => {
        this.loadingStopping = false;
        this.store.dispatch(showErrorToast({
          message: error,
          header: 'Annulation d\'avis'
        }))
      })

  }

  changer() {
    this.loadingRenouvellement = true;
    this.eformsNoticesService.modifierNotice(this.savedNotice.id)
      .subscribe(value => {
        this.loadingRenouvellement = false;
        this.store.dispatch(showSuccessToast({
          message: "Votre formulaire est créé",
          header: 'Changement de formulaire'
        }));
        this.routingInterneService.navigateFromContexte('agent/avis/' + value.id)
      }, (error) => {
        this.loadingRenouvellement = false;
        this.store.dispatch(showErrorToast({
          message: error,
          header: 'Changement de formulaire'
        }))
      })

  }

  reprendre() {
    this.loadingReprendre = true;
    this.eformsNoticesService.reprendre(this.savedNotice.id)
      .subscribe(value => {
        this.loadingReprendre = false;
        this.store.dispatch(showSuccessToast({
          message: "Votre formulaire est créé",
          header: 'Changement de formulaire'
        }));
        this.routingInterneService.navigateFromContexte('agent/avis/' + value.id)
      }, (error) => {
        this.loadingReprendre = false;
        this.store.dispatch(showErrorToast({
          message: error,
          header: 'Changement de formulaire'
        }))
      })

  }

  exportNotice() {
    if (this.savedNotice.tedVersion === 'esentool') {
      const blob = new Blob([this.savedNotice.xmlGenere],
        {type: "application/xml"});
      const url = window.URL.createObjectURL(blob);
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", "notice-" + this.savedNotice.idConsultation + "-" + this.savedNotice.idNotice + "-" + this.savedNotice.uuid + ".xml");
      document.body.appendChild(link);
      link.click();
      /* eslint-disable */
      if (!!link.parentNode)
        link.parentNode.removeChild(link)
      this.store.dispatch(showSuccessToast({
        message: "Votre formulaire est exporté sous XML",
        header: 'Export XML'
      }))
    } else {
      this.loadingExport = true;
      this.eformsNoticesService.saveNotice(this.savedNotice.id, this.object)
        .subscribe(value => {
          this.loadingExport = false;
          this.savedNotice = value;
          let formulaire = Base64.fromBase64(value.formulaire);
          this.object = JSON.parse(formulaire);
          this.formulaireObject = JSON.parse(formulaire);
          if (value.svrlString)
            this.svrlString = Base64.fromBase64(value.svrlString);
          const blob = new Blob([Base64.fromBase64(value.xmlGenere)],
            {type: "application/xml"});
          const url = window.URL.createObjectURL(blob);
          const link = document.createElement("a");
          link.href = url;
          link.setAttribute("download", "notice-" + this.savedNotice.idConsultation + "-" + this.savedNotice.idNotice + "-" + this.savedNotice.uuid + ".xml");
          document.body.appendChild(link);
          link.click();
          /* eslint-disable */
          if (!!link.parentNode)
            link.parentNode.removeChild(link)
          this.store.dispatch(showSuccessToast({
            message: "Votre formulaire est exporté sous XML",
            header: 'Export XML'
          }))
        }, (error) => {
          this.loadingExport = false;
          this.store.dispatch(showErrorToast({
            message: "Votre formulaire n'est pas exporté",
            header: 'Export XML'
          }))
        })
    }
  }

  exportNoticeSVRL() {
    if (this.savedNotice.tedVersion !== 'esentool') {
      const blob = new Blob([this.svrlString], {type: 'application/svrl'});
      const url = window.URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', 'notice-' + this.savedNotice.idConsultation + '-' + this.savedNotice.idNotice + '-' + this.savedNotice.uuid + '.svrl');
      document.body.appendChild(link);
      link.click();
      /* eslint-disable */
      if (!!link.parentNode)
        link.parentNode.removeChild(link);
      this.store.dispatch(showSuccessToast({
        message: 'Votre formulaire est exporté sous SVRL',
        header: 'Export SVRL'
      }));
    }
  }

  verify() {
    this.store.dispatch(showInfoAlert({
      messages: ["Votre formulaire est en cours de vérification..."],
      icon: 'la-info',
      keepAfterNavigationChange: false,
      clickable: false
    }))
    this.extended = false;
    this.loadingVerification = true;
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    this.eformsNoticesService.verifyNotice(this.savedNotice.id, this.object)
      .subscribe(value => {
        this.loadingVerification = false;
        this.svrlString = value.svrlString;
        if (value.valid && value.validXml) {
          this.store.dispatch(showSuccessAlert({
            messages: ["Votre formulaire est valide"],
            icon: 'la-thumbs-up',
            keepAfterNavigationChange: false,
            clickable: false
          }))
        } else {

          this.errors = value.errors.map(value => {
            if (value.location) {
              // delete the first element of elements
              value.elements.shift();
            }
            return value;
          })
          console.log(this.errors)
          this.store.dispatch(showErrorAlert({
            messages: this.errors.map(value1 => value1.text),
            icon: 'la-thumbs-down',
            keepAfterNavigationChange: true,
            clickable: true
          }))
        }
      }, (error) => {
        this.loadingVerification = false;
        this.store.dispatch(showErrorAlert({
          messages: [error],
          icon: 'la-thumbs-down',
          keepAfterNavigationChange: false,
          clickable: false
        }))
      })

  }

  validateNotice() {
    this.store.dispatch(showInfoAlert({
      messages: ["Votre formulaire est en cours de validation..."],
      icon: 'la-info',
      keepAfterNavigationChange: false,
      clickable: false
    }))
    this.extended = false;
    this.loadingValidation = true;
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    this.eformsNoticesService.validateNotice(this.savedNotice.id)
      .subscribe(value => {
        this.loadingValidation = false;
        this.savedNotice = value;
        let formulaire = Base64.fromBase64(value.formulaire);
        this.object = JSON.parse(formulaire);
        this.formulaireObject = JSON.parse(formulaire);
        this.store.dispatch(showSuccessAlert({
          messages: ["Votre formulaire est complet"],
          icon: 'la-thumbs-up',
          keepAfterNavigationChange: false,
          clickable: false
        }))

      }, (error) => {
        this.loadingValidation = false;
        this.store.dispatch(showErrorAlert({
          messages: [error],
          icon: 'la-thumbs-down',
          keepAfterNavigationChange: false,
          clickable: false
        }))
      })

  }

  updateFormulaireObject() {
    this.formulaireObject = JSON.parse(JSON.stringify(this.object));
  }

  save() {
    this.store.dispatch(clearAlert());
    this.errors = [];
    this.loadingSave = true;
    this.eformsNoticesService.saveNotice(this.savedNotice.id, JSON.parse(JSON.stringify(this.object)))
      .subscribe(value => {
        this.loadingSave = false;
        this.savedNotice = value;
        let formulaire = Base64.fromBase64(value.formulaire);
        this.object = JSON.parse(formulaire);
        this.formulaireObject = JSON.parse(formulaire);
        if (value.svrlString)
          this.svrlString = Base64.fromBase64(value.svrlString);
        this.store.dispatch(setFormulaire({formulaire: JSON.parse(JSON.stringify(this.formulaireObject))}))
        this.store.dispatch(showSuccessToast({
          message: "Votre formulaire est sauvegardé",
          header: 'Sauvegarde Avis'
        }))


      }, (error) => {
        this.loadingSave = false;
        this.store.dispatch(showErrorToast({
          message: error,
          header: 'Sauvegarde Avis'
        }))
      })

  }


  changeGroupObject(event: { object: any, nodeIds: XmlStructureDetails[] }) {
    let {object} = buildObjectFromXmlStructure(event, event.nodeIds.length - 1)
    this.object = mergeJsonRecuresevily(this.object, object);
    this.formulaireObject = JSON.parse(JSON.stringify(this.object));
    this.object = JSON.parse(JSON.stringify(this.object));
    this.store.dispatch(setFormulaire({formulaire: JSON.parse(JSON.stringify(this.formulaireObject))}))
    console.log('changeGroupObject', this.formulaireObject)
  }

  modifyList(event: { itemGroup: any; index: number; object: any, nodeIds: XmlStructureDetails[] }) {
    if (event.nodeIds.length < 2) {
      let objectElement = event.object[getNodeId(event.itemGroup)];
      console.log('objectElement', objectElement, event.index);
      if (isArray(objectElement) && objectElement.length > 0 && event.index > -1) {
        let objectElementElement = objectElement.length > 1 ? objectElement[event.index]: objectElement[0];
        console.log('objectElementElement', objectElementElement);
        let objectElementElementElement = this.object['ND-Root'][getNodeId(event.itemGroup)][event.index];
        console.log('objectElementElementElement', objectElementElementElement)
        this.object['ND-Root'][getNodeId(event.itemGroup)][event.index] = mergeJsonRecuresevily(objectElementElementElement, objectElementElement);

      } else if (isArray(objectElement)) {
        this.object['ND-Root'][getNodeId(event.itemGroup)] = objectElement;
      } else {
        this.object['ND-Root'][getNodeId(event.itemGroup)] = [objectElement];
      }
    } else {
      let {object} = buildObjectFromXmlStructure(event, event.nodeIds.length - 1)
      this.object = mergeJsonRecuresevily(this.object, object);

    }
    this.formulaireObject = JSON.parse(JSON.stringify(this.object));
    this.object = JSON.parse(JSON.stringify(this.object));
    this.store.dispatch(setFormulaire({formulaire: JSON.parse(JSON.stringify(this.formulaireObject))}))
    console.log('modifyList', this.formulaireObject)

  }


  ngOnDestroy(): void {
    this.store.dispatch(clearAlert())
    this._router.navigate([], {
      queryParams: {
        sectionId: null,
        subSectionId: null
      },
      queryParamsHandling: 'merge',
    });
  }

  downloadPdf() {
    this.downloadingAnnonce = true;
    this.eformsNoticesService.pdf(this.savedNotice.id).subscribe(value => {
      this.downloadingAnnonce = false;
      const filename = 'avis_publicite_' + this.savedNotice.referenceConsultation + '.pdf';
      saveFile(value, filename);
    }, () => this.downloadingAnnonce = false)
  }
}
