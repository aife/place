import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidationSipNoticeDashboardComponent } from './validation-sip-notice-dashboard.component';

describe('MyNoticeDashboardComponent', () => {
  let component: ValidationSipNoticeDashboardComponent;
  let fixture: ComponentFixture<ValidationSipNoticeDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValidationSipNoticeDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidationSipNoticeDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
