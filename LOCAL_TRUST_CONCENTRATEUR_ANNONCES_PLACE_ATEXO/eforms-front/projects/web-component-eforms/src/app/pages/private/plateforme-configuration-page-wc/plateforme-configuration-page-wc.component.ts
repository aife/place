import {Component, OnInit} from '@angular/core';
import {PlateformeConfigurationService} from '@shared-global/core/services/plateforme-configuration.service';
import {SupportsService} from '@shared-global/core/services/supports.service';
import {OffreDTO, PlateformeConfigurationDTO, SubNotice, SupportDTO} from '@shared-global/core/models/api/eforms.api';
import {BreadcrumbModel} from '@shared-global/core/models/breadcrumb.model';
import {saveFile} from '@shared-global/core/utils/download/doawnload.utils';
import {EformsNoticesService} from '@shared-global/core/services/eforms-notices.service';
import {EformsSdkService} from '@shared-global/core/services/eforms-sdk.service';

@Component({
  selector: 'atx-plateforme-configuration-page-wc',
  templateUrl: './plateforme-configuration-page-wc.component.html',
  styleUrls: ['./plateforme-configuration-page-wc.component.css']
})
export class PlateformeConfigurationPageWcComponent implements OnInit {
  param: BreadcrumbModel = {
    mainlabel: 'configuration-plateforme.titre',
    links: [{
      name: 'configuration-plateforme.parametrage',
      isLink: false,
    }]
  };
  plateformeConfiguration: PlateformeConfigurationDTO;
  plateformeConfigurations: PlateformeConfigurationDTO[];
  europeenOffres: OffreDTO[] = [];
  selectedOffre: OffreDTO;
  loading = false;
  loadingExport = false;
  loadingImport = false;
  extended = true;
  extendedFormulaireEuropeen = true;
  loadingSave = false;
  supports: SupportDTO[];
  structure: SubNotice;

  constructor(
    public supportsService: SupportsService,
    public eformsSdkService: EformsSdkService,
    private readonly plateformeConfigurationService: PlateformeConfigurationService) {
  }

  ngOnInit() {
    this.supportsService.getOffres(null, null, true, false, ['TED']).subscribe(value => {
      this.europeenOffres = value;
    });
    this.supportsService.getSupports().subscribe(value => {
      this.supports = value;
    });
    this.loading = true;
    this.plateformeConfigurationService.getAll().subscribe(value => {
      this.plateformeConfigurations = value;
      this.loading = false;
    }, () => {
      this.loading = false;
    });
  }


  export() {
    this.loadingExport = true;
    this.plateformeConfigurationService.exporter(this.plateformeConfiguration.id).subscribe(file => {
      saveFile(file, 'Association ' + this.plateformeConfiguration.idPlateforme + '.xlsx');
      this.loadingExport = false;
    }, () => this.loadingExport = false);
  }

  deleteItem(item: any, list: Array<any>) {
    if (!item || !list) {
      return list;
    }
  }

  save() {
    this.loadingSave = true;
    this.plateformeConfigurationService.modify(this.plateformeConfiguration).subscribe(value => {
        this.plateformeConfiguration = value;
        this.loadingSave = false;
      }, () => this.loadingSave = false
    );
  }

  openDialog() {
    document.getElementById('fileInput').click();
  }

  importer(file: any) {
    this.loadingImport = true;
    this.plateformeConfigurationService.importer(this.plateformeConfiguration.id, file).subscribe(file => {
      this.loadingImport = false;

    }, () => this.loadingImport = false);
  }

  chargerFormulaire(event: OffreDTO) {
    console.log(event);
    this.structure = null;
    this.eformsSdkService.getStructure(this.plateformeConfiguration.idPlateforme, event.code.replace('_ENOTICES', '')).subscribe(value => {
      this.structure = value;
    });
  }
}
