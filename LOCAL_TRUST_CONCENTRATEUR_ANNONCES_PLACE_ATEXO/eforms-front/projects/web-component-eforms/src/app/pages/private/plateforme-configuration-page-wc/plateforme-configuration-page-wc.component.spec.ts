import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlateformeConfigurationPageWcComponent } from './plateforme-configuration-page-wc.component';

describe('FormsPageComponent', () => {
  let component: PlateformeConfigurationPageWcComponent;
  let fixture: ComponentFixture<PlateformeConfigurationPageWcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlateformeConfigurationPageWcComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlateformeConfigurationPageWcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
