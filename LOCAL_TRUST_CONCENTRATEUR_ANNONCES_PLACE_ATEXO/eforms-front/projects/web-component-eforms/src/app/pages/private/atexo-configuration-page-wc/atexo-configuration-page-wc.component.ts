import {Component, OnInit} from '@angular/core';
import {PlateformeConfigurationService} from '@shared-global/core/services/plateforme-configuration.service';
import {SupportsService} from '@shared-global/core/services/supports.service';
import {OffreDTO, SubNotice} from '@shared-global/core/models/api/eforms.api';
import {BreadcrumbModel} from '@shared-global/core/models/breadcrumb.model';
import {saveFile} from '@shared-global/core/utils/download/doawnload.utils';
import {EformsSdkService} from '@shared-global/core/services/eforms-sdk.service';

@Component({
  selector: 'atx-atexo-configuration-page-wc',
  templateUrl: './atexo-configuration-page-wc.component.html',
  styleUrls: ['./atexo-configuration-page-wc.component.css']
})
export class AtexoConfigurationPageWcComponent implements OnInit {
  param: BreadcrumbModel = {
    mainlabel: 'configuration-atexo.titre',
    links: [{
      name: 'configuration-atexo.parametrage',
      isLink: false,
    }]
  };
  europeenOffres: OffreDTO[] = [];
  selectedOffre: OffreDTO;
  loading = false;
  loadingExport = false;
  loadingImport = false;
  extended = true;
  extendedFormulaireEuropeen = true;
  loadingSave = false;
  structure: SubNotice;

  constructor(
    public eformsSdkService: EformsSdkService,
    public supportsService: SupportsService,
    private readonly plateformeConfigurationService: PlateformeConfigurationService) {
  }

  ngOnInit() {
    this.supportsService.getOffres(null, null, true, false).subscribe(value => {
      this.europeenOffres = value;
    });
  }

  export() {
    this.loadingExport = true;
    this.plateformeConfigurationService.exporterAtexo().subscribe(file => {
      saveFile(file, 'Association ATEXO.xlsx');
      this.loadingExport = false;
    }, () => this.loadingExport = false);
  }


  importer(file) {
    this.loadingImport = true;
    this.plateformeConfigurationService.importerAtexo(file).subscribe(file => {
      this.loadingImport = false;

    }, () => this.loadingImport = false);
  }

  chargerFormulaire(event: OffreDTO) {
    console.log(event);
    this.structure = null;
    this.eformsSdkService.getStructure(null, event.code.replace('_ENOTICES', '')).subscribe(value => {
      this.structure = value;
    });
  }
}
