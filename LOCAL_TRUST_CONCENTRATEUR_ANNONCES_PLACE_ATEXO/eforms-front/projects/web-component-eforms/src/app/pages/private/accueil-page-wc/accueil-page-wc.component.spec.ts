import {ComponentFixture, TestBed} from '@angular/core/testing';

import {AccueilPageWCComponent} from './accueil-page-wc.component';

describe('AccueilPageWCComponent', () => {
  let component: AccueilPageWCComponent;
  let fixture: ComponentFixture<AccueilPageWCComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccueilPageWCComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccueilPageWCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
