import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoticeIframeWcComponent } from './notice-iframe-wc.component';

describe('NoticeIframeComponent', () => {
  let component: NoticeIframeWcComponent;
  let fixture: ComponentFixture<NoticeIframeWcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoticeIframeWcComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoticeIframeWcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
