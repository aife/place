import {Component, OnDestroy} from '@angular/core';
import {BreadcrumbModel} from "@shared-global/core/models/breadcrumb.model";
import {
    AnnonceStatistique,
    StatutStatistique,
    StatutTypeAvisAnnonceEnum
} from "@shared-global/core/models/api/eforms.api";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {saveFile} from "@shared-global/core/utils/download/doawnload.utils";
import moment from "moment";
import {AnnoncesTypeAvisService} from "@shared-global/core/services/annonces-type-avis.service";

@Component({
    selector: 'app-sip-statitique',
    templateUrl: './statistique-sip-notice-dashboard.component.html',
    styleUrls: ['./statistique-sip-notice-dashboard.component.css']
})
export class StatistiqueSipNoticeDashboardComponent implements OnDestroy {
    loadingExport = false;
    pageDescription: BreadcrumbModel = {
        mainlabel: 'validation-avis-sip.titre',
        links: [{
            name: 'validation-avis-sip.dashboard',
            isLink: true,
            link: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.VALIDATION_SIP_NOTICES
        }]
    };
    offres: AnnonceStatistique[];
    loading = false;

    statuts: Array<{ key: string, label: string, icon: string, value: StatutTypeAvisAnnonceEnum }> =
        [{
            key: 'AT',
            value: 'EN_ATTENTE_VALIDATION_SIP',
            icon: 'la la-gavel',
            label: 'A_TRAITER'
        },
            {key: 'EP', value: 'ENVOI_PLANIFIER', icon: 'la la-calendar', label: 'ENVOI_PLANIFIER'}, {
            key: 'E',
            value: 'ENVOYE',
            icon: 'la la-rocket',
            label: 'ENVOYE'
        },
            {key: 'R', value: 'REJETE_SIP', icon: 'la la-ban', label: 'REJETE_SIP'}, {
            key: 'ER',
            value: 'REJETE',
            icon: 'la la-ban',
            label: 'REJETE'
        }]
    private interval: number;

    constructor(private readonly annonceService: AnnoncesTypeAvisService, private readonly routingInterne: RoutingInterneService) {
      this.loading = true;
      this.updateStatistiques();
        this.interval = setInterval(() => this.updateStatistiques(), 10000);
    }

    private updateStatistiques() {
        this.annonceService.statistiques(true).subscribe(value => {
            this.loading = false;
            this.offres = value
        }, () => this.loading = false);
    }

    getTotalByStatus(statuts: StatutStatistique[], value: "BROUILLON" | "EN_ATTENTE_VALIDATION_ECO" | "EN_ATTENTE_VALIDATION_SIP" | "ENVOI_PLANIFIER" | "ENVOYE" | "REJETE") {
        let statut = statuts.find(value1 => value1.statut === value);
        return statut ? statut.total : 0;
    }

    goToValidation(statistique: AnnonceStatistique, value: "BROUILLON" | "EN_ATTENTE_VALIDATION_ECO" | "EN_ATTENTE_VALIDATION_SIP" | "ENVOI_PLANIFIER" | "ENVOYE" | "REJETE") {
        const total = this.getTotalByStatus(statistique.statuts, value);
        if (total > 0) {
            this.routingInterne.navigateFromContexte(AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.VALIDATION_SIP_NOTICES_DASHBOARD, {
                statut: value,
                offre: statistique.offre.code
            })
        }
    }

    getNational(offres: AnnonceStatistique[]) {
        return offres?.filter(value => !value.offre.europeen).sort((a, b) => a.offre.code.localeCompare(b.offre.code));
    }

    getEuropeen(offres: AnnonceStatistique[]) {
        return offres?.filter(value => value.offre.europeen).sort((a, b) => a.offre.code.localeCompare(b.offre.code));
    }

    ngOnDestroy(): void {
        if (this.interval) {
            clearInterval(this.interval);
        }
    }

    export() {
        this.loadingExport = true;
        this.annonceService.exportStatistiques(true).subscribe((value) => {
            saveFile(value, "Export_SIP_" + moment(new Date()).format("YYYYMMDD") + '.xlsx');
            this.loadingExport = false;
        }, () => this.loadingExport = false, () => this.loadingExport = false);
    }
}
