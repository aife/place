import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoticePageWcComponent } from './notice-page-wc.component';

describe('FormsPageComponent', () => {
  let component: NoticePageWcComponent;
  let fixture: ComponentFixture<NoticePageWcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoticePageWcComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoticePageWcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
