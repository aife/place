import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatistiqueSipNoticeDashboardComponent } from './statistique-sip-notice-dashboard.component';

describe('MyNoticeDashboardComponent', () => {
  let component: StatistiqueSipNoticeDashboardComponent;
  let fixture: ComponentFixture<StatistiqueSipNoticeDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatistiqueSipNoticeDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatistiqueSipNoticeDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
