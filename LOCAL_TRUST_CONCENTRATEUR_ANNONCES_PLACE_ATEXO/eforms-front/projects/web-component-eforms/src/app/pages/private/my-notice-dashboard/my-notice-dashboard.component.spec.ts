import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyNoticeDashboardComponent } from './my-notice-dashboard.component';

describe('MyNoticeDashboardComponent', () => {
  let component: MyNoticeDashboardComponent;
  let fixture: ComponentFixture<MyNoticeDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyNoticeDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyNoticeDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
