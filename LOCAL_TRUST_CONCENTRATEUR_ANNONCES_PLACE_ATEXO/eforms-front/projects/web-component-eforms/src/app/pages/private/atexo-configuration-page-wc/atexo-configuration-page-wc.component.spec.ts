import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtexoConfigurationPageWcComponent } from './atexo-configuration-page-wc.component';

describe('FormsPageComponent', () => {
  let component: AtexoConfigurationPageWcComponent;
  let fixture: ComponentFixture<AtexoConfigurationPageWcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtexoConfigurationPageWcComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtexoConfigurationPageWcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
