import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidationEcoNoticeDashboardComponent } from './validation-eco-notice-dashboard.component';

describe('MyNoticeDashboardComponent', () => {
  let component: ValidationEcoNoticeDashboardComponent;
  let fixture: ComponentFixture<ValidationEcoNoticeDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValidationEcoNoticeDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidationEcoNoticeDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
