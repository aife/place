import {RouterTestingModule} from '@angular/router/testing';
import {NgModule} from '@angular/core';
import {NotConnectedGuard} from "@shared-global/core/guards/not-connected.guard";
import {AuthGuard} from "@shared-global/core/guards/auth.guard";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {LoginPageWCComponent} from "@pages-wc/public/login-page-wc/login-page-wc.component";

import {AccueilPageWCComponent} from "@pages-wc/private/accueil-page-wc/accueil-page-wc.component";
import {MyNoticeDashboardComponent} from "@pages-wc/private/my-notice-dashboard/my-notice-dashboard.component";
import {NoticeDashboardComponent} from "@pages-wc/private/notice-dashboard/notice-dashboard.component";
import {
    ValidationSipNoticeDashboardComponent
} from "@pages-wc/private/validation-sip-notice-dashboard/validation-sip-notice-dashboard.component";
import {
    ValidationNoticeDashboardComponent
} from "@pages-wc/private/validation-notice-dashboard/validation-notice-dashboard.component";
import {
    ValidationEcoNoticeDashboardComponent
} from "@pages-wc/private/validation-eco-notice-dashboard/validation-eco-notice-dashboard.component";
import {
    StatistiqueEcoNoticeDashboardComponent
} from "@pages-wc/private/statistique-eco-notice-dashboard/statistique-eco-notice-dashboard.component";
import {
    StatistiqueSipNoticeDashboardComponent
} from "@pages-wc/private/statistique-sip-notice-dashboard/statistique-sip-notice-dashboard.component";
import {NoticePageWcComponent} from "@pages-wc/private/notice-page-wc/notice-page-wc.component";
import {
    GestionNoticeDashboardComponent
} from "@pages-wc/private/gestion-notice-dashboard/gestion-notice-dashboard.component";
import {
    PlateformeConfigurationPageWcComponent
} from "@pages-wc/private/plateforme-configuration-page-wc/plateforme-configuration-page-wc.component";
import {
    AtexoConfigurationPageWcComponent
} from "@pages-wc/private/atexo-configuration-page-wc/atexo-configuration-page-wc.component";
import {
    SessionExpiredPageWcComponent
} from "@pages-wc/public/session-expired-page-wc/session-expired-page-wc.component";

const routes = [
    {
        path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.ACCUEIL,
        component: AccueilPageWCComponent,
        outlet: 'eforms',
        canActivate: [AuthGuard]
    },
    {
        path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.NOTICES_CONSULTATION,
        outlet: 'eforms',
        component: NoticePageWcComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.NOTICES,
        outlet: 'eforms',
        component: NoticeDashboardComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.MY_NOTICES,
        outlet: 'eforms',
        component: MyNoticeDashboardComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.VALIDATION_NOTICES,
        outlet: 'eforms',
        component: ValidationNoticeDashboardComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.GESTION_NOTICES,
        outlet: 'eforms',
        component: GestionNoticeDashboardComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.VALIDATION_ECO_NOTICES_DASHBOARD,
        outlet: 'eforms',
        component: ValidationEcoNoticeDashboardComponent,
        canActivate: [AuthGuard]
    }, {
        path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.VALIDATION_ECO_NOTICES,
        outlet: 'eforms',
        component: StatistiqueEcoNoticeDashboardComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.VALIDATION_SIP_NOTICES_DASHBOARD,
        outlet: 'eforms',
        component: ValidationSipNoticeDashboardComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.VALIDATION_SIP_NOTICES,
        outlet: 'eforms',
        component: StatistiqueSipNoticeDashboardComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.PLATEFORMES_CONFIGURATION,
        outlet: 'eforms',
        component: PlateformeConfigurationPageWcComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.ATEXO_CONFIGURATION,
        outlet: 'eforms',
        component: AtexoConfigurationPageWcComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppInternalPathEnum.LOGIN + "/" + AppInternalPathEnum.LOGIN_PARAMS,
        outlet: 'eforms',
        component: LoginPageWCComponent, canActivate: [NotConnectedGuard]

    },
    {
        path: AppInternalPathEnum.LOGIN + "/" + AppInternalPathEnum.LOGIN_PARAMS_AGENT,
        outlet: 'eforms',
        component: LoginPageWCComponent, canActivate: [NotConnectedGuard]

    },
    {
        path: AppInternalPathEnum.LOGIN + "/" + AppInternalPathEnum.SESSION_EXPIRED,
        outlet: 'eforms',
        component: SessionExpiredPageWcComponent

    },
    {
        path: AppInternalPathEnum.LOGIN,
        outlet: 'eforms',
        component: LoginPageWCComponent,
        canActivate: [NotConnectedGuard]
    }];

@NgModule({
    imports: [
        RouterTestingModule.withRoutes(routes),
    ],
    exports: [RouterTestingModule],
})
export class GlobalRoutingModule {
}
