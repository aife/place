import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {State} from "@shared-global/store"
import {Store} from "@ngrx/store";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {setAccessToken, setDefaultParams} from "@shared-global/store/user/user.action";
import {AppInternalPathEnum} from '@shared-global/core/enums/app-internal-path.enum';
import {EformsNoticesService} from "@shared-global/core/services/eforms-notices.service";
import {BsLocaleService} from "ngx-bootstrap/datepicker";

@Component({
  selector: 'app-global',
  templateUrl: './global.component.html',
  styleUrls: ['./global.component.css']
})
export class GlobalComponent implements OnChanges {

  @Input() token: string;
  @Input() lang: string;
  @Input() organisme: string;
  @Input() plateforme: string;
  @Input() page: string = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.ACCUEIL;

  constructor(
    private store: Store<State>,
    private bsLocaleService: BsLocaleService,
    private readonly eformsNoticesService: EformsNoticesService,
    private router: RoutingInterneService, private translateService: TranslateService) {
    console.log('constructor')
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('initSession')
    this.initSession();
  }


  initSession() {
    if (!!this.token) {
      this.store.dispatch(setAccessToken({
        accessToken: this.token,
        organisme: this.organisme,
        redirect: this.page,
        plateforme: this.plateforme
      }))

      if (!this.lang) {
        this.lang = "fr";
      }
      this.translateService.use(this.lang);
      this.bsLocaleService.use(this.lang);
      this.naviagteToPage();
    }

  }


  private naviagteToPage() {
    console.log(this.page)
    this.store.dispatch(setDefaultParams({
      defaultPage: this.page,
      isWebComponent: true
    }));
    this.store.select(state => state.userReducer.connected).subscribe(connected => {
      if (!connected) {
        console.log('!connected')
        this.router.navigateFromContexte('login/' + this.plateforme + "/" + this.organisme + "/" + this.token);

      } else {
        console.log('connected')
        this.router.navigateFromContexte(this.page);
      }
    })
  }
}
