import {ApplicationRef, DoBootstrap, Injector, LOCALE_ID, NgModule} from '@angular/core';
import {APP_BASE_HREF, CommonModule, PlatformLocation, registerLocaleData} from '@angular/common';
import {GlobalComponent} from './global.component';
import {RouterModule} from "@angular/router";
import {ElementZoneStrategyFactory} from "elements-zone-strategy";
import {createCustomElement} from "@angular/elements";
import {TranslateLoader, TranslateModule, TranslateService, TranslateStore} from "@ngx-translate/core";
import {HttpClient} from "@angular/common/http";
import {PublicWcModule} from "@pages-wc/public/public-wc.module";
import {CoreModule} from "@shared-global/core/core.module";
import {PrivateWcModule} from "@pages-wc/private/private-wc.module";
import {BibliotequeCommuneModule} from "@shared-global/app.module";
import {GlobalRoutingModule} from "@global-wc/global-routing.module";
import {defineLocale} from "ngx-bootstrap/chronos";
import {deLocale, enGbLocale, frLocale} from "ngx-bootstrap/locale";
import localeFr from "@angular/common/locales/fr";
import localeEn from "@angular/common/locales/en";
import localeDe from "@angular/common/locales/de";
import {ComponentsModule} from "@shared-global/components/components.module";
import {PerfectScrollbarModule} from "ngx-perfect-scrollbar";
import {NgxSpinnerModule} from "ngx-spinner";
import {LoadingBarModule} from "@ngx-loading-bar/core";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {MockPlatformLocation} from "@angular/common/testing";
import {NgSelectModule} from "@ng-select/ng-select";
import {EformsSdkService} from "@shared-global/core/services/eforms-sdk.service";
import {forkJoin, Observable} from "rxjs";
import {map} from "rxjs/operators";
import * as _ from "lodash";
import {BsLocaleService} from "ngx-bootstrap/datepicker";
import {HAMMER_GESTURE_CONFIG, HammerGestureConfig} from "@angular/platform-browser";
import {NgbCarouselConfig, NgbModalConfig} from "@ng-bootstrap/ng-bootstrap";
import {BlockUIService} from "ng-block-ui";

defineLocale('fr', frLocale);
defineLocale('en', enGbLocale);
defineLocale('de', deLocale);
registerLocaleData(localeFr);
registerLocaleData(localeEn);
registerLocaleData(localeDe);


export class CustomLoader implements TranslateLoader {

    constructor(private http: HttpClient, private platformLocation: PlatformLocation,
                private eformsSdkService: EformsSdkService) {
    }

    public getTranslation(lang: string): Observable<any> {
        let app = this.eformsSdkService.getI18n(lang).pipe();
        let api = this.eformsSdkService.getSdkI18n(lang).pipe();
    //    let surcharge = this.eformsSdkService.getSurchageI18n(lang).pipe();
        return forkJoin(app, api).pipe(
            map(([a, b]) => {
                return _.merge(a, b)
            }));
    }
}

@NgModule({
    declarations: [
        GlobalComponent
    ],
    imports: [
        PerfectScrollbarModule,
        NgxSpinnerModule,
        BibliotequeCommuneModule, CommonModule, PrivateWcModule,
        PublicWcModule, RouterModule, GlobalRoutingModule,
        NgSelectModule,
        CoreModule, TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useClass: CustomLoader,
                deps: [HttpClient, PlatformLocation, EformsSdkService]
            }, isolate: true
        }), ComponentsModule, LoadingBarModule
    ],
    providers: [TranslateStore,
        BsModalRef,
        {
            provide: PlatformLocation,
            useClass: MockPlatformLocation
        },
        {
            provide: APP_BASE_HREF,
            useValue: '/web-component-eforms'
        },
        {provide: LOCALE_ID, useValue: 'fr-FR'}, {
            provide: HAMMER_GESTURE_CONFIG,
            useClass: HammerGestureConfig
        },
        NgbCarouselConfig,
        NgbModalConfig,
        BsModalService,
        BlockUIService],
    entryComponents: [GlobalComponent]

})
export class GlobalModule implements DoBootstrap {

    constructor(private injector: Injector, private bsLocaleService: BsLocaleService, private translateService: TranslateService) {
        let lang = 'fr';
        this.translateService.use(lang);
        this.bsLocaleService.use(lang);
        const strategyFactory = new ElementZoneStrategyFactory(GlobalComponent, injector);
        const webComponent = createCustomElement(GlobalComponent, {injector, strategyFactory});
        customElements.define('atx-eforms', webComponent);
    }

    ngDoBootstrap(appRef: ApplicationRef): void {
    }
}
