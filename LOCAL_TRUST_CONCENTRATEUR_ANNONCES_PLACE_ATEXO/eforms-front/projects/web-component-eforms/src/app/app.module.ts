import {ApplicationRef, DoBootstrap, LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {GlobalModule} from "./global/global.module";
import {TranslateLoader, TranslateModule, TranslateStore} from "@ngx-translate/core";
import {HttpClient} from "@angular/common/http";
import {APP_BASE_HREF, PlatformLocation} from "@angular/common";
import {EformsSdkService} from "@shared-global/core/services/eforms-sdk.service";
import {forkJoin, Observable} from "rxjs";
import {map} from "rxjs/operators";
import * as _ from "lodash";
import {NgbCarouselConfig, NgbModalConfig} from "@ng-bootstrap/ng-bootstrap";
import {BsModalService} from "ngx-bootstrap/modal";
import {BlockUIService} from "ng-block-ui";


export class CustomLoader implements TranslateLoader {

  constructor(private http: HttpClient, private platformLocation: PlatformLocation,
              private eformsSdkService: EformsSdkService) {
  }

  public getTranslation(lang: string): Observable<any> {
    let app = this.eformsSdkService.getI18n(lang).pipe();
    let api = this.eformsSdkService.getSdkI18n(lang).pipe();
   // let surcharge = this.eformsSdkService.getSurchageI18n(lang).pipe();
    return forkJoin(app, api).pipe(
        map(([a, b]) => {
          return _.merge(a, b)
        }));
  }
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    GlobalModule,
    AppRoutingModule, TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useClass: CustomLoader,
        deps: [HttpClient, PlatformLocation, EformsSdkService]
      }, isolate: true
    })
  ],
  providers: [TranslateStore,
    {
      provide: APP_BASE_HREF,
      useValue: '/web-component-eforms'

    },
    {provide: LOCALE_ID, useValue: 'fr-FR'},
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: HammerGestureConfig
    },
    NgbCarouselConfig,
    NgbModalConfig,
    BsModalService,
    BlockUIService],
})
export class AppModule implements DoBootstrap {

  ngDoBootstrap(appRef: ApplicationRef): void {
    if (document.querySelector('app-root')) {
      appRef.bootstrap(AppComponent);
    }
  }
}
