import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LayoutModule} from '@core-eforms/layout/layout.module';
import {SettingsModule} from '@core-eforms/layout/settings/settings.module';
import {DataApiService} from '@core-eforms/services/layout/data.api';
import {NavbarService} from '@core-eforms/services/layout/navbar.service';
import {MenuSettingsConfig, ThemeSettingsConfig} from "@core-eforms/constantes/settings.const";
import {AuthGuard} from "@shared-global/core/guards/auth.guard";
import {NotConnectedGuard} from "@shared-global/core/guards/not-connected.guard";
import {RouterModule} from "@angular/router";


@NgModule({
  imports: [
    LayoutModule,
    CommonModule,
    RouterModule,
    SettingsModule.forRoot(ThemeSettingsConfig, MenuSettingsConfig)],
  providers: [AuthGuard, NotConnectedGuard, DataApiService, NavbarService]
})
export class EformsCoreModule {
}
