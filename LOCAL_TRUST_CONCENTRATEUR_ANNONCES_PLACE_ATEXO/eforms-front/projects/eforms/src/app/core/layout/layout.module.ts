import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';
import {PublicLayoutComponent} from './public-layout/public-layout.component';
import {PrivateLayoutComponent} from './private-layout/private-layout.component';
import {HeaderComponent} from './header/header.component';
import {HorizontalComponent} from './header/horizontal/horizontal.component';
import {NavigationComponent} from './navigation/navigation.component';
import {HorizontalnavComponent} from './navigation/horizontalnav/horizontalnav.component';
import {PerfectScrollbarModule} from "ngx-perfect-scrollbar";
import {TranslateModule} from '@ngx-translate/core';
import {BibliotequeCommuneModule} from "@shared-global/app.module";
import {PipesModule} from "@shared-global/pipes/pipes.module";

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    NgbModule,
    RouterModule.forChild([]),
    PerfectScrollbarModule,
    BibliotequeCommuneModule,
    CommonModule,
    PipesModule,
    RouterModule,

  ],
  declarations: [
    PublicLayoutComponent,
    PrivateLayoutComponent,
    HeaderComponent,
    HorizontalComponent,
    NavigationComponent,
    HorizontalnavComponent],
  exports: [
    PublicLayoutComponent,
    PrivateLayoutComponent,
  ]
})
export class LayoutModule {
}
