import {Component, HostListener, OnInit, Renderer2} from '@angular/core';
import {takeUntil} from 'rxjs/operators';

import {ThemeSettingsService} from '../settings/theme-settings.service';
import {Subject} from 'rxjs';
import {AppConstants} from "@core-eforms/constantes/app.constants";
import {ActivatedRoute} from "@angular/router";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  layout: string;
  private _themeSettingsConfig: any;
  private _unsubscribeAll: Subject<any>;
  isMobile = false;
  isWebComponent = true;

  constructor(private _renderer: Renderer2, private route: ActivatedRoute,
              private store: Store<State>,
              private _themeSettingsService: ThemeSettingsService) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit() {
    this.isMobile = window.innerWidth < AppConstants.MOBILE_RESPONSIVE_WIDTH_HORIZONTAL;
    this._themeSettingsService.config
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((config) => {
        this._themeSettingsConfig = config;
        if (config.layout && config.layout.style &&
          config.layout.style === 'vertical') {
          this.layout = 'vertical';
        } else {
          this.layout = 'horizontal';
        }
      });

  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.isMobile = event.target.innerWidth < AppConstants.MOBILE_RESPONSIVE_WIDTH_HORIZONTAL;
  }
}
