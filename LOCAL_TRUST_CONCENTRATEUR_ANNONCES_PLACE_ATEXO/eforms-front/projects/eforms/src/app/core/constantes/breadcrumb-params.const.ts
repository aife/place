export const BreadcrumbParamsConst = {
  FORMULAIRE: {
    url: 'forms',
    generationBreadcrumb: {
      'mainlabel': 'Génération des formulaires',
      'links': [
        {
          'name': 'Formulaire',
          'isLink': false,
        },
        {
          'name': 'Génération',
          'isLink': false
        }
      ]
    }
  }, CONFIGURATION_PLATEFORME: {
    url: 'configuration',
    generationBreadcrumb: {
      'mainlabel': 'Configuration de la plateforme',
      'links': [
        {
          'name': 'Configuration de la plateforme',
          'isLink': false,
        },
        {
          'name': 'Paramétrage',
          'isLink': false
        }
      ]
    }
  }
};
