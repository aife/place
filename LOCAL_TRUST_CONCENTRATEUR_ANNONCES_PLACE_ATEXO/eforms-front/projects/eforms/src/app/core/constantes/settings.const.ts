import {MenuConfig} from "@core-eforms/models/menu-settings.model";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";

export const MenuSettingsConfig: MenuConfig = {
  items: [
    {
      icon: 'la la-home',
      page: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.ACCUEIL,
      roles: ['*']
    },
    {
      icon: 'la la-file-text',
      title: 'Validation des annonces',
      page: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.VALIDATION_NOTICES,
      roles: ['*'],
      submenu: {
        items: [{
          title: 'Toutes les annonces',
          page: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.VALIDATION_NOTICES,
          roles: ['*']

        }, {
          title: 'Mes annonces SIP à valider',
          page: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.VALIDATION_SIP_NOTICES,
          roles: ['*']
        }, {
          title: 'Mes annonces ECO à valider',
          page: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.VALIDATION_ECO_NOTICES,
          roles: ['*']
        }]
      }

    },
    {
      icon: 'la la-file-text',
      title: 'Gestion des avis',
      page: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.GESTION_NOTICES,
      roles: ['*']

    }
  ]
}


export const ThemeSettingsConfig = {
  colorTheme: 'semi-dark', // light, semi-light, semi-dark, dark
  layout: {
    style: 'horizontal', // style: 'vertical', horizontal,
    pattern: 'fixed' // fixed, boxed, static
  },
  menuColor: 'menu-dark', // Vertical: [menu-dark, menu-light] , Horizontal: [navbar-dark, navbar-light]
  navigation: 'menu-collapsible', // menu-collapsible, menu-accordation
  menu: 'expand', // collapse, expand
  header: 'static', // fix, static
  footer: 'static', // fix, static
  headerIcons: {
    maximize: 'off', // on, off
    search: 'off', // on, off
    internationalization: 'on', // on, off
    notification: 'off', // on, off
    email: 'off' // on, off
  },
  brand: {
    brand_name: 'Eforms',
    router: 'http://www.atexo.com/',
    logo: {
      type: 'url',
      value: 'assets/images/logo/atexo.png'
    },
  },
  defaultTitleSuffix: 'Eforms'
};
