// Default menu settings configurations

import {FailedAssertRepresentation, SubNoticeMetadata} from "@shared-global/core/models/api/eforms.api";

export interface MenuItem {
  id?: string;
  title: string;
  itemGroup: SubNoticeMetadata;
  object: object;
  icon: string;
  page: string;
  isOpen: boolean;
  hasError: boolean;
  errors: FailedAssertRepresentation[];
  isSelected: boolean;
  isExternalLink?: boolean;
  deletable?: boolean;
  internalModule?: string;
  internalModuleUrl?: string;
  issupportExternalLink?: boolean;
  badge: { type: string, value: string };
  submenu: {
    items: Partial<MenuItem>[];
  };
  roles: Array<string>;
  section: string;
  index: number
}

export interface MenuConfig {
  items: Partial<MenuItem>[]

}






