import {RouterModule, Routes} from '@angular/router';
import {PrivateLayoutComponent} from "@core-eforms/layout/private-layout/private-layout.component";
import {PublicLayoutComponent} from "@core-eforms/layout/public-layout/public-layout.component";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {AuthGuard} from "@shared-global/core/guards/auth.guard";
import {NotConnectedGuard} from "@shared-global/core/guards/not-connected.guard";
import {AccueilPageWCComponent} from "@pages-wc/private/accueil-page-wc/accueil-page-wc.component";
import {LoginPageWCComponent} from "@pages-wc/public/login-page-wc/login-page-wc.component";
import {NoticePageWcComponent} from "@pages-wc/private/notice-page-wc/notice-page-wc.component";
import {NoticeDashboardComponent} from "@pages-wc/private/notice-dashboard/notice-dashboard.component";
import {MyNoticeDashboardComponent} from "@pages-wc/private/my-notice-dashboard/my-notice-dashboard.component";
import {
  ValidationSipNoticeDashboardComponent
} from "@pages-wc/private/validation-sip-notice-dashboard/validation-sip-notice-dashboard.component";
import {
  ValidationNoticeDashboardComponent
} from "@pages-wc/private/validation-notice-dashboard/validation-notice-dashboard.component";
import {
  ValidationEcoNoticeDashboardComponent
} from "@pages-wc/private/validation-eco-notice-dashboard/validation-eco-notice-dashboard.component";
import {
  StatistiqueEcoNoticeDashboardComponent
} from "@pages-wc/private/statistique-eco-notice-dashboard/statistique-eco-notice-dashboard.component";
import {
  StatistiqueSipNoticeDashboardComponent
} from "@pages-wc/private/statistique-sip-notice-dashboard/statistique-sip-notice-dashboard.component";
import {NoticeIframeWcComponent} from "@pages-wc/private/notice-iframe-wc/notice-iframe-wc.component";
import {
  GestionNoticeDashboardComponent
} from "@pages-wc/private/gestion-notice-dashboard/gestion-notice-dashboard.component";
import {
  PlateformeConfigurationPageWcComponent
} from "@pages-wc/private/plateforme-configuration-page-wc/plateforme-configuration-page-wc.component";
import {
  AtexoConfigurationPageWcComponent
} from "@pages-wc/private/atexo-configuration-page-wc/atexo-configuration-page-wc.component";
import {
  SessionExpiredPageWcComponent
} from "@pages-wc/public/session-expired-page-wc/session-expired-page-wc.component";


const appRoutes: Routes = [

  // Private layout
  {
    path: 'agent',
    component: PrivateLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: AppInternalPathEnum.ACCUEIL,
        component: AccueilPageWCComponent,
        canActivate: [AuthGuard]
      }, {
        path: AppInternalPathEnum.NOTICES_CONSULTATION,
        component: NoticePageWcComponent,
        canActivate: [AuthGuard]
      }, {
        path: AppInternalPathEnum.NOTICES_PUBLIE_CONSULTATION,
        component: NoticeIframeWcComponent,
        canActivate: [AuthGuard]
      }, {
        path: AppInternalPathEnum.VALIDATION_NOTICES_CONSULTATION,
        component: NoticePageWcComponent,
        canActivate: [AuthGuard]
      }, {
        path: AppInternalPathEnum.NOTICES,
        component: NoticeDashboardComponent,
        canActivate: [AuthGuard]
      }, {
        path: AppInternalPathEnum.VALIDATION_NOTICES,
        component: ValidationNoticeDashboardComponent,
        canActivate: [AuthGuard]
      }, {
        path: AppInternalPathEnum.GESTION_NOTICES,
        component: GestionNoticeDashboardComponent,
        canActivate: [AuthGuard]
      }, {
        path: AppInternalPathEnum.MY_NOTICES,
        component: MyNoticeDashboardComponent,
        canActivate: [AuthGuard]
      }, {
        path: AppInternalPathEnum.VALIDATION_SIP_NOTICES,
        component: StatistiqueSipNoticeDashboardComponent,
        canActivate: [AuthGuard]
      }, {
        path: AppInternalPathEnum.PLATEFORMES_CONFIGURATION,
        component: PlateformeConfigurationPageWcComponent,
        canActivate: [AuthGuard]
      }, {
        path: AppInternalPathEnum.ATEXO_CONFIGURATION,
        component: AtexoConfigurationPageWcComponent,
        canActivate: [AuthGuard]
      }, {
        path: AppInternalPathEnum.VALIDATION_SIP_NOTICES_DASHBOARD,
        component: ValidationSipNoticeDashboardComponent,
        canActivate: [AuthGuard]
      }, {
        path: AppInternalPathEnum.VALIDATION_ECO_NOTICES,
        component: StatistiqueEcoNoticeDashboardComponent,
        canActivate: [AuthGuard]
      }, {
        path: AppInternalPathEnum.VALIDATION_ECO_NOTICES_DASHBOARD,
        component: ValidationEcoNoticeDashboardComponent,
        canActivate: [AuthGuard]
      },
      {
        path: '',
        redirectTo: AppInternalPathEnum.ACCUEIL,
        pathMatch: 'prefix'
      }
    ],
  },// Public layout
  {
    path: AppInternalPathEnum.LOGIN,
    component: PublicLayoutComponent,
    children: [
      {
        path: AppInternalPathEnum.LOGIN_PARAMS,
        component: LoginPageWCComponent
      }, {
        path: AppInternalPathEnum.LOGIN_PARAMS_AGENT,
        component: LoginPageWCComponent
      },
      {
        path: AppInternalPathEnum.SESSION_EXPIRED,
        component: SessionExpiredPageWcComponent,
      },
      {
        path: '',
        component: LoginPageWCComponent,
        canActivate: [NotConnectedGuard]
      },
    ]
  },
  {path: '', redirectTo: '/login', pathMatch: 'full'}

];

export const routing = RouterModule.forRoot(appRoutes, {
  scrollOffset: [0, 0],
  scrollPositionRestoration: 'top',
  relativeLinkResolution: 'legacy'
});
