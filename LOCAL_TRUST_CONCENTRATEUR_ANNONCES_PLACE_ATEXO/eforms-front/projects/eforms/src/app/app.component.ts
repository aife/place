import {MenuSettingsService} from '@core-eforms/layout/settings/menu-settings.service';
import {Component, Inject, Injectable, OnInit} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {LoadingBarService} from '@ngx-loading-bar/core';
import {
  NavigationCancel,
  NavigationEnd,
  NavigationStart,
  RouteConfigLoadEnd,
  RouteConfigLoadStart,
  Router
} from '@angular/router';
import {DeviceDetectorService} from 'ngx-device-detector';
import {DOCUMENT} from '@angular/common';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {AppConstants} from "@core-eforms/constantes/app.constants";
import {State} from "@shared-global/store";
import {Store} from "@ngrx/store";
import {logoutUser} from "@shared-global/store/user/user.action";
import {BsModalService} from "ngx-bootstrap/modal";
import {showWarningToast} from "@shared-global/store/toast/toast.action";
import {ThemesService} from "@shared-global/core/services/themes.service";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";

@Component({
  selector: 'app-main',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.css']
})
@Injectable()
export class AppComponent implements OnInit {
  public _menuSettingsConfig: any;
  public _unsubscribeAll: Subject<any>;
  private _unsubscribeAllMenu: Subject<any>;
  showContent = false;
  warningToken = false;
  invalideToken = false;
  public title;
  interval;

  constructor(private spinner: NgxSpinnerService,
              @Inject(DOCUMENT) private document: Document,
              private router: Router,
              private themesService: ThemesService,
              private modalService: BsModalService,
              private store: Store<State>,
              private routingInterne: RoutingInterneService,
              public loader: LoadingBarService,
              private deviceService: DeviceDetectorService,
              public _menuSettingsService: MenuSettingsService
  ) {
    this._unsubscribeAll = new Subject();
    this._unsubscribeAllMenu = new Subject();
  }

  ngOnInit() {
    this._menuSettingsService.config
      .pipe(takeUntil(this._unsubscribeAllMenu))
      .subscribe((config) => {
        this._menuSettingsConfig = config;
      });
    this.subscribePageProgressBar();
    this.store.select(state => state.userReducer.accessToken).subscribe(value => {
      if (value) {
        this.checkTokenValidity(value);
        this.interval = setInterval(() => {
          this.checkTokenValidity(value);
        }, 60 * 1000);
        this.applyRemoteCss()

      }
    });

  }

  private checkTokenValidity(value: string) {
    let tokenDurationRemain = this.tokenDurationRemain(value);
    console.log('tokenDurationRemain', tokenDurationRemain)
    this.warningToken = tokenDurationRemain <= 10;
    this.invalideToken = tokenDurationRemain <= 0;
    if (this.warningToken && !this.invalideToken) {
      this.store.dispatch(showWarningToast({
        message: 'Il vous reste ' + tokenDurationRemain + ' minutes avant l\'expiration de votre session.',
        header: 'Attention'
      }))
    }
    if (this.invalideToken) {
      clearInterval(this.interval);
      this.store.dispatch(logoutUser());
      let config = {
        backdrop: true,
        ignoreBackdropClick: true,
        class: "modal-semi-full-width"
      };
      this.routingInterne.navigateFromContexte(AppInternalPathEnum.LOGIN + "/" + AppInternalPathEnum.SESSION_EXPIRED);
      this.showContent = false;
    }
  }

  private tokenDurationRemain(token: string): number {
    const tokenDuration = JSON.parse(atob(token.split('.')[1])).exp;
    const date = new Date();
    return Math.floor((tokenDuration - Math.floor(date.getTime() / 1000)) / 60);
  }

  async applyRemoteCss() {
    try {
      const cssContent = await this.themesService.getRemoteCss();
      if (cssContent) {
        const styleElement = document.createElement('style');
        styleElement.innerHTML = cssContent;
        document.head.appendChild(styleElement);
      }
    } catch (error) {
      console.error('Error fetching remote CSS:', error);
    }
  }

  private subscribePageProgressBar() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        // set page progress bar loading to start on NavigationStart event router
        this.loader.start();
      }
      if (event instanceof RouteConfigLoadStart) {
        this.loader.increment(35);
      }
      if (event instanceof RouteConfigLoadEnd) {
        this.loader.increment(75);
      }
      if (event instanceof NavigationEnd || event instanceof NavigationCancel) {
        // set page progress bar loading to end on NavigationEnd event router
        this.loader.complete();
        this.showContent = true;
        if (this.deviceService.isMobile() || window.innerWidth < AppConstants.MOBILE_RESPONSIVE_WIDTH) {
          if (document.body.classList.contains('menu-open')) {
            document.body.classList.remove('menu-open');
            document.body.classList.add('menu-close');
          }
        }
      }
    });
  }
}
