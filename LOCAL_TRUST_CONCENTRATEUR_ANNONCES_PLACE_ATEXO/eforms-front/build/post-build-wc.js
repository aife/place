const fs = require('fs-extra');
const concat = require('concat');
const distSourcingWebComponent = './dist/web-component-eforms/';

build = async () => {
  const files = [
    distSourcingWebComponent + 'runtime.js',
    distSourcingWebComponent + 'polyfills.js',
    distSourcingWebComponent + 'main.js'
  ];

  await concat(files, distSourcingWebComponent + 'web-component-eforms.js');
  files.forEach(file => fs.remove(file, (err) => {
    if (err) return console.log(err);
    console.log("Given file " + file + " is deleted");
  }))
  let index = distSourcingWebComponent + 'index.html';
  fs.remove(index, (err) => {
    if (err) return console.log(err);
    console.log("Given file " + index + " is deleted");
  });
}
build();
