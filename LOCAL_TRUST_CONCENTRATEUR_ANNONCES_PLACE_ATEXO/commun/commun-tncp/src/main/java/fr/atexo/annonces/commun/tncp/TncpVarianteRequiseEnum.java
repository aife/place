package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@AllArgsConstructor
@Getter
public enum TncpVarianteRequiseEnum {
    REQUIS("requis"),
    AUTORISE("autorisé"),
    NON_AUTORISE("non autorisé"),
    SANS_OBJET("sans objet");

    private final String value;


    @JsonCreator
    public static TncpVarianteRequiseEnum fromValue(String value) {
        for (TncpVarianteRequiseEnum operator : values()) {
            if (operator.getValue().equals(value)) {
                return operator;
            }
        }
        return null;
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
