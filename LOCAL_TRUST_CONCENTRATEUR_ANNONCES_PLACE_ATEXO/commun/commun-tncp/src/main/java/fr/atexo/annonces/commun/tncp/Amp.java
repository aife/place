package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Amp{
	private Boolean oui;
	private Boolean non;
}