package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class CritereCoutItem{
	private String critere;
	private String poids;
}