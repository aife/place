package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class Titulaires{
	private List<TitulaireItem> titulaire;
	private String publication;
}