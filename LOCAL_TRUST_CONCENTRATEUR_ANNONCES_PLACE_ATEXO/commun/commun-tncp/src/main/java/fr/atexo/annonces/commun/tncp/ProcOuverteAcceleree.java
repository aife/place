package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ProcOuverteAcceleree{
	private String justification;
}