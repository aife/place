package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AuLieuDe{
	private String date;
	private String vide;
	private Cpv cpv;
	private String texte;
}