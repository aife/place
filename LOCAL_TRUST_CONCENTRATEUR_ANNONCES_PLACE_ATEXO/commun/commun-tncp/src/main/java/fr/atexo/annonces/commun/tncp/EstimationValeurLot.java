package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class EstimationValeurLot{
	private Integer valeur;
	private String devise;
}