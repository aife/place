package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ConditionsParticipation{
	private SousTraitants sousTraitants;
	private OperateursEconomiques operateursEconomiques;
}