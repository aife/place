package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Form05{
	private ProcedureConjointe procedureConjointe;
	private AvisImplique avisImplique;
	private DescriptionMarche descriptionMarche;
	private Amp amp;
	private RenseignementsAdm renseignementsAdm;
	private Procedure procedure;
	private AvisInitial avisInitial;
	private EligibleMPS eligibleMPS;
	private String infoCompl;
	private RenseignementsCompl renseignementsCompl;
	private String pasAvisInitial;
	private Communication communication;
	private Situation situation;
}