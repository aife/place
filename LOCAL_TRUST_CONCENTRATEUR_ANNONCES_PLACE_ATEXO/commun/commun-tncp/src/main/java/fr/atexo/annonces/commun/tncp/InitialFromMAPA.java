package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class InitialFromMAPA{
	private InfosSup infosSup;
	private String marcheUnique;
	private Delais delais;
	private Justifications justifications;
	private Description description;
	private AdressesComplt adressesComplt;
	private Procedure procedure;
	private Criteres criteres;
	private Caracteristiques caracteristiques;
	private Lots lots;
	private EligibleMPS eligibleMPS;
	private Renseignements renseignements;
	private Duree duree;
	private String delaiUrgence;
	private Conditions conditions;
	private NatureMarche natureMarche;
}