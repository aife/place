package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Caracteristiques{
	private String optionsNon;
	private EstimationValeur estimationValeur;
	private Options options;
	private String quantites;
	private Variantes variantes;
	private OptionReconduction optionReconduction;
	private CalendrierMarchesulterieurs calendrierMarchesulterieurs;
	private TabCarLibre tabCarLibre;
	private TabCarMinMax tabCarMinMax;
	private String principales;
	private TabCar1 tabCar1;
}