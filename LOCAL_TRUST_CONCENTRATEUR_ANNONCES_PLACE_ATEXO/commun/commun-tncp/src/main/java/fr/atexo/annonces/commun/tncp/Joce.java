package fr.atexo.annonces.commun.tncp;

import lombok.Data;

import java.util.List;

@Data
public class Joce{
	private String datePublication;
	private String numAnnonce;
	private AvisPreInfo avisPreInfo;
	private List<AutrePubItem> autrePub;
	private AvisMarche avisMarche;
	private AvisSimplifie avisSimplifie;
	private AvisProfilAcheteur avisProfilAcheteur;
	private AvisExAnte avisExAnte;
	private AvisPeriodiqueIndicatif avisPeriodiqueIndicatif;
	private AvisSystemeQualification avisSystemeQualification;
}
