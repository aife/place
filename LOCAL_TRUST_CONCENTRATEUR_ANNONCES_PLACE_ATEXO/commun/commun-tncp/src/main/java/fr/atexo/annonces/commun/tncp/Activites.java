package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Activites{
	private String protectionSociale;
	private String education;
	private String sante;
	private String defense;
	private String ordreSecuritePub;
	private String logementDevCol;
	private String affairesEcoFi;
	private String loisirsCultureReligion;
	private String environnement;
	private String servicesGenerauxAdmPub;
	private String autre;
}