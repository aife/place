package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder(builderMethodName = "with")
public record Adresse(
		@JsonProperty("code") String code,
		@JsonProperty("codePays") String codePays,
		@JsonProperty("localite") String localite,
		@JsonProperty("nomVoie") String nomVoie,
		@JsonProperty("numeroVoie") String numeroVoie,
		@JsonProperty("typeVoie") String typeVoie
) {
}
