package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Procedure{
	private String procedureAdaptee;
	private String procedureAdapteeO;
	private String capaciteExercice;
	private String procedureAdapteeR;
	private String capaciteEcoFin;
	private String dateReceptionOffres;
	private Variantes variantes;
	private String criteresAttrib;
	private String capaciteTech;
	private String categorieAcheteur;
	private NbCandidatReduit nbCandidatReduit;
	private PresOffres presOffres;
	private AttributionSansNegociation attributionSansNegociation;
	private TechAchat techAchat;
	private String appelRestreint;
	private String marcheNegocie;
	private String appelOuvert;
	private String concoursRestreint;
	private String dialogueCompetitif;
	private String concoursOuvert;
	private String autres;
	private MarcheSansMiseEnConcurrence marcheSansMiseEnConcurrence;
	private MarcheNegocieSansPublication marcheNegocieSansPublication;
	private String appelRestreintAccelere;
	private String marcheNegocieAccelere;
	private String marcheNegociePublication;
	private NomsParticipants nomsParticipants;
	private Integer nbMaxParticipants;
	private Integer nbParticipants;
	private Integer nbMinParticipants;
	private String partenariatInnovation;
	private String procRestreinte;
	private String procConcNegociee;
	private ProcNegocieeSansPublication procNegocieeSansPublication;
	private String procOuverte;
	private String procNegociee;
	private ProcOuverteAcceleree procOuverteAcceleree;
	private ProcConcNegocieeAcceleree procConcNegocieeAcceleree;
	private ProcRestreinteAcceleree procRestreinteAcceleree;
	private String procAttributionAvecPublication;
	private ProcAttributionSansPublication procAttributionSansPublication;
	private Directive81 directive81;
	private Directive25 directive25;
	private Directive24 directive24;
	private Directive23 directive23;
	private String procNegocieeAvecAppelConc;
}