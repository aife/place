package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Directive24{
	private ProcNegocieeSansPublication procNegocieeSansPublication;
	private AttributionSansMiseEnConcurrence attributionSansMiseEnConcurrence;
	private Criteres criteres;
}