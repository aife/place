package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class SousTypeAvisItem{
	private String code;
	private String libelle;
}