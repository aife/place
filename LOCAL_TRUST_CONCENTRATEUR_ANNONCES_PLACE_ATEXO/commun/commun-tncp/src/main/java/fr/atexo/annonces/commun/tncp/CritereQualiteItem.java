package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class CritereQualiteItem{
	private String critere;
	private String poids;
}