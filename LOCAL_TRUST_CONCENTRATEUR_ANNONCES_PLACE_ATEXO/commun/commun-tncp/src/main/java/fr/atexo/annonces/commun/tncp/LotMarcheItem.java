package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class LotMarcheItem{
	private String numMarche;
	private String numLot;
	private AttributionNon attributionNon;
	private AttributionOui attributionOui;
	private String intitule;
	private String description;
	private Resultat resultat;
	private MotsClef motsClef;
}