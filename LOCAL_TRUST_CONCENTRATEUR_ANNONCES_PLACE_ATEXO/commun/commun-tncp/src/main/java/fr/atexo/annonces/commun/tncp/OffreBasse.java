package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class OffreBasse{
	private Integer valeur;
	private String devise;
}