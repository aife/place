package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ModalitesOuvertureOffres{
	private String ooPersAutorisees;
	private String ooLieu;
	private String ooDate;
	private String ooPersAutoriseesNon;
}