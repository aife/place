package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AccordCadre{
	private OperateurPlusieurs operateurPlusieurs;
	private Integer dureeAccordCadreAn;
	private String justifSuperieurSeptAns;
	private String operateurUnSeul;
	private Integer dureeAccordCadreMois;
	private ValeurTotaleAcquisitions valeurTotaleAcquisitions;
	private Integer maxNombreParticipants;
	private String justifSuperieurQuatreAns;
	private String justifSuperieurHuitAns;
	private Boolean oui;
	private Boolean non;
}
