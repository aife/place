package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Duree{
	private String dateJusquau;
	private Integer nbJours;
	private String dateACompterDu;
	private Integer nbMois;
	private String dateDebutTravaux;
	private String txtLibre;
	private String dateDebutPrestation;
}