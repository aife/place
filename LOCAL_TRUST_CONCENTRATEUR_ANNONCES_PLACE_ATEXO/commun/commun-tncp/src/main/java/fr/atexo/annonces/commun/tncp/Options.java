package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Options{
	private Integer calendrierPrevisionnelMois;
	private Integer calendrierPrevisionnelJours;
	private Integer nbReconductions;
	private String descriptionOptions;
	private NbReconductionsFourchette nbReconductionsFourchette;
}