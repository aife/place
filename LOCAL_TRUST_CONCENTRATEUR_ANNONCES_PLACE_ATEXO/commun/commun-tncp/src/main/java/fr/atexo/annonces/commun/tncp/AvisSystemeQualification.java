package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AvisSystemeQualification{
	private String datePublication;
	private String numAnnonce;
}