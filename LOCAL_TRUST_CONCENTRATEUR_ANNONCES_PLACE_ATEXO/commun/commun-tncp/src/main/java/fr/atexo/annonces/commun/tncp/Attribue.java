package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Attribue{
	private MontantAnnuel montantAnnuel;
	private String montantIndefini;
	private String trancheConditionnelle;
	private String trancheFerme;
	private Offres offres;
	private SousTraitanceOui sousTraitanceOui;
	private Titulaire titulaire;
	private Montant montant;
	private String sousTraitanceNon;
}