package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class DescriptionReduite{
	private MotsClef motsClef;
	private String objet;
	private LieuExecution lieuExecution;
	private MarcheTravaux marcheTravaux;
	private LieuExecutionLivraison lieuExecutionLivraison;
	private String marcheServices;
	private Cpv cpv;
	private MarcheFournitures marcheFournitures;
	private LieuLivraison lieuLivraison;
}