package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class PourcentValeurMarcheMisEnConcurrence{
	private Integer pourcentMin;
	private Integer pourcentMax;
}