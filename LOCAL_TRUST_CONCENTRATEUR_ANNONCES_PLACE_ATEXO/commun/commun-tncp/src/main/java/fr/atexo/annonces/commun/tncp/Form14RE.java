package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class Form14RE{
	private AvisInitial avisInitial;
	private RaisonRectif raisonRectif;
	private DescriptionMarche descriptionMarche;
	private String infoSup;
	private String nePasPublierJOUE;
	private List<InfosRectifItem> infosRectif;
}