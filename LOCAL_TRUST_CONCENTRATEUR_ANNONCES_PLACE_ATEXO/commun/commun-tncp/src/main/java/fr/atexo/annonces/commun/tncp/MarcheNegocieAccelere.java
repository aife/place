package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class MarcheNegocieAccelere{
	private String justification;
}