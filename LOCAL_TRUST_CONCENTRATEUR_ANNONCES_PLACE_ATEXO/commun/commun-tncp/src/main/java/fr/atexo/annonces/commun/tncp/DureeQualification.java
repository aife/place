package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class DureeQualification{
	private String dateQualifACompterDu;
	private String dureeIndeterminee;
	private String dateQualifJusquau;
}