package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder(builderMethodName = "with")
public record Compte(@JsonProperty("id") String id, @JsonProperty("email") String email, @JsonProperty("password") String password) {
}
