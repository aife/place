package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class LotMarche{
	private String numMarche;
	private String numLot;
	private AttributionOui attributionOui;
	private String intitule;
}