package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AvisConcerne{
	private String sad;
	private String accordCadre;
	private String standard;
	@JsonProperty("mise_En_Concurrence")
	private Boolean miseEnConcurrence;
	private Concours concours;
	private String preinformation;
	private String periodique;
	private AppelOffre appelOffre;
	private String attribution;
	private String appelOFFRE;
	private String directive1046;
	private String directive24;
	private String miseENCONCURRENCE;
	private String reductionDELAI;
	private String directive81;
	private String directive25;
	private String directive23;
}
