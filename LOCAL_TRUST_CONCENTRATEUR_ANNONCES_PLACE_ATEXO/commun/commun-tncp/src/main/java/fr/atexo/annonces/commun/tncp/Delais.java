package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Delais{
	private String envoiInvitation;
	private String receptionOffres;
	private String receptCandidatures;
	private Validite validite;
}