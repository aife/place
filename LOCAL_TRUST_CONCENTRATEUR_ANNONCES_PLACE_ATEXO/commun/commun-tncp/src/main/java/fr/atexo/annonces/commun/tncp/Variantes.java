package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Variantes{
	private Boolean oui;
	private Boolean non;
}