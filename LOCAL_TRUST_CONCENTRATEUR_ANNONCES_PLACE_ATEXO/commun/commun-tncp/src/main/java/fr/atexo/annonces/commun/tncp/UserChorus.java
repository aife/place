package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class UserChorus{
	private Boolean oui;
	private Boolean non;
}