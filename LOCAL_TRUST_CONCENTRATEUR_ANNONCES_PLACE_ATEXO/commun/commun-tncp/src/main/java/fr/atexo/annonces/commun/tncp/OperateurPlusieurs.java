package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class OperateurPlusieurs{
	private Integer maxNombreParticipants;
	private Integer nombreParticipants;
}