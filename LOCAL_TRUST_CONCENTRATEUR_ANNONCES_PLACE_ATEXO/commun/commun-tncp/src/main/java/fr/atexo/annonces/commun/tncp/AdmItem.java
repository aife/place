package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AdmItem{
	private Coord coord;
	private PersonnePhysique personnePhysique;
	private String personneMorale;
	private Adr adr;
}