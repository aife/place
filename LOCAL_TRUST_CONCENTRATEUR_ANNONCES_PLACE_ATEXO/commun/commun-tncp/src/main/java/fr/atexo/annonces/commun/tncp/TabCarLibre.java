package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class TabCarLibre{
	private List<TrItem> tr;
}