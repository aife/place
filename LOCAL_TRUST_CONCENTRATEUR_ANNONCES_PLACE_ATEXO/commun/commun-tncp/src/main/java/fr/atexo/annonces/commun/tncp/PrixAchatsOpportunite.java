package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class PrixAchatsOpportunite{
	private Integer valeur;
	private String devise;
}