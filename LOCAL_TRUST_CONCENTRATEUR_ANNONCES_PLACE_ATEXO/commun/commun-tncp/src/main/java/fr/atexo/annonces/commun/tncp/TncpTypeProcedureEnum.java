package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TncpTypeProcedureEnum {
    PROC_ADAPTEE_OUVERTE("procédure adaptée ouverte"),
    PROC_ADAPTEE_RESTREINTE("procédure adaptée restreinte"),
    AO_RESTREINT("appel d'offres restreint"),
    AO_OUVERT("appel d'offres ouvert"),
    PRO_NEGOCIATION("procédure avec négociation"),
    MARCHE_PUBLIC_SANS_PUB("marché public sans publicité ni mise en concurrence préalable"),
    DIALOGUE_COMPETITIF("dialogue compétitif");

    private final String value;


    @JsonCreator
    public static TncpTypeProcedureEnum fromValue(String code) {
        for (TncpTypeProcedureEnum operator : values()) {
            if (operator.name().equalsIgnoreCase(code)) {
                return operator;
            }
        }
        return null;
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
