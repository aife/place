package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class RenseignementsCompl{
	private InstanceChargeeRecours instanceChargeeRecours;
	private IntroductionRecoursRenseignements introductionRecoursRenseignements;
	private WebCommande webCommande;
	private String introductionRecoursPrecisions;
	private WebPaiement webPaiement;
	private InstanceChargeeMediation instanceChargeeMediation;
	private WebFacturation webFacturation;
	private String recurrentOui;
	private String recurrentNon;
	private String calendrierPublication;
}