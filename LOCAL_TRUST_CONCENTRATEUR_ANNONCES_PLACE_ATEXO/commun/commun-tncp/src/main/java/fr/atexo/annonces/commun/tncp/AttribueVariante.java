package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AttribueVariante{
	private Boolean oui;
	private Boolean non;
}