package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class PersonnePhysique{
	private String nom;
	private String fonc;
	private String civilite;
	private String pren;
}