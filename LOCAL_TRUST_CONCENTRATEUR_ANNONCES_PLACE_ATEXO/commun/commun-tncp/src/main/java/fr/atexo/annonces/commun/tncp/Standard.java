package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Standard{
	private String directive1046;
	private String directive24;
}