package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class DivisibleLotOui{
	private String tousLots;
	private String lotsReserves;
	private Integer nbMaxLotAttrib;
	private Integer nbMaxLotOffre;
	private String unLot;
}