package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AdmEtTech{
	private Coord coord;
	private PersonnePhysique personnePhysique;
	private String personneMorale;
	private Adr adr;
}