package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class DeptPublication{
	private String deptPub;
	private List<String> rappel;
}