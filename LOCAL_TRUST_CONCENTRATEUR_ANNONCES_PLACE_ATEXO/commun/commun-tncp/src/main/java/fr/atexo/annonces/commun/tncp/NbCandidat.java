package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class NbCandidat{
	private String criteresLimitation;
	private Integer nombreMin;
	private Integer nombreMax;
	private Integer nombre;
	private ReductionProgressive reductionProgressive;
}