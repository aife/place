package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class LieuExecutionLivraison{
	private String ville;
	private Voie voie;
	private String cp;
}