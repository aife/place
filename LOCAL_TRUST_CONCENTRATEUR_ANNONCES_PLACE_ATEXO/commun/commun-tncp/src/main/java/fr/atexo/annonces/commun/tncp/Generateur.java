package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Generateur{
	private String idAppli;
	private String versionAppli;
}