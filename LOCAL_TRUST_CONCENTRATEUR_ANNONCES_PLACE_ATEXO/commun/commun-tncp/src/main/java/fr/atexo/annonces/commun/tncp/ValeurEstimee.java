package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ValeurEstimee{
	private Fourchette fourchette;
	private Integer valeur;
	private String devise;
}