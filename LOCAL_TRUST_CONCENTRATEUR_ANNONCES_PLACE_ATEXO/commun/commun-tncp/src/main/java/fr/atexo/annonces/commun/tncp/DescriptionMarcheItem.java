package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class DescriptionMarcheItem{
	private OffreBasse offreBasse;
	private Cpv cpv;
	private String description;
	private ValeurTotal valeurTotal;
	private String divisibleLotOui;
	private String numeroReference;
	private LotTncp lot;
	private Lots lots;
	private MotsClef motsClef;
	private EstimationValeurTotal estimationValeurTotal;
	private OffreHaute offreHaute;
	private String calculEstimation;
	private String divisibleLotNon;
	private String titreMarche;
	private NatureMarche natureMarche;
	private String dateLancementPassation;
	private Valeur valeur;
}
