package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Lire{
	private String date;
	private String vide;
	private Cpv cpv;
	private String texte;
}