package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Joue{
	private FormulaireJoueEtBoampOption formulaireJoueEtBoampOption;
	private OrganismeTncp organisme;
	private FormulaireJoueEtBoampOblig formulaireJoueEtBoampOblig;
}
