package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class DocSecurite{
	private String oui;
	private Non non;
}