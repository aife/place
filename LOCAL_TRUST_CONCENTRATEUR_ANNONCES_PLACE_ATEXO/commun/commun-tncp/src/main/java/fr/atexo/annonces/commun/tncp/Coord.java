package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Coord{
	private String mel;
	private String tel;
	private String poste;
	private String fax;
	private String url;
}