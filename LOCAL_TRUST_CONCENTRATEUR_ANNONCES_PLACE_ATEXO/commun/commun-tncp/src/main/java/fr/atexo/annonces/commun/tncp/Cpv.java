package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Builder;
import lombok.NonNull;

@Builder(builderMethodName = "with")
public record Cpv(@NonNull String value) {

    @JsonValue
    @Override
    public String value() {
        return value;
    }
}

