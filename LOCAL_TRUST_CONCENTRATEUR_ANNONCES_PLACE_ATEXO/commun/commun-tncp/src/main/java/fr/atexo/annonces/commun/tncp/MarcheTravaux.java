package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class MarcheTravaux{
	private String execution;
	private String conceptionRealisation;
}