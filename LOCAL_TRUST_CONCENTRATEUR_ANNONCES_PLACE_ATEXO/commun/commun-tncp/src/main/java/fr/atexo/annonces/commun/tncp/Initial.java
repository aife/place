package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Initial{
	private AvisImplique avisImplique;
	private InfosSup infosSup;
	private String marcheUnique;
	private ConditionsParticipation conditionsParticipation;
	private NbCandidat nbCandidat;
	private Delais delais;
	private String joceNon;
	private Justifications justifications;
	private String dateLimiteHabilitation;
	private Description description;
	private AdressesComplt adressesComplt;
	private String procedure;
	private Criteres criteres;
	private ProcedureRecours procedureRecours;
	private Caracteristiques caracteristiques;
	private Lots lots;
	private Renseignements renseignements;
	private Joce joce;
	private Duree duree;
	private Conditions conditions;
	private NatureMarche natureMarche;
	private EligibleMPS eligibleMPS;
	private AvisConcerne avisConcerne;
	@JsonProperty("rens_Complt")
	private RensComplt rensComplt;
}
