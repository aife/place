package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class WebPaiement{
	private Boolean oui;
	private Boolean non;
}