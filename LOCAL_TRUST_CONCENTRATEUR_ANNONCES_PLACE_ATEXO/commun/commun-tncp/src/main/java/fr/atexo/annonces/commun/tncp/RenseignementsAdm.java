package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class RenseignementsAdm{
	private Langue langue;
	private String numAnnonce;
	private DecisionJuryContraignante decisionJuryContraignante;
	private MembresJury membresJury;
	private String primeOui;
	private MarcheAttrAuLaureat marcheAttrAuLaureat;
	private String dateEnvoiInvitation;
	private String detailsPaiement;
	private String primeNon;
	private String montantPrime;
	private String receptionInteret;
	private String datePublication;
	private String abandonSAD;
	private String abandonProcedure;
	private String infoOuverture;
	private String dateOuverture;
	private Integer validiteOffreDuree;
	private String lieuOuverture;
	private String validiteOffreDate;
	private String lancementProcAttrib;
}