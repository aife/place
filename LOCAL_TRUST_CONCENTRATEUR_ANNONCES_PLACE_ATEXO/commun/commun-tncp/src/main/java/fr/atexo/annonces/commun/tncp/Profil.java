package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.*;

@Getter
public enum Profil {
	AVIS_PUBLICITE_GESTION("AVIS_PUBLICITE_GESTION");

	@JsonValue
	private String value;

	Profil(String value) {
		this.value = value;
	}

	Profil() {
	}
}
