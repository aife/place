package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class CapaTechDoc{
	private Boolean oui;
	private Boolean non;
}