package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class InformComplementaire{
	private Oui oui;
	private Boolean non;
	private String autresInformComplementaire;
}