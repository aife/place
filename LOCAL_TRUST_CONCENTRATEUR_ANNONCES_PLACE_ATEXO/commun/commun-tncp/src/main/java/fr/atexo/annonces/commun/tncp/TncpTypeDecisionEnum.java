package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TncpTypeDecisionEnum {
    DECISION_ATTRIBUTION_MARCHE("attribution"),
    DECISION_DECLARATION_SANS_SUITE("sans suite autre"),
    DECISION_DECLARATION_INFRUCTUEUX("infructuosité"),
    DECISION_SELECTION_ENTREPRISE("attribution"),
    DECISION_ATTRIBUTION_ACCORD_CADRE("attribution"),
    DECISION_ADMISSION_SAD("attribution"),
    DECISION_AUTRE("sans suite autre");

    private final String value;


    @JsonCreator
    public static TncpTypeDecisionEnum fromValue(String code) {
        for (TncpTypeDecisionEnum operator : values()) {
            if (operator.name().equalsIgnoreCase(code)) {
                return operator;
            }
        }
        return null;
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
