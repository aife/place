package fr.atexo.annonces.commun.tncp;

import lombok.Data;

import java.util.List;

@Data
public class Rectificatif{
	private AvisImplique avisImplique;
	private AvisInitial avisInitial;
	private Renseignements renseignements;
	private Description description;
	private Procedure procedure;
	private MotsClef motsClef;
	private String typeProcedure;
	private LieuExecution lieuExecution;
	private LieuExecutionLivraison lieuExecutionLivraison;
	private List<InfosRectifItem> infosRectif;
	private String objet;
	private LieuLivraison lieuLivraison;
}
