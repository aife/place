package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class PresOffres{
	private String autorisee;
	private String exigee;
	private String interdite;
}