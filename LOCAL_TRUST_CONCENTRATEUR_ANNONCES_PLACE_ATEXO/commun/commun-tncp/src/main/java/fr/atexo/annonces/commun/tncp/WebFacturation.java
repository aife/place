package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class WebFacturation{
	private Boolean oui;
	private Boolean non;
}