package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class Justifications{
	private String detailFournitures;
	private AutorisationSpecifique autorisationSpecifique;
	private String redressementJudiciaire;
	private String articlesD8222;
	private String certificatPaysOrigine;
	private String indicationTitresOperateur;
	private QualificationsProfessionnelles qualificationsProfessionnelles;
	private String documentsAutreOperateur;
	private String dc2;
	private String dc1;
	private String dc4;
	private String dc3;
	private String traductionFrancaiseAttributaire;
	private List<String> justificationAutre;
	private String listeTravaux;
	private String chiffresAffaires;
	private String defenseNationaliteCandidat;
	private String articleL3243;
	private String articlesL1221L3243;
	private String article43;
	private String equipementFournituresServices;
	private String listeFournituresServices;
	private String indicationTitresCadres;
	private String justificationComment;
	private String defenseHabilitationSecret;
	private String effectifs;
	private String bilans;
	private ConformiteFournitures conformiteFournitures;
	private String equipementTechnique;
	private String attestationObligationsFiscales;
	private String articlesL5212;
	private String risquesProfessionels;
	private String traductionFrancaiseCandidat;
}