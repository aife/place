package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class OffresRecues{
	private Integer nbOffresPME;
	private Integer nbOffresElec;
	private String publication;
	private Integer nbOffresEU;
	private Integer nbOffresNonEU;
	private AttribGroupement attribGroupement;
	private Integer nbOffresRecues;
	private Integer nbParticipantsNonUE;
	private Integer nbParticipants;
	private Integer nbParticipantsPME;
}