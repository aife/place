package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Raisons{
	private String circonstancesImprevues;
	private String besoinsSupplementaires;
}