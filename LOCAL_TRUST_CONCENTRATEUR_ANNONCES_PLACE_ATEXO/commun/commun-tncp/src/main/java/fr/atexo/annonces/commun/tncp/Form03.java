package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Form03{
	private ProcedureConjointe procedureConjointe;
	private AvisImplique avisImplique;
	private AvisInitial avisInitial;
	private String infoCompl;
	private DescriptionMarche descriptionMarche;
	private Amp amp;
	private AvisConcerne avisConcerne;
	private RenseignementsCompl renseignementsCompl;
	private Attribution attribution;
	private String pasAvisInitial;
	private RenseignementsAdm renseignementsAdm;
	private Procedure procedure;
}