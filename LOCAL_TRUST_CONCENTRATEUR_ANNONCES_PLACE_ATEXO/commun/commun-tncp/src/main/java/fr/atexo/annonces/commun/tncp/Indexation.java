package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Indexation {
    private String criteresEnvironnementaux;
    private String nomOrganisme;
    private String objetAvis;
    private String criteresSociaux;
    private String dateFinDiffusion;
}
