package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AvisPreInfo{
	private String datePublication;
	private String numAnnonce;
}