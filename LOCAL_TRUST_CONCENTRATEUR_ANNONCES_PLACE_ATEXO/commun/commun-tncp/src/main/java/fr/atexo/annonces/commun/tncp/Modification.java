package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Modification{
	private String lieuExecutionLivraison;
	private String descriptionMarche;
	private DureeLot dureeLot;
	private Titulaire titulaire;
	private Cpv cpv;
	private AttribGroupement attribGroupement;
	private Montant montant;
	private ValeurTotaleAvant valeurTotaleAvant;
	private Titulaires titulaires;
	private Raisons raisons;
	private ValeurTotaleApres valeurTotaleApres;
	private String descriptionModification;
	private LieuCodeNUTS lieuCodeNUTS;
	private String justification;
}