package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Singular;

import java.util.Set;

@Builder(builderMethodName = "with")
public record Avis(
        @JsonProperty("user") Utilisateur utilisateur,
        @JsonProperty("siret") String siret,

        @JsonProperty("targetApplication") String targetApplication,
        @JsonProperty("service") String service,
        @JsonProperty("profiles") @Singular Set<Profil> profils,
        @JsonProperty("payload") Payload payload

) {
}
