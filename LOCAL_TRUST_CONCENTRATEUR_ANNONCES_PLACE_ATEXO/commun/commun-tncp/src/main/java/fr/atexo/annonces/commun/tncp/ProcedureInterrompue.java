package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ProcedureInterrompue{
	private String dateEnvoiInitiale;
	private String numAnnonce;
}