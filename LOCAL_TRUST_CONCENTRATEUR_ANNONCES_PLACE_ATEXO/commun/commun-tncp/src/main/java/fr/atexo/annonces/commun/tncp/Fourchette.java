package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Fourchette{
	private Integer valeurBasse;
	private Integer valeurHaute;
}