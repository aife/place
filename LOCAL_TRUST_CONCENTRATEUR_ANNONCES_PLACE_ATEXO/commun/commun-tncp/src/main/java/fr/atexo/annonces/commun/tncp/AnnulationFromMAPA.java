package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AnnulationFromMAPA{
	private AvisInitial avisInitial;
	private String infosAnnulation;
	private Description description;
	private Procedure procedure;
}