package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Participation{
	private String adresseContact;
	private String urlParticipation;
	private AdresseParticipation adresseParticipation;
}