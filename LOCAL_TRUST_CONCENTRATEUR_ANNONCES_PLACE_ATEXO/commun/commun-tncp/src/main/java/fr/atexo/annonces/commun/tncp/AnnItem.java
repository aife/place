package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AnnItem{
	private Joue joue;
	private Divers divers;
	private Mapa mapa;
	private Indexation indexation;
	private FnSimple fnSimple;
	private FormAD formAD;
	private Fns fns;
	private String idAnnEmetteur;
}