package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Pme{
	private Boolean oui;
	private Boolean non;
}