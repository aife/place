package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class CriteresEvaluation{
	private String publication;
	private String value;
}