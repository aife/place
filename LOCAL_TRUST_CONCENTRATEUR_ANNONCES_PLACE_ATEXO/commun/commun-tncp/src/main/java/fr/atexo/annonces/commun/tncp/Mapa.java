package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Mapa{
	private RectificatifFromMAPA rectificatifFromMAPA;
	private GroupeInfosCommunes groupeInfosCommunes;
	private IntentionConclureFromMAPA intentionConclureFromMAPA;
	private OrganismeTncp organisme;
	private InitialFromMAPA initialFromMAPA;
	private AnnulationFromMAPA annulationFromMAPA;
	private AttributionFromMAPA attributionFromMAPA;
}
