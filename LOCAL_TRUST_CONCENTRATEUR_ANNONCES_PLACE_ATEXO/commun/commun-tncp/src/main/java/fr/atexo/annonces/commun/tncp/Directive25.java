package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Directive25{
	private ProcNegocieeSansPublication procNegocieeSansPublication;
	private AttributionSansMiseEnConcurrence attributionSansMiseEnConcurrence;
	private Criteres criteres;
}