package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.time.ZonedDateTime;

@Builder(builderMethodName = "with")
public record Identifiers(
        @JsonProperty("avisType") String avisType,
        @JsonProperty("procedureId") String procedureId,
        @JsonProperty("consultationReference") String consultationReference,
        @JsonProperty("consultationTitle") String consultationTitle,
        @JsonProperty("consultationId") String consultationId,
        @JsonProperty("boampDateSent") ZonedDateTime boampDateSent,
        @JsonProperty("tedDateSent") ZonedDateTime tedDateSent,
        @JsonProperty("noticeId") String noticeId,
        @JsonProperty("noticeTitle") String noticeTitle

) {
}
