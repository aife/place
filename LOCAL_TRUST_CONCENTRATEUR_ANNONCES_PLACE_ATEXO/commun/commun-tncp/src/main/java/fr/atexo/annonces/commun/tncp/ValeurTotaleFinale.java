package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ValeurTotaleFinale{
	private Fourchette fourchette;
	private Integer valeur;
	private String devise;
	private Integer tva;
}