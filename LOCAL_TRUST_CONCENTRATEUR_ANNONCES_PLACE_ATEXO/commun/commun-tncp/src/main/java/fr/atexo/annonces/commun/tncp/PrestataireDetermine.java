package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class PrestataireDetermine{
	private String raisonsExclusivite;
	private String raisonsArtistiques;
	private String raisonsTechniques;
	private String proprieteIntellectuelle;
}