package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AvisImplique{
	private RectificationAjout rectificationAjout;
	private ProcedureIncomplete procedureIncomplete;
	private AccordCadre accordCadre;
	private EnchereElectronique enchereElectronique;
	private String sad;
	private ReductionProgressive reductionProgressive;
	private String procAttribDesc;
	private String urlProceduresNationales;
	private String attribOffresInitiales;
}
