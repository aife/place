package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class SousTraitanceOui{
	private Integer partPct;
	private PartValeur partValeur;
	private String partInconnue;
	private String sousTraitanceDesc;
}