package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class PossibilitePartieSousTraitee{
	private Boolean oui;
	private Boolean non;
}