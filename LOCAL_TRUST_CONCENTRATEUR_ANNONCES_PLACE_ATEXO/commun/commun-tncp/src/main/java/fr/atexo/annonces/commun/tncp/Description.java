package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class Description{
	private MotsClef motsClef;
	private LieuExecutionLivraison lieuExecutionLivraison;
	private Amp amp;
	private Cpv cpv;
	private List<String> lieuCodeNUTS;
	private String titreMarche;
	private String objet;
	private SousTraitance sousTraitance;
	private LieuExecution lieuExecution;
	private MarcheTravaux marcheTravaux;
	private String marcheServices;
	private MarcheFournitures marcheFournitures;
	private LieuLivraison lieuLivraison;
	private NatureMarche natureMarche;
	private Lots lots;
	private String dateLancementPassation;
	private DureeProceduresPassation dureeProceduresPassation;
	private String marcheUnique;
	private String infosComplementaires;
}