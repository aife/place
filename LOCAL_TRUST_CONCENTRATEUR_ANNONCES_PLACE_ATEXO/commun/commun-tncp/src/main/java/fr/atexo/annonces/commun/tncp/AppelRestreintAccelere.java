package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AppelRestreintAccelere{
	private String justification;
}