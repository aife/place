package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class FormulairesItem{
	private String code;
	private String directiveAssociee;
	private List<SousTypeAvisItem> sousTypeAvis;
	private String noticeInformation;
	private String montant;
	private List<Object> criteres;
	private String typeNorme;
	private String intitule;
	private String urlNoticeInformation;
	private List<String> typeAvis;
}