package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class IntentionConclureFromMAPA{
	private InfosSup infosSup;
	private String joceNon;
	private Description description;
	private Procedure procedure;
	private Criteres criteres;
	private ProcedureRecours procedureRecours;
	private AutresInformations autresInformations;
	private AvisInitial avisInitial;
	private AvisCouvert avisCouvert;
	private Joce joce;
	private Attribution attribution;
	private String pasAvisInitial;
	private NatureMarche natureMarche;
}