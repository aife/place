package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder(builderMethodName = "with")
public record Payload(
        @JsonProperty("consultation") Consultation consultation,
        @JsonProperty("consultationReferences") References references,
        @JsonProperty("piampCredentials") Compte compte
) {
}
