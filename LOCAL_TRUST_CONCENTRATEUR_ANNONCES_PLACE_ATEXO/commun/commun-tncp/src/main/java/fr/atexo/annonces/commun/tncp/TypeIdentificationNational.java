package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class TypeIdentificationNational{
	private String siren;
	private String irep;
	private String tvaIntra;
	private String tahiti;
	private String frwf;
	private String ridet;
	private String siret;
}