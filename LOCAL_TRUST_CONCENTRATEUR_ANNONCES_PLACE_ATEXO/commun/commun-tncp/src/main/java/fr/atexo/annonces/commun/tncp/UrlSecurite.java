package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class UrlSecurite{
	private Oui oui;
	private String non;
}