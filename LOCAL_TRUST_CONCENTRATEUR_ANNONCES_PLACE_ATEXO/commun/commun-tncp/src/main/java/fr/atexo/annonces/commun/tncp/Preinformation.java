package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Preinformation{
	private String directive81;
	private String directive24;
	private String qualification;
	private String periodique;
	private String standard;
	private String miseEnConcurrence;
}