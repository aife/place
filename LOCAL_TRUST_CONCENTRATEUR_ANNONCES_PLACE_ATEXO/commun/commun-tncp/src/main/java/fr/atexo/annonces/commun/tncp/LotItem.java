package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.atexo.annonces.commun.wrapper.JsonWrapped;
import lombok.Builder;

@Builder(builderMethodName = "with")
public record LotItem(
        @JsonProperty("codeCpvPrincipal") Cpv cpvPrincipal,
        @JsonProperty("decision") TncpTypeDecisionEnum decision,
        @JsonProperty("description") String description,
        @JsonProperty("lieuExecution") LieuExecution lieuExecution,
        @JsonProperty("montant") Double montant,
        @JsonProperty("numero") String numero,
        @JsonWrapped("quantite") @JsonProperty("unite") String unite,
        @JsonWrapped("quantite") @JsonProperty("valeur") Double valeur
) {
}
