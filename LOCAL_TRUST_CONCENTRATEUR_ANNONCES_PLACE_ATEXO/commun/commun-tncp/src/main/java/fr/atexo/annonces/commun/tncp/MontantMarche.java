package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class MontantMarche{
	private String categorie;
	private String code;
	private String libelle;
}