package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Valeurs{
	private Integer recette;
	private Integer estimationInitiale;
	private Integer montant;
	private Integer avantagesFinanciers;
	private String devise;
}