package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class NbReconductionsFourchette{
	private Integer valeurBasse;
	private Integer valeurHaute;
}