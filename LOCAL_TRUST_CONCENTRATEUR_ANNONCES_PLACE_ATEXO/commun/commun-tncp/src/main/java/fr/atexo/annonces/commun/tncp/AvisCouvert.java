package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AvisCouvert{
	private String directive200417;
	private String directive200981;
	private String directive200418;
}