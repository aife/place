package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AppelOffre{
	private String standard;
	private String qualification;
	private String periodique;
}