package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Tranches{
	private Boolean oui;
	private Boolean non;
}