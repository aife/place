package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class Form22{
	private ProcedureConjointe procedureConjointe;
	private AvisImplique avisImplique;
	private List<DescriptionMarcheItem> descriptionMarche;
	private RenseignementsAdm renseignementsAdm;
	private Procedure procedure;
	private AvisInitial avisInitial;
	private EligibleMPS eligibleMPS;
	private String infoCompl;
	private AvisConcerne avisConcerne;
	private RenseignementsCompl renseignementsCompl;
	private Attribution attribution;
	private String pasAvisInitial;
	private Communication communication;
	private Situation situation;
}