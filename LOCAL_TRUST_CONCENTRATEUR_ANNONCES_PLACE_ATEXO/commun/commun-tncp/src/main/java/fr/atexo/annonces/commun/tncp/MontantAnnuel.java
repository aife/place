package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class MontantAnnuel{
	private Integer montantMiniAnnuel;
	private String devise;
	private Integer montantMaxiAnnuel;
}