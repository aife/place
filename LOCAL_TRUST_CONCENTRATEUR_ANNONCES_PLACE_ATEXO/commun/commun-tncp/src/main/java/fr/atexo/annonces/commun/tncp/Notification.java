package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.time.ZonedDateTime;

@Builder(builderMethodName = "with")
public record Notification(
        @JsonProperty("notifInfoId") Long notifInfoId,
        @JsonProperty("donneesNotif") String donneesNotif,
        @JsonProperty("bulleMetierInfos") BulleMetierInfos bulleMetierInfos,
        //@JsonProperty("dateReception") ZonedDateTime dateReception,
        @JsonProperty("dossierId") String dossierId,
        @JsonProperty("complementId") String complementId,
        @JsonProperty("idNotifMetier") Long idNotifMetier
) {
}
