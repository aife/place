package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AttributionSansMiseEnConcurrence{
	private String justificationAttribution;
	private String explication;
	private String justificationHorsDirective;
	private String justificationServicesListes;
}