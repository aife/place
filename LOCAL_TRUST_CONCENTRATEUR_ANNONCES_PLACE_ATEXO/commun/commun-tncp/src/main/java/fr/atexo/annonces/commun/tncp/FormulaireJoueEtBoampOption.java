package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class FormulaireJoueEtBoampOption{
	private String dateEnvoi;
	private Form03 form03;
	private Form25 form25;
	private Form04 form04;
	private Form15 form15;
	private Form01 form01;
	private Form23 form23;
	private Form02 form02;
	private Form24 form24;
	private Form21 form21;
	private Form22 form22;
	private Form14DE form14DE;
	private Form20 form20;
	private GroupeInfosCommunes groupeInfosCommunes;
	private Form14RE form14RE;
	private Form08 form08;
	private String nePasPublierBOAMP;
	private Form05 form05;
	private Form16 form16;
	private Form06 form06;
}