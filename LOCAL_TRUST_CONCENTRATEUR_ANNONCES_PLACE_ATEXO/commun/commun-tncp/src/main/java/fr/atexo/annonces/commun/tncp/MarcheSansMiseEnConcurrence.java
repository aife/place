package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class MarcheSansMiseEnConcurrence{
	private String urgenceImperieuse;
	private String matieresPremieres;
	private PrestataireDetermine prestataireDetermine;
	private String raisonAttributionLicite;
	private String infructueuxOuverte;
	private String marcheAccordCadre;
	private String prestationsIdentiques;
	private String recherche;
	private ConditionsAvantageuses conditionsAvantageuses;
	private String infructueuxRestreinte;
	private String laureatsConcours;
	private String marcheComplementaire;
	private String infructueuxProcedureNegociee;
	private String offresIrregulieres;
	private AutreJustification autreJustification;
}