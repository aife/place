package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class JustificationProcedure{
	private String infructueux;
	private String urgenceImperieuse;
	private String laureatsConcours;
	private String matieresPremieres;
	private PrestataireDetermine prestataireDetermine;
	private String achatOpportunite;
	private String marcheComplementaire;
	private String prestationsIdentiques;
	private String recherche;
	private ConditionsAvantageuses conditionsAvantageuses;
	private String infructueuxRestreinte;
	private String infructueuxOuverte;
	private String autreService;
	private String delaisIncompatibles;
	private String infructueuxProcNegocieeSansPublication;
	private String serviceMaritime;
	private String infructueuxDialogueCompetitif;
	private String offresIrregulieres;
}