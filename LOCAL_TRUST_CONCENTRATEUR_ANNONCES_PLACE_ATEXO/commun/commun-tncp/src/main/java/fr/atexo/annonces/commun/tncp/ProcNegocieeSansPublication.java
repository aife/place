package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ProcNegocieeSansPublication{
	private String justificationAttribution;
	private JustificationProcedure justificationProcedure;
	private String explication;
}