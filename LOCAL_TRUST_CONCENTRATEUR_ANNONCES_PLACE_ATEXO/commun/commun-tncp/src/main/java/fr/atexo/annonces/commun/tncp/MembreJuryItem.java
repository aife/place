package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class MembreJuryItem{
	private PersonnePhysique personnePhysique;
	private String personneMorale;
}