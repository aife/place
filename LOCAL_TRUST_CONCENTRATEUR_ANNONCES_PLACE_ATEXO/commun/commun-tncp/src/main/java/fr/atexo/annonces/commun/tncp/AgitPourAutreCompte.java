package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AgitPourAutreCompte{
	private Oui oui;
	private String non;
}
