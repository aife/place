package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder(builderMethodName = "with")
public record PublicationDetails(@JsonProperty("boampPublication") PublicationDetail boampPublication,
                                 @JsonProperty("tedPublication") PublicationDetail tedPublication

) {
}
