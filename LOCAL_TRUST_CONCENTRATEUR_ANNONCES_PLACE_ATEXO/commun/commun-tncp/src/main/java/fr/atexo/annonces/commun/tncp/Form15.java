package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Form15{
	private AvisImplique avisImplique;
	private String infoCompl;
	private DescriptionMarche descriptionMarche;
	private Amp amp;
	private AvisConcerne avisConcerne;
	private RenseignementsCompl renseignementsCompl;
	private Attribution attribution;
	private RenseignementsAdm renseignementsAdm;
	private Procedure procedure;
}