package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder(builderMethodName = "with")
public record References(
        @JsonProperty("technicalId") Integer technicalId,
        @JsonProperty("reference") String reference,
        @JsonProperty("externalReference") String externalReference
) {
}
