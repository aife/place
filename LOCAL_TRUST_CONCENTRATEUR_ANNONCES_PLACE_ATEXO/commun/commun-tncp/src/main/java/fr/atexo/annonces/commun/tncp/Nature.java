package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Optional;

@RequiredArgsConstructor
@Getter
public enum Nature {
    TRAVAUX("travaux", "/api/v2/referentiels/nature-prestations/1"),
    FOURNITURES("fournitures", "/api/v2/referentiels/nature-prestations/2"),
    SERVICES("services", "/api/v2/referentiels/nature-prestations/3");

    @NonNull
    @JsonValue
    private final String value;
    @NonNull
    private final String url;

    public static Optional<Nature> from(String nature) {
        return Arrays.stream(values()).filter(n -> n.value.equalsIgnoreCase(nature) || n.url.equalsIgnoreCase(nature)).findFirst();
    }


}
