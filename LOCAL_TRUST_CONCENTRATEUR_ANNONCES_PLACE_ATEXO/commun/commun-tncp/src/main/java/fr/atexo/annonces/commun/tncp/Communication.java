package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Communication{
	private String telContact;
	private String urlDocConsul;
	private String identifiantInterne;
	private String adresseMailContact;
	private String urlProfilAch;
	private DocSecurite docSecurite;
	private String autre;
	private String nomContact;
	private UrlSecurite urlSecurite;
	private Participation participation;
	private String urlOutilLogiciel;
	private DocumentMarche documentMarche;
	private InfosCompl infosCompl;
}