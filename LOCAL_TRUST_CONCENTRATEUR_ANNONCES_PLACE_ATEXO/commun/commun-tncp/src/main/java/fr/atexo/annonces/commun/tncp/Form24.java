package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class Form24{
	private EligibleMPS eligibleMPS;
	private String infoCompl;
	private List<DescriptionMarcheItem> descriptionMarche;
	private Amp amp;
	private RenseignementsCompl renseignementsCompl;
	private RenseignementsAdm renseignementsAdm;
	private Communication communication;
	private Situation situation;
}