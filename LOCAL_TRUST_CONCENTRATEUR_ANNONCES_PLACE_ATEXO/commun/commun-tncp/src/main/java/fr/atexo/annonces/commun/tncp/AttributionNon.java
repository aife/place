package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AttributionNon{
	private ProcedureInterrompue procedureInterrompue;
	private String procedureInfructueuse;
}