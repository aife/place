package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class Langue{
	private String langAutre;
	private List<String> langEC;
}