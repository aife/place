package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ProcedureIncomplete{
	private String procedureInterrompue;
	private String procedureInfructueuse;
	private String marcheNonAttribue;
	private String nouvellePublication;
}