package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class NatureMarche{
	private Travaux travaux;
	private Fournitures fournitures;
	private String services;
	private ReservationMarche reservationMarche;
	private LieuExecution lieuExecution;
	private Integer dureeMois;
	private String description;
	private Integer valeurEstimee;
	private String intitule;
	private CodeCPV codeCPV;
	private NatureMarche natureMarche;
	private Tranches tranches;
	private MotsClef motsClef;
	private String criteresAttrib;
}
