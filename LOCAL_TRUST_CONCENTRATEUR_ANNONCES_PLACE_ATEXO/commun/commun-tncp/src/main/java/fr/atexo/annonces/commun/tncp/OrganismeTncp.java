package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class OrganismeTncp {
	private CorrespondantPRM correspondantPRM;
	private ActivitesOrganisme activitesOrganisme;
	private String acheteurPublic;
	private Coord coord;
	private AgitPourAutreCompte agitPourAutreCompte;
	private ClassificationOrganismeJOUE classificationOrganismeJOUE;
	private String classificationOrganisme;
	private String pointDeContact;
	private Adr adr;
	private String urlProfilAcheteur;
	private String ville;
	private GrpCommandes grpCommandes;
	private TypeIdentificationNational typeIdentificationNational;
	private String codeIdentificationNational;
	private String cp;
	private String nomContact;
	private String nomOfficiel;
	private String urlParticipation;
	private String codeNUTS;
	private PersonnePhysique personnePhysique;
	private String urlInformation;
	private String urlPouvoirAdjudicateur;
}
