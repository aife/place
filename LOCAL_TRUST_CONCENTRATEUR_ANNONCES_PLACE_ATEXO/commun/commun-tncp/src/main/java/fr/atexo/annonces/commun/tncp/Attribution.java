package fr.atexo.annonces.commun.tncp;

import lombok.Data;

import java.util.List;

@Data
public class Attribution{
	private AvisInitial avisInitial;
	private Attribution attribution;
	private String pasAvisInitial;
	private NatureMarche natureMarche;
	private List<LotMarcheItem> lotMarche;
	private Resultat resultat;
	private AutresInformations autresInformations;
	private DescriptionReduite descriptionReduite;
	private CriteresAttribution criteresAttribution;
	private String procedure;
}
