package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class LotTncp {
	private InfosSup infosSup;
	private MotsClef motsClef;
	private String reconductionNon;
	private String lieuExecutionLivraison;
	private String reconductionDesc;
	private Cpv cpv;
	private String description;
	private LieuCodeNUTS lieuCodeNUTS;
	private String reconductionOui;
	private Criteres criteres;
	private DureeQualification dureeQualification;
	private String numLot;
	private String infosComplementaires;
	private String intituleLot;
	private Candidats candidats;
	private DureeLot dureeLot;
	private EstimationValeur estimationValeur;
	private String justification;
	private Directive25 directive25;
	private Directive24 directive24;
	private Directive23 directive23;
	private Directive81 directive81;
}
