package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Form17{
	private AvisImplique avisImplique;
	private InfosSup infosSup;
	private String marcheUnique;
	private ConditionsParticipation conditionsParticipation;
	private NbCandidat nbCandidat;
	private Delais delais;
	private String joceNon;
	private Justifications justifications;
	private String dateLimiteHabilitation;
	private Description description;
	private AdressesComplt adressesComplt;
	private Procedure procedure;
	private Criteres criteres;
	private ProcedureRecours procedureRecours;
	private Caracteristiques caracteristiques;
	private Lots lots;
	private Renseignements renseignements;
	private Joce joce;
	private Duree duree;
	private Conditions conditions;
	private NatureMarche natureMarche;
}