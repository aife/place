package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class GroupeInfosCommunes{
	private String dateEnvoi;
	private DeptPublication deptPublication;
	private Facturation facturation;
}