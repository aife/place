package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class CodeCPV{
	private ObjetPrincipal objetPrincipal;
	private List<ObjetComplementaireItem> objetComplementaire;
}