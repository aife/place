package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Resultat{
	private String infructueux;
	private Attribue attribue;
	private String sansSuite;
	private String descSousTraitance;
	private Offres offres;
	private SousTraitanceOui sousTraitanceOui;
	private Titulaire titulaire;
	private Integer nbOffresRecues;
	private Montant montant;
	private String sousTraitanceNon;
	private Directive200981CE directive200981CE;
	private String dateAttribution;
	private EstimationInitiale estimationInitiale;
	private Integer nbOffresElectroniqueRecues;
	private Integer mensuel;
	private Integer annuel;
}