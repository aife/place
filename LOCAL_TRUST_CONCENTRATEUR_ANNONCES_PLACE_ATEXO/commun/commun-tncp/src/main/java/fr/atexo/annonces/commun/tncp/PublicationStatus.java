package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.time.ZonedDateTime;

@Builder(builderMethodName = "with")
public record PublicationStatus(@JsonProperty("annonceId") String annonceId,
                                @JsonProperty("journalOfficielId") String journalOfficielId,
                                @JsonProperty("state") String state

) {
}
