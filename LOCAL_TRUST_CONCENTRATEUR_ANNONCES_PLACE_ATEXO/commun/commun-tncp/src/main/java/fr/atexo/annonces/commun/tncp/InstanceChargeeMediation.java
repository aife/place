package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class InstanceChargeeMediation{
	private Coord coord;
	private String codeIdentificationNational;
	private String personneMorale;
	private Adr adr;
	private String nomOfficiel;
}