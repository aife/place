package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class EditionAndParutionAndNumAnnParution{
	private NumAnnParution numAnnParution;
	private Joce joce;
	private String datePublication;
	private Parution parution;
	private Edition edition;
	private Integer localDateTime;
}
