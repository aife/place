package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class OptionReconduction{
	private String reconductionNon;
	private String optionsNon;
	private Options options;
	private Reconduction reconduction;
}