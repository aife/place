package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ExclusionOffresBasses{
	private Boolean oui;
	private Boolean non;
}