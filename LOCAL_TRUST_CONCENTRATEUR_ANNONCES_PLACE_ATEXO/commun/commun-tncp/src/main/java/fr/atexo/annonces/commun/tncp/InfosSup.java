package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class InfosSup{
	private IndicationNomQualificationPersonnel indicationNomQualificationPersonnel;
	private ConditionsObtentionSpec conditionsObtentionSpec;
	private String marchePeriodiqueNon;
	private String fondsCommunautairesNon;
	private String prestationReservee;
	private String prestationReserveeNon;
	private String fondsCommunautairesRef;
	private String marchePeriodiqueCalendrier;
	private ModalitesOuvertureOffres modalitesOuvertureOffres;
	private String conditionsModePaiement;
	private String optionOui;
	private String optionDesc;
	private String optionNon;
	private String reconductionNon;
	private String catalogueElecNon;
	private String reconductionDesc;
	private String varianteOui;
	private String catalogueElecOui;
	private String reconductionOui;
	private String varianteNon;
}