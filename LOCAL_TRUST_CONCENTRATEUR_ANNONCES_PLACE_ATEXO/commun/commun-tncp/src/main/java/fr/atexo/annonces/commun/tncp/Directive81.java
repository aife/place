package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Directive81{
	private ProcNegocieeSansPublication procNegocieeSansPublication;
	private AttributionSansMiseEnConcurrence attributionSansMiseEnConcurrence;
	private Criteres criteres;
	private PourcentValeurMarcheMisEnConcurrence pourcentValeurMarcheMisEnConcurrence;
	private String contratsSousTraitanceMisEnConcurrence;
}