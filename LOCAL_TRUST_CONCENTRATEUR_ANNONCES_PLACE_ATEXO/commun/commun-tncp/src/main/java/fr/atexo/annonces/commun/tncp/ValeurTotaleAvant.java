package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ValeurTotaleAvant{
	private Integer valeur;
	private String devise;
}