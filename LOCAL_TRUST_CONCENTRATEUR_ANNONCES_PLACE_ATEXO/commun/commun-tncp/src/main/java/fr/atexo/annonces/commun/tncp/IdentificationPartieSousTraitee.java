package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class IdentificationPartieSousTraitee{
	private Boolean oui;
	private Boolean non;
}