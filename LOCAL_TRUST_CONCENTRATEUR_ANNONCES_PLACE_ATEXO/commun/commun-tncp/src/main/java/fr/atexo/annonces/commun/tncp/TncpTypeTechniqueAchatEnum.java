package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TncpTypeTechniqueAchatEnum {
    ACCORD_CADRE("accord-cadre"),
    SYSTEME_ACQUISATION_DYNAMIQUE("système d'acquisition dynamique"),
    SANS_OBJET(null);

    private final String value;

    @JsonCreator
    public static TncpTypeTechniqueAchatEnum fromValue(String value) {
        if (value == null) return null;
        for (TncpTypeTechniqueAchatEnum operator : values()) {
            if (operator.name().equalsIgnoreCase(value)) {
                return operator;
            }
        }
        return null;
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
