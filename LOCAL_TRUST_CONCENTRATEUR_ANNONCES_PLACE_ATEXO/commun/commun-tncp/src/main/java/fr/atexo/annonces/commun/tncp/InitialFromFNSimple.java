package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class InitialFromFNSimple{
	private Lots lots;
	private InformComplementaire informComplementaire;
	private Procedure procedure;
	private Communication communication;
	private NatureMarche natureMarche;
}