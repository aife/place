package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Valeur{
	private OffreHaute offreHaute;
	private OffreBasse offreBasse;
	private String publication;
	private ValeurTotal valeurTotal;
	private EstimationValeurTotal estimationValeurTotal;
	private Offres offres;
	private Montant montant;
	private PrimeAttribuee primeAttribuee;
}