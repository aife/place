package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class QualificationItem{
	private String methodes;
	private String conditions;
}