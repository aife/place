package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class RectificatifFromFNSimple{
	private AvisInitial avisInitial;
	private List<String> infosRectif;
	private NatureMarche natureMarche;
}