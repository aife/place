package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class AdmOuTech{
	private List<TechItem> tech;
	private List<AdmItem> adm;
}