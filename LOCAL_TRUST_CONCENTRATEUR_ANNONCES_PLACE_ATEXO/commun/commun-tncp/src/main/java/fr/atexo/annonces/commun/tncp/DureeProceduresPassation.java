package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class DureeProceduresPassation{
	private String dateJusquau;
	private Integer nbJours;
	private String dateACompterDu;
	private Integer nbMois;
}