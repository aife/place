package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Renseignements{
	private String rensgComplt;
	private String idMarche;
}