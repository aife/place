package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class DocPayants{
	private Oui oui;
	private String non;
}