package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AttribGroupement{
	private Boolean oui;
	private Boolean non;
}