package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class RensComplt{
	private IntLibre intLibre;
	private TxtLibre txtLibre;
}