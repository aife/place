package fr.atexo.annonces.commun.tncp;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderMethodName = "with")
public class CriterePondereItem {
    private String critere;
    private Integer criterePCT;
    private String criterePoids;
}
