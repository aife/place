package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class Form16{
	private InfosSup infosSup;
	private Renseignements renseignements;
	private Justifications justifications;
	private AdressesComplt adressesComplt;
	private Conditions conditions;
	private List<ObjetsMarcheItem> objetsMarche;
	private Situation situation;
}