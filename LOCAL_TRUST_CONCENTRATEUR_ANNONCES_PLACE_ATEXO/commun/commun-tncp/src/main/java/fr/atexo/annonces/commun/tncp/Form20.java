package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Form20{
	private AvisInitial avisInitial;
	private DescriptionMarche descriptionMarche;
	private RenseignementsCompl renseignementsCompl;
	private Attribution attribution;
	private RenseignementsAdm renseignementsAdm;
	private Modification modification;
}