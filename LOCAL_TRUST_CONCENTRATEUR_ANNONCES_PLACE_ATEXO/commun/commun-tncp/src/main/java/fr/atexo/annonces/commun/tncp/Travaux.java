package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Travaux{
	private String execution;
	private String executionOuvrage;
	private String conceptionRealisation;
}