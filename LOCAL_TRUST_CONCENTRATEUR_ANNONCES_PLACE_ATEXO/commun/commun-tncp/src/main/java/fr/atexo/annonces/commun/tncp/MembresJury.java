package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class MembresJury{
	private List<MembreJuryItem> membreJury;
}