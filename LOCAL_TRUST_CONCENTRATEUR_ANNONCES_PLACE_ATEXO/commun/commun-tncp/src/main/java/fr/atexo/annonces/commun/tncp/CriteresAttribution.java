package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.util.Set;

@Builder(builderMethodName = "with")
public record CriteresAttribution(
        @JsonProperty("criteres") Set<String> criteres,
        @JsonProperty("criteresPonderes") Set<CriterePondereItem> criteresPonderes
) {
}
