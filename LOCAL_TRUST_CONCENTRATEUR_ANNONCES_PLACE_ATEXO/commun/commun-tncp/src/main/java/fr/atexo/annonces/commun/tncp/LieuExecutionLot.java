package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class LieuExecutionLot{
	private String ville;
	private Voie voie;
	private String cp;
}