package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Form14DE{
	private AvisImplique avisImplique;
	private AvisInitial avisInitial;
	private Renseignements renseignements;
	private Description description;
	private String nePasPublierJOUE;
	private Procedure procedure;
}