package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ProcedureRecours{
	private InstanceChargeeRecours instanceChargeeRecours;
	private IntroductionRecoursRenseignements introductionRecoursRenseignements;
	private String introductionRecoursPrecisions;
	private InstanceChargeeMediation instanceChargeeMediation;
}