package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AvisMarche{
	private String datePublication;
	private String numAnnonce;
}