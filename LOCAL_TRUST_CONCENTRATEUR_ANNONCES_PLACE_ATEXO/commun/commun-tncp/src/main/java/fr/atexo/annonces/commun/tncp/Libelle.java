package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Libelle{
	private String ville;
	private String tvaintracommunautaire;
	private String adr4;
	private String adr3;
	private ClasseProfit classeProfit;
	private String cp;
	private String siret;
	private String adr2;
	private String adr1;
	private String denomination;
	private String pays;
}