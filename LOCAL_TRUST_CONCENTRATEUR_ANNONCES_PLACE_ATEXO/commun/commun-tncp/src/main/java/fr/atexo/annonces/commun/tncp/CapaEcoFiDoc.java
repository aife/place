package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class CapaEcoFiDoc{
	private Boolean oui;
	private Boolean non;
}