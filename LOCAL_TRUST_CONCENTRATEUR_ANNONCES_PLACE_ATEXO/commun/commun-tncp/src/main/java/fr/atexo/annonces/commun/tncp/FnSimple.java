package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class FnSimple{
	private AnnulationFromFNSimple annulationFromFNSimple;
	private IntentionConclureFromFNSimple intentionConclureFromFNSimple;
	private RectificatifFromFNSimple rectificatifFromFNSimple;
	private GroupeInfosCommunes groupeInfosCommunes;
	private Attribution attribution;
	private InitialFromFNSimple initialFromFNSimple;
	private OrganismeTncp organisme;
}
