package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder(builderMethodName = "with")
public record BulleMetierInfos(@JsonProperty("bulleMetier") String bulleMetier,
                               @JsonProperty("adresseWeb") String adresseWeb, @JsonProperty("pathLogo") String pathLogo,
                               @JsonProperty("courriel") String courriel, @JsonProperty("urlCGU") String urlCGU) {
}
