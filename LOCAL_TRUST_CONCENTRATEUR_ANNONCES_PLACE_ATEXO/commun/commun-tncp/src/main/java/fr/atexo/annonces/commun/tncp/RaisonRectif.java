package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class RaisonRectif{
	private String modifInfoOriginales;
	private String pubTEDNonConforme;
}