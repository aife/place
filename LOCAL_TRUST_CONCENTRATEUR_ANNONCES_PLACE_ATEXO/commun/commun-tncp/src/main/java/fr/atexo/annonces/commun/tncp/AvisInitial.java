package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AvisInitial{
	private EditionAndParutionAndNumAnnParution editionAndParutionAndNumAnnParution;
	private String idWeb;
}