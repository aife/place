package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class OffreHaute{
	private Integer valeur;
	private String devise;
}