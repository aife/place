package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class IndicationNomQualificationPersonnel{
	private Boolean oui;
	private Boolean non;
}