package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class TechAchat{
	private String catalogueElec;
	private String enchereElec;
	private String sad;
	private String accordCadre;
	private String concours;
	private String autres;
	private String sysQuali;
}