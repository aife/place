package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class RensComplementaires{
	private Coord coord;
	private PersonnePhysique personnePhysique;
	private String codeIdentificationNational;
	private String personneMorale;
	private String pointDeContact;
	private Adr adr;
}