package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ReservationMarche{
	private Oui oui;
	private Boolean non;
	private String structureInsertion;
	private String entrepriseAdaptee;
	private String servicesSociaux;
}