package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class PrimeAttribuee{
	private Integer valeur;
	private String devise;
}