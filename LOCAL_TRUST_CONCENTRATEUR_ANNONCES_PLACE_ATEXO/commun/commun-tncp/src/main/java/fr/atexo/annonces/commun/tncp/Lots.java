package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class Lots{
	private List<LotItem> lot;
	private String tousLots;
	private String plusieursLot;
	private String unLot;
	private Lots lots;
	private Boolean oui;
	private Boolean non;
}