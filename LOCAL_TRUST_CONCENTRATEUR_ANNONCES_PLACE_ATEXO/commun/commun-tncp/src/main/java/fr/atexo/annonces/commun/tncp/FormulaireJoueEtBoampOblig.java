package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class FormulaireJoueEtBoampOblig{
	private Form12 form12;
	private Form13 form13;
	private GroupeInfosCommunes groupeInfosCommunes;
	private Form07 form07;
	private Form18 form18;
	private Form17 form17;
}