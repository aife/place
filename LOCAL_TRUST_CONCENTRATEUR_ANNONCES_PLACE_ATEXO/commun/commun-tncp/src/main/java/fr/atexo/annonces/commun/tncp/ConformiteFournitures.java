package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ConformiteFournitures{
	private String precisions;
}