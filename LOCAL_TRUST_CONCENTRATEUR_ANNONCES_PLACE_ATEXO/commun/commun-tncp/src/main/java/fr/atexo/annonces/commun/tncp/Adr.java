package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Adr{
	private String lieuDit;
	private String ville;
	private Voie voie;
	@JsonProperty("voie_e")
	private VoieE voieE;
	private Integer cx;
	private String cp;
	@JsonProperty("cp_e")
	private String cpE;
	private Integer bp;
	private String pays;
	private TypeVoieE typeVoieE;
	private TypeVoie typeVoie;
	private String paysFrance;
}
