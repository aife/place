package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ObjetsMarcheItem{
	private AvisImplique avisImplique;
	private Description description;
	private NatureMarche natureMarche;
	private Caracteristiques caracteristiques;
}