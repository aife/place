package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.time.ZonedDateTime;

@Builder(builderMethodName = "with")
public record PublicationDetail(
        @JsonProperty("id") String id,
        @JsonProperty("date") ZonedDateTime date,
        @JsonProperty("url") String url

) {
}
