package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AnnulationFromFNSimple{
	private AvisInitial avisInitial;
	private String infosAnnulation;
	private NatureMarche natureMarche;
}