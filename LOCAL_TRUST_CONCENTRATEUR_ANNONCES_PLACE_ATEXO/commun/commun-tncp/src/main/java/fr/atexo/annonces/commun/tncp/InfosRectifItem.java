package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class InfosRectifItem{
	private String auLieuDe;
	private String lire;
	private String ajouter;
	private String apresLaMention;
	private String supprimer;
	private Adresse adresse;
	private String rubrique;
	private String lireDate;
	private String auLieuDeDate;
	private String rubDelais;
}