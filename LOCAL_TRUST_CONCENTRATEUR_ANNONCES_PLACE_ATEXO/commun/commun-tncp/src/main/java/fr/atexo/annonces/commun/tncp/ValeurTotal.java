package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ValeurTotal{
	private Integer valeur;
	private String devise;
}