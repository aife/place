package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder(builderMethodName = "with")
public record Utilisateur(
		@JsonProperty("email") String email,
		@JsonProperty("firstName") String prenom,
		@JsonProperty("lastName") String nom
) {
}
