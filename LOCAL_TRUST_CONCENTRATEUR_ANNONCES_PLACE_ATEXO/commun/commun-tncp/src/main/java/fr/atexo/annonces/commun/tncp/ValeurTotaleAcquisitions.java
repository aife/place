package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ValeurTotaleAcquisitions{
	private String frequenceValeurMarches;
	private ValeurEstimee valeurEstimee;
}