package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class PartValeur{
	private Integer valeur;
	private String devise;
}