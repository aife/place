package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class NbCandidatReduit{
	private Oui oui;
	private Boolean non;
}