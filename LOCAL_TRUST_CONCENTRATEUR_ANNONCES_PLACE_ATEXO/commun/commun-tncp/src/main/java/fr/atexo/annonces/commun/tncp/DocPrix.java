package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class DocPrix{
	private String devise;
	private Integer value;
}