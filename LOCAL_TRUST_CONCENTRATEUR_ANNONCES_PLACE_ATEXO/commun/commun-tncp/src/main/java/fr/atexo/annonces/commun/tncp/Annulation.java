package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Annulation{
	private AvisImplique avisImplique;
	private AvisInitial avisInitial;
	private Renseignements renseignements;
	private Description description;
	private Procedure procedure;
}