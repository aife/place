package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Voie{
	private String typevoie;
	private String num;
	private String nomvoie;
}