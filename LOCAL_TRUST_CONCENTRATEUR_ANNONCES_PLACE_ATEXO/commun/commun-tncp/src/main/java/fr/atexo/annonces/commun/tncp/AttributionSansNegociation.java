package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AttributionSansNegociation{
	private Boolean oui;
	private Boolean non;
}