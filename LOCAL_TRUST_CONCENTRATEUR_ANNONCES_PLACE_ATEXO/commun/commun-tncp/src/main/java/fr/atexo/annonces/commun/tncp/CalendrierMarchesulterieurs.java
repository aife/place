package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class CalendrierMarchesulterieurs{
	private Integer calendrierUlterieursMois;
	private Integer calendrierUlterieursJours;
}