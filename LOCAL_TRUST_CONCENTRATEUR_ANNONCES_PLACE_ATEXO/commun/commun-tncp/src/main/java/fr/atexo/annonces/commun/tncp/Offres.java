package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Offres{
	private Integer offreElevee;
	private Integer offreBasse;
	private String devise;
	private Integer tva;
}