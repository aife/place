package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AttributionFromMAPA{
	private AutresInformations autresInformations;
	private AvisInitial avisInitial;
	private DescriptionReduite descriptionReduite;
	private Attribution attribution;
	private CriteresAttribution criteresAttribution;
	private String pasAvisInitial;
	private Procedure procedure;
}