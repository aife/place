package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class OriginePays{
	private String origineUE;
	private String origineNonUE;
}