package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class TabCarMinMax{
	private List<TrItem> tr;
}