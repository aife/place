package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AnnulationFromDivers{
	private AvisInitial avisInitial;
	private String infosAnnulation;
	private MotsClef motsClef;
	private String typeProcedure;
	private LieuExecution lieuExecution;
	private LieuExecutionLivraison lieuExecutionLivraison;
	private Description description;
	private String objet;
	private LieuLivraison lieuLivraison;
}