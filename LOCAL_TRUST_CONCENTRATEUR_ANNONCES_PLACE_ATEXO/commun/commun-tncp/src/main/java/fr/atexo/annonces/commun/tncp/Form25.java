package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class Form25{
	private AvisInitial avisInitial;
	private String infoCompl;
	private List<DescriptionMarcheItem> descriptionMarche;
	private Amp amp;
	private RenseignementsCompl renseignementsCompl;
	private Attribution attribution;
	private String caracteristiquesAttribution;
	private String pasAvisInitial;
	private RenseignementsAdm renseignementsAdm;
	private Procedure procedure;
}