package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class LieuLivraison{
	private String ville;
	private Voie voie;
	private String cp;
}