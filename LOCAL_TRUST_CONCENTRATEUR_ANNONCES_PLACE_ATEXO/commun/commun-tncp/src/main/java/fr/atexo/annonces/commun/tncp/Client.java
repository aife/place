package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Builder(builderMethodName = "with")
public record Client(@JsonProperty("client_id") @NonNull String clientId, @JsonProperty("client_secret") @NonNull String clientSecret) {
}
