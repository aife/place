package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AutreJustification{
	private String servicesHorsDirective;
	private String servicesDansDirective;
}