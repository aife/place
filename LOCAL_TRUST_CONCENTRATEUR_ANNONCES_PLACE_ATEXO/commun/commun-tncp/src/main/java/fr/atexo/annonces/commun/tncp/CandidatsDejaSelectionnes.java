package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class CandidatsDejaSelectionnes{
	private Boolean oui;
	private Boolean non;
}