package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class Critere{
	private List<FormulairesItem> formulaires;
	private TypeOrganisme typeOrganisme;
	private MontantMarche montantMarche;
	private TypeMarche typeMarche;
	private Integer id;
	private TypeAvis typeAvis;
}