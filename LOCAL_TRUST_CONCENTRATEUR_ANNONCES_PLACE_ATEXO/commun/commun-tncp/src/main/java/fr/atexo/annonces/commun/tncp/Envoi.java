package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Envoi{
	private Coord coord;
	private PersonnePhysique personnePhysique;
	private String codeIdentificationNational;
	private String personneMorale;
	private String pointDeContact;
	private Adr adr;
	private String conditionsRemise;
}