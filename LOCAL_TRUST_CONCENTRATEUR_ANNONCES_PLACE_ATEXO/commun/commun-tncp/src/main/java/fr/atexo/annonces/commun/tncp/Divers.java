package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Divers{
	private AnnulationFromDivers annulationFromDivers;
	private Initial initial;
	private GroupeInfosCommunes groupeInfosCommunes;
	private TypeAdresseCompleteObligatoire typeAdresseCompleteObligatoire;
	private Attribution attribution;
	private Rectificatif rectificatif;
}