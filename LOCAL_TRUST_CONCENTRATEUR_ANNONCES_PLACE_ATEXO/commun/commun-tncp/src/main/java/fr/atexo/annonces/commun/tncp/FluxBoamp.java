package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class FluxBoamp{
	private String dateEnvoi;
	private String idJoue;
	private String infosRejet;
	private String numParutionBoamp;
	private String datePublicationJoue;
	private String etat;
	private String urlJoue;
	private String idAnnonce;
	private String datePublicationBoamp;
	private String infosRegles;
	private String urlBoamp;
	private String numAnnonceJoue;
	private Integer id;
	private String nomFichier;
	private String xmlEnvoye;
}