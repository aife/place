package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AdresseParticipation{
	private Coord coord;
	private String codeNUTS;
	private String codeIdentificationNational;
	private String pointDeContact;
	private Adr adr;
	private String nomOfficiel;
	private String urlProfilAcheteur;
}