package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Fns{
	private Defense defense;
	private GroupeInfosCommunes groupeInfosCommunes;
	private OrganismeTncp organisme;
}
