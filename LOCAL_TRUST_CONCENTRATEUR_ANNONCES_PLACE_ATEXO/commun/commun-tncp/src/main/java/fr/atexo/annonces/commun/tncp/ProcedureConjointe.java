package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class ProcedureConjointe{
	private String appelOffreConjoint;
	private String acheteurCentral;
	private List<AdressePouvoirAdjudicateurItem> adressePouvoirAdjudicateur;
	private String appelOffreConjointLoi;
}