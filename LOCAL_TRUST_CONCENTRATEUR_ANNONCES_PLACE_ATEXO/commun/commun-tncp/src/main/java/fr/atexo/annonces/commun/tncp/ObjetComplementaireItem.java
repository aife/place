package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class ObjetComplementaireItem{
	private String classPrincipale;
	private List<String> classSupplementaire;
}