package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Conditions{
	private String autresConditionsNon;
	private String euro;
	private String modFinancement;
	private String formeJuridique;
	private String garanties;
	private String langueFR;
	private Langue langue;
	private String autresConditionsOui;
}