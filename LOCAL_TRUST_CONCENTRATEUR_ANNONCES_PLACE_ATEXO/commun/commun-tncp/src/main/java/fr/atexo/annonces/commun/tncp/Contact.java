package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.RequiredArgsConstructor;

@Builder(builderMethodName = "with")
public record Contact(
		@JsonProperty("adresse") Adresse adresse,
		@JsonProperty("adresseElectronique") String email,
		@JsonProperty("nom") String nom,
		@JsonProperty("prenom") String prenom,
		@JsonProperty("raisonSociale") String raisonSociale,
		@JsonProperty("siret") String siret,
		@JsonProperty("telephone") String telephone
) {
}
