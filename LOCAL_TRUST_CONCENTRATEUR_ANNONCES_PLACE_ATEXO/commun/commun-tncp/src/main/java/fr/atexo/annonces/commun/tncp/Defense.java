package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Defense{
	private Annulation annulation;
	private Initial initial;
	private String intentionConclure;
	private String attribution;
	private Rectificatif rectificatif;
}