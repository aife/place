package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ActivitesOrganisme{
	private ActivitesSecteursSpeciaux activitesSecteursSpeciaux;
	private Activites activites;
}