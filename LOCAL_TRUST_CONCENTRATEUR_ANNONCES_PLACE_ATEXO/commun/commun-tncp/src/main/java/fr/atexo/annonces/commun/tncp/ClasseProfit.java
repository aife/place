package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ClasseProfit{
	private String debiteurEtranger;
	private String departement;
	private String etablissementsPublicsSante;
	private String autresOrganismesPublics;
	private String commune;
	private String debiteurPrivePersMorale;
	private String groupementCollectivites;
	private String debiteurPrivePersPhysique;
	private String region;
	private String etat;
}