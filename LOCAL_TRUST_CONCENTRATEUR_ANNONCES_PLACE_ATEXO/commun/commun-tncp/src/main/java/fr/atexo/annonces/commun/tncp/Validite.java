package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Validite{
	private String dateJusquau;
	private Integer nbJours;
	private Integer nbMois;
}