package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Candidats{
	private Integer nbMaxCandidat;
	private Integer nbMinCandidat;
	private Integer nbEnvisageCandidat;
	private String critereCandidat;
}