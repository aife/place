package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class Form04{
	private ProcedureConjointe procedureConjointe;
	private AvisImplique avisImplique;
	private EligibleMPS eligibleMPS;
	private List<DescriptionMarcheItem> descriptionMarche;
	private Amp amp;
	private AvisConcerne avisConcerne;
	private RenseignementsCompl renseignementsCompl;
	private RenseignementsAdm renseignementsAdm;
	private Procedure procedure;
	private Communication communication;
	private Situation situation;
}