package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class InstanceChargeeRecours{
	private Coord coord;
	private String codeIdentificationNational;
	private String personneMorale;
	private Adr adr;
	private String nomOfficiel;
}