package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class FormAD{
	private String logo;
	private Facturation facturation;
	private String textArea;
}