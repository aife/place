package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Situation{
	private String executionMarche;
	private Qualifications qualifications;
	private MarcheReserve marcheReserve;
	private ProfessionParticuliere professionParticuliere;
	private String executionPersonnel;
	private String criteresSelection;
	private String formeJuridique;
	private String critereParticipation;
	private String modalitesPaiement;
	private String capaEcoFiInfo;
	private CapaEcoFiDoc capaEcoFiDoc;
	private String situationPropre;
	private String capaEcoFiMin;
	private CapaTechDoc capaTechDoc;
	private String capaTechInfo;
	private String capaTechMin;
	private String garantiesExigees;
}