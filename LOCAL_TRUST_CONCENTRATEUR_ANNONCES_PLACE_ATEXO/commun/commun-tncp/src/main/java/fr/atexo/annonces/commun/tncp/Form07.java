package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Form07{
	private ProcedureConjointe procedureConjointe;
	private AvisImplique avisImplique;
	private EligibleMPS eligibleMPS;
	private String infoCompl;
	private DescriptionMarche descriptionMarche;
	private AvisConcerne avisConcerne;
	private RenseignementsCompl renseignementsCompl;
	private RenseignementsAdm renseignementsAdm;
	private Communication communication;
	private Situation situation;
}