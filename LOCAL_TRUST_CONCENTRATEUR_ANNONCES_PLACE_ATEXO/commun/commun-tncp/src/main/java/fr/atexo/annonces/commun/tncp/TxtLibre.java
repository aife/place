package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class TxtLibre{
	private List<String> champsLibre;
}