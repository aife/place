package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AutrePubItem{
	private String datePublication;
	private String numAnnonce;
}