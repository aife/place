package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class FluxShal{
	private String htmlEnvoye;
	private String mailShal;
	private String adresseFacturation;
	private Integer id;
	private String telecopieur;
	private String objetmail;
}