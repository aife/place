package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AutresInformations{
	private String dateAttribution;
	private Integer nbParticipants;
	private ValeurTotaleEstimee valeurTotaleEstimee;
	private Integer nbOffresRecues;
	private PrimeAttribuee primeAttribuee;
	private String informations;
	private Integer nbParticipantsEtrangers;
	private AvisConcerne avisConcerne;
	private String idMarche;
	private ValeurTotaleFinale valeurTotaleFinale;
}