package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class TypeAdresseCompleteObligatoire{
	private CorrespondantPRM correspondantPRM;
	private ActivitesOrganisme activitesOrganisme;
	private String acheteurPublic;
	private Coord coord;
	private AgitPourAutreCompte agitPourAutreCompte;
	private String classificationOrganismeJOUE;
	private String classificationOrganisme;
	private String pointDeContact;
	private Adr adr;
	private String urlProfilAcheteur;
}