package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class PartSousTraitee{
	private Integer pourcentageMax;
	private Integer pourcentageMin;
}