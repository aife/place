package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ClassificationOrganismeJOUE{
	private String value;
	private String autre;
}