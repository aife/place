package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Directive200981CE{
	private PourcentValeurMarcheMisEnConcurrence pourcentValeurMarcheMisEnConcurrence;
	private String contratsSousTraitanceMisEnConcurrence;
}