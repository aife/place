package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class RectificatifFromMAPA{
	private AvisInitial avisInitial;
	private Description description;
	private Procedure procedure;
	private List<InfosRectifItem> infosRectif;
}