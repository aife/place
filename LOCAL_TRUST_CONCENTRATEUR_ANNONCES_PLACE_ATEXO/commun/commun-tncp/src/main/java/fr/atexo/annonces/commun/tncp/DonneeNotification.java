package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder(builderMethodName = "with")
public record DonneeNotification(
        @JsonProperty("identifiers") Identifiers identifiers,
        @JsonProperty("publicationDetails") PublicationDetails publicationDetails,
        @JsonProperty("publicationStatus") PublicationStatus publicationStatus

) {
}
