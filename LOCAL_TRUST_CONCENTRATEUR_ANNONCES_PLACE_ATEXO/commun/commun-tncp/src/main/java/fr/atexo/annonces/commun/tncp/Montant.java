package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Montant{
	private Integer valeur;
	private String devise;
	private Integer tva;
}