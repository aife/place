package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ConformeDirective81{
	private Boolean oui;
	private Boolean non;
}