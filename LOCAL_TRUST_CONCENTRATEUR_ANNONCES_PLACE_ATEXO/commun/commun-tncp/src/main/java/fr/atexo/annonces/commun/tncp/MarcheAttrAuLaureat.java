package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class MarcheAttrAuLaureat{
	private Boolean oui;
	private Boolean non;
}