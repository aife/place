package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ValeurTotaleEstimee{
	private Integer valeur;
	private String devise;
}