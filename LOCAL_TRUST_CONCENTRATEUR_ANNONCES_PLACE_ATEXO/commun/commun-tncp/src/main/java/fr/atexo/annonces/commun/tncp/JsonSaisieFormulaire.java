package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class JsonSaisieFormulaire{
	private List<AnnItem> ann;
	private Emetteur emetteur;
	private Generateur generateur;
	private Annonceur annonceur;
}