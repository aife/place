package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class SousTraitance{
	private PartSousTraitee partSousTraitee;
	private ConformeDirective81 conformeDirective81;
	private PossibilitePartieSousTraitee possibilitePartieSousTraitee;
	private ChangementSousTraitant changementSousTraitant;
	private IdentificationPartieSousTraitee identificationPartieSousTraitee;
}