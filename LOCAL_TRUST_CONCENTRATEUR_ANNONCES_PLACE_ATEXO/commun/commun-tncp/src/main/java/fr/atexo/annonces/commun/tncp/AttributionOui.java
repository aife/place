package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AttributionOui{
	private String dateAttribution;
	private Titulaire titulaire;
	private AttribGroupement attribGroupement;
	private Montant montant;
	private Titulaires titulaires;
	private Integer nbOffresPME;
	private Offres offres;
	private SousTraitanceOui sousTraitanceOui;
	private Integer nbOffresEU;
	private Integer nbOffresRecues;
	private String sousTraitanceNon;
	private EstimationInitiale estimationInitiale;
	private Integer nbOffresElec;
	private Integer nbOffresNonEU;
	private String infoValeur;
	private Valeurs valeurs;
	private Valeur valeur;
	private Directive81 directive81;
	private OffresRecues offresRecues;
	private AttribueVariante attribueVariante;
	private Integer nbMarchesAttribues;
	private OriginePays originePays;
	private PrixAchatsOpportunite prixAchatsOpportunite;
	private ExclusionOffresBasses exclusionOffresBasses;
}