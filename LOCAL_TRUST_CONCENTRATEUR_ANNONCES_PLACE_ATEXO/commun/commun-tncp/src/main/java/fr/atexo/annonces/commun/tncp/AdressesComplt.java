package fr.atexo.annonces.commun.tncp;

import lombok.Data;

import java.util.List;

@Data
public class AdressesComplt{
	private Envoi envoi;
	private Document document;
	private RensComplementaires rensComplementaires;
	private AdmOuTech admOuTech;
	private AdmEtTech admEtTech;
	private AdresseParticipation adresseParticipation;
	private List<AdressePouvoirAdjudicateurItem> adressePouvoirAdjudicateur;
	private AdresseInfosSuppl adresseInfosSuppl;
}
