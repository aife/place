package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class MarcheReserve{
	private MarcheReserveOUI marcheReserveOUI;
	private String marcheReserveNON;
}