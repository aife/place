package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Emetteur{
	private String mel;
	private String login;
}