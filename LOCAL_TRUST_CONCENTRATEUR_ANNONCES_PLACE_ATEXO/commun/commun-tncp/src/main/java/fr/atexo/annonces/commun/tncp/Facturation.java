package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Facturation{
	private Libelle libelle;
	private String numBonCommande;
	private String noServiceExecutant;
	private UserChorus userChorus;
}