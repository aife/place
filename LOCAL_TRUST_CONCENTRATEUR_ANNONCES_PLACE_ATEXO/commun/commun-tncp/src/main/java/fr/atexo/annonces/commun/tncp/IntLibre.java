package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class IntLibre{
	private String intituleLibreLibelle;
	private String intituleLibre;
}