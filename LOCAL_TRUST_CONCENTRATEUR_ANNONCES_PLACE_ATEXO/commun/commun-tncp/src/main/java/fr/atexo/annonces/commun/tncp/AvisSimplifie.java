package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AvisSimplifie{
	private String datePublication;
	private String numAnnonce;
}