package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Fournitures{
	private String plusieursFormes;
	private String location;
	private String achat;
	private String creditBail;
	private String locationVente;
}