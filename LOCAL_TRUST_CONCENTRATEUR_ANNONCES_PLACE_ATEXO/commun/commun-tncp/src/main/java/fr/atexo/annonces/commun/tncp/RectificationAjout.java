package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class RectificationAjout{
	private List<InfosRectifItem> infosRectif;
	private String dansDossierAppelOffres;
	private String modifInfoOriginales;
	private String informationComplementaire;
	private DansAvisOriginal dansAvisOriginal;
	private String rectification;
	private String pubTEDNonConforme;
}