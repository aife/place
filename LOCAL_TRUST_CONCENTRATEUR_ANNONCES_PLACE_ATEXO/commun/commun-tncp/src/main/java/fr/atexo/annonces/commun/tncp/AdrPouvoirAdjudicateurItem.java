package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AdrPouvoirAdjudicateurItem{
	private String codeIdentificationNational;
	private String personneMorale;
	private Adr adr;
}