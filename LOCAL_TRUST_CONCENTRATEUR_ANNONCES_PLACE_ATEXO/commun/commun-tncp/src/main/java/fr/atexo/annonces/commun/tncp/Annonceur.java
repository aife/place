package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Annonceur {
    private String mail;
    private String mdp;
    private Integer id;
    private String login;
    private String motdepasse;
}
