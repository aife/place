package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class IntentionConclureFromFNSimple{
	private String autresRenseignement;
	private AvisInitial avisInitial;
	private String attribution;
	private String pasAvisInitial;
	private NatureMarche natureMarche;
}