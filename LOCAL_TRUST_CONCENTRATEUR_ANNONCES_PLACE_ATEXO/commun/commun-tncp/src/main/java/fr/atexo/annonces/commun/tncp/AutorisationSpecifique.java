package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AutorisationSpecifique{
	private String precisions;
}