package fr.atexo.annonces.commun.tncp;

import lombok.Data;

import java.util.List;

@Data
public class Oui{
	private DocPrix docPrix;
	private String grpAcheteur;
	private ReservationMarche reservationMarche;
	private String mesuresConfurlConfidentialite;
	private Integer nbCandidat;
	private String criteres;
	private String visiteDetail;
	private List<AdrPouvoirAdjudicateurItem> adrPouvoirAdjudicateur;
}
