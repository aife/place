package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ConditionsAvantageuses{
	private String liquidateurs;
	private String cessationActivite;
}