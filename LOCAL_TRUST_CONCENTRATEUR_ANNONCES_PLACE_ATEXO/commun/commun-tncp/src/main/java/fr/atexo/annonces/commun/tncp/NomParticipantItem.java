package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class NomParticipantItem{
	private PersonnePhysique personnePhysique;
	private String personneMorale;
}