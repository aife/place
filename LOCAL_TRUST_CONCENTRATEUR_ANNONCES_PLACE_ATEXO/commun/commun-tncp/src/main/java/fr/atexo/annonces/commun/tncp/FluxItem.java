package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class FluxItem{
	private String dateCreation;
	private Integer delaiAttente;
	private String dateEmission;
	private FluxBoamp fluxBoamp;
	private FluxShal fluxShal;
	private Id id;
	private Annonceur annonceur;
	private String statut;
	private String dateReception;
}