package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ProcConcNegocieeAcceleree{
	private String justification;
}