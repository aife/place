package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class TitulaireItem{
	private Adresse adresse;
	private Pme pme;
}