package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class AvisPeriodiqueIndicatif{
	private String datePublication;
	private String numAnnonce;
}