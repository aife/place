package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ActivitesSecteursSpeciaux{
	private String activitesPortuaires;
	private String electricite;
	private String servicesCheminFerUrbains;
	private String combustiblesSolides;
	private String eau;
	private String servicesPostaux;
	private String gazPetrole;
	private String servicesCheminFer;
	private String activitesAeroportuaires;
	private String autre;
	private String gazChaleur;
}