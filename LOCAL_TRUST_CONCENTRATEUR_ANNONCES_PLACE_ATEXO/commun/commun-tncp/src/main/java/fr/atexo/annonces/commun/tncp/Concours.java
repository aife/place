package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Concours{
	private String directive25;
	private String directive24;
}