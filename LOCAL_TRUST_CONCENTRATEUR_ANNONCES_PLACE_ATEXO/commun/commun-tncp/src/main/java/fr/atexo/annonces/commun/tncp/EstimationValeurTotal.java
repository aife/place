package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class EstimationValeurTotal{
	private Integer valeur;
	private String devise;
}