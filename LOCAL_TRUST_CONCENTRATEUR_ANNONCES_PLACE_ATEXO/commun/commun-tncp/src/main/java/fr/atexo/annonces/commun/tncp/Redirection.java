package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.net.URI;

@Builder(builderMethodName = "with")
public record Redirection(@JsonProperty("redirection-url") URI uri) {
}
