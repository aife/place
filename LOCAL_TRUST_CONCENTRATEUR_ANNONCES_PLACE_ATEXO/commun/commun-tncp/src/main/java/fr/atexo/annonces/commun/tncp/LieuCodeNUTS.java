package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class LieuCodeNUTS{
	private List<String> codeNUTS;
}