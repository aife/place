package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Form12{
	private ProcedureConjointe procedureConjointe;
	private String infoCompl;
	private DescriptionMarche descriptionMarche;
	private AvisConcerne avisConcerne;
	private RenseignementsCompl renseignementsCompl;
	private String criteresEvaluation;
	private RenseignementsAdm renseignementsAdm;
	private Procedure procedure;
	private Communication communication;
	private Situation situation;
}