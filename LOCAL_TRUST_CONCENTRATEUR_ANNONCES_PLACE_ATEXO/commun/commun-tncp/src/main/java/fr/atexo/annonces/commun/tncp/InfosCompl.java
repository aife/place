package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class InfosCompl{
	private AdresseInfosCompl adresseInfosCompl;
	private String adresseContact;
}