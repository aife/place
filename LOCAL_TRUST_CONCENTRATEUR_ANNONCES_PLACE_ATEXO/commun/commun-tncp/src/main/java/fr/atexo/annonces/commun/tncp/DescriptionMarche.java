package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class DescriptionMarche{
	private LotTncp lot;
	private MotsClef motsClef;
	private Cpv cpv;
	private String titreMarche;
	private NatureMarche natureMarche;
	private String numeroReference;
	private Lots lots;
	private Valeur valeur;
	private String description;
	private String divisibleLotNon;
	private String divisibleLotOui;
	private EstimationValeurTotal estimationValeurTotal;
	private OffreBasse offreBasse;
	private ValeurTotal valeurTotal;
	private OffreHaute offreHaute;
}
