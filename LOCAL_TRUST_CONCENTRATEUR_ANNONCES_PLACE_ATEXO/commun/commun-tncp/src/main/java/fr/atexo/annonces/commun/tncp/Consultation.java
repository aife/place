package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.atexo.annonces.commun.wrapper.JsonWrapped;
import lombok.Builder;
import lombok.Singular;

import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Builder(builderMethodName = "with")
public record Consultation(@JsonProperty("categoriePrincipale") Nature nature,
                           @JsonProperty("codeCpvPrincipal") Cpv cpvPrincipal,
                           @JsonProperty("codesCpvSecondaires") @Singular Set<Cpv> cpvSecondaires,
                           @JsonProperty("contactAcheteur") Contact acheteur,
                           @JsonProperty("criteresAttribution") CriteresAttribution criteresAttribution,
                           @JsonProperty("dateDebutContratEstimee") LocalDate dateDebutContratEstimee,
                           @JsonProperty("dateFinContratEstimee") LocalDate dateFinContratEstimee,
                           @JsonProperty("dateLimiteRemiseCandidature") LocalDate dateLimiteRemiseCandidature,
                           @JsonProperty("dateLimiteRemiseOffre") String dateLimiteRemiseOffre,
                           @JsonProperty("delaiValiditeOffreAcheteur") Integer delaiValiditeOffreAcheteur,
                           @JsonProperty("dureeContratEstimee") LocalDate dureeContratEstimee,
                           @JsonProperty("lieuExecution") LieuExecution lieuExecution,
                           @JsonProperty("lots") Set<LotItem> lots, @JsonProperty("marcheReserve") String marcheReserve,
                           @JsonProperty("objet") String objet, @JsonProperty("offreTousLots") Boolean offreTousLots,
                           @JsonWrapped("phaseConsultation") @JsonProperty("nombreMaxCandidat") Long nombreMaxCandidat,
                           @JsonWrapped("phaseConsultation") @JsonProperty("reductionSuccessiveCandidats") Boolean reductionSuccessiveCandidats,
                           @JsonProperty("tranche") Boolean tranche,
                           @JsonProperty("typeProcedure") TncpTypeProcedureEnum typeProcedure,
                           @JsonProperty("typeTechniqueAchat") TncpTypeTechniqueAchatEnum typeTechniqueAchat,
                           @JsonProperty("urlConsultation") URI urlConsultation,
                           @JsonProperty("urlProfilAcheteur") URI urlProfilAcheteur,
                           @JsonProperty("usageNegociation") TncpUsageNegociationEnum usageNegociation,
                           @JsonProperty("variantesRequises") TncpVarianteRequiseEnum variantesRequises) {
}
