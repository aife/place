package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Form18{
	private AutresInformations autresInformations;
	private AvisInitial avisInitial;
	private InfosSup infosSup;
	private Joce joce;
	private String joceNon;
	private Attribution attribution;
	private Description description;
	private String pasAvisInitial;
	private Procedure procedure;
	private Criteres criteres;
	private ProcedureRecours procedureRecours;
	private NatureMarche natureMarche;
}