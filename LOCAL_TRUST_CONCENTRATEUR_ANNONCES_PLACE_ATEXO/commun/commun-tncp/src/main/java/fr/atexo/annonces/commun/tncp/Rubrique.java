package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Rubrique{
	private String lot;
	private String apresLaMention;
	private String section;
}