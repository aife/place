package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class QualificationsProfessionnelles{
	private String precisions;
}