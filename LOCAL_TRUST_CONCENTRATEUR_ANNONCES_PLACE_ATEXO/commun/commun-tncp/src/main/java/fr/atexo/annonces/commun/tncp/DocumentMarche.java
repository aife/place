package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class DocumentMarche{
	private String accesComplet;
	private String urlDocument;
	private String accesRestreint;
}