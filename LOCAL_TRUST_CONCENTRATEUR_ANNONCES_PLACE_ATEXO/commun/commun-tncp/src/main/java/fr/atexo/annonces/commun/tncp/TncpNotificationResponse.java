package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.util.List;

@Builder(builderMethodName = "with")
public record TncpNotificationResponse(@JsonProperty("response") Response response
        , @JsonProperty("status") int status

) {
}
