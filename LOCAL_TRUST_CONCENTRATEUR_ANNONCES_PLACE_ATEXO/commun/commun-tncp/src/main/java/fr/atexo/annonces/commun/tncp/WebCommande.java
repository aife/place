package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class WebCommande{
	private Boolean oui;
	private Boolean non;
}