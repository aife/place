package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class OperateursEconomiques{
	private MarcheReserve marcheReserve;
	private String situationPropre;
	private String capaEcoFiMin;
	private String capaEcoFi;
	private String capaTech;
	private String capaTechMin;
}