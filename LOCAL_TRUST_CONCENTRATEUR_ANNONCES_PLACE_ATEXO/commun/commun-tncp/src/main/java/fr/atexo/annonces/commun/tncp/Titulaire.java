package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Titulaire{
	private Coord coord;
	private PersonnePhysique personnePhysique;
	private String personneMorale;
	private Adr adr;
	private Adresse adresse;
	private Pme pme;
	private String codeIdentificationNational;
}