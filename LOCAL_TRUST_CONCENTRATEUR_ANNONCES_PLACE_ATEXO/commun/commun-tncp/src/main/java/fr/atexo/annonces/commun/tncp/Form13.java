package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Form13{
	private ProcedureConjointe procedureConjointe;
	private AvisInitial avisInitial;
	private String infoCompl;
	private DescriptionMarche descriptionMarche;
	private AvisConcerne avisConcerne;
	private RenseignementsCompl renseignementsCompl;
	private Attribution attribution;
	private CriteresEvaluation criteresEvaluation;
	private String pasAvisInitial;
	private RenseignementsAdm renseignementsAdm;
	private Procedure procedure;
}