package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class MarcheReserveOUI{
	private String emploisProteges;
	private String ateliersProteges;
	private String servicePublicReserves;
}