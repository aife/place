package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class Criteres{
	private EnchereElectronique enchereElectronique;
	private String critereCDC;
	private List<String> critereLibre;
	private List<CriterePondereItem> criterePondere;
	private CriterePrix criterePrix;
	private List<CritereQualiteItem> critereQualite;
	private List<CritereCoutItem> critereCout;
	private String critereDoc;
	private List<String> critere;
	private String publication;
}
