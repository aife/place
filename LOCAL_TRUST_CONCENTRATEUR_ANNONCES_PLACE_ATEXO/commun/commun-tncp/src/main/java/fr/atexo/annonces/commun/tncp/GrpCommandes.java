package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class GrpCommandes{
	private Oui oui;
	private Non non;
}