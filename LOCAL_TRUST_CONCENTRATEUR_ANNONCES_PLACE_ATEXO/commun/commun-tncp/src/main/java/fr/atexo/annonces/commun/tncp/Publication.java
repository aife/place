package fr.atexo.annonces.commun.tncp;

import lombok.Data;

import java.util.List;

@Data
public class Publication {
	private String dateCreation;
	private String dateModification;
	private String datePublication;
	private Integer nbLots;
	private String mdpBoampweb;
	private JsonSaisieFormulaire jsonSaisieFormulaire;
	private Integer avisId;
	private String refAvisLie;
	private String adrContactJAL;
	private String telecopieur;
	private String idBoampweb;
	private String reference;
	private Critere critere;
	private String mailBoampweb;
	private String dateRejet;
	private String refConsultation;
	private Formulaire formulaire;
	private String adresseFacturation;
	private List<FluxItem> flux;
	private String codeCpvFormulaire;
	private Annonceur annonceur;
	private Utilisateur user;
	private String statut;
	private String url;
}
