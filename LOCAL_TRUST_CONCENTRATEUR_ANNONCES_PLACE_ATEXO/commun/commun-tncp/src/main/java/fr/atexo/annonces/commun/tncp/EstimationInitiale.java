package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class EstimationInitiale{
	private Integer valeur;
	private String devise;
	private Integer tva;
}