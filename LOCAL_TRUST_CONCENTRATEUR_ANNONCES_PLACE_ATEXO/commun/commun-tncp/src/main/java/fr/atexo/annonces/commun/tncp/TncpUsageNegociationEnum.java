package fr.atexo.annonces.commun.tncp;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@AllArgsConstructor
@Getter
public enum TncpUsageNegociationEnum {
    NON_UTILISE("non utilisé"),
    USAGE_POSSIBLE("usage possible"),
    USAGE_CERTAIN("usage certain");

    private final String value;


    @JsonCreator
    public static TncpUsageNegociationEnum fromValue(String value) {
        for (TncpUsageNegociationEnum operator : values()) {
            if (operator.getValue().equals(value)) {
                return operator;
            }
        }
        return null;
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
