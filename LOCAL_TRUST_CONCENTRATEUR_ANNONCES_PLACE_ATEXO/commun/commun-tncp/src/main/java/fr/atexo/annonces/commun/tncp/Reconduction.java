package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class Reconduction{
	private Integer nbReconductions;
	private CalendrierMarchesulterieurs calendrierMarchesulterieurs;
	private NbReconductionsFourchette nbReconductionsFourchette;
}