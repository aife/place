package fr.atexo.annonces.commun.tncp;

import java.util.List;
import lombok.Data;

@Data
public class Form08{
	private ProcedureConjointe procedureConjointe;
	private String infoCompl;
	private List<DescriptionMarcheItem> descriptionMarche;
	private AvisConcerne avisConcerne;
	private Communication communication;
}