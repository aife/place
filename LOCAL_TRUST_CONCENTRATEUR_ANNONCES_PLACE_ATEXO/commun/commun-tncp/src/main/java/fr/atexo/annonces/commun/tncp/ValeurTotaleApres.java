package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ValeurTotaleApres{
	private Integer valeur;
	private String devise;
}