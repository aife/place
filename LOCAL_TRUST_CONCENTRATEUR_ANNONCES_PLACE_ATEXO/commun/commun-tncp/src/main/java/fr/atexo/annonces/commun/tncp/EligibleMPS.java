package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class EligibleMPS{
	private Boolean oui;
	private Boolean non;
}