package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class EnchereElectronique{
	private String eeNon;
	private EeOui eeOui;
}