package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class ConditionsObtentionSpec{
	private String conditionsModePaiement;
	private DocPayants docPayants;
	private String dateLimiteObtention;
}