package fr.atexo.annonces.commun.tncp;

import lombok.Data;

@Data
public class MiseENCONCURRENCE{
	private String directive1046;
	private String directive24;
}