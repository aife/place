package com.atexo.annonces.commun.mpe;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true, builderMethodName = "with")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrganismeMpe {
    private Long id;
    private String sigle;
    @JsonProperty("denomination")
    @JsonAlias("denominationOrganisme")
    private String denomination;
    private String siren;
    private String complement;
    private String siret;
    private String description;
    private String acronyme;
    private String adresse;
    private String adresse2;
    private String codePostal;
    private String ville;
    private String idExterne;
    private String formeJuridique;
    private String url;
}
