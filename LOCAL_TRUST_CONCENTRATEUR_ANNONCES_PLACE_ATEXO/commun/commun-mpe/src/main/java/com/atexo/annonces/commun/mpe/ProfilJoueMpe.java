package com.atexo.annonces.commun.mpe;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true, builderMethodName = "with")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProfilJoueMpe {
    private String nomOfficiel;
    private String pays;
    private String ville;
    private String adresse;
    private String codePostal;
    private String pointContact;
    private String aAttentionDe;
    private String telephone;
    private String fax;
    private String email;
    private String adressePouvoirAdjudicateur;
    private String adresseProfilAcheteur;
    private boolean autoriteNationale;
    private boolean officeNationale;
    private boolean collectiviteTerritoriale;
    private boolean officeRegionale;
    private boolean organismePublic;
    private boolean organisationEuropenne;
    private boolean autreTypePouvoirAdjudicateur;

    private boolean servicesGeneraux;
    private boolean defense;
    private boolean securitePublic;
    private boolean environnement;
    private boolean developpementCollectif;
    private boolean affairesEconomiques;
    private boolean sante;
    private boolean protectionSociale;
    private boolean loisirs;
    private boolean eduction;
    private boolean autreActivitesPrincipales;
    private boolean pouvoirAdjudicateurAgit;
    private boolean pouvoirAdjudicateurMarcheCouvert;
    private boolean entiteAdjudicatriceMarcheCouvert;
    private String autreLibelleTypePouvoirAdjudicateur;
    private String autreLibelleActivitesPrincipales;
    private String numeroNationalIdentification;

}
