package com.atexo.annonces.commun.mpe;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true, builderMethodName = "with")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Api {
    private String url;
    private String token;
    private String refreshToken;
}
