package com.atexo.annonces.commun.mpe;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderMethodName = "with")
public class AgentMpe {

    private Long id;
    private String login;
    private String email;
    private String nom;
    private String prenom;
    private String telephone;
    private String fax;
    private String service;
    private String organisme;


}
