package com.atexo.annonces.commun.mpe;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class ReferentielMpe {
    private String libelle;
    private String abreviation;
    private String idExterne;
}

