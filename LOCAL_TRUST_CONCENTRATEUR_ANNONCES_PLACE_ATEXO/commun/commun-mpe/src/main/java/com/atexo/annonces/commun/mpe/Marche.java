package com.atexo.annonces.commun.mpe;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true, builderMethodName = "with")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Marche {
    private ConsultationMpe consultation;
    private DonneesComplementaires donneesComplementaires;
    private EnvoiAgent agent;
    private Set<LotMpe> lots;
    private Set<AcheteurMpe> acheteurs;
    private AcheteurMpe acheteur;
    private OrganismeMpe organisme;
    private ServiceMpe service;

}
