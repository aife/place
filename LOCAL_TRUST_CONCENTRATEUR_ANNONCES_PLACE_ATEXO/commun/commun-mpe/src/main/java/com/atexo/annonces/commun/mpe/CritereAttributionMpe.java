package com.atexo.annonces.commun.mpe;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true, builderMethodName = "with")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CritereAttributionMpe {
    private Integer id;
    private Integer ordre;
    private List<SousCritereAttributionMpe> sousCriteres;
    private String donneeComplementaire;
    private Integer idDonneeComplementaire;
    private String ponderation;
    private String lot;
}
