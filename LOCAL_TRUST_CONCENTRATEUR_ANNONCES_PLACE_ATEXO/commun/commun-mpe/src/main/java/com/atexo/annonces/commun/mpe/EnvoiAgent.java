package com.atexo.annonces.commun.mpe;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true, builderMethodName = "with")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EnvoiAgent {
    private Long id;
    @JsonProperty("login")
    @JsonAlias("identifiant")
    private String login;
    private String nom;
    private String prenom;
    private String email;
    private String telephone;
    private String fax;
    private String plateforme;
    private OrganismeMpe organisme;
    private ServiceMpe service;
    private String acronymeOrganisme;
    private String dateCreation;
    private String dateModification;
    private Long idExterne;
    private Boolean actif;
    private String sigleUrl;
    private String nomCourantAcheteurPublic;
    private Api api;
}
