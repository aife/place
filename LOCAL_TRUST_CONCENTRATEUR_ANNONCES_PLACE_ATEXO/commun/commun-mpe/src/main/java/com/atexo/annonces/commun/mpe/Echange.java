package com.atexo.annonces.commun.mpe;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true, builderMethodName = "with")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Echange {
    private EnvoiAgent agent;
    private OrganismeMpe organisme;
    private ServiceMpe service;
    private ConsultationMpe consultation;
}
