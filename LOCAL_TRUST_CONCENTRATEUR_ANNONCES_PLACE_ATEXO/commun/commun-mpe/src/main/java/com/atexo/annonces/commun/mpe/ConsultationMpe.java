package com.atexo.annonces.commun.mpe;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true, builderMethodName = "with")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConsultationMpe {
    private String dateLimiteRemisePlis;
    private String dateValidation;
    private Boolean etatValidation;
    private Boolean enveloppeAnonymat;
    private Boolean dume;
    private Boolean dumeSimplifie;
    private List<ClauseN1> clauses;
    private String statutCalcule;
    private Boolean entiteAdjudicatrice;
    private String urlConsultation;
    private Boolean alloti;
    private String organisme;
    private String poursuivreAffichageUnite;
    private String naturePrestation;
    private String reference;
    private String reponseElectronique;
    private String typeContrat;
    private String donneeComplementaire;
    private Boolean enveloppeOffreTechnique;
    private Integer id;
    private String dateModification;
    private ZonedDateTime dateLimiteRemiseOffres;
    private Boolean enveloppeCandidature;
    private ZonedDateTime datefin;
    private ZonedDateTime datefinAffichage;
    private ZonedDateTime dateMiseEnLigneCalcule;
    private Integer idCreateur;
    private Boolean chiffrement;
    private Boolean enveloppeOffre;
    private Boolean organismeDecentralise;
    private String objet;
    private String intitule;
    private Boolean bourseCotraitance;
    private List<String> lieuxExecution;
    private List<String> codesNuts;
    private String typeProcedure;
    private String typeProcedureDume;
    private Integer poursuivreAffichage;
    private String signatureElectronique;
    private String directionService;
    private String commentaireInterne;
    private String codeCpvPrincipal;
    private String codeCpvSecondaire1;
    private String codeCpvSecondaire2;
    private String codeCpvSecondaire3;
    private String modeOuvertureReponse;
    private String signatureActeEngagement;
    private String marchePublicSimplifie;
    private String codeProcedure;
    private String urlConsultationExterne;
    private String typeDecisionDeclarationSansSuite;
    private String typeDecisionDeclarationInfructueux;
    private String typeDecisionARenseigner;
    private String typeDecisionAutre;
    private String typeDecisionAttributionMarche;
    private String typeDecisionAdmissionSad;
    private String typeDecisionAttributionAccordCadre;
    private Boolean donneeComplementaireObligatoire;
    private Boolean donneePubliciteObligatoire;
    private Boolean consultationRestreinte;
    private Integer nombreLots;
    private Map<String, String> departmentsWithNames;
    private Long valeurEstimee;
    private String numeroSad;
    private String justificationNonAlloti;
    private String dateDecision;
    private String autreTechniqueAchat;
    private String referentielAchat;
    private String adresseRetraisDossiers;
    private String adresseDepotOffres;
    private String lieuOuverturePlis;
    private Boolean groupement;
    private Boolean procedureOuverte;
    private String typeDecision;
    private String modaliteRetraitDossier;
    private String receptionOffresCandidatures;
    private String lieuOuvertureOffresRemiseCandidatures;

}
