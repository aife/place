package com.atexo.annonces.commun.mpe;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.time.ZonedDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true, builderMethodName = "with")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DonneesComplementaires {
    private Object adresseDepotOffres;
    private Object modalitesReconduction;
    private Object cautionProvisoire;
    private Object idTrAdresseDepotOffres;
    private String motsCles;
    private Object piecesDossierAdditif;
    private Integer typePrestation;
    private Object idFormePrix;
    private String publicationMontantEstimation;
    private Object idChoixMoisJour;
    private Object decompositionLotsTechniques;
    private Boolean phaseSuccessive;
    private Integer idDureeDelaiDescription;
    private Object idTrancheTypePrix;
    private Object nombreReconductions;
    private Object catalogueElectronique;
    private Object initializer;
    private Boolean avisAttributionPresent;
    private ZonedDateTime dureeDateDebut;
    private Object visiteDescription;
    private Integer idGroupementAttributaire;
    private Object dateLimiteEchantillon;
    private Object estimationDateValeurPreinscription;
    private Object idTrAdresseRetraisDossiers;
    private Object idTrPiecesDossierAdmin;
    private List<String> criteresAttribution;
    private Object idCcagReference;
    private Object lieuOuverturePlis;
    private Object estimationBcMaxAttPressenti;
    private Object idNbCandidatsAdmis;
    private Object idTrLieuOuverturePlis;
    private Object prixAquisitionPlans;
    private Object idTrAddReunion;
    private Object economiqueFinanciere;
    private Long montantMarche;
    private String projetFinanceFondsUnionEuropeenne;
    private String identificationProjet;
    private Object cloner;
    private Object dureeDescription;
    private Object visiteObligatoire;
    private Object estimationBcMaxPreinscription;
    private Object dureeMarche;
    private Integer idCritereAttribution;
    private Object estimationBcMinAttPressenti;
    private Object idTrPiecesDossierTech;
    private String varianteCalcule;
    private Boolean variantesAutorisees;
    private Long nombreCandidatsMax;
    private Integer idDonneeComplementaire;
    private Object estimationPfAttPressenti;
    private Boolean variantesTechniquesObligatoires;
    private String visitesLieux;
    private Integer delaiValiditeOffres;
    private Long nombreCandidatsFixe;
    private Object addEchantillon;
    private Object estimationPfTabOuvOffre;
    private Object estimationPfPreinscription;
    private Object piecesDossierTech;
    private Boolean reconductible;
    private Object dateReunion;
    private String echantillon;
    private String delaiPartiel;
    private Object nombreCandidatsMin;
    private Object estimationBcMinPreinscription;
    private Object variantesTechniquesDescription;
    private Object addReunion;
    private Object idTrPiecesDossierAdditif;
    private Object valeurMontantEstimationPubliee;
    private Boolean variantePrestationsSuppEventuelles;
    private Object techniquesProfessionels;
    private Object attributionSansNegociation;
    private String modalitesFinancementRegimeFinancier;
    private Object activiteProfessionel;
    private Object idCcagDpi;
    private Boolean isInitialized;
    private String justificationNonAlloti;
    private String cautionnementRegimeFinancier;
    private ZonedDateTime dureeDateFin;
    private Object variantes;
    private Object idTrAddEchantillon;
    private Object lieuExecution;
    private Object idUnite;
    private String reunion;
    private Object estimationDqeTabOuvOffre;
    private Object piecesDossierAdmin;
    private Boolean procedureAccordMarchesPublicsOmc;
    private Object adresseRetraisDossiers;
    private String criteresIdentiques;
    private String informationComplementaire;
    private Boolean varianteExigee;
    private Boolean avecTranche;

    private String formuleSignature;
    private String autrePouvoirAdjudicateur;
    private String autresInformations;
    private String conditionParticipation;
    private String modaliteVisiteLieuxReunion;

}
