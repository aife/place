package com.atexo.annonces.commun.mpe;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClauseN2 {

    private String label;
    private String slug;
    private String iri;
    private List<Clause> clausesN3;
}
