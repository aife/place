package com.atexo.annonces.commun.mpe;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class ContratMpe extends ReferentielMpe {
    private String typeTechniqueAchat;
    private String marcheSubsequent;
    private String associationProcedures;
}

