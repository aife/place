package com.atexo.annonces.commun.mpe;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true, builderMethodName = "with")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LotMpe {
    private String numero;
    private String codeCpvPrincipal;
    private String codeCpvSecondaire2;
    private List<ClauseN1> clauses;
    private String codeCpvSecondaire1;
    private String codeCpvSecondaire3;
    private String descriptionSuccinte;
    private Integer id;
    @JsonProperty("@id")
    private String hydraId;
    private String consultation;
    private String intitule;
    private String naturePrestation;
    private String typeDecisionDeclarationSansSuite;
    private String typeDecisionDeclarationInfructueux;
    private String typeDecisionARenseigner;
    private String typeDecisionAutre;
    private String typeDecisionAttributionMarche;
    private String typeDecisionAdmissionSad;
    private String typeDecisionAttributionAccordCadre;
    private String typeDecision;
    private String decision;
    private DonneesComplementaires donneesComplementaires;
}
