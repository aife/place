package com.atexo.annonces.commun.mpe;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClauseN1 {

    private String label;
    private String slug;
    private String iri;
    private List<ClauseN2> clausesN2;
}
