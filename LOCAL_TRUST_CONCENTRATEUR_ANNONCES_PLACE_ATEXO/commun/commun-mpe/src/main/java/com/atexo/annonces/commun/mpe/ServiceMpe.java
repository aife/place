package com.atexo.annonces.commun.mpe;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true, builderMethodName = "with")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ServiceMpe {
    private Long id;
    private String codePostal;
    private String url;


    private String acronymeOrg;
    private String typeService;
    private String libelle;
    private String sigle;
    private String adresse;
    private String adresseSuite;
    private String cp;
    private String ville;
    private String telephone;
    private String fax;
    private String mail;
    private String pays;
    private String idExterne;
    private String siren;
    private String siret;
    private String complement;
    private String libelleAr;
    private String adresseAr;
    private String adresseSuiteAr;
    private String villeAr;
    private String paysAr;
    private String libelleFr;
    private String adresseFr;
    private String adresseSuiteFr;
    private String villeFr;
    private String paysFr;
    private String libelleEs;
    private String adresseEs;
    private String adresseSuiteEs;
    private String villeEs;
    private String paysEs;
    private String libelleEn;
    private String adresseEn;
    private String adresseSuiteEn;
    private String villeEn;
    private String paysEn;
    private String libelleSu;
    private String adresseSu;
    private String adresseSuiteSu;
    private String villeSu;
    private String paysSu;
    private String libelleDu;
    private String adresseDu;
    private String adresseSuiteDu;
    private String villeDu;
    private String paysDu;
    private String libelleCz;
    private String adresseCz;
    private String adresseSuiteCz;
    private String villeCz;
    private String paysCz;
    private String libelleIt;
    private String adresseIt;
    private String adresseSuiteIt;
    private String villeIt;
    private String paysIt;
    private String cheminComplet;
    private String cheminCompletFr;
    private String cheminCompletEn;
    private String cheminCompletEs;
    private String cheminCompletSu;
    private String cheminCompletDu;
    private String cheminCompletCz;
    private String cheminCompletAr;
    private String cheminCompletIt;
    private String nomServiceArchiveur;
    private String identifiantServiceArchiveur;
    private String affichageService;
    private String activationFuseauHoraire;
    private String decalageHoraire;
    private String lieuResidence;
    private String alerte;
    private String accesChorus;
    private String idParent;
    private List<String> consultations;
    private String organisme;
    private String formeJuridique;
    private String formeJuridiqueCode;
    private List<String> agents;
    private String synchronisationExec;
}
