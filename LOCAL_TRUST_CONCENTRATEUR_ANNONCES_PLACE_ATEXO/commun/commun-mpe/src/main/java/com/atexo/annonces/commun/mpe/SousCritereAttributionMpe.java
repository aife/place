package com.atexo.annonces.commun.mpe;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true, builderMethodName = "with")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SousCritereAttributionMpe {
    private Integer idSousCritereAttribution;
    private String critereAttribution;
    private String enonce;
    private String ponderation;
    private Integer idCritereAttribution;
}
