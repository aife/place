package com.atexo.annonces.commun.mpe;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Clause {

    private String label;
    private String slug;
    private List<Clause> clausesN2;
}
