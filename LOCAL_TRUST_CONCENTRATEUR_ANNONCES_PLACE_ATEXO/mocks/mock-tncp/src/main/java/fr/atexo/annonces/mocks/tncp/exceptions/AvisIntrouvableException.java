package fr.atexo.annonces.mocks.tncp.exceptions;

import lombok.Getter;
import org.slf4j.helpers.MessageFormatter;

import java.text.MessageFormat;

@Getter
public final class AvisIntrouvableException extends RuntimeException {

	private final Integer id;

	public AvisIntrouvableException(Integer id) {
		super(MessageFormat.format("Avis {0} introuvable", id));

		this.id = id;
	}
}
