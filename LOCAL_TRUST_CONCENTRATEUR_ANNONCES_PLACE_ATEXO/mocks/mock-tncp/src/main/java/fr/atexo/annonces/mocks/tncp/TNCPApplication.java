package fr.atexo.annonces.mocks.tncp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TNCPApplication {
    public static void main(String[] args) {
        SpringApplication.run(TNCPApplication.class, args);
    }
}
