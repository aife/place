package fr.atexo.annonces.mocks.tncp.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.atexo.annonces.commun.tncp.Token;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/oauth/token")
@Slf4j
@RequiredArgsConstructor
public class AuthController {

    @Value("classpath:tncp/token.json")
    private Resource token;


    @NonNull
    private ObjectMapper objectMapper;

    @PostMapping(consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    @SneakyThrows
    public Token login(@RequestParam Map<String, String> body) {
        log.info("authentification de {}", body.get("client_id"));
        return objectMapper.readValue(token.getFile(), Token.class);
    }

}
