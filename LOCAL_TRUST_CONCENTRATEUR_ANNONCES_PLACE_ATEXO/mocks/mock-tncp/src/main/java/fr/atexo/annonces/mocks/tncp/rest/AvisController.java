package fr.atexo.annonces.mocks.tncp.rest;

import fr.atexo.annonces.commun.tncp.Avis;
import fr.atexo.annonces.commun.tncp.Redirection;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/avis")
@RequiredArgsConstructor
public class AvisController {
	private final Map<UUID, String> publications = new HashMap<>();

	@Value("${server.port}")
	private int port;

	@Value("${server.servlet.context-path:}")
	private String path;

	@PostMapping("/redirections-contexts")
	public Redirection create(@RequestBody Avis avis) throws URISyntaxException {
		log.info("Création de l'avis {}", avis);

		var uuid = UUID.randomUUID();

		publications.put(uuid, "ENVOYE");

		return Redirection.with().uri(new URI(MessageFormat.format("http://localhost:{0,number,#}{1}/index.html?id={2}", port, path, uuid))).build();
	}

	@PatchMapping("/{uuid}")
	public void validate(@PathVariable("uuid") UUID uuid) {
		log.info("Validation de l'avis {}", uuid);

		this.publications.put(uuid, "PUBLIE");
	}

	@GetMapping({"/{uuid}", "/check/{uuid}"})
	public String get(@PathVariable("uuid") UUID uuid) {
		log.info("Vérification des avis {}", uuid);

		return this.publications.get(uuid);
	}
}
