module.exports = {
    publicPath: '/',
    chainWebpack: config => {
        config.module
            .rule('images')
            .use('url-loader')
            .loader('url-loader')
            .tap(options => Object.assign(options, {limit: 10}))
            .end()

    },
    devServer: {
        proxy:
            {
                '^/web-component-eforms/': {
                    target: 'http://10.77.0.35/',
                    changeOrigin: true
                },
                '^/concentrateur-annonces/': {
                    target: 'http://localhost:8080',
                    ws: true,
                    changeOrigin: true
                },
                '^/rest/': {
                    target: 'http://localhost:8080/concentrateur-annonces',
                    ws: true,
                    changeOrigin: true
                }
            },
        port: 8081

    }
}
