# Publicite

Ce module sera intégré à l'étape publicité de la création de la consultation dans MPE. 
C'est un composant web (web component) réutilisable. 
Ce projet vuejs a été créé via l'interface vue ui et utilise yarn pour la gestion des dépendances.


## liens jira

## webcomponent

```html
<consultation-publicite
    plateforme="mpe_place"
    id-consultation="2073"
    xml-annonces="PHJvb3Q+Cgo8aW5mb0Fubm9uY2VzPm9rIHRlc3Q8L2luZm9Bbm5vbmNlcz4KCjwvcm9vdD4K"
    access-token=""
    url-api="/concentrateur-annonces"
    endpoint-annonces="/rest/annonces"
    endpoint-supports="/rest/v2/supports"
    endpoint-token="/rest/v2/oauth/token"
    basic-token="YW5ub25jZXMtY2xpZW50OkNvbnZlcmdlbmNlIQ=="
    primary="red"
    secondary="grey"
    id-consultation="2073"
  ></consultation-publicite>

```

|   Attributs	|   Type	|   Obligatoire	| Description |  	
|---	|---	|---	|---    |
|   plateforme	|   String	|   oui	| Identifiant de la plateforme|  
|   id-consultation	|   String	|   oui	| Identifiant de la consultation|  
|   access-token	|   String	|   oui	|  Token d'acces au webservice (précalculé) |
|   xml_annonces	|   String 	|   oui	|  Xml en base 64 |
|   basic-token	|   String	|   non	|  Token de récupération du access_token  |
|   primary	|   String	|   oui	|  Couleur primaire (bouton info) |
|   secondary	|   String	|   oui	|  Couleur secondaire (bouton info) |
|   url-api	|   String	|   non	|  Segment d'url (doit être paramètré avec un proxypass |
|   endpoint-annonces	|   String	|   non	|  End point ws annonce |
|   endpoint-supports	|   String	|   non	|  End point ws supports |
|   endpoint-token	|   String	|   non	|  End point ws basic auth |


## apache config

```bash

ProxyPass               /concentrateur-annonces/          http://172.16.60.210:8080/concentrateur-annonces/ retry=0
ProxyPassReverse        /concentrateur-annonces/          http://172.16.60.210:8080/concentrateur-annonces/

ProxyPass               /concentrateur/          http://172.16.60.210/ retry=0
ProxyPassReverse        /concentrateur/          http://172.16.60.210/
```

## Installation yarn vue-cli


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```
### Configuration
 La configuration de ce projet peut se faire via l'interface graphique. 
 Pour se faire lancez la commande suivante :
 
```bash
vue ui
```

### Generate le web component

 
```bash
# générer un web component (shadow dom + sutom element) => non compatible IE 11
vue-cli-service build --target wc --name pub-consultation 

# générer un composant vue
vue-cli-service build --target lib --name consultationPublicite 'src/components/PubliciteConsultation.vue'

```




### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
