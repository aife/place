import publicite1 from '../components/Publicite.vue';
import publicite2 from '../components/Publicite2.vue';
import publicite3 from '../components/Publicite3.vue';

export default [
  // Redirects to /route-one as the default route.
  {
    path: '/'
  },
  {
    path: '/publicite/publicite1/:id',
    component: publicite1
  },
  {
    path: '/publicite/publicite2/:id',
    component: publicite2
  }
  ,
  {
    path: '/publicite/publicite3/:id',
    component: publicite3
  }
];