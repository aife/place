class Download {
    async pdf(idConsultation, xmlAnnonce, webservice, referenceUtilisateur = '', simplifie = false, target,annonce) {
        let response = await webservice.postPdf(idConsultation, xmlAnnonce, simplifie,annonce.support,annonce.offre);
        if (response !== null) {
            this.downloadPdf(response.data, referenceUtilisateur, target);
        }


        return 'OK'
    }

    async pdfAnnonce(webservice, pdfAnnonce, xmlAnnonce, target) {
        let response = await webservice.postAnnoncePdf(pdfAnnonce, xmlAnnonce);
        if (response !== null) {
            this.downloadPdf(response.data, pdfAnnonce.referenceUtilisateur, target);
        }


        return 'OK'
    }

    downloadPdf(data, referenceUtilisateur = '', target) {
        const filename = 'avis_publicite_' + referenceUtilisateur + '.pdf';
        var fileURL = window.URL.createObjectURL(new Blob([data]));
        var fileLink = document.createElement('a');
        fileLink.href = fileURL;
        fileLink.setAttribute('download', filename);
        var el = document.getElementById(target)
        if (!el)
            el = document.createElement("div");
        el.appendChild(fileLink);
        fileLink.click();
    }
}

module.exports = Download;
