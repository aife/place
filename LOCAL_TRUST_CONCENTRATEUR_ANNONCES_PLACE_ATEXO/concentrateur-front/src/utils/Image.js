/**
 * Object de recupération d'image
 */
class Image {

    get(name){
        try {
            return require('../components/assets/support-' + name + '.gif');
        } catch (e) {
            return false;
        }
    }

}

module.exports = Image;
