import {Base64} from 'js-base64';

/**
 * Object de traduction
 */
class translate {

    constructor(arr) {
        this.arr = JSON.parse(arr);
        this.defaultArr = {
            "TITRE_BLOC_BOAMPJOUE": "Ma sélection de support(s) pour l'avis personnalisé BOAMP/JOUE (formulaire à saisir)",
            "TITRE_BLOC_NATIONAL": "Ma sélection de support(s) pour les avis nationaux",
            "TITRE_BLOC_MOL": "Création/Suivi de la publicité via le MOL",
            "TITRE_BLOC_TNCP": "Création/Suivi de la publicité",
            "TITRE_BLOC_TED": "Ma sélection de support(s) pour les formulaires européens (formulaire à saisir)",
            "DRAFT": "Brouillon",
            "SUBMITTED": "Soumis",
            "STOPPED": "Publication interrompue",
            "PUBLISHING": "En cours de publication",
            "PUBLISHED": "Publié",
            "ARCHIVED": "Archivé",
            "VALIDATION_FAILED": "Validation échouée",
            "DELETED": "Supprimé",
            "NOT_PUBLISHED": "Non publié",
            "CONVERTED_FROM_PUBLISHED": "Converti d'un avis publié",
            "REPRENDRE": "Repris après Rejet",
            "TEXT_SELECTION_OFFRE_ECHOS": "Sélectionner",
            "DATES": "Date",
            "STATUT_ANNONCE_A_COMPLETER": "Brouillon",
            "STATUT_ANNONCE_PUBLIE": "Publié",
            "STATUT_ANNONCE_EN_ATTENTE_PUBLICATION": "En attente de publication",
            "STATUT_ANNONCE_PREPARATION_PUBLICATION": "En attente de publication",
            "STATUT_ANNONCE_ENVOYER": "Transmis",
            "DATE_ANNONCE_ENVOYER": "Date d'envoi",
            "DATE_ANNONCE_PREPARATION_PUBLICATION": "Initialisation",
            "STATUT_ANNONCE_EN_COURS_D_ARRET": "En cours d'annulation",
            "STATUT_ANNONCE_REJETE": "Rejeté",
            "STATUT_ANNONCE_ARRETER": "Annulé",
            "STATUT_ANNONCE_REPRENDRE": "Dupliqué après un rejet",
            "STATUT_ANNONCE_MODIFIER": "avis de changement",
            "STATUT_PUBLICATION": "Statut publication",
            "STATUT_ANNONCE_VALIDER_ECO": "Validé ECO",
            "DATE_ANNONCE_VALIDER_ECO": "Validation ECO",
            "STATUT_ANNONCE_VALIDER_SIP": "Validé SIP",
            "DATE_ANNONCE_VALIDER_SIP": "Validation SIP",
            "ACTIONS_PUBLICATION": "Actions",
            "DATE_ANNONCE_A_COMPLETER": "Date de création",
            "DATE_ANNONCE_PUBLIE": "Date d'envoi",
            "DATE_ANNONCE_MODIFIER": "Date de changement",
            "DATE_ANNONCE_REPRENDRE": "Date de duplication",
            "DATE_ANNONCE_EN_COURS_D_ARRET": "Date de demande d'annulation",
            "DATE_ANNONCE_ARRETER": "Date d'annulation",
            "DATE_ANNONCE_REJETE": "Date de rejet",
            "DATE_ANNONCE_REJETER_CONCENTRATEUR_VALIDATION_SIP": "Date de rejet de validation SIP",
            "DATE_ANNONCE_REJETER_CONCENTRATEUR_VALIDATION_ECO": "Date de rejet de validation ECO",
            "STATUT_ANNONCE_COMPLET": "Complet",
            "STATUT_ANNONCE_REJETER_CONCENTRATEUR_VALIDATION_ECO": "Rejet de validation ECO",
            "STATUT_ANNONCE_REJETER_CONCENTRATEUR_VALIDATION_SIP": "Rejet de validation SIP",
            "STATUT_ANNONCE_EN_ATTENTE_VALIDATION_ECO": "En attente de validation ECO",
            "STATUT_ANNONCE_ENVOI_PLANIFIER": "Envoi planifié",
            "STATUT_ANNONCE_EN_ATTENTE_VALIDATION_SIP": "En attente de validation SIP",
            "DATE_ANNONCE_EN_ATTENTE_PUBLICATION": "Date traitement",
            "DATE_ANNONCE_ENVOI_PLANIFIER": "Date d'envoi planifiée",
            "SELECTIONNEZ_TYPE_AVIS": "Sélectionnez un type d'avis",
            "SELECTIONNEZ_FACTURATION": "Sélectionnez une adresse de facturation",
            "LISTE_FACTURATION": "Liste des adresses de facturation",
            "DEMANDEZ_VALIDATION_AVIS_PUB": "Demandez la validation",
            "OUVRIR_BOAMP_TNCP": "Saisir/Suivre ses avis",
            "OUVRIR_BOAMP_MOL": "Saisir les avis sur le MOL",
            "REMPLIR_BOAMP_FORMULAIRE": "Ce choix exige la saisie d’un formulaire complémentaire MarchésOnline ci dessous.",
            "REMPLIR_FACTURATION_FORMULAIRE": "Ce choix exige la sélection d'une adresse de facturation.",
            "REMPLIR_EUROPEEN_FORMULAIRE": "Ce choix exige la saisie d’un formulaire complémentaire eNotices ci dessous.",
            "SELECTIONNER_SUPPORT_PUBLICITE_CONSULTATION": "Sélectionnez",
            "MSG_ERREUR_UTILISATEUR": "Un problème technique est survenu.",
            "NOUS_VOUS_RAPPELONS_QUE": "Nous vous rappelons que :",
            "RAPPEL_LES_ECHOS": "&lt;ul class=\"liste-actions\"&gt; &lt;li&gt;Pour toute procédure&lt;strong&gt; MAPA &gt; 90 K &amp;euro; HT&lt;/strong&gt;, l&#39;acheteur a l&#39;obligation de faire une publicité dans un journal d&#39;Annonces Légales (presse spécialisée si nécessaire ou au BOAMP).&lt;/li&gt;&lt;/ul&gt;",
            "EXPLICATION_PUBLICITE_AVIS_BOAMP_VALIDER_CONSULTATION": "Etes vous certain de vouloir demander la validation de cette consultation ?",
            "MSG_CONFIRMATION_VALIDATION_ELABORATION": "Etes vous certain de vouloir demander la validation de cette consultation ?",
            "NOM_COMPTE_PUB": "Vous avez sélectionné le(s) support(s) suivant(s) :",
            "RAPPEL_COMPLETER_AVIS_BOAMP_DANS_MOL": "Les avis à publier aux supports BOAMP, JOUE et le Moniteur doivent être complétés via MarcheOnline avant publication. &lt;br /&gt; Les avis à publier aux autres supports seront envoyés directement lors de la validation de la consultation.",
            "ENVOI_PUBLICITE_SIMPLIFIER_APRES_VALIDATION_CONSULTATION": "L&#39;envoi de la publicité aux autres supports se fait lors de la validation de la consultation",
            "EXPLICATION_PUBLICITE_SIMPLIFIER_VALIDER_CONSULTATION": "&lt;p&gt;L&#39;avis prévisualisable depuis cette page (bouton \"Prévisualiser l&#39;avis de publicité\") est directement envoyé au(x) support(s) sélectionné(s). Rendez-vous sur la page de gestion de la publicité de la consultation pour le suivi de cet avis.&lt;/p&gt;",
            "PREVISUALISEZ_AVIS_PUB": "Prévisualisez l&#39;avis de publicité",
            "SELECTIONNER_COMPTES_BOAMP_MOL":"Sélectionnez votre compte BOAMP",
            "SELECTIONNER_COMPTES_SUPPORT":"Sélectionnez votre compte",
            "SELECTIONNER_COMPTES_BOAMP_TNCP":"Sélectionnez votre compte BOAMP",

        }
        return this
    }

    /**
     * retourne une traduction qui peut avoir été préalablement encodée en base64
     * @param key
     * @returns {*}
     */
    get(key) {
        if (this.arr[key]) {
            try {
                atob(this.arr[key]);
                return this.filter(Base64.decode(this.arr[key]));
                // return this.filter(this.arr[key]);
            } catch (e) {
                return this.filter(this.arr[key]);
            }
        } else if (this.defaultArr[key]) {
            return this.defaultArr[key];
        }
        return key;
    }

    filter(str) {
        return str.replace(/liste-actions/g, "");
    }
}

module.exports = translate;
