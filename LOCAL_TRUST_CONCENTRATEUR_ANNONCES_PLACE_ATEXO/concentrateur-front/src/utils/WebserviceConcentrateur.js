import axios from "axios";
import {Base64} from 'js-base64';
import Statut from "../utils/Statut";

/**
 * Gère les appels au ws du concentrateur
 */
class webserviceConcentrateur {
    constructor(
        idConsultation,
        url,
        token,
        endpointSupports,
        endpointAnnonces,
        endpointContextes,
        endpointSuivis,
        endpointToken,
        basicToken,
        idPlateform,
        dmls,
        europeen,
        jalList,
        blocList,
        envoi,
        facturationList,
        endpointAgents
    ) {
        this.idPlateform = idPlateform
        this.url = url;
        this.token = token;
        this.endpointSupports = endpointSupports;
        this.endpointAnnonces = endpointAnnonces;
        this.endpointAgents = endpointAgents;
        this.endpointContextes = endpointContextes;
        this.endpointSuivis = endpointSuivis;
        this.endpointToken = endpointToken;
        this.basicToken = basicToken;
        this.ax = null;
        this.idConsultation = idConsultation;
        this.europeen = europeen;
        this.dmls = dmls;
        this.blocList = blocList;
        this.jalList = jalList;
        this.facturationList = facturationList;
        if (envoi !== null && envoi !== undefined && envoi !== 'null')
            this.envoi = JSON.parse(Base64.decode(envoi));
        return this;

    }

    /**
     * return void
     */
    init(montant = null, jal = null, pqr = null, national = null, departements = null, europeen = null) {
        var self = this;
        self.montant = montant
        self.jal = jal
        self.pqr = pqr
        self.national = national
        self.europeen = europeen
        self.departements = departements

        return this.getDepartements().then(departementsInit => {
            self.departementsInit = departementsInit
            if (self.departements === true) {
                self.departements = departementsInit;
            }
            return this.getSupports(self.montant, self.jal, self.pqr, self.national, self.departements, self.europeen);

        })
            .then(supports => {
                self.supports = supports;
                return this.getAnnonce(this.idConsultation)
            })
            .then(annonces => {
                self.annonces = annonces;
                return self

            })
            .catch(error => {
                throw error
            })
    }

    /**
     * @returns {Promise<T>}
     */
    getToken(idConsultation) {
        if (!this.token || "" === this.token) {
            return axios.create({
                baseURL: this.url,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + this.basicToken,
                    'Access-Control-Allow-Origin': '*'
                }
            }).get(this.endpointToken + '?grant_type=client_credentials&idPlatform=' + this.idPlateform + '&idConsultation=' + idConsultation)
                .then(res => {
                    return res.data.access_token
                });
        } else {

            var self = this;
            return new Promise(function (resolve) {
                resolve(self.token)
            })
        }
    }

    /**
     *
     * @param idConsultation
     * @param support
     * @param codeOffreF
     * @param xml
     * @param statut
     * @returns {Promise}
     */
    putAnnonce(idConsultation, annonce, xml, statut = null) {
        if (statut === null) {
            statut = Statut.BROUILLON;
        }
        var statutAnnonce = true === annonce.support.external ? Statut.EXTERNAL : statut;

        var suiviAnnonceDTO = {
            id: annonce.id,
            idConsultation: idConsultation,
            offre: annonce.offre,
            xml: xml,
            envoi: this.envoi,
            statut: statutAnnonce,
            simplifie: annonce.simplifie,
            consultationContexte: this.configuration.contexte
        };
        let PARAMS
        if (annonce.offre) {
            PARAMS = '?codeSupport=' + annonce.support.code + '&codeOffre=' + annonce.offre.code + '&idPlatform=' + this.idPlateform + '&idConsultation=' + idConsultation;
        } else {
            PARAMS = '?codeSupport=' + annonce.support.code + '&codeOffre=JOUE_2-FR&idPlatform=' + this.idPlateform + '&idConsultation=' + idConsultation;
        }

        return this.ax.put(this.endpointAnnonces + PARAMS, JSON.stringify(suiviAnnonceDTO))
            .then(res => {
                return res
            }).catch(error => {
                throw error
            })
    }

    getFiltre(xmlAnnonce) {
        var self = this;
        return this.getToken(this.idConsultation)
            .then(token => {
                this.token = token;
                this.ax = axios.create({
                    baseURL: this.url,
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token,
                        'Access-Control-Allow-Origin': '*'
                    }
                });

            }, (e) => {
                throw e;
            }).then(() => {
                let PARAMS = '/filtre?idPlatform=' + this.idPlateform + '&idConsultation=' + this.idConsultation;
                if (this.europeen === true || this.europeen === false) {
                    PARAMS += "&europeen=" + this.europeen;
                }
                return this.ax.get(this.endpointSupports + PARAMS).then(res => {
                    self.filtre = res.data;
                    if (!this.envoi) {
                        return this.getConfiguration();
                    }
                    return this.verifAgent(xmlAnnonce)
                }).then(configuration => {
                    self.configuration = configuration;
                    return self
                }).catch(error => {
                    throw error
                })
            });
    }

    ajoutAgent(xml) {
        return this.ax.post(this.endpointAgents, this.envoi).then(() => {
            return this.updateConfiguration(xml);
        }).catch(error => {
            throw error
        })

    }

    verifAgent(xml) {
        return this.ax.get(this.endpointAgents + "/exists").then(res => {
            console.log('verif Agent', res);
            return res.data === true ? this.updateConfiguration(xml) : this.ajoutAgent(xml);
        }).catch(error => {
            throw error
        })

    }

    updateConfiguration(xmlAnnonce) {
        let PARAMS = '/configuration';
        var suiviAnnonceDTO = {
            idConsultation: this.idConsultation,
            "xml": xmlAnnonce,
            "idPlatform": this.idPlateform,
            dmls: this.dmls,
            europeen: this.europeen,
            jalList: this.jalList,
            facturationList: this.facturationList,
            blocList: this.blocList,
        };
        var jsonParams = JSON.stringify(suiviAnnonceDTO);

        return this.ax.post(this.endpointAnnonces + PARAMS, jsonParams).then(() => {
            return this.getConfiguration();
        }).catch(error => {
            throw error
        })

    }


    getConfiguration() {
        var self = this;

        let PARAMS = '/configuration/' + this.idConsultation + "?idPlatform=" + this.idPlateform;
        let headers = {};
        return this.ax.get(this.endpointAnnonces + PARAMS, {headers}).then(res => {
            self.configuration = res.data;
            return res.data;
        }).catch(error => {
            throw error;
        })
    }

    /**
     *
     * @param idConsultation
     * @param codeSupport
     * @returns {Promise<T>}
     */
    deleteAnnonce(id) {
        const PARAMS = `/${id}?idPlatform=` + this.idPlateform + '&idConsultation=' + this.idConsultation;
        return this.ax.delete(this.endpointAnnonces + PARAMS)
            .then(res => {
                return res
            }).catch(error => {
                throw error
            })
    }

    /**
     *
     * @param idConsultation
     * @returns {Promise<T>}
     */
    getAnnonce(idConsultation) {
        const PARAMS = '?include=support&idPlatform=' + this.idPlateform + '&idConsultation=' + idConsultation;
        return this.ax.get(this.endpointAnnonces + PARAMS)
            .then(res => {
                return res.data
            }).catch(error => {
                error;
                return []
            })
    }

    /**
     *
     * @returns {Promise<T>}
     */
    getSupports(montant = 0, jal = null, pqr = null, national = null, departements = null, europeen = null) {
        var extra = "";
        if (null !== departements && departements.length > 0) {
            departements.forEach(function (departement) {
                if (true === jal) {
                    extra += "&jal=" + departement.code;
                }
                if (true === pqr) {
                    extra += "&pqr=" + departement.code;
                }
            });
        } else {
            if (true === jal) {
                extra += "&allJal=true";
            }
            if (true === pqr) {
                extra += "&allPqr=true";
            }
        }
        if (null !== montant) {
            extra += "&montant=" + montant;
        }
        if (null !== national) {
            extra += "&national=" + national;
        }
        if (null !== europeen) {
            extra += "&europeen=" + europeen;
        }
        if (null !== this.configuration && null !== this.configuration.blocList) {
            this.configuration.blocList.forEach(function (departement) {
                extra += "&blocs=" + departement;
            });
        }
        extra += "&idPlatform=" + this.idPlateform;
        extra += "&idConsultation=" + this.idConsultation;
        if (this.configuration && this.configuration.contexte && this.configuration.contexte.typeProcedureConsultation) {
            extra += "&typeProcedure=" + this.configuration.contexte.typeProcedureConsultation.abreviation;
        }
        if (this.configuration && this.configuration.contexte && this.configuration.contexte.organismeConsultation) {
            extra += "&organismes=" + this.configuration.contexte.organismeConsultation.acronyme;
        }

        const PARAMS = '?include=offres' + extra;
        return this.ax.get(this.endpointSupports + PARAMS)
            .then(res => {
                return res.data
            }).catch(error => {
                throw error
            })
    }

    getSupportsCodeGroup() {
        let extra = "?idPlatform=" + this.idPlateform;

        if (null !== this.configuration && null !== this.configuration.blocList) {
            this.configuration.blocList.forEach(blocs => {
                extra += "&blocs=" + blocs;
            });
        }
        if (this.configuration && this.configuration.contexte && this.configuration.contexte.typeProcedureConsultation) {
            extra += "&typeProcedure=" + this.configuration.contexte.typeProcedureConsultation.abreviation;
        }
        if (this.configuration && this.configuration.contexte && this.configuration.contexte.organismeConsultation) {
            extra += "&organismes=" + this.configuration.contexte.organismeConsultation.acronyme;
        }

        return this.ax.get(this.endpointSupports + '/plateforme/codeGroupe' + extra)
            .then(res => {
                return res.data
            }).catch(error => {
                throw error
            })
    }

    getAllSupports() {
        let extra = "?idPlatform=" + this.idPlateform;

        if (null !== this.configuration && null !== this.configuration.blocList) {
            this.configuration.blocList.forEach(blocs => {
                extra += "&blocs=" + blocs;
            });
        }
        if (this.configuration && this.configuration.contexte && this.configuration.contexte.typeProcedureConsultation) {
            extra += "&typeProcedure=" + this.configuration.contexte.typeProcedureConsultation.abreviation;
        }
        if (this.configuration && this.configuration.contexte && this.configuration.contexte.organismeConsultation) {
            extra += "&organismes=" + this.configuration.contexte.organismeConsultation.acronyme;
        }

        return this.ax.get(this.endpointSupports + '/plateforme/supports' + extra)
            .then(res => {
                return res.data
            }).catch(error => {
                throw error
            })
    }

    /**
     *
     * @returns {Promise<T>}
     */
    getPdf(idConsultation, support, codeOffre) {
        const PARAMS = '?codeSupport=' + support.code + '&codeOffre=' + codeOffre + '&idPlatform=' + this.idPlateform + '&idConsultation=' + idConsultation;
        return this.ax.get(this.endpointSupports + '/pdf' + PARAMS)
            .then(res => {
                return res
            })
            .catch(error => {
                throw error
            })
    }

    /**
     *
     * @returns {Promise<T>}
     */
    postPdf(idConsultation, xmlAnnonce, simplifie) {
        var suiviAnnonceDTO = {
            idConsultation: idConsultation,
            simplifie: simplifie,
            "statut": "BROUILLON",
            "xml": xmlAnnonce,
            "idPlatform": this.idPlateform
        };
        var jsonParams = JSON.stringify(suiviAnnonceDTO);

        return this.ax.post(this.endpointAnnonces + '/pdf', jsonParams, {responseType: 'blob'})
            .then(res => {
                return res;
            }).catch(error => {
                throw error
            })
    }

    postAnnoncePdf(suiviAnnonceDTO, xmlAnnonce) {
        suiviAnnonceDTO.xml = xmlAnnonce;
        var jsonParams = JSON.stringify(suiviAnnonceDTO);

        return this.ax.post(this.endpointAnnonces + '/pdf', jsonParams, {responseType: 'blob'})
            .then(res => {
                return res;
            }).catch(error => {
                throw error
            })
    }

    /**
     * @returns {Promise<T>}
     */
    getDepartements() {

        return this.ax.get('/rest/v2/departements')
            .then(res => {
                return res.data
            }).catch(error => {
                console.error(error);
                return []
            })
    }

    getContexte(contexte) {
        return this.ax.get(this.endpointContextes + '/' + contexte);
    }

    suivre(suivi) {
        return this.ax.put(this.endpointSuivis, JSON.stringify(suivi));
    }


    openCompte(request, rie) {
        let url = this.endpointAnnonces + '/' + request.annonce.id + '/ouvrir?idPlatform=' + this.idPlateform + '&idConsultation=' + this.idConsultation + '&rie=' + rie;
        return this.ax.get(url)
            .then(res => {
                console.log(res)
                return res
            }).catch(error => {
                throw error
            });
    }

    updateSimplifie(request) {
        let body = {simplifie: request.simplifie, idPlatform: this.idPlateform, idConsultation: this.idConsultation};
        return this.ax.patch(this.endpointAnnonces + '/' + request.id + '/simplifie', JSON.stringify(body))
            .then(res => {
                console.log(res)
                return res
            }).catch(error => {
                throw error
            });
    }

    editAnnonceDocument(id, simplifie) {
        return this.ax.get(this.endpointAnnonces + '/' + id + '/editeur-en-ligne?idPlatform=' + this.idPlateform + '&idConsultation=' + this.idConsultation + "&simplifie=" + simplifie)
            .then(res => {
                console.log(res)
                return res
            }).catch(error => {
                throw error
            });
    }

    reinitAnnonceDocument(id, simplifie) {
        return this.ax.patch(this.endpointAnnonces + '/' + id + '/reinit-document?idPlatform=' + this.idPlateform + '&idConsultation=' + this.idConsultation + "&simplifie=" + simplifie, null)
            .then(res => {
                console.log(res)
                return res
            }).catch(error => {
                throw error
            });
    }

    reprendre(id) {
        return this.ax.patch(this.endpointAnnonces + '/' + id + '/reprendre?idPlatform=' + this.idPlateform + '&idConsultation=' + this.idConsultation, null)
            .then(res => {
                console.log(res)
                return res
            }).catch(error => {
                throw error
            });
    }

    change(id) {
        return this.ax.patch(this.endpointAnnonces + '/' + id + '/change?idPlatform=' + this.idPlateform + '&idConsultation=' + this.idConsultation, null)
            .then(res => {
                console.log(res)
                return res
            }).catch(error => {
                throw error
            });
    }

    getRemoteRessourceText(apiUrl) {
        return this.ax.get(apiUrl, {responseType: 'text'});
    }
}

module.exports = webserviceConcentrateur;
