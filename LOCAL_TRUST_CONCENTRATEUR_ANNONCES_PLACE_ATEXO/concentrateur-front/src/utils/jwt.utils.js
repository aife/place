import {isNullOrUndefined} from "./technical.utils";
import jwt_decode from "jwt-decode";

class JwtUtils {
    constructor() {
    }

    static parseJwt(token) {
        if (isNullOrUndefined(token)) {
            return null;
        }
        return jwt_decode(token)
    }

    static checkTokenValidity(exp) {
        if (isNullOrUndefined(exp))
            return false;
        const elapsed = exp * 1000 - Date.now();
        return elapsed > 0;
    }

    static checkTokenWarning(exp) {
        if (isNullOrUndefined(exp))
            return false;
        const elapsed = exp * 1000 - Date.now();
        var difference = new Date(elapsed);
        let minutes = difference.getMinutes();
        console.log("Token encore valide pour", difference.getUTCHours(), "heure(s) et", minutes, "minute(s)")
        return difference.getUTCHours() === 0 && minutes < 10 && minutes !== 0;
    }

    static getDateRestant(exp) {
        if (isNullOrUndefined(exp))
            return new Date();
        const elapsed = exp * 1000 - Date.now();
        return  new Date(elapsed);

    }

}

module.exports = JwtUtils;
