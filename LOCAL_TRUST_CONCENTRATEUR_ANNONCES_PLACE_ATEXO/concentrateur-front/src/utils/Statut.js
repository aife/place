import {faEdit} from "@fortawesome/free-solid-svg-icons/faEdit";
import {faCheckSquare} from "@fortawesome/free-solid-svg-icons/faCheckSquare";
import {faShare} from "@fortawesome/free-solid-svg-icons/faShare";
import {faClock} from "@fortawesome/free-solid-svg-icons/faClock";
import {faCheckCircle} from "@fortawesome/free-solid-svg-icons/faCheckCircle";
import {faBan} from "@fortawesome/free-solid-svg-icons/faBan";

/**
 * Object de traduction
 */
class Statut {

    static EXTERNAL = "EXTERNAL";

    static BROUILLON = "BROUILLON";

    static MODIFIER = "MODIFIER";

    static COMPLET = "COMPLET";

    static TRANSMIS = "TRANSMIS";

    static EN_ATTENTE = "EN_ATTENTE";
    static EN_ATTENTE_VALIDATION_ECO = "EN_ATTENTE_VALIDATION_ECO";
    static EN_ATTENTE_VALIDATION_SIP = "EN_ATTENTE_VALIDATION_SIP";
    static REPRENDRE = "REPRENDRE";
    static EN_COURS_DE_PUBLICATION = "EN_COURS_DE_PUBLICATION";
    static PREPARATION_PUBLICATION = "PREPARATION_PUBLICATION";

    static PUBLIER = "PUBLIER";

    static REJETER_SUPPORT = "REJETER_SUPPORT";

    static REJETER_CONCENTRATEUR = "REJETER_CONCENTRATEUR";
    static REJETER_CONCENTRATEUR_VALIDATION_ECO = "REJETER_CONCENTRATEUR_VALIDATION_ECO";
    static REJETER_CONCENTRATEUR_VALIDATION_SIP = "REJETER_CONCENTRATEUR_VALIDATION_SIP";
    static EN_COURS_D_ARRET = "EN_COURS_D_ARRET";
    static ARRETER = "ARRETER";
    static ENVOI_PLANIFIER = "ENVOI_PLANIFIER";
    static ENVOI_PLANIFIER_EUROPEEN = "ENVOI_PLANIFIER_EUROPEEN";
    static VALIDER_ECO = "VALIDER_ECO";
    static VALIDER_SIP = "VALIDER_SIP";

    constructor(translator) {

        this.translator = translator;

        this.statut = [];

        this.statut[Statut.BROUILLON] = [
            "STATUT_ANNONCE_A_COMPLETER", 'DATE_ANNONCE_A_COMPLETER', faEdit, "default primary-client-bg"
        ];
        this.statut[Statut.REPRENDRE] = [
            "STATUT_ANNONCE_REPRENDRE", 'DATE_ANNONCE_REPRENDRE', faEdit, "default primary-client-bg"
        ];
        this.statut[Statut.MODIFIER] = [
            "STATUT_ANNONCE_MODIFIER", 'DATE_ANNONCE_MODIFIER', faEdit, "default primary-client-bg"
        ];

        this.statut[Statut.EXTERNAL] = [
            "STATUT_ANNONCE_A_COMPLETER", 'DATE_ANNONCE_A_COMPLETER', faEdit, "default primary-client-bg"
        ];

        this.statut[Statut.COMPLET] = [
            "STATUT_ANNONCE_COMPLET", "DATE_ANNONCE_COMPLET", faCheckSquare, "primary primary-client-bg"
        ];

        this.statut[Statut.COMPLET] = [
            "STATUT_ANNONCE_COMPLET", "DATE_ANNONCE_COMPLET", faCheckSquare, "primary primary-client-bg"
        ];

        this.statut[Statut.TRANSMIS] = [
            "STATUT_ANNONCE_ENVOYER", "DATE_ANNONCE_ENVOYER", faShare, "info"
        ];

        this.statut[Statut.EN_ATTENTE] = [
            "STATUT_ANNONCE_EN_ATTENTE_PUBLICATION", "DATE_ANNONCE_EN_ATTENTE_PUBLICATION", faClock, "warning"
        ];
        this.statut[Statut.EN_ATTENTE_VALIDATION_ECO] = [
            "STATUT_ANNONCE_EN_ATTENTE_VALIDATION_ECO", "DATE_ANNONCE_EN_ATTENTE_VALIDATION_ECO", faClock, "warning"
        ];
        this.statut[Statut.EN_ATTENTE_VALIDATION_SIP] = [
            "STATUT_ANNONCE_EN_ATTENTE_VALIDATION_SIP", "DATE_ANNONCE_EN_ATTENTE_VALIDATION_SIP", faClock, "warning"
        ];

        this.statut[Statut.EN_COURS_DE_PUBLICATION] = [
            "STATUT_ANNONCE_ENVOYER", "DATE_ANNONCE_ENVOYER", faShare, "info"
        ];

        this.statut[Statut.PREPARATION_PUBLICATION] = [
            "STATUT_ANNONCE_PREPARATION_PUBLICATION", "DATE_ANNONCE_PREPARATION_PUBLICATION", faClock, "info"
        ];

        this.statut[Statut.VALIDER_ECO] = [
            "STATUT_ANNONCE_VALIDER_ECO", "DATE_ANNONCE_VALIDER_ECO", faCheckCircle, "success"
        ];
        this.statut[Statut.VALIDER_SIP] = [
            "STATUT_ANNONCE_VALIDER_SIP", "DATE_ANNONCE_VALIDER_SIP", faCheckCircle, "success"
        ];
        this.statut[Statut.PUBLIER] = [
            "STATUT_ANNONCE_PUBLIE", "DATE_ANNONCE_PUBLIE", faCheckCircle, "success"
        ];     this.statut[Statut.ENVOI_PLANIFIER] = [
            "STATUT_ANNONCE_ENVOI_PLANIFIER", "DATE_ANNONCE_ENVOI_PLANIFIER", faCheckCircle, "success"
        ];  this.statut[Statut.ENVOI_PLANIFIER] = [
            "STATUT_ANNONCE_ENVOI_PLANIFIER", "DATE_ANNONCE_ENVOI_PLANIFIER", faCheckCircle, "success"
        ];

        this.statut[Statut.REJETER_SUPPORT] = [
            "STATUT_ANNONCE_REJETE", "DATE_ANNONCE_REJETE", faBan, "danger"
        ];

        this.statut[Statut.ARRETER] = [
            "STATUT_ANNONCE_ARRETER", "DATE_ANNONCE_ARRETER", faBan, "info"
        ];

        this.statut[Statut.REJETER_CONCENTRATEUR] = [
            "STATUT_ANNONCE_REJETE", "DATE_ANNONCE_REJETE", faBan, "danger"
        ];
        this.statut[Statut.REJETER_CONCENTRATEUR_VALIDATION_SIP] = [
            "STATUT_ANNONCE_REJETER_CONCENTRATEUR_VALIDATION_SIP", "DATE_ANNONCE_REJETER_CONCENTRATEUR_VALIDATION_SIP", faBan, "danger"
        ];
        this.statut[Statut.REJETER_CONCENTRATEUR_VALIDATION_ECO] = [
            "STATUT_ANNONCE_REJETER_CONCENTRATEUR_VALIDATION_ECO", "DATE_ANNONCE_REJETER_CONCENTRATEUR_VALIDATION_ECO", faBan, "danger"
        ];
        this.statut[Statut.EN_COURS_D_ARRET] = [
            "STATUT_ANNONCE_EN_COURS_D_ARRET", "DATE_ANNONCE_EN_COURS_D_ARRET", faClock, "info"
        ];

        return this;
    }


    get(param) {
        if (this.statut[param]) {
            const [libelle, date, fa, type] = this.statut[param];

            return {
                "libelle": this.translator.get(libelle),
                "date": this.translator.get(date),
                "fa": fa,
                "type": type
            };
        }
        return false
    }

}

module.exports = Statut;
