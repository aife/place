import axios from "axios";
import Statut from "../utils/Statut"

/**
 * Gère les appels au ws du concentrateur
 */
class webserviceConcentrateur {
    constructor(
        idConsultation,
        url,
        token,
        endpointSupports,
        endpointAnnonces,
        endpointToken,
        basicToken,
        idPlateform
    ) {
        this.idPlateform = idPlateform
        this.url = url;
        this.token = token;
        this.endpointSupports = endpointSupports;
        this.endpointAnnonces = endpointAnnonces;
        this.endpointToken = endpointToken;
        this.basicToken = basicToken;
        this.ax = null;
        this.idConsultation = idConsultation;
        return this;
    }

    /**
     * return void
     */
    init(montant = null, jal = null, pqr = null, departements = null) {
        var self = this;
        self.montant = montant
        self.jal = jal
        self.pqr = pqr
        self.departements = departements

        return this.getToken(this.idConsultation)

            .then(token => {
                self.token = token;
                self.ax = axios.create({
                    baseURL: this.url,
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token,
                        'Access-Control-Allow-Origin': '*'
                    }
                });

            }).then(() => {
                return this.getDepartements();
            }).then(departementsInit => {
                self.departementsInit = departementsInit
                if (self.departements === true) {
                    self.departements = departementsInit;
                }
                return this.getSupports(self.montant, self.jal, self.pqr, self.departements);

            })
            .then(supports => {
                self.supports = supports;
                return this.getAnnonce(this.idConsultation)
            })
            .then(annonces => {
                self.annonces = annonces;
                return self
            })
            .catch(error => {
                throw error
            })
    }

    /**
     * @returns {Promise<T>}
     */
    getToken(idConsultation) {
        if (!this.token || "" === this.token) {
            return axios.create({
                baseURL: this.url,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + this.basicToken,
                    'Access-Control-Allow-Origin': '*'
                }
            }).get(this.endpointToken + '?grant_type=client_credentials&idPlatform=' + this.idPlateform + '&idConsultation=' + idConsultation)
                .then(res => {
                    return res.data.access_token
                });
        } else {

            var self = this;
            return new Promise(function (resolve) {
                resolve(self.token)
            })
        }
    }

    /**
     *
     * @param idConsultation
     * @param support
     * @param codeOffre
     * @param xml
     * @param statut
     * @returns {Promise}
     */
    putAnnonce(idConsultation, support, codeOffre, xml, statut = null) {
        if (statut === null) {
            statut = Statut.BROUILLON;
        }
        var statutAnnonce = true === support.external ? Statut.EXTERNAL : statut;

        var suiviAnnonceDTO = {
            idConsultation: idConsultation,
            offre: {
                code: codeOffre,
                libelle: codeOffre,
                support: {
                    code: support.code,
                    libelle: support.code
                }
            },
            xml: xml,
            statut: statutAnnonce
        };
        const PARAMS = '?codeSupport=' + support.code + '&idPlatform=' + this.idPlateform + '&idConsultation=' + idConsultation;
        return this.ax.put(this.endpointAnnonces + PARAMS, JSON.stringify(suiviAnnonceDTO))
            .then(res => {
                return res
            }).catch(error => {
                throw error
            })
    }

    /**
     *
     * @param idConsultation
     * @param codeSupport
     * @returns {Promise<T>}
     */
    deleteAnnonce(idConsultation, codeSupport) {
        const PARAMS = '?codeSupport=' + codeSupport + '&idPlatform=' + this.idPlateform + '&idConsultation=' + idConsultation;
        return this.ax.delete(this.endpointAnnonces + PARAMS)
            .then(res => {
                return res
            }).catch(error => {
                throw error
            })
    }

    /**
     *
     * @param idConsultation
     * @returns {Promise<T>}
     */
    getAnnonce(idConsultation) {
        const PARAMS = '?include=support&idPlatform=' + this.idPlateform + '&idConsultation=' + idConsultation;
        return this.ax.get(this.endpointAnnonces + PARAMS)
            .then(res => {
                return res.data
            }).catch(error => {
                error;
                return []
            })
    }

    /**
     *
     * @returns {Promise<T>}
     */
    getSupports(montant = 0, jal = null, pqr = null, departements = null) {
        var extra = "";
        if (null !== departements) {
            departements.forEach(function (departement) {
                if (true === jal) {
                    extra += "&jal=" + departement.code;
                }
                if (true === pqr) {
                    extra += "&pqr=" + departement.code;
                }
            });
        }
        if (null !== montant) {
            extra += "&montant=" + montant;
        }
        extra += '&idPlatform=' + this.idPlateform;

        const PARAMS = '?include=offres&national=true' + extra;
        return this.ax.get(this.endpointSupports + PARAMS)
            .then(res => {
                return res.data
            }).catch(error => {
                throw error
            })
    }

    /**
     *
     * @returns {Promise<T>}
     */
    getPdf(idConsultation, support) {
        const PARAMS = '?codeSupport=' + support.code + '&idPlatform=' + this.idPlateform + '&idConsultation=' + idConsultation;
        return this.ax.get(this.endpointSupports + '/pdf' + PARAMS)
            .then(res => {
                return res
            })
            .catch(error => {
                throw error
            })
    }

    /**
     *
     * @returns {Promise<T>}
     */
    postPdf(idConsultation, xmlAnnonce, simplifie, support, offre) {
        console.log(support, offre)
        var suiviAnnonceDTO = {
            idConsultation: idConsultation,
            "statut": "BROUILLON",
            "xml": xmlAnnonce,
            "idPlatform": this.idPlateform,
            "simplifie": simplifie,
            "offre": offre,
            "support": support
        };
        var jsonParams = JSON.stringify(suiviAnnonceDTO);

        return this.ax.post(this.endpointAnnonces + '/pdf', jsonParams, {responseType: 'blob'})
            .then(res => {
                return res;
            }).catch(error => {
                throw error
            })
    }

    /**
     * @returns {Promise<T>}
     */
    getDepartements() {

        return this.ax.get('/rest/v2/departements')
            .then(res => {
                return res.data
            }).catch(error => {
                error;
                return []
            })
    }
}

module.exports = webserviceConcentrateur;
