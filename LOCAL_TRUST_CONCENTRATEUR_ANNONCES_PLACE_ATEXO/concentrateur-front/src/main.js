import 'es6-promise/auto'
import '@babel/polyfill'
import Vue from 'vue'

//import App from './App-consultationFormulaire.vue'
// import App from './App-annonceRecap.vue'
// import App from './App-bcrypt.vue'
import App from './App-AnnonceSuivi.vue'
//import App from './views/Home.vue'

new Vue({
    render: h => h(App),
}).$mount('#app');
