package com.atexo.admin.security;


import com.atexo.admin.dao.UserEntity;
import com.atexo.admin.dao.UserRepository;
import com.atexo.admin.model.AtexoUserDetails;
import com.atexo.admin.model.MessageResponse;
import com.atexo.admin.model.SignupRequest;
import com.atexo.admin.services.IUserPort;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserAdapter implements IUserPort {
	private final UserRepository userRepository;


	public UserAdapter(UserRepository userRepository) {
		this.userRepository = userRepository;

	}

	@Override
	public AtexoUserDetails findByUsername(String username) {
		UserEntity user = userRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

		return this.build(user);
	}

	@Override
	public MessageResponse registerUser(SignupRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new MessageResponse(HttpStatus.BAD_REQUEST, "Error: Username is already taken!");

		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new MessageResponse(HttpStatus.BAD_REQUEST, "Error: Email is already in use!");
		}

		// Create new user's account
		UserEntity user = UserEntity.builder().username(signUpRequest.getUsername())
				.email(signUpRequest.getEmail())
				.password(new BCryptPasswordEncoder().encode(signUpRequest.getPassword()))
				.build();


		userRepository.save(user);

		return new MessageResponse(HttpStatus.OK, "User registered successfully!");
	}


	public AtexoUserDetails build(UserEntity user) {


		return new AtexoUserDetails(
				user.getId(),
				user.getUsername(),
				user.getEmail(),
				user.getPassword(),
				null);
	}
}
