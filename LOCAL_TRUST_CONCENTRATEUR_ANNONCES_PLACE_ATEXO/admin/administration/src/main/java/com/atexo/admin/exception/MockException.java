package com.atexo.admin.exception;

import lombok.Getter;

@Getter
public class MockException extends RuntimeException {
    private final String errorMessage;

    public MockException(String errorMessage, Throwable err) {
        super(errorMessage, err);
        this.errorMessage = errorMessage;
    }

    public MockException(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }
}
