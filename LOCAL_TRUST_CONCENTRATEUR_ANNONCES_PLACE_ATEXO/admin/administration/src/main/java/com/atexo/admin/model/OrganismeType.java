//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2021.05.06 à 09:33:04 AM CEST 
//


package com.atexo.admin.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour OrganismeType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="OrganismeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="acronyme" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="denominationOrganisme" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="siren" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="complement" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrganismeType", propOrder = {

})
public class OrganismeType {

    protected int id;
    @XmlElement(required = true)
    protected String acronyme;
    @XmlElement(required = true)
    protected String denominationOrganisme;
    @XmlElement(required = true)
    protected String siren;
    @XmlElement(required = true)
    protected String complement;

    /**
     * Obtient la valeur de la propriété id.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propriété acronyme.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcronyme() {
        return acronyme;
    }

    /**
     * Définit la valeur de la propriété acronyme.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcronyme(String value) {
        this.acronyme = value;
    }

    /**
     * Obtient la valeur de la propriété denominationOrganisme.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenominationOrganisme() {
        return denominationOrganisme;
    }

    /**
     * Définit la valeur de la propriété denominationOrganisme.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenominationOrganisme(String value) {
        this.denominationOrganisme = value;
    }

    /**
     * Obtient la valeur de la propriété siren.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiren() {
        return siren;
    }

    /**
     * Définit la valeur de la propriété siren.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiren(String value) {
        this.siren = value;
    }

    /**
     * Obtient la valeur de la propriété complement.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComplement() {
        return complement;
    }

    /**
     * Définit la valeur de la propriété complement.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComplement(String value) {
        this.complement = value;
    }

}
