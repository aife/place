package com.atexo.admin.services;


import com.atexo.admin.model.AtexoUserDetails;
import com.atexo.admin.model.MessageResponse;
import com.atexo.admin.model.SignupRequest;

public interface IUserPort {

	AtexoUserDetails findByUsername(String username);
	MessageResponse registerUser(SignupRequest admin);

}
