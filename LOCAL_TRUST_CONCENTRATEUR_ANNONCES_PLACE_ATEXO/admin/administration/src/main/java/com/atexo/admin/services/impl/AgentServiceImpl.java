package com.atexo.admin.services.impl;

import com.atexo.admin.config.AdminProperties;
import com.atexo.admin.config.AgentProperties;
import com.atexo.admin.model.AdminDto;
import com.atexo.admin.model.AgentResponse;
import com.atexo.admin.model.EFormsFormulaireSearch;
import com.atexo.admin.model.Token;
import com.atexo.admin.services.AgentService;
import com.atexo.annonces.commun.mpe.EnvoiMpe;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.nio.charset.Charset;

@Slf4j
@Service
public class AgentServiceImpl implements AgentService {


    private final AdminProperties adminProperties;
    public final AgentProperties agentProperties;

    @Value("${administration.url}")
    private String urlAgentApi;

    @Value("${administration.api.url}")
    private String apiAgentUrl;

    public AgentServiceImpl(AdminProperties adminProperties, AgentProperties agentProperties) {
        this.adminProperties = adminProperties;
        this.agentProperties = agentProperties;
    }


    @Override
    public AdminDto calculateToken() {
        Token token = null;
        try {
            token = new RestTemplate().postForObject(adminProperties.getTokenUrl(), new HttpEntity<>(createHeaders(adminProperties.getLogin(),
                    adminProperties.getPassword())), Token.class);
        } catch (Exception e) {
            log.error(e.getMessage());
            token = Token.builder().accessToken(e.getMessage()).build();
        }
        return AdminDto.builder()
                .login(adminProperties.getLogin())
                .password(adminProperties.getPassword())
                .plateforme(adminProperties.getPlateforme())
                .email(adminProperties.getEmail())
                .token(token.getAccessToken())
                .build();
    }

    @Override
    public EFormsFormulaireSearch getDefaultAgent() {
        return EFormsFormulaireSearch.builder()
                .organisme(agentProperties.getOrganisme())
                .plateforme(adminProperties.getPlateforme())
                .idNotice("16")
                .lang("FRA")
                .auteurEmail("ismail.atitallah@atexo.com")
                .idConsultation(508127)
                .build();
    }

    @Override
    public AgentResponse createContext(EnvoiMpe agt) {
        AdminDto adminDto = this.calculateToken();
        String token = adminDto.getToken();
        String idContexte = this.createAgent(token, agt);
        return AgentResponse.builder()
                .urlAgentApi(urlAgentApi)
                .token(token)
                .idContexte(idContexte)
                .build();

    }

    HttpHeaders createHeaders(String username, String password) {
        return new HttpHeaders() {
            {
                String auth = username + ":" + password;
                byte[] encodedAuth = Base64.encodeBase64(
                        auth.getBytes(Charset.forName("US-ASCII")));
                String authHeader = "Basic " + new String(encodedAuth);
                set("Authorization", authHeader);
            }
        };
    }

    public String createAgent(String token, EnvoiMpe mpe) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            URI uri = new URI(this.apiAgentUrl);
            //HEADER
            HttpHeaders headers = new HttpHeaders();
            String authHeader = "Bearer " + token;
            headers.set("Authorization", authHeader);
            headers.setContentType(MediaType.APPLICATION_JSON);
            //BODY
            //init
            HttpEntity<EnvoiMpe> request = new HttpEntity<>(mpe, headers);
            ResponseEntity<String> result = restTemplate.postForEntity(uri, request, String.class);
            return result.getBody();

        } catch (Exception e) {
            return null;
        }
    }
}
