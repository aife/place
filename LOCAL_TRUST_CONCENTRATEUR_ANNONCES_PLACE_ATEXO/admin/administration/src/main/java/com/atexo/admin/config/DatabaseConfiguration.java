package com.atexo.admin.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EntityScan("com.atexo.admin.dao")
@EnableJpaRepositories("com.atexo.admin.dao")
public class DatabaseConfiguration {

	@Value("${administration.datasource.driver-class-name}")
	private String driver;

	@Value("${administration.datasource.url}")
	private String url;

	@Value("${admin.login}")
	private String login;

	@Value("${admin.password}")
	private String password;

	@Bean
	@Primary
	public DataSource getDataSource() {
		DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
		dataSourceBuilder.driverClassName(driver);
		dataSourceBuilder.url(url);
		dataSourceBuilder.username(login);
		dataSourceBuilder.password(password);
		return dataSourceBuilder.build();
	}

}
