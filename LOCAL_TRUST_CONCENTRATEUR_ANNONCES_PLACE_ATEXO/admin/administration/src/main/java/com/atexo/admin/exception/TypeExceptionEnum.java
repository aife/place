package com.atexo.admin.exception;

public enum TypeExceptionEnum {
	 ERREUR_FICHIER(1);
	private final int code;

	TypeExceptionEnum(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}
}
