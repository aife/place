package com.atexo.admin.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AdminDto {
    private String login;
    private String password;
    private String plateforme;
    private String email;
    private String token;
}
