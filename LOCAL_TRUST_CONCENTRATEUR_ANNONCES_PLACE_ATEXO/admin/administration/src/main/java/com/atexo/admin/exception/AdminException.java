package com.atexo.admin.exception;


public class AdminException extends RuntimeException {
    private static final long serialVersionUID = 100L;
    private final TypeExceptionEnum type;


    public AdminException(String message) {
        super(message);
        this.type = TypeExceptionEnum.ERREUR_FICHIER;
    }
    
    public TypeExceptionEnum getType() {
        return type;
    }
    
}
