package com.atexo.admin.services;


import com.atexo.admin.model.AdminDto;
import com.atexo.admin.model.AgentResponse;
import com.atexo.admin.model.EFormsFormulaireSearch;
import com.atexo.annonces.commun.mpe.EnvoiMpe;

public interface AgentService {

    AdminDto calculateToken();

    EFormsFormulaireSearch getDefaultAgent();

    AgentResponse createContext(EnvoiMpe agent);

}
