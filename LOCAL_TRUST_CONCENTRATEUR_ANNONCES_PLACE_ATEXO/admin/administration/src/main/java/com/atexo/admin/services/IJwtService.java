package com.atexo.admin.services;


import org.springframework.security.core.Authentication;

public interface IJwtService {

	String generateJwtToken(Authentication authentication);

	boolean validateJwtToken(String authToken);

	String getUserNameFromJwtToken(String jwt);
}
