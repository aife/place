package com.atexo.admin.model;

import lombok.*;

import java.io.Serializable;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
public class EFormsFormulaire implements Serializable {

    private Long id;

    private String uuid;

    private String idNotice;

    private String idConsultation;

    private String type;

    private String xml;

    private String xmlGenere;

    private String formulaire;


}
