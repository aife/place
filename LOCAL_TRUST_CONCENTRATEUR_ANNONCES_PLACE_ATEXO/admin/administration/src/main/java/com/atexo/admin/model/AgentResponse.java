package com.atexo.admin.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AgentResponse implements Serializable {
    private static final long serialVersionUID = -5260054892149009386L;
    private String urlAgentApi;
    private String token;
    private String idContexte;
}
