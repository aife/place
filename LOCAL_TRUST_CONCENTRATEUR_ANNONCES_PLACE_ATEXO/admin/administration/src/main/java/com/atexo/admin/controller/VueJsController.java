package com.atexo.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class VueJsController {


    @GetMapping({"/", "/dashboard", "/bcrypt", "/formulaire", "/login", "/simulateur", "/swagger", "/annonce-suivi", "/simulateur-atexo-configuration", "/simulateur-plateformes-configuration",
            "/javamelody", "/logs", "/annonce-recap", "/simulateur-wc", "/simulateur-validation", "/simulateur-validation-wc", "/add-configuration", "modify-configuration/**",
            "/dashboard-configuration"})
    public String vueJs() {
        return "index";
    }


}
