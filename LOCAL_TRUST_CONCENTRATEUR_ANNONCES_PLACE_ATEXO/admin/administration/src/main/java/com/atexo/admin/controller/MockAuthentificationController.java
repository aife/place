package com.atexo.admin.controller;

import com.atexo.admin.model.AdminDto;
import com.atexo.admin.model.AgentResponse;
import com.atexo.admin.model.EFormsFormulaireSearch;
import com.atexo.admin.services.AgentService;
import com.atexo.annonces.commun.mpe.EnvoiMpe;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RequestMapping("administration")
@RestController
public class MockAuthentificationController {

    private final AgentService agentService;

    public MockAuthentificationController(AgentService agentService) {
        this.agentService = agentService;
    }

    @GetMapping("token")
    public AdminDto getToken() {
        return agentService.calculateToken();
    }

    @GetMapping("default-agent")
    public EFormsFormulaireSearch getDefaultAgent() {
        return agentService.getDefaultAgent();

    }

    @PostMapping("create-contexte")
    public AgentResponse createContext(@RequestBody EnvoiMpe agent) {
        return agentService.createContext(agent);
    }


}
