package com.atexo.admin.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "administration.agent")
@Getter
@Setter
public class AgentProperties {
    private String identifiant;
    private String nom;
    private String prenom;
    private String organisme;
    private String acheteurPublic;
    private String photoUrl;
    private String sigleUrl;
    private String serveurUrl;
    private String serveurLogin;
    private String serveurPassword;
    private List<String> roles;

}
