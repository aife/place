package com.atexo.admin.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class EFormsFormulaireSearch {
    private String plateforme;
    private String organisme;
    private String tokenMpe;
    private String mpeUrl;
    private String lang;
    private String idNotice;
    private String auteurEmail;
    private Integer idConsultation;
}
