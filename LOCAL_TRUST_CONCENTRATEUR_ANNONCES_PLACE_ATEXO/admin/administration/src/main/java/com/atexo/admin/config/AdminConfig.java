package com.atexo.admin.config;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.InputStream;

@Configuration
public class AdminConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/atexo-icone.png")
                .addResourceLocations("classpath:/templates/atexo-icone.png");
        registry
                .addResourceHandler("/css/**")
                .addResourceLocations("classpath:/templates/css/");
        registry
                .addResourceHandler("/js/**")
                .addResourceLocations("classpath:/templates/js/");
        registry
                .addResourceHandler("/img/**")
                .addResourceLocations("classpath:/templates/img/");
        registry
                .addResourceHandler("/fonts/**")
                .addResourceLocations("classpath:/templates/fonts/");
    }

    @Bean
    public JoranConfigurator log() {
        LoggerContext context = (LoggerContext) org.slf4j.LoggerFactory.getILoggerFactory();
        JoranConfigurator configurator = null;
        try {
            configurator = new JoranConfigurator();
            configurator.setContext(context);
            context.reset();
            InputStream logConfStream;
            logConfStream = AdminConfig.class.getResourceAsStream("/logback-mock.xml");
            if (logConfStream == null) {
                logConfStream = AdminConfig.class.getResourceAsStream("/logback-local.xml");
            }
            configurator.doConfigure(logConfStream);
        } catch (JoranException e) {
        }
        StatusPrinter.printInCaseOfErrorsOrWarnings(context);
        return configurator;
    }
}
