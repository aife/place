import api from "./api";

class MonitoringService{
    getAllLogs(){
        return api
            .get("/administration/files-logs/all")
    }

    downloadLog(nom:string) {
        return api
            .get("/administration/files-logs?name=" + nom)
    }
}
export default new MonitoringService();
