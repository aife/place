import api from "./api";
import AgentContexteModel from "@/models/agentContexte-model";

class MockAgentService {

    createConnexionContexte(agent: AgentContexteModel) {
        return api
            .post("./administration/create-contexte",agent)
    }
    getDefaultAgent() {
        return api
            .get("./administration/default-agent")
    }
    getToken() {
        return api
            .get("./administration/token")
    }
}
export default new MockAgentService();
