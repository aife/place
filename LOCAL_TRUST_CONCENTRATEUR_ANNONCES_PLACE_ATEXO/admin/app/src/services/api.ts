import axios from 'axios'

function getAxiosInstance() {
    const api = axios.create();
    api.defaults.timeout = 5 * 60000;
    return api;
}

export default getAxiosInstance()
