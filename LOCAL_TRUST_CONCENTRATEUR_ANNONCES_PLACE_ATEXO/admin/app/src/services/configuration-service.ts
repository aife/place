import api from "./api-concentrateur";

class ConfigurationService {
    getAllSupports() {
        return api
            .get("/concentrateur-annonces/rest/v2/supports/all")
    }

    getAllConfiguration() {
        return api
            .get("/concentrateur-annonces/rest/v2/plateformes-configuration")
    }

    addOrUpdate(configuration: any) {
        return api
            .post("/concentrateur-annonces/rest/v2/plateformes-configuration", configuration)
    }

    getById(id: number) {
        return api
            .get("/concentrateur-annonces/rest/v2/plateformes-configuration/" + id)
    }

    delete(id: number) {
        return api
            .delete("/concentrateur-annonces/rest/v2/plateformes-configuration/" + id)
    }

    getToken(basicToken: string, idPlateform: string) {
        return api.get('/concentrateur-annonces/rest/v2/oauth/token?grant_type=client_credentials&idPlatform=' + idPlateform,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + basicToken,
                    'Access-Control-Allow-Origin': '*'
                }
            })
            .then(res => {
                localStorage.setItem("tokenAnnonces", res.data.access_token)
                return res.data.access_token
            });

    }
}

export default new ConfigurationService();
