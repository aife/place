var xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
    "<TED_ESENDERS xmlns=\"http://publications.europa.eu/resource/schema/ted/R2.0.9/reception\"\n" +
    "              xmlns:n2016=\"http://publications.europa.eu/resource/schema/ted/2016/nuts\"\n" +
    "              VERSION=\"R2.0.9.S04\">\n" +
    "    <SENDER>\n" +
    "        <IDENTIFICATION>\n" +
    "            <ESENDER_LOGIN>ATEXOREL</ESENDER_LOGIN>\n" +
    "            <NO_DOC_EXT>2022-507134</NO_DOC_EXT>\n" +
    "        </IDENTIFICATION>\n" +
    "        <CONTACT>\n" +
    "            <ORGANISATION>Organisme de démonstration ATEXO</ORGANISATION>\n" +
    "            <COUNTRY VALUE=\"EU\"/>\n" +
    "                                    <PHONE>+33 123456789</PHONE>\n" +
    "                                        <E_MAIL>11362@atexo.com</E_MAIL>\n" +
    "                            </CONTACT>\n" +
    "    </SENDER>\n" +
    "    <FORM_SECTION>\n" +
    "        <F05_2014 CATEGORY=\"ORIGINAL\" FORM=\"F05\" LG=\"FR\">\n" +
    "            <LEGAL_BASIS VALUE=\"32014L0025\"/>\n" +
    "            <CONTRACTING_BODY>\n" +
    "                                <ADDRESS_CONTRACTING_BODY>\n" +
    "                                <OFFICIALNAME>Atexo</OFFICIALNAME>\n" +
    "\n" +
    "    <SIRET>21690123103734</SIRET>\n" +
    "\n" +
    "<ADDRESS>63 Boulevard Haussman</ADDRESS>\n" +
    "<TOWN>Paris</TOWN>\n" +
    "<POSTAL_CODE>75008</POSTAL_CODE>\n" +
    "<COUNTRY VALUE=\"FR\"/>\n" +
    "<CONTACT_POINT>Quentin Bagot</CONTACT_POINT>\n" +
    "\n" +
    "    <PHONE>+33 123456789</PHONE>\n" +
    "    <E_MAIL>11362@atexo.com</E_MAIL>\n" +
    "\n" +
    "                                                            <n2016:NUTS CODE=\"FRI13\"/>\n" +
    "                                                                                                                                                                                                                                                                                                                                                                                                                                        <URL_GENERAL>http://atexo.com</URL_GENERAL>\n" +
    "<URL_BUYER>https://mpe-release.local-trust.com/</URL_BUYER>\n" +
    "                </ADDRESS_CONTRACTING_BODY>\n" +
    "                <DOCUMENT_FULL/>\n" +
    "                <URL_DOCUMENT>https://mpe-release.local-trust.com//index.php?page=Entreprise.EntrepriseDetailsConsultation&amp;id=507134&amp;orgAcronyme=pmi-min-1</URL_DOCUMENT>\n" +
    "                <ADDRESS_FURTHER_INFO>\n" +
    "                                <OFFICIALNAME>Atexo</OFFICIALNAME>\n" +
    "\n" +
    "    <SIRET>21690123103734</SIRET>\n" +
    "\n" +
    "<ADDRESS>63 Boulevard Haussman</ADDRESS>\n" +
    "<TOWN>Paris</TOWN>\n" +
    "<POSTAL_CODE>75008</POSTAL_CODE>\n" +
    "<COUNTRY VALUE=\"FR\"/>\n" +
    "\n" +
    "    <PHONE>+33 123456789</PHONE>\n" +
    "    <E_MAIL>11362@atexo.com</E_MAIL>\n" +
    "\n" +
    "                                                            <n2016:NUTS CODE=\"FRI13\"/>\n" +
    "                                                                                                                                                                                                                                                                                                                                                                                                                                        <URL_GENERAL>http://atexo.com</URL_GENERAL>\n" +
    "                </ADDRESS_FURTHER_INFO>\n" +
    "                                <URL_PARTICIPATION>https://mpe-release.local-trust.com//index.php?page=Entreprise.EntrepriseDetailsConsultation&amp;id=507134&amp;orgAcronyme=pmi-min-1</URL_PARTICIPATION>\n" +
    "                <ADDRESS_PARTICIPATION_IDEM/>\n" +
    "                <CE_ACTIVITY VALUE=\"ELECTRICITY\"/>\n" +
    "            </CONTRACTING_BODY>\n" +
    "            <OBJECT_CONTRACT>\n" +
    "                <TITLE>\n" +
    "                    <P>KatalonTNR-68105_20220912_0001</P>\n" +
    "                </TITLE>\n" +
    "                <REFERENCE_NUMBER>KatalonTNR-68105_20220912_0001</REFERENCE_NUMBER>\n" +
    "                                                            <NO_GROUPEMENT/>\n" +
    "                                                    <CPV_MAIN>\n" +
    "                                        <CPV_CODE CODE=\"45000000\"/>\n" +
    "                </CPV_MAIN>\n" +
    "                                <LIEU_EXECUTION>40</LIEU_EXECUTION>\n" +
    "                                                    <TYPE_CONTRACT CTYPE=\"WORKS\"/>\n" +
    "                                                    <SHORT_DESCR>\n" +
    "                        <P>KatalonTNR-68105_20220912_0001</P>\n" +
    "                    </SHORT_DESCR>\n" +
    "                                                    <VAL_ESTIMATED_TOTAL\n" +
    "                            CURRENCY=\"EUR\">10000</VAL_ESTIMATED_TOTAL>\n" +
    "                                                    <NO_LOT_DIVISION />\n" +
    "                                                                                                                                        <DURATION TYPE=\"MONTH\">6</DURATION>\n" +
    "            \n" +
    "                        <OBJECT_DESCR ITEM=\"1\">\n" +
    "    <TITLE>\n" +
    "                    <P>KatalonTNR-68105_20220912_0001</P>\n" +
    "            </TITLE>\n" +
    "        <CPV_ADDITIONAL>\n" +
    "        <CPV_CODE CODE=\"45000000\"/>\n" +
    "    </CPV_ADDITIONAL>\n" +
    "                                        <n2016:NUTS CODE=\"FRI13\"/>\n" +
    "                                                <n2016:NUTS CODE=\"FR101\"/>\n" +
    "                                                <n2016:NUTS CODE=\"FR102\"/>\n" +
    "                                                <n2016:NUTS CODE=\"FR103\"/>\n" +
    "                                                <n2016:NUTS CODE=\"FR104\"/>\n" +
    "                                                <n2016:NUTS CODE=\"FR105\"/>\n" +
    "                                                <n2016:NUTS CODE=\"FR106\"/>\n" +
    "                                                <n2016:NUTS CODE=\"FR107\"/>\n" +
    "                                                <n2016:NUTS CODE=\"FR108\"/>\n" +
    "                            <SHORT_DESCR>\n" +
    "                    <P>KatalonTNR-68105_20220912_0001</P>\n" +
    "            </SHORT_DESCR>\n" +
    "        <AC>\n" +
    "            <AC_COST>\n" +
    "            <AC_CRITERION>coût</AC_CRITERION>\n" +
    "            <AC_WEIGHTING>100%</AC_WEIGHTING>\n" +
    "        </AC_COST>\n" +
    "    </AC>\n" +
    "                                <DURATION TYPE=\"MONTH\">6</DURATION>\n" +
    "            \n" +
    "    <NO_RENEWAL/>\n" +
    "            <NO_ACCEPTED_VARIANTS/>\n" +
    "        <NO_OPTIONS/>\n" +
    "\n" +
    "\n" +
    "            <NO_EU_PROGR_RELATED />\n" +
    "    </OBJECT_DESCR>\n" +
    "                                                </OBJECT_CONTRACT>\n" +
    "\n" +
    "                            <LEFTI>\n" +
    "                    <LEGAL_FORM>\n" +
    "                        <P>Forme requise en cas de groupement : Solidaire</P>\n" +
    "                    </LEGAL_FORM>\n" +
    "                </LEFTI>\n" +
    "                            <PROCEDURE>\n" +
    "                    \n" +
    "                    <PT_01_AOO/>\n" +
    "\n" +
    "                                                                        <NO_CONTRACT_COVERED_GPA/>\n" +
    "                                            \n" +
    "                                            <TECHNIQUE_ACHAT>Directe</TECHNIQUE_ACHAT>\n" +
    "                                        <DATE_RECEIPT_TENDERS>2030-01-20</DATE_RECEIPT_TENDERS>\n" +
    "                    <TIME_RECEIPT_TENDERS>12:00</TIME_RECEIPT_TENDERS>\n" +
    "\n" +
    "                                        <DATE_ONLINE_TENDERS>2023-04-20</DATE_ONLINE_TENDERS>\n" +
    "                    <TIME_ONLINE_TENDERS>12:00</TIME_ONLINE_TENDERS>\n" +
    "\n" +
    "                                                                \n" +
    "                    <LANGUAGES>\n" +
    "                        <LANGUAGE VALUE=\"FR\"/>\n" +
    "                    </LANGUAGES>\n" +
    "                    <CONDITIONS>\n" +
    "                                                    <ACTIVITE_PROFESSIONEL>Conditions énoncées dans les documents de la consultation</ACTIVITE_PROFESSIONEL>\n" +
    "                                                                            <ECONOMIQUE_FINANCIERE>Conditions énoncées dans les documents de la consultation</ECONOMIQUE_FINANCIERE>\n" +
    "                                                                            <TECHNIQUES_PROFESSIONELS>Conditions énoncées dans les documents de la consultation</TECHNIQUES_PROFESSIONELS>\n" +
    "                                            </CONDITIONS>\n" +
    "                                            <DURATION_TENDER_VALID TYPE=\"MONTH\">6</DURATION_TENDER_VALID>\n" +
    "                                                    <NO_VISITE_OBLIGATOIRE/>\n" +
    "                        \n" +
    "                                                    <ATTRIB_SANS_NEGO />\n" +
    "                        \n" +
    "                                                    <CATALOGUE_ELECTRONIQUE>\n" +
    "                                                                Autorisée\n" +
    "                            </CATALOGUE_ELECTRONIQUE>\n" +
    "                                                            </PROCEDURE>\n" +
    "            <COMPLEMENTARY_INFO>\n" +
    "                <NO_RECURRENT_PROCUREMENT/>\n" +
    "                <INFO_ADD>\n" +
    "    <P></P><P>Critères sociaux et environnementaux : </P><P>La consultation intègre des critères sociaux : Non</P><P>La consultation intègre des critères environnementaux : Non</P>\n" +
    "</INFO_ADD>\n" +
    "                <ADDRESS_REVIEW_BODY>\n" +
    "                                                                    <OFFICIALNAME>Atexo</OFFICIALNAME>\n" +
    "\n" +
    "    <SIRET>21690123103734</SIRET>\n" +
    "\n" +
    "<ADDRESS>63 Boulevard Haussman</ADDRESS>\n" +
    "<TOWN>Paris</TOWN>\n" +
    "<POSTAL_CODE>75008</POSTAL_CODE>\n" +
    "<COUNTRY VALUE=\"FR\"/>\n" +
    "\n" +
    "\n" +
    "<URL_GENERAL>http://atexo.com</URL_GENERAL>\n" +
    "                                    </ADDRESS_REVIEW_BODY>\n" +
    "                <DATE_DISPATCH_NOTICE>\n" +
    "                    2022-09-12\n" +
    "                </DATE_DISPATCH_NOTICE>\n" +
    "            </COMPLEMENTARY_INFO>\n" +
    "        </F05_2014>\n" +
    "    </FORM_SECTION>\n" +
    "</TED_ESENDERS>\n";


export default xml
