import Vue from 'vue';
import Router from 'vue-router';
import store from '../store';

const ifAuthenticated = (to: any,
                         from: any,
                         next: any) => {
    if (store.getters['auth/isLoggedIn'] || store.getters['auth/authStatus'] === 'loading') {
        next();
        return;
    }
    next('/login?redirectUrl=' + to.path);
};

Vue.use(Router);

const router = new Router({
    mode: 'history', //removes # (hashtag) from url
    base: '/administration',
    fallback: true, //router should fallback to hash (#) mode when the browser does not support history.pushState

    routes: [
        {
            path: '/',
            redirect: {name: 'dashboard'}
        },
        {
            path: '/simulateur-wc',
            name: 'simulateur-wc',
            beforeEnter: ifAuthenticated,
            component: () => import('/src/views/SimulateurWc.vue')
        },
        {
            path: '/simulateur-validation',
            name: 'simulateur-validation',
            beforeEnter: ifAuthenticated,
            component: () => import('/src/views/SimulateurValidation.vue')
        },
        {
            path: '/simulateur-atexo-configuration',
            name: 'simulateur-atexo-configuration',
            beforeEnter: ifAuthenticated,
            component: () => import('/src/views/AtexoConfiguration.vue')
        },
        {
            path: '/simulateur-plateformes-configuration',
            name: 'simulateur-plateformes-configuration',
            beforeEnter: ifAuthenticated,
            component: () => import('/src/views/PlateformesConfiguration.vue')
        },
        {
            path: '/simulateur-validation-wc',
            name: 'simulateur-validation-wc',
            beforeEnter: ifAuthenticated,
            component: () => import('/src/views/SimulateurValidationWc.vue')
        },
        {
            path: '/simulateur',
            name: 'simulateur',
            beforeEnter: ifAuthenticated,
            component: () => import('/src/views/Simulateur.vue')
        },
        {
            path: '/add-configuration',
            name: 'add-configuration',
            beforeEnter: ifAuthenticated,
            component: () => import('/src/views/ConfigurationPlateforme.vue')
        },{
            path: '/dashboard-configuration',
            name: 'dashboard-configuration',
            beforeEnter: ifAuthenticated,
            component: () => import('/src/views/DashboardConfigurationPlateforme.vue')
        },
        {
            path: '/modify-configuration/:id',
            name: 'modify-configuration',
            beforeEnter: ifAuthenticated,
            component: () => import('/src/views/ConfigurationPlateforme.vue')
        },
        {
            path: '/formulaire',
            name: 'formulaire',
            beforeEnter: ifAuthenticated,
            component: () => import('/src/views/ConsultationFormulaire.vue')
        },
        {
            path: '/annonce-recap',
            name: 'annonce-recap',
            beforeEnter: ifAuthenticated,
            component: () => import('/src/views/AnnonceRecap.vue')
        },
        {
            path: '/annonce-suivi',
            name: 'annonce-suivi',
            beforeEnter: ifAuthenticated,
            component: () => import('/src/views/AnnonceSuivi.vue')
        },
        {
            path: '/dashboard',
            beforeEnter: ifAuthenticated,
            name: 'dashboard',
            component: () => import('/src/views/Home.vue')
        },
        {
            path: '/swagger',
            beforeEnter: ifAuthenticated,
            name: 'swagger',
            component: () => import('/src/views/Swagger.vue')
        },
        {
            path: '/logs',
            beforeEnter: ifAuthenticated,
            name: 'logs',
            component: () => import('/src/views/Logs.vue')
        },
        {
            path: '/bcrypt',
            beforeEnter: ifAuthenticated,
            name: 'bcrypt',
            component: () => import('/src/views/bcrypt.vue')
        },
        {
            path: '/login',
            name: 'login',
            component: () => import('/src/views/Login.vue')
        }
    ],
    scrollBehavior() {
        return {x: 0, y: 0};
    }
});

export default router;
