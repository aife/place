import Vue from 'vue'

import Vuex from 'vuex'
import {configuration} from "./configuration.module"
import {monitoring} from "./monitoring.module"
import {auth} from "./auth.module"
import {mockAgent} from "./mock-agent.module"

Vue.use(Vuex)

export default new Vuex.Store({

    modules: {monitoring,auth,mockAgent,configuration}
})
