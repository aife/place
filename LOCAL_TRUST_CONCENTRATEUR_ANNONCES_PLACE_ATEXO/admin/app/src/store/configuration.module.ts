import ConfigurationService from "../services/configuration-service";

export const configuration = {
    namespaced: true,
    state: {},
    mutations: {},
    actions: {
        getToken(context: any, config: { basicToken: string, idPlateform: string }) {
            return ConfigurationService.getToken(config.basicToken, config.idPlateform).then(
                (response: any) => {

                    return Promise.resolve(response);
                },
                (error: any) => {

                    return Promise.reject(error);
                }
            );
        }, getAllSupports(context: any) {
            return ConfigurationService.getAllSupports().then(
                (response: any) => {

                    return Promise.resolve(response);
                },
                (error: any) => {

                    return Promise.reject(error);
                }
            );
        },
        getAllConfiguration(context: any) {
            return ConfigurationService.getAllConfiguration().then(
                (response: any) => {

                    return Promise.resolve(response);
                },
                (error: any) => {

                    return Promise.reject(error);
                }
            );
        },
        addOrUpdate(context: any, configuration: any) {
            return ConfigurationService.addOrUpdate(configuration).then(
                (response: any) => {
                    return Promise.resolve(response);
                },
                (error: any) => {

                    return Promise.reject(error);
                }
            )
        },
        delete(context: any, id: number) {
            return ConfigurationService.delete(id).then(
                (response: any) => {
                    return Promise.resolve(response);
                },
                (error: any) => {

                    return Promise.reject(error);
                }
            )
        },
        getById(context: any, id: number) {
            return ConfigurationService.getById(id).then(
                (response: any) => {
                    return Promise.resolve(response);
                },
                (error: any) => {

                    return Promise.reject(error);
                }
            )
        }
    }


}
