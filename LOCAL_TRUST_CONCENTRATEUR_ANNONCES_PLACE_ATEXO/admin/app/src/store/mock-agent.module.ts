import MockAgentService from "@/services/mockAgent.service";
import AgentModel from "@/models/agent-model";
import AgentContexteModel from "@/models/agentContexte-model";


export const mockAgent = {
    state: {
        urlAgent: '',
        tokenAgent: '',
        idContexte: '',
        agent:{}
    },
    mutations: {
        loginContexteSuccess(state: any, agent: AgentModel) {
            state.urlAgent = agent.urlAgentApi
            state.tokenAgent = agent.token
            state.idContexte = agent.idContexte
        },
        defaultAgentSuccess(state: any, agt: AgentContexteModel) {
            state.agent = agt
        }
    },
    actions: {
        createConnexionContexte(context: any, agent: AgentContexteModel) {
            return MockAgentService.createConnexionContexte(agent).then(
                (response: { data: AgentModel }) => {
                    context.commit('loginContexteSuccess',response.data);
                    return Promise.resolve(response.data);
                },
                (error: any) => {
                    return Promise.reject(error);
                }
            );
        },
        getInfosAgent(context: any) {
            return MockAgentService.getDefaultAgent().then(
                (response: { data: AgentContexteModel })=> {
                    context.commit('defaultAgentSuccess', response.data)
                },
                (error: any) => {
                    return Promise.reject(error);
                }
            );
        },
        getToken(context: any) {
            return MockAgentService.getToken().then(
                (response: { data: string }) => {
                    return Promise.resolve(response.data);
                },
                (error: any) => {
                    return Promise.reject(error);
                }
            );
        }
    },
    getters: {
        getAgent: (state: any) => state.agent,
        getUrlAgent: (state: any) => state.urlAgent,
        getTokenAgent: (state: any) => state.tokenAgent,
        getIdContexte: (state: any) => state.idContexte
    }
}
