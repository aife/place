export default class AgentContexteModel {

    constructor(public tokenMpe: string, public mpeUrl: string, public lang: string,
                public organisme: string, public plateforme: string, public idNotice: string,
                public idConsultation: number
    ) {
    }
}
