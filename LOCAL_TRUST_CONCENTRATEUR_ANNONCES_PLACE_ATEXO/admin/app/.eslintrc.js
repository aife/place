module.exports = {
    root: true,
    env: {
       node: true
    },
    'extends': [
       'plugin:vue/essential',
       'eslint:recommended',
       '@vue/typescript'
     ],
    rules: {
       'no-undef': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
       'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
       'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off'
    },
    parserOptions: {
       parser: '@typescript-eslint/parser'
    }
 }