package com.atexo.connecteur.batch.protocole.themis;

import com.atexo.connecteur.batch.MainProcessor;
import com.atexo.connecteur.commun.config.ConfigurationServiceThemisMarseille;
import fr.atexo.rsem.noyau.ws.beans.ContratBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.AcquitementMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.ListDocumentsMiseAdispositionBean;
import org.apache.http.entity.mime.content.StringBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author KBE
 */
@Service
@Profile("themis")
public class ThemisAlfrescoSend implements MainProcessor {
    private static final String NAME_SPACE = "{http://wwww.mairie-marseille.fr/model}";
    private static final String DUPLICATE_CHILD = "Duplicate child name not allowed";
    private static final String DUPLICATE_CHILD_1 = "Duplicate entry";
    private static final String DUPLICATE_CHILD_2 = "duplicate key value violates unique constraint";
    private static Logger logger = LoggerFactory.getLogger(ThemisAlfrescoSend.class);
    @Autowired
    ConfigurationServiceThemisMarseille config;
    private StringBody applicationId;
    private StringBody actionId;
    private Map<String, String> procedureMap;
    private Map<String, String> typeDocumentMap;
    private String hostname;
    private String protocole;
    private int port;
    private String uri;

    @Override
    public boolean needsContrat() {
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.atexo.connecteur.dispatcher.modules.ProtocoleDispatcherService#EnvoyerFichier
     * (java.io.File)
     */
    @Override
    public AcquitementMiseAdispositionBean acquitter(ListDocumentsMiseAdispositionBean documentsList, ContratBean contrat) {

        /*String url = config.getUrl();
        String password = config.getAlfrescoThemisPassword();
        String login = config.getAlfrescoThemisLogin();
        applicationId = config.getAlfrescoThemisapplicationId();
        actionId = config.getAlfrescoThemisActionId();
        procedureMap = config.getThemisProcedureMap();
        typeDocumentMap = config.getTypeDocumentMap();
        populateHostProperties();

        AcquitementMiseAdispositionBean acquitementDto = null;
        logger.info("Nombre de docuements a traiter pour cette mise a disposition : " + documentsList.getDocumentsList().size());
        for (DocumentMiseAdispositionBean document : documentsList.getDocumentsList()) {
            HttpHost targetHost = new HttpHost(hostname, port, protocole);
            DefaultHttpClient httpclient = new DefaultHttpClient();
            try {
                httpclient.getCredentialsProvider().setCredentials(new AuthScope(targetHost.getHostName(), targetHost.getPort()), new UsernamePasswordCredentials(login, password));

                // Create AuthCache instance
                AuthCache authCache = new BasicAuthCache();
                // Generate BASIC scheme object and add it to the local
                // auth cache
                BasicScheme basicAuth = new BasicScheme();
                authCache.put(targetHost, basicAuth);

                // Add AuthCache to the execution wsClient
                BasicHttpContext localcontext = new BasicHttpContext();
                localcontext.setAttribute(ClientContext.AUTH_CACHE, authCache);

                HttpPost httppost = new HttpPost(uri);
                httppost.setHeader("accept-charset", "UTF-8");
                MimetypesFileTypeMap mime = new MimetypesFileTypeMap();
                Collection mimes = MimeUtil.getMimeTypes(document.getNomDeFichier());
                Iterator it = mimes.iterator();
                FileBody fichier = new FileBody(document.getPathFichier(), document.getNomDeFichier(), it.next().toString(), null);
                MultipartEntity reqEntity = new MultipartEntity();
                reqEntity.addPart("filedata", fichier);
                reqEntity.addPart("applicationId", applicationId);
                reqEntity.addPart("actionId", actionId);
                StringBody pathProperties = getPathProperties(document.getStatut(), documentsList);
                if (pathProperties == null) {
                    acquitementDto = new AcquitementMiseAdispositionBean();
                    acquitementDto.setStatut(StatutMiseAdispositionEnum.ECHOUE.name());
                    logger.error("Erreur de connexion avec Alfresco. le parametre pathproperties est null");

                }
                reqEntity.addPart("pathproperties", pathProperties);
                //reqEntity.addPart("aspects", new StringBody("{aspects:[\"pieceMarche\",\"documentConsultation\"]}"));

                StringBody fileProperties = getFileProperties(documentsList, document);
                if (fileProperties == null) {
                    acquitementDto = new AcquitementMiseAdispositionBean();
                    acquitementDto.setStatut(StatutMiseAdispositionEnum.ECHOUE.name());
                    logger.error("Erreur de connexion avec Alfresco. le parametre fileProperties est null");

                }
                reqEntity.addPart("fileProperties", fileProperties);


                httppost.setEntity(reqEntity);
                httppost.setHeader("Accept-Charset", "UTF-8");
                HttpResponse response = httpclient.execute(targetHost, httppost, localcontext);
                logger.info("requete Alfresco : " + httpclient.toString());
                HttpEntity resEntity = response.getEntity();
                String resultat = null;
                if (resEntity != null) {
                    resultat = IOUtils.toString(resEntity.getContent());
                }
                if (resultat.contains(DUPLICATE_CHILD.subSequence(0, DUPLICATE_CHILD.length())) || resultat.contains(DUPLICATE_CHILD_1.subSequence(0, DUPLICATE_CHILD_1.length())) || resultat.contains(DUPLICATE_CHILD_2.subSequence(0, DUPLICATE_CHILD_2.length()))) {
                    String name = document.getNomDeFichier();
                    DateFormat format = new SimpleDateFormat("dd-MM-yyyy_HH_mm_ss");
                    name = FilenameUtils.getBaseName(document.getNomDeFichier()) + "_" + format.format(new Date()) + '.' + FilenameUtils.getExtension(name);
                    document.setNomDeFichier(name);
                    // fixme pourquoi on fait ça ? => i--;
                    continue;
                } else {
                    acquitementDto = valideResultat(resultat);
                    document.getPathFichier().delete();
                }
                logger.info("resultat Alfresco : " + resultat);
            } catch (IOException e) {
                acquitementDto = new AcquitementMiseAdispositionBean();
                acquitementDto.setStatut(StatutMiseAdispositionEnum.ECHOUE.name());
                logger.error("Erreur de connexion avec Alfresco", e.fillInStackTrace());
            } finally {
                try {
                    httpclient.getConnectionManager().shutdown();
                } catch (Exception ignore) {

                }
            }
            acquitementDto.setMiseAdisposition(documentsList.getMiseAdispositionId());
        }

        return acquitementDto;
    }

    private void populateHostProperties() {
        String url = config.getUrl();
        if (url != null) {
            protocole = StringUtils.substring(url, 0, url.indexOf(":"));
            if (url.indexOf(":", 7) != -1) {
                hostname = StringUtils.substring(url, url.indexOf(":") + 3, url.indexOf(":", 7));
                port = Integer.parseInt(StringUtils.substring(url, url.indexOf(":", 7) + 1, url.indexOf("/", url.indexOf(":", 7))));
                uri = StringUtils.substring(url, url.indexOf("/", url.indexOf(":", 7)));
            } else {
                port = 80;
                hostname = StringUtils.substring(url, url.indexOf(":") + 3, url.indexOf("/", 7));
                uri = StringUtils.substring(url, url.indexOf("/", url.indexOf("/", 7)));
            }
        }
    }

    private StringBody getPathProperties(String status, ListDocumentsMiseAdispositionBean documentsList) {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        StringBody body = null;
        ContratMiseAdispositionBean contrat = documentsList.getMiseADispositionContrat();
        ConsultationMiseAdispositionBean consultation = documentsList.getMiseADispositionConsultation();
        try {
            List<String> listeAnnee = new ArrayList<>();
            listeAnnee.add("[ANNEE NOTIFICATION]");
            if (contrat.getDateNotification() != null) {
                DateFormat dateFormat = new SimpleDateFormat("yyyy");
                listeAnnee.add(dateFormat.format(contrat.getDateNotification()));
            } else {
                listeAnnee.add("NC");
            }

            jsonArray.put(0, listeAnnee);
            List<String> listeNature = new ArrayList<>();
            listeNature.add("[NATURE]");
            listeNature.add(StringUtils.capitalize(consultation.getNaturePrestation().toLowerCase()));
            jsonArray.put(1, listeNature);
            List<String> listeMarche = new ArrayList<>();
            listeMarche.add("[NO MARCHE]");
            if (contrat.getNumero() != null) {
                listeMarche.add(contrat.getNumero());
            } else {
                listeMarche.add("NC");
            }
            jsonArray.put(2, listeMarche);
            List<String> listeEtape = new ArrayList<>();
            if (consultation.getStatut() != null) {
                listeEtape.add("[ETAPE]");
                if (status == null) {
                    listeEtape.add(consultation.getStatut());
                } else {
                    listeEtape.add(status);
                }
            } else {
                listeEtape.add("[ETAPE]");
                listeEtape.add(contrat.getNumero());
            }
            jsonArray.put(3, listeEtape);

            jsonObject.put("pathProperties", jsonArray);
            logger.info(jsonObject.toString());
            body = new StringBody(jsonObject.toString(), Charset.forName("UTF-8"));
        } catch (JSONException | UnsupportedEncodingException ignored) {
        }
        return body;

    }

    private StringBody getFileProperties(ListDocumentsMiseAdispositionBean documentsList, DocumentMiseAdispositionBean document) {
        ConsultationMiseAdispositionBean consultation = documentsList.getMiseADispositionConsultation();
        ContratMiseAdispositionBean contrat = documentsList.getMiseADispositionContrat();
        JSONObject jsonObject = new JSONObject();
        StringBody body = null;
        try {
            JSONArray jsonArray = new JSONArray();
            jsonObject.put("fileProperties", jsonArray);
            List<String> liste = new ArrayList<>();
            liste.add("{http://www.alfresco.org/model/content/1.0}title");
            //liste.add(document.getNomDeFichier());
            //Demande de V-Marseille
            if (typeDocumentMap.get(document.getType()) != null) {
                liste.add(typeDocumentMap.get(document.getType()));
            } else {
                liste.add("Autre");
            }
            jsonArray.put(liste);
            //Inutile pour la nouvelle version
            *//*liste = new ArrayList<String>();
            liste.add("{http://www.alfresco.org/model/content/1.0}name");
            liste.add(document.getNomDeFichier());
            jsonArray.put(liste);*//*
            liste = new ArrayList<>();
            liste.add(NAME_SPACE + "nomDocumentMarche");
            //Si le type n'est pas défini alors nous settons à Autre.
            //Il faut passer pas la map typeDocuementMap pour changer les caracteres des types
            if (typeDocumentMap.get(document.getType()) != null) {
                liste.add(typeDocumentMap.get(document.getType()));
            } else {
                liste.add("Autre");
            }
            jsonArray.put(liste);
            liste = new ArrayList<>();
            liste.add(NAME_SPACE + "objetConsultation");
            liste.add(consultation.getIntitule());
            jsonArray.put(liste);
            liste = new ArrayList<>();
            liste.add(NAME_SPACE + "numeroConsultation");
            liste.add(consultation.getReference());
            jsonArray.put(liste);
            liste = new ArrayList<>();
            liste.add(NAME_SPACE + "naturePrestation");
            liste.add(StringUtils.capitalize(consultation.getNaturePrestation().toLowerCase()));
            jsonArray.put(liste);
            if (procedureMap != null && procedureMap.get(consultation.getCodeProcedure()) != null) {
                liste = new ArrayList<>();
                liste.add(NAME_SPACE + "procedureMarche");
                liste.add(procedureMap.get(consultation.getCodeProcedure()).trim());
                jsonArray.put(liste);
            }
            liste = new ArrayList<>();
            liste.add(NAME_SPACE + "responsableMarche");
            StringBuilder direction = new StringBuilder(consultation.getLibelleDirectionResponsable());
            liste.add(direction.toString());
            jsonArray.put(liste);
            liste = new ArrayList<>();
            liste.add(NAME_SPACE + "beneficiaireMarche");
            direction = new StringBuilder(consultation.getLibelleDirectionBeneficaire());
            liste.add(direction.toString());
            jsonArray.put(liste);
            if (contrat != null) {
                liste = new ArrayList<>();
                liste.add(NAME_SPACE + "numeroMarche");
                liste.add(contrat.getNumero().trim());
                jsonArray.put(liste);
                liste = new ArrayList<>();
                liste.add(NAME_SPACE + "objetMarche");
                liste.add(contrat.getObjet());
                jsonArray.put(liste);
                liste = new ArrayList<>();
                liste.add(NAME_SPACE + "titulaireMarche");
                liste.add(contrat.getTitulaire());
                jsonArray.put(liste);
                if (contrat.getDateNotification() != null) {
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
                    liste = new ArrayList<>();
                    liste.add(NAME_SPACE + "dateNotificationMarche");
                    liste.add(dateFormat.format(contrat.getDateNotification()));
                    jsonArray.put(liste);
                }
            }
            logger.info(jsonObject.toString());
            body = new StringBody(jsonObject.toString(), Charset.forName("UTF-8"));
        } catch (UnsupportedEncodingException | JSONException e) {
        }
        return body;

    }

    public void setApplicationId(String applicationId) {
        try {
            this.applicationId = new StringBody(applicationId);
        } catch (UnsupportedEncodingException e) {
            logger.error("Erreur encoding de parametre (applicationId) avec la requete Alfresco " + applicationId, e.fillInStackTrace());
        }
    }

    public void setActionId(String actionId) {
        try {
            this.actionId = new StringBody(actionId);
        } catch (UnsupportedEncodingException e) {
            logger.error("Erreur encoding de parametre (actionId) avec la requete Alfresco " + actionId, e.fillInStackTrace());

        }
    }

    public void setProcedureMap(Map<String, String> procedureMap) {
        this.procedureMap = procedureMap;
    }

    public void setTypeDocumentMap(Map<String, String> typeDocumentMap) {
        this.typeDocumentMap = typeDocumentMap;
    }

    private AcquitementMiseAdispositionBean valideResultat(String resultat) {
        AcquitementMiseAdispositionBean acquitementDto = new AcquitementMiseAdispositionBean();
        try {
            if (resultat != null && !resultat.isEmpty()) {
                org.json.JSONObject jsonObject = new JSONObject(resultat);
                JSONObject uploadTag = jsonObject.getJSONObject("upload");
                if (uploadTag != null) {
                    JSONObject statusTag = uploadTag.getJSONObject("status");
                    if (statusTag != null) {
                        if (statusTag.getInt("code") == 200) {
                            acquitementDto.setStatut(StatutMiseAdispositionEnum.ACQUITTE.name());
                            return acquitementDto;
                        }
                    }
                }
            }
        } catch (JSONException e) {
            logger.error("Le flux Json renvoye par Alfresco est incorrect " + resultat, e.fillInStackTrace());
        }
        acquitementDto.setStatut(StatutMiseAdispositionEnum.ECHOUE.name());*/
        return null;
    }

    public String isOk() {
        /*StringBuilder resultat = new StringBuilder("<b>ALFRESCO</b>");
        resultat.append("<br/>");
        resultat.append("hostname : ").append(hostname);
        resultat.append("<br/>");
        resultat.append("port     : ").append(port);
        resultat.append("<br/>");
        resultat.append("protocole: ").append(protocole);
        resultat.append("<br/>");
        resultat.append("statut: ");
        HttpHost targetHost = new HttpHost(hostname, port, protocole);
        DefaultHttpClient httpclient = new DefaultHttpClient();
        httpclient.getCredentialsProvider().setCredentials(new AuthScope(targetHost.getHostName(), targetHost.getPort()), new UsernamePasswordCredentials(config.getAlfrescoThemisLogin(), config.getAlfrescoThemisPassword()));

        // Create AuthCache instance
        AuthCache authCache = new BasicAuthCache();
        // Generate BASIC scheme object and add it to the local
        // auth cache
        BasicScheme basicAuth = new BasicScheme();
        authCache.put(targetHost, basicAuth);

        // Add AuthCache to the execution wsClient
        BasicHttpContext localcontext = new BasicHttpContext();
        localcontext.setAttribute(ClientContext.AUTH_CACHE, authCache);

        HttpPost httppost = new HttpPost(uri);
        MultipartEntity reqEntity = new MultipartEntity();
        reqEntity.addPart("applicationId", applicationId);
        reqEntity.addPart("actionId", actionId);

        httppost.setEntity(reqEntity);

        HttpResponse response;
        try {
            response = httpclient.execute(targetHost, httppost, localcontext);
            HttpEntity resEntity = response.getEntity();
            if (resEntity != null) {
                String tmp = IOUtils.toString(resEntity.getContent());
                if (StringUtils.contains(tmp.subSequence(0, tmp.length()), "\"name\": \"Mauvaise requ&ecirc;te\"")) {
                    resultat.append("Connection OK");
                } else {
                    resultat.append(IOUtils.toString(resEntity.getContent()));
                }
            }
        } catch (IOException e) {
            resultat.append(e.getCause().toString());
        }
        return resultat.toString();*/
        return null;
    }

}
