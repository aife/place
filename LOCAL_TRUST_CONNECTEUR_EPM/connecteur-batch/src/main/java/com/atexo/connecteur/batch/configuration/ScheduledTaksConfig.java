package com.atexo.connecteur.batch.configuration;

import com.atexo.connecteur.batch.MainBatchLauncher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDateTime;

@EnableScheduling
@Configuration
public class ScheduledTaksConfig {
    private static Logger logger = LoggerFactory.getLogger(ScheduledTaksConfig.class);
    @Autowired
    MainBatchLauncher mainBatchLauncher;

    @Scheduled(cron = "${batch.cronExpression}")
    private void launchJob() {
        logger.info("Lancement du batch => {}", LocalDateTime.now());
        mainBatchLauncher.lancerBatch();
        logger.info("Fin du batch => {}", LocalDateTime.now());
    }
}
