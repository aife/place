package com.atexo.connecteur.batch.protocole.pastell;

import com.atexo.connecteur.commun.config.ConfigurationServicePastell;
import com.atexo.connecteur.rsemwsclient.RsemWsClientFacade;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.AcquitementDocumentBean;
import fr.paris.epm.noyau.rest.impl.RsemWsClientUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.zeroturnaround.zip.ZipUtil;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
@Profile("pastell")
public class ProtocolePastellRSEMSender {
    private static Logger logger = LoggerFactory.getLogger(ProtocolePastellRSEMSender.class);
    @Autowired
    ConfigurationServicePastell config;

    @Autowired
    RsemWsClientFacade facade;

    @Scheduled(cron = "${batch.cronExpression}")
    private void process() {
        File infFolder = new File(config.getPathOut());
        infFolder.mkdirs();
        if (!infFolder.exists() || !infFolder.isDirectory()) {
            logger.warn("Le répertoire  d'entrée {} n'est pas bien renseigné", infFolder);
            return;
        }

        Arrays.stream(infFolder.listFiles()).filter(file -> file.getName().toLowerCase().endsWith(".zip")).forEach(zipFile -> {
            try {
                File tmpZipFolder = Files.createTempDirectory(zipFile.getName() + "_").toFile();
                ZipUtil.unpack(zipFile, tmpZipFolder);
                List<File> zipFiles = Arrays.asList(tmpZipFolder.listFiles());
                if (zipFiles.size() != 2) {
                    logger.warn("Le zip contient {} fichiers , risque d'upload de documents en double", zipFiles.size());
                }
                Optional<File> metadataFile = zipFiles.stream().filter(f -> f.getName().equalsIgnoreCase(config.getMetaDataFileName())).findFirst();
                Optional<File> jetonSignatureFile = zipFiles.stream().filter(f -> f.getName().toLowerCase().endsWith(config.getJetonSignatureExtension())).findFirst();
                if (!metadataFile.isPresent()) {
                    logger.warn("le fichier {} est introuvable dans le zip {}", config.getMetaDataFileName(), zipFile);
                } else if (jetonSignatureFile.isPresent()) {
                    logger.warn("le fichier {} correspond à un jeton de signature et ne sera pas envoyé à RSEM", jetonSignatureFile);
                } else {
                    // todo à parser
                    AcquitementDocumentBean acquittementDocumentBean = RsemWsClientUtils.parse(new FileInputStream(metadataFile.get()), AcquitementDocumentBean.class);
                    for (File fileToUpload : zipFiles) {
                        // ne pas uploader les metadonnees
                        if (!fileToUpload.getName().equalsIgnoreCase(config.getMetaDataFileName())) {
                            Integer idFichierSurDisqueRSEM = facade.upload(fileToUpload);
                            logger.info("Fichier sur disque bien sauvegardé , ID : {}", idFichierSurDisqueRSEM);
                            if (idFichierSurDisqueRSEM != null && acquittementDocumentBean != null) {
                                Integer idDocument = facade.sauvegarderDocument(idFichierSurDisqueRSEM, acquittementDocumentBean);
                                logger.info("Document bien sauvegardé , ID : {}", idDocument);
                                if (idDocument != null) {
                                    logger.info("Déplacement du zip {} et suppression de {}", zipFile, tmpZipFolder);
                                    Files.move(zipFile.toPath(), Paths.get(config.getPathProcessed(), zipFile.getName()), StandardCopyOption.REPLACE_EXISTING);
                                    if (tmpZipFolder.exists()) FileSystemUtils.deleteRecursively(tmpZipFolder);
                                }
                            }
                        }
                    }
                }
            } catch (IOException | JAXBException e) {
                logger.error("Erreur lor du traitement du fichier {}\n{}", zipFile, e);
            }

        });

    }
}
