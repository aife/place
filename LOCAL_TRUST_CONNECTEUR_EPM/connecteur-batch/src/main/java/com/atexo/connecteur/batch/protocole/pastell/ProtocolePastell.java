package com.atexo.connecteur.batch.protocole.pastell;

import com.atexo.connecteur.batch.MainProcessor;
import com.atexo.connecteur.commun.config.ConfigurationServicePastell;
import com.atexo.connecteur.commun.constant.StatutMiseAdispositionEnum;
import com.atexo.connecteur.commun.xml.PastellGeneratedXml;
import com.atexo.connecteur.mappings.pastell.PastellMapper;
import fr.atexo.rsem.noyau.ws.beans.ContratBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.AcquitementMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.DocumentMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.ListDocumentsMiseAdispositionBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.zeroturnaround.zip.ZipUtil;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by dcs on 10/01/17.
 * for Atexo
 */
@Service
@Profile("pastell")
public class ProtocolePastell implements MainProcessor {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss");
    @Autowired
    ConfigurationServicePastell config;
    private static Logger logger = LoggerFactory.getLogger(ProtocolePastell.class);
    @Autowired
    PastellMapper pastellMapper;

    @Override
    public boolean needsContrat() {
        return true;
    }

    @Override
    public AcquitementMiseAdispositionBean acquitter(ListDocumentsMiseAdispositionBean documentsList, ContratBean contrat) {
        AcquitementMiseAdispositionBean acquittement = new AcquitementMiseAdispositionBean();
        acquittement.setStatut(StatutMiseAdispositionEnum.ACQUITTE.name());
        StringBuilder raisonsEchec = new StringBuilder();
        File pastellEntreeFolderMiseADisposition = new File(config.getPathIn() + documentsList.getMiseAdispositionId() + "/");
        pastellEntreeFolderMiseADisposition.mkdirs();
        // vérificatin de l'existance du dossier fournie par le fichier de config
        if (!pastellEntreeFolderMiseADisposition.isDirectory()) {
            logger.error("{} n'existe pas", pastellEntreeFolderMiseADisposition);
            return null;
        }
        PastellGeneratedXml generateurXml = pastellMapper.toXML(documentsList, contrat);
        File metadataXML = new File(pastellEntreeFolderMiseADisposition.getAbsolutePath() + File.separator + config.getMetaDataFileName());
        // si on retente une nouvelle mise à disposition avec le meme métadonnée fichier , on supprime le fichier existant
        if (metadataXML.exists()) {
            try {
                Files.delete(metadataXML.toPath());
            } catch (IOException e) {
                logger.error("Impossible de supprimer le fichier de métadonnées : {}", metadataXML);
            }
        }
        // Création du contenu du fichier xml
        logger.info("Création du fichier : {}", config.getMetaDataFileName());
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(PastellGeneratedXml.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(generateurXml, metadataXML);

            for (DocumentMiseAdispositionBean document : documentsList.getDocumentsList()) {
                // Création du document
                File newFile = new File(pastellEntreeFolderMiseADisposition.getAbsolutePath() + File.separator + document.getNomDeFichier());
                // si on retente une nouvelle mise à disposition avec le meme nom de fichier , on supprime le fichier existant
                if (newFile.exists()) {
                    Files.delete(newFile.toPath());
                }
                // Copier le fichier récupéré de RSEM dans le systeme de fichier
                logger.info("Création du fichier : {} depuis : {}", newFile.getName(), newFile);
                Files.copy(document.getPathFichier().toPath(), newFile.toPath());

            }
            // création du zip final
            String zipName = "";
            String horodatage = dateTimeFormatter.format(LocalDateTime.now());
            zipName = "Consultation_" + documentsList.getMiseADispositionConsultation().getReference() + "_" + horodatage;
            if (contrat != null) {
                zipName = "Contrat_" + contrat.getNumeroContrat() + "_" + horodatage;
            }
            File zipDestination = new File(config.getPathIn() + zipName + ".zip");
            ZipUtil.pack(pastellEntreeFolderMiseADisposition, zipDestination);
            FileSystemUtils.deleteRecursively(pastellEntreeFolderMiseADisposition);

        } catch (JAXBException | IOException e) {
            logger.error("Erreur lors de la création du fichier des métadonnées", e);
            raisonsEchec.append(e.getMessage());
            acquittement.setStatut(StatutMiseAdispositionEnum.ECHOUE.name());
        }
        acquittement.setMiseAdisposition(documentsList.getMiseAdispositionId());
        return acquittement;
    }
}
