package com.atexo.connecteur.batch;

import com.atexo.connecteur.commun.constant.StatutMiseAdispositionEnum;
import com.atexo.connecteur.rsemwsclient.RsemWsClientFacade;
import fr.atexo.rsem.noyau.ws.beans.ContratBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.AcquitementMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.DocumentMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.ListDocumentsMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.MiseAdispositionBean;
import fr.paris.epm.noyau.rest.exceptions.RsemWsClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.File;
import java.util.List;

@Service
public class MainBatchLauncher {
    private static Logger logger = LoggerFactory.getLogger(MainBatchLauncher.class);
    @Autowired
    private RsemWsClientFacade facade;
    @Autowired
    private MainProcessor mainProcessor;

    public void lancerBatch() {
        List<MiseAdispositionBean> listeMiseADispositions = facade.getMiseADispositionList();
        if (CollectionUtils.isEmpty(listeMiseADispositions)) {
            logger.warn("Il n'y a aucune mise à disposition à traiter");
        } else {
            for (MiseAdispositionBean miseAdispositio : listeMiseADispositions) {
                String id = miseAdispositio.getId();
                ListDocumentsMiseAdispositionBean documentsMiseADisposition = facade.getDocumentsByIdMiseADisposition(miseAdispositio.getId());
                if (documentsMiseADisposition != null) {
                    AcquitementMiseAdispositionBean acquitement = null;
                    ContratBean contrat = null;
                    if (documentsMiseADisposition.getMiseADispositionContrat() != null && mainProcessor.needsContrat()) {
                        try {
                            contrat = facade.getContrat(documentsMiseADisposition.getMiseADispositionContrat().getNumero());
                            acquitement = mainProcessor.acquitter(documentsMiseADisposition, contrat);
                        } catch (RsemWsClientException ex) {
                            acquitement = new AcquitementMiseAdispositionBean(id, StatutMiseAdispositionEnum.ECHOUE.name(), ex.getMessage());
                        }
                    } else {
                        acquitement = mainProcessor.acquitter(documentsMiseADisposition, contrat);
                    }
                    if (acquitement != null) {
                        facade.acquittementMiseAdisposition(id, acquitement);
                    } else {
                        logger.warn("Aucun document disponible pour la mise à dispotion : {}", id);
                    }
                    clean(documentsMiseADisposition);
                }

            }

        }
        facade.deconnexion();
    }

    /**
     * nettoyage des fichiers temporaires
     *
     * @param documentsMiseADisposition liste des documents de la mise à disposition
     */
    private void clean(ListDocumentsMiseAdispositionBean documentsMiseADisposition) {
        for (DocumentMiseAdispositionBean document : documentsMiseADisposition.getDocumentsList()) {
            File file = document.getPathFichier();
            if (file != null && file.exists()) {
                file.delete();
                logger.debug("Suppression du fichier : {}", file);
            }
        }
    }
}
