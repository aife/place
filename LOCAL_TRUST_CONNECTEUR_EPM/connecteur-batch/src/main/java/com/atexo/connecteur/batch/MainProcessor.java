package com.atexo.connecteur.batch;

import fr.atexo.rsem.noyau.ws.beans.ContratBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.AcquitementMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.ListDocumentsMiseAdispositionBean;

public interface MainProcessor {
    boolean needsContrat();

    AcquitementMiseAdispositionBean acquitter(ListDocumentsMiseAdispositionBean documentsList, ContratBean contrat);
}
