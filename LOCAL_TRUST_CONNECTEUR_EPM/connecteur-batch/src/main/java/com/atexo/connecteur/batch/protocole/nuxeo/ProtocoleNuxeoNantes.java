package com.atexo.connecteur.batch.protocole.nuxeo;

import com.atexo.connecteur.batch.MainProcessor;
import com.atexo.connecteur.commun.config.ConfigurationServiceNuxeo;
import com.atexo.connecteur.commun.constant.StatutMiseAdispositionEnum;
import com.atexo.connecteur.commun.xml.NuxeoGeneratedXml;
import fr.atexo.rsem.noyau.ws.beans.ContratBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.AcquitementMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.DocumentMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.ListDocumentsMiseAdispositionBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dcs on 10/01/17.
 * for Atexo
 */
@Service
@Profile("nuxeo")
public class ProtocoleNuxeoNantes implements MainProcessor {

    private static Logger LOG = LoggerFactory.getLogger(ProtocoleNuxeoNantes.class);
    @Autowired
    ConfigurationServiceNuxeo config;

    @Override
    public boolean needsContrat() {
        return true;
    }

    @Override
    public AcquitementMiseAdispositionBean acquitter(ListDocumentsMiseAdispositionBean documentsList, ContratBean contrat) {

        AcquitementMiseAdispositionBean acquittement = new AcquitementMiseAdispositionBean();
        acquittement.setStatut(StatutMiseAdispositionEnum.ACQUITTE.name());
        StringBuilder raisonsEchec = new StringBuilder();
        File nuxeoRepertoire = new File(config.getPath());
        nuxeoRepertoire.mkdirs();
        // vérificatin de l'existance du dossier fournie par le fichier de config
        if (!nuxeoRepertoire.isDirectory()) {
            LOG.error(nuxeoRepertoire + " n'existe pas");
            return null;
        }
        // Création des fichiers
        List<File> fichierGeneres = new ArrayList<>();
        for (DocumentMiseAdispositionBean document : documentsList.getDocumentsList()) {

            // Création des noms
            String nomFichier = document.getNomDeFichier();
            String newXmlName = nomFichier + ".xml";

            // Création du document
            File newFile = new File(nuxeoRepertoire.getAbsolutePath() + "/" + nomFichier);
            // si on retente une nouvelle mise à disposition avec le meme nom de fichier , on supprime le fichier existant
            if (newFile.exists()) newFile.delete();

            // Instanciation de l'objet a convertir en xml
            // TODO en attente de la spécification on prend le xml renvoyé par RSEM
            NuxeoGeneratedXml generateurXml = new NuxeoGeneratedXml(document, documentsList.getMiseADispositionConsultation(), documentsList.getMiseADispositionContrat());
            // Création du fichier xml
            File newXml = new File(nuxeoRepertoire.getAbsolutePath() + "/" + newXmlName);
            // si on retente une nouvelle mise à disposition avec le meme métadonnée fichier , on supprime le fichier existant
            if (newXml.exists()) newXml.delete();
            try {
                // Copier le fichier récupéré de RSEM dans le systeme de fichier
                Files.copy(document.getPathFichier().toPath(), newFile.toPath());
                LOG.info("Création du fichier : {}", newFile.getName());

                // Création du contenu du fichier xml
                JAXBContext jaxbContext = JAXBContext.newInstance(NuxeoGeneratedXml.class);
                Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                jaxbMarshaller.marshal(generateurXml, newXml);
                LOG.info("Création du fichier : {}", newXml.getName());

            } catch (IOException | JAXBException e) {
                acquittement.setStatut(StatutMiseAdispositionEnum.ECHOUE.name());
                raisonsEchec.append(document.getNomDeFichier() + " " + e.getMessage() + "\n");
                LOG.error("impossible de copier le fichier suivant " + document.getPathFichier(), e.fillInStackTrace());
                for (File fichier : fichierGeneres) {
                    fichier.delete();
                }
            } finally {
                if (document.getPathFichier() != null) {
                    document.getPathFichier().delete();
                }
            }
            fichierGeneres.add(newFile);

        }
        acquittement.setMiseAdisposition(documentsList.getMiseAdispositionId());
        return acquittement;
    }
}
