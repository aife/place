package com.atexo.connecteur.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import java.util.Arrays;


/**
 * Created by dcs on 15/09/16.
 * For Atexo
 */
@SpringBootApplication(scanBasePackages = "com.atexo.connecteur")
public class Application extends SpringBootServletInitializer {
    private static final String DEV_PROFILE = "dev";
    private static Logger LOG = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        String activeProfiles = System.getProperty("spring.profiles.active");
        boolean isDev = false;
        if (activeProfiles != null) {
            if (Arrays.stream(activeProfiles.split(",")).filter(p -> DEV_PROFILE.equalsIgnoreCase(p)).findFirst().isPresent()) {
                isDev = true;
            }
        }
        StringBuilder properties = new StringBuilder("spring.config.location=classpath:application.yml,classpath:esb.yml");
        if (isDev) {
            properties.append(",classpath:dev-esb.yml");
        }
        LOG.info("Profils activés : {}", activeProfiles);
        return builder.sources(Application.class).properties(properties.toString());
    }
}
