package com.atexo.connecteur.batch.protocole.cmis;

import com.atexo.connecteur.batch.MainProcessor;
import com.atexo.connecteur.commun.config.ConfigurationServiceCmis;
import com.atexo.connecteur.commun.constant.StatutMiseAdispositionEnum;
import fr.atexo.rsem.noyau.ws.beans.ContratBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.*;
import org.apache.chemistry.opencmis.client.api.*;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.Action;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.exceptions.CmisConnectionException;
import org.apache.chemistry.opencmis.commons.exceptions.CmisContentAlreadyExistsException;
import org.apache.chemistry.opencmis.commons.impl.MimeTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.zeroturnaround.zip.commons.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dcs on 05/10/16.
 * For Atexo
 */
@Service
@Profile("cmis")
public class ProtocoleCMISToulouse implements MainProcessor {
    private static Logger logger = LoggerFactory.getLogger(ProtocoleCMISToulouse.class);
    @Autowired
    private ConfigurationServiceCmis config;
    private Session localSession;

    @Override
    public AcquitementMiseAdispositionBean acquitter(ListDocumentsMiseAdispositionBean documentsList, ContratBean contrat) {
        AcquitementMiseAdispositionBean acquitementDto = new AcquitementMiseAdispositionBean();
        acquitementDto.setStatut(StatutMiseAdispositionEnum.ACQUITTE.name());
        StringBuilder raisonsEchec = new StringBuilder();
        Document doc;
        for (DocumentMiseAdispositionBean document : documentsList.getDocumentsList()) {
            String docName = document.getNomDeFichier();
            File docPath = document.getPathFichier();

            // Mapper du context
            Map<String, Object> properties = setProperties(document, documentsList);

            Session session = getSession();
            try {

                // Grab a reference to the folder where we want to create content
                //@ToDo utiliter?
                Folder folder = (Folder) session.getObjectByPath("/" + config.getPathRacine());

                byte[] content = IOUtils.toByteArray(new FileInputStream(docPath));

                InputStream stream = new ByteArrayInputStream(content);
                ContentStream contentStream;


                // Gestion du mimeType

                String mimeTypes = MimeTypes.getMIMEType(docName);
                logger.debug(mimeTypes);

                //contentStream = session.getObjectFactory().createContentStream(docName, (long) content.length, "application/pdf", stream);
                contentStream = session.getObjectFactory().createContentStream(docName, (long) content.length, mimeTypes, stream);

                //if (Action.CAN_CREATE_DOCUMENT in folder.getAllowableActions()){}
                Boolean canWrite = false;
                for (Action action : folder.getAllowableActions().getAllowableActions()) {
                    if (action == Action.CAN_CREATE_DOCUMENT) {
                        canWrite = true;
                    }
                }

                if (canWrite) try {
                    doc = folder.createDocument(properties, contentStream, VersioningState.MINOR);

                    logger.info("Created: " + doc.getId());
                    logger.info("Content Length: " + doc.getContentStreamLength());
                    document.getPathFichier().delete();
                } catch (CmisContentAlreadyExistsException ccaee) {
                    acquitementDto.setStatut(StatutMiseAdispositionEnum.ECHOUE.name());
                    raisonsEchec.append(document.getNomDeFichier() + " " + ccaee.getMessage() + "\n");
                    logger.error(ccaee.toString());
                }
                else {
                    acquitementDto.setStatut(StatutMiseAdispositionEnum.ECHOUE.name());
                    logger.error("Nous n'avons pas l'autorisation d'écrire dans le dossier : " + folder.getName());
                }
            } catch (Exception exception) {
                acquitementDto.setStatut(StatutMiseAdispositionEnum.ECHOUE.name());
                raisonsEchec.append(document.getNomDeFichier() + " " + exception.getMessage() + "\n");
                logger.error(exception.toString());
            } /*finally {
                if (document != null && document.getArchive() != null) {
                    document.getArchive().delete();
                }
            }*/
        }
        acquitementDto.setResponse(raisonsEchec.toString());
        return acquitementDto;
    }

    public Session getSession() {

        if (localSession == null) {
            try {
                // default factory implementation
                SessionFactory factory = SessionFactoryImpl.newInstance();
                Map<String, String> parameter = new HashMap<>();

                // user credentials
                parameter.put(SessionParameter.USER, config.getAlfrescoLogin());
                parameter.put(SessionParameter.PASSWORD, config.getAlfrescoPassword());

                // connection settings
                parameter.put(SessionParameter.ATOMPUB_URL, config.getAlfrescoServiceUrl()); // Uncomment for Atom Pub binding
                parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value()); // Uncomment for Atom Pub binding

                //parameter.put(SessionParameter.BROWSER_URL, getServiceUrl()); // Uncomment for Browser binding
                //parameter.put(SessionParameter.BINDING_TYPE, BindingType.BROWSER.value()); // Uncomment for Browser binding

                // Set the alfresco object factory
                // Used when using the CMIS extension for Alfresco for working with aspects and CMIS 1.0
                // This is not needed when using CMIS 1.1
                //parameter.put(SessionParameter.OBJECT_FACTORY_CLASS, "org.alfresco.cmis.client.impl.AlfrescoObjectFactoryImpl");

                List<Repository> repositories = factory.getRepositories(parameter);

                this.localSession = repositories.get(0).createSession();
            } catch (CmisConnectionException cce) {
                logger.error(cce.toString());
            }
        }

        return this.localSession;
    }


    public Map<String, Object> setProperties(DocumentMiseAdispositionBean documentDto, ListDocumentsMiseAdispositionBean documentsList) {

        //Mapper du document a renvoyer
        Map<String, Object> properties = new HashMap<>();
        // Create a Map of objects with the props we want to set

        //Renommage du document
        String fileName = rename(documentDto, documentsList);

        properties.put(PropertyIds.NAME, fileName);

        // To set the content type and add the webable and productRelated aspects
        // using CMIS 1.0 and the OpenCMIS extension for Alfresco, do this:
        //properties.put(PropertyIds.OBJECT_TYPE_ID, "D:sc:whitepaper,P:sc:webable,P:sc:productRelated");
        //properties.put(PropertyIds.NAME, filename);

        // To set the content type and add the webable and productRelated aspects
        // using CMIS 1.1 which has aspect support natively, do this:
        //properties.put(PropertyIds.OBJECT_TYPE_ID, "D:sc:whitepaper");
        properties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");

        //properties.put(PropertyIds.SECONDARY_OBJECT_TYPE_IDS, Arrays.asList("P:sc:webable", "P:sc:productRelated", "P:cm:generalclassifiable"));
        //properties.put("sc:isActive", true);
        //properties.put("sc:published", publishDate);
        return properties;
    }

    private Map<String, String> getTypeContrat() {
        return config.getDocumentType();
    }

    private String rename(DocumentMiseAdispositionBean documentDto, ListDocumentsMiseAdispositionBean documentsList) {
        String newName = "";
        Map<String, String> typeDocumentMap = getTypeContrat();


        String typedoc = documentDto.getType();

        // TODO: 11/01/17 Change to real type
        String test = "RECENS";
        test = typeDocumentMap.get(test);

        //Variable Switch case
        String toCertify;
        String patternBegin;
        String result = "fake";

        String regle = typeDocumentMap.get(typedoc);

        if (regle != null) {
            switch (regle) {

                case "REGLE1":
                    // TODO: 11/01/17
                    // Pattern : MAR-CONVGRPT-XXXXX
                    toCertify = "MAR-CONVGRPT-XXXXX";
                    patternBegin = "MAR-CONVGRPT-";

                    result = certificateWithoutNumMarche(patternBegin, toCertify);

                    break;

                case "REGLE2":
                    // TODO: 11/01/17
                    // Pattern : MAR-DELIB-XXXXX
                    toCertify = "MAR-DELIB-XXXXX";
                    patternBegin = "MAR-DELIB-";

                    result = certificateWithoutNumMarche(patternBegin, toCertify);

                    break;

                case "REGLE3":
                    // TODO: 11/01/17
                    // Pattern : MAR-DECPOURS-$Marche-XXXXX
                    toCertify = "MAR-DECPOURS-20160SMP011034-XXXXX";
                    patternBegin = "MAR-DECPOURS-";

                    result = certificateWithNumMarche(patternBegin, toCertify);
                    break;

                case "REGLE4":
                    // TODO: 11/01/17
                    // Pattern : MAR-DC4MODIF-$Marche-XXXXX
                    toCertify = "MAR-DC4MODIF-20160SMP011034-XXXXX";
                    patternBegin = "MAR-DC4MODIF-";

                    result = certificateWithNumMarche(patternBegin, toCertify);
                    break;

                case "REGLE5":
                    // TODO: 11/01/17
                    // Pattern : MAR-RECOND-$Marche-XXXXX
                    toCertify = "MAR-RECOND-20160SMP011034-XXXXX";
                    patternBegin = "MAR-RECOND-";

                    result = certificateWithNumMarche(patternBegin, toCertify);
                    break;

                case "REGLE6":
                    // Pattern : MAR-TYPE-NConsultation
                    result = renameWithNumConsultation(documentDto, documentsList.getMiseADispositionConsultation());
                    break;

                case "REGLE7":
                    // Pattern : MAR-TYPE-NContrat
                    result = renameWithNumContrat(documentDto, documentsList.getMiseADispositionContrat());
                    break;

                default:
                    logger.error("This rule didn't exist");

            }
        } else {
            logger.error("{}, est un type de document qui n'est pas dans le fichier properties", typedoc);
        }

        newName = result;
        logger.debug(result);

        return newName;
    }

    private String certificateWithoutNumMarche(String patternBegin, String toCertify) {
        String first = toCertify.substring(0, patternBegin.length());
        String last = toCertify.substring(first.length());
        if (patternBegin.contentEquals(first) && last.length() == 5) {
            logger.debug("Certification de la regle de type (le nombre de X n'est pas determinant) :  MAR-XXXXXX-XXXXX");
        } else {
            logger.debug("La regle de type (le nombre de X n'est pas determinant) :  MAR-XXXXXX-XXXXX n'est pas respecté");
        }

        return toCertify;
    }

    private String certificateWithNumMarche(String patternBegin, String toCertify) {
        String first = toCertify.substring(0, patternBegin.length());

        String templateNumMarche = "XXXXXXXXXXXXXX-";
        String tmp = toCertify.substring(patternBegin.length(), patternBegin.length() + templateNumMarche.length());
        String numContratName = tmp.substring(0, tmp.length() - 1);

        String last = toCertify.substring(first.length() + tmp.length());

        // TODO: 11/01/17 Changer le numero contrat par fusion.getNumeromarche()
        if (patternBegin.contentEquals(first) && numContratName.contentEquals("20160SMP011034")//To change
                && last.length() == 5) {
            logger.debug("Certification de la regle de type (le nombre de X n'est pas determinant) :  MAR-XXXXXX-XXXXXXXXXXXXXX-XXXXX ");
        } else {
            logger.debug("La regle de type (le nombre de X n'est pas determinant) :  MAR-XXXXXX-XXXXXXXXXXXXXX-XXXXX n'est pas respecté");
        }

        return toCertify;
    }

    private String renameWithNumConsultation(DocumentMiseAdispositionBean documentDto, ConsultationMiseAdispositionBean consultation) {
        String result = "MAR-";
        String typedoc = documentDto.getType();
        String numero = consultation.getReference();
        result = result + typedoc + "-" + numero;
        return result;
    }

    private String renameWithNumContrat(DocumentMiseAdispositionBean documentDto, ContratMiseAdispositionBean contrat) {
        String result = "MAR-";
        String typedoc = documentDto.getType();
        String numero = contrat.getNumero();
        result = result + typedoc + "-" + numero;
        return result;
    }


    @Override
    public boolean needsContrat() {
        return false;
    }

}
