
package fr.atexo.rsem.noyau.ws.beans;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * Bean utilisé pour l'echange d'informations avec des entites externes a RSEM.
 *
 * @author RDA
 * @version $Revision$, $Date$, $Author$
 */
@XmlType(name = "formePrix")
@XmlEnum
public enum FormePrixBean {

    @XmlEnumValue("FORFAITAIRE")
    FORFAITAIRE("FORFAITAIRE"),
    @XmlEnumValue("UNITAIRE")
    UNITAIRE("UNITAIRE"),
    @XmlEnumValue("MIXTE")
    MIXTE("MIXTE");
    private String value;

    FormePrixBean(String v) {
        value = v;
    }

    public static FormePrixBean fromValue(String v) {
        for (FormePrixBean c : FormePrixBean.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return "FormePrixBean{" + "value='" + value + '\'' + '}';
    }
}
