package fr.atexo.rsem.noyau.ws.beans;

/**
 * Bean répresentant les informations du contractant d'un attributaire du contrat. Ce
 * bean est utilisé par le Webservice REST pour échanges des données du contrat
 * d'EPM avec des applications tiers
 *
 * @author Rebeca Dantas
 */
public class ContractantBean {

    /**
     * Rôle juridique associé au contractant
     */
    private String roleJuridique;

    /**
     * Montant de prestation HT
     */
    private Double montantPrestationHT;

    /**
     * Rôle opérationnel
     */
    private String roleOperationnel;

    private EtablissementEntrepriseBean entreprise;

    /**
     * indique si le contractant est mandataire
     */
    private boolean mandataire;

    /**
     * indique si le contractant est solidaire
     */
    private boolean solidaire;


    /**
     * @return roleJuridique : retourne le code externe du rôle juridique associé au contractant
     */
    public String getRoleJuridique() {
        return roleJuridique;
    }

    /**
     * @param valeur : défine le code externe du rôle juridique associé au contractant
     */
    public void setRoleJuridique(String valeur) {
        this.roleJuridique = valeur;
    }

    /**
     * @return montantPrestationHT : retourne le montant de prestation HT
     */
    public Double getMontantPrestationHT() {
        return montantPrestationHT;
    }

    /**
     * @param valeur : défine le montant de prestation HT
     */
    public void setMontantPrestationHT(Double valeur) {
        this.montantPrestationHT = valeur;
    }

    /**
     * @return roleOperationnel : retourne le rôle opérationnel
     */
    public String getRoleOperationnel() {
        return roleOperationnel;
    }

    /**
     * @param valeur : défine le rôle opérationnel
     */
    public void setRoleOperationnel(String valeur) {
        this.roleOperationnel = valeur;
    }

    public EtablissementEntrepriseBean getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(EtablissementEntrepriseBean entreprise) {
        this.entreprise = entreprise;
    }

    /**
     * @return the mandataire
     */
    public boolean isMandataire() {
        return mandataire;
    }

    /**
     * @param mandataire the mandataire to set
     */
    public void setMandataire(boolean valeur) {
        this.mandataire = valeur;
    }

    /**
     * @return the solidaire
     */
    public boolean isSolidaire() {
        return solidaire;
    }

    /**
     * @param valeur the solidaire to set
     */
    public void setSolidaire(boolean valeur) {
        this.solidaire = valeur;
    }

    @Override
    public String toString() {
        return "ContractantBean{" + "roleJuridique='" + roleJuridique + '\'' + ", montantPrestationHT=" + montantPrestationHT + ", roleOperationnel='" + roleOperationnel + '\'' + ", entreprise=" + entreprise + ", mandataire=" + mandataire + ", solidaire=" + solidaire + '}';
    }
}
