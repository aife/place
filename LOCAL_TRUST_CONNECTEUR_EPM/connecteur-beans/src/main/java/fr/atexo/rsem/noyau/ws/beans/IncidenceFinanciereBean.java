package fr.atexo.rsem.noyau.ws.beans;

/**
 * Bean répresentant les informations de l'incidence financière des avenants. Ce bean est
 * utilisé par le Webservice REST pour échanges des données du contrat d'EPM avec des applications tiers
 *
 * @author Rebeca Dantas
 */
public class IncidenceFinanciereBean {

    /**
     * Montant de l'avenant PF + DQE
     */
    private double montantPfDqe;

    /**
     * Montant de l'avenant : bons de commandes min
     */
    private double montantBcMin;

    /**
     * Montant de l'avenant : bons de commandes max
     */
    private double montantBcMax;

    /**
     * @return the montantPfDqe
     */
    public double getMontantPfDqe() {
        return montantPfDqe;
    }

    /**
     * @param montantPfDqe the montantPfDqe to set
     */
    public void setMontantPfDqe(double valeur) {
        this.montantPfDqe = valeur;
    }

    /**
     * @return the montantBcMin
     */
    public double getMontantBcMin() {
        return montantBcMin;
    }

    /**
     * @param montantBcMin the montantBcMin to set
     */
    public void setMontantBcMin(double valeur) {
        this.montantBcMin = valeur;
    }

    /**
     * @return the montantBcMax
     */
    public double getMontantBcMax() {
        return montantBcMax;
    }

    /**
     * @param montantBcMax the montantBcMax to set
     */
    public void setMontantBcMax(double valeur) {
        this.montantBcMax = valeur;
    }

    @Override
    public String toString() {
        return "IncidenceFinanciereBean{" + "montantPfDqe=" + montantPfDqe + ", montantBcMin=" + montantBcMin + ", montantBcMax=" + montantBcMax + '}';
    }
}
