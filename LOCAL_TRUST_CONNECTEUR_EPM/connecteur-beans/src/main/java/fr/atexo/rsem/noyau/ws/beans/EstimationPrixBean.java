
package fr.atexo.rsem.noyau.ws.beans;

import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.Date;
import java.util.List;

/**
 * Bean répresentant les estimations de la forme de prix lors de la création
 * d'une consultation. Ce bean est utilisé par le Webservice REST pour échanges
 * des données du contrat d'EPM avec des applications tiers
 *
 * @author RDA
 * @version $Revision$, $Date$, $Author$
 */
public class EstimationPrixBean {

    /**
     * Type de prix : Prix sur catalogue, Bordereau de prix ou Autre
     */
    private List<String> typePrix;
    /**
     * Variation du prix partie unitaire : Actualisables  ou Révisables ou Fermes.
     */

    private List<String> variationPrixUnitaire;
    /**
     * Variation du prix partie forfaitaire : Actualisables  ou Révisables ou Fermes.
     */
    private List<String> variationPrixForfaitaire;

    private FormePrixBean formePrix;
    private Date datePrix;
    private double estimationHTForfataire;
    private double estimationHTUnitaire;
    private double estimationTTCForfataire;
    private double estimationTTCUnitaire;

    /**
     * @return the typePrix
     */
    @XmlElementWrapper(name = "listeTypesPrix")
    public List<String> getTypePrix() {
        return typePrix;
    }

    /**
     * @param typePrix the typePrix to set
     */
    public void setTypePrix(List<String> valeur) {
        this.typePrix = valeur;
    }

    /**
     * @return the variationPrixUnitaire
     */
    @XmlElementWrapper(name = "listeVariationsPrixUnitaire")
    public List<String> getVariationPrixUnitaire() {
        return variationPrixUnitaire;
    }

    /**
     * @param variationPrixUnitaire the variationprix to set
     */
    public void setVariationPrixUnitaire(List<String> valeur) {
        this.variationPrixUnitaire = valeur;
    }

    /**
     * @return the variationPrixForfaitaire
     */
    @XmlElementWrapper(name = "listeVariationsPrixForfaitaire")
    public List<String> getVariationPrixForfaitaire() {
        return variationPrixForfaitaire;
    }

    /**
     * @param variationPrixForfaitaire the variationprix to set
     */
    public void setVariationPrixForfaitaire(List<String> valeur) {
        this.variationPrixForfaitaire = valeur;
    }

    /**
     * @return the formePrix
     */
    public FormePrixBean getFormePrix() {
        return formePrix;
    }

    /**
     * @param formePrix the formePrix to set
     */
    public void setFormePrix(FormePrixBean valeur) {
        this.formePrix = valeur;
    }

    /**
     * @return the datePrix
     */
    public Date getDatePrix() {
        return datePrix;
    }

    /**
     * @param datePrix the datePrix to set
     */
    public void setDatePrix(Date valeur) {
        this.datePrix = valeur;
    }

    /**
     * @return the estimationHTForfataire
     */
    public double getEstimationHTForfataire() {
        return estimationHTForfataire;
    }

    /**
     * @param estimationHTForfataire the estimationHTForfataire to set
     */
    public void setEstimationHTForfataire(double valeur) {
        this.estimationHTForfataire = valeur;
    }

    /**
     * @return the estimationHTUnitaire
     */
    public double getEstimationHTUnitaire() {
        return estimationHTUnitaire;
    }

    /**
     * @param estimationHTUnitaire the estimationHTUnitaire to set
     */
    public void setEstimationHTUnitaire(double valeur) {
        this.estimationHTUnitaire = valeur;
    }

    /**
     * @return the estimationTTCForfataire
     */
    public double getEstimationTTCForfataire() {
        return estimationTTCForfataire;
    }

    /**
     * @param estimationTTCForfataire the estimationTTCForfataire to set
     */
    public void setEstimationTTCForfataire(double valeur) {
        this.estimationTTCForfataire = valeur;
    }

    /**
     * @return the estimationTTCUnitaire
     */
    public double getEstimationTTCUnitaire() {
        return estimationTTCUnitaire;
    }

    /**
     * @param estimationTTCUnitaire the estimationTTCUnitaire to set
     */
    public void setEstimationTTCUnitaire(double valeur) {
        this.estimationTTCUnitaire = valeur;
    }

    @Override
    public String toString() {
        return "EstimationPrixBean{" + "typePrix=" + typePrix + ", variationPrixUnitaire=" + variationPrixUnitaire + ", variationPrixForfaitaire=" + variationPrixForfaitaire + ", formePrix=" + formePrix + ", datePrix=" + datePrix + ", estimationHTForfataire=" + estimationHTForfataire + ", estimationHTUnitaire=" + estimationHTUnitaire + ", estimationTTCForfataire=" + estimationTTCForfataire + ", estimationTTCUnitaire=" + estimationTTCUnitaire + '}';
    }
}
