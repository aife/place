/**
 *
 */
package fr.atexo.rsem.noyau.ws.beans.miseADisposition;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/**
 * cette classe represente la reponse de la webméthode connection le résultat de
 * cette classe sera utilisé pour s'autentifier a chaque fois ou on utilise la
 * web Méthode
 *
 * @author KHALED BENARI
 */
@XmlRootElement(name = "ticket")
public class TokenBean {
    private String ticket;

    /**
     * @return the ticket
     */
    @XmlValue
    public String getTicket() {
        return ticket;
    }

    /**
     * @param ticket the ticket to set
     */
    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    @Override
    public String toString() {
        return "TokenBean{" + "ticket='" + ticket + '\'' + '}';
    }
}
