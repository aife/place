/**
 *
 */
package fr.atexo.rsem.noyau.ws.beans.miseADisposition;

import java.util.Date;

/**
 * @author KHALED BEANRI
 */
public class ContratMiseAdispositionBean {
    private String numero;
    private String objet;
    private String titulaire;
    private String numeroLot;
    private String intituleLot;
    private String type;
    private Date dateNotification;

    /**
     * @return the numero
     */
    public String getNumero() {
        return numero;
    }

    /**
     * @param numero the numero to set
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public String getTitulaire() {
        return titulaire;
    }

    public void setTitulaire(String titulaire) {
        this.titulaire = titulaire;
    }

    public Date getDateNotification() {
        return dateNotification;
    }

    public void setDateNotification(Date dateNotofocation) {
        this.dateNotification = dateNotofocation;
    }

    public String getNumeroLot() {
        return numeroLot;
    }

    public void setNumeroLot(String numeroLot) {
        this.numeroLot = numeroLot;
    }

    public String getIntituleLot() {
        return intituleLot;
    }

    public void setIntituleLot(String intituleLot) {
        this.intituleLot = intituleLot;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ContratMiseAdispositionBean{" + "numero='" + numero + '\'' + ", objet='" + objet + '\'' + ", titulaire='" + titulaire + '\'' + ", numeroLot='" + numeroLot + '\'' + ", intituleLot='" + intituleLot + '\'' + ", type='" + type + '\'' + ", dateNotification=" + dateNotification + '}';
    }
}
