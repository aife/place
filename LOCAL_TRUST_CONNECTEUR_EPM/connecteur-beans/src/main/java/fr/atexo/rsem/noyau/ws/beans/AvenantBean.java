package fr.atexo.rsem.noyau.ws.beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "avenant")
public class AvenantBean extends ProcedureBean {

    /**
     * Numéro du contrat initial
     */
    private String numeroInitialContrat;

    private ContratSimpleBean contrat;

    private String commentaire;

    private boolean passageCAO;

    private boolean passageCommissionPermanente;

    /**
     * @return the numeroInitialContrat
     */
    public String getNumeroInitialContrat() {
        return numeroInitialContrat;
    }

    /**
     * @param numeroInitialContrat the numeroInitialContrat to set
     */
    public void setNumeroInitialContrat(String valeur) {
        this.numeroInitialContrat = valeur;
    }

    /**
     * @return the contrat
     */
    public ContratSimpleBean getContrat() {
        return contrat;
    }

    /**
     * @param contrat the contrat to set
     */
    public void setContrat(ContratSimpleBean valeur) {
        this.contrat = valeur;
    }

    /**
     * @return the commentaire
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * @param commentaire the commentaire to set
     */
    public void setCommentaire(String valeur) {
        this.commentaire = valeur;
    }

    /**
     * @return the passageCAO
     */
    public boolean isPassageCAO() {
        return passageCAO;
    }

    /**
     * @param passageCAO the passageCAO to set
     */
    public void setPassageCAO(boolean valeur) {
        this.passageCAO = valeur;
    }

    /**
     * @return the passageCommissionPermanente
     */
    public boolean isPassageCommissionPermanente() {
        return passageCommissionPermanente;
    }

    /**
     * @param passageCommissionPermanente the passageCommissionPermanente to set
     */
    public void setPassageCommissionPermanente(
            boolean valeur) {
        this.passageCommissionPermanente = valeur;
    }

    @Override
    public String toString() {
        return "AvenantBean{" + "numeroInitialContrat='" + numeroInitialContrat + '\'' + ", contrat=" + contrat + ", commentaire='" + commentaire + '\'' + ", passageCAO=" + passageCAO + ", passageCommissionPermanente=" + passageCommissionPermanente + '}';
    }
}
