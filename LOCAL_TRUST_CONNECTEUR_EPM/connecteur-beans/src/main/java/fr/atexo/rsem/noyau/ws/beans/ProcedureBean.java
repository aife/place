package fr.atexo.rsem.noyau.ws.beans;

public class ProcedureBean {

    /**
     * L'intitulé : consultation / avenant
     */
    private String intitule;

    /**
     * L'objet : consultation / avenant
     */
    private String objet;

    /**
     * Nom du pouvoir adjudicateur de la consultation
     */
    private String nomContractantPublicMarche;
    /**
     * Code externe du pouvoir adjudicateur de la consultation
     */
    private String codeContractantPublicMarche;
    /**
     * Nom de la direction service responsable de la consultation
     */
    private String nomResponsableMarche;

    /**
     * Code externe de la direction service responsable de la consultation
     */
    private String codeResponsableMarche;

    /**
     * Nom de la direction service beneficiaire de la consultation
     */
    private String nomServiceGestionnaireDuMarche;
    /**
     * Code externe de la direction service beneficiaire de la consultation
     */
    private String codeServiceGestionnaireDuMarche;

    /**
     * Nom de la direction service
     */
    private String nomDirectionServiceMarche;

    /**
     * Code de la direction service
     */
    private String codeDirectionServiceMarche;

    /**
     * Procédure de passation
     */
    private String procedure;

    /**
     * Code d'organisme (gérer le multi-entité)
     */
    private String codeOrganisme;

    /**
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * @param intitule the intitule to set
     */
    public void setIntitule(String valeur) {
        this.intitule = valeur;
    }

    /**
     * @return the objet
     */
    public String getObjet() {
        return objet;
    }

    /**
     * @param objet the objet to set
     */
    public void setObjet(String valeur) {
        this.objet = valeur;
    }

    /**
     * @return the nomContractantPublicMarche
     */
    public String getNomContractantPublicMarche() {
        return nomContractantPublicMarche;
    }

    /**
     * @param nomContractantPublicMarche the nomContractantPublicMarche to set
     */
    public void setNomContractantPublicMarche(String valeur) {
        this.nomContractantPublicMarche = valeur;
    }

    /**
     * @return the codeContractantPublicMarche
     */
    public String getCodeContractantPublicMarche() {
        return codeContractantPublicMarche;
    }

    /**
     * @param codeContractantPublicMarche the codeContractantPublicMarche to set
     */
    public void setCodeContractantPublicMarche(String valeur) {
        this.codeContractantPublicMarche = valeur;
    }

    /**
     * @return the nomResponsableMarche
     */
    public String getNomResponsableMarche() {
        return nomResponsableMarche;
    }

    /**
     * @param nomResponsableMarche the nomResponsableMarche to set
     */
    public void setNomResponsableMarche(String valeur) {
        this.nomResponsableMarche = valeur;
    }

    /**
     * @return the codeResponsableMarche
     */
    public String getCodeResponsableMarche() {
        return codeResponsableMarche;
    }

    /**
     * @param codeResponsableMarche the codeResponsableMarche to set
     */
    public void setCodeResponsableMarche(String valeur) {
        this.codeResponsableMarche = valeur;
    }

    /**
     * @return the nomServiceGestionnaireDuMarche
     */
    public String getNomServiceGestionnaireDuMarche() {
        return nomServiceGestionnaireDuMarche;
    }

    /**
     * @param nomServiceGestionnaireDuMarche the nomServiceGestionnaireDuMarche to set
     */
    public void setNomServiceGestionnaireDuMarche(String valeur) {
        this.nomServiceGestionnaireDuMarche = valeur;
    }

    /**
     * @return the codeServiceGestionnaireDuMarche
     */
    public String getCodeServiceGestionnaireDuMarche() {
        return codeServiceGestionnaireDuMarche;
    }

    /**
     * @param codeServiceGestionnaireDuMarche the codeServiceGestionnaireDuMarche to set
     */
    public void setCodeServiceGestionnaireDuMarche(String valeur) {
        this.codeServiceGestionnaireDuMarche = valeur;
    }

    /**
     * @return the nomDirectionServiceMarche
     */
    public String getNomDirectionServiceMarche() {
        return nomDirectionServiceMarche;
    }

    /**
     * @param nomDirectionServiceMarche the nomDirectionServiceMarche to set
     */
    public void setNomDirectionServiceMarche(String valeur) {
        this.nomDirectionServiceMarche = valeur;
    }

    /**
     * @return the codeDirectionServiceMarche
     */
    public String getCodeDirectionServiceMarche() {
        return codeDirectionServiceMarche;
    }

    /**
     * @param codeDirectionServiceMarche the codeDirectionServiceMarche to set
     */
    public void setCodeDirectionServiceMarche(String valeur) {
        this.codeDirectionServiceMarche = valeur;
    }

    /**
     * @return the procedure
     */
    public String getProcedure() {
        return procedure;
    }

    /**
     * @param procedure the procedure to set
     */
    public void setProcedure(String valeur) {
        this.procedure = valeur;
    }

    public String getCodeOrganisme() {
        return codeOrganisme;
    }

    public void setCodeOrganisme(String valeur) {
        this.codeOrganisme = valeur;
    }

    @Override
    public String toString() {
        return "ProcedureBean{" + "intitule='" + intitule + '\'' + ", objet='" + objet + '\'' + ", nomContractantPublicMarche='" + nomContractantPublicMarche + '\'' + ", codeContractantPublicMarche='" + codeContractantPublicMarche + '\'' + ", nomResponsableMarche='" + nomResponsableMarche + '\'' + ", codeResponsableMarche='" + codeResponsableMarche + '\'' + ", nomServiceGestionnaireDuMarche='" + nomServiceGestionnaireDuMarche + '\'' + ", codeServiceGestionnaireDuMarche='" + codeServiceGestionnaireDuMarche + '\'' + ", nomDirectionServiceMarche='" + nomDirectionServiceMarche + '\'' + ", codeDirectionServiceMarche='" + codeDirectionServiceMarche + '\'' + ", procedure='" + procedure + '\'' + ", codeOrganisme='" + codeOrganisme + '\'' + '}';
    }
}
