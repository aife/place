package fr.atexo.rsem.noyau.ws.beans.miseADisposition;

import javax.xml.bind.annotation.XmlTransient;
import java.io.File;
import java.util.Date;

public class DocumentMiseAdispositionBean {

    /**
     * Chemin du fichier après la décompression de l'archive. Utilisé uniquement
     * par les clients du webService. N'est pas utilisé pour la génération du
     * XML.
     */
    private File pathFichier;

    /**
     * nom du document
     */
    private String nomDeFichier;
    /**
     * type du document
     */
    private String type;

    /**
     * La catégorie du document (piéce du dce, piéce de la réponse, aapc etc...)
     */
    private String categorie;

    /**
     * La date du document (date de création de du document ou date saisie par l'utilisateur dans l'interface)
     */
    private Date date;

    /**
     * Commmentaire du document saisi pas l'utilisateur.
     */
    private String commentaire;

    /**
     * Objet du document dans le cas d'un document externe.
     */
    private String objet;

    private boolean preRemplis = false;

    /**
     * indiquer étape spécifique
     */
    private String statut;

    /**
     * @return String
     */
    public String getNomDeFichier() {
        return nomDeFichier;
    }

    /**
     * @param nomDeFichier
     */

    public void setNomDeFichier(String nomDeFichier) {
        this.nomDeFichier = nomDeFichier;
    }

    /**
     * @return String
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(final String valeur) {
        this.type = valeur;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(final String valeur) {
        this.categorie = valeur;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(final Date valeur) {
        this.date = valeur;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(final String valeur) {
        this.commentaire = valeur;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(final String valeur) {
        this.objet = valeur;
    }

    public boolean isPreRemplis() {
        return preRemplis;
    }

    public void setPreRemplis(final boolean valeur) {
        this.preRemplis = valeur;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(final String valeur) {
        this.statut = valeur;
    }

    /**
     * @return Chemin du fichier après la décompression de l'archive. Utilisé uniquement
     * par les clients du webService. N'est pas utilisé pour la génération du
     * XML.
     */
    @XmlTransient
    public File getPathFichier() {
        return pathFichier;
    }

    /**
     * @param valeur chemin du fichier après la décompression de l'archive. Utilisé uniquement
     *               par les clients du webService. N'est pas utilisé pour la génération du
     *               XML.
     */
    public void setPathFichier(final File valeur) {
        this.pathFichier = valeur;
    }

    @Override
    public String toString() {
        return "DocumentMiseAdispositionBean{" + "pathFichier=" + pathFichier + ", nomDeFichier='" + nomDeFichier + '\'' + ", type='" + type + '\'' + ", categorie='" + categorie + '\'' + ", date=" + date + ", commentaire='" + commentaire + '\'' + ", objet='" + objet + '\'' + ", preRemplis=" + preRemplis + ", statut='" + statut + '\'' + '}';
    }
}
