package fr.atexo.rsem.noyau.ws.beans.miseADisposition;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.File;
import java.util.List;

@XmlRootElement(name = "resultat")
public class ListDocumentsMiseAdispositionBean {

    private List<fr.atexo.rsem.noyau.ws.beans.miseADisposition.DocumentMiseAdispositionBean> documentsList;
    /**
     * la destination de la mise à disposition
     */
    private String applicationtiers;

    /**
     * Id de la mise dispotion a laquel appartient le contrat
     */
    private String miseAdispositionId;

    /**
     * information de la consultation
     */
    private fr.atexo.rsem.noyau.ws.beans.miseADisposition.ConsultationMiseAdispositionBean miseADispositionConsultation;

    /**
     * information de la consultation
     */
    private fr.atexo.rsem.noyau.ws.beans.miseADisposition.ContratMiseAdispositionBean miseADispositionContrat;

    /**
     * la liste des documents
     */
    private File archive;

    /**
     * utilisateur qui fait la mise à disposition
     */
    private fr.atexo.rsem.noyau.ws.beans.miseADisposition.UtilisateurBean utilisateur;

    /**
     * @return the documentsList
     */
    @XmlElementWrapper(name = "listDocuments")
    @XmlElement(name = "Document")
    public List<fr.atexo.rsem.noyau.ws.beans.miseADisposition.DocumentMiseAdispositionBean> getDocumentsList() {
        return documentsList;
    }

    /**
     * @param documentsList the documentsList to set
     */
    public void setDocumentsList(List<DocumentMiseAdispositionBean> documentsList) {
        this.documentsList = documentsList;
    }

    /**
     * @return the applicationtiers
     */

    @XmlElement(name = "codeDestination")
    public String getApplicationtiers() {
        return applicationtiers;
    }

    /**
     * @param applicationtiers the applicationtiers to set
     */
    public void setApplicationtiers(String applicationtiers) {
        this.applicationtiers = applicationtiers;
    }

    /**
     * @return the miseAdispositionId
     */
    @XmlElement(name = "idMiseADisposition")
    public String getMiseAdispositionId() {
        return miseAdispositionId;
    }

    /**
     * @param miseAdispositionId the miseAdispositionId to set
     */
    public void setMiseAdispositionId(String miseAdispositionId) {
        this.miseAdispositionId = miseAdispositionId;
    }

    /**
     * @return the miseADispositionConsultation
     */
    @XmlElement(name = "consultation")
    public fr.atexo.rsem.noyau.ws.beans.miseADisposition.ConsultationMiseAdispositionBean getMiseADispositionConsultation() {
        return miseADispositionConsultation;
    }

    /**
     * @param miseADispositionConsultation the miseADispositionConsultation to set
     */
    public void setMiseADispositionConsultation(ConsultationMiseAdispositionBean miseADispositionConsultation) {
        this.miseADispositionConsultation = miseADispositionConsultation;
    }

    /**
     * @return the miseADispositionContrat
     */
    /**
     * @return the miseAdispositionId
     */
    @XmlElement(name = "contrat")
    public fr.atexo.rsem.noyau.ws.beans.miseADisposition.ContratMiseAdispositionBean getMiseADispositionContrat() {
        return miseADispositionContrat;
    }

    /**
     * @param miseADispositionContrat the miseADispositionContrat to set
     */
    public void setMiseADispositionContrat(ContratMiseAdispositionBean miseADispositionContrat) {
        this.miseADispositionContrat = miseADispositionContrat;
    }

    @XmlTransient
    public File getArchive() {
        return archive;
    }

    public void setArchive(final File valeur) {
        this.archive = valeur;
    }

    public fr.atexo.rsem.noyau.ws.beans.miseADisposition.UtilisateurBean getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(final UtilisateurBean valeur) {
        this.utilisateur = valeur;
    }

    @Override
    public String toString() {
        return "ListDocumentsMiseAdispositionBean{" + "documentsList=" + documentsList + ", applicationtiers='" + applicationtiers + '\'' + ", miseAdispositionId='" + miseAdispositionId + '\'' + ", miseADispositionConsultation=" + miseADispositionConsultation + ", miseADispositionContrat=" + miseADispositionContrat + ", archive=" + archive + ", utilisateur=" + utilisateur + '}';
    }
}
