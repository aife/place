package fr.atexo.rsem.noyau.ws.beans;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Bean répresentant une liste de consultations à envoyer comme reponse à
 * l'appel ws.
 *
 * @author RDA
 * @version $Revision$, $Date$, $Author$
 */
@XmlRootElement(name = "resultat")
public class ResponseBean {

    private List<ConsultationBean> consultations;

    private List<AvenantBean> avenants;

    @XmlElementWrapper(name = "consultations")
    @XmlElement(name = "consultation")
    public List<ConsultationBean> getConsultations() {
        return consultations;
    }

    public void setConsultations(List<ConsultationBean> consultations) {
        this.consultations = consultations;
    }

    @XmlElementWrapper(name = "avenants")
    @XmlElement(name = "avenant")
    public List<AvenantBean> getAvenants() {
        return avenants;
    }

    public void setAvenants(List<AvenantBean> valeur) {
        this.avenants = valeur;
    }

    @Override
    public String toString() {
        return "ResponseBean{" + "consultations=" + consultations + ", avenants=" + avenants + '}';
    }
}
