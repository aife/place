package fr.atexo.rsem.noyau.ws.beans;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.Date;
import java.util.List;

/**
 * Bean répresentant les informations du attributaire d'une consultation. Ce
 * bean est utilisé par le Webservice REST pour échanges des données du contrat
 * d'EPM avec des applications tiers
 *
 * @author Rebeca Dantas
 * @version $Revision$, $Date$, $Author$
 */
public class AttributaireBean {

    private String raisonSocial;
    private String siret;
    private String adresse;
    private String codePostal;
    private String commune;
    private String telephone;
    private String fax;
    private String email;
    private List<ContractantBean> contractants;
    private String moyenNotification;
    private Date dateNotification;
    private String typeAttributaire;

    public String getSiret() {
        return siret;
    }

    public void setSiret(String valeur) {
        this.siret = valeur;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String valeur) {
        this.adresse = valeur;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String valeur) {
        this.codePostal = valeur;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String valeur) {
        this.commune = valeur;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String valeur) {
        this.fax = valeur;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String valeur) {
        this.email = valeur;
    }

    public String getRaisonSocial() {
        return raisonSocial;
    }

    public void setRaisonSocial(String valeur) {
        this.raisonSocial = valeur;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String valeur) {
        this.telephone = valeur;
    }

    /**
     * @return the contractants
     */
    @XmlElementWrapper(name = "contractants")
    @XmlElement(name = "contractant")
    public List<ContractantBean> getContractants() {
        return contractants;
    }

    /**
     * @param contractants the contractants to set
     */
    public void setContractants(List<ContractantBean> valeur) {
        this.contractants = valeur;
    }

    /**
     * @return the moyenNotification
     */
    public String getMoyenNotification() {
        return moyenNotification;
    }

    /**
     * @param moyenNotification the moyenNotification to set
     */
    public void setMoyenNotification(String valeur) {
        moyenNotification = valeur;
    }


    public Date getDateNotification() {
        return dateNotification;
    }

    public void setDateNotification(Date dateDeNotification) {
        dateNotification = dateDeNotification;
    }

    public String getTypeAttributaire() {
        return typeAttributaire;
    }

    public void setTypeAttributaire(String valeur) {
        this.typeAttributaire = valeur;
    }

    @Override
    public String toString() {
        return "AttributaireBean{" + "raisonSocial='" + raisonSocial + '\'' + ", siret='" + siret + '\'' + ", adresse='" + adresse + '\'' + ", codePostal='" + codePostal + '\'' + ", commune='" + commune + '\'' + ", telephone='" + telephone + '\'' + ", fax='" + fax + '\'' + ", email='" + email + '\'' + ", contractants=" + contractants + ", moyenNotification='" + moyenNotification + '\'' + ", dateNotification=" + dateNotification + ", typeAttributaire='" + typeAttributaire + '\'' + '}';
    }
}
