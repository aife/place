package fr.atexo.rsem.noyau.ws.beans;

/**
 * Utilise pour l'echange d'informations avec des entites externes a RSEM.
 *
 * @author RVI
 * @version $Revision$, $Date$, $Author$
 */
public class EntrepriseBean {

    private String siret;

    private String raisonSociale;

    private AdresseBean adresse;

    public AdresseBean getAdresse() {
        return adresse;
    }

    public void setAdresse(final AdresseBean valeur) {
        this.adresse = valeur;
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(final String valeur) {
        this.siret = valeur;
    }

    public String getRaisonSociale() {
        return raisonSociale;
    }

    public void setRaisonSociale(final String valeur) {
        this.raisonSociale = valeur;
    }

    @Override
    public String toString() {
        return "EntrepriseBean{" + "siret='" + siret + '\'' + ", raisonSociale='" + raisonSociale + '\'' + ", adresse=" + adresse + '}';
    }
}
