/**
 *
 */
package fr.atexo.rsem.noyau.ws.beans.miseADisposition;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author KBE
 */
@XmlRootElement(name = "resultat")
public class AcquitementMiseAdispositionBean {

    private String response;

    private String statut;

    private String miseAdisposition;

    public AcquitementMiseAdispositionBean() {

    }

    public AcquitementMiseAdispositionBean(String miseAdisposition, String statut, String response) {
        this.response = response;
        this.statut = statut;
        this.miseAdisposition = miseAdisposition;
    }

    /**
     * @return the statut
     */
    public String getStatut() {
        return statut;
    }

    /**
     * @param statut the statut to set
     */
    public void setStatut(String statut) {
        this.statut = statut;
    }

    /**
     * @return the miseAdisposition
     */
    public String getMiseAdisposition() {
        return miseAdisposition;
    }

    /**
     * @param miseAdisposition the miseAdisposition to set
     */
    public void setMiseAdisposition(String miseAdisposition) {
        this.miseAdisposition = miseAdisposition;
    }

    @XmlElement
    public String getResponse() {
        return response;
    }

    public void setResponse(String reponse) {
        this.response = reponse;
    }

    @Override
    public String toString() {
        return "AcquitementMiseAdispositionBean{" + "response='" + response + '\'' + ", statut='" + statut + '\'' + ", miseAdisposition='" + miseAdisposition + '\'' + '}';
    }
}
