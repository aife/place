
package fr.atexo.rsem.noyau.ws.beans;

/**
 * Bean répresentant les informations d'une tranche de la consultation. Ce
 * bean est utilisé par le Webservice REST pour échanges des données du contrat
 * d'EPM avec des applications tiers
 *
 * @author RDA
 * @version $Revision$, $Date$, $Author$
 */
public class TrancheBean {

    private String codeTranche;
    private String natureTranche;
    private String intituleTranche;
    private EstimationPrixBean estimationPrix;
    private ConsolidePrixBean consolidePrix;
    private Boolean trancheFixe;

    public String getCodeTranche() {
        return codeTranche;
    }

    public void setCodeTranche(String codeTranche) {
        this.codeTranche = codeTranche;
    }

    public EstimationPrixBean getEstimationPrix() {
        return estimationPrix;
    }

    public void setEstimationPrix(EstimationPrixBean estimationPrix) {
        this.estimationPrix = estimationPrix;
    }

    public String getIntituleTranche() {
        return intituleTranche;
    }

    public void setIntituleTranche(String intituleTranche) {
        this.intituleTranche = intituleTranche;
    }

    public String getNatureTranche() {
        return natureTranche;
    }

    public void setNatureTranche(String natureTranche) {
        this.natureTranche = natureTranche;
    }

    public Boolean getTrancheFixe() {
        return trancheFixe;
    }

    public void setTrancheFixe(Boolean trancheFixe) {
        this.trancheFixe = trancheFixe;
    }

    public ConsolidePrixBean getConsolidePrix() {
        return consolidePrix;
    }

    public void setConsolidePrix(ConsolidePrixBean valeur) {
        this.consolidePrix = valeur;
    }

    @Override
    public String toString() {
        return "TrancheBean{" + "codeTranche='" + codeTranche + '\'' + ", natureTranche='" + natureTranche + '\'' + ", intituleTranche='" + intituleTranche + '\'' + ", estimationPrix=" + estimationPrix + ", consolidePrix=" + consolidePrix + ", trancheFixe=" + trancheFixe + '}';
    }
}
