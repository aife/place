package fr.atexo.rsem.noyau.ws.beans.miseADisposition;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "resultat")
public class ListMiseAdispositionBean {

    private List<fr.atexo.rsem.noyau.ws.beans.miseADisposition.MiseAdispositionBean> miseAdispositionList;

    /**
     * @return the documentsList
     */
    @XmlElementWrapper(name = "listMiseAdispostion")
    @XmlElement(name = "miseAdisposition")
    public List<fr.atexo.rsem.noyau.ws.beans.miseADisposition.MiseAdispositionBean> getMiseAdispositionList() {
        return miseAdispositionList;
    }

    /**
     * @param documentsList the documentsList to set
     */
    public void setMiseAdispositionList(List<MiseAdispositionBean> value) {
        this.miseAdispositionList = value;
    }

    @Override
    public String toString() {
        return "ListMiseAdispositionBean{" + "miseAdispositionList=" + miseAdispositionList + '}';
    }
}
