package fr.atexo.rsem.noyau.ws.beans;

public enum ErreurEnum {

    AUTRE(1), TECHNIQUE_BASE_DE_DONNEES(2), AUTHENTIFICATION(3), ENTITE_NON_TROUVEE(4);

    private final int valeur;

    ErreurEnum(int valeurParam) {
        valeur = valeurParam;
    }

    public int getValeur() {
        return valeur;
    }
}
