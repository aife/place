package fr.atexo.rsem.noyau.ws.beans;

/**
 * Bean utilisé pour l'echange d'informations avec des entites externes a RSEM.
 *
 * @author RDA
 * @version $Revision$, $Date$, $Author$
 */
public class ConsolidePrixBean {

    private double consolideHTForfaitaire;
    private double consolideTTCForfaitaire;
    private double consolideHTUnitaire;
    private double consolideTTCUnitaire;

    private double consolidePfMn;
    private double consolideBcHTMin;
    private double consolideBcHTMax;
    private double consolideBcTTCMin;
    private double consolideBcTTCMax;


    /**
     * @return the consolideHTForfaitaire
     */
    public double getConsolideHTForfaitaire() {
        return consolideHTForfaitaire;
    }

    /**
     * @param consolideHTForfaitaire the consolideHTForfaitaire to set
     */
    public void setConsolideHTForfaitaire(double valeur) {
        this.consolideHTForfaitaire = valeur;
    }

    /**
     * @return the consolideTTCForfaitaire
     */
    public double getConsolideTTCForfaitaire() {
        return consolideTTCForfaitaire;
    }

    /**
     * @param consolideTTCForfaitaire the consolideTTCForfaitaire to set
     */
    public void setConsolideTTCForfaitaire(double valeur) {
        this.consolideTTCForfaitaire = valeur;
    }

    /**
     * @return the consolideHTUnitaire
     */
    public double getConsolideHTUnitaire() {
        return consolideHTUnitaire;
    }

    /**
     * @param consolideHTUnitaire the consolideHTUnitaire to set
     */
    public void setConsolideHTUnitaire(double valeur) {
        this.consolideHTUnitaire = valeur;
    }

    /**
     * @return the consolideHTUnitaire
     */
    public double getConsolideTTCUnitaire() {
        return consolideTTCUnitaire;
    }

    /**
     * @param consolideHTUnitaire the consolideHTUnitaire to set
     */
    public void setConsolideTTCUnitaire(double valeur) {
        this.consolideTTCUnitaire = valeur;
    }

    /**
     * @return the consolidePfMn
     */
    public double getConsolidePfMn() {
        return consolidePfMn;
    }

    /**
     * @param consolidePfMn the consolidePfMn to set
     */
    public void setConsolidePfMn(double valeur) {
        this.consolidePfMn = valeur;
    }

    /**
     * @return the consolideBcHTMin
     */
    public double getConsolideBcHTMin() {
        return consolideBcHTMin;
    }

    /**
     * @param consolideBcHTMin the consolideBcHTMin to set
     */
    public void setConsolideBcHTMin(double consolideBcHTMin) {
        this.consolideBcHTMin = consolideBcHTMin;
    }

    /**
     * @return the consolideBcHTMax
     */
    public double getConsolideBcHTMax() {
        return consolideBcHTMax;
    }

    /**
     * @param consolideBcHTMax the consolideBcHTMax to set
     */
    public void setConsolideBcHTMax(double consolideBcHTMax) {
        this.consolideBcHTMax = consolideBcHTMax;
    }

    /**
     * @return the consolideBcTTCMin
     */
    public double getConsolideBcTTCMin() {
        return consolideBcTTCMin;
    }

    /**
     * @param consolideBcTTCMin the consolideBcTTCMin to set
     */
    public void setConsolideBcTTCMin(double consolideBcTTCMin) {
        this.consolideBcTTCMin = consolideBcTTCMin;
    }

    /**
     * @return the consolideBcTTCMax
     */
    public double getConsolideBcTTCMax() {
        return consolideBcTTCMax;
    }

    /**
     * @param consolideBcTTCMax the consolideBcTTCMax to set
     */
    public void setConsolideBcTTCMax(double consolideBcTTCMax) {
        this.consolideBcTTCMax = consolideBcTTCMax;
    }

    @Override
    public String toString() {
        return "ConsolidePrixBean{" + "consolideHTForfaitaire=" + consolideHTForfaitaire + ", consolideTTCForfaitaire=" + consolideTTCForfaitaire + ", consolideHTUnitaire=" + consolideHTUnitaire + ", consolideTTCUnitaire=" + consolideTTCUnitaire + ", consolidePfMn=" + consolidePfMn + ", consolideBcHTMin=" + consolideBcHTMin + ", consolideBcHTMax=" + consolideBcHTMax + ", consolideBcTTCMin=" + consolideBcTTCMin + ", consolideBcTTCMax=" + consolideBcTTCMax + '}';
    }
}
