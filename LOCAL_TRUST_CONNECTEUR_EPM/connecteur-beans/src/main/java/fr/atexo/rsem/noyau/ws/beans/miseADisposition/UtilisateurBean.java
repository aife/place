/**
 *
 */
package fr.atexo.rsem.noyau.ws.beans.miseADisposition;


public class UtilisateurBean {

    /**
     * login de l'utilisateur
     */
    private String login;

    /**
     * nom de l'utilisateur
     */
    private String nom;

    /**
     * prénom de l'utilisateur
     */
    private String prenom;

    private String email;


    public String getNom() {
        return nom;
    }

    public void setNom(final String valeur) {
        this.nom = valeur;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(final String valeur) {
        this.prenom = valeur;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "UtilisateurBean{" + "login='" + login + '\'' + ", nom='" + nom + '\'' + ", prenom='" + prenom + '\'' + ", email='" + email + '\'' + '}';
    }
}
