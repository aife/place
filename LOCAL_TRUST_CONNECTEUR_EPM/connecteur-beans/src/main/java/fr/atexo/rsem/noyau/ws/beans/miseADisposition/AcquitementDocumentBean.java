/**
 *
 */
package fr.atexo.rsem.noyau.ws.beans.miseADisposition;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author KBE
 */
@XmlRootElement(name = "document")
public class AcquitementDocumentBean {

    private String numeroConsultation;
    private String numeroContrat;
    private String nomDeFichier;
    private String message;
    private String type;

    public AcquitementDocumentBean() {
    }

    public String getNumeroConsultation() {
        return numeroConsultation;
    }

    public void setNumeroConsultation(String numeroConsultation) {
        this.numeroConsultation = numeroConsultation;
    }

    public String getNumeroContrat() {
        return numeroContrat;
    }

    public void setNumeroContrat(String numeroContrat) {
        this.numeroContrat = numeroContrat;
    }

    public String getNomDeFichier() {
        return nomDeFichier;
    }

    public void setNomDeFichier(String nomDeFichier) {
        this.nomDeFichier = nomDeFichier;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "AcquitementDocumentBean{" + "numeroConsultation='" + numeroConsultation + '\'' + ", numeroContrat='" + numeroContrat + '\'' + ", nomDeFichier='" + nomDeFichier + '\'' + ", message='" + message + '\'' + ", type='" + type + '\'' + '}';
    }
}
