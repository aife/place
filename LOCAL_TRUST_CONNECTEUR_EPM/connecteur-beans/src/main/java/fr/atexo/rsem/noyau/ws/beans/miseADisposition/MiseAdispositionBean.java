package fr.atexo.rsem.noyau.ws.beans.miseADisposition;

public class MiseAdispositionBean {

    /**
     * id de la mise à disposition
     */
    private String id;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "MiseAdispositionBean{" + "id='" + id + '\'' + '}';
    }
}
