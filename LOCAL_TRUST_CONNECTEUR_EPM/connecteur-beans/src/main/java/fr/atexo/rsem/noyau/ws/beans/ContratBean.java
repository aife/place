
package fr.atexo.rsem.noyau.ws.beans;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.Date;
import java.util.List;

/**
 * Bean répresentant les informations d'un contrat. Ce
 * bean est utilisé par le Webservice REST pour échanges des données du contrat
 * d'EPM avec des applications tiers
 *
 * @author RDA
 * @version $Revision$, $Date$, $Author$
 */
public class ContratBean extends ContratSimpleBean {

    /**
     * Date de demande de legalité lors de l'attribution du marché
     */
    private Date dateDemandeLegalite;
    /**
     * Date de signature lors de l'attribution du marché
     */
    private Date dateSignature;

    /**
     * Le prix estimé lors de la définition des données de la consultation
     * (formulaire amont)
     */
    private EstimationPrixBean estimationPrix;

    private String numeroLot;

    private Boolean marcheABonDeCommande;

    private List<TrancheBean> tranches;

    private List<LotTechniqueBean> lotsTechniques;
    private Integer nombreReconductions;
    private String modalitesReconduction;
    private String variantesTechniques;

    private Integer idMiseADispositionExecution;

    /**
     * Type d'attributaire : Groupement, Solidaire
     */
    private String typeAttributaire;

    /**
     * @return the dateDemandeLegalite
     */
    public Date getDateDemandeLegalite() {
        return dateDemandeLegalite;
    }

    /**
     * @param dateDemandeLegalite the dateDemandeLegalite to set
     */
    public void setDateDemandeLegalite(Date valeur) {
        this.dateDemandeLegalite = valeur;
    }

    /**
     * @return the dateSignature
     */
    public Date getDateSignature() {
        return dateSignature;
    }

    /**
     * @param dateSignature the dateSignature to set
     */
    public void setDateSignature(Date valeur) {
        this.dateSignature = valeur;
    }

    /**
     * @return the estimationPrix
     */
    public EstimationPrixBean getEstimationPrix() {
        return estimationPrix;
    }

    /**
     * @param estimationPrix the estimationPrix to set
     */
    public void setEstimationPrix(EstimationPrixBean valeur) {
        this.estimationPrix = valeur;
    }

    /**
     * @return the numeroLot
     */
    public String getNumeroLot() {
        return numeroLot;
    }

    /**
     * @param numeroLot the numeroLot to set
     */
    public void setNumeroLot(String valeur) {
        this.numeroLot = valeur;
    }

    /**
     * @return the marcheABonDeCommande
     */
    public Boolean getMarcheABonDeCommande() {
        return marcheABonDeCommande;
    }

    /**
     * @param marcheABonDeCommande the marcheABonDeCommande to set
     */
    public void setMarcheABonDeCommande(Boolean valeur) {
        this.marcheABonDeCommande = valeur;
    }

    /**
     * @return the tranches
     */
    @XmlElementWrapper(name = "tranches")
    @XmlElement(name = "tranche")
    public List<TrancheBean> getTranches() {
        return tranches;
    }

    /**
     * @param tranches the tranches to set
     */
    public void setTranches(List<TrancheBean> valeur) {
        this.tranches = valeur;
    }

    /**
     * @return the lotsTechniques
     */
    @XmlElementWrapper(name = "lotsTechniques")
    @XmlElement(name = "lotTechnique")
    public List<LotTechniqueBean> getLotsTechniques() {
        return lotsTechniques;
    }

    /**
     * @param lotsTechniques the lotsTechniques to set
     */
    public void setLotsTechniques(List<LotTechniqueBean> valeur) {
        this.lotsTechniques = valeur;
    }

    /**
     * @return the typeAttributaire
     */
    public String getTypeAttributaire() {
        return typeAttributaire;
    }

    /**
     * @param typeAttributaire the typeAttributaire to set
     */
    public void setTypeAttributaire(String valeur) {
        this.typeAttributaire = valeur;
    }

    /**
     * @return the nombreReconductions
     */
    public Integer getNombreReconductions() {
        return nombreReconductions;
    }

    /**
     * @param nombreReconductions the nombreReconductions to set
     */
    public void setNombreReconductions(Integer valeur) {
        this.nombreReconductions = valeur;
    }

    /**
     * @return the modalitesReconduction
     */
    public String getModalitesReconduction() {
        return modalitesReconduction;
    }

    /**
     * @param modalitesReconduction the modalitesReconduction to set
     */
    public void setModalitesReconduction(String valeur) {
        this.modalitesReconduction = valeur;
    }

    /**
     * @return the variantesTechniques
     */
    public String getVariantesTechniques() {
        return variantesTechniques;
    }

    /**
     * @param variantesTechniques the variantesTechniques to set
     */
    public void setVariantesTechniques(String valeur) {
        this.variantesTechniques = valeur;
    }

    public Integer getIdMiseADispositionExecution() {
        return idMiseADispositionExecution;
    }

    public void setIdMiseADispositionExecution(final Integer valeur) {
        this.idMiseADispositionExecution = valeur;
    }

    @Override
    public String toString() {
        return "ContratBean{" + "dateDemandeLegalite=" + dateDemandeLegalite + ", dateSignature=" + dateSignature + ", estimationPrix=" + estimationPrix + ", numeroLot='" + numeroLot + '\'' + ", marcheABonDeCommande=" + marcheABonDeCommande + ", tranches=" + tranches + ", lotsTechniques=" + lotsTechniques + ", nombreReconductions=" + nombreReconductions + ", modalitesReconduction='" + modalitesReconduction + '\'' + ", variantesTechniques='" + variantesTechniques + '\'' + ", idMiseADispositionExecution=" + idMiseADispositionExecution + ", typeAttributaire='" + typeAttributaire + '\'' + '}';
    }
}
