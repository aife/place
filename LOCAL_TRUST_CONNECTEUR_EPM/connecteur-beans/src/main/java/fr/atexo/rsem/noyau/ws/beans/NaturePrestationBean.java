
package fr.atexo.rsem.noyau.ws.beans;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * Bean utilisé pour l'echange d'informations avec des entites externes a RSEM.
 *
 * @author RDA
 * @version $Revision$, $Date$, $Author$
 */
@XmlType(name = "naturePrestation")
@XmlEnum
public enum NaturePrestationBean {

    @XmlEnumValue("Travail")
    TRAVAIL("Travail"),
    @XmlEnumValue("Fourniture")
    FOURNITURE("Fourniture"),
    @XmlEnumValue("Service")
    SERVICE("Service");
    private String value;

    NaturePrestationBean(String v) {
        value = v;
    }

    public static NaturePrestationBean fromValue(String v) {
        for (NaturePrestationBean c : NaturePrestationBean.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return "NaturePrestationBean{" + "value='" + value + '\'' + '}';
    }
}
