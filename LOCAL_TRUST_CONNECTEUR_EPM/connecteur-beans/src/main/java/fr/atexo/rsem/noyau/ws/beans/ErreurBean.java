package fr.atexo.rsem.noyau.ws.beans;


import javax.xml.bind.annotation.XmlElement;

/**
 * Utilise pour l'echange d'informations avec des entites externes a RSEM.
 * Envoye pour decrire une erreur.
 *
 * @author RVI
 * @version $Revision$, $Date$, $Author$
 */
public class ErreurBean extends ResponseBean {

    @XmlElement(name = "code")
    private int code;

    public void setCodeMessage(ErreurEnum valeurCode, String valeurMessage) {
        code = valeurCode.getValeur();
        message = valeurMessage;
    }

    private String message;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String valeur) {
        this.message = valeur;
    }

    @Override
    public String toString() {
        return "ErreurBean{" + "code=" + code + ", message='" + message + '\'' + '}';
    }
}
