package fr.atexo.rsem.noyau.ws.beans;

import javax.xml.bind.annotation.XmlElement;

/**
 * Utilise pour l'echange d'informations avec des entites externes a RSEM.
 *
 * @author RVI
 * @version $Revision$, $Date$, $Author$
 */
public class AdresseBean {

    private String adresse;

    private String codePostal;

    private String ville;

    private String telephone;

    private String fax;

    private String email;

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(final String valeur) {
        this.adresse = valeur;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(final String valeur) {
        this.codePostal = valeur;
    }

    @XmlElement(name = "commune")
    public String getVille() {
        return ville;
    }

    public void setVille(final String valeur) {
        this.ville = valeur;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(final String valeur) {
        this.telephone = valeur;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(final String valeur) {
        this.fax = valeur;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String valeur) {
        this.email = valeur;
    }

    @Override
    public String toString() {
        return "AdresseBean{" + "adresse='" + adresse + '\'' + ", codePostal='" + codePostal + '\'' + ", ville='" + ville + '\'' + ", telephone='" + telephone + '\'' + ", fax='" + fax + '\'' + ", email='" + email + '\'' + '}';
    }
}
