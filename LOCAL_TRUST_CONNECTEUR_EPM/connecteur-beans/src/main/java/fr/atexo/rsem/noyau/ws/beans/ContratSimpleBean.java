package fr.atexo.rsem.noyau.ws.beans;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.Date;
import java.util.List;

public class ContratSimpleBean {

    /**
     * Le numéro du contrat = reference du marché
     */
    private String numeroContrat;

    private Date dateNotificationMarche;

    /**
     * L'objet du contrat : composé par l'intitulé de la consultation, en cas
     * d'une consultation non alloti. Sinon, dans le cas d'une consultation
     * alloti, ce champ sera composé de l'intitulé de la consultation + intitulé
     * du lot
     */
    private String objetContrat;

    /**
     * Type de contrat : DSP, Accord cadre, Marché
     */
    private String typeContrat;

    /**
     * Le prix consolidé est associé au marché lors de l'attribution
     */
    private ConsolidePrixBean consolidePrix;

    private String intituleLot;

    private List<AttributaireBean> attributaires;

    /**
     * @return the numeroContrat
     */
    public String getNumeroContrat() {
        return numeroContrat;
    }

    /**
     * @param numeroContrat the numeroContrat to set
     */
    public void setNumeroContrat(String valeur) {
        this.numeroContrat = valeur;
    }

    /**
     * @return the objetContrat
     */
    public String getObjetContrat() {
        return objetContrat;
    }

    /**
     * @param objetContrat the objetContrat to set
     */
    public void setObjetContrat(String valeur) {
        this.objetContrat = valeur;
    }

    /**
     * @return the typeContrat
     */
    public String getTypeContrat() {
        return typeContrat;
    }

    /**
     * @param typeContrat the typeContrat to set
     */
    public void setTypeContrat(String valeur) {
        this.typeContrat = valeur;
    }

    /**
     * @return the consolidePrix
     */
    public ConsolidePrixBean getConsolidePrix() {
        return consolidePrix;
    }

    /**
     * @param consolidePrix the consolidePrix to set
     */
    public void setConsolidePrix(ConsolidePrixBean valeur) {
        this.consolidePrix = valeur;
    }

    /**
     * @return the intituleLot
     */
    public String getIntituleLot() {
        return intituleLot;
    }

    /**
     * @param intituleLot the intituleLot to set
     */
    public void setIntituleLot(String valeur) {
        this.intituleLot = valeur;
    }

    /**
     * @return the attributaire
     */
    @XmlElementWrapper(name = "attributaires")
    @XmlElement(name = "attributaire")
    public List<AttributaireBean> getAttributaires() {
        return attributaires;
    }

    /**
     * @param attributaire the attributaire to set
     */
    public void setAttributaires(List<AttributaireBean> valeur) {
        this.attributaires = valeur;
    }

    /**
     * @return the dateNotificationMarche
     */
    public Date getDateNotificationMarche() {
        return dateNotificationMarche;
    }

    /**
     * @param dateNotificationMarche the dateNotificationMarche to set
     */
    public void setDateNotificationMarche(Date valeur) {
        this.dateNotificationMarche = valeur;
    }

    @Override
    public String toString() {
        return "ContratSimpleBean{" + "numeroContrat='" + numeroContrat + '\'' + ", dateNotificationMarche=" + dateNotificationMarche + ", objetContrat='" + objetContrat + '\'' + ", typeContrat='" + typeContrat + '\'' + ", consolidePrix=" + consolidePrix + ", intituleLot='" + intituleLot + '\'' + ", attributaires=" + attributaires + '}';
    }
}
