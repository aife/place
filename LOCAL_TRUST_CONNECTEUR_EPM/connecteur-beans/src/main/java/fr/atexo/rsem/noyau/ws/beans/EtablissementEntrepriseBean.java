package fr.atexo.rsem.noyau.ws.beans;


/**
 * Bean répresentant les informations de l'établissement lié à un contractant d'un contrat. Ce
 * bean est utilisé par le Webservice REST pour échanges des données du contrat
 * d'EPM avec des applications tiers
 *
 * @author rebeca
 */
public class EtablissementEntrepriseBean {

    /**
     * id dans le noyau
     */
    Integer id;

    /**
     * numéro de son téléphone fixe.
     */
    private String telephoneFixe;

    /**
     * Numero voie, par exemple '12'.
     */
    private String numeroVoie;

    /**
     * Nom de la voie, par exemple 'rue royale'
     */
    private String nomVoie;

    /**
     * addresse de l'établissement.
     */
    private String complementAdresse;

    /**
     * code postal de l'établissement.
     */
    private String codePostal;

    /**
     * ville de l'établissement.
     */
    private String ville;

    /**
     * Cedex de l'établissement.
     */
    private String cedex;

    /**
     * Code du tier dans l'interface finance du client.
     */
    private String codeTierFinance;

    /**
     * Code de l'établissement.
     */
    private String codeEtablissement;

    /**
     * Type de l'établissement dans l'entreprise (siége ou autre).
     */

    private String typeEtablissement;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return téléphone fixe de la personne
     */
    public String getTelephoneFixe() {
        return telephoneFixe;
    }


    /**
     * @param valeur téléphone fixe de la personne
     */
    public void setTelephoneFixe(final String valeur) {
        this.telephoneFixe = valeur;
    }

    /**
     * @return code postal associé a l'adresse
     */
    public String getCodePostal() {
        return codePostal;
    }

    /**
     * @param valeur code postal associé à l'adresse
     */
    public void setCodePostal(final String valeur) {
        this.codePostal = valeur;
    }

    /**
     * @return ville associé à l'adresse
     */
    public String getVille() {
        return ville;
    }

    /**
     * @param valeur ville associé à l'adresse
     */
    public void setVille(final String valeur) {
        this.ville = valeur;
    }

    /**
     * @return le code de l'établissement.
     */
    public String getCodeEtablissement() {
        return codeEtablissement;
    }

    /**
     * @param valeur code de l'établissement
     */
    public void setCodeEtablissement(String valeur) {
        this.codeEtablissement = valeur;
    }

    public String getNumeroVoie() {
        return numeroVoie;
    }

    public void setNumeroVoie(final String valeur) {
        this.numeroVoie = valeur;
    }

    public String getNomVoie() {
        return nomVoie;
    }

    public void setNomVoie(final String valeur) {
        this.nomVoie = valeur;
    }

    public String getComplementAdresse() {
        return complementAdresse;
    }

    public void setComplementAdresse(final String valeur) {
        this.complementAdresse = valeur;
    }

    public String getCedex() {
        return cedex;
    }

    public void setCedex(final String valeur) {
        this.cedex = valeur;
    }

    public String getCodeTierFinance() {
        return codeTierFinance;
    }

    public void setCodeTierFinance(final String valeur) {
        this.codeTierFinance = valeur;
    }

    public String getTypeEtablissement() {
        return typeEtablissement;
    }

    public void setTypeEtablissement(final String valeur) {
        this.typeEtablissement = valeur;
    }

    /**
     * @return numéro_voie +' '+nom_voie
     */
    public String getAdresseFormatee() {
        return numeroVoie + ' ' + nomVoie;
    }

    @Override
    public String toString() {
        return "EtablissementEntrepriseBean{" + "id=" + id + ", telephoneFixe='" + telephoneFixe + '\'' + ", numeroVoie='" + numeroVoie + '\'' + ", nomVoie='" + nomVoie + '\'' + ", complementAdresse='" + complementAdresse + '\'' + ", codePostal='" + codePostal + '\'' + ", ville='" + ville + '\'' + ", cedex='" + cedex + '\'' + ", codeTierFinance='" + codeTierFinance + '\'' + ", codeEtablissement='" + codeEtablissement + '\'' + ", typeEtablissement='" + typeEtablissement + '\'' + '}';
    }
}

