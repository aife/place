package fr.atexo.rsem.noyau.ws.beans;

/**
 * Bean répresentant les informations du lot technique d'une consultation. Ce
 * bean est utilisé par le Webservice REST pour échanges des données du contrat
 * d'EPM avec des applications tiers
 *
 * @author Rebeca Dantas
 */
public class LotTechniqueBean {

    private String identifiantLot;

    private String intituleLot;

    private boolean lotPrincipal;

    /**
     * @return the identifiantLot
     */
    public String getIdentifiantLot() {
        return identifiantLot;
    }

    /**
     * @param identifiantLot the identifiantLot to set
     */
    public void setIdentifiantLot(String valeur) {
        this.identifiantLot = valeur;
    }

    /**
     * @return the intituleLot
     */
    public String getIntituleLot() {
        return intituleLot;
    }

    /**
     * @param intituleLot the intituleLot to set
     */
    public void setIntituleLot(String valeur) {
        this.intituleLot = valeur;
    }

    /**
     * @return the lotPrincipal
     */
    public boolean isLotPrincipal() {
        return lotPrincipal;
    }

    /**
     * @param lotPrincipal the lotPrincipal to set
     */
    public void setLotPrincipal(boolean valeur) {
        this.lotPrincipal = valeur;
    }

    @Override
    public String toString() {
        return "LotTechniqueBean{" + "identifiantLot='" + identifiantLot + '\'' + ", intituleLot='" + intituleLot + '\'' + ", lotPrincipal=" + lotPrincipal + '}';
    }
}
