package fr.atexo.rsem.noyau.ws.beans;

/**
 * Utilise pour l'echange d'informations avec des entites externes a RSEM.
 *
 * @author RVI
 * @version $Revision$, $Date$, $Author$
 */
public class DepotBean {

    private EntrepriseBean entreprise;

    public EntrepriseBean getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(final EntrepriseBean valeur) {
        this.entreprise = valeur;
    }

    @Override
    public String toString() {
        return "DepotBean{" + "entreprise=" + entreprise + '}';
    }
}
