/**
 *
 */
package fr.atexo.rsem.noyau.ws.beans.miseADisposition;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author khaled.benari
 */
@XmlRootElement(name = "resultat")
public class MisADispositionAcquitementBean {
    private String response;

    /**
     * @return the response
     */
    @XmlElement(name = "response")
    public String getResponse() {
        return response;
    }

    /**
     * @param response the response to set
     */
    public void setResponse(String valeur) {
        this.response = valeur;
    }

    @Override
    public String toString() {
        return "MisADispositionAcquitementBean{" + "response='" + response + '\'' + '}';
    }
}
