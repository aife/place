/**
 *
 */
package fr.atexo.rsem.noyau.ws.beans.miseADisposition;

import java.util.Date;

/**
 * @author KHALED BENARI
 */
public class ConsultationMiseAdispositionBean {

    private String naturePrestation;
    private String statut;
    private String reference;
    private String intitule;
    private String codeProcedure;
    private String libelleDirectionResponsable;
    private String codeDirectionResponsable;
    private String libelleDirectionBeneficaire;
    private String codeDirectionBeneficaire;
    private Date datePublicite;
    private String numeroConsultationInitiale;
    private String numeroConsultation;
    private String objet;
    private Integer dureeMarche;
    private boolean groupementCommande;
    private boolean marcheCloture;
    private String pouvoirAdjudicateur;
    private String uniteDureeDuMarche;
    private fr.atexo.rsem.noyau.ws.beans.miseADisposition.UtilisateurBean responsable;

    /**
     * @return the naturePrestation
     */
    public String getNaturePrestation() {
        return naturePrestation;
    }

    /**
     * @param naturePrestation the naturePrestation to set
     */
    public void setNaturePrestation(String naturePrestation) {
        this.naturePrestation = naturePrestation;
    }

    /**
     * @return the statut
     */
    public String getStatut() {
        return statut;
    }

    /**
     * @param statut the statut to set
     */
    public void setStatut(String statut) {
        this.statut = statut;
    }

    /**
     * @return the reference
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param reference the reference to set
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getCodeProcedure() {
        return codeProcedure;
    }

    public void setCodeProcedure(String codeProcedure) {
        this.codeProcedure = codeProcedure;
    }

    public String getLibelleDirectionResponsable() {
        return libelleDirectionResponsable;
    }

    public void setLibelleDirectionResponsable(String libelleDirectionResponsable) {
        this.libelleDirectionResponsable = libelleDirectionResponsable;
    }

    public String getCodeDirectionResponsable() {
        return codeDirectionResponsable;
    }

    public void setCodeDirectionResponsable(String codeDirectionResponsable) {
        this.codeDirectionResponsable = codeDirectionResponsable;
    }

    public String getLibelleDirectionBeneficaire() {
        return libelleDirectionBeneficaire;
    }

    public void setLibelleDirectionBeneficaire(String libelleDirectionBeneficaire) {
        this.libelleDirectionBeneficaire = libelleDirectionBeneficaire;
    }

    public String getCodeDirectionBeneficaire() {
        return codeDirectionBeneficaire;
    }

    public void setCodeDirectionBeneficaire(String codeDirectionBeneficaire) {
        this.codeDirectionBeneficaire = codeDirectionBeneficaire;
    }

    public Date getDatePublicite() {
        return datePublicite;
    }

    public void setDatePublicite(Date datePublicite) {
        this.datePublicite = datePublicite;
    }

    public String getNumeroConsultationInitiale() {
        return numeroConsultationInitiale;
    }

    public void setNumeroConsultationInitiale(String numeroConsultationInitiale) {
        this.numeroConsultationInitiale = numeroConsultationInitiale;
    }

    public String getNumeroConsultation() {
        return numeroConsultation;
    }

    public void setNumeroConsultation(String numeroConsultation) {
        this.numeroConsultation = numeroConsultation;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public Integer getDureeMarche() {
        return dureeMarche;
    }

    public void setDureeMarche(Integer dureeMarche) {
        this.dureeMarche = dureeMarche;
    }

    public boolean isGroupementCommande() {
        return groupementCommande;
    }

    public void setGroupementCommande(boolean groupementCommande) {
        this.groupementCommande = groupementCommande;
    }

    public boolean isMarcheCloture() {
        return marcheCloture;
    }

    public void setMarcheCloture(boolean marcheCloture) {
        this.marcheCloture = marcheCloture;
    }

    public String getPouvoirAdjudicateur() {
        return pouvoirAdjudicateur;
    }

    public void setPouvoirAdjudicateur(String pouvoirAdjudicateur) {
        this.pouvoirAdjudicateur = pouvoirAdjudicateur;
    }

    public String getUniteDureeDuMarche() {
        return uniteDureeDuMarche;
    }

    public void setUniteDureeDuMarche(String uniteDureeDuMarche) {
        this.uniteDureeDuMarche = uniteDureeDuMarche;
    }

    public fr.atexo.rsem.noyau.ws.beans.miseADisposition.UtilisateurBean getResponsable() {
        return responsable;
    }

    public void setResponsable(UtilisateurBean responsable) {
        this.responsable = responsable;
    }

    @Override
    public String toString() {
        return "ConsultationMiseAdispositionBean{" + "naturePrestation='" + naturePrestation + '\'' + ", statut='" + statut + '\'' + ", reference='" + reference + '\'' + ", intitule='" + intitule + '\'' + ", codeProcedure='" + codeProcedure + '\'' + ", libelleDirectionResponsable='" + libelleDirectionResponsable + '\'' + ", codeDirectionResponsable='" + codeDirectionResponsable + '\'' + ", libelleDirectionBeneficaire='" + libelleDirectionBeneficaire + '\'' + ", codeDirectionBeneficaire='" + codeDirectionBeneficaire + '\'' + ", datePublicite=" + datePublicite + ", numeroConsultationInitiale='" + numeroConsultationInitiale + '\'' + ", numeroConsultation='" + numeroConsultation + '\'' + ", objet='" + objet + '\'' + ", dureeMarche=" + dureeMarche + ", groupementCommande=" + groupementCommande + ", marcheCloture=" + marcheCloture + ", pouvoirAdjudicateur='" + pouvoirAdjudicateur + '\'' + ", uniteDureeDuMarche='" + uniteDureeDuMarche + '\'' + ", responsable=" + responsable + '}';
    }
}
