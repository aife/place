package fr.atexo.rsem.noyau.ws.beans;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Bean répresentant les informations d'une consultation attribué. Ce bean est
 * utilisé par le Webservice REST pour échanges des données du contrat d'EPM
 * avec des applications tiers
 *
 * @author RVI / RDA
 * @version $Revision$, $Date$, $Author$
 */
@XmlRootElement(name = "consultation")
public class ConsultationBean extends ProcedureBean {

    private String numeroConsultation;

    /**
     * Date de lancement de la consultation
     */
    private Date dateLancement;
    /**
     * Date de publicité de la consultation
     */
    private Date datePublicite;

    private NaturePrestationBean naturePrestation;
    /**
     * Valeur de la durée du marché
     */
    private Integer valeurDureeDuMarche;
    /**
     * Code externe de l'unité de durée du marché : J/M/A
     */
    private String uniteDureeDuMarche;

    /**
     * Ensemble de depots de la consultation
     */
    private List<DepotBean> depotList = new ArrayList<DepotBean>();

    /**
     * Nombre de depots electroniques
     */
    private String nombreDepotElectronique;
    /**
     * Nombre de depots
     */
    private String nombreTotalDepot;

    /**
     * True si marché clôturé
     */
    private Boolean marcheCloture;
    /**
     * True si procédure en phases sucessives
     */
    private Boolean marchePhaseSuccessive;
    /**
     * True si consultation possède de clause environammentale
     */
    private Boolean marcheAClauseEnvironnementale;
    /**
     * True si consultation possède de clause sociale
     */
    private Boolean marcheAClauseSociale;

    /**
     * Ensemble de contrats de la consultation
     */
    private List<ContratBean> contrats;

    private String numeroConsultationInitial;

    private String numeroContratInitial;

    /**
     * @return the numeroConsultation
     */
    public String getNumeroConsultation() {
        return numeroConsultation;
    }

    /**
     * @param numeroConsultation the numeroConsultation to set
     */
    public void setNumeroConsultation(String valeur) {
        this.numeroConsultation = valeur;
    }

    /**
     * @return the dateLancement
     */
    public Date getDateLancement() {
        return dateLancement;
    }

    /**
     * @param dateLancement the dateLancement to set
     */
    public void setDateLancement(Date valeur) {
        this.dateLancement = valeur;
    }

    /**
     * @return the datePublicite
     */
    public Date getDatePublicite() {
        return datePublicite;
    }

    /**
     * @param datePublicite the datePublicite to set
     */
    public void setDatePublicite(Date valeur) {
        this.datePublicite = valeur;
    }

    /**
     * @return the naturePrestation
     */
    public NaturePrestationBean getNaturePrestation() {
        return naturePrestation;
    }

    /**
     * @param naturePrestation the naturePrestation to set
     */
    public void setNaturePrestation(NaturePrestationBean valeur) {
        this.naturePrestation = valeur;
    }

    /**
     * @return the valeurDureeDuMarche
     */
    public Integer getValeurDureeDuMarche() {
        return valeurDureeDuMarche;
    }

    /**
     * @param valeurDureeDuMarche the valeurDureeDuMarche to set
     */
    public void setValeurDureeDuMarche(Integer valeur) {
        this.valeurDureeDuMarche = valeur;
    }

    /**
     * @return the uniteDureeDuMarche
     */
    public String getUniteDureeDuMarche() {
        return uniteDureeDuMarche;
    }

    /**
     * @param uniteDureeDuMarche the uniteDureeDuMarche to set
     */
    public void setUniteDureeDuMarche(String valeur) {
        this.uniteDureeDuMarche = valeur;
    }

    /**
     * @return the depotList
     */
    @XmlElement(name = "depot")
    @XmlElementWrapper(name = "depots")
    public List<DepotBean> getDepotList() {
        return depotList;
    }

    /**
     * @param depotList the depotList to set
     */
    public void setDepotList(List<DepotBean> valeur) {
        this.depotList = valeur;
    }

    /**
     * @return the nombreDepotElectronique
     */
    public String getNombreDepotElectronique() {
        return nombreDepotElectronique;
    }

    /**
     * @param nombreDepotElectronique the nombreDepotElectronique to set
     */
    public void setNombreDepotElectronique(String valeur) {
        this.nombreDepotElectronique = valeur;
    }

    /**
     * @return the nombreTotalDepot
     */
    public String getNombreTotalDepot() {
        return nombreTotalDepot;
    }

    /**
     * @param nombreTotalDepot the nombreTotalDepot to set
     */
    public void setNombreTotalDepot(String valeur) {
        this.nombreTotalDepot = valeur;
    }

    /**
     * @return the marcheCloture
     */
    public Boolean getMarcheCloture() {
        return marcheCloture;
    }

    /**
     * @param marcheCloture the marcheCloture to set
     */
    public void setMarcheCloture(Boolean valeur) {
        this.marcheCloture = valeur;
    }

    /**
     * @return the marchePhaseSuccessive
     */
    public Boolean getMarchePhaseSuccessive() {
        return marchePhaseSuccessive;
    }

    /**
     * @param marchePhaseSuccessive the marchePhaseSuccessive to set
     */
    public void setMarchePhaseSuccessive(Boolean valeur) {
        this.marchePhaseSuccessive = valeur;
    }

    /**
     * @return the marcheAClauseEnvironnementale
     */
    public Boolean getMarcheAClauseEnvironnementale() {
        return marcheAClauseEnvironnementale;
    }

    /**
     * @param marcheAClauseEnvironnementale the marcheAClauseEnvironnementale to set
     */
    public void setMarcheAClauseEnvironnementale(Boolean valeur) {
        this.marcheAClauseEnvironnementale = valeur;
    }

    /**
     * @return the marcheAClauseSociale
     */
    public Boolean getMarcheAClauseSociale() {
        return marcheAClauseSociale;
    }

    /**
     * @param marcheAClauseSociale the marcheAClauseSociale to set
     */
    public void setMarcheAClauseSociale(Boolean valeur) {
        this.marcheAClauseSociale = valeur;
    }

    /**
     * @return the contrats
     */
    @XmlElementWrapper(name = "contrats")
    @XmlElement(name = "contrat")
    public List<ContratBean> getContrats() {
        return contrats;
    }

    /**
     * @param contrats the contrats to set
     */
    public void setContrats(List<ContratBean> valeur) {
        this.contrats = valeur;
    }

    /**
     * @return le numero de la consultation initial = réference du marché initial en cas
     * de suite à accord cadre
     */
    public String getNumeroConsultationInitial() {
        return numeroConsultationInitial;
    }

    /**
     * @param numeroConsultationInitial : le numero de la consultation initial = réference du
     *                                  marché initial en cas de suite à accord cadre
     */
    public void setNumeroConsultationInitial(final String valeur) {
        this.numeroConsultationInitial = valeur;
    }

    /**
     * @return the numeroContratInitial
     */
    public String getNumeroContratInitial() {
        return numeroContratInitial;
    }

    /**
     * @param numeroContratInitial the numeroContratInitial to set
     */
    public void setNumeroContratInitial(String valeur) {
        this.numeroContratInitial = valeur;
    }

    @Override
    public String toString() {
        return "ConsultationBean{" + "numeroConsultation='" + numeroConsultation + '\'' + ", dateLancement=" + dateLancement + ", datePublicite=" + datePublicite + ", naturePrestation=" + naturePrestation + ", valeurDureeDuMarche=" + valeurDureeDuMarche + ", uniteDureeDuMarche='" + uniteDureeDuMarche + '\'' + ", depotList=" + depotList + ", nombreDepotElectronique='" + nombreDepotElectronique + '\'' + ", nombreTotalDepot='" + nombreTotalDepot + '\'' + ", marcheCloture=" + marcheCloture + ", marchePhaseSuccessive=" + marchePhaseSuccessive + ", marcheAClauseEnvironnementale=" + marcheAClauseEnvironnementale + ", marcheAClauseSociale=" + marcheAClauseSociale + ", contrats=" + contrats + ", numeroConsultationInitial='" + numeroConsultationInitial + '\'' + ", numeroContratInitial='" + numeroContratInitial + '\'' + '}';
    }
}
