package com.atexo.connecteur.rsemwsclient.ws.services.impl;

import com.atexo.connecteur.commun.config.ConfigurationService;
import com.atexo.connecteur.commun.exceptions.RsemClientException;
import fr.atexo.rsem.noyau.ws.beans.ContratBean;
import fr.atexo.rsem.noyau.ws.beans.ResponseBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.*;
import fr.paris.epm.noyau.rest.exceptions.RsemWsClientException;
import fr.paris.epm.noyau.rest.impl.RsemWsClientUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.zeroturnaround.zip.ZipUtil;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * @author KHALED BENARI
 * @since 29/07/2013
 */
@Service
public class RsemRESTWrapper {
    private static final String CONSULTATION = "/consultation";
    private static final String CONTRAT = "/contrat";
    private static final String URL_MISE_ADISPOSITION = "/miseAdisposition";
    private static final String URL_MISE_ADISPOSITION_ACQUITTEMENT = "/miseAdisposition/acquitter";
    private static final String LISTE_MISE_A_DISPOSITION_WEB_METHOD = "/listeMisADisposition";
    private static final String WEB_SERVICE_AUTHENTIFICATION = "/authentification";
    private static final String LISTE_DOCUMENT_MISE_A_DISPOSITION_WEB_METHOD = "/archive";
    private static final String CONNEXION = "/connexion";
    private static final String DECONNEXION = "/deconnexion";
    private static final String REST = "/rest";
    private static final String TICKET = "ticket";
    private static final String PARAMETERS = "parameters";
    private static final String API = "/api";
    private static final String DOCUMENTS = "/documents";
    private static final String UPLOAD = "/upload";
    private static final String SAVE = "/save";
    private static final String CONSULTATION_NUMBER = "numeroConsultation";
    private static final String CONTRACT_NUMBER = "numeroContrat";
    @Autowired
    ConfigurationService configuration;

    private Logger logger = LoggerFactory.getLogger(RsemRESTWrapper.class);

    private RestTemplate restTemplate = new RestTemplate();

    /**
     * appele un url du genre
     * http://localhost:8080/epm.noyau/rest/miseAdisposition
     * /listemiseadisposition?ticket=token
     *
     * @param token ticket d'authentification
     */
    public ListMiseAdispositionBean getMiseADispositionList(String token, String applicationTiers) throws RsemClientException {
        ListMiseAdispositionBean result = null;
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(configuration.getBaseUri()).path(REST).path(URL_MISE_ADISPOSITION).path(LISTE_MISE_A_DISPOSITION_WEB_METHOD).queryParam(TICKET, token).queryParam("applicationTiers", applicationTiers);
            HttpEntity<String> entity = new HttpEntity<>(PARAMETERS, getHeaders());
            result = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, ListMiseAdispositionBean.class).getBody();

        } catch (RestClientException me) {
            logger.warn("Impossible de parser la réponse", me.getMessage());
        } catch (Exception e) {
            logger.error("Erreur de connection avec le serveur RSEM", e);
        }
        return result;

    }

    /**
     * appele un url du genre
     * http://localhost:8080/epm.noyau/rest/miseAdisposition/archive/<numero
     * mise a disposition>
     */
    public ListDocumentsMiseAdispositionBean getDocumentsDeLaMiseADisposition(String miseAdisposition, String token) throws RsemClientException {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(configuration.getBaseUri()).path(REST).path(URL_MISE_ADISPOSITION).path(LISTE_DOCUMENT_MISE_A_DISPOSITION_WEB_METHOD).path("/").path(miseAdisposition).queryParam(TICKET, token);
            restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());
            HttpEntity<String> entity = new HttpEntity<>(PARAMETERS, getHeaders());
            byte[] response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, byte[].class).getBody();
            File archive = Files.createTempFile(miseAdisposition + "_", ".zip").toFile();
            Files.write(archive.toPath(), response);
            ListDocumentsMiseAdispositionBean listDocumentDeLaMisedispositionBean = unzip(archive, miseAdisposition);
            listDocumentDeLaMisedispositionBean.setArchive(archive);
            return listDocumentDeLaMisedispositionBean;
        } catch (Exception ex) {
            logger.error("Impossible de récupérer le zip", ex.getMessage());
            throw new RsemClientException(ex, "Impossible de récupérer le zip depuis RSEM");
        }
    }

    /**
     * cette methode construit le bean list ListDocumentDeLaMisedispositionBean
     * à partir du fichier XML inclus dans le zip
     *
     * @param fichierZip       fichiier zip contenant l'ensemble des fichier de la mise à disposition
     * @param miseAdisposition
     * @return ListDocumentDeLaMisedispositionBean
     * @throws Exception
     */

    public ListDocumentsMiseAdispositionBean unzip(File fichierZip, String miseAdisposition) throws RsemClientException {
        ListDocumentsMiseAdispositionBean listDocumentDeLaMisedispositionBean = null;
        File repertoireDestination = null;
        try {
            repertoireDestination = Files.createTempDirectory(miseAdisposition + "_").toFile();
        } catch (IOException e) {
            logger.error("Impossible de créer le répertoire temporaire pour la mise à disposition {}\n{}", miseAdisposition, e);
            throw new RsemClientException("Impossible de créer le répertoire temporaire");
        }
        ZipUtil.unpack(fichierZip, repertoireDestination);
        File xmlFile = null;
        List<File> zipFiles = Arrays.asList(repertoireDestination.listFiles());
        if (CollectionUtils.isEmpty(zipFiles)) throw new RsemClientException("Aucun fichier n'est présent pour la mise à disposition " + miseAdisposition);
        xmlFile = zipFiles.stream().filter(fichier -> fichier.getName().endsWith("_ListDocuments.xml")).findFirst().get();
        try {
            listDocumentDeLaMisedispositionBean = RsemWsClientUtils.parse(new FileInputStream(xmlFile), ListDocumentsMiseAdispositionBean.class);
            logger.info("contenu XML : \n{}", listDocumentDeLaMisedispositionBean);
        } catch (JAXBException | FileNotFoundException e) {
            logger.error("Impossible de parser le fichier de métadonnées pour la mise à disposition {}\n{}", miseAdisposition, e);
            throw new RsemClientException("Impossible de parser le fichier des métadonnées");
        }
        for (DocumentMiseAdispositionBean document : listDocumentDeLaMisedispositionBean.getDocumentsList()) {
            {
                Optional<File> corresponingFile = zipFiles.stream().filter(f -> f.getName().equals(document.getNomDeFichier())).findFirst();
                if (!corresponingFile.isPresent()) {
                    throw new RsemClientException("Fichier ZIP corrompi , aucune correspondance pour le fichier " + document.getNomDeFichier() + " dans la mise à disposition " + miseAdisposition);
                } else {
                    document.setPathFichier(corresponingFile.get());
                }
            }
        }
        return listDocumentDeLaMisedispositionBean;
    }

    /**
     * acquitte une mise a disposition au niveau de RSEM(non-Javadoc)
     *
     * @param acquitementMiseAdispositionBean
     * @return String
     * @throws RsemClientException
     */

    public AcquitementMiseAdispositionBean acquiteMiseADisposition(AcquitementMiseAdispositionBean acquitementMiseAdispositionBean, String token) throws RsemClientException {
        AcquitementMiseAdispositionBean result = null;
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(configuration.getBaseUri()).path(REST).path(URL_MISE_ADISPOSITION).path("/" + acquitementMiseAdispositionBean.getMiseAdisposition()).path("/" + acquitementMiseAdispositionBean.getStatut()).queryParam(TICKET, token);
            HttpEntity<String> entity = new HttpEntity<>(PARAMETERS, getHeaders());
            result = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, AcquitementMiseAdispositionBean.class).getBody();
            logger.info("la mise a disposition : {} a pour statut {}", acquitementMiseAdispositionBean.getMiseAdisposition(), acquitementMiseAdispositionBean.getStatut());
            return result;
        } catch (Exception e) {
            throw new RsemClientException("Impossible d'acquitter la mise à disposition " + acquitementMiseAdispositionBean.getMiseAdisposition());
        }
    }

    /**
     * acquitte une mise a disposition au niveau de RSEM(non-Javadoc)
     *
     * @param acquitementMiseAdispositionBean
     * @return String
     * @throws RsemClientException
     */

    public AcquitementMiseAdispositionBean acquiteMiseADispositionAvecDetails(AcquitementMiseAdispositionBean acquitementMiseAdispositionBean, String token) throws RsemClientException {
        AcquitementMiseAdispositionBean result = null;
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(configuration.getBaseUri()).path(REST).path(URL_MISE_ADISPOSITION_ACQUITTEMENT).path("/" + acquitementMiseAdispositionBean.getMiseAdisposition()).queryParam(TICKET, token);
            HttpEntity<AcquitementMiseAdispositionBean> entity = new HttpEntity<>(acquitementMiseAdispositionBean, getHeaders());
            result = restTemplate.exchange(builder.toUriString(), HttpMethod.POST, entity, AcquitementMiseAdispositionBean.class).getBody();
            logger.info("la mise a disposition : {} a pour statut {}", acquitementMiseAdispositionBean.getMiseAdisposition(), acquitementMiseAdispositionBean.getStatut());
            return result;
        } catch (Exception e) {
            logger.error("Impossible d'acquitter la mise à disposition", e);
            throw new RsemClientException("Impossible d'acquitter la mise à disposition " + acquitementMiseAdispositionBean.getMiseAdisposition());
        }
    }


    /**
     * appele un url du genre
     * http://rsem-integ/epm.noyau/rest/consultation/contrat/
     * numerocontrat/?ticket=token
     *
     * @param token ticket
     */
    public ContratBean getContrat(String token, String numContrat) throws RsemWsClientException {
        ResponseBean result;
        ContratBean contratBean = null;
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(configuration.getBaseUri()).path(REST).path(CONSULTATION).path(CONTRAT).path("/" + numContrat).queryParam(TICKET, token);
            HttpEntity<String> entity = new HttpEntity<>(PARAMETERS, getHeaders());
            result = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, ResponseBean.class).getBody();
            if (!CollectionUtils.isEmpty(result.getConsultations()) && !CollectionUtils.isEmpty(result.getConsultations().get(0).getContrats()))
                contratBean = result.getConsultations().get(0).getContrats().get(0);
            return contratBean;
        } catch (Exception e) {
            throw new RsemWsClientException(e, "Erreur lors de la récupération du CONTRAT " + numContrat);
        }
    }

    public TokenBean connexion(String loginUtilisateur, String motDePasse) throws RsemWsClientException {
        try {

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(configuration.getBaseUri()).path(REST).path(WEB_SERVICE_AUTHENTIFICATION).path(CONNEXION).path("/" + loginUtilisateur).path("/" + motDePasse);
            HttpEntity<String> entity = new HttpEntity<>(PARAMETERS, getHeaders());
            TokenBean retour = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, TokenBean.class).getBody();
            return retour;
        } catch (Exception ex) {
            logger.error("Impossible de récupérer le jeton d'authentification", ex);
            throw new RsemWsClientException(ex, "Impossible de récupérer le jeton d'authentification");
        }
    }

    public String deconnexion(String token) throws RsemWsClientException {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(configuration.getBaseUri()).path(REST).path(WEB_SERVICE_AUTHENTIFICATION).path(DECONNEXION).queryParam(TICKET, token);
            HttpEntity<String> entity = new HttpEntity<>(PARAMETERS, getHeaders());
            String retour = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, String.class).getBody();
            return retour;
        } catch (Exception e) {
            logger.error("Impossible de se déconnecter ", e);
            throw new RsemWsClientException(e, "Erreur de connection avec le serveur RSEM");
        }
    }

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_XML));
        return headers;
    }

    public Integer upload(String ticket, File fileToUpload) throws RsemWsClientException {
        try {
            MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
            bodyMap.add("file", new FileSystemResource(fileToUpload));
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);
            RestTemplate restTemplate = new RestTemplate();
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(configuration.getBaseUri()).path(API).path(DOCUMENTS).path(UPLOAD);
            ResponseEntity<Integer> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST, requestEntity, Integer.class);
            return response.getBody();
        } catch (Exception e) {
            logger.error("Impossible de sauvegarder le fichier ", e);
            throw new RsemWsClientException(e, "Erreur de connection avec le serveur RSEM");
        }
    }

    public Integer sauvegarderDocument(String ticket, Integer idFichierSurDisqueRSEM, String numeroConsultation, String numeroContrat, AcquitementDocumentBean acquittementDocumentBean) throws RsemWsClientException {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(configuration.getBaseUri()).path(API).path(DOCUMENTS).path(SAVE).path("/" + idFichierSurDisqueRSEM);
            if (numeroConsultation != null) {
                builder.queryParam(CONSULTATION_NUMBER, numeroConsultation);
            }
            if (numeroContrat != null) {
                builder.queryParam(CONTRACT_NUMBER, numeroContrat);
            }
            HttpEntity<AcquitementDocumentBean> entity = new HttpEntity<>(acquittementDocumentBean);
            Integer idDocument = restTemplate.exchange(builder.toUriString(), HttpMethod.POST, entity, Integer.class).getBody();
            return idDocument;
            //return restTemplate.exchange(builder.toUriString(), HttpMethod.POST, entity, Integer.class).getBody();
        } catch (Exception e) {
            logger.error("Impossible de sauvegarder les métadonnées ", e);
            throw new RsemWsClientException(e, "Erreur de sauvegarde des métadonnées");
        }
    }
}
