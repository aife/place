package com.atexo.connecteur.rsemwsclient;

import fr.atexo.rsem.noyau.ws.beans.ContratBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.AcquitementDocumentBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.AcquitementMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.ListDocumentsMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.MiseAdispositionBean;
import fr.paris.epm.noyau.rest.exceptions.RsemWsClientException;

import java.io.File;
import java.util.List;

public interface RsemWsClientFacade {
    List<MiseAdispositionBean> getMiseADispositionList();

    ListDocumentsMiseAdispositionBean getDocumentsByIdMiseADisposition(String miseADispositionId);

    ContratBean getContrat(String numeroContrat) throws RsemWsClientException;

    void acquittementMiseAdisposition(String idMiseADisposition, AcquitementMiseAdispositionBean acquitementDto);

    void deconnexion();

    Integer upload(File fileToUpload);

    Integer sauvegarderDocument(Integer idFichierSurDisqueRSEM, AcquitementDocumentBean acquittementDocumentBean);
}
