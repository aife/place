package com.atexo.connecteur.rsemwsclient.ws.services.impl;

import com.atexo.connecteur.commun.config.ConfigurationService;
import com.atexo.connecteur.commun.exceptions.RsemClientException;
import com.atexo.connecteur.rsemwsclient.RsemWsClientFacade;
import fr.atexo.rsem.noyau.ws.beans.ContratBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.*;
import fr.paris.epm.noyau.rest.exceptions.RsemWsClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Service
public class RsemWsClientFacadeImpl implements RsemWsClientFacade {

    @Autowired
    RsemRESTWrapper rsemRESTWrapper;

    @Autowired
    ConfigurationService configuration;
    // TODO tropuver une solution pour conserver le token
    private TokenBean tokenBean;

    private Logger logger = LoggerFactory.getLogger(RsemWsClientFacadeImpl.class);

    public List<MiseAdispositionBean> getMiseADispositionList() {
        List<MiseAdispositionBean> misesADisposition = new ArrayList<>();
        try {

            if (!CollectionUtils.isEmpty(configuration.getApplicationTiers())) {
                for (String applicationTiers : configuration.getApplicationTiers()) {
                    logger.info("traitement des mises à disposition pour le type d'application : {}", applicationTiers);
                    ListMiseAdispositionBean listMiseAdispositionBean = rsemRESTWrapper.getMiseADispositionList(getToken().getTicket(), applicationTiers);
                    if (listMiseAdispositionBean != null) {
                        List<MiseAdispositionBean> miseADispositionParApplication = listMiseAdispositionBean.getMiseAdispositionList();
                        misesADisposition.addAll(miseADispositionParApplication);
                    }
                }
            }
        } catch (RsemClientException e) {
            logger.error("Impossible de récupérer la liste des mise à disposition", e);
        }
        return misesADisposition;
    }

    public ListDocumentsMiseAdispositionBean getDocumentsByIdMiseADisposition(String miseADispositionId) {
        ListDocumentsMiseAdispositionBean miseAdispositionBean = null;
        try {
            logger.info("traitement des documents de la mise à disposition {}", miseAdispositionBean);
            miseAdispositionBean = rsemRESTWrapper.getDocumentsDeLaMiseADisposition(miseADispositionId, getToken().getTicket());
        } catch (RsemClientException e) {
            logger.error("Impossible de récupérer les documents pour la mise à disposition {}", miseADispositionId, e);
        }
        return miseAdispositionBean;

    }

    public ContratBean getContrat(String numeroContrat) throws RsemWsClientException {
        ContratBean contratBean = null;
        try {
            contratBean = rsemRESTWrapper.getContrat(getToken().getTicket(), numeroContrat);
            logger.info("Récupération du contrat N° {}", numeroContrat);
        } catch (RsemWsClientException e) {
            logger.error("Impossible de récupérer le contrat {}", numeroContrat, e);
            throw e;
        }
        return contratBean;
    }

    @Override
    public void acquittementMiseAdisposition(String idMiseADisposition, AcquitementMiseAdispositionBean acquitementDto) {
        try {
            acquitementDto.setMiseAdisposition(idMiseADisposition);
            rsemRESTWrapper.acquiteMiseADispositionAvecDetails(acquitementDto, getToken().getTicket());
            logger.info("Acquittement de la mise à disposition : {}", idMiseADisposition);
        } catch (RsemClientException e) {
            logger.error("Impossible d'acquitter la mise à disposition", e);
        }
    }

    @Override
    public void deconnexion() {
        if (tokenBean != null) {
            try {
                rsemRESTWrapper.deconnexion(tokenBean.getTicket());
                logger.info("Déconnexion OK");
            } catch (RsemWsClientException e) {
                logger.error("Impossible de se déconnecter", e);
            } finally {
                tokenBean = null;
            }
        }
    }

    @Override
    public Integer upload(File fileToUpload) {
        try {
            logger.info("Upload du fichier {}", fileToUpload);
            return rsemRESTWrapper.upload(getToken().getTicket(), fileToUpload);
        } catch (RsemWsClientException e) {
            logger.error("Impossible d'enregistrer le fichier provenant de l'application tierce", e);
            return null;
        }
    }

    @Override
    public Integer sauvegarderDocument(Integer idFichierSurDisqueRSEM, AcquitementDocumentBean acquittementDocumentBean) {
        try {
            logger.info("Sauvegarde du document  idFichierSurDisque : {} , metadonnées : {}", idFichierSurDisqueRSEM, acquittementDocumentBean);
            return rsemRESTWrapper.sauvegarderDocument(getToken().getTicket(), idFichierSurDisqueRSEM, acquittementDocumentBean.getNumeroConsultation(), acquittementDocumentBean.getNumeroContrat(), acquittementDocumentBean);
        } catch (RsemWsClientException e) {
            logger.error("Impossible d'enregistrer les métadonnées", e);
            return null;
        }

    }

    private TokenBean getToken() {
        if (tokenBean == null) {
            try {
                tokenBean = rsemRESTWrapper.connexion(configuration.getLogin(), configuration.getPassword());
            } catch (RsemWsClientException e) {
                logger.error("Impossible de récupérer un token depuis RSEM ou le token a expiré", e);
            }
        }
        return tokenBean;

    }
}
