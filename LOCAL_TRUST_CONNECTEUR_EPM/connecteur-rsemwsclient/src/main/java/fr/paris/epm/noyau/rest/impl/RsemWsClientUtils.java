package fr.paris.epm.noyau.rest.impl;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;

/**
 * @author KBE
 *
 */
public class RsemWsClientUtils {
    
    public static <T> T parse(InputStream input, Class<T> _class) throws JAXBException {
        Unmarshaller unmarshaller = JAXBContext.newInstance(_class).createUnmarshaller();
        return _class.cast(unmarshaller.unmarshal(input));
    }
}
