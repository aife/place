/**
 * 
 */
package fr.paris.epm.noyau.rest.exceptions;

/**
 * @author KBE
 *
 */
public class RsemWsClientException extends Throwable {
	private static final long serialVersionUID = 1L;

	public RsemWsClientException() {
		super();

	}

	/**
	 * @param cause
	 */
	public RsemWsClientException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public RsemWsClientException(String message) {
		super(message);
	}

	public RsemWsClientException(Throwable cause, String message) {
		super(message, cause);
	}

}
