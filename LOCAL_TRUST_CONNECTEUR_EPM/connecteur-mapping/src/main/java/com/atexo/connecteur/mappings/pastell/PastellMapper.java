package com.atexo.connecteur.mappings.pastell;

import com.atexo.connecteur.commun.xml.PastellGeneratedXml;
import fr.atexo.rsem.noyau.ws.beans.ContratBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.ListDocumentsMiseAdispositionBean;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper
public interface PastellMapper {
    @Mappings({@Mapping(source = "contrat", target = "contrat")})
    PastellGeneratedXml toXML(ListDocumentsMiseAdispositionBean documentsList, ContratBean contrat);
}
