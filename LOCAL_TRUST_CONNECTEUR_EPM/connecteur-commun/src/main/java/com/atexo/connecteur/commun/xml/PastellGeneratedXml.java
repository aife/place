package com.atexo.connecteur.commun.xml;


import fr.atexo.rsem.noyau.ws.beans.miseADisposition.ConsultationMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.DocumentMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.UtilisateurBean;

import javax.xml.bind.annotation.*;
import java.io.File;
import java.util.List;

@XmlType(name = "", propOrder = {"applicationtiers", "documentsList", "miseADispositionConsultation", "contrat", "miseAdispositionId", "utilisateur"})
@XmlRootElement(name = "resultat")
public class PastellGeneratedXml {

    private ContratPastellBean contrat;

    private List<DocumentMiseAdispositionBean> documentsList;
    /**
     * la destination de la mise à disposition
     */
    private String applicationtiers;

    /**
     * Id de la mise dispotion a laquel appartient le contrat
     */
    private String miseAdispositionId;

    /**
     * information de la consultation
     */
    private ConsultationMiseAdispositionBean miseADispositionConsultation;

    /**
     * la liste des documents
     */
    private File archive;

    /**
     * utilisateur qui fait la mise à disposition
     */
    private UtilisateurBean utilisateur;

    /**
     * @return the documentsList
     */
    @XmlElementWrapper(name = "listDocuments")
    @XmlElement(name = "Document")
    public List<DocumentMiseAdispositionBean> getDocumentsList() {
        return documentsList;
    }

    /**
     * @param documentsList the documentsList to set
     */
    public void setDocumentsList(List<DocumentMiseAdispositionBean> documentsList) {
        this.documentsList = documentsList;
    }

    /**
     * @return the applicationtiers
     */

    @XmlElement(name = "codeDestination")
    public String getApplicationtiers() {
        return applicationtiers;
    }

    /**
     * @param applicationtiers the applicationtiers to set
     */
    public void setApplicationtiers(String applicationtiers) {
        this.applicationtiers = applicationtiers;
    }

    /**
     * @return the miseAdispositionId
     */
    @XmlElement(name = "idMiseADisposition")
    public String getMiseAdispositionId() {
        return miseAdispositionId;
    }

    /**
     * @param miseAdispositionId the miseAdispositionId to set
     */
    public void setMiseAdispositionId(String miseAdispositionId) {
        this.miseAdispositionId = miseAdispositionId;
    }

    /**
     * @return the miseADispositionConsultation
     */
    @XmlElement(name = "consultation")
    public ConsultationMiseAdispositionBean getMiseADispositionConsultation() {
        return miseADispositionConsultation;
    }

    /**
     * @param miseADispositionConsultation the miseADispositionConsultation to set
     */
    public void setMiseADispositionConsultation(ConsultationMiseAdispositionBean miseADispositionConsultation) {
        this.miseADispositionConsultation = miseADispositionConsultation;
    }

    @XmlTransient
    public File getArchive() {
        return archive;
    }


    public void setArchive(final File valeur) {
        this.archive = valeur;
    }

    public UtilisateurBean getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(final UtilisateurBean valeur) {
        this.utilisateur = valeur;
    }

    @XmlElement(name = "contrat")
    public ContratPastellBean getContrat() {
        return contrat;
    }

    public void setContrat(ContratPastellBean contrat) {
        this.contrat = contrat;
    }

    @Override
    public String toString() {
        return "PastellGeneratedXml{" + "contrat=" + contrat + ", documentsList=" + documentsList + ", applicationtiers='" + applicationtiers + '\'' + ", miseAdispositionId='" + miseAdispositionId + '\'' + ", miseADispositionConsultation=" + miseADispositionConsultation + ", archive=" + archive + ", utilisateur=" + utilisateur + '}';
    }

}
