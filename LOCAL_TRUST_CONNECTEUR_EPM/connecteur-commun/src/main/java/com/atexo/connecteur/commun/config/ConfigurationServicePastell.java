package com.atexo.connecteur.commun.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@ConfigurationProperties(prefix = "pastell")
@Profile({"pastell"})
@Configuration
public class ConfigurationServicePastell {

    // répertoire de sortie de l'ESB / RSEM
    private String pathIn;
    // répertoire de sortie de PASTELL
    private String pathOut;
    private String pathProcessed;
    private String metaDataFileName;
    private String jetonSignatureExtension;

    public String getPathIn() {
        return pathIn;
    }

    public void setPathIn(String pathIn) {
        this.pathIn = pathIn;
    }

    public String getPathOut() {
        return pathOut;
    }

    public void setPathOut(String pathOut) {
        this.pathOut = pathOut;
    }

    public String getPathProcessed() {
        return pathProcessed;
    }

    public void setPathProcessed(String pathProcessed) {
        this.pathProcessed = pathProcessed;
    }

    public String getMetaDataFileName() {
        return metaDataFileName;
    }

    public void setMetaDataFileName(String metaDataFileName) {
        this.metaDataFileName = metaDataFileName;
    }

    public String getJetonSignatureExtension() {
        return jetonSignatureExtension;
    }

    public void setJetonSignatureExtension(String jetonSignatureExtension) {
        this.jetonSignatureExtension = jetonSignatureExtension;
    }
}

