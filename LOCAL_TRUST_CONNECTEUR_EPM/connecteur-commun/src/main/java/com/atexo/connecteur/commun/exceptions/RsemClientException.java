package com.atexo.connecteur.commun.exceptions;

public class RsemClientException extends Exception {
    public RsemClientException(Exception ex, String s) {
        super(s, ex);
    }

    public RsemClientException(String s) {
        super(s);
    }
}
