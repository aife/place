package com.atexo.connecteur.commun.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Map;

/**
 * Created by dcs on 26/10/16.
 * for Atexo
 */
@ConfigurationProperties(prefix = "ciril")
@Profile({"ciril"})
@Configuration
public class ConfigurationServiceSdis64 {

    //Ciril Sdis64

    String url;

    String accessid;

    String processid;

    String etat;

    String coduti;

    String codbud;

    String codcol;

    String inddeprec;

    String indpri;

    String objet;

    String mode;

    String typgar;

    String codquatie;

    Map<String, String> procedureCode;

    //Getter pour Ciril

    public String getUrl() {
        return url;
    }

    public String getAccessid() {
        return accessid;
    }

    public String getProcessid() {
        return processid;
    }

    public String getEtat() {
        return etat;
    }

    public String getCoduti() {
        return coduti;
    }

    public String getCodbud() {
        return codbud;
    }

    public String getCodcol() {
        return codcol;
    }

    public String getInddeprec() {
        return inddeprec;
    }

    public String getIndpri() {
        return indpri;
    }

    public String getObjet() {
        return objet;
    }

    public String getMode() {
        return mode;
    }

    public String getTypgar() {
        return typgar;
    }

    public String getCodquatie() {
        return codquatie;
    }

    public Map<String, String> getProcedureCode() {
        return procedureCode;
    }
}
