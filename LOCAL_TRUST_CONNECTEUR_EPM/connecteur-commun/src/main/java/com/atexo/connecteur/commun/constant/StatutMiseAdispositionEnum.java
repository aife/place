package com.atexo.connecteur.commun.constant;

public enum StatutMiseAdispositionEnum {
    ACQUITTE("ACQUITTE", 2), ATTENTE("ATTENTE", 1), ECHOUE("ECHOUE", 3);
    private String value;
    private int code;

    StatutMiseAdispositionEnum(String value, int code) {
        this.value = value;
        this.code = code;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @return the code
     */
    public int getCode() {
        return code;
    }

}
