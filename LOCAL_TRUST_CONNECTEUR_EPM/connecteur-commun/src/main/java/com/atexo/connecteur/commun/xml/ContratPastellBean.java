
package com.atexo.connecteur.commun.xml;

import fr.atexo.rsem.noyau.ws.beans.AttributaireBean;
import fr.atexo.rsem.noyau.ws.beans.LotTechniqueBean;

import java.util.Date;
import java.util.List;

/**
 * Class pour generer le XML specifique PASTELL , les formes de prix ont ete enleves
 */
public class ContratPastellBean {

    private List<AttributaireBean> attributaires;
    private Date dateNotificationMarche;
    private String intituleLot;
    private String numeroContrat;
    private String objetContrat;
    private String typeContrat;
    private Date dateSignature;
    private List<LotTechniqueBean> lotsTechniques;
    private Boolean marcheABonDeCommande;
    private String modalitesReconduction;
    private Integer nombreReconductions;
    private String numeroLot;

    public ContratPastellBean() {
    }

    public List<AttributaireBean> getAttributaires() {
        return attributaires;
    }

    public void setAttributaires(List<AttributaireBean> attributaires) {
        this.attributaires = attributaires;
    }

    public Date getDateNotificationMarche() {
        return dateNotificationMarche;
    }

    public void setDateNotificationMarche(Date dateNotificationMarche) {
        this.dateNotificationMarche = dateNotificationMarche;
    }

    public String getIntituleLot() {
        return intituleLot;
    }

    public void setIntituleLot(String intituleLot) {
        this.intituleLot = intituleLot;
    }

    public String getNumeroContrat() {
        return numeroContrat;
    }

    public void setNumeroContrat(String numeroContrat) {
        this.numeroContrat = numeroContrat;
    }

    public String getObjetContrat() {
        return objetContrat;
    }

    public void setObjetContrat(String objetContrat) {
        this.objetContrat = objetContrat;
    }

    public String getTypeContrat() {
        return typeContrat;
    }

    public void setTypeContrat(String typeContrat) {
        this.typeContrat = typeContrat;
    }

    public Date getDateSignature() {
        return dateSignature;
    }

    public void setDateSignature(Date dateSignature) {
        this.dateSignature = dateSignature;
    }

    public List<LotTechniqueBean> getLotsTechniques() {
        return lotsTechniques;
    }

    public void setLotsTechniques(List<LotTechniqueBean> lotsTechniques) {
        this.lotsTechniques = lotsTechniques;
    }

    public Boolean getMarcheABonDeCommande() {
        return marcheABonDeCommande;
    }

    public void setMarcheABonDeCommande(Boolean marcheABonDeCommande) {
        this.marcheABonDeCommande = marcheABonDeCommande;
    }

    public String getModalitesReconduction() {
        return modalitesReconduction;
    }

    public void setModalitesReconduction(String modalitesReconduction) {
        this.modalitesReconduction = modalitesReconduction;
    }

    public Integer getNombreReconductions() {
        return nombreReconductions;
    }

    public void setNombreReconductions(Integer nombreReconductions) {
        this.nombreReconductions = nombreReconductions;
    }

    public String getNumeroLot() {
        return numeroLot;
    }

    public void setNumeroLot(String numeroLot) {
        this.numeroLot = numeroLot;
    }

    @Override
    public String toString() {
        return "ContratPastellBean{" + "attributaires=" + attributaires + ", dateNotificationMarche=" + dateNotificationMarche + ", intituleLot='" + intituleLot + '\'' + ", numeroContrat='" + numeroContrat + '\'' + ", objetContrat='" + objetContrat + '\'' + ", typeContrat='" + typeContrat + '\'' + ", dateSignature=" + dateSignature + ", lotsTechniques=" + lotsTechniques + ", marcheABonDeCommande=" + marcheABonDeCommande + ", modalitesReconduction='" + modalitesReconduction + '\'' + ", nombreReconductions=" + nombreReconductions + ", numeroLot='" + numeroLot + '\'' + '}';
    }
}
