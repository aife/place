package com.atexo.connecteur.commun.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Created by dcs on 26/10/16.
 * for Atexo
 */
@ConfigurationProperties(prefix = "airs")
@Profile({"airs"})
@Configuration
public class ConfigurationServiceAirs {

    String path;

    String applicationName;

    String cou_idtype;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getCou_idtype() {
        return cou_idtype;
    }

    public void setCou_idtype(String cou_idtype) {
        this.cou_idtype = cou_idtype;
    }
}
