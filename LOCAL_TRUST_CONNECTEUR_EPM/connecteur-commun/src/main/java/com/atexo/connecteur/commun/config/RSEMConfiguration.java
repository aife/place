package com.atexo.connecteur.commun.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import java.util.List;

/**
 * Created by dcs on 26/10/16.
 * for Atexo
 */
@ConfigurationProperties(prefix = "rsem")
@PropertySources({@PropertySource(value = "classpath:dev-esb.yml"), @PropertySource(value = "classpath:esb.yml", ignoreResourceNotFound = true)})
@Configuration
public class RSEMConfiguration implements ConfigurationService {
    //RSEM Configue

    private String baseUri;

    private String login;

    private String password;

    private List<String> applicationTiers;

    //Getter pour config du client facade RSEM
    @Override
    public String getBaseUri() {
        return baseUri;
    }

    @Override
    public String getLogin() {
        return login;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public List<String> getApplicationTiers() {
        return applicationTiers;
    }

    public void setApplicationTiers(List<String> applicationTiers) {
        this.applicationTiers = applicationTiers;
    }

    public void setBaseUri(String baseUri) {
        this.baseUri = baseUri;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
