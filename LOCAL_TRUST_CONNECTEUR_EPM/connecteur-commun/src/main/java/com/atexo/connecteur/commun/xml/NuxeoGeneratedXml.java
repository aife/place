package com.atexo.connecteur.commun.xml;


import fr.atexo.rsem.noyau.ws.beans.miseADisposition.ConsultationMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.ContratMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.DocumentMiseAdispositionBean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by dcs on 10/01/17.
 * for Atexo
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "resultat")
public class NuxeoGeneratedXml {
    private DocumentMiseAdispositionBean document;
    private ConsultationMiseAdispositionBean consultation;
    private ContratMiseAdispositionBean contrat;

    public NuxeoGeneratedXml() {
    }

    public NuxeoGeneratedXml(DocumentMiseAdispositionBean document, ConsultationMiseAdispositionBean consultation, ContratMiseAdispositionBean contrat) {
        this.document = document;
        this.consultation = consultation;
        this.contrat = contrat;
    }

    public DocumentMiseAdispositionBean getDocument() {
        return document;
    }

    public void setDocument(DocumentMiseAdispositionBean document) {
        this.document = document;
    }

    public ConsultationMiseAdispositionBean getConsultation() {
        return consultation;
    }

    public void setConsultation(ConsultationMiseAdispositionBean consultation) {
        this.consultation = consultation;
    }

    public ContratMiseAdispositionBean getContrat() {
        return contrat;
    }

    public void setContrat(ContratMiseAdispositionBean contrat) {
        this.contrat = contrat;
    }
}
