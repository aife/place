package com.atexo.connecteur.commun.config;

import java.util.List;

/**
 * Created by dcs on 28/09/16.
 * For Atexo
 */
public interface ConfigurationService {

    String getBaseUri();

    String getLogin();

    String getPassword();

    List<String> getApplicationTiers();

}
