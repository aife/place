package com.atexo.connecteur.commun.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Map;


/**
 * Created by dcs on 26/10/16.
 * for Atexo
 */
@ConfigurationProperties
@Profile({"cmis"})
@Configuration
public class ConfigurationServiceCmis {

    //CMIS Toulouse configue
    String pathRacine;

    String alfrescoLogin;

    String alfrescoPassword;

    String alfrescoServiceUrl;

    Map<String, String> documentType;

    public String getPathRacine() {
        return pathRacine;
    }

    public void setPathRacine(String pathRacine) {
        this.pathRacine = pathRacine;
    }

    public String getAlfrescoLogin() {
        return alfrescoLogin;
    }

    public void setAlfrescoLogin(String alfrescoLogin) {
        this.alfrescoLogin = alfrescoLogin;
    }

    public String getAlfrescoPassword() {
        return alfrescoPassword;
    }

    public void setAlfrescoPassword(String alfrescoPassword) {
        this.alfrescoPassword = alfrescoPassword;
    }

    public String getAlfrescoServiceUrl() {
        return alfrescoServiceUrl;
    }

    public void setAlfrescoServiceUrl(String alfrescoServiceUrl) {
        this.alfrescoServiceUrl = alfrescoServiceUrl;
    }

    public Map<String, String> getDocumentType() {
        return documentType;
    }

    public void setDocumentType(Map<String, String> documentType) {
        this.documentType = documentType;
    }
}
