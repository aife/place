package com.atexo.connecteur.commun.config;

import org.apache.http.entity.mime.content.StringBody;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Map;

/**
 * Created by dcs on 26/10/16.
 * for Atexo
 */
@ConfigurationProperties(prefix = "themis")
@Profile({"themis"})
@Configuration
public class ConfigurationServiceThemisMarseille {

    String url;

    String alfrescoThemisLogin;

    @Value("${alfresco.themis.password}")
    String alfrescoThemisPassword;

    @Value("${alfresco.themis.actionId}")
    StringBody alfrescoThemisActionId;

    @Value("${alfresco.themis.applicationId}")
    StringBody alfrescoThemisapplicationId;

    @Value("#{${procedure.libelle}}")
    Map<String, String> procedureMap;

    @Value("#{${document.type}}")
    Map<String, String> typeDocumentMap;

    //Getter pour config themis Marseillle

    public String getUrl() {
        return url;
    }

    public String getAlfrescoThemisLogin() {return alfrescoThemisLogin;}

    public String getAlfrescoThemisPassword() {return alfrescoThemisPassword;}

    public StringBody getAlfrescoThemisActionId() {return alfrescoThemisActionId;}

    public StringBody getAlfrescoThemisapplicationId() {return alfrescoThemisapplicationId;}

    public Map<String, String> getThemisProcedureMap() {return procedureMap;}

    public Map<String, String> getTypeDocumentMap() {return typeDocumentMap;}

}
