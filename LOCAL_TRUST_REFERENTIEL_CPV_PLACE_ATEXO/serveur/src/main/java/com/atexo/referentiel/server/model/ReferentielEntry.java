package com.atexo.referentiel.server.model;

import java.util.List;

import com.google.common.collect.Lists;

public class ReferentielEntry {

	String id;

	String code;

	String label;

	List<ReferentielEntry> childs;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public ReferentielEntry() {

	}

	public ReferentielEntry(String id, String code, String label) {
		super();
		this.id = id;
		this.code = code;
		this.label = label;
	}

	public List<ReferentielEntry> getChilds() {
		if (childs == null) {
			childs = Lists.newArrayList();
		}
		return childs;
	}

	public void setChilds(List<ReferentielEntry> childs) {
		this.childs = childs;
	}

	@Override
	public String toString() {
		return "ReferentielEntry [id=" + id + ", code=" + code + ", label=" + label + "]";
	}

}
