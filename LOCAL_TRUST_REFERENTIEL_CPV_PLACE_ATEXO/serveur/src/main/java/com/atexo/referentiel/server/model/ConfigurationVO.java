package com.atexo.referentiel.server.model;

import java.util.List;

public class ConfigurationVO {

	private String token;

	private Configuration configuration;

	private List<ReferentielEntry> selection;

	private List<ReferentielEntry> favorites;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	public List<ReferentielEntry> getSelection() {
		return selection;
	}

	public void setSelection(List<ReferentielEntry> selection) {
		this.selection = selection;
	}

	public List<ReferentielEntry> getFavorites() {
		return favorites;
	}

	public void setFavorites(List<ReferentielEntry> favorites) {
		this.favorites = favorites;
	}

}
