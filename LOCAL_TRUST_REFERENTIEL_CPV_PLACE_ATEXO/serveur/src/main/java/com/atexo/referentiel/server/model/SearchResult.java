package com.atexo.referentiel.server.model;

import java.util.List;

public class SearchResult {

	boolean tree = false;

	boolean searchError = false;

	List<ReferentielEntry> referentielList;

	ReferentielEntry referentielTree;

	public boolean isTree() {
		return tree;
	}

	public void setTree(boolean tree) {
		this.tree = tree;
	}

	public boolean isSearchError() {
		return searchError;
	}

	public void setSearchError(boolean searchError) {
		this.searchError = searchError;
	}

	public List<ReferentielEntry> getReferentielList() {
		return referentielList;
	}

	public void setReferentielList(List<ReferentielEntry> referentielList) {
		this.referentielList = referentielList;
	}

	public ReferentielEntry getReferentielTree() {
		return referentielTree;
	}

	public void setReferentielTree(ReferentielEntry referentielTree) {
		this.referentielTree = referentielTree;
	}

}
