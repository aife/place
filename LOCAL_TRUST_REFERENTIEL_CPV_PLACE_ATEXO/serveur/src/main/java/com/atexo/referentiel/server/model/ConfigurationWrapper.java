package com.atexo.referentiel.server.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ConfigurationWrapper {

	String token;
	String urlBase;
	String configPath;
	Configuration configuration;
	List<String> locales = new ArrayList<>();

	private long lastModified;

	public ConfigurationWrapper(String token, String urlBase, String configPath, Configuration configuration) {
		super();
		this.token = token;
		this.urlBase = urlBase;
		this.configPath = configPath;
		this.configuration = configuration;
	}

	public ConfigurationWrapper partialCloneForSerialisation() {
		ConfigurationWrapper result = new ConfigurationWrapper(new String(token), null, null, configuration);

		return result;
	}

	@JsonIgnore
	public String getUrlBase() {
		return urlBase;
	}

	public void setUrlBase(String urlBase) {
		this.urlBase = urlBase;
	}

	@JsonIgnore
	public String getConfigPath() {
		return configPath;
	}

	public void setConfigPath(String configPath) {
		this.configPath = configPath;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	@JsonIgnore
	public List<String> getLocales() {
		return locales;
	}

	public void setLocales(List<String> locales) {
		this.locales = locales;
	}

	@JsonIgnore
	public boolean islocaleReferentielLoaded(String locale) {
		return locales.contains(locale);
	}

	@JsonIgnore
	public long getLastModified() {
		return lastModified;
	}

	public void setLastModified(long lastModified) {
		this.lastModified = lastModified;
	}

}
