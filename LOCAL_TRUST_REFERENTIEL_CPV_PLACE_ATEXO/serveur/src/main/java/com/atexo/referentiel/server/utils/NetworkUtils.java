package com.atexo.referentiel.server.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.protocol.Protocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atexo.referentiel.server.service.ReferentielServiceImpl;

public class NetworkUtils {

	final static Logger loggger = LoggerFactory.getLogger(ReferentielServiceImpl.class);

	static final int PORT_HTTPS = 443;
	static final int ERROR_NOT_FOUND = 404;
	static final int ERROR_OK = 200;

	public static InputStream extraireFichierInfo(final String path) throws IOException {
		InputStream input = null;
		if (path.startsWith("http")) {
			Protocol easyhttps = new Protocol("https", new EasySSLProtocolSocketFactory(), PORT_HTTPS);
			Protocol.registerProtocol("https", easyhttps);
			HttpClient httpclient = new HttpClient();
			GetMethod httpget = new GetMethod();
			httpget.setURI(new URI(path, true, "utf-8"));
			httpget.getParams().setContentCharset("utf-8");

			int stCode;
			try {
				httpclient.executeMethod(httpget);
				stCode = httpget.getStatusCode();
			} catch (UnknownHostException e) {
				loggger.error(" [UnknownHostException]  Could not find Host in" + path);
				loggger.error("Opening Default Config File...");
				loggger.error(e.getMessage(), e);
				// url non valide
				stCode = ERROR_NOT_FOUND;
				throw new IOException(e.fillInStackTrace());
			} catch (IOException e) {
				loggger.error(" [IOException]  Could not find Host in" + path);
				loggger.error("Opening Default Config File...");
				loggger.error(e.getMessage(), e);
				// url non valide
				stCode = ERROR_NOT_FOUND;
				throw new IOException(e.fillInStackTrace());
			}

			// Cas ou le chemin de fichiers de données ou xml fourni n'est pas
			// valide
			if (stCode != ERROR_OK) {
				httpget.releaseConnection();
				throw new IOException("Fichier non valide \"" + path + "\"");
			} else {
				String charSet = "UTF-8";
				try {
					String contenuFichier = httpget.getResponseBodyAsString();
					input = new ByteArrayInputStream(contenuFichier.getBytes(httpget.getRequestCharSet()));
				} catch (UnsupportedEncodingException e) {
					loggger.error("[Serveur : extraireFichierInfo] : Non-acceptation de la lecture de charset type" + charSet);
					loggger.error(e.getMessage(), e);
					throw new IOException(e.fillInStackTrace());
				} catch (IOException e) {
					loggger.error("[Serveur : extraireFichierInfo] : Échec de connexion http ou https");
					loggger.error(e.getMessage(), e);
					throw new IOException(e.fillInStackTrace());
				}
				httpget.releaseConnection();
			}

		} else {
			File fichier = new File(path);
			if (!fichier.exists()) {
				loggger.error("[Serveur : extraireFichierInfo] : ");
				loggger.error("Le fichier " + fichier + " n'existe pas");
				throw new IOException("Erreur à la lecture des fichiers de configuration");
			} else
				try {
					input = new FileInputStream(fichier);
				} catch (FileNotFoundException e) {
					loggger.error("[Serveur : extraireFichierInfo] : Échec de lecture des Fichiers en local");
					loggger.error(e.getMessage(), e);
					throw new IOException(e.fillInStackTrace());
				}
		}
		return input;
	}

	public static long extraireDateDuHeader(Header[] header) throws ParseException {
		long longDate = 0;

		for (Header h : header) {
			String str = h.getName();
			if (str.contentEquals("Last-Modified")) {
				String foo = h.getValue();
				DateFormat df = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", new Locale("en"));
				Date date = df.parse(foo);
				longDate = date.getTime();
				break;
			}
		}
		return longDate;
	}

	/**
	 * Methode qui essaie de se connecter à un serveur ailleurs et dans le cas
	 * echeant, le traitemant est fait pour qu'un fichier par default soit pris
	 * en compte
	 */
	public static long extraireDateLastModified(final String chemin) { // IOException
		long rawDate = 0;

		// Chemin fourni est une URL
		if (chemin.startsWith("http")) {
			Protocol easyhttps = new Protocol("https", new EasySSLProtocolSocketFactory(), 443);
			Protocol.registerProtocol("https", easyhttps);
			HttpClient httpclient = new HttpClient();
			GetMethod httpget = new GetMethod(chemin);

			int stCode;
			try {
				httpclient.executeMethod(httpget);
				stCode = httpget.getStatusCode();
			} catch (UnknownHostException e) {
				loggger.error(" [UnknownHostException]  Could not find Host in" + chemin);
				loggger.error("Opening Default Config File...");
				loggger.error(e.getMessage());
				// url non valide
				stCode = 404;
			} catch (IOException e) {
				loggger.error(" [IOException] Could not find Host in" + chemin);
				loggger.error("Opening Default Config File...");
				loggger.error(e.getMessage());
				// url non valide
				stCode = 404;
			}

			// Cas ou le chemin de fichiers de données ou xml fourni n'est pas
			// valide
			if (stCode != 200) {
				httpget.releaseConnection();
				Date now = new Date();
				Date nowD = new Date(now.getTime());
				loggger.debug("-->Default Config File Date Read..." + nowD);
			} else {
				try {
					Header[] head = httpget.getResponseHeaders();
					rawDate = extraireDateDuHeader(head);
				} catch (Exception e) {
					loggger.error(e.getMessage(), e);
					rawDate = 0;
				} finally {
					httpget.releaseConnection();
				}
			}
			// Chemin fourni n'est pas une URL, il es donc traité comme un
			// fichier
		} else {
			File fichier = new File(chemin);
			if (!fichier.exists()) {
				loggger.error("[UpdateFichier : extraireDateLastModified] Le fichier " + fichier + " n'existe pas");
			} else {
				try {
					rawDate = fichier.lastModified();
				} catch (Exception e) {
					loggger.debug(e.getMessage(), e);
				}
			}
		}
		return rawDate;
	}
}
