package com.atexo.referentiel.server.service;

import java.util.StringTokenizer;

public class UserQueryTranslator {

	private enum CodeLangue {
		CZ,
		ES,
		FR,
		DE,
		IT,
		SV,
		EN
	}

	public static String translateUserQuery(String userQuery, String locale) {

		String parsedQuery = userQuery.toLowerCase();
		CodeLangue enumval = CodeLangue.valueOf(locale.toUpperCase());

		switch (enumval) {
		case ES:
			parsedQuery = translateOperators(parsedQuery, "y", "o");
			break;
		case CZ:
			parsedQuery = translateOperators(parsedQuery, "a", "alebo");
			break;
		case SV:
			parsedQuery = translateOperators(parsedQuery, "och", "eller");
			break;
		case FR:
			parsedQuery = translateOperators(parsedQuery, "et", "ou");
			break;
		case IT:
			parsedQuery = translateOperators(parsedQuery, "e", "o");
			break;
		case DE:
			parsedQuery = translateOperators(parsedQuery, "und", "oder");
			break;
		default:
			parsedQuery = translateOperators(parsedQuery, "and", "or");
			break;
		}

		return parsedQuery;
	}

	private static String translateOperators(String blocDeTexte, String and, String or) {
		StringTokenizer stringTokenizer = new StringTokenizer(blocDeTexte);
		StringBuilder translationBuilder = new StringBuilder();
		while (stringTokenizer.hasMoreTokens()) {
			String token = stringTokenizer.nextToken();
			if (token.equals(and)) {
				translationBuilder.append("AND");
			} else if (token.equals(or)) {
				translationBuilder.append("OR");
			} else {
				translationBuilder.append(token);
			}
			if (stringTokenizer.hasMoreTokens()) {
				translationBuilder.append(" ");
			}
		}
		return translationBuilder.toString();
	}
}
