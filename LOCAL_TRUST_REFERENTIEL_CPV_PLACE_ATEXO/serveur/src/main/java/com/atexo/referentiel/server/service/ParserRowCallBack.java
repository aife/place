package com.atexo.referentiel.server.service;

public interface ParserRowCallBack {

	/**
	 * @param level
	 * @param parentId
	 * @param id
	 * @param code
	 * @param label
	 * @throws Exception
	 */
	public void processRow(int level, String parentId, String path, String id, String code, String label) throws Exception;

}
