package com.atexo.referentiel.server.service;

import java.util.List;

import com.atexo.referentiel.server.model.ConfigurationWrapper;
import com.atexo.referentiel.server.model.ReferentielEntry;

public interface ReferentielService {

	/**
	 * @param urlBase
	 * @param configPath
	 * @return
	 * @throws Exception
	 */
	public ConfigurationWrapper bootstrapConfiguration(String urlBase, String configPath) throws Exception;

	/**
	 * @param token
	 * @param locale
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public List<ReferentielEntry> getByCodes(String token, String locale, List<String> codes) throws Exception;

	/**
	 * @param token
	 * @param locale
	 * @param userQuery
	 * @return
	 * @throws Exception
	 */
	public List<ReferentielEntry> searchReferentiel(String token, String locale, String userQuery) throws Exception;

	/**
	 * @param token
	 * @param locale
	 * @param userQuery
	 * @return
	 * @throws Exception
	 */
	public ReferentielEntry searchReferentielTree(String token, String locale, String userQuery) throws Exception;

	/**
	 * 
	 */
	public void checkConfigurationsUpdates();

}