package com.atexo.referentiel.server.service;

public class ParserOptions {

	String codeTrailingCharacter = null;

	boolean trimCodeTrailingControl = false;

	String codeTrailingControlSeparator = null;

	public String getCodeTrailingCharacter() {
		return codeTrailingCharacter;
	}

	public void setCodeTrailingCharacter(String codeTrailingCharacter) {
		this.codeTrailingCharacter = codeTrailingCharacter;
	}

	public boolean isTrimCodeTrailingControl() {
		return trimCodeTrailingControl;
	}

	public void setTrimCodeTralingControl(boolean trimCodeTrailingControl) {
		this.trimCodeTrailingControl = trimCodeTrailingControl;
	}

	public String getCodeTrailingControlSeparator() {
		return codeTrailingControlSeparator;
	}

	public void setCodeTrailingControlSeparator(String codeTrailingControlSeparator) {
		this.codeTrailingControlSeparator = codeTrailingControlSeparator;
	}

}
