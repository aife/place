package com.atexo.referentiel.server.controller;

import com.atexo.referentiel.server.model.ConfigurationVO;
import com.atexo.referentiel.server.model.ConfigurationWrapper;
import com.atexo.referentiel.server.model.ReferentielEntry;
import com.atexo.referentiel.server.model.SearchResult;
import com.atexo.referentiel.server.service.ReferentielService;
import com.atexo.referentiel.server.service.ReferentielServiceImpl;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping
public class ReferentielController {

    final static Logger loggger = LoggerFactory.getLogger(ReferentielServiceImpl.class);

    @Autowired
    private ReferentielService referentielService;

    @Autowired(required = false)
    ServletContext context;

    @GetMapping("/bootstrapConfiguration")
    @ResponseBody
    public ConfigurationVO bootstrapConfiguration(@RequestParam(required = false) String urlBase, @RequestParam String configPath) throws Exception {

        if (StringUtils.isBlank(urlBase) && context != null) {
            urlBase = context.getRealPath("/");
        }

        ConfigurationWrapper configurationWrapper = referentielService.bootstrapConfiguration(urlBase, configPath);
        ConfigurationVO configurationVO = new ConfigurationVO();
        configurationVO.setToken(configurationWrapper.getToken());
        configurationVO.setConfiguration(configurationWrapper.getConfiguration());
        return configurationVO;
    }


    @GetMapping("/bootstrapConfigurationWithInitialisationData")
    @ResponseBody
    public ConfigurationVO bootstrapConfigurationWithInitialisationData(HttpServletRequest request, @RequestParam(required = false) String urlBase, @RequestParam String configPath, @RequestParam(required = true) String locale, @RequestParam(required = false) String selectionCodes, @RequestParam(required = false) String favoritesCodes) throws Exception {

        ConfigurationVO configurationVO = bootstrapConfiguration(urlBase, configPath);

        if (StringUtils.isNotBlank(selectionCodes)) {
            List<String> codes = Lists.newArrayList(selectionCodes.split(","));
            configurationVO.setSelection(referentielService.getByCodes(configurationVO.getToken(), locale, codes));
        }

        if (StringUtils.isNotBlank(favoritesCodes)) {
            List<String> codes = Lists.newArrayList(favoritesCodes.split(","));
            configurationVO.setFavorites(referentielService.getByCodes(configurationVO.getToken(), locale, codes));
        }
        return configurationVO;
    }


    @GetMapping("/getReferentiels")
    @ResponseBody
    public List<ReferentielEntry> bootstrapConfigurationWithInitialisationData(@RequestParam() String token, @RequestParam(required = true) String locale, @RequestParam(required = true) String codes
    ) throws Exception {
        if (StringUtils.isNotBlank(codes)) {
            List<String> codesList = Lists.newArrayList(codes.split(","));
            return referentielService.getByCodes(token, locale, codesList);
        } else {
            return new ArrayList<>();
        }
    }

    @GetMapping("/searchReferentiel")
    @ResponseBody
    public SearchResult searchReferentiel(@RequestParam String token, @RequestParam String locale,
                                          @RequestParam String userQuery) {
        SearchResult searchResult = new SearchResult();
        try {
            List<ReferentielEntry> searchReferentiel = referentielService.searchReferentiel(token, locale, userQuery);
            searchResult.setReferentielList(searchReferentiel);
            searchResult.setTree(false);
            searchResult.setSearchError(false);
        } catch (Exception e) {
            loggger.error("Erreur lors de la recherche", e);
            searchResult.setSearchError(true);
        }
        return searchResult;
    }

    @GetMapping("/searchReferentielTree")
    @ResponseBody
    public SearchResult searchReferentielTree(@RequestParam(required = true) String token, @RequestParam(required = true) String locale,
                                       @RequestParam(required = true) String userQuery) {
        SearchResult searchResult = new SearchResult();
        try {
            ReferentielEntry searchReferentielTree = referentielService.searchReferentielTree(token, locale, userQuery);
            searchResult.setReferentielTree(searchReferentielTree);
            searchResult.setTree(true);
            searchResult.setSearchError(false);
        } catch (Exception e) {
            loggger.error("Erreur lors de la recherche", e);
            searchResult.setSearchError(true);
        }
        return searchResult;
    }

}
