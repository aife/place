package com.atexo.referentiel.server.service;

import com.atexo.referentiel.server.model.Configuration;
import com.atexo.referentiel.server.model.Configuration.Donnees;
import com.atexo.referentiel.server.model.ConfigurationWrapper;
import com.atexo.referentiel.server.model.ReferentielEntry;
import com.atexo.referentiel.server.repository.ReferentielRepository;
import com.atexo.referentiel.server.utils.NetworkUtils;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentMap;

@Service
public class ReferentielServiceImpl implements ReferentielService {

	final static Logger loggger = LoggerFactory.getLogger(ReferentielServiceImpl.class);

	@Autowired
	ReferentielRepository referentielRepository;

	Cache<String, ConfigurationWrapper> configurations = null;

	@PostConstruct
	public void init() {
		configurations = CacheBuilder.newBuilder().maximumSize(100).build();
	}

	@Override
	public synchronized ConfigurationWrapper bootstrapConfiguration(String urlBase, String configPath) throws Exception {
		String configAbsoultePath = urlBase + configPath;
		String token = DigestUtils.shaHex(configAbsoultePath);

		ConfigurationWrapper configurationWrapper = configurations.getIfPresent(token);
		if (configurationWrapper != null) {
			return configurationWrapper;
		}

		InputStream inputStream = null;
		Configuration config = null;
		try {
			inputStream = NetworkUtils.extraireFichierInfo(configAbsoultePath);
			JAXBContext jc = JAXBContext.newInstance(Configuration.class);
			Unmarshaller um = jc.createUnmarshaller();
			config = (Configuration) um.unmarshal(inputStream);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		configurationWrapper = new ConfigurationWrapper(token, urlBase, configPath, config);
		configurationWrapper.setLastModified(NetworkUtils.extraireDateLastModified(configAbsoultePath));

		// chargement du référentiel pour la locale par défaut
		if (StringUtils.isNotBlank(config.getDonnees().getLocaleParDefaut())) {
			loadReferentielForLocale(configurationWrapper, config.getDonnees().getLocaleParDefaut());
		}

		configurations.put(token, configurationWrapper);
		loggger.info("La configuration " + configurationWrapper.getUrlBase() + configurationWrapper.getConfigPath()
				+ " a été chargé ( token : " + token + " )");

		return configurationWrapper;
	}

	public synchronized void loadReferentielForLocale(ConfigurationWrapper configurationWrapper, String locale) throws Exception {
		if (configurationWrapper.islocaleReferentielLoaded(locale)) {
			return;
		}

		Donnees donnees = configurationWrapper.getConfiguration().getDonnees();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(configurationWrapper.getUrlBase());
		stringBuilder.append(donnees.getCheminDonnees());
		stringBuilder.append(donnees.getPrefixFichier());
		stringBuilder.append(locale);
		stringBuilder.append(".csv");
		String referentielFilePath = stringBuilder.toString();

		InputStream inputStream = null;
		try {
			inputStream = NetworkUtils.extraireFichierInfo(referentielFilePath);
			referentielRepository.processReferentiel(inputStream, configurationWrapper.getConfiguration().getFormatReferentiel(),
					buildClassifier(configurationWrapper.getToken(), locale));

			configurationWrapper.getLocales().add(locale);
			loggger.info("La référentiel pour la locale " + locale + " de la configuration " + configurationWrapper.getUrlBase()
					+ configurationWrapper.getConfigPath() + " a été chargé");
		} finally {
			IOUtils.closeQuietly(inputStream);
		}
	}

	@Override
	// ( 10s , 5m )
	@Scheduled(initialDelay = 10000, fixedRate = 300000)
	public void checkConfigurationsUpdates() {
		loggger.info("Mise à jour des configurations ...");
		ConcurrentMap<String, ConfigurationWrapper> configurationsAsMap = configurations.asMap();
		List<String> evictTokens = new ArrayList<>();
		for (String token : configurationsAsMap.keySet()) {
			ConfigurationWrapper configurationWrapper = configurationsAsMap.get(token);
			String configAbsoultePath = configurationWrapper.getUrlBase() + configurationWrapper.getConfigPath();
			long lastModified = NetworkUtils.extraireDateLastModified(configAbsoultePath);
			if (configurationWrapper.getLastModified() < lastModified) {
				evictTokens.add(token);
			}
		}
		for (String token : evictTokens) {
			// on suprprime l'entrée du cahche, et on nettoie les référentiels
			ConfigurationWrapper config = configurations.getIfPresent(token);
			for (String locale : config.getLocales()) {
				try {
					referentielRepository.removeReferentielByClassifier(buildClassifier(token, locale));
				} catch (Exception e) {
					loggger.error(e.getMessage(), e);
				}
			}
			configurations.invalidate(token);
			try {
				bootstrapConfiguration(config.getUrlBase(), config.getConfigPath());
				loggger.info("La configuration " + config.getUrlBase() + config.getConfigPath()
						+ " a été mise à jour suite à une modification de la source");
			} catch (Exception e) {
				loggger.error(e.getMessage(), e);
			}
		}
	}

	String parseUserQuery(String userQuery, String locale) {
		return UserQueryTranslator.translateUserQuery(userQuery, locale);
	}

	@Override
	public List<ReferentielEntry> getByCodes(String token, String locale, List<String> codes) throws Exception {
		ConfigurationWrapper configurationWrapper = configurations.getIfPresent(token);
		if (configurationWrapper == null) {
			return null;
		}
		if (!configurationWrapper.islocaleReferentielLoaded(locale)) {
			loadReferentielForLocale(configurationWrapper, locale);
		}
		return referentielRepository.getByCodes(codes, buildClassifier(token, locale));
	}

	@Override
	public List<ReferentielEntry> searchReferentiel(String token, String locale, String userQuery) throws Exception {
		ConfigurationWrapper configurationWrapper = configurations.getIfPresent(token);
		if (configurationWrapper == null) {
			return null;
		}
		if (!configurationWrapper.islocaleReferentielLoaded(locale)) {
			loadReferentielForLocale(configurationWrapper, locale);
		}
		String query = parseUserQuery(userQuery, locale);
		String classifier = buildClassifier(token, locale);
		return referentielRepository.searchReferentiel(classifier, query);
	}

	@Override
	public ReferentielEntry searchReferentielTree(String token, String locale, String userQuery) throws Exception {
		ConfigurationWrapper configurationWrapper = configurations.getIfPresent(token);
		if (configurationWrapper == null) {
			return null;
		}
		if (!configurationWrapper.islocaleReferentielLoaded(locale)) {
			loadReferentielForLocale(configurationWrapper, locale);
		}
		return referentielRepository.searchReferentielTree(buildClassifier(token, locale), parseUserQuery(userQuery, locale));
	}

	private String buildClassifier(String token, String locale) {
		return token + "/" + locale;
	}

}
