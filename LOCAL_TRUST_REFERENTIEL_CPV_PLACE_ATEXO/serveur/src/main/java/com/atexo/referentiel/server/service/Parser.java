package com.atexo.referentiel.server.service;

import java.io.InputStream;

public interface Parser {

	/**
	 * @param inputStream
	 * @param parserOptions
	 * @param callBack
	 * @throws Exception
	 */
	public void parse(InputStream inputStream, ParserOptions parserOptions, ParserRowCallBack callBack) throws Exception;

	/**
	 * @param inputStream
	 * @param callBack
	 * @throws Exception
	 */
	public void parse(InputStream inputStream, ParserRowCallBack callBack) throws Exception;

}
