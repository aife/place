package com.atexo.referentiel.server.repository;

import java.io.InputStream;
import java.util.List;

import com.atexo.referentiel.server.model.Configuration.FormatReferentiel;
import com.atexo.referentiel.server.model.ReferentielEntry;

public interface ReferentielRepository {

	public void processReferentiel(InputStream inputStream, FormatReferentiel formatReferentiel, String classifier) throws Exception;

	public List<ReferentielEntry> getByCodes(List<String> codes, String classifier) throws Exception;

	public void removeReferentielByClassifier(String classifier) throws Exception;

	public List<ReferentielEntry> searchReferentiel(String classifier, String userQuery) throws Exception;

	public ReferentielEntry searchReferentielTree(String classifier, String userQuery) throws Exception;

}
