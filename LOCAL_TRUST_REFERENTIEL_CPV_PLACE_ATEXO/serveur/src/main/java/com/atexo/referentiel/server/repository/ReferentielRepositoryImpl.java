package com.atexo.referentiel.server.repository;

import com.atexo.referentiel.server.model.Configuration.FormatReferentiel;
import com.atexo.referentiel.server.model.ReferentielEntry;
import com.atexo.referentiel.server.service.IndexationAnalyser;
import com.atexo.referentiel.server.service.Parser;
import com.atexo.referentiel.server.service.ParserOptions;
import com.atexo.referentiel.server.service.ParserRowCallBack;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.*;

@Service
public class ReferentielRepositoryImpl implements ReferentielRepository {

	private final static Logger LOG = LoggerFactory.getLogger(ReferentielRepositoryImpl.class);

	interface DocumentFields {
		public String CLASSIFIER = "classifier";
		public String LEVEL = "level";
		public String PARENT_ID = "parentId";
		public String PATH = "path";
		public String ID = "id";
		public String CODE = "code";
		public String LABEL = "label";
		public String CODE_LABEL = "code_label";
	}

	@Autowired
	private Parser parser;

	@Value("${indexPath}")
	private String indexPath;

	private final Object indexWriteLock = new Object();

	@PostConstruct
	public void init() {
		try {
			File path = new File(indexPath);
			if (path.exists()) {
				FileUtils.deleteDirectory(path);
			}
			if (!path.mkdirs()) {
				throw new IOException("Impossible de créer le dossier " + path.getAbsolutePath());
			}
		} catch (IOException e) {
			LOG.error("Impossible d'intiaiser l'index lucene", e);
		}
	}

	private Directory getIndexDirectory() throws IOException {
		return FSDirectory.open(new File(indexPath));
	}

	private IndexWriterConfig buildIndexWriterConfig() {
		IndexationAnalyser analyzer = new IndexationAnalyser();
		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40, analyzer);
		return config;
	}

	@Override
	public void processReferentiel(InputStream inputStream, FormatReferentiel formatReferentiel, final String classifier)
			throws Exception {
		IndexWriter indexWriter = null;

		synchronized (indexWriteLock) {
			try {

				indexWriter = new IndexWriter(getIndexDirectory(), buildIndexWriterConfig());
				final IndexWriter indexWriterRef = indexWriter;
				ParserRowCallBack parserRowCallBack = (level, parentId, path, id, code, label) -> {
					Document document = new Document();
					document.add(new StringField(DocumentFields.CLASSIFIER, classifier, Field.Store.NO));
					document.add(new StringField(DocumentFields.LEVEL, String.valueOf(level), Field.Store.YES));
					if (parentId != null) {
						document.add(new StringField(DocumentFields.PARENT_ID, parentId, Field.Store.YES));
					}

					document.add(new StringField(DocumentFields.PATH, path, Field.Store.YES));
					document.add(new StringField(DocumentFields.ID, id, Field.Store.YES));
					document.add(new StringField(DocumentFields.CODE, code, Field.Store.YES));
					document.add(new TextField(DocumentFields.LABEL, label, Field.Store.YES));
					document.add(new TextField(DocumentFields.CODE_LABEL, code.replaceAll("\\.", "") + " " + label, Field.Store.NO));
					indexWriterRef.addDocument(document);
				};

				parser.parse(inputStream, buildParserOptions(formatReferentiel), parserRowCallBack);

			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				throw e;
			} finally {
				if (indexWriter != null) {
					indexWriter.close();
				}
			}
		}
	}

	public ParserOptions buildParserOptions(FormatReferentiel formatReferentiel) {
		ParserOptions parserOptions = new ParserOptions();
		if (formatReferentiel != null) {
			if (StringUtils.isNotBlank(formatReferentiel.getBruit())) {
				parserOptions.setCodeTrailingCharacter(formatReferentiel.getBruit());
			}
			parserOptions.setTrimCodeTralingControl(formatReferentiel.isEnleverCleControle());
			if (StringUtils.isNotBlank(formatReferentiel.getSeparateurCleControle())) {
				parserOptions.setCodeTrailingControlSeparator(formatReferentiel.getSeparateurCleControle());
			}
		}
		return parserOptions;
	}

	String buildCodeLabelLuceneQuery(String userQuery) throws IllegalArgumentException {
		if (userQuery == null) {
			throw new IllegalArgumentException();
		}

		if (userQuery.length() < 3) {
			throw new IllegalArgumentException();
		}

		StringBuilder queryBuilder = new StringBuilder();

		try (IndexationAnalyser analyser = new IndexationAnalyser(false)) {
			StringTokenizer stringTokenizer = new StringTokenizer(userQuery);
			boolean firstToken = true;
			boolean previousTokenOr = false;
			boolean previousTokenOperator = false;

			while (stringTokenizer.hasMoreTokens()) {
				String token = stringTokenizer.nextToken();

				boolean isOr = "OR".equals(token);
				boolean isAnd = "AND".equals(token);
				boolean isOperator = isOr || isAnd;

				// taille query < 3
				if (firstToken && token.length() < 3) {
					throw new IllegalArgumentException();
				}
				token = token.replaceAll("\\.", "");

				// le premier mot est un opérateur
				if (firstToken && isOperator) {
					throw new IllegalArgumentException();
				}

				// 2 opérateurs de suite
				if (!firstToken && isOperator && previousTokenOperator) {
					throw new IllegalArgumentException();
				}

				// on ignore le dernier opérateur
				if (isOperator && !stringTokenizer.hasMoreTokens()) {
					LOG.debug("Query incomplète : " + userQuery + " Le dernier opérateur sera ignoré");
				} else {
					// Si le opérateur précédent est un OU et l'opérateur
					// courant est de taille < 3
					if (!firstToken && previousTokenOr && token.length() < 3) {
						throw new IllegalArgumentException();
					}
					if (isOperator) {
						queryBuilder.append(token).append(" ");
					} else {
						try {
							String analyzedToken = analyseTokenWithLucene(analyser, token);
							queryBuilder.append(analyzedToken).append("*").append(" ");
						} catch (Exception e) {
							throw new IllegalArgumentException(e);
						}
					}
				}
				firstToken = false;
				previousTokenOr = isOr;
				previousTokenOperator = isOperator;
			}

		} catch (IllegalArgumentException e) {
			throw e;
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
		return queryBuilder.toString().trim();
	}

	String analyseTokenWithLucene(IndexationAnalyser analyser, String token) throws IOException {
		List<String> result = new ArrayList<>();
		try (TokenStream tokenStream = analyser.tokenStream(null, new StringReader(token))) {

			CharTermAttribute attr = tokenStream.addAttribute(CharTermAttribute.class);
			tokenStream.reset();
			while (tokenStream.incrementToken()) {
				result.add(attr.toString());
			}
			tokenStream.end();
		}
		return String.join(" ", result);
	}

	@Override
	public List<ReferentielEntry> getByCodes(List<String> codes, String classifier) throws Exception {
		List<ReferentielEntry> result = new ArrayList<>();
		IndexReader reader = null;
		try {
			reader = DirectoryReader.open(getIndexDirectory());
			IndexSearcher indexSearcher = new IndexSearcher(reader);

			for (String code : codes) {
				if (StringUtils.isBlank(code)) {
					continue;
				}

				Document document = getDoucmentByCode(indexSearcher, code, classifier);
				if (document != null) {
					result.add(documentToReferentielEntry(document));
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw e;
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					LOG.error(e.getMessage(), e);
				}
			}
		}
		return result;
	}

	@Override
	@Cacheable("searchReferentiel")
	public List<ReferentielEntry> searchReferentiel(String classifier, String userQuery) throws Exception {
		List<ReferentielEntry> result = new ArrayList<>();
		IndexReader reader = null;
		try {
			reader = DirectoryReader.open(getIndexDirectory());
			IndexSearcher indexSearcher = new IndexSearcher(reader);

			BooleanQuery query = new BooleanQuery();
			TermQuery classifierQuery = new TermQuery(new Term(DocumentFields.CLASSIFIER, classifier));
			query.add(classifierQuery, BooleanClause.Occur.MUST);

			String queryAsString = buildCodeLabelLuceneQuery(userQuery);
			QueryParser queryParser = new QueryParser(Version.LUCENE_40, DocumentFields.CODE_LABEL, new IndexationAnalyser());
			Query codeLabelquery = queryParser.parse(queryAsString);
			query.add(codeLabelquery, BooleanClause.Occur.MUST);

			ScoreDoc[] hits = indexSearcher.search(query, 100).scoreDocs;

			for (int i = 0; i < hits.length; ++i) {
				int documentId = hits[i].doc;
				Document document = indexSearcher.doc(documentId);
				ReferentielEntry referentielEntry = documentToReferentielEntry(document);
				result.add(referentielEntry);
			}

		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					LOG.error(e.getMessage(), e);
				}
			}
		}
		return result;
	}

	@Override
	@Cacheable("searchReferentielTree")
	public ReferentielEntry searchReferentielTree(String classifier, String userQuery) throws Exception {

		List<ReferentielEntry> searchTrees = new ArrayList<>();
		ReferentielEntry result = new ReferentielEntry(); // root
		IndexReader reader = null;

		try {

			reader = DirectoryReader.open(getIndexDirectory());
			IndexSearcher searcher = new IndexSearcher(reader);

			BooleanQuery query = new BooleanQuery();
			TermQuery classifierQuery = new TermQuery(new Term(DocumentFields.CLASSIFIER, classifier));
			query.add(classifierQuery, BooleanClause.Occur.MUST);

			String queryAsString = buildCodeLabelLuceneQuery(userQuery);
			QueryParser queryParser = new QueryParser(Version.LUCENE_40, DocumentFields.CODE_LABEL, new IndexationAnalyser());
			Query codeLabelquery = queryParser.parse(queryAsString.toString());
			query.add(codeLabelquery, BooleanClause.Occur.MUST);

			ScoreDoc[] hits = searcher.search(query, 100).scoreDocs;

			List<Document> documents = new ArrayList<>();
			// on supprime les noeuds intermidiaires
			for (int i = 0; i < hits.length; ++i) {
				int docId = hits[i].doc;
				Document document = searcher.doc(docId);
				String path = document.get(DocumentFields.PATH);
				boolean hit = false;
				for (int j = 0; j < hits.length; ++j) {
					if (i == j) {
						continue;
					}
					int docIdInner = hits[j].doc;
					Document documentInner = searcher.doc(docIdInner);
					String innerPath = documentInner.get(DocumentFields.PATH);
					if (innerPath.contains(path)) {
						hit = true;
						break;
					}
				}
				if (!hit) {
					documents.add(document);
				}
			}

			Map<String, ReferentielEntry> parentsMap = new HashMap<>();
			// reconstitution des arborescences
			for (Document document : documents) {
				String parentIdAsString = null;
				ReferentielEntry referentielEntryIter = documentToReferentielEntry(document);
				Document documentIter = document;

				// on remonte au noeud racine et on s'arrête si un des noeuds
				// parents existe déjà
				boolean upToRoot = true;
				while ((parentIdAsString = documentIter.get(DocumentFields.PARENT_ID)) != null) {

					if (parentsMap.containsKey(parentIdAsString)) {
						ReferentielEntry parent = parentsMap.get(parentIdAsString);
						parent.getChilds().add(referentielEntryIter);
						upToRoot = false;
						break;
					}

					Document parentDocument = getDoumentById(searcher, parentIdAsString, classifier);
					assert parentDocument != null;
					ReferentielEntry parent = documentToReferentielEntry(parentDocument);
					parentsMap.put(parentIdAsString, parent);
					parent.getChilds().add(referentielEntryIter);
					referentielEntryIter = parent;
					documentIter = parentDocument;
				}
				if (upToRoot) {
					searchTrees.add(referentielEntryIter);
				}

			}
			result.setChilds(searchTrees);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					LOG.error(e.getMessage(), e);
				}
			}
		}
		return result;
	}

	private ReferentielEntry documentToReferentielEntry(Document document) {
		return new ReferentielEntry(document.get(DocumentFields.ID), document.get(DocumentFields.CODE),
				document.get(DocumentFields.LABEL));
	}

	private Document getDoumentById(IndexSearcher indexSearcher, String id, String classifier) throws IOException {
		BooleanQuery query = new BooleanQuery();
		TermQuery classifierQuery = new TermQuery(new Term(DocumentFields.CLASSIFIER, classifier));
		query.add(classifierQuery, BooleanClause.Occur.MUST);
		Query idQuery = new TermQuery(new Term(DocumentFields.ID, id));
		query.add(idQuery, BooleanClause.Occur.MUST);

		ScoreDoc[] hits = indexSearcher.search(query, 1).scoreDocs;
		if (hits == null || hits.length == 0) {
			return null;
		}
		return indexSearcher.doc(hits[0].doc);
	}

	private Document getDoucmentByCode(IndexSearcher indexSearcher, String code, String classifier) throws IOException {
		BooleanQuery query = new BooleanQuery();
		TermQuery classifierQuery = new TermQuery(new Term(DocumentFields.CLASSIFIER, classifier));
		query.add(classifierQuery, BooleanClause.Occur.MUST);
		Query codeQuery = new TermQuery(new Term(DocumentFields.CODE, code));
		query.add(codeQuery, BooleanClause.Occur.MUST);

		ScoreDoc[] hits = indexSearcher.search(query, 1).scoreDocs;
		if (hits == null || hits.length == 0) {
			return null;
		}
		return indexSearcher.doc(hits[0].doc);
	}

	@Override
	// TODO: Evict du cache par classifier
	@Caching(evict = { @CacheEvict(value = "searchReferentiel", allEntries = true),
			@CacheEvict(value = "searchReferentielTree", allEntries = true) })
	public void removeReferentielByClassifier(String classifier) throws Exception {
		synchronized (indexWriteLock) {
			try (IndexWriter indexWriter = new IndexWriter(getIndexDirectory(), buildIndexWriterConfig())) {
				indexWriter.deleteDocuments(new Term(DocumentFields.CLASSIFIER, classifier));
				indexWriter.commit();
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				throw e;
			}
		}
	}
}
