package com.atexo.referentiel.server.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Stack;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class ParserImpl implements Parser {

	@Override
	public void parse(InputStream inputStream, ParserRowCallBack callBack) throws Exception {
		parse(inputStream, new ParserOptions(), callBack);
	}

	@Override
	public void parse(InputStream inputStream, ParserOptions parserOptions, ParserRowCallBack callBack) throws Exception {
		InputStreamReader inputStreamReader = null;
		BufferedReader bufferedReader = null;
		try {

			inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
			bufferedReader = new BufferedReader(inputStreamReader);

			String line = null;
			String previousId = null;
			String parentId = null;
			int currentLevel = 0;

			Stack<String> idsStack = new Stack<String>();

			while ((line = bufferedReader.readLine()) != null) {

				String[] elements = StringUtils.split(line, '|');
				String id = null;
				String code = null;
				String label = null;

				if (elements.length == 2) {
					if (parserOptions.isTrimCodeTrailingControl()) {
						String firstElement = elements[0];
						id = StringUtils.split(firstElement, parserOptions.getCodeTrailingControlSeparator())[0];
					} else {
						id = elements[0];
					}
					code = id;
					label = elements[1];
				} else if (elements.length == 3 || elements.length == 4) {
					id = elements[0];
					code = elements[1];
					label = elements[elements.length - 1];
				} else {
					throw new IllegalStateException();
				}

				if (StringUtils.isNotBlank(parserOptions.getCodeTrailingCharacter())) {
					id = trimTrailingCharacters(id, parserOptions.getCodeTrailingCharacter());
				}

				if (previousId == null) {
					currentLevel = 1;
					previousId = id;
					parentId = null;
				} else {
					if (id.contains(previousId)) { // niveau inférieur
						currentLevel++;
						parentId = previousId;
						idsStack.push(parentId);
					} else {

						while (!idsStack.isEmpty()) {
							String stackPeek = idsStack.peek();
							if (id.contains(stackPeek)) { // même niveau
								parentId = stackPeek;
								break;
							} else {
								// un des niveaux supérieurs
								idsStack.pop();
								if (id.contains(stackPeek)) {
									parentId = stackPeek;
									break;
								}
								currentLevel--;
							}
						}

						if (idsStack.isEmpty()) {
							previousId = null;
							parentId = null;
						}
					}
				}

				// Construction du path
				StringBuilder pathBuilder = new StringBuilder("/");
				for (String idFromtack : idsStack) {
					pathBuilder.append(idFromtack).append("/");
				}

				pathBuilder.append(id);
				callBack.processRow(currentLevel, parentId, pathBuilder.toString(), id, code, label);
				previousId = id;
			}

		} finally {
			IOUtils.closeQuietly(bufferedReader);
			IOUtils.closeQuietly(inputStreamReader);
		}
	}

	String trimTrailingCharacters(String string, String characaterAsString) {
		return string.replaceAll("[" + characaterAsString + "]*$", "");
	}

}
