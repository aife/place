package com.atexo.referentiel.server.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UserQueryTranslatorTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		Assert.assertEquals("graines AND AND", UserQueryTranslator.translateUserQuery("graines et et", "fr"));
		Assert.assertEquals("graines AND soja", UserQueryTranslator.translateUserQuery("graines    et soja", "fr"));
		Assert.assertEquals("graines OR soja OR blé", UserQueryTranslator.translateUserQuery("graines ou   soja ou blé", "fr"));
		Assert.assertEquals("graines OR soja OR blé", UserQueryTranslator.translateUserQuery("graines or   soja or blé", "en"));
	}

}
