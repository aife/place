package com.atexo.referentiel.server.web;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.atexo.referentiel.server.controller.ReferentielController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/root-context.xml", "file:src/main/webapp/WEB-INF/dispatcher-servlet.xml" })
public class ReferentielControllerTest {

	final static Logger LOGGGER = LoggerFactory.getLogger(ReferentielControllerTest.class);

	@Autowired
	ReferentielController referentielController;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(referentielController).build();
	}

	@Test
	public void test() {
		try {
			mockMvc.perform(get("/bootstrapConfiguration")).andExpect(status().isBadRequest());

			final String urlBase = "src/main/webapp";
			final String configPath = "/resources/config/naf-config.xml";

			String configAbsoultePath = urlBase + configPath;
			String token = DigestUtils.shaHex(configAbsoultePath);

			// Bootstrap d'une config
			mockMvc.perform(get("/bootstrapConfiguration").param("urlBase", urlBase).param("configPath", configPath)).andExpect(status().isOk())
					.andExpect(jsonPath("$.token", is(token)));

			// recherche Flat
			mockMvc.perform(get("/searchReferentiel").param("token", token).param("locale", "fr").param("userQuery", "indifférenciées")).andExpect(status().isOk())
					.andExpect(jsonPath("$.searchError", is(false))).andExpect(jsonPath("$.referentielList", hasSize(8)));

			// recherche Tree
			mockMvc.perform(get("/searchReferentielTree").param("token", token).param("locale", "fr").param("userQuery", "indifférenciées")).andExpect(status().isOk())
					.andExpect(jsonPath("$.searchError", is(false))).andExpect(jsonPath("$.referentielTree.childs", hasSize(1)));

		} catch (Exception e) {
			LOGGGER.error(e.getMessage(), e);
			Assert.fail();
		}
	}
}
