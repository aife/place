package com.atexo.referentiel.server.service;

import java.io.FileInputStream;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.atexo.referentiel.server.model.ConfigurationWrapper;
import com.atexo.referentiel.server.model.ReferentielEntry;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/root-context.xml" })
public class ReferentielServiceTest {

	@Autowired
	ReferentielService referentielService;

	String configBasePath = "src/main/webapp/resources/config/";
	String dataBasePath = "src/main/webapp/data/";

	@Test
	public void test() {

		FileInputStream configFis = null;
		FileInputStream dataFis = null;

		try {

			ConfigurationWrapper bootstrapConfiguration = referentielService.bootstrapConfiguration("src/main/webapp", "/resources/config/naf-config.xml");

			Assert.assertNotNull(bootstrapConfiguration);
			Assert.assertNotNull(bootstrapConfiguration.getConfiguration());

			ReferentielEntry root = referentielService.searchReferentielTree(bootstrapConfiguration.getToken(), "fr", "indifférenciées");
			Assert.assertEquals(1, root.getChilds().size());

			// forcer un refresh
			bootstrapConfiguration.setLastModified(bootstrapConfiguration.getLastModified() - 1001);
			referentielService.checkConfigurationsUpdates();

			// test de recherche après refresh
			List<ReferentielEntry> searchInReferentiel = referentielService.searchReferentiel(bootstrapConfiguration.getToken(), "fr", "gaz");
			Assert.assertNotEquals(0, searchInReferentiel.size());

		} catch (Exception e) {
			Assert.fail();
		} finally {
			IOUtils.closeQuietly(configFis);
			IOUtils.closeQuietly(dataFis);
		}
	}

}
