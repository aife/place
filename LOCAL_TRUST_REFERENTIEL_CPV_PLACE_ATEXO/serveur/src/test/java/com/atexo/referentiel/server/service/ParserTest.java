package com.atexo.referentiel.server.service;

import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.atomic.AtomicInteger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.atexo.referentiel.server.model.Configuration;
import com.atexo.referentiel.server.model.Configuration.Donnees;
import com.atexo.referentiel.server.repository.ReferentielRepositoryImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/root-context.xml" })
public class ParserTest {

	final static Logger loggger = LoggerFactory.getLogger(ParserTest.class);

	@Autowired
	Parser parser;

	String configBasePath = "src/main/webapp/resources/config/";
	String dataBasePath = "src/main/webapp/data/";

	@Test
	public void testParse() {

		ConfigLastLevel[] configs = new ConfigLastLevel[] {

		new ConfigLastLevel("agrements-config.xml", 1), new ConfigLastLevel("certifinfo-config.xml", 2), new ConfigLastLevel("contractualisations-config.xml", 1),
				new ConfigLastLevel("cper-config.xml", -1), new ConfigLastLevel("cpv-config.xml", -1), new ConfigLastLevel("cpv-demandePrincipal-config.xml", -1),
				new ConfigLastLevel("domaine-config.xml", -1), new ConfigLastLevel("domaineFormation-config.xml", 2), new ConfigLastLevel("domaines-activites-config.xml", -1),
				new ConfigLastLevel("geo-config.xml", -1), new ConfigLastLevel("hotelRestau-config.xml", -1), new ConfigLastLevel("intervention-config.xml", -1),
				new ConfigLastLevel("localisationsGeographiques-config.xml", 1), new ConfigLastLevel("nace-config.xml", 4),
				new ConfigLastLevel("nace-tous-niveaux-config.xml", -1), new ConfigLastLevel("naf-config.xml", 5), new ConfigLastLevel("nomenclatureAchat-config.xml", -1),
				new ConfigLastLevel("nuts-config.xml", 4), new ConfigLastLevel("programmesCommunautaires-config.xml", -1), new ConfigLastLevel("qualifications-config.xml", -1) };

		for (ConfigLastLevel configLastLevel : configs) {

			FileInputStream configFis = null;
			FileInputStream dataFis = null;

			try {

				configFis = new FileInputStream(new File(configBasePath + configLastLevel.getConfig()));
				JAXBContext jc = JAXBContext.newInstance(Configuration.class);
				Unmarshaller um = jc.createUnmarshaller();
				Configuration config = (Configuration) um.unmarshal(configFis);

				Donnees donnees = config.getDonnees();
				String dataFileName = donnees.getPrefixFichier() + donnees.getLocaleParDefaut() + ".csv";
				dataFis = new FileInputStream(dataBasePath + dataFileName);
				final AtomicInteger testLevel = new AtomicInteger();

				loggger.info("Parse du fichier : " + dataFileName);

				ParserRowCallBack callBack = new ParserRowCallBack() {
					@Override
					public void processRow(int level, String parentId, String path, String id, String code, String label) throws Exception {
						testLevel.set(level);
					}
				};

				parser.parse(dataFis, new ReferentielRepositoryImpl().buildParserOptions(config.getFormatReferentiel()), callBack);

				// on teste le niveau du dernier élément pour vérifier la
				// cohérence du résultat
				if (configLastLevel.getLastLevel() != -1) {
					Assert.assertEquals(configLastLevel.getLastLevel(), testLevel.intValue());
				}

			} catch (Exception e) {
				loggger.error("problème dans le parsing du fichier " + configLastLevel.config, e);
				Assert.fail();
			} finally {
				IOUtils.closeQuietly(configFis);
				IOUtils.closeQuietly(dataFis);
			}
		}
	}

	private static class ConfigLastLevel {
		private String config;
		private int lastLevel;

		public ConfigLastLevel(String config, int lastLevel) {
			super();
			this.config = config;
			this.lastLevel = lastLevel;
		}

		public String getConfig() {
			return config;
		}

		public int getLastLevel() {
			return lastLevel;
		}
	}

	@Test
	public void testTrimTrailingCharacters() {
		ParserImpl parserImpl = new ParserImpl();
		Assert.assertEquals("1253", parserImpl.trimTrailingCharacters("12530000", "0"));
		Assert.assertEquals("125305", parserImpl.trimTrailingCharacters("12530500", "0"));
		Assert.assertEquals("125305", parserImpl.trimTrailingCharacters("125305", "0"));
	}

}
