package com.atexo.referentiel.server.repository;

import com.atexo.referentiel.server.model.Configuration;
import com.atexo.referentiel.server.model.ReferentielEntry;
import com.atexo.referentiel.server.service.IndexationAnalyser;
import com.atexo.referentiel.server.service.ParserTest;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/root-context.xml" })
public class ReferentielRepositoryTest {

	final static Logger loggger = LoggerFactory.getLogger(ReferentielRepositoryTest.class);

	@Autowired
	ReferentielRepository referentielService;

	@Test
	public void testSearchTreeInReferentiel() {
		try {
			ClassLoader classLoader = ParserTest.class.getClassLoader();

			JAXBContext jc = JAXBContext.newInstance(Configuration.class);
			Unmarshaller um = jc.createUnmarshaller();
			Configuration config = (Configuration) um.unmarshal(classLoader.getResourceAsStream("com/atexo/referentiels/cpv-config.xml"));

			Assert.assertNotNull(config);
			Assert.assertEquals("0", config.getFormatReferentiel().getBruit());

			String token = "token";

			referentielService.processReferentiel(classLoader.getResourceAsStream("com/atexo/referentiels/data/cpvs_fr.csv"), config.getFormatReferentiel(), token);

			// recherche "arbre"
			ReferentielEntry root = referentielService.searchReferentielTree(token, "grai");
			Assert.assertNotNull(root);
			List<ReferentielEntry> childs = root.getChilds();

			Assert.assertEquals(6, childs.size());
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testSearchInReferentiel() {
		try {
			ClassLoader classLoader = ParserTest.class.getClassLoader();

			JAXBContext jc = JAXBContext.newInstance(Configuration.class);
			Unmarshaller um = jc.createUnmarshaller();
			Configuration config = (Configuration) um.unmarshal(classLoader.getResourceAsStream("com/atexo/referentiels/cpv-config.xml"));

			Assert.assertNotNull(config);
			Assert.assertEquals("0", config.getFormatReferentiel().getBruit());

			String token = "token";

			referentielService.processReferentiel(classLoader.getResourceAsStream("com/atexo/referentiels/data/cpvs_fr.csv"), config.getFormatReferentiel(), token);

			// recherche "arbre"
			List<ReferentielEntry> results = referentielService.searchReferentiel(token, "0913");
			Assert.assertEquals(19, results.size());

			// on supprime le référentiel
			referentielService.removeReferentielByClassifier(token);

			results = referentielService.searchReferentiel(token, "gazeux");
			Assert.assertEquals(0, results.size());

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	public void testAnalyseTextWithLucene() {
		ReferentielRepositoryImpl referentielRepositoryImpl = new ReferentielRepositoryImpl();
		IndexationAnalyser analyser = new IndexationAnalyser(false);
		try {
			Assert.assertEquals("metier", referentielRepositoryImpl.analyseTokenWithLucene(analyser, "métier"));
			Assert.assertEquals("metiers", referentielRepositoryImpl.analyseTokenWithLucene(analyser, "métiers"));
			Assert.assertEquals("gacher", referentielRepositoryImpl.analyseTokenWithLucene(analyser, "Gâcher"));
			Assert.assertEquals("a", referentielRepositoryImpl.analyseTokenWithLucene(analyser, "a"));
			Assert.assertEquals("s", referentielRepositoryImpl.analyseTokenWithLucene(analyser, "s"));
			Assert.assertEquals("09133000", referentielRepositoryImpl.analyseTokenWithLucene(analyser, "09133000-0"));
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test
	public void testBuildLuceneQuery() {

		ReferentielRepositoryImpl referentielRepositoryImpl = new ReferentielRepositoryImpl();

		Exception exception = null;
		try {
			referentielRepositoryImpl.buildCodeLabelLuceneQuery("");
		} catch (Exception e) {
			exception = e;
		}
		Assert.assertTrue(exception != null && exception instanceof IllegalArgumentException);

		exception = null;
		try {
			referentielRepositoryImpl.buildCodeLabelLuceneQuery("test OR OR");
		} catch (Exception e) {
			exception = e;
		}

		Assert.assertTrue(exception != null && exception instanceof IllegalArgumentException);

		Assert.assertEquals("test*", referentielRepositoryImpl.buildCodeLabelLuceneQuery("test"));
		Assert.assertEquals("test*", referentielRepositoryImpl.buildCodeLabelLuceneQuery("test AND"));
		Assert.assertEquals("test*", referentielRepositoryImpl.buildCodeLabelLuceneQuery("test OR "));
		Assert.assertEquals("test* AND xx*", referentielRepositoryImpl.buildCodeLabelLuceneQuery("tést AND xx"));

		exception = null;
		try {
			referentielRepositoryImpl.buildCodeLabelLuceneQuery("test OR xx");
		} catch (Exception e) {
			exception = e;
		}
		Assert.assertTrue(exception != null && exception instanceof IllegalArgumentException);

		Assert.assertEquals("test* OR xxx*", referentielRepositoryImpl.buildCodeLabelLuceneQuery("test OR xxx"));

		Assert.assertEquals("test* AND s*", referentielRepositoryImpl.buildCodeLabelLuceneQuery("test AND s"));

	}
}
