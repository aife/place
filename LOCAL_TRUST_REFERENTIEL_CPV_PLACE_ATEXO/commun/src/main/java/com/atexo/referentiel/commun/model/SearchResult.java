package com.atexo.referentiel.commun.model;

import java.util.List;

public interface SearchResult {

	public boolean isTree();

	public boolean isSearchError();

	public List<? extends ReferentielItem> getReferentielList();

	public ReferentielItem getReferentielTree();

}
