package com.atexo.referentiel.commun.model;

public class ReferentielsConstants {

	public static class WSEndPoints {

		public static class Init {

			public final static String NAME = "init";

			public final static String PARAM_SELECTION_CODES = "selectionCodes";

			public final static String PARAM_FAVORITES_CODES = "favoritesCodes";

		}

		public static class Search {

			public static class SearchReferentiel {

				public final static String NAME = "searchReferentiel";

			}

			public static class SearchReferentielTree {

				public final static String NAME = "searchReferentielTree";

			}

			public final static String PARAM_USER_QUERY = "userQuery";

		}

		public final static String PARAM_CONTAINER = "container";

		public final static String PARAM_TOKEN = "token";

		public final static String PARAM_LOCALE = "locale";

	}

}
