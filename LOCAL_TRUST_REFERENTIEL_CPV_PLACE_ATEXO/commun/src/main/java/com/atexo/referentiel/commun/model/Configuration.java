package com.atexo.referentiel.commun.model;

import java.util.List;

public interface Configuration {

	public List<? extends ReferentielItem> getSelection();

	public List<? extends ReferentielItem> getFavorites();

}
