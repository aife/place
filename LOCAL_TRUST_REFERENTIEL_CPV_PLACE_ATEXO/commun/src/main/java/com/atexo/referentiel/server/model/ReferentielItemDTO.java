package com.atexo.referentiel.server.model;

import java.util.ArrayList;
import java.util.List;

import com.atexo.referentiel.commun.model.ReferentielItem;

public class ReferentielItemDTO implements ReferentielItem {

	String id;

	String code;

	String label;

	List<? extends ReferentielItem> childs;

	@Override
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public ReferentielItemDTO() {

	}

	public ReferentielItemDTO(String id, String code, String label) {
		super();
		this.id = id;
		this.code = code;
		this.label = label;
	}

	@Override
	public List<? extends ReferentielItem> getChilds() {
		if (childs == null) {
			childs = new ArrayList<>();
		}
		return childs;
	}

	public void setChilds(List<ReferentielItemDTO> childs) {
		this.childs = childs;
	}

	@Override
	public String toString() {
		return "ReferentielEntry [id=" + id + ", code=" + code + ", label=" + label + "]";
	}

}
