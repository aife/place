package com.atexo.referentiel.server.model;

import java.util.List;

import com.atexo.referentiel.commun.model.ReferentielItem;
import com.atexo.referentiel.commun.model.SearchResult;

public class SearchResultDTO implements SearchResult {

	boolean tree = false;

	boolean searchError = false;

	List<ReferentielItemDTO> referentielList;

	ReferentielItem referentielTree;

	@Override
	public boolean isTree() {
		return tree;
	}

	public void setTree(boolean tree) {
		this.tree = tree;
	}

	@Override
	public boolean isSearchError() {
		return searchError;
	}

	public void setSearchError(boolean searchError) {
		this.searchError = searchError;
	}

	@Override
	public List<? extends ReferentielItem> getReferentielList() {
		return referentielList;
	}

	public void setReferentielList(List<ReferentielItemDTO> referentielList) {
		this.referentielList = referentielList;
	}

	@Override
	public ReferentielItem getReferentielTree() {
		return referentielTree;
	}

	public void setReferentielTree(ReferentielItem referentielTree) {
		this.referentielTree = referentielTree;
	}

}
