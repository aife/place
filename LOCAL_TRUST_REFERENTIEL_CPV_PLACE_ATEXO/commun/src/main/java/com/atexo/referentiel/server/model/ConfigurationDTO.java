package com.atexo.referentiel.server.model;

import java.util.List;

import com.atexo.referentiel.commun.model.Configuration;

public class ConfigurationDTO implements Configuration {

	private List<ReferentielItemDTO> selection;

	private List<ReferentielItemDTO> favorites;

	@Override
	public List<ReferentielItemDTO> getSelection() {
		return selection;
	}

	public void setSelection(List<ReferentielItemDTO> selection) {
		this.selection = selection;
	}

	@Override
	public List<ReferentielItemDTO> getFavorites() {
		return favorites;
	}

	public void setFavorites(List<ReferentielItemDTO> favorites) {
		this.favorites = favorites;
	}

}
