package com.atexo.referentiel.commun.model;

import java.util.List;

public interface ReferentielItem {

	public String getId();

	public String getCode();

	public String getLabel();

	public List<? extends ReferentielItem> getChilds();

}
