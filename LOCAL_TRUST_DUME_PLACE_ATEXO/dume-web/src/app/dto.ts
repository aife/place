/**
 * Représentation des DTOs provenant du backend
 */
export interface DataInherited {
  nomOfficiel?: string;
  pays?: string;
  referenceConsultation?: string;
  typeProcedure?: string;
  intitule?: string;
  objet?: string;
}

export interface ContextDTO extends DataInherited {
  idContext?: number;
  metadata?: Metadata;
  lots?: LotDTO[];
  default?: boolean;
  applyMarketFilter?: boolean;
  dumeAcheteurs?: DumeRequestDTO[];
  contact?: EconomicPartyInfo;
}

export interface Metadata {
  dateLimiteRemisePlis?: any;
  identifiantPlateforme?: string;
  organisme?: string;
  idConsultation?: number;
  siret?: string;
  idInscrit?: number;
  idContextDumeAcheteur?: number;
  idTypeProcedure?: string;
  idTypeMarche?: string;
  idNatureMarket?: string;
  simplifie?: boolean;

}
export interface CriteriaLot {
  code?: string;
  lots?: LotDTO[];
}
export interface DumeRequestDTO {
  id?: number;
  selectionCriteriaCodes?: CriteriaLot[];
  exclusionCriteriaCodes?: string[];
  confirmed?: boolean;
}

export interface DumeResponseDTO extends DataInherited {
  id?: number;
  micro?: boolean;
  confirmed?: boolean;
  numDumeAcheteurSN?: string;
  numeroSN?: string;
  selectionCriteriaList?: CriteriaDTO[];
  exclusionCriteriaList?: CriteriaDTO[];
  otherCriteriaList?: CriteriaDTO[];
  economicPartyInfo?: EconomicPartyInfo;
  representantLegals?: RepresentantLegal[];
  lots?: string;
}

export interface EconomicPartyInfo {
  nom?: string;
  rue?: string;
  codePostal?: string;
  ville?: string;
  pays?: EnumDTO;
  siteInternet?: string;
  email?: string;
  telephone?: string;
  contact?: string;
  numTVA?: string;
  numEtrange?: string;
}

export interface RepresentantLegal {
  id?: number;
  prenom?: string;
  nom?: string;
  dateNaissance?: any;
  rue?: string;
  codePostal?: string;
  ville?: string;
  pays?: EnumDTO;
  fonction?: string;
  email?: string;
  telephone?: string;
  details?: string;
  lieuNaissance?: string;
}
export interface CriteriaDTO {
  id?: number;
  uuid?: string;
  shortName?: string;
  description?: string;
  legislationReference?: LegislationReferenceDTO;
  code?: string;
  order?: number;
  enableECertis?: boolean;
  criteriaCategorie?: CriteriaCategorieDTO;
  requirementGroupList?: RequirementGroupDTO[];
}

export interface LegislationReferenceDTO {
  title?: string;
  description?: string;
  url?: string;
  article?: string;
}

export interface CriteriaCategorieDTO {
  description?: string;
  code?: string;
  titre?: string;
  type?: string;
  order?: number;
}

export interface RequirementGroupDTO {
  id?: number;
  uuid?: string;
  requirementList?: RequirementDTO[];
  subGroups?: RequirementGroupDTO[];
  fulfillmentIndicator?: boolean;
  unbounded?: boolean;
  idRequirementGroupResponse?: number;
}

// Extension de l'interface de dto.ts pour ordonner les objets entre eux
export interface RequirementGroupDTO {
  instanceNumber?: number;
}

export interface RequirementDTO {
  id?: string;
  uuid?: string;
  description?: string;
  responseType?: string;
  responseDTO?: Response;
  idRequirementResponse?: number;
}

export interface Response {
  id?: number;
  version?: number;
  idRequirement?: number;
  description?: string;
  indicator?: boolean;
  code?: string;
  amount?: number;
  date?: any;
  percent?: number;
  quantity?: number;
  evidenceUrl?: string;
  idRepeatableRequirementGroup?: number;
  instanceNumber?: number;
}

export interface EnumDTO {
  code?: string;
  label?: string;
}

export interface Token {
  access_token: string;
  expires_in?: number;
  refresh_token?: string;
  scope?: string;
  token_type?: string;
}

export interface AuthenticationUrlParams {
  sso_mode: string;
  sso: string;
}

export interface DumeResponseRetourCreationDTO {
  numeroSN: string;
  idContexteOE: number;
}

export interface LotDTO {
  identifianInterneDUME?: number;
  numeroLot?: string;
  intituleLot?: string;
}

export interface ECertisResponseDTO {
  criterion?: CriterionDTO;
  evidences?: EvidenceDTO[];
}

export interface CriterionDTO {
  texte?: string;
  titreDeLaLegislationReference?: string;
  articleDeLegislationReference?: string;
  descriptionDuSubCriterion?: string;
}

export interface EvidenceDTO {
  uuid?: string;
  texte?: string;
  publiPar?: string;
}
