import {ApplicationRef, Inject, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {DumeComponent} from './dume/dume.component';
import {DumeResponseComponent} from './dume/dume-response/dume-response.component';
import {DOCUMENT} from '@angular/common';
import {environment} from '../environments/environment';


@NgModule(environment.config)
export class AppModule {
  private readonly browserDocument;

  ngDoBootstrap(appRef: ApplicationRef) {
    if (!environment.production) {
      appRef.bootstrap(AppComponent);
    } else {
      if (this.browserDocument.getElementsByTagName('dume-request').length > 0) {
        appRef.bootstrap(DumeComponent);
      }
      if (this.browserDocument.getElementsByTagName('dume-response').length > 0) {
        appRef.bootstrap(DumeResponseComponent);
      }
    }
  }

  constructor(@Inject(DOCUMENT) private document: any) {
    this.browserDocument = document;
  }
}
