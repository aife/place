import {Injectable, isDevMode} from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {ReferentielService} from './referentiel.service';

@Injectable()
export class InitConfigService {

  constructor(private authentifcationService: AuthenticationService, private referentielService: ReferentielService) {
  }

  init(): Promise<any> {
    if (isDevMode()) {
      return this.authentifcationService.init().then(value =>
        this.referentielService.init()
      );
    } else {
      this.authentifcationService.init();
      return this.referentielService.init();
    }

  }
}
