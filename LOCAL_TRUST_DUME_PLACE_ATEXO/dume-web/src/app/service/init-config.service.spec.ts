import {inject, TestBed} from '@angular/core/testing';

import {InitConfigService} from './init-config.service';

describe('InitConfigService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InitConfigService]
    });
  });

  it('should be created', inject([InitConfigService], (service: InitConfigService) => {
    expect(service).toBeTruthy();
  }));
});
