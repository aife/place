import {Injectable} from '@angular/core';
import {EnumDTO} from '../dto';
import {HttpClient} from '@angular/common/http';
import {zip} from 'rxjs/observable/zip';
import {
  dumeReferentielGetCountryPath,
  dumeReferentielGetCurrencyPath,
  dumeReferentielGetMarketTypesPath
} from '../constants';
import {AuthenticationService} from './authentication.service';

@Injectable()
export class ReferentielService {

  private _paysRef: EnumDTO[];
  private _currencyRef: EnumDTO[];
  private _typesMarcheRef: EnumDTO[];

  constructor(private http: HttpClient, private authenticateService: AuthenticationService) {
  }

  init(): Promise<any> {
    const paysObservable = this.http.get<EnumDTO[]>(dumeReferentielGetCountryPath,
      {headers: this.authenticateService.getTokenAccess()});
    const currencyObservable = this.http.get<EnumDTO[]>(dumeReferentielGetCurrencyPath,
      {headers: this.authenticateService.getTokenAccess()});
    const marketTypesObservable = this.http.get<EnumDTO[]>(dumeReferentielGetMarketTypesPath,
      {headers: this.authenticateService.getTokenAccess()});

    return zip(paysObservable, currencyObservable, marketTypesObservable, (pays, currenys, marketTypes) => ({
      pays,
      currenys,
      marketTypes
    }))
      .toPromise()
      .then(
        value => {
          this._currencyRef = value.currenys;
          this._paysRef = value.pays;
          this._typesMarcheRef = value.marketTypes;

        }
      );
  }

  getPaysRef(): EnumDTO[] {
    return this._paysRef;
  }


  getcurrencyRef(): EnumDTO[] {
    return this._currencyRef;
  }

  getMarketTypesRef(): EnumDTO[] {
    return this._typesMarcheRef;
  }
}
