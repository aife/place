import {Injectable, isDevMode} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Token} from '../dto';
import {HttpClient, HttpHeaders} from '@angular/common/http';


@Injectable()
export class AuthenticationService {

  private _token: string;

  constructor(private readonly http: HttpClient) {
  }

  getToken(): Observable<Token> {

    const params = new URLSearchParams();
    params.append('grant_type', 'client_credentials');
    const headers = {
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Basic ' + btoa('dume-client:password')
    };
    return this.http.post<Token>('/dume-api/oauth/token', params.toString(), {headers: headers});
  }

  init() {
    if (isDevMode()) {
      return this.getToken().toPromise().then(value => {
        this._token = value.access_token;
        }
      );
    } else {
      this._token = eval('TOKEN_ACCESS');
    }
  }

  getTokenAccess(): any {
    return new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': 'Bearer ' + this._token
    });
  }

}
