import {Injectable} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';


@Injectable()
export class ErrorHandlingService {

  public static readonly ERROR_UNAUTHORIZED = 'Votre session a expiré. Merci de vous reconnecter.';
  public static readonly ERROR_GENERIC =
    'Une erreur est survenue lors de la communication avec le module DUME. Merci de contacter le support technique.';

  getErrorMessage(error: HttpErrorResponse): string {
    if (error.status === 401) {
      return ErrorHandlingService.ERROR_UNAUTHORIZED;
    }
    return ErrorHandlingService.ERROR_GENERIC;
  }

}
