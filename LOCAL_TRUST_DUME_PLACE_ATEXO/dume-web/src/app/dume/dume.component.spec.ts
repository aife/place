import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DumeComponent} from './dume.component';

describe('DumeComponent', () => {
  let component: DumeComponent;
  let fixture: ComponentFixture<DumeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DumeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
