import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RequestCriteriaView} from '../../frontend-object';

@Component({
  selector: 'dume-checkbox-criteria',
  templateUrl: './checkbox-criteria.component.html',
  styleUrls: ['./checkbox-criteria.component.scss']
})
export class CheckboxCriteriaComponent implements OnInit {

  @Input() criteria: RequestCriteriaView;
  isCollapsed = true;
  @Input() desactive = false;
  @Input() showLot = false;
  @Input() lots = [];
  @Output() clicked = new EventEmitter();


  constructor() {
  }

  ngOnInit() {
  }

  criteriaClicked() {
    this.clicked.emit(this.criteria);
  }
  checkAll($event: any) {
    if ($event) {
      this.criteria.allLot = true;
      this.criteria.lots = [];
    }
  }

  resetAllLot() {
    this.criteria.lots = [];
  }

}
