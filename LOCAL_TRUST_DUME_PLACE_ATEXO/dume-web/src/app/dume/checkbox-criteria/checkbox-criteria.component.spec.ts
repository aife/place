import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CheckboxCriteriaComponent} from './checkbox-criteria.component';

describe('CheckboxCriteriaComponent', () => {
  let component: CheckboxCriteriaComponent;
  let fixture: ComponentFixture<CheckboxCriteriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CheckboxCriteriaComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxCriteriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
