import {Component, ElementRef, isDevMode, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ContextDTO, CriteriaLot} from '../dto';
import {DumeService} from './dume.service';
import {CategorieCriteriaView} from '../frontend-object';
import {map} from 'rxjs/operators';
import {zip} from 'rxjs/observable/zip';
import {Observable} from 'rxjs/Observable';
import {ErrorHandlingService} from '../service/error-handling.service';
import {saveAs} from 'file-saver';
import {ModalDirective} from 'ngx-bootstrap';
import {ReferentielService} from '../service/referentiel.service';

@Component({
  selector: 'dume-request',
  templateUrl: './dume.component.html',
  styleUrls: ['./dume.component.scss']
})
export class DumeComponent implements OnInit {

  uselot = false;
  categoriesExculsion: CategorieCriteriaView[];
  categoriesSelection: CategorieCriteriaView[];
  categorieSelectAll: CategorieCriteriaView;
  selectAll = false;
  context: ContextDTO;
  marketType = 'ND';
  @ViewChild('templateSelectionCriteria') templateSelectionCriteria: ModalDirective;

  constructor(private readonly route: ActivatedRoute, private readonly dumeService: DumeService,
              private readonly errorHandlingService: ErrorHandlingService, private readonly elementRef: ElementRef,
              private readonly referentielService: ReferentielService) {
  }

  ngOnInit() {
    this.uselot = this.elementRef.nativeElement.getAttribute('uselot') ? true : false;
    let id: any;
    if (isDevMode()) {
      id = +this.route.snapshot.paramMap.get('id');
    } else {
      id = eval('MPE_ID_CONTEXTE');
    }

    this.dumeService.getDumeDocumentById(id).subscribe(contextDTO => {
      this.context = contextDTO;
      this.marketType = this.getMarketLabel(contextDTO.metadata.idNatureMarket);
      this.setCriterias();
    });
  }

  setCriterias() {
    let selectionObservable: Observable<CategorieCriteriaView[]>;
    if (this.context.applyMarketFilter) {
      selectionObservable = this.dumeService.getAllSelectionCriteriasByNature(this.context.metadata.idNatureMarket)
        .pipe(
          map(this.dumeService.getCategoriesFromCriteria)
        );
    } else {
      selectionObservable = this.dumeService.getAllSelectionCriterias()
        .pipe(
          map(this.dumeService.getCategoriesFromCriteria)
        );
    }

    const exclusionObservable = this.dumeService.getAllExclusionCriterias()
      .pipe(
        map(this.dumeService.getCategoriesFromCriteria)
      );

    this.setCheckedCriterias(selectionObservable, exclusionObservable);
  }

  getMarketLabel(idNatureMarket: string) {
    const correspondingMarket = this.referentielService.getMarketTypesRef().find(m => m.code === idNatureMarket);
    if (correspondingMarket) {
      return correspondingMarket.label;
    }
    return 'ND';
  }

  applyMarketFilter($event) {
    // on force la mise à jour du modèle
    this.context.applyMarketFilter = $event.target.checked;
    this.devalidate($event);
    this.setCriterias();
  }

  affichModalConfirmation($event) {
    if ($event.currentTarget.checked) {
      if (this.isSelectionCriteriaSelected() === false) {
        this.context.dumeAcheteurs[0].confirmed = false;
        $event.currentTarget.checked = false;
        this.templateSelectionCriteria.show();
      } else {
        this.context.dumeAcheteurs[0].confirmed = true;
        this.updateDumeRequest();
      }
    } else {
      this.context.dumeAcheteurs[0].confirmed = false;
      this.updateDumeRequest();
    }
  }


  private initChecked() {
    this.categoriesSelection.forEach(cat => {
      cat.criterias.forEach(criteria => {
        const criteriaLot = this.context.dumeAcheteurs[0].selectionCriteriaCodes.find(criteriaLot1 => criteriaLot1.code === criteria.code);
        if (criteriaLot) {
          criteria.value = true;
          criteria.lots = criteriaLot.lots;
          criteria.allLot = criteriaLot.lots.length === 0;
        } else {
          criteria.value = false;
        }
      });
    });

    this.categoriesExculsion.forEach(cat => {
      cat.criterias.forEach(criteria => {
        if (this.context.dumeAcheteurs[0].exclusionCriteriaCodes.find(code => code === criteria.code)) {
          criteria.value = true;
        } else {
          criteria.value = false;
        }
      });
    });
    if (this.context.dumeAcheteurs[0].selectionCriteriaCodes.find(criteriaLot => criteriaLot.code === this.categorieSelectAll.criterias[0].code)) {
      this.selectAll = true;
    }
  }
  updateDumeRequest() {
    if (this.isValid()) {
      this.buildContext();
      this.dumeService.updateDumeRequest(this.context).subscribe(dto => {
        this.onUpdateDumeRequest(dto);
      }, err => {
        const funcErr = window['enregistrementDumeAcheteurErreur'];
        if (funcErr) {
          funcErr(this.errorHandlingService.getErrorMessage(err));
        }
      });
    }

  }

  isValid(): boolean {
    // Check lassociation des dumes
    const error_critere = [];
    if (!this.selectAll) {
      for (const categoriesSelectionKey of this.categoriesSelection) {
        for (const criteriat of categoriesSelectionKey.criterias) {
          if (criteriat.value && !criteriat.allLot && criteriat.lots.length === 0) {
            error_critere.push(criteriat.titre);
          }
        }
      }

    }

    if (error_critere.length !== 0) {
      const funcErr = window['enregistrementDumeAcheteurErreur'];
      if (funcErr) {
        funcErr(this.construt_error_critere(error_critere));
      }
      return false;
    }
    return true;

  }

  buildContext() {

    this.context.dumeAcheteurs[0].selectionCriteriaCodes = [];
    if (this.selectAll) {
      this.context.dumeAcheteurs[0].selectionCriteriaCodes.push({
        code: this.categorieSelectAll.criterias[0].code,
        lots: []
      });
    } else {
      this.categoriesSelection.forEach(cat => {
        cat.criterias.forEach(criteria => {
          if (criteria.value) {
            this.context.dumeAcheteurs[0].selectionCriteriaCodes.push({
              code: criteria.code,
              lots: criteria.allLot ? [] : criteria.lots
            });
          }
        });
      });
    }
  }

  isSelectionCriteriaSelected(): boolean {
    if (this.selectAll) {
      return true;
    }
    for (const cat of this.categoriesSelection) {
      for (const criteria of cat.criterias) {
        if (criteria.value) {
          return true;
        }
      }
    }
    return false;
  }

  onUpdateDumeRequest(dto: ContextDTO) {
    const func = window['enregistrementDumeAcheteurSucces'];
    if (func) {
      func();
    }
    this.context = dto;
    this.initChecked();
  }


  /**
   * cette fonction est appele lors du click sur le toggle
   * dans tous cas change la confirmation à false
   * @param $event
   */
  onDefault($event) {
    this.context.default = !$event;
    this.context.dumeAcheteurs.forEach(value => {
      value.confirmed = false;
    });

    this.updateDumeRequest();
  }

  construt_error_critere(errors: string[]): string {
    let message = 'Aucun lot renseigné pour le(s) critère(s) suivant(s) : <ul>';
    for (const error of errors) {
      message += '<li> ' + error + '</li>\n';
    }
    message += '</ul>';
    return message;
  }

  downloadPDF() {
    if (this.isValid()) {
      this.buildContext();
      this.dumeService.updateDumeRequest(this.context).subscribe(dto => {
        this.onUpdateDumeRequest(dto);
        this.dumeService.downloadDumeRequestPDF(dto.idContext).subscribe(data => {
          const blob = new Blob([data], {
            type: 'application/xml'
          });
          const filename = `dumeA_${dto.referenceConsultation}.pdf`;
          saveAs(blob, filename);
        });
      });
    }
  }

  private setCheckedCriterias(selectionObservable: Observable<CategorieCriteriaView[]>,
                              exclusionObservable: Observable<CategorieCriteriaView[]>) {
    zip(selectionObservable,
      exclusionObservable,
      (selection, exclusion, contextDTO) => ({selection, exclusion, contextDTO})).subscribe(value => {
      this.categorieSelectAll = value.selection.splice(
        value.selection.findIndex(function (cat) {
          return cat.code === 'ALL_CRITERIA_SATISFIED';
        }), 1)[0];
      this.categorieSelectAll.criterias[0].value = true;
      this.categoriesSelection = value.selection;
      this.categoriesExculsion = value.exclusion;
      this.context.lots.sort((a, b) => (+a.numeroLot) - (+b.numeroLot));
      this.initChecked();
    });
  }

  devalidate($event) {
    if (!$event.value) {
      this.context.dumeAcheteurs[0].selectionCriteriaCodes.push($event);
    } else {
      this.context.dumeAcheteurs[0].selectionCriteriaCodes = this.context.dumeAcheteurs[0].selectionCriteriaCodes.filter(
        (criteria: CriteriaLot): boolean => criteria.code !== $event.code
      );
    }

    this.dumeService.updateDumeRequest(this.context).subscribe(dto => {
      this.onUpdateDumeRequest(dto);
    }, err => {
      const funcErr = window['enregistrementDumeAcheteurErreur'];
      if (funcErr) {
        funcErr(this.errorHandlingService.getErrorMessage(err));
      }
    });

  }

  select(): void {
    this.selectAll = true;

    this.updateDumeRequest();
  }

  unselect(): void {
    this.selectAll = false;

    this.updateDumeRequest();
  }
}
