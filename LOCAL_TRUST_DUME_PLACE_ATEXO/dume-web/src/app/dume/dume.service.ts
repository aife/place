import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ContextDTO, CriteriaCategorieDTO, CriteriaDTO, DumeResponseDTO, DumeResponseRetourCreationDTO, ECertisResponseDTO, EconomicPartyInfo} from '../dto';
import {CategorieCriteriaView, RequestCriteriaView} from '../frontend-object';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {Router} from '@angular/router';
import {
  createDumeRequestPath,
  createDumeResponsePath, downloadDumeRequestPDF,
  downloadOEXML,
  dumeResponseBasePath, eCertisProofPath,
  findDumeResponsePath,
  getAllDumeRequestPath,
  getAllExclusionCriteriasPath,
  getAllOtherCriteriasPath,
  getAllSelectionCriteriaCategoriesPath,
  getAllSelectionCriteriasPath,
  getDumeOEByIdPath,
  getDumeRequestByIDPath,
  updateAndDownload,
  updateDumeRequestPath,
  updateDumeResponsePath
} from '../constants';
import {AuthenticationService} from '../service/authentication.service';

@Injectable()
export class DumeService {

  token = 'Bearer';

  constructor(private readonly http: HttpClient, private readonly router: Router, private readonly authenticateService: AuthenticationService) {
  }
  getAllSelectionCriterias(): Observable<CriteriaDTO[]> {
    return this.http.get<CriteriaDTO[]>(getAllSelectionCriteriasPath, {headers: this.authenticateService.getTokenAccess()})
      ;
  }

  getAllSelectionCriteriasByNature(idNature: string): Observable<CriteriaDTO[]> {
    return this.http.get<CriteriaDTO[]>(`${getAllSelectionCriteriasPath}/${idNature}`, {headers: this.authenticateService.getTokenAccess()})
      ;
  }

  getAllSelectionCriteriaCategories(): Observable<CriteriaCategorieDTO[]> {
    return this.http.get<CriteriaCategorieDTO[]>(getAllSelectionCriteriaCategoriesPath,
      {headers: this.authenticateService.getTokenAccess()})
      ;
  }

  getAllExclusionCriterias(): Observable<CriteriaDTO[]> {
    return this.http.get<CriteriaDTO[]>(getAllExclusionCriteriasPath, {headers: this.authenticateService.getTokenAccess()})
      ;
  }

  getAllOtherCriterias(): Observable<CriteriaDTO[]> {
    return this.http.get<CriteriaDTO[]>(getAllOtherCriteriasPath, {headers: this.authenticateService.getTokenAccess()})
      ;
  }

  getDumeDocumentById(id: number): Observable<ContextDTO> {
    return this.http.get<ContextDTO>(`${getDumeRequestByIDPath}${id}`, {headers: this.authenticateService.getTokenAccess()})
      ;
  }

  getRequestCriteriaFromDTO(dto: CriteriaDTO): RequestCriteriaView {
    return {code: dto.code, titre: dto.shortName, description: dto.description};
  }

  getCategoriesFromCriteria(dtos: CriteriaDTO[]): CategorieCriteriaView[] {
    //   dtos.pipe( // Observable<CriteriaDTO[]> -> Observable<CriteriaDTO>
    //     mergeMap(dtosArray => from(dtosArray)),
    //     groupBy(dto => dto.criteriaCategorie.code})),
    //     mergeMap(category => category.pipe(
    //       map(criteria => ({code: criteria.code, titre: criteria.shortName, description: criteria.description})),
    //       toArray(),
    //       map(criteriaArray => ({
    //   titre: category.key,
    //   description: category.key,
    //   code: category.key,
    //   criterias: criteriaArray}))
    //     )),
    //   toArray()
    // );
    const result: CategorieCriteriaView[] = [];

    dtos.forEach(dto => {
      const cat = result.find(value => value.code === dto.criteriaCategorie.code);
      if (cat) {
        cat.criterias.push({
          code: dto.code,
          titre: dto.shortName,
          description: dto.description,
          order: dto.order,
          lots: [],
          allLot: true
        });
      } else {
        result.push({
          titre: dto.criteriaCategorie.titre,
          description: dto.criteriaCategorie.description,
          code: dto.criteriaCategorie.code,
          criterias: [{
            code: dto.code,
            titre: dto.shortName,
            description: dto.description,
            order: dto.order,
            lots: [],
            allLot: true
          }]
        });
      }
    });

    result.forEach(category => {
      category.criterias.sort((order1, order2) => {
        if (order1.order > order2.order) {
          return 1;
        }
        if (order1.order < order2.order) {
          return -1;
        }
        return 0;
      });
    });

    return result;
  }

  getCategoriesResponseFromCriteria(dtos: CriteriaDTO[], open = true): CategorieCriteriaView[] {
    return this.fillCategoriesResponseFromCriteria([], dtos, open);
  }

  fillCategoriesResponseFromCriteria(categories: CategorieCriteriaView[], dtos: CriteriaDTO[], open = true): CategorieCriteriaView[] {
    const result = categories;

    dtos.forEach(dto => {
      const cat = result.find(value => value.code === dto.criteriaCategorie.code);
      if (cat) {
        cat.criterias.push(dto);
      } else {
        result.push({
          titre: dto.criteriaCategorie.titre,
          description: dto.criteriaCategorie.description,
          code: dto.criteriaCategorie.code,
          criterias: [dto],
          order: dto.criteriaCategorie.order,
          open: open
        });
      }
    });

    // sort criterias within categories
    result.forEach(category => {
      category.criterias.sort((order1, order2) => {
        if (order1.order > order2.order) {
          return 1;
        }
        if (order1.order < order2.order) {
          return -1;
        }
        return 0;
      });
    });

    // sort categories
    result.sort((order1, order2) => {
      if (order1.order > order2.order) {
        return 1;
      }
      if (order1.order < order2.order) {
        return -1;
      }
      return 0;
    });

    return result;
  }

  getCategorieCriteriaViewFromCategories(categorieDtos: CriteriaCategorieDTO[], open = true): CategorieCriteriaView[] {
    const result: CategorieCriteriaView[] = [];
    categorieDtos.forEach(categorie => {
      result.push({
        titre: categorie.titre,
        description: categorie.description,
        code: categorie.code,
        order: categorie.order,
        criterias: [],
        open: open
      });
    });
    return result;
  }
  updateDumeRequest(contextDto: ContextDTO): Observable<ContextDTO> {
    return this.http.post<ContextDTO>(updateDumeRequestPath, contextDto, {headers: this.authenticateService.getTokenAccess()});
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(`${operation} failed`);

      console.error(error);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getAllDumeRequest(): Observable<ContextDTO[]> {
    return this.http.get<ContextDTO[]>(getAllDumeRequestPath, {headers: this.authenticateService.getTokenAccess()});
  }

  createDume(context: ContextDTO): Observable<number> {
    return this.http.post<number>(createDumeRequestPath, context, {headers: this.authenticateService.getTokenAccess()});
  }

  downloadDumeRequestPDF(idContexte: number): Observable<any> {
    return this.http.get(`${downloadDumeRequestPDF}${idContexte}`,
      {
        headers: this.authenticateService.getTokenAccess().append('Accept', 'application/xml'),
        responseType: 'blob'
      });
  }

    createDumeOE(idDumeRequest: number) {
      const contact: EconomicPartyInfo = {nom: 'Albert Camus', email: 'albert@free.fr', telephone: '060606060606'}
      const context: ContextDTO = {metadata: {idContextDumeAcheteur: idDumeRequest, siret: '44090956200033'}, contact: contact};
        this.http.post<DumeResponseRetourCreationDTO>(createDumeResponsePath, context, {
            headers: this.authenticateService.getTokenAccess()
        })
            .subscribe(value => {
                this.router.navigate([dumeResponseBasePath + value.idContexteOE]);
            });
    }

  createDumeOEFromExistingSNNumber(idResponse: number, snNumber: string) {
    return this.http.post<DumeResponseDTO>(`${createDumeResponsePath}${idResponse}/${snNumber}`, null, {
            headers: this.authenticateService.getTokenAccess()
        });
    }

    getDumeOE(idDumeOE: number): Observable<DumeResponseDTO> {
        return this.http.get<DumeResponseDTO>(getDumeOEByIdPath + idDumeOE, {headers: this.authenticateService.getTokenAccess()});
    }

  updateDumeOe(dumeOE: DumeResponseDTO): Observable<DumeResponseDTO> {
    return this.http.post<DumeResponseDTO>(updateDumeResponsePath, dumeOE, {headers: this.authenticateService.getTokenAccess()});
  }

    updateAndDownloadPDF(dumeOE: DumeResponseDTO): Observable<any> {
        return this.http.post(updateAndDownload, dumeOE,
            {
                headers: this.authenticateService.getTokenAccess().append('Accept', 'application/pdf'),
                responseType: 'blob'
            });
    }

  downloadOEXML(identifierDumeOperateurEconomique: number): Observable<any> {
    return this.http.get(`${downloadOEXML}${identifierDumeOperateurEconomique}`,
      {
        headers: this.authenticateService.getTokenAccess().append('Accept', 'application/xml'),
        responseType: 'blob'
      });
  }

    findDumeOE(identifierDumeOperateurEconomique: number, numeroSN: string) {
      return this.http.get<DumeResponseDTO[]>(`${findDumeResponsePath}${identifierDumeOperateurEconomique}/${numeroSN}`, {
            headers: this.authenticateService.getTokenAccess()
        });
    }

  getECertisProof(identifierDumeOpertauerEconomique: number, criteriaUUID: string, languageCode: string) {
    return this.http.get<ECertisResponseDTO[]>(`${eCertisProofPath}${identifierDumeOpertauerEconomique}/${criteriaUUID}/${languageCode}`, {
      headers: this.authenticateService.getTokenAccess()
    });
  }
}
