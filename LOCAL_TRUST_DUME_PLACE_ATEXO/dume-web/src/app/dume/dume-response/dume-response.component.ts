import {Component, isDevMode, OnInit, ViewChild} from '@angular/core';
import {CategorieCriteriaView} from '../../frontend-object';
import {CriteriaDTO, DumeResponseDTO, ECertisResponseDTO, EnumDTO, RequirementGroupDTO} from '../../dto';
import {ActivatedRoute} from '@angular/router';
import {DumeService} from '../dume.service';
import {ReferentielService} from '../../service/referentiel.service';
import {BsModalService, ModalDirective} from 'ngx-bootstrap';
import {ErrorHandlingService} from '../../service/error-handling.service';
import {saveAs} from 'file-saver';
import {map} from 'rxjs/operators';

@Component({
  selector: 'dume-response',
  templateUrl: './dume-response.component.html',
  styleUrls: ['./dume-response.component.scss']
})
export class DumeResponseComponent implements OnInit {

  categoriesExculsion: CategorieCriteriaView[];
  categoriesSelection: CategorieCriteriaView[];

  categorieOtherA: CategorieCriteriaView;
  categorieOtherC: CategorieCriteriaView;
  categorieOtherD: CategorieCriteriaView;
  categorieFinal: CategorieCriteriaView;

  @ViewChild(ModalDirective) modal: ModalDirective;
  @ViewChild('templateConfirmation') templateConfirmation: ModalDirective;
  @ViewChild('templateSearch') templateSearch: ModalDirective;
  @ViewChild('templatePostSave') templatePostSave: ModalDirective;
  @ViewChild('templateECertis') templateECertis: ModalDirective;

  dumeOE: DumeResponseDTO;

  dumeOEConfirmed: boolean;

  pays: EnumDTO[];

  alert: { type: string; message: string } = null;
  searchResult: DumeResponseDTO[] = [];
  selectedDTO: DumeResponseDTO;
  dumeNumber: string;
  selectedECertisCriteria: CriteriaDTO;
  selectedECertisCountryCode = 'FR';
  eCertisProof: ECertisResponseDTO[];

  constructor(private readonly route: ActivatedRoute, private readonly dumeService: DumeService,
              private readonly referentielService: ReferentielService, private readonly modalService: BsModalService,
              private readonly errorHandlingService: ErrorHandlingService) {
  }

  ngOnInit() {
    this.pays = this.referentielService.getPaysRef();
    let id: any;
    if (isDevMode()) {
      id = +this.route.snapshot.paramMap.get('id');
    } else {
      id = eval('MPE_ID_CONTEXTE');
    }
    this.dumeService.getDumeOE(id).subscribe(value => this.initDumeOe(value));
  }

  updateDumeOE(confirmed: boolean) {
    this.alert = null;
    this.dumeOE.confirmed = confirmed;
    this.suppressionRequirementSpecifique();
    this.dumeService.updateDumeOe(this.dumeOE)
      .subscribe(value => {
        this.initDumeOe(value);
        this.alert = {type: 'success', message: 'Vos modifications ont bien été enregistrées.'};
        window.scroll(0, 0);
        const afterSaveSuccess = window['afterSaveDumeOeSuccess'];
        if (afterSaveSuccess) {
          afterSaveSuccess();
        } else {
          console.log('la fonction afterSaveDumeOeSuccess est introuvable');
        }

      }, error => {
        this.ajoutDurequirementGroupC();
        this.alert = {type: 'danger', message: this.errorHandlingService.getErrorMessage(error)};
        window.scroll(0, 0);
        const afterSaveError = window['afterSaveDumeOeError'];
        if (afterSaveError) {
          afterSaveError();
        } else {
          console.log('la fonction afterSaveError est introuvable');
        }
      });
  }

  initDumeOe(value) {
    this.dumeService.getAllSelectionCriteriaCategories().pipe(
      map(categories => this.dumeService.getCategorieCriteriaViewFromCategories(categories, true))
    ).subscribe(categorieViews => {
      this.categoriesSelection = this.dumeService.fillCategoriesResponseFromCriteria(categorieViews, value.selectionCriteriaList);
    });

    const exclusion = this.dumeService.getCategoriesResponseFromCriteria(value.exclusionCriteriaList, false);
    const other = this.dumeService.getCategoriesResponseFromCriteria(value.otherCriteriaList);
    this.dumeOE = value;
    this.dumeOEConfirmed = this.dumeOE.confirmed ? true : false;
    this.categoriesExculsion = exclusion;
    other.forEach(cat => {
      if (cat.code === 'REDUCTION_OF_CANDIDATES') {
        this.categorieFinal = cat;
      } else if (cat.code === 'DATA_ON_ECONOMIC_OPERATOR') {
        this.categorieOtherA = cat;
      } else if (cat.code === 'RELIES_ON_OTHER_CAPACITIES') {
        this.categorieOtherC = cat;
        this.ajoutDurequirementGroupC();
      } else if (cat.code === 'SUBCONTRACTS_WITH_THIRD_PARTIES') {
        this.categorieOtherD = cat;
      }
    });
  }


  checkValid(categorie: CategorieCriteriaView): boolean {
    let result = true;
    categorie.criterias.forEach(criteria => {
      criteria.requirementGroupList.forEach(value => {
        const res_crit = value.requirementList.find(crit => crit.uuid === '974c8196-9d1c-419c-9ca9-45bb9f5fd59a');
        if (res_crit) {
          result = result && !(res_crit.responseDTO.indicator);
        }
      });
    });
    return result;
  }

  customCompareCountry(o1: EnumDTO, o2: EnumDTO) {
    if (o1 == null && o2 == null) {
      return true;
    }
    if (o1 == null || o2 == null) {
      return false;
    }
    return o1.code === o2.code;
  }

  affichModalConfirmation($event) {
    if ($event.currentTarget.checked) {
      $event.currentTarget.checked = false;
      this.modal.show();
    } else {
      this.dumeOEConfirmed = false;
    }
  }

  onUpdateOE() {
    this.initDumeOe(this.dumeOE);
    this.alert = {type: 'success', message: 'Vos modifications ont bien été enregistrées.'};
    window.scroll(0, 0);
  }
  updateAndDownload() {
    this.alert = null;
    this.suppressionRequirementSpecifique();
    this.dumeService.updateAndDownloadPDF(this.dumeOE).subscribe(data => {
        this.onUpdateOE();
        const blob = new Blob([data], {
          type: 'application/pdf' // must match the Accept type
        });
        const filename = 'dumeOE.pdf';
        saveAs(blob, filename);
      }
      , error => {
        this.ajoutDurequirementGroupC();
        this.alert = {type: 'danger', message: this.errorHandlingService.getErrorMessage(error)};
        window.scroll(0, 0);
      });
  }

  downloadXML() {
    this.dumeService.updateDumeOe(this.dumeOE).subscribe(oe => {
      this.onUpdateOE();
      this.dumeService.downloadOEXML(oe.id).subscribe(data => {
        const blob = new Blob([data], {
          type: 'application/xml'
        });
        const filename = `${oe.numeroSN}.xml`;
        saveAs(blob, filename);
      });
    });
  }

  searchDumeOE() {
        this.selectedDTO = null;
        this.dumeNumber = null;
        this.searchResult = [];
        this.templateSearch.show();
    }
    saveFromSelectedDTO() {
        this.dumeService.createDumeOEFromExistingSNNumber(this.dumeOE.id, this.selectedDTO.numeroSN).subscribe(r => {
        this.templateSearch.hide();
        this.templateConfirmation.hide();
        this.initDumeOe( r);
        this.templatePostSave.show();
        });

  }
  searchDumeOEFromSN() {
        this.dumeService.findDumeOE(this.dumeOE.id, this.dumeNumber).subscribe(r => {
            this.searchResult = r;
        });
    }

  openConfirmationModal(dto: DumeResponseDTO) {
    this.selectedDTO = dto;
    this.templateConfirmation.show();
  }

  /**
   * Fonction qui ajout un message spécifique pour la catégorie C
   */
  ajoutDurequirementGroupC() {
    const reqGrp: RequirementGroupDTO = {
      fulfillmentIndicator: true,
      id: -1,
      subGroups: [],
      uuid: 'fake_uuid',
      idRequirementGroupResponse: -1,
      requirementList: [{
        responseType: 'INFOS',
        id: '99999',
        responseDTO: {},
        description: 'Veuillez fournir pour chacune des entités concernées un formulaire DUME distinct contenant les ' +
        'informations demandées dans les sections A et B de la présente partie et à la partie III, dûment rempli et ' +
        'signé par les entités concernées.Veuillez noter que cela doit également comprendre tous les techniciens ou ' +
        'les organismes techniques qui ne font pas directement partie de l’entreprise de l’opérateur économique, en ' +
        'particulier ceux qui sont responsables du contrôle de la qualité et, lorsqu’il s’agit de marchés publics de ' +
        'travaux, les techniciens ou les organismes techniques auxquels l’opérateur économique pourra faire appel pour ' +
        'l’exécution de l’ouvrage.Dans la mesure où cela est pertinent pour la ou les capacités spécifiques auxquelles ' +
        'l’opérateur économique a recours, veuillez inclure pour chacune des entités concernées les informations demandées ' +
        'dans les parties IV et V.'
      }]
    };


    this.categorieOtherC.criterias[0].requirementGroupList[0].subGroups.push(reqGrp);
  }

  /**
   * Suppression du Requirement Group spécifique quand on essaye de sauvegarder
   */
  suppressionRequirementSpecifique() {
    if (this.categorieOtherC) {
      this.categorieOtherC.criterias[0].requirementGroupList[0].subGroups.pop();
    }

  }

  showECertis(criteria: CriteriaDTO) {
    this.selectedECertisCriteria = criteria;
    this.dumeService.getECertisProof(this.dumeOE.id, criteria.uuid, this.selectedECertisCountryCode).subscribe(proof => {
      this.eCertisProof = proof;
      if (isDevMode()) {
        console.log(proof);
      }
      this.templateECertis.show();
    });
  }

  getProof() {
    this.dumeService.getECertisProof(this.dumeOE.id, this.selectedECertisCriteria.uuid, this.selectedECertisCountryCode)
      .subscribe(proof => {
        this.eCertisProof = proof;
    });
  }
}
