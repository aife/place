import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DumeResponseComponent} from './dume-response.component';

describe('DumeResponseComponent', () => {
  let component: DumeResponseComponent;
  let fixture: ComponentFixture<DumeResponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DumeResponseComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DumeResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
