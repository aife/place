import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FakeMpeComponent } from './fake-mpe.component';

describe('FakeMpeComponent', () => {
  let component: FakeMpeComponent;
  let fixture: ComponentFixture<FakeMpeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FakeMpeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FakeMpeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
