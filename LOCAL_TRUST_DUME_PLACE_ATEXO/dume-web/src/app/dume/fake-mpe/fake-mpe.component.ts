import {Component, OnInit, TemplateRef} from '@angular/core';
import {DumeService} from '../dume.service';
import {ContextDTO} from '../../dto';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'dume-fake-mpe',
  templateUrl: './fake-mpe.component.html',
  styleUrls: ['./fake-mpe.component.scss']
})
export class FakeMpeComponent implements OnInit {

  dumes: ContextDTO[];
  newDume: ContextDTO;
  allotissement = false;
  modalRef: BsModalRef;

  constructor(private readonly dumeservice: DumeService, private readonly modalService: BsModalService) {
  }

  ngOnInit() {
    this.dumeservice.getAllDumeRequest().subscribe(response => {
      this.dumes = response;
    });
  }

  initNewDume(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    this.newDume = {};
    this.newDume.metadata = {
      identifiantPlateforme: 'mpe-place',
      organisme: 'atexo',
      idConsultation: Math.random() * 100000,
      siret: '12345678901234',
      idInscrit: 3,
      idTypeProcedure: '01',
      idTypeMarche: '01',
      idNatureMarket: '01',
      dateLimiteRemisePlis: new Date()
    };
    this.newDume.lots = [];
  }

  addNewDume() {
    if (this.allotissement) {
      for (let i = 0; i < 3; i++) {
        this.newDume.lots.push({numeroLot: i + '', intituleLot: 'lot ' + i});
      }
    }
    this.dumeservice.createDume(this.newDume).subscribe(response => {
      this.newDume.idContext = response;
      this.dumes.push(this.newDume);
      this.newDume = {};
      this.modalRef.hide();
    });
  }

  createDumeOE(id) {
    this.dumeservice.createDumeOE(id);
  }

}
