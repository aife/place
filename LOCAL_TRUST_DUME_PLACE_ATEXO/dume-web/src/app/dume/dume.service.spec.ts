import {inject, TestBed} from '@angular/core/testing';

import {DumeService} from './dume.service';

describe('DumeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DumeService]
    });
  });

  it('should be created', inject([DumeService], (service: DumeService) => {
    expect(service).toBeTruthy();
  }));
});
