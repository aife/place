export interface RequestCriteriaView {
  value?: boolean;
  code?: string;
  titre?: string;
  description?: string;
  order?: number;
  lots?: any[];
  allLot?: boolean;
}

export interface CategorieCriteriaView {
  titre?: string;
  description?: string;
  code?: string;
  criterias?: any[];
  open?: boolean;
  order?: number;
}

