import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CriteriaDTO, RequirementGroupDTO} from '../../dto';
import cloneDeep from 'lodash-es/cloneDeep';
import {ControlContainer, NgForm} from '@angular/forms';

@Component({
  selector: 'dume-criteria-response',
  templateUrl: './criteria-response.component.html',
  styleUrls: ['./criteria-response.component.scss'],
  viewProviders: [
    {
      provide: ControlContainer,
      useFactory: function (form: NgForm) {
        return form;
      },
      deps: [NgForm]
    }
  ]
})
export class CriteriaResponseComponent implements OnInit {

  @Input()
  criteria: CriteriaDTO;

  @Output() eCertisSelected = new EventEmitter();

  customClass = 'customClass';

  rgInstanceNumber = 0;

  constructor() {
  }

  ngOnInit() {
    this.criteria.requirementGroupList.forEach(rg => rg.instanceNumber = ++this.rgInstanceNumber);
    this.criteria.requirementGroupList.sort(this.sortRequirementGroups);
  }


  addReqG(reqGr: RequirementGroupDTO, index: number): void {
    const reqG = cloneDeep(reqGr);
    reqG.instanceNumber = ++this.rgInstanceNumber;
    this.clearResponse(reqG);
    this.criteria.requirementGroupList.splice(index + 1, 0, reqG);

  }

  clearResponse(reqGr: RequirementGroupDTO) {
    reqGr.idRequirementGroupResponse = null;
    reqGr.requirementList.forEach(value => {
      value.idRequirementResponse = null;
      value.responseDTO.indicator = null;
      value.responseDTO.description = null;
      value.responseDTO.percent = null;
      value.responseDTO.amount = null;
      value.responseDTO.quantity = null;
      value.responseDTO.date = null;
      value.responseDTO.code = null;
      value.responseDTO.evidenceUrl = null;
      value.responseDTO.id = null;
    });

    if (reqGr.subGroups && reqGr.subGroups.length > 0) {
      reqGr.subGroups.forEach(value => {
        this.clearResponse(value);
      });
    }
  }

  deleteReqG(i: number) {
    this.criteria.requirementGroupList.splice(i, 1);
  }

  numberUnbounded() {
    return this.criteria.requirementGroupList.filter(value => value.unbounded).length;
  }

  sortRequirementGroups(a: RequirementGroupDTO, b: RequirementGroupDTO): number {
    // Les unbounded en premier
    if (a.unbounded && !b.unbounded) {
      return -1;
    }
    if (!a.unbounded && b.unbounded) {
      return 1;
    }
    // Trier par id : les plus récent en dernier
    if (a.id > b.id) {
      return 1;
    }
    if (b.id < a.id) {
      return -1;
    }
    return 0;
  }

  onECertisSelected() {
    this.eCertisSelected.emit(this.criteria);
  }
}
