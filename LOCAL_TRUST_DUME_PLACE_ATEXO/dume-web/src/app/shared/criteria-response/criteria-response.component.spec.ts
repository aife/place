import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CriteriaResponseComponent} from './criteria-response.component';

describe('CriteriaResponseComponent', () => {
  let component: CriteriaResponseComponent;
  let fixture: ComponentFixture<CriteriaResponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CriteriaResponseComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CriteriaResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
