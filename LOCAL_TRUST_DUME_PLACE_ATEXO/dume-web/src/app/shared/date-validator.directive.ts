import {Directive} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator} from '@angular/forms';
import * as moment from 'moment';

@Directive({
  selector: '[dumeDateValidator],[date]',
  providers: [{provide: NG_VALIDATORS, useExisting: DateValidatorDirective, multi: true}]
})
export class DateValidatorDirective implements Validator {

  constructor() {
  }

  validate(control: AbstractControl): ValidationErrors | null {
    if (control.value == null || control.value.length === 0) {
      return null;
    }
    if (!moment(control.value, 'DD/MM/YYYY', true).isValid()) {
      return {'invalidDate': {value: control.value}};
    } else {
      return null;
    }
  }

}
