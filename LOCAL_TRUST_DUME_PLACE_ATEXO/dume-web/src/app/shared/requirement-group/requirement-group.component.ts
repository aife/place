import {Component, Input, OnInit} from '@angular/core';
import {EnumDTO, RequirementDTO, RequirementGroupDTO} from '../../dto';
import {ReferentielService} from '../../service/referentiel.service';
import {ControlContainer, NgForm} from '@angular/forms';

@Component({
  selector: 'dume-requirement-group',
  templateUrl: './requirement-group.component.html',
  styleUrls: ['./requirement-group.component.scss'],
  viewProviders: [
    {
      provide: ControlContainer,
      useFactory: function (form: NgForm) {
        return form;
      },
      deps: [NgForm]
    }
  ]
})
export class RequirementGroupComponent implements OnInit {

  @Input()
  requirementGroup: RequirementGroupDTO;
  @Input()
  mainIndex: number;
  @Input()
  precisez_enabled = false;
  subGroup1: RequirementGroupDTO[];
  subGroup2: RequirementGroupDTO[];
  pays: EnumDTO[];
  currency: EnumDTO[];

  valeur: RequirementDTO;
  viewGroup: boolean;

  constructor(private referentielService: ReferentielService) {
  }

  ngOnInit() {
    this.requirementGroup.requirementList
      .filter(value => value.responseType === 'AMOUNT')
      .forEach(value => value.responseDTO.code = value.responseDTO.code ? value.responseDTO.code : 'EUR');
    const test = this.requirementGroup.requirementList.find(value2 => value2.responseType === 'INDICATOR');
    if (test) {
      this.valeur = test;
      if (this.requirementGroup.subGroups && this.requirementGroup.subGroups.length > 0) {
        this.viewGroup = this.requirementGroup.subGroups[0].fulfillmentIndicator;

        this.subGroup1 = [];
        this.subGroup2 = [];

        this.subGroup2 = this.requirementGroup.subGroups.filter(req => req.fulfillmentIndicator === this.viewGroup);
        this.subGroup1 = this.requirementGroup.subGroups.filter(req => req.fulfillmentIndicator !== this.viewGroup);
      }
    }

    this.pays = this.referentielService.getPaysRef();
    this.currency = this.referentielService.getcurrencyRef();
  }

}
