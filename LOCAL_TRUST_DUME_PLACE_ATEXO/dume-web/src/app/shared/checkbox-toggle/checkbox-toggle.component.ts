import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'dume-checkbox-toggle',
  templateUrl: './checkbox-toggle.component.html',
  styleUrls: ['./checkbox-toggle.component.scss']
})
export class CheckboxToggleComponent implements OnInit {

  @Input()
  pressed: boolean;
  @Output()
  onPressed = new EventEmitter<boolean>();

  constructor() {
  }

  ngOnInit() {
  }

  clickOnButton() {
    this.onPressed.emit(this.pressed);
  }
}
