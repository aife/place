import {Directive, ElementRef, forwardRef, HostListener, Input, Renderer2} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';


@Directive({
  selector: '[dumeNumberFormat]',
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => NumberFormatDirective), multi: true}
  ]
})
export class NumberFormatDirective implements ControlValueAccessor {

  static FORMAT_PATTERNS = {
    money: '0,0.00',
    integer: '0,0',
    year: '0',
    percent: '0.00'
  };

  @Input('dumeNumberFormat') type: 'money' | 'integer' | 'year' | 'percent' = 'money';

  propagateChange: any = () => {
  }

  @HostListener('blur')
  onTouched: any = () => {
  }

  constructor(private el: ElementRef, private renderer: Renderer2) {
  }

  writeValue(obj: any): void {
    if (obj) {
      this.renderer.setProperty(this.el.nativeElement, 'value', this.formatMoney(obj));
    } else {
      this.renderer.setProperty(this.el.nativeElement, 'value', '');
    }
  }

  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  @HostListener('change', ['$event'])
  onChanges($event: any) {
    const value = $event.target.value;
    let val = this.parseMoney(value);
    if (!isNaN(val) && val != null) {
      const formatedVal = this.formatMoney(val);
      this.renderer.setProperty(this.el.nativeElement, 'value', formatedVal);
      val = this.parseMoney(formatedVal);
    }
    this.propagateChange(val);
  }

  formatMoney(inputMontant: number): string {
    return numeral(inputMontant).format(NumberFormatDirective.FORMAT_PATTERNS[this.type]);
  }

  parseMoney(inputString: string): any {
    const parsedMontant = inputString.replace(',', '.').replace(/\s+/g, '');
    if (parsedMontant === '') {
      return null;
    }
    return Number(parsedMontant);
  }
}
