export const dumeBasePath = '/dume-api/';

// CRITERIA API ----------------------------------------------------
export const criteriaControllerPath = `${dumeBasePath}criteria/`;
export const getAllSelectionCriteriasPath = `${criteriaControllerPath}selection`;
export const getAllSelectionCriteriaCategoriesPath = `${criteriaControllerPath}selectionCategories`;
export const getAllExclusionCriteriasPath = `${criteriaControllerPath}exclusion`;
export const getAllOtherCriteriasPath = `${criteriaControllerPath}other`;

// DUME_REQUEST  API ----------------------------------------------------
export const dumeRequestBasePath = 'acheteur/';
export const dumeRequestControllerPath = `${dumeBasePath}${dumeRequestBasePath}`;
export const getDumeRequestByIDPath = `${dumeRequestControllerPath}get/`;
export const updateDumeRequestPath = `${dumeRequestControllerPath}update/`;
export const getAllDumeRequestPath = `${dumeRequestControllerPath}all/`;
export const createDumeRequestPath = `${dumeRequestControllerPath}create/`;
export const downloadDumeRequestPDF = `${dumeRequestControllerPath}downloadPdf/`;


// DUME_RESPONSE  API ----------------------------------------------------
export const dumeResponseBasePath = 'operateurEconomique/';
export const dumeResponseControllerPath = `${dumeBasePath}${dumeResponseBasePath}`;
export const createDumeResponsePath = `${dumeResponseControllerPath}create/`;
export const findDumeResponsePath = `${dumeResponseControllerPath}find/`;
export const updateDumeResponsePath = `${dumeResponseControllerPath}update/`;
export const getDumeOEByIdPath = `${dumeResponseControllerPath}get/`;
export const updateAndDownload = `${dumeResponseControllerPath}updateAndDownload`;
export const downloadOEXML = `${dumeResponseControllerPath}downloadXml/`;
export const eCertisProofPath = `${dumeResponseControllerPath}e-certis-proof/`

// DUME_Referentiel API ----------------------------------------------------
export const dumeReferentielBasePath = 'referentiel/';
export const dumeReferentielControllerPath = `${dumeBasePath}${dumeReferentielBasePath}`;
export const dumeReferentielGetCountryPath = `${dumeReferentielControllerPath}pays`;
export const dumeReferentielGetCurrencyPath = `${dumeReferentielControllerPath}devises`;
export const dumeReferentielGetMarketTypesPath = `${dumeReferentielControllerPath}typesMarche`;


// DUME_USER API ----------------------------------------------------
export const dumeUserBasePath = 'user/';
export const dumeUserControllerPath = `${dumeBasePath}${dumeUserBasePath}`;
export const dumeAddUserPath = `${dumeUserControllerPath}addUser?name=`;
export const dumeGetAllUserPath = `${dumeUserControllerPath}all`;
