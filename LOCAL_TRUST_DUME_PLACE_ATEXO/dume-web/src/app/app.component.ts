import {Component} from '@angular/core';

@Component({
  selector: 'dume-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
}
