// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

import {BrowserModule} from '@angular/platform-browser';
import {AccordionModule, AlertModule, BsDropdownModule, CollapseModule, ModalModule, PaginationModule, TabsModule, TooltipModule} from 'ngx-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {AuthenticationService} from '../app/service/authentication.service';
import {DumeComponent} from '../app/dume/dume.component';
import {ReferentielService} from '../app/service/referentiel.service';
import {FormsModule} from '@angular/forms';
import {DumeService} from '../app/dume/dume.service';
import {CheckboxCriteriaComponent} from '../app/dume/checkbox-criteria/checkbox-criteria.component';
import {RequirementGroupComponent} from '../app/shared/requirement-group/requirement-group.component';
import {APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {FakeMpeComponent} from '../app/dume/fake-mpe/fake-mpe.component';
import {CriteriaResponseComponent} from '../app/shared/criteria-response/criteria-response.component';
import {AppComponent} from '../app/app.component';
import {InitConfigService} from '../app/service/init-config.service';
import {DumeResponseComponent} from '../app/dume/dume-response/dume-response.component';
import {RouterModule} from '@angular/router';
import {NumberFormatDirective} from '../app/shared/number-format.directive';
import {DateValidatorDirective} from '../app/shared/date-validator.directive';
import {CheckboxToggleComponent} from '../app/shared/checkbox-toggle/checkbox-toggle.component';
import {ErrorHandlingService} from '../app/service/error-handling.service';
import {NgSelectModule} from '@ng-select/ng-select';

export const environment = {
  production: false,
  config: {
    declarations: [
      AppComponent,
      DumeComponent,
      CheckboxCriteriaComponent,
      FakeMpeComponent,
      DumeResponseComponent,
      CriteriaResponseComponent,
      RequirementGroupComponent,
      CheckboxToggleComponent,
      NumberFormatDirective,
      DateValidatorDirective
    ],
    imports: [
      RouterModule.forRoot([
        {path: '', redirectTo: '/fakeMpe', pathMatch: 'full'},
        {path: 'acheteur/:id', component: DumeComponent},
        {path: 'operateurEconomique/:id', component: DumeResponseComponent},
        {path: 'fakeMpe', component: FakeMpeComponent}
      ]),
      BrowserModule,
      HttpClientModule,
      FormsModule,
      NgSelectModule,
      BsDropdownModule.forRoot(),
      TabsModule.forRoot(),
      CollapseModule.forRoot(),
      AccordionModule.forRoot(),
      ModalModule.forRoot(),
      TooltipModule.forRoot(),
      AlertModule.forRoot(),
      BsDropdownModule.forRoot(),
      PaginationModule.forRoot()
    ],
    providers: [
      DumeService,
      ReferentielService,
      AuthenticationService,
      InitConfigService,
      ErrorHandlingService,
      {
        provide: APP_INITIALIZER,
        useFactory: (ds: InitConfigService) => function () {
          return ds.init();
        },
        deps: [InitConfigService],
        multi: true
      }
    ],
    exports: [RouterModule],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
      entryComponents: [AppComponent]
  }
};
