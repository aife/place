import {BrowserModule} from '@angular/platform-browser';
import {AccordionModule, AlertModule, BsDropdownModule, CollapseModule, ModalModule, PaginationModule, TabsModule, TooltipModule} from 'ngx-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {AuthenticationService} from '../app/service/authentication.service';
import {DumeComponent} from '../app/dume/dume.component';
import {ReferentielService} from '../app/service/referentiel.service';
import {FormsModule} from '@angular/forms';
import {DumeService} from '../app/dume/dume.service';
import {CheckboxCriteriaComponent} from '../app/dume/checkbox-criteria/checkbox-criteria.component';
import {RequirementGroupComponent} from '../app/shared/requirement-group/requirement-group.component';
import {APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CriteriaResponseComponent} from '../app/shared/criteria-response/criteria-response.component';
import {InitConfigService} from '../app/service/init-config.service';
import {DumeResponseComponent} from '../app/dume/dume-response/dume-response.component';
import {RouterModule} from '@angular/router';
import {APP_BASE_HREF} from '@angular/common';
import {DateValidatorDirective} from '../app/shared/date-validator.directive';
import {NumberFormatDirective} from '../app/shared/number-format.directive';
import {CheckboxToggleComponent} from '../app/shared/checkbox-toggle/checkbox-toggle.component';
import {ErrorHandlingService} from '../app/service/error-handling.service';
import {NgSelectModule} from '@ng-select/ng-select';

export function init(ds: InitConfigService) {
  return ds.init();
}
export const environment = {
  production: true,
  config: {
    declarations: [
      DumeResponseComponent,
      DumeComponent,
      CheckboxCriteriaComponent,
      CriteriaResponseComponent,
      RequirementGroupComponent,
      CheckboxToggleComponent,
      NumberFormatDirective,
      DateValidatorDirective
    ],
    imports: [
      RouterModule.forRoot([], {initialNavigation: 'disabled'}),
      BrowserModule,
      HttpClientModule,
      FormsModule,
      NgSelectModule,
      BsDropdownModule.forRoot(),
      TabsModule.forRoot(),
      CollapseModule.forRoot(),
      AccordionModule.forRoot(),
      ModalModule.forRoot(),
      TooltipModule.forRoot(),
      AlertModule.forRoot(),
      BsDropdownModule.forRoot(),
      PaginationModule.forRoot()
    ],
    providers: [
      DumeService,
      ReferentielService,
      AuthenticationService,
      InitConfigService,
      ErrorHandlingService,
      {
        provide: APP_INITIALIZER,
        useFactory: (ds: InitConfigService) => function () {
          return ds.init();
        },
        deps: [InitConfigService],
        multi: true
      }, {
        provide: APP_BASE_HREF,
        useValue: '/'
      }
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
      entryComponents: [DumeComponent, DumeResponseComponent]
  }
};
