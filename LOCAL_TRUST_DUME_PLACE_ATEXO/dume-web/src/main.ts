import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from './app/app.module';
import {environment} from './environments/environment';
import {ErrorHandlingService} from './app/service/error-handling.service';

if (environment.production) {
  enableProdMode();
}

numeral.locale('fr');

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => {
    if (document.getElementsByTagName('dume-request').length > 0) {
      handleBootstrapFailure(document.getElementsByTagName('dume-request')[0]);
    }
    if (document.getElementsByTagName('dume-response').length > 0) {
      handleBootstrapFailure(document.getElementsByTagName('dume-response')[0]);
    }
  });

function handleBootstrapFailure(element: Element) {
  const alertDiv = document.createElement('div');
  alertDiv.className = 'alert alert-danger';
  alertDiv.innerText = ErrorHandlingService.ERROR_GENERIC;
  element.parentNode.replaceChild(alertDiv, element);
}
