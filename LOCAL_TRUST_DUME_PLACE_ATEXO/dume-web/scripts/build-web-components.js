const concat = require('concat');
const dumeWebComponent = './dist/dume/';

build = async (jsVersion) => {
  const files = [
    `${dumeWebComponent}/inline.bundle.js`,
    `${dumeWebComponent}/main.bundle.js`,
    `${dumeWebComponent}/polyfills.bundle.js`,
    `${dumeWebComponent}/scripts.bundle.js`,
    `${dumeWebComponent}/vendor.bundle.js`
  ];

  await concat(files, `${dumeWebComponent}/dume.js`);
  await concat(files, `${dumeWebComponent}/dume-${jsVersion}.js`);
}

console.log("build des web components dume en ES2015")
build('es2015');

console.log("build des web components dume en ES5")
build('es5');
