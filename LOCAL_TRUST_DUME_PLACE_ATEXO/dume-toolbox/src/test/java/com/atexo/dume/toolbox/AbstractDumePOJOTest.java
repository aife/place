package com.atexo.dume.toolbox;


import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import org.junit.Before;
import org.junit.Test;
import org.springframework.util.CollectionUtils;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.*;
import java.util.Map.Entry;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static java.lang.reflect.Modifier.isAbstract;
import static org.junit.Assert.*;

/**
 * A utility class which allows for testing entity and transfer object classes. This is mainly for code coverage since
 * these types of objects are normally nothing more than getters and setters. If any logic exists in the method, then
 * the get method name should be sent in as an ignored field and a custom test function should be written.
 */
public abstract class AbstractDumePOJOTest {

    /**
     * A map of default mappers for common objects.
     */
    private static final ImmutableMap<Class<?>, Supplier<?>> DEFAULT_MAPPERS;

    private static Map<Class<?>, Object> mappings = new HashMap<>();
    private static Set<Class<?>> nonAbstractClasses;


    static {
        final Builder<Class<?>, Supplier<?>> mapperBuilder = ImmutableMap.builder();

        /* Primitives */
        mapperBuilder.put(int.class, () -> 0);
        mapperBuilder.put(double.class, () -> 0.0d);
        mapperBuilder.put(float.class, () -> 0.0f);
        mapperBuilder.put(long.class, () -> 0l);
        mapperBuilder.put(boolean.class, () -> true);
        mapperBuilder.put(short.class, () -> (short) 0);
        mapperBuilder.put(byte.class, () -> (byte) 0);
        mapperBuilder.put(char.class, () -> (char) 0);

        mapperBuilder.put(Integer.class, () -> Integer.valueOf(0));
        mapperBuilder.put(Double.class, () -> Double.valueOf(0.0));
        mapperBuilder.put(Float.class, () -> Float.valueOf(0.0f));
        mapperBuilder.put(Long.class, () -> Long.valueOf(0));
        mapperBuilder.put(Boolean.class, () -> Boolean.TRUE);
        mapperBuilder.put(Short.class, () -> Short.valueOf((short) 0));
        mapperBuilder.put(Byte.class, () -> Byte.valueOf((byte) 0));
        mapperBuilder.put(Character.class, () -> Character.valueOf((char) 0));

        mapperBuilder.put(BigDecimal.class, () -> BigDecimal.ONE);
        mapperBuilder.put(Date.class, () -> new Date());

        /* Collection Types. */
        mapperBuilder.put(Set.class, () -> Collections.emptySet());
        mapperBuilder.put(SortedSet.class, () -> Collections.emptySortedSet());
        mapperBuilder.put(List.class, () -> Collections.emptyList());
        mapperBuilder.put(Map.class, () -> Collections.emptyMap());
        mapperBuilder.put(SortedMap.class, () -> Collections.emptySortedMap());

        DEFAULT_MAPPERS = mapperBuilder.build();
    }

    /**
     * The get fields to ignore and not try to test.
     */
    private final Set<String> ignoredGetFields;

    /**
     * A custom mapper. Normally used when the test class has abstract objects.
     */
    private final ImmutableMap<Class<?>, Supplier<?>> mappers;

    /**
     * Creates an instance of {@link AbstractDumePOJOTest} with the default ignore fields.
     */
    protected AbstractDumePOJOTest() {
        this(null, null);
    }

    /**
     * Creates an instance of {@link AbstractDumePOJOTest} with ignore fields and additional custom mappers.
     *
     * @param customMappers Any custom mappers for a given class type.
     * @param ignoreFields  The getters which should be ignored (e.g., "getId" or "isActive").
     */
    protected AbstractDumePOJOTest(Map<Class<?>, Supplier<?>> customMappers, Set<String> ignoreFields) {
        this.ignoredGetFields = new HashSet<>();
        if (ignoreFields != null) {
            this.ignoredGetFields.addAll(ignoreFields);
        }
        this.ignoredGetFields.add("getClass");

        if (customMappers == null) {
            this.mappers = DEFAULT_MAPPERS;
        } else {
            final Builder<Class<?>, Supplier<?>> builder = ImmutableMap.builder();
            builder.putAll(customMappers);
            builder.putAll(DEFAULT_MAPPERS);
            this.mappers = builder.build();
        }
    }

    /**
     * Calls a getter and verifies the result is what is expected.
     *
     * @param fieldName The field name (used for error messages).
     * @param getter    The get {@link Method}.
     * @param instance  The test instance.
     * @param expected  The expected result.
     * @throws IllegalAccessException    if this Method object is enforcing Java language access control and the underlying
     *                                   method is inaccessible.
     * @throws IllegalArgumentException  if the method is an instance method and the specified object argument is not an
     *                                   instance of the class or interface declaring the underlying method (or of a subclass or implementor
     *                                   thereof); if the number of actual and formal parameters differ; if an unwrapping conversion for
     *                                   primitive arguments fails; or if, after possible unwrapping, a parameter value cannot be converted to
     *                                   the corresponding formal parameter type by a method invocation conversion.
     * @throws InvocationTargetException if the underlying method throws an exception.
     */
    private void callGetter(String fieldName, Method getter, Object instance, Object expected)
            throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {

        //We Have to cast our instance to the corresponding Class
        instance = get(findClass(instance));


        final Object getResult = getter.invoke(instance);

        if (getter.getReturnType().isPrimitive()) {
            /* Calling assetEquals() here due to autoboxing of primitive to object type. */
            assertEquals(fieldName + " is different", expected, getResult);
        } else {
            /* This is a normal object. The object passed in should be the exactly same object we get back. */
            assertSame(fieldName + " is different", expected, getResult);
        }
    }

    /**
     * Creates an object for the given {@link Class}.
     *
     * @param fieldName The name of the field.
     * @param clazz     The {@link Class} type to create.
     * @return A new instance for the given {@link Class}.
     * @throws InstantiationException If this Class represents an abstract class, an interface, an array class, a
     *                                primitive type, or void; or if the class has no nullary constructor; or if the instantiation fails
     *                                for some other reason.
     * @throws IllegalAccessException If the class or its nullary constructor is not accessible.
     */
    private Object createObject(String fieldName, Class<?> clazz)
            throws InstantiationException, IllegalAccessException {

        try {
            final Supplier<?> supplier = this.mappers.get(clazz);
            if (supplier != null) {
                return supplier.get();
            }

            if (clazz.isEnum()) {
                return clazz.getEnumConstants()[0];
            }

            return clazz.newInstance();
        } catch (IllegalAccessException | InstantiationException e) {
            throw new RuntimeException("Unable to create objects for field '" + fieldName + "'.", e);
        }
    }

    /**
     * Returns an instance list to be used to test the get and set methods.
     *
     * @return An instance to use to test the get and set methods.
     */
    protected abstract List<Object> getInstanceList() throws IllegalAccessException, InstantiationException;

    /**
     * Returns a list of all concerned class.
     *
     * @return A Set of all scanned class by the module.
     */
    protected abstract Set<Class<?>> getClasses();


    /**
     * This method will add a created instance for the test to a huge list
     * to be used for performing the unit tests.
     *
     * @param instance the instanace to be added.
     */
    protected abstract void addInstance(final Object instance);


    /**
     * This method will get all classes in a package to scan.
     *
     * @return Set<Class<?>>
     */
    protected abstract Set<Class<?>> getAllClassForPackage();

    /**
     * Tests all the getters and setters. Verifies that when a set method is called, that the get method returns the
     * same thing. This will also use reflection to set the field if no setter exists (mainly used for user immutable
     * entities but Hibernate normally populates).
     *
     * @throws Exception If an expected error occurs.
     */
    @Test
    public void testGettersAndSetters() throws Exception {
        /* Sort items for consistent test runs. */
        final SortedMap<String, GetterSetterPair> getterSetterMapping = new TreeMap<>();

        final List<Object> instanceTList = getInstanceList();
        for (final Object instance : instanceTList) {
            getterSetterMapping.clear();
            if (instance.getClass().getDeclaredMethods().length > 0) {
                for (final Method method : instance.getClass().getDeclaredMethods()) {
                    final String methodName = method.getName();

                    if (this.ignoredGetFields.contains(methodName)) {
                        continue;
                    }

                    String objectName;
                    if (methodName.startsWith("get") && method.getParameters().length == 0) {
                /* Found the get method. */
                        objectName = methodName.substring("get".length());

                        GetterSetterPair getterSettingPair = getterSetterMapping.get(objectName);
                        if (getterSettingPair == null) {
                            getterSettingPair = new GetterSetterPair();
                            getterSetterMapping.put(objectName, getterSettingPair);
                        }
                        getterSettingPair.setGetter(method);
                    } else if (methodName.startsWith("set") && method.getParameters().length == 1) {
                /* Found the set method. */
                        objectName = methodName.substring("set".length());

                        GetterSetterPair getterSettingPair = getterSetterMapping.get(objectName);
                        if (getterSettingPair == null) {
                            getterSettingPair = new GetterSetterPair();
                            getterSetterMapping.put(objectName, getterSettingPair);
                        }
                        getterSettingPair.setSetter(method);
                    } else if (methodName.startsWith("is") && method.getParameters().length == 0) {
                /* Found the is method, which really is a get method. */
                        objectName = methodName.substring("is".length());

                        GetterSetterPair getterSettingPair = getterSetterMapping.get(objectName);
                        if (getterSettingPair == null) {
                            getterSettingPair = new GetterSetterPair();
                            getterSetterMapping.put(objectName, getterSettingPair);
                        }
                        getterSettingPair.setGetter(method);
                    }
                }

            /*
         * Found all our mappings. Now call the getter and setter or set the field via reflection and call the getting
         * it doesn't have a setter.
         */
                for (final Entry<String, GetterSetterPair> entry : getterSetterMapping.entrySet()) {
                    final GetterSetterPair pair = entry.getValue();

                    final String objectName = entry.getKey();
                    final String fieldName = objectName.substring(0, 1).toLowerCase() + objectName.substring(1);

                    if (pair.hasGetterAndSetter()) {
                /* Create an object. */
                        final Class<?> parameterType = pair.getSetter().getParameterTypes()[0];
                        final Object newObject = createObject(fieldName, parameterType);

                        pair.getSetter().invoke(instance, newObject);

                        callGetter(fieldName, pair.getGetter(), instance, newObject);
                    } else if (pair.getGetter() != null) {
                /*
                 * Object is immutable (no setter but Hibernate or something else sets it via reflection). Use
                 * reflection to set object and verify that same object is returned when calling the getter.
                 */
                        final Object newObject = createObject(fieldName, pair.getGetter().getReturnType());
                        final Field field = instance.getClass().getDeclaredField(fieldName);
                        field.setAccessible(true);
                        field.set(instance, newObject);

                        callGetter(fieldName, pair.getGetter(), instance, newObject);
                    }
                }
            }
        }


    }

    public static void addMappings(final Map<Class<?>, Object> maps) {
        mappings.putAll(maps);
    }


    public <T> T get(Class<T> clazz) {
        return clazz.cast(mappings.get(clazz));
    }

    public <T> void put(Class<T> clazz, T favorite) {
        mappings.put(clazz, favorite);
    }


    private static Class<?> findClass(final Object instance) {
        Class<?> retValue = null;
        final List<Class<?>> result = mappings.entrySet().stream().filter(map -> instance.equals(map.getValue()))
                .map(map -> map.getKey())
                .collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(result)) {
            retValue = result.get(0);
        }

        return retValue;
    }

    @Test
    public void testAllConstructors() throws IllegalAccessException, InstantiationException {
        for (final Class<?> clazz : nonAbstractClasses) {
                final Object instance = clazz.newInstance();
                assertNotNull(instance);
        }
    }


    @Test
    public void testAllGetterAndSetter() throws IllegalAccessException, InstantiationException {
        for (final Class<?> clazz : nonAbstractClasses) {
                final Object instance1 = clazz.newInstance();
        }
    }

    @Test
    public void testAllEqualsMethodsGlobal() throws IllegalAccessException, InstantiationException {
        for (final Class<?> clazz : nonAbstractClasses) {
                final Object instance1 = clazz.newInstance();
                final Object instance2 = clazz.newInstance();
                assertNotNull(instance1);
                assertNotNull(instance2);
                assertTrue(instance1.equals(instance2));
        }
    }

    @Test
    public void testAllEqualsMethodsSameObject() throws IllegalAccessException, InstantiationException {
        for (final Class<?> clazz : nonAbstractClasses) {
                final Object instance1 = clazz.newInstance();
                assertNotNull(instance1);
                assertTrue(instance1.equals(instance1));
        }
    }

    @Test
    public void testAllEqualsMethodsNullObject() throws IllegalAccessException, InstantiationException {
        for (final Class<?> clazz : nonAbstractClasses) {
                final Object instance1 = clazz.newInstance();
                assertNotNull(instance1);
                assertFalse(instance1.equals(null));
        }
    }

    @Test
    public void testAllEqualsMethodsFinalStep() throws IllegalAccessException, InstantiationException {
        final PodamFactory factory = new PodamFactoryImpl();
        for (final Class<?> clazz : nonAbstractClasses) {
                final Object instance1 = factory.manufacturePojo(clazz);
                final Object instance2 = factory.manufacturePojo(clazz);
                assertNotNull(instance1);
                assertNotNull(instance2);
                assertFalse(instance1.equals(instance2));
        }
    }

    @Test
    public void testAllHashCodeMethods() throws IllegalAccessException, InstantiationException {
        for (final Class<?> clazz : nonAbstractClasses) {
                final Object instance1 = clazz.newInstance();
                final Object instance2 = clazz.newInstance();
                assertNotNull(instance1);
                assertNotNull(instance2);
                assertEquals(instance1.hashCode(), instance2.hashCode());
        }
    }

    @Test
    public void testAllToStringMethods() throws IllegalAccessException, InstantiationException {
        for (final Class<?> clazz : nonAbstractClasses) {
                final Object instance1 = clazz.newInstance();
                assertNotNull(instance1);
                assertNotNull(instance1.toString());
                assertTrue(instance1.toString().contains(clazz.getSimpleName()));
        }
    }


    @Before
    public void setUp() throws IllegalAccessException, InstantiationException {
        if (CollectionUtils.isEmpty(getInstanceList())) {
            final Map<Class<?>, Object> map = new HashMap<>();
            nonAbstractClasses = new HashSet<>();
            for (final Class<?> clazz : getClasses()) {
                if (!isAbstract(clazz.getModifiers())) {
                    nonAbstractClasses.add(clazz);
                    final Object instance = clazz.newInstance();
                    addInstance(instance);
                    map.put(clazz, instance);
                }
            }
            addMappings(map);
        }

    }

}
