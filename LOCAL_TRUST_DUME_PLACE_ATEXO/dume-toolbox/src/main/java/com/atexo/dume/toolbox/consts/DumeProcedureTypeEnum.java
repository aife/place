package com.atexo.dume.toolbox.consts;

import com.fasterxml.jackson.annotation.JsonValue;

public enum DumeProcedureTypeEnum {

    ADAPTED_PROC(1),
    OPEN_TENDER(2),
    RESTRICTED_TENDER(3),
    COMP_PROC_WITH_NEGO(4),
    NEGOCIATED_PROC_WITH_PRIOR_CALL(5),
    NEGOCIATED_MARKET_WITHOUT_ADS_OR_PRIOR_CALL(6),
    COMPETITIVE_DIALOGUE(7),
    CONTEST(8),
    OTHER(9);

    private int procedure;
    
    DumeProcedureTypeEnum(int procedure) {
        this.procedure = procedure;
    }

    @JsonValue
    public int getProcedure() {
        return procedure;
    }


}
