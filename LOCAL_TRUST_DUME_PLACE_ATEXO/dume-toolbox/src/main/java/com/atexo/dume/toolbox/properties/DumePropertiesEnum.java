package com.atexo.dume.toolbox.properties;

import java.util.Optional;

import static java.util.Arrays.asList;

public enum DumePropertiesEnum {

    DUME_SN_TIMEOUT("dume.sn.timeout"), JWT_SIGNING_KEY("JWT_SIGNING_KEY"), SN_BASE_PATH("SN_BASE_PATH"), SN_OAUTH2_PATH("SN_OAUTH2_PATH"), PDF_STORAGE_FOLDER("PDF_STORAGE_FOLDER"), DUME_HTTP_CLIENT_KEEPALIVE("dume.httpclient.keepalive"), DUME_HTTP_CLIENT_POOL_SIZE("dume.httpclient.pool.size"), CRON_EXPRESSION_ACHETEUR("cron.expression.acheteur"), CRON_EXPRESSION_OE("cron.expression.oe"), CRON_EXPRESSION_DONNEES_ESSENTIELLES("cron.expression.donneesEssentielles"), JAVA_MELODY_LOGIN_PASSWORD("javamelody.init-parameters.authorized-users"), PUBLICATION_MAX_ATTEMPTS("PUBLICATION_MAX_ATTEMPTS"), SN_OAUTH2_ENABLED("SN_OAUTH2_ENABLED");

    private final String value;

    DumePropertiesEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static DumePropertiesEnum getByValue(String value) {
        Optional<DumePropertiesEnum> optionalDumePropertiesEnum = asList(DumePropertiesEnum.values()).stream().filter(dumePropertiesEnum -> dumePropertiesEnum.getValue().equals(value)).findFirst();

        if (optionalDumePropertiesEnum.isPresent()) {
            return optionalDumePropertiesEnum.get();
        }
        throw new IllegalArgumentException("il n'existe aucun enum avec la valeur suivante" + value);
    }
}
