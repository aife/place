package com.atexo.dume.toolbox.consts;

public final class DumeJsonConstants {

    // RetourStatus && PublicationsStatus
    public static final String STATUS = "statut";
    public static final String MESSAGE = "message";
    public static final String IDCONTEXT = "idContext";
    public static final String IDCONTEXT_OE = "idContexteOE";
    public static final String NUMERO_SN = "numeroSN";
    public static final String VERSION = "version";
    public static final String NOM_OFFICIEL = "nomOfficiel";
    public static final String PAYS = "pays";
    public static final String REFERENCE_CONSULTATION = "referenceConsultation";
    public static final String TYPE_PROCEDURE = "typeProcedure";
    public static final String INTITULE = "intitule";
    public static final String OBJET = "objet";
    public static final String METADATA = "metadata";
    public static final String LOTS = "lots";
    public static final String DUME_ACHETEURS = "dumeAcheteurs";

    public static final String IDENTIFIAN_INTERNE_DUME = "identifianInterneDUME";
    public static final String NUMERO_LOT = "numeroLot";
    public static final String INTILULE_LOT = "intituleLot";
    public static final String NUMEROS_DUME = "numerosDume";

    public static final String IS_STANDARD = "isStandard";
    public static final String MESSAGE_ERREUR = "message";
    public static final String PHONE = "telephone";
    public static final String CONTACT = "contact";

    public static final String IDS_CONTEXTE_GROUPEMENT = "idsContexteGroupement";
    public static final String SIRET_MANDATAIRE = "siretMandataire";

    //SN PART
    public static final String PLATFORM_IDENTIFIER_TYPE = "typeIdPlateforme";
    public static final String PLATFORM_IDENTIFIER = "idPlateforme";
    public static final String PLATFORM_TECHNICAL_IDENTIFIER = "idTechniquePlateforme";
    public static final String PLATFORM = "plateforme";
    public static final String OPERATION = "operation";
    public static final String FORMAT = "format";
    public static final String REQUESTER_IDENTIFIER = "idDemandeur";
    public static final String PLACE_REQUESTER_NAME = "rsDemandeur";
    public static final String DUME_IDENTIFIER = "idDume";
    public static final String DUME_ID_HOLDER = "dume";
    public static final String DUME_STATUS_IDENTIFIER = "idStatutDume";
    public static final String PROCEDURE_TYPE_IDENTIFIER = "idTypeProcedure";
    public static final String MARKET_TYPE_IDENTIFIER = "idTypeMarche";
    public static final String MARKET_NATURE_IDENTIFIER = "idNatureMarche";
    public static final String CONSULTATION_IDENTIFIER = "idConsultation";
    public static final String IDENTIFIER_A = "identifiantA";
    public static final String CONSULTATION_LABEL = "libelleConsultation";
    public static final String CONSULTATION_FUNC_REF = "refFonctionnelleConsultation";
    public static final String FOLDS_DEADLINE_DELEVERY = "DLRO";
    public static final String ACCESSIBLE = "accessible";
    public static final String IMPACTED_LOT = "lotsConcernes";
    public static final String REQUESTER_SIRET = "siretAcheteur";
    public static final String DUME_A = "dumeA";
    public static final String DUME_OE = "dumeOE";
    public static final String DUME_XML = "xmlDume";
    public static final String SIRET_OE = "siretOE";

    private DumeJsonConstants() {
        throw new IllegalStateException("DumeJsonConstants class");
    }

}
