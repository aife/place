package com.atexo.dume.toolbox.csv;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Class Utility for the CSV Reader
 */
public class CSVReader {

    private CSVReader() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * @param type        the type of Class to parse
     * @param inputStream the source
     * @param <T>         the classe
     * @return List<T> of object from the input stream
     * @throws IOException
     */
    public static <T> List<T> loadObjectList(Class<T> type, InputStream inputStream) throws IOException {
        CsvSchema bootstrapSchema = CsvSchema.emptySchema().withHeader();
        CsvMapper mapper = new CsvMapper();
        MappingIterator<T> readValues =
                mapper.readerFor(type).with(bootstrapSchema).readValues(inputStream);
        return readValues.readAll();
    }
}
