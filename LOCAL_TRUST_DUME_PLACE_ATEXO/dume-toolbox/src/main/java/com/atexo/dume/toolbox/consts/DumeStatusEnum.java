package com.atexo.dume.toolbox.consts;

public enum DumeStatusEnum {

    DRAFT_COPY("01"),
    VALIDATED("02"),
    REPLACED("03");

    private String status;

    DumeStatusEnum(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

}
