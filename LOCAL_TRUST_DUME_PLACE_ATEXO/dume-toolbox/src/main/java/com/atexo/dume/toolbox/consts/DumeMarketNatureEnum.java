package com.atexo.dume.toolbox.consts;

import com.fasterxml.jackson.annotation.JsonValue;

public enum DumeMarketNatureEnum {

    MARKET(1),
    PARTNERSHIP(2),
    FRAMEWORK_AGREEMENT(3),
    SUBSEQUENT_CONTRACT(4),
    RESERVED_MARKET(5),
    OTHER(6);


    private int marketNature;

    DumeMarketNatureEnum(int marketNature) {
        this.marketNature = marketNature;
    }

    @JsonValue
    public int getMarketNature() {
        return marketNature;
    }
}
