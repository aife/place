package com.atexo.dume.toolbox.rest;

public class NationalServiceOperations {

    public static final String SAVE_DUME = "enregistrerDume";
    public static final String GET_DUME_MODEL = "recupererModelDume";
    public static final String GET_FORMATED_DUME = "recupererDumeFormate";
    public static final String GET_DUME_REQUEST_WITH_BOAMP = "recupererDumeAAvecBOAMP";
    public static final String REMOVE_DUME_REQUEST = "supprimerDume";
    public static final String DOWNLOAD_DUME_REQUEST_WITH_PJ = "telechargerDumeAvecPJ";
    public static final String SEARCH_DUMES = "rechercherDumes";
    public static final String ATTACH_PJ_FOR_DUME_REQUEST = "rattacherPieceJointe";
    public static final String REMOVE_DUME_REQUEST_PJ = "supprimerPieceJointe";
    public static final String MERGE_DUMES_WITH_ENTREPRISE_DATA = "FusionnerDumeAvecDonneesEntreprise";
    public static final String MERGE_DUME = "FusionnerDumes";
    public static final String GET_DUME_RESPONSE_WITH_BOAMP = "recupererDumeOEAvecBOAMP";
    public static final String GET_DUME_WITH_BOAMP = "FusionnerDumeAvecBOAMP";
    public static final String MERGE_DUME_RESPONSE_WITH_DATA_AND_BOAMP = "FusionnerDumeAvecDonneesEtBOAMP";
    public static final String UPDATE_METADATA = "mettreAJourDume";
    public static final String SAVE_ESSENTIAL_DATA = "enregistrerDE";
    public static final String UPDATE_ESSENTIAL_DATA = "mettreAJourDonneesEssentielles";
    public static final String SAVE_SIMPLIFIED_DUME = "creerDumeSimple";
    public static final String GET_E_CERTIS = "recupererEvidence";

    private NationalServiceOperations() {
    }
}
