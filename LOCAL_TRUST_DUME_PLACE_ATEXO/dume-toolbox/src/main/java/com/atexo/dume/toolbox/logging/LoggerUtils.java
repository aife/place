package com.atexo.dume.toolbox.logging;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

public class LoggerUtils {

    private static final Logger logger = LoggerFactory.getLogger(LoggerUtils.class);
    private static final String TARGET_LOGGING_FILE = "target_file";
    private LoggerUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static final String toJSON(Object o) {
        ObjectMapper jsonMapper = new ObjectMapper();
        try {
            return jsonMapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage(), e);
            return "";
        }
    }

    public static void populateLog(String discriminant){
        if(discriminant!=null)
            MDC.put(TARGET_LOGGING_FILE,discriminant);
    }

    public static void removeLog() {
        MDC.clear();
    }
}
