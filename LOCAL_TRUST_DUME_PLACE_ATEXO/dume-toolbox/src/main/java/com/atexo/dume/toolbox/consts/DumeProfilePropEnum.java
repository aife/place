package com.atexo.dume.toolbox.consts;

public enum DumeProfilePropEnum {

    MODEL_DUME_REQUEST("ID_DUME_REQUEST_MODEL");

    private String label;

    DumeProfilePropEnum(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

}
