package com.atexo.dume.toolbox.audit.aware.impl;

import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

/**
 * AuditorAware class responsible of getting all needed informations
 * in order to save object states.
 */
public class AuditorAwareImpl implements AuditorAware<String> {


    /**
     * Get the current user connected and starting actions
     * in order to log actions
     * @return
     */
    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.of("dev-user");
    }

}
