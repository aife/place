package com.atexo.dume.toolbox.consts;

import com.fasterxml.jackson.annotation.JsonValue;

public enum DumeMarketType {

    WORKS(1),
    SUPPLY(2),
    SERVICE(3);

    private int marketTypeid;

    DumeMarketType(int marketTypeid) {
        this.marketTypeid = marketTypeid;
    }

    @JsonValue
    public int getMarketTypeid() {
        return marketTypeid;
    }
}
