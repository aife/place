package com.atexo.dume.toolbox.rest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestClientWrapper {

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
