package com.atexo.dume.toolbox.consts;

public enum DumeSchemaProtocols {

    HTTPS("https"),
    HTTP("http");
    private String schema;

    DumeSchemaProtocols(String schema) {
        this.schema = schema;
    }

    public String getSchema() {
        return schema;
    }
}
