package com.atexo.dume.toolbox.consts;

/**
 * Class contenant les tags pour la documentation SWAGGER
 */

public class SwaggerConstants {

    private SwaggerConstants() {
        throw new IllegalStateException("Utility class");
    }

    public static final String REFERENTIELS = "Référentiels";
    public static final String ADMINISTRATION = "Administration";
    public static final String CRITERES = "Critères";
    public static final String DUME_OE = "DUME Opérateur Economique (OE)";
    public static final String DUME_A = "DUME Acheteur";
    public static final String CONTEXTE = "Contexte";
    public static final String APPEL_MPE_AUTRE = "Appels MPE / autres plateformes";
    public static final String APPEL_FRONT = "Appels front (Angular)";
    public static final String AUTHENTIFICATION = "Authentification";
    public static final String DONNEES_ESSENTIELS = "Donnees Essentielles";
    // HEADERS
    public static final String BEARER = "Bearer";

}
