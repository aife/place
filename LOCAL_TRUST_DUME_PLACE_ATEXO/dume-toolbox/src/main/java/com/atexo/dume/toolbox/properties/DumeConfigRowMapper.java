package com.atexo.dume.toolbox.properties;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DumeConfigRowMapper implements RowMapper<DumeProperty> {

    private static final String VALUE = "DUME_PROP_VALUE";
    private static final String DESC = "DUME_PROP_DESC";
    private static final String LABEL = "DUME_PROP_LABEL";

    /**
     * Implementations must implement this method to map each row of data
     * in the ResultSet. This method should not call {@code next()} on
     * the ResultSet; it is only supposed to map values of the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row
     * @throws SQLException if a SQLException is encountered getting
     *                      column values (that is, there's no need to catch SQLException)
     */
    @Nullable
    @Override
    public DumeProperty mapRow(final ResultSet rs, int rowNum) throws SQLException {
        final DumeProperty prop = new DumeProperty();
        prop.setValue(rs.getString(VALUE));
        prop.setDesc(rs.getString(DESC));
        prop.setLabel(rs.getString(LABEL));
        return prop;
    }
}
