package com.atexo.dume.toolbox.properties;

public class DumeProperty {

    private String label;
    private String value;
    private String desc;

    public DumeProperty() {
    }

    public DumeProperty(String value, String desc, final String label) {
        this.value = value;
        this.desc = desc;
        this.label = label;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DumeProperty)) {
            return false;
        }

        DumeProperty that = (DumeProperty) o;

        if (!getValue().equals(that.getValue())) {
            return false;
        }
        return getDesc().equals(that.getDesc());
    }

    @Override
    public int hashCode() {
        int result = getValue().hashCode();
        result = 31 * result + getLabel().hashCode();
        result = 31 * result + getDesc().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "DumeProperty{" +
                ", value='" + value + '\'' +
                ", label=" + label +
                ", desc='" + desc + '\'' +
                '}';
    }
}
