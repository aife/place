package com.atexo.dume.toolbox.consts;

import com.fasterxml.jackson.annotation.JsonValue;

public enum DumePlatformTypeEnum {

    ID_TYPE_1(1),
    ID_TYPE_2(2),
    ID_TYPE_3(3),
    ID_TYPE_4(4),
    ID_TYPE_5(5),
    EMPTY(null);

    private Integer typePlat;

    DumePlatformTypeEnum(Integer typePlat) {
        this.typePlat = typePlat;
    }

    @JsonValue
    public Integer getTypePlat() {
        return typePlat;
    }


}
