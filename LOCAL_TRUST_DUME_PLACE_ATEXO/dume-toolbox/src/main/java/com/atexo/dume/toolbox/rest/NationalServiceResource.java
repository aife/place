package com.atexo.dume.toolbox.rest;

public enum NationalServiceResource {

    DUME_REQUEST("dumeA/"),
    DUME_RESPONSE("dumeOE/"),
    DUME_DATA("donnees/"), METADATA("metadonnees/"), ESSENTIAL_DATA("donneesEssentielles/"),
    ATTEST("attestations/"), REF("referentiels/"), E_CERTIS("ecertis/");
    private String resource;

    NationalServiceResource(String resource) {
        this.resource = resource;
    }

    public String getResource() {
        return resource;
    }
}
