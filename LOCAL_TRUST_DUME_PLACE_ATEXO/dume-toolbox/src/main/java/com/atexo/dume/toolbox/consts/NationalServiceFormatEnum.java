package com.atexo.dume.toolbox.consts;

public class NationalServiceFormatEnum {

    public static final String DUME_PDF = "1";
    public static final String DUME_XML = "2";
    public static final String DUME_HTML = "3";
    public static final String NO_DUME = "0";


    private NationalServiceFormatEnum() {
    }

}
