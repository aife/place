package com.atexo.dume.toolbox.consts;

public enum DomainEnum {
    DOM_ATEXO("ATEXO", 1L);

    private String label;

    private Long id;

    DomainEnum(String label, Long id) {
        this.label = label;
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public Long getId() {
        return id;
    }
}
