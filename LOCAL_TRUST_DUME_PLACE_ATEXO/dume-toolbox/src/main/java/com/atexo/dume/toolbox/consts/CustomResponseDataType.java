package com.atexo.dume.toolbox.consts;

/**
 * Types de requirement spécifiques à LT-DUME. Ces responseDataTypes ne doivent pas être sérialisés dans le xml. Ils
 * facilitent l'affichage des données dans la vue en profitant du caractère dynamique de la construction du formulaire
 * et évitent de créer des conditions spécifiques pour des requirements précis.
 * Les requirements de type INFOS ont pour but d'afficher dans la vue un encart informatif.
 * Les requirements de type INFO_TITLE sont utilisés par les champ "chiffre d'affaire", pour afficher un titre
 */
public enum CustomResponseDataType {
    INFOS("INFOS"),
    INFO_TITLE("INFO_TITLE");

    private String value;

    CustomResponseDataType(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
