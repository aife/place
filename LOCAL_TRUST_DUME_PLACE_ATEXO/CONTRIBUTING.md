# Contribuer au développement

Bienvenue au développement de ce projet et bon courage ! 🐲

## Introduction

Ce document a pour but de décrire les processus de développement, la structure du projet et la façon dont certaines
fonctionnalités sont implémentées pour une meilleure compréhension globale.

Builder LT-DUME génère deux livrables : un zip des pages angular à déployer sur un apache, et un war à déployer sur un
serveur tomcat. Bien que les pages angular doivent être déployées sur chaque MPE s'interfaçant avec LT-DUME, le war
LT-DUME a pour finalité de n'être déployé que sur un seul serveur. Plusieurs plateformes MPE s'interfaceront donc avec
un unique serveur LT-DUME. Chaque plateforme possède son identifiant qui doit être connu de LT-DUME (table `DUME_PLATFORM`)
et un couple identifiant/mot de passe permettant de demander des jetons d'authentification (table `oauth_client_details`).

Pour transmettre les DUMEs, notamment au Service National (SN), le format d'échange est le XML, comme défini par la
réglementation européenne. Dans un souci de maintenabilité, les données stockées en base sont sous forme de tables et
d'objets ; le XML correspondant est généré à la volée. Pour générer le XML, le projet se base sur la librairie externe
[`exchange-model`](https://github.com/ESPD/ESPD-EDM), faite par le groupe de travail européen.

Il existe un utilitaire de génération de DUME en ligne, accessible à cette adresse : https://ec.europa.eu/tools/espd/
Cet outil a servi de référence pour LT-DUME, que ce soit au niveau de la génération du XML ou du comportement des
formulaires de dume acheteur ou de dume opérateur économique. Cependant, l'outil européen étant voué à disparaître, on pourra
se baser sur l'utilitaire français à cette adresse : https://dume.chorus-pro.gouv.fr/

## Processus de développement

Pour lancer les serveurs back et front en local, se référer au [README.md](README.md). Ne pas oublier qu'il existe un
mode dev et prod sur chacun de ces serveurs. La différence entre ces deux modes sur le serveur back ne concerne que
l'accès à la base de données. En revanche, pour le serveur front, le mode prod change radicalement le build. Il est donc
important de tester les deux modes de fonctionnement pour la partie front.

Il est possible de connecter son serveur en local à une plateforme de test du SN. L'url de connexion à cette
plateforme est configurée dans la table `DUME_PROPERTIES`, propriété `SN_BASE_PATH`. Il est possible que cette plateforme
ne soit accessible que via un proxy, auquel cas il faut mettre à `true` la propriété `dume.sn.via.proxy` et utiliser les
deux propriétés `atexo.http.proxy.hostname` et `atexo.http.proxy.port` pour pointer sur le proxy. Enfin, il reste possible
d'utiliser `SN_BASE_PATH` pour pointer sur le serveur dume-mocks si les plateformes SN sont inaccessibles ; voir
[README.md](README.md#lancer-le-mock-du-sn), rubrique "Lancer le mock du SN".

Sur [gitlab](https://gitlab.local-trust.com/transversal/lt_dume), le projet est configuré avec des pipelines. Les merge
requests doivent réussir un build avec jenkins pour pouvoir être acceptés : ils ne doivent donc pas provoquer de 
régression sur les tests unitaires. Ils doivent également passer les quality gates de sonar (TODO : rétablir les quality
gates). Il faut également s'assurer que la couverture de code n'a pas baissé.

Les user stories et bugs sont tracés sur [JIRA](https://jira.local-trust.com/issues/?jql=project%20%3D%20DUME).

## Structure du projet

Le projet LT-DUME se décompose en plusieurs parties :

* Des pages construites en angular et destinées à être intégrées à MPE ;
* Une API REST qui reçoit les appels provenant soit des pages angular, soit du serveur MPE ;
* Une couche de service permettant des opérations de CRUD sur les dumes acheteur (dume A ou dume Request) et
  operateur économique (dume OE ou dume Response) ;
* Des mappers permettant de convertir les objets du modèle de données en objets de la librairie [`exchange-model`](https://github.com/ESPD/ESPD-EDM) de
  l'utilitaire européen, dans le but de les sérialiser en XML ;
* Une couche d'appel au SN ;
* Deux routines qui traitent des demandes d'opération au SN en attente ;
* Un stockage de fichiers PDF provenant du SN et à destination de MPE.

### Architecture des modules

* __dume-config__ : Toutes les classes de configuration de Spring sont ici : connexion à la base de données, sécurité des URLs, etc.
* __dume-dto__ : Tous les DTOs utilisés pour communiquer entre LT-DUME et MPE, entre LT-DUME et les pages angular, et
  entre LT-DUME et le SN.
* __dume-mapper__ : Tous les mappers utilisés pour convertir les objets du modèle LT-DUME en DTOs SN, en DTOs angular ou
  en DTOs MPE (et inversement). 
* __dume-mocks__ : Permet de lancer un serveur de mock du SN avec des ressources prédéfinies.
* __dume-model__ : Toutes les entités hibernate du projet.
* __dume-repository__ : Tous les Repository Spring permettant d'accéder à la base de données.
* __dume-rest__ : Tous les webservices de l'API REST.
* __dume-services__ : La couche de service gérant les différentes modifications faites aux objets métier.
* __dume-services-api__ : Interfaces du module de services.
* __dume-toolbox__ : Toutes les classes utilitaires.
* __dume-war__ : Génère le war de l'application. Inclus le launcher principal, les propriétés spring et les changelogs de liquibase.
* __dume-web__ : Application angular-cli : contient toutes les pages angular. Génère le zip de la partie front.

### Modèle de données

Une vue d'ensemble des tables est disponible sur [Confluence](https://confluence.local-trust.com/pages/viewpage.action?pageId=29688101).

Les formulaires de dume A et dume OE se basent sur un ensemble de questions et de champs de réponse prédéfinis. Cette
structure provient directement de la spécification du DUME établie par la réglementation européenne. Un dume A correspond
à la sélection des questions qui seront répondues dans un dume OE associé à ce dume A.

Le formulaire de dume se décompose en certaines pages statiques et en "critères". Les critères contiennent au moins un "requirement
group", qui peut contenir des "requirements" ou bien d'autres requirement groups. Un requirement est associé à une "réponse"
uniquement dans un dume OE. 

#### Dume A

L'ensemble des critères de réponse se trouve déjà en base de données et est inséré au premier démarrage
du serveur via un script liquibase (table `CRITERIA`, `REQUIREMENT_GROUP` et `REQUIREMENT`). À l'affichage du formulaire de dume A, la totalité des critères sont transmis. Dans
ce formulaire, l'agent sélectionne les critères qu'il souhaite. Lors de la sauvegarde, sont insérés en base uniquement
les liens entre le dume A (table `DUME_DOCUMENT`) et les critères sélectionnés via les tables de jointures `DUME_DOCUMENT_CRITERIA_EXCLUSION`
et `DUME_DOCUMENT_CRITERIA_SELECTION`.

Une consultation MPE peut définir plusieurs dume A dans le cas où la consultation est allotie (un lot doit avoir un dume A ;
une consultation à n lots peut avoir jusqu'à n dume A différents). La communication entre MPE et LT-DUME passe par un id
de contexte associé à une consultation et représenté dans la table `REQUEST_CONTEXT`. Chaque `DUME_DOCUMENT` est donc
associé à un `REQUEST_CONTEXT`.

#### Dume OE

Un dume OE (table `DUME_RESPONSE`) est associé à un dume A. Un dume OE propose le remplissage du formulaire construit à
l'aide du dume A. Les réponses sont stockées dans la table `RESPONSE`. Chaque réponse est lié à un requirement via
la table de relation `REQUIREMENT_RESPONSE`. Chaque dume OE duplique l'arbre `CRITERIA`-`REQUIREMENT_GROUP`-`REQUIREMENT` en
version `_RESPONSE`. Cette structure a été établie pour faciliter le mapping avec les objets de la libraire `exchange-model`,
notamment sur le cas particulier du bouton '+' (exemple : critère "FINANCIAL_RATIO") qui permet d'ajouter plusieurs "requirement group réponse"
pour un seul requirement group.

## Quelques fonctionnalités expliquées

### Gestion de l'authentification

L'authentification est nécessaire pour appeler les WS de l'API. L'authentification se fait de serveur à serveur : seules
les plateformes MPE doivent demander un token d'accès via l'url `/oauth/token`. À cet effet, Spring est configuré pour
utiliser OAuth en mode client_credentials. Les informations des clients sont stockées dans la table `oauth_client_details`.
Chaque plateforme MPE doit avoir un couple identifiant/mot de passe dans cette table.

Les jetons d'accès retournés par LT-DUME sont au format [JWT](https://jwt.io/) : ils sont "self-contained". Cela signifie qu'il n'y a pas
de token store : le jeton est signé par une clé (propriété `JWT_SIGNING_KEY` de la table `DUME_PROPERTIES`) que seul le 
serveur connaît. Vérifier la validité du jeton revient à vérifier la validité de la signature.

La partie angular nécessite un jeton d'accès pour pouvoir appeler l'API de LT-DUME directement, sans passer par MPE. Ce
jeton est injecté dans la page via du javascript (voir [section suivante](#intégration-des-pages-angular), "Intégration des pages angular"). Pour éviter
que le client récupère le jeton afin d'appeler des WS de l'API de manière mal intentionnée, le jeton d'accès transmis à la
partie front est "scopé". Cela signifie que plusieurs "scopes" oauth spécifiques lui sont associés (et contenus dans le jeton lui-même).
Tous les WS de l'API nécessitent un certain scope pour être habilité à effectuer leur opération : "read", "write", etc.
Les scopes demandés à chaque WS sont définis dans leur annotation `@PreAuthorize`. MPE fournit à la partie angular un
jeton suffisamment scopé pour lui permettre d'afficher et modifier uniquement le dume en cours, ainsi que d'appeler des
WS nécessaires à l'affichage du formulaire (référentiels...).

### Intégration des pages angular

Pour intégrer les pages angular, MPE lie les scripts du livrable de la partie front (le zip) dans les pages voulues.
Ces scripts bootstrappent l'application angular au niveau du tag également inséré dans la page MPE. En mode prod, le
module racine [`app.module.ts`](dume-web/src/app/app.module.ts) détermine le tag utilisé puis boostrappe le module adapté :
[`DumeComponent`](dume-web/src/app/dume/dume.component.ts) pour afficher le formulaire de dume A ou
[`DumeResponseComponent`](dume-web/src/app/dume/dume-response/dume-response.component.ts) pour afficher le formulaire de dume OE.

Deux informations sont injectées sous forme de variables globales javascript et récupérées au démarrage de l'application
angular :
* `TOKEN_ACCESS` contient le jeton d'accès scopé permettant d'appeler l'API de LT-DUME. Cette variable est récupérée à
  l'initialisation de [`AuthenticationService`](dume-web/src/app/service/authentication.service.ts)
* `MPE_ID_CONTEXTE` contient l'id de la ressource (dume A ou OE) qui doit être affichée. Cette variable est récupérée à
  l'initialisation de [`DumeComponent`](dume-web/src/app/dume/dume.component.ts) ou [`DumeResponseComponent`](dume-web/src/app/dume/dume-response/dume-response.component.ts).

Lorsque les pages sont intégrées, les styles sont entièrement hérités de la page hôte, donc de MPE.

### Dume modèles

Pour créer un dume A, LT-DUME se base sur une hiérarchie de dumes "modèles". Au moins un dume modèle doit être défini
sans quoi l'application ne peut créer de dume A (voir [README.md](README.md#lancer-le-backend), rubrique "Lancer le backend"). Un dume
modèle peut être défini par plateforme ou par éditeur ("domain"). Un dume modèle est un dume A dont le `TYPE_DOCUMENT` est `MODEL`.
Les dumes modèles à utiliser sont identifiés par un système de propriétés générique. Les propriétés sont définies dans la
table `DUME_PROFILE_PROP`. Les valeurs des propriétés sont stockées par plateforme dans la table `DUME_PLAT_PROP_VALUE`
et par "domain" dans `DUME_DOM_PROP_VALUE`. Il n'y a qu'un seul domain "ATEXO". Lorsque la valeur de la propriété est récupérée,
la valeur par plateforme est d'abord cherchée, puis la valeur par domain, d'où la hiérarchie. Pour la propriété "dume
modèle" (`ID_DUME_REQUEST_MODEL`), la valeur correspond à l'id d'un dume A `MODEL`.

### Gestion des logs

Les logs utilisent [MDC](https://logback.qos.ch/manual/mdc.html) dans le but de créer un fichier de log par consultation
afin de faciliter la maintenance. Le contexte de log est setté au début des appels WS et est nettoyé à la fin de ceux-ci,
ou dans [l'exception handler](dume-rest/src/main/java/com/atexo/dume/rest/controller/ControllerExceptionHandler.java). Ce contexte contient le nom de la consultation ; il est utilisé dans [`logback.xml`](dume-config/src/main/resources/logback.xml)
au niveau du nom du fichier de log.

### Analyse du build du front

On peut analyser le résultat du build de la partie front en exécutant `npm run-script bundle-report`. Cette
commande permet d'afficher la taille finale de chaque librairie dans le livrable et d'explorer des pistes d'amélioration du
processus de build.

### Fonctionnement des données essentielles

Une routine (déclenchée via l'expression CRON `cron.expression.donneesEssentielles` par défaut, s'exécute toutes les 10 minutes) de publication des données essentielles boucle sur toutes les plateformes ayant activé la fonctionnalité en ayant la colonne `ENABLE_ESSENTIAL_DATA` à "1" dans la table `DUME_PLATFORM`.

Pour chaque plateforme, on appelle le WS `/donnees-essentielles/contrat/format-etendu` à l'aide de l'URL de la plateforme, du login et mot de passe d'authentification (colonnes `URL`, `LOGIN`, `PASSWORD` de `DUME_PLATFORM`). Pour chaque résultat retourné par ce WS :

- Si le "contrat" n'existe pas en base de lt-dume (table `CONTRACT`), on appelle le SN en transmettant les données du contrat et on insère en base l'identification du nouveau contrat ;
- Si le contrat existe, on récupère le numéro SN, on appelle le SN en transmettant les données du contrat ainsi que le numéro SN et on met à jour le contrat en base avec le nouveau numéro SN.
Dans le cas d'une mise à jour du contrat, un lien est mis en place à travers la colonne `ID_CONTRAT_ORIGIN`.

## Administration et maintenance

Les logs sont distribués dans des fichiers par consultation, de format `dume_[Nom consultation]_yyyy-MM-dd.log`.
Un fichier de log est également dédié aux appels au SN : `sn_communication_yyyy-MM-dd.log`. La totalité des logs se
retrouve dans le fichier de sortie par défaut, `catalina.out` sur tomcat.

Un swagger existe à l'adresse `/swagger-ui.html`. Les WS sont triés par catégorie.

Java Melody est accessible à l'adresse `/monitoring`. L'utilisateur et le mot de passe se trouvent dans la table
`DUME_PROPERTIES`, propriété `javamelody.init-parameters.authorized-users`.

Si on connaît un client/mot de passe de la table `oauth_client_details`, on peut appeler manuellement les WS de l'API,
en commençant par demander un jeton d'accès, afin de debugguer les échanges entre MPE et LT-DUME. Des scénarios d'appels
manuels aux WS sont disponibles sur [Confluence](https://confluence.local-trust.com/pages/viewpage.action?pageId=29688148).

Des pages d'administration sont en construction et permettront d'accéder à diverses informations de debug.

Quelques requêtes utiles :

* Afficher les dumes A en attente de publication :
  
    ```sql
    SELECT RC.*,DD.ID,DD.ETAT,DD.NUMERO_SN FROM REQUEST_CONTEXT RC INNER JOIN DUME_DOCUMENT DD ON DD.CONTEXT_ID = RC.ID WHERE DD.ETAT = 'A_PUBLIER';
    ```

* Afficher les dumes OE en attente de publication :

    ```sql
    SELECT * FROM DUME_RESPONSE WHERE ETAT = 'A_PUBLIER';
    ```