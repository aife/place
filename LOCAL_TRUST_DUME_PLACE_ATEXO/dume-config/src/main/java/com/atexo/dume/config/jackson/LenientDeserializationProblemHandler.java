package com.atexo.dume.config.jackson;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.DeserializationProblemHandler;
import com.fasterxml.jackson.databind.util.ClassUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Handler des exceptions de désérialisation. Permet de ne pas bloquer la désérialisation si un problème de formattage
 * est détecté. Le problème est loggé et la valeur affectée à l'attribut en cause sera null.
 */
public class LenientDeserializationProblemHandler extends DeserializationProblemHandler {

    private final Logger logger = LoggerFactory.getLogger(LenientDeserializationProblemHandler.class);

    @Override
    public Object handleWeirdStringValue(DeserializationContext ctxt, Class<?> targetType, String valueToConvert, String failureMsg) throws IOException {
        String clazz = ClassUtil.nameOf(targetType);
        logger.warn("Problème de déserialisation : impossible de convertir la chaine de caractéres \"{}\" en {}. La valeur affectée à l'attribut sera null. Json path : {}",
                valueToConvert, clazz, ctxt.getParser().getParsingContext().pathAsPointer());
        return null;
    }

    @Override
    public Object handleWeirdNumberValue(DeserializationContext ctxt, Class<?> targetType, Number valueToConvert, String failureMsg) throws IOException {
        String clazz = ClassUtil.nameOf(targetType);
        logger.warn("Problème de déserialisation : impossible de convertir la valeur \" {} \" en {} . La valeur affectée à l'attribut sera null. Json path : {}"
                , valueToConvert, clazz, ctxt.getParser().getParsingContext().pathAsPointer());
        return null;
    }

}
