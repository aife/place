package com.atexo.dume.config.jndi;

import java.util.HashMap;
import java.util.Map;

public class MultiDataSourcePlatformConfiguratorImpl implements MultiDataSourcePlatformConfigurator {

    private Map<String, DataSourceDetails> dataSourceDetailsMap = new HashMap<>();

    @Override
    public void addDetails(String key, DataSourceDetails dataSourceDetails) {
        dataSourceDetailsMap.put(key, dataSourceDetails);
    }

    @Override
    public DataSourceDetails getDetails(String key) {
        return dataSourceDetailsMap.get(key);
    }
}
