package com.atexo.dume.config.utils;

public enum HibernateProperties {

    HBM2DDL("hibernate.hbm2ddl.auto"),
    DIALECT("hibernate.dialect"),
    DIALECT_STORAGE_ENGINE("hibernate.dialect.storage_engine"),
    CACHE_USE_SECOND_LEVEL("hibernate.cache.use_second_level_cache"),
    CACHE_USES_QUERY_CACHE("hibernate.cache.use_query_cache"),
    GLOBALLY_QUOTED_IDENTIFIERS("hibernate.globally_quoted_identifiers"),
    HIBERNATE_SHOW_SQL("hibernate.show_sql"),
    HIBERNATE_AUTO_COMMIT("hibernate.connection.autocommit");


    private String value;

    HibernateProperties(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
