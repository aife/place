package com.atexo.dume.config;

import net.bull.javamelody.JavaMelodyAutoConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@DependsOn("dumeConfigurations")
@Configuration

public class JavaMelodyConfiguration extends JavaMelodyAutoConfiguration {
    private final Logger logger = LoggerFactory.getLogger(JavaMelodyConfiguration.class);

    public JavaMelodyConfiguration() {
        logger.info("Instantiation de la configuration de JavaMelody");
    }

}