package com.atexo.dume.config.datasource;

import com.atexo.dume.config.utils.Profiles;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
@Profile(Profiles.DEV)
@PropertySource("classpath:dume-database.properties")
public class DevDataSourceConfig {

    @Value("${dume.database.driver}")
    private String dataBaseDriver;

    @Value("${dume.database.url}")
    private String dataBaseUrl;

    @Value("${dume.database.username}")
    private String dataBaseUserName;

    @Value("${dume.database.password}")
    private String dataBasePassword;

    /**
     * DataSource definition for database connection. Settings are read from
     * the application.properties file (using the env object).
     */
    @Bean
    @Profile(Profiles.DEV)
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(dataBaseDriver);
        dataSource.setUrl(dataBaseUrl);
        dataSource.setUsername(dataBaseUserName);
        dataSource.setPassword(dataBasePassword);
        return dataSource;
    }
}
