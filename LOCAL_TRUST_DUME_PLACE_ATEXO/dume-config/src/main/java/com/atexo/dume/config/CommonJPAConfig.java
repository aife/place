package com.atexo.dume.config;

import com.atexo.dume.toolbox.properties.DumeConfigRowMapper;
import com.atexo.dume.toolbox.properties.DumePropertiesEnum;
import com.atexo.dume.toolbox.properties.DumeProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.CollectionUtils;

import javax.sql.DataSource;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static com.atexo.dume.config.utils.HibernateProperties.*;

@Configuration
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class})
@EnableTransactionManagement
@EnableJpaAuditing
@PropertySource("classpath:dume-database.properties")
@DependsOn({"dataSource","liquibase"})
public class CommonJPAConfig {

    private final Logger logger = LoggerFactory.getLogger(CommonJPAConfig.class);

    @Value("${hibernate.dialect}")
    private String dialect;

    @Value("${dume.database.password}")
    private String showSql;

    @Value("${hibernate.hbm2ddl.auto}")
    private String hbm2ddl;

    @Value("${hibernate.cache.use_second_level_cache}")
    private String use2LevelCache;

    @Value("${hibernate.cache.use_query_cache}")
    private String useQueryCache;

    @Value("${hibernate.globally_quoted_identifiers}")
    private String useIdentifierQuoted;

    @Value("${hibernate.connection.autocommit}")
    private String hibernateAutoCommit;

    @Value("${hibernate.dialect.storage_engine}")
    private String dialectStorage;


    @Autowired
    private DataSource dataSource;

    private static final String PROP_QUERY = "SELECT DUME_PROP_VALUE,DUME_PROP_DESC,DUME_PROP_LABEL FROM DUME_PROPERTIES";

    @Autowired
    private Environment env;


    /**
     * Declare the transaction manager.
     */
    @Bean
    public JpaTransactionManager transactionManager() {
        JpaTransactionManager transactionManager =
                new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(
                entityManagerFactory().getObject());
        return transactionManager;
    }

    /**
     * PersistenceExceptionTranslationPostProcessor is a bean post processor
     * which adds an advisor to any bean annotated with Repository so that any
     * platform-specific exceptions are caught and then rethrown as one
     * Spring's unchecked data access exceptions (i.e. a subclass of
     * DataAccessException).
     */
    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    /**
     * Declare the JPA entity manager factory.
     */
    @Bean
    @DependsOn("dumeConfigurations")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        final LocalContainerEntityManagerFactoryBean entityManagerFactory =
                new LocalContainerEntityManagerFactoryBean();
        entityManagerFactory.setDataSource(dataSource);
        // Classpath scanning of @Component, @Service, etc annotated class
        entityManagerFactory.setPackagesToScan("com.atexo.dume.model");
        // Vendor adapter
        final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        entityManagerFactory.setJpaVendorAdapter(vendorAdapter);
        // Hibernate properties
        final Properties additionalProperties = new Properties();
        additionalProperties.put(DIALECT.value(), dialect);
        additionalProperties.put(HIBERNATE_SHOW_SQL.value(), showSql);
        additionalProperties.put(HBM2DDL.value(), hbm2ddl);
        additionalProperties.put(GLOBALLY_QUOTED_IDENTIFIERS.value(), useIdentifierQuoted);
        additionalProperties.put(CACHE_USE_SECOND_LEVEL.value(), use2LevelCache);
        additionalProperties.put(CACHE_USES_QUERY_CACHE.value(), useQueryCache);
        additionalProperties.put(HIBERNATE_AUTO_COMMIT.value(), hibernateAutoCommit);
        additionalProperties.put(DIALECT_STORAGE_ENGINE.value(), dialectStorage);
        entityManagerFactory.setJpaProperties(additionalProperties);
        return entityManagerFactory;
    }


    @Bean("dumeConfigurations")
    @Scope("singleton")
    @Autowired//forcing singleton scope !! 🐲
    public Map<DumePropertiesEnum, DumeProperty> dumeConfigurations(final JdbcTemplate jdbcTemplate) {
        if (logger.isDebugEnabled()) {
            logger.debug("Populating Dume System DB properties...");
        }
        MutablePropertySources propertySources = ((ConfigurableEnvironment) env).getPropertySources();
        Properties properties = new Properties();
        org.springframework.core.env.PropertySource propertySource = new PropertiesPropertySource("dumeDataBasePropertySource", properties);
        propertySources.addLast(propertySource);
        final Map<DumePropertiesEnum, DumeProperty> retValue = new EnumMap<>(DumePropertiesEnum.class);
        final List<DumeProperty> propertiesList = jdbcTemplate.query(PROP_QUERY, new DumeConfigRowMapper());

        if (!CollectionUtils.isEmpty(propertiesList)) {
            for (final DumeProperty property : propertiesList) {
                properties.setProperty(property.getLabel(), property.getValue());//Add property to context
                try {
                    retValue.put(DumePropertiesEnum.getByValue(property.getLabel()), property);
                } catch (IllegalArgumentException e) {
                    logger.warn("Ce enum {} ne sera pas défini dans la map", property.getLabel());
                }

            }
        }
        //Il est préférable d'utiliser les @value au lieu de la map
        return retValue;
    }

    @Bean
    @Scope("singleton")
    @Autowired//forcing singleton scope !! 🐲
    public JdbcTemplate jdbcTemplate(final DataSource dataSource) {
        if (logger.isDebugEnabled()) {
            logger.debug("Creating jdbcTemplate bean...");
        }
        return new JdbcTemplate(dataSource);
    }
}
