package com.atexo.dume.config.jndi;


/**
 * Proxy for database and transaction manager for different profiles.
 * Is going to injected where {@link javax.sql.DataSource}
 * or {@link org.springframework.transaction.PlatformTransactionManager}
 * is needed.
 *
 */
public interface MultiDataSourcePlatformConfigurator {

    /**
     * Add {@link DataSourceDetails}
     *
     * @param key
     * @param dataSourceDetails
     */
    void addDetails(String key, DataSourceDetails dataSourceDetails);

    /**
     * Retrieve {@link DataSourceDetails}
     *
     * @param key
     * @return
     */
    DataSourceDetails getDetails(String key);
}
