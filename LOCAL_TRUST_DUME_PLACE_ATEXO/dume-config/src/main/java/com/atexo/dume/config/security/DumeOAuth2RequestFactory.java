package com.atexo.dume.config.security;

import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Permet la création de token avec des scopes dynamiques, c'est-à-dire qui ne sont pas enregistrés dans la définition
 * des clients et qui dépendent d'autres paramètres lors de la demande de token. Doit être couplé avec un
 * RequestValidator custom pour pouvoir passer la validation des scopes.
 *
 * @see DumeOAuth2RequestValidator
 */
@Component
public class DumeOAuth2RequestFactory extends DefaultOAuth2RequestFactory {

    private static final String DUME_CONTEXT_ACHETEUR_SCOPE_REQUEST_PARAM = "idContextDumeAcheteur";
    private static final String DUME_CONTEXT_OE_SCOPE_REQUEST_PARAM = "idContextDumeOE";
    private static final String CONTEXT_DUME_A_SCOPE_READ = "contextDumeA:read:";
    private static final String CONTEXT_DUME_A_SCOPE_WRITE = "contextDumeA:write:";
    private static final String CONTEXT_DUME_OE_SCOPE_READ = "contextDumeOE:read:";
    private static final String CONTEXT_DUME_OE_SCOPE_WRITE = "contextDumeOE:write:";
    private static final String CRITERIA_SCOPE_READ = "criteria:read";
    private static final String REFERENTIEL_SCOPE_READ = "referentiel:read";

    public DumeOAuth2RequestFactory(ClientDetailsService clientDetailsService) {
        super(clientDetailsService);
    }

    @Override
    public AuthorizationRequest createAuthorizationRequest(Map<String, String> authorizationParameters) {
        AuthorizationRequest authorizationRequest = super.createAuthorizationRequest(authorizationParameters);
        Set<String> replacementScopes = extractDumeContextScope(authorizationParameters);
        if (!replacementScopes.isEmpty()) {
            authorizationRequest.setScope(replacementScopes);
        }
        return authorizationRequest;
    }

    @Override
    public TokenRequest createTokenRequest(Map<String, String> requestParameters, ClientDetails authenticatedClient) {
        TokenRequest tokenRequest = super.createTokenRequest(requestParameters, authenticatedClient);
        Set<String> replacementScopes = extractDumeContextScope(requestParameters);
        if (!replacementScopes.isEmpty()) {
            // Si les scopes liés à context dume existent, ne garder que ceux-là
            tokenRequest.setScope(replacementScopes);
        }
        return tokenRequest;
    }

    /**
     * Si la demande de token contenait un paramètre liant cette demande à un contextDume spécifique, ajouter
     * des scopes fixes sur ce contextDume. Ces scopes serviront à restreindre l'accès au WS pour cloisonner les
     * accès aux dumes.
     *
     * @param parameters Paramètres http de la requête
     * @return Un Set contenant les scopes spécifiques dume, ou vide si la demande de token n'était pas spécifique à un
     * dume
     */
    private Set<String> extractDumeContextScope(Map<String, String> parameters) {
        Set<String> extractedScopes = new HashSet<>();
        if (parameters.containsKey(DUME_CONTEXT_ACHETEUR_SCOPE_REQUEST_PARAM)) {
            extractedScopes.add(CONTEXT_DUME_A_SCOPE_READ + parameters.get(DUME_CONTEXT_ACHETEUR_SCOPE_REQUEST_PARAM));
            extractedScopes.add(CONTEXT_DUME_A_SCOPE_WRITE + parameters.get(DUME_CONTEXT_ACHETEUR_SCOPE_REQUEST_PARAM));
            // Ajouter ces deux scopes statiques qui sont nécessaires pour afficher un Dume
            extractedScopes.add(CRITERIA_SCOPE_READ);
            extractedScopes.add(REFERENTIEL_SCOPE_READ);
        } else if (parameters.containsKey(DUME_CONTEXT_OE_SCOPE_REQUEST_PARAM)) {
            extractedScopes.add(CONTEXT_DUME_OE_SCOPE_READ + parameters.get(DUME_CONTEXT_OE_SCOPE_REQUEST_PARAM));
            extractedScopes.add(CONTEXT_DUME_OE_SCOPE_WRITE + parameters.get(DUME_CONTEXT_OE_SCOPE_REQUEST_PARAM));
            // Ajouter ces deux scopes statiques qui sont nécessaires pour afficher un Dume
            extractedScopes.add(CRITERIA_SCOPE_READ);
            extractedScopes.add(REFERENTIEL_SCOPE_READ);
        }
        return extractedScopes;
    }
}
