package com.atexo.dume.config.jndi;

import com.atexo.dume.config.utils.Profiles;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile(Profiles.PROD)
public class JndiProdProperties {

    @Value("${lt_dume.datasource.url}")
    private String jndiString;


    public String getJndiStringOne() {
        return jndiString;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("JndiProdProperties{");
        sb.append("jndiStringOne='").append(jndiString).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
