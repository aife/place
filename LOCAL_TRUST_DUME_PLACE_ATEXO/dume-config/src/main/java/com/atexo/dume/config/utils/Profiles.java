package com.atexo.dume.config.utils;

/**
 * Profile definitions.
 */
public final class Profiles {


    public static final String PROD = "production";
    public static final String DEV = "dev";

    private Profiles() {
    }
}
