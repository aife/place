package com.atexo.dume.config;

import grow.names.specification.ubl.schema.xsd.espdrequest_1.ESPDRequestType;
import grow.names.specification.ubl.schema.xsd.espdresponse_1.ESPDResponseType;
import grow.names.specification.ubl.schema.xsd.espdresponse_1.ObjectFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import javax.xml.bind.Marshaller;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class JaxbConfiguration {

    @Value("classpath:schema/maindoc/ESPDRequest-1.0.xsd")
    private Resource requestSchema;

    @Value("classpath:schema/maindoc/ESPDResponse-1.0.xsd")
    private Resource responseSchema;

    @Bean
    public Jaxb2Marshaller jaxb2Marshaller() {
        Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
        jaxb2Marshaller.setPackagesToScan(ESPDRequestType.class.getPackage().getName(),
                ESPDResponseType.class.getPackage().getName());
        Map<String, Object> map = new HashMap<>(2);
        map.put(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxb2Marshaller.setMarshallerProperties(map);
        jaxb2Marshaller.setSchemas(requestSchema,responseSchema);
        return jaxb2Marshaller;
    }

    @Bean
    public ObjectFactory responseObjectFactory() {
        return new ObjectFactory();
    }

    @Bean
    public grow.names.specification.ubl.schema.xsd.espdrequest_1.ObjectFactory requestObjectFactory() {
        return new grow.names.specification.ubl.schema.xsd.espdrequest_1.ObjectFactory();
    }

}