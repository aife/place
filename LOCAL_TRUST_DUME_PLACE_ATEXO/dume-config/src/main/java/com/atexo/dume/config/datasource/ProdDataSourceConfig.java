package com.atexo.dume.config.datasource;

import com.atexo.dume.config.jndi.JndiProdProperties;
import com.atexo.dume.config.utils.Profiles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jndi.JndiTemplate;

import javax.naming.NamingException;
import javax.sql.DataSource;

@Configuration
@Profile(Profiles.PROD)
public class ProdDataSourceConfig {

    @Autowired
    private JndiProdProperties jndiProdProperties;

    @Bean
    public DataSource dataSource() throws NamingException {
        final JndiTemplate jndiTemplate = new JndiTemplate();
        return (DataSource) jndiTemplate.lookup(jndiProdProperties.getJndiStringOne());
    }
}
