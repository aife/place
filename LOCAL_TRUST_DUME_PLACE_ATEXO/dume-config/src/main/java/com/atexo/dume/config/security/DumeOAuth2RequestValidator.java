package com.atexo.dume.config.security;

import org.springframework.security.oauth2.common.exceptions.InvalidScopeException;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.OAuth2RequestValidator;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.regex.Pattern;

/**
 * Permet de valider les scopes dynamiques provenant de {@link DumeOAuth2RequestFactory} tout en supportant la
 * validation traditionnelle des scopes définis dans les informations du client.
 */
@Component
public class DumeOAuth2RequestValidator implements OAuth2RequestValidator {

    private static final Pattern DUME_CONTEXT_SCOPE_REGEX = Pattern.compile("contextDume(A|OE):(read|write):\\d+");

    @Override
    public void validateScope(AuthorizationRequest authorizationRequest, ClientDetails client) {
        validateScope(authorizationRequest.getScope(), client.getScope());
    }

    @Override
    public void validateScope(TokenRequest tokenRequest, ClientDetails client) {
        validateScope(tokenRequest.getScope(), client.getScope());
    }

    private void validateScope(Set<String> requestScopes, Set<String> clientScopes) {

        if (clientScopes != null && !clientScopes.isEmpty()) {
            for (String scope : requestScopes) {
                if (!clientScopes.contains(scope) && !isContextDumeScope(scope)) {
                    throw new InvalidScopeException("Invalid scope: " + scope, clientScopes);
                }
            }
        }

        if (requestScopes.isEmpty()) {
            throw new InvalidScopeException("Empty scope (either the client or the user is not allowed the requested scopes)");
        }
    }

    private boolean isContextDumeScope(String scope) {
        return DUME_CONTEXT_SCOPE_REGEX.matcher(scope).matches();
    }
}
