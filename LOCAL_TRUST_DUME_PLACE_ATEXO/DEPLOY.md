# Déploiement

## Récupérer les livrables

https://archiva-ovh.local-trust.com/repository/atexo/com/atexo/dume/dume-war/\[version\]/dume-war-\[version\].war

https://archiva-ovh.local-trust.com/repository/atexo/com/atexo/dume/dume-web/\[version\]/dume-web-\[version\]-dist.zip

## Déployer

Extraire le zip et copier le contenu sur un apache. Copier le war dans le webapps d'un tomcat et le démarrer.
Ajouter une datasource dans `context.xml` de nom `jdbc/dume` pointant vers une base de données existante mais vierge.

## Configurer

### Propriétés en base

La table `DUME_PROPERTIES` a besoin d'être configurée :

| DUME_PROP_LABEL                             | Description 
| ------------------------------------------- | -------------------------------------
| SN_BASE_PATH                                | URL d'accès au Service National
| JWT_SIGNING_KEY                             | Clé secrète permettant de générer des jetons d'accès. Cette clé doit être générée de manière aléatoire. Il n'y a pas de taille imposée.
| PDF_STORAGE_FOLDER                          | Répertoire de stockage des fichiers PDFs
| cron.expression.acheteur                    | Fréquence de lancement de la routine de traitement des dumes A. Doit respecter la syntaxe cron.
| cron.expression.oe                          | Fréquence de lancement de la routine de traitement des dumes OE. Doit respecter la syntaxe cron.
| cron.expression.donneesEssentielles         | Fréquence de lancement de la routine de traitement des données essentielles . Doit respecter la syntaxe cron.
| javamelody.init-parameters.authorized-users | Utilisateur/mdp pour accéder à la page de monitoring de java melody
| dume.sn.via.proxy                           | Activer l'utilisation d'un proxy pour appeler le Service National (`true` ou `false`)
| atexo.http.proxy.hostname                   | Hostname du proxy d'appel au SN (uniquement si dume.sn.via.proxy=true)
| atexo.http.proxy.port                       | Port du proxy d'appel au SN (uniquement si dume.sn.via.proxy=true)
| dume.sn.timeout                             | Timeout des requêtes HTTP au SN en millisecondes. Une valeur acceptable est 60000 (60s). 
| dume.httpclient.keepalive                   | Non utilisé
| dume.httpclient.pool.size                   | Non utilisé
| dume.keystore.entry.app-name                | Paramètre de certificat pour se connecter au SN
| dume.keystore.password                      | Paramètre de certificat pour se connecter au SN
| dume.keystore.type                          | Paramètre de certificat pour se connecter au SN
| dume.keystore.path                          | Paramètre de certificat pour se connecter au SN
| dume.truststore.path                        | Paramètre de certificat pour se connecter au SN
| dume.truststore.password                    | Paramètre de certificat pour se connecter au SN
| dume.trustStore.type                        | Paramètre de certificat pour se connecter au SN

### Liste des plateformes

Chaque plateforme est identifiée dans LT-DUME dans la table `DUME_PLATFORM`.

| Colonne                           | Description
| --------------------------------- | ---------------------------------------------
| PLAT_DESC                         | Description de la plateforme (champ texte sans effet)
| PLAT_LABEL                        | Raison sociale de la plateforme transmis au SN
| PLATFORM_IDENTIFIER               | Identifiant de la plateforme transmis par MPE
| SN_PLATFORM_IDENTIFIER            | SIRET de la plateforme transmis au SN
| SN_PLATFORM_TECHNICAL_IDENTIFIER	| Identifiant technique de la plateforme transmis au SN
| URL	                            | URL publique permettant la connexion aux api de la plateforme (utilisée dans le cadre des données essentielles)
| LOGIN	                            | Login technique pour les API
| PASSWORD	                        | Mot de passe technique pour les API
| ENABLE_ESSENTIAL_DATE	            | Booléen permettant de définir si une plateforme est éligible à publier les données essentielles

### Authentification

Chaque plateforme a besoin d'un couple identifiant/mot de passe unique renseigné dans MPE et dans la table `oauth_client_details` :

| Colonne                 | Description
| ----------------------- | ---------------------------------------------
| client_id               | Nom d'utilisateur de la plateforme
| resource_ids            | Nom de l'api accédée. Doit être égal à `dume_rest_api`
| client_secret           | Mot de passe de la plateforme. Le mot de passe doit être chiffré avec bcrypt et précédé du texte `{bcrypt}`.
| scope                   | Doit être égal à `read,write,criteria:read,referentiel:read`
| authorized_grant_types  | Doit être égal à `client_credentials`
| web_server_redirect_uri | Non utilisé
| authorities             | Non utilisé
| access_token_validity   | Durée de validité du jeton d'accès, en secondes. Une valeur acceptable est 86400 soit 1j.
| refresh_token_validity  | Non utilisé
| additional_information  | Non utilisé
| autoapprove             | Non utilisé

### Dume modèles

Voir la partie d'administration (en construction)

### Libellés

Voir la partie d'administration (en construction)