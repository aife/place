package com.atexo.dume.rest.controller;

import com.atexo.dume.dto.CriteriaCategorieDTO;
import com.atexo.dume.dto.ExclusionCriteriaDTO;
import com.atexo.dume.dto.OtherCriteriaDTO;
import com.atexo.dume.dto.SelectionCriteriaDTO;
import com.atexo.dume.services.api.CriteriaCategorieService;
import com.atexo.dume.services.api.CriteriaService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.atexo.dume.toolbox.consts.SwaggerConstants.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * This is class will act like a RestController for all criteria operations.
 *
 * @author ash
 */
@RestController
@RequestMapping("/dume-api/criteria")
public class CriteriaController {

    @Autowired
    private CriteriaService criteriaService;

    @Autowired
    private CriteriaCategorieService criteriaCategorieService;

    /**
     * Ws qui récuperer tout les critéres de Selection
     *
     * @return
     */
    @PreAuthorize("#oauth2.hasScope('read') or #oauth2.hasScope('criteria:read')")
    @RequestMapping(value = "/selection", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Renvoie tous les critères de sélection", authorizations = {@Authorization(value = BEARER)}, tags = {CRITERES, APPEL_FRONT})
    public ResponseEntity<List<SelectionCriteriaDTO>> getAllSelectionCriteria() {
        ResponseEntity retValue;
        try {
            retValue = new ResponseEntity(criteriaService.getAllSelectionCriteriaDTO(), HttpStatus.OK);

        } catch (Exception e) {
            retValue = new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return retValue;
    }

    /**
     * Ws qui récuperer tout les critéres de Selection
     *
     * @return
     */
    @PreAuthorize("#oauth2.hasScope('read') or #oauth2.hasScope('criteria:read')")
    @RequestMapping(value = "/selection/{idNatureMarket}", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Renvoie tous les critères de sélection par nature de la consultation", authorizations = {@Authorization(value = BEARER)}, tags = {CRITERES, APPEL_FRONT})
    public ResponseEntity<List<SelectionCriteriaDTO>> getAllSelectionCriteriaByNature(@PathVariable String idNatureMarket) {
        ResponseEntity retValue;
        try {
            retValue = new ResponseEntity(criteriaService.getAllSelectionCriteriaDTOByNature(idNatureMarket), HttpStatus.OK);

        } catch (Exception e) {
            retValue = new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return retValue;
    }

    /**
     * Ws qui récuperer toutes les catégories des critères de Selection
     *
     * @return
     */
    @PreAuthorize("#oauth2.hasScope('read') or #oauth2.hasScope('criteria:read')")
    @RequestMapping(value = "/selectionCategories", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Renvoie toutes les catégories pour les critères de sélection", authorizations = {@Authorization(value = BEARER)}, tags = {CRITERES, APPEL_FRONT})
    public ResponseEntity<List<CriteriaCategorieDTO>> getAllSelectionCriteriaCategories() {
        ResponseEntity retValue;
        try {
            retValue = new ResponseEntity(criteriaCategorieService.getAllSelectionCriteriaCategorieDTO(), HttpStatus.OK);

        } catch (Exception e) {
            retValue = new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return retValue;
    }

    /**
     * Ws qui récuperer tout les critéres d'Exclusion
     *
     * @return
     */
    @PreAuthorize("#oauth2.hasScope('read') or #oauth2.hasScope('criteria:read')")
    @RequestMapping(value = "/exclusion", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Renvoie tous les critères d'exclusion", authorizations = {@Authorization(value = BEARER)}, tags = {CRITERES, APPEL_FRONT})
    public ResponseEntity<List<ExclusionCriteriaDTO>> getAllExclusionCriteria() {
        ResponseEntity retValue;
        try {
            retValue = new ResponseEntity(criteriaService.getAllExclusionCriteriaDTO(), HttpStatus.OK);

        } catch (Exception e) {
            retValue = new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return retValue;
    }

    /**
     * Ws qui récuperer tout les autres critéres
     *
     * @return
     */
    @PreAuthorize("#oauth2.hasScope('read') or #oauth2.hasScope('criteria:read')")
    @RequestMapping(value = "/other", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Renvoie les critères 'Other'", authorizations = {@Authorization(value = BEARER)}, tags = {CRITERES, APPEL_FRONT})
    public ResponseEntity<List<OtherCriteriaDTO>> getAllOtherCriteria() {
        ResponseEntity retValue;
        try {
            retValue = new ResponseEntity(criteriaService.getAllOtherCriteriaDTO(), HttpStatus.OK);

        } catch (Exception e) {
            retValue = new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return retValue;
    }
}
