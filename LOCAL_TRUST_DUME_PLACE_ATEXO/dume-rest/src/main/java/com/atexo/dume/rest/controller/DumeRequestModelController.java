package com.atexo.dume.rest.controller;

import com.atexo.dume.services.api.ImportXMLService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

import static com.atexo.dume.toolbox.consts.SwaggerConstants.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/dume-api/model")
public class DumeRequestModelController {

    private final Logger logger = LoggerFactory.getLogger(DumeRequestModelController.class);

    @Autowired
    private ImportXMLService importXMLService;


    @PreAuthorize("#oauth2.hasScope('write')")
    @RequestMapping(value = "/importerXML/editeur/{editeurLabel}", method = POST, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Importe un modèle DUME acheteur EDITEUR", authorizations = {@Authorization(value = BEARER)}, tags = {APPEL_FRONT, ADMINISTRATION})
    public ResponseEntity<Boolean> importerXMLEdition(@RequestPart @NotNull MultipartFile model, @NotNull @PathVariable String editeurLabel) {
        ResponseEntity retValue;
        try {
            retValue = new ResponseEntity(importXMLService.importDumeRequestModelDomaine(model.getInputStream(), editeurLabel), HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            retValue = new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return retValue;
    }

    @PreAuthorize("#oauth2.hasScope('write')")
    @RequestMapping(value = "/importerXML/platform/{platformlabel}", method = POST, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Importe un modèle DUME acheteur PLATEFORME", authorizations = {@Authorization(value = BEARER)}, tags = {APPEL_FRONT, ADMINISTRATION})
    public ResponseEntity<Boolean> importerXMLPlatform(@RequestPart @NotNull MultipartFile model, @NotNull @PathVariable String platformlabel) {
        ResponseEntity retValue;
        try {
            retValue = new ResponseEntity(importXMLService.importDumeRequestModelPlatform(model.getInputStream(), platformlabel), HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            retValue = new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return retValue;
    }

}
