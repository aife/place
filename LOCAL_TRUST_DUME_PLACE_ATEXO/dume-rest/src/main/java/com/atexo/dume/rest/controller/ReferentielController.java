package com.atexo.dume.rest.controller;

import com.atexo.dume.dto.EnumDTO;
import com.atexo.dume.services.api.ReferentielService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

import static com.atexo.dume.toolbox.consts.SwaggerConstants.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/dume-api/referentiel")
public class ReferentielController {

    @Autowired
    private ReferentielService referentielService;

    @PreAuthorize("#oauth2.hasScope('read') or #oauth2.hasScope('referentiel:read')")
    @RequestMapping(value = "/pays", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Renvoie le référentiel pays", authorizations = {@Authorization(value = "read"), @Authorization(value = BEARER)}, tags = {REFERENTIELS, APPEL_FRONT})
    ResponseEntity<List<EnumDTO>> getPays() {
        return new ResponseEntity(referentielService.getCountryEnum(), HttpStatus.OK);
    }

    @PreAuthorize("#oauth2.hasScope('read') or #oauth2.hasScope('referentiel:read')")
    @RequestMapping(value = "/devises", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Renvoie le référentiel devises", authorizations = {@Authorization(value = BEARER)}, tags = {REFERENTIELS, APPEL_FRONT})
    ResponseEntity<List<EnumDTO>> getDevises() {
        return new ResponseEntity(referentielService.getCurrencyEnum(), HttpStatus.OK);
    }

    @PreAuthorize("#oauth2.hasScope('read') or #oauth2.hasScope('referentiel:read')")
    @RequestMapping(value = "/typesMarche", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Renvoie le référentiel type marché", authorizations = @Authorization(value = BEARER), tags = {REFERENTIELS, APPEL_FRONT})
    ResponseEntity<List<EnumDTO>> getTypesMarche() {
        return new ResponseEntity(referentielService.getMarketEnum(), HttpStatus.OK);
    }

    @PreAuthorize("#oauth2.hasScope('write')")
    @RequestMapping(value = "/updateCriteria", method = POST, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Mise à jour des descriptions des critères à partir d'un template", authorizations = {@Authorization(value = BEARER)}, tags = {ADMINISTRATION, APPEL_FRONT})
    public ResponseEntity<Boolean> updateCriteria(@RequestPart MultipartFile criteria) throws IOException {
        return new ResponseEntity(referentielService.updateDescriptionCriteria(criteria.getInputStream()), HttpStatus.OK);
    }

    @PreAuthorize("#oauth2.hasScope('write')")
    @RequestMapping(value = "/updateRequirement", method = POST, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Mise à jour des descriptions des 'Requirements' à partir d'un template", authorizations = {@Authorization(value = BEARER)}, tags = {ADMINISTRATION, APPEL_FRONT})
    public ResponseEntity<Boolean> updateRequirement(@RequestPart MultipartFile requirement) throws IOException {

        return new ResponseEntity(referentielService.updateDescriptionRequirement(requirement.getInputStream()), HttpStatus.OK);
    }
}
