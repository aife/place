package com.atexo.dume.rest.controller;

import com.atexo.dume.dto.ApiErrorDTO;
import com.atexo.dume.services.exception.DumeResourceNotFoundException;
import com.atexo.dume.services.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static com.atexo.dume.toolbox.logging.LoggerUtils.removeLog;

/**
 * This class handle all execption catched in the Controller
 *
 * This 3 types of Execption :
 * <ul>
 *     <li>ServiceException : This exception is thrown by the service layer</li>
 *     <li>DumeResourceNotFoundException : This exception is thrown if resources is not found</li>
 *     <li>RuntimeException : This is security layer to catch all unhandled execption</li>
 * </ul>
 */
@ControllerAdvice
public class ControllerExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(ControllerExceptionHandler.class);
    public static final String EXCEPTION_HANDLING = "Exception handling: {}";

    @ExceptionHandler
    public ResponseEntity<ApiErrorDTO> handleServiceException(ServiceException e) {

        logger.error(EXCEPTION_HANDLING, e.getMessage());
        removeLog();

        ApiErrorDTO apiErrorDTO = new ApiErrorDTO(HttpStatus.INTERNAL_SERVER_ERROR, "Une erreur est survenue.", e);
        return buildResponseEntity(apiErrorDTO);

    }

    @ExceptionHandler
    public ResponseEntity<ApiErrorDTO> handleDumeResourceNotFoundException(DumeResourceNotFoundException e) {

        logger.error(EXCEPTION_HANDLING, e.getMessage());
        removeLog();

        ApiErrorDTO apiErrorDTO = new ApiErrorDTO(HttpStatus.NOT_FOUND, "La ressource demandée n'a pas été trouvée.", e);
        return buildResponseEntity(apiErrorDTO);

    }

    @ExceptionHandler
    public ResponseEntity<ApiErrorDTO> handleRuntimeException(RuntimeException e) {

        logger.error(EXCEPTION_HANDLING, e);
        removeLog();

        ApiErrorDTO apiErrorDTO = new ApiErrorDTO(HttpStatus.INTERNAL_SERVER_ERROR, "Une erreur non prévue est survenue.", e);
        return buildResponseEntity(apiErrorDTO);

    }

    private ResponseEntity<ApiErrorDTO> buildResponseEntity(ApiErrorDTO apiErrorDTO) {
        return new ResponseEntity<>(apiErrorDTO, apiErrorDTO.getStatus());
    }
}
