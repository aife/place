package com.atexo.dume.rest.controller;

import com.atexo.dume.dto.*;
import com.atexo.dume.services.api.DumeResponseService;
import com.atexo.dume.services.exception.DumeResourceNotFoundException;
import com.atexo.dume.services.exception.ServiceException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import static com.atexo.dume.toolbox.consts.SwaggerConstants.*;
import static org.springframework.http.MediaType.*;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * This is class will act like a RestController for all DumeResponse operations.
 *
 * @author ash
 */
@RestController
@RequestMapping("/dume-api/operateurEconomique")
public class DumeResponseController {


    @Autowired
    private DumeResponseService dumeResponseService;

    /**
     * Un WS qui permet de récuperer un dumeResponseDTO à partir d'un id
     *
     * @param id du DumeResponse
     * @return ResponseEntity
     */
    @PreAuthorize("#oauth2.hasScope('read') or #oauth2.hasScope('contextDumeOE:read:'+#id)")
    @RequestMapping(value = "/get/{id}", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Récupère un DUME OE à partir de son ID", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_OE, APPEL_FRONT})
    public ResponseEntity<DumeResponseDTO> findDumeResponseById(@PathVariable Long id) throws DumeResourceNotFoundException {
        return new ResponseEntity(dumeResponseService.getDumeResponseDTOById(id), HttpStatus.OK);
    }

    /**
     * WS qui permet l'update d'un dumeResponse à partir d'un dto
     *
     * @param dumeResponseDTO le dto a mettre à jour
     * @return Le Dto mis à jour
     */
    @PreAuthorize("#oauth2.hasScope('write') or #oauth2.hasScope('contextDumeOE:write:'+#dumeResponseDTO.id)")
    @RequestMapping(value = "/update", method = POST, produces = APPLICATION_JSON_UTF8_VALUE, consumes = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Met à jour un DUME OE à partir d'un DTO", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_OE, APPEL_FRONT})
    public ResponseEntity<DumeResponseDTO> updateDumeResponseByDTO(@NotNull @RequestBody DumeResponseDTO dumeResponseDTO) throws DumeResourceNotFoundException {
        return new ResponseEntity(dumeResponseService.updateDumeResponseByDTO(dumeResponseDTO), HttpStatus.OK);
    }

    @PreAuthorize("#oauth2.hasScope('write')")
    @RequestMapping(value = "/updateMetadonnee/{idContextDumeOperateurEconomique}", method = POST, produces = APPLICATION_JSON_UTF8_VALUE, consumes = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Met à jour les métadonnées d'un DUME OE à partir de son ID", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_OE, APPEL_MPE_AUTRE})
    public ResponseEntity<Long> updateContextByIdentifier(final @RequestBody DumeRequestContextDTO context, @PathVariable Long idContextDumeOperateurEconomique) throws DumeResourceNotFoundException {
        return new ResponseEntity(dumeResponseService.updateMetadonneesAndLot(context, idContextDumeOperateurEconomique), HttpStatus.OK);
    }

    @PreAuthorize("#oauth2.hasScope('write')")
    @RequestMapping(value = "/create", method = POST, produces = APPLICATION_JSON_UTF8_VALUE, consumes = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Crée un DUME OE à partir d'un contexte", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_OE, APPEL_MPE_AUTRE, APPEL_FRONT})
    public ResponseEntity<DumeResponseRetourCreationDTO> createDumeResponse(final @RequestBody DumeRequestContextDTO context) throws ServiceException, IOException, DumeResourceNotFoundException {
        return new ResponseEntity(dumeResponseService.createDumeResponseBy(context), HttpStatus.OK);
    }

    @PreAuthorize("#oauth2.hasScope('write') or #oauth2.hasScope('contextDumeOE:write:'+#idResponse)")
    @RequestMapping(value = "/create/{idResponse}/{snNumber}", method = POST, produces = APPLICATION_JSON_UTF8_VALUE, consumes = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Crée un DUME OE à partir d'un DUME OE publié dans le service national", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_OE, APPEL_FRONT})
    public ResponseEntity<DumeResponseDTO> createDumeResponseFromExisitingSNNumber(@PathVariable @NotNull Long idResponse, @PathVariable @NotNull String snNumber) throws ServiceException, DumeResourceNotFoundException {
        return new ResponseEntity(dumeResponseService.createDumeResponseFromExisitingSNNumber(idResponse, snNumber), HttpStatus.OK);
    }

    @PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping(value = "/publication/list/{idContexteDumeOperateurEconomique}", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Récuprère le statut de publication d'un DUME OE à partir de son ID", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_OE, APPEL_MPE_AUTRE})
    public ResponseEntity<PublicationStatus> getAllPublishedDumeResponseForContext(final @PathVariable Long idContexteDumeOperateurEconomique) throws DumeResourceNotFoundException {
        return new ResponseEntity(dumeResponseService.recupererStatusPublicationDumeOe(idContexteDumeOperateurEconomique), HttpStatus.OK);
    }

    @PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping(value = "/pdf/{numeroSN}", method = GET, produces = APPLICATION_PDF_VALUE)
    @ApiOperation(value = "Récupère le fichier PDF correspondant à un numero SN DUME OE", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_OE, APPEL_MPE_AUTRE})
    public ResponseEntity<InputStreamResource> getPDF(final @PathVariable String numeroSN) throws FileNotFoundException, DumeResourceNotFoundException {

        File file = dumeResponseService.getPDFFile(numeroSN);
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + file.getName());
        return ResponseEntity.ok().headers(headers).contentLength(file.length()).contentType(parseMediaType("application/octet-stream")).body(resource);

    }

    @PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping(value = "/validation/{identifierDumeOE}", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Valide un DUME OE et renvoie son nouveau statut", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_OE, APPEL_MPE_AUTRE})
    public ResponseEntity<RetourStatus> validateDumeResponse(final @PathVariable Long identifierDumeOE) throws DumeResourceNotFoundException {
        return new ResponseEntity(dumeResponseService.validateDumeOE(identifierDumeOE), HttpStatus.OK);
    }

    @PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping(value = "/publication/{identifierDumeOperateurEconomique}", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Ajoute le DUME OE correspondant à l'ID à la file de publication des DUME OE", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_OE, APPEL_MPE_AUTRE})
    public ResponseEntity<RetourStatus> publish(final @PathVariable Long identifierDumeOperateurEconomique) throws DumeResourceNotFoundException {

        return new ResponseEntity(dumeResponseService.publishDumeOE(identifierDumeOperateurEconomique), HttpStatus.OK);
    }

    @PreAuthorize("#oauth2.hasScope('write')")
    @RequestMapping(value = "/publicationGroupement", method = POST, produces = APPLICATION_JSON_UTF8_VALUE, consumes = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Ajoute les DUME OE passés en paramètre à la file de publication des DUME OE", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_OE, APPEL_MPE_AUTRE})
    public ResponseEntity<RetourStatus> publishGroupement(@RequestBody PublicationGroupementDTO publicationGroupementDTO) throws DumeResourceNotFoundException, ServiceException {

        return new ResponseEntity(dumeResponseService.publishDumeOEGroupement(publicationGroupementDTO), HttpStatus.OK);
    }

    /**
     * @param snNumber identifian du Dume SNResponse
     * @return le contenu XML de la réponse
     */
    @PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping(value = "/xml/{snNumber}", method = GET)
    @ApiOperation(value = "Génère le flux XML correspondant à un DUME OE", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_OE})
    public ResponseEntity generateDumeResponseXML(@NotNull @PathVariable String snNumber) throws ServiceException, DumeResourceNotFoundException {
        byte[] res = dumeResponseService.generateDumeResponseXML(snNumber);
        HttpHeaders header = new HttpHeaders();
        header.setContentType(APPLICATION_XML);
        header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + snNumber + ".xml");
        header.setContentLength(res.length);
        return new ResponseEntity(res, header, HttpStatus.OK);
    }


    /**
     * @param identifierDumeOperateurEconomique identifiant du Dume Opérateur économique
     * @return le contenu XML de la réponse
     */
    @PreAuthorize("#oauth2.hasScope('read') or #oauth2.hasScope('contextDumeOE:read:'+#identifierDumeOperateurEconomique)")
    @RequestMapping(value = "/downloadXml/{identifierDumeOperateurEconomique}", method = GET)
    @ApiOperation(value = "Génère le flux XML correspondant à un DUME OE à partir du FRONT", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_OE, APPEL_FRONT})
    public ResponseEntity generateDumeResponseXMLFromId(@NotNull @PathVariable Long identifierDumeOperateurEconomique) throws ServiceException, DumeResourceNotFoundException {
        byte[] res = dumeResponseService.generateDumeResponseXMLById(identifierDumeOperateurEconomique);
        HttpHeaders header = new HttpHeaders();
        header.setContentType(APPLICATION_XML);
        header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=dumeOE.xml");
        header.setContentLength(res.length);
        return new ResponseEntity(res, header, HttpStatus.OK);
    }

    @PreAuthorize("#oauth2.hasScope('write') or #oauth2.hasScope('contextDumeOE:write:'+#dumeResponseDTO.id)")
    @RequestMapping(value = "/updateAndDownload", method = POST, produces = {APPLICATION_JSON_UTF8_VALUE, APPLICATION_PDF_VALUE}, consumes = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Met à jour un DUME OE et récupère le fichier PDF lui correspondant", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_OE, APPEL_FRONT})
    public ResponseEntity saveAndDownloadPDF(@RequestBody DumeResponseDTO dumeResponseDTO) throws ServiceException, DumeResourceNotFoundException {

        byte[] res = dumeResponseService.downloadPDFAfterAsaveDumeOE(dumeResponseDTO);
        HttpHeaders header = new HttpHeaders();
        header.setContentType(APPLICATION_PDF);
        header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + dumeResponseDTO.getId() + ".pdf");
        header.setContentLength(res.length);

        return new ResponseEntity(res, header, HttpStatus.OK);
    }


    /**
     * This WS will let the user to know the status of SN.
     * If No SN  so error.
     */
    @PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping(value = "/checkAccess/{dumeRequestContextIdentifier}", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Vérifie la disponibilité du service national pour un DUME OE donné", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_OE, APPEL_MPE_AUTRE})
    public ResponseEntity<RetourStatus> checkAccess(@NotNull @PathVariable final Long dumeRequestContextIdentifier) {
        ResponseEntity retValue;
        try {
            retValue = ResponseEntity.ok().body(dumeResponseService.checkAccessForGivenResponse(dumeRequestContextIdentifier));
        } catch (Exception e) {
            retValue = ResponseEntity.badRequest().body(e.getMessage());
        }
        return retValue;
    }

    @PreAuthorize("#oauth2.hasScope('read') or #oauth2.hasScope('contextDumeOE:read:'+#identifierDumeOperateurEconomique)")
    @RequestMapping(value = "/find/{identifierDumeOperateurEconomique}/{snNumber}", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Récupère les DUME OE publiés dans le SN et correspondant à numéro SN donné", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_OE, APPEL_FRONT})
    public ResponseEntity<List<DumeResponseDTO>> findDumeOEFromSN(@NotNull @PathVariable Long identifierDumeOperateurEconomique, @NotNull @PathVariable final String snNumber) {
        ResponseEntity retValue;
        try {
            List<DumeResponseDTO> retour = dumeResponseService.findDumeOEFromSN(identifierDumeOperateurEconomique, snNumber);
            retValue = ResponseEntity.ok().body(retour);
        } catch (Exception e) {
            retValue = new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return retValue;
    }

    /**
     * Ws qui récupere les preuves d'évidences selon l'uuid d'un critère et le code pays
     *
     * @return
     */
    @PreAuthorize("#oauth2.hasScope('read') or #oauth2.hasScope('criteria:read')")
    @RequestMapping(value = "/e-certis-proof/{identifierDumeOperateurEconomique}/{criteriaUUID}/{countryCode}", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Renvoie les preuves e-certis pour un critère donné", authorizations = {@Authorization(value = BEARER)}, tags = {CRITERES, APPEL_FRONT})
    public ResponseEntity<List<ECertisResponseDTO>> getProofsByCriteriaUuid(@PathVariable Long identifierDumeOperateurEconomique, @PathVariable String criteriaUUID, @PathVariable String countryCode) throws ServiceException, DumeResourceNotFoundException {
        return new ResponseEntity(dumeResponseService.getECertisProof(identifierDumeOperateurEconomique, criteriaUUID, countryCode), HttpStatus.OK);
    }

}


