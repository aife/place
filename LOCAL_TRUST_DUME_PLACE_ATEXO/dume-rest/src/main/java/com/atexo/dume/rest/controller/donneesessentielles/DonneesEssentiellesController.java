package com.atexo.dume.rest.controller.donneesessentielles;

import com.atexo.dume.dto.donneesessentielles.RetourStatusContract;
import com.atexo.dume.services.api.DonneesEssentiellesService;
import com.atexo.dume.services.exception.DumeResourceNotFoundException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.atexo.dume.toolbox.consts.SwaggerConstants.BEARER;
import static com.atexo.dume.toolbox.consts.SwaggerConstants.DONNEES_ESSENTIELS;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * This class will act like a RestController for all Donnees Essentielles operations
 */
@RestController
@RequestMapping("/dume-api/donneesEssentielles")
public class DonneesEssentiellesController {

    @Autowired
    private DonneesEssentiellesService donneesEssentiellesService;

    @PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping(value = "/statutContrat/{contratId}/{updateNumber}", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Envoie le statut du contrat", authorizations = {@Authorization(value = BEARER)}, tags = DONNEES_ESSENTIELS)
    public ResponseEntity<RetourStatusContract> checkContract(@PathVariable String contratId, @PathVariable String updateNumber) throws DumeResourceNotFoundException {

        return ResponseEntity.ok(donneesEssentiellesService.checkStatutContrat(contratId + updateNumber));
    }

    @PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping(value = "/statutContrat/{contratId}/{updateNumber}/{initialContractUuid}", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Envoie le statut du contrat", authorizations = {@Authorization(value = BEARER)}, tags = DONNEES_ESSENTIELS)
    public ResponseEntity<RetourStatusContract> checkContractByUuid(@PathVariable String contratId, @PathVariable String updateNumber, @PathVariable String initialContractUuid) throws DumeResourceNotFoundException {

        return ResponseEntity.ok(donneesEssentiellesService.checkStatutContrat(contratId + updateNumber, initialContractUuid));
    }
}
