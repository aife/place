package com.atexo.dume.rest.controller;


import com.atexo.dume.dto.DumeRequestContextDTO;
import com.atexo.dume.dto.PublicationStatus;
import com.atexo.dume.dto.RetourStatus;
import com.atexo.dume.services.api.DumeRequestContextService;
import com.atexo.dume.services.api.DumeRequestService;
import com.atexo.dume.services.exception.DumeResourceNotFoundException;
import com.atexo.dume.services.exception.ServiceException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import static com.atexo.dume.toolbox.consts.SwaggerConstants.*;
import static org.springframework.http.MediaType.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * This is class will act like a RestController for all DumeRequest operations.
 *
 * @author ash
 */
@RestController
@RequestMapping("/dume-api/acheteur")
public class DumeRequestController {

    @Autowired
    private DumeRequestContextService dumeRequestContextService;

    @Autowired
    private DumeRequestService dumeRequestService;


    /**
     * Création d'un DumeRequestContext  à partir du DTO (les données Hérités)
     *
     * @param context Context (les données Hérités)
     * @return le numero Dume
     */
    @PreAuthorize("#oauth2.hasScope('write')")
    @RequestMapping(value = "/create", method = POST, produces = APPLICATION_JSON_UTF8_VALUE, consumes = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Crée un contexte acheteur à partir d'un DTO contexte et renvoie son ID", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_A, APPEL_MPE_AUTRE})
    public ResponseEntity<Long> createDumeRequest(@RequestBody DumeRequestContextDTO context) throws ServiceException, DumeResourceNotFoundException {

        return new ResponseEntity(dumeRequestContextService.saveContext(context).getId(), HttpStatus.OK);
    }

    /**
     * Un WS qui permet de récupérer un ContextdumeRequestDTO à partir d'un id
     *
     * @param id du DumeRequest
     * @return ResponseEntity
     */
    @PreAuthorize("#oauth2.hasScope('read') or #oauth2.hasScope('contextDumeA:read:'+#id)")
    @RequestMapping(value = "/get/{id}", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recherche un contexte à partinr d'un ID", authorizations = {@Authorization(value = "rad"), @Authorization(value = BEARER)}, tags = {DUME_A, APPEL_FRONT})
    public ResponseEntity<DumeRequestContextDTO> findDumeRequestById(@PathVariable Long id) throws DumeResourceNotFoundException {

        DumeRequestContextDTO dumeRequestContextDTO = dumeRequestContextService.getDumeRequestContextById(id);
        return new ResponseEntity(dumeRequestContextDTO, HttpStatus.OK);
    }

    /**
     * WS qui permet de mettre à jour les données héritées avec les metadata
     *
     * @param id      du ContextDumeRequest a mettre à jour
     * @param context les nouvelles données héritées
     * @return l'id du dumeRequest mis à jour
     */
    @PreAuthorize("#oauth2.hasScope('write')")
    @RequestMapping(value = "/updateMetadonnee/{id}", method = POST, produces = APPLICATION_JSON_UTF8_VALUE, consumes = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Met à jour les métadonnées d'un contexte à partir d'un DTO", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_A, APPEL_MPE_AUTRE})
    public ResponseEntity<Long> updateContextById(@PathVariable Long id, @RequestBody DumeRequestContextDTO context) throws DumeResourceNotFoundException {

        Long idUpdated = dumeRequestContextService.updateContextWithoutRequest(context, id);
        return new ResponseEntity(idUpdated, HttpStatus.OK);
    }

    /**
     * WS qui permet de supprimer un DumeRequest à partir de son ID
     *
     * @param id du dumeRequest à supprimer
     * @return True si le dumeRequest est supprimé
     */
    @PreAuthorize("#oauth2.hasScope('write')")
    @RequestMapping(value = "/delete/{id}", method = DELETE, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Supprime un contexte à partir de son ID", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_A})
    public ResponseEntity<Boolean> deleteDumeRequestById(@PathVariable Long id) throws DumeResourceNotFoundException {

        Boolean deleted = dumeRequestContextService.deleteContextDumeRequest(id);
        return new ResponseEntity(deleted, HttpStatus.OK);
    }

    /**
     * WS qui permet l'update d'un dumeRequest à partir d'un dto
     *
     * @param dumeRequestDTO est le dto a mettre à jour
     * @return Le Dto mis à jour
     */
    @PreAuthorize("#oauth2.hasScope('write') or #oauth2.hasScope('contextDumeA:write:'+#dumeRequestDTO.id)")
    @RequestMapping(value = "/update", method = POST, produces = APPLICATION_JSON_UTF8_VALUE, consumes = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Met à jour un contexte à partit d'un DTO", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_A, APPEL_FRONT})
    public ResponseEntity<DumeRequestContextDTO> updateDumeRequestByDTO(@RequestBody DumeRequestContextDTO dumeRequestDTO) throws ServiceException, DumeResourceNotFoundException {

        DumeRequestContextDTO dto = dumeRequestContextService.updateDumeContextFromDTO(dumeRequestDTO);
        return new ResponseEntity(dto, HttpStatus.OK);
    }

    @PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping(value = "/all", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Récupère tous les contextes", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_A, APPEL_FRONT})
    public ResponseEntity<List<DumeRequestContextDTO>> getAllDumeRequest() {

        return new ResponseEntity(dumeRequestContextService.getAllDumeRequestContext(), HttpStatus.OK);
    }


    @PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping(value = "/publication/list/{identifierContextDumeAcheteur}", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Publie tous les DUME acheteurs attachés à un contexte", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_A, APPEL_MPE_AUTRE})
    public ResponseEntity<PublicationStatus> getAllPublishedDumeRequestForContext(final @PathVariable Long identifierContextDumeAcheteur) throws DumeResourceNotFoundException {

        PublicationStatus publicationStatus = dumeRequestContextService.recupererStatusPublicationDumeAcheteur(identifierContextDumeAcheteur);
        return new ResponseEntity(publicationStatus, HttpStatus.OK);
    }

    @PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping(value = "/pdf/{numeroSN}", method = GET, produces = {APPLICATION_PDF_VALUE, APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Récupère le fichier PDF correspondant à un numero SN acheteur", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_A, APPEL_MPE_AUTRE})
    public ResponseEntity<InputStreamResource> getPDF(final @PathVariable String numeroSN) throws DumeResourceNotFoundException {

        File file = dumeRequestService.getPDFFile(numeroSN);
        InputStreamResource resource;
        try {
            resource = new InputStreamResource(new FileInputStream(file));
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + file.getName());
            return ResponseEntity.ok().headers(headers).contentLength(file.length()).contentType(MediaType.parseMediaType("application/octet-stream")).body(resource);
        } catch (FileNotFoundException e) {
            // N'est pas censé arriver ; le service s'assure que le fichier existe.
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping(value = "/xml/{numeroSN}", method = GET)
    @ApiOperation(value = "Récupère le XML correspondant à un numero SN acheteur", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_A, APPEL_MPE_AUTRE})
    public ResponseEntity getXMLDumeRequestByNumeroSN(@NotNull @PathVariable String numeroSN) throws ServiceException, DumeResourceNotFoundException {
        byte[] res = dumeRequestService.generateXMLByNumeroSN(numeroSN);
        HttpHeaders header = new HttpHeaders();
        header.setContentType(APPLICATION_XML);
        header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + numeroSN + ".xml");
        header.setContentLength(res.length);
        return new ResponseEntity(res, header, HttpStatus.OK);
    }


    @PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping(value = "/validation/{identifierContextDumeAcheteur}", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Vérifie la validité des DUME acheteur attachés à un contexte à partir d'un ID contexte  ", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_A, APPEL_MPE_AUTRE})
    public ResponseEntity<RetourStatus> validateDumeRequest(final @PathVariable Long identifierContextDumeAcheteur) throws DumeResourceNotFoundException {

        RetourStatus retourStatus = dumeRequestContextService.checkValidContextDume(identifierContextDumeAcheteur);
        return new ResponseEntity(retourStatus, HttpStatus.OK);
    }

    @PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping(value = "/publication/{identifierContextDumeAcheteur}", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Ajoute tous les DUME acheteur attachés à un contexte à la file de publication des DUME acheteurs", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_A})
    public ResponseEntity<RetourStatus> publish(final @PathVariable Long identifierContextDumeAcheteur) throws DumeResourceNotFoundException, ServiceException {

        RetourStatus retourStatus = dumeRequestContextService.publishAllDumeRequest(identifierContextDumeAcheteur);
        return new ResponseEntity(retourStatus, HttpStatus.OK);
    }

    /**
     * @param identifierDumeRequest identifian du Dume Request
     * @return le contenu XML du Dume Request
     */
    @PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping(value = "/genererXML/{identifierDumeRequest}",method = GET,produces =APPLICATION_XML_VALUE)
    @ApiOperation(value = "Génère le flux XML correspondant à un DUME acheteur", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_A})
    public ResponseEntity<String> generateDumeRequestXML(@NotNull @PathVariable Long identifierDumeRequest) throws ServiceException, DumeResourceNotFoundException {

        String xml = dumeRequestService.generateDumeRequestXML(identifierDumeRequest);
        return new ResponseEntity<>(xml, HttpStatus.OK);
    }

    @PreAuthorize("#oauth2.hasScope('write')")
    @RequestMapping(value = "/updateDLRO/{idDumeRequestContext}", method = POST, produces = APPLICATION_JSON_UTF8_VALUE, consumes = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Met à jour la date limire de remise des offres (DLRO) à partir d'un ID contexte et un DTO contexte", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_A})
    public ResponseEntity<Boolean> updateDLRO(@NotNull @RequestBody DumeRequestContextDTO dto, @PathVariable Long idDumeRequestContext) throws DumeResourceNotFoundException {
        return new ResponseEntity<>(dumeRequestContextService.updateDLRO(idDumeRequestContext, dto.getMetadata().getBidClosingDate()), HttpStatus.OK);
    }

    @PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping(value = "/replacePublished/{idDumeRequestContext}", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Ajoute tous les DUME acheteur attachés à un contexte à la file de publication des DUME acheteurs avec le statut 'A_REMPLACER'", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_A})
    public ResponseEntity<RetourStatus> replacePublishedDumeRequest(@NotNull @PathVariable Long idDumeRequestContext) throws DumeResourceNotFoundException, ServiceException {
        return ResponseEntity.ok(dumeRequestContextService.updateRequestContextAfterPublication(idDumeRequestContext));
    }

    @PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping(value = "/checkPurge/{idDumeRequestContext}", method = POST, produces = APPLICATION_JSON_UTF8_VALUE,consumes = APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Teste si ce changement de lot nécessite une purge de la relation critère lot'", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_A})
    public ResponseEntity<RetourStatus> checkPurge(@PathVariable Long idDumeRequestContext,@RequestBody DumeRequestContextDTO dto) throws DumeResourceNotFoundException {
        return ResponseEntity.ok(dumeRequestContextService.checkPurgeCriteriaLot(idDumeRequestContext,dto.getLotDTOSet()));
    }

    /**
     * @param idDumeRequestContext identifiant du Dume Context
     * @return le PDF correspondant à un Dume Acheteur
     */
    @PreAuthorize("#oauth2.hasScope('read') or #oauth2.hasScope('contextDumeA:read:'+#idDumeRequestContext)")
    @RequestMapping(value = "/downloadPdf/{idDumeRequestContext}", method = GET)
    @ApiOperation(value = "Génère un flux PDF correspondant d'un DUME A à partir du FRONT", authorizations = {@Authorization(value = BEARER)}, tags = {DUME_A, APPEL_FRONT})
    public ResponseEntity generateDumeResponseXMLFromId(@NotNull @PathVariable Long idDumeRequestContext) throws ServiceException, DumeResourceNotFoundException {
        byte[] res = dumeRequestContextService.downloadPdf(idDumeRequestContext);
        HttpHeaders header = new HttpHeaders();
        header.setContentType(APPLICATION_PDF);
        header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=dumeA.pdf");
        header.setContentLength(res.length);
        return new ResponseEntity(res, header, HttpStatus.OK);
    }
}
