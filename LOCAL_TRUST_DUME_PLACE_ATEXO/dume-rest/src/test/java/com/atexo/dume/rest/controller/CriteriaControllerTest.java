package com.atexo.dume.rest.controller;

import com.atexo.dume.dto.ExclusionCriteriaDTO;
import com.atexo.dume.dto.OtherCriteriaDTO;
import com.atexo.dume.dto.SelectionCriteriaDTO;
import com.atexo.dume.dto.dataset.ExclusionCriteriaDTODataSet;
import com.atexo.dume.dto.dataset.OtherCriteriaDTODataSet;
import com.atexo.dume.dto.dataset.SelectionCriteriaDTODataSet;
import com.atexo.dume.services.api.CriteriaService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.atexo.dume.dto.dataset.ExclusionCriteriaDTODataSet.EXCLUSION_CRITERIA_DTO_IDENTIFIER_1;
import static com.atexo.dume.dto.dataset.ExclusionCriteriaDTODataSet.EXCLUSION_CRITERIA_DTO_IDENTIFIER_2;
import static com.atexo.dume.dto.dataset.OtherCriteriaDTODataSet.OTHER_CRITERIA_DTO_IDENTIFIER_1;
import static com.atexo.dume.dto.dataset.OtherCriteriaDTODataSet.OTHER_CRITERIA_DTO_IDENTIFIER_2;
import static com.atexo.dume.dto.dataset.SelectionCriteriaDTODataSet.SELECTION_CRITERIA_DTO_IDENTIFIER_1;
import static com.atexo.dume.dto.dataset.SelectionCriteriaDTODataSet.SELECTION_CRITERIA_DTO_IDENTIFIER_2;
import static com.atexo.dume.rest.controller.Utils.Constants.*;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CriteriaControllerTest {

    @InjectMocks
    private CriteriaController criteriaController;

    @Mock
    private CriteriaService criteriaService;

    private MockMvc mockMvc;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(criteriaController)
                .build();
    }

    @Test
    public void getAllSelectionCriteriaOk() throws Exception {
        final List<SelectionCriteriaDTO> selectionCriteriaDTOS =
                asList(SelectionCriteriaDTODataSet.sample1WithId(),
                        SelectionCriteriaDTODataSet.sample2WithId());

        when(criteriaService.getAllSelectionCriteriaDTO()).thenReturn(selectionCriteriaDTOS);

        mockMvc.perform(get(API_CRITERIA_SELECTION_PATH))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath(JSON_ROOT, hasSize(2)))
                .andExpect(jsonPath(JSON_1_USER_DTO_ID, is(SELECTION_CRITERIA_DTO_IDENTIFIER_1.intValue())))
                .andExpect(jsonPath(JSON_2_USER_DTO_ID, is(SELECTION_CRITERIA_DTO_IDENTIFIER_2.intValue())));

    }

    @Test
    public void getAllSelectionCriteriaByNatureOk() throws Exception {
        final List<SelectionCriteriaDTO> selectionCriteriaDTOS = asList(SelectionCriteriaDTODataSet.sample1WithId(), SelectionCriteriaDTODataSet.sample2WithId());

        when(criteriaService.getAllSelectionCriteriaDTOByNature(anyString())).thenReturn(selectionCriteriaDTOS);

        mockMvc.perform(get(API_CRITERIA_SELECTION_PATH + "/01")).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(jsonPath(JSON_ROOT, hasSize(2))).andExpect(jsonPath(JSON_1_USER_DTO_ID, is(SELECTION_CRITERIA_DTO_IDENTIFIER_1.intValue()))).andExpect(jsonPath(JSON_2_USER_DTO_ID, is(SELECTION_CRITERIA_DTO_IDENTIFIER_2.intValue())));

    }

    @Test
    public void getAllSelectionCriteriaByNatureKO() throws Exception {
        final List<SelectionCriteriaDTO> selectionCriteriaDTOS = asList(SelectionCriteriaDTODataSet.sample1WithId(), SelectionCriteriaDTODataSet.sample2WithId());

        when(criteriaService.getAllSelectionCriteriaDTOByNature(anyString())).thenThrow(new IllegalArgumentException());

        mockMvc.perform(get(API_CRITERIA_SELECTION_PATH + "/01")).andExpect(status().isInternalServerError());

    }

    @Test
    public void getAllExclusionCriteriaOk() throws Exception {
        final List<ExclusionCriteriaDTO> exclusionCriteriaDTOS =
                asList(ExclusionCriteriaDTODataSet.sample1WithId(),
                        ExclusionCriteriaDTODataSet.sample2WithId());

        when(criteriaService.getAllExclusionCriteriaDTO()).thenReturn(exclusionCriteriaDTOS);
        mockMvc.perform(get(API_CRITERIA_EXCLUSION_PATH))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath(JSON_ROOT, hasSize(2)))
                .andExpect(jsonPath(JSON_1_USER_DTO_ID, is(EXCLUSION_CRITERIA_DTO_IDENTIFIER_1.intValue())))
                .andExpect(jsonPath(JSON_2_USER_DTO_ID, is(EXCLUSION_CRITERIA_DTO_IDENTIFIER_2.intValue())));


    }

    @Test
    public void getAllSelectionCriteriaException() throws Exception {
        when(criteriaService.getAllSelectionCriteriaDTO()).thenThrow(new IllegalArgumentException());

        mockMvc.perform(get(API_CRITERIA_SELECTION_PATH))
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void getAllExclusionCriteriaException() throws Exception {
        when(criteriaService.getAllExclusionCriteriaDTO()).thenThrow(new IllegalArgumentException());
        mockMvc.perform(get(API_CRITERIA_EXCLUSION_PATH))
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void getAllOtherCriteriaOk() throws Exception {
        final List<OtherCriteriaDTO> otherCriteriaDTOS =
                asList(OtherCriteriaDTODataSet.sample1WithId(),
                        OtherCriteriaDTODataSet.sample2WithId());

        when(criteriaService.getAllOtherCriteriaDTO()).thenReturn(otherCriteriaDTOS);

        mockMvc.perform(get(API_CRITERIA_OTHER_PATH))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath(JSON_ROOT, hasSize(2)))
                .andExpect(jsonPath(JSON_1_USER_DTO_ID, is(OTHER_CRITERIA_DTO_IDENTIFIER_1.intValue())))
                .andExpect(jsonPath(JSON_2_USER_DTO_ID, is(OTHER_CRITERIA_DTO_IDENTIFIER_2.intValue())));

    }

    @Test
    public void getAllOtherCriteriaException() throws Exception {
        when(criteriaService.getAllOtherCriteriaDTO()).thenThrow(new IllegalArgumentException());
        mockMvc.perform(get(API_CRITERIA_OTHER_PATH))
                .andExpect(status().isInternalServerError());

    }

}
