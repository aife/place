package com.atexo.dume.rest.controller;

import com.atexo.dume.dto.StatusDTO;
import com.atexo.dume.dto.donneesessentielles.RetourStatusContract;
import com.atexo.dume.rest.controller.donneesessentielles.DonneesEssentiellesController;
import com.atexo.dume.services.api.DonneesEssentiellesService;
import com.atexo.dume.services.exception.DumeResourceNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static com.atexo.dume.rest.controller.Utils.Constants.API_DE_CONTRAT_STATUT;
import static com.atexo.dume.rest.controller.Utils.Constants.JSON_STATUS;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class DonneesEssentiellesControllerTest {

    @InjectMocks
    private DonneesEssentiellesController donneesEssentiellesController;

    @Mock
    private DonneesEssentiellesService donneesEssentiellesService;

    private MockMvc mockMvc;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(donneesEssentiellesController)
                .setControllerAdvice(new ControllerExceptionHandler())
                .build();
    }

    @Test
    public void checkContractNotFound() throws Exception {
        when(donneesEssentiellesService.checkStatutContrat(anyString())).thenThrow(new DumeResourceNotFoundException("Yes", "YOUPU"));

        mockMvc.perform(get(API_DE_CONTRAT_STATUT + 12546 + "/01")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(donneesEssentiellesService, times(1)).checkStatutContrat(anyString());
        verifyNoMoreInteractions(donneesEssentiellesService);
    }

    @Test
    public void checkContractOk() throws Exception {
        when(donneesEssentiellesService.checkStatutContrat(anyString())).thenReturn(new RetourStatusContract(StatusDTO.ERREUR_SN, "Problème de connexion SN", null));

        mockMvc.perform(get(API_DE_CONTRAT_STATUT + 12546 + "/01")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_STATUS, is("ERREUR_SN")));

        verify(donneesEssentiellesService, times(1)).checkStatutContrat(anyString());
        verifyNoMoreInteractions(donneesEssentiellesService);
    }

    @Test
    public void checkContractWithUuidNotFound() throws Exception {
        when(donneesEssentiellesService.checkStatutContrat(anyString(), anyString())).thenThrow(new DumeResourceNotFoundException("Yes", "YOUPU"));

        mockMvc.perform(get(API_DE_CONTRAT_STATUT + 12546 + "/01/uuid01").contentType(APPLICATION_JSON)).andExpect(status().isNotFound());

        verify(donneesEssentiellesService, times(1)).checkStatutContrat(anyString(), anyString());
        verifyNoMoreInteractions(donneesEssentiellesService);
    }

    @Test
    public void checkContractWithUuidOk() throws Exception {
        when(donneesEssentiellesService.checkStatutContrat(anyString(), anyString())).thenReturn(new RetourStatusContract(StatusDTO.ERREUR_SN, "Problème de connexion SN", null));

        mockMvc.perform(get(API_DE_CONTRAT_STATUT + 12546 + "/01/uuid01").contentType(APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath(JSON_STATUS, is("ERREUR_SN")));

        verify(donneesEssentiellesService, times(1)).checkStatutContrat(anyString(), anyString());
        verifyNoMoreInteractions(donneesEssentiellesService);
    }
}
