package com.atexo.dume.rest.controller.Utils;

public class Constants {
    public static final String DUME_REST_API_BASE_PATH = "/dume-api/";
    public static final String API_USER_PATH = DUME_REST_API_BASE_PATH + "user/";
    public static final String ADD_USER_OP = "addUser";
    public static final String GET_ALL_USER_OP = "all";

    public static final String API_DUME_REQUEST_PATH = DUME_REST_API_BASE_PATH +"acheteur/";
    public static final String API_DUME_REQUEST_CREATE_FULL_PATH = API_DUME_REQUEST_PATH + "create";
    public static final String API_DUME_REQUEST_GET_FULL_PATH = API_DUME_REQUEST_PATH + "get/";
    public static final String API_DUME_REQUEST_UPDATE_FULL_PATH = API_DUME_REQUEST_PATH + "updateMetadonnee/";
    public static final String API_DUME_REQUEST_UPDATE = API_DUME_REQUEST_PATH + "update/";
    public static final String API_DUME_REQUEST_DELETE_FULL_PATH = API_DUME_REQUEST_PATH + "delete/";
    public static final String API_DUME_REQUEST_ALL_FULL_PATH = API_DUME_REQUEST_PATH + "all";
    public static final String API_DUME_REQUEST_EXPORTER_XML = API_DUME_REQUEST_PATH + "genererXML";
    public static final String API_DUME_REQUEST_GET_XML_FULL_PATH = API_DUME_REQUEST_PATH + "xml/";
    public static final String API_DUME_REQUEST_VALIDATE = API_DUME_REQUEST_PATH + "validation/";
    public static final String API_DUME_REQUEST_PUBLISH = API_DUME_REQUEST_PATH + "publication/";
    public static final String API_DUME_REQUEST_PUBLISH_LIST = API_DUME_REQUEST_PUBLISH + "list/";
    public static final String API_DUME_REQUEST_UPDATE_DLRO = API_DUME_REQUEST_PATH + "updateDLRO/";
    public static final String API_DUME_REQUEST_UPDATE_PUBLISHED = API_DUME_REQUEST_PATH + "replacePublished/";
    public static final String API_DUME_REQUEST_CHECK_PURGE = API_DUME_REQUEST_PATH + "checkPurge/";
    public static final String API_DUME_REQUEST_DOWNLOAD_PDF_FULL_PATH = API_DUME_REQUEST_PATH + "downloadPdf/";


    public static final String API_DUME_REFERENTIEL_PATH = DUME_REST_API_BASE_PATH + "referentiel/";
    public static final String API_DUME_REFERENTIEL_PAYS_PATH = API_DUME_REFERENTIEL_PATH + "pays";
    public static final String API_DUME_REFERENTIEL_DEVISES_PATH = API_DUME_REFERENTIEL_PATH + "devises";
    public static final String API_DUME_REFERENTIEL_TYPES_MARCHE_PATH = API_DUME_REFERENTIEL_PATH + "typesMarche";
    public static final String API_DUME_REFERENTIEL_REQUIREMNT_PATH = API_DUME_REFERENTIEL_PATH + "updateRequirement";
    public static final String API_DUME_REFERENTIEL_CRITERIA_PATH = API_DUME_REFERENTIEL_PATH + "updateCriteria";

    public static final String API_DUME_RESPONSE_PATH = DUME_REST_API_BASE_PATH + "operateurEconomique/";
    public static final String API_DUME_RESPONSE_GET_FULL_PATH = API_DUME_RESPONSE_PATH + "get/";
    public static final String API_DUME_RESPONSE_UPDATE = API_DUME_RESPONSE_PATH + "update";
    public static final String API_DUME_RESPONSE_CREATE_FULL_PATH = API_DUME_RESPONSE_PATH + "create/";
    public static final String API_DUME_RESPONSE_VALIDATE_FULL_PATH = API_DUME_RESPONSE_PATH + "validation/";
    public static final String API_DUME_RESPONSE_PUBLICATION_FULL_PATH = API_DUME_RESPONSE_PATH + "publication/";
    public static final String API_DUME_RESPONSE_PUBLICATION_GROUPEMENT_FULL_PATH = API_DUME_RESPONSE_PATH + "publicationGroupement";
    public static final String API_DUME_RESPONSE_PUBLICATION_LIST_FULL_PATH = API_DUME_RESPONSE_PUBLICATION_FULL_PATH + "list/";
    public static final String API_DUME_RESPONSE_SAVE_AND_DOWLOAD_PDF_FULL_PATH = API_DUME_RESPONSE_PATH + "updateAndDownload";
    public static final String API_DUME_RESPONSE_DOWNLOAD_XML_FULL_PATH = API_DUME_RESPONSE_PATH + "xml/";
    public static final String API_DUME_RESPONSE_DOWNLOAD_XML_FRONT_FULL_PATH = API_DUME_RESPONSE_PATH + "downloadXml/";
    public static final String API_DUME_RESPONSE_UPDATE_METADONNE_FULL_PATH = API_DUME_RESPONSE_PATH + "updateMetadonnee/";
    public static final String API_DUME_RESPONSE_FIN_BY_SN_NUMBER_FULL_PATH = API_DUME_RESPONSE_PATH + "find/";
    public static final String API_DUME_GET_E_CERTIS_PROOF_FULL_PATH = API_DUME_RESPONSE_PATH + "e-certis-proof/";


    public static final String API_CRITERIA_PATH = DUME_REST_API_BASE_PATH + "criteria/";
    public static final String API_CRITERIA_SELECTION_PATH = API_CRITERIA_PATH + "selection";
    public static final String API_CRITERIA_EXCLUSION_PATH = API_CRITERIA_PATH + "exclusion";
    public static final String API_CRITERIA_OTHER_PATH = API_CRITERIA_PATH + "other";

    public static final String API_MODEL_PATH = DUME_REST_API_BASE_PATH + "model/";
    public static final String API_MODEL_PATH_IMPORTER_EDITION = API_MODEL_PATH + "importerXML/editeur/";
    public static final String API_MODEL_PATH_IMPORTER_PLATFORM = API_MODEL_PATH + "importerXML/platform/";

    public static final String API_DONNEES_ESSENTIELLES = DUME_REST_API_BASE_PATH + "donneesEssentielles/";
    public static final String API_DE_CONTRAT_STATUT = API_DONNEES_ESSENTIELLES + "statutContrat/";

    public static final String JSON_ROOT = "$";
    public static final String JSON_STATUS = "$.statut";

    public static final String JSON_ID_DUME_REQUEST = "$.id";

    public static final String JSON_ID_DUME_RESPONSE = "$.id";

    public static final String JSON_1_USER_DTO_ID = "$[0].id";
    public static final String JSON_1_USER_DTO_NAME = "$[0].name";

    public static final String JSON_CONTEXT_DTO = "$.idContext";
    public static final String JSON_1_CONTEXT_DTO = "$[0].idContext";
    public static final String JSON_2_CONTEXT_DTO = "$[1].idContext";

    public static final String JSON_2_USER_DTO_ID = "$[1].id";
    public static final String JSON_2_USER_DTO_NAME = "$[1].name";

    public static final String JSON_USER_IDENTIFIER = "$.id";

    public static final String JSON_DUME_RESPONSE_CREATION_NUMERO_SN = "$.numeroSN";
    public static final String JSON_DUME_RESPONSE_CREATION_ID_CONTEXT = "$.idContexteOE";

    public static final String USER_CREATION_EX_MSG = "Can't Create the User";
    public static final String USER_GET_ALL_EX_MSG = "Problem occurred when contacting the DB";
}
