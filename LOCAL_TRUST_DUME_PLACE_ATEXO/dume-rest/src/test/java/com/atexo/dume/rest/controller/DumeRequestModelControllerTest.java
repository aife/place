package com.atexo.dume.rest.controller;

import com.atexo.dume.services.api.ImportXMLService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static com.atexo.dume.rest.controller.Utils.Constants.API_MODEL_PATH_IMPORTER_EDITION;
import static com.atexo.dume.rest.controller.Utils.Constants.API_MODEL_PATH_IMPORTER_PLATFORM;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DumeRequestModelControllerTest {

    @InjectMocks
    private DumeRequestModelController dumeRequestModelController;

    @Mock
    private ImportXMLService importXMLService;

    private MockMvc mockMvc;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(dumeRequestModelController)
                .build();
    }

    @Test
    public void importerXMLEditionException() throws Exception {
        MockMultipartFile acheteur = new MockMultipartFile("model", "filename.txt", "text/plain", "some xml".getBytes());

        when(importXMLService.importDumeRequestModelDomaine(any(), any())).thenThrow(new IllegalArgumentException());

        mockMvc.perform(MockMvcRequestBuilders.multipart(API_MODEL_PATH_IMPORTER_EDITION + "TEST")
                .file(acheteur))
                .andExpect(status().isInternalServerError());

        verify(importXMLService, times(1)).importDumeRequestModelDomaine(any(), any());
        verifyNoMoreInteractions(importXMLService);
    }

    @Test
    public void importerXMLEditionOK() throws Exception {
        MockMultipartFile acheteur = new MockMultipartFile("model", "filename.txt", "text/plain", "some xml".getBytes());

        when(importXMLService.importDumeRequestModelDomaine(any(), any())).thenReturn(Boolean.TRUE);

        mockMvc.perform(MockMvcRequestBuilders.multipart(API_MODEL_PATH_IMPORTER_EDITION + "TEST")
                .file(acheteur))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(Boolean.TRUE)));

        verify(importXMLService, times(1)).importDumeRequestModelDomaine(any(), any());
        verifyNoMoreInteractions(importXMLService);
    }

    @Test
    public void importerXMLPlatformException() throws Exception {
        MockMultipartFile acheteur = new MockMultipartFile("model", "filename.txt", "text/plain", "some xml".getBytes());

        when(importXMLService.importDumeRequestModelPlatform(any(), any())).thenThrow(new IllegalArgumentException());

        mockMvc.perform(MockMvcRequestBuilders.multipart(API_MODEL_PATH_IMPORTER_PLATFORM + "TEST")
                .file(acheteur))
                .andExpect(status().isInternalServerError());
        verify(importXMLService, times(1)).importDumeRequestModelPlatform(any(), any());
        verifyNoMoreInteractions(importXMLService);
    }

    @Test
    public void importerXMLPlatformOK() throws Exception {
        MockMultipartFile acheteur = new MockMultipartFile("model", "filename.txt", "text/plain", "some xml".getBytes());

        when(importXMLService.importDumeRequestModelPlatform(any(), any())).thenReturn(Boolean.TRUE);

        mockMvc.perform(MockMvcRequestBuilders.multipart(API_MODEL_PATH_IMPORTER_PLATFORM + "TEST")
                .file(acheteur))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(Boolean.TRUE)));
        verify(importXMLService, times(1)).importDumeRequestModelPlatform(any(), any());
        verifyNoMoreInteractions(importXMLService);
    }

}
