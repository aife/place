package com.atexo.dume.rest.controller;

import com.atexo.dume.dto.DumeRequestContextDTO;
import com.atexo.dume.dto.PublicationStatus;
import com.atexo.dume.dto.RetourStatus;
import com.atexo.dume.dto.StatusDTO;
import com.atexo.dume.dto.dataset.DumeRequestContextDataDTOSet;
import com.atexo.dume.services.api.DumeRequestContextService;
import com.atexo.dume.services.api.DumeRequestService;
import com.atexo.dume.services.exception.DumeResourceNotFoundException;
import com.atexo.dume.services.exception.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.atexo.dume.dto.dataset.DumeRequestContextDataDTOSet.SIMPLE_CONTX_ID;
import static com.atexo.dume.rest.controller.Utils.Constants.*;
import static com.atexo.dume.rest.controller.Utils.JsonUtils.asJsonString;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DumeRequestControllerTest {

    @InjectMocks
    private DumeRequestController dumeRequestController;

    @Mock
    private DumeRequestContextService dumeRequestContextService;

    @Mock
    private DumeRequestService dumeRequestService;

    private MockMvc mockMvc;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(dumeRequestController)
                .setControllerAdvice(new ControllerExceptionHandler())
                .build();
    }

    @Test
    public void createDumeTest() throws Exception {
        final DumeRequestContextDTO context = new DumeRequestContextDTO();
        context.setId(1000L);
        context.setConsultRef("58");
        when(dumeRequestContextService.saveContext(context)).thenReturn(context);

        mockMvc.perform(post(API_DUME_REQUEST_CREATE_FULL_PATH)
                .contentType(APPLICATION_JSON)
                .content(asJsonString(context)))
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_ROOT, is(1000)));

        verify(dumeRequestContextService, times(1)).saveContext(context);
        verifyNoMoreInteractions(dumeRequestContextService);
    }

    @Test
    public void createDumeTestWithException() throws Exception {
        final DumeRequestContextDTO context = new DumeRequestContextDTO();
        context.setId(1000L);
        context.setConsultRef("58");
        when(dumeRequestContextService.saveContext(any())).thenThrow(new ServiceException(USER_GET_ALL_EX_MSG));

        mockMvc.perform(post(API_DUME_REQUEST_CREATE_FULL_PATH)
                .contentType(APPLICATION_JSON)
                .content(asJsonString(context)))
                .andExpect(status().isInternalServerError());

        verify(dumeRequestContextService, times(1)).saveContext(any());
        verifyNoMoreInteractions(dumeRequestContextService);

    }

    @Test
    public void findByIdOk() throws Exception {
        final Long id = SIMPLE_CONTX_ID;
        final DumeRequestContextDTO dumeRequestDTO = DumeRequestContextDataDTOSet.contextSample();
        when(dumeRequestContextService.getDumeRequestContextById(id)).thenReturn(dumeRequestDTO);

        mockMvc.perform(get(API_DUME_REQUEST_GET_FULL_PATH + id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_CONTEXT_DTO, is(id)));

        verify(dumeRequestContextService, times(1)).getDumeRequestContextById(id);
        verifyNoMoreInteractions(dumeRequestContextService);

    }

    @Test
    public void findByIdNotFound() throws Exception {
        final Long id = SIMPLE_CONTX_ID;

        when(dumeRequestContextService.getDumeRequestContextById(id)).thenThrow(new DumeResourceNotFoundException("DumeRequest", SIMPLE_CONTX_ID));

        mockMvc.perform(get(API_DUME_REQUEST_GET_FULL_PATH + id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(dumeRequestContextService, times(1)).getDumeRequestContextById(id);
        verifyNoMoreInteractions(dumeRequestContextService);

    }

    @Test
    public void findByIdException() throws Exception {
        final Long id = SIMPLE_CONTX_ID;

        when(dumeRequestContextService.getDumeRequestContextById(id)).thenThrow(new IllegalArgumentException(USER_GET_ALL_EX_MSG));

        mockMvc.perform(get(API_DUME_REQUEST_GET_FULL_PATH + id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isInternalServerError());

        verify(dumeRequestContextService, times(1)).getDumeRequestContextById(id);
        verifyNoMoreInteractions(dumeRequestContextService);

    }

    @Test
    public void updateContextByIdOk() throws Exception {
        final Long id = SIMPLE_CONTX_ID;
        final DumeRequestContextDTO context = DumeRequestContextDataDTOSet.contextSample();
        when(dumeRequestContextService.updateContextWithoutRequest(any(), any())).thenReturn(id);

        mockMvc.perform(post(API_DUME_REQUEST_UPDATE_FULL_PATH + id)
                .contentType(APPLICATION_JSON)
                .content(asJsonString(context)))
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_ROOT, is(id)));

        verify(dumeRequestContextService, times(1)).updateContextWithoutRequest(any(), any());
        verifyNoMoreInteractions(dumeRequestContextService);

    }

    @Test
    public void updateContextByIdNOk() throws Exception {
        final Long id = SIMPLE_CONTX_ID;
        final DumeRequestContextDTO context = DumeRequestContextDataDTOSet.contextSample();

        when(dumeRequestContextService.updateContextWithoutRequest(any(), any())).thenThrow(new DumeResourceNotFoundException("DumeRequestContext", SIMPLE_CONTX_ID));

        mockMvc.perform(post(API_DUME_REQUEST_UPDATE_FULL_PATH + id)
                .contentType(APPLICATION_JSON)
                .content(asJsonString(context)))
                .andExpect(status().isNotFound());

        verify(dumeRequestContextService, times(1)).updateContextWithoutRequest(any(), any());
        verifyNoMoreInteractions(dumeRequestContextService);

    }

    @Test
    public void updateContextByIdException() throws Exception {
        final Long id = SIMPLE_CONTX_ID;
        final DumeRequestContextDTO context = DumeRequestContextDataDTOSet.contextSample();

        when(dumeRequestContextService.updateContextWithoutRequest(any(), any())).thenThrow(new IllegalArgumentException(USER_GET_ALL_EX_MSG));

        mockMvc.perform(post(API_DUME_REQUEST_UPDATE_FULL_PATH + id)
                .contentType(APPLICATION_JSON)
                .content(asJsonString(context)))
                .andExpect(status().isInternalServerError());

        verify(dumeRequestContextService, times(1)).updateContextWithoutRequest(any(), any());
        verifyNoMoreInteractions(dumeRequestContextService);

    }

    @Test
    public void deleteDumeRequestByIdOk() throws Exception {
        final Long id = SIMPLE_CONTX_ID;

        when(dumeRequestContextService.deleteContextDumeRequest(id)).thenReturn(true);

        mockMvc.perform(delete(API_DUME_REQUEST_DELETE_FULL_PATH + id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_ROOT, is(true)));

        verify(dumeRequestContextService, times(1)).deleteContextDumeRequest(id);
        verifyNoMoreInteractions(dumeRequestContextService);

    }

    @Test
    public void deleteDumeRequestByIdNOk() throws Exception {
        final Long id = SIMPLE_CONTX_ID;

        when(dumeRequestContextService.deleteContextDumeRequest(id)).thenThrow(new DumeResourceNotFoundException("01", "01"));

        mockMvc.perform(delete(API_DUME_REQUEST_DELETE_FULL_PATH + id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(dumeRequestContextService, times(1)).deleteContextDumeRequest(id);
        verifyNoMoreInteractions(dumeRequestContextService);

    }

    @Test
    public void deleteDumeRequestByIdException() throws Exception {
        final Long id = SIMPLE_CONTX_ID;

        when(dumeRequestContextService.deleteContextDumeRequest(id)).thenThrow(new IllegalArgumentException(USER_GET_ALL_EX_MSG));

        mockMvc.perform(delete(API_DUME_REQUEST_DELETE_FULL_PATH + id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isInternalServerError());

        verify(dumeRequestContextService, times(1)).deleteContextDumeRequest(id);
        verifyNoMoreInteractions(dumeRequestContextService);

    }

    @Test
    public void updateDumeRequestByDTOOk() throws Exception {
        final DumeRequestContextDTO contextDTO = DumeRequestContextDataDTOSet.contextSample();

        when(dumeRequestContextService.updateDumeContextFromDTO(any())).thenReturn(contextDTO);

        mockMvc.perform(post(API_DUME_REQUEST_UPDATE)
                .contentType(APPLICATION_JSON)
                .content(asJsonString(contextDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_CONTEXT_DTO, is(contextDTO.getId())));

        verify(dumeRequestContextService, times(1)).updateDumeContextFromDTO(any());
        verifyNoMoreInteractions(dumeRequestContextService);

    }

    @Test
    public void updateDumeRequestByDTONOk() throws Exception {
        final DumeRequestContextDTO contextDTO = DumeRequestContextDataDTOSet.contextSample();
        when(dumeRequestContextService.updateDumeContextFromDTO(any())).thenThrow(new DumeResourceNotFoundException("DumeRequest", SIMPLE_CONTX_ID));

        mockMvc.perform(post(API_DUME_REQUEST_UPDATE)
                .contentType(APPLICATION_JSON)
                .content(asJsonString(contextDTO)))
                .andExpect(status().isNotFound());

        verify(dumeRequestContextService, times(1)).updateDumeContextFromDTO(any());
        verifyNoMoreInteractions(dumeRequestContextService);

    }


    @Test
    public void updateDumeRequestByDTOException() throws Exception {
        final DumeRequestContextDTO contextDTO = DumeRequestContextDataDTOSet.contextSample();

        when(dumeRequestContextService.updateDumeContextFromDTO(any())).thenThrow(new IllegalArgumentException(USER_GET_ALL_EX_MSG));

        mockMvc.perform(post(API_DUME_REQUEST_UPDATE)
                .contentType(APPLICATION_JSON)
                .content(asJsonString(contextDTO)))
                .andExpect(status().isInternalServerError());

        verify(dumeRequestContextService, times(1)).updateDumeContextFromDTO(any());
        verifyNoMoreInteractions(dumeRequestContextService);

    }

    @Test
    public void getAllDumeRequestOk() throws Exception {
        final List<DumeRequestContextDTO> dtos = asList(DumeRequestContextDataDTOSet.contextSample(), DumeRequestContextDataDTOSet.contextSample());

        when(dumeRequestContextService.getAllDumeRequestContext()).thenReturn(dtos);
        mockMvc.perform(get(API_DUME_REQUEST_ALL_FULL_PATH))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath(JSON_ROOT, hasSize(2)))
                .andExpect(jsonPath(JSON_1_CONTEXT_DTO, is(SIMPLE_CONTX_ID)))
                .andExpect(jsonPath(JSON_2_CONTEXT_DTO, is(SIMPLE_CONTX_ID)));

    }

    @Test
    public void getAllDumeRequestExeception() throws Exception {

        when(dumeRequestContextService.getAllDumeRequestContext()).thenThrow(new IllegalArgumentException());

        mockMvc.perform(get(API_DUME_REQUEST_ALL_FULL_PATH))
                .andExpect(status().isInternalServerError());

    }

    @Test
    public void validateDumeRequestOK() throws Exception {
        final RetourStatus retourStatus = new RetourStatus(StatusDTO.VALIDE, "");

        when(dumeRequestContextService.checkValidContextDume(any())).thenReturn(retourStatus);

        mockMvc.perform(get(API_DUME_REQUEST_VALIDATE + 100L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath(JSON_STATUS, is(StatusDTO.VALIDE.name())));

        verify(dumeRequestContextService, times(1)).checkValidContextDume(any());
        verifyNoMoreInteractions(dumeRequestContextService);
    }

    @Test
    public void validateDumeRequestNotFound() throws Exception {
        when(dumeRequestContextService.checkValidContextDume(any())).thenThrow(new DumeResourceNotFoundException("DumeRequest", 12L));

        mockMvc.perform(get(API_DUME_REQUEST_VALIDATE + 100L))
                .andExpect(status().isNotFound());

        verify(dumeRequestContextService, times(1)).checkValidContextDume(any());
        verifyNoMoreInteractions(dumeRequestContextService);
    }

    @Test
    public void validateDumeRequestException() throws Exception {
        when(dumeRequestContextService.checkValidContextDume(any())).thenThrow(new IllegalArgumentException());

        mockMvc.perform(get(API_DUME_REQUEST_VALIDATE + 100L))
                .andExpect(status().isInternalServerError());

        verify(dumeRequestContextService, times(1)).checkValidContextDume(any());
        verifyNoMoreInteractions(dumeRequestContextService);

    }

    @Test
    public void publishOK() throws Exception {
        final RetourStatus retourStatus = new RetourStatus(StatusDTO.OK, "");

        when(dumeRequestContextService.publishAllDumeRequest(100L)).thenReturn(retourStatus);

        mockMvc.perform(get(API_DUME_REQUEST_PUBLISH + 100L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath(JSON_STATUS, is(StatusDTO.OK.name())));

        verify(dumeRequestContextService, times(1)).publishAllDumeRequest(100L);
        verifyNoMoreInteractions(dumeRequestContextService);
    }


    @Test
    public void publishNOTfound() throws Exception {
        when(dumeRequestContextService.publishAllDumeRequest(any())).thenThrow(new DumeResourceNotFoundException("DumeRequest", 12L));

        mockMvc.perform(get(API_DUME_REQUEST_PUBLISH + 100L))
                .andExpect(status().isNotFound());

        verify(dumeRequestContextService, times(1)).publishAllDumeRequest(100L);
        verifyNoMoreInteractions(dumeRequestContextService);
    }


    @Test
    public void publishException() throws Exception {
        when(dumeRequestContextService.publishAllDumeRequest(any())).thenThrow(new IllegalArgumentException());

        mockMvc.perform(get(API_DUME_REQUEST_PUBLISH + 100L))
                .andExpect(status().isInternalServerError());

        verify(dumeRequestContextService, times(1)).publishAllDumeRequest(100L);
        verifyNoMoreInteractions(dumeRequestContextService);
    }

    @Test
    public void getAllPublishedDumeRequestForContextOK() throws Exception {
        final PublicationStatus publicationStatus = new PublicationStatus(StatusDTO.OK, new ArrayList<>());
        when(dumeRequestContextService.recupererStatusPublicationDumeAcheteur(100L)).thenReturn(publicationStatus);

        mockMvc.perform(get(API_DUME_REQUEST_PUBLISH_LIST + 100L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath(JSON_STATUS, is(StatusDTO.OK.name())));

        verify(dumeRequestContextService, times(1)).recupererStatusPublicationDumeAcheteur(100L);
        verifyNoMoreInteractions(dumeRequestContextService);
    }

    @Test
    public void getAllPublishedDumeRequestForContextNotFound() throws Exception {

        when(dumeRequestContextService.recupererStatusPublicationDumeAcheteur(100L)).thenThrow(new DumeResourceNotFoundException("DumeRequest", 100L));

        mockMvc.perform(get(API_DUME_REQUEST_PUBLISH_LIST + 100L))
                .andExpect(status().isNotFound());

        verify(dumeRequestContextService, times(1)).recupererStatusPublicationDumeAcheteur(100L);
        verifyNoMoreInteractions(dumeRequestContextService);
    }

    @Test
    public void getAllPublishedDumeRequestForContextException() throws Exception {
        when(dumeRequestContextService.recupererStatusPublicationDumeAcheteur(100L)).thenThrow(new IllegalArgumentException());

        mockMvc.perform(get(API_DUME_REQUEST_PUBLISH_LIST + 100L))
                .andExpect(status().isInternalServerError());

        verify(dumeRequestContextService, times(1)).recupererStatusPublicationDumeAcheteur(100L);
        verifyNoMoreInteractions(dumeRequestContextService);
    }


    @Test
    public void updateDLROOK() throws Exception {
        final DumeRequestContextDTO dumeRequestContextDTO = DumeRequestContextDataDTOSet.contextSample();
        when(dumeRequestContextService.updateDLRO(anyLong(), any(ZonedDateTime.class))).thenReturn(Boolean.TRUE);

        mockMvc.perform(post(API_DUME_REQUEST_UPDATE_DLRO + 100L)
                .content(asJsonString(dumeRequestContextDTO))
                .contentType(APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath(JSON_ROOT, is(true)));

        verify(dumeRequestContextService, times(1)).updateDLRO(anyLong(), any(ZonedDateTime.class));
        verifyNoMoreInteractions(dumeRequestContextService);
    }

    @Test
    public void updateDLRONotFound() throws Exception {
        final DumeRequestContextDTO dumeRequestContextDTO = DumeRequestContextDataDTOSet.contextSample();
        when(dumeRequestContextService.updateDLRO(anyLong(), any(ZonedDateTime.class))).thenThrow(new DumeResourceNotFoundException("DumeRequest", 100L));

        mockMvc.perform(post(API_DUME_REQUEST_UPDATE_DLRO + 100L)
                .content(asJsonString(dumeRequestContextDTO))
                .contentType(APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound());

        verify(dumeRequestContextService, times(1)).updateDLRO(anyLong(), any(ZonedDateTime.class));
        verifyNoMoreInteractions(dumeRequestContextService);
    }

    @Test
    public void updateDLROException() throws Exception {
        final DumeRequestContextDTO dumeRequestContextDTO = DumeRequestContextDataDTOSet.contextSample();
        when(dumeRequestContextService.updateDLRO(anyLong(), any(ZonedDateTime.class))).thenThrow(new IllegalArgumentException());

        mockMvc.perform(post(API_DUME_REQUEST_UPDATE_DLRO + 100L)
                .content(asJsonString(dumeRequestContextDTO))
                .contentType(APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isInternalServerError());

        verify(dumeRequestContextService, times(1)).updateDLRO(anyLong(), any(ZonedDateTime.class));
        verifyNoMoreInteractions(dumeRequestContextService);
    }

    @Test
    public void replacePublishedDumeRequestOK() throws Exception {
        when(dumeRequestContextService.updateRequestContextAfterPublication(anyLong())).thenReturn(new RetourStatus());

        mockMvc.perform(get(API_DUME_REQUEST_UPDATE_PUBLISHED + 100L)
                .contentType(APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));

        verify(dumeRequestContextService, times(1)).updateRequestContextAfterPublication(anyLong());
        verifyNoMoreInteractions(dumeRequestContextService);
    }

    @Test
    public void replacePublishedDumeNOTFound() throws Exception {
        when(dumeRequestContextService.updateRequestContextAfterPublication(anyLong())).thenThrow(new DumeResourceNotFoundException("Context", "TEST"));

        mockMvc.perform(get(API_DUME_REQUEST_UPDATE_PUBLISHED + 100L)
                .contentType(APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound());

        verify(dumeRequestContextService, times(1)).updateRequestContextAfterPublication(anyLong());
        verifyNoMoreInteractions(dumeRequestContextService);
    }


    @Test
    public void checkPurgeOK() throws Exception {
        when(dumeRequestContextService.checkPurgeCriteriaLot(anyLong(), any())).thenReturn(new RetourStatus(StatusDTO.PURGE, ""));

        mockMvc.perform(post(API_DUME_REQUEST_CHECK_PURGE + 100L)
                .contentType(APPLICATION_JSON_UTF8_VALUE)
                .content(asJsonString(DumeRequestContextDataDTOSet.contextSample())))
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_STATUS, is("PURGE")));

        verify(dumeRequestContextService, times(1)).checkPurgeCriteriaLot(anyLong(), any());
        verifyNoMoreInteractions(dumeRequestContextService);

    }

    @Test
    public void checkPurgeNotFound() throws Exception {
        when(dumeRequestContextService.checkPurgeCriteriaLot(anyLong(), any())).thenThrow(new DumeResourceNotFoundException("OK", "YEAH"));

        mockMvc.perform(post(API_DUME_REQUEST_CHECK_PURGE + 100L)
                .contentType(APPLICATION_JSON_UTF8_VALUE)
                .content(asJsonString(DumeRequestContextDataDTOSet.contextSample())))
                .andExpect(status().isNotFound());

        verify(dumeRequestContextService, times(1)).checkPurgeCriteriaLot(anyLong(), any());
        verifyNoMoreInteractions(dumeRequestContextService);
    }

    @Test
    public void checkPurgeException() throws Exception {
        when(dumeRequestContextService.checkPurgeCriteriaLot(anyLong(), any())).thenThrow(new IllegalArgumentException("OK"));

        mockMvc.perform(post(API_DUME_REQUEST_CHECK_PURGE + 100L)
                .contentType(APPLICATION_JSON_UTF8_VALUE)
                .content(asJsonString(DumeRequestContextDataDTOSet.contextSample())))
                .andExpect(status().isInternalServerError());

        verify(dumeRequestContextService, times(1)).checkPurgeCriteriaLot(anyLong(), any());
        verifyNoMoreInteractions(dumeRequestContextService);
    }

    @Test
    public void downloadPdfOk() throws Exception {
        final DumeRequestContextDTO dumeRequestContextDTO = DumeRequestContextDataDTOSet.contextSample();
        doReturn(new byte[]{0}).when(dumeRequestContextService).downloadPdf(anyLong());

        mockMvc.perform(get(API_DUME_REQUEST_DOWNLOAD_PDF_FULL_PATH + dumeRequestContextDTO.getId())).andExpect(header().string(CONTENT_TYPE, "application/pdf")).andExpect(header().string(CONTENT_DISPOSITION, "attachment; filename=dumeA.pdf")).andExpect(status().isOk());

        verify(dumeRequestContextService, times(1)).downloadPdf(anyLong());
        verifyNoMoreInteractions(dumeRequestContextService);
    }

    @Test
    public void downloadPdfNotFound() throws Exception {
        final DumeRequestContextDTO dumeRequestContextDTO = DumeRequestContextDataDTOSet.contextSample();
        when(dumeRequestContextService.downloadPdf(anyLong())).thenThrow(new DumeResourceNotFoundException("DumeRequestContext", dumeRequestContextDTO.getId()));

        mockMvc.perform(get(API_DUME_REQUEST_DOWNLOAD_PDF_FULL_PATH + dumeRequestContextDTO.getId())).andExpect(status().isNotFound());

        verify(dumeRequestContextService, times(1)).downloadPdf(anyLong());
        verifyNoMoreInteractions(dumeRequestContextService);
    }

    @Test
    public void downloadPdfException() throws Exception {
        final DumeRequestContextDTO dumeRequestContextDTO = DumeRequestContextDataDTOSet.contextSample();
        doThrow(new ServiceException("TEST")).when(dumeRequestContextService).downloadPdf(anyLong());

        mockMvc.perform(get(API_DUME_REQUEST_DOWNLOAD_PDF_FULL_PATH + dumeRequestContextDTO.getId())).andExpect(status().isInternalServerError());
        verify(dumeRequestContextService, times(1)).downloadPdf(anyLong());
        verifyNoMoreInteractions(dumeRequestContextService);
    }

    @Test
    public void getXMLDumeRequestByNumeroSNOk() throws Exception {

        doReturn(new byte[]{0}).when(dumeRequestService).generateXMLByNumeroSN(anyString());

        mockMvc.perform(get(API_DUME_REQUEST_GET_XML_FULL_PATH + "1235"))
                .andExpect(header().string(CONTENT_TYPE, APPLICATION_XML_VALUE))
                .andExpect(header().string(CONTENT_DISPOSITION, "attachment; filename=" + "1235" + ".xml"))
                .andExpect(status().isOk());

        verify(dumeRequestService, times(1)).generateXMLByNumeroSN(anyString());
        verifyNoMoreInteractions(dumeRequestService);
    }

    @Test
    public void getXMLDumeRequestByNumeroSNNotFound() throws Exception {
        final DumeRequestContextDTO dumeRequestContextDTO = DumeRequestContextDataDTOSet.contextSample();
        when(dumeRequestService.generateXMLByNumeroSN(anyString())).thenThrow(new DumeResourceNotFoundException("DumeRequestContext", dumeRequestContextDTO.getId()));

        mockMvc.perform(get(API_DUME_REQUEST_GET_XML_FULL_PATH + "1235"))
                .andExpect(status().isNotFound());

        verify(dumeRequestService, times(1)).generateXMLByNumeroSN(anyString());
        verifyNoMoreInteractions(dumeRequestService);
    }

    @Test
    public void getXMLDumeRequestByNumeroSNException() throws Exception {
        final DumeRequestContextDTO dumeRequestContextDTO = DumeRequestContextDataDTOSet.contextSample();
        doThrow(new ServiceException("TEST")).when(dumeRequestService).generateXMLByNumeroSN(any());

        mockMvc.perform(get(API_DUME_REQUEST_GET_XML_FULL_PATH + "1235"))
                .andExpect(status().isInternalServerError());
        verify(dumeRequestService, times(1)).generateXMLByNumeroSN(anyString());
        verifyNoMoreInteractions(dumeRequestService);
    }

}
