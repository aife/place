package com.atexo.dume.rest.controller;

import com.atexo.dume.dto.*;
import com.atexo.dume.dto.dataset.DumeRequestContextDataDTOSet;
import com.atexo.dume.dto.dataset.DumeResponseDTODataSet;
import com.atexo.dume.dto.dataset.PublicationGroupementDTODataSet;
import com.atexo.dume.dto.dataset.SelectionCriteriaDTODataSet;
import com.atexo.dume.services.api.DumeResponseService;
import com.atexo.dume.services.exception.DumeResourceNotFoundException;
import com.atexo.dume.services.exception.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;

import static com.atexo.dume.dto.dataset.DumeResponseDTODataSet.DUME_RESPONSE_DTO_IDENTIFIER_1;
import static com.atexo.dume.dto.dataset.DumeResponseDTODataSet.DUME_RESPONSE_NUMERO_SN_1;
import static com.atexo.dume.rest.controller.Utils.Constants.*;
import static com.atexo.dume.rest.controller.Utils.JsonUtils.asJsonString;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class DumeResponseControllerTest {

    @InjectMocks
    private DumeResponseController dumeResponseController;

    @Mock
    private DumeResponseService dumeResponseService;

    private MockMvc mockMvc;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(dumeResponseController)
                .setControllerAdvice(new ControllerExceptionHandler())
                .build();
    }

    @Test
    public void findByIdOk() throws Exception {
        final Long id = DUME_RESPONSE_DTO_IDENTIFIER_1;
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithId();
        when(dumeResponseService.getDumeResponseDTOById(id)).thenReturn(dumeResponseDTO);

        mockMvc.perform(get(API_DUME_RESPONSE_GET_FULL_PATH + id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_ID_DUME_RESPONSE, is(id.intValue())));

        verify(dumeResponseService, times(1)).getDumeResponseDTOById(id);
        verifyNoMoreInteractions(dumeResponseService);

    }

    @Test
    public void findByIdNotFound() throws Exception {
        final Long id = DUME_RESPONSE_DTO_IDENTIFIER_1;

        when(dumeResponseService.getDumeResponseDTOById(id)).thenThrow(new DumeResourceNotFoundException("DumeResponse", id));

        mockMvc.perform(get(API_DUME_RESPONSE_GET_FULL_PATH + id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(dumeResponseService, times(1)).getDumeResponseDTOById(id);
        verifyNoMoreInteractions(dumeResponseService);

    }

    @Test
    public void findByIdException() throws Exception {
        final Long id = DUME_RESPONSE_DTO_IDENTIFIER_1;

        when(dumeResponseService.getDumeResponseDTOById(id)).thenThrow(new IllegalArgumentException(USER_GET_ALL_EX_MSG));

        mockMvc.perform(get(API_DUME_RESPONSE_GET_FULL_PATH + id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isInternalServerError());

        verify(dumeResponseService, times(1)).getDumeResponseDTOById(id);
        verifyNoMoreInteractions(dumeResponseService);

    }


    @Test
    public void updateDumeResponseByDTOOk() throws Exception {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithId();
        dumeResponseDTO.setConsultRef("85");
        when(dumeResponseService.updateDumeResponseByDTO(any())).thenReturn(dumeResponseDTO);

        mockMvc.perform(post(API_DUME_RESPONSE_UPDATE)
                .contentType(APPLICATION_JSON)
                .content(asJsonString(dumeResponseDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_ID_DUME_RESPONSE, is(dumeResponseDTO.getId().intValue())));

        verify(dumeResponseService, times(1)).updateDumeResponseByDTO(any());
        verifyNoMoreInteractions(dumeResponseService);

    }

    @Test
    public void updateDumeResponseByDTONOk() throws Exception {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithId();
        dumeResponseDTO.setConsultRef("85");
        when(dumeResponseService.updateDumeResponseByDTO(any())).thenThrow(new DumeResourceNotFoundException("TEST", "TEST"));

        mockMvc.perform(post(API_DUME_RESPONSE_UPDATE)
                .contentType(APPLICATION_JSON)
                .content(asJsonString(dumeResponseDTO)))
                .andExpect(status().isNotFound());

        verify(dumeResponseService, times(1)).updateDumeResponseByDTO(any());
        verifyNoMoreInteractions(dumeResponseService);

    }

    @Test
    public void updateDumeResponseByDTOException() throws Exception {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithId();
        dumeResponseDTO.setConsultRef("85");
        when(dumeResponseService.updateDumeResponseByDTO(any())).thenThrow(new IllegalArgumentException(USER_GET_ALL_EX_MSG));

        mockMvc.perform(post(API_DUME_RESPONSE_UPDATE)
                .contentType(APPLICATION_JSON)
                .content(asJsonString(dumeResponseDTO)))
                .andExpect(status().isInternalServerError());

        verify(dumeResponseService, times(1)).updateDumeResponseByDTO(any());
        verifyNoMoreInteractions(dumeResponseService);

    }

    @Test
    public void createDumeResponseOk() throws Exception {
        final Long id = DUME_RESPONSE_DTO_IDENTIFIER_1;
        final String numeroSn = DUME_RESPONSE_NUMERO_SN_1;
        final DumeRequestContextDTO context = DumeRequestContextDataDTOSet.contextSample();
        context.setMetadata(new MetaDataDTO());
        context.getMetadata().setIdContextDumeAcheteur(String.valueOf(id));
        when(dumeResponseService.createDumeResponseBy(context)).thenReturn(new DumeResponseRetourCreationDTO(numeroSn, id));

        mockMvc.perform(post(API_DUME_RESPONSE_CREATE_FULL_PATH)
                .content(asJsonString(context))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_DUME_RESPONSE_CREATION_ID_CONTEXT, is(id.intValue())))
                .andExpect(jsonPath(JSON_DUME_RESPONSE_CREATION_NUMERO_SN, is(numeroSn)));

        verify(dumeResponseService, times(1)).createDumeResponseBy(any());
        verifyNoMoreInteractions(dumeResponseService);

    }

    @Test
    public void createDumeResponseNotFound() throws Exception {
        final Long id = DUME_RESPONSE_DTO_IDENTIFIER_1;
        final DumeRequestContextDTO context = DumeRequestContextDataDTOSet.contextSample();
        context.setMetadata(new MetaDataDTO());
        context.getMetadata().setIdContextDumeAcheteur(String.valueOf(id));
        when(dumeResponseService.createDumeResponseBy(any())).thenThrow(new DumeResourceNotFoundException("DumeResponse", id));

        mockMvc.perform(post(API_DUME_RESPONSE_CREATE_FULL_PATH)
                .content(asJsonString(context))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(dumeResponseService, times(1)).createDumeResponseBy(any());
        verifyNoMoreInteractions(dumeResponseService);

    }

    @Test
    public void createDumeResponseException() throws Exception {
        final Long id = DUME_RESPONSE_DTO_IDENTIFIER_1;
        final DumeRequestContextDTO context = DumeRequestContextDataDTOSet.contextSample();
        context.setMetadata(new MetaDataDTO());
        context.getMetadata().setIdContextDumeAcheteur(String.valueOf(id));

        when(dumeResponseService.createDumeResponseBy(context)).thenThrow(new IllegalArgumentException(USER_GET_ALL_EX_MSG));

        mockMvc.perform(post(API_DUME_RESPONSE_CREATE_FULL_PATH)
                .content(asJsonString(context))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isInternalServerError());

        verify(dumeResponseService, times(1)).createDumeResponseBy(any());
        verifyNoMoreInteractions(dumeResponseService);

    }

    @Test
    public void validateDumeResponseOK() throws Exception {
        final Long id = DUME_RESPONSE_DTO_IDENTIFIER_1;
        when(dumeResponseService.validateDumeOE(id)).thenReturn(new RetourStatus(StatusDTO.VALIDE, ""));

        mockMvc.perform(get(API_DUME_RESPONSE_VALIDATE_FULL_PATH + id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_STATUS, is(StatusDTO.VALIDE.name())));

        verify(dumeResponseService, times(1)).validateDumeOE(id);
        verifyNoMoreInteractions(dumeResponseService);

    }

    @Test
    public void validateDumeResponseNOTFound() throws Exception {
        final Long id = DUME_RESPONSE_DTO_IDENTIFIER_1;
        when(dumeResponseService.validateDumeOE(id)).thenThrow(new DumeResourceNotFoundException("DumeResponse", id));

        mockMvc.perform(get(API_DUME_RESPONSE_VALIDATE_FULL_PATH + id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(dumeResponseService, times(1)).validateDumeOE(id);
        verifyNoMoreInteractions(dumeResponseService);
    }

    @Test
    public void validateDumeResponseExecption() throws Exception {
        final Long id = DUME_RESPONSE_DTO_IDENTIFIER_1;
        when(dumeResponseService.validateDumeOE(id)).thenThrow(new IllegalArgumentException());

        mockMvc.perform(get(API_DUME_RESPONSE_VALIDATE_FULL_PATH + id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isInternalServerError());

        verify(dumeResponseService, times(1)).validateDumeOE(id);
        verifyNoMoreInteractions(dumeResponseService);
    }

    @Test
    public void publishOK() throws Exception {
        final RetourStatus retourStatus = new RetourStatus(StatusDTO.OK, "");
        final Long id = DUME_RESPONSE_DTO_IDENTIFIER_1;
        when(dumeResponseService.publishDumeOE(id)).thenReturn(retourStatus);

        mockMvc.perform(get(API_DUME_RESPONSE_PUBLICATION_FULL_PATH + id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_STATUS, is(StatusDTO.OK.name())));

        verify(dumeResponseService, times(1)).publishDumeOE(id);
        verifyNoMoreInteractions(dumeResponseService);

    }

    @Test
    public void publishNotFound() throws Exception {
        final Long id = DUME_RESPONSE_DTO_IDENTIFIER_1;
        when(dumeResponseService.publishDumeOE(id)).thenThrow(new DumeResourceNotFoundException("DumeRespones", id));

        mockMvc.perform(get(API_DUME_RESPONSE_PUBLICATION_FULL_PATH + id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(dumeResponseService, times(1)).publishDumeOE(id);
        verifyNoMoreInteractions(dumeResponseService);
    }

    @Test
    public void publishException() throws Exception {
        final Long id = DUME_RESPONSE_DTO_IDENTIFIER_1;
        when(dumeResponseService.publishDumeOE(id)).thenThrow(new IllegalArgumentException());

        mockMvc.perform(get(API_DUME_RESPONSE_PUBLICATION_FULL_PATH + id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isInternalServerError());

        verify(dumeResponseService, times(1)).publishDumeOE(id);
        verifyNoMoreInteractions(dumeResponseService);
    }

    @Test
    public void publishGroupementOK() throws Exception {
        final RetourStatus retourStatus = new RetourStatus(StatusDTO.OK, "");
        PublicationGroupementDTO publicationGroupementDTO = PublicationGroupementDTODataSet.dummyPublicationGroupementDTO();
        when(dumeResponseService.publishDumeOEGroupement(publicationGroupementDTO)).thenReturn(retourStatus);

        mockMvc.perform(post(API_DUME_RESPONSE_PUBLICATION_GROUPEMENT_FULL_PATH)
                .content(asJsonString(publicationGroupementDTO))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_STATUS, is(StatusDTO.OK.name())));

        verify(dumeResponseService, times(1)).publishDumeOEGroupement(publicationGroupementDTO);
        verifyNoMoreInteractions(dumeResponseService);

    }

    @Test
    public void publishGroupementNotFound() throws Exception {
        PublicationGroupementDTO publicationGroupementDTO = PublicationGroupementDTODataSet.dummyPublicationGroupementDTO();
        when(dumeResponseService.publishDumeOEGroupement(publicationGroupementDTO)).thenThrow(new DumeResourceNotFoundException("DumeResponse", ""));

        mockMvc.perform(post(API_DUME_RESPONSE_PUBLICATION_GROUPEMENT_FULL_PATH)
                .content(asJsonString(publicationGroupementDTO))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(dumeResponseService, times(1)).publishDumeOEGroupement(publicationGroupementDTO);
        verifyNoMoreInteractions(dumeResponseService);
    }

    @Test
    public void publishGroupementException() throws Exception {
        PublicationGroupementDTO publicationGroupementDTO = PublicationGroupementDTODataSet.dummyPublicationGroupementDTO();
        doThrow(new ServiceException("TEST")).when(dumeResponseService).publishDumeOEGroupement(any());

        mockMvc.perform(post(API_DUME_RESPONSE_PUBLICATION_GROUPEMENT_FULL_PATH)
                .content(asJsonString(publicationGroupementDTO))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isInternalServerError());

        verify(dumeResponseService, times(1)).publishDumeOEGroupement(any());
        verifyNoMoreInteractions(dumeResponseService);
    }

    @Test
    public void getAllPublishedDumeResponseForContextOk() throws Exception {
        final Long id = DUME_RESPONSE_DTO_IDENTIFIER_1;
        PublicationStatus publicationStatus = new PublicationStatus();
        publicationStatus.setStatut(StatusDTO.OK);
        when(dumeResponseService.recupererStatusPublicationDumeOe(id)).thenReturn(publicationStatus);

        mockMvc.perform(get(API_DUME_RESPONSE_PUBLICATION_LIST_FULL_PATH + id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_STATUS, is(StatusDTO.OK.name())));
        verify(dumeResponseService, times(1)).recupererStatusPublicationDumeOe(id);
        verifyNoMoreInteractions(dumeResponseService);
    }

    @Test
    public void getAllPublishedDumeResponseForContextNotFound() throws Exception {
        final Long id = DUME_RESPONSE_DTO_IDENTIFIER_1;

        when(dumeResponseService.recupererStatusPublicationDumeOe(id)).thenThrow(new DumeResourceNotFoundException("DumeRespone", id));

        mockMvc.perform(get(API_DUME_RESPONSE_PUBLICATION_LIST_FULL_PATH + id)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound());
        verify(dumeResponseService, times(1)).recupererStatusPublicationDumeOe(id);
        verifyNoMoreInteractions(dumeResponseService);

    }


    @Test
    public void saveAndDownloadPDFOk() throws Exception {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithId();
        doReturn(new byte[]{0}).when(dumeResponseService).downloadPDFAfterAsaveDumeOE(any());

        mockMvc.perform(post(API_DUME_RESPONSE_SAVE_AND_DOWLOAD_PDF_FULL_PATH)
                .content(asJsonString(dumeResponseDTO))
                .contentType(APPLICATION_JSON))
                .andExpect(header().string(CONTENT_TYPE, "application/pdf"))
                .andExpect(header().string(CONTENT_DISPOSITION, "attachment; filename=" + DUME_RESPONSE_DTO_IDENTIFIER_1 + ".pdf"))
                .andExpect(status().isOk());

        verify(dumeResponseService, times(1)).downloadPDFAfterAsaveDumeOE(any());
        verifyNoMoreInteractions(dumeResponseService);
    }

    @Test
    public void saveAndDownloadPDFNotFound() throws Exception {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithId();
        when(dumeResponseService.downloadPDFAfterAsaveDumeOE(any())).thenThrow(new DumeResourceNotFoundException("DumeResponse", dumeResponseDTO.getId()));

        mockMvc.perform(post(API_DUME_RESPONSE_SAVE_AND_DOWLOAD_PDF_FULL_PATH)
                .content(asJsonString(dumeResponseDTO))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(dumeResponseService, times(1)).downloadPDFAfterAsaveDumeOE(any());
        verifyNoMoreInteractions(dumeResponseService);
    }

    @Test
    public void saveAndDownloadPDFException() throws Exception {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithId();
        doThrow(new ServiceException("TEST")).when(dumeResponseService).downloadPDFAfterAsaveDumeOE(any());

        mockMvc.perform(post(API_DUME_RESPONSE_SAVE_AND_DOWLOAD_PDF_FULL_PATH)
                .content(asJsonString(dumeResponseDTO))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isInternalServerError());

        verify(dumeResponseService, times(1)).downloadPDFAfterAsaveDumeOE(any());
        verifyNoMoreInteractions(dumeResponseService);
    }

    @Test
    public void updateContextByIdentifierOk() throws Exception {
        final DumeRequestContextDTO dumeRequestContextDTO = DumeRequestContextDataDTOSet.contextSample();
        doReturn(dumeRequestContextDTO.getId()).when(dumeResponseService).updateMetadonneesAndLot(any(), anyLong());

        mockMvc.perform(post(API_DUME_RESPONSE_UPDATE_METADONNE_FULL_PATH + 12L)
                .content(asJsonString(dumeRequestContextDTO))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_ROOT, is(dumeRequestContextDTO.getId())));
        verify(dumeResponseService, times(1)).updateMetadonneesAndLot(any(), anyLong());
        verifyNoMoreInteractions(dumeResponseService);
    }

    @Test
    public void updateContextByIdentifierException() throws Exception {
        final DumeRequestContextDTO dumeRequestContextDTO = DumeRequestContextDataDTOSet.contextSample();
        doThrow(new DumeResourceNotFoundException("DumeResponse", 12L)).when(dumeResponseService).updateMetadonneesAndLot(any(), anyLong());

        mockMvc.perform(post(API_DUME_RESPONSE_UPDATE_METADONNE_FULL_PATH + 12L)
                .content(asJsonString(dumeRequestContextDTO))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound());
        verify(dumeResponseService, times(1)).updateMetadonneesAndLot(any(), anyLong());
        verifyNoMoreInteractions(dumeResponseService);
    }

    @Test
    public void findBySNNumberOK() throws Exception {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithId();
        doReturn(new ArrayList<DumeResponseDTO>()).when(dumeResponseService).findDumeOEFromSN(any(), any());

        mockMvc.perform(get(API_DUME_RESPONSE_FIN_BY_SN_NUMBER_FULL_PATH + dumeResponseDTO.getId() + "/" + dumeResponseDTO.getNumeroSN())
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk());
        verify(dumeResponseService, times(1)).findDumeOEFromSN(any(), any());
        verifyNoMoreInteractions(dumeResponseService);
    }

    @Test
    public void findBySNNumberKO() throws Exception {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithId();
        when(dumeResponseService.findDumeOEFromSN(any(), any())).thenThrow(new ServiceException());
        mockMvc.perform(get(API_DUME_RESPONSE_FIN_BY_SN_NUMBER_FULL_PATH + dumeResponseDTO.getId() + "/" + dumeResponseDTO.getNumeroSN()).contentType(APPLICATION_JSON)).andExpect(status().isInternalServerError());
        verify(dumeResponseService, times(1)).findDumeOEFromSN(any(), any());
        verifyNoMoreInteractions(dumeResponseService);
    }

    @Test
    public void createDumeResponseFromSNNumberOk() throws Exception {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithIdAndSNNumber();
        when(dumeResponseService.createDumeResponseFromExisitingSNNumber(dumeResponseDTO.getId(), dumeResponseDTO.getNumeroSN())).thenReturn(dumeResponseDTO);

        mockMvc.perform(post(API_DUME_RESPONSE_CREATE_FULL_PATH + dumeResponseDTO.getId() + "/" + dumeResponseDTO.getNumeroSN()).content(asJsonString(dumeResponseDTO)).contentType(APPLICATION_JSON)).andExpect(status().isOk());
        verify(dumeResponseService, times(1)).createDumeResponseFromExisitingSNNumber(any(), any());
        verifyNoMoreInteractions(dumeResponseService);

    }

    @Test
    public void createDumeResponseFromSNNumberNotFound() throws Exception {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithIdAndSNNumber();
        when(dumeResponseService.createDumeResponseFromExisitingSNNumber(dumeResponseDTO.getId(), dumeResponseDTO.getNumeroSN()))
                .thenThrow(new DumeResourceNotFoundException("DumeResponse", "test"));

        mockMvc.perform(post(API_DUME_RESPONSE_CREATE_FULL_PATH + dumeResponseDTO.getId() + "/" + dumeResponseDTO.getNumeroSN())
                .content(asJsonString(dumeResponseDTO))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound());
        verify(dumeResponseService, times(1))
                .createDumeResponseFromExisitingSNNumber(dumeResponseDTO.getId(), dumeResponseDTO.getNumeroSN());
        verifyNoMoreInteractions(dumeResponseService);

    }

    @Test
    public void createDumeResponseFromSNNUumberException() throws Exception {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithIdAndSNNumber();
        when(dumeResponseService.createDumeResponseFromExisitingSNNumber(dumeResponseDTO.getId(), dumeResponseDTO.getNumeroSN())).thenThrow(new ServiceException());
        mockMvc.perform(post(API_DUME_RESPONSE_CREATE_FULL_PATH + dumeResponseDTO.getId() + "/" + dumeResponseDTO.getNumeroSN()).content(asJsonString(dumeResponseDTO)).contentType(APPLICATION_JSON)).andExpect(status().isInternalServerError());
        verify(dumeResponseService, times(1)).createDumeResponseFromExisitingSNNumber(any(), any());
        verifyNoMoreInteractions(dumeResponseService);
    }


    @Test
    public void downloadXMLOk() throws Exception {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithIdAndSNNumber();
        doReturn(new byte[]{0}).when(dumeResponseService).generateDumeResponseXML(anyString());

        mockMvc.perform(get(API_DUME_RESPONSE_DOWNLOAD_XML_FULL_PATH + dumeResponseDTO.getNumeroSN())).andExpect(header().string(CONTENT_TYPE, "application/xml")).andExpect(header().string(CONTENT_DISPOSITION, "attachment; filename=" + dumeResponseDTO.getNumeroSN() + ".xml")).andExpect(status().isOk());

        verify(dumeResponseService, times(1)).generateDumeResponseXML(anyString());
        verifyNoMoreInteractions(dumeResponseService);
    }

    @Test
    public void downloadXMLOkNotFound() throws Exception {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithIdAndSNNumber();
        when(dumeResponseService.generateDumeResponseXML(anyString())).thenThrow(new DumeResourceNotFoundException("DumeResponse", dumeResponseDTO.getId()));

        mockMvc.perform(get(API_DUME_RESPONSE_DOWNLOAD_XML_FULL_PATH + dumeResponseDTO.getNumeroSN())).andExpect(status().isNotFound());

        verify(dumeResponseService, times(1)).generateDumeResponseXML(anyString());
        verifyNoMoreInteractions(dumeResponseService);
    }

    @Test
    public void downloadXMLException() throws Exception {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithIdAndSNNumber();
        doThrow(new ServiceException("TEST")).when(dumeResponseService).generateDumeResponseXML(anyString());

        mockMvc.perform(get(API_DUME_RESPONSE_DOWNLOAD_XML_FULL_PATH + dumeResponseDTO.getNumeroSN())).andExpect(status().isInternalServerError());
        verify(dumeResponseService, times(1)).generateDumeResponseXML(anyString());
        verifyNoMoreInteractions(dumeResponseService);
    }

    @Test
    public void downloadXMLFrontOk() throws Exception {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithIdAndSNNumber();
        doReturn(new byte[]{0}).when(dumeResponseService).generateDumeResponseXMLById(anyLong());

        mockMvc.perform(get(API_DUME_RESPONSE_DOWNLOAD_XML_FRONT_FULL_PATH + dumeResponseDTO.getId())).andExpect(header().string(CONTENT_TYPE, "application/xml")).andExpect(header().string(CONTENT_DISPOSITION, "attachment; filename=dumeOE.xml")).andExpect(status().isOk());

        verify(dumeResponseService, times(1)).generateDumeResponseXMLById(anyLong());
        verifyNoMoreInteractions(dumeResponseService);
    }

    @Test
    public void downloadXMLFrontNotFound() throws Exception {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithIdAndSNNumber();
        when(dumeResponseService.generateDumeResponseXMLById(anyLong())).thenThrow(new DumeResourceNotFoundException("DumeResponse", dumeResponseDTO.getId()));

        mockMvc.perform(get(API_DUME_RESPONSE_DOWNLOAD_XML_FRONT_FULL_PATH + dumeResponseDTO.getId())).andExpect(status().isNotFound());

        verify(dumeResponseService, times(1)).generateDumeResponseXMLById(anyLong());
        verifyNoMoreInteractions(dumeResponseService);
    }

    @Test
    public void downloadXMLFrontException() throws Exception {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithIdAndSNNumber();
        doThrow(new ServiceException("TEST")).when(dumeResponseService).generateDumeResponseXMLById(anyLong());

        mockMvc.perform(get(API_DUME_RESPONSE_DOWNLOAD_XML_FRONT_FULL_PATH + dumeResponseDTO.getId())).andExpect(status().isInternalServerError());
        verify(dumeResponseService, times(1)).generateDumeResponseXMLById(anyLong());
        verifyNoMoreInteractions(dumeResponseService);
    }

    @Test
    public void getECertisProofOk() throws Exception {
        final Long id = DUME_RESPONSE_DTO_IDENTIFIER_1;
        SelectionCriteriaDTO criteria = SelectionCriteriaDTODataSet.sample1WithUuid();
        String countryCode = "fr";
        when(dumeResponseService.getECertisProof(id, criteria.getUuid(), countryCode)).thenReturn(new ArrayList<>());

        mockMvc.perform(get(API_DUME_GET_E_CERTIS_PROOF_FULL_PATH + id + "/" + criteria.getUuid() + "/" + countryCode).contentType(APPLICATION_JSON)).andExpect(status().isOk());

        verify(dumeResponseService, times(1)).getECertisProof(id, criteria.getUuid(), countryCode);
        verifyNoMoreInteractions(dumeResponseService);

    }

    @Test
    public void getECertisProofNotFound() throws Exception {
        final Long id = DUME_RESPONSE_DTO_IDENTIFIER_1;
        SelectionCriteriaDTO criteria = SelectionCriteriaDTODataSet.sample1WithUuid();
        String countryCode = "fr";
        when(dumeResponseService.getECertisProof(id, criteria.getUuid(), countryCode)).thenThrow(new DumeResourceNotFoundException("Dume OE", id));

        mockMvc.perform(get(API_DUME_GET_E_CERTIS_PROOF_FULL_PATH + id + "/" + criteria.getUuid() + "/" + countryCode).contentType(APPLICATION_JSON))
               .andExpect(status().isNotFound());

        verify(dumeResponseService, times(1)).getECertisProof(id, criteria.getUuid(), countryCode);
        verifyNoMoreInteractions(dumeResponseService);

    }

    @Test
    public void getECertisProofException() throws Exception {
        final Long id = DUME_RESPONSE_DTO_IDENTIFIER_1;
        SelectionCriteriaDTO criteria = SelectionCriteriaDTODataSet.sample1WithUuid();
        String countryCode = "fr";
        when(dumeResponseService.getECertisProof(id, criteria.getUuid(), countryCode)).thenThrow(new IllegalArgumentException(USER_GET_ALL_EX_MSG));

        mockMvc.perform(get(API_DUME_GET_E_CERTIS_PROOF_FULL_PATH + id + "/" + criteria.getUuid() + "/" + countryCode).contentType(APPLICATION_JSON))
               .andExpect(status().isInternalServerError());

        verify(dumeResponseService, times(1)).getECertisProof(id, criteria.getUuid(), countryCode);
        verifyNoMoreInteractions(dumeResponseService);

    }




}
