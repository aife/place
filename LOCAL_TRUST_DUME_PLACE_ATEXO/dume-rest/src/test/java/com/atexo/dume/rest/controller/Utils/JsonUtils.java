package com.atexo.dume.rest.controller.Utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtils {

    /**
     * Utility method to convert an object to it's Json representation
     *
     * @param t   The object to be converted
     * @param <T> The type of the Object
     * @return String representation of the Object
     * @throws JsonProcessingException if something goes wrong.
     */
    public static <T> String asJsonString(final T t) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(t);

    }
}
