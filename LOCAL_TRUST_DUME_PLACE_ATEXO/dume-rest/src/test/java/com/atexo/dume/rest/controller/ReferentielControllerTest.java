package com.atexo.dume.rest.controller;

import com.atexo.dume.dto.EnumDTO;
import com.atexo.dume.model.Country;
import com.atexo.dume.model.Currency;
import com.atexo.dume.model.MarketType;
import com.atexo.dume.services.api.ReferentielService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static com.atexo.dume.rest.controller.Utils.Constants.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ReferentielControllerTest {

    @InjectMocks
    private ReferentielController referentielController;

    @Mock
    private ReferentielService referentielService;

    private MockMvc mockMvc;

    private static List<EnumDTO> currencyDTOS = Arrays.asList(new EnumDTO(Currency.EUR.name(), Currency.EUR.getDescription()));
    private static List<EnumDTO> paysDTOS = Arrays.asList(new EnumDTO(Country.MA.getIso2Code(), Country.MA.getCountryName()));
    private static List<EnumDTO> marketTypesDTOS = Arrays.asList(new EnumDTO(MarketType.Travaux.getCode(), MarketType.Travaux.name()));

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(referentielController)
                .setControllerAdvice(new ControllerExceptionHandler())
                .build();
    }

    @Test
    public void getPaysOK() throws Exception {
        when(referentielService.getCountryEnum()).thenReturn(paysDTOS);

        mockMvc.perform(get(API_DUME_REFERENTIEL_PAYS_PATH))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath(JSON_ROOT, hasSize(1)));

        verify(referentielService, times(1)).getCountryEnum();
        verifyNoMoreInteractions(referentielService);

    }

    @Test
    public void getPaysWithException() throws Exception {
        when(referentielService.getCountryEnum()).thenThrow(new IllegalArgumentException());

        mockMvc.perform(get(API_DUME_REFERENTIEL_PAYS_PATH))
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void getDevisesOK() throws Exception {
        when(referentielService.getCurrencyEnum()).thenReturn(currencyDTOS);

        mockMvc.perform(get(API_DUME_REFERENTIEL_DEVISES_PATH))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath(JSON_ROOT, hasSize(1)));
        verify(referentielService, times(1)).getCurrencyEnum();
        verifyNoMoreInteractions(referentielService);
    }

    @Test
    public void getDevisesWithException() throws Exception {
        when(referentielService.getCurrencyEnum()).thenThrow(new IllegalArgumentException());

        mockMvc.perform(get(API_DUME_REFERENTIEL_DEVISES_PATH))
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void getMarketTypesOK() throws Exception {
        when(referentielService.getMarketEnum()).thenReturn(marketTypesDTOS);

        mockMvc.perform(get(API_DUME_REFERENTIEL_TYPES_MARCHE_PATH))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath(JSON_ROOT, hasSize(1)));
        verify(referentielService, times(1)).getMarketEnum();
        verifyNoMoreInteractions(referentielService);
    }

    @Test
    public void getMarketTypesWithException() throws Exception {
        when(referentielService.getMarketEnum()).thenThrow(new IllegalArgumentException());

        mockMvc.perform(get(API_DUME_REFERENTIEL_TYPES_MARCHE_PATH))
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void updateCriteriaOK() throws Exception {
        MockMultipartFile criteria = new MockMultipartFile("criteria", "filename.txt", "text/plain", "some xml".getBytes());
        when(referentielService.updateDescriptionCriteria(any())).thenReturn(Boolean.TRUE);

        mockMvc.perform(MockMvcRequestBuilders.multipart(API_DUME_REFERENTIEL_CRITERIA_PATH)
                .file(criteria)).andExpect(status().isOk())
                .andExpect(jsonPath("$", is(Boolean.TRUE)));
    }

    @Test
    public void updateRequirement() throws Exception {
        MockMultipartFile criteria = new MockMultipartFile("requirement", "filename.txt", "text/plain", "some xml".getBytes());
        when(referentielService.updateDescriptionRequirement(any())).thenReturn(Boolean.TRUE);

        mockMvc.perform(MockMvcRequestBuilders.multipart(API_DUME_REFERENTIEL_REQUIREMNT_PATH)
                .file(criteria)).andExpect(status().isOk())
                .andExpect(jsonPath("$", is(Boolean.TRUE)));
    }

}
