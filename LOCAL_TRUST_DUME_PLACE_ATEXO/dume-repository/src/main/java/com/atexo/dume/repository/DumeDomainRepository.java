package com.atexo.dume.repository;

import com.atexo.dume.model.DumeDomain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DumeDomainRepository extends JpaRepository<DumeDomain, Long>, QuerydslPredicateExecutor<DumeDomain> {

    Optional<DumeDomain> findByDomLabel(String label);
}
