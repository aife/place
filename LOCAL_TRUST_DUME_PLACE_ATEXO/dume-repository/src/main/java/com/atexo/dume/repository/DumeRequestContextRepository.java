package com.atexo.dume.repository;

import com.atexo.dume.model.DumePlatform;
import com.atexo.dume.model.DumeRequestContext;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.Optional;

/**
 * this interface define a Repository for the {@link DumeRequestContext}.
 * it add Query dsl capabilities.
 *
 * @author ama
 */
public interface DumeRequestContextRepository extends JpaRepository<DumeRequestContext, Long>, QuerydslPredicateExecutor<DumeRequestContext> {


    Optional<DumeRequestContext> findByMetadataConsultationIdentifierAndPlatform(Long identifier, DumePlatform platform);
}
