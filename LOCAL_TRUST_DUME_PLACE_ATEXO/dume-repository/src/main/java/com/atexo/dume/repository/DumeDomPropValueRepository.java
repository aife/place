package com.atexo.dume.repository;

import com.atexo.dume.model.profile.DumeDomainPropValue;
import com.atexo.dume.model.profile.pk.DumeDomPropValuePK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DumeDomPropValueRepository extends JpaRepository<DumeDomainPropValue, DumeDomPropValuePK>, QuerydslPredicateExecutor<DumeDomainPropValue> {
    
}
