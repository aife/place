package com.atexo.dume.repository;

import com.atexo.dume.model.OtherCriteria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CriteriaOtherRepository extends JpaRepository<OtherCriteria, Long>, QuerydslPredicateExecutor<OtherCriteria> {

    OtherCriteria findByCode(String code);
}
