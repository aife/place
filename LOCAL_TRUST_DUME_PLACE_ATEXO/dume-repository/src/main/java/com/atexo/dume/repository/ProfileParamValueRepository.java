package com.atexo.dume.repository;

import com.atexo.dume.model.profile.DumeProfileProp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileParamValueRepository  extends JpaRepository<DumeProfileProp, Long>, QuerydslPredicateExecutor<DumeProfileProp> {

    DumeProfileProp findByPropLabel(final String label);

}
