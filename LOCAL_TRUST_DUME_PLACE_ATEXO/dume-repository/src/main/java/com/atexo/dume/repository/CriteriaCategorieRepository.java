package com.atexo.dume.repository;

import com.atexo.dume.model.CategorieType;
import com.atexo.dume.model.CriteriaCategorie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

/**
 * this interface define a Repository for the {@link CriteriaCategorie}.
 * it add Query dsl capabilities.
 */
public interface CriteriaCategorieRepository extends JpaRepository<CriteriaCategorie, Long>,
        QuerydslPredicateExecutor<CriteriaCategorie> {

    List<CriteriaCategorie> findByType(CategorieType type);

}
