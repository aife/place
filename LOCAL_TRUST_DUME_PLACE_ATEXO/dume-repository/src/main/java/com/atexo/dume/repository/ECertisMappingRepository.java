package com.atexo.dume.repository;

import com.atexo.dume.model.ECertisMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * this interface define a Repository for the {@link com.atexo.dume.model.ECertisMapping}.
 * it add Query dsl capabilities.
 */
@Repository
public interface ECertisMappingRepository extends JpaRepository<ECertisMapping, Long> {

    Optional<ECertisMapping> findByCountryCode(String countryCode);
}
