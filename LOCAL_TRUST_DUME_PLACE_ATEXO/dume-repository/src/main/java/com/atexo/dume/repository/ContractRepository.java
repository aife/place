package com.atexo.dume.repository;

import com.atexo.dume.model.donneesessentielles.Contract;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.Optional;

public interface ContractRepository extends JpaRepository<Contract, Long>, QuerydslPredicateExecutor<Contract> {
    Optional<Contract> getContractByContractNumber(String contractNumber);

    Optional<Contract> getContractByUuid(String uuid);

    Optional<Contract> getContractByContractNumberAndContractOriginUuid(String contractNumber, String uuid);
}
