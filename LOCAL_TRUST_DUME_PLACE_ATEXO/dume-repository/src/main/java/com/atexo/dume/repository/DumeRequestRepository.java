package com.atexo.dume.repository;

import com.atexo.dume.model.DumeRequest;
import com.atexo.dume.model.EtatDume;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * this interface define a Repository for the {@link DumeRequest}.
 * it add Query dsl capabilities.
 *
 * @author ASH
 */
@Repository
public interface DumeRequestRepository extends JpaRepository<DumeRequest, Long>, QuerydslPredicateExecutor<DumeRequest> {

    /**
     * get all {@link DumeRequest} corrsponding to a given {@link EtatDume}
     *
     * @param etat {@link EtatDume}
     * @return
     */
    List<DumeRequest> findAllByEtatDume(EtatDume etat);

    /**
     * @param numeroSN numéro du service national
     * @return retrieve a {@link DumeRequest} related to a numeroSN
     */
    Optional<DumeRequest> findByNumDumeSN(String numeroSN);

    /**
     * @param etat         {@link EtatDume}
     * @param pdfRetrieved indicates whether PDF is retrieved or not
     * @return retrieve a list of {@link DumeRequest} related to a numeroSN and PDF state
     */
    List<DumeRequest> findAllByEtatDumeAndNumDumeSNIsNotNullAndPdfRetrieved(EtatDume etat, Boolean pdfRetrieved);

    /**
     * @param etat           {@link EtatDume}
     * @param pdfRetrieved   indicates whether PDF is retrieved or not
     * @param xmlOERetrieved indicates whether XML OE  is retrieved or not
     * @return retrieve a list of {@link DumeRequest} related to a numeroSN and PDF state and xmlOEretrieved
     */
    List<DumeRequest> findAllByEtatDumeAndNumDumeSNIsNotNullAndPdfRetrievedAndXmlOERetrieved(EtatDume etat, Boolean pdfRetrieved, Boolean xmlOERetrieved);
}
