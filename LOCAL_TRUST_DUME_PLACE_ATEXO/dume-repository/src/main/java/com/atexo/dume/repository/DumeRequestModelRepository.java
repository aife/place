package com.atexo.dume.repository;

import com.atexo.dume.model.DumeRequestModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface DumeRequestModelRepository extends JpaRepository<DumeRequestModel, Long>, QuerydslPredicateExecutor<DumeRequestModel> {
}
