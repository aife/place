package com.atexo.dume.repository;

import com.atexo.dume.model.Criteria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.Optional;


public interface CriteriaRepository extends JpaRepository<Criteria, Long>, QuerydslPredicateExecutor<Criteria> {

    Criteria findByCode(String code);

    Optional<Criteria> findByUuid(String uuid);
}
