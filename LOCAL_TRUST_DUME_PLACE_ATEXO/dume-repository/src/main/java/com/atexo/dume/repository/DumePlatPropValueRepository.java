package com.atexo.dume.repository;

import com.atexo.dume.model.profile.DumePlatPropValue;
import com.atexo.dume.model.profile.pk.DumePlatPropValuePK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DumePlatPropValueRepository extends JpaRepository<DumePlatPropValue, DumePlatPropValuePK>, QuerydslPredicateExecutor<DumePlatPropValue> {

}
