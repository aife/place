package com.atexo.dume.repository;

import com.atexo.dume.model.SelectionCriteria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * this interface define a Repository for the {@link SelectionCriteria}.
 * it add Query dsl capabilities.
 *
 * @author ASH
 */
@Repository
public interface CriteriaSelectionRepository extends JpaRepository<SelectionCriteria, Long>, QuerydslPredicateExecutor<SelectionCriteria> {

    List<SelectionCriteria> findByCodeIn(List<String> codes);

    SelectionCriteria findByCode(String code);

    List<SelectionCriteria> findAllByCodeNotLike(String code);

    List<SelectionCriteria> findAllByNatureMarketIdentifierIsNullOrNatureMarketIdentifierEquals(String idNatureMarket);

}
