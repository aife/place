package com.atexo.dume.repository;

import com.atexo.dume.model.CriteriaSelectionDocument;
import com.atexo.dume.model.DumeDocument;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CriteriaSelectionDocumentRepository extends JpaRepository<CriteriaSelectionDocument, Long> {

    Optional<CriteriaSelectionDocument> findByCriteriaCodeAndDumeDocumentId(String code, Long id);

    List<CriteriaSelectionDocument> findByDumeDocument(DumeDocument dumeDocument);
}
