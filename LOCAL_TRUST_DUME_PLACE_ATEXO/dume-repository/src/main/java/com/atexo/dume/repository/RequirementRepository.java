package com.atexo.dume.repository;

import com.atexo.dume.model.Requirement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface RequirementRepository extends JpaRepository<Requirement, Long>, QuerydslPredicateExecutor<Requirement> {

    List<Requirement> findByUuidAndResponseTypeIsNotIn(String uuid, List<String> response);
}
