package com.atexo.dume.repository;

import com.atexo.dume.model.DumeResponse;
import com.atexo.dume.model.EtatDume;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * This interface defines a Repository for the {@link DumeResponse}.
 * it add Query dsl capabilities.
 */
@Repository
public interface DumeResponseRepository extends JpaRepository<DumeResponse, Long>, QuerydslPredicateExecutor<DumeResponse> {
    /**
     * @param numeroSN numéro du service national
     * @return retrieve a {@link DumeResponse} related to a numeroSN
     */
    Optional<DumeResponse> findByNumeroSN(String numeroSN);

    List<DumeResponse> findAllByEtatDume(EtatDume etatDume);

    List<DumeResponse> findAllByEtatDumeAndNumeroSNIsNotNullAndPdfRetreived(EtatDume etat, Boolean pdfRetrieved);

    List<DumeResponse> findAllByDumeRequestIdAndEtatDume(Long idDumeRequest, EtatDume etatDume);
}
