package com.atexo.dume.repository;

import com.atexo.dume.model.ExclusionCriteria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * this interface define a Repository for the {@link ExclusionCriteria}.
 * it add Query dsl capabilities.
 *
 * @author ASH
 */
@Repository
public interface CriteriaExclusionRepository extends JpaRepository<ExclusionCriteria, Long>, QuerydslPredicateExecutor<ExclusionCriteria> {

    List<ExclusionCriteria> findByCodeIn(List<String> codes);

    ExclusionCriteria findByCode(String code);
}
