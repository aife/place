package com.atexo.dume.repository;

import com.atexo.dume.model.DumePlatform;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DumePlatformRepository extends JpaRepository<DumePlatform, Long>, QuerydslPredicateExecutor<DumePlatform> {

    /**
     * @param platformIdentifier the platfor identifier already registred in the LT Dume Platform , this identifier should be unique
     * @return a single {@link DumePlatform} corresponding to this ID platform
     */
    Optional<DumePlatform> findByPlatformIdentifier(String platformIdentifier);

    /**
     * @param enabled
     * @return a list of {@link DumePlatform} that enables essential data
     */
    List<DumePlatform> findByEnableEssentialData(boolean enabled);
}
