package com.atexo.dume.repository;

import com.atexo.dume.model.profile.DumeUsrPropValue;
import com.atexo.dume.model.profile.pk.DumeUsrPropValuePK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DumeUsrPropValueRepository extends JpaRepository<DumeUsrPropValue, DumeUsrPropValuePK>, QuerydslPredicateExecutor<DumeUsrPropValue> {

}
