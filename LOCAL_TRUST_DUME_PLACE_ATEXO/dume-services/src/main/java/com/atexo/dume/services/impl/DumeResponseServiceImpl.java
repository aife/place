package com.atexo.dume.services.impl;

import com.atexo.dume.dto.*;
import com.atexo.dume.dto.sn.request.SNECertisDTO;
import com.atexo.dume.dto.sn.request.SNRequestDTO;
import com.atexo.dume.dto.sn.response.*;
import com.atexo.dume.mapper.*;
import com.atexo.dume.mapper.sn.SNRequestMapper;
import com.atexo.dume.model.*;
import com.atexo.dume.repository.*;
import com.atexo.dume.services.api.*;
import com.atexo.dume.services.exception.DumeResourceNotFoundException;
import com.atexo.dume.services.exception.NationalServiceException;
import com.atexo.dume.services.exception.ServiceException;
import grow.names.specification.ubl.schema.xsd.espdresponse_1.ESPDResponseType;
import isa.names.specification.ubl.schema.xsd.ccv_commonaggregatecomponents_1.CriterionType;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.oxm.XmlMappingException;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import javax.xml.bind.JAXBElement;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.atexo.dume.dto.StatusDTO.*;
import static com.atexo.dume.toolbox.consts.DumeStatusEnum.DRAFT_COPY;
import static com.atexo.dume.toolbox.logging.LoggerUtils.*;
import static com.atexo.dume.toolbox.rest.NationalServiceOperations.MERGE_DUME;
import static java.util.Arrays.asList;

/**
 * Implémente les opérations de {@link DumeResponseService}
 */
@Service
public class DumeResponseServiceImpl implements DumeResponseService {

    private static final Logger logger = LoggerFactory.getLogger(DumeResponseServiceImpl.class);
    public static final String DUME_RESPONSE = "DumeResponse";

    @Autowired
    private CriteriaSelectionDocumentRepository criteriaSelectionDocumentRepository;

    private DumeResponseRepository dumeResponseRepository;

    @Autowired
    private DumeResponseMapper dumeResponseMapper;

    @Autowired
    private Jaxb2Marshaller jaxb2Marshaller;

    @Autowired
    private CriteriaService criteriaService;

    @Autowired
    private DumeRequestContextRepository dumeRequestContextRepository;

    @Autowired
    private ECertisMappingRepository eCertisMappingRepository;

    @Autowired
    private CriteriaRepository criteriaRepository;

    @Autowired
    private CriteriaMapper criteriaMapper;

    @Autowired
    private ECertisMapper eCertisMapper;

    @Autowired
    private EconomicPartyInfoMapper economicPartyInfoMapper;

    @Autowired
    private RepresentantLegalMapper representantLegalMapper;

    @Autowired
    private XMLDumeService xmlDumeService;


    private SNRequestMapper snRequestMapper;

    @Autowired
    private SchedularTask schedularTask;


    @Value("${PDF_STORAGE_FOLDER}")
    private String pdfStorageFolder;

    private NationalServiceRequestService nationalServiceRequestService;

    @Autowired
    private DumeRequestContextService dumeRequestContextService;

    // A hack to use it in Unit-Testing
    @Autowired
    public void init(NationalServiceRequestService nationalServiceRequestService, SNRequestMapper snRequestMapper, DumeResponseRepository dumeResponseRepository) {
        this.nationalServiceRequestService = nationalServiceRequestService;
        this.snRequestMapper = snRequestMapper;
        this.dumeResponseRepository = dumeResponseRepository;
    }

    @Override
    public DumeResponseDTO getDumeResponseDTOById(Long id) throws DumeResourceNotFoundException {
        Optional<DumeResponse> dumeResponse = dumeResponseRepository.findById(id);
        if (dumeResponse.isPresent()) {
            return getDumeResponseDTO(dumeResponse.get());
        }
        throw new DumeResourceNotFoundException(DUME_RESPONSE, id);
    }


    /**
     * Permet de mettre à jour les réponses d'un dume response
     *
     * @param dumeResponseDTO le dume response contenant les réponses à mettre à jour
     * @return le dume response mis à jour
     */
    @Override
    public DumeResponseDTO updateDumeResponseByDTO(DumeResponseDTO dumeResponseDTO) throws DumeResourceNotFoundException {

        Optional<DumeResponse> dumeResponseOptional = dumeResponseRepository.findById(dumeResponseDTO.getId());
        if (dumeResponseOptional.isPresent()) {
            DumeResponse dumeResponse = dumeResponseOptional.get();
            dumeResponseMapper.updateDumeReponseUsingDTO(dumeResponse, dumeResponseDTO);
            dumeResponseRepository.save(dumeResponse);
            return dumeResponseDTO;
        }
        throw new DumeResourceNotFoundException(DUME_RESPONSE, dumeResponseDTO.getId());
    }


    @Override
    public DumeResponseRetourCreationDTO createDumeResponseBy(DumeRequestContextDTO dumeRequestContextDTO) throws IOException, ServiceException, DumeResourceNotFoundException {

        Long id = Long.valueOf(dumeRequestContextDTO.getMetadata().getIdContextDumeAcheteur());
        Optional<DumeRequestContext> optionalContext = dumeRequestContextRepository.findById(id);

        if (!optionalContext.isPresent()) {
            throw new DumeResourceNotFoundException("DumeRequestContext", id);
        }

        // Construction de la requête au SN

        DumeRequest dumeRequest = (DumeRequest) optionalContext.get().getDumeDocuments().iterator().next();
        populateLog(dumeRequest.getContext().getConsultRef());
        logger.info("Création d'un dume OE à partir du contexte : {}", dumeRequestContextDTO.getId());
        // Ici on fait un filtre pour la gestion des entreprise étrangére
        // Si "1" (entreprise Française) nous envoyons le siret
        // sinon nous envoyons rien
        String siretOE = "1".equals(dumeRequestContextDTO.getMetadata().getTypeOE()) ? dumeRequestContextDTO.getMetadata().getSiret() : null;

        SNRequestDTO dumeSnRequestDTO = snRequestMapper.toSNFusionRequestDTO(dumeRequest, siretOE);

        if (dumeRequest.getEtatDume() == EtatDume.STANDARD) {
            dumeSnRequestDTO.setDumeA(null);
        }

        try {
            // Appel du SN
            SNDumeFusionResponseDTO responseDTO = (SNDumeFusionResponseDTO) nationalServiceRequestService.restOE(Optional.of(dumeRequest).map(DumeRequest::getContext).map(DumeRequestContext::getPlatform).map(DumePlatform::getId).orElse(null), dumeSnRequestDTO, SNDumeFusionResponseDTO.class).getBody();

            if (responseDTO == null || responseDTO.getResponse() == null || responseDTO.getResponse().getDumeOE() == null) {
                List<SNMessageList> snErreurs = responseDTO == null || responseDTO.getResponse() == null || responseDTO.getResponse()
                                                                                                                       .getMessageList() == null ? Collections.emptyList() : responseDTO
                        .getResponse()
                        .getMessageList();
                throw new ServiceException("Impossible de créer un DUME OE " + snErreurs);
            }
            //Casting

            DumeResponse dumeResponse = new DumeResponse();
            dumeResponse.setDumeRequest(dumeRequest);
            String groupementLabel;
            if (Boolean.FALSE.equals(dumeRequestContextDTO.getMetadata().getFirst())) {
                groupementLabel = RandomStringUtils.randomAlphanumeric(30);
            } else {
                groupementLabel = null;
            }
            dumeResponse.setGroupementLabel(groupementLabel);

            addOrUpdateLotResponse(dumeRequestContextDTO, dumeResponse, dumeResponse.getDumeRequest().getContext().getLotSet());
            try (InputStream inputStream = new ByteArrayInputStream(Base64Utils.decodeFromString(responseDTO.getResponse().getDumeOE().getXmlDume()))) {
                processResponse(dumeResponse, dumeRequestContextDTO.getMetadata().getSiret(), inputStream);
                dumeResponse.setTypeOE(dumeRequestContextDTO.getMetadata().getTypeOE());
                EconomicPartyInfoDTO contactDTO = dumeRequestContextDTO.getEconomicPartyInfo();
                if (contactDTO != null && dumeResponse.getEconomicPartyInfo() != null) {
                    dumeResponse.getEconomicPartyInfo().setContact(contactDTO.getNom());
                    dumeResponse.getEconomicPartyInfo().setEmail(contactDTO.getEmail());
                    dumeResponse.getEconomicPartyInfo().setTelephone(contactDTO.getTelephone());
                }
            }
            // Enregistrement au statut brouillon
            pushToSNAndUpdateSnId(dumeResponse);
            dumeResponse = dumeResponseRepository.save(dumeResponse);
            logger.info("Création d'un dume OE à partir du contexte : {} , retour ID Dume OE => {}", dumeRequestContextDTO.getId(), dumeResponse.getId());
            removeLog();
            return new DumeResponseRetourCreationDTO(dumeResponse.getNumeroSN(), dumeResponse.getId());
        } catch (NationalServiceException e) {
            throw new ServiceException("Problème de communication avec le service national", e);
        }
    }

    private void processResponse(DumeResponse dumeResponse, String siret, InputStream inputStream) {
        JAXBElement<ESPDResponseType> element = (JAXBElement<ESPDResponseType>) jaxb2Marshaller.unmarshal(new StreamSource(inputStream));
        ESPDResponseType espdResponseType = element.getValue();
        createTreeDumeResponse(dumeResponse, espdResponseType);
        if (espdResponseType.getEconomicOperatorParty() != null && espdResponseType.getEconomicOperatorParty().getSMEIndicator() != null) {
            dumeResponse.setMicro(espdResponseType.getEconomicOperatorParty().getSMEIndicator().isValue());
        } else {
            dumeResponse.setMicro(false);
        }

        dumeResponse.setEconomicPartyInfo(economicPartyInfoMapper.getEconomicPartyInfo(espdResponseType.getEconomicOperatorParty().getParty()));

        dumeResponse.setRepresentantLegals(representantLegalMapper.getRepresentantLegals(espdResponseType.getEconomicOperatorParty(), dumeResponse));

        dumeResponse.setSiretOE(siret);

        dumeResponse = dumeResponseRepository.save(dumeResponse);
        logger.info("Création d'un dume OE à partir du SIRET : {} , retour ID Dume OE => {}", siret, dumeResponse.getId());
    }

    /**
     * This method try to push the dumeReponse  in DraftStatus et update the SN
     *
     * @param dumeResponse to update
     */
    private void pushToSNAndUpdateSnId(DumeResponse dumeResponse) throws ServiceException, NationalServiceException {

        logger.info("Enregistrement du dume OE au statut brouillon");

        String xml;
        try {
            xml = xmlDumeService.generateXMLOE(dumeResponse);
        } catch (XmlMappingException ex) {
            logger.error("une erreur est survenue lors de la generation du XML réponse pour l'ID  Reponse {}\n {}", dumeResponse.getId(), ex);
            throw new ServiceException("Génération du XML Impossible");
        }
        // Création du Request qui sera envoyé au SN
        SNRequestDTO snRequestDTO = snRequestMapper.fromDumeResponseToSnRequestDTO(dumeResponse, DRAFT_COPY.getStatus(), new String(Base64Utils.encode(xml.getBytes())));

        SNDumeSavedResponseDTO snDumeSavedResponseDTO = (SNDumeSavedResponseDTO) nationalServiceRequestService.restOE(Optional.of(dumeResponse.getDumeRequest()).map(DumeRequest::getContext).map(DumeRequestContext::getPlatform).map(DumePlatform::getId).orElse(null), snRequestDTO, SNDumeSavedResponseDTO.class).getBody();

        if (snDumeSavedResponseDTO != null && snDumeSavedResponseDTO.getResponse() != null && CollectionUtils.isEmpty(snDumeSavedResponseDTO.getResponse().getMessageList())) {
            dumeResponse.setNumeroSN(snDumeSavedResponseDTO.getResponse().getIdDume());
        } else {
            List<SNMessageList> snErreurs = snDumeSavedResponseDTO == null || snDumeSavedResponseDTO.getResponse() == null || snDumeSavedResponseDTO.getResponse()
                                                                                                                                                    .getMessageList() == null ? Collections
                    .emptyList() : snDumeSavedResponseDTO.getResponse().getMessageList();
            logger.error("JSON envoyé : {}", toJSON(snRequestDTO));
            throw new ServiceException("Erreur lors de l'enregistrement du dume OE au service national : " + snErreurs);
        }
    }

    @Override
    public DumeResponseDTO createDumeResponseFromExisitingSNNumber(Long idResponse, String snNumber) throws ServiceException, DumeResourceNotFoundException {
        Optional<DumeResponse> response = dumeResponseRepository.findById(idResponse);
        if (!response.isPresent()) {
            throw new DumeResourceNotFoundException(DUME_RESPONSE, idResponse);
        }
        DumeResponse dumeResponse = response.get();
        String refConsultation = dumeResponse.getDumeRequest().getContext().getConsultRef();
        populateLog(refConsultation);
        logger.info("Création d'un dume OE à partir de la réponse : {} et du Numéro SN : {}", idResponse, snNumber);
        // setter le numéro SN cible pour construire une demande valide
        SNRequestDTO downloadRequestDTO = snRequestMapper.toSNFusionDumeAWithDumeOEDefiniedByNumSN(dumeResponse, snNumber, MERGE_DUME);

        SNDumeFusionResponseDTO snDumeSavedResponseDTO = null;
        try {
            snDumeSavedResponseDTO = (SNDumeFusionResponseDTO) nationalServiceRequestService.restOE(Optional.of(dumeResponse.getDumeRequest()).map(DumeRequest::getContext).map(DumeRequestContext::getPlatform).map(DumePlatform::getId).orElse(null), downloadRequestDTO, SNDumeFusionResponseDTO.class).getBody();
            if (snDumeSavedResponseDTO != null && snDumeSavedResponseDTO.getResponse() != null && snDumeSavedResponseDTO.getResponse().getDumeOE() != null) {
                String encodedPDF = snDumeSavedResponseDTO.getResponse().getDumeOE().getXmlDume();
                byte[] decodedXML = Base64Utils.decode(encodedPDF.getBytes());
                //Casting
                try (InputStream inputStream = new ByteArrayInputStream(decodedXML)) {
                    processResponse(dumeResponse, dumeResponse.getSiretOE(), inputStream);
                }

            } else {
                List<SNMessageList> snErreurs = snDumeSavedResponseDTO == null || snDumeSavedResponseDTO.getResponse() == null || snDumeSavedResponseDTO.getResponse()
                                                                                                                                                        .getMessageList() == null ? Collections
                        .emptyList() : snDumeSavedResponseDTO.getResponse().getMessageList();
                throw new ServiceException("Erreurs lors de la récupération du XML du dume OE " + snErreurs);
            }

        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e);

        }
        removeLog();
        return getDumeResponseDTO(dumeResponse);
    }

    @Override
    public String generateDumeResponseXML(Long id) throws ServiceException, DumeResourceNotFoundException {
        Optional<DumeResponse> dumeResponse = dumeResponseRepository.findById(id);
        if (dumeResponse.isPresent()) {
            populateLog(dumeResponse.get().getDumeRequest().getContext().getConsultRef());
            try {
                String xmlOE = xmlDumeService.generateXMLOE(dumeResponse.get());
                removeLog();
                return xmlOE;
            } catch (XmlMappingException ex) {
                logger.error("une erreur est survenue lors de la generation du XML réponse pour l'ID  Reponse {}\n {}", dumeResponse.get().getId(), ex);
                throw new ServiceException("Génération du XML Impossible");
            }
        } else {
            throw new DumeResourceNotFoundException(DUME_RESPONSE, id);
        }
    }


    @Override
    public RetourStatus validateDumeOE(Long idDumeResponse) throws DumeResourceNotFoundException {
        Optional<DumeResponse> dumeResponseOptional = dumeResponseRepository.findById(idDumeResponse);
        if (dumeResponseOptional.isPresent()) {
            populateLog(dumeResponseOptional.get().getDumeRequest().getContext().getConsultRef());
            logger.info("Validation du DUME OE : ID = {}", idDumeResponse);
            RetourStatus retour;
            if (Boolean.TRUE.equals(dumeResponseOptional.get().getConfirmed())) {
                retour = new RetourStatus(VALIDE, "");
            } else {
                retour = new RetourStatus(NON_VALIDE, "Le DUME OE (" + idDumeResponse + ") n'est pas valide");
            }
            logger.info("Validation du dume OE : ID = {} , retour => {}", idDumeResponse, retour);
            removeLog();
            return retour;
        }
        throw new DumeResourceNotFoundException(DUME_RESPONSE, idDumeResponse);
    }

    @Override
    public RetourStatus publishDumeOE(Long idDumeResponse) throws DumeResourceNotFoundException {
        Optional<DumeResponse> dumeResponseOptional = dumeResponseRepository.findById(idDumeResponse);
        if (dumeResponseOptional.isPresent()) {
            populateLog(dumeResponseOptional.get().getDumeRequest().getContext().getConsultRef());
            DumeResponse dumeResponse = dumeResponseOptional.get();
            if (dumeResponse.getEtatDume() == EtatDume.PUBLIE) {
                logger.warn("Le DUME OE ID {} est déjà à l'état {}. Son état ne sera pas modifié.", dumeResponse.getId(), dumeResponse.getEtatDume());
            } else {
                dumeResponse.setEtatDume(EtatDume.A_PUBLIER);
                dumeResponse.setDateDemandePublication(LocalDateTime.now());
                dumeResponse.setNbPublicationTries(0);
                dumeResponseRepository.save(dumeResponse);
            }
            RetourStatus retour = new RetourStatus(OK, "");
            logger.info("Publication du Dume OE ID = {} , retour => {}", idDumeResponse, retour);
            removeLog();
            return retour;
        }
        throw new DumeResourceNotFoundException(DUME_RESPONSE, idDumeResponse);
    }

    @Override
    public RetourStatus publishDumeOEGroupement(PublicationGroupementDTO publicationGroupementDTO) throws DumeResourceNotFoundException, ServiceException {
        if (CollectionUtils.isEmpty(publicationGroupementDTO.getIdsContexteGroupement())) {
            RetourStatus retour = new RetourStatus(OK, "Aucun DUME à publier");
            return retour;
        }

        // Mettre le même idGroupement pour tous les dumes concernés
        String groupementLabel = Instant.now().getEpochSecond() + "-" + publicationGroupementDTO.getSiretMandataire();

        for (Long idDumeResponse : publicationGroupementDTO.getIdsContexteGroupement()) {

            Optional<DumeResponse> dumeResponseOptional = dumeResponseRepository.findById(idDumeResponse);
            if (!dumeResponseOptional.isPresent()) {
                throw new DumeResourceNotFoundException(DUME_RESPONSE, idDumeResponse);
            }

            populateLog(dumeResponseOptional.get().getDumeRequest().getContext().getConsultRef());
            DumeResponse dumeResponse = dumeResponseOptional.get();
            if (dumeResponse.getEtatDume() != EtatDume.BROUILLON) {
                throw new ServiceException("Le DUME OE ID " + dumeResponse.getId() + " n'est pas à l'état BROUILLON. Il est à l'état " + dumeResponse.getEtatDume());
            }
            dumeResponse.setEtatDume(EtatDume.A_PUBLIER);
            dumeResponse.setDateDemandePublication(LocalDateTime.now());
            dumeResponse.setNbPublicationTries(0);
            dumeResponse.setSiretMandataire(publicationGroupementDTO.getSiretMandataire());
            dumeResponse.setGroupementLabel(groupementLabel);
            dumeResponseRepository.save(dumeResponse);
            logger.info("Publication du Dume OE ID = {} dans le cadre du groupement ayant pour mandataire {}", idDumeResponse, publicationGroupementDTO.getSiretMandataire());
            removeLog();
        }

        RetourStatus retour = new RetourStatus(OK, "");
        return retour;
    }

    @Override
    public PublicationStatus recupererStatusPublicationDumeOe(Long idDumeResponse) throws DumeResourceNotFoundException {
        Optional<DumeResponse> dumeResponseOptional = dumeResponseRepository.findById(idDumeResponse);
        if (dumeResponseOptional.isPresent()) {
            populateLog(dumeResponseOptional.get().getDumeRequest().getContext().getConsultRef());
            PublicationStatus publicationStatus = new PublicationStatus();
            publicationStatus.setStatut(dumeResponseOptional.get().getEtatDume() == EtatDume.PUBLIE ? OK : NON_PUBLIE);
            if (publicationStatus.getStatut() == OK) {
                publicationStatus.setPublishedDumeList(asList(new PublishedDume(dumeResponseOptional.get().getNumeroSN(), null)));
            }
            logger.info("Récupération du statut de publication pour le Dume OE ID = {} , retour => {}", idDumeResponse, publicationStatus);
            removeLog();
            return publicationStatus;
        }
        throw new DumeResourceNotFoundException(DUME_RESPONSE, idDumeResponse);
    }

    @Override
    public File getPDFFile(String numeroSN) throws DumeResourceNotFoundException {
        Optional<DumeResponse> response = dumeResponseRepository.findByNumeroSN(numeroSN);
        if (!response.isPresent() || response.get().getDumeRequest() == null) {
            throw new DumeResourceNotFoundException(DUME_RESPONSE, "{numéro SN : " + numeroSN + "}");
        }

        populateLog(response.get().getDumeRequest().getContext().getConsultRef());
        DumeRequestContext context = response.get().getDumeRequest().getContext();
        String pdfStoragePath = pdfStorageFolder + File.separator + context.getId() + File.separator;
        Path pdfFilePath = Paths.get(pdfStoragePath, numeroSN + ".pdf");
        logger.info("Récupération du PDF pour le Dume OE Numéro SN = {} , résultat => {}", numeroSN, pdfFilePath);
        if (!pdfFilePath.toFile().isFile()) {
            logger.info("Aucun PDF trouvé pour le Dume OE Numéro SN = {} , résultat => {}", numeroSN, pdfFilePath);
            throw new DumeResourceNotFoundException(" PDF ", "{numéro SN :" + numeroSN + ", filePath: " + pdfFilePath + "}");
        }
        removeLog();
        return pdfFilePath.toFile();

    }

    @Override
    public byte[] downloadPDFAfterAsaveDumeOE(DumeResponseDTO dumeResponseDTO) throws ServiceException, DumeResourceNotFoundException {
        DumeResponseDTO savedDumeOE = updateDumeResponseByDTO(dumeResponseDTO);
        if (savedDumeOE != null) {
            try {
                String xml = generateDumeResponseXML(dumeResponseDTO.getId());
                populateLog(savedDumeOE.getConsultRef());
                var dumeResponse = dumeResponseRepository.findById(dumeResponseDTO.getId()).orElseThrow(() -> new ServiceException("Erreur voir les logs"));

                SNRequestDTO snDownloadRequestDTO = snRequestMapper.
                        toSnDownlaodResponsePDF(new String(Base64Utils.encode(xml.getBytes())), dumeResponse
                                .getDumeRequest()
                                .getContext());
                logger.info("Téléchargement du PDF depuis le SN et sauvegarde du Dume OE : {}", dumeResponseDTO.getId());
                SNDumeSavedResponseDTO response = (SNDumeSavedResponseDTO) nationalServiceRequestService.restOE(Optional.of(dumeResponse.getDumeRequest()).map(DumeRequest::getContext).map(DumeRequestContext::getPlatform).map(DumePlatform::getId).orElse(null), snDownloadRequestDTO, SNDumeSavedResponseDTO.class).getBody();

                if (response != null && response.getResponse() != null && response.getResponse().getDumeOE() != null) {
                    removeLog();
                    return Base64Utils.decode(response.getResponse().getDumeOE().getBytes());
                }
            } catch (NationalServiceException e) {
                throw new ServiceException(e.getMessage(), e);
            }
        }
        throw new DumeResourceNotFoundException(DUME_RESPONSE, dumeResponseDTO.getId());
    }

    @Override

    public void publishAllDumeResponses() {
        List<DumeResponse> dumeResponses = dumeResponseRepository.findAllByEtatDume(EtatDume.A_PUBLIER);
        if (CollectionUtils.isEmpty(dumeResponses)) {
            logger.info("Publication des dumes OE : Rien à publier");
            return;
        }

        for (DumeResponse dumeResponse : dumeResponses) {
            schedularTask.publishDumeOE(dumeResponse.getId());
        }
    }

    @Override
    public void retrieveAllDumesResponsePDF() {

        List<DumeResponse> dumeResponseList = dumeResponseRepository.findAllByEtatDumeAndNumeroSNIsNotNullAndPdfRetreived(EtatDume.PUBLIE, false);

        if (CollectionUtils.isEmpty(dumeResponseList)) {
            logger.info("Récupération des PDF pour les dumes OE : Rien à télécharger");
            return;
        }

        for (DumeResponse dumeResponse : dumeResponseList) {
            schedularTask.getPDFOE(dumeResponse.getId());
        }
    }

    private void createTreeDumeResponse(DumeResponse dumeResponse, ESPDResponseType espdResponseType) {
        List<CriteriaResponse> criteriaResponseList = new ArrayList<>();

        for (CriterionType criterionType : espdResponseType.getCriterion()) {
            Criteria criteria;
            String code = criterionType.getTypeCode().getValue().trim();
            if (code.contains("EXCLUSION")) {
                criteria = criteriaService.getCriteriaExclusionByCode(code);
            } else if (code.contains("SELECTION")) {
                criteria = criteriaService.getCriteriaSelectionByCode(code);
            } else {
                criteria = criteriaService.getCriteriaOtherByCode(code);
            }
            criteriaResponseList.add(criteriaMapper.toEntityFromCriterionType(criteria, dumeResponse, criterionType));
        }
        dumeResponse.getCriteriaResponseList().clear();
        dumeResponse.getCriteriaResponseList().addAll(criteriaResponseList);
    }


    /**
     * This service will do some checking
     * <ul>
     * <li>Check if SN is reachable</li>
     * <li>Check if DumeResponse is publicated</li>
     * <li>check if DumeResponse is Validated</li>
     * </ul>
     *
     * @param dumeResponseIdentifier the identifier to be checked.
     * @return object of {@see RetourStatus}.
     */
    @Override
    public RetourStatus checkAccessForGivenResponse(@NonNull final Long dumeResponseIdentifier) throws DumeResourceNotFoundException, ServiceException {
        final RetourStatus retValue;
        //check the other stuff like
        final Optional<DumeResponse> dumeResponse = dumeResponseRepository.findById(dumeResponseIdentifier);
        if (dumeResponse.isPresent()) {
            var context = dumeResponse.get().getDumeRequest().getContext();
            SNRequestDTO snRequestModelDTO = snRequestMapper.toSnGetModel(context);
            Boolean isReachable = false;
            try {
                isReachable = nationalServiceRequestService.restOE(Optional.of(context).map(DumeRequestContext::getPlatform).map(DumePlatform::getId).orElse(null), snRequestModelDTO, SNModelResponseDTO.class).getStatusCode() != HttpStatus.NOT_FOUND;
            } catch (NationalServiceException e) {
                logger.warn("Vérification de l'accès au SN : une exception est survenue, le SN est considéré comme down.", e);
            }
            if (!isReachable) {
                retValue = new RetourStatus(SN_DOWN, "SN is down !");
            } else {
                final PublicationStatus status = dumeRequestContextService.recupererStatusPublicationDumeAcheteur(dumeResponse.get().getDumeRequest().getContext().getId());
                if (null != status && NON_PUBLIE.equals(status.getStatut())) {
                    retValue = new RetourStatus(status.getStatut(), null);
                } else if (dumeResponse.get().getConfirmed()) {
                    retValue = new RetourStatus(VALIDE, "OE Validated");
                } else {
                    retValue = new RetourStatus(NON_VALIDE, "OE non Validated");
                }
            }
        } else {
            throw new DumeResourceNotFoundException(DUME_RESPONSE, dumeResponseIdentifier);
        }
        return retValue;
    }

    @Override
    public Long updateMetadonneesAndLot(@NotNull DumeRequestContextDTO dumeRequestContextDTO, @NotNull Long idDumeResponse) throws DumeResourceNotFoundException {
        Optional<DumeResponse> dumeResponseOptional = dumeResponseRepository.findById(idDumeResponse);
        populateLog(dumeRequestContextDTO.getConsultRef());
        logger.info("Mise à jour Metadonnés pour le Dume OE  avec l'id = {} , contextOE => {}", idDumeResponse, dumeRequestContextDTO.getId());
        if (!dumeResponseOptional.isPresent()) {
            throw new DumeResourceNotFoundException(DUME_RESPONSE, idDumeResponse);
        }

        DumeResponse dumeResponse = dumeResponseOptional.get();
        dumeResponse.setSiretOE(dumeRequestContextDTO.getMetadata().getSiret());
        dumeResponse.setTypeOE(dumeRequestContextDTO.getMetadata().getTypeOE());
        dumeResponse.getDumeRequest().getContext().getMetadata().setUserInscriptionIdentifier(dumeRequestContextDTO.getMetadata().getUserInscriptionIdentifier());
        addOrUpdateLotResponse(dumeRequestContextDTO, dumeResponse, dumeResponse.getDumeRequest().getContext().getLotSet());


        dumeResponse = dumeResponseRepository.save(dumeResponse);
        logger.info("Retour id = {}", dumeResponse.getId());
        removeLog();

        return dumeResponse.getId();
    }

    private void addOrUpdateLotResponse(@NotNull DumeRequestContextDTO dumeRequestContextDTO, DumeResponse dumeResponse, Set<Lot> lotSet) {
        if (!CollectionUtils.isEmpty(dumeRequestContextDTO.getLotDTOSet())) {
            dumeResponse.getLotSet().clear();
            for (Lot lot : lotSet) {
                boolean res = dumeRequestContextDTO.getLotDTOSet()
                                                   .stream()
                                                   .anyMatch(lotDTO -> Objects.equals(lot.getLotName(), lotDTO.getLotName()) && Objects.equals(lot.getLotNumber(), lotDTO.getLotNumber()));
                if (res) {
                    dumeResponse.getLotSet().add(lot);
                }
            }
        }
    }

    @Override
    public List<DumeResponseDTO> findDumeOEFromSN(Long identifierDumeOperateurEconomique, String snNumber) throws ServiceException {
        Optional<DumeResponse> response = dumeResponseRepository.findById(identifierDumeOperateurEconomique);
        List<DumeResponseDTO> retour = new ArrayList<>();
        if (response.isPresent() && !StringUtils.isEmpty(snNumber)) {
            String refConsultation = response.get().getDumeRequest().getContext().getConsultRef();
            populateLog(refConsultation);
            SNSearchResponseDTO retourSN = null;
            SNRequestDTO requestSN = snRequestMapper.toSnSearch(response.get(), asList(snRequestMapper.toSNDumeOE(snNumber, null)), asList(snRequestMapper.toIdentifiantOE(response.get())));
            try {
                retourSN = (SNSearchResponseDTO) nationalServiceRequestService.restOE(Optional.of(response.get()).map(DumeResponse::getDumeRequest).map(DumeRequest::getContext).map(DumeRequestContext::getPlatform).map(DumePlatform::getId).orElse(null), requestSN, SNSearchResponseDTO.class).getBody();
                logger.debug("Recherche du DUME OE à partir du dume OE {} et Numéro SN : {} , retour => {} ", identifierDumeOperateurEconomique, snNumber, retour);
            } catch (NationalServiceException e) {
                throw new ServiceException(e);
            }
            if (retourSN != null && retourSN.getResponse() != null && retourSN.getResponse().getResponsesRecherche() != null) {
                retour.addAll(retourSN.getResponse()
                                      .getResponsesRecherche()
                                      .values()
                                      .stream()
                                      // envoyer que les DUME OE accessible
                        .filter(r -> r.getDumeOEResponse() != null)
                                      .map(dumeResponseMapper::toResponseDto)
                                      .collect(Collectors.toList()));
            } else {
                logger.error("Erreurs lors de la recherche des DUME OE à partir du dume OE {} : \n {}", identifierDumeOperateurEconomique, retourSN == null || retourSN.getResponse() == null || retourSN
                        .getResponse()
                        .getMessageList() == null ? "" : retourSN.getResponse().getMessageList());
                logger.error("JSON envoyé : {}", toJSON(requestSN));
            }
        }
        return retour;
    }

    private DumeResponseDTO getDumeResponseDTO(DumeResponse dumeResponse) {
        DumeResponseDTO dumeResponseDTO = dumeResponseMapper.toResponseDto(dumeResponse);


        List<Criteria> criteriaList = criteriaSelectionDocumentRepository.findByDumeDocument(dumeResponse.getDumeRequest())
                                                                         .stream()
                                                                         .filter(criteriaSelectionDocument -> CollectionUtils.isEmpty(criteriaSelectionDocument.getLotList()) || CollectionUtils
                                                                                 .containsAny(criteriaSelectionDocument.getLotList(), dumeResponse.getLotSet()))
                                                                         .map(CriteriaSelectionDocument::getCriteria)
                                                                         .collect(Collectors.toList());

        dumeResponseDTO.setSelectionCriteriaList(dumeResponse.getSelectionCriteria()
                                                             .stream()
                                                             .filter(criteriaResponse -> criteriaList.isEmpty() || criteriaList.contains(criteriaResponse.getCriteria()))
                                                             .map(criteriaMapper::toSelectionDTO)
                                                             .collect(Collectors.toList()));
        return dumeResponseDTO;
    }

    @Override
    public byte[] generateDumeResponseXML(String snNumber) throws DumeResourceNotFoundException, ServiceException {
        Optional<DumeResponse> response = dumeResponseRepository.findByNumeroSN(snNumber);
        return generateXMLFromResponse(response, snNumber);
    }

    @Override
    public byte[] generateDumeResponseXMLById(Long identifierDumeOperateurEconomique) throws DumeResourceNotFoundException, ServiceException {
        Optional<DumeResponse> response = dumeResponseRepository.findById(identifierDumeOperateurEconomique);
        return generateXMLFromResponse(response, String.valueOf(identifierDumeOperateurEconomique));
    }

    private byte[] generateXMLFromResponse(Optional<DumeResponse> response, String identifier) throws DumeResourceNotFoundException, ServiceException {
        if (response.isPresent()) {
            try {
                String refConsultation = response.get().getDumeRequest().getContext().getConsultRef();
                populateLog(refConsultation);
                String xmlOE = xmlDumeService.generateXMLOE(response.get());
                removeLog();
                return xmlOE.getBytes();
            } catch (XmlMappingException ex) {
                logger.error("une erreur est survenue lors de la generation du XML réponse pour l'ID  Reponse {}\n {}", response.get().getId(), ex);
                throw new ServiceException("Génération du XML Impossible");
            }
        } else {
            throw new DumeResourceNotFoundException(DUME_RESPONSE, identifier);
        }
    }

    @Override
    public List<ECertisResponseDTO> getECertisProof(Long identifierDumeOperateurEconomique, String criteriaUUID, String countryCode) throws DumeResourceNotFoundException, ServiceException {
        DumeResponse dumeResponse = dumeResponseRepository.findById(identifierDumeOperateurEconomique)
                                                          .orElseThrow(() -> new DumeResourceNotFoundException("Dume OE", identifierDumeOperateurEconomique));
        Criteria criteria = criteriaRepository.findByUuid(criteriaUUID).orElseThrow(() -> new DumeResourceNotFoundException("Critère", criteriaUUID));
        ECertisMapping mapping = eCertisMappingRepository.findByCountryCode(countryCode).orElseThrow(() -> new DumeResourceNotFoundException("E-Certis mapping", countryCode));
        SNECertisDTO snRequest = snRequestMapper.toSnECertis(dumeResponse, criteriaUUID, mapping.geteCertisCountryCode(), mapping.geteCertisLanguageCode());
        try {
            SNECertisResponseDTO snResponse = (SNECertisResponseDTO) nationalServiceRequestService.restECertis(dumeResponse.getDumeRequest().getContext().getPlatform().getId(), snRequest, SNECertisResponseDTO.class).getBody();
            if (snResponse == null || snResponse.getResponse() == null || snResponse.getResponse().getCriterionsAndProofs() == null) {
                return new ArrayList<>();
            } else {
                return snResponse.getResponse().getCriterionsAndProofs().stream().map(eCertisMapper::toEcertis).collect(Collectors.toList());
            }
        } catch (NationalServiceException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
