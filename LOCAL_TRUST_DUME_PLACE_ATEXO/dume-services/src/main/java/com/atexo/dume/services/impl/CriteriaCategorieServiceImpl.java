package com.atexo.dume.services.impl;

import com.atexo.dume.dto.CriteriaCategorieDTO;
import com.atexo.dume.mapper.CriteriaCategorieMapper;
import com.atexo.dume.model.CategorieType;
import com.atexo.dume.repository.CriteriaCategorieRepository;
import com.atexo.dume.services.api.CriteriaCategorieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CriteriaCategorieServiceImpl implements CriteriaCategorieService {

    @Autowired
    private CriteriaCategorieRepository criteriaCategorieRepository;

    @Autowired
    private CriteriaCategorieMapper criteriaCategorieMapper;

    @Override
    public List<CriteriaCategorieDTO> getAllSelectionCriteriaCategorieDTO() {
        return criteriaCategorieMapper.toDTOList(criteriaCategorieRepository.findByType(CategorieType.SELECTION));
    }
}
