package com.atexo.dume.services.impl;

import com.atexo.dume.dto.donneesessentielles.xml.MarcheType;
import com.atexo.dume.dto.donneesessentielles.xml.MarchesType;
import com.atexo.dume.dto.sn.request.SNRequestDTO;
import com.atexo.dume.dto.sn.response.SNDumeFusionResponseDTO;
import com.atexo.dume.dto.sn.response.SNDumeSavedResponseDTO;
import com.atexo.dume.dto.sn.response.SNMessageList;
import com.atexo.dume.mapper.sn.SNRequestMapper;
import com.atexo.dume.model.*;
import com.atexo.dume.repository.DumePlatformRepository;
import com.atexo.dume.repository.DumeRequestRepository;
import com.atexo.dume.repository.DumeResponseRepository;
import com.atexo.dume.services.api.*;
import com.atexo.dume.services.exception.NationalServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.atexo.dume.toolbox.consts.DumeStatusEnum.REPLACED;
import static com.atexo.dume.toolbox.consts.DumeStatusEnum.VALIDATED;
import static com.atexo.dume.toolbox.consts.NationalServiceFormatEnum.DUME_PDF;
import static com.atexo.dume.toolbox.logging.LoggerUtils.*;
import static com.atexo.dume.toolbox.rest.NationalServiceOperations.GET_FORMATED_DUME;
import static java.util.Arrays.asList;

@Service
public class SchedularTaskImpl implements SchedularTask {

    private static final Logger logger = LoggerFactory.getLogger(SchedularTaskImpl.class);
    public static final String PB_COMMUNICATION_SN = "Problème de communication SN";
    private static final String PB_GENERATION_XML = "Problème lors de la génération du XML";

    @Autowired
    private SNRequestMapper snRequestMapper;

    @Autowired
    private ContractService contractService;

    @Autowired
    private NationalServiceRequestService nationalServiceRequestService;

    @Autowired
    private MPEService mpeService;

    @Autowired
    private DumeRequestRepository dumeRequestRepository;

    @Autowired
    private DumeResponseRepository dumeResponseRepository;

    @Autowired
    private DumePlatformRepository dumePlatformRepository;

    @Autowired
    private XMLDumeService xmlDumeService;

    @Value("${PDF_STORAGE_FOLDER}")
    private String pdfStorageFolder;

    @Value("${PUBLICATION_MAX_ATTEMPTS}")
    private int maxPublicationAttempts;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void publishDumeOE(Long idDumeResponse) {
        DumeResponse dumeResponse = dumeResponseRepository.getOne(idDumeResponse);
        populateLog(dumeResponse.getDumeRequest().getContext().getConsultRef());
        String errors = null;
        logger.info("Publication du dume OE ID = {}", dumeResponse.getId());
        if (dumeResponse.getNbPublicationTries() >= maxPublicationAttempts) {
            logger.warn("le DUME OE {} a atteint {} tentatives de publication, on le passe en ERREUR", dumeResponse.getId(), maxPublicationAttempts);
            dumeResponse.setEtatDume(EtatDume.ERREUR_SN);
            dumeResponseRepository.save(dumeResponse);
        } else {
            try {
                String xml = xmlDumeService.generateXMLOE(dumeResponse);
                // Création du Request qui sera envoyé au SN
                SNRequestDTO snRequestDTO = snRequestMapper.fromDumeResponseToSnRequestDTO(dumeResponse, VALIDATED.getStatus(), new String(Base64Utils.encode(xml.getBytes())));

                SNDumeSavedResponseDTO snDumeSavedResponseDTO = (SNDumeSavedResponseDTO) nationalServiceRequestService.restOE(Optional.of(dumeResponse.getDumeRequest()).map(DumeRequest::getContext).map(DumeRequestContext::getPlatform).map(DumePlatform::getId).orElse(null), snRequestDTO, SNDumeSavedResponseDTO.class).getBody();

                if (snDumeSavedResponseDTO != null && snDumeSavedResponseDTO.getResponse() != null && CollectionUtils.isEmpty(snDumeSavedResponseDTO.getResponse().getMessageList())) {
                    dumeResponse.setEtatDume(EtatDume.PUBLIE);
                    dumeResponse.setDatePublication(LocalDateTime.now());
                } else {
                    logger.error("Erreurs lors l'enregistrement du dume OE {} : {}", dumeResponse, snDumeSavedResponseDTO == null || snDumeSavedResponseDTO.getResponse() == null || snDumeSavedResponseDTO.getResponse().getMessageList() == null ? "" : snDumeSavedResponseDTO.getResponse().getMessageList());
                    logger.error("JSON envoyé : {}", toJSON(snRequestDTO));
                    errors = (snDumeSavedResponseDTO == null || snDumeSavedResponseDTO.getResponse() == null) ? "" : snDumeSavedResponseDTO.getResponse().getMessageList().toString();
                    dumeResponse.setEtatDume(EtatDume.ERREUR_SN);
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                dumeResponse.setNbPublicationTries(dumeResponse.getNbPublicationTries() + 1);
                errors = e.getMessage();
            } finally {
                dumeResponse.setErrors(errors);
                dumeResponseRepository.save(dumeResponse);
            }
        }
        removeLog();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void publishDumeAcheteur(Long idDumeRequest) {
        DumeRequest dumeRequest = dumeRequestRepository.getOne(idDumeRequest);
        String errors = null;
        populateLog(dumeRequest.getContext().getConsultRef());
        logger.info("Publication du dume acheteur : ID = {}", dumeRequest.getId());
        if (dumeRequest.getNbPublicationTries() >= maxPublicationAttempts) {
            logger.warn("le DUME A {} a atteint {} tentatives de publication, on le passe en ERREUR", dumeRequest.getId(), maxPublicationAttempts);
            dumeRequest.setEtatDume(EtatDume.ERREUR_SN);
            dumeRequestRepository.save(dumeRequest);
        } else {
            try {
                SNRequestDTO dumeSnRequestDTO = null;
                if (Boolean.TRUE.equals(dumeRequest.getContext().getMetadata().getSimplified())) {
                    dumeSnRequestDTO = snRequestMapper.toSnSimplifiedRequestDto(dumeRequest);
                } else {
                    String xml = xmlDumeService.generateXMLAcheteur(dumeRequest);
                    dumeSnRequestDTO = snRequestMapper.toSnRequestDto(dumeRequest, VALIDATED.getStatus(), new String(Base64Utils.encode(xml.getBytes())));
                }
                SNDumeSavedResponseDTO response = (SNDumeSavedResponseDTO) nationalServiceRequestService.restAcheteur(Optional.of(dumeRequest).map(DumeRequest::getContext).map(DumeRequestContext::getPlatform).map(DumePlatform::getId).orElse(null), dumeSnRequestDTO, SNDumeSavedResponseDTO.class).getBody();
                // si pas de message d'erreurs
                if (response != null && response.getResponse() != null && CollectionUtils.isEmpty(response.getResponse().getMessageList())) {
                    dumeRequest.setNumDumeSN(response.getResponse().getIdDume());
                    dumeRequest.setEtatDume(EtatDume.PUBLIE);
                    dumeRequest.setDatePublication(LocalDateTime.now());

                } else {
                    logger.error("Erreurs lors de la publication du dume Request {} : {}", dumeRequest, response == null ? "" : response.getResponse().getMessageList());
                    logger.error("JSON envoyé : {}", toJSON(dumeSnRequestDTO));
                    errors = (response == null || response.getResponse() == null) ? "" : response.getResponse().getMessageList().toString();
                    dumeRequest.setEtatDume(EtatDume.ERREUR_SN);
                }
            } catch (Exception e) {
                logger.error(PB_GENERATION_XML, e);
                errors = e.getMessage();
                dumeRequest.setNbPublicationTries(dumeRequest.getNbPublicationTries() + 1);
            } finally {
                dumeRequest.setErrors(errors);
                dumeRequestRepository.save(dumeRequest);
            }
        }
        removeLog();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void getPDFOE(Long idDumeResponse) {
        DumeResponse dumeResponse = dumeResponseRepository.getOne(idDumeResponse);
        populateLog(dumeResponse.getDumeRequest().getContext().getConsultRef());
        logger.info("Récupération du PDF pour le DUME OE {}", idDumeResponse);
        // Création du Request Download qui sera envoyé au SN
        SNRequestDTO downloadRequestDTO = snRequestMapper.toSnDownloadResponseDto(dumeResponse, DUME_PDF, GET_FORMATED_DUME, dumeResponse.getNumeroSN());
        SNDumeSavedResponseDTO snDumeSavedResponseDTO = null;
        try {
            snDumeSavedResponseDTO = (SNDumeSavedResponseDTO) nationalServiceRequestService.restOE(Optional.of(dumeResponse.getDumeRequest()).map(DumeRequest::getContext).map(DumeRequestContext::getPlatform).map(DumePlatform::getId).orElse(null), downloadRequestDTO, SNDumeSavedResponseDTO.class).getBody();

            if (snDumeSavedResponseDTO != null && snDumeSavedResponseDTO.getResponse() != null && CollectionUtils.isEmpty(snDumeSavedResponseDTO.getResponse().getMessageList())) {

                saveFile(snDumeSavedResponseDTO.getResponse().getDumeOE(), dumeResponse.getDumeRequest().getContext().getId(), dumeResponse.getNumeroSN() + ".pdf");
                dumeResponse.setPdfRetreived(true);
                dumeResponseRepository.save(dumeResponse);

            } else {
                logger.error("Erreurs lors de la récupération du PDF du dume OE {} : \n {}", dumeResponse, snDumeSavedResponseDTO == null || snDumeSavedResponseDTO.getResponse() == null || snDumeSavedResponseDTO.getResponse().getMessageList() == null ? "" : snDumeSavedResponseDTO.getResponse().getMessageList());
                logger.error("JSON envoyé : {}", toJSON(downloadRequestDTO));
            }
        } catch (NationalServiceException e) {
            logger.error("Problème de communication avec le service national", e);
        } catch (IOException e) {
            logger.error("Problèmes dans la sauvegarde {}", e);
        }

        removeLog();

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void getPDFAcheteur(Long idDumeRequest) {
        DumeRequest dumeRequest = dumeRequestRepository.getOne(idDumeRequest);
        populateLog(dumeRequest.getContext().getConsultRef());
        logger.info("Récupération du PDF SN pour le dume acheteur : ID= {}", dumeRequest.getId());
        try {

            SNRequestDTO dumeSnRequestDTO = snRequestMapper.toSnDownloadRequestDto(dumeRequest, DUME_PDF, GET_FORMATED_DUME);
            SNDumeSavedResponseDTO response = (SNDumeSavedResponseDTO) nationalServiceRequestService.restAcheteur(Optional.of(dumeRequest).map(DumeRequest::getContext).map(DumeRequestContext::getPlatform).map(DumePlatform::getId).orElse(null), dumeSnRequestDTO, SNDumeSavedResponseDTO.class).getBody();
            // si pas de message d'erreurs
            if (response != null && response.getResponse() != null && response.getResponse().getDumeA() != null) {
                String encodedPDF = response.getResponse().getDumeA();

                saveFile(encodedPDF, dumeRequest.getContext().getId(), dumeRequest.getNumDumeSN() + ".pdf");
                dumeRequest.setPdfRetrieved(true);
                dumeRequestRepository.save(dumeRequest);

            } else {
                logger.error("Erreurs lors de la récupération du PDF du dume Request {} : \n {}", dumeRequest, response == null || response.getResponse() == null || response.getResponse().getMessageList() == null ? "" : response.getResponse().getMessageList());
                logger.error("JSON envoyé : {}", toJSON(dumeSnRequestDTO));
            }
        } catch (NationalServiceException e) {
            logger.error(e.getMessage(), e);
        } catch (IOException e) {
            logger.error("Problème lors l'enregistement d'un pdf", e);
        }
        removeLog();

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateDLROAcheteur(Long idDumeRequest) {
        DumeRequest dumeRequest = dumeRequestRepository.getOne(idDumeRequest);
        populateLog(dumeRequest.getContext().getConsultRef());
        String errors = null;
        logger.info("Mise à jour de la DLRO  du dume acheteur : ID = {}", dumeRequest.getId());
        if (dumeRequest.getNbPublicationTries() >= maxPublicationAttempts) {
            logger.warn("le DUME A {} (En statut MAJ DLRO) a atteint {} tentatives de publication, on le passe en ERREUR", dumeRequest.getId(), maxPublicationAttempts);
            dumeRequest.setEtatDume(EtatDume.ERREUR_SN);
            dumeRequestRepository.save(dumeRequest);
        } else {

            try {
                SNRequestDTO snRequestDTO = snRequestMapper.toSNUpdateMetadonneDLRO(dumeRequest.getContext(), dumeRequest.getNumDumeSN());
                SNDumeSavedResponseDTO response = (SNDumeSavedResponseDTO) nationalServiceRequestService.restMetadonnees(dumeRequest.getContext().getPlatform().getId(), snRequestDTO, SNDumeSavedResponseDTO.class).getBody();

                List<SNMessageList> snMessageLists = response.getResponse().getMessageList();
                if (!CollectionUtils.isEmpty(snMessageLists)) {
                    if (snMessageLists.size() == 1 && "INFO".equals(snMessageLists.get(0).getType()) && "MISE-A-JOUR".equals(snMessageLists.get(0).getCode())) {
                        dumeRequest.setEtatDume(EtatDume.PUBLIE);
                    } else {
                        logger.error("la mise à jour de DLRO a rencontré un problème {}", response);
                        logger.error("JSON envoyé : {}", toJSON(snRequestDTO));
                        errors = snMessageLists.toString();
                        dumeRequest.setEtatDume(EtatDume.ERREUR_SN);
                    }
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                errors = e.getMessage();
                dumeRequest.setNbPublicationTries(dumeRequest.getNbPublicationTries() + 1);
            } finally {
                dumeRequest.setErrors(errors);
                dumeRequestRepository.save(dumeRequest);
            }
        }
        removeLog();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void replaceDumeAcheteur(Long idDumeRequest) {
        DumeRequest dumeRequest = dumeRequestRepository.getOne(idDumeRequest);
        populateLog(dumeRequest.getContext().getConsultRef());
        logger.info("Remplacement du dume acheteur : ID = {} , numeroSN = {}", dumeRequest.getId(), dumeRequest.getNumDumeSN());
        String errors = null;
        if (dumeRequest.getNbPublicationTries() >= maxPublicationAttempts) {
            logger.warn("le DUME A {} (en statut A REMPLACER) a atteint {} tentatives de publication, on le passe en ERREUR", dumeRequest.getId(), maxPublicationAttempts);
            dumeRequest.setEtatDume(EtatDume.ERREUR_SN);
            dumeRequestRepository.save(dumeRequest);
        } else {
            try {
                if (Boolean.TRUE != dumeRequest.getContext().getDefault()) {
                    String xml = xmlDumeService.generateXMLAcheteur(dumeRequest);
                    SNRequestDTO dumeSnRequestDTO = snRequestMapper.toSnRequestDto(dumeRequest, REPLACED.getStatus(), new String(Base64Utils.encode(xml.getBytes())));
                    dumeSnRequestDTO.getDumeA().setIdDume(dumeRequest.getNumDumeSN());
                    SNDumeSavedResponseDTO response = (SNDumeSavedResponseDTO) nationalServiceRequestService.restAcheteur(Optional.of(dumeRequest).map(DumeRequest::getContext).map(DumeRequestContext::getPlatform).map(DumePlatform::getId).orElse(null), dumeSnRequestDTO, SNDumeSavedResponseDTO.class).getBody();
                    // si pas de message d'erreurs
                    if (response != null && response.getResponse() != null && CollectionUtils.isEmpty(response.getResponse().getMessageList())) {
                        replaceDumeRequest(dumeRequest, EtatDume.PUBLIE, response.getResponse().getIdDume());
                    } else {
                        logger.error("Erreurs lors du Remplacement du dume Request {} : {}", dumeRequest.getId(), response == null ? "" : response.getResponse().getMessageList());
                        logger.error("JSON envoyé : {}", toJSON(dumeSnRequestDTO));
                        errors = (response == null || response.getResponse() == null) ? "" : response.getResponse().getMessageList().toString();
                        dumeRequest.setEtatDume(EtatDume.ERREUR_SN);
                    }
                } else {
                    replaceDumeRequest(dumeRequest, EtatDume.STANDARD, null);
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                errors = e.getMessage();
                dumeRequest.setNbPublicationTries(dumeRequest.getNbPublicationTries() + 1);
            } finally {
                dumeRequest.setErrors(errors);
                dumeRequestRepository.save(dumeRequest);
            }
        }
        removeLog();
    }

    private void replaceDumeRequest(DumeRequest dumeRequest, EtatDume etatDume, String numeroSN) {
        DumeRequest newDR = new DumeRequest();
        newDR.getExclusionCriteriaList().addAll(dumeRequest.getExclusionCriteriaList());
        List<CriteriaSelectionDocument> criteriaSelectionDocuments = new ArrayList<>();

        for (CriteriaSelectionDocument criteriaSelectionDocument : dumeRequest.getSelectionCriteriaList()) {
            criteriaSelectionDocuments.add(new CriteriaSelectionDocument(criteriaSelectionDocument.getCriteria(), newDR));
        }
        newDR.getSelectionCriteriaList().addAll(criteriaSelectionDocuments);
        // Creation d'un nouveau
        newDR.setEtatDume(etatDume);
        newDR.setNumDumeSN(numeroSN);
        newDR.setContext(dumeRequest.getContext());
        newDR.setConfirmed(Boolean.TRUE);
        newDR.setDatePublication(LocalDateTime.now());
        newDR.setDateDemandePublication(LocalDateTime.now());

        dumeRequest.setReplacedBy(newDR);
        dumeRequest.setContext(null);
        dumeRequest.setEtatDume(EtatDume.REMPLACE);
        dumeRequestRepository.saveAll(asList(dumeRequest, newDR));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void downloadTemplateXMLOE(Long idDumeRequest) {
        DumeRequest dumeRequest = dumeRequestRepository.getOne(idDumeRequest);
        populateLog(dumeRequest.getContext().getConsultRef());
        SNRequestDTO dumeSnRequestDTO = snRequestMapper.toSNFusionRequestDTO(dumeRequest, null);
        SNDumeFusionResponseDTO responseDTO;
        try {
            logger.info("Récupération du XML : Acheteur ID = {} , NumSn = {}", dumeRequest.getId(), dumeRequest.getNumDumeSN());
            // Call SN to GET XML OE for dumeRequest
            responseDTO = (SNDumeFusionResponseDTO) nationalServiceRequestService.restOE(Optional.of(dumeRequest).map(DumeRequest::getContext).map(DumeRequestContext::getPlatform).map(DumePlatform::getId).orElse(null), dumeSnRequestDTO, SNDumeFusionResponseDTO.class).getBody();
            if (responseDTO == null || responseDTO.getResponse() == null || responseDTO.getResponse().getDumeOE() == null) {
                List<SNMessageList> snErreurs = responseDTO == null || responseDTO.getResponse() == null || responseDTO.getResponse().getMessageList() == null ? Collections.emptyList() : responseDTO.getResponse().getMessageList();
                logger.error("Impossible de Télécharger le DUME OE  pour le dumeRequest id : {} , erreur SN {}", dumeRequest.getId(), snErreurs);
                return;
            }

            saveFile(responseDTO.getResponse().getDumeOE().getXmlDume(), dumeRequest.getContext().getId(), dumeRequest.getNumDumeSN() + "_OE.xml");

            dumeRequest.setXmlOERetrieved(Boolean.TRUE);
            dumeRequestRepository.save(dumeRequest);

        } catch (NationalServiceException e) {
            logger.error(PB_COMMUNICATION_SN, e);
        } catch (IOException e) {
            logger.error("Problème lors l'enregistement du XML ", e);
        }
        removeLog();
    }

    @Override
    public void publishDonneesEssentielles(DumePlatform platform) {
        MarchesType tousLesMarches = mpeService.getModifiedContract(platform);
        if (tousLesMarches != null && !CollectionUtils.isEmpty(tousLesMarches.getMarche())) {
            for (MarcheType marche : tousLesMarches.getMarche()) {
                if (!StringUtils.isEmpty(marche.getId()) && !StringUtils.isEmpty(marche.getUuid())) {
                    populateLog("DE_" + marche.getId());
                    logger.info("Traitement du Marché => {}, UUID => {}", marche.getId(), marche.getUuid());
                    contractService.processContract(marche, platform.getId());
                    removeLog();
                } else {
                    logger.warn("Aucun identifiant trouvé pour le Marché => N° :{} , UUID :{}", marche.getId(), marche.getUuid());
                }
            }
        }

    }


    private void saveFile(String encodedFile, Long idContext, String nameFile) throws IOException {
        if (encodedFile != null) {
            byte[] decodedPDF = Base64Utils.decode(encodedFile.getBytes());
            String folderPath = pdfStorageFolder + File.separator + idContext + File.separator;
            Path folder = Paths.get(folderPath);
            // Create the folder if not exist
            if (!folder.toFile().isDirectory()) {
                Files.createDirectories(folder);
            }
            Path pdfFilPath = Paths.get(folderPath, nameFile);
            Files.write(pdfFilPath, decodedPDF);
        }
    }

    public void setMaxPublicationAttempts(int maxPublicationAttempts) {
        this.maxPublicationAttempts = maxPublicationAttempts;
    }
}
