package com.atexo.dume.services.impl;

import com.atexo.dume.dto.*;
import com.atexo.dume.dto.sn.request.SNRequestDTO;
import com.atexo.dume.dto.sn.response.SNDumeSavedResponseDTO;
import com.atexo.dume.mapper.DumeRequestContextMapper;
import com.atexo.dume.mapper.sn.SNRequestMapper;
import com.atexo.dume.model.*;
import com.atexo.dume.repository.DumePlatformRepository;
import com.atexo.dume.repository.DumeRequestContextRepository;
import com.atexo.dume.repository.DumeResponseRepository;
import com.atexo.dume.services.api.DumeRequestContextService;
import com.atexo.dume.services.api.DumeRequestService;
import com.atexo.dume.services.api.NationalServiceRequestService;
import com.atexo.dume.services.exception.DumeResourceNotFoundException;
import com.atexo.dume.services.exception.NationalServiceException;
import com.atexo.dume.services.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.atexo.dume.toolbox.logging.LoggerUtils.populateLog;
import static com.atexo.dume.toolbox.logging.LoggerUtils.removeLog;

@Service
public class DumeRequestContextServiceImpl implements DumeRequestContextService {


    public static final String DUME_REQUEST_CONTEXT = "DumeRequestContext";
    private final Logger logger = LoggerFactory.getLogger(DumeRequestContextServiceImpl.class);
    private static final String DUME_CONTEXT_NON_PUBLIE = "Tous les dumes Acheteurs du context {} ne sont pas publiés";

    @Autowired
    private DumeRequestContextRepository contextRepository;

    @Autowired
    private DumeRequestService dumeRequestService;

    @Autowired
    private DumeRequestContextMapper contextMapper;

    @Autowired
    private DumePlatformRepository platformRepository;

    @Autowired
    private DumeResponseRepository dumeResponseRepository;

    @Autowired
    private SNRequestMapper snRequestMapper;

    @Autowired
    private NationalServiceRequestService nationalServiceRequestService;


    /**
     * @param contextDTO {@see DumeRequestContextDTO}, for all the new requests associate a Context to a know platform {@link DumePlatform }
     * @return
     * @throws Exception
     */
    @Override
    public DumeRequestContextDTO saveContext(final DumeRequestContextDTO contextDTO) throws ServiceException, DumeResourceNotFoundException {
        populateLog(contextDTO.getConsultRef());
        logger.info("Création d'un context à partir du DTO {}", contextDTO.getConsultRef());
        final DumeRequestContext context = contextMapper.contextRqDTOToContextRq(contextDTO);

        if (context.getId() == null && contextDTO.getMetadata() != null) {
            String platformIdentifier = contextDTO.getMetadata().getPlatformIdentifier();
            Optional<DumePlatform> optionalPlatform = platformRepository.findByPlatformIdentifier(platformIdentifier);
            if (!optionalPlatform.isPresent()) {
                throw new ServiceException("La plateforme avec l'identifiant " + platformIdentifier + " est non connue dans LT-DUME.");
            }
            Optional<DumeRequestContext> optionalExistant = contextRepository.findByMetadataConsultationIdentifierAndPlatform(contextDTO.getMetadata().getConsultationIdentifier(), optionalPlatform.get());

            if (optionalExistant.isPresent()) {
                throw new ServiceException("Le contexte avec l'id de la consultation " + contextDTO.getMetadata().getConsultationIdentifier() + " et la plateforme " + optionalPlatform.get().getPlatformIdentifier() + "existe déjà");
            }
            context.setPlatform(optionalPlatform.get());
        }

        context.getLotSet().stream().forEach(lot -> lot.setContext(context));
        context.initDumeDocuments();
        context.addDumeDocumentToContext(dumeRequestService.createDumeRequest(context));
        DumeRequestContextDTO retour = contextMapper.dumeReqContextToDumeReqContextDTO(contextRepository.save(context));
        logger.info("Création d'un context retour => {}", retour);
        removeLog();
        return retour;
    }

    @Override
    public DumeRequestContextDTO getDumeRequestContextById(Long id) throws DumeResourceNotFoundException {
        Optional<DumeRequestContext> dumeRequestContext = contextRepository.findById(id);
        if (!dumeRequestContext.isPresent()) {
            throw new DumeResourceNotFoundException(DUME_REQUEST_CONTEXT, id);
        }
        return contextMapper.dumeReqContextToDumeReqContextDTO(dumeRequestContext.get());
    }

    @Override
    public Long updateContextWithoutRequest(DumeRequestContextDTO dumeRequestContextDTO, Long idContext) throws DumeResourceNotFoundException {

        Optional<DumeRequestContext> dumeRequestContextOptional = contextRepository.findById(idContext);
        if (!dumeRequestContextOptional.isPresent()) {
            throw new DumeResourceNotFoundException(DUME_REQUEST_CONTEXT, idContext);
        }
        populateLog(dumeRequestContextOptional.get().getConsultRef());
        logger.info("Mise à jour du contexte sans mettre à jour le DUME acheteur : idContext = {}", idContext);
        DumeRequestContext dumeRequestContext = dumeRequestContextOptional.get();
        // appliquer le filtre sur la nature de marché en cas de channgement
        boolean applyMarketFilter = !Objects.equals(dumeRequestContextDTO.getMetadata().getMarketTypeIdentifier(), dumeRequestContext.getMetadata().getMarketTypeIdentifier());
        // Check if there is a need for purge
        for (DumeDocument dumedocument : dumeRequestContext.getDumeDocuments()) {
            if (dumeRequestService.checkPurgeCriteriaLot((DumeRequest) dumedocument, dumeRequestContextDTO.getLotDTOSet())) {
                dumeRequestService.purgeCriteriaLot((DumeRequest) dumedocument);
            }
        }
        contextMapper.updateContextFromDTOWithLot(dumeRequestContextDTO, dumeRequestContext);
        dumeRequestContext.setApplyMarketFilter(applyMarketFilter || Boolean.TRUE.equals(dumeRequestContext.getApplyMarketFilter()));
        if (applyMarketFilter) {
            for (DumeDocument dumeDocument : dumeRequestContext.getDumeDocuments()) {
                dumeDocument.setConfirmed(false);
            }
        }
        contextRepository.save(dumeRequestContext);
        Long retour = dumeRequestContext.getId();
        logger.info("Mise à jour du contexte sans mettre à jour le DUME acheteur : retour => {}", retour);
        removeLog();
        return retour;
    }

    @Override
    public DumeRequestContextDTO updateDumeContextFromDTO(DumeRequestContextDTO dumeRequestContextDTO) throws ServiceException, DumeResourceNotFoundException {
        Optional<DumeRequestContext> dumeRequestContextOptional = contextRepository.findById(dumeRequestContextDTO.getId());
        if (!dumeRequestContextOptional.isPresent()) {
            throw new DumeResourceNotFoundException(DUME_REQUEST_CONTEXT, dumeRequestContextDTO.getId());
        }
        DumeRequestContext dumeRequestContext = dumeRequestContextOptional.get();
        populateLog(dumeRequestContext.getConsultRef());

        logger.info("Mise à jour du contexte à partir du DTO : {}", dumeRequestContextDTO.getId());
        contextMapper.updateContextFromContextDTOWithoutDumeRequests(dumeRequestContextDTO, dumeRequestContext);
        dumeRequestContext.initDumeDocuments();
        // update du la colone default
        dumeRequestContext.setDefault(dumeRequestContextDTO.getDefault());
        dumeRequestContext.setApplyMarketFilter(dumeRequestContextDTO.getApplyMarketFilter());

        for (DumeRequestDTO dumeRequestDTO : dumeRequestContextDTO.getDumeRequestSets()) {
            DumeRequest dumeRequest = dumeRequestService.updateDumeRequestByDTO(dumeRequestDTO);
            if (dumeRequest == null) {
                throw new ServiceException("Problème d'enregistrement d'un DUME Acheteur");
            }

            dumeRequestContext.addDumeDocumentToContext(dumeRequest);
        }

        if (Boolean.TRUE.equals(dumeRequestContext.getDefault())) {
            dumeRequestContext.getDumeDocuments().forEach(dumeDocument -> dumeDocument = dumeRequestService.purgeCriteriaLot((DumeRequest) dumeDocument));
        }
        contextRepository.save(dumeRequestContext);
        logger.info("Mise à jour du contexte à partir du DTO , retour => {}", dumeRequestContextDTO.getId());
        removeLog();
        return dumeRequestContextDTO;
    }

    @Override
    public Boolean deleteContextDumeRequest(Long idContext) throws DumeResourceNotFoundException {
        Optional<DumeRequestContext> dumeRequestContextOptional = contextRepository.findById(idContext);
        if (!dumeRequestContextOptional.isPresent()) {
            throw new DumeResourceNotFoundException(DUME_REQUEST_CONTEXT, idContext);
        }
        populateLog(dumeRequestContextOptional.get().getConsultRef());
        logger.info("Suppression du request context : {}", idContext);
        contextRepository.delete(dumeRequestContextOptional.get());
        logger.info("Suppression du request résultat : {}", true);
        removeLog();
        return true;
    }

    @Override
    public List<DumeRequestContextDTO> getAllDumeRequestContext() {
        return contextRepository.findAll().stream().map(contextMapper::dumeReqContextToDumeReqContextDTO).collect(Collectors.toList());
    }

    @Override
    public RetourStatus checkValidContextDume(Long idContextDume) throws DumeResourceNotFoundException {
        Optional<DumeRequestContext> optional = contextRepository.findById(idContextDume);
        if (!optional.isPresent()) {
            throw new DumeResourceNotFoundException(DUME_REQUEST_CONTEXT, idContextDume);
        }
        DumeRequestContext dumeRequestContext = optional.get();
        populateLog(dumeRequestContext.getConsultRef());
        logger.info("Vérification de la validation d'un dume acheteur : id contexte {}", idContextDume);
        return checkValidContextDume(dumeRequestContext);

    }

    /**
     * This method will check if all DumeRequest associated to DumeContexteRequest are confirmed
     *
     * @param dumeRequestContext the object context
     * @return the validation status
     */
    private RetourStatus checkValidContextDume(DumeRequestContext dumeRequestContext) {
        Long idContextDume = dumeRequestContext.getId();
        // Si on utilise un dume par défaut alors le dumeRequestContext est validé
        // sinon on teste tous les dumes achteteurs
        if (!Boolean.TRUE.equals(dumeRequestContext.getDefault()) && !Boolean.TRUE.equals(dumeRequestContext.getMetadata().getSimplified())) {
            for (DumeDocument dumeDocument : dumeRequestContext.getDumeDocuments()) {

                if (!Boolean.TRUE.equals(dumeDocument.getConfirmed())) {
                    return remplirRetourStatusPourValidation(StatusDTO.NON_VALIDE, "Le DUME Acheteur n'est pas confirmé.", idContextDume);
                }

                Set<Lot> nonAssociatedLot = dumeRequestService.checkValidLot((DumeRequest) dumeDocument, dumeRequestContext.getLotSet());

                if (!CollectionUtils.isEmpty(nonAssociatedLot)) {
                    StringBuilder stringBuilder = new StringBuilder("Merci de renseigner au moins un critère pour le(s) lot(s) suivant(s) : <ul>");
                    for (Lot lot : nonAssociatedLot) {
                        stringBuilder.append("<li> Lot ").append(lot.getLotNumber()).append(" - ").append(lot.getLotName()).append("</li>");
                    }
                    stringBuilder.append("</ul>");

                    return remplirRetourStatusPourValidation(StatusDTO.NON_VALIDE, stringBuilder.toString(), idContextDume);
                }
            }
        }
        return remplirRetourStatusPourValidation(StatusDTO.VALIDE, "", idContextDume);
    }

    @Override
    public RetourStatus publishAllDumeRequest(Long idContexteDume) throws DumeResourceNotFoundException, ServiceException {

        Optional<DumeRequestContext> optional = contextRepository.findById(idContexteDume);
        if (!optional.isPresent()) {
            throw new DumeResourceNotFoundException(DUME_REQUEST_CONTEXT, idContexteDume);
        }

        DumeRequestContext dumeRequestContext = optional.get();
        populateLog(dumeRequestContext.getConsultRef());
        logger.info("Publication de tous les DUME Acheteurs : id contexte {}", idContexteDume);


        RetourStatus status = checkValidContextDume(dumeRequestContext);
        if (StatusDTO.NON_VALIDE.equals(status.getStatusDTO())) {
            throw new ServiceException("Le dume n est pas valide impossible de le publier");
        }

        for (DumeDocument dumeDocument : dumeRequestContext.getDumeDocuments()) {
            if (dumeDocument.getEtatDume() == EtatDume.PUBLIE || dumeDocument.getEtatDume() == EtatDume.STANDARD) {
                logger.warn("Le DUME Acheteur ID {} est déjà à l'état {}. Son état ne sera pas modifié.", dumeDocument.getId(), dumeDocument.getEtatDume());
            } else {
                dumeDocument.setEtatDume(Boolean.TRUE.equals(dumeRequestContext.getDefault()) ? EtatDume.STANDARD : EtatDume.A_PUBLIER);
                dumeDocument.setDateDemandePublication(LocalDateTime.now());
                dumeDocument.setNbPublicationTries(0);
            }
        }

        contextRepository.save(dumeRequestContext);
        RetourStatus retour = new RetourStatus(StatusDTO.OK, "");
        retour.setStandard(Boolean.TRUE.equals(dumeRequestContext.getDefault()));
        logger.info("Publication de tous les DUME Acheteurs : id contexte {} , retour => {}", idContexteDume, retour);
        removeLog();
        return retour;
    }

    @Override
    public PublicationStatus recupererStatusPublicationDumeAcheteur(Long idContexteDume) throws DumeResourceNotFoundException {
        Optional<DumeRequestContext> optional = contextRepository.findById(idContexteDume);
        if (!optional.isPresent()) {
            throw new DumeResourceNotFoundException(DUME_REQUEST_CONTEXT, idContexteDume);
        }
        populateLog(optional.get().getConsultRef());
        logger.info("Récupération du statut de publication d'un DUME acheteur : id contexte {}", idContexteDume);
        for (DumeDocument dumeDocument : optional.get().getDumeDocuments()) {
            if (dumeDocument.getEtatDume() != EtatDume.PUBLIE && dumeDocument.getEtatDume() != EtatDume.STANDARD) {
                logger.info(DUME_CONTEXT_NON_PUBLIE, idContexteDume);
                PublicationStatus publicationStatus = new PublicationStatus(StatusDTO.NON_PUBLIE, new ArrayList<>());
                publicationStatus.setMessage(dumeDocument.getErrors());
                publicationStatus.setStandard(Objects.equals(optional.get().getDefault(), Boolean.TRUE));
                logger.info("Récupération du statut de publication d'un DUME acheteur : id contexte {} , retour => {}", idContexteDume, publicationStatus);
                removeLog();
                return publicationStatus;
            }
        }
        PublicationStatus retour = contextMapper.toPublicationStatuts(optional.get());
        logger.info("Récupération du statut de publication d'un DUME acheteur : id contexte {} , retour => {}", idContexteDume, retour);
        removeLog();
        return retour;
    }

    @Override
    public Boolean updateDLRO(Long idContexteDumeAcheteur, ZonedDateTime dlro) throws DumeResourceNotFoundException {
        Optional<DumeRequestContext> optionalContext = contextRepository.findById(idContexteDumeAcheteur);
        if (!optionalContext.isPresent()) {
            throw new DumeResourceNotFoundException(DUME_REQUEST_CONTEXT, idContexteDumeAcheteur);
        }

        DumeRequestContext dumeRequestContext = optionalContext.get();
        populateLog(dumeRequestContext.getConsultRef());
        dumeRequestContext.getMetadata().setBidClosingDate(dlro);
        if (dumeRequestContext.getDumeDocuments().stream().allMatch(dumeDocument -> dumeDocument.getEtatDume() == EtatDume.PUBLIE)) {
            dumeRequestContext.getDumeDocuments().forEach(dumeDocument -> dumeDocument.setEtatDume(EtatDume.DLRO_MODIFIEE));
        }

        contextRepository.save(dumeRequestContext);
        removeLog();
        return Boolean.TRUE;
    }

    @Override
    public RetourStatus updateRequestContextAfterPublication(Long idRequestContext) throws DumeResourceNotFoundException, ServiceException {
        Optional<DumeRequestContext> optionalContext = contextRepository.findById(idRequestContext);
        if (!optionalContext.isPresent()) {
            throw new DumeResourceNotFoundException(DUME_REQUEST_CONTEXT, idRequestContext);
        }
        // Changement de statut des dumes Acheteurs
        DumeRequestContext dumeRequestContext = optionalContext.get();
        populateLog(dumeRequestContext.getConsultRef());
        List<DumeResponse> dumeResponses = new ArrayList<>();
        var enCoursDePublication = dumeRequestContext.getDumeDocuments()
                .stream().filter(d -> d.getEtatDume() == EtatDume.A_PUBLIER).count() > 0;
        if (enCoursDePublication) {
            throw new ServiceException("Impossible de remplacer les dumes associés au contexte " + idRequestContext + " , des dumes sont en cours de publication");
        }
        dumeRequestContext.getDumeDocuments().forEach(dumeDocument -> {
            dumeDocument.setEtatDume(EtatDume.A_REMPLACER);
            dumeResponses.addAll(dumeResponseRepository.findAllByDumeRequestIdAndEtatDume(dumeDocument.getId(), EtatDume.BROUILLON));
        });

        // Récuperation des dumeResponses en etat SUPPRIME
        if (!CollectionUtils.isEmpty(dumeResponses)) {
            dumeResponses.stream().forEach(dumeResponse -> dumeResponse.setEtatDume(EtatDume.SUPPRIME));
        }

        dumeResponseRepository.saveAll(dumeResponses);
        contextRepository.save(dumeRequestContext);
        RetourStatus retour = new RetourStatus(StatusDTO.OK, "");
        retour.setStandard(Boolean.TRUE.equals(dumeRequestContext.getDefault()));
        return retour;
    }

    @Override
    public RetourStatus checkPurgeCriteriaLot(Long idRequestContext, Set<LotDTO> lotDTOS) throws DumeResourceNotFoundException {

        DumeRequestContext dumeRequestContext = contextRepository.findById(idRequestContext).orElseThrow(() -> new DumeResourceNotFoundException(DUME_REQUEST_CONTEXT, idRequestContext));

        for (DumeDocument dumeDocument : dumeRequestContext.getDumeDocuments()) {
            if (dumeRequestService.checkPurgeCriteriaLot((DumeRequest) dumeDocument, lotDTOS)) {
                return new RetourStatus(StatusDTO.PURGE, "Il y une purge des criteriasLots pour le context " + dumeRequestContext.getId());
            }
        }
        return new RetourStatus(StatusDTO.NOT_PURGE, "Il n'y aura aucune purge des criteriasLots");
    }

    private RetourStatus remplirRetourStatusPourValidation(StatusDTO statusDTO, String message, Long idContext) {
        RetourStatus retour = new RetourStatus(statusDTO, message);
        logger.info("Vérification de la validation d'un dume acheteur : id contexte {} , retour => {}", idContext, retour);
        removeLog();
        return retour;
    }

    @Override
    public byte[] downloadPdf(Long identifierDumeContext) throws DumeResourceNotFoundException, ServiceException {
        DumeRequestContext dumeRequestContext = contextRepository.findById(identifierDumeContext).orElseThrow(() -> new DumeResourceNotFoundException(DUME_REQUEST_CONTEXT, identifierDumeContext));
        try {
            for (DumeDocument dumeDocument : dumeRequestContext.getDumeDocuments()) {
                populateLog(dumeRequestContext.getConsultRef());
                String xml = dumeRequestService.generateDumeRequestXML(dumeDocument.getId());
                SNRequestDTO snDownloadRequestDTO = snRequestMapper.
                        toSnDownlaodResponsePDF(new String(Base64Utils.encode(xml.getBytes())), dumeRequestContext);
                logger.info("Téléchargement du PDF depuis le SN pour le Dume A : {}", identifierDumeContext);
                SNDumeSavedResponseDTO response = (SNDumeSavedResponseDTO) nationalServiceRequestService.restAcheteur(Optional.of(dumeRequestContext).map(DumeRequestContext::getPlatform).map(DumePlatform::getId).orElse(null), snDownloadRequestDTO, SNDumeSavedResponseDTO.class).getBody();

                if (response != null && response.getResponse() != null && response.getResponse().getDumeA() != null) {
                    removeLog();
                    return Base64Utils.decode(response.getResponse().getDumeA().getBytes());
                }
            }
        } catch (NationalServiceException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        throw new ServiceException("Génération du PDF Impossible");
    }
}
