package com.atexo.dume.services.impl;

import com.atexo.dume.dto.sn.response.SNOauth2Token;
import com.atexo.dume.model.DumePlatform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class Oauth2Client {

    private final Logger logger = LoggerFactory.getLogger(Oauth2Client.class);

    @Value("${SN_OAUTH2_PATH}")
    String snOauth2Path;

    @Autowired
    RestTemplate restTemplate;

    private static final Map<DumePlatform, SNOauth2Token> tokens = new HashMap<>();

    public SNOauth2Token getOauth2Token(DumePlatform platform) {
        var token = tokens.get(platform);
        if (token != null && token.getExpiresIn() != null && token.getDateRecuperation() != null) {
            var diff = token.getDateRecuperation().until(LocalDateTime.now(), ChronoUnit.SECONDS);
            if (diff < token.getExpiresIn()) {
                logger.info("Récupération du token Oauth2 depuis le cache");
                return token;
            }
        }
        logger.info("Récupération du token Oauth2 depuis le serveur Oauth2");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        var map = new LinkedMultiValueMap<String, String>();
        map.add("client_id", platform.getSnOauth2ClientId());
        map.add("client_secret", platform.getSnOauth2ClientSecret());
        map.add("grant_type", "client_credentials");
        var request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        token = restTemplate.postForEntity(snOauth2Path, request, SNOauth2Token.class).getBody();
        token.setDateRecuperation(LocalDateTime.now());
        tokens.put(platform, token);
        if (logger.isDebugEnabled()) {
            logger.debug("Token Oauth2 récupéré : {}", token);
        }
        return token;
    }

}
