package com.atexo.dume.services.impl;

import com.atexo.dume.dto.CriteriaLotDTO;
import com.atexo.dume.dto.ExclusionCriteriaDTO;
import com.atexo.dume.dto.LotDTO;
import com.atexo.dume.dto.OtherCriteriaDTO;
import com.atexo.dume.dto.SelectionCriteriaDTO;
import com.atexo.dume.mapper.CriteriaMapper;
import com.atexo.dume.model.CriteriaSelectionDocument;
import com.atexo.dume.model.DumeRequest;
import com.atexo.dume.model.ExclusionCriteria;
import com.atexo.dume.model.OtherCriteria;
import com.atexo.dume.model.SelectionCriteria;
import com.atexo.dume.repository.CriteriaExclusionRepository;
import com.atexo.dume.repository.CriteriaOtherRepository;
import com.atexo.dume.repository.CriteriaSelectionDocumentRepository;
import com.atexo.dume.repository.CriteriaSelectionRepository;
import com.atexo.dume.repository.LotRepository;
import com.atexo.dume.services.api.CriteriaService;
import com.atexo.dume.services.exception.DumeResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CriteriaServiceImpl implements CriteriaService {

    @Autowired
    private CriteriaSelectionRepository criteriaSelectionRepository;

    @Autowired
    private CriteriaExclusionRepository criteriaExclusionRepository;

    @Autowired
    private CriteriaOtherRepository criteriaOtherRepository;

    @Autowired
    private CriteriaMapper criteriaMapper;

    @Autowired
    private LotRepository lotRepository;

    @Autowired
    private CriteriaSelectionDocumentRepository criteriaSelectionDocumentRepository;

    @Override
    public List<SelectionCriteriaDTO> getAllSelectionCriteriaDTO() {

        return criteriaMapper.toSelectionDTOList(criteriaSelectionRepository.findAll());
    }

    @Override
    public List<ExclusionCriteriaDTO> getAllExclusionCriteriaDTO() {
        return criteriaMapper.toExclusionDTOList(criteriaExclusionRepository.findAll());
    }

    @Override
    public List<OtherCriteriaDTO> getAllOtherCriteriaDTO() {
        return criteriaOtherRepository.findAll()
                .stream()
                .map(criteriaMapper::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public List<SelectionCriteria> getAllSelectionCriteriaWithoutALLCriteria() {

        return criteriaSelectionRepository.findAllByCodeNotLike("CRITERION.SELECTION.ALL_SATISFIED");
    }

    @Override
    public List<ExclusionCriteria> getAllExclusionCriteria() {
        return criteriaExclusionRepository.findAll();
    }

    @Override
    public List<SelectionCriteria> getAllSelectionCriteriaByCodeIn(List<String> codes) {
        return criteriaSelectionRepository.findByCodeIn(codes);
    }

    @Override
    public SelectionCriteria getCriteriaSelectionByCode(String code) {
        return criteriaSelectionRepository.findByCode(code);
    }

    @Override
    public ExclusionCriteria getCriteriaExclusionByCode(String code) {
        return criteriaExclusionRepository.findByCode(code);
    }

    @Override
    public OtherCriteria getCriteriaOtherByCode(String code) {
        return criteriaOtherRepository.findByCode(code);
    }

    @Override
    public CriteriaSelectionDocument getCriteriaSelection(DumeRequest dumeRequest, CriteriaLotDTO criteriaLotDTO) throws DumeResourceNotFoundException {

        SelectionCriteria criteria = getCriteriaSelectionByCode(criteriaLotDTO.getCode());
        if (null == criteria) {
            return null;
        }

        CriteriaSelectionDocument criteriaSelectionDocument = criteriaSelectionDocumentRepository.findByCriteriaCodeAndDumeDocumentId(criteriaLotDTO.getCode(), dumeRequest.getId()).
                orElse(new CriteriaSelectionDocument(getCriteriaSelectionByCode(criteriaLotDTO.getCode()), dumeRequest));

        criteriaSelectionDocument.getLotList().clear();
        for (LotDTO lotDTO : criteriaLotDTO.getLots()) {
            criteriaSelectionDocument.getLotList().
                    add(lotRepository.findById(lotDTO.getInternalLotIdentifier())
                            .orElseThrow(() ->
                                    new DumeResourceNotFoundException("le lot {} n'existe pas", lotDTO.getInternalLotIdentifier())));
        }

        return criteriaSelectionDocument;
    }

    @Override
    public List<SelectionCriteriaDTO> getAllSelectionCriteriaDTOByNature(String idNatureMarket) {
        return criteriaMapper.toSelectionDTOList(criteriaSelectionRepository.findAllByNatureMarketIdentifierIsNullOrNatureMarketIdentifierEquals(idNatureMarket));
    }
}
