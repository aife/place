package com.atexo.dume.services.impl.scheduledtasks;

import com.atexo.dume.services.api.DonneesEssentiellesService;
import com.atexo.dume.services.api.DumeRequestService;
import com.atexo.dume.services.api.DumeResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Classe regroupant toutes les méthodes appelées périodiquement (routines)
 * Le nombre de threads disponibles pour ces routines est configuré dans SchedulingConfig. Attention à étendre
 * ce pool de threads si on ajoute d'autres routines.
 */
@Component
public class ScheduledTasksLauncher {

    @Autowired
    DumeResponseService dumeResponseService;

    @Autowired
    DumeRequestService dumeRequestService;

    @Autowired
    DonneesEssentiellesService donneesEssentiellesService;


    @Scheduled(cron = "${cron.expression.acheteur}")
    public void routineAcheteur() {

        dumeRequestService.updateDLROIntoSN();
        dumeRequestService.remplaceDumeWithSN();
        dumeRequestService.publishAllDumeRequests();
        dumeRequestService.retrieveAllDumeRequestsPDF();
        dumeRequestService.downloadTemplateOE();
    }

    @Scheduled(cron = "${cron.expression.oe}")
    public void routineOE() {

        dumeResponseService.publishAllDumeResponses();
        dumeResponseService.retrieveAllDumesResponsePDF();
    }

    @Scheduled(cron = "${cron.expression.donneesEssentielles}")
    public void routineDonneesEssentielles() {

        donneesEssentiellesService.publishAllDonneesEssentielles();
    }
}
