package com.atexo.dume.services.impl;

import com.atexo.dume.dto.sn.request.SNECertisDTO;
import com.atexo.dume.dto.sn.request.SNEssentialDataDTO;
import com.atexo.dume.dto.sn.request.SNRequestDTO;
import com.atexo.dume.repository.DumePlatformRepository;
import com.atexo.dume.services.api.NationalServiceRequestService;
import com.atexo.dume.services.exception.NationalServiceException;
import com.atexo.dume.toolbox.rest.NationalServiceResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import static com.atexo.dume.toolbox.logging.LoggerUtils.toJSON;
import static com.atexo.dume.toolbox.rest.NationalServiceResource.*;

@Service
public class NationalServiceRequestServiceImpl implements NationalServiceRequestService {

	private final Logger logger = LoggerFactory.getLogger(NationalServiceRequestServiceImpl.class);

	@Value("${SN_BASE_PATH}")
	private String snBasePath;

	@Value("${SN_OAUTH2_ENABLED}")
	private boolean oauth2Enabled;


	@Autowired
	private Oauth2Client oauth2Client;

	@Autowired
	private DumePlatformRepository platformRepository;

	@Autowired
	private RestTemplate restTemplate;



	@Override
	public ResponseEntity restAcheteur(Long idPlatform, SNRequestDTO snRequestDTO, Class clazz) throws NationalServiceException {
		return processSNRequest(idPlatform, snRequestDTO, DUME_REQUEST, clazz);
	}

	@Override
	public ResponseEntity restOE(Long idPlatform, SNRequestDTO snRequestDTO, Class clazz) throws NationalServiceException {
		return processSNRequest(idPlatform, snRequestDTO, DUME_RESPONSE, clazz);
	}

	@Override
	public ResponseEntity restMetadonnees(Long idPlatform, SNRequestDTO snRequestDTO, Class clazz) throws NationalServiceException {
		return processSNRequest(idPlatform, snRequestDTO, METADATA, clazz);
	}

	@Override
	public <T> ResponseEntity<T> restEssentialData(Long idPlatform, SNEssentialDataDTO snEssentialDataDTO, Class clazz) throws NationalServiceException {
		return processSNRequest(idPlatform, snEssentialDataDTO, ESSENTIAL_DATA, clazz);
	}

	@Override
	public <T> ResponseEntity<T> restECertis(Long idPlatform, SNECertisDTO snRequestDTO, Class clazz) throws NationalServiceException {
		return processSNRequest(idPlatform, snRequestDTO, E_CERTIS, clazz);
	}

	private ResponseEntity processSNRequest(Long idPlatfom, SNRequestDTO snRequestDTO, NationalServiceResource resource, Class clazz) throws NationalServiceException {
		try {
			var platform = platformRepository.findById(idPlatfom).orElseThrow(() -> new NationalServiceException("Plateforme introuvable"));
			logger.info("Communication avec le SN endpoint {} , opération : {} ", resource, snRequestDTO.getOperation());
			if (logger.isDebugEnabled()) {
				String jsonSend = toJSON(snRequestDTO);
				logger.debug("/{} : json envoyé {}", resource, jsonSend);
			}
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			if (oauth2Enabled) {
				logger.info("Appel du SN en mode sécurisé");
				var token = oauth2Client.getOauth2Token(platform);
				if (token == null || token.getAccessToken() == null) {
					throw new NationalServiceException("Impossible de s'authentifier au SN en utilisant Oauth2 token = " + token);
				}
				headers.set("Authorization", "Bearer " + token.getAccessToken());
			} else {
				logger.warn("le SN sera appelé en mode non sécurisé");
			}
			headers.setContentType(MediaType.APPLICATION_JSON);
			var request = new HttpEntity<>(snRequestDTO, headers);
			var response = restTemplate.postForEntity(snBasePath + resource.getResource(), request, clazz);
			if (logger.isDebugEnabled() && response != null) {
				String jsonRecieved = toJSON(response.getBody());
				logger.debug("/{} : json reçu {}", resource, jsonRecieved);
			}
			return response;
		} catch (RestClientException e) {
			logger.error(e.getMessage(), e);
			throw new NationalServiceException(e);
		}
	}


}
