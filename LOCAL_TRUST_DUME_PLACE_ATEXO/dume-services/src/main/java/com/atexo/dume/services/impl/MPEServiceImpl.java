package com.atexo.dume.services.impl;

import com.atexo.dume.dto.donneesessentielles.xml.MarchesType;
import com.atexo.dume.dto.donneesessentielles.xml.TicketType;
import com.atexo.dume.model.DumePlatform;
import com.atexo.dume.services.api.MPEService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;

@Service
public class MPEServiceImpl implements MPEService {

    private final Logger logger = LoggerFactory.getLogger(MPEServiceImpl.class);

    private static final String API_ENDPPOINT = "/app.php/api/v1";
    private static final String CONNEXION_ENDPOINT = "/api.php/ws/authentification/connexion/";
    private static final String FORMAT_ETENDU_END_POINT = API_ENDPPOINT + "/donnees-essentielles/contrat/format-etendu";

    @Override
    public MarchesType getModifiedContract(DumePlatform platform) {
        // le rest template autowired passe par le proxy , le SSL
        RestTemplate restTemplate = new RestTemplate();

        // get token
        UriComponentsBuilder connexionBuilder = UriComponentsBuilder.fromHttpUrl(platform.getUrl()).path(CONNEXION_ENDPOINT).path(platform.getLogin()).path("/" + platform.getPassword());

        try {
            TicketType token = restTemplate.getForObject(connexionBuilder.toUriString(), TicketType.class);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_XML));

            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.add("token", token.getValue());
            map.add("statut_publication_sn", "0");
            map.add("version", "v1");
            map.add("format", "xml");

            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
            UriComponentsBuilder donneesEssentiellesBuilder = UriComponentsBuilder.fromHttpUrl(platform.getUrl()).path(FORMAT_ETENDU_END_POINT);
            ResponseEntity<MarchesType> response = restTemplate.postForEntity(donneesEssentiellesBuilder.toUriString(), request, MarchesType.class);
            return response.getBody();
        }
        catch (RestClientException ex) {
            logger.error("Erreur lors de la communication avec MPE",ex);
            return null;
        }
    }
}
