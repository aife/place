package com.atexo.dume.services.impl;

import com.atexo.dume.model.DumeDomain;
import com.atexo.dume.model.DumePlatform;
import com.atexo.dume.model.profile.DumeDomainPropValue;
import com.atexo.dume.model.profile.DumePlatPropValue;
import com.atexo.dume.model.profile.DumeProfileProp;
import com.atexo.dume.model.profile.DumeUsrPropValue;
import com.atexo.dume.model.profile.pk.DumeDomPropValuePK;
import com.atexo.dume.model.profile.pk.DumePlatPropValuePK;
import com.atexo.dume.repository.DumeDomPropValueRepository;
import com.atexo.dume.repository.DumePlatPropValueRepository;
import com.atexo.dume.repository.DumeUsrPropValueRepository;
import com.atexo.dume.repository.ProfileParamValueRepository;
import com.atexo.dume.services.api.ProfileParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.validation.constraints.NotNull;
import java.util.Optional;

import static com.atexo.dume.model.profile.ProfileParamQueryHolder.domainPredicate;
import static com.atexo.dume.model.profile.ProfileParamQueryHolder.platformPredicate;
import static com.atexo.dume.model.profile.ProfileParamQueryHolder.userPredicate;


@Service
public class ProfileParamServiceImpl implements ProfileParamService {


    @Autowired
    private DumeDomPropValueRepository domPropValueRepository;


    @Autowired
    private DumePlatPropValueRepository platPropValueRepository;

    @Autowired
    private ProfileParamValueRepository profileParamRepository;

    @Autowired
    private DumeUsrPropValueRepository usrValueRepository;



    /**
     * This method will get the value of dume property as defined at domain level
     *
     * @param key           property key
     * @param domIdentifier the dom identifier
     * @return String value of the property
     */
    @Override
    public String getDomProfileValue(final String key, final Long domIdentifier) {
        return getProfileParamValue(key, null, null, domIdentifier);
    }

    /**
     * This method will get the value of dume property as defined at platform level
     *
     * @param key            property key
     * @param platIdentifier the platform identifier
     * @return String value of the property
     */
    @Override
    public String getPlatProfileValue(final String key, final Long platIdentifier) {
        return getProfileParamValue(key, null, platIdentifier, null);
    }


    /**
     * This method will get the value of dume property as defined at user level
     *
     * @param key           property key
     * @param usrIdentifier the user identifier
     * @return String value of the property
     */
    @Override
    public String getUsrProfileValue(final String key, final Long usrIdentifier) {
        return getProfileParamValue(key, usrIdentifier, null, null);
    }


    /**
     * This method will get the value of dume property using priority logic usr>platform>domain
     *
     * @param key                property key
     * @param usrIdentifier      the user identifier
     * @param platformIdentifier the platform identifier
     * @param domainIdentifier   the domain identifier
     * @return String value of the property
     */
    @Override
    public String getValue(final String key, final Long usrIdentifier, final Long platformIdentifier, final Long domainIdentifier) {
        return getProfileParamValue(key, usrIdentifier, platformIdentifier, domainIdentifier);
    }


    /**
     * This method will get the value of dume property using priority logic usr>platform>domain
     *
     * @param key                property key
     * @param userIdentifier     the user identifier
     * @param platformIdentifier the platform identifier
     * @param domainIdentifier   the domain identifier
     * @return String value of the property
     */
    private String getProfileParamValue(@NonNull final String key, final Long userIdentifier, final Long platformIdentifier, final Long domainIdentifier) {

        final DumeProfileProp prop = profileParamRepository.findByPropLabel(key);
        if (null == prop) {
            throw new IllegalArgumentException("Key not found !");
        } else {
            if (null != userIdentifier && Boolean.TRUE.equals(prop.getUsr())) {
                final Optional<DumeUsrPropValue> usrPropValue = usrValueRepository.findOne(userPredicate(prop.getId(), userIdentifier.intValue()));
                if (usrPropValue.isPresent()) {
                    return usrPropValue.get().getValue();
                }
            }
            if (null != platformIdentifier && Boolean.TRUE.equals(prop.getPlatform())) {
                final Optional<DumePlatPropValue> platPropValue = platPropValueRepository.findOne(platformPredicate(prop.getId(), platformIdentifier.intValue()));
                if (platPropValue.isPresent()) {
                    return platPropValue.get().getValue();
                }
            }
            if (null != domainIdentifier && Boolean.TRUE.equals(prop.getDomain())) {
                final Optional<DumeDomainPropValue> domPropValue = domPropValueRepository.findOne(domainPredicate(prop.getId(), domainIdentifier.intValue()));
                if (domPropValue.isPresent()) {
                    return domPropValue.get().getValue();
                }
            }
        }
        return null;
    }


    /**
     * This method will add a new DumeProfileParam to DB.
     * The DumeProfileParam to be saved need at least one value at domain level and to hava isDomain level as TRUE.
     *
     * @param profileProp {@see DumeProfileProp} Object.
     * @return {@see DumeProfileProp} with the db identifier.
     */
    @Override
    public DumeProfileProp createDumeProfileProperty(@NonNull final DumeProfileProp profileProp) {
        if (null == profileProp.getDomain() || !profileProp.getDomain()) {
            throw new IllegalStateException("Profile Property need to be at least Domain level !");
        }
        if (CollectionUtils.isEmpty(profileProp.getDumeDomainPropValues())) {
            throw new IllegalStateException("At least domain level value need to be defined !");
        }
        return profileParamRepository.save(profileProp);
    }

    @Override
    public DumeDomainPropValue getDomPropValueOrCreateByDumeProfileProp(String label, @NotNull DumeDomain dumeDomain) {
        DumeProfileProp prop = profileParamRepository.findByPropLabel(label);
        if (prop == null) {
            throw new IllegalArgumentException("La propriéte n'existe pas");
        }
        DumeDomainPropValue dumeDomainPropValue;
        Optional<DumeDomainPropValue> dumeDomainPropValueOptional = domPropValueRepository.findOne(domainPredicate(prop.getId(), dumeDomain.getId().intValue()));
        if (!dumeDomainPropValueOptional.isPresent()) {
            dumeDomainPropValue = new DumeDomainPropValue();
            dumeDomainPropValue.setDumeDomain(dumeDomain);
            dumeDomainPropValue.setDumeProfileProp(prop);
            dumeDomainPropValue.setId(new DumeDomPropValuePK(dumeDomain.getId().intValue(), prop.getId().intValue()));
            dumeDomainPropValue = domPropValueRepository.save(dumeDomainPropValue);
        } else {
            dumeDomainPropValue = dumeDomainPropValueOptional.get();
        }
        return dumeDomainPropValue;
    }

    @Override
    public DumePlatPropValue getPlatPropValueOrCreateByDumeProfileProp(String label, @NotNull DumePlatform dumePlatform) {

        DumeProfileProp prop = profileParamRepository.findByPropLabel(label);
        if (prop == null) {
            throw new IllegalArgumentException("La propriéte n'existe pas");
        }
        DumePlatPropValue dumePlatPropValue;
        Optional<DumePlatPropValue> dumePlatPropValueOptional = platPropValueRepository.findOne(platformPredicate(prop.getId(), dumePlatform.getId().intValue()));
        if (!dumePlatPropValueOptional.isPresent()) {
            dumePlatPropValue = new DumePlatPropValue();
            dumePlatPropValue.setDumePlatform(dumePlatform);
            dumePlatPropValue.setDumeProfileProp(prop);
            dumePlatPropValue.setId(new DumePlatPropValuePK(dumePlatform.getId().intValue(), prop.getId().intValue()));
            dumePlatPropValue = platPropValueRepository.save(dumePlatPropValue);
        } else {
            dumePlatPropValue = dumePlatPropValueOptional.get();
        }
        return dumePlatPropValue;
    }
}
