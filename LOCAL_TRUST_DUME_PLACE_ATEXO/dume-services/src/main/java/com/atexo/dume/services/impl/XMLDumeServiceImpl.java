package com.atexo.dume.services.impl;

import com.atexo.dume.mapper.DumeRequestMapper;
import com.atexo.dume.mapper.DumeResponseMapper;
import com.atexo.dume.model.Criteria;
import com.atexo.dume.model.CriteriaSelectionDocument;
import com.atexo.dume.model.DumeRequest;
import com.atexo.dume.model.DumeResponse;
import com.atexo.dume.repository.CriteriaOtherRepository;
import com.atexo.dume.repository.CriteriaSelectionDocumentRepository;
import com.atexo.dume.services.api.XMLDumeService;
import grow.names.specification.ubl.schema.xsd.espdrequest_1.ESPDRequestType;
import grow.names.specification.ubl.schema.xsd.espdresponse_1.ESPDResponseType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class XMLDumeServiceImpl implements XMLDumeService {

    private static final Logger logger = LoggerFactory.getLogger(XMLDumeServiceImpl.class);

    @Autowired
    private CriteriaSelectionDocumentRepository criteriaSelectionDocumentRepository;

    @Autowired
    private DumeRequestMapper dumeRequestMapper;

    @Autowired
    private DumeResponseMapper dumeResponseMapper;

    @Autowired
    private Jaxb2Marshaller jaxb2Marshaller;

    @Autowired
    private CriteriaOtherRepository criteriaOtherRepository;

    @Autowired
    private grow.names.specification.ubl.schema.xsd.espdrequest_1.ObjectFactory objectFactoryRequest;

    @Autowired
    private grow.names.specification.ubl.schema.xsd.espdresponse_1.ObjectFactory objectFactoryResponse;


    @Override
    public String generateXMLAcheteur(DumeRequest dumeRequest) {
        StringWriter stringWriter = new StringWriter();
        logger.info("Génération du XML pour le dume acheteur ID : {}", dumeRequest.getId());
        ESPDRequestType espdRequestType = dumeRequestMapper.toFinalEspd(dumeRequest);
        jaxb2Marshaller.marshal(objectFactoryRequest.createESPDRequest(espdRequestType), new StreamResult(stringWriter));
        return stringWriter.toString();
    }


    @Override
    public String generateXMLOE(DumeResponse dumeResponse) {
        List<Criteria> criteriaList = criteriaSelectionDocumentRepository.findByDumeDocument(dumeResponse.getDumeRequest()).stream()
                .filter(criteriaSelectionDocument -> CollectionUtils.isEmpty(criteriaSelectionDocument.getLotList()) || CollectionUtils.containsAny(criteriaSelectionDocument.getLotList(), dumeResponse.getLotSet()))
                .map(CriteriaSelectionDocument::getCriteria)
                .collect(Collectors.toList());

        criteriaList.addAll(dumeResponse.getDumeRequest().getExclusionCriteriaList());
        criteriaList.addAll(criteriaOtherRepository.findAll());
        StringWriter stringWriter = new StringWriter();
        logger.info("Génération du XML pour le dume OE ID : {}", dumeResponse.getId());
        ESPDResponseType espdResponseType = dumeResponseMapper.toFinalResponse(dumeResponse, criteriaList);
        jaxb2Marshaller.marshal(objectFactoryResponse.createESPDResponse(espdResponseType), new StreamResult(stringWriter));
        return stringWriter.toString();
    }
}
