package com.atexo.dume.services.impl;

import com.atexo.dume.dto.donneesessentielles.xml.MarcheType;
import com.atexo.dume.dto.sn.request.SNEssentialDataDTO;
import com.atexo.dume.dto.sn.response.SNDumeSavedResponseDTO;
import com.atexo.dume.mapper.sn.SNEssentialDataMapper;
import com.atexo.dume.model.DumePlatform;
import com.atexo.dume.model.donneesessentielles.Contract;
import com.atexo.dume.model.donneesessentielles.EtatContract;
import com.atexo.dume.model.donneesessentielles.UpdateType;
import com.atexo.dume.repository.ContractRepository;
import com.atexo.dume.repository.DumePlatformRepository;
import com.atexo.dume.services.api.ContractService;
import com.atexo.dume.services.api.NationalServiceRequestService;
import com.atexo.dume.services.exception.NationalServiceException;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.Optional;

import static com.atexo.dume.model.donneesessentielles.UpdateType.*;
import static com.atexo.dume.toolbox.logging.LoggerUtils.toJSON;
import static com.atexo.dume.toolbox.rest.NationalServiceOperations.UPDATE_ESSENTIAL_DATA;


@Service
public class ContractServiceImpl implements ContractService {

    private static final Logger logger = LoggerFactory.getLogger(ContractServiceImpl.class);

    @Autowired
    private ContractRepository contractRepository;

    @Autowired
    private SNEssentialDataMapper snEssentialDataMapper;

    @Autowired
    private NationalServiceRequestService nationalServiceRequestService;

    @Autowired
    private DumePlatformRepository platformRepository;


    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void processContract(MarcheType marche, Long idPlatForm) {
        DumePlatform platform = platformRepository.getOne(idPlatForm);
        Contract contractToPublish = null;
        if (marche.getId().endsWith("00")) {
            contractToPublish = contractRepository.getContractByUuid(marche.getUuid()).orElseGet(() -> getNewContract(marche.getId(), marche.getUuid(), platform));
        } else {
            contractToPublish = contractRepository.getContractByContractNumberAndContractOriginUuid(marche.getId(), marche.getUuid()).orElseGet(() -> getNewContract(marche.getId(), marche.getUuid(), platform));
        }
        if (contractToPublish != null) {

            if (!EtatContract.PUBLIE.equals(contractToPublish.getEtatContract())) {
                try {
                    SNEssentialDataDTO snEssentialDataRequest;
                    if (EtatContract.EN_ATTENTE == contractToPublish.getEtatContract() || (EtatContract.ERREUR_SN == contractToPublish.getEtatContract() && contractToPublish.getContractOrigin() == null)) {
                        snEssentialDataRequest = snEssentialDataMapper.toFinalSNSaveEssentialData(platform, marche, contractToPublish.getConsutationId());

                    } else {
                        snEssentialDataRequest = snEssentialDataMapper.toFinalSNUpdateEssentialData(platform, marche, contractToPublish.getSnNumber());
                    }
                    contractToPublish.setDateDemandePublication(LocalDateTime.now());
                    SNDumeSavedResponseDTO responseDTO = (SNDumeSavedResponseDTO) nationalServiceRequestService.restEssentialData(Optional.of(contractToPublish).map(Contract::getPlatform).map(DumePlatform::getId).orElse(null), snEssentialDataRequest, SNDumeSavedResponseDTO.class).getBody();
                    processSNResponse(contractToPublish, snEssentialDataRequest.getOperation(), snEssentialDataRequest, responseDTO);
                } catch (NationalServiceException e) {
                    contractToPublish.setEtatContract(EtatContract.ERREUR_SN);
                    contractToPublish.setErrors(e.getMessage());
                } finally {
                    contractRepository.save(contractToPublish);
                }
            } else {
                logger.info("Le contrat {} est déja publié , numSN: {}", contractToPublish.getContractNumber(), contractToPublish.getSnNumber());
            }
        }
    }

    /**
     * This method will process the response received from the Sn
     *
     * @param contractToPublish
     * @param operation         Updated or Creation
     * @param responseDTO       the response to be treated
     */
    private void processSNResponse(Contract contractToPublish, String operation, SNEssentialDataDTO request, SNDumeSavedResponseDTO responseDTO) {
        if (UPDATE_ESSENTIAL_DATA.equals(operation)) {
            if (responseDTO.getResponse().getMessageList().stream().anyMatch(snMessageList -> "MiseAJour".equals(snMessageList.getCode()))) {
                contractToPublish.setEtatContract(EtatContract.PUBLIE);
                contractToPublish.setDatePublication(LocalDateTime.now());
            } else {
                contractToPublish.setEtatContract(EtatContract.ERREUR_SN);
                logger.error("Erreur lors de la publication des données essentielles pour le contrat {}\n{}", contractToPublish.getContractNumber(), responseDTO.getResponse().getMessageList());
                logger.error("JSON envoyé : {}", toJSON(request));
                contractToPublish.setErrors(responseDTO.getResponse().getMessageList().toString());
            }

        } else {
            if (!CollectionUtils.isEmpty(responseDTO.getResponse().getMessageList()) && responseDTO.getResponse().getRefFonctionnelleDE() == null) {

                contractToPublish.setEtatContract(EtatContract.ERREUR_SN);
                logger.error("Erreur lors de la publication des données essentielles pour le contrat {}\n{}", contractToPublish.getContractNumber(), responseDTO.getResponse().getMessageList());
                logger.error("JSON envoyé : {}", toJSON(request));
                contractToPublish.setErrors(responseDTO.getResponse().getMessageList().toString());
            } else {
                if (EtatContract.EN_ATTENTE == contractToPublish.getEtatContract() || (EtatContract.ERREUR_SN == contractToPublish.getEtatContract() && contractToPublish.getContractOrigin() == null)) {
                    contractToPublish.setSnNumber(responseDTO.getResponse().getRefFonctionnelleDE());
                }
                contractToPublish.setDatePublication(LocalDateTime.now());
                contractToPublish.setEtatContract(EtatContract.PUBLIE);
            }
        }
    }

    /**
     * This method create a new Contrat if not found, the contract's ID is splitted in three case :
     * <ol>
     *     <li>xxxx00 : initial contract</li>
     *     <li>xxxx0y : (y >0) update of the contract xxxx00</li>
     *     <li>xxxx_0y : (y >0) update of the annual execution data of the contract xxxx00</li>
     * </ol>
     *
     * @param idContract the contract Id retrieved from the marche
     * @param initialContractUuid the initial contract uuid retrieved from the marche
     * @param platform   the platform associated
     * @return
     */
    private Contract getNewContract(String idContract, String initialContractUuid, DumePlatform platform) {

        Contract contractorigin = null;
        Contract contract = new Contract();
        UpdateType updateType = INITIALISATION;
        if (!idContract.endsWith("00")) {
            updateType = MODIFICATION;
            // cas d'une mise à jour des données annuelles d'exécution;
            if (idContract.lastIndexOf('_') != -1) {
                updateType = MAJ_DONNEES_EXECUTION;
            }
            contractorigin = contractRepository.getContractByUuid(initialContractUuid).orElse(null);
        } else {
            contract.setUuid(initialContractUuid);
        }
        contract.setUpdateType(updateType);
        contract.setContractNumber(idContract);
        contract.setPlatform(platform);
        contract.setEtatContract(contractorigin == null ? EtatContract.EN_ATTENTE : EtatContract.MISE_A_JOUR);
        contract.setConsutationId(RandomStringUtils.randomAlphanumeric(13));
        contract.setContractOrigin(contractorigin);
        return contract;
    }
}
