package com.atexo.dume.services.impl;

import com.atexo.dume.dto.CriteriaLotDTO;
import com.atexo.dume.dto.DumeRequestContextDTO;
import com.atexo.dume.dto.DumeRequestDTO;
import com.atexo.dume.dto.LotDTO;
import com.atexo.dume.mapper.DumeRequestMapper;
import com.atexo.dume.model.CriteriaSelectionDocument;
import com.atexo.dume.model.DumeRequest;
import com.atexo.dume.model.DumeRequestContext;
import com.atexo.dume.model.DumeRequestModel;
import com.atexo.dume.model.EtatDume;
import com.atexo.dume.model.Lot;
import com.atexo.dume.repository.DumeRequestModelRepository;
import com.atexo.dume.repository.DumeRequestRepository;
import com.atexo.dume.services.api.CriteriaService;
import com.atexo.dume.services.api.DumeRequestService;
import com.atexo.dume.services.api.ProfileParamService;
import com.atexo.dume.services.api.SchedularTask;
import com.atexo.dume.services.api.XMLDumeService;
import com.atexo.dume.services.exception.DumeResourceNotFoundException;
import com.atexo.dume.services.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.oxm.MarshallingException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atexo.dume.toolbox.consts.DomainEnum.DOM_ATEXO;
import static com.atexo.dume.toolbox.consts.DumeProfilePropEnum.MODEL_DUME_REQUEST;
import static com.atexo.dume.toolbox.logging.LoggerUtils.populateLog;
import static com.atexo.dume.toolbox.logging.LoggerUtils.removeLog;

/**
 * This class is the implementation of the the {@link DumeRequestService}.
 * it contains all possible operations for user entities.
 */
@Service
public class DumeRequestServiceImpl implements DumeRequestService {

    private static final Logger logger = LoggerFactory.getLogger(DumeRequestServiceImpl.class);
    public static final String DUME_REQUEST = "DumeRequest";


    @Autowired
    private DumeRequestRepository dumeRequestRepository;

    @Autowired
    private CriteriaService criteriaService;

    @Autowired
    private DumeRequestMapper dumeRequestMapper;

    @Autowired
    private ProfileParamService profileParamService;


    @Autowired
    private DumeRequestModelRepository dumeRequestModelRepository;

    @Autowired
    private XMLDumeService xmlDumeService;

    @Autowired
    private SchedularTask schedularTask;

    @Value("${PDF_STORAGE_FOLDER}")
    private String pdfStorageFolder;

    @Override
    public DumeRequest createDumeRequest(DumeRequestContext context) throws ServiceException, DumeResourceNotFoundException {
        String idModel = profileParamService.getValue(MODEL_DUME_REQUEST.getLabel(), null, context.getPlatform().getId(), DOM_ATEXO.getId());

        if (idModel == null) {
            throw new ServiceException("Il n'y aucun model pour créer un DUME Acheteur");
        }

        Optional<DumeRequestModel> optional = dumeRequestModelRepository.findById(Long.valueOf(idModel));
        if (!optional.isPresent()) {
            logger.error("Le model avec l'id {} n'existe pas.", idModel);
            throw new DumeResourceNotFoundException("DumeRequestModel", idModel);
        }
        DumeRequest dumeRequest = new DumeRequest();
        dumeRequest.getExclusionCriteriaList().addAll(optional.get().getExclusionCriteriaList());
        List<CriteriaSelectionDocument> criteriaSelectionDocuments = new ArrayList<>();

        for (CriteriaSelectionDocument criteriaSelectionDocument : optional.get().getSelectionCriteriaList()) {
            criteriaSelectionDocuments.add(new CriteriaSelectionDocument(criteriaSelectionDocument.getCriteria(), dumeRequest));
        }
        dumeRequest.getSelectionCriteriaList().addAll(criteriaSelectionDocuments);
        dumeRequest.setContext(context);

        return dumeRequest;
    }

    @Override
    public DumeRequestDTO getDumeRequestDTOById(Long id) throws DumeResourceNotFoundException {
        Optional<DumeRequest> optionalDumeRequest = dumeRequestRepository.findById(id);
        if (!optionalDumeRequest.isPresent()) {
            throw new DumeResourceNotFoundException(DUME_REQUEST, id);
        }
        return dumeRequestMapper.toDto(optionalDumeRequest.get());

    }

    @Override
    public Long updateContextDumeRequestById(Long id, DumeRequestContextDTO context) {
        Optional<DumeRequest> optionalDumeRequest = dumeRequestRepository.findById(id);
        if (optionalDumeRequest.isPresent()) {
            DumeRequest dumeRequest = optionalDumeRequest.get();
            populateLog(dumeRequest.getContext().getConsultRef());
            dumeRequestMapper.updateDumeRequestFromContext(context, dumeRequest);
            dumeRequestRepository.save(dumeRequest);
            logger.info("Mise à jour du contexte pour un dume acheteur id : {} , dumeRequestDTO {}  , retour => {}", id, context.getId(), dumeRequest.getId());
            removeLog();
            return dumeRequest.getId();

        }
        return null;
    }

    @Override
    public String generateDumeRequestXML(Long id) throws ServiceException, DumeResourceNotFoundException {
        Optional<DumeRequest> dumeRequest = dumeRequestRepository.findById(id);
        if (!dumeRequest.isPresent()) {
            throw new DumeResourceNotFoundException(DUME_REQUEST, id);
        }

        populateLog(dumeRequest.get().getContext().getConsultRef());
        logger.info("Génération du XML pour le dume acheteur ID : {}", id);
        try {
            String xml = xmlDumeService.generateXMLAcheteur(dumeRequest.get());
            removeLog();
            return xml;
        } catch (MarshallingException ex) {
            throw new ServiceException("Une erreur est survenue lors de la génération du XML pour l'id du DumeRequest " + id, ex);
        }
    }

    @Override
    public Boolean deleteDumeRequestById(Long id) throws DumeResourceNotFoundException {

        Optional<DumeRequest> optionalDumeRequest = dumeRequestRepository.findById(id);
        if (optionalDumeRequest.isPresent()) {
            populateLog(optionalDumeRequest.get().getContext().getConsultRef());
            dumeRequestRepository.delete(optionalDumeRequest.get());
            logger.info("Suppression du dume acheteur ID : {} , retour => {}", id, true);
            removeLog();
            return true;
        } else {
            throw new DumeResourceNotFoundException(DUME_REQUEST, id);
        }
    }

    @Override
    public DumeRequest updateDumeRequestByDTO(DumeRequestDTO dumeRequestDTO) throws ServiceException, DumeResourceNotFoundException {
        Optional<DumeRequest> optionalDumeRequest = dumeRequestRepository.findById(dumeRequestDTO.getId());
        if (optionalDumeRequest.isPresent()) {
            populateLog(optionalDumeRequest.get().getContext().getConsultRef());
            DumeRequest dumeRequest = optionalDumeRequest.get();
            dumeRequestMapper.updateDumeRequestFromDumeRequestDTO(dumeRequestDTO, dumeRequest);
            dumeRequest.setExclusionCriteriaList(criteriaService.getAllExclusionCriteria());
            dumeRequest.getSelectionCriteriaList().clear();
            List<CriteriaSelectionDocument> criteriaSelectionDocuments = new ArrayList<>();
            for (CriteriaLotDTO criteriaLotDTO : dumeRequestDTO.getSelectionCriteriaCodes()) {
                CriteriaSelectionDocument criteria = criteriaService.getCriteriaSelection(dumeRequest, criteriaLotDTO);

                if (null!=criteria) {
                    criteriaSelectionDocuments.add(criteria);
                }
            }
            dumeRequest.getSelectionCriteriaList().addAll(criteriaSelectionDocuments);
            dumeRequest = dumeRequestRepository.save(dumeRequest);
            logger.info("Mise à jour du Dume acheteur DTO : {} , retour => {}", dumeRequestDTO.getId(), dumeRequest.getId());
            removeLog();
            return dumeRequest;
        }
        return null;
    }

    @Override
    public List<DumeRequestDTO> getAllDumeRequestDTO() {
        return dumeRequestMapper.toDtoList(dumeRequestRepository.findAll());
    }

    @Override
    public Optional<DumeRequest> getDumeRequestById(Long idDumeRequest) {
        return dumeRequestRepository.findById(idDumeRequest);
    }

    @Override
    public void setCriteriaDefaut(DumeRequest dumeRequest) {
        dumeRequest.setExclusionCriteriaList(criteriaService.getAllExclusionCriteria());
        dumeRequest.setSelectionCriteriaList(criteriaService.getAllSelectionCriteriaWithoutALLCriteria().stream().map(selectionCriteria -> new CriteriaSelectionDocument(selectionCriteria, dumeRequest)).collect(Collectors.toList()));
    }

    @Override

    public void publishAllDumeRequests() {

        List<DumeRequest> dumesAPublier = dumeRequestRepository.findAllByEtatDume(EtatDume.A_PUBLIER);
        if (CollectionUtils.isEmpty(dumesAPublier)) {
            logger.info("Publication des dume acheteurs : Rien à publier");
            return;
        }

        for (DumeRequest dumeRequest : dumesAPublier) {
                schedularTask.publishDumeAcheteur(dumeRequest.getId());
        }
    }

    @Override

    public void retrieveAllDumeRequestsPDF() {
        List<DumeRequest> dumesPublies = dumeRequestRepository.findAllByEtatDumeAndNumDumeSNIsNotNullAndPdfRetrieved(EtatDume.PUBLIE, false);
        if (CollectionUtils.isEmpty(dumesPublies)) {
            logger.info("Récupération des PDF pour les dumes acheteurs : Rien à télécharger");
            return;
        }

        for (DumeRequest dumeRequest : dumesPublies) {
            schedularTask.getPDFAcheteur(dumeRequest.getId());

        }
    }

    @Override
    public File getPDFFile(String numeroSN) throws DumeResourceNotFoundException {
        Optional<DumeRequest> request = dumeRequestRepository.findByNumDumeSN(numeroSN);
        if (!request.isPresent() || request.get().getContext() == null) {
            throw new DumeResourceNotFoundException(DUME_REQUEST, "{numéro SN : " + numeroSN + "}");
        }
        populateLog(request.get().getContext().getConsultRef());

        Path filePath = Paths.get(pdfStorageFolder + File.separator + request.get().getContext().getId() + File.separator, numeroSN + ".pdf");
        if (!filePath.toFile().isFile()) {
            throw new DumeResourceNotFoundException(" PDF ", "{numéro SN :" + numeroSN + ", filePath: " + filePath + "}");
        }

        logger.info("Récupération du PDF  pour le dume acheteur Numéro SN = {} fichier PDF => {}", numeroSN, filePath);
        removeLog();
        return filePath.toFile();
    }

    @Override
    public void updateDLROIntoSN() {
        List<DumeRequest> dumeRequests = dumeRequestRepository.findAllByEtatDume(EtatDume.DLRO_MODIFIEE);
        if (CollectionUtils.isEmpty(dumeRequests)) {
            logger.info("Mise à jour des DLRO cotế SN : Rien à modifier");
            return;
        }

        for (DumeRequest dumeRequest : dumeRequests) {
            schedularTask.updateDLROAcheteur(dumeRequest.getId());
        }
    }

    @Override
    public void remplaceDumeWithSN() {
        List<DumeRequest> dumeRequests = dumeRequestRepository.findAllByEtatDume(EtatDume.A_REMPLACER);
        if (CollectionUtils.isEmpty(dumeRequests)) {
            logger.info("Remplacement des DumeRequest côté SN : Rien à modifier");
            return;
        }

        for (DumeRequest dumeRequest : dumeRequests) {
            schedularTask.replaceDumeAcheteur(dumeRequest.getId());
        }

    }

    @Override
    public void downloadTemplateOE() {
        List<DumeRequest> dumeRequests = dumeRequestRepository.findAllByEtatDumeAndNumDumeSNIsNotNullAndPdfRetrievedAndXmlOERetrieved(EtatDume.PUBLIE, true, false);

        if (CollectionUtils.isEmpty(dumeRequests)) {
            logger.info("Récupération du XML : Aucun XML OE à Récuperer");
            return;
        }

        for (DumeRequest dumeRequest : dumeRequests) {
            schedularTask.downloadTemplateXMLOE(dumeRequest.getId());
        }
    }

    @Override
    public Boolean checkPurgeCriteriaLot(DumeRequest dumeDocument, Set<LotDTO> lotDTOS) {
        Set<Lot> associatedLots = new HashSet<>();

        dumeDocument.getSelectionCriteriaList().forEach(criteriaSelectionDocument -> associatedLots.addAll(criteriaSelectionDocument.getLotList()));

        if (associatedLots.isEmpty()) {
            return Boolean.FALSE;
        }
        if (dumeDocument.getContext().getLotSet().size() != lotDTOS.size()) {
            return Boolean.TRUE;
        }
        List<LotDTO> associatedLotDTOs = associatedLots.stream().map(lot -> new LotDTO(lot.getLotNumber(), lot.getLotName())).collect(Collectors.toList());
        return !lotDTOS.containsAll(associatedLotDTOs);
    }

    @Override
    public DumeRequest purgeCriteriaLot(DumeRequest dumeRequest) {
        dumeRequest.getSelectionCriteriaList().forEach(criteriaSelectionDocument -> criteriaSelectionDocument.getLotList().clear());
        return dumeRequest;
    }

    @Override
    public Set<Lot> checkValidLot(DumeRequest dumeRequest, Set<Lot> lotSet) {

        if (CollectionUtils.isEmpty(lotSet)) {
            return Collections.emptySet();
        }
        Set<Lot> associatedLots = new HashSet<>();
        associatedLots.addAll(lotSet);
        for (CriteriaSelectionDocument criteriaSelectionDocument : dumeRequest.getSelectionCriteriaList()) {
            if (CollectionUtils.isEmpty(criteriaSelectionDocument.getLotList())) {
                return Collections.emptySet();
            }
            associatedLots.removeAll(criteriaSelectionDocument.getLotList());
        }
        return associatedLots;
    }

    @Override
    public byte[] generateXMLByNumeroSN(String numeroSN) throws DumeResourceNotFoundException, ServiceException {

        DumeRequest dumeRequest = dumeRequestRepository.findByNumDumeSN(numeroSN).orElseThrow(() -> new DumeResourceNotFoundException(" XML ", "{numéro SN :" + numeroSN + "}"));
        populateLog(dumeRequest.getContext().getConsultRef());
        logger.info("Génération du XML pour le dume acheteur numeroSN : {}", numeroSN);

        try {
            String xml = xmlDumeService.generateXMLAcheteur(dumeRequest);
            removeLog();
            return xml.getBytes();
        } catch (MarshallingException ex) {
            throw new ServiceException("Une erreur est survenue lors de la génération du XML pour le dumeRequest avec le numeroSN " + numeroSN, ex);
        }
    }
}
