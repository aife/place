package com.atexo.dume.services.impl;

import com.atexo.dume.dto.EnumDTO;
import com.atexo.dume.dto.csv.dto.CriteriaCSVDTO;
import com.atexo.dume.dto.csv.dto.RequirementCSVDTO;
import com.atexo.dume.mapper.EnumMapper;
import com.atexo.dume.model.*;
import com.atexo.dume.repository.CriteriaRepository;
import com.atexo.dume.repository.RequirementRepository;
import com.atexo.dume.services.api.ReferentielService;
import com.atexo.dume.toolbox.consts.CustomResponseDataType;
import com.atexo.dume.toolbox.csv.CSVReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReferentielServiceImpl implements ReferentielService {

    private final Logger logger = LoggerFactory.getLogger(ReferentielServiceImpl.class);

    @Autowired
    RequirementRepository requirementRepository;

    @Autowired
    CriteriaRepository criteriaRepository;

    @Override
    @Cacheable
    public List<EnumDTO> getCountryEnum() {
        return Country.getCountryList().stream().map(EnumMapper::countryToDTO).collect(Collectors.toList());
    }

    @Override
    @Cacheable
    public List<EnumDTO> getCurrencyEnum() {
        return Currency.getCurrencyList().stream().map(EnumMapper::currencyToDTO).collect(Collectors.toList());
    }

    @Override
    @Cacheable
    public List<EnumDTO> getMarketEnum() {
        return MarketType.getMarketList().stream().map(EnumMapper::marketToDTO).collect(Collectors.toList());
    }

    @Override
    public Boolean updateDescriptionCriteria(InputStream inputStream) {

        try {
            List<CriteriaCSVDTO> requirementCSVDTOList = CSVReader.loadObjectList(CriteriaCSVDTO.class, inputStream);

            for (CriteriaCSVDTO criteriaCSVDTO : requirementCSVDTOList) {
                Criteria criteria = criteriaRepository.findByCode(criteriaCSVDTO.getCode());
                if (criteria == null) {
                    logger.error("Il n'existe pas de critére avec le code {} ", criteriaCSVDTO.getCode());
                    continue;
                }
                criteria.setShortName(criteriaCSVDTO.getName());
                criteria.setDescription(criteriaCSVDTO.getDescription());
                criteriaRepository.save(criteria);
            }
        } catch (IOException e) {
            logger.error("Error occurred while loading object list from file ", e);
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    @Override
    public Boolean updateDescriptionRequirement(InputStream inputStream) {
        try {
            List<RequirementCSVDTO> requirementCSVDTOList = CSVReader.loadObjectList(RequirementCSVDTO.class, inputStream);

            for (RequirementCSVDTO requirementCSVDTO : requirementCSVDTOList) {
                List<Requirement> requirementList = requirementRepository
                        .findByUuidAndResponseTypeIsNotIn(requirementCSVDTO.getUuid(),
                                Arrays.stream(CustomResponseDataType.values())
                                        .map(CustomResponseDataType::value)
                                        .collect(Collectors.toList()));
                if (CollectionUtils.isEmpty(requirementList)) {
                    logger.error("Il n'existe pas de requirement avec le code {} ", requirementCSVDTO.getUuid());
                }

                requirementList.stream().forEach(requirement -> requirement.setDescription(requirementCSVDTO.getDescription()));
                requirementRepository.saveAll(requirementList);
            }
        } catch (IOException e) {
            logger.error("Error occurred while loading object list from file ", e);
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
}
