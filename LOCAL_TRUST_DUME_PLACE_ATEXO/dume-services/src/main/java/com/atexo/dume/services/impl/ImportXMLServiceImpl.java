package com.atexo.dume.services.impl;

import com.atexo.dume.model.CriteriaSelectionDocument;
import com.atexo.dume.model.DumeDomain;
import com.atexo.dume.model.DumePlatform;
import com.atexo.dume.model.DumeRequestModel;
import com.atexo.dume.model.profile.DumeDomainPropValue;
import com.atexo.dume.model.profile.DumePlatPropValue;
import com.atexo.dume.repository.CriteriaExclusionRepository;
import com.atexo.dume.repository.CriteriaSelectionRepository;
import com.atexo.dume.repository.DumeDomPropValueRepository;
import com.atexo.dume.repository.DumeDomainRepository;
import com.atexo.dume.repository.DumePlatPropValueRepository;
import com.atexo.dume.repository.DumePlatformRepository;
import com.atexo.dume.repository.DumeRequestModelRepository;
import com.atexo.dume.services.api.ImportXMLService;
import com.atexo.dume.services.api.ProfileParamService;
import grow.names.specification.ubl.schema.xsd.espdrequest_1.ESPDRequestType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBElement;
import javax.xml.transform.stream.StreamSource;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.atexo.dume.toolbox.consts.DumeProfilePropEnum.MODEL_DUME_REQUEST;

@Service
public class ImportXMLServiceImpl implements ImportXMLService {

    private static final Logger logger = LoggerFactory.getLogger(ImportXMLServiceImpl.class);

    @Autowired
    private Jaxb2Marshaller jaxb2Marshaller;

    @Autowired
    private DumeRequestModelRepository dumeRequestModelRepository;

    @Autowired
    private CriteriaExclusionRepository criteriaExclusionRepository;

    @Autowired
    private CriteriaSelectionRepository criteriaSelectionRepository;

    @Autowired
    private ProfileParamService profileParamService;

    @Autowired
    private DumeDomPropValueRepository domPropValueRepository;

    @Autowired
    private DumePlatformRepository dumePlatformRepository;

    @Autowired
    private DumePlatPropValueRepository platPropValueRepository;

    @Autowired
    private DumeDomainRepository dumeDomainRepository;

    @Override
    public Boolean importDumeRequestModelDomaine(InputStream xml, String domaine) {

        Optional<DumeDomain> dumeDomainOptional = dumeDomainRepository.findByDomLabel(domaine);
        if (!dumeDomainOptional.isPresent()) {
            logger.error("Aucun domaine est défini avec le label {}", domaine);
            return false;
        }
        DumeDomainPropValue dumeDomainPropValue = profileParamService.getDomPropValueOrCreateByDumeProfileProp(MODEL_DUME_REQUEST.getLabel(), dumeDomainOptional.get());
        DumeRequestModel dumeRequestModel = getDumeRequestModel(dumeDomainPropValue.getValue());
        dumeRequestModel = initDocumentModelUsingXML(xml, dumeRequestModel);
        dumeDomainPropValue.setValue(dumeRequestModel.getId() + "");
        domPropValueRepository.save(dumeDomainPropValue);
        return true;
    }


    @Override
    public Boolean importDumeRequestModelPlatform(InputStream xml, String platform) {
        Optional<DumePlatform> optionalDumePlatform = dumePlatformRepository.findByPlatformIdentifier(platform);
        if (!optionalDumePlatform.isPresent()) {
            logger.error("Aucun domaine est défini avec le label {}", platform);
            return false;
        }
        DumePlatPropValue dumePlatPropValue = profileParamService.getPlatPropValueOrCreateByDumeProfileProp(MODEL_DUME_REQUEST.getLabel(), optionalDumePlatform.get());
        DumeRequestModel dumeRequestModel = getDumeRequestModel(dumePlatPropValue.getValue());
        dumeRequestModel = initDocumentModelUsingXML(xml, dumeRequestModel);
        dumePlatPropValue.setValue(dumeRequestModel.getId() + "");
        platPropValueRepository.save(dumePlatPropValue);
        return true;
    }

    private DumeRequestModel getDumeRequestModel(String value) {
        DumeRequestModel dumeRequestModel;
        if (value == null) {
            logger.debug("Création d'un nouveau DumeRequestModel");
            dumeRequestModel = new DumeRequestModel();
        } else {
            Optional<DumeRequestModel> optional = dumeRequestModelRepository.findById(Long.valueOf(value));
            if (!optional.isPresent()) {
                throw new IllegalArgumentException("Aucun Model avec l'id " + value);
            }
            logger.debug("Mise à jour d'un nouveau DumeRequestModel");
            dumeRequestModel = optional.get();
        }
        return dumeRequestModel;
    }

    private DumeRequestModel initDocumentModelUsingXML(InputStream xml, DumeRequestModel dumeRequestModel) {

        logger.debug("Conversion du XML");
        JAXBElement<ESPDRequestType> element = (JAXBElement<ESPDRequestType>) jaxb2Marshaller.unmarshal(new StreamSource(xml));
        ESPDRequestType requestType = element.getValue();

        List<String> codes = requestType.getCriterion().stream()
                .map(criterionType -> criterionType.getTypeCode().getValue())
                .collect(Collectors.toList());

        dumeRequestModel.setExclusionCriteriaList(criteriaExclusionRepository.findByCodeIn(codes));
        dumeRequestModel.setSelectionCriteriaList(criteriaSelectionRepository.findByCodeIn(codes).stream().map(selectionCriteria -> new CriteriaSelectionDocument(selectionCriteria, dumeRequestModel)).collect(Collectors.toList()));
        return dumeRequestModelRepository.save(dumeRequestModel);
    }


}
