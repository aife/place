package com.atexo.dume.services.impl;

import com.atexo.dume.dto.StatusDTO;
import com.atexo.dume.dto.donneesessentielles.RetourStatusContract;
import com.atexo.dume.model.DumePlatform;
import com.atexo.dume.model.donneesessentielles.Contract;
import com.atexo.dume.model.donneesessentielles.EtatContract;
import com.atexo.dume.repository.ContractRepository;
import com.atexo.dume.repository.DumePlatformRepository;
import com.atexo.dume.services.api.DonneesEssentiellesService;
import com.atexo.dume.services.api.SchedularTask;
import com.atexo.dume.services.exception.DumeResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class DonneesEssentiellesServiceImpl implements DonneesEssentiellesService {

    private static final Logger logger = LoggerFactory.getLogger(DonneesEssentiellesServiceImpl.class);

    @Autowired
    private ContractRepository contractRepository;

    @Autowired
    private DumePlatformRepository platformRepository;


    @Autowired
    private SchedularTask schedularTask;

    @Override
    public void publishAllDonneesEssentielles() {
        List<DumePlatform> platforms = platformRepository.findByEnableEssentialData(true);
        if (CollectionUtils.isEmpty(platforms)) {
            logger.info("Publication des données essentielles : aucune plateforme trouvée");
            return;
        }

        for (DumePlatform platform : platforms) {
            logger.info("Publication des données essentielles pour la plateforme : {} - {}", platform.getPlatLabel(), platform.getUrl());
            schedularTask.publishDonneesEssentielles(platform);
        }

    }

    @Override
    public RetourStatusContract checkStatutContrat(String contratNumber) throws DumeResourceNotFoundException {

        Contract contract = contractRepository.getContractByContractNumber(contratNumber).orElseThrow(() -> new DumeResourceNotFoundException("Contract", contratNumber));
        return getContractStatuts(contract);
    }

    @Override
    public RetourStatusContract checkStatutContrat(String contratNumber, String initialContractUuid) throws DumeResourceNotFoundException {
        Contract contract = null;
        if (contratNumber.endsWith("00")) {
            contract = contractRepository.getContractByUuid(initialContractUuid).orElseThrow(() -> new DumeResourceNotFoundException("Contract", contratNumber));
        } else {
            contract = contractRepository.getContractByContractNumberAndContractOriginUuid(contratNumber, initialContractUuid).orElseThrow(() -> new DumeResourceNotFoundException("Contract", contratNumber));
        }
        return getContractStatuts(contract);
    }

    private RetourStatusContract getContractStatuts(Contract contract) {
        if (contract.getEtatContract() == EtatContract.EN_ATTENTE) {
            return new RetourStatusContract(StatusDTO.NON_PUBLIE, "Le contrat n'est pas encore publié côté SN", null);
        }
        if (contract.getEtatContract() == EtatContract.ERREUR_SN) {
            var statut = new RetourStatusContract(StatusDTO.ERREUR_SN, "Problème de communication avec le SN", null);
            statut.setMessage(contract.getErrors());
            return statut;
        }

        return new RetourStatusContract(contract.getEtatContract() == EtatContract.PUBLIE ? StatusDTO.PUBLIE : StatusDTO.MISE_A_JOUR, "les données ont été publié côté SN", contract.getSnNumber());
    }
}
