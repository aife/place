package com.atexo.dume.services;

import com.atexo.dume.dto.EnumDTO;
import com.atexo.dume.model.*;
import com.atexo.dume.model.dataset.RequirementDataSet;
import com.atexo.dume.model.dataset.SelectionCriteriaDataSet;
import com.atexo.dume.repository.CriteriaRepository;
import com.atexo.dume.repository.RequirementRepository;
import com.atexo.dume.services.impl.ReferentielServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class ReferentielServiceImplTest {


    @InjectMocks
    private ReferentielServiceImpl referentielService;

    @Mock
    RequirementRepository requirementRepository;

    @Mock
    CriteriaRepository criteriaRepository;



    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getCountryEnumOK() {
        List<EnumDTO> enumDTOList = referentielService.getCountryEnum();
        assertNotNull(enumDTOList);
        assertEquals(enumDTOList.size(), Country.getCountryList().size());
    }

    @Test
    public void getMarketTypeEnumOK() {
        List<EnumDTO> enumDTOList = referentielService.getMarketEnum();
        assertNotNull(enumDTOList);
        assertEquals(enumDTOList.size(), MarketType.getMarketList().size());
    }



    @Test
    public void getCurrencyEnumOK() {
        List<EnumDTO> enumDTOList = referentielService.getCurrencyEnum();
        assertNotNull(enumDTOList);
        assertEquals(enumDTOList.size(), Currency.getCurrencyList().size());
    }

    @Test
    public void updateDescriptionCriteriaOK() throws IOException {
        InputStream inputStream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("criteria.csv");
        Criteria criteria = SelectionCriteriaDataSet.sample1WithId();
        when(criteriaRepository.findByCode("CRITERION.EXCLUSION.CONVICTIONS.PARTICIPATION_IN_CRIMINAL_ORGANISATION")).thenReturn(criteria);
        when(criteriaRepository.save(any())).thenReturn(criteria);

        Boolean res = referentielService.updateDescriptionCriteria(inputStream);
        assertNotNull(res);
        assertTrue(res);
    }

    @Test
    public void updateDescriptionCriteriaIOException() throws IOException {

        Boolean res = referentielService.updateDescriptionCriteria(null);
        assertNotNull(res);
        assertFalse(res);
    }

    @Test
    public void updateDescriptionRequirementOK() throws IOException {
        InputStream inputStream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("libelle.csv");

        Requirement requirement = RequirementDataSet.sampleWithId();

        when(requirementRepository.findByUuidAndResponseTypeIsNotIn(any(), any())).thenReturn(asList(requirement));
        when(requirementRepository.saveAll(any())).thenReturn(new ArrayList<>());

        Boolean res = referentielService.updateDescriptionRequirement(inputStream);
        assertNotNull(res);
        assertTrue(res);
    }

    @Test
    public void updateDescriptionRequirementEmpty() throws IOException {
        InputStream inputStream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("libelle.csv");

        when(requirementRepository.findByUuidAndResponseTypeIsNotIn(any(), any())).thenReturn(new ArrayList<>());
        when(requirementRepository.saveAll(any())).thenReturn(new ArrayList<>());

        Boolean res = referentielService.updateDescriptionRequirement(inputStream);
        assertNotNull(res);
        assertTrue(res);
    }

    @Test
    public void updateDescriptionRequirementException() throws IOException {

        Boolean res = referentielService.updateDescriptionRequirement(null);
        assertNotNull(res);
        assertFalse(res);
    }

}
