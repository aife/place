package com.atexo.dume.services;

import com.atexo.dume.dto.*;
import com.atexo.dume.dto.dataset.DumeRequestContextDataDTOSet;
import com.atexo.dume.dto.dataset.NationalServiceDTODataSet;
import com.atexo.dume.dto.sn.response.SNDumeSavedResponseDTO;
import com.atexo.dume.mapper.DumeRequestContextMapper;
import com.atexo.dume.mapper.sn.SNRequestMapper;
import com.atexo.dume.model.*;
import com.atexo.dume.model.dataset.DumePlatformDataSet;
import com.atexo.dume.model.dataset.DumeRequestContextDataSet;
import com.atexo.dume.model.dataset.DumeRequestDataSet;
import com.atexo.dume.model.dataset.DumeResponseDataSet;
import com.atexo.dume.repository.DumePlatformRepository;
import com.atexo.dume.repository.DumeRequestContextRepository;
import com.atexo.dume.repository.DumeResponseRepository;
import com.atexo.dume.services.api.DumeRequestService;
import com.atexo.dume.services.api.NationalServiceRequestService;
import com.atexo.dume.services.exception.DumeResourceNotFoundException;
import com.atexo.dume.services.exception.NationalServiceException;
import com.atexo.dume.services.exception.ServiceException;
import com.atexo.dume.services.impl.DumeRequestContextServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

import static com.atexo.dume.dto.StatusDTO.*;
import static com.atexo.dume.dto.dataset.DumeRequestContextDataDTOSet.SIMPLE_CONTX_ID;
import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static org.hibernate.validator.internal.util.CollectionHelper.asSet;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

/**
 * Created by ahmedsalmi on 08/02/2018.
 */

public class DumeRequestContextServiceImplTest {

    @InjectMocks
    private DumeRequestContextServiceImpl contextService;

    @Mock
    private DumeRequestContextRepository contextRepository;

    @Mock
    private DumeRequestService dumeRequestService;

    @Mock
    private DumeRequestContextMapper contextMapper;

    @Mock
    private DumeResponseRepository dumeResponseRepository;

    @Mock
    private DumePlatformRepository platformRepository;

    @Mock
    private SNRequestMapper snRequestMapper;

    @Mock
    private NationalServiceRequestService nationalServiceRequestService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void saveContextOkPlatformOK() throws ServiceException, DumeResourceNotFoundException {
        final DumeRequestContextDTO dumeRequestContextDTO = DumeRequestContextDataDTOSet.contextSample();
        final DumeRequestContext dumeRequestContext = DumeRequestContextDataSet.sampleContext();
        dumeRequestContext.setId(null);
        final DumePlatform platform = DumePlatformDataSet.samplePlatform();
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithId();
        when(contextMapper.contextRqDTOToContextRq(dumeRequestContextDTO)).thenReturn(dumeRequestContext);
        when(dumeRequestService.createDumeRequest(dumeRequestContext)).thenReturn(dumeRequest);
        when(contextRepository.save(dumeRequestContext)).thenReturn(dumeRequestContext);
        when(contextMapper.dumeReqContextToDumeReqContextDTO(dumeRequestContext)).thenReturn(dumeRequestContextDTO);
        when(platformRepository.findByPlatformIdentifier(any())).thenReturn(Optional.of(platform));
        when(contextRepository.findByMetadataConsultationIdentifierAndPlatform(any(), any())).thenReturn(Optional.empty());

        DumeRequestContextDTO res = contextService.saveContext(dumeRequestContextDTO);

        assertNotNull(res);
        assertEquals(res.getId(), dumeRequestContextDTO.getId());


    }

    @Test(expected = ServiceException.class)
    public void saveContextExistantNOK() throws ServiceException, DumeResourceNotFoundException {
        final DumeRequestContextDTO dumeRequestContextDTO = DumeRequestContextDataDTOSet.contextSample();
        final DumeRequestContext dumeRequestContext = DumeRequestContextDataSet.sampleContext();
        dumeRequestContext.setId(null);
        final DumePlatform platform = DumePlatformDataSet.samplePlatform();
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithId();
        when(contextMapper.contextRqDTOToContextRq(dumeRequestContextDTO)).thenReturn(dumeRequestContext);
        when(dumeRequestService.createDumeRequest(dumeRequestContext)).thenReturn(dumeRequest);

        when(contextMapper.dumeReqContextToDumeReqContextDTO(dumeRequestContext)).thenReturn(dumeRequestContextDTO);
        when(platformRepository.findByPlatformIdentifier(any())).thenReturn(Optional.of(platform));
        when(contextRepository.findByMetadataConsultationIdentifierAndPlatform(any(), any())).thenReturn(Optional.of(dumeRequestContext));

        DumeRequestContextDTO res = contextService.saveContext(dumeRequestContextDTO);
    }

    @Test(expected = ServiceException.class)
    public void saveContextOkPlatformNOK() throws ServiceException, DumeResourceNotFoundException {
        final DumeRequestContextDTO dumeRequestContextDTO = DumeRequestContextDataDTOSet.contextSample();
        final DumeRequestContext dumeRequestContext = DumeRequestContextDataSet.sampleContext();
        dumeRequestContext.setId(null);
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithId();
        when(contextMapper.contextRqDTOToContextRq(dumeRequestContextDTO)).thenReturn(dumeRequestContext);
        when(dumeRequestService.createDumeRequest(dumeRequestContext)).thenReturn(dumeRequest);
        when(contextRepository.save(dumeRequestContext)).thenReturn(dumeRequestContext);
        when(contextMapper.dumeReqContextToDumeReqContextDTO(dumeRequestContext)).thenReturn(dumeRequestContextDTO);
        when(platformRepository.findByPlatformIdentifier(any())).thenReturn(Optional.empty());

        DumeRequestContextDTO res = contextService.saveContext(dumeRequestContextDTO);

        assertNotNull(res);
        assertEquals(SIMPLE_CONTX_ID, res.getId());


    }


    @Test
    public void updateContextWithoutRequestOk() throws DumeResourceNotFoundException {

        final Long id = DumeRequestContextDataSet.SIMPLE_CONTX_ID;
        final DumeRequestContextDTO dumeRequestContextDTO = DumeRequestContextDataDTOSet.contextSample();
        final Optional<DumeRequestContext> dumeRequestContextOptional = Optional.of(DumeRequestContextDataSet.sampleContext());

        when(contextRepository.findById(id)).thenReturn(dumeRequestContextOptional);
        doNothing().when(contextMapper).updateContextFromDTOWithLot(dumeRequestContextDTO, dumeRequestContextOptional.get());
        when(contextRepository.save(dumeRequestContextOptional.get())).thenReturn(dumeRequestContextOptional.get());

        Long idRes = contextService.updateContextWithoutRequest(dumeRequestContextDTO, id);

        assertNotNull(idRes);
        assertEquals(idRes, dumeRequestContextOptional.get().getId());
    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void updateContextWithoutRequestNOk() throws DumeResourceNotFoundException {

        final Long id = DumeRequestContextDataSet.SIMPLE_CONTX_ID;
        final DumeRequestContextDTO dumeRequestContextDTO = DumeRequestContextDataDTOSet.contextSample();
        final Optional<DumeRequestContext> dumeRequestContextOptional = Optional.empty();

        when(contextRepository.findById(id)).thenReturn(dumeRequestContextOptional);

        Long idRes = contextService.updateContextWithoutRequest(dumeRequestContextDTO, id);

        assertNull(idRes);

    }

    @Test
    public void getDumeRequestContextByIdOk() throws DumeResourceNotFoundException {
        final Long id = DumeRequestContextDataSet.SIMPLE_CONTX_ID;
        final DumeRequestContextDTO dumeRequestContextDTO = DumeRequestContextDataDTOSet.contextSample();
        final Optional<DumeRequestContext> dumeRequestContextOptional = Optional.of(DumeRequestContextDataSet.sampleContext());

        when(contextRepository.findById(id)).thenReturn(dumeRequestContextOptional);
        when(contextMapper.dumeReqContextToDumeReqContextDTO(dumeRequestContextOptional.get())).thenReturn(dumeRequestContextDTO);

        DumeRequestContextDTO res = contextService.getDumeRequestContextById(id);

        assertNotNull(res);
        assertEquals(res.getId(), dumeRequestContextDTO.getId());
    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void getDumeRequestContextByIdNotFound() throws DumeResourceNotFoundException {
        final Long id = DumeRequestContextDataSet.SIMPLE_CONTX_ID;
        final Optional<DumeRequestContext> dumeRequestContextOptional = Optional.empty();

        when(contextRepository.findById(id)).thenReturn(dumeRequestContextOptional);

        DumeRequestContextDTO res = contextService.getDumeRequestContextById(id);

        assertNull(res);

    }

    @Test
    public void deleteContextDumeRequestOk() throws DumeResourceNotFoundException {
        final Long id = DumeRequestContextDataSet.SIMPLE_CONTX_ID;
        final Optional<DumeRequestContext> dumeRequestContextOptional = Optional.of(DumeRequestContextDataSet.sampleContext());

        when(contextRepository.findById(id)).thenReturn(dumeRequestContextOptional);
        doNothing().when(contextRepository).delete(any());
        Boolean res = contextService.deleteContextDumeRequest(id);

        assertNotNull(res);
        assertTrue(res.booleanValue());

    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void deleteContextDumeRequestNotFound() throws DumeResourceNotFoundException {

        final Long id = DumeRequestContextDataSet.SIMPLE_CONTX_ID;
        final Optional<DumeRequestContext> dumeRequestContextOptional = Optional.empty();

        when(contextRepository.findById(id)).thenReturn(dumeRequestContextOptional);

        Boolean res = contextService.deleteContextDumeRequest(id);

        assertNotNull(res);
        assertFalse(res.booleanValue());
    }

    @Test
    public void updateDumeContextFromDTOOk() throws ServiceException, DumeResourceNotFoundException {
        final DumeRequestContextDTO dumeRequestContextDTO = DumeRequestContextDataDTOSet.contextSample();
        final Optional<DumeRequestContext> dumeRequestContextOptional = Optional.of(DumeRequestContextDataSet.sampleContext());

        when(contextRepository.findById(dumeRequestContextDTO.getId())).thenReturn(dumeRequestContextOptional);
        doNothing().when(contextMapper).updateContextFromContextDTOWithoutDumeRequests(any(), any());
        when(contextRepository.save(dumeRequestContextOptional.get())).thenReturn(dumeRequestContextOptional.get());

        DumeRequestContextDTO res = contextService.updateDumeContextFromDTO(dumeRequestContextDTO);

        assertNotNull(res);
        assertEquals(res.getId(), dumeRequestContextDTO.getId());
    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void updateDumeContextFromDTONotFound() throws ServiceException, DumeResourceNotFoundException {
        final DumeRequestContextDTO dumeRequestContextDTO = DumeRequestContextDataDTOSet.contextSample();
        final Optional<DumeRequestContext> dumeRequestContextOptional = Optional.empty();

        when(contextRepository.findById(dumeRequestContextDTO.getId())).thenReturn(dumeRequestContextOptional);


        DumeRequestContextDTO res = contextService.updateDumeContextFromDTO(dumeRequestContextDTO);

        assertNull(res);
    }

    @Test
    public void getAllDumeRequestContextOk() {
        final List<DumeRequestContext> dumeRequestContexts = asList(DumeRequestContextDataSet.sampleContext());
        final DumeRequestContextDTO dumeRequestContextDTO = DumeRequestContextDataDTOSet.contextSample();
        when(contextRepository.findAll()).thenReturn(dumeRequestContexts);
        when(contextMapper.dumeReqContextToDumeReqContextDTO(any())).thenReturn(dumeRequestContextDTO);

        List<DumeRequestContextDTO> res = contextService.getAllDumeRequestContext();

        assertNotNull(res);
        assertEquals(res.size(), dumeRequestContexts.size());

    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void checkValidContextDumeNotFound() throws DumeResourceNotFoundException {

        when(contextRepository.findById(any())).thenReturn(Optional.empty());

        RetourStatus retourStatus = contextService.checkValidContextDume(100L);

        assertNull(retourStatus);
    }

    @Test
    public void checkValidContextDumeNonValide() throws DumeResourceNotFoundException {
        final DumeRequestContext dumeRequestContext = DumeRequestContextDataSet.sampleContext();
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithId();
        dumeRequest.setConfirmed(false);
        dumeRequestContext.addDumeDocumentToContext(dumeRequest);

        when(contextRepository.findById(any())).thenReturn(Optional.of(dumeRequestContext));

        RetourStatus retourStatus = contextService.checkValidContextDume(100L);

        assertNotNull(retourStatus);
        assertEquals(NON_VALIDE, retourStatus.getStatusDTO());
    }

    @Test
    public void checkValidContextDumeValide() throws DumeResourceNotFoundException {
        final DumeRequestContext dumeRequestContext = DumeRequestContextDataSet.sampleContext();
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithId();
        dumeRequest.setConfirmed(true);
        dumeRequestContext.addDumeDocumentToContext(dumeRequest);

        when(contextRepository.findById(any())).thenReturn(Optional.of(dumeRequestContext));

        RetourStatus retourStatus = contextService.checkValidContextDume(100L);

        assertNotNull(retourStatus);
        assertEquals(VALIDE, retourStatus.getStatusDTO());
    }

    @Test
    public void checkValidContextDumeDefault() throws DumeResourceNotFoundException {
        final DumeRequestContext dumeRequestContext = DumeRequestContextDataSet.sampleContext();
        dumeRequestContext.setDefault(true);
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithId();
        dumeRequestContext.addDumeDocumentToContext(dumeRequest);

        when(contextRepository.findById(any())).thenReturn(Optional.of(dumeRequestContext));

        RetourStatus retourStatus = contextService.checkValidContextDume(100L);

        assertNotNull(retourStatus);
        assertEquals(VALIDE, retourStatus.getStatusDTO());
    }

    @Test
    public void checkValidContextDumeSimplified() throws DumeResourceNotFoundException {
        final DumeRequestContext dumeRequestContext = DumeRequestContextDataSet.sampleContext();
        dumeRequestContext.getMetadata().setSimplified(true);
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithId();
        dumeRequestContext.addDumeDocumentToContext(dumeRequest);

        when(contextRepository.findById(any())).thenReturn(Optional.of(dumeRequestContext));

        RetourStatus retourStatus = contextService.checkValidContextDume(100L);

        assertNotNull(retourStatus);
        assertEquals(VALIDE, retourStatus.getStatusDTO());
    }

    @Test
    public void checkValidContextDumeNotValidLot() throws DumeResourceNotFoundException {
        final DumeRequestContext dumeRequestContext = DumeRequestContextDataSet.sampleContext();
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithId();
        dumeRequest.setConfirmed(true);
        dumeRequestContext.addDumeDocumentToContext(dumeRequest);

        when(contextRepository.findById(any())).thenReturn(Optional.of(dumeRequestContext));
        when(dumeRequestService.checkValidLot(any(DumeRequest.class), any())).thenReturn(asSet(new Lot("1", "1", null)));
        RetourStatus retourStatus = contextService.checkValidContextDume(100L);

        assertNotNull(retourStatus);
        assertEquals(NON_VALIDE, retourStatus.getStatusDTO());
    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void changeEtatToApublierForAllDumeNotFound() throws DumeResourceNotFoundException, ServiceException {

        when(contextRepository.findById(any())).thenReturn(Optional.empty());

        RetourStatus retourStatus = contextService.publishAllDumeRequest(100L);

        assertNull(retourStatus);
    }

    @Test
    public void changeEtatToApublierForAllDumeOk() throws DumeResourceNotFoundException, ServiceException {
        final DumeRequestContext dumeRequestContext = DumeRequestContextDataSet.sampleContext();
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithId();
        dumeRequest.setConfirmed(Boolean.TRUE);
        dumeRequestContext.addDumeDocumentToContext(dumeRequest);

        when(contextRepository.findById(any())).thenReturn(Optional.of(dumeRequestContext));

        RetourStatus retourStatus = contextService.publishAllDumeRequest(100L);

        assertNotNull(retourStatus);
        assertEquals(OK, retourStatus.getStatusDTO());
        assertEquals(false, retourStatus.getStandard());
    }

    @Test(expected = ServiceException.class)
    public void changeEtatToApublierForAllDumeNotValide() throws DumeResourceNotFoundException, ServiceException {
        final DumeRequestContext dumeRequestContext = DumeRequestContextDataSet.sampleContext();
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithId();
        dumeRequest.setConfirmed(Boolean.FALSE);
        dumeRequestContext.addDumeDocumentToContext(dumeRequest);

        when(contextRepository.findById(any())).thenReturn(Optional.of(dumeRequestContext));

        contextService.publishAllDumeRequest(100L);
    }

    @Test
    public void changeEtatToApublierForAllDumeAlreadyPublish() throws DumeResourceNotFoundException, ServiceException {
        final DumeRequestContext dumeRequestContext = DumeRequestContextDataSet.sampleContext();
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithId();
        dumeRequest.setConfirmed(Boolean.TRUE);
        dumeRequest.setEtatDume(EtatDume.PUBLIE);
        dumeRequestContext.addDumeDocumentToContext(dumeRequest);

        when(contextRepository.findById(any())).thenReturn(Optional.of(dumeRequestContext));

        RetourStatus retourStatus = contextService.publishAllDumeRequest(100L);

        assertNotNull(retourStatus);
        assertEquals(OK, retourStatus.getStatusDTO());
        assertEquals(false, retourStatus.getStandard());
    }

    @Test
    public void changeEtatToApublierForAllDumeStandardOk() throws DumeResourceNotFoundException, ServiceException {
        final DumeRequestContext dumeRequestContext = DumeRequestContextDataSet.sampleContext();
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithId();
        dumeRequestContext.addDumeDocumentToContext(dumeRequest);
        dumeRequestContext.setDefault(true);
        when(contextRepository.findById(any())).thenReturn(Optional.of(dumeRequestContext));

        RetourStatus retourStatus = contextService.publishAllDumeRequest(100L);

        assertNotNull(retourStatus);
        assertEquals(OK, retourStatus.getStatusDTO());
        assertEquals(true, retourStatus.getStandard());
    }

    @Test
    public void recupererStatusPublicationDumeAcheteurOK() throws DumeResourceNotFoundException {
        final DumeRequestContext dumeRequestContext = DumeRequestContextDataSet.sampleContext();
        final PublishedDume publishedDume = new PublishedDume();
        publishedDume.setDumeSNIdentifier("hiulmp");
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithId();
        dumeRequest.setEtatDume(EtatDume.PUBLIE);
        dumeRequest.setNumDumeSN("hiulmp");
        dumeRequestContext.addDumeDocumentToContext(dumeRequest);

        when(contextRepository.findById(any())).thenReturn(Optional.of(dumeRequestContext));
        when(contextMapper.toPublicationStatuts(any())).thenReturn(
                new PublicationStatus(OK, asList(publishedDume)));
        PublicationStatus publicationStatus = contextService.recupererStatusPublicationDumeAcheteur(100L);

        assertNotNull(publicationStatus);
        assertEquals(OK, publicationStatus.getStatut());
        assertEquals(dumeRequestContext.getDumeDocuments().size(), publicationStatus.getPublishedDumeList().size());
        assertEquals("hiulmp", publicationStatus.getPublishedDumeList().get(0).getDumeSNIdentifier());
    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void recupererStatusPublicationDumeAcheteurNotFound() throws DumeResourceNotFoundException {
        when(contextRepository.findById(any())).thenReturn(Optional.empty());
        PublicationStatus publicationStatus = contextService.recupererStatusPublicationDumeAcheteur(100L);
        assertNull(publicationStatus);
    }

    @Test
    public void recupererStatusPublicationDumeAcheteurNonPublie() throws DumeResourceNotFoundException {

        final DumeRequestContext dumeRequestContext = DumeRequestContextDataSet.sampleContext();
        final PublishedDume publishedDume = new PublishedDume();
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithId();
        dumeRequest.setEtatDume(EtatDume.A_PUBLIER);
        dumeRequestContext.addDumeDocumentToContext(dumeRequest);

        when(contextRepository.findById(any())).thenReturn(Optional.of(dumeRequestContext));
        when(contextMapper.toPublicationStatuts(any())).thenReturn(
                new PublicationStatus(OK, asList(publishedDume)));
        PublicationStatus publicationStatus = contextService.recupererStatusPublicationDumeAcheteur(100L);

        assertNotNull(publicationStatus);
        assertEquals(NON_PUBLIE, publicationStatus.getStatut());
    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void updateDLRONotFound() throws DumeResourceNotFoundException {
        final Optional<DumeRequestContext> optionalContext = Optional.empty();

        when(contextRepository.findById(any())).thenReturn(optionalContext);

        Boolean res = contextService.updateDLRO(12L, any());
    }

    @Test
    public void updateDLROOk() throws DumeResourceNotFoundException {
        final Optional<DumeRequestContext> optionalContext = Optional.of(DumeRequestContextDataSet.sampleContext());

        when(contextRepository.findById(any())).thenReturn(optionalContext);

        Boolean res = contextService.updateDLRO(12L, any());

        assertEquals(Boolean.TRUE, res);
    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void updateRequestContextAfterPublicationException() throws DumeResourceNotFoundException, ServiceException {
        Optional<DumeRequestContext> optionalContext = Optional.empty();

        when(contextRepository.findById(any())).thenReturn(optionalContext);

        RetourStatus status = contextService.updateRequestContextAfterPublication(12L);
    }

    @Test(expected = ServiceException.class)
    public void updateRequestContextAfterPublicationWithPublishedDumeException() throws DumeResourceNotFoundException, ServiceException {
        var context = new DumeRequestContext();
        var dume = new DumeDocument();
        dume.setEtatDume(EtatDume.A_PUBLIER);
        context.getDumeDocuments().add(dume);
        Optional<DumeRequestContext> optionalContext = Optional.of(context);
        when(contextRepository.findById(any())).thenReturn(optionalContext);

        RetourStatus status = contextService.updateRequestContextAfterPublication(12L);
    }


    @Test
    public void updateRequestContextAfterPublicationOK() throws DumeResourceNotFoundException, ServiceException {
        DumeRequestContext context = DumeRequestContextDataSet.sampleContext();
        DumeResponse dumeResponse = DumeResponseDataSet.sampleWithId();
        context.addDumeDocumentToContext(DumeRequestDataSet.sampleWithSimpleStructure());
        final PublishedDume publishedDume = new PublishedDume();

        Optional<DumeRequestContext> optionalContext = Optional.of(context);
        when(contextRepository.findById(any())).thenReturn(optionalContext);
        when(dumeResponseRepository.findAllByDumeRequestIdAndEtatDume(anyLong(), any())).thenReturn(asList(dumeResponse));
        when(contextMapper.toPublicationStatuts(any())).thenReturn(
                new PublicationStatus(OK, asList(publishedDume)));
        PublicationStatus publicationStatus = contextService.recupererStatusPublicationDumeAcheteur(100L);


        RetourStatus status = contextService.updateRequestContextAfterPublication(12L);
        assertNotNull(status);
        assertEquals(StatusDTO.OK, status.getStatusDTO());

    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void checkPurgeCriteriaLotException() throws DumeResourceNotFoundException {

        when(contextRepository.findById(anyLong())).thenReturn(Optional.empty());

        RetourStatus retourStatus = contextService.checkPurgeCriteriaLot(12L, emptySet());
    }

    @Test
    public void checkPurgeCriteriaLotPurged() throws DumeResourceNotFoundException {
        DumeRequestContext context = DumeRequestContextDataSet.sampleContext();
        context.addDumeDocumentToContext(DumeRequestDataSet.sampleWithSimpleStructure());
        when(contextRepository.findById(anyLong())).thenReturn(Optional.of(context));
        when(dumeRequestService.checkPurgeCriteriaLot(any(DumeRequest.class), any())).thenReturn(Boolean.TRUE);
        RetourStatus retourStatus = contextService.checkPurgeCriteriaLot(12L, emptySet());

        assertNotNull(retourStatus);
        assertEquals(StatusDTO.PURGE, retourStatus.getStatusDTO());
    }

    @Test
    public void checkPurgeCriteriaLotNotPurged() throws DumeResourceNotFoundException {
        DumeRequestContext context = DumeRequestContextDataSet.sampleContext();
        context.addDumeDocumentToContext(DumeRequestDataSet.sampleWithSimpleStructure());
        when(contextRepository.findById(anyLong())).thenReturn(Optional.of(context));
        when(dumeRequestService.checkPurgeCriteriaLot(any(DumeRequest.class), any())).thenReturn(Boolean.FALSE);
        RetourStatus retourStatus = contextService.checkPurgeCriteriaLot(12L, emptySet());

        assertNotNull(retourStatus);
        assertEquals(StatusDTO.NOT_PURGE, retourStatus.getStatusDTO());
    }

    @Test
    public void downloadPdfOK() throws DumeResourceNotFoundException, ServiceException, NationalServiceException {
        DumeRequestContext context = DumeRequestContextDataSet.sampleContextWithDocument();
        SNDumeSavedResponseDTO snSearchResponseDTO = NationalServiceDTODataSet.sampleResponseWithDumeA();
        when(dumeRequestService.generateDumeRequestXML(anyLong())).thenReturn("dummyXML");
        when(contextRepository.findById(context.getId())).thenReturn(Optional.of(context));
        when(nationalServiceRequestService.restAcheteur(any(), any(), any())).thenReturn(ResponseEntity.ok(snSearchResponseDTO));
        byte[] result = contextService.downloadPdf(context.getId());
        assertNotNull(result);
        assertEquals(new String(result), NationalServiceDTODataSet.DUME_A_CONTENT);
    }

    @Test(expected = ServiceException.class)
    public void downloadPdfWithNoAttachedDumeRequestKO() throws DumeResourceNotFoundException, ServiceException {
        DumeRequestContext context = DumeRequestContextDataSet.sampleContext();
        when(contextRepository.findById(context.getId())).thenReturn(Optional.of(context));
        contextService.downloadPdf(context.getId());
    }

    @Test(expected = ServiceException.class)
    public void downloadPdfWithSNExceptionKO() throws DumeResourceNotFoundException, ServiceException, NationalServiceException {
        DumeRequestContext context = DumeRequestContextDataSet.sampleContext();
        when(contextRepository.findById(context.getId())).thenReturn(Optional.of(context));
        when(nationalServiceRequestService.restAcheteur(any(), any(), any())).thenThrow(new NationalServiceException());
        byte[] result = contextService.downloadPdf(context.getId());
    }

    @Test(expected = ServiceException.class)
    public void downloadPdfWithNoResponseKO() throws DumeResourceNotFoundException, ServiceException, NationalServiceException {
        DumeRequestContext context = DumeRequestContextDataSet.sampleContextWithDocument();
        SNDumeSavedResponseDTO snSearchResponseDTO = new SNDumeSavedResponseDTO();
        when(dumeRequestService.generateDumeRequestXML(anyLong())).thenReturn("dummyXML");
        when(contextRepository.findById(context.getId())).thenReturn(Optional.of(context));
        when(nationalServiceRequestService.restAcheteur(any(), any(), any())).thenReturn(ResponseEntity.ok(snSearchResponseDTO));
        contextService.downloadPdf(context.getId());
    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void downloadPdfContextNotFound() throws DumeResourceNotFoundException, ServiceException {
        DumeRequestContext context = DumeRequestContextDataSet.sampleContextWithDocument();
        when(contextRepository.findById(any())).thenReturn(Optional.empty());
        contextService.downloadPdf(context.getId());
    }
}
