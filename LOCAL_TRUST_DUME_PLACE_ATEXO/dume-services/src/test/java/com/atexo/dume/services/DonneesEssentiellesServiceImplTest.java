package com.atexo.dume.services;

import com.atexo.dume.dto.StatusDTO;
import com.atexo.dume.dto.donneesessentielles.RetourStatusContract;
import com.atexo.dume.model.dataset.ContractDataSet;
import com.atexo.dume.model.donneesessentielles.Contract;
import com.atexo.dume.repository.ContractRepository;
import com.atexo.dume.services.exception.DumeResourceNotFoundException;
import com.atexo.dume.services.impl.DonneesEssentiellesServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;


public class DonneesEssentiellesServiceImplTest {

    @Mock
    private ContractRepository contractRepository;

    @InjectMocks
    private DonneesEssentiellesServiceImpl donneesEssentiellesService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void checkStatutContratNotFound() throws DumeResourceNotFoundException {

        when(contractRepository.getContractByContractNumber(anyString())).thenReturn(Optional.empty());

        donneesEssentiellesService.checkStatutContrat("01234");

    }

    @Test
    public void checkStatutContratEnAttente() throws DumeResourceNotFoundException {
        Contract contract = ContractDataSet.sampleWithEtatEnAttente();

        when(contractRepository.getContractByContractNumber(anyString())).thenReturn(Optional.of(contract));

        RetourStatusContract res = donneesEssentiellesService.checkStatutContrat("012345");
        assertNotNull(res);
        assertEquals(StatusDTO.NON_PUBLIE, res.getStatusDTO());
        assertNull(res.getSnNumber());

    }

    @Test
    public void checkStatutContratErreurSn() throws DumeResourceNotFoundException {

        Contract contract = ContractDataSet.sampleWithErreurSN();

        when(contractRepository.getContractByContractNumber(anyString())).thenReturn(Optional.of(contract));

        RetourStatusContract res = donneesEssentiellesService.checkStatutContrat("012345");
        assertNotNull(res);
        assertEquals(StatusDTO.ERREUR_SN, res.getStatusDTO());
    }

    @Test
    public void checkStatutContratPublie() throws DumeResourceNotFoundException {

        Contract contract = ContractDataSet.sampleWithEtatPublie();

        when(contractRepository.getContractByContractNumber(anyString())).thenReturn(Optional.of(contract));

        RetourStatusContract res = donneesEssentiellesService.checkStatutContrat("012345");
        assertNotNull(res);
        assertNotNull(res.getSnNumber());
        assertEquals(StatusDTO.PUBLIE, res.getStatusDTO());
    }

    @Test
    public void checkStatutContratMiseAJour() throws DumeResourceNotFoundException {

        Contract contract = ContractDataSet.sampleWithEtatMiseAjour();

        when(contractRepository.getContractByContractNumber(anyString())).thenReturn(Optional.of(contract));

        RetourStatusContract res = donneesEssentiellesService.checkStatutContrat("012345");
        assertNotNull(res);
        assertNotNull(res.getSnNumber());
        assertEquals(StatusDTO.MISE_A_JOUR, res.getStatusDTO());
    }

    @Test
    public void checkStatutContratWithUuidPublie() throws DumeResourceNotFoundException {

        Contract contract = ContractDataSet.sampleWithEtatPublie();
        contract.setContractNumber("initialContrat00");

        when(contractRepository.getContractByUuid(anyString())).thenReturn(Optional.of(contract));

        RetourStatusContract res = donneesEssentiellesService.checkStatutContrat(contract.getContractNumber(), contract.getUuid());
        assertNotNull(res);
        assertNotNull(res.getSnNumber());
        assertEquals(StatusDTO.PUBLIE, res.getStatusDTO());
    }

    @Test
    public void checkStatutContratWithUuidMiseAJour() throws DumeResourceNotFoundException {

        Contract contract = ContractDataSet.sampleWithEtatMiseAjour();
        contract.setContractNumber("update01");

        when(contractRepository.getContractByContractNumberAndContractOriginUuid(anyString(), anyString())).thenReturn(Optional.of(contract));

        RetourStatusContract res = donneesEssentiellesService.checkStatutContrat(contract.getContractNumber(), contract.getUuid());
        assertNotNull(res);
        assertNotNull(res.getSnNumber());
        assertEquals(StatusDTO.MISE_A_JOUR, res.getStatusDTO());
    }
}
