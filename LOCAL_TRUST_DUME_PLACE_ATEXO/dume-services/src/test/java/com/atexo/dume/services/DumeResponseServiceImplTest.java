package com.atexo.dume.services;

import com.atexo.dume.dto.*;
import com.atexo.dume.dto.dataset.DumeRequestContextDataDTOSet;
import com.atexo.dume.dto.dataset.DumeResponseDTODataSet;
import com.atexo.dume.dto.dataset.NationalServiceDTODataSet;
import com.atexo.dume.dto.dataset.PublicationGroupementDTODataSet;
import com.atexo.dume.dto.sn.request.SNRequestDTO;
import com.atexo.dume.dto.sn.response.SNDumeSavedResponseDTO;
import com.atexo.dume.dto.sn.response.SNResponse;
import com.atexo.dume.dto.sn.response.SNSearchOEResponse;
import com.atexo.dume.dto.sn.response.SNSearchResponseDTO;
import com.atexo.dume.mapper.CriteriaMapper;
import com.atexo.dume.mapper.DumeResponseMapper;
import com.atexo.dume.mapper.ECertisMapper;
import com.atexo.dume.mapper.sn.SNRequestMapper;
import com.atexo.dume.model.DumeRequest;
import com.atexo.dume.model.DumeRequestContext;
import com.atexo.dume.model.DumeResponse;
import com.atexo.dume.model.EtatDume;
import com.atexo.dume.model.dataset.DumeRequestContextDataSet;
import com.atexo.dume.model.dataset.DumeRequestDataSet;
import com.atexo.dume.model.dataset.DumeResponseDataSet;
import com.atexo.dume.repository.CriteriaSelectionDocumentRepository;
import com.atexo.dume.repository.DumeResponseRepository;
import com.atexo.dume.repository.ECertisMappingRepository;
import com.atexo.dume.services.api.NationalServiceRequestService;
import com.atexo.dume.services.api.SchedularTask;
import com.atexo.dume.services.exception.DumeResourceNotFoundException;
import com.atexo.dume.services.exception.NationalServiceException;
import com.atexo.dume.services.exception.ServiceException;
import com.atexo.dume.services.impl.DumeResponseServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class DumeResponseServiceImplTest {
    @Mock
    private CriteriaSelectionDocumentRepository criteriaSelectionDocumentRepository;

    @Mock
    private CriteriaMapper criteriaMapper;

    @Mock
    private DumeResponseRepository dumeResponseRepository;

    @Mock
    private DumeResponseMapper dumeResponseMapper;

    @Mock
    private SNRequestMapper snRequestMapper;

    @Mock
    private SchedularTask schedularTask;

    @Mock
    private ECertisMapper eCertisMapper;

    @Mock
    private ECertisMappingRepository eCertisMappingRepository;

    @Mock
    private NationalServiceRequestService nationalServiceRequestService;


    @InjectMocks
    private DumeResponseServiceImpl dumeResponseService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getDumeResponseDTOByIdSimpleTestOk() throws DumeResourceNotFoundException {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithId();
        final Optional<DumeResponse> dumeResponse = Optional.of(DumeResponseDataSet.sampleWithId());
        final Long id = DumeResponseDTODataSet.DUME_RESPONSE_DTO_IDENTIFIER_1;


        when(dumeResponseRepository.findById(id)).thenReturn(dumeResponse);
        when(criteriaSelectionDocumentRepository.findByDumeDocument(dumeResponse.get().getDumeRequest())).thenReturn(emptyList());
        when(dumeResponseMapper.toResponseDto(any(DumeResponse.class))).thenReturn(dumeResponseDTO);

        DumeResponseDTO dtoResult = dumeResponseService.getDumeResponseDTOById(id);
        assertNotNull("Le Dto est NULL", dtoResult);

        assertEquals(dtoResult.getId(), id);
    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void getDumeResponseDTOByIdSimpleTestNotFound() throws DumeResourceNotFoundException {
        final Optional<DumeResponse> dumeResponse = Optional.empty();
        final Long id = DumeResponseDTODataSet.DUME_RESPONSE_DTO_IDENTIFIER_1;


        when(dumeResponseRepository.findById(id)).thenReturn(dumeResponse);

        DumeResponseDTO dtoResult = dumeResponseService.getDumeResponseDTOById(id);

    }

    @Test
    public void updateDumeResponseByDTOOk() throws DumeResourceNotFoundException {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithId();
        final Optional<DumeResponse> dumeResponse = Optional.of(DumeResponseDataSet.sampleWithId());

        when(dumeResponseRepository.findById(any())).thenReturn(dumeResponse);
        doNothing().when(dumeResponseMapper).updateDumeReponseUsingDTO(any(), any());
        when(dumeResponseRepository.save(any())).thenReturn(dumeResponse.get());

        DumeResponseDTO res = dumeResponseService.updateDumeResponseByDTO(dumeResponseDTO);


        assertNotNull(res);
        assertEquals(res.getId(), dumeResponseDTO.getId());

    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void updateDumeResponseByDTONotFound() throws DumeResourceNotFoundException {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithId();
        final Optional<DumeResponse> dumeResponse = Optional.empty();

        when(dumeResponseRepository.findById(any())).thenReturn(dumeResponse);

        DumeResponseDTO res = dumeResponseService.updateDumeResponseByDTO(dumeResponseDTO);

    }

    @Test
    public void validateDumeOEValide() throws DumeResourceNotFoundException {
        final Optional<DumeResponse> dumeResponseOptional = Optional.of(DumeResponseDataSet.sampleWithRequestAndContext());
        dumeResponseOptional.get().setConfirmed(true);

        when(dumeResponseRepository.findById(any())).thenReturn(dumeResponseOptional);

        RetourStatus retourStatus = dumeResponseService.validateDumeOE(any());

        assertNotNull(retourStatus);
        assertEquals(StatusDTO.VALIDE, retourStatus.getStatusDTO());
    }

    @Test
    public void validateDumeOENonValide() throws DumeResourceNotFoundException {
        final Optional<DumeResponse> dumeResponseOptional = Optional.of(DumeResponseDataSet.sampleWithRequestAndContext());
        dumeResponseOptional.get().setConfirmed(false);

        when(dumeResponseRepository.findById(any())).thenReturn(dumeResponseOptional);

        RetourStatus retourStatus = dumeResponseService.validateDumeOE(any());

        assertNotNull(retourStatus);
        assertEquals(StatusDTO.NON_VALIDE, retourStatus.getStatusDTO());
    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void validateDumeOENotFound() throws DumeResourceNotFoundException {
        final Optional<DumeResponse> dumeResponseOptional = Optional.empty();


        when(dumeResponseRepository.findById(any())).thenReturn(dumeResponseOptional);

        RetourStatus retourStatus = dumeResponseService.validateDumeOE(any());

        assertNull(retourStatus);
    }

    @Test
    public void publishDumeOEOK() throws DumeResourceNotFoundException {
        final Optional<DumeResponse> dumeResponseOptional = Optional.of(DumeResponseDataSet.sampleWithRequestAndContext());

        when(dumeResponseRepository.findById(any())).thenReturn(dumeResponseOptional);

        RetourStatus retourStatus = dumeResponseService.publishDumeOE(100L);

        assertNotNull(retourStatus);
        assertEquals(StatusDTO.OK, retourStatus.getStatusDTO());
    }


    @Test(expected = DumeResourceNotFoundException.class)
    public void publishDumeOENotFound() throws DumeResourceNotFoundException {
        final Optional<DumeResponse> dumeResponseOptional = Optional.empty();

        when(dumeResponseRepository.findById(any())).thenReturn(dumeResponseOptional);

        RetourStatus retourStatus = dumeResponseService.publishDumeOE(100L);
    }

    @Test
    public void recupererStatusPublicationDumeOeOK() throws DumeResourceNotFoundException {
        final Optional<DumeResponse> dumeResponseOptional = Optional.of(DumeResponseDataSet.sampleWithRequestAndContext());
        dumeResponseOptional.get().setEtatDume(EtatDume.PUBLIE);
        dumeResponseOptional.get().setNumeroSN("test05");

        when(dumeResponseRepository.findById(any())).thenReturn(dumeResponseOptional);

        PublicationStatus publicationStatus = dumeResponseService.recupererStatusPublicationDumeOe(200L);

        assertNotNull(publicationStatus);
        assertEquals(StatusDTO.OK, publicationStatus.getStatut());
        assertEquals("test05", publicationStatus.getPublishedDumeList().get(0).getDumeSNIdentifier());
    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void recupererStatusPublicationDumeOeNotFound() throws DumeResourceNotFoundException {
        final Optional<DumeResponse> dumeResponseOptional = Optional.empty();

        when(dumeResponseRepository.findById(any())).thenReturn(dumeResponseOptional);
        PublicationStatus publicationStatus = dumeResponseService.recupererStatusPublicationDumeOe(200L);

    }

    @Test
    public void recupererStatusPublicationDumeOeNonPublie() throws DumeResourceNotFoundException {
        final Optional<DumeResponse> dumeResponseOptional = Optional.of(DumeResponseDataSet.sampleWithRequestAndContext());
        dumeResponseOptional.get().setEtatDume(EtatDume.A_PUBLIER);

        when(dumeResponseRepository.findById(any())).thenReturn(dumeResponseOptional);

        PublicationStatus publicationStatus = dumeResponseService.recupererStatusPublicationDumeOe(200L);
        assertNotNull(publicationStatus);
        assertEquals(StatusDTO.NON_PUBLIE, publicationStatus.getStatut());

    }

    @Test
    public void downloadPDFAfterAsaveDumeOEOK() throws Exception {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithId();
        final DumeResponse dumeResponse = DumeResponseDataSet.sampleWithId();
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithId();
        final DumeRequestContext dumeRequestContext = DumeRequestContextDataSet.sampleContext();
        dumeResponse.setDumeRequest(dumeRequest);
        dumeRequest.setContext(dumeRequestContext);
        final NationalServiceRequestService nationalServiceRequestService = spy(NationalServiceRequestService.class);
        final SNRequestMapper snRequestMapper = spy(SNRequestMapper.class);
        final DumeResponseRepository dumeResponseRepository = spy(DumeResponseRepository.class);
        final DumeResponseServiceImpl dumeResponseService = spy(DumeResponseServiceImpl.class);

        doReturn(Optional.of(DumeResponseDataSet.sampleWithRequestAndContext())).when(dumeResponseRepository).findById(any());
        dumeResponseService.init(nationalServiceRequestService, snRequestMapper, dumeResponseRepository);
        final SNResponse snResponse = new SNResponse();
        snResponse.setDumeOE("test02");
        final SNDumeSavedResponseDTO snDumeSavedResponseDTO = new SNDumeSavedResponseDTO();
        snDumeSavedResponseDTO.setResponse(snResponse);
        ResponseEntity<SNDumeSavedResponseDTO> responseWrapper = ResponseEntity.ok(snDumeSavedResponseDTO);
        doReturn(Optional.of(dumeResponse)).when(dumeResponseRepository).findById(any());
        doReturn(new SNRequestDTO()).when(snRequestMapper).toSnDownlaodResponsePDF(any(), any());
        doReturn(responseWrapper).when(nationalServiceRequestService).restOE(any(), any(), any());
        doReturn(new SNRequestDTO()).when(snRequestMapper).toSnDownloadResponseDto(any(), any(), any(), any());
        doReturn(dumeResponseDTO).when(dumeResponseService).updateDumeResponseByDTO(dumeResponseDTO);
        doReturn("TESTXM").when(dumeResponseService).generateDumeResponseXML(dumeResponseDTO.getId());


        byte[] inputStream = dumeResponseService.downloadPDFAfterAsaveDumeOE(dumeResponseDTO);

        assertNotNull(inputStream);
    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void downloadPDFAfterAsaveDumeOEReponseNULL() throws Exception {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithId();
        final DumeResponse dumeResponse = DumeResponseDataSet.sampleWithId();
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithId();
        final DumeRequestContext dumeRequestContext = DumeRequestContextDataSet.sampleContext();
        dumeResponse.setDumeRequest(dumeRequest);
        dumeRequest.setContext(dumeRequestContext);
        final NationalServiceRequestService nationalServiceRequestService = spy(NationalServiceRequestService.class);
        final DumeResponseRepository dumeResponseRepository = spy(DumeResponseRepository.class);
        final SNRequestMapper snRequestMapper = spy(SNRequestMapper.class);
        final DumeResponseServiceImpl dumeResponseService = spy(DumeResponseServiceImpl.class);

        dumeResponseService.init(nationalServiceRequestService, snRequestMapper, dumeResponseRepository);

        doReturn(Optional.of(DumeResponseDataSet.sampleWithRequestAndContext())).when(dumeResponseRepository).findById(any());
        final SNDumeSavedResponseDTO snDumeSavedResponseDTO = new SNDumeSavedResponseDTO();
        var responseWrapper = ResponseEntity.ok(snDumeSavedResponseDTO);

        doReturn(responseWrapper).when(nationalServiceRequestService).restOE(any(), any(), any());
        doReturn(Optional.of(dumeResponse)).when(dumeResponseRepository).findById(any());
        doReturn(new SNRequestDTO()).when(snRequestMapper).toSnDownlaodResponsePDF(any(), any());
        doReturn(dumeResponseDTO).when(dumeResponseService).updateDumeResponseByDTO(dumeResponseDTO);
        doReturn("TESTXM").when(dumeResponseService).generateDumeResponseXML(dumeResponseDTO.getId());


        byte[] inputStream = dumeResponseService.downloadPDFAfterAsaveDumeOE(dumeResponseDTO);

        assertEquals(0, inputStream.length);
    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void downloadPDFAfterAsaveDumeOEUpdateNull() throws Exception {
        final DumeResponseDTO dumeResponseDTO = DumeResponseDTODataSet.sampleWithId();
        final NationalServiceRequestService nationalServiceRequestService = spy(NationalServiceRequestService.class);
        final DumeResponseRepository dumeResponseRepository = spy(DumeResponseRepository.class);
        final SNRequestMapper snRequestMapper = spy(SNRequestMapper.class);
        final DumeResponseServiceImpl dumeResponseService = spy(DumeResponseServiceImpl.class);

        dumeResponseService.init(nationalServiceRequestService, null, dumeResponseRepository);
        doReturn(null).when(dumeResponseService).updateDumeResponseByDTO(dumeResponseDTO);


        byte[] inputStream = dumeResponseService.downloadPDFAfterAsaveDumeOE(dumeResponseDTO);

    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void updateMetadonneesAndLotException() throws Exception {
        Optional<DumeResponse> dumeResponse = Optional.empty();

        when(dumeResponseRepository.findById(any())).thenReturn(dumeResponse);

        Long res = dumeResponseService.updateMetadonneesAndLot(new DumeRequestContextDTO(), 12L);

    }

    @Test
    public void updateMetadonneesAndLotOK() throws Exception {
        Optional<DumeResponse> dumeResponse = Optional.of(DumeResponseDataSet.sampleWithId());
        dumeResponse.get().setDumeRequest(DumeRequestDataSet.sampleWithSimpleStructureWithContext());
        DumeRequestContextDTO dumeRequestContextDTO = DumeRequestContextDataDTOSet.contextSample();
        when(dumeResponseRepository.findById(any())).thenReturn(dumeResponse);
        when(dumeResponseRepository.save(any())).thenReturn(dumeResponse.get());
        Long res = dumeResponseService.updateMetadonneesAndLot(dumeRequestContextDTO, 12L);
        assertNotNull(res);
        assertEquals(DumeResponseDataSet.SIMPLE_DUME_RESPONSE_IDENTIFIER_1, res);
    }

    @Test
    public void findBySNNumberOK() throws NationalServiceException, ServiceException {
        final Optional<DumeResponse> dumeResponseOptional = Optional.of(DumeResponseDataSet.sampleWithRequestAndContext());
        DumeResponse response = dumeResponseOptional.get();
        dumeResponseOptional.get().setEtatDume(EtatDume.A_PUBLIER);
        SNSearchResponseDTO snSearchResponseDTO = NationalServiceDTODataSet.sampleWithAccesssible();
        DumeResponseDTO responseDTO = DumeResponseDTODataSet.sampleWithIdAndSNNumber();
        final NationalServiceRequestService nationalServiceRequestService = spy(NationalServiceRequestService.class);
        dumeResponseService.init(nationalServiceRequestService, snRequestMapper, dumeResponseRepository);
        when(dumeResponseRepository.findById(any())).thenReturn(dumeResponseOptional);
        when(snRequestMapper.toSnSearch(any(), any(), any())).thenReturn(new SNRequestDTO());
        when(nationalServiceRequestService.restOE(any(), any(), any())).thenReturn(ResponseEntity.ok(snSearchResponseDTO));
        //when(dumeResponseMapper.toResponseDto(any(SNSearchOEResponse.class))).thenReturn(responseDTO);
        List<DumeResponseDTO> list = dumeResponseService.findDumeOEFromSN(response.getId(), response.getNumeroSN());
        assertFalse(list.isEmpty());
        assertEquals(1, list.size());
    }

    @Test
    public void findBySNNumberOKIgnoreAccessibilite() throws NationalServiceException, ServiceException {
        final Optional<DumeResponse> dumeResponseOptional = Optional.of(DumeResponseDataSet.sampleWithRequestAndContext());
        DumeResponse response = dumeResponseOptional.get();
        dumeResponseOptional.get().setEtatDume(EtatDume.A_PUBLIER);
        SNSearchResponseDTO snSearchResponseDTO = NationalServiceDTODataSet.sampleWithoutAccesssible();
        DumeResponseDTO responseDTO = DumeResponseDTODataSet.sampleWithIdAndSNNumber();
        final NationalServiceRequestService nationalServiceRequestService = spy(NationalServiceRequestService.class);
        dumeResponseService.init(nationalServiceRequestService, snRequestMapper, dumeResponseRepository);
        when(dumeResponseRepository.findById(any())).thenReturn(dumeResponseOptional);
        when(snRequestMapper.toSnSearch(any(), any(), any())).thenReturn(new SNRequestDTO());
        when(nationalServiceRequestService.restOE(any(), any(), any())).thenReturn(ResponseEntity.ok(snSearchResponseDTO));
        when(dumeResponseMapper.toResponseDto(any(SNSearchOEResponse.class))).thenReturn(responseDTO);
        List<DumeResponseDTO> list = dumeResponseService.findDumeOEFromSN(response.getId(), response.getNumeroSN());
        assertFalse(list.isEmpty());
    }

    @Test(expected = ServiceException.class)
    public void findBySNNumberKOSN() throws NationalServiceException, ServiceException {
        final Optional<DumeResponse> dumeResponseOptional = Optional.of(DumeResponseDataSet.sampleWithRequestAndContext());
        DumeResponse response = dumeResponseOptional.get();
        final NationalServiceRequestService nationalServiceRequestService = spy(NationalServiceRequestService.class);
        dumeResponseService.init(nationalServiceRequestService, snRequestMapper, dumeResponseRepository);
        when(dumeResponseRepository.findById(any())).thenReturn(dumeResponseOptional);
        when(snRequestMapper.toSnSearch(any(), any(), any())).thenReturn(new SNRequestDTO());
        when(nationalServiceRequestService.restOE(any(), any(), any())).thenThrow(new NationalServiceException());
        dumeResponseService.findDumeOEFromSN(response.getId(), response.getNumeroSN());
    }

    @Test
    public void publishAllDumeResponsesNotEmpty() {
        final DumeResponse dumeResponse = DumeResponseDataSet.sampleWithRequestAndContext();

        when(dumeResponseRepository.findAllByEtatDume(EtatDume.A_PUBLIER)).thenReturn(asList(dumeResponse));

        doNothing().when(schedularTask).publishDumeOE(any(Long.class));

        dumeResponseService.publishAllDumeResponses();
    }

    @Test
    public void publishAllDumeResponsesEmpty() {

        when(dumeResponseRepository.findAllByEtatDume(EtatDume.A_PUBLIER)).thenReturn(emptyList());
        dumeResponseService.publishAllDumeResponses();

    }

    @Test
    public void retrieveAllDumesResponsePDFNotEmpty() {
        final DumeResponse dumeResponse = DumeResponseDataSet.sampleWithRequestAndContext();
        when(dumeResponseRepository.findAllByEtatDumeAndNumeroSNIsNotNullAndPdfRetreived(EtatDume.PUBLIE, false)).thenReturn(asList(dumeResponse));
        doNothing().when(schedularTask).getPDFOE(any(Long.class));

        dumeResponseService.retrieveAllDumesResponsePDF();
    }

    @Test
    public void retrieveAllDumesResponsePDF() {

        when(dumeResponseRepository.findAllByEtatDumeAndNumeroSNIsNotNullAndPdfRetreived(EtatDume.PUBLIE, false)).thenReturn(emptyList());

        dumeResponseService.retrieveAllDumesResponsePDF();
    }

    @Test
    public void publishDumeOEGroupementEmpty() throws DumeResourceNotFoundException, ServiceException {

        RetourStatus retourStatus = dumeResponseService.publishDumeOEGroupement(PublicationGroupementDTODataSet.dummyEmptyPublicationGroupementDTO());

        assertNotNull(retourStatus);
        assertEquals(StatusDTO.OK, retourStatus.getStatusDTO());
    }

    @Test
    public void publishDumeOEGroupementOk() throws DumeResourceNotFoundException, ServiceException {

        final Optional<DumeResponse> dumeResponseOptional1 = Optional.of(DumeResponseDataSet.sampleBrouillon());
        final Optional<DumeResponse> dumeResponseOptional2 = Optional.of(DumeResponseDataSet.sampleBrouillon());

        when(dumeResponseRepository.findById(PublicationGroupementDTODataSet.DUMMY_SIMPLE_ID_CONTEXTE_1)).thenReturn(dumeResponseOptional1);
        when(dumeResponseRepository.findById(PublicationGroupementDTODataSet.DUMMY_SIMPLE_ID_CONTEXTE_2)).thenReturn(dumeResponseOptional2);

        RetourStatus retourStatus = dumeResponseService.publishDumeOEGroupement(PublicationGroupementDTODataSet.dummyPublicationGroupementDTO());

        assertNotNull(retourStatus);
        assertEquals(StatusDTO.OK, retourStatus.getStatusDTO());
        assertEquals(PublicationGroupementDTODataSet.DUMMY_SIMPLE_SIRET_MANDATAIRE, dumeResponseOptional1.get().getSiretMandataire());
        assertEquals(EtatDume.A_PUBLIER, dumeResponseOptional1.get().getEtatDume());
        assertEquals(0, dumeResponseOptional1.get().getNbPublicationTries());
        assertTrue(dumeResponseOptional1.get().getGroupementLabel() != null
                && dumeResponseOptional1.get().getGroupementLabel().contains(PublicationGroupementDTODataSet.DUMMY_SIMPLE_SIRET_MANDATAIRE));
        assertEquals(PublicationGroupementDTODataSet.DUMMY_SIMPLE_SIRET_MANDATAIRE, dumeResponseOptional2.get().getSiretMandataire());
        assertEquals(EtatDume.A_PUBLIER, dumeResponseOptional2.get().getEtatDume());
        assertEquals(0, dumeResponseOptional2.get().getNbPublicationTries());
        assertTrue(dumeResponseOptional2.get().getGroupementLabel() != null
                && dumeResponseOptional2.get().getGroupementLabel().contains(PublicationGroupementDTODataSet.DUMMY_SIMPLE_SIRET_MANDATAIRE));
    }


    @Test(expected = DumeResourceNotFoundException.class)
    public void publishDumeOEGroupementNotFound() throws DumeResourceNotFoundException, ServiceException {
        final Optional<DumeResponse> dumeResponseOptional1 = Optional.of(DumeResponseDataSet.sampleBrouillon());
        final Optional<DumeResponse> dumeResponseOptional2 = Optional.empty();

        when(dumeResponseRepository.findById(PublicationGroupementDTODataSet.DUMMY_SIMPLE_ID_CONTEXTE_1)).thenReturn(dumeResponseOptional1);
        when(dumeResponseRepository.findById(PublicationGroupementDTODataSet.DUMMY_SIMPLE_ID_CONTEXTE_2)).thenReturn(dumeResponseOptional2);

        dumeResponseService.publishDumeOEGroupement(PublicationGroupementDTODataSet.dummyPublicationGroupementDTO());
    }

    @Test(expected = ServiceException.class)
    public void publishDumeOEGroupementException() throws DumeResourceNotFoundException, ServiceException {
        final Optional<DumeResponse> dumeResponseOptional1 = Optional.of(DumeResponseDataSet.sampleAPublier());

        when(dumeResponseRepository.findById(PublicationGroupementDTODataSet.DUMMY_SIMPLE_ID_CONTEXTE_1)).thenReturn(dumeResponseOptional1);

        dumeResponseService.publishDumeOEGroupement(PublicationGroupementDTODataSet.dummyPublicationGroupementDTO());
    }
}
