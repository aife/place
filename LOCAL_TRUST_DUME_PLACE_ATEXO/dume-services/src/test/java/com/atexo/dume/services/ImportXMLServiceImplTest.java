package com.atexo.dume.services;

import com.atexo.dume.model.DumeDomain;
import com.atexo.dume.model.DumePlatform;
import com.atexo.dume.model.DumeRequestModel;
import com.atexo.dume.model.profile.DumeDomainPropValue;
import com.atexo.dume.model.profile.DumePlatPropValue;
import com.atexo.dume.repository.CriteriaExclusionRepository;
import com.atexo.dume.repository.CriteriaSelectionRepository;
import com.atexo.dume.repository.DumeDomPropValueRepository;
import com.atexo.dume.repository.DumeDomainRepository;
import com.atexo.dume.repository.DumePlatPropValueRepository;
import com.atexo.dume.repository.DumePlatformRepository;
import com.atexo.dume.repository.DumeRequestModelRepository;
import com.atexo.dume.services.api.ProfileParamService;
import com.atexo.dume.services.impl.ImportXMLServiceImpl;
import grow.names.specification.ubl.schema.xsd.espdrequest_1.ESPDRequestType;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Optional;

import static com.atexo.dume.toolbox.consts.DumeProfilePropEnum.MODEL_DUME_REQUEST;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ImportXMLServiceImplTest {

    @Mock
    private Jaxb2Marshaller jaxb2Marshaller;

    @Mock
    private DumeRequestModelRepository dumeRequestModelRepository;

    @Mock
    private CriteriaExclusionRepository criteriaExclusionRepository;

    @Mock
    private CriteriaSelectionRepository criteriaSelectionRepository;

    @Mock
    private ProfileParamService profileParamService;

    @Mock
    private DumeDomPropValueRepository domPropValueRepository;

    @Mock
    private DumePlatformRepository dumePlatformRepository;

    @Mock
    private DumePlatPropValueRepository platPropValueRepository;

    @Mock
    private DumeDomainRepository dumeDomainRepository;


    @InjectMocks
    private ImportXMLServiceImpl importXMLService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void importDumeRequestModelDomaineNOTFound() throws Exception {
        InputStream input = mock(InputStream.class);
        Optional<DumeDomain> dumeDomainOptional = Optional.empty();

        when(dumeDomainRepository.findByDomLabel(any())).thenReturn(dumeDomainOptional);

        Boolean result = importXMLService.importDumeRequestModelDomaine(input, "TEST");

        assertNotNull(result);
        assertFalse(result);

    }

    @Test
    public void importDumeRequestModelPlatformNotFound() throws Exception {
        InputStream input = mock(InputStream.class);
        Optional<DumePlatform> dumePlatform = Optional.empty();

        when(dumePlatformRepository.findByPlatformIdentifier(any())).thenReturn(dumePlatform);

        Boolean result = importXMLService.importDumeRequestModelPlatform(input, "TEST");

        assertNotNull(result);
        assertFalse(result);
    }

    @Test
    public void importDumeRequestModelDomainePropValueIsNULL() {

        InputStream input = mock(InputStream.class);
        ESPDRequestType espdRequestType = new ESPDRequestType();
        Optional<DumeDomain> dumeDomainOptional = Optional.of(new DumeDomain());
        DumeDomainPropValue dumeDomainPropValue = new DumeDomainPropValue();

        when(jaxb2Marshaller.unmarshal(any())).thenReturn(new JAXBElement(new QName("test"), ESPDRequestType.class, espdRequestType));
        when(criteriaExclusionRepository.findByCodeIn(any())).thenReturn(new ArrayList<>());
        when(criteriaSelectionRepository.findByCodeIn(any())).thenReturn(new ArrayList<>());
        when(dumeDomainRepository.findByDomLabel(any())).thenReturn(dumeDomainOptional);
        when(profileParamService.getDomPropValueOrCreateByDumeProfileProp(MODEL_DUME_REQUEST.getLabel(), dumeDomainOptional.get())).thenReturn(dumeDomainPropValue);
        when(dumeRequestModelRepository.save(any())).thenReturn(new DumeRequestModel());
        Boolean result = importXMLService.importDumeRequestModelDomaine(input, "TEST");

        assertNotNull(result);
        assertTrue(result);
    }

    @Test
    public void importDumeRequestModelPlatformValueIsNULL() {

        InputStream input = mock(InputStream.class);
        ESPDRequestType espdRequestType = new ESPDRequestType();
        Optional<DumePlatform> dumeDomainOptional = Optional.of(new DumePlatform());
        DumePlatPropValue dumePlatPropValue = new DumePlatPropValue();

        when(jaxb2Marshaller.unmarshal(any())).thenReturn(new JAXBElement(new QName("test"), ESPDRequestType.class, espdRequestType));
        when(criteriaExclusionRepository.findByCodeIn(any())).thenReturn(new ArrayList<>());
        when(criteriaSelectionRepository.findByCodeIn(any())).thenReturn(new ArrayList<>());
        when(dumePlatformRepository.findByPlatformIdentifier(any())).thenReturn(dumeDomainOptional);
        when(profileParamService.getPlatPropValueOrCreateByDumeProfileProp(MODEL_DUME_REQUEST.getLabel(), dumeDomainOptional.get())).thenReturn(dumePlatPropValue);
        when(dumeRequestModelRepository.save(any())).thenReturn(new DumeRequestModel());
        Boolean result = importXMLService.importDumeRequestModelPlatform(input, "TEST");

        assertNotNull(result);
        assertTrue(result);
    }

    @Test
    public void importDumeRequestModelDomainePropValueIsNotNULL() {

        InputStream input = mock(InputStream.class);
        ESPDRequestType espdRequestType = new ESPDRequestType();
        Optional<DumeDomain> dumeDomainOptional = Optional.of(new DumeDomain());
        DumeDomainPropValue dumeDomainPropValue = new DumeDomainPropValue();
        dumeDomainPropValue.setValue("15");
        DumeRequestModel dumeRequestModel = new DumeRequestModel();


        when(jaxb2Marshaller.unmarshal(any())).thenReturn(new JAXBElement(new QName("test"), ESPDRequestType.class, espdRequestType));
        when(criteriaExclusionRepository.findByCodeIn(any())).thenReturn(new ArrayList<>());
        when(criteriaSelectionRepository.findByCodeIn(any())).thenReturn(new ArrayList<>());
        when(dumeDomainRepository.findByDomLabel(any())).thenReturn(dumeDomainOptional);
        when(profileParamService.getDomPropValueOrCreateByDumeProfileProp(MODEL_DUME_REQUEST.getLabel(), dumeDomainOptional.get())).thenReturn(dumeDomainPropValue);
        when(dumeRequestModelRepository.save(any())).thenReturn(dumeRequestModel);
        when(dumeRequestModelRepository.findById(any())).thenReturn(Optional.of(dumeRequestModel));
        Boolean result = importXMLService.importDumeRequestModelDomaine(input, "TEST");

        assertNotNull(result);
        assertTrue(result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void importDumeRequestModelDomainePropValueIsNotNULLAndException() {

        InputStream input = mock(InputStream.class);
        ESPDRequestType espdRequestType = new ESPDRequestType();
        Optional<DumeDomain> dumeDomainOptional = Optional.of(new DumeDomain());
        DumeDomainPropValue dumeDomainPropValue = new DumeDomainPropValue();
        dumeDomainPropValue.setValue("15");
        DumeRequestModel dumeRequestModel = new DumeRequestModel();


        when(jaxb2Marshaller.unmarshal(any())).thenReturn(new JAXBElement(new QName("test"), ESPDRequestType.class, espdRequestType));
        when(criteriaExclusionRepository.findByCodeIn(any())).thenReturn(new ArrayList<>());
        when(criteriaSelectionRepository.findByCodeIn(any())).thenReturn(new ArrayList<>());
        when(dumeDomainRepository.findByDomLabel(any())).thenReturn(dumeDomainOptional);
        when(profileParamService.getDomPropValueOrCreateByDumeProfileProp(MODEL_DUME_REQUEST.getLabel(), dumeDomainOptional.get())).thenReturn(dumeDomainPropValue);
        when(dumeRequestModelRepository.save(any())).thenReturn(dumeRequestModel);
        when(dumeRequestModelRepository.findById(any())).thenReturn(Optional.empty());
        Boolean result = importXMLService.importDumeRequestModelDomaine(input, "TEST");

        assertNotNull(result);
        assertTrue(result);
    }
}
