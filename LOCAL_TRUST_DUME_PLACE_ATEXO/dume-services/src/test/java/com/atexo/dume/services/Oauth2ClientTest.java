package com.atexo.dume.services;

import com.atexo.dume.dto.sn.response.SNOauth2Token;
import com.atexo.dume.model.DumePlatform;
import com.atexo.dume.services.impl.Oauth2Client;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

public class Oauth2ClientTest {

    private static final Long expirationTimeSeconds = 2L;
    private final DumePlatform platform = new DumePlatform();
    private final SNOauth2Token snOauth2Token = new SNOauth2Token();
    @Mock
    RestTemplate restTemplate;
    @InjectMocks
    Oauth2Client oauth2Client;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        platform.setPlatDesc("pf");
        platform.setPlatLabel("pf");

        snOauth2Token.setAccessToken("My token");
        snOauth2Token.setDateRecuperation(LocalDateTime.now());
        snOauth2Token.setExpiresIn(expirationTimeSeconds);
    }

    @Test
    public void getOauth2Token() {
        when(restTemplate.postForEntity(anyString(), any(), any())).thenReturn(ResponseEntity.ok(snOauth2Token));
        var token = oauth2Client.getOauth2Token(platform);
        assertEquals(token, snOauth2Token);
    }

    @Test
    public void getOauth2TokenFromCache() throws InterruptedException {
        snOauth2Token.setExpiresIn(expirationTimeSeconds);
        snOauth2Token.setDateRecuperation(LocalDateTime.now());
        when(restTemplate.postForEntity(anyString(), any(), any())).thenReturn(ResponseEntity.ok(snOauth2Token));
        var initalToken = oauth2Client.getOauth2Token(platform);
        // wait ...
        Thread.sleep(expirationTimeSeconds - 1);
        var anotherToken = oauth2Client.getOauth2Token(platform);
        assertEquals(anotherToken, initalToken);
    }

    @Test
    public void getOauth2TokenNewToken() throws InterruptedException {

        when(restTemplate.postForEntity(anyString(), any(), any()))
                .thenReturn(ResponseEntity.ok(new SNOauth2Token(UUID.randomUUID().toString(), expirationTimeSeconds, LocalDateTime.now())))
                .thenReturn(ResponseEntity.ok(new SNOauth2Token(UUID.randomUUID().toString(), expirationTimeSeconds, LocalDateTime.now())));

        var initalToken = oauth2Client.getOauth2Token(platform);
        // wait ...
        Thread.sleep((expirationTimeSeconds + 2) * 1000);
        var anotherToken = oauth2Client.getOauth2Token(platform);
        assertNotEquals(anotherToken, initalToken);
    }

}
