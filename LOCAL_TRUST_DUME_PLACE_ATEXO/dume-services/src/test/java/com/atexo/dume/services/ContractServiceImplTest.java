package com.atexo.dume.services;

import com.atexo.dume.dto.donneesessentielles.xml.AcheteurType;
import com.atexo.dume.dto.donneesessentielles.xml.MarcheType;
import com.atexo.dume.dto.donneesessentielles.xml.TitulaireType;
import com.atexo.dume.dto.donneesessentielles.xml.TypeIdentifiantType;
import com.atexo.dume.dto.sn.essentialData.TitulaireDTO;
import com.atexo.dume.dto.sn.request.SNEssentialDataDTO;
import com.atexo.dume.mapper.sn.SNEssentialDataMapper;
import com.atexo.dume.mapper.sn.SNEssentialDataMapperImpl;
import com.atexo.dume.model.DumePlatform;
import com.atexo.dume.model.dataset.ContractDataSet;
import com.atexo.dume.model.donneesessentielles.Contract;
import com.atexo.dume.model.donneesessentielles.EtatContract;
import com.atexo.dume.repository.ContractRepository;
import com.atexo.dume.repository.DumePlatformRepository;
import com.atexo.dume.services.api.NationalServiceRequestService;
import com.atexo.dume.services.exception.NationalServiceException;
import com.atexo.dume.services.impl.ContractServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class ContractServiceImplTest {

    String longText = "7lD6ysMyvlPoiE4R3rXU7S7gs2S5LLCeUN1gPqUQsy75YsLmuqU7bEgSnC3uwOFyNBYUDwoYmnm31jVULdPHiPd0cPEYwnrptCHgdAo3VWVdDsHFxy428UhYbePtcgRVW5Pgafc4hfH3GFbF9C7UB5uqdxWEPzUDkB8YpfDCq760LkLYrlecGAoBfPwTwUdZH2ofP7Cd7lD6ysMyvlPoiE4R3rXU7S7gs2S5LLCeUN1gPqUQsy75YsLmuqU7bEgSnC3uwOFyNBYUDwoYmnm31jVULdPHiPd0cPEYwnrptCHgdAo3VWVdDsHFxy428UhYbePtcgRVW5Pgafc4hfH3GFbF9C7UB5uqdxWEPzUDkB8YpfDCq760LkLYrlecGAoBfPwTwUdZH2ofP7Cd";

    @InjectMocks
    private SNEssentialDataMapperImpl essentialDataMapper;

    @Mock
    NationalServiceRequestService nationalServiceRequestService;
    @Mock
    SNEssentialDataMapper contractEssentialDataMapper;

    @InjectMocks
    SNEssentialDataMapperImpl contractEssentialDataMapperImpl;

    @Mock
    DumePlatformRepository dumePlatformRepository;
    @InjectMocks
    private ContractServiceImpl contractService;
    @Mock
    private ContractRepository contractRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void checkSaveRsDemandeurLength() {

        MarcheType marche = new MarcheType();
        AcheteurType acheteur = new AcheteurType();
        acheteur.setNom(longText);
        marche.setAcheteur(acheteur);


        assertEquals(longText.substring(0, 100).length(), essentialDataMapper.toFinalSNSaveEssentialData(new DumePlatform(), marche, "fake-id").getRsDemandeur().length());
        assertEquals(longText.substring(0, 255).length(), essentialDataMapper.toFinalSNSaveEssentialData(new DumePlatform(), marche, "fake-id").getAcheteur().getNom().length());


        acheteur.setNom(null);
        assertNull(essentialDataMapper.toFinalSNSaveEssentialData(new DumePlatform(), marche, "fake-id").getRsDemandeur());

    }

    @Test
    public void checkUpdateRsDemandeurLength() {

        MarcheType marche = new MarcheType();
        AcheteurType acheteur = new AcheteurType();
        acheteur.setNom(longText);
        marche.setAcheteur(acheteur);


        assertEquals(longText.substring(0, 100).length(), essentialDataMapper.toFinalSNUpdateEssentialData(new DumePlatform(), marche, "fake-id").getRsDemandeur().length());

        acheteur.setNom(null);
        assertNull(essentialDataMapper.toFinalSNUpdateEssentialData(new DumePlatform(), marche, "fake-id").getRsDemandeur());

    }

    @Test
    public void checkUpdateStatusAfterSNError() throws Exception {

        MarcheType marche = new MarcheType();
        marche.setId("contractNumber00");
        Contract contract = ContractDataSet.sampleWithEtatEnAttente();
        when(contractRepository.getContractByUuid(any())).thenReturn(Optional.of(contract));
        when(contractEssentialDataMapper.toFinalSNSaveEssentialData(any(), any(), any())).thenReturn(new SNEssentialDataDTO());
        when(nationalServiceRequestService.restEssentialData(any(), any(), any())).thenThrow(new NationalServiceException("Rest Exception"));
        when(dumePlatformRepository.getOne(any())).thenReturn(new DumePlatform());

        contractService.processContract(marche, 1L);
        assertEquals(contract.getEtatContract(), EtatContract.ERREUR_SN);
        assertNotNull(contract.getErrors());
    }

    @Test
    public void checkTypeIdentifiant() {
        TitulaireType titulaireType = new TitulaireType();
        titulaireType.setTypeIdentifiant(TypeIdentifiantType.HORS_UE);
        TitulaireDTO titulaireDTO = contractEssentialDataMapperImpl.toTitulaire(titulaireType);
        assertEquals(titulaireDTO.getTypeIdentifiant(), "HORS-UE");
    }

    @Test
    public void checkIdIdentifiantUE() {
        var titulaireType = new TitulaireType();
        titulaireType.setTypeIdentifiant(TypeIdentifiantType.UE);
        var initialId = "thisisaveryveryverylonglongvalue";
        titulaireType.setId(initialId);
        TitulaireDTO titulaireDTO = contractEssentialDataMapperImpl.toTitulaire(titulaireType);
        assertEquals(titulaireDTO.getTypeIdentifiant(), "UE");
        assertEquals(titulaireDTO.getId(), initialId);
    }

    @Test
    public void checkIdIdentifiantHorsUEShort() {
        var titulaireType = new TitulaireType();
        titulaireType.setTypeIdentifiant(TypeIdentifiantType.HORS_UE);
        var initialId = "short";
        titulaireType.setId(initialId);
        TitulaireDTO titulaireDTO = contractEssentialDataMapperImpl.toTitulaire(titulaireType);
        assertEquals(titulaireDTO.getTypeIdentifiant(), "HORS-UE");
        assertEquals(titulaireDTO.getId(), initialId);
    }

    @Test
    public void checkIdIdentifiantHorsUELong() {
        var titulaireType = new TitulaireType();
        titulaireType.setTypeIdentifiant(TypeIdentifiantType.HORS_UE);
        var initialId = "thisisaveryveryverylonglongvaluethisisaveryveryverylonglongvalue";
        titulaireType.setId(initialId);
        TitulaireDTO titulaireDTO = contractEssentialDataMapperImpl.toTitulaire(titulaireType);
        assertEquals(titulaireDTO.getTypeIdentifiant(), "HORS-UE");
        assertEquals(titulaireDTO.getId().length(), 18);
    }

}
