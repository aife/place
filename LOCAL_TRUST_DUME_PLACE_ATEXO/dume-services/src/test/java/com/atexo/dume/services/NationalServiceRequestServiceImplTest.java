package com.atexo.dume.services;

import com.atexo.dume.dto.sn.request.SNEssentialDataDTO;
import com.atexo.dume.dto.sn.request.SNRequestDTO;
import com.atexo.dume.dto.sn.response.SNOauth2Token;
import com.atexo.dume.model.DumePlatform;
import com.atexo.dume.repository.DumePlatformRepository;
import com.atexo.dume.services.exception.NationalServiceException;
import com.atexo.dume.services.impl.NationalServiceRequestServiceImpl;
import com.atexo.dume.services.impl.Oauth2Client;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;

/**
 * Class Test for the @NationalServiceRequestServiceImpl
 */
public class NationalServiceRequestServiceImplTest {

    @Mock
    private Oauth2Client oauth2Client;

    @Mock
    private DumePlatformRepository platformRepository;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private NationalServiceRequestServiceImpl nationalServiceRequestService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        var token = new SNOauth2Token();
        token.setAccessToken("My access token");
        when(oauth2Client.getOauth2Token(any())).thenReturn(token);
        when(platformRepository.findById(anyLong())).thenReturn(Optional.of(new DumePlatform()));
        //when(restTemplate.postForEntity(any(),any(),any())).thenReturn(ResponseEntity.ok(null));
    }

    @Test
    public void restA() throws NationalServiceException {
        SNRequestDTO snRequestDTO = new SNRequestDTO();
        nationalServiceRequestService.restAcheteur(anyLong(), snRequestDTO, Object.class);

    }

    @Test
    public void restOE() throws NationalServiceException {
        SNRequestDTO snRequestDTO = new SNRequestDTO();
        nationalServiceRequestService.restOE(any(), snRequestDTO, Object.class);

    }

    @Test
    public void restMetadonne() throws NationalServiceException {
        SNRequestDTO snRequestDTO = new SNRequestDTO();
        nationalServiceRequestService.restMetadonnees(any(), snRequestDTO, Object.class);
    }

    @Test
    public void restDonneesEssentielles() throws NationalServiceException {

        SNEssentialDataDTO snEssentialDataDTO = new SNEssentialDataDTO();
        nationalServiceRequestService.restEssentialData(any(), snEssentialDataDTO, Object.class);
    }

    @Test(expected = NationalServiceException.class)
    public void restAException() throws NationalServiceException {
        SNRequestDTO snRequestDTO = new SNRequestDTO();
        when(restTemplate.postForEntity(anyString(), any(), any())).thenThrow(new RestClientException("ERROR"));
        nationalServiceRequestService.restAcheteur(anyLong(), snRequestDTO, Object.class);

    }

    @Test(expected = NationalServiceException.class)
    public void restOEException() throws NationalServiceException {
        SNRequestDTO snRequestDTO = new SNRequestDTO();
        when(restTemplate.postForEntity(anyString(), any(), any())).thenThrow(new RestClientException("ERROR"));
        nationalServiceRequestService.restOE(anyLong(), snRequestDTO, Object.class);

    }

    @Test(expected = NationalServiceException.class)
    public void restMetadonneException() throws NationalServiceException {
        SNRequestDTO snRequestDTO = new SNRequestDTO();
        when(restTemplate.postForEntity(anyString(), any(), any())).thenThrow(new RestClientException("ERROR"));
        nationalServiceRequestService.restMetadonnees(anyLong(), snRequestDTO, Object.class);
    }

    @Test(expected = NationalServiceException.class)
    public void restDonneesEssentiellesException() throws NationalServiceException {
        SNEssentialDataDTO snEssentialDataDTO = new SNEssentialDataDTO();
        when(restTemplate.postForEntity(anyString(), any(), any())).thenThrow(new RestClientException("ERROR"));
        nationalServiceRequestService.restEssentialData(any(), snEssentialDataDTO, Object.class);
    }
}
