package com.atexo.dume.services;

import com.atexo.dume.dto.DumeRequestContextDTO;
import com.atexo.dume.dto.DumeRequestDTO;
import com.atexo.dume.dto.LotDTO;
import com.atexo.dume.dto.dataset.DumeRequestContextDataDTOSet;
import com.atexo.dume.dto.dataset.DumeRequestDTODataSet;
import com.atexo.dume.mapper.DumeRequestMapper;
import com.atexo.dume.model.DumeRequest;
import com.atexo.dume.model.DumeRequestContext;
import com.atexo.dume.model.DumeRequestModel;
import com.atexo.dume.model.EtatDume;
import com.atexo.dume.model.Lot;
import com.atexo.dume.model.dataset.DumeRequestContextDataSet;
import com.atexo.dume.model.dataset.DumeRequestDataSet;
import com.atexo.dume.model.dataset.ExclusionCriteriaDataSet;
import com.atexo.dume.model.dataset.SelectionCriteriaDataSet;
import com.atexo.dume.repository.DumeRequestModelRepository;
import com.atexo.dume.repository.DumeRequestRepository;
import com.atexo.dume.services.api.CriteriaService;
import com.atexo.dume.services.api.ProfileParamService;
import com.atexo.dume.services.api.SchedularTask;
import com.atexo.dume.services.api.XMLDumeService;
import com.atexo.dume.services.exception.DumeResourceNotFoundException;
import com.atexo.dume.services.exception.NationalServiceException;
import com.atexo.dume.services.exception.ServiceException;
import com.atexo.dume.services.impl.DumeRequestServiceImpl;
import grow.names.specification.ubl.schema.xsd.espdrequest_1.ESPDRequestType;
import grow.names.specification.ubl.schema.xsd.espdrequest_1.ObjectFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.oxm.MarshallingFailureException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static org.hibernate.validator.internal.util.CollectionHelper.asSet;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

public class DumeRequestServiceImplTest {

    @Mock
    private DumeRequestRepository dumeRequestRepository;

    @Mock
    private CriteriaService criteriaService;

    @Mock
    private DumeRequestMapper dumeRequestMapper;

    @Mock
    private XMLDumeService xmlDumeService;

    @Mock
    private DumeRequestModelRepository dumeRequestModelRepository;
    @Mock
    private ProfileParamService profileParamService;

    @Mock
    private ObjectFactory objectFactory;

    @Mock
    private SchedularTask schedularTask;

    @InjectMocks
    private DumeRequestServiceImpl dumeRequestService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = ServiceException.class)
    public void createDumeRequestException() throws ServiceException, DumeResourceNotFoundException {
        final DumeRequestContext context = DumeRequestContextDataSet.sampleContext();

        when(criteriaService.getAllSelectionCriteriaWithoutALLCriteria()).thenReturn(new ArrayList<>());
        when(criteriaService.getAllExclusionCriteria()).thenReturn(new ArrayList<>());

        dumeRequestService.createDumeRequest(context);
    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void createDumeRequestModelNotFoundException() throws ServiceException, DumeResourceNotFoundException {
        Optional<DumeRequestModel> dumeRequestModelOptional = Optional.empty();
        final DumeRequestContext context = DumeRequestContextDataSet.sampleContext();

        when(profileParamService.getValue(any(), any(), any(), any())).thenReturn("12");
        when(dumeRequestModelRepository.findById(any())).thenReturn(dumeRequestModelOptional);
        when(criteriaService.getAllSelectionCriteriaWithoutALLCriteria()).thenReturn(new ArrayList<>());
        when(criteriaService.getAllExclusionCriteria()).thenReturn(new ArrayList<>());

        dumeRequestService.createDumeRequest(context);


    }

    @Test
    public void createDumeRequestModelOK() throws ServiceException, DumeResourceNotFoundException {
        Optional<DumeRequestModel> dumeRequestModelOptional = Optional.of(new DumeRequestModel());
        final DumeRequestContext context = DumeRequestContextDataSet.sampleContext();
        dumeRequestModelOptional.get().setSelectionCriteriaList(new ArrayList<>());
        dumeRequestModelOptional.get().setExclusionCriteriaList(new ArrayList<>());

        when(profileParamService.getValue(any(), any(), any(), any())).thenReturn("12");
        when(dumeRequestModelRepository.findById(any())).thenReturn(dumeRequestModelOptional);

        DumeRequest dumeRequest1 = dumeRequestService.createDumeRequest(context);
        assertNotNull("Le service retourne rien", dumeRequest1);
        assertEquals("la valeur d'id ne sont pas egal", context.getId(), dumeRequest1.getContext().getId());

    }
    @Test
    public void getDumeRequestDTOByIdTestOk() throws DumeResourceNotFoundException {
        final DumeRequestDTO dumeRequestDTO = DumeRequestDTODataSet.sampleWithId();
        final Optional<DumeRequest> dto = Optional.of(DumeRequestDataSet.sampleWithId());
        final Long id = DumeRequestDTODataSet.DUME_REQUEST_DTO_IDENTIFIER_1;
        when(dumeRequestRepository.findById(id)).thenReturn(dto);
        when(dumeRequestMapper.toDto(any())).thenReturn(dumeRequestDTO);

        DumeRequestDTO dtoResult = dumeRequestService.getDumeRequestDTOById(id);
        assertNotNull("Le Dto est NULL", dtoResult);
        assertEquals(dtoResult.getId(), id);
    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void getDumeRequestDTOByIdTestNOk() throws DumeResourceNotFoundException {
        final Optional<DumeRequest> dto = Optional.empty();
        final Long id = DumeRequestDTODataSet.DUME_REQUEST_DTO_IDENTIFIER_1;

        when(dumeRequestRepository.findById(id)).thenReturn(dto);
        DumeRequestDTO dtoResult = dumeRequestService.getDumeRequestDTOById(id);

    }

    @Test
    public void updateDumeRequestOk() {
        final Optional<DumeRequest> optional = Optional.of(DumeRequestDataSet.sampleWithId());
        final DumeRequestContextDTO context = DumeRequestContextDataDTOSet.contextSample();
        optional.get().setContext(DumeRequestContextDataSet.sampleContext());
        final Long id = DumeRequestDTODataSet.DUME_REQUEST_DTO_IDENTIFIER_1;

        when(dumeRequestRepository.findById(id)).thenReturn(optional);
        Long idResult = dumeRequestService.updateContextDumeRequestById(id, context);

        assertNotNull(idResult);
        assertEquals(optional.get().getId(), idResult);
    }

    @Test
    public void updateDumeRequestNOk() {
        final Optional<DumeRequest> optional = Optional.empty();
        final DumeRequestContextDTO context = DumeRequestContextDataDTOSet.contextSample();
        final Long id = DumeRequestDTODataSet.DUME_REQUEST_DTO_IDENTIFIER_1;

        when(dumeRequestRepository.findById(id)).thenReturn(optional);

        Long idResult = dumeRequestService.updateContextDumeRequestById(id, context);

        assertNull(idResult);
    }

    @Test
    public void deleteDumeRequestOK() throws DumeResourceNotFoundException {
        final Optional<DumeRequest> optional = Optional.of(DumeRequestDataSet.sampleWithSimpleStructure());
        final Long id = DumeRequestDTODataSet.DUME_REQUEST_DTO_IDENTIFIER_1;
        optional.get().setContext(DumeRequestContextDataSet.sampleContext());

        when(dumeRequestRepository.findById(id)).thenReturn(optional);
        Boolean deleted = dumeRequestService.deleteDumeRequestById(id);

        assertNotNull(deleted);
        assertTrue(deleted);
    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void deleteDumeRequestNOK() throws DumeResourceNotFoundException {
        final Optional<DumeRequest> optional = Optional.empty();
        final Long id = DumeRequestDTODataSet.DUME_REQUEST_DTO_IDENTIFIER_1;

        when(dumeRequestRepository.findById(id)).thenReturn(optional);

        dumeRequestService.deleteDumeRequestById(id);
    }


    @Test
    public void updateDumeRequestByDTOOkWithoutEcrassement() throws ServiceException, DumeResourceNotFoundException {
        final Optional<DumeRequest> optional = Optional.of(DumeRequestDataSet.sampleWithSimpleStructure());
        optional.get().setContext(DumeRequestContextDataSet.sampleContext());
        final Optional<DumeRequestDTO> optionalDTO = Optional.of(DumeRequestDTODataSet.sampleWithId());

        final Long id = DumeRequestDataSet.SIMPLE_USER_IDENTIFIER_1;

        when(dumeRequestRepository.findById(any())).thenReturn(optional);
        when(dumeRequestRepository.save(any())).thenReturn(optional.get());
        when(dumeRequestMapper.toDto(any())).thenReturn(optionalDTO.get());


        DumeRequest dumeRequest = dumeRequestService.updateDumeRequestByDTO(optionalDTO.get());

        assertNotNull(dumeRequest);
        assertEquals(dumeRequest.getId(), id);
    }

    @Test
    public void updateDumeRequestByDTOOkWithEcrassement() throws ServiceException, DumeResourceNotFoundException {
        final Optional<DumeRequest> optional = Optional.of(DumeRequestDataSet.sampleWithSimpleStructure());
        optional.get().setContext(DumeRequestContextDataSet.sampleContext());
        final Optional<DumeRequestDTO> optionalDTO = Optional.of(DumeRequestDTODataSet.sampleWithId());

        final Long id = DumeRequestDataSet.SIMPLE_USER_IDENTIFIER_1;

        when(dumeRequestRepository.findById(any())).thenReturn(optional);
        when(dumeRequestRepository.save(any())).thenReturn(optional.get());
        when(dumeRequestMapper.toDto(any())).thenReturn(optionalDTO.get());

        DumeRequest dumeRequest = dumeRequestService.updateDumeRequestByDTO(optionalDTO.get());

        assertNotNull(dumeRequest);
        assertEquals(dumeRequest.getId(), id);
    }

    @Test
    public void updateDumeRequestByDTONOK() throws ServiceException, DumeResourceNotFoundException {
        final Optional<DumeRequestDTO> optionalDTO = Optional.of(DumeRequestDTODataSet.sampleWithId());

        when(dumeRequestRepository.findById(any())).thenReturn(Optional.empty());

        DumeRequest dumeRequest = dumeRequestService.updateDumeRequestByDTO(optionalDTO.get());

        assertNull(dumeRequest);
    }

    @Test
    public void getAllDumeRequestDTOOk() {
        final List<DumeRequest> dumeRequests = asList(DumeRequestDataSet.sampleWithId(), DumeRequestDataSet.sample2WithId());
        final List<DumeRequestDTO> dumeRequestDTOS = asList(DumeRequestDTODataSet.sampleWithId(), DumeRequestDTODataSet.sample2WithId());

        when(dumeRequestRepository.findAll()).thenReturn(dumeRequests);
        when(dumeRequestMapper.toDtoList(dumeRequests)).thenReturn(dumeRequestDTOS);

        List<DumeRequestDTO> requestDTOList = dumeRequestService.getAllDumeRequestDTO();
        assertNotNull(requestDTOList);
        assertEquals(requestDTOList.size(), dumeRequestDTOS.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getAllDumeRequestDTOException() {

        when(dumeRequestRepository.findAll()).thenThrow(new IllegalArgumentException());

        List<DumeRequestDTO> requestDTOList = dumeRequestService.getAllDumeRequestDTO();
    }

    @Test
    public void getDumeRequestByIdOk() {
        final Optional<DumeRequest> optional = Optional.of(DumeRequestDataSet.sampleWithId());

        when(dumeRequestRepository.findById(any())).thenReturn(optional);

        Optional<DumeRequest> res = dumeRequestService.getDumeRequestById(100L);

        assertNotNull(res);
        assertTrue(res.isPresent());
    }

    @Test
    public void generateXMLByIdOK() throws Exception {
        final Optional<DumeRequest> optional = Optional.of(DumeRequestDataSet.sampleWithSimpleStructure());
        DumeRequest request=optional.get();
        request.setContext(DumeRequestContextDataSet.sampleContext());
        when(dumeRequestRepository.findById(any())).thenReturn(optional);
        when(xmlDumeService.generateXMLAcheteur(any(DumeRequest.class))).thenReturn("");
        String resultXML=dumeRequestService.generateDumeRequestXML(request.getId());
        assertNotNull(resultXML);
    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void generateXMLByIdNOK() throws ServiceException, DumeResourceNotFoundException {
        final Optional<DumeRequest> optional = Optional.empty();
        when(dumeRequestRepository.findById(any())).thenReturn(optional);
        String resultXML = dumeRequestService.generateDumeRequestXML(100L);
        assertNotNull(resultXML);
    }

    @Test(expected = ServiceException.class)
    public void generateXMLByIdException() throws ServiceException, DumeResourceNotFoundException {
        final Optional<DumeRequest> optional = Optional.of(DumeRequestDataSet.sampleWithSimpleStructure());
        DumeRequest request = optional.get();
        request.setContext(DumeRequestContextDataSet.sampleContext());
        ESPDRequestType espdRequestType = new ESPDRequestType();
        when(xmlDumeService.generateXMLAcheteur(any(DumeRequest.class))).thenThrow(new MarshallingFailureException(""));
        when(dumeRequestRepository.findById(any())).thenReturn(optional);
        dumeRequestService.generateDumeRequestXML(100L);
    }

    @Test
    public void setCriteriaDefautOk() {
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithId();

        when(criteriaService.getAllSelectionCriteriaWithoutALLCriteria()).thenReturn(
                asList(SelectionCriteriaDataSet.sample1WithId(), SelectionCriteriaDataSet.sample1WithId()));
        when(criteriaService.getAllExclusionCriteria()).thenReturn(
                asList(ExclusionCriteriaDataSet.sample1WithId()));

        dumeRequestService.setCriteriaDefaut(dumeRequest);

        assertEquals(1, dumeRequest.getExclusionCriteriaList().size());
        assertEquals(2, dumeRequest.getSelectionCriteriaList().size());
    }


    @Test
    public void updateDLROIntoSNEmpty() {

        when(dumeRequestRepository.findAllByEtatDume(EtatDume.DLRO_MODIFIEE)).thenReturn(new ArrayList<>());

        dumeRequestService.updateDLROIntoSN();
    }

    @Test
    public void updateDLROIntoSNNotEmpty() throws NationalServiceException {

        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithSimpleStructureWithContext();
        when(dumeRequestRepository.findAllByEtatDume(EtatDume.DLRO_MODIFIEE)).thenReturn(asList(dumeRequest));
        doNothing().when(schedularTask).updateDLROAcheteur(any(Long.class));

        dumeRequestService.updateDLROIntoSN();
    }


    @Test
    public void remplaceDumeWithSNNotEmpty() throws NationalServiceException {

        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithSimpleStructureWithContext();

        when(dumeRequestRepository.findAllByEtatDume(EtatDume.A_REMPLACER)).thenReturn(asList(dumeRequest));
        doNothing().when(schedularTask).updateDLROAcheteur(any(Long.class));


        dumeRequestService.remplaceDumeWithSN();
    }


    @Test
    public void remplaceDumeWithSNEmpty() {

        when(dumeRequestRepository.findAllByEtatDume(EtatDume.A_REMPLACER)).thenReturn(new ArrayList<>());
        doNothing().when(schedularTask).replaceDumeAcheteur(any(Long.class));
        dumeRequestService.remplaceDumeWithSN();
    }

    @Test
    public void downloadTemplateOEEmpty() {
        when(dumeRequestRepository.findAllByEtatDumeAndNumDumeSNIsNotNullAndPdfRetrievedAndXmlOERetrieved(EtatDume.PUBLIE, true, false)).thenReturn(new ArrayList<>());

        dumeRequestService.downloadTemplateOE();
    }

    @Test
    public void downloadTemplateOENotEmpty() {
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithSimpleStructureWithContext();
        when(dumeRequestRepository.findAllByEtatDumeAndNumDumeSNIsNotNullAndPdfRetrievedAndXmlOERetrieved(EtatDume.PUBLIE, true, false)).thenReturn(asList(dumeRequest));

        doNothing().when(schedularTask).downloadTemplateXMLOE(any(Long.class));

        dumeRequestService.downloadTemplateOE();


    }

    @Test
    public void publishAllDumeRequestsEmpty() {
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithSimpleStructureWithContext();
        when(dumeRequestRepository.findAllByEtatDume(EtatDume.A_PUBLIER)).thenReturn(new ArrayList<>());

        dumeRequestService.publishAllDumeRequests();

    }

    @Test
    public void publishAllDumeRequestsNotEmpty() {
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithSimpleStructureWithContext();
        when(dumeRequestRepository.findAllByEtatDume(EtatDume.A_PUBLIER)).thenReturn(asList(dumeRequest));
        doNothing().when(schedularTask).publishDumeAcheteur(any(Long.class));
        dumeRequestService.publishAllDumeRequests();
    }

    @Test
    public void retrieveAllDumeRequestsPDFEmty() {
        when(dumeRequestRepository.findAllByEtatDumeAndNumDumeSNIsNotNullAndPdfRetrieved(EtatDume.PUBLIE, false)).thenReturn(new ArrayList<>());

        dumeRequestService.retrieveAllDumeRequestsPDF();
    }

    @Test
    public void retrieveAllDumeRequestsPDFNotEmpty() {
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithSimpleStructureWithContext();
        when(dumeRequestRepository.findAllByEtatDumeAndNumDumeSNIsNotNullAndPdfRetrieved(EtatDume.PUBLIE, false)).thenReturn(asList(dumeRequest));

        doNothing().when(schedularTask).getPDFAcheteur(any(Long.class));

        dumeRequestService.retrieveAllDumeRequestsPDF();

    }

    @Test
    public void checkPurgeCriteriaLotEmptyLotDTO() {
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithSimpleStructure();

        Boolean result = dumeRequestService.checkPurgeCriteriaLot(dumeRequest, emptySet());

        assertNotNull(result);
        assertEquals(Boolean.FALSE, result);


    }

    @Test
    public void checkPurgeCriteriaLotSuppression() {
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithSimpleStructureAndLot();

        Boolean result = dumeRequestService.checkPurgeCriteriaLot(dumeRequest, emptySet());

        assertNotNull(result);
        assertEquals(Boolean.TRUE, result);
    }

    @Test
    public void checkPurgeCriteriaLotAdding() {
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithSimpleStructureAndLot();

        Boolean result = dumeRequestService.checkPurgeCriteriaLot(dumeRequest, asSet(new LotDTO("1", "1"), new LotDTO("2", "2")));

        assertNotNull(result);
        assertEquals(Boolean.TRUE, result);

    }

    @Test
    public void checkPurgeCriteriadoNothing() {
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithSimpleStructureAndLot();

        Boolean result = dumeRequestService.checkPurgeCriteriaLot(dumeRequest, asSet(new LotDTO("1", "1")));

        assertNotNull(result);
        assertEquals(Boolean.FALSE, result);

    }

    @Test
    public void checkPurgeCriteriaLotModification() {
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithSimpleStructureAndLot();

        Boolean result = dumeRequestService.checkPurgeCriteriaLot(dumeRequest, asSet(new LotDTO("1", "2"), new LotDTO("2", "2")));

        assertNotNull(result);
        assertEquals(Boolean.TRUE, result);
    }

    @Test
    public void purgeCriteriaLot() {
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithSimpleStructure();
        DumeRequest result = dumeRequestService.purgeCriteriaLot(dumeRequest);

        assertNotNull(result);
    }

    @Test
    public void checkValidLotNotAlloti() {
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithSimpleStructureAndLot();
        Set<Lot> result = dumeRequestService.checkValidLot(dumeRequest, emptySet());

        assertNotNull(result);
        assertTrue(result.isEmpty());

    }

    @Test
    public void checkValidLotAllotiAtLeastOneCriteiraAssiocatedToAll() {
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithSimpleStructure();
        Set<Lot> result = dumeRequestService.checkValidLot(dumeRequest, asSet(new Lot("1", "1", null)));

        assertNotNull(result);
        assertTrue(result.isEmpty());

    }

    @Test
    public void checkValidLotAllotiCheckCriteriaValid() {
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithSimpleStructureAndLot();
        Set<Lot> result = dumeRequestService.checkValidLot(dumeRequest, asSet(new Lot("1", "1", null)));

        assertNotNull(result);
        assertTrue(result.isEmpty());

    }

    @Test
    public void checkValidLotAllotiCheckCriteriaNotValid() {
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithSimpleStructureAndLot();
        Set<Lot> result = dumeRequestService.checkValidLot(dumeRequest, asSet(new Lot("2", "1", null)));

        assertNotNull(result);
        assertFalse(result.isEmpty());

    }

    @Test(expected = DumeResourceNotFoundException.class)
    public void generateXMLByNumeroSNNotFound() throws DumeResourceNotFoundException, ServiceException {
        when(dumeRequestRepository.findByNumDumeSN(any())).thenReturn(Optional.empty());

        byte[] res = dumeRequestService.generateXMLByNumeroSN("012");
    }

    @Test(expected = ServiceException.class)
    public void generateXMLByNumeroSNMarshallingException() throws DumeResourceNotFoundException, ServiceException {

        when(dumeRequestRepository.findByNumDumeSN(any())).thenReturn(Optional.of(DumeRequestDataSet.sampleWithSimpleStructureAndLot()));

        when(xmlDumeService.generateXMLAcheteur(any())).thenThrow(new MarshallingFailureException(""));

        byte[] res = dumeRequestService.generateXMLByNumeroSN("012");


    }

    @Test
    public void generateXMLByNumeroSNOK() throws DumeResourceNotFoundException, ServiceException {

        when(dumeRequestRepository.findByNumDumeSN(any())).thenReturn(Optional.of(DumeRequestDataSet.sampleWithSimpleStructureAndLot()));

        when(xmlDumeService.generateXMLAcheteur(any())).thenReturn("XMLTEST");

        byte[] res = dumeRequestService.generateXMLByNumeroSN("012");
        assertNotNull(res);
        assertArrayEquals("XMLTEST".getBytes(), res);


    }
}
