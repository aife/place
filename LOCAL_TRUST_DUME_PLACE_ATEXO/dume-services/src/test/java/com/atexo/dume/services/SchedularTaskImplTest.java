package com.atexo.dume.services;

import com.atexo.dume.dto.sn.request.SNDumeA;
import com.atexo.dume.dto.sn.request.SNRequestDTO;
import com.atexo.dume.dto.sn.response.SNDumeSavedResponseDTO;
import com.atexo.dume.dto.sn.response.SNMessageList;
import com.atexo.dume.dto.sn.response.SNResponse;
import com.atexo.dume.mapper.sn.SNRequestMapper;
import com.atexo.dume.model.DumeRequest;
import com.atexo.dume.model.DumeRequestContext;
import com.atexo.dume.model.DumeResponse;
import com.atexo.dume.model.EtatDume;
import com.atexo.dume.model.dataset.DumeRequestDataSet;
import com.atexo.dume.model.dataset.DumeResponseDataSet;
import com.atexo.dume.repository.DumeRequestRepository;
import com.atexo.dume.repository.DumeResponseRepository;
import com.atexo.dume.services.api.NationalServiceRequestService;
import com.atexo.dume.services.api.XMLDumeService;
import com.atexo.dume.services.exception.NationalServiceException;
import com.atexo.dume.services.impl.SchedularTaskImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Test Class for the {@see SchedularTaskImpl}
 */
public class SchedularTaskImplTest {

    @Mock
    private SNRequestMapper snRequestMapper;

    @Mock
    private NationalServiceRequestService nationalServiceRequestService;

    @Mock
    private DumeRequestRepository dumeRequestRepository;

    @Mock
    private DumeResponseRepository dumeResponseRepository;

    @Mock
    private XMLDumeService xmlDumeService;

    @InjectMocks
    private SchedularTaskImpl schedularTask;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void publishDumeOEOK() throws NationalServiceException {
        schedularTask.setMaxPublicationAttempts(3);
        final DumeResponse response = DumeResponseDataSet.sampleWithRequestAndContext();
        SNRequestDTO snRequestDTO = new SNRequestDTO();
        SNDumeSavedResponseDTO snDumeSavedResponseDTO = new SNDumeSavedResponseDTO();
        snDumeSavedResponseDTO.setResponse(new SNResponse());
        snDumeSavedResponseDTO.getResponse().setMessageList(new ArrayList<>());
        var wrapper = ResponseEntity.ok(snDumeSavedResponseDTO);
        when(dumeResponseRepository.getOne(any())).thenReturn(response);
        when(xmlDumeService.generateXMLOE(any())).thenReturn("xmlString");
        when(snRequestMapper.toSnRequestDto(any(), any(), any())).thenReturn(snRequestDTO);
        doReturn(wrapper).when(nationalServiceRequestService).restOE(any(), any(), any());
        schedularTask.publishDumeOE(response.getId());
        verify(xmlDumeService, times(1)).generateXMLOE(any(DumeResponse.class));
        verify(nationalServiceRequestService, times(1)).restOE(any(), any(), any());
        assertEquals(response.getEtatDume(), EtatDume.PUBLIE);
    }

    @Test
    public void publishDumeOEKO() throws NationalServiceException {
        schedularTask.setMaxPublicationAttempts(3);
        final DumeResponse response = DumeResponseDataSet.sampleWithRequestAndContext();
        response.setNbPublicationTries(3);
        when(dumeResponseRepository.getOne(any())).thenReturn(response);
        schedularTask.publishDumeOE(response.getId());
        verify(dumeResponseRepository, times(1)).save(response);
        assertEquals(response.getEtatDume(), EtatDume.ERREUR_SN);
    }

    @Test
    public void publishDumeOESNError() throws NationalServiceException {
        schedularTask.setMaxPublicationAttempts(3);
        final DumeResponse response = DumeResponseDataSet.sampleWithRequestAndContext();

        SNRequestDTO snRequestDTO = new SNRequestDTO();
        snRequestDTO.setDumeA(new SNDumeA());

        SNDumeSavedResponseDTO snDumeSavedResponseDTO = new SNDumeSavedResponseDTO();
        snDumeSavedResponseDTO.setResponse(new SNResponse());
        snDumeSavedResponseDTO.getResponse().setMessageList(new ArrayList<>());
        SNMessageList erreurSN = new SNMessageList();
        erreurSN.setMessage("ERREUR SN");
        snDumeSavedResponseDTO.getResponse().getMessageList().add(erreurSN);

        when(dumeResponseRepository.getOne(any())).thenReturn(response);
        when(xmlDumeService.generateXMLOE(any())).thenReturn("xmlString");
        when(snRequestMapper.toSnRequestDto(any(), any(), any())).thenReturn(snRequestDTO);
        var wrapper = ResponseEntity.ok(snDumeSavedResponseDTO);
        doReturn(wrapper).when(nationalServiceRequestService).restOE(any(), any(), any());
        schedularTask.publishDumeOE(1L);
        verify(xmlDumeService, times(1)).generateXMLOE(any(DumeResponse.class));
        verify(nationalServiceRequestService, times(1)).restOE(any(), any(), any());
        verify(dumeResponseRepository, times(1)).save(response);
        assertEquals(response.getEtatDume(), EtatDume.ERREUR_SN);
    }

    @Test
    public void publishDumeAcheteurOK() throws NationalServiceException {
        schedularTask.setMaxPublicationAttempts(3);
        final DumeRequest request = DumeRequestDataSet.sampleWithSimpleStructureWithContext();
        SNRequestDTO snRequestDTO = new SNRequestDTO();
        snRequestDTO.setDumeA(new SNDumeA());
        SNDumeSavedResponseDTO snDumeSavedResponseDTO = new SNDumeSavedResponseDTO();
        snDumeSavedResponseDTO.setResponse(new SNResponse());
        snDumeSavedResponseDTO.getResponse().setMessageList(new ArrayList<>());
        var wrapper = ResponseEntity.ok(snDumeSavedResponseDTO);
        when(dumeRequestRepository.getOne(any())).thenReturn(request);
        when(xmlDumeService.generateXMLAcheteur(any())).thenReturn("xmlString");
        when(snRequestMapper.toSnRequestDto(any(), any(), any())).thenReturn(snRequestDTO);
        doReturn(wrapper).when(nationalServiceRequestService).restAcheteur(any(), any(), any());
        schedularTask.publishDumeAcheteur(request.getId());
        verify(xmlDumeService, times(1)).generateXMLAcheteur(any(DumeRequest.class));
        verify(snRequestMapper, times(1)).toSnRequestDto(any(DumeRequest.class), anyString(), anyString());
        verify(nationalServiceRequestService, times(1)).restAcheteur(any(), any(), any());
        assertEquals(request.getEtatDume(), EtatDume.PUBLIE);
    }

    @Test
    public void publishDumeAcheteurKO() throws NationalServiceException {
        schedularTask.setMaxPublicationAttempts(3);
        final DumeRequest request = DumeRequestDataSet.sampleWithSimpleStructureWithContext();
        request.setNbPublicationTries(3);
        when(dumeRequestRepository.getOne(any())).thenReturn(request);
        schedularTask.publishDumeAcheteur(request.getId());
        verify(dumeRequestRepository, times(1)).save(request);
        assertEquals(request.getEtatDume(), EtatDume.ERREUR_SN);
    }

    @Test
    public void publishDumeAcheteurSNError() throws NationalServiceException {
        schedularTask.setMaxPublicationAttempts(3);
        final DumeRequest request = DumeRequestDataSet.sampleWithSimpleStructureWithContext();

        SNRequestDTO snRequestDTO = new SNRequestDTO();
        snRequestDTO.setDumeA(new SNDumeA());

        SNDumeSavedResponseDTO snDumeSavedResponseDTO = new SNDumeSavedResponseDTO();
        snDumeSavedResponseDTO.setResponse(new SNResponse());
        snDumeSavedResponseDTO.getResponse().setMessageList(new ArrayList<>());
        SNMessageList erreurSN = new SNMessageList();
        erreurSN.setMessage("ERREUR SN");
        snDumeSavedResponseDTO.getResponse().getMessageList().add(erreurSN);

        when(dumeRequestRepository.getOne(any())).thenReturn(request);
        when(xmlDumeService.generateXMLAcheteur(any())).thenReturn("xmlString");
        when(snRequestMapper.toSnRequestDto(any(), any(), any())).thenReturn(snRequestDTO);
        var wrapper = ResponseEntity.ok(snDumeSavedResponseDTO);
        doReturn(wrapper).when(nationalServiceRequestService).restAcheteur(any(), any(), any());


        schedularTask.publishDumeAcheteur(1L);

        verify(xmlDumeService, times(1)).generateXMLAcheteur(any(DumeRequest.class));
        verify(snRequestMapper, times(1)).toSnRequestDto(any(DumeRequest.class), anyString(), anyString());
        verify(nationalServiceRequestService, times(1)).restAcheteur(any(), any(), any());
        verify(dumeRequestRepository, times(1)).save(request);
        assertEquals(request.getEtatDume(), EtatDume.ERREUR_SN);
    }


    @Test
    public void getPDFOE() {
        //TODO Add a test
    }

    @Test
    public void getPDFAcheteur() {
        //TODO Add a test
    }

    @Test
    public void updateDLROOK() throws NationalServiceException {
        schedularTask.setMaxPublicationAttempts(3);
        final DumeRequest request = DumeRequestDataSet.sampleWithSimpleStructureWithContext();

        SNRequestDTO snRequestDTO = new SNRequestDTO();
        snRequestDTO.setDumeA(new SNDumeA());

        SNDumeSavedResponseDTO snDumeSavedResponseDTO = new SNDumeSavedResponseDTO();
        snDumeSavedResponseDTO.setResponse(new SNResponse());
        snDumeSavedResponseDTO.getResponse().setMessageList(new ArrayList<>());
        SNMessageList message = new SNMessageList();
        message.setType("INFO");
        message.setCode("MISE-A-JOUR");
        snDumeSavedResponseDTO.getResponse().getMessageList().add(message);
        var wrapper = ResponseEntity.ok(snDumeSavedResponseDTO);
        when(dumeRequestRepository.getOne(any())).thenReturn(request);
        when(snRequestMapper.toSNUpdateMetadonneDLRO(any(), any())).thenReturn(snRequestDTO);
        doReturn(wrapper).when(nationalServiceRequestService).restMetadonnees(any(), any(), any());
        schedularTask.updateDLROAcheteur(1L);
        verify(snRequestMapper, times(1)).toSNUpdateMetadonneDLRO(any(DumeRequestContext.class), anyString());
        verify(nationalServiceRequestService, times(1)).restMetadonnees(any(), any(), any());
        assertEquals(request.getEtatDume(), EtatDume.PUBLIE);
    }

    @Test
    public void updateDLRORuntimeError() throws NationalServiceException {
        schedularTask.setMaxPublicationAttempts(3);
        final DumeRequest request = DumeRequestDataSet.sampleWithSimpleStructureWithContext();

        SNRequestDTO snRequestDTO = new SNRequestDTO();
        snRequestDTO.setDumeA(new SNDumeA());

        SNDumeSavedResponseDTO snDumeSavedResponseDTO = new SNDumeSavedResponseDTO();
        snDumeSavedResponseDTO.setResponse(new SNResponse());
        snDumeSavedResponseDTO.getResponse().setMessageList(new ArrayList<>());

        when(dumeRequestRepository.getOne(any())).thenReturn(request);
        when(snRequestMapper.toSNUpdateMetadonneDLRO(any(), any())).thenReturn(snRequestDTO);

        doThrow(new RestClientException("Exception")).when(nationalServiceRequestService).restMetadonnees(any(), any(), any());


        schedularTask.updateDLROAcheteur(1L);
        verify(snRequestMapper, times(1)).toSNUpdateMetadonneDLRO(any(DumeRequestContext.class), anyString());
        verify(nationalServiceRequestService, times(1)).restMetadonnees(any(), any(), any());
        verify(dumeRequestRepository, times(1)).save(any(DumeRequest.class));
        assertEquals(request.getNbPublicationTries(), 1);
    }

    @Test
    public void updateDLROSNError() throws NationalServiceException {
        schedularTask.setMaxPublicationAttempts(3);
        final DumeRequest request = DumeRequestDataSet.sampleWithSimpleStructureWithContext();

        SNRequestDTO snRequestDTO = new SNRequestDTO();
        snRequestDTO.setDumeA(new SNDumeA());

        SNDumeSavedResponseDTO snDumeSavedResponseDTO = new SNDumeSavedResponseDTO();
        snDumeSavedResponseDTO.setResponse(new SNResponse());
        snDumeSavedResponseDTO.getResponse().setMessageList(new ArrayList<>());
        SNMessageList erreurSN = new SNMessageList();
        erreurSN.setMessage("ERREUR SN");
        snDumeSavedResponseDTO.getResponse().getMessageList().add(erreurSN);

        when(dumeRequestRepository.getOne(any())).thenReturn(request);
        when(snRequestMapper.toSnRequestDto(any(), any(), any())).thenReturn(snRequestDTO);
        var wrapper = ResponseEntity.ok(snDumeSavedResponseDTO);
        doReturn(wrapper).when(nationalServiceRequestService).restMetadonnees(any(), any(), any());


        schedularTask.updateDLROAcheteur(1L);

        verify(snRequestMapper, times(1)).toSNUpdateMetadonneDLRO(any(DumeRequestContext.class), anyString());
        verify(nationalServiceRequestService, times(1)).restMetadonnees(any(), any(), any());
        verify(dumeRequestRepository, times(1)).save(any(DumeRequest.class));
        assertEquals(request.getEtatDume(), EtatDume.ERREUR_SN);
    }


    @Test
    public void remplaceDumeAcheteurOK() throws NationalServiceException {
        schedularTask.setMaxPublicationAttempts(3);
        final DumeRequest request = DumeRequestDataSet.sampleWithSimpleStructureWithContext();

        SNRequestDTO snRequestDTO = new SNRequestDTO();
        snRequestDTO.setDumeA(new SNDumeA());

        SNDumeSavedResponseDTO snDumeSavedResponseDTO = new SNDumeSavedResponseDTO();
        snDumeSavedResponseDTO.setResponse(new SNResponse());
        snDumeSavedResponseDTO.getResponse().setMessageList(new ArrayList<>());

        var wrapper = ResponseEntity.ok(snDumeSavedResponseDTO);


        when(dumeRequestRepository.getOne(any())).thenReturn(request);
        when(xmlDumeService.generateXMLAcheteur(any())).thenReturn("xmlString");
        when(snRequestMapper.toSnRequestDto(any(), any(), any())).thenReturn(snRequestDTO);

        doReturn(wrapper).when(nationalServiceRequestService).restAcheteur(any(), any(), any());


        schedularTask.replaceDumeAcheteur(1L);

        verify(xmlDumeService, times(1)).generateXMLAcheteur(any(DumeRequest.class));
        verify(snRequestMapper, times(1)).toSnRequestDto(any(DumeRequest.class), anyString(), anyString());
        verify(nationalServiceRequestService, times(1)).restAcheteur(any(), any(), any());
        assertEquals(request.getEtatDume(), EtatDume.REMPLACE);
    }

    @Test
    public void remplaceDumeAcheteurRuntimeError() throws NationalServiceException {
        schedularTask.setMaxPublicationAttempts(3);
        final DumeRequest request = DumeRequestDataSet.sampleWithSimpleStructureWithContext();

        SNRequestDTO snRequestDTO = new SNRequestDTO();
        snRequestDTO.setDumeA(new SNDumeA());

        SNDumeSavedResponseDTO snDumeSavedResponseDTO = new SNDumeSavedResponseDTO();
        snDumeSavedResponseDTO.setResponse(new SNResponse());
        snDumeSavedResponseDTO.getResponse().setMessageList(new ArrayList<>());

        when(dumeRequestRepository.getOne(any())).thenReturn(request);
        when(xmlDumeService.generateXMLAcheteur(any())).thenReturn("xmlString");
        when(snRequestMapper.toSnRequestDto(any(), any(), any())).thenReturn(snRequestDTO);

        doThrow(new RestClientException("Exception")).when(nationalServiceRequestService).restAcheteur(any(), any(), any());


        schedularTask.replaceDumeAcheteur(1L);

        verify(xmlDumeService, times(1)).generateXMLAcheteur(any(DumeRequest.class));
        verify(snRequestMapper, times(1)).toSnRequestDto(any(DumeRequest.class), anyString(), anyString());
        verify(nationalServiceRequestService, times(1)).restAcheteur(any(), any(), any());
        verify(dumeRequestRepository, times(1)).save(any(DumeRequest.class));
        assertEquals(request.getNbPublicationTries(), 1);
    }

    @Test
    public void remplaceDumeAcheteurSNError() throws NationalServiceException {
        schedularTask.setMaxPublicationAttempts(3);
        final DumeRequest request = DumeRequestDataSet.sampleWithSimpleStructureWithContext();

        SNRequestDTO snRequestDTO = new SNRequestDTO();
        snRequestDTO.setDumeA(new SNDumeA());

        SNDumeSavedResponseDTO snDumeSavedResponseDTO = new SNDumeSavedResponseDTO();
        snDumeSavedResponseDTO.setResponse(new SNResponse());
        snDumeSavedResponseDTO.getResponse().setMessageList(new ArrayList<>());
        SNMessageList erreurSN = new SNMessageList();
        erreurSN.setMessage("ERREUR SN");
        snDumeSavedResponseDTO.getResponse().getMessageList().add(erreurSN);

        when(dumeRequestRepository.getOne(any())).thenReturn(request);
        when(xmlDumeService.generateXMLAcheteur(any())).thenReturn("xmlString");
        when(snRequestMapper.toSnRequestDto(any(), any(), any())).thenReturn(snRequestDTO);
        var wrapper = ResponseEntity.ok(snDumeSavedResponseDTO);
        doReturn(wrapper).when(nationalServiceRequestService).restAcheteur(any(), any(), any());


        schedularTask.replaceDumeAcheteur(1L);

        verify(xmlDumeService, times(1)).generateXMLAcheteur(any(DumeRequest.class));
        verify(snRequestMapper, times(1)).toSnRequestDto(any(DumeRequest.class), anyString(), anyString());
        verify(nationalServiceRequestService, times(1)).restAcheteur(any(), any(), any());
        verify(dumeRequestRepository, times(1)).save(any(DumeRequest.class));
        assertEquals(request.getEtatDume(), EtatDume.ERREUR_SN);
    }

    @Test
    public void remplaceDumeAcheteurStandard() throws NationalServiceException {
        schedularTask.setMaxPublicationAttempts(3);
        final DumeRequest request = DumeRequestDataSet.sampleWithSimpleStructureWithContext();
        request.getContext().setDefault(Boolean.TRUE);

        SNRequestDTO snRequestDTO = new SNRequestDTO();
        snRequestDTO.setDumeA(new SNDumeA());

        SNDumeSavedResponseDTO snDumeSavedResponseDTO = new SNDumeSavedResponseDTO();
        snDumeSavedResponseDTO.setResponse(new SNResponse());
        snDumeSavedResponseDTO.getResponse().setMessageList(new ArrayList<>());

        when(dumeRequestRepository.getOne(any())).thenReturn(request);


        schedularTask.replaceDumeAcheteur(1L);

        verify(xmlDumeService, times(0)).generateXMLAcheteur(any(DumeRequest.class));
        verify(snRequestMapper, times(0)).toSnRequestDto(any(DumeRequest.class), anyString(), anyString());
        verify(nationalServiceRequestService, times(0)).restAcheteur(any(), any(), any());
    }
}
