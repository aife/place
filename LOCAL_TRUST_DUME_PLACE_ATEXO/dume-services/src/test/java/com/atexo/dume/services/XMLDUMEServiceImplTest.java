package com.atexo.dume.services;

import com.atexo.dume.mapper.DumeRequestMapper;
import com.atexo.dume.mapper.DumeResponseMapper;
import com.atexo.dume.model.DumeRequest;
import com.atexo.dume.model.DumeResponse;
import com.atexo.dume.model.dataset.DumeRequestDataSet;
import com.atexo.dume.model.dataset.DumeResponseDataSet;
import com.atexo.dume.repository.CriteriaOtherRepository;
import com.atexo.dume.repository.CriteriaSelectionDocumentRepository;
import com.atexo.dume.services.impl.XMLDumeServiceImpl;
import grow.names.specification.ubl.schema.xsd.espdrequest_1.ESPDRequestType;
import grow.names.specification.ubl.schema.xsd.espdresponse_1.ESPDResponseType;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import javax.xml.transform.stream.StreamResult;
import java.util.ArrayList;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

/**
 * est Class for the {@link XMLDumeServiceImpl}
 */
public class XMLDUMEServiceImplTest {

    @Mock
    private CriteriaSelectionDocumentRepository criteriaSelectionDocumentRepository;

    @Mock
    private DumeRequestMapper dumeRequestMapper;

    @Mock
    private DumeResponseMapper dumeResponseMapper;

    @Mock
    private CriteriaOtherRepository criteriaOtherRepository;

    @Mock
    private Jaxb2Marshaller jaxb2Marshaller;

    @Mock
    private grow.names.specification.ubl.schema.xsd.espdrequest_1.ObjectFactory objectFactoryRequest;

    @Mock
    private grow.names.specification.ubl.schema.xsd.espdresponse_1.ObjectFactory objectFactoryResponse;

    @InjectMocks
    private XMLDumeServiceImpl xmldumeService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void generateXMLAcheteur() {
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithSimpleStructureAndLot();

        when(dumeRequestMapper.toFinalEspd(dumeRequest)).thenReturn(new ESPDRequestType());
        when(objectFactoryRequest.createESPDRequest(any(ESPDRequestType.class))).thenReturn(null);
        doNothing().when(jaxb2Marshaller).marshal(any(), any(StreamResult.class));

        xmldumeService.generateXMLAcheteur(dumeRequest);
    }

    @Test
    public void generateXMLOE() {
        final DumeRequest dumeRequest = DumeRequestDataSet.sampleWithSimpleStructureAndLot();
        DumeResponse dumeResponse = DumeResponseDataSet.sampleWithSimpleStructure();
        dumeResponse.setDumeRequest(dumeRequest);
        when(criteriaOtherRepository.findAll()).thenReturn(new ArrayList<>());
        when(criteriaSelectionDocumentRepository.findByDumeDocument(dumeResponse.getDumeRequest())).thenReturn(new ArrayList<>());
        when(dumeResponseMapper.toFinalResponse(any(), any())).thenReturn(new ESPDResponseType());
        when(objectFactoryResponse.createESPDResponse(any(ESPDResponseType.class))).thenReturn(null);
        doNothing().when(jaxb2Marshaller).marshal(any(), any(StreamResult.class));

        xmldumeService.generateXMLOE(dumeResponse);
    }

}
