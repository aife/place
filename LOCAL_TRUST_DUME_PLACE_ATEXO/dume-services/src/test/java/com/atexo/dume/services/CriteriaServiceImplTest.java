package com.atexo.dume.services;

import com.atexo.dume.dto.ExclusionCriteriaDTO;
import com.atexo.dume.dto.OtherCriteriaDTO;
import com.atexo.dume.dto.SelectionCriteriaDTO;
import com.atexo.dume.dto.dataset.ExclusionCriteriaDTODataSet;
import com.atexo.dume.dto.dataset.OtherCriteriaDTODataSet;
import com.atexo.dume.dto.dataset.SelectionCriteriaDTODataSet;
import com.atexo.dume.mapper.CriteriaMapper;
import com.atexo.dume.model.ExclusionCriteria;
import com.atexo.dume.model.OtherCriteria;
import com.atexo.dume.model.SelectionCriteria;
import com.atexo.dume.model.dataset.ExclusionCriteriaDataSet;
import com.atexo.dume.model.dataset.OtherCriteriaDataSet;
import com.atexo.dume.model.dataset.SelectionCriteriaDataSet;
import com.atexo.dume.repository.CriteriaExclusionRepository;
import com.atexo.dume.repository.CriteriaOtherRepository;
import com.atexo.dume.repository.CriteriaSelectionRepository;
import com.atexo.dume.services.impl.CriteriaServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

public class CriteriaServiceImplTest {
    @Mock
    private CriteriaExclusionRepository criteriaExclusionRepository;

    @Mock
    private CriteriaSelectionRepository criteriaSelectionRepository;

    @Mock
    private CriteriaOtherRepository criteriaOtherRepository;

    @Mock
    private CriteriaMapper criteriaMapper;

    @InjectMocks
    private CriteriaServiceImpl criteriaService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllSelectionCriteriaDTOOk() {
        final List<SelectionCriteria> selectionCriteriaList =
                asList(SelectionCriteriaDataSet.sample1WithId(),
                        SelectionCriteriaDataSet.sample2WithId());

        final List<SelectionCriteriaDTO> selectionCriteriaDTOS =
                asList(SelectionCriteriaDTODataSet.sample1WithId(),
                        SelectionCriteriaDTODataSet.sample2WithId());

        when(criteriaSelectionRepository.findAll()).thenReturn(selectionCriteriaList);
        when(criteriaMapper.toSelectionDTOList(selectionCriteriaList)).thenReturn(selectionCriteriaDTOS);

        List<SelectionCriteriaDTO> listResult = criteriaService.getAllSelectionCriteriaDTO();

        assertNotNull(listResult);
        assertEquals(listResult.size(), selectionCriteriaDTOS.size());

    }

    @Test
    public void getAllSelectionCriteriaDTOByNatureOk() {
        final List<SelectionCriteria> selectionCriteriaList = asList(SelectionCriteriaDataSet.sample1WithId(), SelectionCriteriaDataSet.sample2WithId());

        final List<SelectionCriteriaDTO> selectionCriteriaDTOS = asList(SelectionCriteriaDTODataSet.sample1WithId(), SelectionCriteriaDTODataSet.sample2WithId());

        when(criteriaSelectionRepository.findAllByNatureMarketIdentifierIsNullOrNatureMarketIdentifierEquals(anyString())).thenReturn(selectionCriteriaList);
        when(criteriaMapper.toSelectionDTOList(selectionCriteriaList)).thenReturn(selectionCriteriaDTOS);

        List<SelectionCriteriaDTO> listResult = criteriaService.getAllSelectionCriteriaDTOByNature("02");

        assertNotNull(listResult);
        assertEquals(listResult.size(), selectionCriteriaDTOS.size());

    }

    @Test
    public void getAllExclusionCriteriaDTOOk() {

        final List<ExclusionCriteria> exclusionCriteriaList =
                asList(ExclusionCriteriaDataSet.sample1WithId(),
                        ExclusionCriteriaDataSet.sample2WithId());

        final List<ExclusionCriteriaDTO> exclusionCriteriaDTOS =
                asList(ExclusionCriteriaDTODataSet.sample1WithId(),
                        ExclusionCriteriaDTODataSet.sample2WithId());

        when(criteriaExclusionRepository.findAll()).thenReturn(exclusionCriteriaList);
        when(criteriaMapper.toExclusionDTOList(exclusionCriteriaList)).thenReturn(exclusionCriteriaDTOS);

        List<ExclusionCriteriaDTO> listResult = criteriaService.getAllExclusionCriteriaDTO();

        assertNotNull(listResult);
        assertEquals(listResult.size(), exclusionCriteriaDTOS.size());

    }

    @Test(expected = IllegalArgumentException.class)
    public void getAllSelectionCriteriaDTOException() {
        when(criteriaSelectionRepository.findAll()).thenThrow(new IllegalArgumentException());
        List<SelectionCriteriaDTO> listResult = criteriaService.getAllSelectionCriteriaDTO();

    }

    @Test(expected = IllegalArgumentException.class)
    public void getAllExclusionCriteriaDTOException() {
        when(criteriaExclusionRepository.findAll()).thenThrow(new IllegalArgumentException());
        List<ExclusionCriteriaDTO> listResult = criteriaService.getAllExclusionCriteriaDTO();
    }

    @Test
    public void getAllOtherCriteriaDTOOk() {
        final List<OtherCriteria> otherCriterias =
                asList(OtherCriteriaDataSet.sample1WithId(),
                        OtherCriteriaDataSet.sample2WithId());

        final List<OtherCriteriaDTO> selectionCriteriaDTOS =
                asList(OtherCriteriaDTODataSet.sample1WithId(),
                        OtherCriteriaDTODataSet.sample2WithId());

        when(criteriaOtherRepository.findAll()).thenReturn(otherCriterias);
        when(criteriaMapper.toDTO(OtherCriteriaDataSet.sample1WithId())).thenReturn(OtherCriteriaDTODataSet.sample1WithId());
        when(criteriaMapper.toDTO(OtherCriteriaDataSet.sample2WithId())).thenReturn(OtherCriteriaDTODataSet.sample2WithId());

        List<OtherCriteriaDTO> listResult = criteriaService.getAllOtherCriteriaDTO();

        assertNotNull(listResult);
        assertEquals(listResult.size(), selectionCriteriaDTOS.size());

    }

    @Test(expected = IllegalArgumentException.class)
    public void getAllOtherCriteriaDTOException() {
        when(criteriaOtherRepository.findAll()).thenThrow(new IllegalArgumentException());
        List<OtherCriteriaDTO> listResult = criteriaService.getAllOtherCriteriaDTO();

    }

    @Test
    public void getAllSelectionCriteriaWithoutALLCriteriaOK() {
        final List<SelectionCriteria> selectionCriteriaList =
                asList(SelectionCriteriaDataSet.sample1WithId(),
                        SelectionCriteriaDataSet.sample2WithId());
        when(criteriaSelectionRepository.findAllByCodeNotLike(any())).thenReturn(selectionCriteriaList);

        List<SelectionCriteria> criteriaList = criteriaService.getAllSelectionCriteriaWithoutALLCriteria();

        assertNotNull(criteriaList);
        assertNotEquals(criteriaList.size(), 0);
        assertEquals(selectionCriteriaList.size(), criteriaList.size());
    }

    @Test
    public void getAllExclusionCriteria() {
        final List<ExclusionCriteria> exclusionCriteriaList =
                asList(ExclusionCriteriaDataSet.sample1WithId(),
                        ExclusionCriteriaDataSet.sample2WithId());
        when(criteriaExclusionRepository.findAll()).thenReturn(exclusionCriteriaList);

        List<ExclusionCriteria> criteriaList = criteriaService.getAllExclusionCriteria();

        assertNotNull(criteriaList);
        assertNotEquals(criteriaList.size(), 0);
        assertEquals(exclusionCriteriaList.size(), criteriaList.size());
    }

    @Test
    public void getAllSelectionCriteriaByCodeInOK() {
        final List<SelectionCriteria> selectionCriteriaList =
                asList(SelectionCriteriaDataSet.sample1WithId(),
                        SelectionCriteriaDataSet.sample2WithId());
        when(criteriaSelectionRepository.findByCodeIn(any())).thenReturn(selectionCriteriaList);

        List<SelectionCriteria> criteriaList = criteriaService.getAllSelectionCriteriaByCodeIn(new ArrayList<>());

        assertNotNull(criteriaList);
        assertNotEquals(criteriaList.size(), 0);
        assertEquals(selectionCriteriaList.size(), criteriaList.size());
    }

    @Test
    public void getCriteriaSelectionByCode() {
        final SelectionCriteria selectionCriteria = SelectionCriteriaDataSet.sample1WithId();

        when(criteriaSelectionRepository.findByCode(any())).thenReturn(selectionCriteria);

        SelectionCriteria criteria = criteriaService.getCriteriaSelectionByCode("");

        assertNotNull(criteria);
        assertEquals(criteria.getCode(), selectionCriteria.getCode());
    }

    @Test
    public void getCriteriaExclusionByCode() {

        final ExclusionCriteria exclusionCriteria = ExclusionCriteriaDataSet.sample1WithId();

        when(criteriaExclusionRepository.findByCode(any())).thenReturn(exclusionCriteria);

        ExclusionCriteria criteria = criteriaService.getCriteriaExclusionByCode("");

        assertNotNull(criteria);
        assertEquals(criteria.getCode(), exclusionCriteria.getCode());

    }

    @Test
    public void getCriteriaOtherByCode() {
        final OtherCriteria otherCriteria = OtherCriteriaDataSet.sample1WithId();

        when(criteriaOtherRepository.findByCode(any())).thenReturn(otherCriteria);

        OtherCriteria criteria = criteriaService.getCriteriaOtherByCode("");

        assertNotNull(criteria);
        assertEquals(criteria.getCode(), otherCriteria.getCode());
    }

}
