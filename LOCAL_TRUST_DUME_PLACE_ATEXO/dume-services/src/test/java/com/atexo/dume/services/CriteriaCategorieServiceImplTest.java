package com.atexo.dume.services;

import com.atexo.dume.dto.CriteriaCategorieDTO;
import com.atexo.dume.mapper.CriteriaCategorieMapper;
import com.atexo.dume.model.CategorieType;
import com.atexo.dume.model.CriteriaCategorie;
import com.atexo.dume.repository.CriteriaCategorieRepository;
import com.atexo.dume.services.impl.CriteriaCategorieServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class CriteriaCategorieServiceImplTest {

    @Mock
    private CriteriaCategorieRepository criteriaCategorieRepository;

    @Mock
    private CriteriaCategorieMapper criteriaCategorieMapper;

    @InjectMocks
    private CriteriaCategorieServiceImpl criteriaCategorieService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllSelectionCriteriaCategorieDTOOK() {
        final CriteriaCategorieDTO criteriaCategorieDTO = new CriteriaCategorieDTO();
        final CriteriaCategorie criteriaCategorie = new CriteriaCategorie();
        when(criteriaCategorieMapper.toDTOList(any())).thenReturn(Arrays.asList(criteriaCategorieDTO));
        when(criteriaCategorieRepository.findByType(CategorieType.SELECTION)).thenReturn(Arrays.asList(criteriaCategorie));

        List<CriteriaCategorieDTO> results = criteriaCategorieService.getAllSelectionCriteriaCategorieDTO();

        assertNotNull(results);
        assertEquals(1, results.size());
    }
}
