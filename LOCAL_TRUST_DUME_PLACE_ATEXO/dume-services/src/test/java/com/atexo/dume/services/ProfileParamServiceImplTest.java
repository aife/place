package com.atexo.dume.services;

import com.atexo.dume.model.DumeDomain;
import com.atexo.dume.model.DumePlatform;
import com.atexo.dume.model.profile.DumeDomainPropValue;
import com.atexo.dume.model.profile.DumePlatPropValue;
import com.atexo.dume.model.profile.DumeProfileProp;
import com.atexo.dume.model.profile.DumeUsrPropValue;
import com.atexo.dume.repository.DumeDomPropValueRepository;
import com.atexo.dume.repository.DumePlatPropValueRepository;
import com.atexo.dume.repository.DumeUsrPropValueRepository;
import com.atexo.dume.repository.ProfileParamValueRepository;
import com.atexo.dume.services.impl.ProfileParamServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Optional;

import static com.atexo.dume.model.profile.ProfileParamQueryHolder.domainPredicate;
import static com.atexo.dume.model.profile.ProfileParamQueryHolder.platformPredicate;
import static com.atexo.dume.model.profile.ProfileParamQueryHolder.userPredicate;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

public class ProfileParamServiceImplTest {
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }


    @Mock
    private DumeDomPropValueRepository domPropValueRepository;


    @Mock
    private DumePlatPropValueRepository platPropValueRepository;

    @Mock
    private ProfileParamValueRepository profileParamRepository;

    @Mock
    private DumeUsrPropValueRepository usrValueRepository;

    @InjectMocks
    ProfileParamServiceImpl profileParamService;

    @Test(expected = IllegalArgumentException.class)
    public void getDomProfileValueException() {

        when(profileParamRepository.findByPropLabel(any())).thenReturn(null);

        profileParamService.getDomProfileValue("TEST", 12L);

    }

    @Test
    public void getDomProfileValueReturnNull() {
        DumeProfileProp dumeProfileProp = new DumeProfileProp();

        when(profileParamRepository.findByPropLabel(any())).thenReturn(dumeProfileProp);

        String res = profileParamService.getDomProfileValue("TEST", 12L);

        assertNull(res);
    }

    @Test
    public void getDomProfileValueOKButValueNull() {
        DumeProfileProp dumeProfileProp = new DumeProfileProp();
        dumeProfileProp.setId(12L);
        dumeProfileProp.setDomain(true);
        Optional<DumeDomainPropValue> optionalDumeDomainPropValue = Optional.empty();
        when(profileParamRepository.findByPropLabel(any())).thenReturn(dumeProfileProp);
        when(domPropValueRepository.findOne(domainPredicate(12L, anyInt()))).thenReturn(optionalDumeDomainPropValue);
        String res = profileParamService.getDomProfileValue("TEST", 12L);

        assertNull(res);
    }

    @Test
    public void getDomProfileValueOKButValueNotNull() {
        DumeProfileProp dumeProfileProp = new DumeProfileProp();
        dumeProfileProp.setId(12L);
        dumeProfileProp.setDomain(true);
        Optional<DumeDomainPropValue> optionalDumeDomainPropValue = Optional.of(new DumeDomainPropValue());
        optionalDumeDomainPropValue.get().setValue("TEST");
        when(profileParamRepository.findByPropLabel(any())).thenReturn(dumeProfileProp);
        when(domPropValueRepository.findOne(domainPredicate(12L, anyInt()))).thenReturn(optionalDumeDomainPropValue);
        String res = profileParamService.getDomProfileValue("TEST", 12L);

        assertNotNull(res);
        assertEquals("TEST", res);
    }

    @Test
    public void getPlatProfileValueButValueNotNull() {
        DumeProfileProp dumeProfileProp = new DumeProfileProp();
        dumeProfileProp.setId(12L);
        dumeProfileProp.setPlatform(true);
        Optional<DumePlatPropValue> optionalDumeDomainPropValue = Optional.of(new DumePlatPropValue());
        optionalDumeDomainPropValue.get().setValue("TEST");
        when(profileParamRepository.findByPropLabel(any())).thenReturn(dumeProfileProp);
        when(platPropValueRepository.findOne(platformPredicate(12L, anyInt()))).thenReturn(optionalDumeDomainPropValue);
        String res = profileParamService.getPlatProfileValue("TEST", 12L);

        assertNotNull(res);
        assertEquals("TEST", res);
    }

    @Test
    public void getUsrProfileValueButValueNotNull() {
        DumeProfileProp dumeProfileProp = new DumeProfileProp();
        dumeProfileProp.setId(12L);
        dumeProfileProp.setUsr(true);
        Optional<DumeUsrPropValue> optionalDumeDomainPropValue = Optional.of(new DumeUsrPropValue());
        optionalDumeDomainPropValue.get().setValue("TEST");
        when(profileParamRepository.findByPropLabel(any())).thenReturn(dumeProfileProp);
        when(usrValueRepository.findOne(userPredicate(12L, anyInt()))).thenReturn(optionalDumeDomainPropValue);
        String res = profileParamService.getUsrProfileValue("TEST", 12L);

        assertNotNull(res);
        assertEquals("TEST", res);
    }

    @Test
    public void getValueButValueNotNull() {
        DumeProfileProp dumeProfileProp = new DumeProfileProp();
        dumeProfileProp.setId(12L);
        dumeProfileProp.setUsr(true);
        Optional<DumeUsrPropValue> optionalDumeDomainPropValue = Optional.of(new DumeUsrPropValue());
        optionalDumeDomainPropValue.get().setValue("TEST");
        when(profileParamRepository.findByPropLabel(any())).thenReturn(dumeProfileProp);
        when(usrValueRepository.findOne(userPredicate(12L, anyInt()))).thenReturn(optionalDumeDomainPropValue);
        String res = profileParamService.getValue("TEST", 12L, null, null);

        assertNotNull(res);
        assertEquals("TEST", res);
    }

    @Test(expected = IllegalStateException.class)
    public void createDumeProfilePropertyException() {
        DumeProfileProp dumeProfileProp = new DumeProfileProp();
        profileParamService.createDumeProfileProperty(dumeProfileProp);
    }

    @Test(expected = IllegalStateException.class)
    public void createDumeProfilePropertyExceptionSecond() {
        DumeProfileProp dumeProfileProp = new DumeProfileProp();
        dumeProfileProp.setDomain(true);
        profileParamService.createDumeProfileProperty(dumeProfileProp);
    }

    @Test
    public void createDumeProfilePropertyOk() {
        DumeProfileProp dumeProfileProp = new DumeProfileProp();
        dumeProfileProp.setDomain(true);
        dumeProfileProp.setDumeDomainPropValues(Arrays.asList(new DumeDomainPropValue()));
        when(profileParamRepository.save(any())).thenReturn(dumeProfileProp);

        DumeProfileProp res = profileParamService.createDumeProfileProperty(dumeProfileProp);

        assertNotNull(res);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getDomPropValueOrCreateByDumeProfilePropExection() {
        DumeDomain dumeDomain = new DumeDomain();

        when(profileParamRepository.findByPropLabel(any())).thenReturn(null);

        DumeDomainPropValue res = profileParamService.getDomPropValueOrCreateByDumeProfileProp("TEST", dumeDomain);
    }

    @Test
    public void getDomPropValueOrCreateByDumeProfilePropGETOK() {
        DumeDomain dumeDomain = new DumeDomain();
        dumeDomain.setId(12L);
        DumeProfileProp dumeProfileProp = new DumeProfileProp();
        dumeProfileProp.setId(12L);

        Optional<DumeDomainPropValue> optionalDumeDomainPropValue = Optional.of(new DumeDomainPropValue());

        when(profileParamRepository.findByPropLabel(any())).thenReturn(dumeProfileProp);

        when(domPropValueRepository.findOne(domainPredicate(12L, anyInt()))).thenReturn(optionalDumeDomainPropValue);
        DumeDomainPropValue res = profileParamService.getDomPropValueOrCreateByDumeProfileProp("TEST", dumeDomain);

        assertNotNull(res);
    }

    @Test
    public void getDomPropValueOrCreateByDumeProfilePropCREATEOK() {
        DumeDomain dumeDomain = new DumeDomain();
        dumeDomain.setId(12L);
        DumeProfileProp dumeProfileProp = new DumeProfileProp();
        dumeProfileProp.setId(12L);

        Optional<DumeDomainPropValue> optionalDumeDomainPropValue = Optional.empty();

        when(profileParamRepository.findByPropLabel(any())).thenReturn(dumeProfileProp);
        when(domPropValueRepository.save(any())).thenReturn(new DumeDomainPropValue());
        when(domPropValueRepository.findOne(domainPredicate(12L, anyInt()))).thenReturn(optionalDumeDomainPropValue);
        DumeDomainPropValue res = profileParamService.getDomPropValueOrCreateByDumeProfileProp("TEST", dumeDomain);

        assertNotNull(res);
    }


    @Test(expected = IllegalArgumentException.class)
    public void getPlatPropValueOrCreateByDumeProfilePropException() {
        DumePlatform dumePlatform = new DumePlatform();

        when(profileParamRepository.findByPropLabel(any())).thenReturn(null);

        DumePlatPropValue res = profileParamService.getPlatPropValueOrCreateByDumeProfileProp("TEST", dumePlatform);
    }

    @Test
    public void getPlatPropValueOrCreateByDumeProfilePropGETOK() {
        DumePlatform dumePlatform = new DumePlatform();
        dumePlatform.setId(12L);
        DumeProfileProp dumeProfileProp = new DumeProfileProp();
        dumeProfileProp.setId(12L);

        Optional<DumePlatPropValue> optionalDumeDomainPropValue = Optional.of(new DumePlatPropValue());

        when(profileParamRepository.findByPropLabel(any())).thenReturn(dumeProfileProp);

        when(platPropValueRepository.findOne(platformPredicate(12L, anyInt()))).thenReturn(optionalDumeDomainPropValue);
        DumePlatPropValue res = profileParamService.getPlatPropValueOrCreateByDumeProfileProp("TEST", dumePlatform);

        assertNotNull(res);
    }

    @Test
    public void getPlatPropValueOrCreateByDumeProfilePropCREATEOK() {
        DumePlatform dumePlatform = new DumePlatform();
        dumePlatform.setId(12L);
        DumeProfileProp dumeProfileProp = new DumeProfileProp();
        dumeProfileProp.setId(12L);

        Optional<DumePlatPropValue> optionalDumeDomainPropValue = Optional.empty();

        when(profileParamRepository.findByPropLabel(any())).thenReturn(dumeProfileProp);
        when(platPropValueRepository.save(any())).thenReturn(new DumePlatPropValue());
        when(platPropValueRepository.findOne(platformPredicate(12L, anyInt()))).thenReturn(optionalDumeDomainPropValue);
        DumePlatPropValue res = profileParamService.getPlatPropValueOrCreateByDumeProfileProp("TEST", dumePlatform);

        assertNotNull(res);
    }


}
