package com.atexo.dume.mapper.sn;

import com.atexo.dume.model.Lot;
import com.atexo.dume.model.dataset.DumeRequestContextDataSet;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SNRequestMapperTest {

    String longText = "7lD6ysMyvlPoiE4R3rXU7S7gs2S5LLCeUN1gPqUQsy75YsLmuqU7bEgSnC3uwOFyNBYUDwoYmnm31jVULdPHiPd0cPEYwnrptCHgdAo3VWVdDsHFxy428UhYbePtcgRVW5Pgafc4hfH3GFbF9C7UB5uqdxWEPzUDkB8YpfDCq760LkLYrlecGAoBfPwTwUdZH2ofP7Cd7lD6ysMyvlPoiE4R3rXU7S7gs2S5LLCeUN1gPqUQsy75YsLmuqU7bEgSnC3uwOFyNBYUDwoYmnm31jVULdPHiPd0cPEYwnrptCHgdAo3VWVdDsHFxy428UhYbePtcgRVW5Pgafc4hfH3GFbF9C7UB5uqdxWEPzUDkB8YpfDCq760LkLYrlecGAoBfPwTwUdZH2ofP7Cd";

    @InjectMocks
    SNRequestMapperImpl snRequestMapper;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void toLotMaxLength() {
        var lot = new Lot();
        lot.setLotName(longText);
        lot.setLotNumber("01");
        lot.setContext(DumeRequestContextDataSet.sampleContext());
        var snLot = snRequestMapper.toLot(lot);
        assertNotNull(snLot);
        assertEquals("libellé du lot ne dépassant pas 255 caractères", snLot.getLibelle().length(), 255);
    }
}
