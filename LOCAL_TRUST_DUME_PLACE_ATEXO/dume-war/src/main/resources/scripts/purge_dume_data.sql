create table if not exists platforms_to_keep (id bigint(20));
drop procedure if exists purge_dume_data;
DELIMITER //
create procedure purge_dume_data()
BEGIN
    if ((select count(*) from platforms_to_keep) = 0)
    then
        SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO=30001, MESSAGE_TEXT='aucune plateforme à garder';
    end if;
    select 'Génération du fichier de purge du FS';
    select '#!/bin/bash' union
    select concat('# script de suppression des fichiers du FS') union
    select concat('# script de suppression des fichiers du FS pour la PF : ',PLATFORM_IDENTIFIER) from dume_platform where ID not in (select id from platforms_to_keep) union
    select concat('rm -rf ',(select dume_prop_value from dume_properties where dume_prop_label = 'PDF_STORAGE_FOLDER' limit 1),'/',ID,'/ 2>> erreurs_suppression_fs.log')
    from request_context
    where PLATFORM_ID not in (select id from platforms_to_keep)
    into outfile '/tmp/purge_fs_dume.sh';

    select 'Purge de la table dume_response_lot';
    delete rl.* from dume_response_lot rl
                         join dume_response dr3 on dr3.ID = rl.ID_DUME_RESPONSE
                         join dume_document dd4 on dr3.ID_DUME_REQUEST = dd4.ID
                         join request_context rc4 on dd4.CONTEXT_ID = rc4.ID
    where rc4.PLATFORM_ID not in (select id from platforms_to_keep);

    select 'Purge de la table criteria_lot';
    delete rl.* from criteria_lot rl
                         join lot l on rl.ID_LOT=l.ID
                         join request_context rc4 on l.CONTEXT_ID = rc4.ID
    where rc4.PLATFORM_ID not in (select id from platforms_to_keep);

    select 'Purge de la table dume_response_lot';
    delete drl.* from dume_response_lot drl
                          join dume_response dr on drl.ID_DUME_RESPONSE = dr.ID
                          join dume_document dd on dr.ID_DUME_REQUEST = dd.ID
                          join request_context rc on dd.CONTEXT_ID = rc.ID
    where rc.PLATFORM_ID not in (select id from platforms_to_keep);
    delete drl.* from dume_response_lot drl
                          join lot l on drl.ID_LOT = l.ID
                          join request_context rc on l.CONTEXT_ID = rc.ID
    where rc.PLATFORM_ID not in (select id from platforms_to_keep);

    select 'Purge de la table lot';
    delete l.* from lot l
                        join request_context rc4 on l.CONTEXT_ID = rc4.ID
    where rc4.PLATFORM_ID not in (select id from platforms_to_keep);

    select 'Purge de la table dume_document_criteria_selection';
    delete ddcs.* from dume_document_criteria_selection ddcs
                           join dume_document d on ddcs.ID_DUME_DOCUMENT = d.ID
                           join request_context rc5 on d.CONTEXT_ID = rc5.ID
    where rc5.PLATFORM_ID not in (select id from platforms_to_keep);

    select 'Purge de la table dume_document_criteria_exclusion';
    delete ddce.* from dume_document_criteria_exclusion ddce
                           join dume_document d on ddce.ID_DUME_DOCUMENT = d.ID
                           join request_context rc5 on d.CONTEXT_ID = rc5.ID
    where rc5.PLATFORM_ID not in (select id from platforms_to_keep);


    select 'Purge de la table representant_legal';
    delete rl.* from representant_legal rl
                         join dume_response dr3 on dr3.ID = rl.ID_DUME_RESPONSE
                         join dume_document dd4 on dr3.ID_DUME_REQUEST = dd4.ID
                         join request_context rc4 on dd4.CONTEXT_ID = rc4.ID
    where rc4.PLATFORM_ID not in (select id from platforms_to_keep);

    select 'Purge de la table requirement_response avec requirement group response orhelins';
    delete rs.* from requirement_response rs
                         join requirement_group_response rgr on rs.ID_REQUIREMENT_GROUP = rgr.ID
    where rgr.ID_CRITERIA_RESPONSE is null;

    #  select 'Purge de la table requirement_response sans id_criteria_response';
    # delete from requirement_group_response where id in (select ID from requirement_group_response rs where ID_CRITERIA_RESPONSE is null order by ID desc,ID_PARENT asc);

    select 'Purge de la table requirement_response';
    delete rs.* from requirement_response rs
                         join requirement_group_response rgr on rs.ID_REQUIREMENT_GROUP = rgr.ID
                         join criteria_response c on rgr.ID_CRITERIA_RESPONSE = c.ID
                         join dume_response r on c.ID_DUME_RESPONSE = r.ID
                         join dume_document dd2 on r.ID_DUME_REQUEST = dd2.ID
                         join request_context rc2 on dd2.CONTEXT_ID = rc2.ID
    where rc2.PLATFORM_ID not in (select id from platforms_to_keep);

    alter table requirement_group_response add column if not exists old_parent_id bigint(20);
    update requirement_group_response set old_parent_id = ID_PARENT where ID_CRITERIA_RESPONSE is null;
    update requirement_group_response set ID_PARENT = null where ID_CRITERIA_RESPONSE is null;

    select 'Purge de la table requirement_group_response';
    delete from requirement_group_response where ID in (
        WITH RECURSIVE tree ( id , id_parent, level ) AS
                           (

                               SELECT
                                   parent.id, parent.ID_PARENT, 0 as level
                               FROM requirement_group_response as parent
                                        join criteria_response c on parent.ID_CRITERIA_RESPONSE = c.ID
                                        join dume_response r on c.ID_DUME_RESPONSE = r.ID
                                        join dume_document dd2 on r.ID_DUME_REQUEST = dd2.ID
                                        join request_context rc2 on dd2.CONTEXT_ID = rc2.ID
                               WHERE parent.ID_PARENT IS NULL and rc2.PLATFORM_ID not in (select id from platforms_to_keep)

                               UNION ALL

                               SELECT
                                   child.id , child.ID_PARENT, t.level +1
                               FROM requirement_group_response AS child
                                        INNER JOIN tree AS t
                                                   ON t.id = child.ID_PARENT

                           )

        SELECT id
        FROM tree
        ORDER BY id desc, ID_PARENT asc
    )   ;

    select 'Purge de la table criteria_response';
    delete cr.* from criteria_response cr
                         join dume_response dr2 on cr.ID_DUME_RESPONSE = dr2.ID
                         join dume_document dd3 on dr2.ID_DUME_REQUEST = dd3.ID
                         join request_context rc3 on dd3.CONTEXT_ID = rc3.ID
    where rc3.PLATFORM_ID not in (select id from platforms_to_keep);

    select 'Purge de la table contrats';
    alter table contract add column if not exists OLD_ID_CONTRACT_ORIGIN bigint(20);
    update contract set OLD_ID_CONTRACT_ORIGIN = ID_CONTRACT_ORIGIN,ID_CONTRACT_ORIGIN=null where ID_PLATFORM not in (select id from platforms_to_keep);
    delete from contract where ID_PLATFORM not in (select id from platforms_to_keep);

    select 'Purge de la table criteria_response';
    delete cr.* from criteria_response cr
                         join dume_response dr on cr.ID_DUME_RESPONSE = dr.ID
                         join dume_document dd on dr.ID_DUME_REQUEST = dd.ID
                         join request_context rc on dd.CONTEXT_ID = rc.ID
    where rc.PLATFORM_ID not in (select id from platforms_to_keep);



    select 'Purge de la table dume_response';
    delete dr.* from dume_response dr
                         join dume_document dd on dr.ID_DUME_REQUEST = dd.ID
                         join request_context rc on dd.CONTEXT_ID = rc.ID
    where rc.PLATFORM_ID not in (select id from platforms_to_keep);

    alter table dume_document add column if not exists old_replaced_by_id bigint(20);
    select 'Purge de la table dume_document replaced by';
    update dume_document remplacant
                         join dume_document dd on dd.ID_DUME_REPLACED_BY = remplacant.ID
    join request_context rc on rc.ID = remplacant.CONTEXT_ID
    set dd.old_replaced_by_id= dd.ID_DUME_REPLACED_BY, dd.ID_DUME_REPLACED_BY = null
    where rc.PLATFORM_ID not in (select id from platforms_to_keep);

    select 'Purge de la table dume_document';
    delete dd.* from dume_document dd
                         join request_context rc on dd.CONTEXT_ID = rc.ID
    where rc.PLATFORM_ID not in (select id from platforms_to_keep);

    select 'Purge de la table request_context';
    delete rq.* from request_context rq
    where rq.PLATFORM_ID not in (select id from platforms_to_keep);

    select 'Purge de la table dume_platform';
    delete from dume_platform where ID not in (select id from platforms_to_keep);

    select 'n''oubliez pas d''executer le script de suppression de fichiers (/tmp/purge_fs_dume.sh) et de le supprimer apres';

END //

DELIMITER ;

-- call purge_dume_data();


