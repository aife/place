package com.atexo.dume.model.dataset;

import com.atexo.dume.model.DumePlatform;

public class DumePlatformDataSet {

    public final static Long ID_PLATFORM = 1l;
    public final static String PLATFORM_DESC = "platform desc";
    public final static String PLATFORM_LABEL = "platform label";
    public final static String PLATFORM_IDENTIFIER = "mpe-place";
    public final static String PLATFORM_SN_IDENTIFIER = "33592022900036";
    public final static String PLATFORM_SN_TECHNICAL_IDENTIFIER = "azerty";

    public static DumePlatform samplePlatform() {
        DumePlatform platform = new DumePlatform();
        platform.setId(ID_PLATFORM);
        platform.setPlatDesc(PLATFORM_DESC);
        platform.setPlatLabel(PLATFORM_LABEL);
        platform.setPlatformIdentifier(PLATFORM_IDENTIFIER);
        platform.setSnPlatformIdentifier(PLATFORM_SN_IDENTIFIER);
        platform.setSnPlatformTechnicalIdentifier(PLATFORM_SN_TECHNICAL_IDENTIFIER);
        return platform;
    }
}
