package com.atexo.dume.model.dataset;

import com.atexo.dume.model.builder.ContractBuilder;
import com.atexo.dume.model.donneesessentielles.Contract;
import com.atexo.dume.model.donneesessentielles.EtatContract;

public class ContractDataSet {

    public static Contract sampleWithEtatEnAttente() {
        return ContractBuilder.aContract()
                .withContractNumber("00012")
                .withEtatContract(EtatContract.EN_ATTENTE)
                .build();
    }

    public static Contract sampleWithEtatPublie() {
        return ContractBuilder.aContract()
                .withContractNumber("00012").withUuid("uuid1")
                .withEtatContract(EtatContract.PUBLIE)
                .withSnNumber("dume_01234567")
                .build();
    }

    public static Contract sampleWithEtatMiseAjour() {
        return ContractBuilder.aContract()
                .withContractNumber("00012").withUuid("uuid1")
                .withEtatContract(EtatContract.MISE_A_JOUR)
                .withSnNumber("dume_01234567")
                .build();
    }

    public static Contract sampleWithErreurSN() {
        return ContractBuilder.aContract()
                .withContractNumber("00012").withUuid("uuid1")
                .withEtatContract(EtatContract.ERREUR_SN)
                .build();
    }
}
