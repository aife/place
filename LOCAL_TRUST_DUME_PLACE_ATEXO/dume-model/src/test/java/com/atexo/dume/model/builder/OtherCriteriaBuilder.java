package com.atexo.dume.model.builder;

import com.atexo.dume.model.CriteriaCategorie;
import com.atexo.dume.model.LegislationReference;
import com.atexo.dume.model.OtherCriteria;
import com.atexo.dume.model.RequirementGroup;

import java.util.List;

public final class OtherCriteriaBuilder {
    private String uuid;
    private String shortName;
    private String description;
    private Long id;
    private int version;
    private LegislationReference legislationReference;
    private String code;
    private int order;
    private CriteriaCategorie criteriaCategorie;
    private List<RequirementGroup> requirementGroupList;

    private OtherCriteriaBuilder() {
    }

    public static OtherCriteriaBuilder anOtherCriteria() {
        return new OtherCriteriaBuilder();
    }

    public OtherCriteriaBuilder withUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public OtherCriteriaBuilder withShortName(String shortName) {
        this.shortName = shortName;
        return this;
    }

    public OtherCriteriaBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public OtherCriteriaBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public OtherCriteriaBuilder withVersion(int version) {
        this.version = version;
        return this;
    }

    public OtherCriteriaBuilder withLegislationReference(LegislationReference legislationReference) {
        this.legislationReference = legislationReference;
        return this;
    }

    public OtherCriteriaBuilder withCode(String code) {
        this.code = code;
        return this;
    }

    public OtherCriteriaBuilder withOrder(int order) {
        this.order = order;
        return this;
    }

    public OtherCriteriaBuilder withCriteriaCategorie(CriteriaCategorie criteriaCategorie) {
        this.criteriaCategorie = criteriaCategorie;
        return this;
    }

    public OtherCriteriaBuilder withRequirementGroupList(List<RequirementGroup> requirementGroupList) {
        this.requirementGroupList = requirementGroupList;
        return this;
    }

    public OtherCriteria build() {
        OtherCriteria otherCriteria = new OtherCriteria();
        otherCriteria.setUuid(uuid);
        otherCriteria.setShortName(shortName);
        otherCriteria.setDescription(description);
        otherCriteria.setId(id);
        otherCriteria.setVersion(version);
        otherCriteria.setLegislationReference(legislationReference);
        otherCriteria.setCode(code);
        otherCriteria.setOrder(order);
        otherCriteria.setCriteriaCategorie(criteriaCategorie);
        otherCriteria.setRequirementGroupList(requirementGroupList);
        return otherCriteria;
    }
}
