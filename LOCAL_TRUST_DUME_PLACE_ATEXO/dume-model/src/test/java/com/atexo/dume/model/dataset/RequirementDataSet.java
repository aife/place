package com.atexo.dume.model.dataset;

import com.atexo.dume.model.Requirement;
import com.atexo.dume.model.builder.RequirementBuilder;

public class RequirementDataSet {

    public static final Long SIMPLE_REQUIREMENT_IDENTIFIER_1 = 200L;


    public static Requirement sampleWithId() {
        return RequirementBuilder.aRequirement()
                                 .withId(SIMPLE_REQUIREMENT_IDENTIFIER_1)
                                 .build();
    }

    public static Requirement sampleWithoutId() {
        return RequirementBuilder.aRequirement()
                                 .build();
    }
}
