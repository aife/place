package com.atexo.dume.model.dataset;

import com.atexo.dume.model.OtherCriteria;
import com.atexo.dume.model.builder.OtherCriteriaBuilder;

public class OtherCriteriaDataSet {
    public static final Long OTHER_CRITERIA_IDENTIFIER_1 = 10000L;
    public static final Long OTHER_CRITERIA_IDENTIFIER_2 = 10001L;

    public static OtherCriteria sample1WithId() {
        return OtherCriteriaBuilder.anOtherCriteria()
                .withId(OTHER_CRITERIA_IDENTIFIER_1)
                .withCode("CodeExclusion1")
                .build();
    }

    public static OtherCriteria sample2WithId() {
        return OtherCriteriaBuilder.anOtherCriteria()
                .withId(OTHER_CRITERIA_IDENTIFIER_2)
                .withCode("CodeExclusion2")
                .build();
    }
}
