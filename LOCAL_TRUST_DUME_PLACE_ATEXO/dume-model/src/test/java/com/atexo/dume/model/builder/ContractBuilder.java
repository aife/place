package com.atexo.dume.model.builder;

import com.atexo.dume.model.DumePlatform;
import com.atexo.dume.model.donneesessentielles.Contract;
import com.atexo.dume.model.donneesessentielles.EtatContract;

public final class ContractBuilder {
    private String contractNumber;
    private String uuid;
    private String snNumber;
    private DumePlatform platform;
    private Long id;
    private int version;
    private EtatContract etatContract;

    private ContractBuilder() {
    }

    public static ContractBuilder aContract() {
        return new ContractBuilder();
    }

    public ContractBuilder withContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
        return this;
    }

    public ContractBuilder withUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public ContractBuilder withSnNumber(String snNumber) {
        this.snNumber = snNumber;
        return this;
    }

    public ContractBuilder withPlatform(DumePlatform platform) {
        this.platform = platform;
        return this;
    }

    public ContractBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public ContractBuilder withVersion(int version) {
        this.version = version;
        return this;
    }

    public ContractBuilder withEtatContract(EtatContract etatContract) {
        this.etatContract = etatContract;
        return this;
    }

    public Contract build() {
        Contract contract = new Contract();
        contract.setContractNumber(contractNumber);
        contract.setSnNumber(snNumber);
        contract.setPlatform(platform);
        contract.setId(id);
        contract.setVersion(version);
        contract.setEtatContract(etatContract);
        return contract;
    }
}
