package com.atexo.dume.model.builder;

import com.atexo.dume.model.Response;

import java.math.BigDecimal;
import java.time.LocalDate;

public final class ResponseBuilder {
    private Long id;
    private String description;
    private int version;
    private Boolean indicator;
    private String code;
    private BigDecimal amount;
    private LocalDate date;
    private BigDecimal percent;
    private BigDecimal quantity;
    private String evidenceUrl;


    private ResponseBuilder() {
    }

    public static ResponseBuilder aResponse() {
        return new ResponseBuilder();
    }


    public ResponseBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public ResponseBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public ResponseBuilder withVersion(int version) {
        this.version = version;
        return this;
    }

    public ResponseBuilder withIndicator(Boolean indicator) {
        this.indicator = indicator;
        return this;
    }

    public ResponseBuilder withCode(String code) {
        this.code = code;
        return this;
    }

    public ResponseBuilder withAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public ResponseBuilder withDate(LocalDate date) {
        this.date = date;
        return this;
    }

    public ResponseBuilder withPercent(BigDecimal percent) {
        this.percent = percent;
        return this;
    }

    public ResponseBuilder withQuantity(BigDecimal quantity) {
        this.quantity = quantity;
        return this;
    }

    public ResponseBuilder withEvidenceUrl(String evidenceUrl) {
        this.evidenceUrl = evidenceUrl;
        return this;
    }


    public Response build() {
        Response response = new Response();
        response.setId(id);
        response.setDescription(description);
        response.setVersion(version);
        response.setIndicator(indicator);
        response.setCode(code);
        response.setAmount(amount);
        response.setDate(date);
        response.setPercent(percent);
        response.setQuantity(quantity);
        response.setEvidenceUrl(evidenceUrl);
        return response;
    }
}
