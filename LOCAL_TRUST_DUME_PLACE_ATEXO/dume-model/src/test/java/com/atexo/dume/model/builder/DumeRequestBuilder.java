package com.atexo.dume.model.builder;

import com.atexo.dume.model.CriteriaSelectionDocument;
import com.atexo.dume.model.DumeRequest;
import com.atexo.dume.model.DumeRequestContext;
import com.atexo.dume.model.ExclusionCriteria;

import java.util.List;

public final class DumeRequestBuilder {
    private Long id;
    private int version;
    private List<CriteriaSelectionDocument> selectionCriteriaList;
    private List<ExclusionCriteria> exclusionCriteriaList;
    private DumeRequestContext context;
    private Boolean confirmed;

    private DumeRequestBuilder() {
    }

    public static DumeRequestBuilder aDumeRequest() {
        return new DumeRequestBuilder();
    }


    public DumeRequestBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public DumeRequestBuilder withVersion(int version) {
        this.version = version;
        return this;
    }

    public DumeRequestBuilder withContext(final DumeRequestContext context) {
        this.context = context;
        return this;
    }


    public DumeRequestBuilder withSelectionCriteriaList(List<CriteriaSelectionDocument> selectionCriteriaList) {
        this.selectionCriteriaList = selectionCriteriaList;
        return this;
    }

    public DumeRequestBuilder withExclusionCriteriaList(List<ExclusionCriteria> exclusionCriteriaList) {
        this.exclusionCriteriaList = exclusionCriteriaList;
        return this;
    }

    public DumeRequestBuilder isConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
        return this;
    }
    public DumeRequest build() {
        final DumeRequest dumeRequest = new DumeRequest();
        dumeRequest.setContext(context);
        dumeRequest.setSelectionCriteriaList(selectionCriteriaList);
        dumeRequest.setExclusionCriteriaList(exclusionCriteriaList);
        dumeRequest.setId(id);
        dumeRequest.setVersion(version);
        dumeRequest.setConfirmed(confirmed);
        return dumeRequest;
    }
}
