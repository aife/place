package com.atexo.dume.model.builder;

import com.atexo.dume.model.DumeDocument;
import com.atexo.dume.model.DumeRequestContext;
import com.atexo.dume.model.Lot;
import com.atexo.dume.model.MetaData;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by ahmedsalmi on 08/02/2018.
 */
public final class DumeRequestContextBuilder {
    private Long id;
    private int version;
    private String officialName;
    private String country;
    private String consultRef;
    private String procedureType;
    private String entitled;
    private String object;
    private MetaData metadata;
    private Boolean isDefault = false;
    private Set<DumeDocument> dumeDocuments;
    private Set<Lot> lots = new HashSet<>();

    private DumeRequestContextBuilder() {
    }

    public static DumeRequestContextBuilder aDumeRequestContext() {
        return new DumeRequestContextBuilder();
    }

    public DumeRequestContextBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public DumeRequestContextBuilder withVersion(int version) {
        this.version = version;
        return this;
    }

    public DumeRequestContextBuilder withOfficialName(String officialName) {
        this.officialName = officialName;
        return this;
    }

    public DumeRequestContextBuilder withCountry(String country) {
        this.country = country;
        return this;
    }

    public DumeRequestContextBuilder withConsultRef(String consultRef) {
        this.consultRef = consultRef;
        return this;
    }

    public DumeRequestContextBuilder withProcedureType(String procedureType) {
        this.procedureType = procedureType;
        return this;
    }

    public DumeRequestContextBuilder withEntitled(String entitled) {
        this.entitled = entitled;
        return this;
    }

    public DumeRequestContextBuilder withObject(String object) {
        this.object = object;
        return this;
    }

    public DumeRequestContextBuilder withMetadata(MetaData metadata) {
        this.metadata = metadata;
        return this;
    }

    public DumeRequestContextBuilder withDumeDocuments(Set<DumeDocument> dumeDocuments) {
        this.dumeDocuments = dumeDocuments;
        return this;
    }

    public DumeRequestContextBuilder withDefault(Boolean isDefault) {
        this.isDefault = isDefault;
        return this;
    }

    public DumeRequestContextBuilder withLot(Set<Lot> lotSet) {
        this.lots = lotSet;
        return this;
    }

    public DumeRequestContext build() {
        DumeRequestContext dumeRequestContext = new DumeRequestContext();
        dumeRequestContext.setId(id);
        dumeRequestContext.setVersion(version);
        dumeRequestContext.setOfficialName(officialName);
        dumeRequestContext.setCountry(country);
        dumeRequestContext.setConsultRef(consultRef);
        dumeRequestContext.setProcedureType(procedureType);
        dumeRequestContext.setEntitled(entitled);
        dumeRequestContext.setObject(object);
        dumeRequestContext.setDefault(isDefault);
        dumeRequestContext.setMetadata(metadata);
        dumeRequestContext.getLotSet().clear();
        dumeRequestContext.getLotSet().addAll(lots);
        dumeDocuments.stream().forEach(dumeDocument -> {
            dumeRequestContext.addDumeDocumentToContext(dumeDocument);
        });
        return dumeRequestContext;
    }

}
