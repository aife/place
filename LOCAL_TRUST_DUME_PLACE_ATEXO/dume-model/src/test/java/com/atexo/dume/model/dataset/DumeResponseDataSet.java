package com.atexo.dume.model.dataset;

import com.atexo.dume.model.DumeResponse;
import com.atexo.dume.model.EtatDume;
import com.atexo.dume.model.Response;
import com.atexo.dume.model.builder.DumeResponseBuilder;

import java.util.ArrayList;
import java.util.List;

public class DumeResponseDataSet {

    public static final Long SIMPLE_DUME_RESPONSE_IDENTIFIER_1 = 10000L;
    public static final Long SIMPLE_DUME_RESPONSE_IDENTIFIER_2 = 10001L;
    public static final String SN_NUMBER_1 = "SN_NUMBER_1";

    public static DumeResponse sampleWithId() {
        return DumeResponseBuilder.aDumeResponse()
                                  .withId(SIMPLE_DUME_RESPONSE_IDENTIFIER_1)
                                  .build();
    }

    public static DumeResponse sampleWithIdAndSNNumber() {
        return DumeResponseBuilder.aDumeResponse().withId(SIMPLE_DUME_RESPONSE_IDENTIFIER_1).withSNNumber(SN_NUMBER_1).build();
    }

    public static DumeResponse sampleWithSimpleStructure() {
        List<Response> responseList = new ArrayList<>();
        responseList.add(ResponseDataSet.sampleWithAssignedRequirement());
        return DumeResponseBuilder.aDumeResponse()
                                  .withId(SIMPLE_DUME_RESPONSE_IDENTIFIER_1)
                                  .withResponseList(responseList)
                                  .withDumeRequest(DumeRequestDataSet.sampleWithSimpleStructure())
                                  .build();


    }

    public static DumeResponse sample2WithId() {
        return DumeResponseBuilder.aDumeResponse()
                                  .withId(SIMPLE_DUME_RESPONSE_IDENTIFIER_2)
                                  .build();
    }

    public static DumeResponse sampleWithoutId() {
        return DumeResponseBuilder.aDumeResponse()
                                  .build();
    }

    public static DumeResponse sampleWithRequestAndContext() {
        return DumeResponseBuilder.aDumeResponse().withSNNumber(SN_NUMBER_1).withDumeRequest(DumeRequestDataSet.sampleWithSimpleStructureWithContext()).build();
    }

    public static DumeResponse sampleBrouillon() {
        return DumeResponseBuilder.aDumeResponse().withId(SIMPLE_DUME_RESPONSE_IDENTIFIER_1).withEtatDume(EtatDume.BROUILLON)
                .withDumeRequest(DumeRequestDataSet.sampleWithSimpleStructureWithContext()).build();
    }

    public static DumeResponse sampleAPublier() {
        return DumeResponseBuilder.aDumeResponse().withId(SIMPLE_DUME_RESPONSE_IDENTIFIER_1).withEtatDume(EtatDume.A_PUBLIER)
                .withDumeRequest(DumeRequestDataSet.sampleWithSimpleStructureWithContext()).build();
    }
}
