package com.atexo.dume.model.builder;

import com.atexo.dume.model.Requirement;
import com.atexo.dume.model.RequirementGroup;

public final class RequirementBuilder {
    private String uuid;
    private String description;
    private Long id;
    private String responseType;
    private int version;
    private RequirementGroup requirementGroup;

    private RequirementBuilder() {
    }

    public static RequirementBuilder aRequirement() {
        return new RequirementBuilder();
    }

    public RequirementBuilder withUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public RequirementBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public RequirementBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public RequirementBuilder withResponseType(String responseType) {
        this.responseType = responseType;
        return this;
    }

    public RequirementBuilder withVersion(int version) {
        this.version = version;
        return this;
    }

    public RequirementBuilder withRequirementGroup(RequirementGroup requirementGroup) {
        this.requirementGroup = requirementGroup;
        return this;
    }

    public Requirement build() {
        Requirement requirement = new Requirement();
        requirement.setUuid(uuid);
        requirement.setDescription(description);
        requirement.setId(id);
        requirement.setResponseType(responseType);
        requirement.setVersion(version);
        requirement.setRequirementGroup(requirementGroup);
        return requirement;
    }
}
