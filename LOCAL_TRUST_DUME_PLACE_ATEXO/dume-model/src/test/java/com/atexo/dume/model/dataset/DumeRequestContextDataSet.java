package com.atexo.dume.model.dataset;

import com.atexo.dume.model.DumeDocument;
import com.atexo.dume.model.DumeRequestContext;
import com.atexo.dume.model.Lot;
import com.atexo.dume.model.MetaData;
import com.atexo.dume.model.builder.DumeRequestContextBuilder;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by ahmedsalmi on 08/02/2018.
 */
public class DumeRequestContextDataSet {

    public static final String SIMPLE_OBJECT = "object";
    public static final String SIMPLE_COUNTRY = "Pays";
    public static final String SIMPLE_PROC_TYPE = "Type1";
    public static final String SIMPLE_OFF_NAME = "NomOfficiel";
    public static final String SIMPLE_ENTITLED = "intitule";
    public static final String SIMPLE_REF_CONS = "01245697";
    public static final Long SIMPLE_CONTX_ID = 65843546352L;
    public static final String DUMMY_SIMPLE_LOT_NUMBER = "646143654";
    public static final String DUMMY_SIMPLE_LOT_LABEL = "THIS_IS_ME";

    public static DumeRequestContext sampleContext() {
        Set<Lot> lots = new HashSet<>();
        lots.add(new Lot(DUMMY_SIMPLE_LOT_NUMBER, DUMMY_SIMPLE_LOT_LABEL, null));
        return DumeRequestContextBuilder.aDumeRequestContext().withObject(SIMPLE_OBJECT)
                .withCountry(SIMPLE_COUNTRY)
                .withId(SIMPLE_CONTX_ID)
                .withConsultRef(SIMPLE_REF_CONS)
                .withProcedureType(SIMPLE_PROC_TYPE)
                .withOfficialName(SIMPLE_OFF_NAME).withLot(lots)
                .withEntitled(SIMPLE_ENTITLED).withMetadata(new MetaData())
                .withDumeDocuments(new HashSet<>())
                .build();
    }

    public static DumeRequestContext sampleContextWithDocument() {
        List<DumeDocument> documents = Arrays.asList(new DumeDocument());
        return DumeRequestContextBuilder.aDumeRequestContext().withObject(SIMPLE_OBJECT).withCountry(SIMPLE_COUNTRY).withId(SIMPLE_CONTX_ID).withConsultRef(SIMPLE_REF_CONS).withProcedureType(SIMPLE_PROC_TYPE).withEntitled(SIMPLE_ENTITLED).withMetadata(new MetaData()).withDumeDocuments(new HashSet<>(documents)).build();
    }
}
