package com.atexo.dume.model.builder;

import com.atexo.dume.model.CriteriaCategorie;
import com.atexo.dume.model.LegislationReference;
import com.atexo.dume.model.RequirementGroup;
import com.atexo.dume.model.SelectionCriteria;

import java.util.List;

public final class SelectionCriteriaBuilder {
    private Long id;
    private int version;
    private String uuid;
    private String shortName;
    private String description;
    private LegislationReference legislationReference;
    private String code;
    private CriteriaCategorie criteriaCategorie;
    private List<RequirementGroup> requirementGroupList;

    private SelectionCriteriaBuilder() {
    }

    public static SelectionCriteriaBuilder aSelectionCriteria() {
        return new SelectionCriteriaBuilder();
    }

    public SelectionCriteriaBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public SelectionCriteriaBuilder withVersion(int version) {
        this.version = version;
        return this;
    }

    public SelectionCriteriaBuilder withUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public SelectionCriteriaBuilder withShortName(String shortName) {
        this.shortName = shortName;
        return this;
    }

    public SelectionCriteriaBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public SelectionCriteriaBuilder withLegislationReference(LegislationReference legislationReference) {
        this.legislationReference = legislationReference;
        return this;
    }

    public SelectionCriteriaBuilder withCode(String code) {
        this.code = code;
        return this;
    }

    public SelectionCriteriaBuilder withCriteriaCategorie(CriteriaCategorie criteriaCategorie) {
        this.criteriaCategorie = criteriaCategorie;
        return this;
    }

    public SelectionCriteriaBuilder withRequirementGroupList(List<RequirementGroup> requirementGroupList) {
        this.requirementGroupList = requirementGroupList;
        return this;
    }

    public SelectionCriteria build() {
        SelectionCriteria selectionCriteria = new SelectionCriteria();
        selectionCriteria.setId(id);
        selectionCriteria.setVersion(version);
        selectionCriteria.setUuid(uuid);
        selectionCriteria.setShortName(shortName);
        selectionCriteria.setDescription(description);
        selectionCriteria.setLegislationReference(legislationReference);
        selectionCriteria.setCode(code);
        selectionCriteria.setCriteriaCategorie(criteriaCategorie);
        selectionCriteria.setRequirementGroupList(requirementGroupList);
        return selectionCriteria;
    }
}
