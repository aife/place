package com.atexo.dume.model.dataset;

import com.atexo.dume.model.Response;
import com.atexo.dume.model.builder.ResponseBuilder;

public class ResponseDataSet {

    public static final Long SIMPLE_RESPONSE_IDENTIFIER_1 = 400L;


    public static Response sampleWithId() {
        return ResponseBuilder.aResponse()
                              .withId(SIMPLE_RESPONSE_IDENTIFIER_1)
                              .build();
    }

    public static Response sampleWithAssignedRequirement() {
        return ResponseBuilder.aResponse()
                              .withId(SIMPLE_RESPONSE_IDENTIFIER_1)
                              .build();
    }

    public static Response sampleWithoutId() {
        return ResponseBuilder.aResponse()
                              .build();
    }
}
