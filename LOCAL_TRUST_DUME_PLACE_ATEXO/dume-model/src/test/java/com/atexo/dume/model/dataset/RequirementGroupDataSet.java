package com.atexo.dume.model.dataset;

import com.atexo.dume.model.Requirement;
import com.atexo.dume.model.RequirementGroup;
import com.atexo.dume.model.builder.RequirementGroupBuilder;

import java.util.ArrayList;
import java.util.List;

public class RequirementGroupDataSet {

    public static final Long SIMPLE_REQUIREMENT_GROUP_IDENTIFIER_1 = 300L;


    public static RequirementGroup sampleWithId() {
        return RequirementGroupBuilder.aRequirementGroup()
                                      .withId(SIMPLE_REQUIREMENT_GROUP_IDENTIFIER_1)
                                      .build();
    }

    public static RequirementGroup sampleWithSimpleStructure() {
        List<Requirement> requirements = new ArrayList<>();
        requirements.add(RequirementDataSet.sampleWithId());
        return RequirementGroupBuilder.aRequirementGroup()
                                      .withId(SIMPLE_REQUIREMENT_GROUP_IDENTIFIER_1)
                                      .withRequirementList(requirements)
                                      .build();
    }

    public static RequirementGroup sampleWithoutId() {
        return RequirementGroupBuilder.aRequirementGroup()
                                      .build();
    }
}
