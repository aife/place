package com.atexo.dume.model.builder;

import com.atexo.dume.model.Criteria;
import com.atexo.dume.model.Requirement;
import com.atexo.dume.model.RequirementGroup;

import java.util.List;

public final class RequirementGroupBuilder {
    private String uuid;
    private Long id;
    private int version;
    private Criteria criteria;
    private List<Requirement> requirementList;
    private RequirementGroup parent;
    private List<RequirementGroup> subGroups;
    private Boolean fulfillmentIndicator;
    private Boolean unbounded;

    private RequirementGroupBuilder() {
    }

    public static RequirementGroupBuilder aRequirementGroup() {
        return new RequirementGroupBuilder();
    }

    public RequirementGroupBuilder withUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public RequirementGroupBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public RequirementGroupBuilder withVersion(int version) {
        this.version = version;
        return this;
    }

    public RequirementGroupBuilder withCriteria(Criteria criteria) {
        this.criteria = criteria;
        return this;
    }

    public RequirementGroupBuilder withRequirementList(List<Requirement> requirementList) {
        this.requirementList = requirementList;
        return this;
    }

    public RequirementGroupBuilder withParent(RequirementGroup parent) {
        this.parent = parent;
        return this;
    }

    public RequirementGroupBuilder withSubGroups(List<RequirementGroup> subGroups) {
        this.subGroups = subGroups;
        return this;
    }

    public RequirementGroupBuilder withFulfillmentIndicator(Boolean fulfillmentIndicator) {
        this.fulfillmentIndicator = fulfillmentIndicator;
        return this;
    }

    public RequirementGroupBuilder withUnbounded(Boolean unbounded) {
        this.unbounded = unbounded;
        return this;
    }

    public RequirementGroup build() {
        RequirementGroup requirementGroup = new RequirementGroup();
        requirementGroup.setUuid(uuid);
        requirementGroup.setId(id);
        requirementGroup.setVersion(version);
        requirementGroup.setCriteria(criteria);
        requirementGroup.setRequirementList(requirementList);
        requirementGroup.setParent(parent);
        requirementGroup.setSubGroups(subGroups);
        requirementGroup.setFulfillmentIndicator(fulfillmentIndicator);
        requirementGroup.setUnbounded(unbounded);
        return requirementGroup;
    }
}
