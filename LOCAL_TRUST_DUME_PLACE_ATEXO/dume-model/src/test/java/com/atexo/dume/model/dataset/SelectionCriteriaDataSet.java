package com.atexo.dume.model.dataset;

import com.atexo.dume.model.RequirementGroup;
import com.atexo.dume.model.SelectionCriteria;
import com.atexo.dume.model.builder.SelectionCriteriaBuilder;

import java.util.ArrayList;
import java.util.List;

public class SelectionCriteriaDataSet {

    public static final Long SELECTION_CRITERIA_IDENTIFIER_1 = 10000L;
    public static final Long SELECTION_CRITERIA_IDENTIFIER_2 = 10001L;


    public static SelectionCriteria sample1WithId() {
        return SelectionCriteriaBuilder.aSelectionCriteria()
                .withId(SELECTION_CRITERIA_IDENTIFIER_1)
                .withCode("CodeSelection1")
                .build();
    }

    public static SelectionCriteria sampleWithSimpleStructure() {
        List<RequirementGroup> requirementGroups = new ArrayList<>();
        requirementGroups.add(RequirementGroupDataSet.sampleWithSimpleStructure());
        return SelectionCriteriaBuilder.aSelectionCriteria()
                                       .withId(SELECTION_CRITERIA_IDENTIFIER_1)
                                       .withRequirementGroupList(requirementGroups)
                                       .build();
    }

    public static SelectionCriteria sample2WithId() {
        return SelectionCriteriaBuilder.aSelectionCriteria()
                .withId(SELECTION_CRITERIA_IDENTIFIER_2)
                .withCode("CodeSelection2")
                .build();
    }

}
