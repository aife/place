package com.atexo.dume.model.dataset;

import com.atexo.dume.model.CriteriaSelectionDocument;
import com.atexo.dume.model.DumeRequest;
import com.atexo.dume.model.Lot;
import com.atexo.dume.model.builder.DumeRequestBuilder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static java.util.Arrays.asList;

public class DumeRequestDataSet {

    public static final Long SIMPLE_USER_IDENTIFIER_1 = 10000L;
    public static final Long SIMPLE_USER_IDENTIFIER_2 = 10001L;

    public static DumeRequest sampleWithId() {
        return DumeRequestBuilder.aDumeRequest()
                .withId(SIMPLE_USER_IDENTIFIER_1)
                .build();
    }

    public static DumeRequest sampleWithSimpleStructure() {
        List<CriteriaSelectionDocument> selectionCriteriaList = new ArrayList<>();
        CriteriaSelectionDocument criteriaSelectionDocument = new CriteriaSelectionDocument();
        criteriaSelectionDocument.setCriteria(SelectionCriteriaDataSet.sampleWithSimpleStructure());
        criteriaSelectionDocument.setLotList(new HashSet<>());
        selectionCriteriaList.add(criteriaSelectionDocument);
        return DumeRequestBuilder.aDumeRequest()
                                 .withId(SIMPLE_USER_IDENTIFIER_1)
                                 .withSelectionCriteriaList(selectionCriteriaList)
                                 .build();
    }

    public static DumeRequest sampleWithSimpleStructureAndLot() {
        List<CriteriaSelectionDocument> selectionCriteriaList = new ArrayList<>();
        CriteriaSelectionDocument criteriaSelectionDocument = new CriteriaSelectionDocument();
        criteriaSelectionDocument.setCriteria(SelectionCriteriaDataSet.sampleWithSimpleStructure());
        criteriaSelectionDocument.setLotList(new HashSet<>());
        Lot lot = new Lot("1", "1", null);
        criteriaSelectionDocument.getLotList().add(lot);
        selectionCriteriaList.add(criteriaSelectionDocument);
        return DumeRequestBuilder.aDumeRequest()
                .withId(SIMPLE_USER_IDENTIFIER_1)
                .withContext(DumeRequestContextDataSet.sampleContext())
                .withSelectionCriteriaList(selectionCriteriaList).withExclusionCriteriaList(new ArrayList<>())
                .build();
    }

    public static DumeRequest sample2WithId() {
        return DumeRequestBuilder.aDumeRequest()
                .withId(SIMPLE_USER_IDENTIFIER_2)
                .build();
    }

    public static DumeRequest sampleWithoutId() {
        return DumeRequestBuilder.aDumeRequest()
                .build();
    }

    public static DumeRequest sampleWithSimpleStructureWithContext() {
        List<CriteriaSelectionDocument> selectionCriteriaList = new ArrayList<>();
        CriteriaSelectionDocument criteriaSelectionDocument = new CriteriaSelectionDocument();
        criteriaSelectionDocument.setCriteria(SelectionCriteriaDataSet.sampleWithSimpleStructure());
        criteriaSelectionDocument.setLotList(new HashSet<>());
        selectionCriteriaList.add(criteriaSelectionDocument);
        return DumeRequestBuilder.aDumeRequest()
                .withId(SIMPLE_USER_IDENTIFIER_2)
                .withSelectionCriteriaList(selectionCriteriaList)
                .withExclusionCriteriaList(asList(ExclusionCriteriaDataSet.sample1WithId()))
                .withContext(DumeRequestContextDataSet.sampleContext())
                .build();
    }
}
