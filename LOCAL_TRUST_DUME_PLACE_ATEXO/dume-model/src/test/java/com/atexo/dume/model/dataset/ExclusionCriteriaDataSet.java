package com.atexo.dume.model.dataset;

import com.atexo.dume.model.ExclusionCriteria;
import com.atexo.dume.model.builder.ExclusionCriteriaBuilder;

public class ExclusionCriteriaDataSet {

    public static final Long EXCLUSION_CRITERIA_IDENTIFIER_1 = 10000L;
    public static final Long EXCLUSION_CRITERIA_IDENTIFIER_2 = 10001L;

    public static ExclusionCriteria sample1WithId() {
        return ExclusionCriteriaBuilder.anExclusionCriteria()
                .withId(EXCLUSION_CRITERIA_IDENTIFIER_1)
                .withCode("CodeExclusion1")
                .build();
    }

    public static ExclusionCriteria sample2WithId() {
        return ExclusionCriteriaBuilder.anExclusionCriteria()
                .withId(EXCLUSION_CRITERIA_IDENTIFIER_2)
                .withCode("CodeExclusion2")
                .build();
    }
}
