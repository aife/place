package com.atexo.dume.model.builder;

import com.atexo.dume.model.CriteriaCategorie;
import com.atexo.dume.model.ExclusionCriteria;
import com.atexo.dume.model.LegislationReference;
import com.atexo.dume.model.RequirementGroup;

import java.util.List;

public final class ExclusionCriteriaBuilder {
    private Long id;
    private int version;
    private String uuid;
    private String shortName;
    private String description;
    private LegislationReference legislationReference;
    private String code;
    private CriteriaCategorie criteriaCategorie;
    private List<RequirementGroup> requirementGroupList;

    private ExclusionCriteriaBuilder() {
    }

    public static ExclusionCriteriaBuilder anExclusionCriteria() {
        return new ExclusionCriteriaBuilder();
    }

    public ExclusionCriteriaBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public ExclusionCriteriaBuilder withVersion(int version) {
        this.version = version;
        return this;
    }

    public ExclusionCriteriaBuilder withUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public ExclusionCriteriaBuilder withShortName(String shortName) {
        this.shortName = shortName;
        return this;
    }

    public ExclusionCriteriaBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public ExclusionCriteriaBuilder withLegislationReference(LegislationReference legislationReference) {
        this.legislationReference = legislationReference;
        return this;
    }

    public ExclusionCriteriaBuilder withCode(String code) {
        this.code = code;
        return this;
    }

    public ExclusionCriteriaBuilder withCriteriaCategorie(CriteriaCategorie criteriaCategorie) {
        this.criteriaCategorie = criteriaCategorie;
        return this;
    }

    public ExclusionCriteriaBuilder withRequirementGroupList(List<RequirementGroup> requirementGroupList) {
        this.requirementGroupList = requirementGroupList;
        return this;
    }

    public ExclusionCriteria build() {
        ExclusionCriteria exclusionCriteria = new ExclusionCriteria();
        exclusionCriteria.setId(id);
        exclusionCriteria.setVersion(version);
        exclusionCriteria.setUuid(uuid);
        exclusionCriteria.setShortName(shortName);
        exclusionCriteria.setDescription(description);
        exclusionCriteria.setLegislationReference(legislationReference);
        exclusionCriteria.setCode(code);
        exclusionCriteria.setCriteriaCategorie(criteriaCategorie);
        exclusionCriteria.setRequirementGroupList(requirementGroupList);
        return exclusionCriteria;
    }
}
