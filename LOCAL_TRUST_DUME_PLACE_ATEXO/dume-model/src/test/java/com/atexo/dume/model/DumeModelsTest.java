package com.atexo.dume.model;

import com.atexo.dume.toolbox.AbstractDumePOJOTest;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import org.springframework.util.CollectionUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * This generic class will tests All DTO objects.
 *
 * @author ama
 */
public class DumeModelsTest extends AbstractDumePOJOTest {

    private static Set<Class<?>> classes;
    private static List<Object> instances = new LinkedList<>();

    /**
     * Returns an instance to use to test the get and set methods.
     *
     * @return An instance to use to test the get and set methods.
     */
    @Override
    protected List<Object> getInstanceList() throws IllegalAccessException, InstantiationException {
        return instances;
    }

    /**
     * Returns an instance to use to test the get and set methods.
     *
     * @return An instance to use to test the get and set methods.
     */
    @Override
    protected void addInstance(final Object instance){
         instances.add(instance);
    }

    /**
     * Returns a list of all concerned class.
     *
     * @return A Set of all scanned class by the module.
     */
    @Override
    protected Set<Class<?>> getClasses() {
        if(CollectionUtils.isEmpty(classes)){
            classes = getAllClassForPackage();
        }
        return classes;
    }

    @Override
    protected Set<Class<?>> getAllClassForPackage() {
        final List<ClassLoader> classLoadersList = new LinkedList<>();
        classLoadersList.add(ClasspathHelper.contextClassLoader());
        classLoadersList.add(ClasspathHelper.staticClassLoader());
        final Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner())
                .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("com.atexo.dume.model"))
                        .exclude(FilterBuilder.prefix("com.atexo.dume.model.builder"))
                        .exclude(FilterBuilder.prefix("com.atexo.dume.model.dataset"))));
        return reflections.getSubTypesOf(Object.class);
    }
}
