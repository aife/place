package com.atexo.dume.model.builder;

import com.atexo.dume.model.DumeRequest;
import com.atexo.dume.model.DumeResponse;
import com.atexo.dume.model.EtatDume;
import com.atexo.dume.model.Response;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public final class DumeResponseBuilder {
    private Long id;
    private int version;
    private String snNumber;
    private List<Response> responseList;
    private DumeRequest dumeRequest;
    private EtatDume etatDume;

    private DumeResponseBuilder() {
    }

    public static DumeResponseBuilder aDumeResponse() {
        return new DumeResponseBuilder();
    }

    public DumeResponseBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public DumeResponseBuilder withVersion(int version) {
        this.version = version;
        return this;
    }

    public DumeResponseBuilder withResponseList(List<Response> responseList) {
        this.responseList = responseList;
        return this;
    }

    public DumeResponseBuilder withDumeRequest(DumeRequest dumeRequest) {
        this.dumeRequest = dumeRequest;
        return this;
    }

    public DumeResponse build() {
        DumeResponse dumeResponse = new DumeResponse();
        dumeResponse.setId(id);
        dumeResponse.setVersion(version);
        dumeResponse.setDumeRequest(dumeRequest);
        dumeResponse.setNumeroSN(snNumber);
        dumeResponse.setLotSet(new HashSet<>());
        dumeResponse.setCriteriaResponseList(new ArrayList<>());
        dumeResponse.setEtatDume(etatDume);
        return dumeResponse;
    }

    public DumeResponseBuilder withSNNumber(String snNumber) {
        this.snNumber = snNumber;
        return this;
    }

    public DumeResponseBuilder withEtatDume(EtatDume etatDume) {
        this.etatDume = etatDume;
        return this;
    }
}
