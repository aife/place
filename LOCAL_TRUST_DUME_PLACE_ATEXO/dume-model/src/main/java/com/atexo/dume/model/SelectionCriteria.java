package com.atexo.dume.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("SELECTION")
public class SelectionCriteria extends Criteria {
    @Override
    public String toString() {
        return "SelectionCriteria{" + super.toString() + "}";
    }
}
