package com.atexo.dume.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "CRITERIA_RESPONSE")
public class CriteriaResponse extends AbstractEntity {

    @ManyToOne
    @JoinColumn(name = "ID_DUME_RESPONSE")
    private DumeResponse dumeResponse;

    @ManyToOne
    @JoinColumn(name = "ID_CRITERIA")
    private Criteria criteria;

    @OneToMany(mappedBy = "criteriaResponse", cascade = CascadeType.ALL)
    private List<RequirementGroupResponse> requirementGroupResponseList;

    public DumeResponse getDumeResponse() {
        return dumeResponse;
    }

    public void setDumeResponse(DumeResponse dumeRequest) {
        this.dumeResponse = dumeRequest;
    }

    public Criteria getCriteria() {
        return criteria;
    }

    public void setCriteria(Criteria criteria) {
        this.criteria = criteria;
    }

    public List<RequirementGroupResponse> getRequirementGroupResponseList() {
        return requirementGroupResponseList;
    }

    public void setRequirementGroupResponseList(List<RequirementGroupResponse> requirementGroupResponseList) {
        this.requirementGroupResponseList = requirementGroupResponseList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CriteriaResponse)) {
            return false;
        }

        CriteriaResponse that = (CriteriaResponse) o;

        if (dumeResponse != null ? !dumeResponse.equals(that.dumeResponse) : that.dumeResponse != null) {
            return false;
        }
        if (criteria != null ? !criteria.equals(that.criteria) : that.criteria != null) {
            return false;
        }
        return requirementGroupResponseList != null ? requirementGroupResponseList.equals(that.requirementGroupResponseList) : that.requirementGroupResponseList == null;
    }

    @Override
    public int hashCode() {
        int result = dumeResponse != null ? dumeResponse.hashCode() : 0;
        result = 31 * result + (criteria != null ? criteria.hashCode() : 0);
        result = 31 * result + (requirementGroupResponseList != null ? requirementGroupResponseList.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CriteriaResponse{" +
                ", criteria=" + criteria +
                ", requirementGroupResponseList=" + requirementGroupResponseList +
                '}';
    }
}
