package com.atexo.dume.model;

/**
 * Représente le type des catégories de critères.
 */
public enum CategorieType {
    EXCLUSION,
    SELECTION,
    ALL_SELECTION,
    INFORMATION,
    FINALISATION
}
