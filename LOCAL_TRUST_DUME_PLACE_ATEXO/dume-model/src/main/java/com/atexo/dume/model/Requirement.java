package com.atexo.dume.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "REQUIREMENT")
public class Requirement extends AbstractEntity {

    @Column(name = "UUID")
    private String uuid;

    @Column(name = "DESCRIPTION")
    @Lob
    private String description;

    @Column(name = "RESPONSE_TYPE")
    private String responseType;

    @ManyToOne
    @JoinColumn(name = "ID_REQUIREMENT_GROUP")
    private RequirementGroup requirementGroup;


    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public RequirementGroup getRequirementGroup() {
        return requirementGroup;
    }

    public void setRequirementGroup(RequirementGroup requirementGroup) {
        this.requirementGroup = requirementGroup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Requirement)) {
            return false;
        }

        Requirement that = (Requirement) o;

        if (uuid != null ? !uuid.equals(that.uuid) : that.uuid != null) {
            return false;
        }
        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }

        if (responseType != null ? !responseType.equals(that.responseType) : that.responseType != null) {
            return false;
        }
        return requirementGroup != null ? requirementGroup.equals(that.requirementGroup) : that.requirementGroup == null;
    }

    @Override
    public int hashCode() {
        int result = uuid != null ? uuid.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (responseType != null ? responseType.hashCode() : 0);
        result = 31 * result + (requirementGroup != null ? requirementGroup.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Requirement{" +
                "uuid='" + uuid + '\'' +
                ", description='" + description + '\'' +
                ", responseType='" + responseType + '\'' +
                '}';
    }
}
