package com.atexo.dume.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "REQUIREMENT_GROUP_RESPONSE")
public class RequirementGroupResponse extends AbstractEntity {

    @ManyToOne
    @JoinColumn(name = "ID_REQUIREMENT_GROUP")
    private RequirementGroup requirementGroup;

    @ManyToOne
    @JoinColumn(name = "ID_PARENT")
    private RequirementGroupResponse parent;

    @ManyToOne
    @JoinColumn(name = "ID_CRITERIA_RESPONSE")
    private CriteriaResponse criteriaResponse;

    @OneToMany(mappedBy = "requirementGroupResponse", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<RequirementResponse> requirementResponseList;

    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<RequirementGroupResponse> subGroups;

    public RequirementGroup getRequirementGroup() {
        return requirementGroup;
    }

    public void setRequirementGroup(RequirementGroup requirementGroup) {
        this.requirementGroup = requirementGroup;
    }

    public RequirementGroupResponse getParent() {
        return parent;
    }

    public void setParent(RequirementGroupResponse parent) {
        this.parent = parent;
    }

    public CriteriaResponse getCriteriaResponse() {
        return criteriaResponse;
    }

    public void setCriteriaResponse(CriteriaResponse criteriaResponse) {
        this.criteriaResponse = criteriaResponse;
    }

    public List<RequirementResponse> getRequirementResponseList() {
        if (requirementResponseList == null) {
            requirementResponseList = new ArrayList<>();
        }
        return requirementResponseList;
    }

    public void setRequirementResponseList(List<RequirementResponse> requirementResponseList) {
        this.requirementResponseList = requirementResponseList;
    }

    public List<RequirementGroupResponse> getSubGroups() {
        return subGroups;
    }

    public void setSubGroups(List<RequirementGroupResponse> subGroups) {
        this.subGroups = subGroups;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequirementGroupResponse)) {
            return false;
        }

        RequirementGroupResponse that = (RequirementGroupResponse) o;

        if (requirementGroup != null ? !requirementGroup.equals(that.requirementGroup) : that.requirementGroup != null) {
            return false;
        }
        if (parent != null ? !parent.equals(that.parent) : that.parent != null) {
            return false;
        }
        if (criteriaResponse != null ? !criteriaResponse.equals(that.criteriaResponse) : that.criteriaResponse != null) {
            return false;
        }
        if (subGroups != null ? !subGroups.equals(that.subGroups) : that.subGroups != null) {
            return false;
        }
        return requirementResponseList != null ? requirementResponseList.equals(that.requirementResponseList) : that.requirementResponseList == null;
    }

    @Override
    public int hashCode() {
        int result = requirementGroup != null ? requirementGroup.hashCode() : 0;
        result = 31 * result + (parent != null ? parent.hashCode() : 0);
        result = 31 * result + (criteriaResponse != null ? criteriaResponse.hashCode() : 0);
        result = 31 * result + (requirementResponseList != null ? requirementResponseList.hashCode() : 0);
        result = 31 * result + (subGroups != null ? subGroups.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RequirementGroupResponse{" +
                "requirementGroup=" + requirementGroup +
                ", requirementResponseList=" + requirementResponseList +
                ", subGroups=" + subGroups +
                '}';
    }
}
