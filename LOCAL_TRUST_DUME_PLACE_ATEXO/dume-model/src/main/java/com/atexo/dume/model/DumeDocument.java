package com.atexo.dume.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance
@DiscriminatorColumn(name = "TYPE_DOCUMENT")
@Table(name = "DUME_DOCUMENT")
public class DumeDocument extends Publishable {


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTEXT_ID")
    private DumeRequestContext context;


    @OneToMany(mappedBy = "dumeDocument", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<CriteriaSelectionDocument> selectionCriteriaList;

    @ManyToMany
    @JoinTable(name = "DUME_DOCUMENT_CRITERIA_EXCLUSION",
            joinColumns = @JoinColumn(name = "ID_DUME_DOCUMENT", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "ID_CRITERIA", referencedColumnName = "ID"))
    private List<ExclusionCriteria> exclusionCriteriaList;

    @Column(name = "CONFIRMED")
    private Boolean confirmed = false;

    @Column(name = "ETAT")
    @Enumerated(EnumType.STRING)
    private EtatDume etatDume = EtatDume.BROUILLON;

    @Column(name = "NUMERO_SN")
    private String numDumeSN;

    @Column(name = "PDF_RETRIEVED")
    private Boolean pdfRetrieved = false;

    @Column(name = "XML_OE_RETRIEVED")
    private Boolean xmlOERetrieved = false;

    @ManyToOne
    @JoinColumn(name = "ID_DUME_REPLACED_BY")
    private DumeDocument replacedBy;

    @Column(name = "CREATION_DATE")
    private LocalDateTime dateCreation = LocalDateTime.now();


    public DumeDocument() {
        this.selectionCriteriaList = new ArrayList<>();
        this.exclusionCriteriaList = new ArrayList<>();
    }

    public DumeDocument(@NotNull DumeRequestContext context) {
        this.context = context;
    }

    public DumeDocument(List<CriteriaSelectionDocument> selectionCriteriaList, List<ExclusionCriteria> exclusionCriteriaList) {
        this.selectionCriteriaList = selectionCriteriaList;
        this.exclusionCriteriaList = exclusionCriteriaList;
    }

    public DumeRequestContext getContext() {
        if (context == null && replacedBy != null) {
            return replacedBy.getContext();
        }
        return context;
    }

    public DumeRequestContext getContextPure() {
        return context;
    }


    public void setContext(DumeRequestContext context) {
        this.context = context;
    }

    public List<CriteriaSelectionDocument> getSelectionCriteriaList() {
        return selectionCriteriaList;
    }

    public void setSelectionCriteriaList(List<CriteriaSelectionDocument> selectionCriteriaList) {
        this.selectionCriteriaList = selectionCriteriaList;
    }

    public List<ExclusionCriteria> getExclusionCriteriaList() {
        return exclusionCriteriaList;
    }

    public void setExclusionCriteriaList(List<ExclusionCriteria> exclusionCriteriaList) {
        this.exclusionCriteriaList = exclusionCriteriaList;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public EtatDume getEtatDume() {
        return etatDume;
    }

    public void setEtatDume(EtatDume etatDume) {
        this.etatDume = etatDume;
    }

    public String getNumDumeSN() {
        return numDumeSN;
    }

    public void setNumDumeSN(String numDumeSN) {
        this.numDumeSN = numDumeSN;
    }

    public Boolean getPdfRetrieved() {
        return pdfRetrieved;
    }

    public void setPdfRetrieved(Boolean pdfRetrieved) {
        this.pdfRetrieved = pdfRetrieved;
    }

    public DumeDocument getReplacedBy() {
        return replacedBy;
    }

    public void setReplacedBy(DumeDocument replacedBy) {
        this.replacedBy = replacedBy;
    }

    public Boolean getXmlOERetrieved() {
        return xmlOERetrieved;
    }

    public void setXmlOERetrieved(Boolean xmlOERetrieved) {
        this.xmlOERetrieved = xmlOERetrieved;
    }

    public LocalDateTime getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(LocalDateTime dateCreation) {
        this.dateCreation = dateCreation;
    }



    @Override
    public String toString() {
        return "DumeDocument{" +
                "context=" + context +
                ", selectionCriteriaList=" + selectionCriteriaList +
                ", exclusionCriteriaList=" + exclusionCriteriaList +
                ", confirmed=" + confirmed +
                ", etatDume=" + etatDume +
                ", numDumeSN='" + numDumeSN + '\'' +
                ", pdfRetrieved=" + pdfRetrieved +
                ", xmlOERetrieved=" + xmlOERetrieved +
                ", replacedBy=" + replacedBy +
                ", dateCreation=" + dateCreation +
                ", dateDemandePublication=" + getDateDemandePublication() +
                ", datePublication=" + getDatePublication() +
                '}';
    }
}
