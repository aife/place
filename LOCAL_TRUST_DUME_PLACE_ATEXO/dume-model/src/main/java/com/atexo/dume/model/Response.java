package com.atexo.dume.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "RESPONSE")
public class Response extends AbstractEntity {

    @Column(name = "DESCRIPTION")
    @Lob
    private String description;

    @Column(name = "INDICATOR")
    private Boolean indicator;

    @Column(name = "CODE")
    private String code;

    @Column(name = "AMOUNT")
    private BigDecimal amount;

    @Column(name = "DATE")
    private LocalDate date;

    @Column(name = "PERCENT")
    private BigDecimal percent;

    @Column(name = "QUANTITY")
    private BigDecimal quantity;

    @Column(name = "EVIDENCE_URL")
    private String evidenceUrl;



    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIndicator() {
        return indicator;
    }

    public void setIndicator(Boolean indicator) {
        this.indicator = indicator;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public BigDecimal getPercent() {
        return percent;
    }

    public void setPercent(BigDecimal percent) {
        this.percent = percent;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getEvidenceUrl() {
        return evidenceUrl;
    }

    public void setEvidenceUrl(String evidenceUrl) {
        this.evidenceUrl = evidenceUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Response)) {
            return false;
        }

        Response response = (Response) o;

        if (description != null ? !description.equals(response.description) : response.description != null) {
            return false;
        }
        if (indicator != null ? !indicator.equals(response.indicator) : response.indicator != null) {
            return false;
        }
        if (code != null ? !code.equals(response.code) : response.code != null) {
            return false;
        }
        if (amount != null ? !amount.equals(response.amount) : response.amount != null) {
            return false;
        }
        if (date != null ? !date.equals(response.date) : response.date != null) {
            return false;
        }
        if (percent != null ? !percent.equals(response.percent) : response.percent != null) {
            return false;
        }
        if (quantity != null ? !quantity.equals(response.quantity) : response.quantity != null) {
            return false;
        }
        return evidenceUrl != null ? evidenceUrl.equals(response.evidenceUrl) : response.evidenceUrl == null;
    }

    @Override
    public int hashCode() {
        int result = description != null ? description.hashCode() : 0;
        result = 31 * result + (indicator != null ? indicator.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (amount != null ? amount.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (percent != null ? percent.hashCode() : 0);
        result = 31 * result + (quantity != null ? quantity.hashCode() : 0);
        result = 31 * result + (evidenceUrl != null ? evidenceUrl.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Response{" +
                ", description='" + description + '\'' +
                ", indicator=" + indicator +
                ", code='" + code + '\'' +
                ", amount=" + amount +
                ", date=" + date +
                ", percent=" + percent +
                ", quantity=" + quantity +
                ", evidenceUrl='" + evidenceUrl + '\'' +
                '}';
    }
}
