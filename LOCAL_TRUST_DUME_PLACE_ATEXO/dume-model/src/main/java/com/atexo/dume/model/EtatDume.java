package com.atexo.dume.model;

/**
 * This Enum represent the different status of a DUME
 */
public enum EtatDume {
    BROUILLON,
    A_PUBLIER,
    PUBLIE,
    STANDARD,
    DLRO_MODIFIEE,
    A_REMPLACER,
    REMPLACE,
    SUPPRIME,
    ERREUR_SN,
    ABANDON
}
