package com.atexo.dume.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "DUME_DOCUMENT_CRITERIA_SELECTION")
public class CriteriaSelectionDocument implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ID_CRITERIA")
    private Criteria criteria;

    @ManyToOne
    @JoinColumn(name = "ID_DUME_DOCUMENT")
    private DumeDocument dumeDocument;

    /**
     * Pour des raisons de compatibilité avec les anciennes versions, l'absence de lot associé à un critère équivaut
     * à ce critère associé à l'ensemble des lots
     */
    @ManyToMany
    @JoinTable(name = "CRITERIA_LOT",
            joinColumns = @JoinColumn(name = "ID_DUME_DOCUMENT_CRITERIA", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "ID_LOT", referencedColumnName = "ID"))
    private Set<Lot> lotList;

    public CriteriaSelectionDocument() {
        //Default Constructor
    }

    public CriteriaSelectionDocument(Criteria criteria, DumeDocument dumeDocument) {
        this.criteria = criteria;
        this.dumeDocument = dumeDocument;
        this.lotList = new HashSet<>();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Criteria getCriteria() {
        return criteria;
    }

    public void setCriteria(Criteria criteria) {
        this.criteria = criteria;
    }

    public DumeDocument getDumeDocument() {
        return dumeDocument;
    }

    public void setDumeDocument(DumeDocument dumeDocument) {
        this.dumeDocument = dumeDocument;
    }

    public Set<Lot> getLotList() {
        return lotList;
    }

    public void setLotList(Set<Lot> lotList) {
        this.lotList = lotList;
    }
}
