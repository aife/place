package com.atexo.dume.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("MODEL")
public class DumeRequestModel extends DumeDocument {

    public DumeRequestModel() {
        // constructeur Par defaut
    }

}
