package com.atexo.dume.model.profile;

import com.atexo.dume.model.DumePlatform;
import com.atexo.dume.model.profile.pk.DumePlatPropValuePK;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;


/**
 * The persistent class for the DUME_PLAT_PROP_VALUE database table.
 */
@Entity
@Table(name = "DUME_PLAT_PROP_VALUE")
public class DumePlatPropValue implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private DumePlatPropValuePK id;

    @Column(name = "VALUE", length = 500)
    private String value;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PLAT", nullable = false, insertable = false, updatable = false)
    private DumePlatform dumePlatform;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROP_ID", nullable = false, insertable = false, updatable = false)
    private DumeProfileProp dumeProfileProp;

    public DumePlatPropValue() {
        // Constructeur par défaut
    }

    public DumePlatPropValuePK getId() {
        return this.id;
    }

    public void setId(DumePlatPropValuePK id) {
        this.id = id;
    }


    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public DumePlatform getDumePlatform() {
        return this.dumePlatform;
    }

    public void setDumePlatform(DumePlatform dumePlatform) {
        this.dumePlatform = dumePlatform;
    }

    public DumeProfileProp getDumeProfileProp() {
        return this.dumeProfileProp;
    }

    public void setDumeProfileProp(DumeProfileProp dumeProfileProp) {
        this.dumeProfileProp = dumeProfileProp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DumePlatPropValue)) {
            return false;
        }

        final DumePlatPropValue that = (DumePlatPropValue) o;

        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) {
            return false;
        }
        if (getValue() != null ? !getValue().equals(that.getValue()) : that.getValue() != null) {
            return false;
        }
        if (getDumePlatform() != null ? !getDumePlatform().equals(that.getDumePlatform()) : that.getDumePlatform() != null) {
            return false;
        }
        return getDumeProfileProp() != null ? getDumeProfileProp().equals(that.getDumeProfileProp()) : that.getDumeProfileProp() == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getValue() != null ? getValue().hashCode() : 0);
        result = 31 * result + (getDumePlatform() != null ? getDumePlatform().hashCode() : 0);
        result = 31 * result + (getDumeProfileProp() != null ? getDumeProfileProp().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DumePlatPropValue{" +
                "id=" + id +
                ", value='" + value + '\'' +
                ", dumePlatform=" + dumePlatform +
                ", dumeProfileProp=" + dumeProfileProp +
                '}';
    }
}