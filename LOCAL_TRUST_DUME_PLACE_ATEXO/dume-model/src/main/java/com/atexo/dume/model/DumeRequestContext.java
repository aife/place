package com.atexo.dume.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Model Class from DumeRequest Context.
 */

@Entity
@Table(name = "REQUEST_CONTEXT")
@SuppressWarnings("unused")
public class DumeRequestContext extends AbstractEntity {


    @Column(name = "OFFICIAL_NAME")
    @Lob
    private String officialName;

    @NotNull
    @Column(name = "COUNTRY")
    private String country;

    @NotNull
    @Column(name = "CONSULT_REF")
    private String consultRef;

    @NotNull
    @Column(name = "PROCEDURE_TYPE")
    private String procedureType;

    @NotNull
    @Column(name = "ENTITLED")
    @Lob
    private String entitled;

    @NotNull
    @Column(name = "OBJECT")
    @Lob
    private String object;

    @NotNull
    @Embedded
    private MetaData metadata;

    @Column(name = "IS_DEFAULT")
    private Boolean isDefault;

    @OneToMany(mappedBy = "context", cascade = CascadeType.ALL)
    private Set<DumeDocument> dumeDocuments;


    @OneToMany(mappedBy = "context", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Lot> lotSet;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PLATFORM_ID")
    private DumePlatform platform;

    @Column(name = "IS_APPLY_MARKET_FILTER")
    private Boolean isApplyMarketFilter;


    public DumeRequestContext() {
        this.officialName = null;
        this.country = "";
        this.consultRef = "";
        this.procedureType = "";
        this.entitled = "";
        this.object = "";
        this.metadata = new MetaData();
        this.isDefault = false;
        this.dumeDocuments = new HashSet<>();
        this.lotSet = new HashSet<>();
        this.platform = new DumePlatform();
        this.isApplyMarketFilter = true;
    }

    public String getOfficialName() {
        return officialName;
    }

    public void setOfficialName(@NotNull final String officialName) {
        this.officialName = officialName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(@NotNull final String country) {
        this.country = country;
    }

    public String getConsultRef() {
        return consultRef;
    }

    public void setConsultRef(@NotNull final String consultRef) {
        this.consultRef = consultRef;
    }

    public String getProcedureType() {
        return procedureType;
    }

    public void setProcedureType(@NotNull final String procedureType) {
        this.procedureType = procedureType;
    }

    public String getEntitled() {
        return entitled;
    }

    public void setEntitled(@NotNull final String entitled) {
        this.entitled = entitled;
    }

    public String getObject() {
        return object;
    }

    public void setObject(@NotNull final String object) {
        this.object = object;
    }

    public MetaData getMetadata() {
        return metadata;
    }

    public void setMetadata(@NotNull final MetaData metadata) {
        this.metadata = metadata;
    }

    public Boolean getDefault() {
        return isDefault;
    }

    public void setDefault(Boolean aDefault) {
        isDefault = aDefault;
    }

    public Boolean getApplyMarketFilter() {
        return isApplyMarketFilter;
    }

    public void setApplyMarketFilter(Boolean applyMarketFilter) {
        isApplyMarketFilter = applyMarketFilter;
    }

    public Set<DumeDocument> getDumeDocuments() {
        return dumeDocuments;
    }


    public Set<Lot> getLotSet() {
        if (lotSet == null) {
            lotSet = new HashSet<>();
        }
        return lotSet;
    }


    public void setPlatform(DumePlatform platform) {
        this.platform = platform;
    }

    public DumePlatform getPlatform() {
        return platform;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DumeRequestContext)) {
            return false;
        }

        DumeRequestContext that = (DumeRequestContext) o;

        if (!Objects.equals(officialName, that.officialName)) {
            return false;
        }
        if (!Objects.equals(country, that.country)) {
            return false;
        }
        if (!Objects.equals(consultRef, that.consultRef)) {
            return false;
        }
        if (!Objects.equals(procedureType, that.procedureType)) {
            return false;
        }
        if (!Objects.equals(entitled, that.entitled)) {
            return false;
        }
        if (!Objects.equals(object, that.object)) {
            return false;
        }
        if (!Objects.equals(metadata, that.metadata)) {
            return false;
        }
        if (!Objects.equals(isDefault, that.isDefault)) {
            return false;
        }
        if (!Objects.equals(isApplyMarketFilter, that.isApplyMarketFilter)) {
            return false;
        }
        if (!Objects.equals(dumeDocuments, that.dumeDocuments)) {
            return false;
        }
        return Objects.equals(lotSet, that.lotSet);
    }

    @Override
    public int hashCode() {
        int result = officialName != null ? officialName.hashCode() : 0;
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (consultRef != null ? consultRef.hashCode() : 0);
        result = 31 * result + (procedureType != null ? procedureType.hashCode() : 0);
        result = 31 * result + (entitled != null ? entitled.hashCode() : 0);
        result = 31 * result + (object != null ? object.hashCode() : 0);
        result = 31 * result + (metadata != null ? metadata.hashCode() : 0);
        result = 31 * result + (isDefault != null ? isDefault.hashCode() : 0);
        result = 31 * result + (isApplyMarketFilter != null ? isApplyMarketFilter.hashCode() : 0);
        result = 31 * result + (dumeDocuments != null ? dumeDocuments.hashCode() : 0);
        result = 31 * result + (lotSet != null ? lotSet.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DumeRequestContext{" +
                "officialName='" + officialName + '\'' +
                ", country=" + country +
                ", consultRef='" + consultRef + '\'' +
                ", procedureType='" + procedureType + '\'' +
                ", entitled='" + entitled + '\'' +
                ", object='" + object + '\'' +
                ", isDefault='" + isDefault + '\'' +
                ", isApplyMarketFilter='" + isApplyMarketFilter + '\'' +
                '}';
    }

    public final void addDumeDocumentToContext(@NotNull final DumeDocument document) {
        dumeDocuments.add(document);
    }

    public final void addLotToContext(@NotNull final Lot lot) {
        lotSet.add(lot);
    }

    public final void initDumeDocuments() {
        dumeDocuments = new HashSet<>();
    }

}
