package com.atexo.dume.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "DUME_RESPONSE")
public class DumeResponse extends Publishable {


    @OneToMany(mappedBy = "dumeResponse", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<CriteriaResponse> criteriaResponseList;

    @ManyToOne
    @JoinColumn(name = "ID_DUME_REQUEST")
    private DumeRequest dumeRequest;

    @Column(name = "MICRO")
    private Boolean micro = false;

    @Embedded
    private EconomicPartyInfo economicPartyInfo;

    @OneToMany(mappedBy = "dumeResponse", cascade = CascadeType.ALL)
    private List<RepresentantLegal> representantLegals;

    @Column(name = "CONFIRMED")
    private Boolean confirmed = false;

    @Column(name = "ETAT")
    @Enumerated(EnumType.STRING)
    private EtatDume etatDume = EtatDume.BROUILLON;

    @Column(name = "NUMERO_SN")
    private String numeroSN;

    @Column(name = "PDF_RETRIEVED")
    private Boolean pdfRetreived = Boolean.FALSE;

    @Column(name = "SIRET_OE")
    private String siretOE;

    @Column(name = "TYPE_OE")
    private String typeOE;

    @Column(name = "GROUPEMENT_LABEL")
    private String groupementLabel;

    @Column(name = "SIRET_MANDATAIRE")
    private String siretMandataire;

    @ManyToMany
    @JoinTable(name = "DUME_RESPONSE_LOT",
            joinColumns = @JoinColumn(name = "ID_DUME_RESPONSE", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "ID_LOT", referencedColumnName = "ID"))
    private Set<Lot> lotSet;

    @Column(name = "CREATION_DATE")
    private LocalDateTime dateCreation = LocalDateTime.now();

    public DumeRequest getDumeRequest() {
        return dumeRequest;
    }

    public void setDumeRequest(DumeRequest dumeRequest) {
        this.dumeRequest = dumeRequest;
    }

    public Boolean getMicro() {
        return micro;
    }

    public void setMicro(Boolean micro) {
        this.micro = micro;
    }

    public EconomicPartyInfo getEconomicPartyInfo() {
        return economicPartyInfo;
    }

    public void setEconomicPartyInfo(EconomicPartyInfo economicPartyInfo) {
        this.economicPartyInfo = economicPartyInfo;
    }

    public List<RepresentantLegal> getRepresentantLegals() {
        return representantLegals;
    }

    public void setRepresentantLegals(List<RepresentantLegal> representantLegals) {
        this.representantLegals = representantLegals;
    }

    public List<CriteriaResponse> getCriteriaResponseList() {
        if(criteriaResponseList==null)
            criteriaResponseList=new ArrayList<>();
        return criteriaResponseList;
    }

    public void setCriteriaResponseList(List<CriteriaResponse> criteriaResponseList) {
        this.criteriaResponseList = criteriaResponseList;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public EtatDume getEtatDume() {
        return etatDume;
    }

    public void setEtatDume(EtatDume etatDume) {
        this.etatDume = etatDume;
    }

    public String getNumeroSN() {
        return numeroSN;
    }

    public void setNumeroSN(String numeroSN) {
        this.numeroSN = numeroSN;
    }

    public Boolean getPdfRetreived() {
        return pdfRetreived;
    }

    public void setPdfRetreived(Boolean pdfRecuperated) {
        this.pdfRetreived = pdfRecuperated;
    }

    public String getSiretOE() {
        return siretOE;
    }

    public void setSiretOE(String siretOE) {
        this.siretOE = siretOE;
    }

    public String getTypeOE() {
        return typeOE;
    }

    public void setTypeOE(String typeOE) {
        this.typeOE = typeOE;
    }

    public Set<Lot> getLotSet() {
        if (lotSet == null) {
            lotSet = new HashSet<>();
        }
        return lotSet;
    }

    public void setLotSet(Set<Lot> lotSet) {
        this.lotSet = lotSet;
    }

    public String getGroupementLabel() {
        return groupementLabel;
    }

    public void setGroupementLabel(String groupementLabel) {
        this.groupementLabel = groupementLabel;
    }

    public String getSiretMandataire() {
        return siretMandataire;
    }

    public void setSiretMandataire(String siretMandataire) {
        this.siretMandataire = siretMandataire;
    }

    @Transient
    public List<CriteriaResponse> getSelectionCriteria() {
        if (criteriaResponseList == null) {
            return new ArrayList<>();
        }
        return criteriaResponseList.stream()
                .filter(criteriaResponse -> criteriaResponse.getCriteria() instanceof SelectionCriteria)
                .collect(Collectors.toList());
    }

    @Transient
    public List<CriteriaResponse> getExclusionCriteria() {
        if (criteriaResponseList == null) {
            return new ArrayList<>();
        }
        return criteriaResponseList.stream()
                .filter(criteriaResponse -> criteriaResponse.getCriteria() instanceof ExclusionCriteria)
                .collect(Collectors.toList());
    }

    @Transient
    public List<CriteriaResponse> getOtherCriteria() {
        if (criteriaResponseList == null) {
            return new ArrayList<>();
        }
        return criteriaResponseList.stream()
                .filter(criteriaResponse -> criteriaResponse.getCriteria() instanceof OtherCriteria)
                .collect(Collectors.toList());
    }

    public LocalDateTime getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(LocalDateTime dateCreation) {
        this.dateCreation = dateCreation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DumeResponse)) {
            return false;
        }

        DumeResponse that = (DumeResponse) o;

        if (criteriaResponseList != null ? !criteriaResponseList.equals(that.criteriaResponseList) : that.criteriaResponseList != null) {
            return false;
        }
        if (dumeRequest != null ? !dumeRequest.equals(that.dumeRequest) : that.dumeRequest != null) {
            return false;
        }
        if (micro != null ? !micro.equals(that.micro) : that.micro != null) {
            return false;
        }
        if (economicPartyInfo != null ? !economicPartyInfo.equals(that.economicPartyInfo) : that.economicPartyInfo != null) {
            return false;
        }
        if (representantLegals != null ? !representantLegals.equals(that.representantLegals) : that.representantLegals != null) {
            return false;
        }
        if (confirmed != null ? !confirmed.equals(that.confirmed) : that.confirmed != null) {
            return false;
        }
        if (etatDume != that.etatDume) {
            return false;
        }
        if (numeroSN != null ? !numeroSN.equals(that.numeroSN) : that.numeroSN != null) {
            return false;
        }
        if (pdfRetreived != null ? !pdfRetreived.equals(that.pdfRetreived) : that.pdfRetreived != null) {
            return false;
        }
        if (siretOE != null ? !siretOE.equals(that.siretOE) : that.siretOE != null) {
            return false;
        }
        if (typeOE != null ? !typeOE.equals(that.typeOE) : that.typeOE != null) {
            return false;
        }
        if (groupementLabel != null ? !groupementLabel.equals(that.groupementLabel) : that.groupementLabel != null) {
            return false;
        }
        if (siretMandataire != null ? !siretMandataire.equals(that.siretMandataire) : that.siretMandataire != null) {
            return false;
        }
        return lotSet != null ? lotSet.equals(that.lotSet) : that.lotSet == null;
    }

    @Override
    public int hashCode() {
        int result = criteriaResponseList != null ? criteriaResponseList.hashCode() : 0;
        result = 31 * result + (dumeRequest != null ? dumeRequest.hashCode() : 0);
        result = 31 * result + (micro != null ? micro.hashCode() : 0);
        result = 31 * result + (economicPartyInfo != null ? economicPartyInfo.hashCode() : 0);
        result = 31 * result + (representantLegals != null ? representantLegals.hashCode() : 0);
        result = 31 * result + (confirmed != null ? confirmed.hashCode() : 0);
        result = 31 * result + (etatDume != null ? etatDume.hashCode() : 0);
        result = 31 * result + (numeroSN != null ? numeroSN.hashCode() : 0);
        result = 31 * result + (pdfRetreived != null ? pdfRetreived.hashCode() : 0);
        result = 31 * result + (siretOE != null ? siretOE.hashCode() : 0);
        result = 31 * result + (typeOE != null ? typeOE.hashCode() : 0);
        result = 31 * result + (groupementLabel != null ? groupementLabel.hashCode() : 0);
        result = 31 * result + (siretMandataire != null ? siretMandataire.hashCode() : 0);
        result = 31 * result + (lotSet != null ? lotSet.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DumeResponse{" +
                "criteriaResponseList=" + criteriaResponseList +
                ", dumeRequest=" + dumeRequest +
                ", micro=" + micro +
                ", economicPartyInfo=" + economicPartyInfo +
                ", representantLegals=" + representantLegals +
                ", confirmed=" + confirmed +
                ", etatDume=" + etatDume +
                ", numeroSN='" + numeroSN + '\'' +
                ", pdfRetreived=" + pdfRetreived +
                ", siretOE='" + siretOE + '\'' +
                ", typeOE='" + typeOE + '\'' +
                ", groupementLabel='" + groupementLabel + '\'' +
                ", siretMandataire='" + siretMandataire + '\'' +
                ", lotSet=" + lotSet +
                ", dateCreation=" + dateCreation +
                ", dateDemandePublication=" + getDateDemandePublication() +
                ", datePublication=" + getDatePublication() +
                '}';
    }
}
