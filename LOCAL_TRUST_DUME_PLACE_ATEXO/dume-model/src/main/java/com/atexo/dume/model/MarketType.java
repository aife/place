package com.atexo.dume.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Représente le type de marchés
 */
public enum MarketType {
    Travaux("01"),
    Fournitures("02"),
    Services("03");

    public static final List<MarketType> MARKET_LIST = Collections.unmodifiableList(Arrays.asList(values()));
    private final String code;

    MarketType(String code) {
        this.code = code;
    }

    public static List<MarketType> getMarketList() {
        return MARKET_LIST;
    }

    public String getCode() {
        return code;
    }
}
