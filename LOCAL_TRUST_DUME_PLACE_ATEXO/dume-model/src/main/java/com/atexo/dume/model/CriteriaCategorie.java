package com.atexo.dume.model;

import javax.persistence.*;

@Entity
@Table(name = "CRITERIA_CATEGORIE")
public class CriteriaCategorie extends AbstractEntity {

    @Lob
    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "CODE")
    private String code;

    @Lob
    @Column(name = "TITRE")
    private String titre;

    @Column(name = "CATEGORIE_TYPE")
    @Enumerated(EnumType.STRING)
    private CategorieType type;

    @Column(name = "ORDER_VALUE")
    private int order;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public CategorieType getType() {
        return type;
    }

    public void setType(CategorieType type) {
        this.type = type;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CriteriaCategorie)) {
            return false;
        }

        CriteriaCategorie that = (CriteriaCategorie) o;

        if (order != that.order) {
            return false;
        }
        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        if (code != null ? !code.equals(that.code) : that.code != null) {
            return false;
        }
        if (titre != null ? !titre.equals(that.titre) : that.titre != null) {
            return false;
        }
        return type == that.type;
    }

    @Override
    public int hashCode() {
        int result = description != null ? description.hashCode() : 0;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (titre != null ? titre.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + order;
        return result;
    }

    @Override
    public String toString() {
        return "CriteriaCategorie{" +
                "description='" + description + '\'' +
                ", code='" + code + '\'' +
                ", titre='" + titre + '\'' +
                ", type=" + type +
                ", order=" + order +
                "} " + super.toString();
    }
}
