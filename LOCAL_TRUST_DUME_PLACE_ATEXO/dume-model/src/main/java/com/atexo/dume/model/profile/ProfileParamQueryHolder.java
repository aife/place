package com.atexo.dume.model.profile;

import com.querydsl.core.types.Predicate;

public class ProfileParamQueryHolder {


    private static final QDumeDomainPropValue DOMAIN_PROP_VALUE = QDumeDomainPropValue.dumeDomainPropValue;
    private static final QDumePlatPropValue PLAT_PROP_VALUE = QDumePlatPropValue.dumePlatPropValue;
    private static final QDumeUsrPropValue USR_PROP_VALUE = QDumeUsrPropValue.dumeUsrPropValue;

    private ProfileParamQueryHolder() {
    }

    public static Predicate userPredicate(final Long propId, final Integer userId) {
        return USR_PROP_VALUE.id.propId.eq(propId.intValue()).and(USR_PROP_VALUE.id.usrId.eq(userId));
    }

    public static Predicate platformPredicate(final Long key, final Integer platformId) {
        return PLAT_PROP_VALUE.id.propId.eq(key.intValue()).and(PLAT_PROP_VALUE.id.idPlat.eq(platformId));
    }


    public static Predicate domainPredicate(final Long key, final Integer domainId) {
        return DOMAIN_PROP_VALUE.id.propId.eq(key.intValue()).and(DOMAIN_PROP_VALUE.id.idDom.eq(domainId));
    }


}
