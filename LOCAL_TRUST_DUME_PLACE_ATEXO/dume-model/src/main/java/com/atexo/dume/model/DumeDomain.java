package com.atexo.dume.model;

import com.atexo.dume.model.profile.DumeDomainPropValue;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;


/**
 * The persistent class for the DUME_DOMAIN database table.
 */
@Entity
@Table(name = "DUME_DOMAIN")
public class DumeDomain extends AbstractEntity {

    @Column(name = "DOM_DESC", nullable = false, length = 500)
    private String domDesc;

    @Column(name = "DOM_LABEL", nullable = false, length = 100)
    private String domLabel;

    @OneToMany(mappedBy = "dumeDomain")
    private List<DumeDomainPropValue> dumeDomainPropValues;

    public DumeDomain() {
        // Constructeur par défaut
    }


    public String getDomDesc() {
        return this.domDesc;
    }

    public void setDomDesc(String domDesc) {
        this.domDesc = domDesc;
    }


    public String getDomLabel() {
        return this.domLabel;
    }

    public void setDomLabel(String domLabel) {
        this.domLabel = domLabel;
    }


    //bi-directional many-to-one association to DumeDomainPropValue
    @OneToMany(mappedBy = "dumeDomain")
    public List<DumeDomainPropValue> getDumeDomainPropValues() {
        return this.dumeDomainPropValues;
    }

    public void setDumeDomainPropValues(List<DumeDomainPropValue> dumeDomainPropValues) {
        this.dumeDomainPropValues = dumeDomainPropValues;
    }

    public DumeDomainPropValue addDumeDomPropValue(DumeDomainPropValue dumeDomainPropValue) {
        getDumeDomainPropValues().add(dumeDomainPropValue);
        dumeDomainPropValue.setDumeDomain(this);

        return dumeDomainPropValue;
    }

    public DumeDomainPropValue removeDumeDomPropValue(DumeDomainPropValue dumeDomainPropValue) {
        getDumeDomainPropValues().remove(dumeDomainPropValue);
        dumeDomainPropValue.setDumeDomain(null);

        return dumeDomainPropValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DumeDomain)) {
            return false;
        }

        DumeDomain that = (DumeDomain) o;

        if (!getDomDesc().equals(that.getDomDesc())) {
            return false;
        }
        if (!getDomLabel().equals(that.getDomLabel())) {
            return false;
        }
        return getDumeDomainPropValues() != null ? getDumeDomainPropValues().equals(that.getDumeDomainPropValues()) : that.getDumeDomainPropValues() == null;
    }

    @Override
    public int hashCode() {
        int result = getDomDesc().hashCode();
        result = 31 * result + getDomLabel().hashCode();
        result = 31 * result + (getDumeDomainPropValues() != null ? getDumeDomainPropValues().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DumeDomain{" +
                "domDesc='" + domDesc + '\'' +
                ", domLabel='" + domLabel + '\'' +
                ", dumeDomainPropValues=" + dumeDomainPropValues +
                '}';
    }
}