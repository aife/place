package com.atexo.dume.model.profile.pk;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The primary key class for the DUME_USR_PROP_VALUE database table.
 */
@Embeddable
public class DumeUsrPropValuePK implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "USR_ID", unique = true, nullable = false)
    private int usrId;

    @Column(name = "PROP_ID", insertable = false, updatable = false, unique = true, nullable = false)
    private int propId;

    public DumeUsrPropValuePK() {
        // Constructeur par défaut
    }


    public int getUsrId() {
        return this.usrId;
    }

    public void setUsrId(int usrId) {
        this.usrId = usrId;
    }


    public int getPropId() {
        return this.propId;
    }

    public void setPropId(int propId) {
        this.propId = propId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DumeUsrPropValuePK)) {
            return false;
        }

        DumeUsrPropValuePK that = (DumeUsrPropValuePK) o;

        if (getUsrId() != that.getUsrId()) {
            return false;
        }
        return getPropId() == that.getPropId();
    }

    @Override
    public int hashCode() {
        int result = getUsrId();
        result = 31 * result + getPropId();
        return result;
    }

    @Override
    public String toString() {
        return "DumeUsrPropValuePK{" +
                "usrId=" + usrId +
                ", propId=" + propId +
                '}';
    }
}