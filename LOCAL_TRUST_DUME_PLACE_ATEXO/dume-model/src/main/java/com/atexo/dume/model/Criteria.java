package com.atexo.dume.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Inheritance
@DiscriminatorColumn(name = "TYPE_CRITERIA")
@Table(name = "CRITERIA")
public class Criteria extends AbstractEntity {

    @Column(name = "UUID")
    private String uuid;

    @Column(name = "SHORT_NAME")
    @Lob
    private String shortName;

    @Lob
    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToOne
    @JoinColumn(name = "ID_LEGISLATION_REFERENCE")
    private LegislationReference legislationReference;

    @Column(name = "CODE")
    private String code;

    @Column(name = "ORDER_VALUE")
    private int order;

    @Column(name = "ENABLE_E_CERTIS")
    private Boolean enableECertis;

    @ManyToOne
    @JoinColumn(name = "ID_CRITERIA_CATEGORIE")
    private CriteriaCategorie criteriaCategorie;

    @OneToMany(mappedBy = "criteria")
    private List<RequirementGroup> requirementGroupList;

    @Column(name = "ID_NATURE_MARKET")
    private String natureMarketIdentifier;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LegislationReference getLegislationReference() {
        return legislationReference;
    }

    public void setLegislationReference(LegislationReference legislationReference) {
        this.legislationReference = legislationReference;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public Boolean getEnableECertis() {
        return enableECertis;
    }

    public void setEnableECertis(Boolean enableECertis) {
        this.enableECertis = enableECertis;
    }

    public CriteriaCategorie getCriteriaCategorie() {
        return criteriaCategorie;
    }

    public void setCriteriaCategorie(CriteriaCategorie criteriaCategorie) {
        this.criteriaCategorie = criteriaCategorie;
    }

    public List<RequirementGroup> getRequirementGroupList() {
        return requirementGroupList;
    }

    public void setRequirementGroupList(List<RequirementGroup> requirementGroupList) {
        this.requirementGroupList = requirementGroupList;
    }

    public String getNatureMarketIdentifier() {
        return natureMarketIdentifier;
    }

    public void setNatureMarketIdentifier(String natureMarketIdentifier) {
        this.natureMarketIdentifier = natureMarketIdentifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Criteria)) {
            return false;
        }

        Criteria criteria = (Criteria) o;

        if (order != criteria.order) {
            return false;
        }

        if (enableECertis != null ? !enableECertis.equals(criteria.enableECertis) : criteria.enableECertis != null) {
            return false;
        }

        if (uuid != null ? !uuid.equals(criteria.uuid) : criteria.uuid != null) {
            return false;
        }
        if (shortName != null ? !shortName.equals(criteria.shortName) : criteria.shortName != null) {
            return false;
        }
        if (description != null ? !description.equals(criteria.description) : criteria.description != null) {
            return false;
        }
        if (legislationReference != null ? !legislationReference.equals(criteria.legislationReference) : criteria.legislationReference != null) {
            return false;
        }
        if (code != null ? !code.equals(criteria.code) : criteria.code != null) {
            return false;
        }
        if (criteriaCategorie != null ? !criteriaCategorie.equals(criteria.criteriaCategorie) : criteria.criteriaCategorie != null) {
            return false;
        }
        if (natureMarketIdentifier != null ? !natureMarketIdentifier.equals(criteria.natureMarketIdentifier) : criteria.natureMarketIdentifier != null) {
            return false;
        }
        return requirementGroupList != null ? requirementGroupList.equals(criteria.requirementGroupList) : criteria.requirementGroupList == null;
    }

    @Override
    public int hashCode() {
        int result = uuid != null ? uuid.hashCode() : 0;
        result = 31 * result + (shortName != null ? shortName.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (legislationReference != null ? legislationReference.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + order;
        result = 31 * result + (enableECertis != null ? enableECertis.hashCode() : 0);
        result = 31 * result + (criteriaCategorie != null ? criteriaCategorie.hashCode() : 0);
        result = 31 * result + (natureMarketIdentifier != null ? natureMarketIdentifier.hashCode() : 0);
        result = 31 * result + (requirementGroupList != null ? requirementGroupList.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Criteria{" +
                "uuid='" + uuid + '\'' +
                ", shortName='" + shortName + '\'' +
                ", description='" + description + '\'' +
                ", legislationReference=" + legislationReference +
                ", code='" + code + '\'' +
                ", order=" + order + ", enableECertis=" + enableECertis +
                ", criteriaCategorie=" + criteriaCategorie +
                ", requirementGroupList=" + requirementGroupList +
                '}';
    }
}
