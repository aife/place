package com.atexo.dume.model.profile;

import com.atexo.dume.model.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;


/**
 * The persistent class for the DUME_PROFILE_PROP database table.
 */
@Entity
@Table(name = "DUME_PROFILE_PROP")
public class DumeProfileProp extends AbstractEntity {

    @Column(name = "PROP_DESC", nullable = false, length = 500)
    private String propDesc;

    @Column(name = "PROP_LABEL", nullable = false, length = 100)
    private String propLabel;

    @Column(name = "IS_DOM")
    private Boolean isDomain;

    @Column(name = "IS_PLAT")
    private Boolean isPlatform;

    @Column(name = "IS_USR")
    private Boolean isUsr;

    @OneToMany(mappedBy = "dumeProfileProp")
    private List<DumeDomainPropValue> dumeDomainPropValues;

    @OneToMany(mappedBy = "dumeProfileProp")
    private List<DumePlatPropValue> dumePlatPropValues;

    @OneToMany(mappedBy = "dumeProfileProp")
    private List<DumeUsrPropValue> dumeUsrPropValues;

    public DumeProfileProp() {
        // Constructeur par défaut
    }


    public String getPropDesc() {
        return this.propDesc;
    }

    public void setPropDesc(String propDesc) {
        this.propDesc = propDesc;
    }


    public String getPropLabel() {
        return this.propLabel;
    }

    public void setPropLabel(String propLabel) {
        this.propLabel = propLabel;
    }

    public List<DumeDomainPropValue> getDumeDomainPropValues() {
        return this.dumeDomainPropValues;
    }

    public void setDumeDomainPropValues(List<DumeDomainPropValue> dumeDomainPropValues) {
        this.dumeDomainPropValues = dumeDomainPropValues;
    }

    public DumeDomainPropValue addDumeDomPropValue(DumeDomainPropValue dumeDomainPropValue) {
        getDumeDomainPropValues().add(dumeDomainPropValue);
        dumeDomainPropValue.setDumeProfileProp(this);

        return dumeDomainPropValue;
    }

    public DumeDomainPropValue removeDumeDomPropValue(DumeDomainPropValue dumeDomainPropValue) {
        getDumeDomainPropValues().remove(dumeDomainPropValue);
        dumeDomainPropValue.setDumeProfileProp(null);

        return dumeDomainPropValue;
    }

    public List<DumePlatPropValue> getDumePlatPropValues() {
        return this.dumePlatPropValues;
    }

    public void setDumePlatPropValues(List<DumePlatPropValue> dumePlatPropValues) {
        this.dumePlatPropValues = dumePlatPropValues;
    }

    public DumePlatPropValue addDumePlatPropValue(DumePlatPropValue dumePlatPropValue) {
        getDumePlatPropValues().add(dumePlatPropValue);
        dumePlatPropValue.setDumeProfileProp(this);

        return dumePlatPropValue;
    }

    public DumePlatPropValue removeDumePlatPropValue(DumePlatPropValue dumePlatPropValue) {
        getDumePlatPropValues().remove(dumePlatPropValue);
        dumePlatPropValue.setDumeProfileProp(null);

        return dumePlatPropValue;
    }

    public List<DumeUsrPropValue> getDumeUsrPropValues() {
        return this.dumeUsrPropValues;
    }

    public void setDumeUsrPropValues(List<DumeUsrPropValue> dumeUsrPropValues) {
        this.dumeUsrPropValues = dumeUsrPropValues;
    }

    public DumeUsrPropValue addDumeUsrPropValue(DumeUsrPropValue dumeUsrPropValue) {
        getDumeUsrPropValues().add(dumeUsrPropValue);
        dumeUsrPropValue.setDumeProfileProp(this);

        return dumeUsrPropValue;
    }

    public DumeUsrPropValue removeDumeUsrPropValue(DumeUsrPropValue dumeUsrPropValue) {
        getDumeUsrPropValues().remove(dumeUsrPropValue);
        dumeUsrPropValue.setDumeProfileProp(null);

        return dumeUsrPropValue;
    }

    public Boolean getDomain() {
        return isDomain;
    }

    public void setDomain(Boolean domain) {
        isDomain = domain;
    }

    public Boolean getPlatform() {
        return isPlatform;
    }

    public void setPlatform(Boolean platform) {
        isPlatform = platform;
    }

    public Boolean getUsr() {
        return isUsr;
    }

    public void setUsr(Boolean usr) {
        isUsr = usr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DumeProfileProp)) {
            return false;
        }

        final DumeProfileProp that = (DumeProfileProp) o;

        if (getPropDesc() != null ? !getPropDesc().equals(that.getPropDesc()) : that.getPropDesc() != null) {
            return false;
        }
        if (getPropLabel() != null ? !getPropLabel().equals(that.getPropLabel()) : that.getPropLabel() != null) {
            return false;
        }
        if (isDomain != null ? !isDomain.equals(that.isDomain) : that.isDomain != null) {
            return false;
        }
        if (isPlatform != null ? !isPlatform.equals(that.isPlatform) : that.isPlatform != null) {
            return false;
        }
        if (isUsr != null ? !isUsr.equals(that.isUsr) : that.isUsr != null) {
            return false;
        }
        if (getDumeDomainPropValues() != null ? !getDumeDomainPropValues().equals(that.getDumeDomainPropValues()) : that.getDumeDomainPropValues() != null) {
            return false;
        }
        if (getDumePlatPropValues() != null ? !getDumePlatPropValues().equals(that.getDumePlatPropValues()) : that.getDumePlatPropValues() != null) {
            return false;
        }
        return getDumeUsrPropValues() != null ? getDumeUsrPropValues().equals(that.getDumeUsrPropValues()) : that.getDumeUsrPropValues() == null;
    }

    @Override
    public int hashCode() {
        int result = getPropDesc() != null ? getPropDesc().hashCode() : 0;
        result = 31 * result + (getPropLabel() != null ? getPropLabel().hashCode() : 0);
        result = 31 * result + (isDomain != null ? isDomain.hashCode() : 0);
        result = 31 * result + (isPlatform != null ? isPlatform.hashCode() : 0);
        result = 31 * result + (isUsr != null ? isUsr.hashCode() : 0);
        result = 31 * result + (getDumeDomainPropValues() != null ? getDumeDomainPropValues().hashCode() : 0);
        result = 31 * result + (getDumePlatPropValues() != null ? getDumePlatPropValues().hashCode() : 0);
        result = 31 * result + (getDumeUsrPropValues() != null ? getDumeUsrPropValues().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DumeProfileProp{" +
                "propDesc='" + propDesc + '\'' +
                ", propLabel='" + propLabel + '\'' +
                ", isDomain=" + isDomain +
                ", isPlatform=" + isPlatform +
                ", isUsr=" + isUsr +
                ", dumeDomainPropValues=" + dumeDomainPropValues +
                ", dumePlatPropValues=" + dumePlatPropValues +
                ", dumeUsrPropValues=" + dumeUsrPropValues +
                '}';
    }
}