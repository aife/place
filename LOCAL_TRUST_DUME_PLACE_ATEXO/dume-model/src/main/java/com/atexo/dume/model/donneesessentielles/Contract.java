package com.atexo.dume.model.donneesessentielles;

import com.atexo.dume.model.DumePlatform;
import com.atexo.dume.model.Publishable;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "CONTRACT")
public class Contract extends Publishable {

    @Column(name = "CONTRACT_NUMBER")
    private String contractNumber;

    @Column(name = "SN_NUMBER")
    private String snNumber;

    @ManyToOne
    @JoinColumn(name = "ID_PLATFORM")
    private DumePlatform platform;

    @Column(name = "STATUT")
    @Enumerated(EnumType.STRING)
    private EtatContract etatContract;

    @Column(name = "UPDATE_TYPE")
    @Enumerated(EnumType.STRING)
    private UpdateType updateType;

    @ManyToOne
    @JoinColumn(name = "ID_CONTRACT_ORIGIN")
    private Contract contractOrigin;

    @Column(name = "CONSULTATION_ID")
    private String consutationId;

    @Column(name = "CREATION_DATE")
    private LocalDateTime dateCreation = LocalDateTime.now();

    @Column(name = "UUID")
    private String uuid;

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getSnNumber() {
        if (contractOrigin != null) {
            return contractOrigin.getSnNumber();
        }
        return snNumber;
    }

    public void setSnNumber(String snNumber) {
        this.snNumber = snNumber;
    }

    public DumePlatform getPlatform() {
        return platform;
    }

    public void setPlatform(DumePlatform platform) {
        this.platform = platform;
    }

    public EtatContract getEtatContract() {
        return etatContract;
    }

    public void setEtatContract(EtatContract etatContract) {
        this.etatContract = etatContract;
    }

    public UpdateType getUpdateType() {
        return updateType;
    }

    public void setUpdateType(UpdateType updateType) {
        this.updateType = updateType;
    }

    public Contract getContractOrigin() {
        return contractOrigin;
    }

    public void setContractOrigin(Contract contractOrigin) {
        this.contractOrigin = contractOrigin;
    }

    public String getConsutationId() {
        return consutationId;
    }

    public void setConsutationId(String consutationId) {
        this.consutationId = consutationId;
    }

    public LocalDateTime getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(LocalDateTime dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Contract)) {
            return false;
        }

        Contract contract = (Contract) o;

        if (contractNumber != null ? !contractNumber.equals(contract.contractNumber) : contract.contractNumber != null) {
            return false;
        }
        if (uuid != null ? !uuid.equals(contract.uuid) : contract.uuid != null) {
            return false;
        }
        if (snNumber != null ? !snNumber.equals(contract.snNumber) : contract.snNumber != null) {
            return false;
        }
        if (platform != null ? !platform.equals(contract.platform) : contract.platform != null) {
            return false;
        }
        return etatContract == contract.etatContract;
    }

    @Override
    public int hashCode() {
        int result = contractNumber != null ? contractNumber.hashCode() : 0;
        result = 31 * result + (uuid != null ? uuid.hashCode() : 0);
        result = 31 * result + (snNumber != null ? snNumber.hashCode() : 0);
        result = 31 * result + (platform != null ? platform.hashCode() : 0);
        result = 31 * result + (etatContract != null ? etatContract.hashCode() : 0);
        result = 31 * result + (updateType != null ? updateType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Contract{" +
                "contractNumber='" + contractNumber + '\'' + ", uuid='" + uuid + '\'' +
                ", snNumber='" + snNumber + '\'' +
                ", platform=" + platform +
                ", etatContract=" + etatContract + ", updateType=" + updateType +
                '}';
    }
}
