package com.atexo.dume.model.profile;

import com.atexo.dume.model.DumeDomain;
import com.atexo.dume.model.profile.pk.DumeDomPropValuePK;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;


/**
 * The persistent class for the DUME_DOM_PROP_VALUE database table.
 */
@Entity
@Table(name = "DUME_DOM_PROP_VALUE")
public class DumeDomainPropValue implements Serializable {

    @EmbeddedId
    private DumeDomPropValuePK id;

    @Column(name = "VALUE", length = 500)
    private String value;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_DOM", nullable = false, insertable = false, updatable = false)
    private DumeDomain dumeDomain;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROP_ID", nullable = false, insertable = false, updatable = false)
    private DumeProfileProp dumeProfileProp;

    public DumeDomainPropValue() {
        // Constructeur par défaut
    }


    public DumeDomPropValuePK getId() {
        return this.id;
    }

    public void setId(DumeDomPropValuePK id) {
        this.id = id;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public DumeDomain getDumeDomain() {
        return this.dumeDomain;
    }

    public void setDumeDomain(DumeDomain dumeDomain) {
        this.dumeDomain = dumeDomain;
    }

    public DumeProfileProp getDumeProfileProp() {
        return this.dumeProfileProp;
    }

    public void setDumeProfileProp(DumeProfileProp dumeProfileProp) {
        this.dumeProfileProp = dumeProfileProp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DumeDomainPropValue)) {
            return false;
        }

        DumeDomainPropValue that = (DumeDomainPropValue) o;

        if (!getId().equals(that.getId())) {
            return false;
        }
        if (getValue() != null ? !getValue().equals(that.getValue()) : that.getValue() != null) {
            return false;
        }
        if (!getDumeDomain().equals(that.getDumeDomain())) {
            return false;
        }
        return getDumeProfileProp().equals(that.getDumeProfileProp());
    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + (getValue() != null ? getValue().hashCode() : 0);
        result = 31 * result + getDumeDomain().hashCode();
        result = 31 * result + getDumeProfileProp().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "DumeDomainPropValue{" +
                "id=" + id +
                ", value='" + value + '\'' +
                ", dumeDomain=" + dumeDomain +
                ", dumeProfileProp=" + dumeProfileProp +
                '}';
    }
}