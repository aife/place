package com.atexo.dume.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Model Class for DUME CONTEXT  Lots.
 */

@Entity
@Table(name = "LOT")
@SuppressWarnings("unused")
public class Lot extends AbstractEntity {

    @NotNull
    @Column(name = "LOT_NUMBER")
    private String lotNumber;

    @NotNull
    @Column(name = "LOT_LABEL")
    @Lob
    private String lotName;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTEXT_ID")
    private DumeRequestContext context;

    public Lot() {
        this(null, null, null);
    }

    public Lot(String lotNumber, String lotName, DumeRequestContext context) {
        this.lotNumber = lotNumber;
        this.lotName = lotName;
        this.context = context;
    }

    public String getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }

    public String getLotName() {
        return lotName;
    }

    public void setLotName(String lotName) {
        this.lotName = lotName;
    }

    public DumeRequestContext getContext() {
        return context;
    }

    public void setContext(DumeRequestContext context) {
        this.context = context;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Lot)) {
            return false;
        }

        Lot lot = (Lot) o;

        if (lotNumber != null ? !lotNumber.equals(lot.lotNumber) : lot.lotNumber != null) {
            return false;
        }
        if (lotName != null ? !lotName.equals(lot.lotName) : lot.lotName != null) {
            return false;
        }
        return context != null ? context.equals(lot.context) : lot.context == null;
    }

    @Override
    public int hashCode() {
        int result = lotNumber != null ? lotNumber.hashCode() : 0;
        result = 31 * result + (lotName != null ? lotName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Lot{" +
                "lotNumber='" + lotNumber + '\'' +
                ", lotName='" + lotName + '\'' +
                ", context=" + context.getId() +
                '}';
    }
}
