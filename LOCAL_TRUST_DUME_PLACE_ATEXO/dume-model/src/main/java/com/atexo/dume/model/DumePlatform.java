package com.atexo.dume.model;

import com.atexo.dume.model.profile.DumePlatPropValue;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the DUME_PLATFORM database table.
 */
@Entity
@Table(name = "DUME_PLATFORM")
public class DumePlatform extends AbstractEntity implements Serializable {

    @Column(name = "PLAT_DESC", nullable = false, length = 500)
    private String platDesc;
    @Column(name = "PLAT_LABEL", nullable = false, length = 100)
    private String platLabel;

    // MPE
    @Column(name = "PLATFORM_IDENTIFIER")
    private String platformIdentifier;

    @Column(name = "SN_PLATFORM_IDENTIFIER")
    private String snPlatformIdentifier;

    @Column(name = "SN_PLATFORM_TECHNICAL_IDENTIFIER")
    private String snPlatformTechnicalIdentifier;

    @Column(name = "SN_OAUTH2_CLIENT_ID")
    private String snOauth2ClientId;

    @Column(name = "SN_OAUTH2_CLIENT_SECRET")
    private String snOauth2ClientSecret;

    @Column(name = "URL")
    private String url;

    @Column(name = "LOGIN")
    private String login;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "ENABLE_ESSENTIAL_DATA")
    private Boolean enableEssentialData;

    @OneToMany(mappedBy = "dumePlatform")
    private List<DumePlatPropValue> dumePlatPropValues;

    public DumePlatform() {
        // Constructeur par défaut
    }


    public String getPlatDesc() {
        return this.platDesc;
    }

    public void setPlatDesc(String platDesc) {
        this.platDesc = platDesc;
    }


    public String getPlatLabel() {
        return this.platLabel;
    }

    public void setPlatLabel(String platLabel) {
        this.platLabel = platLabel;
    }


    public List<DumePlatPropValue> getDumePlatPropValues() {
        return this.dumePlatPropValues;
    }

    public void setDumePlatPropValues(List<DumePlatPropValue> dumePlatPropValues) {
        this.dumePlatPropValues = dumePlatPropValues;
    }

    public String getPlatformIdentifier() {
        return platformIdentifier;
    }

    public void setPlatformIdentifier(String platformIdentifier) {
        this.platformIdentifier = platformIdentifier;
    }

    public String getSnPlatformIdentifier() {
        return snPlatformIdentifier;
    }

    public void setSnPlatformIdentifier(String snPlatformIdentifier) {
        this.snPlatformIdentifier = snPlatformIdentifier;
    }

    public String getSnOauth2ClientId() {
        return snOauth2ClientId;
    }

    public void setSnOauth2ClientId(String snOauth2ClientId) {
        this.snOauth2ClientId = snOauth2ClientId;
    }

    public String getSnOauth2ClientSecret() {
        return snOauth2ClientSecret;
    }

    public void setSnOauth2ClientSecret(String snOauth2ClientSecret) {
        this.snOauth2ClientSecret = snOauth2ClientSecret;
    }

    public String getSnPlatformTechnicalIdentifier() {
        return snPlatformTechnicalIdentifier;
    }

    public void setSnPlatformTechnicalIdentifier(String snPlatformTechnicalIdentifier) {
        this.snPlatformTechnicalIdentifier = snPlatformTechnicalIdentifier;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEnableEssentialData() {
        return enableEssentialData;
    }

    public void setEnableEssentialData(Boolean enableEssentialData) {
        this.enableEssentialData = enableEssentialData;
    }

    public DumePlatPropValue addDumePlatPropValue(DumePlatPropValue dumePlatPropValue) {
        getDumePlatPropValues().add(dumePlatPropValue);
        dumePlatPropValue.setDumePlatform(this);

        return dumePlatPropValue;
    }

    public DumePlatPropValue removeDumePlatPropValue(DumePlatPropValue dumePlatPropValue) {
        getDumePlatPropValues().remove(dumePlatPropValue);
        dumePlatPropValue.setDumePlatform(null);

        return dumePlatPropValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DumePlatform)) {
            return false;
        }

        DumePlatform that = (DumePlatform) o;

        if (!getPlatDesc().equals(that.getPlatDesc())) {
            return false;
        }
        if (!getPlatLabel().equals(that.getPlatLabel())) {
            return false;
        }
        return getDumePlatPropValues() != null ? getDumePlatPropValues().equals(that.getDumePlatPropValues()) : that.getDumePlatPropValues() == null;
    }

    @Override
    public int hashCode() {
        int result = getPlatDesc().hashCode();
        result = 31 * result + getPlatLabel().hashCode();
        result = 31 * result + (getDumePlatPropValues() != null ? getDumePlatPropValues().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DumePlatform{" + "platDesc='" + platDesc + '\'' + ", platLabel='" + platLabel + '\'' + ", platformIdentifier='" + platformIdentifier + '\'' + ", snPlatformIdentifier='" + snPlatformIdentifier + '\'' + ", snPlatformTechnicalIdentifier='" + snPlatformTechnicalIdentifier + '\'' + ", url='" + url + '\'' + ", login='" + login + '\'' + ", password='" + password + '\'' + ", enableEssentialData=" + enableEssentialData + '}';
    }
}
