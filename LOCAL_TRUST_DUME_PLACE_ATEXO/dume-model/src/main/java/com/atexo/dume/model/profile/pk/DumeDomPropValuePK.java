package com.atexo.dume.model.profile.pk;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The primary key class for the DUME_DOM_PROP_VALUE database table.
 */
@Embeddable
public class DumeDomPropValuePK implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "ID_DOM", insertable = false, updatable = false, unique = true, nullable = false)
    private int idDom;

    @Column(name = "PROP_ID", insertable = false, updatable = false, unique = true, nullable = false)
    private int propId;

    public DumeDomPropValuePK() {
        // Constructeur par défaut
    }

    public DumeDomPropValuePK(int idDom, int propId) {
        this.idDom = idDom;
        this.propId = propId;
    }

    public int getIdDom() {
        return this.idDom;
    }

    public void setIdDom(int idDom) {
        this.idDom = idDom;
    }


    public int getPropId() {
        return this.propId;
    }

    public void setPropId(int propId) {
        this.propId = propId;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof DumeDomPropValuePK)) {
            return false;
        }
        DumeDomPropValuePK castOther = (DumeDomPropValuePK) other;
        return
                (this.idDom == castOther.idDom)
                        && (this.propId == castOther.propId);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + this.idDom;
        hash = hash * prime + this.propId;

        return hash;
    }

    @Override
    public String toString() {
        return "DumeDomPropValuePK{" +
                "idDom=" + idDom +
                ", propId=" + propId +
                '}';
    }
}