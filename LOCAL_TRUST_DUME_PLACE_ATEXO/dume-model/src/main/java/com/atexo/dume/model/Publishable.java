package com.atexo.dume.model;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class Publishable extends AbstractEntity {
    @Column(name = "PUBLICATION_REQ_DATE")
    private LocalDateTime dateDemandePublication;

    @Column(name = "PUBLICATION_DATE")
    private LocalDateTime datePublication;

    @Column(name = "NB_PUBLICATION_TRIES")
    private int nbPublicationTries = 0;

    @Column(name = "COMMENT")
    @Lob
    private String comment;

    @Column(name = "PUBLICATION_ERRORS")
    @Lob
    private String errors;

    public LocalDateTime getDateDemandePublication() {
        return dateDemandePublication;
    }

    public void setDateDemandePublication(LocalDateTime dateDemandePublication) {
        this.dateDemandePublication = dateDemandePublication;
    }

    public LocalDateTime getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(LocalDateTime datePublication) {
        this.datePublication = datePublication;
    }

    public int getNbPublicationTries() {
        return nbPublicationTries;
    }

    public void setNbPublicationTries(int nbPublicationTries) {
        this.nbPublicationTries = nbPublicationTries;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }
}
