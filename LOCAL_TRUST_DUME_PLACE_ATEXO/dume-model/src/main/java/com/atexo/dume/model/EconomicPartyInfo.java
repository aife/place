package com.atexo.dume.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;

@Embeddable
public class EconomicPartyInfo implements Serializable {

    @Column(name = "NOM")
    private String nom;

    @Column(name = "RUE")
    private String rue;

    @Column(name = "CODE_POSTAL")
    private String codePostal;

    @Column(name = "VILLE")
    private String ville;

    @Column(name = "PAYS")
    @Enumerated(EnumType.STRING)
    private Country pays;

    @Column(name = "SITE_INTERNET")
    private String siteInternet;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "TELEPHONE")
    private String telephone;

    @Column(name = "CONTACT")
    private String contact;

    @Column(name = "NUM_TVA")
    private String numTVA;

    @Column(name = "NUM_TVA_ETRANGER")
    private String numEtrange;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public Country getPays() {
        return pays;
    }

    public void setPays(Country pays) {
        this.pays = pays;
    }

    public String getSiteInternet() {
        return siteInternet;
    }

    public void setSiteInternet(String siteInternet) {
        this.siteInternet = siteInternet;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getNumTVA() {
        return numTVA;
    }

    public void setNumTVA(String numTVA) {
        this.numTVA = numTVA;
    }

    public String getNumEtrange() {
        return numEtrange;
    }

    public void setNumEtrange(String numEtrange) {
        this.numEtrange = numEtrange;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EconomicPartyInfo)) {
            return false;
        }

        EconomicPartyInfo that = (EconomicPartyInfo) o;

        if (nom != null ? !nom.equals(that.nom) : that.nom != null) {
            return false;
        }
        if (rue != null ? !rue.equals(that.rue) : that.rue != null) {
            return false;
        }
        if (codePostal != null ? !codePostal.equals(that.codePostal) : that.codePostal != null) {
            return false;
        }
        if (ville != null ? !ville.equals(that.ville) : that.ville != null) {
            return false;
        }
        if (pays != null ? !pays.equals(that.pays) : that.pays != null) {
            return false;
        }
        if (siteInternet != null ? !siteInternet.equals(that.siteInternet) : that.siteInternet != null) {
            return false;
        }
        if (email != null ? !email.equals(that.email) : that.email != null) {
            return false;
        }
        if (telephone != null ? !telephone.equals(that.telephone) : that.telephone != null) {
            return false;
        }
        if (contact != null ? !contact.equals(that.contact) : that.contact != null) {
            return false;
        }
        if (numEtrange != null ? !numEtrange.equals(that.numEtrange) : that.numEtrange != null) {
            return false;
        }
        return numTVA != null ? numTVA.equals(that.numTVA) : that.numTVA == null;
    }

    @Override
    public int hashCode() {
        int result = nom != null ? nom.hashCode() : 0;
        result = 31 * result + (rue != null ? rue.hashCode() : 0);
        result = 31 * result + (codePostal != null ? codePostal.hashCode() : 0);
        result = 31 * result + (ville != null ? ville.hashCode() : 0);
        result = 31 * result + (pays != null ? pays.hashCode() : 0);
        result = 31 * result + (siteInternet != null ? siteInternet.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (telephone != null ? telephone.hashCode() : 0);
        result = 31 * result + (contact != null ? contact.hashCode() : 0);
        result = 31 * result + (numTVA != null ? numTVA.hashCode() : 0);
        result = 31 * result + (numEtrange != null ? numEtrange.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EconomicPartyInfo{" +
                "nom='" + nom + '\'' +
                ", rue='" + rue + '\'' +
                ", codePostal='" + codePostal + '\'' +
                ", ville='" + ville + '\'' +
                ", pays='" + pays + '\'' +
                ", siteInternet='" + siteInternet + '\'' +
                ", email='" + email + '\'' +
                ", telephone='" + telephone + '\'' +
                ", contact='" + contact + '\'' +
                ", numTVA='" + numTVA + '\'' +
                ", numEtrange='" + numEtrange + '\'' +
                '}';
    }
}
