package com.atexo.dume.model.donneesessentielles;

/**
 * This Enum represent the different status of a Contract {@link Contract}
 */
public enum EtatContract {
    EN_ATTENTE,
    PUBLIE,
    MISE_A_JOUR,
    ERREUR_SN
}
