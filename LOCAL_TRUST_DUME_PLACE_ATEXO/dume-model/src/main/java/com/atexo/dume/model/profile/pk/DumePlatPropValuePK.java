package com.atexo.dume.model.profile.pk;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The primary key class for the DUME_PLAT_PROP_VALUE database table.
 */
@Embeddable
public class DumePlatPropValuePK implements Serializable {


    private static final long serialVersionUID = 1L;

    @Column(name = "ID_PLAT", insertable = false, updatable = false, unique = true, nullable = false)
    private int idPlat;

    @Column(name = "PROP_ID", insertable = false, updatable = false, unique = true, nullable = false)
    private int propId;

    public DumePlatPropValuePK() {
        // Constructeur par défaut
    }

    public DumePlatPropValuePK(int idPlat, int propId) {
        this.idPlat = idPlat;
        this.propId = propId;
    }

    public int getIdPlat() {
        return this.idPlat;
    }

    public void setIdPlat(int idPlat) {
        this.idPlat = idPlat;
    }


    public int getPropId() {
        return this.propId;
    }

    public void setPropId(int propId) {
        this.propId = propId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DumePlatPropValuePK)) {
            return false;
        }

        final DumePlatPropValuePK that = (DumePlatPropValuePK) o;

        if (getIdPlat() != that.getIdPlat()) {
            return false;
        }
        return getPropId() == that.getPropId();
    }

    @Override
    public int hashCode() {
        int result = getIdPlat();
        result = 31 * result + getPropId();
        return result;
    }

    @Override
    public String toString() {
        return "DumePlatPropValuePK{" +
                "idPlat=" + idPlat +
                ", propId=" + propId +
                '}';
    }
}