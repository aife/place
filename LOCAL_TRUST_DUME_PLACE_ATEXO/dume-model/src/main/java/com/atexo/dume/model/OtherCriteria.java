package com.atexo.dume.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("OTHER")
public class OtherCriteria extends Criteria {
    @Override
    public String toString() {
        return "OtherCriteria{" + super.toString() + "}";
    }
}
