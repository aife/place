package com.atexo.dume.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "REPRESENTANT_LEGAL")
public class RepresentantLegal extends AbstractEntity {


    @Column(name = "PRENOM")
    private String prenom;

    @Column(name = "NOM")
    private String nom;

    @Column(name = "DATE_NAISSANCE")
    private LocalDate dateNaissance;

    @Column(name = "LIEU_NAISSANCE")
    private String lieuNaissance;

    @Column(name = "RUE")
    private String rue;

    @Column(name = "CODE_POSTAL")
    private String codePostal;

    @Column(name = "VILLE")
    private String ville;

    @Column(name = "PAYS")
    @Enumerated(EnumType.STRING)
    private Country pays;

    @Column(name = "FONCTION")
    private String fonction;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "TELEPHONE")
    private String telephone;

    @Column(name = "DETAILS")
    @Lob
    private String details;

    @ManyToOne
    @JoinColumn(name = "ID_DUME_RESPONSE")
    private DumeResponse dumeResponse;

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public Country getPays() {
        return pays;
    }

    public void setPays(Country pays) {
        this.pays = pays;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public DumeResponse getDumeResponse() {
        return dumeResponse;
    }

    public void setDumeResponse(DumeResponse dumeResponse) {
        this.dumeResponse = dumeResponse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RepresentantLegal)) {
            return false;
        }

        RepresentantLegal that = (RepresentantLegal) o;

        if (prenom != null ? !prenom.equals(that.prenom) : that.prenom != null) {
            return false;
        }
        if (nom != null ? !nom.equals(that.nom) : that.nom != null) {
            return false;
        }
        if (dateNaissance != null ? !dateNaissance.equals(that.dateNaissance) : that.dateNaissance != null) {
            return false;
        }
        if (lieuNaissance != null ? !lieuNaissance.equals(that.lieuNaissance) : that.lieuNaissance != null) {
            return false;
        }
        if (rue != null ? !rue.equals(that.rue) : that.rue != null) {
            return false;
        }
        if (codePostal != null ? !codePostal.equals(that.codePostal) : that.codePostal != null) {
            return false;
        }
        if (ville != null ? !ville.equals(that.ville) : that.ville != null) {
            return false;
        }
        if (pays != null ? !pays.equals(that.pays) : that.pays != null) {
            return false;
        }
        if (fonction != null ? !fonction.equals(that.fonction) : that.fonction != null) {
            return false;
        }
        if (email != null ? !email.equals(that.email) : that.email != null) {
            return false;
        }
        if (telephone != null ? !telephone.equals(that.telephone) : that.telephone != null) {
            return false;
        }
        if (dumeResponse != null ? !dumeResponse.equals(that.dumeResponse) : that.dumeResponse != null) {
            return false;
        }
        return details != null ? details.equals(that.details) : that.details == null;
    }

    @Override
    public int hashCode() {
        int result = prenom != null ? prenom.hashCode() : 0;
        result = 31 * result + (nom != null ? nom.hashCode() : 0);
        result = 31 * result + (dateNaissance != null ? dateNaissance.hashCode() : 0);
        result = 31 * result + (lieuNaissance != null ? lieuNaissance.hashCode() : 0);
        result = 31 * result + (rue != null ? rue.hashCode() : 0);
        result = 31 * result + (codePostal != null ? codePostal.hashCode() : 0);
        result = 31 * result + (ville != null ? ville.hashCode() : 0);
        result = 31 * result + (pays != null ? pays.hashCode() : 0);
        result = 31 * result + (fonction != null ? fonction.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (telephone != null ? telephone.hashCode() : 0);
        result = 31 * result + (details != null ? details.hashCode() : 0);
        result = 31 * result + (dumeResponse != null ? dumeResponse.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RepresentantLegal{" +
                "prenom='" + prenom + '\'' +
                ", nom='" + nom + '\'' +
                ", dateNaissance=" + dateNaissance +
                ", lieuNaissance=" + lieuNaissance +
                ", rue='" + rue + '\'' +
                ", codePostal='" + codePostal + '\'' +
                ", ville='" + ville + '\'' +
                ", pays='" + pays + '\'' +
                ", fonction='" + fonction + '\'' +
                ", email='" + email + '\'' +
                ", telephone='" + telephone + '\'' +
                ", details='" + details + '\'' +
                '}';
    }
}
