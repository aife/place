package com.atexo.dume.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("EXCLUSION")
public class ExclusionCriteria extends Criteria {
    @Override
    public String toString() {
        return "ExclusionCriteria{" + super.toString() + "}";
    }
}
