package com.atexo.dume.model.donneesessentielles;

/**
 * This Enum represent the different update types of a Contract {@link Contract}
 */
public enum UpdateType {
    INITIALISATION, MODIFICATION, MAJ_DONNEES_EXECUTION
}
