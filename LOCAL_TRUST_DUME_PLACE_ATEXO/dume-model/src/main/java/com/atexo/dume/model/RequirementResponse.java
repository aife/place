package com.atexo.dume.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "REQUIREMENT_RESPONSE")
public class RequirementResponse extends AbstractEntity {

    @ManyToOne
    @JoinColumn(name = "ID_REQUIREMENT")
    private Requirement requirement;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_RESPONSE")
    private Response response;

    @ManyToOne
    @JoinColumn(name = "ID_REQUIREMENT_GROUP")
    private RequirementGroupResponse requirementGroupResponse;

    public Requirement getRequirement() {
        return requirement;
    }

    public void setRequirement(Requirement requirement) {
        this.requirement = requirement;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public RequirementGroupResponse getRequirementGroupResponse() {
        return requirementGroupResponse;
    }

    public void setRequirementGroupResponse(RequirementGroupResponse requirementGroupResponse) {
        this.requirementGroupResponse = requirementGroupResponse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequirementResponse)) {
            return false;
        }

        RequirementResponse that = (RequirementResponse) o;

        if (requirement != null ? !requirement.equals(that.requirement) : that.requirement != null) {
            return false;
        }
        if (response != null ? !response.equals(that.response) : that.response != null) {
            return false;
        }
        return requirementGroupResponse != null ? requirementGroupResponse.equals(that.requirementGroupResponse) : that.requirementGroupResponse == null;
    }

    @Override
    public int hashCode() {
        int result = requirement != null ? requirement.hashCode() : 0;
        result = 31 * result + (response != null ? response.hashCode() : 0);
        result = 31 * result + (requirementGroupResponse != null ? requirementGroupResponse.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RequirementResponse{" +
                "requirement=" + requirement +
                ", response=" + response +
                '}';
    }
}
