package com.atexo.dume.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum Country {

    AF("AF", "Afghanistan"),
    ZA("ZA", "Afrique du Sud"),
    AX("AX", "Åland, Îles"),
    AL("AL", "Albanie"),
    DZ("DZ", "Algérie"),
    DE("DE", "Allemagne"),
    AD("AD", "Andorre"),
    AO("AO", "Angola"),
    AI("AI", "Anguilla"),
    AQ("AQ", "Antarctique"),
    AG("AG", "Antigua et Barbuda"),
    SA("SA", "Arabie Saoudite"),
    AR("AR", "Argentine"),
    AM("AM", "Arménie"),
    AW("AW", "Aruba"),
    AU("AU", "Australie"),
    AT("AT", "Autriche"),
    AZ("AZ", "Azerbaïdjan"),
    BS("BS", "Bahamas"),
    BH("BH", "Bahrein"),
    BD("BD", "Bangladesh"),
    BB("BB", "Barbade"),
    BY("BY", "Bélarus"),
    BE("BE", "Belgique"),
    BZ("BZ", "Bélize"),
    BJ("BJ", "Bénin"),
    BM("BM", "Bermudes"),
    BT("BT", "Bhoutan"),
    BO("BO", "Bolivie (État plurinational de)"),
    BQ("BQ", "Bonaire, Saint-Eustache et Saba"),
    BA("BA", "Bosnie-Herzégovine"),
    BW("BW", "Botswana"),
    BV("BV", "Bouvet, Ile"),
    BR("BR", "Brésil"),
    BN("BN", "Brunéi Darussalam"),
    BG("BG", "Bulgarie"),
    BF("BF", "Burkina Faso"),
    BI("BI", "Burundi"),
    CV("CV", "Cabo Verde"),
    KY("KY", "Caïmans, Iles"),
    KH("KH", "Cambodge"),
    CM("CM", "Cameroun"),
    CA("CA", "Canada"),
    CL("CL", "Chili"),
    CN("CN", "Chine"),
    CX("CX", "Christmas, île"),
    CY("CY", "Chypre"),
    CC("CC", "Cocos/Keeling (Îles)"),
    CO("CO", "Colombie"),
    KM("KM", "Comores"),
    CG("CG", "Congo"),
    CD("CD", "Congo, République démocratique du"),
    CK("CK", "Cook, Iles"),
    KR("KR", "Corée, République de"),
    KP("KP", "Corée, République populaire démocratique de"),
    CR("CR", "Costa Rica"),
    CI("CI", "Côte d'Ivoire"),
    HR("HR", "Croatie"),
    CU("CU", "Cuba"),
    CW("CW", "Curaçao"),
    DK("DK", "Danemark"),
    DJ("DJ", "Djibouti"),
    DO("DO", "Dominicaine, République"),
    DM("DM", "Dominique"),
    EG("EG", "Egypte"),
    SV("SV", "El Salvador"),
    AE("AE", "Emirats arabes unis"),
    EC("EC", "Equateur"),
    ER("ER", "Erythrée"),
    ES("ES", "Espagne"),
    EE("EE", "Estonie"),
    US("US", "Etats-Unis d'Amérique"),
    ET("ET", "Ethiopie"),
    FK("FK", "Falkland/Malouines (Îles)"),
    FO("FO", "Féroé, îles"),
    FJ("FJ", "Fidji"),
    FI("FI", "Finlande"),
    FR("FR", "France"),
    GA("GA", "Gabon"),
    GM("GM", "Gambie"),
    GE("GE", "Géorgie"),
    GS("GS", "Géorgie du sud et les îles Sandwich du sud"),
    GH("GH", "Ghana"),
    GI("GI", "Gibraltar"),
    GR("GR", "Grèce"),
    GD("GD", "Grenade"),
    GL("GL", "Groenland"),
    GP("GP", "Guadeloupe"),
    GU("GU", "Guam"),
    GT("GT", "Guatemala"),
    GG("GG", "Guernesey"),
    GN("GN", "Guinée"),
    GW("GW", "Guinée-Bissau"),
    GQ("GQ", "Guinée équatoriale"),
    GY("GY", "Guyana"),
    GF("GF", "Guyane française"),
    HT("HT", "Haïti"),
    HM("HM", "Heard, Ile et MacDonald, îles"),
    HN("HN", "Honduras"),
    HK("HK", "Hong Kong"),
    HU("HU", "Hongrie"),
    IM("IM", "Île de Man"),
    UM("UM", "Îles mineures éloignées des Etats-Unis"),
    VG("VG", "Îles vierges britanniques"),
    VI("VI", "Îles vierges des Etats-Unis"),
    IN("IN", "Inde"),
    IO("IO", "Indien (Territoire britannique de l'océan)"),
    ID("ID", "Indonésie"),
    IR("IR", "Iran, République islamique d'"),
    IQ("IQ", "Iraq"),
    IE("IE", "Irlande"),
    IS("IS", "Islande"),
    IL("IL", "Israël"),
    IT("IT", "Italie"),
    JM("JM", "Jamaïque"),
    JP("JP", "Japon"),
    JE("JE", "Jersey"),
    JO("JO", "Jordanie"),
    KZ("KZ", "Kazakhstan"),
    KE("KE", "Kenya"),
    KG("KG", "Kirghizistan"),
    KI("KI", "Kiribati"),
    KW("KW", "Koweït"),
    LA("LA", "Lao, République démocratique populaire"),
    LS("LS", "Lesotho"),
    LV("LV", "Lettonie"),
    LB("LB", "Liban"),
    LR("LR", "Libéria"),
    LY("LY", "Libye"),
    LI("LI", "Liechtenstein"),
    LT("LT", "Lituanie"),
    LU("LU", "Luxembourg"),
    MO("MO", "Macao"),
    MK("MK", "Macédoine, l'ex-République yougoslave de"),
    MG("MG", "Madagascar"),
    MY("MY", "Malaisie"),
    MW("MW", "Malawi"),
    MV("MV", "Maldives"),
    ML("ML", "Mali"),
    MT("MT", "Malte"),
    MP("MP", "Mariannes du nord, Iles"),
    MA("MA", "Maroc"),
    MH("MH", "Marshall, Iles"),
    MQ("MQ", "Martinique"),
    MU("MU", "Maurice"),
    MR("MR", "Mauritanie"),
    YT("YT", "Mayotte"),
    MX("MX", "Mexique"),
    FM("FM", "Micronésie, Etats Fédérés de"),
    MD("MD", "Moldova, République de"),
    MC("MC", "Monaco"),
    MN("MN", "Mongolie"),
    ME("ME", "Monténégro"),
    MS("MS", "Montserrat"),
    MZ("MZ", "Mozambique"),
    MM("MM", "Myanmar"),
    NA("NA", "Namibie"),
    NR("NR", "Nauru"),
    NP("NP", "Népal"),
    NI("NI", "Nicaragua"),
    NE("NE", "Niger"),
    NG("NG", "Nigéria"),
    NU("NU", "Niue"),
    NF("NF", "Norfolk, Ile"),
    NO("NO", "Norvège"),
    NC("NC", "Nouvelle-Calédonie"),
    NZ("NZ", "Nouvelle-Zélande"),
    OM("OM", "Oman"),
    UG("UG", "Ouganda"),
    UZ("UZ", "Ouzbékistan"),
    PK("PK", "Pakistan"),
    PW("PW", "Palaos"),
    PS("PS", "Palestine, Etat de"),
    PA("PA", "Panama"),
    PG("PG", "Papouasie-Nouvelle-Guinée"),
    PY("PY", "Paraguay"),
    NL("NL", "Pays-Bas"),
    XX("XX", "Pays inconnu"),
    ZZ("ZZ", "Pays multiples"),
    PE("PE", "Pérou"),
    PH("PH", "Philippines"),
    PN("PN", "Pitcairn"),
    PL("PL", "Pologne"),
    PF("PF", "Polynésie française"),
    PR("PR", "Porto Rico"),
    PT("PT", "Portugal"),
    QA("QA", "Qatar"),
    SY("SY", "République arabe syrienne"),
    CF("CF", "République centrafricaine"),
    RE("RE", "Réunion"),
    RO("RO", "Roumanie"),
    GB("GB", "Royaume-Uni de Grande-Bretagne et d'Irlande du Nord"),
    RU("RU", "Russie, Fédération de"),
    RW("RW", "Rwanda"),
    EH("EH", "Sahara occidental"),
    BL("BL", "Saint-Barthélemy"),
    KN("KN", "Saint-Kitts-et-Nevis"),
    SM("SM", "Saint-Marin"),
    MF("MF", "Saint-Martin (partie française)"),
    SX("SX", "Saint-Martin (partie néerlandaise)"),
    PM("PM", "Saint-Pierre-et-Miquelon"),
    VA("VA", "Saint-Siège"),
    VC("VC", "Saint-Vincent-et-les-Grenadines"),
    SH("SH", "Sainte-Hélène, Ascension et Tristan da Cunha"),
    LC("LC", "Sainte-Lucie"),
    SB("SB", "Salomon, Iles"),
    WS("WS", "Samoa"),
    AS("AS", "Samoa américaines"),
    ST("ST", "Sao Tomé-et-Principe"),
    SN("SN", "Sénégal"),
    RS("RS", "Serbie"),
    SC("SC", "Seychelles"),
    SL("SL", "Sierra Leone"),
    SG("SG", "Singapour"),
    SK("SK", "Slovaquie"),
    SI("SI", "Slovénie"),
    SO("SO", "Somalie"),
    SD("SD", "Soudan"),
    SS("SS", "Soudan du Sud"),
    LK("LK", "Sri Lanka"),
    SE("SE", "Suède"),
    CH("CH", "Suisse"),
    SR("SR", "Suriname"),
    SJ("SJ", "Svalbard et île Jan Mayen"),
    SZ("SZ", "Swaziland"),
    TJ("TJ", "Tadjikistan"),
    TW("TW", "Taïwan, Province de Chine"),
    TZ("TZ", "Tanzanie, République unie de"),
    TD("TD", "Tchad"),
    CZ("CZ", "Tchèque, République"),
    TF("TF", "Terres australes françaises"),
    TH("TH", "Thaïlande"),
    TL("TL", "Timor-Leste"),
    TG("TG", "Togo"),
    TK("TK", "Tokelau"),
    TO("TO", "Tonga"),
    TT("TT", "Trinité-et-Tobago"),
    TN("TN", "Tunisie"),
    TM("TM", "Turkménistan"),
    TC("TC", "Turks-et-Caïcos (Îles)"),
    TR("TR", "Turquie"),
    TV("TV", "Tuvalu"),
    UA("UA", "Ukraine"),
    UY("UY", "Uruguay"),
    VU("VU", "Vanuatu"),
    VE("VE", "Venezuela (République bolivarienne du)"),
    VN("VN", "Viet Nam"),
    WF("WF", "Wallis et Futuna"),
    YE("YE", "Yémen"),
    ZM("ZM", "Zambie"),
    ZW("ZW", "Zimbabwe");

    private final String countryName;

    private final String iso2Code;

    public static final List<Country> COUNTRY_LIST = Collections.unmodifiableList(Arrays.asList(values()));

    private static final Map<String, Country> BY_ISO2_CODE = new HashMap<>(COUNTRY_LIST.size());

    static {
        for (Country c : COUNTRY_LIST) {
            BY_ISO2_CODE.put(c.getIso2Code().toLowerCase(), c);
        }
    }

    Country(String iso2Code, String countryName) {
        this.countryName = countryName;
        this.iso2Code = iso2Code;
    }

    public String getName() {
        return countryName;
    }


    public static Country findByIso2Code(String iso2Code) {
        return BY_ISO2_CODE.get(trimToEmpty(iso2Code).toLowerCase());
    }

    public String getCountryName() {
        return countryName;
    }

    public String getIso2Code() {
        return iso2Code;
    }


    public static Map<String, Country> getByIso2Code() {
        return BY_ISO2_CODE;
    }

    private static String trimToEmpty(String str) {
        return str == null ? "" : str.trim();
    }

    public static List<Country> getCountryList() {
        return COUNTRY_LIST;
    }


}
