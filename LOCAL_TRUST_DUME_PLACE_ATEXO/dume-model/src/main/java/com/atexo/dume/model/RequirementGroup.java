package com.atexo.dume.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "REQUIREMENT_GROUP")
public class RequirementGroup extends AbstractEntity {

    @Column(name = "UUID")
    private String uuid;

    @ManyToOne
    @JoinColumn(name = "ID_CRITERIA", nullable = true)
    private Criteria criteria;

    @OneToMany(mappedBy = "requirementGroup")
    private List<Requirement> requirementList;
    @ManyToOne
    @JoinColumn(name = "ID_PARENT", nullable = true)
    private RequirementGroup parent;

    @OneToMany(mappedBy = "parent")
    private List<RequirementGroup> subGroups;

    @Column(name = "FULFILLMENT_INDICATOR")
    private Boolean fulfillmentIndicator;

    @Column(name = "UNBOUNDED")
    private Boolean unbounded;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Criteria getCriteria() {
        return criteria;
    }

    public void setCriteria(Criteria criteria) {
        this.criteria = criteria;
    }

    public List<Requirement> getRequirementList() {
        return requirementList;
    }

    public void setRequirementList(List<Requirement> requirementList) {
        this.requirementList = requirementList;
    }

    public RequirementGroup getParent() {
        return parent;
    }

    public void setParent(RequirementGroup parent) {
        this.parent = parent;
    }

    public List<RequirementGroup> getSubGroups() {
        return subGroups;
    }

    public void setSubGroups(List<RequirementGroup> subGroups) {
        this.subGroups = subGroups;
    }

    public Boolean getFulfillmentIndicator() {
        return fulfillmentIndicator;
    }

    public void setFulfillmentIndicator(Boolean fulfillmentIndicator) {
        this.fulfillmentIndicator = fulfillmentIndicator;
    }

    public Boolean getUnbounded() {
        return unbounded;
    }

    public void setUnbounded(Boolean unbounded) {
        this.unbounded = unbounded;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequirementGroup)) {
            return false;
        }

        RequirementGroup that = (RequirementGroup) o;

        if (uuid != null ? !uuid.equals(that.uuid) : that.uuid != null) {
            return false;
        }
        if (criteria != null ? !criteria.equals(that.criteria) : that.criteria != null) {
            return false;
        }
        if (requirementList != null ? !requirementList.equals(that.requirementList) : that.requirementList != null) {
            return false;
        }
        if (parent != null ? !parent.equals(that.parent) : that.parent != null) {
            return false;
        }
        if (subGroups != null ? !subGroups.equals(that.subGroups) : that.subGroups != null) {
            return false;
        }
        if (fulfillmentIndicator != null ? !fulfillmentIndicator.equals(that.fulfillmentIndicator) : that.fulfillmentIndicator != null) {
            return false;
        }
        return unbounded != null ? unbounded.equals(that.unbounded) : that.unbounded == null;
    }

    @Override
    public int hashCode() {
        int result = uuid != null ? uuid.hashCode() : 0;
        result = 31 * result + (criteria != null ? criteria.hashCode() : 0);
        result = 31 * result + (requirementList != null ? requirementList.hashCode() : 0);
        result = 31 * result + (parent != null ? parent.hashCode() : 0);
        result = 31 * result + (subGroups != null ? subGroups.hashCode() : 0);
        result = 31 * result + (fulfillmentIndicator != null ? fulfillmentIndicator.hashCode() : 0);
        result = 31 * result + (unbounded != null ? unbounded.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RequirementGroup{" +
                "uuid='" + uuid + '\'' +
                ", requirementList=" + requirementList +
                ", subGroups=" + subGroups +
                ", fulfillmentIndicator=" + fulfillmentIndicator +
                ", unbounded=" + unbounded +
                '}';
    }
}
