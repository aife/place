package com.atexo.dume.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * Model Class for DUME CONTEXT  MetaData.
 */

@Embeddable
@SuppressWarnings("unused")
public class MetaData implements Serializable {

    @Column(name = "BID_CLOSING_DATE")
    private ZonedDateTime bidClosingDate;

    @Column(name = "ORGANIZATION")
    private String organization;

    @Column(name = "CONSULTATION_IDENTIFIER")
    private Long consultationIdentifier;

    @Column(name = "SIRET")
    private String siret;

    @Column(name = "USER_INSC_IDENTIFIER")
    private Long userInscriptionIdentifier;

    @Column(name = "ID_TYPE_PROCEDURE")
    private String procedureTypeIdentifier;

    @Column(name = "ID_NATURE_MARKET")
    private String natureMarketIdentifier;

    @Column(name = "ID_TYPE_MARKET")
    private String marketTypeIdentifier;

    @Column(name = "IS_SIMPLIFIED")
    private Boolean simplified;


    public MetaData() {
        this(null, null, null, null, null);
    }

    public MetaData(final ZonedDateTime bidClosingDate, final String organization, final Long consultationIdentifier, final String siret, final Long userInscriptionIdentifier) {
        this.bidClosingDate = bidClosingDate;
        this.organization = organization;
        this.consultationIdentifier = consultationIdentifier;
        this.siret = siret;
        this.userInscriptionIdentifier = userInscriptionIdentifier;
    }

    public ZonedDateTime getBidClosingDate() {
        return bidClosingDate;
    }

    public void setBidClosingDate(final ZonedDateTime bidClosingDate) {
        this.bidClosingDate = bidClosingDate;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(final String organization) {
        this.organization = organization;
    }

    public Long getConsultationIdentifier() {
        return consultationIdentifier;
    }

    public void setConsultationIdentifier(final Long consultationIdentifier) {
        this.consultationIdentifier = consultationIdentifier;
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(final String siret) {
        this.siret = siret;
    }

    public Long getUserInscriptionIdentifier() {
        return userInscriptionIdentifier;
    }

    public void setUserInscriptionIdentifier(final Long userInscriptionIdentifier) {
        this.userInscriptionIdentifier = userInscriptionIdentifier;
    }

    public String getProcedureTypeIdentifier() {
        return procedureTypeIdentifier;
    }

    public void setProcedureTypeIdentifier(String procedureTypeIdentifier) {
        this.procedureTypeIdentifier = procedureTypeIdentifier;
    }

    public String getNatureMarketIdentifier() {
        return natureMarketIdentifier;
    }

    public void setNatureMarketIdentifier(String natureMarketIdentifier) {
        this.natureMarketIdentifier = natureMarketIdentifier;
    }

    public String getMarketTypeIdentifier() {
        return marketTypeIdentifier;
    }

    public void setMarketTypeIdentifier(String marketTypeIdentifier) {
        this.marketTypeIdentifier = marketTypeIdentifier;
    }

    public Boolean getSimplified() {
        return simplified;
    }

    public void setSimplified(Boolean simplified) {
        this.simplified = simplified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MetaData)) {
            return false;
        }

        MetaData metaData = (MetaData) o;

        if (bidClosingDate != null ? !bidClosingDate.equals(metaData.bidClosingDate) : metaData.bidClosingDate != null) {
            return false;
        }
        if (organization != null ? !organization.equals(metaData.organization) : metaData.organization != null) {
            return false;
        }
        if (consultationIdentifier != null ? !consultationIdentifier.equals(metaData.consultationIdentifier) : metaData.consultationIdentifier != null) {
            return false;
        }
        if (siret != null ? !siret.equals(metaData.siret) : metaData.siret != null) {
            return false;
        }
        if (userInscriptionIdentifier != null ? !userInscriptionIdentifier.equals(metaData.userInscriptionIdentifier) : metaData.userInscriptionIdentifier != null) {
            return false;
        }
        if (procedureTypeIdentifier != null ? !procedureTypeIdentifier.equals(metaData.procedureTypeIdentifier) : metaData.procedureTypeIdentifier != null) {
            return false;
        }
        if (natureMarketIdentifier != null ? !natureMarketIdentifier.equals(metaData.natureMarketIdentifier) : metaData.natureMarketIdentifier != null) {
            return false;
        }
        if (simplified != null ? !simplified.equals(metaData.simplified) : metaData.simplified != null) {
            return false;
        }
        return marketTypeIdentifier != null ? marketTypeIdentifier.equals(metaData.marketTypeIdentifier) : metaData.marketTypeIdentifier == null;
    }

    @Override
    public int hashCode() {
        int result = bidClosingDate != null ? bidClosingDate.hashCode() : 0;
        result = 31 * result + (organization != null ? organization.hashCode() : 0);
        result = 31 * result + (consultationIdentifier != null ? consultationIdentifier.hashCode() : 0);
        result = 31 * result + (siret != null ? siret.hashCode() : 0);
        result = 31 * result + (userInscriptionIdentifier != null ? userInscriptionIdentifier.hashCode() : 0);
        result = 31 * result + (procedureTypeIdentifier != null ? procedureTypeIdentifier.hashCode() : 0);
        result = 31 * result + (natureMarketIdentifier != null ? natureMarketIdentifier.hashCode() : 0);
        result = 31 * result + (marketTypeIdentifier != null ? marketTypeIdentifier.hashCode() : 0);
        result = 31 * result + (simplified != null ? simplified.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MetaData{" +
                "bidClosingDate=" + bidClosingDate +
                ", organization='" + organization + '\'' +
                ", consultationIdentifier=" + consultationIdentifier +
                ", siret='" + siret + '\'' +
                ", userInscriptionIdentifier=" + userInscriptionIdentifier +
                ", procedureTypeIdentifier='" + procedureTypeIdentifier + '\'' +
                ", natureMarketIdentifier='" + natureMarketIdentifier + '\'' +
                ", marketTypeIdentifier='" + marketTypeIdentifier + '\'' + ", simplified='" + simplified + '\'' +
                '}';
    }
}
