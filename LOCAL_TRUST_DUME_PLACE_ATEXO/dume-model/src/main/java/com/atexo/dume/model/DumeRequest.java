package com.atexo.dume.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@DiscriminatorValue("REQUEST")
public class DumeRequest extends DumeDocument {
    public DumeRequest() {
        super();
    }

    public DumeRequest(@NotNull DumeRequestContext context) {
        super(context);
    }

    public DumeRequest(@NotNull final List<CriteriaSelectionDocument> selectionCriteriaList, @NotNull final List<ExclusionCriteria> exclusionCriteriaList) {
        super(selectionCriteriaList, exclusionCriteriaList);
    }

    public DumeRequest(@NotNull final List<CriteriaSelectionDocument> selectionCriteriaList,
                       @NotNull final List<ExclusionCriteria> exclusionCriteriaList,
                       @NotNull final DumeRequestContext dumeRequestContext) {
        super(selectionCriteriaList, exclusionCriteriaList);
        this.setContext(dumeRequestContext);
    }

    @Override
    public String toString() {
        return "DumeRequest{" + super.toString() + "}";
    }
}
