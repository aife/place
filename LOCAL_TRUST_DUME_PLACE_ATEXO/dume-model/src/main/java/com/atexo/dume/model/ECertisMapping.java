package com.atexo.dume.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "E_CERTIS_MAPPING")
public class ECertisMapping extends AbstractEntity {

    @Column(name = "COUNTRY_CODE", unique = true)
    private String countryCode;

    @Column(name = "E_CERTIS_COUNTRY_CODE")
    private String eCertisCountryCode;

    @Column(name = "E_CERTIS_LANGUAGE_CODE")
    private String eCertisLanguageCode;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String geteCertisCountryCode() {
        return eCertisCountryCode;
    }

    public void seteCertisCountryCode(String eCertisCountryCode) {
        this.eCertisCountryCode = eCertisCountryCode;
    }

    public String geteCertisLanguageCode() {
        return eCertisLanguageCode;
    }

    public void seteCertisLanguageCode(String eCertisLanguageCode) {
        this.eCertisLanguageCode = eCertisLanguageCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ECertisMapping that = (ECertisMapping) o;

        if (countryCode != null ? !countryCode.equals(that.countryCode) : that.countryCode != null) return false;
        if (eCertisCountryCode != null ? !eCertisCountryCode.equals(that.eCertisCountryCode) : that.eCertisCountryCode != null) return false;
        return eCertisLanguageCode != null ? eCertisLanguageCode.equals(that.eCertisLanguageCode) : that.eCertisLanguageCode == null;
    }

    @Override
    public int hashCode() {
        int result = countryCode != null ? countryCode.hashCode() : 0;
        result = 31 * result + (eCertisCountryCode != null ? eCertisCountryCode.hashCode() : 0);
        result = 31 * result + (eCertisLanguageCode != null ? eCertisLanguageCode.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ECertisMapping{" + "countryCode='" + countryCode + '\'' + ", eCertisCountryCode='" + eCertisCountryCode + '\'' + ", eCertisLanguageCode='" + eCertisLanguageCode + '\'' + '}';
    }
}
