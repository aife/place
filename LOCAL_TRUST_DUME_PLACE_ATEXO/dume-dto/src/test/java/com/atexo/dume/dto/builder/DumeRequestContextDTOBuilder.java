package com.atexo.dume.dto.builder;

import com.atexo.dume.dto.DumeRequestContextDTO;
import com.atexo.dume.dto.DumeRequestDTO;
import com.atexo.dume.dto.LotDTO;
import com.atexo.dume.dto.MetaDataDTO;

import java.util.Set;

public final class DumeRequestContextDTOBuilder {


    private String officialName;
    private String country;
    private String consultRef;
    private String procedureType;
    private String entitled;
    private String object;
    private MetaDataDTO metadata;
    private Boolean isDefault;
    private Set<LotDTO> lotDTOList;
    private Set<DumeRequestDTO> dumeRequestDTOSet;
    private Long identifier;


    private DumeRequestContextDTOBuilder() {
    }

    public static DumeRequestContextDTOBuilder aContext() {
        return new DumeRequestContextDTOBuilder();
    }

    public DumeRequestContextDTOBuilder withOfficialName(final String officialName) {
        this.officialName = officialName;
        return this;
    }

    public DumeRequestContextDTOBuilder withDocuments(final Set<DumeRequestDTO> dumeRequestDTOSet) {
        this.dumeRequestDTOSet = dumeRequestDTOSet;
        return this;
    }

    public DumeRequestContextDTOBuilder withCountry(final String country) {
        this.country = country;
        return this;
    }

    public DumeRequestContextDTOBuilder withConsultRef(final String consultRef) {
        this.consultRef = consultRef;
        return this;
    }

    public DumeRequestContextDTOBuilder withProcedureType(final String procedureType) {
        this.procedureType = procedureType;
        return this;
    }

    public DumeRequestContextDTOBuilder withEntitled(final String entitled) {
        this.entitled = entitled;
        return this;
    }

    public DumeRequestContextDTOBuilder withIdentifier(final Long identifier) {
        this.identifier = identifier;
        return this;
    }


    public DumeRequestContextDTOBuilder withObject(final String object) {
        this.object = object;
        return this;
    }

    public DumeRequestContextDTOBuilder withMetaData(final MetaDataDTO metadata) {
        this.metadata = metadata;
        return this;
    }

    public DumeRequestContextDTOBuilder withLots(final Set<LotDTO> lotDTOList) {
        this.lotDTOList = lotDTOList;
        return this;
    }

    public DumeRequestContextDTOBuilder withDefault(final Boolean isDefault) {
        this.isDefault = isDefault;
        return this;
    }

    public DumeRequestContextDTO build() {
        final DumeRequestContextDTO contextRequest = new DumeRequestContextDTO(officialName, country, consultRef, procedureType, entitled, object, isDefault);
        contextRequest.setMetadata(metadata);
        contextRequest.setLotDTOSet(lotDTOList);
        contextRequest.setDumeRequestSets(dumeRequestDTOSet);
        contextRequest.setId(identifier);
        return contextRequest;
    }
}
