package com.atexo.dume.dto.builder;

import com.atexo.dume.dto.CriteriaCategorieDTO;
import com.atexo.dume.dto.LegislationReferenceDTO;
import com.atexo.dume.dto.OtherCriteriaDTO;
import com.atexo.dume.dto.RequirementGroupDTO;

import java.util.List;

public final class OtherCriteriaDTOBuilder {
    private Long id;
    private String uuid;
    private String shortName;
    private String description;
    private LegislationReferenceDTO legislationReference;
    private String code;
    private int order;
    private CriteriaCategorieDTO criteriaCategorie;
    private List<RequirementGroupDTO> requirementGroupDTOS;

    private OtherCriteriaDTOBuilder() {
    }

    public static OtherCriteriaDTOBuilder anOtherCriteriaDTO() {
        return new OtherCriteriaDTOBuilder();
    }

    public OtherCriteriaDTOBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public OtherCriteriaDTOBuilder withUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public OtherCriteriaDTOBuilder withShortName(String shortName) {
        this.shortName = shortName;
        return this;
    }

    public OtherCriteriaDTOBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public OtherCriteriaDTOBuilder withLegislationReference(LegislationReferenceDTO legislationReference) {
        this.legislationReference = legislationReference;
        return this;
    }

    public OtherCriteriaDTOBuilder withCode(String code) {
        this.code = code;
        return this;
    }

    public OtherCriteriaDTOBuilder withOrder(int order) {
        this.order = order;
        return this;
    }

    public OtherCriteriaDTOBuilder withCriteriaCategorie(CriteriaCategorieDTO criteriaCategorie) {
        this.criteriaCategorie = criteriaCategorie;
        return this;
    }

    public OtherCriteriaDTOBuilder withRequirementGroupDTOS(List<RequirementGroupDTO> requirementGroupDTOS) {
        this.requirementGroupDTOS = requirementGroupDTOS;
        return this;
    }

    public OtherCriteriaDTO build() {
        OtherCriteriaDTO otherCriteriaDTO = new OtherCriteriaDTO();
        otherCriteriaDTO.setId(id);
        otherCriteriaDTO.setUuid(uuid);
        otherCriteriaDTO.setShortName(shortName);
        otherCriteriaDTO.setDescription(description);
        otherCriteriaDTO.setLegislationReference(legislationReference);
        otherCriteriaDTO.setCode(code);
        otherCriteriaDTO.setOrder(order);
        otherCriteriaDTO.setCriteriaCategorie(criteriaCategorie);
        otherCriteriaDTO.setRequirementGroupList(requirementGroupDTOS);
        return otherCriteriaDTO;
    }
}
