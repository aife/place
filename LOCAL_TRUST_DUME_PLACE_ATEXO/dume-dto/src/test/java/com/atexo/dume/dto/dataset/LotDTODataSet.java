package com.atexo.dume.dto.dataset;

import com.atexo.dume.dto.LotDTO;
import com.atexo.dume.dto.builder.LotDTOBuilder;

import java.util.HashSet;
import java.util.Set;

public class LotDTODataSet {

    public static final Long DUMMY_SIMPLE_INTERNAL_ID = 354321L;
    public static final String DUMMY_SIMPLE_LOT_NUMBER = "646143654";
    public static final String DUMMY_SIMPLE_LOT_LABEL = "THIS_IS_ME";

    public static LotDTO dummySimpleLot() {

        return LotDTOBuilder.aLot().
                withInternalLotIdentifier(DUMMY_SIMPLE_INTERNAL_ID)
                .withLlotNumber(DUMMY_SIMPLE_LOT_NUMBER)
                .withLotName(DUMMY_SIMPLE_LOT_LABEL)
                .build();
    }

    public static Set<LotDTO> dummySimpleLotListWithOneElement() {
        final Set<LotDTO> retValue = new HashSet<>();
        retValue.add(LotDTOBuilder.aLot().
                withInternalLotIdentifier(DUMMY_SIMPLE_INTERNAL_ID)
                .withLlotNumber(DUMMY_SIMPLE_LOT_NUMBER)
                .withLotName(DUMMY_SIMPLE_LOT_LABEL)
                .build());

        return retValue;
    }
}
