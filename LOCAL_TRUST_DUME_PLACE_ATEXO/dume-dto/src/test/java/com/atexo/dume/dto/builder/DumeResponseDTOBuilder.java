package com.atexo.dume.dto.builder;

import com.atexo.dume.dto.DumeResponseDTO;
import com.atexo.dume.dto.ExclusionCriteriaDTO;
import com.atexo.dume.dto.SelectionCriteriaDTO;

import java.util.List;

public final class DumeResponseDTOBuilder {
    private Long id;
    private String snNumber;
    private List<SelectionCriteriaDTO> selectionCriteriaList;
    private List<ExclusionCriteriaDTO> exclusionCriteriaList;

    private DumeResponseDTOBuilder() {
    }

    public static DumeResponseDTOBuilder aDumeResponseDTO() {
        return new DumeResponseDTOBuilder();
    }

    public DumeResponseDTOBuilder withId(Long id) {
        this.id = id;
        return this;
    }
    public DumeResponseDTOBuilder withSelectionCriteriaList(List<SelectionCriteriaDTO> selectionCriteriaList) {
        this.selectionCriteriaList = selectionCriteriaList;
        return this;
    }

    public DumeResponseDTOBuilder withExclusionCriteriaList(List<ExclusionCriteriaDTO> exclusionCriteriaList) {
        this.exclusionCriteriaList = exclusionCriteriaList;
        return this;
    }

    public DumeResponseDTO build() {
        DumeResponseDTO dumeResponseDTO = new DumeResponseDTO();
        dumeResponseDTO.setId(id);
        dumeResponseDTO.setNumeroSN(snNumber);
        dumeResponseDTO.setSelectionCriteriaList(selectionCriteriaList);
        dumeResponseDTO.setExclusionCriteriaList(exclusionCriteriaList);
        return dumeResponseDTO;
    }

    public DumeResponseDTOBuilder witSNNumber(String snNumber) {
        this.snNumber=snNumber;
        return this;
    }
}
