package com.atexo.dume.dto.builder;

import com.atexo.dume.dto.CriteriaCategorieDTO;
import com.atexo.dume.dto.LegislationReferenceDTO;
import com.atexo.dume.dto.RequirementGroupDTO;
import com.atexo.dume.dto.SelectionCriteriaDTO;

import java.util.List;

public final class SelectionCriteriaDTOBuilder {
    private Long id;
    private String uuid;
    private String shortName;
    private String description;
    private LegislationReferenceDTO legislationReference;
    private String code;
    private CriteriaCategorieDTO criteriaCategorie;
    private List<RequirementGroupDTO> requirementGroupList;

    private SelectionCriteriaDTOBuilder() {
    }

    public static SelectionCriteriaDTOBuilder aSelectionCriteriaDTO() {
        return new SelectionCriteriaDTOBuilder();
    }

    public SelectionCriteriaDTOBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public SelectionCriteriaDTOBuilder withUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public SelectionCriteriaDTOBuilder withShortName(String shortName) {
        this.shortName = shortName;
        return this;
    }

    public SelectionCriteriaDTOBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public SelectionCriteriaDTOBuilder withLegislationReference(LegislationReferenceDTO legislationReference) {
        this.legislationReference = legislationReference;
        return this;
    }

    public SelectionCriteriaDTOBuilder withCode(String code) {
        this.code = code;
        return this;
    }

    public SelectionCriteriaDTOBuilder withCriteriaCategorie(CriteriaCategorieDTO criteriaCategorie) {
        this.criteriaCategorie = criteriaCategorie;
        return this;
    }

    public SelectionCriteriaDTOBuilder withRequirementGroupList(List<RequirementGroupDTO> requirementGroupList) {
        this.requirementGroupList = requirementGroupList;
        return this;
    }

    public SelectionCriteriaDTO build() {
        SelectionCriteriaDTO selectionCriteriaDTO = new SelectionCriteriaDTO();
        selectionCriteriaDTO.setId(id);
        selectionCriteriaDTO.setUuid(uuid);
        selectionCriteriaDTO.setShortName(shortName);
        selectionCriteriaDTO.setDescription(description);
        selectionCriteriaDTO.setLegislationReference(legislationReference);
        selectionCriteriaDTO.setCode(code);
        selectionCriteriaDTO.setCriteriaCategorie(criteriaCategorie);
        selectionCriteriaDTO.setRequirementGroupList(requirementGroupList);
        return selectionCriteriaDTO;
    }
}
