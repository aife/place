package com.atexo.dume.dto.dataset;


import com.atexo.dume.dto.ResponseDTO;
import com.atexo.dume.dto.builder.ResponseDTOBuilder;

import java.util.ArrayList;
import java.util.List;

import static com.atexo.dume.dto.dataset.RequirementDTODataSet.SIMPLE_REQUIREMENT_IDENTIFIER_1;
import static com.atexo.dume.dto.dataset.RequirementDTODataSet.SIMPLE_REQUIREMENT_IDENTIFIER_2;

public class ResponseDTODataSet {

    public static final Long SIMPLE_RESPONSE_IDENTIFIER_1 = 400L;
    public static final Long SIMPLE_RESPONSE_IDENTIFIER_2 = 401L;


    public static ResponseDTO sampleWithId() {
        return ResponseDTOBuilder.aResponseDTO()
                                 .withId(SIMPLE_RESPONSE_IDENTIFIER_1)
                                 .build();
    }

    public static ResponseDTO sampleWithAssignedRequirement() {
        return ResponseDTOBuilder.aResponseDTO()
                                 .withId(SIMPLE_RESPONSE_IDENTIFIER_1)
                                 .withIdRequirement(SIMPLE_REQUIREMENT_IDENTIFIER_1)
                                 .build();
    }

    public static ResponseDTO sampleWithoutId() {
        return ResponseDTOBuilder.aResponseDTO()
                                 .build();
    }

    public static List<ResponseDTO> sampleListWithAssignedRequirement() {
        List<ResponseDTO> responseDTOList = new ArrayList<>();
        responseDTOList.add(ResponseDTOBuilder.aResponseDTO()
                                              .withId(SIMPLE_RESPONSE_IDENTIFIER_1)
                                              .withIdRequirement(SIMPLE_REQUIREMENT_IDENTIFIER_1)
                                              .build());
        responseDTOList.add(ResponseDTOBuilder.aResponseDTO()
                                              .withId(SIMPLE_RESPONSE_IDENTIFIER_2)
                                              .withIdRequirement(SIMPLE_REQUIREMENT_IDENTIFIER_2)
                                              .build());
        return responseDTOList;
    }


}
