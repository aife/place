package com.atexo.dume.dto.builder;

import com.atexo.dume.dto.RequirementDTO;
import com.atexo.dume.dto.ResponseDTO;

public final class RequirementDTOBuilder {
    private Long id;
    private String uuid;
    private String description;
    private String responseType;
    private ResponseDTO responseDTO;

    private RequirementDTOBuilder() {
    }

    public static RequirementDTOBuilder aRequirementDTO() {
        return new RequirementDTOBuilder();
    }

    public RequirementDTOBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public RequirementDTOBuilder withUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public RequirementDTOBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public RequirementDTOBuilder withResponseType(String responseType) {
        this.responseType = responseType;
        return this;
    }

    public RequirementDTOBuilder withResponseDTO(ResponseDTO responseDTO) {
        this.responseDTO = responseDTO;
        return this;
    }

    public RequirementDTO build() {
        RequirementDTO requirementDTO = new RequirementDTO();
        requirementDTO.setId(id);
        requirementDTO.setUuid(uuid);
        requirementDTO.setDescription(description);
        requirementDTO.setResponseType(responseType);
        requirementDTO.setResponseDTO(responseDTO);
        return requirementDTO;
    }
}
