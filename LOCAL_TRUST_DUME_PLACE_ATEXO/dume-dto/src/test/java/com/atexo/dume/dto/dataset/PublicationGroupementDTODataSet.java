package com.atexo.dume.dto.dataset;

import com.atexo.dume.dto.PublicationGroupementDTO;
import com.atexo.dume.dto.builder.PublicationGroupementDTOBuilder;

import java.util.ArrayList;
import java.util.List;

public class PublicationGroupementDTODataSet {

    public static final String DUMMY_SIMPLE_SIRET_MANDATAIRE = "44090956200033";
    public static final Long DUMMY_SIMPLE_ID_CONTEXTE_1 = 1L;
    public static final Long DUMMY_SIMPLE_ID_CONTEXTE_2 = 2L;

    public static PublicationGroupementDTO dummyPublicationGroupementDTO() {

        List<Long> list = new ArrayList<>();
        list.add(DUMMY_SIMPLE_ID_CONTEXTE_1);
        list.add(DUMMY_SIMPLE_ID_CONTEXTE_2);

        return PublicationGroupementDTOBuilder.aPublicationGroupementDTO()
                .withSiretMandataire(DUMMY_SIMPLE_SIRET_MANDATAIRE)
                .withIdsContexteGroupement(list)
                .build();
    }

    public static PublicationGroupementDTO dummyEmptyPublicationGroupementDTO() {

        return PublicationGroupementDTOBuilder.aPublicationGroupementDTO()
                .withSiretMandataire(DUMMY_SIMPLE_SIRET_MANDATAIRE)
                .withIdsContexteGroupement(new ArrayList<>())
                .build();
    }

}
