package com.atexo.dume.dto.dataset;

import com.atexo.dume.dto.donneesessentielles.xml.AcheteurType;
import com.atexo.dume.dto.donneesessentielles.xml.ConsultationType;
import com.atexo.dume.dto.donneesessentielles.xml.FormePrixType;
import com.atexo.dume.dto.donneesessentielles.xml.LieuExecutionType;
import com.atexo.dume.dto.donneesessentielles.xml.MarcheType;
import com.atexo.dume.dto.donneesessentielles.xml.TitulaireType;
import com.atexo.dume.dto.donneesessentielles.xml.TypeCodeType;
import com.atexo.dume.dto.donneesessentielles.xml.TypeIdentifiantType;

import java.math.BigDecimal;
import java.math.BigInteger;

import static com.atexo.dume.dto.donneesessentielles.xml.NatureType.MARCHÉ_DE_PARTENARIAT;
import static com.atexo.dume.dto.donneesessentielles.xml.ProcedureType.APPEL_D_OFFRES_OUVERT;


public class MarcheTypeDataSet {

    public static MarcheType sample() {

        MarcheType marcheType = new MarcheType();
        marcheType.setAcheteur(new AcheteurType());
        marcheType.getAcheteur().setNom("Chambres de Commerce et d'Industrie (CCI)");
        marcheType.getAcheteur().setId("41384672900033");
        marcheType.setConsultation(new ConsultationType());
        marcheType.getConsultation().setId("36417");
        marcheType.setId("2018SU00002601");
        marcheType.setNature(MARCHÉ_DE_PARTENARIAT);
        marcheType.setObjet("Restauration de l'empire Romain");
        marcheType.setCodeCPV("38343000");
        marcheType.setProcedure(APPEL_D_OFFRES_OUVERT);
        marcheType.setLieuExecution(new LieuExecutionType());
        marcheType.getLieuExecution().setCode("94");
        marcheType.getLieuExecution().setTypeCode(TypeCodeType.CODE_DÉPARTEMENT);
        marcheType.getLieuExecution().setNom("(94) Val-de-Marne");
        marcheType.setDureeMois(BigInteger.ONE);
        marcheType.setMontant(BigDecimal.valueOf(534.0));
        marcheType.setFormePrix(FormePrixType.FERME);
        marcheType.setTitulaires(new MarcheType.Titulaires());
        TitulaireType titulaireType = new TitulaireType();
        titulaireType.setId("44090956200033");
        titulaireType.setTypeIdentifiant(TypeIdentifiantType.SIRET);
        titulaireType.setDenominationSociale("ATEXO");
        marcheType.getTitulaires().getTitulaire().add(titulaireType);
        return marcheType;
    }
}
