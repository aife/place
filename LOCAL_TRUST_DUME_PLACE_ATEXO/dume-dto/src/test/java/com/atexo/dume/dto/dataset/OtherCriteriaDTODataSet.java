package com.atexo.dume.dto.dataset;

import com.atexo.dume.dto.OtherCriteriaDTO;
import com.atexo.dume.dto.builder.OtherCriteriaDTOBuilder;

public class OtherCriteriaDTODataSet {
    public static final Long OTHER_CRITERIA_DTO_IDENTIFIER_1 = 10000L;
    public static final Long OTHER_CRITERIA_DTO_IDENTIFIER_2 = 10001L;

    public static OtherCriteriaDTO sample1WithId() {
        return OtherCriteriaDTOBuilder.anOtherCriteriaDTO()
                .withId(OTHER_CRITERIA_DTO_IDENTIFIER_1)
                .withCode("CodeExclusion1")
                .build();
    }

    public static OtherCriteriaDTO sample2WithId() {
        return OtherCriteriaDTOBuilder.anOtherCriteriaDTO()
                .withId(OTHER_CRITERIA_DTO_IDENTIFIER_2)
                .withCode("CodeExclusion2")
                .build();
    }
}
