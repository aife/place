package com.atexo.dume.dto.builder;

import com.atexo.dume.dto.CriteriaCategorieDTO;
import com.atexo.dume.dto.ExclusionCriteriaDTO;
import com.atexo.dume.dto.LegislationReferenceDTO;
import com.atexo.dume.dto.RequirementGroupDTO;

import java.util.List;

public final class ExclusionCriteriaDTOBuilder {
    private Long id;
    private String uuid;
    private String shortName;
    private String description;
    private LegislationReferenceDTO legislationReference;
    private String code;
    private CriteriaCategorieDTO criteriaCategorie;
    private List<RequirementGroupDTO> requirementGroupList;

    private ExclusionCriteriaDTOBuilder() {
    }

    public static ExclusionCriteriaDTOBuilder anExclusionCriteriaDTO() {
        return new ExclusionCriteriaDTOBuilder();
    }

    public ExclusionCriteriaDTOBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public ExclusionCriteriaDTOBuilder withUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public ExclusionCriteriaDTOBuilder withShortName(String shortName) {
        this.shortName = shortName;
        return this;
    }

    public ExclusionCriteriaDTOBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public ExclusionCriteriaDTOBuilder withLegislationReference(LegislationReferenceDTO legislationReference) {
        this.legislationReference = legislationReference;
        return this;
    }

    public ExclusionCriteriaDTOBuilder withCode(String code) {
        this.code = code;
        return this;
    }

    public ExclusionCriteriaDTOBuilder withCriteriaCategorie(CriteriaCategorieDTO criteriaCategorie) {
        this.criteriaCategorie = criteriaCategorie;
        return this;
    }

    public ExclusionCriteriaDTOBuilder withRequirementGroupList(List<RequirementGroupDTO> requirementGroupList) {
        this.requirementGroupList = requirementGroupList;
        return this;
    }

    public ExclusionCriteriaDTO build() {
        ExclusionCriteriaDTO exclusionCriteriaDTO = new ExclusionCriteriaDTO();
        exclusionCriteriaDTO.setId(id);
        exclusionCriteriaDTO.setUuid(uuid);
        exclusionCriteriaDTO.setShortName(shortName);
        exclusionCriteriaDTO.setDescription(description);
        exclusionCriteriaDTO.setLegislationReference(legislationReference);
        exclusionCriteriaDTO.setCode(code);
        exclusionCriteriaDTO.setCriteriaCategorie(criteriaCategorie);
        exclusionCriteriaDTO.setRequirementGroupList(requirementGroupList);
        return exclusionCriteriaDTO;
    }
}
