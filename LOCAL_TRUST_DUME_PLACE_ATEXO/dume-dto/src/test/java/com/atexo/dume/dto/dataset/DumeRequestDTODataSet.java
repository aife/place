package com.atexo.dume.dto.dataset;

import com.atexo.dume.dto.CriteriaLotDTO;
import com.atexo.dume.dto.DumeRequestDTO;
import com.atexo.dume.dto.builder.DumeRequestDTOBuilder;

import static java.util.Arrays.asList;

public class DumeRequestDTODataSet {

    public static final Long DUME_REQUEST_DTO_IDENTIFIER_1 = 10000L;
    public static final Long DUME_REQUEST_DTO_IDENTIFIER_2 = 10001L;

    public static DumeRequestDTO sampleWithId() {
        return DumeRequestDTOBuilder.aDumeRequestDTO()
                .withId(DUME_REQUEST_DTO_IDENTIFIER_1)
                .withSelectionCriteriaCodes(asList(new CriteriaLotDTO()))
                .build();
    }

    public static DumeRequestDTO sample2WithId() {
        return DumeRequestDTOBuilder.aDumeRequestDTO()
                .withId(DUME_REQUEST_DTO_IDENTIFIER_2)
                .build();
    }

    public static DumeRequestDTO sampleWithoutId() {
        return DumeRequestDTOBuilder.aDumeRequestDTO()
                .build();
    }


}
