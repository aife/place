package com.atexo.dume.dto.dataset;

import com.atexo.dume.dto.ExclusionCriteriaDTO;
import com.atexo.dume.dto.builder.ExclusionCriteriaDTOBuilder;

public class ExclusionCriteriaDTODataSet {
    public static final Long EXCLUSION_CRITERIA_DTO_IDENTIFIER_1 = 10000L;
    public static final Long EXCLUSION_CRITERIA_DTO_IDENTIFIER_2 = 10001L;

    public static ExclusionCriteriaDTO sample1WithId() {
        return ExclusionCriteriaDTOBuilder.anExclusionCriteriaDTO()
                .withId(EXCLUSION_CRITERIA_DTO_IDENTIFIER_1)
                .withCode("CodeExclusion1")
                .build();
    }

    public static ExclusionCriteriaDTO sample2WithId() {
        return ExclusionCriteriaDTOBuilder.anExclusionCriteriaDTO()
                .withId(EXCLUSION_CRITERIA_DTO_IDENTIFIER_2)
                .withCode("CodeExclusion2")
                .build();
    }
}
