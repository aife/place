package com.atexo.dume.dto.dataset;

import com.atexo.dume.dto.DumeResponseDTO;
import com.atexo.dume.dto.SelectionCriteriaDTO;
import com.atexo.dume.dto.builder.DumeResponseDTOBuilder;

import java.util.ArrayList;
import java.util.List;

public class DumeResponseDTODataSet {

    public static final Long DUME_RESPONSE_DTO_IDENTIFIER_1 = 10000L;
    public static final Long DUME_RESPONSE_DTO_IDENTIFIER_2 = 10001L;
    public static final String DUME_RESPONSE_NUMERO_SN_1 = "aZeRtY";
    public static final String DUME_RESPONSE_SN_NUMBER_2 = "SN_NB1";

    public static DumeResponseDTO sampleWithId() {
        return DumeResponseDTOBuilder.aDumeResponseDTO()
                                     .withId(DUME_RESPONSE_DTO_IDENTIFIER_1)
                                     .build();
    }

    public static DumeResponseDTO sampleWithIdAndSNNumber() {
        return DumeResponseDTOBuilder.aDumeResponseDTO()
                .withId(DUME_RESPONSE_DTO_IDENTIFIER_1)
                .witSNNumber(DUME_RESPONSE_SN_NUMBER_2)
                .build();
    }

    public static DumeResponseDTO sampleWithSimpleStructure() {
        List<SelectionCriteriaDTO> selectionCriteriaDTOList = new ArrayList<>();
        selectionCriteriaDTOList.add(SelectionCriteriaDTODataSet.sampleWithSimpleStructure());
        return DumeResponseDTOBuilder.aDumeResponseDTO()
                                     .withId(DUME_RESPONSE_DTO_IDENTIFIER_1)
                                     .withSelectionCriteriaList(selectionCriteriaDTOList)
                                     .build();
    }

    public static DumeResponseDTO sample2WithId() {
        return DumeResponseDTOBuilder.aDumeResponseDTO()
                                     .withId(DUME_RESPONSE_DTO_IDENTIFIER_2)
                                     .build();
    }

    public static DumeResponseDTO sampleWithoutId() {
        return DumeResponseDTOBuilder.aDumeResponseDTO()
                                     .build();
    }

    public static DumeResponseDTO sampleWithNestedStructure() {
        List<SelectionCriteriaDTO> selectionCriteriaDTOList = new ArrayList<>();
        selectionCriteriaDTOList.add(SelectionCriteriaDTODataSet.sampleWithNestedStructure());
        return DumeResponseDTOBuilder.aDumeResponseDTO()
                                     .withId(DUME_RESPONSE_DTO_IDENTIFIER_1)
                                     .withSelectionCriteriaList(selectionCriteriaDTOList)
                                     .build();
    }

    public static DumeResponseDTO sampleWithRequirementGroupInstanceStructure() {
        List<SelectionCriteriaDTO> selectionCriteriaDTOList = new ArrayList<>();
        selectionCriteriaDTOList.add(SelectionCriteriaDTODataSet.sampleWithRequirementGroupInstanceStructure());
        return DumeResponseDTOBuilder.aDumeResponseDTO()
                                     .withId(DUME_RESPONSE_DTO_IDENTIFIER_1)
                                     .withSelectionCriteriaList(selectionCriteriaDTOList)
                                     .build();
    }

}
