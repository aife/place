package com.atexo.dume.dto.dataset;

import com.atexo.dume.dto.MetaDataDTO;
import com.atexo.dume.dto.builder.MetaDataDTOBuilder;

import java.time.ZonedDateTime;

public class MetaDataDTODataSet {

    public static final Long DUMMY_CONSULT_ID = 125454L;
    public static final Long USER_INSC_ID = 542L;
    public static final String DUMMY_ORG = "DUMMY_ORG";
    public static final String DUMMY_PLAT_IDENT = "DUMMY_PLAT_IDENT";
    public static final String DUMMY_SIRET = "DUMMY_SIRET";

    public static MetaDataDTO dummyMetaData() {
        return MetaDataDTOBuilder.aMetaData()
                                 .withConsultationIdentifier(DUMMY_CONSULT_ID)
                                 .withBidClosingDate(ZonedDateTime.now())
                                 .withOrganization(DUMMY_ORG)
                                 .withPlatformIdentifier(DUMMY_PLAT_IDENT)
                                 .withSerInscriptionIdentifier(USER_INSC_ID)
                                 .withSiret(DUMMY_SIRET)
                                 .build();
    }

}
