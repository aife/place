package com.atexo.dume.dto.dataset;

import com.atexo.dume.dto.DumeRequestContextDTO;
import com.atexo.dume.dto.builder.DumeRequestContextDTOBuilder;

import java.util.HashSet;

import static com.atexo.dume.dto.dataset.LotDTODataSet.dummySimpleLotListWithOneElement;
import static com.atexo.dume.dto.dataset.MetaDataDTODataSet.dummyMetaData;

public class DumeRequestContextDataDTOSet {

    public static final String SIMPLE_OBJECT = "object";
    public static final String SIMPLE_COUNTRY = "Pays";
    public static final String SIMPLE_PROC_TYPE = "Type1";
    public static final String SIMPLE_OFF_NAME = "NomOfficiel";
    public static final String SIMPLE_ENTITLED = "intitule";
    public static final String SIMPLE_REF_CONS = "01245697";
    public static final Long SIMPLE_CONTX_ID = 65843546352L;


    public static DumeRequestContextDTO contextSample() {

        return DumeRequestContextDTOBuilder.aContext().withObject(SIMPLE_OBJECT)
                .withCountry(SIMPLE_COUNTRY)
                .withIdentifier(SIMPLE_CONTX_ID)
                .withConsultRef(SIMPLE_REF_CONS)
                .withProcedureType(SIMPLE_PROC_TYPE)
                .withOfficialName(SIMPLE_OFF_NAME)
                .withMetaData(dummyMetaData())
                .withLots(dummySimpleLotListWithOneElement())
                .withDocuments(new HashSet<>())
                .withEntitled(SIMPLE_ENTITLED).build();
    }
}
