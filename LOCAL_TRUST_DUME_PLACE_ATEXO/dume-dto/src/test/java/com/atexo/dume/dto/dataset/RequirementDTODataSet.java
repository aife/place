package com.atexo.dume.dto.dataset;


import com.atexo.dume.dto.RequirementDTO;
import com.atexo.dume.dto.builder.RequirementDTOBuilder;

public class RequirementDTODataSet {

    public static final Long SIMPLE_REQUIREMENT_IDENTIFIER_1 = 200L;
    public static final Long SIMPLE_REQUIREMENT_IDENTIFIER_2 = 201L;


    public static RequirementDTO sampleWithId() {
        return RequirementDTOBuilder.aRequirementDTO()
                                    .withId(SIMPLE_REQUIREMENT_IDENTIFIER_1)
                                    .build();
    }

    public static RequirementDTO sample2WithId() {
        return RequirementDTOBuilder.aRequirementDTO()
                                    .withId(SIMPLE_REQUIREMENT_IDENTIFIER_2)
                                    .build();
    }

    public static RequirementDTO sampleWithoutId() {
        return RequirementDTOBuilder.aRequirementDTO()
                                    .build();
    }
}
