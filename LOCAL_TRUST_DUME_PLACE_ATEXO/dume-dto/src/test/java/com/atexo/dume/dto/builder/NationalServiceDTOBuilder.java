package com.atexo.dume.dto.builder;

import com.atexo.dume.dto.sn.response.*;

import java.util.HashMap;
import java.util.Map;

public final class NationalServiceDTOBuilder {
    private String snNumber;
    private boolean accessible;
    private String dumeA;

    private NationalServiceDTOBuilder() {
    }

    public static NationalServiceDTOBuilder aDumeResponse() {
        return new NationalServiceDTOBuilder();
    }

    public SNSearchResponseDTO build() {
        SNSearchResponseDTO dumeResponseDTO = new SNSearchResponseDTO();
        dumeResponseDTO.setResponse(new SNResponse());
        Map<String, SNSearchOEResponse> map = new HashMap<>();
        SNSearchOEResponse responseElement = new SNSearchOEResponse();
        SNDumeOEResponse snDumeOEResponse = new SNDumeOEResponse();
        snDumeOEResponse.setAccessible(accessible);
        snDumeOEResponse.setDumeOEId(snNumber);
        ;
        responseElement.setDumeOEResponse(snDumeOEResponse);
        map.put("1", responseElement);
        dumeResponseDTO.getResponse().setResponsesRecherche(map);
        dumeResponseDTO.getResponse().setDumeA(dumeA);
        return dumeResponseDTO;
    }

    public SNDumeSavedResponseDTO buildResponse() {
        SNDumeSavedResponseDTO dumeSavedResponseDTO = new SNDumeSavedResponseDTO();
        dumeSavedResponseDTO.setResponse(new SNResponse());
        dumeSavedResponseDTO.getResponse().setDumeA(dumeA);
        return dumeSavedResponseDTO;
    }

    public NationalServiceDTOBuilder withSNNumber(String snNumber, boolean accessible) {
        this.snNumber = snNumber;
        this.accessible = accessible;
        return this;
    }

    public NationalServiceDTOBuilder withDumeA(String dumeA) {
        this.dumeA = dumeA;
        return this;
    }
}
