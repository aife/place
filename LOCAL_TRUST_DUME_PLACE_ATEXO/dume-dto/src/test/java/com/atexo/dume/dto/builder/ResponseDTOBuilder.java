package com.atexo.dume.dto.builder;

import com.atexo.dume.dto.ResponseDTO;

import java.math.BigDecimal;
import java.time.LocalDate;

public final class ResponseDTOBuilder {
    private Long id;
    private Long idRequirement;
    private String description;
    private Boolean indicator;
    private String code;
    private BigDecimal amount;
    private LocalDate date;
    private BigDecimal percent;
    private BigDecimal quantity;
    private String evidenceUrl;
    private Long idRepeatableRequirementGroup;
    private Long instanceNumber;

    private ResponseDTOBuilder() {
    }

    public static ResponseDTOBuilder aResponseDTO() {
        return new ResponseDTOBuilder();
    }

    public ResponseDTOBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public ResponseDTOBuilder withIdRequirement(Long idRequirement) {
        this.idRequirement = idRequirement;
        return this;
    }

    public ResponseDTOBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public ResponseDTOBuilder withIndicator(Boolean indicator) {
        this.indicator = indicator;
        return this;
    }

    public ResponseDTOBuilder withCode(String code) {
        this.code = code;
        return this;
    }

    public ResponseDTOBuilder withAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public ResponseDTOBuilder withDate(LocalDate date) {
        this.date = date;
        return this;
    }

    public ResponseDTOBuilder withPercent(BigDecimal percent) {
        this.percent = percent;
        return this;
    }

    public ResponseDTOBuilder withQuantity(BigDecimal quantity) {
        this.quantity = quantity;
        return this;
    }

    public ResponseDTOBuilder withEvidenceUrl(String evidenceUrl) {
        this.evidenceUrl = evidenceUrl;
        return this;
    }

    public ResponseDTOBuilder withIdRepeatableRequirementGroup(Long idRepeatableRequirementGroup) {
        this.idRepeatableRequirementGroup = idRepeatableRequirementGroup;
        return this;
    }

    public ResponseDTOBuilder withInstanceNumber(Long instanceNumber) {
        this.instanceNumber = instanceNumber;
        return this;
    }

    public ResponseDTO build() {
        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setId(id);

        responseDTO.setDescription(description);
        responseDTO.setIndicator(indicator);
        responseDTO.setCode(code);
        responseDTO.setAmount(amount);
        responseDTO.setDate(date);
        responseDTO.setPercent(percent);
        responseDTO.setQuantity(quantity);
        responseDTO.setEvidenceUrl(evidenceUrl);
        return responseDTO;
    }
}
