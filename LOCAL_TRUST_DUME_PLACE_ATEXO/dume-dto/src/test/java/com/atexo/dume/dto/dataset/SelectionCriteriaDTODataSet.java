package com.atexo.dume.dto.dataset;

import com.atexo.dume.dto.RequirementGroupDTO;
import com.atexo.dume.dto.SelectionCriteriaDTO;
import com.atexo.dume.dto.builder.SelectionCriteriaDTOBuilder;

import java.util.ArrayList;
import java.util.List;

public class SelectionCriteriaDTODataSet {

    public static final Long SELECTION_CRITERIA_DTO_IDENTIFIER_1 = 10000L;
    public static final Long SELECTION_CRITERIA_DTO_IDENTIFIER_2 = 10001L;
    public static final String SELECTION_CRITERIA_DTO_UUID_1 = "7604bd40-4462-4086-8763-a50da51a869c";
    public static SelectionCriteriaDTO sample1WithId() {
        return SelectionCriteriaDTOBuilder.aSelectionCriteriaDTO()
                .withId(SELECTION_CRITERIA_DTO_IDENTIFIER_1)
                .withCode("CodeSelection1")
                .build();
    }

    public static SelectionCriteriaDTO sample1WithUuid() {
        return SelectionCriteriaDTOBuilder.aSelectionCriteriaDTO()
                                          .withId(SELECTION_CRITERIA_DTO_IDENTIFIER_1)
                                          .withCode("CodeSelection1")
                                          .withUuid(SELECTION_CRITERIA_DTO_UUID_1)
                                          .build();
    }

    public static SelectionCriteriaDTO sampleWithSimpleStructure() {
        List<RequirementGroupDTO> requirementGroups = new ArrayList<>();
        requirementGroups.add(RequirementGroupDTODataSet.sampleWithSimpleStructure());
        return SelectionCriteriaDTOBuilder.aSelectionCriteriaDTO()
                                          .withId(SELECTION_CRITERIA_DTO_IDENTIFIER_1)
                                          .withRequirementGroupList(requirementGroups)
                                          .build();
    }

    public static SelectionCriteriaDTO sample2WithId() {
        return SelectionCriteriaDTOBuilder.aSelectionCriteriaDTO()
                .withId(SELECTION_CRITERIA_DTO_IDENTIFIER_2)
                .withCode("CodeSelection2")
                .build();
    }

    public static SelectionCriteriaDTO sampleWithNestedStructure() {
        List<RequirementGroupDTO> requirementGroups = new ArrayList<>();
        requirementGroups.add(RequirementGroupDTODataSet.sampleWithNestedStructure());
        return SelectionCriteriaDTOBuilder.aSelectionCriteriaDTO()
                                          .withId(SELECTION_CRITERIA_DTO_IDENTIFIER_1)
                                          .withRequirementGroupList(requirementGroups)
                                          .build();
    }

    public static SelectionCriteriaDTO sampleWithRequirementGroupInstanceStructure() {
        List<RequirementGroupDTO> requirementGroups = new ArrayList<>();
        requirementGroups.add(RequirementGroupDTODataSet.sampleWithSimpleStructureInstance1());
        requirementGroups.add(RequirementGroupDTODataSet.sampleWithSimpleStructureInstance2());
        return SelectionCriteriaDTOBuilder.aSelectionCriteriaDTO()
                                          .withId(SELECTION_CRITERIA_DTO_IDENTIFIER_1)
                                          .withRequirementGroupList(requirementGroups)
                                          .build();
    }
}
