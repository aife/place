package com.atexo.dume.dto.builder;

import com.atexo.dume.dto.RequirementDTO;
import com.atexo.dume.dto.RequirementGroupDTO;

import java.util.List;

public final class RequirementGroupDTOBuilder {
    private Long id;
    private String uuid;
    private List<RequirementDTO> requirementList;
    private List<RequirementGroupDTO> subGroups;
    private Boolean fulfillmentIndicator;
    private Long instanceNumber;

    private RequirementGroupDTOBuilder() {
    }

    public static RequirementGroupDTOBuilder aRequirementGroupDTO() {
        return new RequirementGroupDTOBuilder();
    }

    public RequirementGroupDTOBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public RequirementGroupDTOBuilder withUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public RequirementGroupDTOBuilder withRequirementList(List<RequirementDTO> requirementList) {
        this.requirementList = requirementList;
        return this;
    }

    public RequirementGroupDTOBuilder withSubGroups(List<RequirementGroupDTO> subGroups) {
        this.subGroups = subGroups;
        return this;
    }

    public RequirementGroupDTOBuilder withFulfillmentIndicator(Boolean fulfillmentIndicator) {
        this.fulfillmentIndicator = fulfillmentIndicator;
        return this;
    }

    public RequirementGroupDTOBuilder withInstanceNumber(Long instanceNumber) {
        this.instanceNumber = instanceNumber;
        return this;
    }

    public RequirementGroupDTO build() {
        RequirementGroupDTO requirementGroupDTO = new RequirementGroupDTO();
        requirementGroupDTO.setId(id);
        requirementGroupDTO.setUuid(uuid);
        requirementGroupDTO.setRequirementList(requirementList);
        requirementGroupDTO.setSubGroups(subGroups);
        requirementGroupDTO.setFulfillmentIndicator(fulfillmentIndicator);
        requirementGroupDTO.setIdRequirementGroupResponse(instanceNumber);
        return requirementGroupDTO;
    }
}
