package com.atexo.dume.dto.builder;

import com.atexo.dume.dto.PublicationGroupementDTO;

import java.util.List;

public final class PublicationGroupementDTOBuilder {
    private String siretMandataire;
    private List<Long> idsContexteGroupement;

    private PublicationGroupementDTOBuilder() {
    }

    public static PublicationGroupementDTOBuilder aPublicationGroupementDTO() {
        return new PublicationGroupementDTOBuilder();
    }

    public PublicationGroupementDTOBuilder withSiretMandataire(String siretMandataire) {
        this.siretMandataire = siretMandataire;
        return this;
    }

    public PublicationGroupementDTOBuilder withIdsContexteGroupement(List<Long> idsContexteGroupement) {
        this.idsContexteGroupement = idsContexteGroupement;
        return this;
    }

    public PublicationGroupementDTO build() {
        PublicationGroupementDTO publicationGroupementDTO = new PublicationGroupementDTO(idsContexteGroupement, siretMandataire);
        return publicationGroupementDTO;
    }
}
