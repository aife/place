package com.atexo.dume.dto.builder;

import com.atexo.dume.dto.LotDTO;

public class LotDTOBuilder {

    private Long internalLotIdentifier;

    private String lotNumber;

    private String lotName;

    private LotDTOBuilder() {
    }

    public static LotDTOBuilder aLot() {
        return new LotDTOBuilder();
    }

    public LotDTOBuilder withInternalLotIdentifier(final Long internalLotIdentifier) {
        this.internalLotIdentifier = internalLotIdentifier;
        return this;
    }

    public LotDTOBuilder withLlotNumber(final String lotNumber) {
        this.lotNumber = lotNumber;
        return this;
    }

    public LotDTOBuilder withLotName(final String lotName) {
        this.lotName = lotName;
        return this;
    }


    public LotDTO build() {
        final LotDTO metaDataDTO = new LotDTO(internalLotIdentifier, lotNumber, lotName);
        return metaDataDTO;
    }

}
