package com.atexo.dume.dto.dataset;

import com.atexo.dume.dto.builder.NationalServiceDTOBuilder;
import com.atexo.dume.dto.sn.response.SNDumeSavedResponseDTO;
import com.atexo.dume.dto.sn.response.SNSearchResponseDTO;
import org.springframework.util.Base64Utils;

public class NationalServiceDTODataSet {

    public static final String NUMERO_SN_1 = "aZeRtY";
    public static final String DUME_A_CONTENT = "dumeADummyContent";

    public static SNSearchResponseDTO sampleWithAccesssible() {
        return NationalServiceDTOBuilder.aDumeResponse().withSNNumber(NUMERO_SN_1, true).build();
    }

    public static SNSearchResponseDTO sampleWithoutAccesssible() {
        return NationalServiceDTOBuilder.aDumeResponse().withSNNumber(NUMERO_SN_1, false).build();
    }

    public static SNDumeSavedResponseDTO sampleResponseWithDumeA() {
        return NationalServiceDTOBuilder.aDumeResponse().withDumeA(Base64Utils.encodeToString(DUME_A_CONTENT.getBytes())).buildResponse();
    }
}
