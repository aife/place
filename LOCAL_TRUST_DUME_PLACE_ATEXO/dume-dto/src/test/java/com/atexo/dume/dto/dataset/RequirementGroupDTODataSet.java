package com.atexo.dume.dto.dataset;


import com.atexo.dume.dto.RequirementDTO;
import com.atexo.dume.dto.RequirementGroupDTO;
import com.atexo.dume.dto.builder.RequirementGroupDTOBuilder;

import java.util.ArrayList;
import java.util.List;

public class RequirementGroupDTODataSet {

    public static final Long SIMPLE_REQUIREMENT_GROUP_IDENTIFIER_1 = 300L;
    public static final Long SIMPLE_REQUIREMENT_GROUP_INSTANCE_NUMBER_1 = 0L;
    public static final Long SIMPLE_REQUIREMENT_GROUP_INSTANCE_NUMBER_2 = 1L;


    public static RequirementGroupDTO sampleWithId() {
        return RequirementGroupDTOBuilder.aRequirementGroupDTO()
                                         .withId(SIMPLE_REQUIREMENT_GROUP_IDENTIFIER_1)
                                         .build();
    }

    public static RequirementGroupDTO sampleWithSimpleStructure() {
        List<RequirementDTO> requirements = new ArrayList<>();
        requirements.add(RequirementDTODataSet.sampleWithId());
        return RequirementGroupDTOBuilder.aRequirementGroupDTO()
                                         .withId(SIMPLE_REQUIREMENT_GROUP_IDENTIFIER_1)
                                         .withRequirementList(requirements)
                                         .build();
    }

    public static RequirementGroupDTO sampleWithoutId() {
        return RequirementGroupDTOBuilder.aRequirementGroupDTO()
                                         .build();
    }

    public static RequirementGroupDTO sampleWithNestedStructure() {
        List<RequirementDTO> requirements = new ArrayList<>();
        requirements.add(RequirementDTODataSet.sample2WithId());
        List<RequirementGroupDTO> subGroups = new ArrayList<>();
        subGroups.add(sampleWithSimpleStructure());
        return RequirementGroupDTOBuilder.aRequirementGroupDTO()
                                         .withId(SIMPLE_REQUIREMENT_GROUP_IDENTIFIER_1)
                                         .withRequirementList(requirements)
                                         .withSubGroups(subGroups)
                                         .build();
    }

    public static RequirementGroupDTO sampleWithSimpleStructureInstance1() {
        List<RequirementDTO> requirements = new ArrayList<>();
        requirements.add(RequirementDTODataSet.sampleWithId());
        return RequirementGroupDTOBuilder.aRequirementGroupDTO()
                                         .withId(SIMPLE_REQUIREMENT_GROUP_IDENTIFIER_1)
                                         .withInstanceNumber(SIMPLE_REQUIREMENT_GROUP_INSTANCE_NUMBER_1)
                                         .withRequirementList(requirements)
                                         .build();
    }

    public static RequirementGroupDTO sampleWithSimpleStructureInstance2() {
        List<RequirementDTO> requirements = new ArrayList<>();
        requirements.add(RequirementDTODataSet.sampleWithId());
        return RequirementGroupDTOBuilder.aRequirementGroupDTO()
                                         .withId(SIMPLE_REQUIREMENT_GROUP_IDENTIFIER_1)
                                         .withInstanceNumber(SIMPLE_REQUIREMENT_GROUP_INSTANCE_NUMBER_2)
                                         .withRequirementList(requirements)
                                         .build();
    }
}
