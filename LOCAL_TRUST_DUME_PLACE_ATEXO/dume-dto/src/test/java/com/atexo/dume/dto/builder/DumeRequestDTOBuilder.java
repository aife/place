package com.atexo.dume.dto.builder;

import com.atexo.dume.dto.CriteriaLotDTO;
import com.atexo.dume.dto.DumeRequestDTO;

import java.util.List;

public final class DumeRequestDTOBuilder {
    private Long id;
    private List<CriteriaLotDTO> selectionCriteriaCodes;
    private List<String> exclusionCriteriaCodes;

    private DumeRequestDTOBuilder() {
    }

    public static DumeRequestDTOBuilder aDumeRequestDTO() {
        return new DumeRequestDTOBuilder();
    }

    public DumeRequestDTOBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public DumeRequestDTOBuilder withSelectionCriteriaCodes(List<CriteriaLotDTO> selectionCriteriaDTOS) {
        this.selectionCriteriaCodes = selectionCriteriaDTOS;
        return this;
    }

    public DumeRequestDTOBuilder withExclusionCriteriaCodes(List<String> exclusionCriteriaDTOS) {
        this.exclusionCriteriaCodes = exclusionCriteriaDTOS;
        return this;
    }

    public DumeRequestDTO build() {
        DumeRequestDTO dumeRequestDTO = new DumeRequestDTO();
        dumeRequestDTO.setId(id);
        dumeRequestDTO.setSelectionCriteriaCodes(selectionCriteriaCodes);
        dumeRequestDTO.setExclusionCriteriaCodes(exclusionCriteriaCodes);
        return dumeRequestDTO;
    }
}
