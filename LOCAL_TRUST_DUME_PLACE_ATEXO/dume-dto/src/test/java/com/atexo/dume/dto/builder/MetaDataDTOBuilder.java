package com.atexo.dume.dto.builder;

import com.atexo.dume.dto.MetaDataDTO;

import java.time.ZonedDateTime;

public class MetaDataDTOBuilder {

    private ZonedDateTime bidClosingDate;
    private String platformIdentifier;
    private String organization;
    private Long consultationIdentifier;
    private String siret;
    private Long userInscriptionIdentifier;

    private MetaDataDTOBuilder() {
    }

    public static MetaDataDTOBuilder aMetaData() {
        return new MetaDataDTOBuilder();
    }

    public MetaDataDTOBuilder withBidClosingDate(final ZonedDateTime bidClosingDate) {
        this.bidClosingDate = bidClosingDate;
        return this;
    }

    public MetaDataDTOBuilder withPlatformIdentifier(final String platformIdentifier) {
        this.platformIdentifier = platformIdentifier;
        return this;
    }

    public MetaDataDTOBuilder withOrganization(final String organization) {
        this.organization = organization;
        return this;
    }

    public MetaDataDTOBuilder withConsultationIdentifier(final Long consultationIdentifier) {
        this.consultationIdentifier = consultationIdentifier;
        return this;
    }

    public MetaDataDTOBuilder withSiret(final String siret) {
        this.siret = siret;
        return this;
    }

    public MetaDataDTOBuilder withSerInscriptionIdentifier(final Long userInscriptionIdentifier) {
        this.userInscriptionIdentifier = userInscriptionIdentifier;
        return this;
    }

    public MetaDataDTO build() {
        final MetaDataDTO metaDataDTO = new MetaDataDTO(bidClosingDate, platformIdentifier, organization, consultationIdentifier, siret, userInscriptionIdentifier);
        return metaDataDTO;
    }
}
