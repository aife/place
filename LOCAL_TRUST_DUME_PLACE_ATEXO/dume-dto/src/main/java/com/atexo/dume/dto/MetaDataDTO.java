package com.atexo.dume.dto;

import com.atexo.dume.toolbox.serialization.ZonedDateTimeDeserialize;
import com.atexo.dume.toolbox.serialization.ZonedDateTimeSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * This DTO will encapsulate all ContextRequestMetaData
 */
@JsonIgnoreProperties
public class MetaDataDTO implements Serializable {

    @JsonSerialize(using = ZonedDateTimeSerializer.class)
    @JsonDeserialize(using = ZonedDateTimeDeserialize.class)
    @JsonProperty("dateLimiteRemisePlis")
    private ZonedDateTime bidClosingDate;

    @JsonProperty("identifiantPlateforme")
    private String platformIdentifier;

    @JsonProperty("organisme")
    private String organization;

    @JsonProperty("idConsultation")
    private Long consultationIdentifier;

    @JsonProperty("siret")
    private String siret;

    @JsonProperty("idInscrit")
    private Long userInscriptionIdentifier;

    private String idContextDumeAcheteur;

    @JsonProperty("idTypeProcedure")
    private String procedureTypeIdentifier;

    // Le code où meurt cette verveine
    // d'un coup d'ahmed fut fêlé
    // le coup dut l'effleurer à peine
    // n'y touchez pas, il est inversé
    //
    // Les deux propriétés suivantes sont inversées, mais elles le sont aussi côté MPE !
    // Donc les deux erreurs s'annulent. Impossible de corriger ça sans compromettre la rétrocompatibilité.
    // Conclusion : à laisser comme ça.
    @JsonProperty("idTypeMarche")
    private String natureMarketIdentifier;

    @JsonProperty("idNatureMarket")
    private String marketTypeIdentifier;

    @JsonProperty("typeOE")
    private String typeOE;

    @JsonProperty("first")
    private Boolean first;

    @JsonProperty("simplifie")
    private Boolean simplified;

    public MetaDataDTO() {
    }

    public MetaDataDTO(final ZonedDateTime bidClosingDate, final String platformIdentifier, final String organization, final Long consultationIdentifier, final String siret, final Long userInscriptionIdentifier) {
        this.bidClosingDate = bidClosingDate;
        this.platformIdentifier = platformIdentifier;
        this.organization = organization;
        this.consultationIdentifier = consultationIdentifier;
        this.siret = siret;
        this.userInscriptionIdentifier = userInscriptionIdentifier;
    }

    public ZonedDateTime getBidClosingDate() {
        return bidClosingDate;
    }

    public void setBidClosingDate(final ZonedDateTime bidClosingDate) {
        this.bidClosingDate = bidClosingDate;
    }

    public String getPlatformIdentifier() {
        return platformIdentifier;
    }

    public void setPlatformIdentifier(final String platformIdentifier) {
        this.platformIdentifier = platformIdentifier;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(final String organization) {
        this.organization = organization;
    }

    public Long getConsultationIdentifier() {
        return consultationIdentifier;
    }

    public void setConsultationIdentifier(final Long consultationIdentifier) {
        this.consultationIdentifier = consultationIdentifier;
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(final String siret) {
        this.siret = siret;
    }

    public Long getUserInscriptionIdentifier() {
        return userInscriptionIdentifier;
    }

    public void setUserInscriptionIdentifier(final Long userInscriptionIdentifier) {
        this.userInscriptionIdentifier = userInscriptionIdentifier;
    }

    public String getIdContextDumeAcheteur() {
        return idContextDumeAcheteur;
    }

    public void setIdContextDumeAcheteur(String idContextDumeAcheteur) {
        this.idContextDumeAcheteur = idContextDumeAcheteur;
    }

    public String getProcedureTypeIdentifier() {
        return procedureTypeIdentifier;
    }

    public void setProcedureTypeIdentifier(String procedureTypeIdentifier) {
        this.procedureTypeIdentifier = procedureTypeIdentifier;
    }

    public String getNatureMarketIdentifier() {
        return natureMarketIdentifier;
    }

    public void setNatureMarketIdentifier(String natureMarketIdentifier) {
        this.natureMarketIdentifier = natureMarketIdentifier;
    }

    public String getMarketTypeIdentifier() {
        return marketTypeIdentifier;
    }

    public void setMarketTypeIdentifier(String marketTypeIdentifier) {
        this.marketTypeIdentifier = marketTypeIdentifier;
    }

    public String getTypeOE() {
        return typeOE;
    }

    public void setTypeOE(String typeOE) {
        this.typeOE = typeOE;
    }

    public Boolean getFirst() {
        return first;
    }

    public void setFirst(Boolean first) {
        this.first = first;
    }

    public Boolean getSimplified() {
        return simplified;
    }

    public void setSimplified(Boolean simplified) {
        this.simplified = simplified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MetaDataDTO)) {
            return false;
        }

        MetaDataDTO that = (MetaDataDTO) o;

        if (bidClosingDate != null ? !bidClosingDate.equals(that.bidClosingDate) : that.bidClosingDate != null) {
            return false;
        }
        if (platformIdentifier != null ? !platformIdentifier.equals(that.platformIdentifier) : that.platformIdentifier != null) {
            return false;
        }
        if (organization != null ? !organization.equals(that.organization) : that.organization != null) {
            return false;
        }
        if (consultationIdentifier != null ? !consultationIdentifier.equals(that.consultationIdentifier) : that.consultationIdentifier != null) {
            return false;
        }
        if (siret != null ? !siret.equals(that.siret) : that.siret != null) {
            return false;
        }
        if (userInscriptionIdentifier != null ? !userInscriptionIdentifier.equals(that.userInscriptionIdentifier) : that.userInscriptionIdentifier != null) {
            return false;
        }
        if (idContextDumeAcheteur != null ? !idContextDumeAcheteur.equals(that.idContextDumeAcheteur) : that.idContextDumeAcheteur != null) {
            return false;
        }
        if (procedureTypeIdentifier != null ? !procedureTypeIdentifier.equals(that.procedureTypeIdentifier) : that.procedureTypeIdentifier != null) {
            return false;
        }
        if (natureMarketIdentifier != null ? !natureMarketIdentifier.equals(that.natureMarketIdentifier) : that.natureMarketIdentifier != null) {
            return false;
        }
        if (typeOE != null ? !typeOE.equals(that.typeOE) : that.typeOE != null) {
            return false;
        }
        if (first != null ? !first.equals(that.first) : that.first != null) {
            return false;
        }
        if (simplified != null ? !simplified.equals(that.simplified) : that.simplified != null) {
            return false;
        }

        return marketTypeIdentifier != null ? marketTypeIdentifier.equals(that.marketTypeIdentifier) : that.marketTypeIdentifier == null;
    }

    @Override
    public int hashCode() {
        int result = bidClosingDate != null ? bidClosingDate.hashCode() : 0;
        result = 31 * result + (platformIdentifier != null ? platformIdentifier.hashCode() : 0);
        result = 31 * result + (organization != null ? organization.hashCode() : 0);
        result = 31 * result + (consultationIdentifier != null ? consultationIdentifier.hashCode() : 0);
        result = 31 * result + (siret != null ? siret.hashCode() : 0);
        result = 31 * result + (userInscriptionIdentifier != null ? userInscriptionIdentifier.hashCode() : 0);
        result = 31 * result + (idContextDumeAcheteur != null ? idContextDumeAcheteur.hashCode() : 0);
        result = 31 * result + (procedureTypeIdentifier != null ? procedureTypeIdentifier.hashCode() : 0);
        result = 31 * result + (natureMarketIdentifier != null ? natureMarketIdentifier.hashCode() : 0);
        result = 31 * result + (marketTypeIdentifier != null ? marketTypeIdentifier.hashCode() : 0);
        result = 31 * result + (typeOE != null ? typeOE.hashCode() : 0);
        result = 31 * result + (first != null ? first.hashCode() : 0);
        result = 31 * result + (simplified != null ? simplified.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MetaDataDTO{" +
                "bidClosingDate=" + bidClosingDate +
                ", platformIdentifier='" + platformIdentifier + '\'' +
                ", organization='" + organization + '\'' +
                ", consultationIdentifier=" + consultationIdentifier +
                ", siret='" + siret + '\'' +
                ", userInscriptionIdentifier=" + userInscriptionIdentifier +
                ", idContextDumeAcheteur='" + idContextDumeAcheteur + '\'' +
                ", procedureTypeIdentifier='" + procedureTypeIdentifier + '\'' +
                ", natureMarketIdentifier='" + natureMarketIdentifier + '\'' +
                ", first='" + first + '\'' + ", simplified='" + simplified + '\'' +
                ", marketTypeIdentifier='" + marketTypeIdentifier + '\'' + ", typeOE='" + typeOE + '\'' +
                '}';
    }
}
