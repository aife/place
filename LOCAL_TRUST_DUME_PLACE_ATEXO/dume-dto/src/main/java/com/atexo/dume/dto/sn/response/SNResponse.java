package com.atexo.dume.dto.sn.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"idDume", "statutDume", "dumeA", "messageList"})
public class SNResponse {

    @JsonProperty("idDume")
    private String idDume;

    @JsonProperty("statutDume")
    private String statutDume;

    @JsonProperty("messageList")
    @Valid
    private List<SNMessageList> messageList = new ArrayList<>();

    @JsonProperty("dumeA")
    private String dumeA;

    @JsonProperty("dumeOE")
    private String dumeOE;

    @JsonProperty("model")
    private String model;

    @JsonProperty("responsesRecherche")
    private Map<String, SNSearchOEResponse> responsesRecherche;

    @JsonProperty("refFonctionnelleDE")
    private String refFonctionnelleDE;

    @JsonProperty("Criterions et preuves")
    private List<SNCriterionsAndProof> criterionsAndProofs;

    @JsonProperty("dumeA")
    public String getDumeA() {
        return dumeA;
    }

    @JsonProperty("dumeA")
    public void setDumeA(String dumeA) {
        this.dumeA = dumeA;
    }

    public String getIdDume() {
        return idDume;
    }

    public void setIdDume(String idDume) {
        this.idDume = idDume;
    }

    public String getStatutDume() {
        return statutDume;
    }

    public void setStatutDume(String statutDume) {
        this.statutDume = statutDume;
    }

    public String getDumeOE() {
        return dumeOE;
    }

    public void setDumeOE(String dumeOE) {
        this.dumeOE = dumeOE;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @JsonProperty("responsesRecherche")
    public Map<String, SNSearchOEResponse> getResponsesRecherche() {
        return responsesRecherche;
    }

    @JsonProperty("responsesRecherche")
    public void setResponsesRecherche(Map<String, SNSearchOEResponse> responsesRecherche) {
        this.responsesRecherche = responsesRecherche;
    }

    @JsonProperty("messageList")
    public List<SNMessageList> getMessageList() {
        return messageList;
    }

    @JsonProperty("messageList")
    public void setMessageList(List<SNMessageList> messageList) {
        this.messageList = messageList;
    }

    @JsonProperty("refFonctionnelleDE")
    public String getRefFonctionnelleDE() {
        return refFonctionnelleDE;
    }

    @JsonProperty("refFonctionnelleDE")
    public void setRefFonctionnelleDE(String refFonctionnelleDE) {
        this.refFonctionnelleDE = refFonctionnelleDE;
    }

    @JsonProperty("Criterions et preuves")
    public List<SNCriterionsAndProof> getCriterionsAndProofs() {
        return criterionsAndProofs;
    }

    @JsonProperty("Criterions et preuves")
    public void setCriterionsAndProofs(List<SNCriterionsAndProof> criterionsAndProofs) {
        this.criterionsAndProofs = criterionsAndProofs;
    }
}
