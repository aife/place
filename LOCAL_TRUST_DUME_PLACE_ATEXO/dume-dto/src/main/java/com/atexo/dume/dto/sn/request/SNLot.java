package com.atexo.dume.dto.sn.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"numeroLot", "libelle"})
public class SNLot {

    @JsonProperty("numeroLot")
    private String numeroLot;
    @JsonProperty("libelle")
    private String libelle;
    @JsonProperty("numeroLot")
    public String getNumeroLot() {
        return numeroLot;
    }

    @JsonProperty("numeroLot")
    public void setNumeroLot(String numeroLot) {
        this.numeroLot = numeroLot;
    }

    @JsonProperty("libelle")
    public String getLibelle() {
        return libelle;
    }

    @JsonProperty("libelle")
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
