package com.atexo.dume.dto.sn.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"typeIdPlateforme", "idPlateforme", "idTechniquePlateforme"})
public class SNPlateforme {

    @JsonProperty("typeIdPlateforme")
    private String typeIdPlateforme;
    @JsonProperty("idPlateforme")
    private String idPlateforme;
    @JsonProperty("idTechniquePlateforme")
    private String idTechniquePlateforme;

    @JsonProperty("typeIdPlateforme")
    public String getTypeIdPlateforme() {
        return typeIdPlateforme;
    }

    @JsonProperty("typeIdPlateforme")
    public void setTypeIdPlateforme(String typeIdPlateforme) {
        this.typeIdPlateforme = typeIdPlateforme;
    }

    @JsonProperty("idPlateforme")
    public String getIdPlateforme() {
        return idPlateforme;
    }

    @JsonProperty("idPlateforme")
    public void setIdPlateforme(String idPlateforme) {
        this.idPlateforme = idPlateforme;
    }

    @JsonProperty("idTechniquePlateforme")
    public String getIdTechniquePlateforme() {
        return idTechniquePlateforme;
    }

    @JsonProperty("idTechniquePlateforme")
    public void setIdTechniquePlateforme(String idTechniquePlateforme) {
        this.idTechniquePlateforme = idTechniquePlateforme;
    }

}
