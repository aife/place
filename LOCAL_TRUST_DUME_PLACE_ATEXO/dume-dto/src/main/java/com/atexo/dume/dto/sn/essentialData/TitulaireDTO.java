package com.atexo.dume.dto.sn.essentialData;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class TitulaireDTO {

    private String typeIdentifiant;
    private String id;
    private String denominationSociale;

    public String getTypeIdentifiant() {
        return typeIdentifiant;
    }

    public void setTypeIdentifiant(String value) {
        this.typeIdentifiant = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String value) {
        this.id = value;
    }

    public String getDenominationSociale() {
        return denominationSociale;
    }

    public void setDenominationSociale(String value) {
        this.denominationSociale = value;
    }

}
