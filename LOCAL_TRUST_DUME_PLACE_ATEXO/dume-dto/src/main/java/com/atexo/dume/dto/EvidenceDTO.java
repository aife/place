
package com.atexo.dume.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EvidenceDTO {

    @JsonProperty("uuid")
    private String uuid;
    @JsonProperty("texte")
    private String texte;
    @JsonProperty("Publié par ")
    private String publiPar;

    @JsonProperty("uuid")
    public String getUuid() {
        return uuid;
    }

    @JsonProperty("uuid")
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @JsonProperty("texte")
    public String getTexte() {
        return texte;
    }

    @JsonProperty("texte")
    public void setTexte(String texte) {
        this.texte = texte;
    }

    @JsonProperty("publiPar")
    public String getPubliPar() {
        return publiPar;
    }

    @JsonProperty("publiPar")
    public void setPubliPar(String publiPar) {
        this.publiPar = publiPar;
    }

}
