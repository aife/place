package com.atexo.dume.dto;

public enum StatusDTO {
    VALIDE, // Utilisé par les WS de validation
    NON_VALIDE, // Utilisé par les WS de validation
    ERREUR_SN, // Gestion des erreurs
    OK, // Utilisé par les WS de publication
    NON_PUBLIE,
    SN_DOWN,
    NOT_FOUND,
    PURGE,
    PUBLIE,
    MISE_A_JOUR,
    NOT_PURGE
}
