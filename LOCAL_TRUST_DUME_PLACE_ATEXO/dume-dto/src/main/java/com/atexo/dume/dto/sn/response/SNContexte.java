
package com.atexo.dume.dto.sn.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "langue",
    "uuid",
    "nationalentity"
})
public class SNContexte {

    @JsonProperty("langue")
    private String langue;
    @JsonProperty("uuid")
    private String uuid;
    @JsonProperty("nationalentity")
    private String nationalentity;

    @JsonProperty("langue")
    public String getLangue() {
        return langue;
    }

    @JsonProperty("langue")
    public void setLangue(String langue) {
        this.langue = langue;
    }

    @JsonProperty("uuid")
    public String getUuid() {
        return uuid;
    }

    @JsonProperty("uuid")
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @JsonProperty("nationalentity")
    public String getNationalentity() {
        return nationalentity;
    }

    @JsonProperty("nationalentity")
    public void setNationalentity(String nationalentity) {
        this.nationalentity = nationalentity;
    }

}
