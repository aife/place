package com.atexo.dume.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

import static com.atexo.dume.toolbox.consts.DumeJsonConstants.IDENTIFIAN_INTERNE_DUME;
import static com.atexo.dume.toolbox.consts.DumeJsonConstants.INTILULE_LOT;
import static com.atexo.dume.toolbox.consts.DumeJsonConstants.NUMERO_LOT;


@JsonIgnoreProperties
public class LotDTO implements Serializable {

    @JsonProperty(IDENTIFIAN_INTERNE_DUME)
    private Long internalLotIdentifier;

    @JsonProperty(NUMERO_LOT)
    private String lotNumber;

    @JsonProperty(INTILULE_LOT)
    private String lotName;

    public LotDTO() {
    }

    public LotDTO(String lotNumber, String lotName) {
        this.lotNumber = lotNumber;
        this.lotName = lotName;
    }

    public LotDTO(Long internalLotIdentifier, String lotNumber, String lotName) {
        this.internalLotIdentifier = internalLotIdentifier;
        this.lotNumber = lotNumber;
        this.lotName = lotName;
    }

    public Long getInternalLotIdentifier() {
        return internalLotIdentifier;
    }

    public void setInternalLotIdentifier(Long internalLotIdentifier) {
        this.internalLotIdentifier = internalLotIdentifier;
    }

    public String getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }

    public String getLotName() {
        return lotName;
    }

    public void setLotName(String lotName) {
        this.lotName = lotName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LotDTO)) {
            return false;
        }

        LotDTO lotDTO = (LotDTO) o;

        if (internalLotIdentifier != null ? !internalLotIdentifier.equals(lotDTO.internalLotIdentifier) : lotDTO.internalLotIdentifier != null) {
            return false;
        }
        if (lotNumber != null ? !lotNumber.equals(lotDTO.lotNumber) : lotDTO.lotNumber != null) {
            return false;
        }
        return lotName != null ? lotName.equals(lotDTO.lotName) : lotDTO.lotName == null;
    }

    @Override
    public int hashCode() {
        int result = internalLotIdentifier != null ? internalLotIdentifier.hashCode() : 0;
        result = 31 * result + (lotNumber != null ? lotNumber.hashCode() : 0);
        result = 31 * result + (lotName != null ? lotName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LotDTO{" +
                "internalLotIdentifier=" + internalLotIdentifier +
                ", lotNumber='" + lotNumber + '\'' +
                ", lotName='" + lotName + '\'' +
                '}';
    }
}
