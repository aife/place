package com.atexo.dume.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.atexo.dume.toolbox.consts.DumeJsonConstants.IDCONTEXT_OE;
import static com.atexo.dume.toolbox.consts.DumeJsonConstants.NUMERO_SN;

/**
 * Permet de retourner le retour de la création d'un dume OE : son numéro SN et son numéro de contexte au sein de
 * LT-DUME
 */
public class DumeResponseRetourCreationDTO {

    @JsonProperty(NUMERO_SN)
    private String numeroSN;

    @JsonProperty(IDCONTEXT_OE)
    private Long idContexteOE;

    public String getNumeroSN() {
        return numeroSN;
    }

    public void setNumeroSN(String numeroSN) {
        this.numeroSN = numeroSN;
    }

    public Long getIdContexteOE() {
        return idContexteOE;
    }

    public void setIdContexteOE(Long idContexteOE) {
        this.idContexteOE = idContexteOE;
    }

    public DumeResponseRetourCreationDTO(String numeroSN, Long idContexteOE) {
        this.numeroSN = numeroSN;
        this.idContexteOE = idContexteOE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DumeResponseRetourCreationDTO)) {
            return false;
        }

        DumeResponseRetourCreationDTO that = (DumeResponseRetourCreationDTO) o;

        if (numeroSN != null ? !numeroSN.equals(that.numeroSN) : that.numeroSN != null) {
            return false;
        }
        return idContexteOE != null ? idContexteOE.equals(that.idContexteOE) : that.idContexteOE == null;
    }

    @Override
    public int hashCode() {
        int result = numeroSN != null ? numeroSN.hashCode() : 0;
        result = 31 * result + (idContexteOE != null ? idContexteOE.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DumeResponseRetourCreationDTO{" +
                "numeroSN='" + numeroSN + '\'' +
                ", idContexteOE=" + idContexteOE +
                '}';
    }
}
