package com.atexo.dume.dto;

import java.io.Serializable;

public class CriteriaCategorieDTO implements Serializable {

    private String description;

    private String code;

    private String titre;

    private String type;

    private String order;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CriteriaCategorieDTO)) {
            return false;
        }

        CriteriaCategorieDTO that = (CriteriaCategorieDTO) o;

        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        if (code != null ? !code.equals(that.code) : that.code != null) {
            return false;
        }
        if (titre != null ? !titre.equals(that.titre) : that.titre != null) {
            return false;
        }
        if (type != null ? !type.equals(that.type) : that.type != null) {
            return false;
        }
        return order != null ? order.equals(that.order) : that.order == null;
    }

    @Override
    public int hashCode() {
        int result = description != null ? description.hashCode() : 0;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (titre != null ? titre.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (order != null ? order.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CriteriaCategorieDTO{" +
                "description='" + description + '\'' +
                ", code='" + code + '\'' +
                ", titre='" + titre + '\'' +
                ", type='" + type + '\'' +
                ", order='" + order + '\'' +
                '}';
    }
}
