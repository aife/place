package com.atexo.dume.dto.sn.essentialData;

public class ConsultationDTO {

    private String id;
    private String numero;

    public ConsultationDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
}
