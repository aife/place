package com.atexo.dume.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import static com.atexo.dume.toolbox.consts.DumeJsonConstants.*;

public class PublicationStatus {

    @JsonProperty(STATUS)
    private StatusDTO status;

    @JsonProperty(NUMEROS_DUME)
    private List<PublishedDume> publishedDumeList;

    @JsonProperty(IS_STANDARD)
    private Boolean standard;

    @JsonProperty(MESSAGE_ERREUR)
    private String message;

    public PublicationStatus() {
    }

    public PublicationStatus(StatusDTO statut, List<PublishedDume> publishedDumeList) {
        this.status = statut;
        this.publishedDumeList = publishedDumeList;
    }

    public StatusDTO getStatut() {
        return status;
    }

    public void setStatut(StatusDTO status) {
        this.status = status;
    }

    public List<PublishedDume> getPublishedDumeList() {
        return publishedDumeList;
    }

    public void setPublishedDumeList(List<PublishedDume> publishedDumeList) {
        this.publishedDumeList = publishedDumeList;
    }

    public Boolean getStandard() {
        return standard;
    }

    public void setStandard(Boolean standard) {
        this.standard = standard;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PublicationStatus that = (PublicationStatus) o;

        if (status != null ? !status.equals(that.status) : that.status != null) {
            return false;
        }
        if (standard != null ? !standard.equals(that.standard) : that.standard != null) {
            return false;
        }
        return publishedDumeList != null ? publishedDumeList.equals(that.publishedDumeList) : that.publishedDumeList == null;
    }

    @Override
    public int hashCode() {
        int result = status != null ? status.hashCode() : 0;
        result = 31 * result + (publishedDumeList != null ? publishedDumeList.hashCode() : 0);
        result = 31 * result + (standard != null ? standard.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PublicationStatus{" +
                "statut=" + status +
                ", publishedDumeList=" + publishedDumeList +
                ", standard=" + standard +
                ", message=" + message +
                '}';
    }
}
