package com.atexo.dume.dto.sn.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SNECertisResponseDTO {

    @JsonProperty("response")
    SNResponse response;

    @JsonProperty("response")
    public SNResponse getResponse() {
        return response;
    }

    @JsonProperty("response")
    public void setResponse(SNResponse response) {
        this.response = response;
    }
}
