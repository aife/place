package com.atexo.dume.dto.sn.essentialData;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class LieuExecutionDTO {

    private String code;
    private String typeCode;
    private String nom;

    public String getCode() {
        return code;
    }

    public void setCode(String value) {
        this.code = value;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String value) {
        this.typeCode = value;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String value) {
        this.nom = value;
    }

}
