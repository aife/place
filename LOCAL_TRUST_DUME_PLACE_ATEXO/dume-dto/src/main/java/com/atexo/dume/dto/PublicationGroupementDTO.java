package com.atexo.dume.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import static com.atexo.dume.toolbox.consts.DumeJsonConstants.IDS_CONTEXTE_GROUPEMENT;
import static com.atexo.dume.toolbox.consts.DumeJsonConstants.SIRET_MANDATAIRE;

public class PublicationGroupementDTO {

    @JsonProperty(IDS_CONTEXTE_GROUPEMENT)
    private List<Long> idsContexteGroupement;

    @JsonProperty(SIRET_MANDATAIRE)
    private String siretMandataire;

    public PublicationGroupementDTO() {
    }

    public PublicationGroupementDTO(List<Long> idsContexteGroupement, String siretMandataire) {
        this.idsContexteGroupement = idsContexteGroupement;
        this.siretMandataire = siretMandataire;
    }

    public List<Long> getIdsContexteGroupement() {
        return idsContexteGroupement;
    }

    public void setIdsContexteGroupement(List<Long> idsContexteGroupement) {
        this.idsContexteGroupement = idsContexteGroupement;
    }

    public String getSiretMandataire() {
        return siretMandataire;
    }

    public void setSiretMandataire(String siretMandataire) {
        this.siretMandataire = siretMandataire;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PublicationGroupementDTO)) {
            return false;
        }

        PublicationGroupementDTO that = (PublicationGroupementDTO) o;

        if (idsContexteGroupement != null ? !idsContexteGroupement.equals(that.idsContexteGroupement) : that.idsContexteGroupement != null) {
            return false;
        }
        return siretMandataire != null ? siretMandataire.equals(that.siretMandataire) : that.siretMandataire == null;
    }

    @Override
    public int hashCode() {
        int result = idsContexteGroupement != null ? idsContexteGroupement.hashCode() : 0;
        result = 31 * result + (siretMandataire != null ? siretMandataire.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PublicationGroupementDTO{" +
                "idsContexteGroupement=" + idsContexteGroupement +
                ", siretMandataire='" + siretMandataire + '\'' +
                '}';
    }
}
