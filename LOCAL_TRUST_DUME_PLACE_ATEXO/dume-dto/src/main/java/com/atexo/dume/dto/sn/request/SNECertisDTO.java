package com.atexo.dume.dto.sn.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class SNECertisDTO extends SNRequestDTO {

    @JsonProperty("criterion")
    private String criterion;

    @JsonProperty("langue")
    private String langue;

    @JsonProperty("nationalEntities")
    private String nationalEntities;

    public String getCriterion() {
        return criterion;
    }

    public void setCriterion(String criterion) {
        this.criterion = criterion;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(String langue) {
        this.langue = langue;
    }

    public String getNationalEntities() {
        return nationalEntities;
    }

    public void setNationalEntities(String nationalEntities) {
        this.nationalEntities = nationalEntities;
    }
}
