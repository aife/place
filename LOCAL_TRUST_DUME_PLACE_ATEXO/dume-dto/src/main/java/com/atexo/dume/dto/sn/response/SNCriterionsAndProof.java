
package com.atexo.dume.dto.sn.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "criterion",
    "evidences"
})
public class SNCriterionsAndProof {

    @JsonProperty("criterion")
    private SNCriterion criterion;
    @JsonProperty("evidences")
    private List<SNEvidence> evidences = null;

    @JsonProperty("criterion")
    public SNCriterion getCriterion() {
        return criterion;
    }

    @JsonProperty("criterion")
    public void setCriterion(SNCriterion criterion) {
        this.criterion = criterion;
    }

    @JsonProperty("evidences")
    public List<SNEvidence> getEvidences() {
        return evidences;
    }

    @JsonProperty("evidences")
    public void setEvidences(List<SNEvidence> evidences) {
        this.evidences = evidences;
    }

}
