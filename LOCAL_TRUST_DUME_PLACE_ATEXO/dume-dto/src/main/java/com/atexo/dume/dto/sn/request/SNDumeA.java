package com.atexo.dume.dto.sn.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"xmlDume", "idDume"})
public class SNDumeA {

    @JsonProperty("xmlDume")
    private String xmlDume;
    @JsonProperty("idDume")
    private String idDume;

    public SNDumeA() {
    }

    public SNDumeA(String xmlDume) {
        this.xmlDume = xmlDume;
    }

    @JsonProperty("xmlDume")
    public String getXmlDume() {
        return xmlDume;
    }

    @JsonProperty("xmlDume")
    public void setXmlDume(String xmlDume) {
        this.xmlDume = xmlDume;
    }

    @JsonProperty("idDume")
    public String getIdDume() {
        return idDume;
    }

    @JsonProperty("idDume")
    public void setIdDume(String idDume) {
        this.idDume = idDume;
    }
}
