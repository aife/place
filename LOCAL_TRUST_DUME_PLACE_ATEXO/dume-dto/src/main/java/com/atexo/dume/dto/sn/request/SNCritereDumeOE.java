package com.atexo.dume.dto.sn.request;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class SNCritereDumeOE {

    private String idDume;
    private String idStatutDume;
    private String identifiantOE;

    public SNCritereDumeOE() {
        // constructeur par défaut
    }

    public String getIdDume() {
        return idDume;
    }

    public void setIdDume(String idDume) {
        this.idDume = idDume;
    }

    public String getIdStatutDume() {
        return idStatutDume;
    }

    public void setIdStatutDume(String idStatutDume) {
        this.idStatutDume = idStatutDume;
    }

    public String getIdentifiantOE() {
        return identifiantOE;
    }

    public void setIdentifiantOE(String identifiantOE) {
        this.identifiantOE = identifiantOE;
    }
}
