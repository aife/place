package com.atexo.dume.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CriteriaLotDTO implements Serializable {

    private String code;

    private List<LotDTO> lots;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<LotDTO> getLots() {
        return lots;
    }

    public void setLots(List<LotDTO> lots) {
        this.lots = lots;
    }

    public CriteriaLotDTO() {
        this.lots = new ArrayList<>();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CriteriaLotDTO)) {
            return false;
        }

        CriteriaLotDTO that = (CriteriaLotDTO) o;

        if (code != null ? !code.equals(that.code) : that.code != null) {
            return false;
        }
        return lots != null ? lots.equals(that.lots) : that.lots == null;
    }

    @Override
    public int hashCode() {
        int result = code != null ? code.hashCode() : 0;
        result = 31 * result + (lots != null ? lots.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CriteriaLotDTO{" +
                "code='" + code + '\'' +
                ", lots=" + lots +
                '}';
    }
}
