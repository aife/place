package com.atexo.dume.dto.sn.essentialData;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class ModificationDTO {

    protected String dateSignatureModification;
    protected String datePublicationDonneesModification;
    List<TitulaireDTO> titulaires;
    List<TitulaireDTO> concessionnaires;
    private String objetModification;
    private Integer dureeMois;
    private BigDecimal montant;
    private BigDecimal valeurGlobale;

    public String getObjetModification() {
        return objetModification;
    }

    public void setObjetModification(String objetModification) {
        this.objetModification = objetModification;
    }

    public String getDateSignatureModification() {
        return dateSignatureModification;
    }

    public void setDateSignatureModification(String dateSignatureModification) {
        this.dateSignatureModification = dateSignatureModification;
    }

    public String getDatePublicationDonneesModification() {
        return datePublicationDonneesModification;
    }

    public void setDatePublicationDonneesModification(String datePublicationDonneesModification) {
        this.datePublicationDonneesModification = datePublicationDonneesModification;
    }

    public Integer getDureeMois() {
        return dureeMois;
    }

    public void setDureeMois(Integer dureeMois) {
        this.dureeMois = dureeMois;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public BigDecimal getValeurGlobale() {
        return valeurGlobale;
    }

    public void setValeurGlobale(BigDecimal valeurGlobale) {
        this.valeurGlobale = valeurGlobale;
    }

    public List<TitulaireDTO> getTitulaires() {
        return titulaires;
    }

    public void setTitulaires(List<TitulaireDTO> titulaires) {
        this.titulaires = titulaires;
    }

    public List<TitulaireDTO> getConcessionnaires() {
        return concessionnaires;
    }

    public void setConcessionnaires(List<TitulaireDTO> concessionnaires) {
        this.concessionnaires = concessionnaires;
    }
}
