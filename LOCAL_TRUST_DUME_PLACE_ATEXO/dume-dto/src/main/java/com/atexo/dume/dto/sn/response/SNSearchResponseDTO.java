package com.atexo.dume.dto.sn.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SNSearchResponseDTO {
    @JsonProperty("response")
    @Valid
    private SNResponse response;

    public SNSearchResponseDTO() {
        // constructeur par défaut
    }

    public SNResponse getResponse() {
        return response;
    }

    public void setResponse(SNResponse response) {
        this.response = response;
    }

}
