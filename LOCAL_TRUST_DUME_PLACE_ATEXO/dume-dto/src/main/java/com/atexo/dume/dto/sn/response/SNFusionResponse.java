package com.atexo.dume.dto.sn.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"messageList", "dumeOE"})
public class SNFusionResponse {

    @JsonProperty("messageList")
    @Valid
    private List<SNMessageList> messageList = new ArrayList<>();

    @JsonProperty("dumeOE")
    private SNDumeOEResponse dumeOE;

    public SNDumeOEResponse getDumeOE() {
        return dumeOE;
    }

    public void setDumeOE(SNDumeOEResponse dumeOE) {
        this.dumeOE = dumeOE;
    }

    @JsonProperty("messageList")
    public List<SNMessageList> getMessageList() {
        return messageList;
    }

    @JsonProperty("messageList")
    public void setMessageList(List<SNMessageList> messageList) {
        this.messageList = messageList;
    }
}
