package com.atexo.dume.dto;

import java.io.Serializable;

public class EconomicPartyInfoDTO implements Serializable {

    private String nom;

    private String rue;

    private String codePostal;

    private String ville;

    private EnumDTO pays;

    private String siteInternet;

    private String email;

    private String telephone;

    private String contact;

    private String numTVA;

    private String numEtrange;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public EnumDTO getPays() {
        return pays;
    }

    public void setPays(EnumDTO pays) {
        this.pays = pays;
    }

    public String getSiteInternet() {
        return siteInternet;
    }

    public void setSiteInternet(String siteInternet) {
        this.siteInternet = siteInternet;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getNumTVA() {
        return numTVA;
    }

    public void setNumTVA(String numTVA) {
        this.numTVA = numTVA;
    }

    public String getNumEtrange() {
        return numEtrange;
    }

    public void setNumEtrange(String numEtrange) {
        this.numEtrange = numEtrange;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EconomicPartyInfoDTO)) {
            return false;
        }

        EconomicPartyInfoDTO that = (EconomicPartyInfoDTO) o;

        if (nom != null ? !nom.equals(that.nom) : that.nom != null) {
            return false;
        }
        if (rue != null ? !rue.equals(that.rue) : that.rue != null) {
            return false;
        }
        if (codePostal != null ? !codePostal.equals(that.codePostal) : that.codePostal != null) {
            return false;
        }
        if (ville != null ? !ville.equals(that.ville) : that.ville != null) {
            return false;
        }
        if (pays != null ? !pays.equals(that.pays) : that.pays != null) {
            return false;
        }
        if (siteInternet != null ? !siteInternet.equals(that.siteInternet) : that.siteInternet != null) {
            return false;
        }
        if (email != null ? !email.equals(that.email) : that.email != null) {
            return false;
        }
        if (telephone != null ? !telephone.equals(that.telephone) : that.telephone != null) {
            return false;
        }
        if (contact != null ? !contact.equals(that.contact) : that.contact != null) {
            return false;
        }
        if (numEtrange != null ? !numEtrange.equals(that.numEtrange) : that.numEtrange != null) {
            return false;
        }
        return numTVA != null ? numTVA.equals(that.numTVA) : that.numTVA == null;
    }

    @Override
    public int hashCode() {
        int result = nom != null ? nom.hashCode() : 0;
        result = 31 * result + (rue != null ? rue.hashCode() : 0);
        result = 31 * result + (codePostal != null ? codePostal.hashCode() : 0);
        result = 31 * result + (ville != null ? ville.hashCode() : 0);
        result = 31 * result + (pays != null ? pays.hashCode() : 0);
        result = 31 * result + (siteInternet != null ? siteInternet.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (telephone != null ? telephone.hashCode() : 0);
        result = 31 * result + (contact != null ? contact.hashCode() : 0);
        result = 31 * result + (numTVA != null ? numTVA.hashCode() : 0);
        result = 31 * result + (numEtrange != null ? numEtrange.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EconomicPartyInfoDTO{" +
                "nom='" + nom + '\'' +
                ", rue='" + rue + '\'' +
                ", codePostal='" + codePostal + '\'' +
                ", ville='" + ville + '\'' +
                ", pays='" + pays + '\'' +
                ", siteInternet='" + siteInternet + '\'' +
                ", email='" + email + '\'' +
                ", telephone='" + telephone + '\'' +
                ", contact='" + contact + '\'' +
                ", numTVA='" + numTVA + '\'' +
                ", numEtrange='" + numEtrange + '\'' +
                '}';
    }
}
