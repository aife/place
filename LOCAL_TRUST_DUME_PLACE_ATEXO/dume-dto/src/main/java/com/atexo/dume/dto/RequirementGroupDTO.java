package com.atexo.dume.dto;

import java.io.Serializable;
import java.util.List;

public class RequirementGroupDTO implements Serializable {

    private Long id;

    private String uuid;

    private List<RequirementDTO> requirementList;

    private List<RequirementGroupDTO> subGroups;

    private Boolean fulfillmentIndicator;

    private Boolean unbounded;

    private Long idRequirementGroupResponse;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public List<RequirementDTO> getRequirementList() {
        return requirementList;
    }

    public void setRequirementList(List<RequirementDTO> requirementList) {
        this.requirementList = requirementList;
    }

    public List<RequirementGroupDTO> getSubGroups() {
        return subGroups;
    }

    public void setSubGroups(List<RequirementGroupDTO> subGroups) {
        this.subGroups = subGroups;
    }

    public Boolean getFulfillmentIndicator() {
        return fulfillmentIndicator;
    }

    public void setFulfillmentIndicator(Boolean fulfillmentIndicator) {
        this.fulfillmentIndicator = fulfillmentIndicator;
    }

    public Boolean getUnbounded() {
        return unbounded;
    }

    public void setUnbounded(Boolean unbounded) {
        this.unbounded = unbounded;
    }

    public Long getIdRequirementGroupResponse() {
        return idRequirementGroupResponse;
    }

    public void setIdRequirementGroupResponse(Long idRequirementGroupResponse) {
        this.idRequirementGroupResponse = idRequirementGroupResponse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequirementGroupDTO)) {
            return false;
        }

        RequirementGroupDTO that = (RequirementGroupDTO) o;

        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (uuid != null ? !uuid.equals(that.uuid) : that.uuid != null) {
            return false;
        }
        if (requirementList != null ? !requirementList.equals(that.requirementList) : that.requirementList != null) {
            return false;
        }
        if (subGroups != null ? !subGroups.equals(that.subGroups) : that.subGroups != null) {
            return false;
        }
        if (unbounded != null ? !unbounded.equals(that.unbounded) : that.unbounded != null) {
            return false;
        }

        if (fulfillmentIndicator != null ? !fulfillmentIndicator.equals(that.fulfillmentIndicator) : that.fulfillmentIndicator != null) {
            return false;
        }
        return idRequirementGroupResponse != null ? idRequirementGroupResponse.equals(that.idRequirementGroupResponse) : that.idRequirementGroupResponse == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (uuid != null ? uuid.hashCode() : 0);
        result = 31 * result + (requirementList != null ? requirementList.hashCode() : 0);
        result = 31 * result + (subGroups != null ? subGroups.hashCode() : 0);
        result = 31 * result + (fulfillmentIndicator != null ? fulfillmentIndicator.hashCode() : 0);
        result = 31 * result + (unbounded != null ? unbounded.hashCode() : 0);
        result = 31 * result + (idRequirementGroupResponse != null ? idRequirementGroupResponse.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RequirementGroupDTO{" +
                "id=" + id +
                ", uuid='" + uuid + '\'' +
                ", requirementList=" + requirementList +
                ", subGroups=" + subGroups +
                ", fulfillmentIndicator=" + fulfillmentIndicator +
                ", idRequirementGroupResponse=" + idRequirementGroupResponse +
                ", unbounded=" + unbounded +
                '}';
    }
}
