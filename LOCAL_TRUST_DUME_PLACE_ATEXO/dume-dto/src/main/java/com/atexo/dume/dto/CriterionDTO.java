
package com.atexo.dume.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CriterionDTO {

    @JsonProperty("texte")
    private String texte;
    @JsonProperty("titreDeLaLegislationReference")
    private String titreDeLaLegislationReference;
    @JsonProperty("articleDeLegislationReference")
    private String articleDeLegislationReference;
    @JsonProperty("descriptionDuSubCriterion")
    private String descriptionDuSubCriterion;

    @JsonProperty("texte")
    public String getTexte() {
        return texte;
    }

    @JsonProperty("texte")
    public void setTexte(String texte) {
        this.texte = texte;
    }

    @JsonProperty("titreDeLaLegislationReference")
    public String getTitreDeLaLegislationReference() {
        return titreDeLaLegislationReference;
    }

    @JsonProperty("titreDeLaLegislationReference")
    public void setTitreDeLaLegislationReference(String titreDeLaLegislationReference) {
        this.titreDeLaLegislationReference = titreDeLaLegislationReference;
    }

    @JsonProperty("articleDeLegislationReference")
    public String getArticleDeLegislationReference() {
        return articleDeLegislationReference;
    }

    @JsonProperty("articleDeLegislationReference")
    public void setArticleDeLegislationReference(String articleDeLegislationReference) {
        this.articleDeLegislationReference = articleDeLegislationReference;
    }

    @JsonProperty("descriptionDuSubCriterion")
    public String getDescriptionDuSubCriterion() {
        return descriptionDuSubCriterion;
    }

    @JsonProperty("descriptionDuSubCriterion")
    public void setDescriptionDuSubCriterion(String descriptionDuSubCriterion) {
        this.descriptionDuSubCriterion = descriptionDuSubCriterion;
    }

}
