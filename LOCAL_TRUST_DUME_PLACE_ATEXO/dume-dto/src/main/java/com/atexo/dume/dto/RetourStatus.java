package com.atexo.dume.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.atexo.dume.toolbox.consts.DumeJsonConstants.IS_STANDARD;
import static com.atexo.dume.toolbox.consts.DumeJsonConstants.MESSAGE;
import static com.atexo.dume.toolbox.consts.DumeJsonConstants.STATUS;

public class RetourStatus {
    @JsonProperty(STATUS)
    private StatusDTO statusDTO;

    @JsonProperty(MESSAGE)
    private String message;

    @JsonProperty(IS_STANDARD)
    private Boolean standard;

    public RetourStatus() {
    }

    public RetourStatus(StatusDTO statusDTO, String message) {
        this.statusDTO = statusDTO;
        this.message = message;
    }

    public StatusDTO getStatusDTO() {
        return statusDTO;
    }

    public void setStatusDTO(StatusDTO statusDTO) {
        this.statusDTO = statusDTO;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStandard() {
        return standard;
    }

    public void setStandard(Boolean standard) {
        this.standard = standard;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RetourStatus that = (RetourStatus) o;

        if (statusDTO != null ? !statusDTO.equals(that.statusDTO) : that.statusDTO != null) {
            return false;
        }
        if (standard != null ? !standard.equals(that.standard) : that.standard != null) {
            return false;
        }
        return message != null ? message.equals(that.message) : that.message == null;
    }

    @Override
    public int hashCode() {
        int result = statusDTO != null ? statusDTO.hashCode() : 0;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (standard != null ? standard.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RetourStatus{" +
                "statusDTO='" + statusDTO + '\'' +
                ", message='" + message + '\'' +
                ", standard='" + standard + '\'' +
                '}';
    }
}
