package com.atexo.dume.dto.sn.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.Valid;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder("response")
public class SNDumeSavedResponseDTO {

    @JsonProperty("response")
    @Valid
    private SNResponse response;

    @JsonProperty("response")
    public SNResponse getResponse() {
        return response;
    }

    @JsonProperty("response")
    public void setResponse(SNResponse response) {
        this.response = response;
    }

}
