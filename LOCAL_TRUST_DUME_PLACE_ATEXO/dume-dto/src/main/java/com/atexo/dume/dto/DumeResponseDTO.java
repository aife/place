package com.atexo.dume.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

import static com.atexo.dume.toolbox.consts.DumeJsonConstants.*;

/**
 * DTO du Dume OE. Le DTO contient l'arborescence des critères, des rquirementGroups et des Requirements ainsi que
 * leur réponse associée.
 */
@JsonIgnoreProperties
public class DumeResponseDTO implements Serializable {

    private Long id;

    @JsonProperty(NOM_OFFICIEL)
    private String officialName;

    @JsonProperty(PAYS)
    private String country;

    @JsonProperty(REFERENCE_CONSULTATION)
    private String consultRef;

    @JsonProperty(TYPE_PROCEDURE)
    private String procedureType;

    @JsonProperty(INTITULE)
    private String entitled;

    @JsonProperty(OBJET)
    private String object;

    @JsonProperty(NUMERO_SN)
    private String numeroSN;


    private Long idDumeRequest;

    private List<SelectionCriteriaDTO> selectionCriteriaList;

    private List<ExclusionCriteriaDTO> exclusionCriteriaList;

    private List<OtherCriteriaDTO> otherCriteriaList;

    private EconomicPartyInfoDTO economicPartyInfo;

    private List<RepresentantLegalDTO> representantLegals;

    private Boolean micro;

    private Boolean confirmed;

    private String numDumeAcheteurSN;

    private String lots;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdDumeRequest() {
        return idDumeRequest;
    }

    public void setIdDumeRequest(Long idDumeRequest) {
        this.idDumeRequest = idDumeRequest;
    }

    public String getOfficialName() {
        return officialName;
    }

    public void setOfficialName(String officialName) {
        this.officialName = officialName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getConsultRef() {
        return consultRef;
    }

    public void setConsultRef(String consultRef) {
        this.consultRef = consultRef;
    }

    public String getProcedureType() {
        return procedureType;
    }

    public void setProcedureType(String procedureType) {
        this.procedureType = procedureType;
    }

    public String getEntitled() {
        return entitled;
    }

    public void setEntitled(String entitled) {
        this.entitled = entitled;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public List<SelectionCriteriaDTO> getSelectionCriteriaList() {
        return selectionCriteriaList;
    }

    public void setSelectionCriteriaList(List<SelectionCriteriaDTO> selectionCriteriaList) {
        this.selectionCriteriaList = selectionCriteriaList;
    }

    public List<ExclusionCriteriaDTO> getExclusionCriteriaList() {
        return exclusionCriteriaList;
    }

    public void setExclusionCriteriaList(List<ExclusionCriteriaDTO> exclusionCriteriaList) {
        this.exclusionCriteriaList = exclusionCriteriaList;
    }

    public List<OtherCriteriaDTO> getOtherCriteriaList() {
        return otherCriteriaList;
    }

    public void setOtherCriteriaList(List<OtherCriteriaDTO> otherCriteriaDTOList) {
        this.otherCriteriaList = otherCriteriaDTOList;
    }

    public EconomicPartyInfoDTO getEconomicPartyInfo() {
        return economicPartyInfo;
    }

    public void setEconomicPartyInfo(EconomicPartyInfoDTO economicPartyInfo) {
        this.economicPartyInfo = economicPartyInfo;
    }

    public List<RepresentantLegalDTO> getRepresentantLegals() {
        return representantLegals;
    }

    public void setRepresentantLegals(List<RepresentantLegalDTO> representantLegals) {
        this.representantLegals = representantLegals;
    }

    public Boolean getMicro() {
        return micro;
    }

    public void setMicro(Boolean micro) {
        this.micro = micro;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public String getNumDumeAcheteurSN() {
        return numDumeAcheteurSN;
    }

    public void setNumDumeAcheteurSN(String numDumeAcheteurSN) {
        this.numDumeAcheteurSN = numDumeAcheteurSN;
    }

    public String getLots() {
        return lots;
    }

    public void setLots(String lots) {
        this.lots = lots;
    }

    public String getNumeroSN() {
        return numeroSN;
    }

    public void setNumeroSN(String numeroSN) {
        this.numeroSN = numeroSN;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DumeResponseDTO)) {
            return false;
        }

        DumeResponseDTO that = (DumeResponseDTO) o;

        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (officialName != null ? !officialName.equals(that.officialName) : that.officialName != null) {
            return false;
        }
        if (country != null ? !country.equals(that.country) : that.country != null) {
            return false;
        }
        if (consultRef != null ? !consultRef.equals(that.consultRef) : that.consultRef != null) {
            return false;
        }
        if (procedureType != null ? !procedureType.equals(that.procedureType) : that.procedureType != null) {
            return false;
        }
        if (entitled != null ? !entitled.equals(that.entitled) : that.entitled != null) {
            return false;
        }
        if (object != null ? !object.equals(that.object) : that.object != null) {
            return false;
        }
        if (idDumeRequest != null ? !idDumeRequest.equals(that.idDumeRequest) : that.idDumeRequest != null) {
            return false;
        }
        if (selectionCriteriaList != null ? !selectionCriteriaList.equals(that.selectionCriteriaList) : that.selectionCriteriaList != null) {
            return false;
        }
        if (exclusionCriteriaList != null ? !exclusionCriteriaList.equals(that.exclusionCriteriaList) : that.exclusionCriteriaList != null) {
            return false;
        }
        if (otherCriteriaList != null ? !otherCriteriaList.equals(that.otherCriteriaList) : that.otherCriteriaList != null) {
            return false;
        }
        if (economicPartyInfo != null ? !economicPartyInfo.equals(that.economicPartyInfo) : that.economicPartyInfo != null) {
            return false;
        }
        if (representantLegals != null ? !representantLegals.equals(that.representantLegals) : that.representantLegals != null) {
            return false;
        }
        if (micro != null ? !micro.equals(that.micro) : that.micro != null) {
            return false;
        }
        if (confirmed != null ? !confirmed.equals(that.confirmed) : that.confirmed != null) {
            return false;
        }
        if (lots != null ? !lots.equals(that.lots) : that.lots != null) {
            return false;
        }
        return numDumeAcheteurSN != null ? numDumeAcheteurSN.equals(that.numDumeAcheteurSN) : that.numDumeAcheteurSN == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (officialName != null ? officialName.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (consultRef != null ? consultRef.hashCode() : 0);
        result = 31 * result + (procedureType != null ? procedureType.hashCode() : 0);
        result = 31 * result + (entitled != null ? entitled.hashCode() : 0);
        result = 31 * result + (object != null ? object.hashCode() : 0);
        result = 31 * result + (idDumeRequest != null ? idDumeRequest.hashCode() : 0);
        result = 31 * result + (selectionCriteriaList != null ? selectionCriteriaList.hashCode() : 0);
        result = 31 * result + (exclusionCriteriaList != null ? exclusionCriteriaList.hashCode() : 0);
        result = 31 * result + (otherCriteriaList != null ? otherCriteriaList.hashCode() : 0);
        result = 31 * result + (economicPartyInfo != null ? economicPartyInfo.hashCode() : 0);
        result = 31 * result + (representantLegals != null ? representantLegals.hashCode() : 0);
        result = 31 * result + (micro != null ? micro.hashCode() : 0);
        result = 31 * result + (confirmed != null ? confirmed.hashCode() : 0);
        result = 31 * result + (numDumeAcheteurSN != null ? numDumeAcheteurSN.hashCode() : 0);
        result = 31 * result + (lots != null ? lots.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DumeResponseDTO{" +
                "id=" + id +
                ", officialName='" + officialName + '\'' +
                ", country='" + country + '\'' +
                ", consultRef='" + consultRef + '\'' +
                ", procedureType='" + procedureType + '\'' +
                ", entitled='" + entitled + '\'' +
                ", object='" + object + '\'' +
                ", idDumeRequest=" + idDumeRequest +
                ", selectionCriteriaList=" + selectionCriteriaList +
                ", exclusionCriteriaList=" + exclusionCriteriaList +
                ", otherCriteriaList=" + otherCriteriaList +
                ", economicPartyInfo=" + economicPartyInfo +
                ", representantLegals=" + representantLegals +
                ", micro=" + micro +
                ", confirmed=" + confirmed +
                ", numDumeAcheteurSN='" + numDumeAcheteurSN + '\'' +
                ", lots='" + lots + '\'' +
                '}';
    }


}
