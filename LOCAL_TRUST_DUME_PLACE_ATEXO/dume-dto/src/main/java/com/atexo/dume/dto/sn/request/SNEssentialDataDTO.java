package com.atexo.dume.dto.sn.request;

import com.atexo.dume.dto.sn.essentialData.AcheteurDTO;
import com.atexo.dume.dto.sn.essentialData.DonneesAnnuellesDTO;
import com.atexo.dume.dto.sn.essentialData.LieuExecutionDTO;
import com.atexo.dume.dto.sn.essentialData.ModificationDTO;
import com.atexo.dume.dto.sn.essentialData.TitulaireDTO;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class SNEssentialDataDTO extends SNRequestDTO {
    private String lot;
    private String typeDE;
    private String id;
    private AcheteurDTO acheteur;
    private String nature;
    private String objet;
    private String codeCPV;
    private String procedure;
    private LieuExecutionDTO lieuExecution;
    private String dureeMois;
    private String dateNotification;
    private String datePublicationDonnees;
    private Double montant;
    private Double valeurGlobale;
    private String formePrix;
    private String refFonctionnelle;
    private String refFonctionnelleDE;
    private List<TitulaireDTO> titulaires;
    private List<ModificationDTO> modifications;
    private AcheteurDTO autoriteConcedante;
    private List<TitulaireDTO> concessionnaires;
    private String dateSignature;
    private String dateDebutExecution;
    private BigDecimal montantSubventionPublique;
    private List<DonneesAnnuellesDTO> donneesExecution;

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getTypeDE() {
        return typeDE;
    }

    public void setTypeDE(String typeDE) {
        this.typeDE = typeDE;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AcheteurDTO getAcheteur() {
        return acheteur;
    }

    public void setAcheteur(AcheteurDTO acheteur) {
        this.acheteur = acheteur;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public String getCodeCPV() {
        return codeCPV;
    }

    public void setCodeCPV(String codeCPV) {
        this.codeCPV = codeCPV;
    }

    public String getProcedure() {
        return procedure;
    }

    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

    public String getDateNotification() {
        return dateNotification;
    }

    public void setDateNotification(String dateNotification) {
        this.dateNotification = dateNotification;
    }

    public LieuExecutionDTO getLieuExecution() {
        return lieuExecution;
    }

    public void setLieuExecution(LieuExecutionDTO lieuExecution) {
        this.lieuExecution = lieuExecution;
    }

    public String getDureeMois() {
        return dureeMois;
    }

    public void setDureeMois(String dureeMois) {
        this.dureeMois = dureeMois;
    }

    public String getDatePublicationDonnees() {
        return datePublicationDonnees;
    }

    public void setDatePublicationDonnees(String datePublicationDonnees) {
        this.datePublicationDonnees = datePublicationDonnees;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public Double getValeurGlobale() {
        return valeurGlobale;
    }

    public void setValeurGlobale(Double valeurGlobale) {
        this.valeurGlobale = valeurGlobale;
    }

    public String getFormePrix() {
        return formePrix;
    }

    public void setFormePrix(String formePrix) {
        this.formePrix = formePrix;
    }

    public List<TitulaireDTO> getTitulaires() {
        return titulaires;
    }

    public void setTitulaires(List<TitulaireDTO> titulaires) {
        this.titulaires = titulaires;
    }

    public String getRefFonctionnelle() {
        return refFonctionnelle;
    }

    public void setRefFonctionnelle(String refFonctionnelle) {
        this.refFonctionnelle = refFonctionnelle;
    }

    public List<ModificationDTO> getModifications() {
        return modifications;
    }

    public void setModifications(List<ModificationDTO> modifications) {
        this.modifications = modifications;
    }

    public AcheteurDTO getAutoriteConcedante() {
        return autoriteConcedante;
    }

    public void setAutoriteConcedante(AcheteurDTO autoriteConcedante) {
        this.autoriteConcedante = autoriteConcedante;
    }

    public List<TitulaireDTO> getConcessionnaires() {
        return concessionnaires;
    }

    public void setConcessionnaires(List<TitulaireDTO> concessionnaires) {
        this.concessionnaires = concessionnaires;
    }

    public String getRefFonctionnelleDE() {
        return refFonctionnelleDE;
    }

    public void setRefFonctionnelleDE(String refFonctionnelleDE) {
        this.refFonctionnelleDE = refFonctionnelleDE;
    }

    public String getDateSignature() {
        return dateSignature;
    }

    public void setDateSignature(String dateSignature) {
        this.dateSignature = dateSignature;
    }

    public String getDateDebutExecution() {
        return dateDebutExecution;
    }

    public void setDateDebutExecution(String dateDebutExecution) {
        this.dateDebutExecution = dateDebutExecution;
    }

    public BigDecimal getMontantSubventionPublique() {
        return montantSubventionPublique;
    }

    public void setMontantSubventionPublique(BigDecimal montantSubventionPublique) {
        this.montantSubventionPublique = montantSubventionPublique;
    }

    public List<DonneesAnnuellesDTO> getDonneesExecution() {
        return donneesExecution;
    }

    public void setDonneesExecution(List<DonneesAnnuellesDTO> donneesExecution) {
        this.donneesExecution = donneesExecution;
    }
}
