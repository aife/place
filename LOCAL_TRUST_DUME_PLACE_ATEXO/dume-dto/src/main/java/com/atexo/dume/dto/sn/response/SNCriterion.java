
package com.atexo.dume.dto.sn.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "uuid",
    "texte",
    "Titre de la LegislationReference",
    "Article de LegislationReference",
    "Description du SubCriterion"
})
public class SNCriterion {

    @JsonProperty("uuid")
    private String uuid;
    @JsonProperty("texte")
    private String texte;
    @JsonProperty("Titre de la LegislationReference")
    private String titreDeLaLegislationReference;
    @JsonProperty("Article de LegislationReference")
    private String articleDeLegislationReference;
    @JsonProperty("Description du SubCriterion")
    private String descriptionDuSubCriterion;

    @JsonProperty("uuid")
    public String getUuid() {
        return uuid;
    }

    @JsonProperty("uuid")
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @JsonProperty("texte")
    public String getTexte() {
        return texte;
    }

    @JsonProperty("texte")
    public void setTexte(String texte) {
        this.texte = texte;
    }

    @JsonProperty("Titre de la LegislationReference")
    public String getTitreDeLaLegislationReference() {
        return titreDeLaLegislationReference;
    }

    @JsonProperty("Titre de la LegislationReference")
    public void setTitreDeLaLegislationReference(String titreDeLaLegislationReference) {
        this.titreDeLaLegislationReference = titreDeLaLegislationReference;
    }

    @JsonProperty("Article de LegislationReference")
    public String getArticleDeLegislationReference() {
        return articleDeLegislationReference;
    }

    @JsonProperty("Article de LegislationReference")
    public void setArticleDeLegislationReference(String articleDeLegislationReference) {
        this.articleDeLegislationReference = articleDeLegislationReference;
    }

    @JsonProperty("Description du SubCriterion")
    public String getDescriptionDuSubCriterion() {
        return descriptionDuSubCriterion;
    }

    @JsonProperty("Description du SubCriterion")
    public void setDescriptionDuSubCriterion(String descriptionDuSubCriterion) {
        this.descriptionDuSubCriterion = descriptionDuSubCriterion;
    }

}
