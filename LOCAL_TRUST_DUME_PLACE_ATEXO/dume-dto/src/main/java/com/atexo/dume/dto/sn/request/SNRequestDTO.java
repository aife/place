package com.atexo.dume.dto.sn.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(Include.NON_DEFAULT)

@JsonPropertyOrder({"operation", "dumeA", "plateforme", "rsDemandeur", "idDemandeur", "idStatutDume", "idTypeProcedure", "idTypeMarche", "idNatureMarche", "idConsultation", "libelleConsultation", "refFonctionnelleConsultation", "DLRO", "identifiantA", "typeIdA", "lots", "accessible", "format"})
public class SNRequestDTO {

    @JsonProperty("operation")
    private String operation;
    @JsonProperty("dumeA")
    @Valid
    private SNDumeA dumeA;

    @JsonProperty("dumeOE")
    @Valid
    private SNDumeA dumeOE;

    @JsonProperty("dume")
    @Valid
    private SNDumeA dume;
    @JsonProperty("plateforme")
    @Valid
    private SNPlateforme plateforme;
    @JsonProperty("rsDemandeur")
    private String rsDemandeur;
    @JsonProperty("idDemandeur")
    private String idDemandeur;
    @JsonProperty("idStatutDume")
    private String idStatutDume;
    @JsonProperty("idTypeProcedure")
    private String idTypeProcedure;
    @JsonProperty("idTypeMarche")
    private String idTypeMarche;
    @JsonProperty("idNatureMarche")
    private String idNatureMarche;
    @JsonProperty("idConsultation")
    private String idConsultation;
    @JsonProperty("libelleConsultation")
    private String libelleConsultation;
    @JsonProperty("refFonctionnelleConsultation")
    private String refFonctionnelleConsultation;
    @JsonProperty("DLRO")
    private String dLRO;
    @JsonProperty("identifiantA")
    private String identifiantA;
    @JsonProperty("nomIdentifiantA")
    private String nomIdentifiantA;
    @JsonProperty("typeIdA")
    private String typeIdA;
    @JsonProperty("typeIdOE")
    private String typeIdOE;
    @JsonProperty("format")
    private String format;
    @JsonProperty("lots")
    @Valid
    private List<SNLot> lots = new ArrayList<>();
    @JsonProperty("accessible")
    private Boolean accessible;
    @JsonProperty("typeDonnees")
    private String typeDonnees;
    @JsonProperty("siretOE")
    private String siretOE;
    @JsonProperty("identifiantOE")
    private String identifiantOE;

    @JsonProperty("typeReponse")
    private String idTypeResponse;

    @JsonProperty("colonneAModifier")
    private String colonneAModifier;

    @JsonProperty("valeurAModifier")
    private String valeurAModifier;

    @JsonProperty("idGroupement")
    private String idGroupement;

    @JsonProperty("siretNP1")
    private String siretNP1;

    @JsonProperty("operation")
    public String getOperation() {
        return operation;
    }

    @JsonProperty("operation")
    public void setOperation(String operation) {
        this.operation = operation;
    }

    @JsonProperty("dumesOE")
    List<SNCritereDumeOE> dumesOE;

    @JsonProperty("identifiantsOE")
    List<SNCritereDumeOE> identifiantsOE;

    @JsonProperty("dumeA")
    public SNDumeA getDumeA() {
        return dumeA;
    }

    @JsonProperty("dumeA")
    public void setDumeA(SNDumeA dumeA) {
        this.dumeA = dumeA;
    }

    @JsonProperty("dume")
    public SNDumeA getDume() {
        return dume;
    }

    @JsonProperty("dume")
    public void setDume(SNDumeA dume) {
        this.dume = dume;
    }

    @JsonProperty("plateforme")
    public SNPlateforme getPlateforme() {
        return plateforme;
    }

    @JsonProperty("plateforme")
    public void setPlateforme(SNPlateforme plateforme) {
        this.plateforme = plateforme;
    }

    @JsonProperty("rsDemandeur")
    public String getRsDemandeur() {
        return rsDemandeur;
    }

    @JsonProperty("rsDemandeur")
    public void setRsDemandeur(String rsDemandeur) {
        this.rsDemandeur = rsDemandeur;
    }

    @JsonProperty("idDemandeur")
    public String getIdDemandeur() {
        return idDemandeur;
    }

    @JsonProperty("idDemandeur")
    public void setIdDemandeur(String idDemandeur) {
        this.idDemandeur = idDemandeur;
    }

    @JsonProperty("idStatutDume")
    public String getIdStatutDume() {
        return idStatutDume;
    }

    @JsonProperty("idStatutDume")
    public void setIdStatutDume(String idStatutDume) {
        this.idStatutDume = idStatutDume;
    }

    @JsonProperty("idTypeProcedure")
    public String getIdTypeProcedure() {
        return idTypeProcedure;
    }

    @JsonProperty("idTypeProcedure")
    public void setIdTypeProcedure(String idTypeProcedure) {
        this.idTypeProcedure = idTypeProcedure;
    }

    @JsonProperty("idTypeMarche")
    public String getIdTypeMarche() {
        return idTypeMarche;
    }

    @JsonProperty("idTypeMarche")
    public void setIdTypeMarche(String idTypeMarche) {
        this.idTypeMarche = idTypeMarche;
    }

    @JsonProperty("idNatureMarche")
    public String getIdNatureMarche() {
        return idNatureMarche;
    }

    @JsonProperty("idNatureMarche")
    public void setIdNatureMarche(String idNatureMarche) {
        this.idNatureMarche = idNatureMarche;
    }

    @JsonProperty("idConsultation")
    public String getIdConsultation() {
        return idConsultation;
    }

    @JsonProperty("idConsultation")
    public void setIdConsultation(String idConsultation) {
        this.idConsultation = idConsultation;
    }

    @JsonProperty("libelleConsultation")
    public String getLibelleConsultation() {
        return libelleConsultation;
    }

    @JsonProperty("libelleConsultation")
    public void setLibelleConsultation(String libelleConsultation) {
        this.libelleConsultation = libelleConsultation;
    }

    @JsonProperty("refFonctionnelleConsultation")
    public String getRefFonctionnelleConsultation() {
        return refFonctionnelleConsultation;
    }

    @JsonProperty("refFonctionnelleConsultation")
    public void setRefFonctionnelleConsultation(String refFonctionnelleConsultation) {
        this.refFonctionnelleConsultation = refFonctionnelleConsultation;
    }

    @JsonProperty("DLRO")
    public String getDLRO() {
        return dLRO;
    }

    @JsonProperty("DLRO")
    public void setDLRO(String dLRO) {
        this.dLRO = dLRO;
    }

    @JsonProperty("identifiantA")
    public String getIdentifiantA() {
        return identifiantA;
    }

    @JsonProperty("identifiantA")
    public void setIdentifiantA(String identifiantA) {
        this.identifiantA = identifiantA;
    }

    @JsonProperty("nomIdentifiantA")
    public String getNomIdentifiantA() {
        return nomIdentifiantA;
    }

    @JsonProperty("nomIdentifiantA")
    public void setNomIdentifiantA(String nomIdentifiantA) {
        this.nomIdentifiantA = nomIdentifiantA;
    }

    @JsonProperty("typeIdA")
    public String getTypeIdA() {
        return typeIdA;
    }

    @JsonProperty("typeIdA")
    public void setTypeIdA(String typeIdA) {
        this.typeIdA = typeIdA;
    }

    @JsonProperty("lots")
    public List<SNLot> getLots() {
        return lots;
    }

    @JsonProperty("lots")
    public void setLots(List<SNLot> lots) {
        this.lots = lots;
    }

    @JsonProperty("accessible")
    public Boolean getAccessible() {
        return accessible;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public SNDumeA getDumeOE() {
        return dumeOE;
    }

    public void setDumeOE(SNDumeA dumeOE) {
        this.dumeOE = dumeOE;
    }

    public String getIdentifiantOE() {
        return identifiantOE;
    }

    public void setIdentifiantOE(String identifiantOE) {
        this.identifiantOE = identifiantOE;
    }

    @JsonProperty("accessible")
    public void setAccessible(Boolean accessible) {
        this.accessible = accessible;
    }

    @JsonProperty("typeDonnees")
    public String getTypeDonnees() {
        return typeDonnees;
    }

    @JsonProperty("typeDonnees")
    public void setTypeDonnees(String typeDonnees) {
        this.typeDonnees = typeDonnees;
    }

    @JsonProperty("siretOE")
    public String getSiretOE() {
        return siretOE;
    }

    @JsonProperty("siretOE")
    public void setSiretOE(String siretOE) {
        this.siretOE = siretOE;
    }

    public String getIdTypeResponse() {
        return idTypeResponse;
    }

    public void setIdTypeResponse(String idTypeResponse) {
        this.idTypeResponse = idTypeResponse;
    }

    public List<SNCritereDumeOE> getDumesOE() {
        return dumesOE;
    }

    public void setDumesOE(List<SNCritereDumeOE> dumesOE) {
        this.dumesOE = dumesOE;
    }

    public List<SNCritereDumeOE> getIdentifiantsOE() {
        return identifiantsOE;
    }

    public void setIdentifiantsOE(List<SNCritereDumeOE> identifiantsOE) {
        this.identifiantsOE = identifiantsOE;
    }

    public String getColonneAModifier() {
        return colonneAModifier;
    }

    public void setColonneAModifier(String colonneAModifier) {
        this.colonneAModifier = colonneAModifier;
    }

    public String getValeurAModifier() {
        return valeurAModifier;
    }

    public void setValeurAModifier(String valeurAModifier) {
        this.valeurAModifier = valeurAModifier;
    }

    public String getTypeIdOE() {
        return typeIdOE;
    }

    public void setTypeIdOE(String typeIdOE) {
        this.typeIdOE = typeIdOE;
    }

    public String getIdGroupement() {
        return idGroupement;
    }

    public void setIdGroupement(String idGroupement) {
        this.idGroupement = idGroupement;
    }

    public String getSiretNP1() {
        return siretNP1;
    }

    public void setSiretNP1(String siretNP1) {
        this.siretNP1 = siretNP1;
    }

}
