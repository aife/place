package com.atexo.dume.dto.sn.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SNDumeAcheteurResponse {

    @JsonProperty("Id")
    private String id;
    @JsonProperty("Date de création")
    private String dateDeCreation;
    @JsonProperty("Date de modification")
    private String dateDeModification;
    @JsonProperty("Statut")
    private String statut;
    @JsonProperty("Accessibilité")
    private boolean accessibilite;

    public SNDumeAcheteurResponse() {
        // Constructeur par défaut
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDateDeCreation() {
        return dateDeCreation;
    }

    public void setDateDeCreation(String dateDeCreation) {
        this.dateDeCreation = dateDeCreation;
    }

    public String getDateDeModification() {
        return dateDeModification;
    }

    public void setDateDeModification(String dateDeModification) {
        this.dateDeModification = dateDeModification;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public boolean isAccessibilite() {
        return accessibilite;
    }

    public void setAccessibilite(boolean accessibilite) {
        this.accessibilite = accessibilite;
    }
}
