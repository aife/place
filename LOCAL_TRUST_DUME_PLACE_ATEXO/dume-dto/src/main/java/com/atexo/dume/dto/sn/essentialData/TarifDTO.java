package com.atexo.dume.dto.sn.essentialData;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class TarifDTO {

    private String intituleTarif;
    private BigDecimal tarif;

    public String getIntituleTarif() {
        return intituleTarif;
    }

    public void setIntituleTarif(String value) {
        this.intituleTarif = value;
    }

    public BigDecimal getTarif() {
        return tarif;
    }

    public void setTarif(BigDecimal value) {
        this.tarif = value;
    }

}
