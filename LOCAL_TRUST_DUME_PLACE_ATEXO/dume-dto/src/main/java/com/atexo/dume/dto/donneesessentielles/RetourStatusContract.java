package com.atexo.dume.dto.donneesessentielles;

import com.atexo.dume.dto.StatusDTO;
import com.fasterxml.jackson.annotation.JsonProperty;

import static com.atexo.dume.toolbox.consts.DumeJsonConstants.MESSAGE;
import static com.atexo.dume.toolbox.consts.DumeJsonConstants.STATUS;


public class RetourStatusContract {

    @JsonProperty(STATUS)
    private StatusDTO statusDTO;
    @JsonProperty(MESSAGE)
    private String message;
    @JsonProperty("numeroDumeSN")
    private String snNumber;

    public RetourStatusContract() {
        // Constructeur par défaut
    }

    public RetourStatusContract(StatusDTO statusDTO, String message, String snNumber) {
        this.statusDTO = statusDTO;
        this.message = message;
        this.snNumber = snNumber;
    }

    public RetourStatusContract(StatusDTO statusDTO) {
        this.statusDTO = statusDTO;
    }

    public StatusDTO getStatusDTO() {
        return statusDTO;
    }

    public void setStatusDTO(StatusDTO statusDTO) {
        this.statusDTO = statusDTO;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSnNumber() {
        return snNumber;
    }

    public void setSnNumber(String snNumber) {
        this.snNumber = snNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RetourStatusContract)) {
            return false;
        }

        RetourStatusContract that = (RetourStatusContract) o;

        if (statusDTO != that.statusDTO) {
            return false;
        }
        if (message != null ? !message.equals(that.message) : that.message != null) {
            return false;
        }
        return snNumber != null ? snNumber.equals(that.snNumber) : that.snNumber == null;
    }

    @Override
    public int hashCode() {
        int result = statusDTO != null ? statusDTO.hashCode() : 0;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (snNumber != null ? snNumber.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RetourStatusContract{" +
                "statusDTO=" + statusDTO +
                ", message='" + message + '\'' +
                ", snNumber='" + snNumber + '\'' +
                '}';
    }
}
