package com.atexo.dume.dto.sn.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SNSearchOEResponse {

    @JsonProperty("dumeAcheteur")
    private SNDumeAcheteurResponse dumeAcheteur;
    @JsonProperty("consultation")
    private SNConsultationResponse consultation;
    @JsonProperty("Dume OE")
    private SNDumeOEResponse dumeOEResponse;

    public SNSearchOEResponse() {
        // Constructeur par défaut
    }

    public SNDumeAcheteurResponse getDumeAcheteur() {
        return dumeAcheteur;
    }

    public void setDumeAcheteur(SNDumeAcheteurResponse dumeAcheteur) {
        this.dumeAcheteur = dumeAcheteur;
    }

    public SNConsultationResponse getConsultation() {
        return consultation;
    }

    public void setConsultation(SNConsultationResponse consultation) {
        this.consultation = consultation;
    }

    public SNDumeOEResponse getDumeOEResponse() {
        return dumeOEResponse;
    }

    public void setDumeOEResponse(SNDumeOEResponse dumeOEResponse) {
        this.dumeOEResponse = dumeOEResponse;
    }
}
