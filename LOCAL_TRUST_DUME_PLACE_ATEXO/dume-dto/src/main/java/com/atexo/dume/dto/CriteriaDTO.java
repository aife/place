package com.atexo.dume.dto;

import java.io.Serializable;
import java.util.List;

public class CriteriaDTO implements Serializable {

    private Long id;

    private String uuid;

    private String shortName;

    private String description;

    private LegislationReferenceDTO legislationReference;

    private String code;

    private int order;

    private Boolean enableECertis;

    private CriteriaCategorieDTO criteriaCategorie;

    private List<RequirementGroupDTO> requirementGroupList;

    private Long idCriteriaResponse;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LegislationReferenceDTO getLegislationReference() {
        return legislationReference;
    }

    public void setLegislationReference(LegislationReferenceDTO legislationReference) {
        this.legislationReference = legislationReference;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public Boolean getEnableECertis() {
        return enableECertis;
    }

    public void setEnableECertis(Boolean enableECertis) {
        this.enableECertis = enableECertis;
    }

    public CriteriaCategorieDTO getCriteriaCategorie() {
        return criteriaCategorie;
    }

    public void setCriteriaCategorie(CriteriaCategorieDTO criteriaCategorie) {
        this.criteriaCategorie = criteriaCategorie;
    }

    public List<RequirementGroupDTO> getRequirementGroupList() {
        return requirementGroupList;
    }

    public void setRequirementGroupList(List<RequirementGroupDTO> requirementGroupList) {
        this.requirementGroupList = requirementGroupList;
    }

    public Long getIdCriteriaResponse() {
        return idCriteriaResponse;
    }

    public void setIdCriteriaResponse(Long idCriteriaResponse) {
        this.idCriteriaResponse = idCriteriaResponse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CriteriaDTO)) {
            return false;
        }

        CriteriaDTO that = (CriteriaDTO) o;

        if (order != that.order) {
            return false;
        }
        if (enableECertis != null ? !enableECertis.equals(that.enableECertis) : that.enableECertis != null) {
            return false;
        }
        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (uuid != null ? !uuid.equals(that.uuid) : that.uuid != null) {
            return false;
        }
        if (shortName != null ? !shortName.equals(that.shortName) : that.shortName != null) {
            return false;
        }
        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        if (legislationReference != null ? !legislationReference.equals(that.legislationReference) : that.legislationReference != null) {
            return false;
        }
        if (code != null ? !code.equals(that.code) : that.code != null) {
            return false;
        }
        if (criteriaCategorie != null ? !criteriaCategorie.equals(that.criteriaCategorie) : that.criteriaCategorie != null) {
            return false;
        }
        if (idCriteriaResponse != null ? !idCriteriaResponse.equals(that.idCriteriaResponse) : that.idCriteriaResponse != null) {
            return false;
        }
        return requirementGroupList != null ? requirementGroupList.equals(that.requirementGroupList) : that.requirementGroupList == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (uuid != null ? uuid.hashCode() : 0);
        result = 31 * result + (shortName != null ? shortName.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (legislationReference != null ? legislationReference.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + order;
        result = 31 * result + (enableECertis != null ? enableECertis.hashCode() : 0);
        result = 31 * result + (criteriaCategorie != null ? criteriaCategorie.hashCode() : 0);
        result = 31 * result + (requirementGroupList != null ? requirementGroupList.hashCode() : 0);
        result = 31 * result + (idCriteriaResponse != null ? idCriteriaResponse.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CriteriaDTO{" +
                "id=" + id +
                ", uuid='" + uuid + '\'' +
                ", shortName='" + shortName + '\'' +
                ", description='" + description + '\'' +
                ", legislationReference=" + legislationReference +
                ", code='" + code + '\'' +
                ", order=" + order + ", enableECertis=" + enableECertis +
                ", criteriaCategorie=" + criteriaCategorie +
                ", requirementGroupList=" + requirementGroupList +
                ", idCriteriaResponse=" + idCriteriaResponse +
                '}';
    }
}
