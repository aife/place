package com.atexo.dume.dto.sn.essentialData;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DonneesAnnuellesDTO {

    private String datePublicationDonneesExecution;
    private BigDecimal depensesInvestissement;
    private List<TarifDTO> tarifs;

    public String getDatePublicationDonneesExecution() {
        return datePublicationDonneesExecution;
    }

    public void setDatePublicationDonneesExecution(String value) {
        this.datePublicationDonneesExecution = value;
    }

    public BigDecimal getDepensesInvestissement() {
        return depensesInvestissement;
    }

    public void setDepensesInvestissement(BigDecimal value) {
        this.depensesInvestissement = value;
    }

    public List<TarifDTO> getTarifs() {
        return tarifs;
    }

    public void setTarifs(List<TarifDTO> tarifs) {
        this.tarifs = tarifs;
    }
}
