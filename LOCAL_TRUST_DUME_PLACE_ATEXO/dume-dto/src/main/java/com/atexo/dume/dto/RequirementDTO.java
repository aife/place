package com.atexo.dume.dto;

import java.io.Serializable;

public class RequirementDTO implements Serializable {

    private Long id;

    private String uuid;

    private String description;

    private String responseType;

    private ResponseDTO responseDTO;

    private Long idRequirementResponse;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public ResponseDTO getResponseDTO() {
        return responseDTO;
    }

    public void setResponseDTO(ResponseDTO responseDTO) {
        this.responseDTO = responseDTO;
    }

    public Long getIdRequirementResponse() {
        return idRequirementResponse;
    }

    public void setIdRequirementResponse(Long idRequirementResponse) {
        this.idRequirementResponse = idRequirementResponse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequirementDTO)) {
            return false;
        }

        RequirementDTO that = (RequirementDTO) o;

        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (uuid != null ? !uuid.equals(that.uuid) : that.uuid != null) {
            return false;
        }
        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        if (responseType != null ? !responseType.equals(that.responseType) : that.responseType != null) {
            return false;
        }
        if (idRequirementResponse != null ? !idRequirementResponse.equals(that.idRequirementResponse) : that.idRequirementResponse != null) {
            return false;
        }
        return responseDTO != null ? responseDTO.equals(that.responseDTO) : that.responseDTO == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (uuid != null ? uuid.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (responseType != null ? responseType.hashCode() : 0);
        result = 31 * result + (responseDTO != null ? responseDTO.hashCode() : 0);
        result = 31 * result + (idRequirementResponse != null ? idRequirementResponse.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RequirementDTO{" +
                "id=" + id +
                ", uuid='" + uuid + '\'' +
                ", description='" + description + '\'' +
                ", responseType='" + responseType + '\'' +
                ", responseDTOList=" + responseDTO +
                ", idRequirementResponse=" + idRequirementResponse +
                '}';
    }
}
