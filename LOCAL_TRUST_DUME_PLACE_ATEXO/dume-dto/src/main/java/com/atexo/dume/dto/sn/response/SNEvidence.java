
package com.atexo.dume.dto.sn.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "uuid",
    "texte",
    "Publié par "
})
public class SNEvidence {

    @JsonProperty("uuid")
    private String uuid;
    @JsonProperty("texte")
    private String texte;
    @JsonProperty("Publié par ")
    private String publiPar;

    @JsonProperty("uuid")
    public String getUuid() {
        return uuid;
    }

    @JsonProperty("uuid")
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @JsonProperty("texte")
    public String getTexte() {
        return texte;
    }

    @JsonProperty("texte")
    public void setTexte(String texte) {
        this.texte = texte;
    }

    @JsonProperty("Publié par ")
    public String getPubliPar() {
        return publiPar;
    }

    @JsonProperty("Publié par ")
    public void setPubliPar(String publiPar) {
        this.publiPar = publiPar;
    }

}
