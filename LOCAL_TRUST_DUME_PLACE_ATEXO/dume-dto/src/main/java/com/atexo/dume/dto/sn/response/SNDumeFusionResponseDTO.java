package com.atexo.dume.dto.sn.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.Valid;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder("response")
public class SNDumeFusionResponseDTO {

    @JsonProperty("response")
    @Valid
    private SNFusionResponse response;

    public SNDumeFusionResponseDTO() {
        // Constructeur par défaut
    }
    @JsonProperty("response")
    public SNFusionResponse getResponse() {
        return response;
    }

    @JsonProperty("response")
    public void setResponse(SNFusionResponse response) {
        this.response = response;
    }
}
