
package com.atexo.dume.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ECertisResponseDTO {
    @JsonProperty("criterion")
    private CriterionDTO criterion;


    @JsonProperty("evidences")
    private List<EvidenceDTO> evidences;


    public CriterionDTO getCriterion() {
        return criterion;
    }

    public void setCriterion(CriterionDTO criterion) {
        this.criterion = criterion;
    }

    public List<EvidenceDTO> getEvidences() {
        return evidences;
    }

    public void setEvidences(List<EvidenceDTO> evidences) {
        this.evidences = evidences;
    }
}
