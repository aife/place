package com.atexo.dume.dto.sn.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SNOauth2Token {


    public SNOauth2Token() {

    }


    public SNOauth2Token(String accessToken, Long expiresIn, LocalDateTime dateRecuperation) {
        super();
        this.accessToken = accessToken;
        this.expiresIn = expiresIn;
        this.dateRecuperation = dateRecuperation;
    }

    @JsonProperty("access_token")
    private String accessToken;

    @JsonProperty("token_type")
    private String tokenType;

    @JsonProperty("expires_in")
    private Long expiresIn;

    @JsonProperty("scope")
    private String scope;

    @JsonProperty("error_description")
    private String errorDescription;

    @JsonProperty("error")
    private String error;

    private LocalDateTime dateRecuperation;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public Long getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Long expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public LocalDateTime getDateRecuperation() {
        return dateRecuperation;
    }

    public void setDateRecuperation(LocalDateTime dateRecuperation) {
        this.dateRecuperation = dateRecuperation;
    }

    @Override
    public String toString() {
        return "SNOauth2Token{" +
                "accessToken='" + accessToken + '\'' +
                ", tokenType='" + tokenType + '\'' +
                ", expiresIn=" + expiresIn +
                ", scope='" + scope + '\'' +
                ", errorDescription='" + errorDescription + '\'' +
                ", error='" + error + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SNOauth2Token that = (SNOauth2Token) o;
        return Objects.equals(accessToken, that.accessToken) && Objects.equals(expiresIn, that.expiresIn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accessToken, expiresIn);
    }
}
