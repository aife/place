package com.atexo.dume.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PublishedDume {

    @JsonProperty("numeroDumeSN")
    private String dumeSNIdentifier;

    @JsonProperty("numeroLot")
    private String lotNumber;

    public PublishedDume(String dumeSNIdentifier, String lotNumber) {
        this.dumeSNIdentifier = dumeSNIdentifier;
        this.lotNumber = lotNumber;
    }

    public PublishedDume() {
    }

    public String getDumeSNIdentifier() {
        return dumeSNIdentifier;
    }

    public void setDumeSNIdentifier(String dumeSNIdentifier) {
        this.dumeSNIdentifier = dumeSNIdentifier;
    }

    public String getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PublishedDume that = (PublishedDume) o;

        if (dumeSNIdentifier != null ? !dumeSNIdentifier.equals(that.dumeSNIdentifier) : that.dumeSNIdentifier != null)
            return false;
        return lotNumber != null ? lotNumber.equals(that.lotNumber) : that.lotNumber == null;
    }

    @Override
    public int hashCode() {
        int result = dumeSNIdentifier != null ? dumeSNIdentifier.hashCode() : 0;
        result = 31 * result + (lotNumber != null ? lotNumber.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PublishedDume{" +
                "dumeSNIdentifier='" + dumeSNIdentifier + '\'' +
                ", lotNumber='" + lotNumber + '\'' +
                '}';
    }
}
