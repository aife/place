package com.atexo.dume.dto.csv.dto;

public class RequirementCSVDTO {
    String uuid;

    String description;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequirementCSVDTO)) {
            return false;
        }

        RequirementCSVDTO that = (RequirementCSVDTO) o;

        if (uuid != null ? !uuid.equals(that.uuid) : that.uuid != null) {
            return false;
        }
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = uuid != null ? uuid.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RequirementCSVDTO{" +
                "uuid='" + uuid + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
