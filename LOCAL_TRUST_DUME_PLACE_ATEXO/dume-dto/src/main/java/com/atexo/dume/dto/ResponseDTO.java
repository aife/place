package com.atexo.dume.dto;

import com.atexo.dume.toolbox.serialization.LocalDateDeserializer;
import com.atexo.dume.toolbox.serialization.LocalDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

public class ResponseDTO implements Serializable {

    private Long id;

    private String description;

    private Boolean indicator;

    private String code;

    private BigDecimal amount;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate date;

    private BigDecimal percent;

    private BigDecimal quantity;

    private String evidenceUrl;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIndicator() {
        return indicator;
    }

    public void setIndicator(Boolean indicator) {
        this.indicator = indicator;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public BigDecimal getPercent() {
        return percent;
    }

    public void setPercent(BigDecimal percent) {
        this.percent = percent;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getEvidenceUrl() {
        return evidenceUrl;
    }

    public void setEvidenceUrl(String evidenceUrl) {
        this.evidenceUrl = evidenceUrl;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ResponseDTO)) {
            return false;
        }

        ResponseDTO that = (ResponseDTO) o;

        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }

        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        if (indicator != null ? !indicator.equals(that.indicator) : that.indicator != null) {
            return false;
        }
        if (code != null ? !code.equals(that.code) : that.code != null) {
            return false;
        }
        if (amount != null ? !amount.equals(that.amount) : that.amount != null) {
            return false;
        }
        if (date != null ? !date.equals(that.date) : that.date != null) {
            return false;
        }
        if (percent != null ? !percent.equals(that.percent) : that.percent != null) {
            return false;
        }
        if (quantity != null ? !quantity.equals(that.quantity) : that.quantity != null) {
            return false;
        }
        return evidenceUrl != null ? evidenceUrl.equals(that.evidenceUrl) : that.evidenceUrl == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (indicator != null ? indicator.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (amount != null ? amount.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (percent != null ? percent.hashCode() : 0);
        result = 31 * result + (quantity != null ? quantity.hashCode() : 0);
        result = 31 * result + (evidenceUrl != null ? evidenceUrl.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ResponseDTO{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", indicator=" + indicator +
                ", code='" + code + '\'' +
                ", amount=" + amount +
                ", date=" + date +
                ", percent=" + percent +
                ", quantity=" + quantity +
                ", evidenceUrl='" + evidenceUrl + '\'' +
                '}';
    }
}
