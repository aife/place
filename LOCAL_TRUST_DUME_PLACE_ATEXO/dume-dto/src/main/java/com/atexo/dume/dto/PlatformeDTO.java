package com.atexo.dume.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.atexo.dume.toolbox.consts.DumeJsonConstants.PLATFORM_IDENTIFIER;
import static com.atexo.dume.toolbox.consts.DumeJsonConstants.PLATFORM_IDENTIFIER_TYPE;
import static com.atexo.dume.toolbox.consts.DumeJsonConstants.PLATFORM_TECHNICAL_IDENTIFIER;

public class PlatformeDTO {

    @JsonProperty(PLATFORM_IDENTIFIER)
    private String platformIdentifier;

    @JsonProperty(PLATFORM_TECHNICAL_IDENTIFIER)
    private String platformTechnicalIdentifier;

    @JsonProperty(PLATFORM_IDENTIFIER_TYPE)
    private String platformIdentifierType;

    public PlatformeDTO() {
    }

    public PlatformeDTO(String platformIdentifier, String platformTechnicalIdentifier, String platformIdentifierType) {
        this.platformIdentifier = platformIdentifier;
        this.platformTechnicalIdentifier = platformTechnicalIdentifier;
        this.platformIdentifierType = platformIdentifierType;
    }

    public String getPlatformIdentifier() {
        return platformIdentifier;
    }

    public void setPlatformIdentifier(String platformIdentifier) {
        this.platformIdentifier = platformIdentifier;
    }

    public String getPlatformTechnicalIdentifier() {
        return platformTechnicalIdentifier;
    }

    public void setPlatformTechnicalIdentifier(String platformTechnicalIdentifier) {
        this.platformTechnicalIdentifier = platformTechnicalIdentifier;
    }

    public String getPlatformIdentifierType() {
        return platformIdentifierType;
    }

    public void setPlatformIdentifierType(String platformIdentifierType) {
        this.platformIdentifierType = platformIdentifierType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PlatformeDTO)) {
            return false;
        }

        final PlatformeDTO that = (PlatformeDTO) o;

        if (!getPlatformIdentifier().equals(that.getPlatformIdentifier())) {
            return false;
        }
        if (!getPlatformTechnicalIdentifier().equals(that.getPlatformTechnicalIdentifier())) {
            return false;
        }
        return getPlatformIdentifierType() == that.getPlatformIdentifierType();
    }

    @Override
    public int hashCode() {
        int result = getPlatformIdentifier().hashCode();
        result = 31 * result + getPlatformTechnicalIdentifier().hashCode();
        result = 31 * result + getPlatformIdentifierType().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "PlatformeDTO{" +
                "platformIdentifier=" + platformIdentifier +
                ", platformTechnicalIdentifier='" + platformTechnicalIdentifier + '\'' +
                ", platformIdentifierType=" + platformIdentifierType +
                '}';
    }
}
