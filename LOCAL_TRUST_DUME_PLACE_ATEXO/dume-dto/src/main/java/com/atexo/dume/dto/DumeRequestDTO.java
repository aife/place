package com.atexo.dume.dto;

import java.io.Serializable;
import java.util.List;

/**
 * DTO du Dume Acheteur. Celui-ci ne contient uniquement les codes des critères sélectionnés pour un Dume.
 */
public class DumeRequestDTO implements Serializable {

    private Long id;

    private List<CriteriaLotDTO> selectionCriteriaCodes;

    private List<String> exclusionCriteriaCodes;

    private Boolean confirmed;

    public DumeRequestDTO() {
    }

    public DumeRequestDTO(final Long identifier) {
        this.id = identifier;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<CriteriaLotDTO> getSelectionCriteriaCodes() {
        return selectionCriteriaCodes;
    }

    public void setSelectionCriteriaCodes(List<CriteriaLotDTO> selectionCriteriaDTOS) {
        this.selectionCriteriaCodes = selectionCriteriaDTOS;
    }

    public List<String> getExclusionCriteriaCodes() {
        return exclusionCriteriaCodes;
    }

    public void setExclusionCriteriaCodes(List<String> exclusionCriteriaDTOS) {
        this.exclusionCriteriaCodes = exclusionCriteriaDTOS;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DumeRequestDTO)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        DumeRequestDTO that = (DumeRequestDTO) o;

        if (selectionCriteriaCodes != null ? !selectionCriteriaCodes.equals(that.selectionCriteriaCodes) : that.selectionCriteriaCodes != null) {
            return false;
        }
        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (confirmed != null ? !confirmed.equals(that.confirmed) : that.confirmed != null) {
            return false;
        }
        return exclusionCriteriaCodes != null ? exclusionCriteriaCodes.equals(that.exclusionCriteriaCodes) : that.exclusionCriteriaCodes == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (selectionCriteriaCodes != null ? selectionCriteriaCodes.hashCode() : 0);
        result = 31 * result + (exclusionCriteriaCodes != null ? exclusionCriteriaCodes.hashCode() : 0);
        result = 31 * result + (confirmed != null ? confirmed.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DumeRequestDTO{" +
                "id=" + id +
                "selectionCriteriaCodes=" + selectionCriteriaCodes +
                ", exclusionCriteriaCodes=" + exclusionCriteriaCodes +
                ", confirmed=" + confirmed +
                "}";
    }
}
