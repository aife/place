package com.atexo.dume.dto.sn.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SNConsultationResponse {

    @JsonProperty("Id")
    private String id;
    @JsonProperty("referenceFonctionnelle")
    private String referenceFonctionnelle;
    @JsonProperty("idTypeProcedure")
    private String idTypeProcedure;
    @JsonProperty("idNatureMarket")
    private String idNatureMarket;
    @JsonProperty("idTypeMarket")
    private String idTypeMarket;

    public SNConsultationResponse() {
        // Constructeur par défaut
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReferenceFonctionnelle() {
        return referenceFonctionnelle;
    }

    public void setReferenceFonctionnelle(String referenceFonctionnelle) {
        this.referenceFonctionnelle = referenceFonctionnelle;
    }

    public String getIdTypeProcedure() {
        return idTypeProcedure;
    }

    public void setIdTypeProcedure(String idTypeProcedure) {
        this.idTypeProcedure = idTypeProcedure;
    }

    public String getIdNatureMarket() {
        return idNatureMarket;
    }

    public void setIdNatureMarket(String idNatureMarket) {
        this.idNatureMarket = idNatureMarket;
    }

    public String getIdTypeMarket() {
        return idTypeMarket;
    }

    public void setIdTypeMarket(String idTypeMarket) {
        this.idTypeMarket = idTypeMarket;
    }

}
