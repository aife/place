package com.atexo.dume.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Set;

import static com.atexo.dume.toolbox.consts.DumeJsonConstants.*;

/**
 * DumeRequestContextDTO Defines the context inherited from the 3erd party
 */

public class DumeRequestContextDTO implements Serializable {

    @JsonProperty(IDCONTEXT)
    private Long id;

    @JsonProperty(VERSION)
    private int version;

    @JsonProperty(NOM_OFFICIEL)
    private String officialName;

    @JsonProperty(PAYS)
    private String country;

    @JsonProperty(REFERENCE_CONSULTATION)
    private String consultRef;

    @JsonProperty(TYPE_PROCEDURE)
    private String procedureType;

    @JsonProperty(INTITULE)
    private String entitled;

    @JsonProperty(OBJET)
    private String object;

    @JsonProperty(METADATA)
    private MetaDataDTO metadata;

    @JsonProperty(LOTS)
    private Set<LotDTO> lotDTOSet;

    @JsonProperty(DUME_ACHETEURS)
    private Set<DumeRequestDTO> dumeRequestSets;

    @JsonProperty(CONTACT)
    private EconomicPartyInfoDTO economicPartyInfo;

    private Boolean isDefault;

    private Boolean isApplyMarketFilter;

    public DumeRequestContextDTO() {
    }

    public DumeRequestContextDTO(final String officialName, final String country, final String consultRef,
                                 final String procedureType, final String entitled,
                                 final String object, final Boolean isDefault) {
        this.officialName = officialName;
        this.country = country;
        this.consultRef = consultRef;
        this.procedureType = procedureType;
        this.entitled = entitled;
        this.object = object;
        this.isDefault = isDefault;
    }

    public Set<DumeRequestDTO> getDumeRequestSets() {
        return dumeRequestSets;
    }

    public void setDumeRequestSets(Set<DumeRequestDTO> dumeRequestSets) {
        this.dumeRequestSets = dumeRequestSets;
    }

    public String getOfficialName() {
        return officialName;
    }

    public void setOfficialName(String officialName) {
        this.officialName = officialName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getConsultRef() {
        return consultRef;
    }

    public void setConsultRef(String consultRef) {
        this.consultRef = consultRef;
    }

    public String getProcedureType() {
        return procedureType;
    }

    public void setProcedureType(String procedureType) {
        this.procedureType = procedureType;
    }

    public String getEntitled() {
        return entitled;
    }

    public void setEntitled(String entitled) {
        this.entitled = entitled;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public Boolean getDefault() {
        return isDefault;
    }

    public void setDefault(Boolean aDefault) {
        isDefault = aDefault;
    }

    public Boolean getApplyMarketFilter() {
        return isApplyMarketFilter;
    }

    public void setApplyMarketFilter(Boolean applyMarketFilter) {
        isApplyMarketFilter = applyMarketFilter;
    }

    public MetaDataDTO getMetadata() {
        return metadata;
    }

    public void setMetadata(MetaDataDTO metadata) {
        this.metadata = metadata;
    }

    public Set<LotDTO> getLotDTOSet() {
        return lotDTOSet;
    }

    public void setLotDTOSet(Set<LotDTO> lotDTOSet) {
        this.lotDTOSet = lotDTOSet;
    }

    public EconomicPartyInfoDTO getEconomicPartyInfo() {
        return economicPartyInfo;
    }

    public void setEconomicPartyInfo(EconomicPartyInfoDTO economicPartyInfo) {
        this.economicPartyInfo = economicPartyInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DumeRequestContextDTO)) {
            return false;
        }

        DumeRequestContextDTO that = (DumeRequestContextDTO) o;

        if (version != that.version) {
            return false;
        }
        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (officialName != null ? !officialName.equals(that.officialName) : that.officialName != null) {
            return false;
        }
        if (country != null ? !country.equals(that.country) : that.country != null) {
            return false;
        }
        if (consultRef != null ? !consultRef.equals(that.consultRef) : that.consultRef != null) {
            return false;
        }
        if (procedureType != null ? !procedureType.equals(that.procedureType) : that.procedureType != null) {
            return false;
        }
        if (entitled != null ? !entitled.equals(that.entitled) : that.entitled != null) {
            return false;
        }
        if (object != null ? !object.equals(that.object) : that.object != null) {
            return false;
        }
        if (metadata != null ? !metadata.equals(that.metadata) : that.metadata != null) {
            return false;
        }

        if (isDefault != null ? !isDefault.equals(that.isDefault) : that.isDefault != null) {
            return false;
        }
        if (isApplyMarketFilter != null ? !isApplyMarketFilter.equals(that.isApplyMarketFilter) : that.isApplyMarketFilter != null) {
            return false;
        }
        if (lotDTOSet != null ? !lotDTOSet.equals(that.lotDTOSet) : that.lotDTOSet != null) {
            return false;
        }
        return dumeRequestSets != null ? dumeRequestSets.equals(that.dumeRequestSets) : that.dumeRequestSets == null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        int result = getOfficialName().hashCode();
        result = 31 * result + getCountry().hashCode();
        result = 31 * result + getConsultRef().hashCode();
        result = 31 * result + getProcedureType().hashCode();
        result = 31 * result + getEntitled().hashCode();
        result = 31 * result + getObject().hashCode();
        result = 31 * result + getDefault().hashCode();
        result = 31 * result + getMetadata().hashCode();
        result = 31 * result + getLotDTOSet().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "DumeRequestContextDTO{" +
                "officialName='" + officialName + '\'' +
                ", country='" + country + '\'' +
                ", consultRef='" + consultRef + '\'' +
                ", procedureType='" + procedureType + '\'' +
                ", entitled='" + entitled + '\'' +
                ", object='" + object + '\'' +
                ", isDefault='" + isDefault + '\'' +
                ", isApplyMarketFilter='" + isApplyMarketFilter + '\'' +
                '}';
    }
}
