package com.atexo.dume.dto;

import com.atexo.dume.toolbox.serialization.LocalDateDeserializer;
import com.atexo.dume.toolbox.serialization.LocalDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.time.LocalDate;

public class RepresentantLegalDTO implements Serializable {

    private Long id;

    private String prenom;

    private String nom;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate dateNaissance;

    private String lieuNaissance;

    private String rue;

    private String codePostal;

    private String ville;

    private EnumDTO pays;

    private String fonction;

    private String email;

    private String telephone;

    private String details;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public EnumDTO getPays() {
        return pays;
    }

    public void setPays(EnumDTO pays) {
        this.pays = pays;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RepresentantLegalDTO)) {
            return false;
        }

        RepresentantLegalDTO that = (RepresentantLegalDTO) o;

        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (prenom != null ? !prenom.equals(that.prenom) : that.prenom != null) {
            return false;
        }
        if (nom != null ? !nom.equals(that.nom) : that.nom != null) {
            return false;
        }
        if (dateNaissance != null ? !dateNaissance.equals(that.dateNaissance) : that.dateNaissance != null) {
            return false;
        }
        if (lieuNaissance != null ? !lieuNaissance.equals(that.lieuNaissance) : that.lieuNaissance != null) {
            return false;
        }
        if (rue != null ? !rue.equals(that.rue) : that.rue != null) {
            return false;
        }
        if (codePostal != null ? !codePostal.equals(that.codePostal) : that.codePostal != null) {
            return false;
        }
        if (ville != null ? !ville.equals(that.ville) : that.ville != null) {
            return false;
        }
        if (pays != null ? !pays.equals(that.pays) : that.pays != null) {
            return false;
        }
        if (fonction != null ? !fonction.equals(that.fonction) : that.fonction != null) {
            return false;
        }
        if (email != null ? !email.equals(that.email) : that.email != null) {
            return false;
        }
        if (telephone != null ? !telephone.equals(that.telephone) : that.telephone != null) {
            return false;
        }
        return details != null ? details.equals(that.details) : that.details == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (prenom != null ? prenom.hashCode() : 0);
        result = 31 * result + (nom != null ? nom.hashCode() : 0);
        result = 31 * result + (dateNaissance != null ? dateNaissance.hashCode() : 0);
        result = 31 * result + (lieuNaissance != null ? lieuNaissance.hashCode() : 0);
        result = 31 * result + (rue != null ? rue.hashCode() : 0);
        result = 31 * result + (codePostal != null ? codePostal.hashCode() : 0);
        result = 31 * result + (ville != null ? ville.hashCode() : 0);
        result = 31 * result + (pays != null ? pays.hashCode() : 0);
        result = 31 * result + (fonction != null ? fonction.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (telephone != null ? telephone.hashCode() : 0);
        result = 31 * result + (details != null ? details.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RepresentantLegalDTO{" +
                "id=" + id +
                ", prenom='" + prenom + '\'' +
                ", nom='" + nom + '\'' +
                ", dateNaissance=" + dateNaissance +
                ", lieuNaissance=" + lieuNaissance +
                ", rue='" + rue + '\'' +
                ", codePostal='" + codePostal + '\'' +
                ", ville='" + ville + '\'' +
                ", pays='" + pays + '\'' +
                ", fonction='" + fonction + '\'' +
                ", email='" + email + '\'' +
                ", telephone='" + telephone + '\'' +
                ", details='" + details + '\'' +
                '}';
    }
}
