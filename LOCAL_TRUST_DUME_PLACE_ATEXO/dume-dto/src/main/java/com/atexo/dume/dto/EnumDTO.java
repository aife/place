package com.atexo.dume.dto;

import java.io.Serializable;

public class EnumDTO implements Serializable {

    private String code;

    private String label;

    public EnumDTO() {
    }

    public EnumDTO(String code, String label) {
        this.code = code;
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EnumDTO)) {
            return false;
        }

        EnumDTO enumDTO = (EnumDTO) o;

        if (code != null ? !code.equals(enumDTO.code) : enumDTO.code != null) {
            return false;
        }
        return label != null ? label.equals(enumDTO.label) : enumDTO.label == null;
    }

    @Override
    public int hashCode() {
        int result = code != null ? code.hashCode() : 0;
        result = 31 * result + (label != null ? label.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EnumDTO{" +
                "code='" + code + '\'' +
                ", label='" + label + '\'' +
                '}';
    }
}
