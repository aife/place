package com.atexo.dume.dto.sn.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"messageList", "dumeOE"})
public class SNDumeOEResponse {

    @JsonProperty("xmlDume")
    private String xmlDume;

    @JsonProperty("dumeOEId")
    private String dumeOEId;
    @JsonProperty("statutDume")
    private String statutDume;
    @JsonProperty("accessible")
    private boolean accessible;
    @JsonProperty("Date de création")
    private String dateCreation;
    @JsonProperty("Date de modification")
    private String dateModification;

    public SNDumeOEResponse() {
        // Constructeur par défaut
    }

    public String getXmlDume() {
        return xmlDume;
    }

    public void setXmlDume(String xmlDume) {
        this.xmlDume = xmlDume;
    }

    public String getDumeOEId() {
        return dumeOEId;
    }

    public void setDumeOEId(String dumeOEId) {
        this.dumeOEId = dumeOEId;
    }

    public String getStatutDume() {
        return statutDume;
    }

    public void setStatutDume(String statutDume) {
        this.statutDume = statutDume;
    }

    public boolean isAccessible() {
        return accessible;
    }

    public void setAccessible(boolean accessible) {
        this.accessible = accessible;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getDateModification() {
        return dateModification;
    }

    public void setDateModification(String dateModification) {
        this.dateModification = dateModification;
    }

}
