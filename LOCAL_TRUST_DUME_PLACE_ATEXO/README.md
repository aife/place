![Build status](https://gitlab.local-trust.com/transversal/lt_dume/badges/master/build.svg)
[![Coverage](http://172.16.0.18:8080/sonar/api/badges/measure?key=com.atexo.dume:dume-root&metric=coverage)](http://172.16.0.18:8080/sonar/component_measures/metric/coverage/list?id=com.atexo.dume:dume-root)
[![Java Version](https://img.shields.io/badge/Java%20Development%20Kit-8%2B-blue.svg?style=flat)](http://www.oracle.com/technetwork/java/javase/downloads/index.html) 
[![Spring Boot](https://img.shields.io/badge/Spring%20Boot-2.0.0.RELEASE-green.svg?style=flat)](http://projects.spring.io/spring-boot) 
[![Maven](https://img.shields.io/badge/Maven-3.0%2B-blue.svg?style=flat)](https://maven.apache.org) 
# DUME (Document unique de marché européen)


Le Document Unique de Marché Européen (DUME ou ESPD en anglais) est une déclaration sur l’honneur harmonisée et élaborée sur la base d’un formulaire type établi par la Commission européenne. Ce formulaire est utilisé dans les procédures de passation des marchés publics, à la fois par les acheteurs publics (pouvoirs adjudicateurs ou entités adjudicatrices) et les opérateurs économiques de l'Union Européenne.

Le document unique de marché européen (DUME) devrait permettre d’alléger la production de certificats ou autres documents relatifs aux critères d'exclusion et de sélection. Il devrait également fournir les informations pertinentes concernant les entités aux capacités desquelles l'opérateur économique a recours.

En produisant un DUME complété, les soumissionnaires n'ont plus besoin de fournir les différents formulaires (Formulaire DC1 : Lettre de candidature - Désignation du mandataire par ses cotraitants, Formulaire DC2 : Déclaration du candidat individuel ou du membre du groupement, ...) utilisés lors de la procédure de passation du marché.

LT-DUME est un projet visant à s'intégrer à MPE pour proposer la création et le remplissage en ligne d'un formulaire permettant la génération d'un DUME et de l'attacher à un appel d'offre ou à sa réponse. LT-DUME s'interface également avec un service tiers appelé Service National (SN). Le SN permet de centraliser la gestion des dumes et de proposer d'autres fonctionnalités comme la génération de PDF ou la recherche de DUME.

## Données essentielles (DE)

Il s'agit des données essentielles relatives à la commande publique mises à disposition sur le profil d’acheteur qui distinguent :

- les données essentielles relatives aux marchés publics relatives au décret n° 2016-360 du 25 mars 2016 modifié relatif aux marchés publics, notamment son article 107,
- les données essentielles relatives aux marchés publics de défense ou de sécurité relatives au décret n° 2016-361 du 25 mars 2016 modifié relatif aux marchés publics de défense ou de sécurité, notamment son article 94,
- les données essentielles relatives aux contrats de concession relatives au décret n° 2016-86 du 1er février 2016 relatif aux contrats de concession, notamment son article 34.

## Instructions d'installation

Ces instructions ne sont valables que pour développer et tester le projet en local.

### Pré-requis

    Maven 3, JAVA 8, Node 8.11.1+, NPM 5.6.0+

### Cloner

Cloner le projet via git ou l'IDE.

```shell
git clone git@gitlab.local-trust.com:transversal/lt_dume.git
```

### Build

Builder l'ensemble de l'application avec maven

```shell
mvn clean install
```

### Configuration

Le fichier [`dume-database.properties`](dume-config/src/main/resources/dume-database.properties) contient les informations de connexion à la base de données.

LT-DUME utilise une table de configuration en base de données (`DUME_PROPERTIES`). Les valeurs par défaut devraient être suffisantes pour un LT-DUME opérationnel en dev.

L'appel de création de dume acheteur au backend demande un identifiant de plateforme particulier. Le mode dev du frontend utilise un nom de plateforme par défaut
qu'il est nécessaire d'ajouter en base pour pouvoir correctement effectuer cet appel :

```sql
INSERT INTO `DUME_PLATFORM` (`ID`, `VERSION`, `PLAT_DESC`, `PLAT_LABEL`, `PLATFORM_IDENTIFIER`, `SN_PLATFORM_IDENTIFIER`, `SN_PLATFORM_TECHNICAL_IDENTIFIER`) VALUES ('0', '0', 'mpe-place', 'mpe-place', 'mpe-place', '33592022900036', 'azerty');
```

### Lancer le backend

Plusieurs méthodes :

* Lancer le main de [`DumeStandaloneLauncher.java`](dume-war/src/main/java/com/atexo/dume/DumeStandaloneLauncher.java) avec le profil spring `dev`

* Lancer via un tomcat. Utiliser le profil `dev` pour prendre en compte [`dume-database.properties`](dume-config/src/main/resources/dume-database.properties). On peut également
lancer avec le profil `production`. Dans ce cas, les informations de connexion à la base seront récupérées via jndi. Il
faut donc les définir dans le fichier `context.xml` de tomcat :

    ```xml
      <Resource name="jdbc/dume" auth="Container" type="javax.sql.DataSource"
                maxTotal="100" maxIdle="30" maxWaitMillis="10000"
                username="root" password="root" driverClassName="com.mysql.jdbc.Driver"
                url="jdbc:mysql://localhost:3306/dume"/>
    ```

Si le serveur n'est lancé avec aucun profil, le profil par défaut est `production` comme défini dans [`application.properties`](dume-war/src/main/resources/application.properties).

Pour pouvoir créer un dume acheteur, il faut définir un dume modèle. On ajoutera en base un dume "domaine". Pour cela,
on appellera un WS dédié en deux étapes :

1. Récupération du token : appeler le WS d'authentification :

    ```shell
    curl 'http://localhost:8081/dume-api/oauth/token' -H 'Authorization: Basic ZHVtZS1jbGllbnQ6cGFzc3dvcmQ=' -H 'Accept: application/json' --data 'grant_type=client_credentials'
    ```

2. Copier la valeur associée à `access_token` dans la réponse de la requête précédente. Dans la requête suivante, remplacer `TOKEN` par cette valeur. Remplacer également
   le chemin d'accès au fichier uploadé (`model=@...`) par le chemin correct.
   
    ```shell
    curl 'http://localhost:8081/dume-api/model/importerXML/editeur/ATEXO' -F 'model=@dume-mocks/src/main/resources/request/espd-request.xml' -H 'Authorization: Bearer TOKEN' -H 'Accept: application/json'
    ```

### Lancer le mock du SN

Certains traitements font appel au SN. Au cas où le SN est indisponible, il existe un mock à lancer en parallèle pour
répondre aux appels au SN avec des données de test. Il peut se lancer via le main de [`ServiceNationalApplicationLauncher.java`](dume-mocks/src/main/java/com/atexo/dume/mocks/ServiceNationalApplicationLauncher.java).
Il suffit ensuite de faire pointer LT-DUME sur le serveur mock. Pour cela, modifier la table `DUME_PROPERTIES` :

    SN_BASE_PATH            http://localhost:8090/dume/
    dume.sn.via.proxy       false

IDEA a besoin d'importer les projets maven avant de compiler. Comme le module `dume-mocks` est détaché de la structure maven
du projet, si on veut importer les projets dans ce module lorsqu'on clique sur "Reimport All Maven Projets", il faut l'ajouter
manuellement dans le panel Maven Projects (Bouton '+' "Add Maven Projects")

### Lancer le frontend

Installer angular-cli de manière globale.

```shell
npm install -g @angular/cli
```

Installer tous les paquets npm si ce n'a pas déjà été fait par `mvn clean install`.

```shell
npm i
```

Lancer le serveur front.

```shell
npm start
```

La partie angular fonctionne selon un environnement de dev ou de prod. Le serveur d'angular-cli se lance automatiquement
en mode dev. Dans ce mode, un router et une page supplémentaire `/fakeMpe` sont ajoutés pour permettre d'accéder aux
différentes pages de l'application facilement. Le mode prod est destiné à être intégré dans une page MPE.

Grâce à l'intégration dans une page MPE, le front hérite normalement des styles de MPE. En l'absence d'intégration,
comme lorsqu'on est en mode dev, les styles sont absents. Il faut les lier manuellement dans la page index.html. TODO : ce
procédé doit être amélioré : [DUME-375](https://jira.local-trust.com/browse/DUME-375)

## Documentation

La documentation du projet se décompose en :

* README.md : ce fichier, qui décrit comment prendre en main le projet ;
* [CONTRIBUTING.md](CONTRIBUTING.md) : guide de développement et introduction à la structure du projet ;
* [DEPLOY.md](DEPLOY.md) : guide de déploiement du projet sur une plateforme dédiée ;
* [Pages Confluence](https://confluence.local-trust.com/display/PD/Documentation) : des diagrammes de séquence ainsi que d'autres documentations.

Il est important de mettre à jour ces documentations, notamment si les processus de déploiement ou d'installation changent.

## Déploiement

Se référer à [DEPLOY.md](DEPLOY.md)

## Développer

Se référer à [CONTRIBUTING.md](CONTRIBUTING.md)

## Remerciements

À l'équipe LT-DUME et MPE

À Didi, le meilleur chef de produit
