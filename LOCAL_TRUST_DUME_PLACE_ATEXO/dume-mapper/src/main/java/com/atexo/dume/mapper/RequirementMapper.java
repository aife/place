package com.atexo.dume.mapper;

import com.atexo.dume.dto.RequirementDTO;
import com.atexo.dume.model.Requirement;
import com.atexo.dume.model.RequirementGroupResponse;
import com.atexo.dume.model.RequirementResponse;
import com.atexo.dume.model.Response;
import com.atexo.dume.toolbox.consts.CustomResponseDataType;
import isa.names.specification.ubl.schema.xsd.ccv_commonaggregatecomponents_1.RequirementGroupType;
import isa.names.specification.ubl.schema.xsd.ccv_commonaggregatecomponents_1.RequirementType;
import isa.names.specification.ubl.schema.xsd.ccv_commonaggregatecomponents_1.ResponseType;
import isa.names.specification.ubl.schema.xsd.ccv_commonbasiccomponents_1.IndicatorType;
import isa.names.specification.ubl.schema.xsd.cev_commonaggregatecomponents_1.EvidenceType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.AttachmentType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.DocumentReferenceType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.ExternalReferenceType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PeriodType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.AmountType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DateType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PercentType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.QuantityType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TypeCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.URIType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.springframework.util.CollectionUtils;

import java.util.Optional;
import java.util.UUID;

import static com.atexo.dume.mapper.XMLConstants.CRITERION_RELATED_IDS;
import static com.atexo.dume.mapper.XMLConstants.EU_COM_GROW;
import static com.atexo.dume.mapper.XMLConstants.V_1_0;

@Mapper
public interface RequirementMapper {

    RequirementDTO toDTO(Requirement requirement);

    Requirement toEntity(RequirementDTO requirementDTO);

    default RequirementDTO toDTO(RequirementResponse requirementResponse) {
        if (requirementResponse == null) {
            return null;
        }

        ResponseMapper responseMapper = Mappers.getMapper(ResponseMapper.class);
        RequirementDTO requirementDTO = toDTO(requirementResponse.getRequirement());
        requirementDTO.setResponseDTO(responseMapper.toResponseDto(requirementResponse.getResponse()));
        requirementDTO.setIdRequirementResponse(requirementResponse.getId());

        return requirementDTO;
    }

    default void toEntityRequirementResponse(RequirementResponse requirementResponse, RequirementDTO requirementDTO) {
        ResponseMapper responseMapper = Mappers.getMapper(ResponseMapper.class);
        responseMapper.updateResponseFromDTO(requirementDTO.getResponseDTO(), requirementResponse.getResponse());
    }

    // XML
    // Requirement

    @Mapping(target = "responseDataType", source = "responseType")
    @Mapping(target = "ID.schemeID", constant = CRITERION_RELATED_IDS)
    @Mapping(target = "ID.schemeAgencyID", constant = EU_COM_GROW)
    @Mapping(target = "ID.schemeVersionID", constant = V_1_0)
    @Mapping(target = "ID.value", source = "uuid")
    @Mapping(target = "description.value", source = "description")
    RequirementType toRequirementType(Requirement requirement);


    default RequirementType toFinalRequirementWithResponse(RequirementResponse requirementResponse) {
        RequirementType requirementType = toRequirementType(requirementResponse.getRequirement());
        Response responseType = requirementResponse.getResponse();
        ResponseType response = new ResponseType();
        if (responseType.getAmount() != null) {
            AmountType amountType = new AmountType();
            amountType.setCurrencyID(responseType.getCode());
            amountType.setValue(responseType.getAmount());
            response.setAmount(amountType);
        }
        if (responseType.getIndicator() != null) {
            IndicatorType idicatorType = new IndicatorType();
            idicatorType.setValue(responseType.getIndicator());
            response.setIndicator(idicatorType);
        }

        if (responseType.getDescription() != null) {
            DescriptionType descriptionType = new DescriptionType();
            descriptionType.setValue(responseType.getDescription());
            if ("PERIOD".equals(requirementResponse.getRequirement().getResponseType())) {
                PeriodType periodType = new PeriodType();
                periodType.getDescription().add(descriptionType);
                response.setPeriod(periodType);
            } else {
                response.setDescription(descriptionType);
            }
        }

        if (responseType.getCode() != null) {
            TypeCodeType codeType = new TypeCodeType();
            codeType.setValue(responseType.getCode());
            response.setCode(codeType);
        }

        if (responseType.getDate() != null) {
            java.time.LocalDate date = responseType.getDate();
            DateType dateType = new DateType();
            dateType.setValue(new org.joda.time.LocalDate(date.getYear(), date.getMonthValue(), date.getDayOfMonth()));
            response.setDate(dateType);
        }

        if (responseType.getPercent() != null) {
            PercentType percentType = new PercentType();
            percentType.setValue(responseType.getPercent());
            response.setPercent(percentType);
        }

        if (responseType.getQuantity() != null) {
            QuantityType quantityType = new QuantityType();
            quantityType.setValue(responseType.getQuantity());
            response.setQuantity(quantityType);
        }

        if (responseType.getEvidenceUrl() != null) {
            EvidenceType evidenceType = new EvidenceType();
            response.getEvidence().add(evidenceType);
            DocumentReferenceType documentReferenceType = new DocumentReferenceType();
            evidenceType.getEvidenceDocumentReference().add(documentReferenceType);
            IDType idType = new IDType();
            idType.setSchemeAgencyID(EU_COM_GROW);
            idType.setValue(UUID.randomUUID().toString());
            documentReferenceType.setID(idType);
            AttachmentType attachmentType = new AttachmentType();
            documentReferenceType.setAttachment(attachmentType);
            ExternalReferenceType externalReferenceType = new ExternalReferenceType();
            attachmentType.setExternalReference(externalReferenceType);
            URIType uriType = new URIType();
            externalReferenceType.setURI(uriType);
            uriType.setValue(responseType.getEvidenceUrl());
        }

        requirementType.getResponse().add(response);

        return requirementType;
    }

    default void traiterRequirements(RequirementGroupType requirementGroupType, RequirementGroupResponse requirementGroupResponse, Requirement requirement) {
        if (CustomResponseDataType.INFOS.value().equals(requirement.getResponseType())
                || CustomResponseDataType.INFO_TITLE.value().equals(requirement.getResponseType())) {
            // Créer un requirementResponse et un response par défaut. Les requirements de type information
            // sont des ajouts purement graphiques qui n'apparaissent pas dans le XML. On construit quand
            // même un objet response pour ces types de requirement pour faciliter le mapping vers les
            // objets front.
            RequirementResponse requirementResponse = getRequirementResponse(requirementGroupResponse, requirement, null);
            requirementGroupResponse.getRequirementResponseList().add(requirementResponse);
        } else {

            Optional<RequirementType> requirementTypeOpt = requirementGroupType.getRequirement().stream()
                    .filter(requirementType -> requirementType.getID().getValue().trim().equals(requirement.getUuid()))
                    .findFirst();
            if (requirementTypeOpt.isPresent()) {
                RequirementResponse requirementResponse = getRequirementResponse(requirementGroupResponse, requirement, requirementTypeOpt.get());

                requirementGroupResponse.getRequirementResponseList().add(requirementResponse);
            }
        }
    }

    /**
     * Créer un objet requirementResponse lié au requirement et qui lui associe une response contenant les valeurs
     * extraites de l'objet xml
     *
     * @param requirementGroupResponse
     * @param requirement
     * @param requirementType
     * @return
     */
    default RequirementResponse getRequirementResponse(RequirementGroupResponse requirementGroupResponse, Requirement requirement, RequirementType requirementType) {
        RequirementResponse requirementResponse = new RequirementResponse();
        requirementResponse.setRequirementGroupResponse(requirementGroupResponse);
        requirementResponse.setRequirement(requirement);
        requirementResponse.setResponse(new Response());
        if (requirementType != null && !CollectionUtils.isEmpty(requirementType.getResponse())) {
            updateResponse(requirementType, requirementResponse.getResponse());
        }
        return requirementResponse;
    }

    default void updateResponse(RequirementType requirementType, Response response) {

        // on récupere le 1er
        ResponseType responseType = requirementType.getResponse().get(0);
        if (responseType.getAmount() != null) {
            response.setAmount(responseType.getAmount().getValue());
            response.setCode(responseType.getAmount().getCurrencyID());
        }
        if (responseType.getIndicator() != null) {
            response.setIndicator(responseType.getIndicator().isValue());
        }

        if (responseType.getPeriod() != null && !CollectionUtils.isEmpty(responseType.getPeriod().getDescription())) {
            response.setDescription(responseType.getPeriod().getDescription().get(0).getValue());
        }

        if (responseType.getDescription() != null) {
            response.setDescription(responseType.getDescription().getValue());
        }

        if (responseType.getCode() != null) {
            response.setCode(responseType.getCode().getValue());
        }

        if (responseType.getDate() != null) {
            org.joda.time.LocalDate joda = responseType.getDate().getValue();
            response.setDate(java.time.LocalDate.of(joda.getYear(), joda.getMonthOfYear(), joda.getDayOfMonth()));
        }

        if (responseType.getPercent() != null) {
            response.setPercent(responseType.getPercent().getValue());
        }

        if (responseType.getQuantity() != null) {
            response.setQuantity(responseType.getQuantity().getValue());
        }

        updateEvidence(response, responseType);
    }

    default void updateEvidence(Response response, ResponseType responseType) {
        if (!CollectionUtils.isEmpty(responseType.getEvidence()) && !CollectionUtils.isEmpty(responseType.getEvidence().get(0).getEvidenceDocumentReference())) {
            AttachmentType attachment = responseType.getEvidence().get(0).getEvidenceDocumentReference().get(0).getAttachment();
            if (attachment != null && attachment.getExternalReference() != null
                    && attachment.getExternalReference().getURI() != null) {
                response.setEvidenceUrl(attachment.getExternalReference().getURI().getValue());
            }
        }
    }



}
