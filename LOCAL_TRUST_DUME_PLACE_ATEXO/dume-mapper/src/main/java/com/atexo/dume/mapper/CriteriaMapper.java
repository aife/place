package com.atexo.dume.mapper;

import com.atexo.dume.dto.CriteriaDTO;
import com.atexo.dume.dto.CriteriaLotDTO;
import com.atexo.dume.dto.ExclusionCriteriaDTO;
import com.atexo.dume.dto.OtherCriteriaDTO;
import com.atexo.dume.dto.RequirementGroupDTO;
import com.atexo.dume.dto.SelectionCriteriaDTO;
import com.atexo.dume.model.Criteria;
import com.atexo.dume.model.CriteriaResponse;
import com.atexo.dume.model.CriteriaSelectionDocument;
import com.atexo.dume.model.DumeResponse;
import com.atexo.dume.model.ExclusionCriteria;
import com.atexo.dume.model.OtherCriteria;
import com.atexo.dume.model.RequirementGroup;
import com.atexo.dume.model.RequirementGroupResponse;
import com.atexo.dume.model.SelectionCriteria;
import isa.names.specification.ubl.schema.xsd.ccv_commonaggregatecomponents_1.CriterionType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.atexo.dume.mapper.XMLConstants.CRITERIA_ID;
import static com.atexo.dume.mapper.XMLConstants.EU_COM_GROW;
import static com.atexo.dume.mapper.XMLConstants.V_1_0;
import static com.atexo.dume.mapper.XMLConstants.V_1_0_2;

/**
 * Mapper Criteria to/From CriteriaDTO
 */
@Mapper(uses = {RequirementGroupMapper.class, LegislationReferenceMapper.class, CriteriaCategorieMapper.class, LotMapper.class})
public interface CriteriaMapper {

    LegislationReferenceMapper legislationReferenceMapper = Mappers.getMapper(LegislationReferenceMapper.class);
    RequirementGroupMapper requirementGroupMapper = Mappers.getMapper(RequirementGroupMapper.class);


    @Mapping(ignore = true, target = "requirementGroupList")
    SelectionCriteriaDTO toDTO(SelectionCriteria selectionCriteria);

    @Mapping(source = "criteria.code", target = "code")
    @Mapping(source = "lotList", target = "lots")
    CriteriaLotDTO toCriteriaLot(CriteriaSelectionDocument criteriaSelectionDocument);

    List<SelectionCriteriaDTO> toSelectionDTOList(List<SelectionCriteria> selectionCriteriaList);

    @Mapping(ignore = true, target = "requirementGroupList")
    ExclusionCriteriaDTO toDTO(ExclusionCriteria exclusionCriteria);

    List<ExclusionCriteriaDTO> toExclusionDTOList(List<ExclusionCriteria> exclusionCriteriaList);

    @Mapping(ignore = true, target = "requirementGroupList")
    OtherCriteriaDTO toDTO(OtherCriteria otherCriteria);

    default String toCode(Criteria criteria) {
        return criteria.getCode();
    }

    @Mapping(ignore = true, target = "requirementGroupList")
    SelectionCriteria toEntity(SelectionCriteriaDTO selectionCriteriaDTO);

    @Mapping(ignore = true, target = "requirementGroupList")
    ExclusionCriteria toEntity(ExclusionCriteriaDTO exclusionCriteriaDTO);


    default SelectionCriteriaDTO toSelectionDTO(CriteriaResponse criteriaResponse) {
        if (criteriaResponse == null) {
            return null;
        }


        SelectionCriteriaDTO selectionCriteriaDTO = toDTO((SelectionCriteria) criteriaResponse.getCriteria());
        selectionCriteriaDTO.setIdCriteriaResponse(criteriaResponse.getId());
        if (criteriaResponse.getCriteria() != null) {
            selectionCriteriaDTO.setEnableECertis(criteriaResponse.getCriteria().getEnableECertis());
        }

        selectionCriteriaDTO.setRequirementGroupList(
                criteriaResponse.getRequirementGroupResponseList().stream()
                        .map(requirementGroupMapper::toDTO)
                        .collect(Collectors.toList())
        );

        return selectionCriteriaDTO;
    }

    default ExclusionCriteriaDTO toExculsionDTO(CriteriaResponse criteriaResponse) {
        if (criteriaResponse == null) {
            return null;
        }

        ExclusionCriteriaDTO exclusionCriteriaDTO = toDTO((ExclusionCriteria) criteriaResponse.getCriteria());
        exclusionCriteriaDTO.setIdCriteriaResponse(criteriaResponse.getId());

        exclusionCriteriaDTO.setRequirementGroupList(
                criteriaResponse.getRequirementGroupResponseList().stream()
                        .map(requirementGroupMapper::toDTO)
                        .collect(Collectors.toList())
        );

        return exclusionCriteriaDTO;
    }

    default OtherCriteriaDTO toOtherDTO(CriteriaResponse criteriaResponse) {
        if (criteriaResponse == null) {
            return null;
        }

        OtherCriteriaDTO otherCriteriaDTO = toDTO((OtherCriteria) criteriaResponse.getCriteria());
        otherCriteriaDTO.setIdCriteriaResponse(criteriaResponse.getId());

        otherCriteriaDTO.setRequirementGroupList(
                criteriaResponse.getRequirementGroupResponseList().stream()
                        .map(requirementGroupMapper::toDTO)
                        .collect(Collectors.toList())
        );
        return otherCriteriaDTO;
    }

    /**
     * Ajout d'un requirement d'un requirementGroup a un criteriaResponse
     *
     * @param criteriaResponse                  criteriaResponse parent
     * @param requirementGroupMapper            Mapper
     * @param requirementGroupResponseListToAdd La list des requirement a ajouter
     * @param groupDTO                          le requirementGroupDto a ajouter aux criters
     */
    default void addRequirementGroupWithCriteria(CriteriaResponse criteriaResponse, RequirementGroupMapper requirementGroupMapper, List<RequirementGroupResponse> requirementGroupResponseListToAdd, RequirementGroupDTO groupDTO) {
        RequirementGroupResponse requirementGroupResponseToAdd = new RequirementGroupResponse();
        requirementGroupResponseToAdd.setRequirementResponseList(new ArrayList<>());

        requirementGroupResponseToAdd.setCriteriaResponse(criteriaResponse);
        Optional<RequirementGroup> optional = criteriaResponse.getCriteria().getRequirementGroupList().stream().
                filter(requirementGroup -> Objects.equals(requirementGroup.getUuid(), groupDTO.getUuid()))
                .findFirst();
        if (optional.isPresent()) {
            requirementGroupResponseToAdd.setRequirementGroup(
                    optional.get());
        }

        requirementGroupMapper.toEntityRequirementGroup(groupDTO, requirementGroupResponseToAdd);
        requirementGroupResponseListToAdd.add(requirementGroupResponseToAdd);
    }


    /**
     * Update des criteriaResponse  à  partir des criteriaDTO
     *
     * @param criteriaDTO
     * @param criteriaResponse
     */
    default void mapCriteria(CriteriaDTO criteriaDTO, CriteriaResponse criteriaResponse) {


        if (!CollectionUtils.isEmpty(criteriaDTO.getRequirementGroupList())) {
            List<RequirementGroupResponse> requirementGroupResponseListToAddOrUpdate = new ArrayList<>();
            for (RequirementGroupDTO groupDTO : criteriaDTO.getRequirementGroupList()) {
                // Ajout/Update des nouvelles requirement Group
                addOrUpdateRequirementGroup(criteriaResponse, requirementGroupResponseListToAddOrUpdate, groupDTO);
            }
            criteriaResponse.getRequirementGroupResponseList().clear();
            criteriaResponse.getRequirementGroupResponseList().addAll(requirementGroupResponseListToAddOrUpdate);
        }

    }

    /**
     * Update or Add a requirement Group
     *
     * @param criteriaResponse                          to be updated
     * @param requirementGroupResponseListToAddOrUpdate a list
     * @param groupDTO
     */
    default void addOrUpdateRequirementGroup(CriteriaResponse criteriaResponse, List<RequirementGroupResponse> requirementGroupResponseListToAddOrUpdate, RequirementGroupDTO groupDTO) {
        if (groupDTO.getIdRequirementGroupResponse() == null) {
            addRequirementGroupWithCriteria(criteriaResponse, requirementGroupMapper, requirementGroupResponseListToAddOrUpdate, groupDTO);
        } else {
            // Update des RequirementGroup
            for (RequirementGroupResponse groupResponse : criteriaResponse.getRequirementGroupResponseList()) {
                if (Objects.equals(groupResponse.getId(), groupDTO.getIdRequirementGroupResponse())) {
                    requirementGroupMapper.toEntityRequirementGroup(groupDTO, groupResponse);
                    requirementGroupResponseListToAddOrUpdate.add(groupResponse);
                }
            }
        }
    }

    // XML
    //criterion
    // criterion ID
    @Mapping(target = "ID.schemeID", constant = CRITERIA_ID)
    @Mapping(target = "ID.schemeAgencyID", constant = EU_COM_GROW)
    @Mapping(target = "ID.schemeVersionID", constant = V_1_0)
    @Mapping(target = "ID.value", source = "uuid")

    // criterion Type Code
    @Mapping(target = "typeCode.listID", constant = "CriteriaTypeCode")
    @Mapping(target = "typeCode.listAgencyID", constant = EU_COM_GROW)
    @Mapping(target = "typeCode.listVersionID", constant = V_1_0_2)
    @Mapping(target = "typeCode.value", source = "code")

    // criterion Name
    @Mapping(target = "name.value", source = "shortName")
    // criterion Description
    @Mapping(target = "description.value", source = "description")
    @Mapping(target = "legislationReference", ignore = true)
    CriterionType toCriterionType(Criteria criteria);

    /**
     * A mapper From Criteria to CriterionType
     * @param criteria
     * @return CriterionType
     */
    default CriterionType toFinalCriterion(Criteria criteria) {
        CriterionType criterionType = toCriterionType(criteria);
        criterionType.getLegislationReference().add(legislationReferenceMapper.toLegislationType(criteria.getLegislationReference()));
        criterionType.getRequirementGroup().addAll(criteria.getRequirementGroupList().stream().map(requirementGroupMapper::toFinalRequirementGroupType).collect(Collectors.toList()));
        return criterionType;
    }

    /**
     * A mapper from CriteriaSelectionDocuement to CriterionType
     * @param criteria
     * @return
     */
    default CriterionType toFinalCriterion(CriteriaSelectionDocument criteria) {

        CriterionType criterionType = toFinalCriterion(criteria.getCriteria());

        if (!CollectionUtils.isEmpty(criteria.getLotList())) {
            StringBuilder stringBuilder = new StringBuilder(" Ce critère s'applique au(x) seul(s) lot(s)");
            criteria.getLotList().forEach(lot -> stringBuilder.append(" ").append(lot.getLotNumber()).append(","));
            stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(","));
            criterionType.getDescription().setValue(criterionType.getDescription().getValue().concat(stringBuilder.toString()));
        }
        return criterionType;

    }

    /**
     * A mapper From CriteriaResponse to CriterionType
     *
     * @param criteriaResponse
     * @return
     */
    default CriterionType toFinalCriterionWithResponse(CriteriaResponse criteriaResponse) {
        CriterionType criterionType = toCriterionType(criteriaResponse.getCriteria());
        criterionType.getLegislationReference().add(legislationReferenceMapper.toLegislationType(criteriaResponse.getCriteria().getLegislationReference()));
        criterionType.getRequirementGroup().addAll(criteriaResponse.getRequirementGroupResponseList().stream().map(requirementGroupMapper::toFinalRequirementGroupWithResponse).collect(Collectors.toList()));
        return criterionType;
    }

    default CriteriaResponse toEntityFromCriterionType(Criteria criteria, DumeResponse dumeResponse, CriterionType criterionType) {
        CriteriaResponse criteriaResponse = new CriteriaResponse();
        criteriaResponse.setCriteria(criteria);
        criteriaResponse.setDumeResponse(dumeResponse);
        criteriaResponse.setRequirementGroupResponseList(requirementGroupMapper.createResponseFromXml(criteriaResponse,
                criterionType.getRequirementGroup(),
                criteria.getRequirementGroupList(), null));

        return criteriaResponse;

    }
}
