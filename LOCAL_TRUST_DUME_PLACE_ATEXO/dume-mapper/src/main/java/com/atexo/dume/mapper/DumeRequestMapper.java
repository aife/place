package com.atexo.dume.mapper;

import com.atexo.dume.dto.DumeRequestContextDTO;
import com.atexo.dume.dto.DumeRequestDTO;
import com.atexo.dume.dto.PublishedDume;
import com.atexo.dume.model.DumeDocument;
import com.atexo.dume.model.DumeRequest;
import com.atexo.dume.model.DumeRequestContext;
import grow.names.specification.ubl.schema.xsd.espdrequest_1.ESPDRequestType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PartyNameType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.atexo.dume.mapper.XMLConstants.COUNTRY_CODE_IDENTIFIER;
import static com.atexo.dume.mapper.XMLConstants.EU_COM_GROW;
import static com.atexo.dume.mapper.XMLConstants.TED;
import static com.atexo.dume.mapper.XMLConstants.V_1_0_2;

/**
 * Mapper DumeRequest to/From DumeRequestDTO
 */
@Mapper(uses = {CriteriaMapper.class, DumeRequestContextMapper.class})
public interface DumeRequestMapper {

    LotMapper lotMapper = Mappers.getMapper(LotMapper.class);
    CriteriaMapper criteriaMapper = Mappers.getMapper(CriteriaMapper.class);
    EconomicPartyInfoMapper economicPartyInfoMapper = Mappers.getMapper(EconomicPartyInfoMapper.class);

    @Mapping(target = "selectionCriteriaCodes", source = "selectionCriteriaList")
    @Mapping(target = "exclusionCriteriaCodes", source = "exclusionCriteriaList")
    DumeRequestDTO toDto(DumeDocument dumeRequest);

    List<DumeRequestDTO> toDtoList(List<DumeRequest> dumeRequestList);

    DumeRequest toEntity(DumeRequestDTO dumeRequestDTO);

    void updateDumeRequestFromContext(DumeRequestContextDTO context, @MappingTarget DumeRequest dumeRequest);

    void updateDumeRequestFromDumeRequestDTO(DumeRequestDTO dumeRequestDTO, @MappingTarget DumeRequest dumeRequest);

    default DumeRequest maptoDumeRequest(ESPDRequestType espdRequestType) {
        final DumeRequest retValue;
        if (null == espdRequestType) {
            retValue = null;
        } else {
            final DumeRequestContext context = new DumeRequestContext();
            retValue = new DumeRequest();
            retValue.setContext(context);
            extractContractingParty(espdRequestType, context);

            if (!CollectionUtils.isEmpty(espdRequestType.getAdditionalDocumentReference())
                    && null != espdRequestType.getAdditionalDocumentReference().get(0).getAttachment()
                    && null != espdRequestType.getAdditionalDocumentReference().get(0).getAttachment().getExternalReference()) {
                if (null != espdRequestType.getAdditionalDocumentReference().get(0).getAttachment().getExternalReference().getFileName()) {
                    context.setEntitled(espdRequestType.getAdditionalDocumentReference().get(0).getAttachment().getExternalReference().getFileName().getValue());
                }
                if (!CollectionUtils.isEmpty(espdRequestType.getAdditionalDocumentReference().get(0).getAttachment().getExternalReference().getDescription())) {
                    context.setObject(espdRequestType.getAdditionalDocumentReference().get(0).getAttachment().getExternalReference().getDescription().get(0).getValue());
                }
            }
            if (espdRequestType.getContractFolderID() != null) {
                context.setConsultRef(espdRequestType.getContractFolderID().getValue());
            }
            context.addDumeDocumentToContext(retValue);
            retValue.setSelectionCriteriaList(new ArrayList<>());
            retValue.setExclusionCriteriaList(new ArrayList<>());
        }
        return retValue;
    }

    default void extractContractingParty(ESPDRequestType espdRequestType, DumeRequestContext context) {
        if (null != espdRequestType.getContractingParty() && null != espdRequestType.getContractingParty().getParty()) {

            if (!CollectionUtils.isEmpty(espdRequestType.getContractingParty().getParty().getPartyName()) &&
                    null != espdRequestType.getContractingParty().getParty().getPartyName().get(0).getName()) {

                context.setOfficialName(espdRequestType.getContractingParty().getParty().getPartyName().get(0).getName().getValue());
            }
            if (null != espdRequestType.getContractingParty().getParty().getPostalAddress()
                    && null != espdRequestType.getContractingParty().getParty().getPostalAddress().getCountry()
                    && null != espdRequestType.getContractingParty().getParty().getPostalAddress().getCountry().getIdentificationCode()) {
                context.setCountry(espdRequestType.getContractingParty().getParty().getPostalAddress().getCountry().getIdentificationCode().getValue());
            }
        }
    }

    // UBL Version
    @Mapping(target = "UBLVersionID.schemeAgencyID", constant = "OASIS-UBL-TC")
    @Mapping(target = "UBLVersionID.value", constant = "2.1")

    // customization ID
    @Mapping(target = "customizationID.schemeName", constant = "CustomizationID")
    @Mapping(target = "customizationID.schemeAgencyID", constant = "BII")
    @Mapping(target = "customizationID.schemeVersionID", constant = "3.0")
    @Mapping(target = "customizationID.value", constant = "urn:www.cenbii.eu:transaction:biitrns070:ver3.0")

    // ID
    @Mapping(target = "ID.schemeID", constant = "ISO/IEC 9834-8:2008 - 4UUID")
    @Mapping(target = "ID.schemeAgencyID", constant = EU_COM_GROW)
    @Mapping(target = "ID.schemeAgencyName", constant = "DG GROW (European Commission)")
    @Mapping(target = "ID.schemeVersionID", constant = "1.1")
    @Mapping(target = "ID.value", expression = "java(java.util.UUID.randomUUID().toString() )")

    //Copy Indicator
    @Mapping(target = "copyIndicator.value", constant = "false")

    // Version ID
    @Mapping(target = "versionID.schemeVersionID", constant = EU_COM_GROW)
    @Mapping(target = "versionID.value", constant = V_1_0_2)

    // issue date & issue time
    // JDK8 Date Time API not supported by mapstruct
    @Mapping(target = "issueDate.value", expression = "java(org.joda.time.LocalDate.now())", dateFormat = "yyyy-MM-dd")
    @Mapping(target = "issueTime.value", expression = "java(org.joda.time.LocalTime.now())", dateFormat = "hh:mm:ss")


    //Contract Folder ID
    @Mapping(target = "contractFolderID.schemeVersionID", constant = TED)
    @Mapping(target = "contractFolderID.value", source = "context.consultRef")

    @Mapping(target = "contractingParty.party.postalAddress.country.identificationCode.value", source = "context.country")
    @Mapping(target = "contractingParty.party.postalAddress.country.identificationCode.listID", constant = COUNTRY_CODE_IDENTIFIER)
    @Mapping(target = "contractingParty.party.postalAddress.country.identificationCode.listName", constant = COUNTRY_CODE_IDENTIFIER)
    @Mapping(target = "contractingParty.party.postalAddress.country.identificationCode.listVersionID", constant = V_1_0_2)
    @Mapping(target = "contractingParty.party.postalAddress.country.identificationCode.listAgencyID", constant = EU_COM_GROW)
    @Mapping(target = "additionalDocumentReference", source = "context")
    ESPDRequestType toEspdDocument(DumeRequest request);

    // xml
    default ESPDRequestType toFinalEspd(final DumeRequest dumeRequest) {
        ESPDRequestType espdRequestType = toEspdDocument(dumeRequest);
        // lots si pas de lot on ajoute un fake lot
        espdRequestType.getProcurementProjectLot().addAll(lotMapper.toLots(dumeRequest.getContext().getLotSet()));
        espdRequestType.getCriterion().addAll(dumeRequest.getSelectionCriteriaList().stream().map(criteriaMapper::toFinalCriterion).collect(Collectors.toList()));
        espdRequestType.getCriterion().addAll(dumeRequest.getExclusionCriteriaList().stream().map(criteriaMapper::toFinalCriterion).collect(Collectors.toList()));
        espdRequestType.getContractingParty().getParty().getPartyName().add(toContractingParty(dumeRequest));
        return espdRequestType;
    }

    @Mapping(target = "dumeSNIdentifier", source = "numDumeSN")
    PublishedDume toPublishedDume(DumeDocument dumeRequest);

    @Mapping(target = "name.value", source = "context.officialName")
    PartyNameType toContractingParty(DumeRequest dumeRequest);
}
