package com.atexo.dume.mapper;

import com.atexo.dume.dto.EconomicPartyInfoDTO;
import com.atexo.dume.model.Country;
import com.atexo.dume.model.EconomicPartyInfo;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.AddressType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.ContactType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PartyType;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.springframework.util.CollectionUtils;

@Mapper(uses = EnumMapper.class)
public interface EconomicPartyInfoMapper {

    EconomicPartyInfoDTO toDTO(EconomicPartyInfo economicPartyInfo);

    void updateEconomicPartyInfo(EconomicPartyInfoDTO economicPartyInfoDTO, @MappingTarget EconomicPartyInfo economicPartyInfo);

    default EconomicPartyInfo getEconomicPartyInfo(PartyType partyType) {
        EconomicPartyInfo economicPartyInfo = new EconomicPartyInfo();
        if (partyType != null) {

            // Extraction du Nom de l'operateur economique
            if (!CollectionUtils.isEmpty(partyType.getPartyName()) &&
                    partyType.getPartyName().get(0).getName() != null) {
                economicPartyInfo.setNom(partyType.getPartyName().get(0).getName().getValue());
            }
            // Extraction de l'addresse de l'operateur economique
            updateAddressEconomicPartyInfo(partyType, economicPartyInfo);
            // Extraction du site internet
            if (partyType.getWebsiteURI() != null) {
                economicPartyInfo.setSiteInternet(partyType.getWebsiteURI().getValue());
            }

            // Extraction du Contact
            extractionDuContact(partyType, economicPartyInfo);

            // Extraction du Numero TVA
            if (!CollectionUtils.isEmpty(partyType.getPartyIdentification())) {
                if (partyType.getPartyIdentification().get(0).getID() != null) {
                    economicPartyInfo.setNumTVA(partyType.getPartyIdentification().get(0).getID().getValue());
                }
                if (partyType.getPartyIdentification().size() > 1
                        && partyType.getPartyIdentification().get(1).getID() != null) {
                    economicPartyInfo.setNumEtrange(partyType.getPartyIdentification().get(1).getID().getValue());

                }
            }

        }

        return economicPartyInfo;
    }

    default void extractionDuContact(PartyType partyType, EconomicPartyInfo economicPartyInfo) {
        if (partyType.getContact() != null) {
            ContactType contactType = partyType.getContact();

            if (contactType.getName() != null) {
                economicPartyInfo.setContact(contactType.getName().getValue());
            }

            if (contactType.getTelephone() != null) {
                economicPartyInfo.setTelephone(contactType.getTelephone().getValue());
            }

            if (contactType.getElectronicMail() != null) {
                economicPartyInfo.setEmail(contactType.getElectronicMail().getValue());
            }
        }
    }

    default void updateAddressEconomicPartyInfo(PartyType partyType, EconomicPartyInfo economicPartyInfo) {
        if (partyType.getPostalAddress() != null) {
            AddressType addressType = partyType.getPostalAddress();
            if (addressType.getStreetName() != null) {
                economicPartyInfo.setRue(addressType.getStreetName().getValue());
            }

            if (addressType.getCityName() != null) {
                economicPartyInfo.setVille(addressType.getCityName().getValue());
            }

            if (addressType.getPostalZone() != null) {
                economicPartyInfo.setCodePostal(addressType.getPostalZone().getValue());
            }

            if (addressType.getCountry() != null && addressType.getCountry().getIdentificationCode() != null) {
                    economicPartyInfo.setPays(Country.findByIso2Code(addressType.getCountry().getIdentificationCode().getValue()));
            }
        }
    }
}
