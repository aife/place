package com.atexo.dume.mapper;

import com.atexo.dume.dto.EnumDTO;
import com.atexo.dume.model.Country;
import com.atexo.dume.model.Currency;
import com.atexo.dume.model.MarketType;

public class EnumMapper {

    private EnumMapper() {
    }

    public static EnumDTO currencyToDTO(Currency currency) {
        if (currency == null) {
            return null;
        }
        return new EnumDTO(currency.name(), currency.getDescription());
    }

    public static EnumDTO marketToDTO(MarketType marketType) {
        if (marketType == null) {
            return null;
        }
        return new EnumDTO(marketType.getCode(), marketType.name());
    }


    public static EnumDTO countryToDTO(Country country) {
        if (country == null) {
            return null;
        }
        return new EnumDTO(country.getIso2Code(), country.getCountryName());
    }

    public static Currency enumToCurrency(EnumDTO enumDTO) {
        if (enumDTO == null) {
            return null;
        }
        if (enumDTO.getCode() == null) {
            return null;
        }
        return Currency.valueOf(enumDTO.getCode());
    }

    public static Country enumToCountry(EnumDTO enumDTO) {
        if (enumDTO == null) {
            return null;
        }
        if (enumDTO.getCode() == null) {
            return null;
        }
        return Country.findByIso2Code(enumDTO.getCode());
    }

}
